Param(
  [string]$version,
  [string]$apikey
)

Add-Type -AssemblyName System.IO.Compression.Filesystem

$REPOSITORY_ROOT = "https://artifactory.deluxe.com:443/tmsa.r360"

$DEPLOY_SCRIPTS = 
    "killAll.ps1",
    "File Deploy RecHub.ps1"

function get-file ($local:url, $local:dest) {
    $uri = new-object system.uri($REPOSITORY_ROOT + $local:url)
    $authheader = @{"X-JFrog-Art-Api"=$script:apikey}
    write-host "downloading file:" $uri
	[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    invoke-webrequest -uri $uri -outfile $local:dest -headers $authheader
}

# Grab our release package
$root = (get-item -path ".\" -verbose).fullname
$file = "/Releases/" + "Release-" + $version + ".zip"
$extractdir = $root + "/Release-" + $version
$zipfile = $extractdir + ".zip"

# Delete older folders/zips if they exist.
if (test-path $zipfile) {
	remove-item $zipfile -force
}
if (test-path $extractdir) {
	remove-item $extractdir -force -recurse
}

# Download the file
write-host "downloading release"
get-file $file $zipfile

# Unzip it
write-host "unzipping release"
[System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile,$extractdir)

# Copy everything to our root staging folder.
write-host "copying out unzipped files"
copy-item ($extractdir + "/*") $root -force -recurse

# Run our commands specified above.
foreach ($script in $DEPLOY_SCRIPTS) {
	$command = '.\' + '"' + "$script" + '"' + ' -Silent' + " -Version $version"
    write-host "Running command '$command'..."
    invoke-expression "& $command"
}