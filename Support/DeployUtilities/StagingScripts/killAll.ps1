param (
    [string] $path = "D:\WFSApps"
)

$output = .\handle $path -accepteula | where { !($_ -match "explorer.exe") }
$matches = [regex]::matches($output, "pid:\s(\d+)")
$matches | % {
	$match = $_
	$id = $match.captures.groups[1].value
	if ($id) {
		write-host "Killing process $id"
		try {
			stop-process $id -force
		}
		catch {
			write-host "Error Killing $id. Moving on."
		}
	}
}
write-host "Waiting for 10 seconds to allow processes to die..."
start-sleep -seconds 10