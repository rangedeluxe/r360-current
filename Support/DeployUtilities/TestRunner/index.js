const a = require('minimist')(process.argv.slice(2));
const request = require('web-request');
const fs = require('async-file');
const path = require('path');
const rm = require('rimraf');
const exec = require('child_process').exec;

(async(args) => {
    
    // first, clear out our tests.
    console.log(`Clearing workspace...`);
    const testroot = "./tests";
    const resultsroot = "./results";
    if (await fs.exists(testroot))
        rm.sync(testroot);
    await fs.mkdir(testroot);
    if (await fs.exists(resultsroot))
        rm.sync(resultsroot);
    await fs.mkdir(resultsroot);

    // download the list of files from artifactory.
    console.log(`Retrieving list of files...`);
    const apikey = args.apikey;
    const url = "http://artifactory.deluxe.com/api/storage/tmsa.r360/E2E/?list&deep=1&listFolders=0";
    const opts = {
        headers: {
            'Content-Type': 'application/json',
            'X-JFrog-Art-Api': apikey
        }
    };
    const result = (await request.get(url, opts)).body;
    const files = JSON.parse(result).files;

    // create the folder structure
    console.log(`Creating folder structure...`);
    for (let f of files) {
        const dir = path.dirname(f.uri);
        if (!(await fs.exists(`${testroot}${dir}`)))
            await fs.mkdir(`${testroot}${dir}`);
    }

    // download the actual files from artifactory (all at the same time)
    console.log(`Downloading all files...`);
    const downloadurl = "http://artifactory.deluxe.com/tmsa.r360/E2E";
    const work = files.map((f) => {
        return new Promise(async(res) => {
            const dlreq = request.stream(`${downloadurl}${f.uri}`);
            const stream = fs.createWriteStream(`${testroot}${f.uri}`);
            dlreq.pipe(stream);
            await dlreq.response;
            stream.on('finish', () => res(true));
        });
    });
    await Promise.all(work);

    // Update the webdriver
    console.log(`Updating webdriver...`);
    await new Promise((res) => {
        exec('npm run update', (err, stdout, stderr) => {
            res(true);
        });
    });

    // Run the tests
    console.log(`Running tests...`);
    await new Promise((res) => {
        exec('npm run test', (err, stdout, stderr) => {
            res(true);
        });
    });
})(a);