# Deployment Utilities
These tools allow Jenkins to deploy our code out to our environments without any interaction.

There are a few steps needed to take to enable this on a tier(server).

### 1. Installing Command
"Command" is a very small remote-code-execution tool, and allows jenkins to send command to a server to be executed.

To build Command:
```
cd command
npm i
npm run build
```
Now a command.exe will exist.

Just copy these files:
```
command/command.exe
command/nssm.exe
command/install.ps1
command/uninstall.ps1
```
Out into the tier (for example: rechubdevweb02.qalabs.nwk) into folder "D:\WFSApps\Command"

Note: It doesn't actually matter where you put these files, but that's where I have put them previously.

**Question:** What is nssm? "The Non-Sucking Service Manager". Allows any executable to run as a service. There is an uninstall powershell script that can be ran as well, if one wishes to uninstall the service. https://nssm.cc/

Then on that tier, run the install script as administrator:
```
PS > .\install.ps1
```

### 2. Copy a few powershell files
If you're on the webtier, copy these:
```
StagingScripts/deployWeb.ps1
killAll.ps1
StagingScripts/handle.exe
```
Into D:\WFSStaging

Likewise, if you're on the apptier, copy these:
```
StagingScripts/deployApp.ps1
killAll.ps1
StagingScripts/handle.exe
```

And so on and so forth, depending on the tier you're working on.

**Question:** What is handle? It's a small microsoft utility that gives us information about which processes are blocking files.  https://docs.microsoft.com/en-us/sysinternals/downloads/handle

## You're Done!

### Troubleshooting
Some of our tiers, maybe our older tiers, did **not** have the deluxe domain certificate authority installed.  I had to install the certificate from artifactory.deluxe.com (the Root CA one) under trusted root authorities. This gives the tier the ability to download our ZIP packages.