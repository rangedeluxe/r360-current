# What am I looking at?
This is a very small deployment utility that allows someone to execute commands on a remote machine.

## Dependencies
Requires NodeJS to be installed.

## Building
Just run:
```
npm run build
```

## Usage
This application starts up a WebAPI that listens to 'localhost:8000/execute'.  Just pass a command that looks like this to it:
```json
{
    "command": "dir"
}
```

And copy the exe out to any machine you want this available on. This should NOT be deployed to a customer, and is designed to be an internal tool only.