// Designed to be a very small tool to be ran inside of a jenkins build.

let a = require('minimist')(process.argv.slice(2));
let request = require('web-request');

(async(args) => {
    let options = {
        r360version: args.r360version,
        frameworkversion: args.frameworkversion,
        raamversion: args.raamversion,
        apikey: args.apikey,
        tiers: args.tiers,
        raambranch: args.raambranch || 'r360-dev',
        r360branch: args.r360branch || 'dev',
        frameworkbranch: args.frameworkbranch || 'r360-dev'
    };
    console.log("Options: ");
    console.dir(options);

    let tiers = 
          options.tiers == "Dev" ? { web: "rechubdevweb02.qalabs.nwk", app: "rechubdevapp02.qalabs.nwk", file: "rechubdevimg02.qalabs.nwk", client: "rechubdevclnt02.qalabs.nwk", db: "rechubdevdb02.qalabs.nwk" }
        : options.tiers == "RecHub" ? { web: "rechubqaweb02.qalabs.nwk", app: "rechubqaapp02.qalabs.nwk", file: "rechubqaimg02.qalabs.nwk", client: "rechubqaclnt02.qalabs.nwk", db: "rechubqadb02.qalabs.nwk" }
        : options.tiers == "Raam" ? { web: "raamqaweb01.qalabs.nwk", app: "raamqaapp01.qalabs.nwk", file: "raamqanoiis.qalabs.nwk", client: "raamqaclnt01.qalabs.nwk", db: "raamqadb01.qalabs.nwk" }
        : options.tiers == "2016" ? { web: "r36016qaweb01.qalabs.nwk", app: "r36016qaapp01.qalabs.nwk", file: "r36016qafile01.qalabs.nwk", client: "r36016qaclnt01.qalabs.nwk", db: "r36016qadb01.qalabs.nwk" }
        : null;

    let dbname = options.tiers == "Dev" ? "Current_R360_NP" : "WFSDB_R360";
    let fwname = options.tiers == "Dev" ? "Current_Framework" : "WFSPortal";
    let raamname = options.tiers == "Dev" ? "Current_RAAM" : "RAAM";
    let dbversion = options.tiers == "2016" ? "2016" : "2012";
    let dbargs = `--dbargs="-DBSERVER localhost -R360DB ${dbname} -RAAMDB ${raamname} -Verbose 0 -SQLVersion ${dbversion}"`;
    let raamdbargs = `--raamdbargs="-DBSERVER localhost -RAAMDB ${raamname} -Verbose 0 -SQLVersion ${dbversion} -INSTALLFOLDER D:\\WFSStaging\\DB\\${args.raamversion} -LOGFOLDER  D:\\WFSStaging\\DB\\Installlogs"`;

    let clientdrive = options.tiers == "RecHub" ? "E" : "D";
    let filedrive = options.tiers == "Raam" ? "E" : "D";
    let filetier = options.tiers == "Raam" ? "noiis" : "file";

    if (!tiers) {
        console.log("Error.  Invalid environment selection.");
        return 1;
    }
    console.log("Tiers: ");
    console.dir(tiers);

    let headers = {
        'Content-Type': 'application/json'
    };
    let opts = { headers: headers };

    // Explaination - we're going to run the DB first, then APP, then FILE, the other tiers.
    // The APP tier relies on the DB tier. The CLIENT tier relies on the FILE tier, the WEB and CLIENT tiers can be ran together. 
    // This is to help prevent issues like the DIT client service from booting up because it can't log in correctly.

    console.log("Executing DB tier...");
    let results = await request.post(`http://${tiers.db}:8000/execute`, opts, JSON.stringify({ 
        "command": `install-r360.exe --r360branch ${options.r360branch} --frameworkbranch ${options.frameworkbranch} --raambranch ${options.raambranch} --r360v ${options.r360version} --frameworkv ${options.frameworkversion} --raamv ${options.raamversion} --d D --t db --dl ${dbargs} ${raamdbargs}`
    }));

    console.log("DB Tier Results:");
    console.log(JSON.parse(results.body).stdout);
    console.log(`Errors: ${JSON.parse(results.body).stderr}`);

    console.log("");
    console.log("Executing APP Tier...");
    results = await request.post(`http://${tiers.app}:8000/execute`, opts, JSON.stringify({ 
        "command": `install-r360.exe --r360branch ${options.r360branch} --frameworkbranch ${options.frameworkbranch} --raambranch ${options.raambranch} --r360v ${options.r360version} --frameworkv ${options.frameworkversion} --raamv ${options.raamversion} --d D --t app --dl`
    }));
    console.log("APP Tier Results:");
    console.log(JSON.parse(results.body).stdout);
    console.log(`Errors: ${JSON.parse(results.body).stderr}`);

    console.log("");
    console.log("Executing FILE Tier...");
    results = await request.post(`http://${tiers.file}:8000/execute`, opts, JSON.stringify({ 
        "command": `install-r360.exe --r360branch ${options.r360branch} --frameworkbranch ${options.frameworkbranch} --raambranch ${options.raambranch} --r360v ${options.r360version} --frameworkv ${options.frameworkversion} --raamv ${options.raamversion} --d ${filedrive} --t ${filetier} --dl`
    }));
    console.log("FILE Tier Results:");
    console.log(JSON.parse(results.body).stdout);
    console.log(`Errors: ${JSON.parse(results.body).stderr}`);

    console.log("");
    console.log("Executing WEB and CLIENT tiers...");

    let work = [
        request.post(`http://${tiers.web}:8000/execute`, opts, JSON.stringify({
            "command": `install-r360.exe --r360branch ${options.r360branch} --frameworkbranch ${options.frameworkbranch} --raambranch ${options.raambranch} --r360v ${options.r360version} --frameworkv ${options.frameworkversion} --raamv ${options.raamversion} --d D --t web --dl`
        })),
        request.post(`http://${tiers.client}:8000/execute`, opts, JSON.stringify({ 
            "command": `install-r360.exe --r360branch ${options.r360branch} --frameworkbranch ${options.frameworkbranch} --raambranch ${options.raambranch} --r360v ${options.r360version} --frameworkv ${options.frameworkversion} --raamv ${options.raamversion} --d ${clientdrive} --t client --dl`
        }))
    ];

    results = await Promise.all(work);
    
    console.log("");
    console.log("WEB Tier Results: ");
    console.log(JSON.parse(results[0].body).stdout);
    console.log(`Errors: ${JSON.parse(results[0].body).stderr}`);

    console.log("");
    console.log("CLIENT Tier Results: ");
    console.log(JSON.parse(results[1].body).stdout);
    console.log(`Errors: ${JSON.parse(results[1].body).stderr}`);

})(a);