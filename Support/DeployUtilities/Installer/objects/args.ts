export class args {
    r360version: string;
    r360branch: string;
    drive: string;
    tier: string;
    cert: string;
    download: boolean;
    dbargs: string;
    raamdbargs: string;
}