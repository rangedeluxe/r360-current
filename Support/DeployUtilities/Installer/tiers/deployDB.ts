import { Engine, unpack, unzip, stopService, deleteService, deleteSite, killAll, copy, mkdir, newAppPool, newSite, newApp, 
    addSSL, newService, startService } 
    from "nexe-deploy-utils";
import { gatherReleases } from "../utils/gatherReleases";
import { Build } from "nexe-deploy-utils/deployUtils/engine";
import { exec } from '../utils/exec';
import { args } from "../objects/args";

export function getDBBuild(root:string, args:args) : Build {

    return new Engine().describe('R360 DB-Tier Deployment')
        .step(() => gatherReleases(args.download, args.r360version, args.r360branch,false), "Gathering Resource Zip Files")
        .step(() => unpack('./node_modules/nexe-deploy-utils/bin/handle64.exe', './handle64.exe'), 'Unpacking Handle')
        .step(() => unzip(`./${args.r360version}.zip`, `${args.r360version}`), 'Unzipping R360')
        .step(() => copy(`./${args.r360version}/DB`, './DB', false), 'Copying DB to Root, ignoring Empty')
        .step(() => exec(`powershell -file ".\\DB\\${args.r360version}\\Install ${args.r360version}Patch.ps1" ${args.dbargs}`, true, false), 'Running RecHub DB Install Script');
}