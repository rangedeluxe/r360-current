import { Engine, unpack, unzip, stopService, deleteService, deleteSite, killAll, copy, mkdir, newAppPool, newSite, newApp, 
    addSSL, newService, startService } 
    from "nexe-deploy-utils";
import { gatherReleases } from "../utils/gatherReleases";
import { Build } from "nexe-deploy-utils/deployUtils/engine";
import { tryStartService } from "../utils/tryStartService";
import { tryStopService } from "../utils/tryStopService";
import { filterConfig } from "../utils/filterConfig";
import { args } from "../objects/args";

export function getClientBuild(root:string, args:args) : Build {

    return new Engine().describe('R360 Client-Tier Deployment')
        .step(() => gatherReleases(args.download, args.r360version,args.r360branch,false), "Gathering Resource Zip Files")
        .step(() => unpack('./node_modules/nexe-deploy-utils/bin/handle64.exe', './handle64.exe'), 'Unpacking Handle')
        .step(() => unzip(`./${args.r360version}.zip`, `${args.r360version}`), 'Unzipping R360')
        .step(() => tryStopService('DataImportClientSvc'), 'Stopping DataImportClientSvc')
        .step(() => tryStopService('FileImportClientSvc'), 'Stopping FileImportClientSvc')
        .step(() => killAll(root), 'Killing All Handles')
        .step(() => copy(`./${args.r360version}/CLIENT-DIT/${args.r360version}/bin`, `${root}rechub/bin`, false, filterConfig), 'Copying DIT Services')
        .step(() => copy(`./${args.r360version}/CLIENT-FIT/${args.r360version}/bin`, `${root}rechub/bin`, false, filterConfig), 'Copying FIT Services')
        .step(() => copy(`./${args.r360version}/CLIENT-EXTRACT/${args.r360version}/bin2`, `${root}rechub/ExtractDesignManager/bin2`, false, filterConfig), 'Copying EXTRACT Services')
        .step(() => copy(`./${args.r360version}/CLIENT-DIT/${args.r360version}/R360DIT.mdtemplate`, `${root}rechub/R360DIT.mdtemplate`, false, null), 'Copying DIT Service Configurator Templates')
        .step(() => copy(`./${args.r360version}/CLIENT-DIT/${args.r360version}/R360DITConsole.mdtemplate`, `${root}rechub/R360DITConsole.mdtemplate`, false, null), 'Copying DIT Console Configurator Templates')
        .step(() => copy(`./${args.r360version}/CLIENT-FIT/${args.r360version}/R360FIT.mdtemplate`, `${root}rechub/R360FIT.mdtemplate`, false, null), 'Copying FIT Service Configurator Templates')
        .step(() => copy(`./${args.r360version}/CLIENT-FIT/${args.r360version}/R360FITConsole.mdtemplate`, `${root}rechub/R360FITConsole.mdtemplate`, false, null), 'Copying FIT Console Configurator Templates')
        .step(() => copy(`./${args.r360version}/CLIENT-EXTRACT/${args.r360version}/R360EDM.mdtemplate`, `${root}rechub/R360EDM.mdtemplate`, false, null), 'Copying Extract Configurator Templates')
        .step(() => tryStartService('DataImportClientSvc'), 'Starting DataImportClientSvc')
        .step(() => tryStartService('FileImportClientSvc'), 'Starting FileImportClientSvc')
}