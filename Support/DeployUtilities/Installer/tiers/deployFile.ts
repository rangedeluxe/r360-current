import {
    Engine, unpack, unzip, stopService, deleteService, deleteSite, killAll, copy, mkdir, newAppPool, newSite, newApp,
    addSSL, newService, startService, write
}
    from "nexe-deploy-utils";
import { gatherReleases } from "../utils/gatherReleases";
import { Build } from "nexe-deploy-utils/deployUtils/engine";
import { tryStopService } from "../utils/tryStopService";
import { tryStartService } from "../utils/tryStartService";
import { filterConfig } from "../utils/filterConfig";
import { args } from "../objects/args";
import { exec } from "../utils/exec";

const scriptVersion = "1.02";

export function getFileBuild(root: string, args: args): Build {
    const deploypath = `${root}RecHub/DitContainer/deploy/res/`;
    const mssm = `${deploypath}Dependencies/nssm.exe`;
    const configurator = `${root}DLXConfig\\DLXConfigCmd.exe`;
    
    return new Engine().describe(`R360 File-Tier Deployment (Script Version ${scriptVersion})`)
        .step(() => gatherReleases(args.download, args.r360version, args.r360branch,false),
            "Gathering Resource Zip Files")
        .step(() => unpack('./node_modules/nexe-deploy-utils/bin/handle64.exe', './handle64.exe'), 'Unpacking Handle')
        .step(() => unzip(`./${args.r360version}.zip`, `${args.r360version}`), 'Unzipping R360')
        .step(() => copy(`./${args.r360version}/DitContainer/${args.r360version}/InstallDitContainer.exe`, `${root}RecHub\\DitContainer\\InstallDitContainer.exe`), 
            `Copying ACH container installer FROM: ./${args.r360version}/DitContainer/${args.r360version}/InstallDitContainer.exe TO: ${root}RecHub\\DitContainer\\InstallDitContainer.exe`)
        .step(() => tryStopService('FileCleanupSvc'), 'Stopping FileCleanupSvc')
        .step(() => tryStopService('OLFImageSvc'), 'Stopping OLFImageSvc')
        .step(() => tryStopService('FilePurgeSvc'), 'Stopping FilePurgeSvc')
        .step(() => tryStopService('ExtractProcessService'), 'Stopping ExtractProcessService')
        .step(() => tryStopService('achservice'), 'Stopping ACH Service')
        .step(() => killAll(root), 'Killing All Handles')
        .step(() => copy(`./${args.r360version}/FILE/${args.r360version}/wwwroot`, `${root}rechub/wwwroot`, false, filterConfig), 'Copying All R360 Web Applications')
        .step(() => copy(`./${args.r360version}/FILE/${args.r360version}/bin2`, `${root}rechub/bin2`, false, filterConfig), 'Copying All Services')
        .step(() => copy(`./${args.r360version}/FILE/${args.r360version}/R360EDM.mdtemplate`, `${root}rechub/R360EDM.mdtemplate`, false, null), 'Copying Extract Configurator Templates')
        .step(() => copy(`./${args.r360version}/FILE/${args.r360version}/RecHub.mdtemplate`, `${root}rechub/RecHub.mdtemplate`, false, null), 'Copying RecHub Configurator Templates')
        .step(async() => await exec(`${mssm} stop achservice`, true, false))
        .step(async() => await exec(`${mssm} remove achservice confirm`, true, false))
        .step(() => exec(`${root}/RecHub/DitContainer/InstallDitContainer.exe --silentMode`), 'Installing Dit Container')
        .step(() => exec(`${configurator} Import ${root}RecHub\\RecHub.dlxconfig`), 'Executing Configurator')
        .step(async() => await exec(`${mssm} install achservice ${deploypath}/ACHService/ACHService.exe`, true, false))
        .step(async() => await exec(`${mssm} start achservice`, true, false))
        .step(() => tryStartService('FileCleanupSvc'), 'Starting FileCleanupSvc')
        .step(() => tryStartService('OLFImageSvc'), 'Starting OLFImageSvc')
        .step(() => tryStartService('FilePurgeSvc'), 'Starting FilePurgeSvc')
        .step(() => tryStartService('ExtractProcessService'), 'Starting ExtractProcessService');        
}