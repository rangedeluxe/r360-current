import { Engine, unpack, unzip, stopService, deleteService, deleteSite, killAll, copy, mkdir, newAppPool, newSite, newApp, 
    addSSL, newService, startService, stopSite, startSite }
    from "nexe-deploy-utils";
import { gatherReleases } from "../utils/gatherReleases";
import { Build } from "nexe-deploy-utils/deployUtils/engine";
import { filterConfig } from "../utils/filterConfig";
import { args } from "../objects/args";

export function getWebBuild(root:string, args:args) : Build {

    return new Engine().describe('R360 Web-Tier Deployment')
        .step(() => gatherReleases(args.download, args.r360version, args.r360branch,false), "Gathering Resource Zip Files")
        .step(() => unpack('./node_modules/nexe-deploy-utils/bin/handle64.exe', './handle64.exe'), 'Unpacking Handle')
        .step(() => unzip(`./${args.r360version}.zip`, `${args.r360version}`), 'Unzipping R360')
        .step(() => stopService('FileCleanupSvc', false, false), 'Stopping File Cleanup Service')
        .step(() => stopSite('R360'), 'Stopping R360 Site')
        .step(() => killAll(root), 'Killing All Handles')
        .step(() => copy(`./${args.r360version}/WEB/${args.r360version}/wwwroot`, `${root}rechub/wwwroot`, false, filterConfig), 'Copying All R360 Web Applications')
        .step(() => copy(`./${args.r360version}/WEB/${args.r360version}/bin2`, `${root}rechub/bin2`, false, filterConfig), 'Copying All Services')
        .step(() => copy(`./${args.r360version}/WEB/${args.r360version}/RecHub.mdtemplate`, `${root}rechub/RecHub.mdtemplate`, false, null), 'Copying Configurator Templates')
        .step(() => startSite('R360'), 'Starting R360 Site')
        .step(() => startService('FileCleanupSvc'), 'Starting File Cleanup Service');
}
