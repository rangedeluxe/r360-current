import { Engine, unpack, unzip, stopService, deleteService, deleteSite, killAll, copy, mkdir, newAppPool, newSite, newApp, 
    addSSL, newService, startService } 
    from "nexe-deploy-utils";
import { gatherReleases } from "../utils/gatherReleases";
import { Build } from "nexe-deploy-utils/deployUtils/engine";
import { tryStartService } from "../utils/tryStartService";
import { tryStopService } from "../utils/tryStopService";
import { filterConfig } from "../utils/filterConfig";
import { args } from "../objects/args";

export function getFileNoIISBuild(root:string, args:args) : Build {

    return new Engine().describe('R360 File-Tier No IIS Deployment')
        .step(() => gatherReleases(args.download, args.r360version, args.r360branch,true), "Gathering Resource Zip Files")
        .step(() => unpack('./node_modules/nexe-deploy-utils/bin/handle64.exe', './handle64.exe'), 'Unpacking Handle')
        .step(() => unzip(`./${args.r360version}.zip`, `${args.r360version}`), 'Unzipping R360')
        .step(() => tryStopService('FileCleanupSvc'), 'Stopping FileCleanupSvc')
        .step(() => tryStopService('OLFImageSvc'), 'Stopping OLFImageSvc')
        .step(() => tryStopService('FilePurgeSvc'), 'Stopping FilePurgeSvc')
        .step(() => tryStopService('ExtractProcessService'), 'Stopping ExtractProcessService')
        .step(() => tryStopService('DataImportSvc'), 'Stopping DataImportSvc')
        .step(() => tryStopService('FileImportSvc'), 'Stopping FileImportSvc')
        .step(() => tryStopService('FileImportUploadSvc'), 'Stopping FileImportUploadSvc')
        .step(() => tryStopService('ICONServices'), 'Stopping ICONServices')
        .step(() => tryStopService('OLFServices'), 'Stopping OLFServices')
        .step(() => tryStopService('OLFOnlineServices'), 'Stopping OLFOnlineServices')
        .step(() => tryStopService('OLFServicesExternal'), 'Stopping OLFServicesExternal')
        .step(() => tryStopService('RecHubAuditingServices'), 'Stopping RecHubAuditingServices')
        .step(() => killAll(root), 'Killing All Handles')
        .step(() => copy(`./${args.r360version}/FILE/${args.r360version}/wwwroot`, `${root}rechub/wwwroot`, false, filterConfig), 'Copying All R360 Web Applications')
        .step(() => copy(`./${args.r360version}/FILE/${args.r360version}/bin2`, `${root}rechub/bin2`, false, filterConfig), 'Copying All Services')
        .step(() => tryStartService('FileCleanupSvc'), 'Starting FileCleanupSvc')
        .step(() => tryStartService('OLFImageSvc'), 'Starting OLFImageSvc')
        .step(() => tryStartService('FilePurgeSvc'), 'Starting FilePurgeSvc')
        .step(() => tryStartService('ExtractProcessService'), 'Starting ExtractProcessService')
        .step(() => tryStartService('DataImportSvc'), 'Starting DataImportSvc')
        .step(() => tryStartService('FileImportSvc'), 'Starting FileImportSvc')
        .step(() => tryStartService('FileImportUploadSvc'), 'Starting FileImportUploadSvc')
        .step(() => tryStartService('ICONServices'), 'Starting ICONServices')
        .step(() => tryStartService('OLFServices'), 'Starting OLFServices')
        .step(() => tryStartService('OLFOnlineServices'), 'Starting OLFOnlineServices')
        .step(() => tryStartService('OLFServicesExternal'), 'Starting OLFServicesExternal')
        .step(() => tryStartService('RecHubAuditingServices'), 'Starting RecHubAuditingServices');
}