import {write} from 'nexe-deploy-utils';

// A little filter function to NOT copy out things like config files, database files, etc.
//  -- Things that should probably be handled by the configurator tool.
export function filterConfig(src:string, dest:string) : Promise<boolean> {
    return new Promise<boolean>((res) => {
        let result = !(
            /\.config$/.test(src) 
            || /\.json$/.test(src) 
            || /\.sdf$/.test(src));
        write(`filterConfig: File "${src}". Filter: ${result}`, null, false);
        res(result);
    });
}