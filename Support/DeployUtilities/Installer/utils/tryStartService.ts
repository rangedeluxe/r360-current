import {startService, write} from 'nexe-deploy-utils';

// some of our windows services can take quite a while to boot up, and windows reports them as failures.
// so we're going to wrap some of that code up here.
export async function tryStartService(name:string) : Promise<boolean> {
    return new Promise<boolean>(async (res, rej) => {
        let result = false;
        let attempts = 0;
        let total = 10;
        while (!result && ++attempts !== total) {
            write(`tryStartService: Attempt ${attempts} on service ${name}`, null, false);
            result = await startService(name);
            if (!result) {
                let millis = 20000;
                write(`tryStartService: Waiting for  ${millis} on service ${name}`, null, false);
                await wait(millis);
            }
        }
        res(result);
    });
}

let wait = (millis:number) : Promise<boolean> => {
    return new Promise<boolean>((res) => {
        setTimeout(() => res(true), millis);
    });
}