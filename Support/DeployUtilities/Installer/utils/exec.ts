const execcmd = require('child_process').exec;
import {write} from 'nexe-deploy-utils';

export async function exec(cmd:string, verbose: boolean = true, failonerror: boolean = true) : Promise<boolean> {
    return new Promise<boolean>(async (res, rej) => {
        execcmd(cmd, (err:string, stdout:string, stderr:string) => {
            if ((err && err.length > 0) || (stderr && stderr.length > 0) ) {
                write(`exec: error running command '${cmd}'. ${err}`);
                write(stderr);
                res(!failonerror);
            }
            else {
                write(stdout, null, verbose);
                res(true);
            }
        });
    });
}