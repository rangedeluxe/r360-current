import {startService, write, stopService} from 'nexe-deploy-utils';
let exec = require('child_process').exec;

// some of our windows services can also be very stubborn when attempting to kill.
// so we're going to wrap some force killing func around it.
export async function tryStopService(name:string) : Promise<boolean> {
    return new Promise<boolean>(async (res, rej) => {
        let result = false;
        let attempts = 0;
        let total = 10;
        while (!result && ++attempts !== total) {
            write(`tryStopService: Attempt ${attempts} on service ${name}`, null, false);
            result = await stopService(name, false, false);
            if (!result) {
                await killProcess(`${name}.exe`);
            }
        }
        res(result);
    });
}

let killProcess = async (name:string) : Promise<boolean> => {
    return new Promise<boolean>((res) => {
        let cmd = `taskkill /f /im ${name}`;
        write(`killProcess: Attempting to kill process ${name}.`, null, false);
        exec(cmd, (err:string, stdout:string, stderr:string) => {
            write(`output: ${stdout}`, null, false);
            res(true);
        });
    });
}