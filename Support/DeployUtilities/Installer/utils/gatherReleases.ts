import {downloadFile, unpack} from 'nexe-deploy-utils';
import {write} from 'nexe-deploy-utils';

// a little functions to download or unpack all of the releases asynchronously.
// returns false if any one of the releases to download/unpack.
export async function gatherReleases(download:boolean, r360version:string,
    r360branch:string, downloadDitContainer:boolean) : Promise<boolean> {
    return new Promise<boolean>(async (res, rej) => {
        let results;
        var numb = r360version.match(/\d[.]*/g).join('');
        
        write(`gatherReleases: version numbers = "${numb}"`,null, true, true);
        write(`gatherReleases: Download set to '${download}'.`, null, false);        
        if (download) {
            let r360url = `http://artifactory.deluxe.com/tmsa.r360/${r360branch}/Releases/Release-${r360version}.zip`;   
            let ditcontainer =`http://artifactory.deluxe.com/tmsa.r360/${r360branch}/DitContainer/${r360version}/InstallDitContainer${numb}.exe`;
            let downloadFiles = [downloadFile(r360url, `./${r360version}.zip`)];

            write(`gatherReleases: Download file '${r360url}'.`, null, true, true);
            if (downloadDitContainer) {
                write(`gatherReleases: Download file '${ditcontainer}'.`, null, true, true);
                downloadFiles.push(downloadFile(ditcontainer, `./DitContainer${r360version}.exe`));
            }

            write(`gatherReleases: Downloading ${downloadFiles.length} file(s)`)
            results = await Promise.all(downloadFiles);
        }
        else {
            results = await Promise.all([
                unpack(`./Release-${r360version}.zip`, `./${r360version}.zip`)
            ]);
        }
        let r = results.some((x) => x === false);
        res(!r);
    });
}