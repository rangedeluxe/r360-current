import { Engine, assert, write }
    from 'nexe-deploy-utils';
import { gatherReleases } from './utils/gatherReleases';
import { getWebBuild } from './tiers/deployWeb';
import { getAppBuild } from './tiers/deployApp';
import { getFileBuild } from './tiers/deployFile';
import { getClientBuild } from './tiers/deployClient';
import { getDBBuild } from './tiers/deployDB';
import { getFileNoIISBuild } from './tiers/deployFileNoIIS';
import { args } from './objects/args';
let a = require('minimist')(process.argv.slice(2));

(async(args:any) => {
    let cmdargs:args = {
        r360version: args.r360v,
        r360branch: args.r360branch || 'dev',
        drive: args.d,
        tier: args.t,
        cert: args.c,
        download: args.dl,
        dbargs: args.dbargs,
        raamdbargs: args.raamdbargs
    };
    console.dir(cmdargs);
    write(JSON.stringify(cmdargs), null, true, true);
    let engine = new Engine();
    let root = `${cmdargs.drive}:\\WFSApps\\`;
    let result = await engine.describe('Checking provided args')
        .step(() => assert(cmdargs.r360version && cmdargs.r360version.length > 0, `Expected argument --r360v not found (version tag)`), 'Checking arg R360 version')
        .step(() => assert(cmdargs.drive && cmdargs.drive.length > 0, `Expected argument --d not found (drive letter)`), 'Checking arg drive')
        .step(() => assert(cmdargs.tier && cmdargs.tier.length > 0, `Expected argument --t not found (tier web, app, file, noiis, client, or db)`), 'Checking arg tier')
        .exec();
    if (!result) {
        return;
    }

    let build = cmdargs.tier === 'web' ? getWebBuild(root, cmdargs)
        : cmdargs.tier === 'app' ? getAppBuild(root, cmdargs)
        : cmdargs.tier === 'file' ? getFileBuild(root, cmdargs)
        : cmdargs.tier === 'client' ? getClientBuild(root, cmdargs)
        : cmdargs.tier === 'db' ? getDBBuild(root, cmdargs)
        : cmdargs.tier === 'noiis' ? getFileNoIISBuild(root, cmdargs)
        : null;

    if (!build) {
        console.log('Tier argument must be web, app, file, noiis, client, or db.');
        return;
    }

    await build.exec();
})(a);