# Installer
This is a small tool to silently install deluxe products. It produces a single executable which is runnable through an administrator command prompt. 

- Brandon Resheske wrote this. Get a hold of that guy if you have any questions.

**This is an expirement.** The reasoning behind this attempt was to do two things.
 - Move away from powershell all together, as we're seeing just a whole bunch of insabilities when running powershell.
 - (personal opinion) We're considering moving to MSI installs, but to develop for MSI's we'll likely be using WIX. WIX forces you to pretty much write all of your code in XML and I am very reserved about that.
  - Furthermore, the value MSIs provide is a nice UI around strict automation of an installer. I think we can achieve that by a using simple installer while utilizing the configurator tool. More on this discussion later.

## Development Environment
This tool is written in typescript using nodejs. There is currently only one source file; it's a pretty small amount of code.

To install the dependencies:
```bash
npm i
```

To run the typescript watcher for auto-transpilation:
```bash
npm run watch
```

To install on your location machine (requires you have IIS installed, and are running as an administrator):
```bash
npm run test
```

To explain what this script does, here is the npm script that is executed:

```json
"build": "nexe index.js -r \"{./Release-RecHub2.03.02.zip,./node_modules/nexe-deploy-utils/bin/handle64.exe}\" -o dist/install-r360.exe",

"test": "npm run build && cd dist && install-r360.exe --v RecHub2.03.02 --d C --t web --c 46120a42c8668426cdb5a5c42b3f6f13fe014614 && cd ../"
```

Basically, it will do these things:
 - Build script: which runs [nexe](https://github.com/nexe/nexe), a very neat tool for building an exe out of a node application, and packs the release zip files and the handle64 executable inside of the exe.
 - Runs the exe with some test arguments. It's saying to install R360 'RecHub2.03.02' on the C drive, installing only the web tier, using the IIS SSL certificate thumbprint listed. You can change these if you computer has other needs.
 - It puts the install-r360.exe into the dist folder. This is the *only* executable that is required to be copied to a test server. **The server _doesn't_ even need nodejs installed first,** as the nodejs executable is actually included into the install executable as well.

If you don't have the 'Release-[product][version].zip' files locally, you can find them on the sanserver in the respective release folders.

The majority of the tooling is found in the free, open-source, project called [nexe-deploy-utils](https://www.npmjs.com/package/nexe-deploy-utils). If you have any issues, please submit them to the [github repository](https://github.com/bresheske/DeployUtils/issues). If you wish to work on this repository, it's very simple to set that up to read from a local version instead.  Just clone the github repository and change the first line of index.ts to point to that instead:

```typescript
import { Engine, unpack, unzip, stopService, assert, copy, mkdir, newAppPool, newSite, startSite, deleteService, newService, startService, deleteSite, addSSL, newApp, killAll } from 'C:\\dev\\DeployUtils';
```
The important part being the 'from' statement. You're just telling typescript to read from your local drive instead of the node_modules folder when you install your dependencies.

Fun Fact - I also wrote that package, for this exact purpose.

## Publishing
If you want to just build the install exe:
```bash
npm run build
```

Then just copy the exe out to any environment and run it.