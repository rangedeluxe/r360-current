# Just a silly little script to install the command.exe service using nssm.
# By default, NSSM runs the service under the SYSTEM account, you can change it
# if you want by un-commenting the commented lines.

# $user = "$($env:computername)\Administrator"
.\nssm install commandutil command.exe
.\nssm set commandutil AppDirectory "D:\WFSStaging"
# .\nssm set commandutil ObjectName $user Wausau#1
.\nssm start commandutil