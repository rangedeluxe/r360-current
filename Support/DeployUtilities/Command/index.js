let express = require('express');
let parser = require('body-parser');
let exec = require('child_process').exec;
let app = express();
let http = require('http');

app.use(parser.json());
app.route('/execute')
    .post((req, res) => {
        let command = req.body.command;
        exec(command, (err, stdout, stderr) => {
            res.json({
                err: err,
                stdout: stdout,
                stderr: stderr
            });
        });
    });

let server = http.createServer(app);
server.setTimeout(10 * 60 * 1000);
server.listen(8000);
console.log('Deployment tool listening on 8000.');