const a = require('minimist')(process.argv.slice(2));
const request = require('web-request');

(async(args) => {
    const url = args.url;
    const json = args.json;

    const headers = {
        'Content-Type': 'application/json'
    };
    const opts = { headers: headers };

    console.log(`Sending JSON Request to URL: ${url}`);
    console.log(`Data: ${json}`);
    let res = await request.post(url, opts, json);
    console.log(`Request Completed. Results:`);
    console.log(res.body);
})(a);