--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (-1,'2018.03.16.026','','SessionClientAccountEntitlements.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:SessionClientAccountEntitlements.sql,IssueID:-1,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO

RAISERROR('Creating RecHubUser.SessionClientAccountEntitlements',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubUser.SessionClientAccountEntitlements') IS NOT NULL
	RAISERROR('RecHubUser.SessionClientAccountEntitlements already exists.',10,1) WITH NOWAIT
ELSE
BEGIN
CREATE TABLE RecHubUser.SessionClientAccountEntitlements
(
	SessionClientAccountEntitlementID BIGINT IDENTITY(1,1) NOT NULL,
		
	SessionID			UNIQUEIDENTIFIER,
	CreationDateKey		INT NOT NULL
		CONSTRAINT DF_SessionClientAccountEntitlement_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	EntityID			INT NOT NULL,
	SiteBankID			INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	ViewingDays			INT NULL,
	MaximumSearchDays	INT NULL,
	StartDateKey		INT NOT NULL,
	EndDateKey			INT NOT NULL,
	ViewAhead			BIT NOT NULL,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_SessionClientAccountEntitlement_CreationDate DEFAULT GETDATE(),
	EntityName			VARCHAR(128) NOT NULL
) ON Sessions (CreationDateKey);

EXEC sys.sp_addextendedproperty 
@name = N'Table_Description',
@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/11/2014
*
* Purpose: Client Account Keys a session is entitled to view.
*		   
*
* Modification History
* 05/11/2014 WI 145456		JPB	Created
* 07/08/2014 WI 145456		KLC	Changed Logon entity to be actual owning entity
* 09/18/2014 WI 166671		JPB	Added new index.
* 09/18/2014 WI 166672		JPB	Updated IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey
*								with includes.
* 02/23/2015 WI 191263		JBS	Add columns ViewAhead and EndDateKey, Also Change StartDateKey to NOT NULL
* 03/23/2015 WI 197226		JPB	Add IDX_SessionIDCreationDate (used by Session Maint package)
* 03/25/2015 WI 197953		JBS	Add IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead  
*							(used by Advanced Search)
* 06/01/2015 WI 197953		JBS	Changing IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID to Clustered index. Per 216076
* 03/29/2017 PT 141435715	MGE	Added CreationDateKey to table for Partitioning plans
* 04/03/2017 PT 142595657	MGE Added new index and partition keys
* 04/24/2017 PT 143253497	MGE updated column name to remove extra 'Session'
******************************************************************************************************************/
',
@level0type = N'SCHEMA',@level0name = RecHubUser,
@level1type = N'TABLE',@level1name = SessionClientAccountEntitlements;

RAISERROR('Creating Foreign Key',10,1) WITH NOWAIT
ALTER TABLE RecHubUser.SessionClientAccountEntitlements ADD 
    CONSTRAINT PK_SessionClientAccountEntitlements PRIMARY KEY NONCLUSTERED (SessionClientAccountEntitlementID, CreationDateKey)
ALTER TABLE RecHubUser.SessionClientAccountEntitlements ADD 
	CONSTRAINT FK_SessionClientAccountEntitlements_dimClientAccounts FOREIGN KEY (ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey);


RAISERROR('Creating Index RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID',10,1) WITH NOWAIT
CREATE CLUSTERED INDEX IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID ON RecHubUser.SessionClientAccountEntitlements
(
	CreationDateKey,
	SessionClientAccountEntitlementID
) ON Sessions (CreationDateKey);


RAISERROR('Creating Index RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID,
	SiteBankID,
	SiteClientAccountID
) ON Sessions (CreationDateKey);


RAISERROR('Creating Index RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID,
	ClientAccountKey
)
INCLUDE --166672
(
	EntityID,
	EntityName
) ON Sessions (CreationDateKey);

--WI 166671

RAISERROR('Creating Index RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityIDEntityName',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityIDEntityName ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID
) 
INCLUDE 
(
	EntityID,
	EntityName
) ON Sessions (CreationDateKey);

--WI 197226

RAISERROR('Creating Index RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDCreationDate',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDCreationDate ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID,
	CreationDate
) ON Sessions (CreationDateKey);
--WI 197953

RAISERROR('Creating Index RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID
) 
INCLUDE 
(
	EntityID,
	SiteBankID,
	SiteClientAccountID,
	StartDateKey,
	EndDateKey,
	ViewAhead
) ON Sessions (CreationDateKey);

RAISERROR('Creating Index RecHubUser.SessionClientAccountEntitlements.[IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID]',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID ON RecHubUser.SessionClientAccountEntitlements
(
 SessionID ASC,
 SiteBankID ASC,
 SiteClientAccountID ASC
)
INCLUDE
(
 EntityID,
 ViewingDays,
 MaximumSearchDays
) ON Sessions (CreationDateKey);

END
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

