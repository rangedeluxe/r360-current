--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154804395,'2018.03.16.026','','usp_OLWorkgroups_Assign.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_OLWorkgroups_Assign.sql,IssueID:154804395,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
RAISERROR('Creating Stored Procedure RecHubUser.usp_OLWorkgroups_Assign',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubUser.usp_OLWorkgroups_Assign') IS NOT NULL
	DROP PROCEDURE [RecHubUser].[usp_OLWorkgroups_Assign]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE RecHubUser.usp_OLWorkgroups_Assign
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmEntityID				INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 02/12/2018
*
* Purpose: Make the association between the RecHubData.dimClientAccounts
*			and the EntityID, taking default Entity settings
*
* Modification History
* 02/12/2014 PT 154804395 MGE	Created
******************************************************************************/
DECLARE @defaultInvoiceBalancingOption	TINYINT = 0/*TODO: remove 0 default once OLEntities has this column*/;

SET NOCOUNT ON; 

DECLARE @LocalTransaction bit = 0;
DECLARE @errorDescription VARCHAR(1024);

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	IF @parmEntityID IS NOT NULL
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM RecHubUser.OLEntities WHERE EntityID = @parmEntityID)
		BEGIN
			SET @errorDescription = 'Unable to insert/update RecHubUser.OLWorkgroups record, EntityID (' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL') + ' does not exist';
			RAISERROR(@errorDescription, 16, 2);
		END
	END

	DECLARE @curTime datetime = GETDATE();

	MERGE RecHubUser.OLWorkgroups
	USING
	(
		VALUES (@parmSiteBankID, @parmSiteClientAccountID)
	) S (SiteBankID, SiteClientAccountID)
	ON RecHubUser.OLWorkgroups.SiteBankID = S.SiteBankID
		AND RecHubUser.OLWorkgroups.SiteClientAccountID = S.SiteClientAccountID
	WHEN MATCHED AND ISNULL(RecHubUser.OLWorkgroups.EntityID, -1) <> ISNULL(@parmEntityID, -1)
		THEN UPDATE SET EntityID = @parmEntityID
	WHEN NOT MATCHED AND @parmEntityID IS NOT NULL THEN
		INSERT (EntityID, SiteBankID, SiteClientAccountID, ViewingDays, MaximumSearchDays,
				CheckImageDisplayMode, DocumentImageDisplayMode, HOA, DisplayBatchID, InvoiceBalancingOption,
				CreationDate, CreatedBy, ModificationDate, ModifiedBy)
		VALUES (@parmEntityID, @parmSiteBankID, @parmSiteClientAccountID, null, null,
				0, 0, 0, NULL, @defaultInvoiceBalancingOption,
				@curTime, SUSER_NAME(), @curTime, SUSER_NAME());
	

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

GO


GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

