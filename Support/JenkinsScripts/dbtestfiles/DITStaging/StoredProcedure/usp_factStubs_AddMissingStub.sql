--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (153683396,'2018.03.16.026','','usp_factStubs_AddMissingStub.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_factStubs_AddMissingStub.sql,IssueID:153683396,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
RAISERROR('Creating Stored Procedure DITStaging.usp_factStubs_AddMissingStub',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('DITStaging.usp_factStubs_AddMissingStub') IS NOT NULL
	DROP PROCEDURE [DITStaging].[usp_factStubs_AddMissingStub]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE DITStaging.usp_factStubs_AddMissingStub
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/17/2015
*
* Purpose: For every data entry "row" (TranID/BatchSeq) there needs to be a stub row.
*	Add any stubs that are missing based on data entry detail information
*
* Modification History
* 10/17/2015 PT 151414157 JPB	Created
* 01/10/2018 PT 153683396 MGE	Corrected logic used to update factBatchSummary StubCount
*****************************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY

	DECLARE @MissingStubs TABLE
	(
		BatchID BIGINT,
		TransactionID INT,
		BatchSequence INT
	);

	;WITH DataEntryRows AS
	(
		SELECT DISTINCT 
			BatchID,
			TransactionID,
			BatchSequence 
		FROM 
			DITStaging.factDataEntryDetails 
			INNER JOIN DITStaging.DataImportWorkgroupDataEntryColumnKeys ON DITStaging.DataImportWorkgroupDataEntryColumnKeys.WorkgroupDataEntryColumnKey = DITStaging.factDataEntryDetails.WorkGroupDataEntryColumnKey WHERE DITStaging.DataImportWorkgroupDataEntryColumnKeys.IsCheck = 0
		EXCEPT
		SELECT 
			BatchID,
			TransactionID,
			BatchSequence 
		FROM 
			DITStaging.factStubs
	)
	INSERT INTO @MissingStubs
	(
		BatchID,
		TransactionID,
		BatchSequence
	)
	SELECT
		BatchID,
		TransactionID,
		BatchSequence
	FROM 
		DataEntryRows;

	INSERT INTO DITStaging.factStubs
	(
		BatchTrackingID,
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchCueID,
		DepositStatus,
		SystemType,
		TransactionID,
		TxnSequence,
		SequenceWithinTransaction,
		BatchSequence,
		StubSequence,
		IsCorrespondence,
		IsOMRDetected,
		CreationDate,
		ModificationDate,
		BatchSiteCode
	)
	SELECT
		DITStaging.factTransactionSummary.BatchTrackingID,
		DITStaging.factTransactionSummary.IsDeleted,
		DITStaging.factTransactionSummary.BankKey,
		DITStaging.factTransactionSummary.OrganizationKey,
		DITStaging.factTransactionSummary.ClientAccountKey,
		DITStaging.factTransactionSummary.DepositDateKey,
		DITStaging.factTransactionSummary.ImmutableDateKey,
		DITStaging.factTransactionSummary.SourceProcessingDateKey,
		DITStaging.factTransactionSummary.BatchID,
		DITStaging.factTransactionSummary.SourceBatchID,
		DITStaging.factTransactionSummary.BatchNumber,
		DITStaging.factTransactionSummary.BatchSourceKey,
		DITStaging.factTransactionSummary.BatchPaymentTypeKey,
		DITStaging.factTransactionSummary.BatchCueID,
		DITStaging.factTransactionSummary.DepositStatus,
		DITStaging.factTransactionSummary.SystemType,
		DITStaging.factTransactionSummary.TransactionID,
		DITStaging.factTransactionSummary.TxnSequence,
		DITStaging.factTransactionSummary.TxnSequence AS SequenceWithinTransaction,
		MissingStubs.BatchSequence,
		0 AS StubSequence,
		0 AS IsCorrespondence,
		0 AS IsOMRDetected,
		DITStaging.factTransactionSummary.CreationDate,
		DITStaging.factTransactionSummary.ModificationDate,
		DITStaging.factTransactionSummary.BatchSiteCode
	FROM
		@MissingStubs AS MissingStubs
		INNER JOIN DITStaging.factTransactionSummary ON DITStaging.factTransactionSummary.BatchID = MissingStubs.BatchID
			AND DITStaging.factTransactionSummary.TransactionID = MissingStubs.TransactionID;

	/* Update the stub sequence so they are in the correct order according to the batch sequence number */
	;WITH UpdateStubSequence AS
	(
		SELECT
			DITStaging.factStubs.BatchID,
			DITStaging.factStubs.BatchSequence,
			ROW_NUMBER() OVER(PARTITION BY DITStaging.factStubs.BatchID ORDER BY DITStaging.factStubs.BatchID,DITStaging.factStubs.BatchSequence) AS StubSequence
		FROM 
			@MissingStubs MissingStubs
			INNER JOIN DITStaging.factStubs ON DITStaging.factStubs.BatchID = MissingStubs.BatchID
	)
	UPDATE
		DITStaging.factStubs
	SET
		DITStaging.factStubs.StubSequence = UpdateStubSequence.StubSequence
	FROM
		DITStaging.factStubs
		INNER JOIN UpdateStubSequence ON UpdateStubSequence.BatchID = DITStaging.factStubs.BatchID
			AND UpdateStubSequence.BatchSequence = DITStaging.factStubs.BatchSequence;

	/* Update the stub count on the factBatchSummary record */
	;WITH StubInfo AS
	(
		SELECT
			DITStaging.factStubs.BatchID,
			COUNT(DITStaging.factStubs.BatchID) AS StubCount
		FROM
			DITStaging.factStubs
		GROUP BY
			DITStaging.factStubs.BatchID
	)
	UPDATE
		DITStaging.factBatchSummary
	SET
		DITStaging.factBatchSummary.StubCount = StubInfo.StubCount
	FROM
		DITStaging.factBatchSummary
		INNER JOIN StubInfo ON DITStaging.factBatchSummary.BatchID = StubInfo.BatchID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

