--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154233497,'2018.03.16.026','','usp_factNotificationFiles_Upd_IsDeleted_ByWorkTableJoin.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_factNotificationFiles_Upd_IsDeleted_ByWorkTableJoin.sql,IssueID:154233497,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
RAISERROR('Creating Stored Procedure FITStaging.usp_factNotificationFiles_Upd_IsDeleted_ByWorkTableJoin',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('FITStaging.usp_factNotificationFiles_Upd_IsDeleted_ByWorkTableJoin') IS NOT NULL
	DROP PROCEDURE [FITStaging].[usp_factNotificationFiles_Upd_IsDeleted_ByWorkTableJoin]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE FITStaging.usp_factNotificationFiles_Upd_IsDeleted_ByWorkTableJoin
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/29/2012
*
* Purpose: Delete RecHub360 fact rows based from SSIS Staging fact rows.
*
* Modification History
* 08/29/2012 CR 55325 JPB	Created
* 06/20/2013 WI 92109 JBS	Update to 2.0. Change schema to FITStaging
*							Rename proc from usp_OLTAfactNotificationFiles_DeleteByWorkTableJoin
* 11/26/2013 WI 123649 JBS	Added SourceNotificationID to Join logic for setting IsDeleted record
* 03/05/2014 WI 131909 TWE  Modify delete logic
* 02/10/2015 WI 189066 TWE  Modify delete matching logic and to match on ID
* 07/14/2015 WI 223776 TWE  Modify to honor the duplicate detect switch
* 02/06/2018 PT 154233497 JPB Removed OrganizationKey
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON;

DECLARE @FITDuplicateCheck AS INT = 0;

BEGIN TRY

    SELECT 
		@FITDuplicateCheck = Value
	FROM
		RecHubConfig.SystemSetup
	WHERE
		RecHubConfig.SystemSetup.Section = 'FIT' AND
		RecHubConfig.SystemSetup.SetupKey = 'FITDuplicateCheck'; 

	IF  @FITDuplicateCheck = 1
	BEGIN;
		WITH locateKeys AS
		(
		-----------------------
		--  CASE 1
		--  Criteria for duplicate is as follows 
		--  SiteBankID
		--  SiteClientAccountID
		--  Notification Date (ignoring any time component)
		--  User File Name
		--  File Extension
		SELECT DISTINCT
			RecHubData.factNotificationFiles.NotificationDateKey
			,RecHubData.factNotificationFiles.NotificationMessageGroup
			,(SELECT RecHubData.dimBanks.SiteBankID 
			  FROM RecHubData.dimBanks 
			  WHERE RecHubData.dimBanks.BankKey = RecHubData.factNotificationFiles.BankKey)                             AS BankID	
			,(SELECT RecHubData.dimClientAccounts.SiteClientAccountID 
			  FROM RecHubData.dimClientAccounts 
			  WHERE RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factNotificationFiles.ClientAccountKey)  AS SiteClientAccountID
		FROM	
			RecHubData.factNotificationFiles
			INNER JOIN FITStaging.factNotificationFiles AS NotificationFiles
				ON RecHubData.factNotificationFiles.NotificationDateKey = NotificationFiles.NotificationDateKey
					AND RecHubData.factNotificationFiles.UserFileName = NotificationFiles.UserFileName
					AND RecHubData.factNotificationFiles.FileExtension = NotificationFiles.FileExtension
		WHERE
			(SELECT RecHubData.dimBanks.SiteBankID FROM RecHubData.dimBanks WHERE RecHubData.dimBanks.BankKey = RecHubData.factNotificationFiles.BankKey) = 
			(SELECT RecHubData.dimBanks.SiteBankID FROM RecHubData.dimBanks WHERE RecHubData.dimBanks.BankKey = NotificationFiles.BankKey)
			AND
			(SELECT RecHubData.dimClientAccounts.SiteClientAccountID FROM RecHubData.dimClientAccounts WHERE RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factNotificationFiles.ClientAccountKey) =
			(SELECT RecHubData.dimClientAccounts.SiteClientAccountID FROM RecHubData.dimClientAccounts WHERE RecHubData.dimClientAccounts.ClientAccountKey = NotificationFiles.ClientAccountKey)
		)
		UPDATE	RecHubData.factNotificationFiles
		SET		RecHubData.factNotificationFiles.IsDeleted = 1,
				RecHubData.factNotificationFiles.ModificationDate = GETDATE()
		FROM	
			RecHubData.factNotificationFiles
			INNER JOIN locateKeys AS NotificationFiles
				ON RecHubData.factNotificationFiles.NotificationMessageGroup = NotificationFiles.NotificationMessageGroup
		WHERE 
			RecHubData.factNotificationFiles.BankKey > 0   --Make sure alert notifications are not picked up	
		;
		-----------------------
		--  CASE 2
		-- they just submitted the same file load again.
		UPDATE      RecHubData.factNotificationFiles
		SET         RecHubData.factNotificationFiles.IsDeleted = 1,
					   RecHubData.factNotificationFiles.ModificationDate = GETDATE()
		FROM   
			 RecHubData.factNotificationFiles
			 INNER JOIN FITStaging.factNotificationFiles AS NotificationFiles
				 ON RecHubData.factNotificationFiles.SourceNotificationID = NotificationFiles.SourceNotificationID
		WHERE 
			RecHubData.factNotificationFiles.SourceNotificationID != '00000000-0000-0000-0000-000000000000'
			AND RecHubData.factNotificationFiles.BankKey > 0   --Make sure alert notifications are not picked up	
		;
	END;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

