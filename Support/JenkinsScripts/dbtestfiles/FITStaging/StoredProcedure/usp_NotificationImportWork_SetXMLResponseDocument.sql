--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154233497,'2018.03.16.026','','usp_NotificationImportWork_SetXMLResponseDocument.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_NotificationImportWork_SetXMLResponseDocument.sql,IssueID:154233497,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
RAISERROR('Creating Stored Procedure FITStaging.usp_NotificationImportWork_SetXMLResponseDocument',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('FITStaging.usp_NotificationImportWork_SetXMLResponseDocument') IS NOT NULL
	DROP PROCEDURE [FITStaging].[usp_NotificationImportWork_SetXMLResponseDocument]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE FITStaging.usp_NotificationImportWork_SetXMLResponseDocument
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/24/2012
*
* Purpose: 
*
* Modification History
* 08/24/2012 CR 55263 JPB	Created
* 06/20/2013 WI 92101 JBS	Update to 2.0. Change schema to FITStaging
* 05/28/2015 WI 215704 JPB	Updated to to use Notification_Id instead of NotificationTrackingID
*							(Duplicated detect work.)
* 02/06/2018 PT 154233497 JPB Removed OrganizationKey
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON;

DECLARE @Notification_Id BIGINT,
		@Loop INT,
		@ResponseType TINYINT,
		@XMLResponse XML;

BEGIN TRY

	DECLARE @ResponseList TABLE 
	(
		RowID INT IDENTITY(1,1), 
		Notification_Id BIGINT,
		ResponseType TINYINT
	);

	/* Get a distinct list of batch tracking IDs from the response table */
	INSERT INTO @ResponseList(Notification_Id,ResponseType)
	SELECT DISTINCT
		FITStaging.NotificationImportWorkResponses.Notification_Id,
		MIN(FITStaging.NotificationImportWorkResponses.ResponseStatus)
	FROM	
		FITStaging.NotificationImportWorkResponses
	GROUP BY 
		FITStaging.NotificationImportWorkResponses.Notification_Id;
		
	/* Update Work table with error if error row exists */
	UPDATE FITStaging.NotificationImportWork
	SET FITStaging.NotificationImportWork.ResponseStatus = ResponseType
	FROM	
		FITStaging.NotificationImportWork
		INNER JOIN @ResponseList RL 
			ON RL.Notification_Id = FITStaging.NotificationImportWork.Notification_Id;
	
	/* Clear the response list */
	DELETE FROM @ResponseList;
	
	/* Get recods that exist in the response table but not in the work table */
	INSERT INTO @ResponseList(Notification_Id,ResponseType)
	SELECT
		FITStaging.NotificationImportWorkResponses.Notification_Id,
		0
	FROM	
		FITStaging.NotificationImportWorkResponses
	EXCEPT 
	SELECT	
		FITStaging.NotificationImportWork.Notification_Id,
		0
	FROM	
		FITStaging.NotificationImportWork;

	UPDATE @ResponseList
	SET	ResponseType = FITStaging.NotificationImportWorkResponses.ResponseStatus
	FROM	
		@ResponseList RL
		INNER JOIN FITStaging.NotificationImportWorkResponses 
			ON FITStaging.NotificationImportWorkResponses.Notification_Id = RL.Notification_Id

	INSERT INTO FITStaging.NotificationImportWork
	(
		FITStaging.NotificationImportWork.NotificationTrackingID,
		FITStaging.NotificationImportWork.Notification_Id,
		FITStaging.NotificationImportWork.NotificationMessageGroup,
		FITStaging.NotificationImportWork.ResponseStatus,
		FITStaging.NotificationImportWork.BankKey,
		FITStaging.NotificationImportWork.ClientAccountKey,
		FITStaging.NotificationImportWork.NotificationDateKey,
		FITStaging.NotificationImportWork.NotificationSourceKey,
		FITStaging.NotificationImportWork.SourceNotificationID
	)
	SELECT	
		FITStaging.XMLNotification.NotificationTrackingID,
		FITStaging.XMLNotification.Notification_ID,
		0,
		RL.ResponseType, /*ResponseStatus*/
		0, /*BankKey*/
		0, /*ClientAccountKey*/
		0, /*NotificationDateKey*/
		0, /*NotificationSourceKey*/
		FITStaging.XMLNotification.SourceNotificationID
	FROM 
		FITStaging.XMLNotification
		INNER JOIN @ResponseList RL 
			ON RL.Notification_Id = FITStaging.XMLNotification.Notification_Id;

	/* Clear the response list */
	DELETE FROM @ResponseList;
	
	/* Get all records that need to be processed */
	INSERT INTO @ResponseList(Notification_Id,ResponseType)
	SELECT	
		FITStaging.NotificationImportWork.Notification_Id,
		FITStaging.NotificationImportWork.ResponseStatus
	FROM	
		FITStaging.NotificationImportWork;
	
	SET @Loop = 1;

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @ResponseList) )
	BEGIN

		SELECT	
			@Notification_Id = Notification_Id,
			@ResponseType = ResponseType
		FROM	
			@ResponseList 
		WHERE 
			RowID = @Loop;
		
		SELECT @XMLResponse = 
		(
			SELECT	TOP (1) NotificationTrackingID AS '@NotificationTrackingID',
					(
						SELECT CASE	FITStaging.NotificationImportWork.ResponseStatus
							WHEN 0 THEN 'Success'
							WHEN 1 THEN 'Fail' 
							WHEN 2 THEN 'Warning'
						END AS 'Results/*'
						FROM 
							FITStaging.NotificationImportWork
						WHERE 
							Notification_Id = @Notification_Id
						FOR XML PATH(''), TYPE
					),
					(
						SELECT	FITStaging.NotificationImportWorkResponses.ResultsMessage AS 'ErrorMessage/*',NULL
						FROM	
							FITStaging.NotificationImportWorkResponses
						WHERE	
							N1.Notification_Id = FITStaging.NotificationImportWorkResponses.Notification_Id
						FOR XML PATH(''), TYPE
					)
			FROM 
				FITStaging.NotificationImportWork N1
			WHERE 
				N1.Notification_Id = @Notification_Id
			FOR XML PATH('Notification'), TYPE
		);
				
		UPDATE RecHubSystem.DataImportQueue
		SET	
			RecHubSystem.DataImportQueue.XMLResponseDocument = @XMLResponse,
			RecHubSystem.DataImportQueue.QueueStatus = 99,
			RecHubSystem.DataImportQueue.ResponseStatus = @ResponseType,
			RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
			RecHubSystem.DataImportQueue.ModifiedBy = SUSER_NAME()
		WHERE 
			RecHubSystem.DataImportQueue.DataImportQueueID = @Notification_Id;

		SET @Loop = @Loop + 1;	
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

