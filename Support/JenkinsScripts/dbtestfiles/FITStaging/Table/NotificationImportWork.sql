--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154233497,'2018.03.16.026','','NotificationImportWork.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:NotificationImportWork.sql,IssueID:154233497,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
RAISERROR('Dropping FITStaging.NotificationImportWork if it exists.',10,1) WITH NOWAIT
IF OBJECT_ID('FITStaging.NotificationImportWork') IS NOT NULL
       DROP TABLE FITStaging.NotificationImportWork


RAISERROR('Creating FITStaging.NotificationImportWork',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('FITStaging.NotificationImportWork') IS NOT NULL
	RAISERROR('FITStaging.NotificationImportWork already exists.',10,1) WITH NOWAIT
ELSE
BEGIN
CREATE TABLE FITStaging.NotificationImportWork
(
	NotificationTrackingID		UNIQUEIDENTIFIER NOT NULL,
	Notification_Id				BIGINT NOT NULL,
	NotificationMessageGroup	BIGINT NOT NULL
		CONSTRAINT DF_NotificationImportWork_NotificationMessageGroup DEFAULT (NEXT VALUE FOR RecHubSystem.NotificationMessageGroup),
	ResponseStatus				TINYINT NOT NULL
		CONSTRAINT DF_NotificationImportWork_ResponseStatus DEFAULT 255,
	BankKey						INT NOT NULL,
	ClientAccountKey			INT NOT NULL,
	NotificationDateKey			INT NOT NULL,
	NotificationSourceKey		SMALLINT NOT NULL,
	SourceNotificationID		UNIQUEIDENTIFIER NOT NULL,
	MessageWorkgroups			UNIQUEIDENTIFIER NULL
);

EXEC sys.sp_addextendedproperty 
@name = N'Table_Description',
@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2014-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 06/14/2012
*
* Purpose: Workspace for the File Import Toolkit SSIS package.
*		   
*
* ResponseStatus
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	255 = Status not set
*
* Modification History
* 06/14/2012 CR 53537 JPB	Created
* 06/11/2013 WI 92079 JBS	Update to 2.0 Release.  Change Schema to FITStaging.
*							Change references: Customer to Organization,
*							LockBox to ClientAccount
* 03/02/2015 WI 193423 CMC	Changing NotificationSourceKey from tinyint to smallint.
* 03/31/2015 WI 198845 JBS	Adding constraint DF_NotificationImportWork_NotificationMessageGroup
* 02/06/2018 PT 154233497 JPB Added MessageWorkgroups, removed OrganizationKey
******************************************************************************/
',
@level0type = N'SCHEMA',@level0name = FITStaging,
@level1type = N'TABLE',@level1name = NotificationImportWork;

RAISERROR('Creating Index FITStaging.NotificationImportWork.IDX_NotificationImportWork_NotificationDateBankOrganizationClientAccountKey',10,1) WITH NOWAIT
CREATE CLUSTERED INDEX IDX_NotificationImportWork_NotificationDateBankOrganizationClientAccountKey ON FITStaging.NotificationImportWork
(
	NotificationDateKey ASC,
	BankKey				ASC,
	ClientAccountKey 	ASC
);

END
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

