﻿param
(
	[bool] $Build = $false
)
$msBuildExe2015 = "C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild.exe"
$msBuildExe2017 = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"

if( !(Test-Path S:) )
{
	Write-Host "Setup drive"
	net use S: \\8YQBBG2\c$\Dev\R360-Current
}
if( $Build -eq $true )
{
	Write-Host "Clean Base Shredder" -foregroundcolor green
	&"$msBuildExe2017" S:\Codebase\DataImport\BaseShredder\BaseShredder.sln /t:Clean /m
	Write-Host "Build Base Shredder" -foregroundcolor green
	&"$msBuildExe2017" S:\Codebase\DataImport\BaseShredder\BaseShredder.sln /t:Build /m
	Write-Host "Clean Project" -foregroundcolor green
	&"$msBuildExe2017" C:\Dev\R360-Current\Codebase\FileImport\FileImportShredder.sln /t:Clean /m
	Write-Host "Build Project" -foregroundcolor green
	&"$msBuildExe2017" C:\Dev\R360-Current\Codebase\FileImport\FileImportShredder.sln /t:Build /m
}
Write-Host "Start VS2015 and edit package" -foregroundcolor green

