--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (147284587,'2018.03.16.026','','usp_PostDepositTransactionExceptions_Ins.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_PostDepositTransactionExceptions_Ins.sql,IssueID:147284587,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
RAISERROR('Creating Stored Procedure RecHubException.usp_PostDepositTransactionExceptions_Ins',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_Ins') IS NOT NULL
	DROP PROCEDURE [RecHubException].[usp_PostDepositTransactionExceptions_Ins]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Ins
(
	@parmBatchID			BIGINT,
	@parmDepositDateKey		INT,
	@parmTransactionID		INT,
	@parmSID				UNIQUEIDENTIFIER,
	@parmUserName			VARCHAR(128),
	@parmErrorCode			INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 07/12/2017
*
* Purpose: Inserts a new PostDepositTransactionExceptions record when a user locks it
*
* Modification History
* 07/12/2017 PT 144805301 MGE	Created
* 07/17/2017 PT 144805301 MGE	Changed the parameters to use natural key
* 08/04/2017 PT 149496367 JPB	Insert record into batch exceptions table
* 12/11/2017 PT 147284587 JPB	User Activity Audit lock
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction BIT = 0,
		@LockedByUserID INT;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
		SET @parmErrorCode = 0;
	END

	DECLARE @errorDescription VARCHAR(1024),
			@AuditMessage VARCHAR(MAX),
			@TransactionKey	BIGINT;
	DECLARE @curTime DATETIME = GETDATE();

	DECLARE @UserIDs TABLE (UserID INT);

	INSERT INTO @UserIDs
	EXEC RecHubUser.usp_Users_UserID_Get_BySID @parmSID = @parmSID;

	SELECT TOP(1) 
		@LockedByUserID = UserID
	FROM 
		@UserIDs;

	-- Verify User ID
	IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE UserID = @LockedByUserID)
	BEGIN
		SET @errorDescription = 'Unable to insert PostDepositTransactionExceptions record, user (' + ISNULL(CAST(@LockedByUserID AS VARCHAR(10)), 'NULL') + ') does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	-- Verify the Transaction isn't already locked
	IF EXISTS(SELECT 1 FROM RecHubException.PostDepositTransactionExceptions
		WHERE BatchID = @parmBatchID AND DepositDateKey = @parmDepositDateKey AND TransactionID = @parmTransactionID)
	BEGIN
		SET @parmErrorCode = 1; -- 1 = Duplicate
		IF @LocalTransaction = 1 COMMIT TRANSACTION;
	END
	ELSE
		BEGIN
		
		-- Insert the PostDepositTransactionExceptions record
		INSERT INTO RecHubException.PostDepositTransactionExceptions(BatchID, DepositDateKey, TransactionID, LockedDate, LockedByUserID)
		VALUES (@parmBatchID, @parmDepositDateKey, @parmTransactionID, GETDATE(), @LockedByUserID);

		SET @TransactionKey = SCOPE_IDENTITY();

		-- Audit the RecHubException.PostDepositTransactionExceptions insert
		SET @auditMessage = 'Added PostDepositTransactionExceptions Lock for Batch: ' + CAST(@parmBatchID AS VARCHAR(10))
			+ ': DepositDateKey = ' + CAST(@parmDepositDateKey AS VARCHAR(8)) 
			+ ': TransactionID = ' + CAST(@parmTransactionID AS VARCHAR(10))
			+ ': UserID = ' + CAST(@LockedByUserID AS VARCHAR(10))
			+ '.';

		EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
				@parmUserID				= @LockedByUserID,
				@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Ins',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'PostDepositTransactionExceptions',
				@parmColumnName			= 'TransactionKey',
				@parmAuditValue			= @TransactionKey,
				@parmAuditType			= 'INS',
				@parmAuditMessage		= @auditMessage;

		SET @AuditMessage =  @parmUserName + ' locked Post-Deposit Exceptions ';

		SELECT 
			@AuditMessage += 'Transaction: ' + CAST(@parmTransactionID AS VARCHAR(10)) 
			+ ', Transaction Sequence: ' + CAST(TxnSequence AS VARCHAR(10))
			+ ' in Batch: ' + CAST(SourceBatchID AS VARCHAR(10)) 
			+ ' for Deposit Date: ' + CONVERT(VARCHAR(20), CONVERT(DATE, CONVERT(VARCHAR(8), @parmDepositDateKey), 112),110) 
			+ ', Bank: ' + CAST(RecHubData.dimClientAccounts.SiteBankID AS VARCHAR(10))	+ ' - ' + RecHubData.dimBanks.BankName
			+ ', Workgroup: ' + CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(10)) + ' - ' + COALESCE(RecHubData.dimClientAccounts.LongName,RecHubData.dimClientAccounts.ShortName)
			+ ', Payment Source: ' + COALESCE(RecHubData.dimBatchSources.LongName,RecHubData.dimBatchSources.ShortName)
			+ ', Payment Type: ' + COALESCE(RecHubData.dimBatchPaymentTypes.LongName,RecHubData.dimBatchPaymentTypes.ShortName)
		FROM
			RecHubData.factTransactionSummary
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
			INNER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.BankKey = RecHubData.factTransactionSummary.BankKey
			INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
			INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND TransactionID = @parmTransactionID
			AND IsDeleted = 0;

		EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Ins',
			@parmEventName			= 'Post-Deposit Exceptions',
			@parmEventType			= 'Post-Deposit Exceptions',
			@parmUserID				= @LockedByUserID,
			@parmAuditMessage		= @AuditMessage;

		BEGIN TRY
			--Multiple users could be inserting records at the same time. Rather than doing a check and possibly missing
			--the fact that the record already exists, just insert it and throw away any duplicate errors.
			INSERT INTO RecHubException.PostDepositBatchExceptions(DepositDateKey,BatchID,CreationDate)
			VALUES(@parmDepositDateKey,@parmBatchID,GETDATE());
		END TRY
		BEGIN CATCH
			--If the error is not a duplicate record, throw an error, i.e. do not throw an error if it is a duplicate record
			IF( @@ERROR <> 2601)
			BEGIN
				DECLARE @ErrorMessage    NVARCHAR(4000),
						@ErrorSeverity   INT,
						@ErrorState      INT;
				SELECT	@ErrorMessage = ERROR_MESSAGE(),
						@ErrorSeverity = ERROR_SEVERITY(),
						@ErrorState = ERROR_STATE();
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			END
		END CATCH
		-- All updates are complete
		IF @LocalTransaction = 1 COMMIT TRANSACTION;
	END
END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

RAISERROR('Applying Permissions',10,1) WITH NOWAIT
GRANT EXECUTE ON [RecHubException].[usp_PostDepositTransactionExceptions_Ins] TO dbRole_RecHubException;
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

