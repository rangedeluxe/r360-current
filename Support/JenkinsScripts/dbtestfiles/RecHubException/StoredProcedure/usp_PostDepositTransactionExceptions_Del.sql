--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (147284619,'2018.03.16.026','','usp_PostDepositTransactionExceptions_Del.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_PostDepositTransactionExceptions_Del.sql,IssueID:147284619,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
RAISERROR('Creating Stored Procedure RecHubException.usp_PostDepositTransactionExceptions_Del',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_Del') IS NOT NULL
	DROP PROCEDURE [RecHubException].[usp_PostDepositTransactionExceptions_Del]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Del
(
	@parmBatchID			BIGINT,
	@parmDepositDateKey		INT,
	@parmTransactionID		INT,
	@parmSID				UNIQUEIDENTIFIER,
	@parmUserName			VARCHAR(128),
	@parmErrorCode			INT	OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 07/12/2017
*
* Purpose: Remove rows from RecHubException.PostDepositTransactionExceptions whenever 
*			the user completes a locked PostDepositTransactionExceptions record
*
* Modification History
* 07/12/2017 PT 144805301	MGE	Created
* 07/17/2017 PT 144805301	MGE	Changed the parameters to use natural key
* 12/12/2017 PT 147284619	JPB User Activity Audit unlock
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't be deleted without being audited...
DECLARE @LocalTransaction bit = 0,
		@UserID INT;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
		SET @parmErrorCode = 0;
	END

	DECLARE @auditMessage			VARCHAR(1024),
			@errorDescription		VARCHAR(1024),
			@TransactionKey			BIGINT;

	DECLARE @curTime DATETIME = GETDATE();
	
	/* Get user ID from SID */
	DECLARE @UserIDs TABLE (UserID INT);

	INSERT INTO @UserIDs
	EXEC RecHubUser.usp_Users_UserID_Get_BySID @parmSID = @parmSID;

	SELECT TOP(1) 
		@UserID = UserID
	FROM 
		@UserIDs;
	

	-- Verify the Transaction Exists
	SELECT 
		@TransactionKey = TransactionKey 
	FROM 
		RecHubException.PostDepositTransactionExceptions
	WHERE 
		BatchID = @parmBatchID 
		AND DepositDateKey = @parmDepositDateKey 
		AND TransactionID = @parmTransactionID;
	
	IF @TransactionKey IS NULL	
	BEGIN
		SET @parmErrorCode = 2; -- 2 = Does Not Exist
		IF @LocalTransaction = 1 COMMIT TRANSACTION;
	END
	ELSE
	BEGIN
		-- Delete the PostDepositTransactionExceptions record
		DELETE FROM RecHubException.PostDepositTransactionExceptions 
			WHERE BatchID = @parmBatchID AND DepositDateKey = @parmDepositDateKey AND TransactionID = @parmTransactionID

		-- Audit the RecHubException.PostDepositTransactionExceptions delete
		SET @auditMessage = 'Removed PostDepositTransactionExceptions Lock for Batch: ' + CAST(@parmBatchID AS VARCHAR(10))
			+ ': DepositDateKey = ' + CAST(@parmDepositDateKey AS VARCHAR(8)) 
			+ ': TransactionID = ' + CAST(@parmTransactionID AS VARCHAR(10))
			+ ': UserID = ' + CAST(@UserID AS VARCHAR(10))
			+ '.';

		EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
				@parmUserID				= @UserID,
				@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Del',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'PostDepositTransactionExceptions',
				@parmColumnName			= 'TransactionKey',
				@parmAuditValue			= @TransactionKey,
				@parmAuditType			= 'DEL',
				@parmAuditMessage		= @auditMessage;

		SET @AuditMessage =  @parmUserName + ' unlocked Post-Deposit Exceptions ';

		SELECT 
			@AuditMessage += 'Transaction: ' + CAST(@parmTransactionID AS VARCHAR(10)) 
			+ ', Transaction Sequence: ' + CAST(TxnSequence AS VARCHAR(10))
			+ ' in Batch: ' + CAST(SourceBatchID AS VARCHAR(10)) 
			+ ' for Deposit Date: ' + CONVERT(VARCHAR(20), CONVERT(DATE, CONVERT(VARCHAR(8), @parmDepositDateKey), 112),110) 
			+ ', Bank: ' + CAST(RecHubData.dimClientAccounts.SiteBankID AS VARCHAR(10))	+ ' - ' + RecHubData.dimBanks.BankName
			+ ', Workgroup: ' + CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(10)) + ' - ' + COALESCE(RecHubData.dimClientAccounts.LongName,RecHubData.dimClientAccounts.ShortName)
			+ ', Payment Source: ' + COALESCE(RecHubData.dimBatchSources.LongName,RecHubData.dimBatchSources.ShortName)
			+ ', Payment Type: ' + COALESCE(RecHubData.dimBatchPaymentTypes.LongName,RecHubData.dimBatchPaymentTypes.ShortName)
		FROM
			RecHubData.factTransactionSummary
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
			INNER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.BankKey = RecHubData.factTransactionSummary.BankKey
			INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
			INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND TransactionID = @parmTransactionID
			AND IsDeleted = 0;

		EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Del',
			@parmEventName			= 'Post-Deposit Exceptions',
			@parmEventType			= 'Post-Deposit Exceptions',
			@parmUserID				= @UserID,
			@parmAuditMessage		= @AuditMessage;
	END

	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

RAISERROR('Applying Permissions',10,1) WITH NOWAIT
GRANT EXECUTE ON [RecHubException].[usp_PostDepositTransactionExceptions_Del] TO dbRole_RecHubException;
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

