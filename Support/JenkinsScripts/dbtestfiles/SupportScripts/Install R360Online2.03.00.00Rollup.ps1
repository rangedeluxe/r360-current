######### Update the following variables to match the installation #########
#Version of SQL the install is going against
#Valid values: 2012, 2016
$SQLVersion="2012"
#Name of the SQL Database server
$DBSERVER = "SERVERNAME"
#Name of the R360 Database-Default is WFSDB_R360
$R360DB = "WFSDB_R360"
#Name of the RAAM Database-Default is RAAM
$RAAMDB = "RAAM"
#Installation Folder
$INSTALLROOT = "D:\WFSStaging\"
#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\Installlogs"
#DataImportQueue partition information
$DataImportPartitions = 
"<Disks>
	<Disk DiskOrder=""1"" DiskPath=""Path to Data facts""/>
</Disks>";
#SessionActivity partition information
$SessionActivityPartitions = 
"<Disks>
	<Disk DiskOrder=""1"" DiskPath=""Path to SessionActivity Disks""/>
</Disks>";
#SessionActivity partition information
$SessionsPartitions = 
"<Disks>
	<Disk DiskOrder=""1"" DiskPath=""Path to Sessions Disks""/>
</Disks>";
######### End user defined variables #########

function UpdatePartitionDisks([int] $local:PartitionManagerID, [string] $local:Disks, [string] $local:DefaultPath)
{
	$local:DisksXML = New-Object "System.Xml.XmlDocument";
	$local:DisksXML.LoadXml($local:Disks);
	foreach( $local:DiskXML in $local:DisksXML.Disks.Disk )
	{
		if( $local:DiskXML.DiskOrder -eq 1 )
		{
			$local:Script = [string]::Format("IF( EXISTS(SELECT 1 FROM RecHubSystem.PartitionDisks WHERE PartitionManagerID = $local:PartitionManagerID AND DiskOrder = 1 AND DiskPath = '{0}') ) UPDATE RecHubSystem.PartitionDisks SET DiskPath = '{1}' WHERE PartitionManagerID = $local:PartitionManagerID AND DiskOrder = 1;",$local:DefaultPath,($local:DiskXML.DiskPath));
		}
		else
		{
			$local:Script = [string]::Format("IF( NOT EXISTS(SELECT 1 FROM RecHubSystem.PartitionDisks WHERE PartitionManagerID = $local:PartitionManagerID AND DiskOrder = {0}) ) INSERT INTO RecHubSystem.PartitionDisks(PartitionManagerID,DiskOrder,DiskPath) VALUES ($local:PartitionManagerID,{0},'{1}');",$local:DiskXML.DiskOrder,$local:DiskXML.DiskPath);
		}
		
		RunScript $DBSERVER $R360DB $local:Script;
	}
}

function RunInstaller([string] $local:DBServer, [string] $local:DBName, [string] $local:Release, [string] $local:InstallFolderPath, [string] $local:CommandFile)
{
	$local:InstallPath = [System.IO.Path]::Combine($INSTALLROOT,$local:Release,$local:InstallFolderPath);
	&"$Installer" -DBServer $local:DBServer -DBName $local:DBName -CommandFile $local:CommandFile -InstallationFolder $local:InstallPath -LogFolder $LOGFOLDER
}

function RunR360InstallXML([string] $local:Release, [string] $local:Version, [string] $local:InstallFolderPath, [string] $local:CommandFile)
{
	$local:Message = [string]::Format("**** R360 Online rollup starting {0} ****",$local:Version);
	Write-Host $local:Message -ForegroundColor YELLOW;
	RunInstaller $DBSERVER $R360DB $local:Release $local:InstallFolderPath $local:CommandFile;
	$local:Message = [string]::Format("**** R360 Online rollup complete for {0} ****",$local:Version);
	Write-Host $local:Message -ForegroundColor GREEN;
}

function RunRAAMInstallXML([string] $local:Release, [string] $local:Version, [string] $local:InstallFolderPath, [string] $local:CommandFile)
{
	$local:Message = [string]::Format("**** R360 Online RAAM rollup starting {0} ****",$local:Version);
	Write-Host $local:Message -ForegroundColor YELLOW;
	RunInstaller $DBSERVER $RAAMDB $local:Release $local:InstallFolderPath $local:CommandFile;
	$local:Message = [string]::Format("**** R360 Online RAAM rollup complete for {0} ****",$local:Version);
	Write-Host $local:Message -ForegroundColor GREEN;
}

function RunScript([String] $local:DBServer,[String] $local:DBName,[String] $local:SQL)
{
	[String] $local:LogFileName;
	[String] $local:LogFolderName = [System.IO.Path]::Combine($LOGFOLDER,"ManualScripts_" + (Get-Date �f yyyyMMddHHmmss));
	
	if( !(Test-Path $local:LogFolderName) )
	{
		New-Item $local:LogFolderName -type directory | Out-Null;
	}
	
	$CommandLine = New-Object System.Text.StringBuilder; 
	[Void]$CommandLine.Append("SQLCMD ");
	[Void]$CommandLine.Append("-S $local:DBServer ");
	[Void]$CommandLine.Append("-d $local:DBName ");
	
	[Void]$CommandLine.Append("-E ");
	
	#we are going to set these as a standard.
	[Void]$CommandLine.Append("-v DBServer=""$local:DBServer"" ");
	[Void]$CommandLine.Append("-v DBName=""$local:DBName"" ");

	[Void]$CommandLine.Append("-Q ");
	[Void]$CommandLine.Append("""$local:SQL""");
	[Void]$CommandLine.Append(" ");	
	[Void]$CommandLine.Append("-r 1 -V 10 ");
	$local:LogFileName = ([System.IO.Path]::Combine($local:LogFolderName,"PartitionSQL_" + "Results.txt"));
	
	[Void]$CommandLine.Append("-o ");
	[void]$CommandLine.Append("""$local:LogFileName""");

	cmd /c $CommandLine;
}

################################################################################
# This function installs the script on the DBServer/DBName and logs the results.
################################################################################
function InstallScript([String] $local:DBServer,[String] $local:DBName,[String] $local:ScriptName)
{

	[String] $local:LogFileName;
	[String] $local:LogFolderName = [System.IO.Path]::Combine($LOGFOLDER,"ManualScripts_" + (Get-Date �f yyyyMMddHHmmss));
	
	if( !(Test-Path $local:LogFolderName) )
	{
		New-Item $local:LogFolderName -type directory | Out-Null;
	}
	
	$CommandLine = New-Object System.Text.StringBuilder; 
	[Void]$CommandLine.Append("SQLCMD ");
	[Void]$CommandLine.Append("-S $local:DBServer ");
	[Void]$CommandLine.Append("-d $local:DBName ");
	
	[Void]$CommandLine.Append("-E ");
	
	#we are going to set these as a standard.
	[Void]$CommandLine.Append("-v DBServer=""$local:DBServer"" ");
	[Void]$CommandLine.Append("-v DBName=""$local:DBName"" ");

	[Void]$CommandLine.Append("-i ");
	[Void]$CommandLine.Append("""$local:ScriptName""");
	[Void]$CommandLine.Append(" ");	
	[Void]$CommandLine.Append("-r 1 -V 10 ");
	$local:LogFileName = ([System.IO.Path]::Combine($local:LogFolderName,[System.IO.Path]::GetFileNameWithoutExtension($local:ScriptName) + "_" + "Results.txt"));
	
	[Void]$CommandLine.Append("-o ");
	[void]$CommandLine.Append("""$local:LogFileName""");
	
	cmd /c $CommandLine;
	return $?;
}

function Prep2.03.13.00
{
	if( InstallScript $DBSERVER $R360DB ([System.IO.Path]::Combine($INSTALLROOT,"2.02","R360_2.02.13.00\DB\RecHub2.1\2.02.13.00\RecHubSystem","CreateDataImportPartitionManager.sql")) )
	{
		Write-Host "Successfully installed CreateDataImportPartitionManager";
	}
	else
	{
		Write-Host "Failed installing CreateDataImportPartitionManager" -ForegroundColor Red;
	}
	if( InstallScript $DBSERVER $R360DB ([System.IO.Path]::Combine($INSTALLROOT,"2.02","R360_2.02.13.00\DB\RecHub2.1\2.02.13.00\RecHubSystem","CreateSessionsPartitionManager.sql")) )
	{
		Write-Host "Successfully installed CreateSessionsPartitionManager";
	}
	else
	{
		Write-Host "Failed installing CreateSessionsPartitionManager" -ForegroundColor Red;
	}
	if( InstallScript $DBSERVER $R360DB ([System.IO.Path]::Combine($INSTALLROOT,"2.02","R360_2.02.13.00\DB\RecHub2.1\2.02.13.00\RecHubSystem","CreateSessionActivityPartitionManager.sql")) )
	{
		Write-Host "Successfully installed CreateSessionActivityPartitionManager";
	}
	else
	{
		Write-Host "Failed installing CreateSessionActivityPartitionManager" -ForegroundColor Red;
	}

	UpdatePartitionDisks 4 $DataImportPartitions "Path to DataImport";
	UpdatePartitionDisks 5 $SessionsPartitions "Path to Sessions Disks";
	UpdatePartitionDisks 6 $SessionActivityPartitions "Path to SessionActivity Disks";
}


Write-Host "**** Starting R360 Database Installer - Roll-up to 2.03.00.00" -ForegroundColor GREEN;

Write-Host "Be sure to update the PartitionDisks.XML before continuing." -ForegroundColor Red -BackgroundColor White;
$UserInput = Read-Host -Prompt "Press [C] to continue, or any other key to quit.";
if( $UserInput.ToUpper() -ne "C" )
{
	return;
}

if( $SQLVersion -eq "2016" )
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole2016";
}
else
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole";
}

# 2.0
RunR360InstallXML "2.00" "2.00.00.00" "R360H_2.00.00.00\Database" "RecHub2.00.00.00.xml";
RunR360InstallXML "2.00" "2.00.01.00" "R360H_2.00.01.00\Database" "RecHub2.00.01.00Patch.xml";
RunR360InstallXML "2.00" "2.00.02.00" "R360H_2.00.02.00\Database" "RecHub2.00.02.00Patch.xml";
RunR360InstallXML "2.00" "2.00.03.00" "R360H_2.00.03.00\Database" "RecHub2.00.03.00Patch.xml";
RunR360InstallXML "2.00" "2.00.04.00" "R360H_2.00.04.00\Database" "RecHub2.00.04.00Patch.xml";

# 2.01
RunR360InstallXML "2.01" "2.01.00.00" "R360_2.01.00.00\DB\RecHub2.1\2.01.00.00" "RecHub2.01.00.00Patch.xml";
RunRAAMInstallXML "2.01" "2.01.00.00" "R360_2.01.00.00\DB\RecHub2.1\2.01.00.00" "RAAMRecHub2.01.00.00Patch.xml";
RunR360InstallXML "2.01" "2.01.01.00" "R360_2.01.01.00\DB\RecHub2.1\2.01.01.00" "R360Online2.01.01.00Patch.xml";
RunRAAMInstallXML "2.01" "2.01.01.00" "R360_2.01.01.00\DB\RecHub2.1\2.01.01.00" "RAAMR360Online2.01.01.00Patch.xml";

# 2.02
RunR360InstallXML "2.02" "2.02.00.00" "R360_2.02.00.00\DB\RecHub2.1\2.02.00.00" "R360Online2.02.00.00Patch.xml";
RunRAAMInstallXML "2.02" "2.02.00.00" "R360_2.02.00.00\DB\RecHub2.1\2.02.00.00" "RAAMR360Online2.02.00.00Patch.xml";
RunR360InstallXML "2.02" "2.02.02.00" "R360_2.02.02.00\DB\RecHub2.1\2.02.02.00" "R360Online2.02.02.00Patch.xml";
RunR360InstallXML "2.02" "2.02.03.00" "R360_2.02.03.00\DB\RecHub2.1\2.02.03.00" "R360Online2.02.03.00Patch.xml";
RunR360InstallXML "2.02" "2.02.04.00" "R360_2.02.04.00\DB\RecHub2.1\2.02.04.00" "R360Online2.02.04.00Patch.xml";
RunR360InstallXML "2.02" "2.02.06.00" "R360_2.02.06.00\DB\RecHub2.1\2.02.06.00" "R360Online2.02.06.00Patch.xml";
RunR360InstallXML "2.02" "2.02.07.00" "R360_2.02.07.00\DB\RecHub2.1\2.02.07.00" "R360Online2.02.07.00Patch.xml";
RunRAAMInstallXML "2.02" "2.02.07.00" "R360_2.02.07.00\DB\RecHub2.1\2.02.07.00" "RAAMR360Online2.02.07.00Patch.xml";
RunR360InstallXML "2.02" "2.02.08.00" "R360_2.02.08.00\DB\RecHub2.1\2.02.08.00" "R360Online2.02.08.00Patch.xml";
RunR360InstallXML "2.02" "2.02.09.00" "R360_2.02.09.00\DB\RecHub2.1\2.02.09.00" "R360Online2.02.09.00Patch.xml";
RunR360InstallXML "2.02" "2.02.10.00" "R360_2.02.10.00\DB\RecHub2.1\2.02.10.00" "R360Online2.02.10.00Patch.xml";
RunR360InstallXML "2.02" "2.02.11.00" "R360_2.02.11.00\DB\RecHub2.1\2.02.11.00" "R360Online2.02.11.00Patch.xml";
RunRAAMInstallXML "2.02" "2.02.11.00" "R360_2.02.11.00\DB\RecHub2.1\2.02.11.00" "RAAMR360Online2.02.11.00Patch.xml";
RunR360InstallXML "2.02" "2.02.12.00" "R360_2.02.12.00\DB\RecHub2.1\2.02.12.00" "R360Online2.02.12.00Patch.xml";
RunRAAMInstallXML "2.02" "2.02.12.00" "R360_2.02.12.00\DB\RecHub2.1\2.02.12.00" "RAAMR360Online2.02.12.00Patch.xml";

Prep2.03.13.00;

RunR360InstallXML "2.02" "2.02.13.00" "R360_2.02.13.00\DB\RecHub2.1\2.02.13.00" "R360Online2.02.13.00Patch.xml";
RunRAAMInstallXML "2.02" "2.02.13.00" "R360_2.02.13.00\DB\RecHub2.1\2.02.13.00" "RAAMR360Online2.02.13.00Patch.xml";
RunR360InstallXML "2.02" "2.02.14.00" "R360_2.02.14.00\DB\RecHub2.1\2.02.14.00" "R360Online2.02.14.00Patch.xml";
RunRAAMInstallXML "2.02" "2.02.14.00" "R360_2.02.14.00\DB\RecHub2.1\2.02.14.00" "RAAMR360Online2.02.14.00Patch.xml";
RunR360InstallXML "2.02" "2.02.15.00" "R360_2.02.15.00\DB\RecHub2.1\2.02.15.00" "R360Online2.02.15.00Patch.xml";
RunR360InstallXML "2.02" "2.02.16.00" "R360_2.02.16.00\DB\RecHub2.1\2.02.16.00" "R360Online2.02.16.00Patch.xml";

# 2.03
RunR360InstallXML "2.03" "2.03.00.00" "R360_2.03.00.00\DB\RecHub2.03\2.03.00.00" "R360Online2.03.00.00Patch.xml";
RunRAAMInstallXML "2.03" "2.03.00.00" "R360_2.03.00.00\DB\RecHub2.03\2.03.00.00" "RAAMRecHub2.03.00.00Patch.xml";

Write-Host '**** R360 Database Rollup to Patch 2.03.00.00 has completed' -ForegroundColor GREEN | Out-Null