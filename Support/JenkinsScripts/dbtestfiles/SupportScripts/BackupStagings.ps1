﻿################################################################################
#
# 02/20/18 #152316370  MGE  2.03.02   Created script to backup all QA databases and truncate logs.
################################################################################


$DBSERVER = "ServerName"
#Adjust the Names of the Databases below to reflect actual Usage

#Path to save backups - Be sure to have a backslash at the end
$BackupPath = "C:\SQLBackup\"

#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\BackupLogs"		## Not implemented at this time - Logs are in same folder as backup scripts
#Drive we're installing XML DLL to

./BackupDB -DBServer $DBSERVER -DBName WFSDB_R360 -BackupPath $BackupPath ## -LogFolder $LOGFOLDER
./BackupDB -DBServer $DBSERVER -DBName RAAM -BackupPath $BackupPath ## -LogFolder $LOGFOLDER
./BackupDB -DBServer $DBSERVER -DBName WFS_SSIS_Configuration -BackupPath $BackupPath ## -LogFolder $LOGFOLDER
./BackupDB -DBServer $DBSERVER -DBName WFS_SSIS_Logging -BackupPath $BackupPath ## -LogFolder $LOGFOLDER
./BackupDB -DBServer $DBSERVER -DBName Framework -BackupPath $BackupPath ## -LogFolder $LOGFOLDER
./BackupDB -DBServer $DBSERVER -DBName IdentityServer -BackupPath $BackupPath ## -LogFolder $LOGFOLDER
./BackupDB -DBServer $DBSERVER -DBName TokenCache -BackupPath $BackupPath ## -LogFolder $LOGFOLDER

Write-Host "Moving old Backups to sub-folder"

$Date = (Get-Date).ToString("yyyyMMdd")
$DailyBackupPath = Join-Path $BackupPath $Date
If (test-path -PathType Container $DailyBackupPath)
{
    Write-Host "Daily Backup Folder exists"
}
else
{
    Write-Host "Daily Backup Folder Doesn't exist-creating it"
    New-Item -ItemType Directory -Path $DailyBackupPath > $null;
}
Move-Item -Path $BackupPath*$Date*.7z -Destination $DailyBackupPath
Write-Host "Files Moved"

Read-Host 'Database Backups Complete.  Press any key to continue' | Out-Null

	