﻿#Version of SQL the install is going against
#Valid values: 2012, 2016
$SQLVersion="2012"
#Name of the SQL Database server
$DBSERVER = "SERVERNAME"
#Name of the R360 Database-Default is WFSDB_R360
$R360DB = "WFSDB_R360"
#Name of the RAAM Database-Default is RAAM
$RAAMDB = "RAAM"
#Name of the SSIS Configuration Database-Default is WFS_SSIS_Configuration
$SSISConfigDB = "WFS_SSIS_Configuration"
#Name of the SSIS Logging Database-Default is WFS_SSIS_Logging
$SSISLoggingDB = "WFS_SSIS_Logging"
#Installation Folder
$INSTALLFOLDER = "D:\WFSStaging\DB\RecHub2.03\2.03.00.00"
#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\Installlogs"
#Drive we're installing XML DLL to
$INSTALLDRIVE = "D:"

IF( $SQLVersion -eq "2016" )
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole2016"
}
ELSE
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole"
}

&"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360Online2.03.00.00Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER

Read-Host 'R360 Patch 2.03.00.00 has completed-Press any key to Run SSIS Patch 2.03.00.00' | Out-Null

&"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360Online2.03.00.00SSISPatch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER

Read-Host 'SSIS Patch 2.03.00.00 has completed-Press any key to Run SSIS Configuration Patch 2.03.00.00' | Out-Null

&"$Installer" -DBServer $DBSERVER -DBName $SSISConfigDB -CommandFile SSISConfigurationDatabase2.03.00.00Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER

&"$Installer"  -DBServer $DBSERVER -DBName $RAAMDB -CommandFile RAAMRecHub2.03.00.00Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER

#Copy SSIS helper DLLs
$BinSourcePath = Join-Path $INSTALLFOLDER "bin"
$BinTargetPath = Join-Path $INSTALLDRIVE "WFSApps\RecHub\bin"
if (!(Test-Path $BinTargetPath)) {
    New-Item $BinTargetPath -ItemType Directory | Out-Null
}
Copy-Item (Join-Path $BinSourcePath "*") $BinTargetPath -Force

Read-Host 'R360 Patch 2.03.00.00 has completed-Press any key to Exit' | Out-Null
