﻿#Version of SQL the install is going against
#Valid values: 2012, 2016
$SQLVersion="2012"
#Name of the SQL Database server housing CDS database
$DBSERVER = "SERVERNAME"
#Name of the CDS Database
$CDSDB = "CDS_ItemProcessing"
#Installation Folder
$INSTALLFOLDER = "D:\WFSStaging\DB\RecHub2.03\2.03.01.00"
#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\Installlogs"
#Drive we're installing XML DLL to
$INSTALLDRIVE = "D:"

IF( $SQLVersion -eq "2016" )
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole2016"
}
ELSE
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole"
}

&"$Installer" -DBServer $DBSERVER -DBName $CDSDB -CommandFile CDSRecHub2.03.01.00Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER

Read-Host 'CDS Patch 2.03.01.00 has completed-Press any key to Exit' | Out-Null
