﻿<#
	.SYNOPSIS
	PowerShell script used to exec a SSIS packages.
	
	.DESCRIPTION
	The script will process the contents of the supplied XML file, executing the defined
	SSIS package passing the variables and values defined in the XML to the package.
	
	.PARAMETER SSISPackageXML
	A XML document with the SSIS package definitions.

#>

param
(
	[parameter(Mandatory = $true)][xml] $SSISPackageXML
)

$ScriptVerison = "2.01";

Write-Host "Execute SSIS Package PowerShell Script Version " $ScriptVerison

################################################################################
## DELUXE Corp (DLX)
## Copyright © 2013-2018 Deluxe Corp. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2013-2018 Deluxe Corp.  Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of DLX and contain DLX trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# DLX (see the DLX license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of DLX.
################################################################################
#
# 04/26/13 JPB	1.0		WI 99478	Created.
# 03/23/15 JPB	2.01	WI 197247	FP to 2.01.
# 02/28/18 MGE	2.03	PT 155590667	Added /REP flag to suppress warnings
#
################################################################################

################################################################################
# This function executes a dtexec command buffer..
################################################################################
function ExecDTUtilCommand([System.Text.StringBuilder]$local:DTUtilCommand,[String] $local:CommandType, [String] $local:LogFileName)
{
	Invoke-Expression -Command $local:DTUtilCommand;
	if ($LASTEXITCODE -eq 0) 
	{
		"Successfully executed "+$local:CommandType | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $true;
	}
	elseif( $LASTEXITCODE -eq 1 )
	{
		"Fail executing "+$local:CommandType | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}
	elseif( $LASTEXITCODE -eq 4 )
	{
		$local:CommandType+" could not locate packagae" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}
	elseif( $LASTEXITCODE -eq 5 )
	{
		$local:CommandType+" cannot load package" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}
	elseif( $LASTEXITCODE -eq 6 )
	{
		"Syntax error executing "+$local:CommandType | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}
	return $Results;
}

################################################################################
# Main.
################################################################################

#Get the package folder path from the XML file
$SSISFolder = $xml.SSISPackage.PackageFolder;

if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

#Create the full path of the SSIS package
$SSISPackageFQFP=$SSISFolder+$xml.SSISPackage.PackageName+".dtsx";
#Create a full path log file name
$LogFile = $SSISFolder+$xml.SSISPackage.PackageName +"_" + (Get-Date –f yyyyMMddHHmmss) +".txt";

Write-Host "Executing SSIS Package" $xml.SSISPackage.PackageName;
"Execute SSIS Package PowerShell Script Version " + $ScriptVerison | Out-File -FilePath $LogFile -Encoding unicode -Append;
"Executing SSIS Package "+$xml.SSISPackage.PackageName | Out-File -FilePath $LogFile -Encoding unicode -Append;

Write-Host "Parameter List";
"" | Out-File -FilePath $LogFile -Encoding unicode -Append
"Parameter List" | Out-File -FilePath $LogFile -Encoding unicode -Append;
"-----------------------------" | Out-File -FilePath $LogFile -Encoding unicode -Append

#Create a string buffer for the command to be executed
$CommandLine = New-Object System.Text.StringBuilder; 
[Void]$CommandLine.Append("dtexec /REP EP /FILE ");
[Void]$CommandLine.Append("'"+$SSISPackageFQFP+"'");

#Loop through the parameters and add them to the command buffer
foreach( $Parameter in $xml.SSISPackage.Parameters.Parameter )
{
	[Void]$CommandLine.Append(" ""/SET ""\Package.Variables[User::");
	[Void]$CommandLine.Append($Parameter.Name);
	[Void]$CommandLine.Append("].Properties[Value]"";");
	[Void]$CommandLine.Append($Parameter.Value);
	[Void]$CommandLine.Append("""");
	Write-Host $Parameter.Name"="$Parameter.Value;
	$Parameter.Name+"="+$Parameter.Value | Out-File -FilePath $LogFile -Encoding unicode -Append;
}

"-----------------------------" | Out-File -FilePath $LogFile -Encoding unicode -Append
"" | Out-File -FilePath $LogFile -Encoding unicode -Append
"********************************************************************************" | Out-File -FilePath $LogFile -Encoding unicode -Append
"SSIS Package Output" | Out-File -FilePath $LogFile -Encoding unicode -Append
"********************************************************************************" | Out-File -FilePath $LogFile -Encoding unicode -Append

#redirect the SSIS package output to the log file
[Void]$CommandLine.Append(" >> "+$LogFile );

#execute the command
$Results = ExecDTUtilCommand $CommandLine "DTExec" $LogFile;

