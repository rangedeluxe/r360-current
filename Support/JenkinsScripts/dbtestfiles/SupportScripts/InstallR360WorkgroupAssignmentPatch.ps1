﻿#Version of SQL the install is going against
#Valid values: 2012, 2016
$SQLVersion="2012"
#Name of the SQL Database server
$DBSERVER = "SERVERNAME"
#Name of the R360 Database-Default is WFSDB_R360
$R360DB = "WFSDB_R360"
#Name of the RAAM Database-Default is RAAM
$INSTALLFOLDER = "D:\WFSStaging\DB\RecHub2.03\2.03.02.00"
#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\InstallLogs"
#Drive we're installing XML DLL to
$INSTALLDRIVE = "D:"

IF( $SQLVersion -eq "2016" )
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole2016"
}
ELSE
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole"
}

&"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360WorkgroupAssignmentPatch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
Read-Host 'R360 Workgroup Assignment Patch has completed-Press any key to continue' | Out-Null

