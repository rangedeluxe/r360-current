--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154233497,'2018.03.16.026','','factNotificationFiles.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:factNotificationFiles.sql,IssueID:154233497,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO

RAISERROR('Creating RecHubData.factNotificationFiles',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubData.factNotificationFiles') IS NOT NULL
	RAISERROR('RecHubData.factNotificationFiles already exists.',10,1) WITH NOWAIT
ELSE
BEGIN
CREATE TABLE RecHubData.factNotificationFiles
(
	factNotificationFileKey BIGINT IDENTITY(1,1) NOT NULL,
	IsDeleted BIT NOT NULL,
	NotificationMessageGroup BIGINT NOT NULL,
	FileSize BIGINT NOT NULL 
		CONSTRAINT DF_factNotificationFiles_FileSize DEFAULT 0,
	UserNotification BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	UserID INT NOT NULL,
	NotificationDateKey INT NOT NULL,
	NotificationFileTypeKey INT NOT NULL, 
	NotificationSourceKey SMALLINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	CreatedBy VARCHAR(128) NOT NULL,
	ModifiedBy VARCHAR(128) NOT NULL,
	FileIdentifier UNIQUEIDENTIFIER NOT NULL, 
	SourceNotificationID UNIQUEIDENTIFIER NOT NULL,
	UserFileName VARCHAR(255) NOT NULL, 
	FileExtension VARCHAR(24) NOT NULL,
	MessageWorkgroups UNIQUEIDENTIFIER NULL
) $(OnNotificationPartition);

EXEC sys.sp_addextendedproperty 
@name = N'Table_Description',
@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: 
*		   
*
* Column Information
* UserNotification - Used to determine if record is a "workgroup" or user
*	notification. 0 mean user, 1 means workgroup notification.
* UserID - Will be 0 for workgroup notification.
*
*
* Modification History
* 05/30/2012 CR 53529 JPB	Created
* 03/06/2013 WI 90821 JBS	Update Table to 2.0 release. Change Schema Name.
*							Add Column: factNotificationFileKey, IsDeleted.
*							Rename Column: CustomerKey to OrganizationKey,
*							factNotificationFileID to factNotificationFileKey
*							LockboxKey to ClientAccountKey, LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factNotificationFileKey to Clustered Index. 
*							Removed Constraints: DF_factNotificationFiles_CreationDate DEFAULT(GETDATE()),
*							DF_factNotificationFiles_ModificationDate DEFAULT(GETDATE()),
*							DF_factNotificationFiles_CreatedBy DEFAULT(SUSER_SNAME()),
*							DF_factNotificationFiles_ModifiedBy DEFAULT(SUSER_SNAME()).
*							Added Column: IsDeleted
* 08/24/2014 WI 160437 JPB	Added UserNotification, UserID, FK to Users.
*							Changed NotificationSourceKey to SMALLINT.
* 03/06/2017 PT 139475715 JBS	Add Column FileSize.
* 02/02/2018 PT 154233497 JPB	Added MessageWorkgroups
******************************************************************************/
',
@level0type = N'SCHEMA',@level0name = RecHubData,
@level1type = N'TABLE',@level1name = factNotificationFiles;

RAISERROR('Creating Foreign Key',10,1) WITH NOWAIT
ALTER TABLE RecHubData.factNotificationFiles ADD
	CONSTRAINT PK_factNotificationFiles PRIMARY KEY NONCLUSTERED (factNotificationFileKey,NotificationDateKey),
	CONSTRAINT FK_factNotificationFiles_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factNotificationFiles_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factNotificationFiles_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factNotificationFiles_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID),
	CONSTRAINT FK_factNotificationFiles_NotificationDateKey FOREIGN KEY(NotificationDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factNotificationFiles_dimNotificationFileTypes FOREIGN KEY(NotificationFileTypeKey) REFERENCES RecHubData.dimNotificationFileTypes(NotificationFileTypeKey),
	CONSTRAINT FK_factNotificationFiles_dimBatchSources FOREIGN KEY(NotificationSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey);

/*
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimBanks;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimOrganizations;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimClientAccounts;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_NotificationDateKey;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimNotificationFileTypes;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimBatchSources;

*/


RAISERROR('Creating Index OLTA.factNotificationFiles.IDX_factNotifications_NotificationDatefactNotificationFileKey',10,1) WITH NOWAIT
CREATE CLUSTERED INDEX IDX_factNotifications_NotificationDatefactNotificationFileKey ON RecHubData.factNotificationFiles
(
	NotificationDateKey,
	factNotificationFileKey
) $(OnNotificationPartition);

END
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

