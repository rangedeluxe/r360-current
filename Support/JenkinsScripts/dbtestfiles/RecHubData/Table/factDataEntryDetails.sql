--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154229602,'2018.03.16.026','','factDataEntryDetails.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:factDataEntryDetails.sql,IssueID:154229602,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO

RAISERROR('Creating RecHubData.factDataEntryDetails',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubData.factDataEntryDetails') IS NOT NULL
	RAISERROR('RecHubData.factDataEntryDetails already exists.',10,1) WITH NOWAIT
ELSE
BEGIN
CREATE TABLE RecHubData.factDataEntryDetails
(
	factDataEntryDetailKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	DepositStatus INT NOT NULL,
	WorkgroupDataEntryColumnKey BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	DataEntryValueDateTime DATETIME NULL,
	DataEntryValueFloat FLOAT NULL,
	DataEntryValueMoney MONEY NULL,
	DataEntryValue VARCHAR(256) NOT NULL
) $(OnDataPartition);

EXEC sys.sp_addextendedproperty 
@name = N'Table_Description',
@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2009-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every data entry field.
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/14/2009 CR 28223 JPB	ProcessingDateKey now NOT NULL.
* 12/23/2009 CR 28238  CS	Add index on ProcessingDateKey
* 02/10/2010 CR 28977 JPB	Added ModificationDate.
* 02/12/2010 CR 29012 JPB	Added missing foreign key on processing date.
* 01/06/2011 CR 32233 JPB	Added native data types.
* 01/11/2011 CR 32302 JPB	Added BatchSourceKey.
* 01/12/2012 CR 49280 JPB	Added SourceProcessingDateKey.
* 03/20/2012 CR 51368 JPB	Created new index.
* 03/26/2012 CR 51542 JPB	Added BatchPaymentTypeKey
* 07/19/2012 CR 54125 JPB	Added BatchNumber.
* 07/19/2012 CR 54134 JPB	Renamed and updated index with BatchNumber.
* 03/05/2013 WI 90483 JBS	Update table to 2.0 release.  Change Schema Name.
*							Added factDataEntryDetailKey to Clustered Index.
*							Changed Indexes to match schema and column renaming.
*							Added Columns: factDataEntryDetailKey, IsDeleted
*							Rename Columns: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey
*							LoadDate to CreationDate.
*							Remove: constraint on ModificationDate, column GlobalBatchID
*							Forward patch:  WI 83270, WI 87217
* 05/30/2014 WI 144025 JPB	Changed BatchID from INT to BIGINT.
* 05/30/2014 WI 144898 JPB	Added SourceBatchID.
* 05/30/2014 WI 144899 JPB	Changed SourceBatchKey to SMALLINT.
* 06/10/2015 WI 217785 JBS  Adding indexes from Regression Analysis.
* 07/07/2015 WI 221748 JPB	Added WorkgroupDataEntryColumnKey.
* 08/18/2015 WI 230082 JPB	Remove dimDataEntry items.
* 02/06/2018 PT 154229602	MGE	Added index to support Advanced Search
******************************************************************************/
',
@level0type = N'SCHEMA',@level0name = RecHubData,
@level1type = N'TABLE',@level1name = factDataEntryDetails;

RAISERROR('Creating Foreign Key',10,1) WITH NOWAIT
ALTER TABLE RecHubData.factDataEntryDetails ADD
	CONSTRAINT PK_factDataEntryDetails PRIMARY KEY NONCLUSTERED (factDataEntryDetailKey,DepositDateKey),
	CONSTRAINT FK_factDataEntryDetails_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factDataEntryDetails_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factDataEntryDetails_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factDataEntryDetails_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntryDetails_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntryDetails_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntryDetails_dimWorkgroupDataEntryColumns FOREIGN KEY(WorkgroupDataEntryColumnKey) REFERENCES RecHubData.dimWorkgroupDataEntryColumns(WorkgroupDataEntryColumnKey),
	CONSTRAINT FK_factDataEntryDetails_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factDataEntryDetails_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey);

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DepositDatefactDataEntryDetailKey',10,1) WITH NOWAIT
CREATE CLUSTERED INDEX IDX_factDataEntryDetails_DepositDatefactDataEntryDetailKey ON RecHubData.factDataEntryDetails
(
	DepositDateKey,
	factDataEntryDetailKey
) $(OnDataPartition);

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_BankKey',10,1) WITH NOWAIT
CREATE INDEX IDX_factDataEntryDetails_BankKey ON RecHubData.factDataEntryDetails (BankKey) $(OnDataPartition);

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_OrganizationKey',10,1) WITH NOWAIT
CREATE INDEX IDX_factDataEntryDetails_OrganizationKey ON RecHubData.factDataEntryDetails (OrganizationKey) $(OnDataPartition);

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_ClientAccountKey',10,1) WITH NOWAIT
CREATE INDEX IDX_factDataEntryDetails_ClientAccountKey ON RecHubData.factDataEntryDetails (ClientAccountKey) $(OnDataPartition);

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_WorkgroupDataEntryColumnKey',10,1) WITH NOWAIT
CREATE INDEX IDX_factDataEntryDetails_WorkgroupDataEntryColumnKey ON RecHubData.factDataEntryDetails (WorkgroupDataEntryColumnKey) $(OnDataPartition);

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_ImmutableDateKey',10,1) WITH NOWAIT
CREATE INDEX IDX_factDataEntryDetails_ImmutableDateKey ON RecHubData.factDataEntryDetails (ImmutableDateKey) $(OnDataPartition);
--CR 51368, 54134,230082

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DataEntryColumnDepositDateBankOrganizationClientAccountImmutableDateKeyBatchIDNumberTransactionID',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_WGDataEntryColumnDepositDateBankOrganizationClientAccountImmutableDateKeyBatchIDNumberTransactionID ON RecHubData.factDataEntryDetails
(
    WorkgroupDataEntryColumnKey ASC,
    DepositDateKey ASC,
    BankKey ASC,
    OrganizationKey ASC,
    ClientAccountKey ASC,
    ImmutableDateKey ASC,
    BatchID ASC,
    BatchNumber ASC,
    TransactionID ASC
)
INCLUDE 
( 
	BatchSequence,
	DataEntryValue,
	DataEntryValueDateTime,
	DataEntryValueMoney
) $(OnDataPartition);
-- WI 90483 (FP:83270), 230082

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DepositDateKeyBatchTransactionIDBatchSequence',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_DepositDateKeyBatchTransactionIDBatchSequence ON RecHubData.factDataEntryDetails
(
	DepositDateKey ASC,
	BatchID ASC,
	TransactionID ASC,
	BatchSequence ASC
)
INCLUDE
(          
	OrganizationKey,
	WorkgroupDataEntryColumnKey,
	DataEntryValue
)  $(OnDataPartition);
-- WI 90483 (FP:87217)

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DepositDateOrganizationKeyBatchTransactionID',10,1) WITH NOWAIT
CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_DepositDateOrganizationKeyBatchTransactionID ON RecHubData.factDataEntryDetails
(
	DepositDateKey ASC,
	OrganizationKey ASC,
	BatchID ASC,
	TransactionID ASC
) $(OnDataPartition);
-- WI 217785, 230082

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey',10,1) WITH NOWAIT
CREATE INDEX IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey ON RecHubData.factDataEntryDetails 
(
	ImmutableDateKey, 
	SourceBatchID, 
	TransactionID, 
	BatchSequence,
	DepositDateKey
) 
INCLUDE 
(
	factDataEntryDetailKey, 
	IsDeleted, 
	BankKey, 
	OrganizationKey, 
	ClientAccountKey, 
	SourceProcessingDateKey, 
	BatchID, 
	BatchNumber, 
	BatchSourceKey, 
	BatchPaymentTypeKey, 
	DepositStatus, 
	WorkgroupDataEntryColumnKey, 
	CreationDate, 
	ModificationDate, 
	DataEntryValueDateTime, 
	DataEntryValueFloat, 
	DataEntryValueMoney, 
	DataEntryValue
) $(OnDataPartition);
-- WI 217785, 230082 

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_SourceBatchIDDepositDateKey',10,1) WITH NOWAIT
CREATE INDEX IDX_factDataEntryDetails_SourceBatchIDDepositDateKey ON RecHubData.factDataEntryDetails 
(
	SourceBatchID,
	DepositDateKey
) 
INCLUDE 
(
	factDataEntryDetailKey, 
	IsDeleted, 
	BankKey, 
	OrganizationKey, 
	ClientAccountKey, 
	ImmutableDateKey,
	SourceProcessingDateKey, 
	BatchID, 
	BatchNumber, 
	BatchSourceKey, 
	BatchPaymentTypeKey, 
	DepositStatus, 
	WorkgroupDataEntryColumnKey,
	TransactionID, 
	BatchSequence, 
	CreationDate, 
	ModificationDate, 
	DataEntryValueDateTime, 
	DataEntryValueFloat, 
	DataEntryValueMoney,
	DataEntryValue
) $(OnDataPartition);

-- PT 154229602

RAISERROR('Creating Index RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DataEntryValue_ClientAccountKey_BankKey',10,1) WITH NOWAIT
CREATE INDEX IDX_factDataEntryDetails_DataEntryValue_ClientAccountKey_BankKey ON RecHubData.factDataEntryDetails 
(
	DataEntryValue,
	ClientAccountKey,
	BankKey
) 
INCLUDE 
(
	factDataEntryDetailKey, 
	IsDeleted, 
	DepositDateKey,
	BatchID,
	TransactionID,
	BatchSequence
) $(OnDataPartition);

END
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

RAISERROR('Applying Permissions',10,1) WITH NOWAIT
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

