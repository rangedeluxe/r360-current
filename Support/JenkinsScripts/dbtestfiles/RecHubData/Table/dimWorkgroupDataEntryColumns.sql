--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154229602,'2018.03.16.026','','dimWorkgroupDataEntryColumns.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:dimWorkgroupDataEntryColumns.sql,IssueID:154229602,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO

RAISERROR('Creating RecHubData.dimWorkgroupDataEntryColumns',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubData.dimWorkgroupDataEntryColumns') IS NOT NULL
	RAISERROR('RecHubData.dimWorkgroupDataEntryColumns already exists.',10,1) WITH NOWAIT
ELSE
BEGIN
CREATE TABLE RecHubData.dimWorkgroupDataEntryColumns
(
	WorkgroupDataEntryColumnKey BIGINT IDENTITY(1,1) NOT NULL 
		CONSTRAINT PK_dimWorkgroupDataEntryColumns PRIMARY KEY CLUSTERED,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	IsCheck BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsCheck DEFAULT 0,
	IsActive BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsActive DEFAULT 1,
	IsRequired BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsRequired DEFAULT 0,
	MarkSense BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_MarkSense DEFAULT 0,
	DataType TINYINT NOT NULL,
	ScreenOrder TINYINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_ModificationDate DEFAULT GETDATE(),
	UILabel NVARCHAR(64) NOT NULL,
	FieldName NVARCHAR(256) NOT NULL,
	SourceDisplayName NVARCHAR(256) NOT NULL
);

EXEC sys.sp_addextendedproperty 
@name = N'Table_Description',
@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2015-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2015-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 07/06/2015
*
* Purpose: Workgoup Data Entry Columns dimension is a type 0 SCD holding  
*	an entry for each Workgroup/BatchSource data entry column. 
*		   
*
* Modification History
* 07/06/2015 WI 221698 JPB	Created
* 05/25/2016 WI 282808 JPB	Added new index for OTIS import performance
* 12/02/2016 PT 127604133 JPB	Added IsRequired
* 03/31/2017 PT 141048573 JPB	Added SELECT permission for RecHubExtractWizard_User
* 02/06/2018 PT 154229602 MGE	Added index to support advanced search
******************************************************************************/
',
@level0type = N'SCHEMA',@level0name = RecHubData,
@level1type = N'TABLE',@level1name = dimWorkgroupDataEntryColumns;

RAISERROR('Creating Foreign Key',10,1) WITH NOWAIT
ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns ADD 
	CONSTRAINT FK_dimWorkgroupDataEntryColumns_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey);


RAISERROR('Creating Index RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_FieldName',10,1) WITH NOWAIT
CREATE INDEX IDX_dimWorkgroupDataEntryColumns_FieldName ON RecHubData.dimWorkgroupDataEntryColumns(FieldName);


RAISERROR('Creating Index RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_BankClientAccountIDBatchSourceKey',10,1) WITH NOWAIT
CREATE INDEX IDX_dimWorkgroupDataEntryColumns_BankClientAccountIDBatchSourceKey ON RecHubData.dimWorkgroupDataEntryColumns
(
	SiteBankID,
	SiteClientAccountID,
	BatchSourceKey
);

--WI 282808

RAISERROR('Creating Index RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID',10,1) WITH NOWAIT
CREATE INDEX IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID ON RecHubData.dimWorkgroupDataEntryColumns
(
	SiteBankID,
	SiteClientAccountID
)
INCLUDE
(
	IsCheck,
	DataType,
	SourceDisplayName
);

--PT 154229602

RAISERROR('Creating Index RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_BatchSourceIsCheckDataTypeUILabel',10,1) WITH NOWAIT
CREATE INDEX IDX_dimWorkgroupDataEntryColumns_BatchSourceIsCheckDataTypeUILabel ON RecHubData.dimWorkgroupDataEntryColumns
(
	BatchSourceKey,
	IsCheck,
	DataType,
	UILabel	
)
INCLUDE
(
	WorkgroupDataEntryColumnKey,
	SiteBankID,
	SiteClientAccountID,
	FieldName
);

END
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

RAISERROR('Applying Permissions',10,1) WITH NOWAIT
GRANT SELECT ON [RecHubData].[dimWorkgroupDataEntryColumns] TO RecHubExtractWizard_User;
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

