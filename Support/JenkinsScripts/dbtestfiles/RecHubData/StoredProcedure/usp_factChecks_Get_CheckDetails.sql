--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (127613853,'2018.03.16.026','','usp_factChecks_Get_CheckDetails.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_factChecks_Get_CheckDetails.sql,IssueID:127613853,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
RAISERROR('Creating Stored Procedure RecHubData.usp_factChecks_Get_CheckDetails',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubData.usp_factChecks_Get_CheckDetails') IS NOT NULL
	DROP PROCEDURE [RecHubData].[usp_factChecks_Get_CheckDetails]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE RecHubData.usp_factChecks_Get_CheckDetails
(	
	@parmDepositDate		DATETIME,
	@parmBatchID			BIGINT,
	@parmBatchSequence		INT,
	@parmIsCheck			BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: SAS
* Date: 08/20/2014
*
* Purpose: To pull out the Check details for ACH and Wire Transfer images
*
* Modification History
* 08/20/2014 CR 160198, WI 160203  SAS	Created.
* 09/09/2014 WI 164608 SAS Changes done to add DDAKey.
* 09/09/2014 WI 169961 SAS Changes done to pull data using Deposit Data, BatchID and TransactionID.
* 10/23/2014 WI 173869 SAS Changes done to pull data using Deposit Data, BatchID and BatchSequence.
* 10/31/2014 WI 174742 SAS Changes done to pull data using factDocuments batch sequence / factcheck batch sequence.
					If @parmIsCheck is true then the batchsequence will match from factCheck table
					If @parmIsCheck is false then the batchsequence will match from factDocument table
* 11/29/2017 PT 127613853 MGE Always use most recent workgroup name					
******************************************************************************/
SET NOCOUNT ON;

DECLARE @TransactionId INT;
DECLARE	@DepositDateKey INT;

BEGIN TRY
	
	SET @DepositDateKey=CAST(CONVERT(VARCHAR,@parmDepositDate,112) AS INT);
	IF @parmIsCheck=1
		BEGIN
			SELECT							
				RTRIM(RecHubData.factChecks.RemitterName)	AS RemitterName,
				RecHubData.factChecks.Amount,
				RecHubData.factChecks.SourceProcessingDateKey,
				RecHubData.dimClientAccountsView.LongName AS WorkGroupLongName,
				RecHubData.dimDDAs.DDA
			FROM	
				RecHubData.factChecks       	
				INNER JOIN RecHubData.dimClientAccounts ON 
					RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
				INNER JOIN RecHubData.dimClientAccountsView ON
					RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
					AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
				INNER JOIN RecHubData.dimDDAs ON 
				  RecHubData.dimDDAs.DDAKey = RecHubData.factChecks.DDAKey
			WHERE  			
					RecHubData.factChecks.DepositDateKey =@DepositDateKey
				AND RecHubData.factChecks.BatchID = @parmBatchID
				AND RecHubData.factChecks.BatchSequence = @parmBatchSequence
				AND RecHubData.factChecks.IsDeleted=0;
		END 
	ELSE 
		--BatchSequence is requested from factDocumentTable, hence pull the check information 
		--based on the TransactionID that matched with the factdocument table
		BEGIN
			SELECT 
				@TransactionID =RecHubData.factDocuments.TransactionID 
			FROM
				RecHubData.factDocuments
			WHERE 
					RecHubData.factDocuments.DepositDateKey =@DepositDateKey
				AND RecHubData.factDocuments.BatchID = @parmBatchID
				AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence
				AND RecHubData.factDocuments.IsDeleted=0;
			

			SELECT							
				RTRIM(RecHubData.factChecks.RemitterName)	AS RemitterName,
				RecHubData.factChecks.Amount,
				RecHubData.factChecks.SourceProcessingDateKey,
				RecHubData.dimClientAccountsView.LongName AS WorkGroupLongName,
				RecHubData.dimDDAs.DDA
			FROM	
				RecHubData.factChecks       	
				INNER JOIN RecHubData.dimClientAccounts ON 
					RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
				INNER JOIN RecHubData.dimClientAccountsView ON
					RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
					AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID		
				INNER JOIN RecHubData.dimDDAs ON 
				  RecHubData.dimDDAs.DDAKey = RecHubData.factChecks.DDAKey
			WHERE  			
					RecHubData.factChecks.DepositDateKey=@DepositDateKey				
				AND RecHubData.factChecks.BatchID = @parmBatchID
				AND RecHubData.factChecks.TransactionID=@TransactionID
				AND RecHubData.factChecks.IsDeleted=0;
		END
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

RAISERROR('Applying Permissions',10,1) WITH NOWAIT
GRANT EXECUTE ON [RecHubData].[usp_factChecks_Get_CheckDetails] TO dbRole_RecHubData;
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

