--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154252232,'2018.03.16.026','','usp_factNotificationFiles_Get_ByNotificationMessageGroup.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_factNotificationFiles_Get_ByNotificationMessageGroup.sql,IssueID:154252232,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
RAISERROR('Creating Stored Procedure RecHubData.usp_factNotificationFiles_Get_ByNotificationMessageGroup',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubData.usp_factNotificationFiles_Get_ByNotificationMessageGroup') IS NOT NULL
	DROP PROCEDURE [RecHubData].[usp_factNotificationFiles_Get_ByNotificationMessageGroup]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE RecHubData.usp_factNotificationFiles_Get_ByNotificationMessageGroup
	@parmNotificationMessageGroup INT,
	@parmSessionID UNIQUEIDENTIFIER
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/04/2012
*
* Purpose: Returns a list of Notification Files.
*
* Modification History
* 09/04/2012 CR  55380 JMC	Created
* 10/14/2013 WI 117127 JMC	Update to 2.0 release. Change schema to RecHubData
*							Change references: Lockbox to ClientAccount
*							Customer to Organization
* 10/30/2013 WI 119339 MLH  Added IsDeleted = 0
* 11/15/2017 PT 152447729 JPB	Removed dimOrganization
* 03/09/2018 PT 154252232 MGE Added call to check whether user has access to all attachments.
*********************************************************************************************/

SET NOCOUNT ON

BEGIN TRY

DECLARE @NoAccessCount INT;
EXEC RecHubData.usp_factNotifications_MessageGroupNoAccessCount @parmNotificationMessageGroup, @parmSessionID, @NoAccessCount OUT;

SELECT 
	RecHubData.factNotificationFiles.factNotificationFileKey,
	RecHubData.factNotificationFiles.NotificationMessageGroup,
	RecHubData.factNotificationFiles.BankKey,
	RecHubData.factNotificationFiles.OrganizationKey,
	RecHubData.factNotificationFiles.ClientAccountKey,
	RecHubData.factNotificationFiles.NotificationDateKey,
	RecHubData.factNotificationFiles.NotificationSourceKey,
	RecHubData.factNotificationFiles.CreationDate,
	RecHubData.factNotificationFiles.ModificationDate,
	RecHubData.factNotificationFiles.CreatedBy,
	RecHubData.factNotificationFiles.ModifiedBy,
	RecHubData.factNotificationFiles.FileIdentifier,
	RecHubData.factNotificationFiles.UserFileName,
	RecHubData.factNotificationFiles.FileExtension,
	RecHubData.dimNotificationFileTypes.FileTypeDescription,
	CAST(RecHubData.factNotificationFiles.NotificationDateKey AS VARCHAR(8)) + '\' +
	CAST(RecHubData.dimBanks.SiteBankID AS VARCHAR(16)) + '\' +
	'Lbx' + '\' +
	CASE WHEN RecHubData.dimClientAccounts.SiteClientAccountID IS NULL THEN '' ELSE CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(16)) + '\' END +
	CAST(RecHubData.factNotificationFiles.FileIdentifier AS VARCHAR(36)) + 
	CASE WHEN LEN(RecHubData.factNotificationFiles.FileExtension) > 0 AND SUBSTRING(RecHubData.factNotificationFiles.FileExtension, 1, 1) <> '.' THEN '.' ELSE '' END +
	RecHubData.factNotificationFiles.FileExtension AS FilePath
FROM
	RecHubData.factNotificationFiles
		LEFT OUTER JOIN RecHubData.dimClientAccounts ON
			RecHubData.factNotificationFiles.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		LEFT OUTER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.BankKey = RecHubData.factNotificationFiles.BankKey
		INNER JOIN RecHubData.dimNotificationFileTypes ON
			RecHubData.factNotificationFiles.NotificationFileTypeKey = RecHubData.dimNotificationFileTypes.NotificationFileTypeKey
WHERE
	RecHubData.factNotificationFiles.NotificationMessageGroup = @parmNotificationMessageGroup
	AND RecHubData.factNotificationFiles.IsDeleted = 0
	AND @NoAccessCount = 0				--Must have access to all workgroups in file

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

RAISERROR('Applying Permissions',10,1) WITH NOWAIT
GRANT EXECUTE ON [RecHubData].[usp_factNotificationFiles_Get_ByNotificationMessageGroup] TO dbRole_RecHubData;
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

