--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154804395,'2018.03.16.026','','usp_dimClientAccounts_Get_UnassignedCount.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_dimClientAccounts_Get_UnassignedCount.sql,IssueID:154804395,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
RAISERROR('Creating Stored Procedure RecHubData.usp_dimClientAccounts_Get_UnassignedCount',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_UnassignedCount') IS NOT NULL
	DROP PROCEDURE [RecHubData].[usp_dimClientAccounts_Get_UnassignedCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_UnassignedCount
(
	@parmFIEntityID			INT,
	@parmUnassignedCount	INT OUTPUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2010-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: KLC
* Date: 07/01/2014
*
* Purpose: Query dimClientAccounts for a count of records for a given FI that do not have 
*			a corresponding RecHubUser.OLWorkgroups tying them to RAAM
*
* Modification History
* 07/01/2014 WI 151234 KLC	Created
* 02/14/2018 PT 154804395 MGE Only Count Active Workgroups
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT	@parmUnassignedCount = COUNT(*)
	FROM	RecHubData.dimClientAccountsView
		JOIN RecHubData.dimBanksView
			ON RecHubData.dimClientAccountsView.SiteBankID = RecHubData.dimBanksView.SiteBankID
		LEFT JOIN RecHubUser.OLWorkGroups
			ON	RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkGroups.SiteBankID
			AND	RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkGroups.SiteClientAccountID
	WHERE	RecHubData.dimBanksView.EntityID = @parmFIEntityID
		AND RecHubUser.OLWorkGroups.EntityID IS NULL
		AND RecHubData.dimClientAccountsView.IsActive = 1;
                 
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

RAISERROR('Applying Permissions',10,1) WITH NOWAIT
GRANT EXECUTE ON [RecHubData].[usp_dimClientAccounts_Get_UnassignedCount] TO dbRole_RecHubData;
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

