--Installation Wrapper provided by CreateReleaseScript Version 3.0
--Target SQL Server Version 2005
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 1997-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*******************************************************************************/

IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpInstallInfo')) DROP TABLE #tmpInstallInfo
GO
CREATE TABLE #tmpInstallInfo (IssueID INT,BuildNumber VARCHAR(20),ChangeSetID VARCHAR(45),ScriptName VARCHAR(256),SystemName VARCHAR(128))
GO
SET NOCOUNT ON
INSERT INTO #tmpInstallInfo(IssueID,BuildNumber,ChangeSetID,ScriptName,SystemName) VALUES (154252232,'2018.03.16.026','','usp_factNotifications_Get_ByNotificationMessageGroup.sql','RecHub')
SET NOCOUNT OFF
GO
RAISERROR('SourceFile:usp_factNotifications_Get_ByNotificationMessageGroup.sql,IssueID:154252232,BuildNumber:2018.03.16.026,ChangeSetID:',10,1) WITH NOWAIT
GO

SET XACT_ABORT ON
GO

BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

SET QUOTED_IDENTIFIER OFF
GO
SET QUOTED_IDENTIFIER ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
RAISERROR('Creating Stored Procedure RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup',10,1) WITH NOWAIT
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF OBJECT_ID('RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup') IS NOT NULL
	DROP PROCEDURE [RecHubData].[usp_factNotifications_Get_ByNotificationMessageGroup]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE PROCEDURE RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup
(
	@parmNotificationMessageGroup	INT,
    @parmTimeZoneBias               INT = 0,
	@parmSessionID					UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/04/2012
*
* Purpose: 
*
* Modification History
* 09/04/2012 CR 55381 JMC	Created
* 05/02/2103 WI 99083 JBS	Update to 2.0 release. Change schema to RecHubData
*							Forward Patch: WI 88732
*							Change references: Lockbox to ClientAccount,
*							Customer to Organization
* 10/22/2013 WI 118375 JMC	Modified to return Notification Dates as UTC
* 11/26/2013 WI 123421 TWE  Modified to return Notification Date adjusted by TimeBias
* 04/03/2015 WI 199541 CMC  Adding Permissions and forward patch of WI 123421
* 03/09/2018 PT 154252232 MGE Added call to check whether user has access to all attachments.
*********************************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	/****************************************************************************
	* Used to compile all of the message parts for a Notification into a single 
	* column which will be returned by the query.
	****************************************************************************/
	DECLARE @MessageText VARCHAR(MAX),
			@NoAccessCount INT;

	EXEC RecHubData.usp_factNotifications_MessageGroupNoAccessCount @parmNotificationMessageGroup, @parmSessionID, @NoAccessCount OUT;

	SET @MessageText = '';

	SELECT 
		@MessageText = @MessageText + MessageText
	FROM 
		RecHubData.factNotifications 
	WHERE 
		RecHubData.factNotifications.NotificationMessageGroup = @parmNotificationMessageGroup
	ORDER BY 
		RecHubData.factNotifications.NotificationMessagePart;

	SELECT 
		RecHubData.factNotifications.factNotificationKey,
		RecHubData.factNotifications.NotificationMessageGroup,
		RecHubData.factNotifications.SourceNotificationID,
		DATEADD(minute, @parmTimeZoneBias, RecHubData.factNotifications.NotificationDateTime) AS NotificationDate,
		(CASE  
			WHEN @NoAccessCount = 0 THEN RecHubData.factNotifications.NotificationFileCount
			ELSE
			0
		END) AS NotificationFileCount,
		@MessageText										AS MessageText,
		RecHubData.factNotifications.NotificationMessagePart,
		RecHubData.dimOrganizations.SiteBankID				AS BankID,
		RecHubData.dimOrganizations.SiteOrganizationID		AS OrganizationID,
		RecHubData.dimOrganizations.Name					AS OrganizationName,
		RecHubData.dimClientAccounts.SiteClientAccountID	AS ClientAccountID,
		RecHubData.dimClientAccounts.LongName				AS ClientAccountName
	FROM
		RecHubData.factNotifications
		INNER JOIN RecHubData.dimOrganizations ON
			RecHubData.factNotifications.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
		LEFT OUTER JOIN RecHubData.dimClientAccounts ON
			RecHubData.factNotifications.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE
		RecHubData.factNotifications.NotificationMessageGroup = @parmNotificationMessageGroup
		AND RecHubData.factNotifications.NotificationMessagePart = 1
		AND RecHubData.factNotifications.IsDeleted = 0;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

RAISERROR('Applying Permissions',10,1) WITH NOWAIT
GRANT EXECUTE ON [RecHubData].[usp_factNotifications_Get_ByNotificationMessageGroup] TO dbRole_RecHubData;
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
	PRINT 'The database update succeeded'
	COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ScriptInstallAudit' AND COLUMN_NAME = 'Results')
BEGIN
	SET NOCOUNT ON
	DECLARE @Error int SET @Error = 0 SELECT TOP 1 @Error = Error FROM #tmpErrors
	DECLARE @MsgType varchar(20)
	DECLARE @SQLCmd varchar(8000)
	DECLARE @CRNumber varchar(128)
	DECLARE @BuildNumber varchar(20)
	DECLARE @SystemName VARCHAR(64)
	DECLARE @ScriptName VARCHAR(256)
	DECLARE @IssueID INT
	DECLARE @AuditSchema VARCHAR(128)

	SELECT @MsgType = CASE @Error WHEN 0 THEN 'Success' ELSE 'Failure' END

	SELECT
		@IssueID = IssueID,
		@BuildNumber = BuildNumber,
		@SystemName = SystemName,
		@ScriptName = ScriptName
	FROM
		#tmpInstallInfo

	SELECT
		@AuditSchema = TABLE_SCHEMA
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'ScriptInstallAudit'
		AND COLUMN_NAME = 'Results'
	SET @SQLCmd = 'INSERT INTO ' + @AuditSchema + '.ScriptInstallAudit(SystemName, IssueID, ScriptName,BuildNumber, Results) VALUES (' + CHAR(39) + @SystemName + CHAR(39) + ',' + CAST(@IssueID AS VARCHAR(15)) + ',' + CHAR(39) + @ScriptName + CHAR(39) + ',' + CHAR(39) + @BuildNumber + CHAR(39) + ',' + CHAR(39) + @MsgType + CHAR(39) + ')'
	EXEC(@SQLCmd)
	SET NOCOUNT OFF
END
GO
DROP TABLE #tmpErrors
GO
DROP TABLE #tmpInstallInfo
GO

