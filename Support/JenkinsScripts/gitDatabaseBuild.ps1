param 
(
	[parameter(Mandatory = $true)][string] $ProjectConfigFile = "",
	[parameter(Mandatory = $true)][string] $WorkspaceFolder = "",
	[parameter(Mandatory = $true)][string] $DatabaseFolder = "",
	[parameter(Mandatory = $true)][string] $BuildNumber = "",
	[parameter(Mandatory = $true)][string] $SharePointVar = "",
	[string] $JobName = "",
	[string] $SinceCommit = "",
	[string] $Version= "Current",
	[string] $Branch = "Master",
	[bool] $DeployJFrog = $true,
	[string] $JFrogApiKey = $ENV:JenkinsDeploySupportArtifactoryAPIToken,
	[string] $JFrogProj = "tmsa.r360"
)

$ScriptName = "GitDatabaseBuild";
$ScriptVerison = "0.1128";

################################################################################
# 10/24/2016
# Things to do:
# 1) Update config file to use Config as the root element. This will allow the same config file to be used for the
# 	GitPublishApp and GitDatabaseBuild. (Project XMLs will not be the same.)
# 2) Update BuildProject to use the new functions to pull in the project XML elements
# 3) Come up with a standard HTML report library. Way to many repeats/similar functions in multiple scripts now.
################################################################################


################################################################################
# 04/12/2016 WI XXXXXX JPB	1.0	Created.
# 10/27/2017 PT #152317681 JPB .28 Added publish path variables
################################################################################

#$ErrorActionPreference = "Stop";
################################################################################
# Import modules needed for this script.
################################################################################
Import-Module -Name $PSScriptRoot\GitLibrary -Force;
Import-Module -Name $PSScriptRoot\PivotalTrackerLibrary -Force;

################################################################################
# Write-AppConsole
# Process/copy destination folders
################################################################################
function Write-AppConsole([String] $local:ConsoleInformation)
{
	begin
	{
	}
	process
	{
		if( $_ -ne $null )
		{
			Write-Host ([string]::Format("{0}[{1}] {2}",$ScriptName,$ScriptVerison,$_));
		}
	}
	end
	{
		Write-Host ([string]::Format("{0}[{1}] {2}",$ScriptName,$ScriptVerison,$local:ConsoleInformation));
	}
}

################################################################################
# Publish a File to JFrog Artifactory
################################################################################
function JFrog-File($local:filepath,$local:destpath) {
	$location = "http://artifactory.deluxe.com/" + $script:JFrogProj + "/" + $local:destpath
	Write-Host "DEBUG: Artifactory location:" + $location;
    $uri = new-object system.uri($location)
    $authheader = @{"X-JFrog-Art-Api"=$script:JFrogApiKey}    
    $content = invoke-webrequest -uri $uri -infile $local:filepath -method put -headers $authheader -contenttype "application/json"
}

################################################################################
# Build-File
# Call the appropriate build process to create a final output
################################################################################
function Build-File([string] $local:BuildFolder,[string] $local:FormatFolder,[string]$local:WorkspaceFolder, 
[string] $local:FileName,[array] $local:NonSchemaFolders,[array] $local:SubProjectFolders,[string] $local:BuildNumber,
[int] $local:IssueID,[string] $local:SHA1,[System.Xml.XmlElement] $local:XMLSubProjectFolders, [boolean]$local:IsCustom,
[string] $local:BaseBuildFolder)
{
	Write-Host "DEBUG: Build-File start";
	$local:IncludeSubFolder = $true;
	#Create a file name without the datbase folder name
	$local:WorkFileName = $local:FileName.SubString($local:FileName.IndexOf("/")).SubString(1);
	#Create the fully qualified source file name
	$local:SourceFileName = Join-Path -Path $local:WorkspaceFolder -ChildPath $local:WorkFileName.Replace("/","\");
	$local:SubProjectFolder = $local:SubProjectFolders | Where-Object {$local:WorkFileName.ToUpper().StartsWith($_.ToUpper())};
	if( $local:SubProjectFolder -ne $null )
	{
		#strip the sub project folder name from the file name that will be used
		$local:WorkFileName = $local:WorkFileName.SubString($local:WorkFileName.IndexOf("/")).SubString(1);
	}
	#$local:NonSchemaFolder = $local:NonSchemaFolders | Where-Object {$local:WorkFileName.ToUpper().StartsWith($_.ToUpper())};
	$local:NonSchemaFolder = $local:NonSchemaFolders | Where-Object {$local:WorkFileName.ToUpper().Split("/")[0] -eq $_.ToUpper()};
	if( $local:NonSchemaFolder -ne $null )
	{
		$local:FileNameArray = $local:WorkFileName.Split("/");
		switch( $local:NonSchemaFolder.ToUpper() )
		{
			"CR"
			{
				$local:SubFolderName = "CR";
			}
			"COMMANDFILE"
			{
				$local:IncludeSubFolder = $false;
			}
			"SCHEMA"
			{
				$local:IncludeSubFolder = $false;
			}
			"SSIS"
			{
				$local:SubFolderName = "SSIS";
			}
			"SUPPORTSCRIPTS"
			{
				if( [System.IO.Path]::GetFileName($local:WorkFileName).ToUpper().StartsWith("INSTALL") )
				{
					$local:IncludeSubFolder = $false;
				}
				else
				{
					$local:SubFolderName = $local:NonSchemaFolder;
				}
			}
			default
			{
				$local:SubFolderName = $local:NonSchemaFolder;
			}
		}
		$local:FileName = $local:FileNameArray[$local:FileNameArray.Count-1];
	}
	else
	{
		$local:FileNameArray = $local:WorkFileName.Split("/");
		if( $local:SubProjectFolder -eq $null )
		{
			$local:SubFolderName = $local:FileNameArray[0];
		}
		else
		{
			$local:XMLSubProjectFolder = $local:XMLSubProjectFolders.SelectSingleNode([string]::Format("//SubProjectFolders/SubProjectFolder[@Name='{0}']",$local:SubProjectFolder));
			if( $local:XMLSubProjectFolder.GetAttribute("SubFolderName") )
			{
				$local:SubFolderName = $local:XMLSubProjectFolder.GetAttribute("SubFolderName");
			}
			else
			{
				$local:SubFolderName = $local:SubProjectFolder;
			}
		}
		$local:FileName = [System.IO.Path]::GetFileName($local:WorkFileName);
	}
	Write-Host "DEBUG: Build-File set files names";
	Write-Host "DBBUG: Build-File subfolder" $local:SubFolderName;
	if( $local:IncludeSubFolder )
	{
		$local:BuildFileName = Join-Path -Path $local:BuildFolder -ChildPath (Join-Path -Path $local:SubFolderName -ChildPath $local:FileName);
		$local:FormatFileName = Join-Path -Path $local:FormatFolder -ChildPath (Join-Path -Path $local:SubFolderName -ChildPath $local:FileName);
	}
	else
	{
		$local:BuildFileName = Join-Path -Path $local:BuildFolder -ChildPath $local:FileName;
		$local:FormatFileName = Join-Path -Path $local:FormatFolder -ChildPath $local:FileName;
	}

	TestCreate-Path ([System.IO.Path]::GetDirectoryName($local:BuildFileName));
	TestCreate-Path ([System.IO.Path]::GetDirectoryName($local:FormatFileName));
	$local:FileExtension = [System.IO.Path]::GetExtension($local:SourceFileName);
	
	Write-AppConsole ([string]::Format("Source File {0}",$local:SourceFileName));
	Write-AppConsole ([string]::Format("Build File  {0}",$local:BuildFileName));
	Write-AppConsole ([string]::Format("Format File {0}",$local:FormatFileName));

	if( !(Test-Path $local:SourceFileName) )
	{
		Write-AppConsole ([string]::Format("Source file {0} not found",$local:SourceFileName));
	}
	else
	{
		switch( $local:FileExtension.ToUpper() )
		{
			".DTSX"
			{
				Format-BIFile $local:SourceFileName $local:BuildFileName $local:FormatFileName;
			}
			".RDL"
			{
				Format-BIFile $local:SourceFileName $local:BuildFileName $local:FormatFileName;
			}
			".XML"
			{
				Write-Host "calling Format-XMLFile" $local:SourceFileName $local:BuildFileName $local:FormatFileName $local:BuildNumber $local:IssueID $local:SHA1;
				Format-XMLFile $local:SourceFileName $local:BuildFileName $local:FormatFileName $local:BuildNumber $local:IssueID $local:SHA1;
			}
			".CONFIG"
			{
				Write-AppConsole ([string]::Format("Copying {0} to {1}",$local:SourceFileName,$local:FormatFileName));
				Copy-Item "$local:SourceFileName" "$local:FormatFileName";
			}
			".PS1"
			{
				Write-AppConsole ([string]::Format("Copying {0} to {1}",$local:SourceFileName,$local:FormatFileName));
				Copy-Item "$local:SourceFileName" "$local:FormatFileName";
			}
			default
			{
				Format-SQLFile $local:SourceFileName $local:BuildFileName $local:FormatFileName $local:SubFolderName $local:BuildNumber $local:IssueID $local:SHA1;
			}
		}

		# Copy out to artifactory as well.
		if ($local:IsCustom -eq $false) {
			$relpath = $local:FormatFileName.Substring($local:BaseBuildFolder.Length + 1)
			JFrog-File $local:FormatFileName $relpath
		}

	}
}


################################################################################
# Add-HTMLNotification-Header
# Create a HTML report header of the items that were processed
################################################################################
function Add-HTMLNotification-Header([ref]$local:BuildResults,[string]$local:Version,[string]$local:Branch,[string]$local:ReportTitle,[string]$local:BuildNumber, [string]$local:BuildFolder)
{
	#Create HTML document with a table
	$local:BuildResults.value.Append("<html><body><table style=""width:100%"">") | Out-Null;
	
	#Add a report header to the table
	$local:BuildResults.value.Append("<tr>") | Out-Null;
	$local:BuildResults.value.AppendFormat("<td colspan=""4"" style=""text-align:center""><h1>{0}</h1></td>",$local:ReportTitle) | Out-Null;
	$local:BuildResults.value.Append("</tr>") | Out-Null;

	$local:BuildResults.value.Append("<tr>") | Out-Null;
	$local:BuildResults.value.AppendFormat("<td colspan=""4"" style=""text-align:center""><h3>Build completed at {0}</h3></td>",(Get-Date -Format "MM/dd/yyyy HH:mm:ss")) | Out-Null;
	$local:BuildResults.value.Append("</tr>") | Out-Null;
	
	$local:BuildResults.value.Append("<table border=""1"" style=""width:100%"">") | Out-Null;
	
	#Add row headers
	$local:BuildResults.value.Append("<tr>") | Out-Null;
	$local:BuildResults.value.AppendFormat("<td><b>Build Number</b></td><td>{0}</td>",$local:BuildNumber) | Out-Null;
	$local:BuildResults.value.AppendFormat("<td><b>Build Location</b></td><td><a href=""file:///{0}"">{1}</a></td>",$local:BuildFolder.Replace("\","/"),$local:BuildFolder) | Out-Null;
	$local:BuildResults.value.Append("</tr>") | Out-Null;
}

################################################################################
# Add-HTMLNotification-CommitComments
# Add the commit comments to the HTML report
################################################################################
function Add-HTMLNotification-CommitComments([ref]$local:BuildResults,[string]$local:CommitComments,[string]$local:SHA1)
{
	$local:BuildResults.value.AppendFormat("<tr ><td colspan=""2"" align=""center"" bgcolor=""#FFFFB0""><b>Commit</b><br>{0}</td><td colspan=""2"" bgcolor=""#FFFFB0"">{1}</td></tr>",$local:SHA1,$local:CommitComments) | Out-Null;
}

function Add-HTMLNotification-FileNameRow([ref]$local:BuildResults)
{
	$local:BuildResults.value.AppendFormat("<tr><td colspan=""2"" align=""right""><br><b>Affected File(s)</b></td><td colspan=""2"">") | Out-Null;
}

function Close-HTMLNotification-FileNameRow([ref]$local:BuildResults)
{
	$local:BuildResults.value.AppendFormat("</td></tr>")  | Out-Null
}

function Add-HTMLNotification-FileName([ref]$local:BuildResults,[string]$local:FileName)
{
	$local:BuildResults.value.AppendFormat("{0}<br />",$local:FileName) | Out-Null;
}

function Add-HTMLNotification-Issue([ref]$local:BuildResults,[int]$local:IssueID,[string]$local:IssueTitle)
{
	$local:BuildResults.value.AppendFormat("<tr><td colspan=""2"" align=""right"">{0}</td><td colspan=""2"">{1}</td></tr>", $local:IssueID, $local:IssueTitle) | Out-Null;
}

################################################################################
# Format-BIFile 
# Copy a BI file from the build folder to the final formatted folder.
################################################################################
function Format-BIFile([string] $local:SourceFileName,[string] $local:BuildFileName,[string] $local:FormatFolder)
{
	Copy-Item "$local:SourceFileName" "$local:BuildFileName" -Force | Out-Null;
	[System.IO.FileInfo]$local:BuildFileInfo = New-Object System.IO.FileInfo($local:BuildFileName);
	$local:BuildFileInfo.IsReadOnly = $true;
	Copy-Item "$local:BuildFileName" "$local:FormatFolder" -Force | Out-Null;
}

################################################################################
# Format-SQLFile 
# Call the format SQL PoSH and copy to the final formatted folder.
################################################################################
function Format-SQLFile([string] $local:SourceFileName,[string] $local:BuildFileName,[string] $local:FormatFileName,[string]$local:Schema,[string] $local:BuildNumber,[int] $local:IssueID,[string] $local:SHA1)
{
	Copy-Item "$local:SourceFileName" "$local:BuildFileName" -Force | Out-Null;
	#Write-Host -sourcefile "$local:BuildFileName" -sourcefolder "[System.IO.Path]::GetDirectoryName($local:BuildFileName)" -schema $local:Schema -IssueID $local:IssueID -BuildNumber $local:BuildNumber -CreateBackup 0 -ChangeSetID $local:SHA1;
	&$PSScriptRoot\CreateReleaseScriptV3.PS1 -sourcefile "$local:BuildFileName" -sourcefolder "[System.IO.Path]::GetDirectoryName($local:BuildFileName)" -Schema $local:Schema -IssueID $local:IssueID -BuildNumber $local:BuildNumber -CreateBackup 0 -ChangeSetID $local:SHA1;
	Copy-Item "$local:BuildFileName" "$local:FormatFileName" -Force | Out-Null;
}

################################################################################
# Format-XMLFile 
# Add attriutes to a XML file and copy from the build folder to the final formatted folder.
################################################################################
function Format-XMLFile([string] $local:SourceFileName,[string] $local:BuildFileName,[string] $local:FormatFileName,[string] $local:BuildNumber,[int] $local:IssueID,[string] $local:SHA1)
{
	$local:XML = New-Object "System.Xml.XmlDocument";
	$local:XML.Load($local:SourceFileName);
	$IssueAttribute = $XML.CreateAttribute("IssueID");
	$IssueAttribute.Value = $local:IssueID;
	$local:XML.FirstChild.Attributes.Append($IssueAttribute) | Out-Null;
	$local:BuildAttribute = $local:XML.CreateAttribute("BuildNumber");
	$local:BuildAttribute.Value = $local:BuildNumber;
	$local:XML.FirstChild.Attributes.Append($local:BuildAttribute) | Out-Null;
	$ChangeSetAttribute = $XML.CreateAttribute("ChangeID");
	$ChangeSetAttribute.Value = $local:SHA1;
	$XML.FirstChild.Attributes.Append($ChangeSetAttribute) | Out-Null;
	$local:ScriptNameAttribute = $local:XML.CreateAttribute("ScriptName");
	$local:ScriptNameAttribute.Value = [System.IO.Path]::GetFileName($local:BuildFileName);
	$local:XML.FirstChild.Attributes.Append($local:ScriptNameAttribute) | Out-Null;
	$local:XML.Save($local:BuildFileName);

	Copy-Item "$local:BuildFileName" "$local:FormatFileName" -Force | Out-Null;
}

################################################################################
# Get-PTIssueInfoLocal
# Get the issue info from Pivotal Tracker
################################################################################
function Get-PTIssueInfoLocal([int]$local:IssueID)
{
	$local:Story = Get-PTStory $local:IssueID;
	$local:Title = $local:Story.name;
	return $local:Title;
}

################################################################################
# TestCreate-Path
# See if path exists, create it if not
################################################################################
function TestCreate-Path([string] $local:Folder)
{
	if( !(Test-Path $local:Folder) )
	{
		New-Item $local:Folder -type directory | Out-Null;
	}
}

################################################################################
# Get-NonSchemaFolders
################################################################################
function Get-NonSchemaFolders([System.Xml.XmlElement]$local:XMLNonSchemaFolders,[ref]$local:NonSchemaFolders)
{
	if( $local:XMLNonSchemaFolders -ne $null )
	{
		foreach( $local:XMLNonSchemaFolder in $local:XMLNonSchemaFolders.NonSchemaFolder)
		{
			$local:NonSchemaFolders.value+= $local:XMLNonSchemaFolder.Name;
		}
	}
}

################################################################################
# Get-IncludeFileExtensions
################################################################################
function Get-IncludeFileExtensions([System.Xml.XmlElement]$local:XMLIncludeFileExtensions,[ref]$local:IncludeFileExtensions)
{
	if( $local:XMLIncludeFileExtensions -ne $null )
	{
		foreach( $local:XMLIncludeFileExtension in $local:XMLIncludeFileExtensions.IncludeFileExtension)
		{
			if( !$local:XMLIncludeFileExtension.Name.StartsWith(".") )
			{
				$local:XMLIncludeFileExtension.Name = "." + $local:XMLIncludeFileExtension.Name;
			}
			$local:IncludeFileExtensions.value+= $local:XMLIncludeFileExtension.Name.ToUpper();
		}
	}
}

################################################################################
# Get-SubProjectFolders
################################################################################
function Get-SubProjectFolders([System.Xml.XmlElement]$local:XMLSubProjectFolders,[ref]$local:SubProjectFolders)
{
	if( $local:XMLSubProjectFolders -ne $null )
	{
		foreach( $local:XMLSubProjectFolder in $local:XMLSubProjectFolders.SubProjectFolder)
		{
			$local:SubProjectFolders.value+= $local:XMLSubProjectFolder.Name.ToUpper();
		}
	}

}

################################################################################
# BuildCustom
################################################################################
function BuildCustom([string] $local:ConfigFile,[string] $local:ProjectConfigFile,
[string] $local:LocalRepo,[string] $local:Version,[string] $local:Branch,
[string] $local:BuildNumber,[string] $local:LastCommit,[string] $local:WorkspaceFolder, 
[string] $local:BaseBuildFolder,[string] $local:PublishFolder,[string] $local:SharePointVar,[string] $local:JobName)
{
	#Array for the commit list
    $local:BuildList = New-Object System.Collections.ArrayList;
	#Array to hold file names that belong to a commit
	$local:FileNameList = New-Object System.Collections.ArrayList;
	#Array to hold folders that are not schemas
	$local:NonSchemaFolders=@();	
	#Array to hold the file extensions that will be processed
	$local:IncludeFileExtensions=@();
	#Array to hold the sub project folder names
	$local:SubProjectFolders=@();
	
	#Load the Config XML
	$local:ConfigXML = New-Object "System.Xml.XmlDocument";
	$local:ConfigXML.Load($local:ConfigFile);
	
	#Load the project config XML
	$local:ProjectConfigXML = New-Object "System.Xml.XmlDocument";
	$local:ProjectConfigXML.Load($local:ProjectConfigFile);
	
	
	#Load the non schemas array
	Get-NonSchemaFolders $local:ProjectConfigXML.SelectSingleNode("//BuildConfig/NonSchemaFolders") ([ref] $local:NonSchemaFolders);
	
	#Load the file extensions array
	Get-IncludeFileExtensions $local:ProjectConfigXML.SelectSingleNode("//BuildConfig/IncludeFileExtensions") ([ref] $local:IncludeFileExtensions);

	#Load the sub project array
	$local:XMLSubProjectFolders = $local:ProjectConfigXML.SelectSingleNode("//BuildConfig/SubProjectFolders");
	Get-SubProjectFolders $local:ProjectConfigXML.SelectSingleNode("//BuildConfig/SubProjectFolders") ([ref] $local:SubProjectFolders);

	#Setup the build and format folders
	$local:FormatFolder = Join-Path -Path $local:BaseBuildFolder -ChildPath $local:PublishFolder;
	Write-AppConsole ([string]::Format("Publish Path         : {0}",$local:FormatFolder));


	#Set the active folder to the local repo folder
	Set-Location $local:WorkspaceFolder;

	#Get the database folder name from the input path
	$local:FolderList = $local:LocalRepo.Split("\");
	$local:DatabaseFolderName = $local:FolderList[$local:FolderList.Count-1];

	
	#Get a list of files that have changed since the last run
	Get-BuildListByCommit ([ref]$local:BuildList) $local:LastCommit $true $true;
	Write-AppConsole ([string]::Format("Found {0} commits to process",$local:BuildList.Count));
	if( $local:BuildList.Count -gt 0 )
	{
		$local:ReportHTML = New-Object System.Text.StringBuilder; 
		#Walk the list of commits (SHA1)
		foreach( $local:SHA1 in $local:BuildList )
		{

			Get-FileNamesFromSHA1 ([ref] $local:FileNameList) $local:SHA1;
			$local:DBFileNameList = $local:FileNameList -like ("*" + $local:DatabaseFolderName + "*");
			Write-AppConsole ([string]::Format("Found {0} of {1} database files in commit {2}",$local:DBFileNameList.Count,$local:FileNameList.Count,$local:SHA1));
	
			if( $local:DBFileNameList.Count -gt 0 )
			{
				$local:IssueIDs = New-Object System.Collections.ArrayList;
				[string] $local:SystemType="";
				$local:CommitText = Get-IssueIDsFromSHA1 $local:SHA1 ([ref] $local:SystemType) ([ref] $local:IssueIDs);
				
				if( $local:IssueIDs.Count -gt 0 )
				{
					$local:IssueID = $local:IssueIDs[0];
					$local:IssueTitle = Get-PTIssueInfoLocal $local:IssueID;
				}
				else 
				{
					$local:IssueID = -1;
					$local:IssueTitle = "Story ID not linked";
				}

				#Remove merged commits from report.
				if( $local:CommitText.ToUpper().StartsWith("MERGE") )
				{
					continue;
				}
				
				$local:FileNameAdded = $false;
				#Walk the list of file names
				foreach( $local:FileName in $local:DBFileNameList )
				{
					if( ($local:IncludeFileExtensions | Where-Object {$_ -match [System.IO.Path]::GetExtension($local:FileName).ToUpper()}) -ne $null )
					{
						$local:CustomerName = $local:FileName.Split("/")[0]
						$local:BuildFolder = Join-Path -Path $local:WorkspaceFolder -ChildPath (Join-Path -Path $local:CustomerName -ChildPath "Database\Staging");
						$local:FormatFolder = $local:FormatFolder.Replace("[@CUSTOMER]",$local:CustomerName);
						
						TestCreate-Path $local:BuildFolder;
						TestCreate-Path $local:FormatFolder

						#Add the header to the report.
						if( $local:ReportHTML.Length -eq 0 )
						{
							Add-HTMLNotification-Header ([ref]$local:ReportHTML) $local:Version $local:Branch $local:ProjectConfigXML.BuildConfig.Output.Report.Title $local:BuildNumber $local:FormatFolder $local:BuildFolder;
						}
						
						if( !$local:FileNameAdded )
						{
							Add-HTMLNotification-CommitComments ([ref]$local:ReportHTML) $local:CommitText $local:SHA1;
							if( $local:IssueID -ne -1 ) 
							{
								Add-HTMLNotification-Issue ([ref]$local:ReportHTML) $local:IssueID $local:IssueTitle;
							}
							Add-HTMLNotification-FileNameRow ([ref]$local:ReportHTML);
							$local:FileNameAdded = $true;
						}
						
						Add-HTMLNotification-FileName ([ref]$local:ReportHTML) $local:FileName;
						Build-File $local:BuildFolder $local:FormatFolder (Join-Path -Path $local:WorkspaceFolder -ChildPath $local:CustomerName) $local:FileName $local:NonSchemaFolders $local:SubProjectFolders $local:BuildNumber $local:IssueID $local:SHA1 $local:XMLSubProjectFolders $true $local:BaseBuildFolder;
					}
				}
				if( $local:FileNameAdded )
				{
					Close-HTMLNotification-FileNameRow ([ref]$local:ReportHTML);
				}
			}
		}
		if( $local:ReportHTML.Length -gt 0 )
		{
			Write-AppConsole ([string]::Format("Saving Build Report to {0}",$local:SharePointVar));
			$local:ReportHTML.ToString() | Set-Content (Join-Path -Path $local:BuildFolder -ChildPath ($local:JobName + "_" + $local:BuildNumber + ".html"));

			$local:NotificationXML=[string]::Format("<SharePointNotification><ProjectName>{0}</ProjectName><Version>{1}</Version><Branch>{2}</Branch><NotificationFile>{3}</NotificationFile></SharePointNotification>",
				$local:ProjectConfigXML.BuildConfig.Output.Report.SharePoint.ProjectName,
				$local:Version,
				$local:Branch,
				(Join-Path -Path $local:BuildFolder -ChildPath ($local:JobName + "_" + $local:BuildNumber + ".html"))
				);
			[Environment]::SetEnvironmentVariable($local:SharePointVar, $local:NotificationXML, "User");
		}
	}	
}

################################################################################
# BuildProject
# To keep things easy to read, the guts of the script are in this function
################################################################################
function BuildProject([string] $local:ConfigFile,[string] $local:ProjectConfigFile,
[string] $local:LocalRepo,[string] $local:Version,[string] $local:Branch,
[string] $local:BuildNumber,[string] $local:LastCommit,[string] $local:WorkspaceFolder, 
[string] $local:BaseBuildFolder,[string] $local:PublishFolder,[string] $local:SharePointVar)
{

	#Array to hold file names that belong to a commit
	$local:FileNameList = New-Object System.Collections.ArrayList;
	#Arry to hold the issues objects
	$local:Issues = @();
	#Array to hold folders that are not schemas
	$local:NonSchemaFolders=@();	
	#Array to hold the file extensions that will be processed
	$local:IncludeFileExtensions=@();
	#Array to hold the sub project folder names
	$local:SubProjectFolders=@();
	#Array for the commit list
    $local:BuildList = New-Object System.Collections.ArrayList;

	#Get the current year to be used for build info
	$local:BuildYear = (Get-Date -Format yyyy);
	
	#Load the Config XML
	$local:ConfigXML = New-Object "System.Xml.XmlDocument";
	$local:ConfigXML.Load($local:ConfigFile);
	
	#Load the project config XML
	$local:ProjectConfigXML = New-Object "System.Xml.XmlDocument";
	$local:ProjectConfigXML.Load($local:ProjectConfigFile);

	#Load the non schemas array
	$local:XMLNonSchemaFolders = $local:ProjectConfigXML.SelectSingleNode("//BuildConfig/NonSchemaFolders");
	if( $local:XMLNonSchemaFolders -ne $null )
	{
		foreach( $local:XMLNonSchemaFolder in $local:XMLNonSchemaFolders.NonSchemaFolder)
		{
			$local:NonSchemaFolders+= $local:XMLNonSchemaFolder.Name;
		}
	}

	#Load the file extensions array
	$local:XMLIncludeFileExtensions = $local:ProjectConfigXML.SelectSingleNode("//BuildConfig/IncludeFileExtensions");
	if( $local:XMLIncludeFileExtensions -ne $null )
	{
		foreach( $local:XMLIncludeFileExtension in $local:XMLIncludeFileExtensions.IncludeFileExtension)
		{
			if( !$local:XMLIncludeFileExtension.Name.StartsWith(".") )
			{
				$local:XMLIncludeFileExtension.Name = "." + $local:XMLIncludeFileExtension.Name;
			}
			$local:IncludeFileExtensions+= $local:XMLIncludeFileExtension.Name.ToUpper();
		}
	}

	#Load the sub project array
	$local:XMLSubProjectFolders = $local:ProjectConfigXML.SelectSingleNode("//BuildConfig/SubProjectFolders");
	if( $local:XMLSubProjectFolders -ne $null )
	{
		foreach( $local:XMLSubProjectFolder in $local:XMLSubProjectFolders.SubProjectFolder)
		{
			$local:SubProjectFolders+= $local:XMLSubProjectFolder.Name;
		}
	}
	
	#Setup the build and format folders
	$local:BuildFolder = Join-Path -Path $local:WorkspaceFolder -ChildPath (Join-Path -Path $local:LocalRepo -ChildPath "Staging");
	$local:FormatFolder = Join-Path -Path $local:BaseBuildFolder -ChildPath $local:PublishFolder;
	Write-AppConsole ([string]::Format("Build Path           : {0}",$local:BuildFolder));
	Write-AppConsole ([string]::Format("Publish Path         : {0}",$local:FormatFolder));

	TestCreate-Path $local:BuildFolder;
	TestCreate-Path $local:FormatFolder
	
	#Set the active folder to the local repo folder
	Set-Location $local:WorkspaceFolder;
	#Set Git to the branch that will be built
	#Set-Checkout $local:Branch;
	
	#Get the database folder name from the input path
	$local:FolderList = $local:LocalRepo.Split("\");
	$local:DatabaseFolderName = $local:FolderList[$local:FolderList.Count-1];

	#Get a list of files that have changed since the last run
	Get-BuildListByCommit ([ref]$local:BuildList) $local:LastCommit $true $true;
	Write-AppConsole ([string]::Format("Found {0} commits to process",$local:BuildList.Count));
	if( $local:BuildList.Count -gt 0 )
	{
		$local:ReportHTML = New-Object System.Text.StringBuilder; 

		#Walk the list of commits (SHA1)
		foreach( $local:SHA1 in $local:BuildList )
		{
			#Write-AppConsole $local:SHA1;
			
			#Get a list of files names associated with the commit
			Get-FileNamesFromSHA1 ([ref] $local:FileNameList) $local:SHA1;
			$local:DBFileNameList = $local:FileNameList -like ($local:DatabaseFolderName + "*");
			Write-AppConsole ([string]::Format("Found {0} of {1} database files in commit {2}",$local:DBFileNameList.Count,$local:FileNameList.Count,$local:SHA1));
			
			if( $local:DBFileNameList.Count -gt 0 )
			{

				$local:IssueIDs = New-Object System.Collections.ArrayList;
				[string] $local:SystemType="";
				$local:CommitText = Get-IssueIDsFromSHA1 $local:SHA1 ([ref] $local:SystemType) ([ref] $local:IssueIDs);
				if( $local:IssueIDs.Count -gt 0 )
				{
					$local:IssueID = $local:IssueIDs[0];
					$local:IssueTitle = Get-PTIssueInfoLocal $local:IssueID;
				}
				else 
				{
					$local:IssueID = -1;
					$local:IssueTitle = "Story ID not linked";
				}
				
				#Remove merged commits from report.
				if( $local:CommitText.ToUpper().StartsWith("MERGE") )
				{
					continue;
				}
				
				$local:FileNameAdded = $false;
				#Walk the list of file names
				foreach( $local:FileName in $local:DBFileNameList )
				{
					#$local:FileNameAdded = $false;
					if( $local:FileName.StartsWith($local:DatabaseFolderName) )
					{
						Write-Host 	"DEBUG: " $local:FileName;				
						if( ($local:IncludeFileExtensions | Where-Object {$_ -match [System.IO.Path]::GetExtension($local:FileName).ToUpper()}) -ne $null )
						{
							#Add the header to the report.
							if( $local:ReportHTML.Length -eq 0 )
							{
								Add-HTMLNotification-Header ([ref]$local:ReportHTML) $local:Version $local:Branch $local:ProjectConfigXML.BuildConfig.Output.Report.Title $local:BuildNumber $local:FormatFolder $local:BuildFolder;
							}
							if( !$local:FileNameAdded )
							{
#								$local:IssueIDs = New-Object System.Collections.ArrayList;
#								[string] $local:SystemType="";
#								$local:CommitText = Get-IssueIDsFromSHA1 $local:SHA1 ([ref] $local:SystemType) ([ref] $local:IssueIDs);
#								if( $local:IssueIDs.Count -gt 0 )
#								{
#									$local:IssueID = $local:IssueIDs[0];
#									$local:IssueTitle = Get-PTIssueInfoLocal $local:IssueID;
#								}
#								else 
#								{
#									$local:IssueID = -1;
#									$local:IssueTitle = "N/A";
#								}
								Add-HTMLNotification-CommitComments ([ref]$local:ReportHTML) $local:CommitText $local:SHA1;
								if( $local:IssueID -ne -1 ) 
								{
									Add-HTMLNotification-Issue ([ref]$local:ReportHTML) $local:IssueID $local:IssueTitle;
								}
								Add-HTMLNotification-FileNameRow ([ref]$local:ReportHTML);
								$local:FileNameAdded = $true;
							}
							Add-HTMLNotification-FileName ([ref]$local:ReportHTML) $local:FileName;
							Build-File $local:BuildFolder $local:FormatFolder (Join-Path -Path $local:WorkspaceFolder -ChildPath $local:LocalRepo) $local:FileName $local:NonSchemaFolders $local:SubProjectFolders $local:BuildNumber $local:IssueID $local:SHA1 $local:XMLSubProjectFolders $false $local:BaseBuildFolder;
							#Add-BuildNumberFromSHA1 $local:SHA1 $local:BuildNumber;
						}
					}
				}
				if( $local:FileNameAdded )
				{
					Close-HTMLNotification-FileNameRow ([ref]$local:ReportHTML);
				}
			}
		}
		if( $local:ReportHTML.Length -gt 0 )
		{
			Write-AppConsole ([string]::Format("Saving Build Report to {0}",$local:SharePointVar));
			$local:ReportHTML.ToString() | Set-Content (Join-Path -Path $local:BuildFolder -ChildPath ($local:BuildNumber + ".html"));

			$local:NotificationXML=[string]::Format("<SharePointNotification><ProjectName>{0}</ProjectName><Version>{1}</Version><Branch>{2}</Branch><NotificationFile>{3}</NotificationFile></SharePointNotification>",
				$local:ProjectConfigXML.BuildConfig.Output.Report.SharePoint.ProjectName,
				$local:Version,
				$local:Branch,
				(Join-Path -Path $local:BuildFolder -ChildPath ($local:BuildNumber + ".html"))
				);
			[Environment]::SetEnvironmentVariable($local:SharePointVar, $local:NotificationXML, "User");
		}
	}
}

################################################################################
# Update-LastRunTime
# Update the project XML with the latest run time.
################################################################################
function Update-LastRunTime([string] $local:ProjectConfigFile,[string] $local:Version,[string] $local:Branch,[DateTime]$local:CurrentRunTime)
{
	#Load the project config XML
	$local:ProjectConfigXML = New-Object "System.Xml.XmlDocument";
	$local:ProjectConfigXML.Load($local:ProjectConfigFile);
	#Get the branch node from the config XML
	$local:BranchXML = $local:ProjectConfigXML.BuildConfig.Versions.SelectSingleNode([string]::Format("./Version[@Name='{0}']/Branches/Branch[@Name='{1}']",$local:Version,$local:Branch));
	$local:BranchXML.LastRun = $local:CurrentRunTime.ToString();
	$local:ProjectConfigXML.Save($local:ProjectConfigFile);
}

################################################################################
# Main
################################################################################
cls

#Setup var to hold path replacement variables
$PathDictionary = @{};

if( $SinceCommit.Length -eq 0 -or $SinceCommit -eq $null )
{
	$SinceCommit = $Env:GIT_PREVIOUS_SUCCESSFUL_COMMIT;
}

if( $JobName.Length -eq 0 -or $JobName -eq $null )
{
	$JobName = $Env:JOB_NAME;
}

$CurrentRun = (Get-Date -Format "MM/dd/yyyy HH:mm:ss");

#Get the config file name from the project config file
$ConfigFile = [System.IO.Path]::GetFileNameWithoutExtension($ProjectConfigFile);
$ConfigFile = $ConfigFile.SubString(0,$ConfigFile.IndexOf("_"));
$ConfigFile = Join-Path -Path ([System.IO.Path]::GetDirectoryName($ProjectConfigFile)) -ChildPath ($ConfigFile+".Config");

$ProjectXML = New-Object "System.Xml.XmlDocument";
$ProjectXML.Load($ProjectConfigFile);

$PublishFolder = $ProjectXML.BuildConfig.Output.PublishFolder;

switch( $Branch.ToUpper() )
{
	"DEV" 
	{
		$BaseBuildFolder = $ProjectXML.BuildConfig.Output.DevBuildFolder;
	}
	default
	{
		$BaseBuildFolder = $ProjectXML.BuildConfig.Output.QABuildFolder;
	}
}

$ReleaseConfigFileName = (Join-Path -Path $WorkspaceFolder -ChildPath (Join-Path -Path $DatabaseFolder -ChildPath "Release.config"));
if( Test-Path $ReleaseConfigFileName )
{
	$ReleaseConfig = New-Object "System.Xml.XmlDocument";
	$ReleaseConfig.Load($ReleaseConfigFileName);
	$PathDictionary.Add("[@PRODUCTNAME]",$ReleaseConfig.Release.ProductName);
	$PathDictionary.Add("[@RELEASEMAJOR]",$ReleaseConfig.Release.ReleaseMajor);
	$PathDictionary.Add("[@RELEASEMINOR]",$ReleaseConfig.Release.ReleaseMinor);
	$PathDictionary.Add("[@RELEASEHOTFIX]",$ReleaseConfig.Release.ReleaseHotFix);
	$PathDictionary.Add("[@RELEASEPATCH]",$ReleaseConfig.Release.ReleasePatch);
}

$PathDictionary.Add("[@VERSION]",$Version);
$PathDictionary.Add("[@YEAR]",(Get-Date).year);

if( $BaseBuildFolder.ToUpper().Contains("[@SPRINT]") )
{
	if( $JobName.ToUpper().StartsWith("WAU-R360" ) )
	{
		$PTProjectInfo = Get-PTProjectInfo "R360 - Backlog";
	}
	elseif ( $JobName.ToUpper().StartsWith("WAU-INTEGRAPAY" ) )
	{
		$PTProjectInfo = Get-PTProjectInfo "integraPAY - Scattered Brains";
	}
	$PathDictionary.Add("[@SPRINT]",$PTProjectInfo.current_iteration_number);
}

#replace path variables with real values
foreach( $PathDictionaryKey in $PathDictionary.keys )
{
	$BaseBuildFolder = $BaseBuildFolder.Replace($PathDictionaryKey,$PathDictionary[$PathDictionaryKey]);
	$PublishFolder = $PublishFolder.Replace($PathDictionaryKey,$PathDictionary[$PathDictionaryKey]);
}

#write out what is happening
Write-AppConsole ([string]::Format("Build Number         : {0}",$BuildNumber));
Write-AppConsole ([string]::Format("Database Folder      : {0}",$DatabaseFolder));
Write-AppConsole ([string]::Format("Building from Commit : {0}",$SinceCommit));

if( !(Test-Path $ConfigFile) )
{
	Write-AppConsole "Cannot find config file $ConfigFile. Aborting build...";
	return
}


if( !(Test-Path $ProjectConfigFile) )
{
	Write-AppConsole "Cannot find config file $ProjectConfigFile. Aborting build...";
	return
}

if( $Version.ToUpper() -eq "Custom" )
{
	BuildCustom $ConfigFile $ProjectConfigFile $DatabaseFolder $Version $Branch $BuildNumber $SinceCommit $WorkspaceFolder $BaseBuildFolder $PublishFolder $SharePointVar $JobName;
}
else
{
	BuildProject $ConfigFile $ProjectConfigFile $DatabaseFolder $Version $Branch $BuildNumber $SinceCommit $WorkspaceFolder $BaseBuildFolder $PublishFolder $SharePointVar;
}
