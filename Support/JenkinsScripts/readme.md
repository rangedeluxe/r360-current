# What am I looking at?

This is a place to contain some test data, config files, and output for our gitPublishApp.ps1 file.

Just run

```
powershell .\testGitPublishApp.ps1
```

To test the publish script.  Feel free to edit the configuration files and the test script's arguments.

**Note**: The official location for our shared publish scripts is located here:
http://bitbucket.deluxe.com/projects/WFS/repos/shared-common/browse/Src/PowerShellScripts