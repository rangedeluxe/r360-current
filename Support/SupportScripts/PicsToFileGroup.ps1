﻿########################################
# Change these to the file paths you need
########################################
$from = "c:\temp\images\"                  # Location of PICS Image folders
$to = "c:\temp\filegroup"                  # Location to copy folders to  
$stage = "c:\temp\filegroup\images\"       # Staging location for directories to process
$final = "c:\temp\filegroup\new\"          # Location of FileGroup Image Folders

#Remove-Item $stage -recurse
Copy-Item $from $to -recurse 
New-Item $final -type directory

# Create PaymentSource folders in the staging folder
Get-ChildItem $stage -dir |
 ForEach-Object {
        Write-Host $_.FullName 
        Copy-Item $_.FullName $final
     }

# Copy the Date folder under the WorkGroup folder
# copy the images into new folder
Get-ChildItem $stage\*\* -dir |
    ForEach-Object {
        Write-Host $_.FullName 
        $dest = $_.FullName + "\*\*"
        $destFolder = $_.Name
         Get-ChildItem $dest -dir |
            ForEach-Object {
               Write-Host $_.Name 
               $newDest = $_.FullName + "\" + $destFolder
               New-Item $newDest -type directory
               Get-ChildItem $dest\*.tif | 
                ForEach-Object { 
                   Copy-Item $_ $newDest 
                   Remove-Item $_
                }
            }

}

# Copy the new FileGroup Structure to New Folder
Get-ChildItem $stage\*\* -dir |
    ForEach-Object {
       $dest = $_.FullName
       Get-ChildItem $dest -dir |
       ForEach-Object {
          $parent =   $final + (($_.Parent).Parent).Name
          Copy-Item $_.FullName $parent -recurse -force     
       }
    }

