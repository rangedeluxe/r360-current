'*******************************************************************************
'** WAUSAU Financial Systems (WFS)
'** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
'*******************************************************************************
'*******************************************************************************
'** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
'*******************************************************************************
'* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
'* other trademarks cited herein are property of their respective owners.
'* These materials are unpublished confidential and proprietary information 
'* of WFS and contain WFS trade secrets.  These materials may not be used, 
'* copied, modified or disclosed except as expressly permitted in writing by 
'* WFS (see the WFS license agreement for details).  All copies, modifications 
'* and derivative works of these materials are property of WFS.
'*
'* Author:   Joel Caples
'* Date:     04/14/2005
'*
'* Purpose:  Locates Crypto Machine Key and grants access to the Users group.
'*
'* Modification History
'*   04/14/2005 CR 8351 JMC
'*       - Created Script
'*******************************************************************************
Option Explicit

Private Const ScriptPath = "XCACLS.vbs"
Private Const ContainerPath = "C:\Documents and Settings\All Users\Application Data\Microsoft\Crypto\RSA\MachineKeys\"
Private Const TripleDESContainerMask = "f9d507538cb24db46b738ab52e771012_"
Private Const MD5ContainerMask = "543b6ca41c87c9f27aaf1dd8c0992493_"
Private Const DebugMode = 1

Private m_strCurrentDir		'As String

Main()

'******************************************************************
' Purpose: Main file routine.
'******************************************************************
Public Sub Main()
    Dim objFile 	'As File
    Dim objFolder 	'As Folder
    Dim fso 		'As Scripting.FileSystemObject
    Dim objShell    	'As WScript.Shell

    Set objShell = WScript.CreateObject("WScript.Shell")
    m_strCurrentDir = objShell.CurrentDirectory
    Set objShell = Nothing
    
    'Get a reference to the folder containing the Crypto containers.
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set objFolder = fso.GetFolder(ContainerPath)

    For Each objFile In objFolder.Files

        'If the TripleDes container exists, delete it.
        If (StartsWith(objFile.Name, TripleDESContainerMask)) Then

            'Calling this function with Force=True will delete read-only files.
            If(DebugMode=1) Then
                Msgbox "Deleting Triple Des container object."
            End If

            On Error Resume Next
            objFile.Delete True
            If (Err.Number <> 0) Then
                Msgbox "Error occurred while deleting the Triple Des container object: " & Err.Description
            End If
            On Error GoTo 0

        'If the MD5 container exists, delete it.
        ElseIf (StartsWith(objFile.Name, MD5ContainerMask)) Then

            'Calling this function with Force=True will delete read-only files.
            If(DebugMode=1) Then
                Msgbox "Deleting MD5 container object."
            End If

            On Error Resume Next
            objFile.Delete True
            If (Err.Number <> 0) Then
                Msgbox "Error occurred while deleting the MD5 container object: " & Err.Description
            End If
            On Error GoTo 0
        End If
    Next


    '(Re)Create Triple Des container.
    CreateTripleDesContainer()

    '(Re)Create MD5 container.
    CreateMD5Container()

    For Each objFile In objFolder.Files

        'If the TripleDes container exists, grant file access.
        If (StartsWith(objFile.Name, TripleDESContainerMask)) Then
            On Error Resume Next
            GrantFileAccess objFile.Name
            If (Err.Number <> 0) Then
                Msgbox "Error occurred while granting permissions to the Triple Des container object: " & Err.Description
            End If
            On Error GoTo 0
        'If the MD5 container exists, grant file access.
        ElseIf (StartsWith(objFile.Name, MD5ContainerMask)) Then
            On Error Resume Next
            GrantFileAccess objFile.Name
            If (Err.Number <> 0) Then
                Msgbox "Error occurred while granting permissions to the MD5 container object: " & Err.Description
            End If
            On Error GoTo 0
        End If
    Next

    Msgbox "Done."
    
    Set objFolder = Nothing
    Set fso = Nothing
End Sub

'******************************************************************
' Purpose: Tests a string to see if it starts with a specified 
'          value.
'******************************************************************
Private Function StartsWith(ByVal Value, ByVal testVal) 'As Boolean
    If (Len(Value) >= Len(testVal)) Then
        StartsWith = (Left(Value, Len(testVal)) = testVal)
    Else
        StartsWith = False
    End If
End Function

'******************************************************************
' Purpose: Calls the Microsoft XCACLS.vbs script to grant 
'          permission to the specified file.
'******************************************************************
Private Sub GrantFileAccess(ByVal FileName)
    Dim objShell
    Dim strCommand
    Dim fso 			'As Scripting.FileSystemObject
    dim strScriptPath	'As String

    'Build absolute path to the XCACLS.vbs script.
    Set fso = CreateObject("Scripting.FileSystemObject")
    strScriptPath = fso.BuildPath(m_strCurrentDir, ScriptPath)
    Set fso = Nothing

    'Build the script and inform the user.
    strCommand = "CScript.exe " & strScriptPath & " """ & ContainerPath & """" & FileName & " /g Users:f /e"
    MsgBox "Executing Command:" & vbcrlf & vbcrlf & strCommand
    'strCommand = InputBox("Command to execute:", "Execute Command", strCommand)

    'Instantiate the shell and execute the script.
    Set objShell = WScript.CreateObject("WScript.Shell")
    objShell.Run strCommand
    Set objShell = Nothing
End Sub

'******************************************************************
' Purpose: Uses the CBO Crypto object to encrypt a value, ensuring
'          that the container exists.  By doing this, we can run
'          this script without having to run any part of the
'          application first.
'******************************************************************
Public Sub CreateTripleDESContainer()

    Dim objCrypto           'As CBOCrypto.Crypto3DES
    Dim strPlainText        'As String
    Dim strEncryptedText    'As String

    If(DebugMode=1) Then
        Msgbox "Rebuilding Triple Des container object."
    End If
    
    Set objCrypto = CreateObject("CBOCrypto.Crypto3DES")
    
    strPlainText = "Plain Text String"

    On Error Resume Next
    strEncryptedText = objCrypto.Encrypt(CStr(strPlainText))
    If(Err.Number <> 0) Then
        Msgbox "An error occurred accessing the Triple Des Crypto object: " & Err.Description
    End If
    On Error GoTo 0
    
    Set objCrypto = Nothing

End Sub

'******************************************************************
' Purpose: Uses the CBO Crypto object to encrypt a value, ensuring
'          that the container exists.  By doing this, we can run
'          this script without having to run any part of the
'          application first.
'******************************************************************
Public Sub CreateMD5Container()

    Dim objCrypto           'As CBOCrypto.CryptoMD5
    Dim strPlainText        'As String
    Dim strEncryptedText    'As String

    If(DebugMode=1) Then
        Msgbox "Rebuilding MD5 container object."
    End If

    Set objCrypto = CreateObject("CBOCrypto.CryptoMD5")
    
    strPlainText = "123456"

    On Error Resume Next
    strEncryptedText = objCrypto.GetPasswordHash(CStr(strPlainText))
    If(Err.Number <> 0) Then
        Msgbox "An error occurred accessing the MD5 Crypto object: " & Err.Description
    End If
    On Error GoTo 0

    Set objCrypto = Nothing

End Sub
