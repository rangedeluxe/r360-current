:: *************************************
:: * script name: generate_build.bat
:: * date: 04/09/2007
:: * version: 0.1
:: *************************************

SET BUILDMODE=Release
set PROJDIR=..\..\


:: *************************************
:: * Build ipoLib.dll
:: *************************************
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" /rebuild %BUILDMODE% "%PROJDIR%Codebase\Components\ipoLib\ipoLib.csproj"


:: *************************************
:: * Build ipoCrypto.dll
:: *************************************
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" /rebuild %BUILDMODE% "%PROJDIR%Codebase\Components\ipoCrypto\ipoCrypto.csproj"


:: *************************************
:: * Build ipoLog.dll
:: *************************************
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" /rebuild %BUILDMODE% "%PROJDIR%Codebase\Components\ipoLog\ipoLog.csproj"


:: *************************************
:: * Build ipoDB.dll
:: *************************************
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" /rebuild %BUILDMODE% "%PROJDIR%Codebase\Components\ipoDB\ipoDB.csproj"


:: *************************************
:: * Build ipoCommon.dll
:: *************************************
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" /rebuild %BUILDMODE% "%PROJDIR%Codebase\BusinessCommon\ipoCommon\ipoCommon.csproj"


:: *************************************
:: * Build ipoDAL.dll
:: *************************************
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" /rebuild %BUILDMODE% "%PROJDIR%Codebase\BusinessCommon\ipoDAL\ipoDAL.csproj"


:: *************************************
:: * Build CDSConsole.exe
:: *************************************
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv.exe" /rebuild %BUILDMODE% "%PROJDIR%Codebase\Consolidator\CDSConsole\CDSConsole.csproj"


call merge_assemblies.bat %BUILDMODE% %PROJDIR%

call copy_binaries.bat .\build_output\ %BUILDMODE%
