﻿<#

.SYNOPSIS

Retrieves the path to the build folder.

.DESCRIPTION

Get-MKSBuildPath constructs the path to a build folder based on the 
ProjectName and BuildNumber paramters.

.PARAMETER ProjectName

The MKS Project name. Required.

.PARAMETER BuildNumber

The MKS Build Number. Formated as: Bld_xx.xx.xx.xxx where the first xx is the year 
of the build. Required.

.EXAMPLE 

Get-BuildPath "Long Term Archive" Bld_10.5.2.206


.NOTES

Verison 1.0.

#>

function Get-MKSBuildPath
{
	param
	(
		[String] $ProjectName,
		[String] $BuildNumber
	)
	[Void] [int] $local:OLTA | Out-Null;
	[string] $local:SplitBuildNumber | Out-Null;
	[string] $local:YearFolder | Out-Null;
	[string] $local:FinalSourceFolder | Out-Null;

	$local:OLTA = 0;
	if( $ProjectName.Contains('Long Term Archive') -or $ProjectName.Contains('ImageRPS Consolidator (ICON)') )
	{
		$local:OLTA = 1;
	}

	#get the build year from the build number 
	#i.e. Bld_xx.xx.xx.xxx where the first xx is the year of the build
	$local:SplitBuildNumber = $BuildNumber.Split("_.");
	if( $local:SplitBuildNumber[1].Length -ge 2 )
	{
		$local:YearFolder = $local:SplitBuildNumber[1];
	}
	else 
	{
		$local:YearFolder = "0" + $local:SplitBuildNumber[1];
	}

	#build folder information
	if( $local:OLTA -eq 0 )
	{
		if( $local:ProjectName.Contains("/Customer Custom Code") )
		{
			$local:FinalSourceFolder = "\\omafs01\commondev\QA\General_Build_Location\20" + $local:YearFolder + "\" + $BuildNumber + "\CustomCode\database" ;
		}
		else
		{
			$local:FinalSourceFolder = "\\omafs01\commondev\QA\General_Build_Location\20" + $local:YearFolder + "\" + $BuildNumber + "\integraPAY\database" ;
		}
	}
	else
	{
		$local:FinalSourceFolder = "\\omafs01\commondev\QA\General_Build_Location\20" + $local:YearFolder + "\" + $BuildNumber + "\OLTA\database" ;
	}	
	return $local:FinalSourceFolder;
}

<#

.SYNOPSIS

Write string to log file.

.DESCRIPTION

Write-Log logs a string to a file using standard WFS logging practices.

.PARAMETER LogFileName

The fully qualified name of the log file. Required.

.PARAMETER LogInformation

Information to be logged. Required.

.EXAMPLE 

Write-Log LogFile.txt "CR 11111 processed successfully."


.NOTES

Verison 1.0.

If the LogFileName does not include a path, the log will be written to the current directory.
#>

function Write-Log 
{
	param
	(
		[String] $LogFileName,
		[String] $LogInformation
	)
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($LogFileName) -eq $null )
		{
			$LogFileName = Get-Location + "\" + $LogFileName;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $LogFileName -Encoding unicode -Append;
		}
	}
	end
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $LogInformation | Out-File -FilePath $LogFileName -Encoding unicode -Append;
	}
}

<#

.SYNOPSIS

Parse a database configuration XML file.

.DESCRIPTION

Get-DBConfigXML parses a WFS database configuration XML file used by WFS PowerShell scripts.

.PARAMETER ConfigFile

The fully qualified name of the XML configuration file. Required.

.PARAMETER $Node

Node to use the XML file. The default node to process is Default.

.EXAMPLE 

Get-DBConfigXML Database.XML LocalOLTA

<Config>
	<LocalOLTA>
		<DBServer>local</local>
		<DBName>OLTATestDB</DBName>
	</LocalOLTA>
</Config>
.NOTES

Verison 1.0.

#>

function Get-DBConfigXML
{
	param 
	(
		[string] $ConfigFile = "",
		[string] $Node = "Default"
	)
	
	if( $ConfigFile.Length -eq 0 )
	{
		Write-Host "ConfigFile name required.";
		return $null;
	}
	
	#Make sure we can access the XML file
	if ((Test-Path $ConfigFile) -eq $false )
	{
		Write-Host "Could not access config file "$ConfigFile".";
		return $null;
	}

	$Config = New-Object PSObject;

	$xml = New-Object "System.Xml.XmlDocument";
	$xml.load($ConfigFile);
	
	if( $xml.Config.$Node.DBServer -ne $null )
	{
		$Config | add-member -type NoteProperty -Name DBServer -Value $xml.Config.$Node.DBServer;
	}

	if( $xml.Config.$Node.DBName -ne $null )
	{
		$Config | add-member -type NoteProperty -Name DBName -Value $xml.Config.$Node.DBName;
	}

	if( $xml.Config.$Node.CommandFile -ne $null )
	{
		$Config | add-member -type NoteProperty -Name CommandFile -Value $xml.Config.$Node.CommandFile;
	}

	if( $xml.Config.$Node.Partitioned -ne $null )
	{
		$Config | add-member -type NoteProperty -Name Partition -Value $xml.Config.$Node.Partitioned;
	}

	if( $xml.Config.$Node.SQLServerVersion -ne $null )
	{
		$Config | add-member -type NoteProperty -Name SQLServerVersion -Value $xml.Config.$Node.SQLServerVersion;
	}
	return $Config;
}

<#

.SYNOPSIS

Load the TFS .NET functions.

.DESCRIPTION

Get-TFS loads the TFS .NET functions and creates short names for each function.

.PARAMETER ServerName

The fully qualified name of the TFS server. Required.

.EXAMPLE 

Get-TFS http://mosittfs02:8080/tfs/wfs

.NOTES

Verison 1.0.

Short Names:
VCS - Version Control Client
WIT - Work Item Tracking Client
BS - Build Command (Build Store)

#>
function Get-TFS 
(
    [string] $ServerName = $(Throw 'serverName is required')
)
{
    # load the required dll
    [void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.Client");

    $propertiesToAdd = (
        ('VCS', 'Microsoft.TeamFoundation.VersionControl.Client', 'Microsoft.TeamFoundation.VersionControl.Client.VersionControlServer'),
        ('WIT', 'Microsoft.TeamFoundation.WorkItemTracking.Client', 'Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItemStore'),
        ('BS', 'Microsoft.TeamFoundation.Build.Common', 'Microsoft.TeamFoundation.Build.Proxy.BuildStore'),
        ('CSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.ICommonStructureService'),
        ('GSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.IGroupSecurityService')
    );

    # fetch the TFS instance, but add some useful properties to make life easier
    # Make sure to "promote" it to a psobject now to make later modification easier
    [psobject] $tfs = [Microsoft.TeamFoundation.Client.TeamFoundationServerFactory]::GetServer($ServerName);
    foreach ($entry in $propertiesToAdd) 
	{
        $scriptBlock = '
            [System.Reflection.Assembly]::LoadWithPartialName("{0}") > $null
            $this.GetService([{1}])
        ' -f $entry[1],$entry[2];
        $tfs | add-member scriptproperty $entry[0] $ExecutionContext.InvokeCommand.NewScriptBlock($scriptBlock);
    }
    return $tfs;
}

Export-ModuleMember -Function Get-MKSBuildPath
#export-modulemember -function Write-Log
Export-ModuleMember -Function Get-DBConfigXML
Export-ModuleMember -Function Get-TFS 


