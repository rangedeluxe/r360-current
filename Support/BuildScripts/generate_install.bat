::****************************************************************************
::****************************************************************************
::* Script: generate_install.bat
::* Author: Joel Caples
::* Date: 05/13/2009
::* 
::* Purpose: 
::*        This script will use an existing OLTA build, or call the 
::*        generate_build.bat to generate a new build to generate a release
::*        folder that will separate logical OLTA components into Web, App,
::*        and File server tiers.  The full build, and the logical server
::*        installation folders will be copied to .\olta_install\
::*
::* Usage: 
::*        generate_install - will call the generate_build.bat script to
::*          create a new build.
::*
::*        generate_install <Path_To_Build> - will use an existing build
::****************************************************************************
::****************************************************************************
SET OLTA_INSTALL_DIR=.\olta_install
SET OLTA_RELEASE_DIR=%OLTA_INSTALL_DIR%\olta_full_release

mkdir %OLTA_INSTALL_DIR%\

SET RELEASE_LOCATION=%1
if "%RELEASE_LOCATION%" == "" goto generate_build

xcopy %RELEASE_LOCATION%\*.* %OLTA_RELEASE_DIR%\ /s /e
goto generate_release

:generate_build
CALL generate_build %OLTA_RELEASE_DIR%

:generate_release
::****************************************************************************
::WEB SERVER
::****************************************************************************
ECHO .config > generate_install_web_exclusions.txt
ECHO .ini >> generate_install_web_exclusions.txt
ECHO ipoDAL.dll >> generate_install_web_exclusions.txt
ECHO ipoDB.dll >> generate_install_web_exclusions.txt
ECHO ipoCrypto.dll >> generate_install_web_exclusions.txt
ECHO ipoPICS.dll >> generate_install_web_exclusions.txt
ECHO OLFImageAPI.dll >> generate_install_web_exclusions.txt
ECHO OLFImageSvc.exe >> generate_install_web_exclusions.txt
xcopy %OLTA_RELEASE_DIR%\*.* %OLTA_INSTALL_DIR%\web_server\ /s /e /EXCLUDE:generate_install_web_exclusions.txt
DEL generate_install_web_exclusions.txt

RMDIR %OLTA_INSTALL_DIR%\web_server\Img /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\OnlineArchive /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\wwwroot\CSRResearch /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\wwwroot\OLAdmin /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\wwwroot\OLDecisioningServices /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\wwwroot\OLFICONServices /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\wwwroot\OLFServices /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\wwwroot\OLLogonServices /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\wwwroot\OLServices /s /q
DEL %OLTA_INSTALL_DIR%\web_server\support\InstallScripts\*.* /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\support /s /q
DEL %OLTA_INSTALL_DIR%\web_server\bin2\CheckDTD.exe
DEL %OLTA_INSTALL_DIR%\web_server\bin2\Hyland.Types.dll
DEL %OLTA_INSTALL_DIR%\web_server\bin2\ICONSvc.exe
DEL %OLTA_INSTALL_DIR%\web_server\bin2\ipoEncryptIni.exe
DEL %OLTA_INSTALL_DIR%\web_server\bin2\ipoImages.dll
DEL %OLTA_INSTALL_DIR%\web_server\bin2\ipoCommon.dll
DEL %OLTA_INSTALL_DIR%\web_server\bin2\OLFImageImportSvc.exe
DEL %OLTA_INSTALL_DIR%\web_server\bin2\PartitionManager.exe
RMDIR %OLTA_INSTALL_DIR%\web_server\data\Logfiles\DecisioningServicesAPIXml /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\data\Logfiles\LogonServicesAPIXml /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\data\Logfiles\OLServicesAPIXml /s /q
DEL %OLTA_INSTALL_DIR%\web_server\data\ServiceStyles\Online\Templates\*.* /s /q
DEL %OLTA_INSTALL_DIR%\web_server\data\ServiceStyles\Online\Images\*.* /s /q
RMDIR %OLTA_INSTALL_DIR%\web_server\data\ServiceStyles /s /q
DEL %OLTA_INSTALL_DIR%\web_server\bin2\ICONAPI.dll
DEL %OLTA_INSTALL_DIR%\web_server\bin2\ImportImages.exe
DEL %OLTA_INSTALL_DIR%\web_server\bin2\OLFConsole.exe
DEL %OLTA_INSTALL_DIR%\web_server\bin2\OTISConsole.exe
RMDIR %OLTA_INSTALL_DIR%\web_server\database /s /q
MKDIR %OLTA_INSTALL_DIR%\web_server\wwwroot\CENDS\


copy %OLTA_RELEASE_DIR%\bin2\IPOnline.ini %OLTA_INSTALL_DIR%\web_server\bin2\IPOnline.ini.oem
copy %OLTA_RELEASE_DIR%\wwwroot\IPLogon\web.config %OLTA_INSTALL_DIR%\web_server\wwwroot\IPLogon\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\IPOnline\web.config %OLTA_INSTALL_DIR%\web_server\wwwroot\IPOnline\web.config.oem

move %OLTA_INSTALL_DIR%\web_server\bin\CBOCrypto.dll %OLTA_INSTALL_DIR%\web_server\bin\CBOCrypto.dl_
move %OLTA_INSTALL_DIR%\web_server\wwwroot\IPOnline\Styles\Online %OLTA_INSTALL_DIR%\web_server\wwwroot\IPOnline\Styles\Online.oem
move %OLTA_INSTALL_DIR%\web_server\wwwroot\IPLogon\Styles\Online %OLTA_INSTALL_DIR%\web_server\wwwroot\IPLogon\Styles\Online.oem

copy ..\bin\Microsoft\sc.exe %OLTA_INSTALL_DIR%\web_server\bin\sc.exe
::****************************************************************************

::****************************************************************************
::APP SERVER
::****************************************************************************
ECHO .config > generate_install_app_exclusions.txt
ECHO .ini >> generate_install_app_exclusions.txt
ECHO ipoPICS.dll >> generate_install_app_exclusions.txt
ECHO OLFImageAPI.dll >> generate_install_app_exclusions.txt
ECHO OLFImageSvc.exe >> generate_install_app_exclusions.txt
xcopy %OLTA_RELEASE_DIR%\*.* %OLTA_INSTALL_DIR%\app_server\ /s /e /EXCLUDE:generate_install_app_exclusions.txt
DEL generate_install_app_exclusions.txt

RMDIR %OLTA_INSTALL_DIR%\app_server\Img /s /q
RMDIR %OLTA_INSTALL_DIR%\app_server\OnlineArchive /s /q
RMDIR %OLTA_INSTALL_DIR%\app_server\wwwroot\OLFICONServices /s /q
DEL %OLTA_INSTALL_DIR%\app_server\data\ServiceStyles\Online\Templates\*.xsl
DEL %OLTA_INSTALL_DIR%\app_server\data\ServiceStyles\Online\Images\*.xsl
RMDIR %OLTA_INSTALL_DIR%\app_server\data\ServiceStyles /s /q
DEL %OLTA_INSTALL_DIR%\app_server\support\InstallScripts\*.* /s /q
RMDIR %OLTA_INSTALL_DIR%\app_server\support /s /q
DEL %OLTA_INSTALL_DIR%\app_server\bin2\Hyland.Types.dll
DEL %OLTA_INSTALL_DIR%\app_server\bin2\OLFImageImportSvc.exe
DEL %OLTA_INSTALL_DIR%\app_server\bin2\CheckDTD.exe
DEL %OLTA_INSTALL_DIR%\app_server\bin2\ICONSvc.exe
DEL %OLTA_INSTALL_DIR%\app_server\bin2\ICONAPI.dll
DEL %OLTA_INSTALL_DIR%\app_server\bin2\ImportImages.exe
DEL %OLTA_INSTALL_DIR%\app_server\bin2\OLFConsole.exe
DEL %OLTA_INSTALL_DIR%\app_server\bin2\OLFServicesClient.dll
DEL %OLTA_INSTALL_DIR%\app_server\bin2\OTISConsole.exe
RMDIR %OLTA_INSTALL_DIR%\app_server\database /s /q
MKDIR %OLTA_INSTALL_DIR%\app_server\wwwroot\CENDS\

copy %OLTA_RELEASE_DIR%\bin2\IPOnline.ini %OLTA_INSTALL_DIR%\app_server\bin2\IPOnline.ini.oem
copy %OLTA_RELEASE_DIR%\wwwroot\CSRResearch\web.config %OLTA_INSTALL_DIR%\app_server\wwwroot\CSRResearch\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\IPLogon\web.config %OLTA_INSTALL_DIR%\app_server\wwwroot\IPLogon\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\IPOnline\web.config %OLTA_INSTALL_DIR%\app_server\wwwroot\IPOnline\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLAdmin\web.config %OLTA_INSTALL_DIR%\app_server\wwwroot\OLAdmin\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLDecisioningServices\web.config %OLTA_INSTALL_DIR%\app_server\wwwroot\OLDecisioningServices\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLFServices\web.config %OLTA_INSTALL_DIR%\app_server\wwwroot\OLFServices\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLLogonServices\web.config %OLTA_INSTALL_DIR%\app_server\wwwroot\OLLogonServices\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLServices\web.config %OLTA_INSTALL_DIR%\app_server\wwwroot\OLServices\web.config.oem

move %OLTA_INSTALL_DIR%\app_server\bin\CBOCrypto.dll %OLTA_INSTALL_DIR%\app_server\bin\CBOCrypto.dl_
move %OLTA_INSTALL_DIR%\app_server\wwwroot\IPOnline\Styles\Online %OLTA_INSTALL_DIR%\app_server\wwwroot\IPOnline\Styles\Online.oem
move %OLTA_INSTALL_DIR%\app_server\wwwroot\IPLogon\Styles\Online %OLTA_INSTALL_DIR%\app_server\wwwroot\IPLogon\Styles\Online.oem

copy ..\bin\Microsoft\sc.exe %OLTA_INSTALL_DIR%\app_server\bin\sc.exe
::****************************************************************************

::****************************************************************************
::FILE SERVER
::****************************************************************************
ECHO .config > generate_install_file_exclusions.txt
ECHO .ini >> generate_install_file_exclusions.txt
xcopy %OLTA_RELEASE_DIR%\*.* %OLTA_INSTALL_DIR%\file_server\ /s /e /EXCLUDE:generate_install_file_exclusions.txt
DEL generate_install_file_exclusions.txt

RMDIR %OLTA_INSTALL_DIR%\file_server\wwwroot\CSRResearch /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\wwwroot\IPOnline /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\wwwroot\IPLogon /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\wwwroot\OLAdmin /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\wwwroot\OLDecisioningServices /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\wwwroot\OLLogonServices /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\wwwroot\OLServices /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\wwwroot\CENDS /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\data\Logfiles\DecisioningServicesAPIXml /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\data\Logfiles\LogonServicesAPIXml /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\data\Logfiles\OLServicesAPIXml /s /q
DEL %OLTA_INSTALL_DIR%\file_server\support\InstallScripts\*.* /s /q
RMDIR %OLTA_INSTALL_DIR%\file_server\support /s /q
DEL %OLTA_INSTALL_DIR%\file_server\bin2\CheckDTD.exe
DEL %OLTA_INSTALL_DIR%\file_server\bin2\OTISConsole.exe
RMDIR %OLTA_INSTALL_DIR%\file_server\database /s /q
MKDIR %OLTA_INSTALL_DIR%\file_server\OnlineArchive\

copy %OLTA_RELEASE_DIR%\bin2\IPOnline.ini %OLTA_INSTALL_DIR%\file_server\bin2\IPOnline.ini.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLFICONServices\web.config %OLTA_INSTALL_DIR%\file_server\wwwroot\OLFICONServices\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLFServices\web.config %OLTA_INSTALL_DIR%\file_server\wwwroot\OLFServices\web.config.oem

move %OLTA_INSTALL_DIR%\file_server\bin\CBOCrypto.dll %OLTA_INSTALL_DIR%\file_server\bin\CBOCrypto.dl_
move %OLTA_INSTALL_DIR%\file_server\data\ServiceStyles\Online %OLTA_INSTALL_DIR%\file_server\data\ServiceStyles\Online.oem

copy ..\bin\Microsoft\sc.exe %OLTA_INSTALL_DIR%\file_server\bin\sc.exe
::****************************************************************************
