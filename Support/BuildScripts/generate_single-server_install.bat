::****************************************************************************
::****************************************************************************
::* Script: generate_install.bat
::* Author: Joel Caples
::* Date: 05/13/2009
::* 
::* Purpose: 
::*        This script will use an existing OLTA build, or call the 
::*        generate_build.bat to generate a new build to generate a release
::*        folder that will separate logical OLTA components into Web, App,
::*        and File server tiers.  The full build, and the logical server
::*        installation folders will be copied to .\olta_install\
::*
::* Usage: 
::*        generate_install - will call the generate_build.bat script to
::*          create a new build.
::*
::*        generate_install <Path_To_Build> - will use an existing build
::****************************************************************************
::****************************************************************************
SET OLTA_INSTALL_DIR=.\olta_single-server_install
SET OLTA_RELEASE_DIR=%OLTA_INSTALL_DIR%\olta_40703

mkdir %OLTA_INSTALL_DIR%\

SET RELEASE_LOCATION=%1
if "%RELEASE_LOCATION%" == "" goto generate_build

xcopy %RELEASE_LOCATION%\*.* %OLTA_RELEASE_DIR%\ /s /e
goto generate_release

:generate_build
CALL generate_build %OLTA_RELEASE_DIR%

:generate_release
::****************************************************************************
::SERVER
::****************************************************************************
ECHO .config > generate_install_single-server_exclusions.txt
ECHO .ini >> generate_install_single-server_exclusions.txt
xcopy %OLTA_RELEASE_DIR%\*.* %OLTA_INSTALL_DIR%\single_server\ /s /e /EXCLUDE:generate_install_single-server_exclusions.txt
DEL generate_install_single-server_exclusions.txt

copy %OLTA_RELEASE_DIR%\bin2\IPOnline.ini %OLTA_INSTALL_DIR%\single_server\bin2\IPOnline.ini.oem
copy %OLTA_RELEASE_DIR%\wwwroot\CSRResearch\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\CSRResearch\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\IPLogon\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\IPLogon\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\IPOnline\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\IPOnline\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLAdmin\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\OLAdmin\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLDecisioningServices\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\OLDecisioningServices\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLFICONServices\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\OLFICONServices\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLFServices\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\OLFServices\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLLogonServices\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\OLLogonServices\web.config.oem
copy %OLTA_RELEASE_DIR%\wwwroot\OLServices\web.config %OLTA_INSTALL_DIR%\single_server\wwwroot\OLServices\web.config.oem

::copy ..\bin\Microsoft\sc.exe %OLTA_RELEASE_DIR%\bin\sc.exe
::****************************************************************************
