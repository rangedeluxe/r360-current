set BUILDMODE=%1
set PROJDIR=%2

:: *************************************
:: * Use ILMerge combine assemblies and write the new binaries to the output bin folder.
:: *************************************
mkdir %PROJDIR%Codebase\Consolidator\CDSConsole\bin\%BUILDMODE%\Merged\
start /wait C:\tools\SR_2005\ILMerge.exe /ndebug ^
                                 /internalize ^
                                 %PROJDIR%Codebase\Consolidator\CDSConsole\bin\%BUILDMODE%\CDSConsole.exe ^
                                 %PROJDIR%Codebase\Components\ipoLib\bin\%BUILDMODE%\ipoLib.dll ^
                                 %PROJDIR%Codebase\Components\ipoCrypto\bin\%BUILDMODE%\ipoCrypto.dll ^
                                 %PROJDIR%Codebase\Components\ipoLog\bin\%BUILDMODE%\ipoLog.dll ^
                                 %PROJDIR%Codebase\Components\ipoDB\bin\%BUILDMODE%\ipoDB.dll ^
                                 /out:%PROJDIR%Codebase\Consolidator\CDSConsole\bin\%BUILDMODE%\Merged\CDSConsole.exe ^
                                 /log:ILMergeResults.txt ^
                                 /delaysign ^
                                 /keyfile:%PROJDIR%keys\ipoPublicKey.snk ^
                                 /allowDup

echo Running SN to sign the combined assembly.
if "%BUILDMODE%"=="Debug" goto SnDebug

goto SnRelease

:SnDebug
S:\Tools\sn.exe -Vr %PROJDIR%Codebase\Consolidator\CDSConsole\bin\%BUILDMODE%\Merged\CDSConsole.exe
goto SnDone

:SnRelease
S:\Tools\sn.exe -R %PROJDIR%Codebase\Consolidator\CDSConsole\bin\%BUILDMODE%\Merged\CDSConsole.exe  %PROJDIR%keys\ipoKeyPair.snk
goto SnDone

:SnDone
