:: *************************************
:: * script name: copy_binaries.bat
:: * date: 04/09/2007
:: * version: 0.1
:: *************************************


:: Get parameters: $(TargetDirectory) $(ConfigurationName)
set OUTPUT_DIR=%1
set BUILDMODE=%2
@echo Parameters: %OUTPUT_DIR%

if "%OUTPUT_DIR%" == "" goto Usage
if "%BUILDMODE%" == "" goto Usage


del %OUTPUT_DIR%*.* /s /q
rmdir %OUTPUT_DIR% /s /q


mkdir %OUTPUT_DIR%IPOnline\
mkdir %OUTPUT_DIR%IPOnline\bin\
mkdir %OUTPUT_DIR%IPOnline\bin2\
mkdir %OUTPUT_DIR%IPOnline\data\


:: *************************************
:: * Copy binaries to the %OUTPUT_DIR%IPDelivery\bin\ directory
:: *************************************
copy ..\..\Codebase\Consolidator\CDSConsole\bin\%BUILDMODE%\Merged\CDSConsole.exe %OUTPUT_DIR%IPOnline\bin2\ /y

copy ..\..\Ini\CDSConsole.ini %OUTPUT_DIR%IPOnline\bin2\ /y
copy ..\..\Codebase\Consolidator\CDSConsole\App.Config %OUTPUT_DIR%IPOnline\bin2\ /y

:: *************************************
:: * Copy binaries to the %OUTPUT_DIR%IPDelivery\bin\ directory
:: *************************************
copy ..\..\Support\bin\WFS\CBOCrypto.dll         %OUTPUT_DIR%IPOnline\Bin\ /y


goto Done

:Usage

@echo "Usage: copy_binaries <output_dir> <Debug|Release>"


:Done
