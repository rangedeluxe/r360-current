@IF "%1" == "" GOTO ONE
@GOTO TWO

:ONE
@SET BUILD_NAME=OLTA_Rel_4.07.03.XXX
@GOTO GENERATE_BUILD

:TWO
@SET BUILD_NAME=%1

:GENERATE_BUILD
@ECHO %BUILD_NAME%


::***********************************
::* QA Web Server
::***********************************
net use \\172.25.1.57 /user:administrator password
..\bin\Microsoft\NetSvc "World Wide Web Publishing Service" \\172.25.1.57 /stop
..\bin\Microsoft\NetSvc "WFS IPOnline Cleanup Service" \\172.25.1.57 /stop
xcopy .\olta_install\web_server\*.* \\172.25.1.57\d$\IPOnline\ /s /e /y

xcopy .\olta_install\web_server\wwwroot\IPOnline\*.* \\172.25.1.57\d$\IPOnline\wwwroot\IPOnline_pl\ /s /e /y
xcopy .\olta_install\web_server\wwwroot\IPLogon\*.* \\172.25.1.57\d$\IPOnline\wwwroot\IPLogon_pl\ /s /e /y

rmdir \\172.25.1.57\d$\%BUILD_NAME% /s /q
xcopy .\olta_install\olta_40703\*.* \\172.25.1.57\d$\%BUILD_NAME%\ /s /e /y
..\bin\Microsoft\NetSvc "World Wide Web Publishing Service" \\172.25.1.57 /start
..\bin\Microsoft\NetSvc "WFS IPOnline Cleanup Service" \\172.25.1.57 /start

..\bin\Microsoft\psexec.exe \\172.25.1.57 C:\Windows\System32\RegSvr32.exe /s E:\IPOnline\bin\CBOCrypto.dll

net use \\172.25.1.57 /delete

::***********************************
::* QA App Server
::***********************************
net use \\172.25.1.69 /user:administrator password
..\bin\Microsoft\NetSvc "World Wide Web Publishing Service" \\172.25.1.69 /stop
..\bin\Microsoft\NetSvc "WFS IPOnline Cleanup Service" \\172.25.1.69 /stop
xcopy .\olta_install\app_server\*.* \\172.25.1.69\d$\IPOnline\ /s /e /y
rmdir \\172.25.1.69\d$\%BUILD_NAME% /s /q
xcopy .\olta_install\olta_40703\*.* \\172.25.1.69\d$\%BUILD_NAME%\ /s /e /y
..\bin\Microsoft\NetSvc "World Wide Web Publishing Service" \\172.25.1.69 /start
..\bin\Microsoft\NetSvc "WFS IPOnline Cleanup Service" \\172.25.1.69 /start

..\bin\Microsoft\psexec.exe \\172.25.1.69 C:\Windows\System32\RegSvr32.exe /s E:\IPOnline\bin\CBOCrypto.dll

net use \\172.25.1.69 /delete

::***********************************
::* QA File Server
::***********************************
..\bin\Microsoft\NetSvc "World Wide Web Publishing Service" \\172.25.1.70 /stop
..\bin\Microsoft\NetSvc "WFS IPOnline Cleanup Service" \\172.25.1.70 /stop
..\bin\Microsoft\NetSvc "WFS IPOnline Image Import Service" \\172.25.1.70 /stop
..\bin\Microsoft\NetSvc "WFS IPOnline Image Service" \\172.25.1.70 /stop
..\bin\Microsoft\NetSvc "WFS ImageRPS Consolidator Service" \\172.25.1.70 /stop
xcopy .\olta_install\file_server\*.* \\172.25.1.70\d$\IPOnline\ /s /e /y
rmdir \\172.25.1.70\d$\%BUILD_NAME% /s /q
xcopy .\olta_install\olta_40703\*.* \\172.25.1.70\d$\%BUILD_NAME%\ /s /e /y
..\bin\Microsoft\NetSvc "World Wide Web Publishing Service" \\172.25.1.70 /start
..\bin\Microsoft\NetSvc "WFS IPOnline Image Service" \\172.25.1.70 /start
::..\bin\Microsoft\NetSvc "WFS IPOnline Image Import Service" \\172.25.1.70 /start
..\bin\Microsoft\NetSvc "WFS IPOnline Cleanup Service" \\172.25.1.70 /start
..\bin\Microsoft\NetSvc "WFS ImageRPS Consolidator Service" \\172.25.1.70 /start

..\bin\Microsoft\psexec.exe \\172.25.1.70 C:\Windows\System32\RegSvr32.exe /s E:\IPOnline\bin\CBOCrypto.dll
