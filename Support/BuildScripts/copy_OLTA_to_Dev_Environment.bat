@IF "%1" == "" GOTO ONE
@GOTO TWO

:ONE
@SET BUILD_NAME=OLTA_Rel_4.07.03.XXX
@GOTO GENERATE_BUILD

:TWO
@SET BUILD_NAME=%1

:GENERATE_BUILD
@ECHO %BUILD_NAME%


::***********************************
::* Development Server
::***********************************
net use \\172.25.1.2
..\bin\Microsoft\NetSvc "World Wide Web Publishing Service" \\172.25.1.2 /stop
..\bin\Microsoft\NetSvc "WFS IPOnline Image Service" \\172.25.1.2 /stop
..\bin\Microsoft\NetSvc "WFS IPOnline Cleanup Service" \\172.25.1.2 /stop
..\bin\Microsoft\NetSvc "WFS ImageRPS Consolidator Service" \\172.25.1.2 /stop
xcopy .\olta_single-server_install\single_server\*.* \\172.25.1.2\e$\IPOnline\ /s /e /y
rmdir \\172.25.1.2\e$\%BUILD_NAME% /s /q
xcopy .\olta_single-server_install\olta_40703\*.* \\172.25.1.2\e$\%BUILD_NAME%\ /s /e /y
..\bin\Microsoft\NetSvc "World Wide Web Publishing Service" \\172.25.1.2 /start
..\bin\Microsoft\NetSvc "WFS IPOnline Image Service" \\172.25.1.2 /start
..\bin\Microsoft\NetSvc "WFS IPOnline Cleanup Service" \\172.25.1.2 /start
..\bin\Microsoft\NetSvc "WFS ImageRPS Consolidator Service" \\172.25.1.2 /start

..\bin\Microsoft\psexec.exe \\172.25.1.2 C:\Windows\System32\RegSvr32.exe /s E:\IPOnline\bin\CBOCrypto.dll

net use \\172.25.1.2 /delete

