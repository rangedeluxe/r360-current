﻿param 
(
	[string] $BuildNumber = "", 
	[string] $TFSServer = "http://mosittfs02:8080/tfs/wfs"
)

$ScriptVerison = "1.0";
################################################################################
# 1/30/13 WJS	1.0	Created.
################################################################################

Write-Host $buildCode "Version" $ScriptVerison;
[string] $BuildFolder | Out-Null;
[string] $SourceCode | Out-Null;

$BuildFolder="";



function Build-VisualStudioSolution            
{            
    param            
    (            
        [parameter(Mandatory=$false)]            
        [ValidateNotNullOrEmpty()]             
        [String] $SourceCodePath = $SourceCode ,            
            
        [parameter(Mandatory=$false)]            
        [ValidateNotNullOrEmpty()]             
        [String] $SolutionFile =  "integraPAY_Online.sln",       
                    
        [parameter(Mandatory=$false)]            
        [ValidateNotNullOrEmpty()]             
        [String] $Configuration = "Release",            
                    
        [parameter(Mandatory=$false)]            
        [ValidateNotNullOrEmpty()]             
        [Boolean] $AutoLaunchBuildLog = $false,            
            
        [parameter(Mandatory=$false)]            
        [ValidateNotNullOrEmpty()]             
        [Switch] $MsBuildHelp,                            
                    
        [ValidateNotNullOrEmpty()]             
        [string] $BuildLogFile      
    )            
                
    process            
    {            
        # Local Variables            
        $MsBuild = $env:systemroot + "\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe";            
                 
        # Local Variables            
        $SlnFilePath = $SourceCodePath + "\\" + $SolutionFile;            
        $SlnFileParts = $SolutionFile.Split("\");            
        $SlnFileName = $SlnFileParts[$SlnFileParts.Length - 1];            
        $BuildLog = $BuildLogOutputPath + $BuildLogFile            
        $bOk = $true;            
                    
        try            
        {            
          
            # Display Progress            
            Write-Progress -Id 20275 -Activity $SlnFileName  -Status "Building..." -PercentComplete 60;            
                        
            # Prepare the Args for the actual build            
            $BuildArgs = @{            
                FilePath = $MsBuild            
                ArgumentList = $SlnFilePath, "/t:rebuild", ("/p:Configuration=" + $Configuration), "/v:minimal"   
                Wait = $true            
                #WindowStyle = "Hidden"            
            }            
        
            # Start the build            
            Start-Process @BuildArgs #| Out-String -stream -width 1024 > $DebugBuildLogFile             
                        
            # Display Progress            
            Write-Progress -Id 20275 -Activity $SlnFileName  -Status "Done building." -PercentComplete 100;            
        }            
        catch            
        {            
            $bOk = $false;            
            Write-Error ("Unexpect error occured while building " + $SlnFileParts[$SlnFileParts.Length - 1] + ": " + $_.Message);            
        }              
    }   
}
################################################################################
# Run Create Build Folder.
################################################################################
function CreateBuildFolder([ref]$local:BuildFolder,[ref] $local:BuildNumber)
{
	
	$local:BuildFolder.Value = "\\omafs01\commondev\QA\RecHub_Build_Location\";
	$YearFolder = (Get-Date -Format yyyy);
	$local:BuildFolder.Value += $YearFolder;
	switch ($buildCode)
	{
		"Build102" 
		{
			$local:BuildFolder.Value += "\\1.02";
		}
		"Build104" 
		{
			$local:BuildFolder.Value += "\\1.04";
		}
	   "Build10108"
	   {
			$local:BuildFolder.Value += "\\1.01.08";
		}
	}
	
	$local:BuildNumber.Value = "Bld_" + (Get-Date –f yy.MM.dd);
	#if( $local:BuildNumber.Value.Length -eq 0 )
	#{
	#	$local:BuildNumber.Value = "Bld_" + (Get-Date –f yy.MM.dd);
	#}
	#else
	#{
	#	if( !$local:BuildNumber.Value.StartsWith("Bld_") )
		#{
		#	$local:BuildNumber.Value = "Bld_" + $local:BuildNumber.Value;
		#}
	#}
	
	if( $local:BuildNumber.Value.Split(".").Count-1 -eq 2 )
	{
		$local:NeedDailyNumber = $true;
	}
	else
	{
		$local:NeedDailyNumber = $false;
	}
	
	$local:BuildFolder.Value += "\" + $local:BuildNumber.Value;
	$local:DailyBuildNumber = 1;
	$local:FolderCreated = $false;
	
	
	while( !$local:FolderCreated )
	{
		if( $local:NeedDailyNumber )
		{
			$TestFolder = $local:BuildFolder.Value + "." + $local:DailyBuildNumber;
		}
		else
		{
			$TestFolder = $local:BuildFolder.Value;
		}
		if((Test-Path $TestFolder) -ne $true )
		{
			New-Item $TestFolder -type directory | Out-Null;
			$local:FolderCreated = $true;
			$local:BuildFolder.Value = $TestFolder;
			$local:BuildNumber.Value += "." + $local:DailyBuildNumber;
		}
		else
		{
			if( $local:NeedDailyNumber )
			{
				$local:DailyBuildNumber++;
			}
			else
			{
				$local:FolderCreated = $true;
			}
		}
	}
}

################################################################################
# Determine if a child WI is complete
################################################################################
function IsChildComplete
{
	param
	(
		[int] $local:ChildID
	)
	$local:Completed = $false;
	$local:ChildWorkItem = $TFS.WIT.GetWorkItem($local:ChildID);
	if( $local:ChildWorkItem.State -eq "Resolved" -or $local:ChildWorkItem.State -eq "Closed" )
	{
		$local:Completed = $true;
	}
	return $local:Completed;
}

################################################################################
# Determine if all childern of the parent WI are complete
################################################################################
function IsParentComplete
{
	param
	(
		[int] $local:ParentID
	)
	
	$local:Complete = $true;
	$local:ParentWorkItem = $TFS.WIT.GetWorkItem($local:ParentID);
	foreach( $local:Link in $local:ParentWorkItem.Links)
	{
		if( $local:Link.BaseType -eq "RelatedLink")
		{
			if( $local:Link.LinkTypeEnd.Name -eq "Child" )
			{
				if( (IsChildComplete $Local:Link.RelatedWorkItemId) -eq $false )
				{
					$local:Complete = $false;
					break;
				}
			}
		}
	}
	if( $local:Complete -eq $true )
	{
		foreach( $local:Field in $local:ParentWorkItem.Fields )
		{
			if( $local:Field.ReferenceName -eq "System.AssignedTo" )
			{
				$local:Field.Value = "Steve Eckhart";
			}
		}
		$local:ParentWorkItem.Save();
	}
}

################################################################################
# Reassign the WI to QA if all childern of the parent are complete
################################################################################
function AssignToQA
{
	param
	(
		[object] $local:WorkItem
	)
	foreach( $local:Link in $local:WorkItem.Links )
	{
		if( $local:Link.BaseType -eq "RelatedLink" )
		{
			if( $local:Link.LinkTypeEnd.Name -eq "Parent" )
			{
				IsParentComplete $local:Link.RelatedWorkItemId
			}
		}
	}
}
function RecursiveCopy()
{
	param
	(
		[object] $local:sourceDir,
		[object] $local:targetDir,
		[object] $local:Filter
	)
	Get-ChildItem $local:sourceDir -filter  $local:Filter -recurse | `
	   foreach{ 
		   $local:targetFile = $local:targetDir  + $_.FullName.SubString($local:sourceDir.Length);
		   New-Item -ItemType File -Path $targetFile -Force;  
		   Copy-Item $_.FullName -destination $targetFile
	   } 

}

function Build-TFSBuild([ref] $local:BuildNumber )
{

	switch ($WorkItem.AreaPath)
	{
		"Receivables_Hub\Apps\Create Initial Admin Utility"
		{
			$areaPath ="RecHub.CreateInitialAdminUserUtility"
		}
		"Receivables_Hub\Apps\Encrypt Ini"
		{
			$areaPath ="RecHub.EncryptIni"
		}
		"Receivables_Hub\Apps\Crypto Dialog"
		{
			$areaPath ="RecHub.CryptoDialog"
		}
		"Receivables_Hub\Components\Common\ltaFileCollector"
		{
			$areaPath ="RecHub.Components.ltaFileCollector"
		}
		"Receivables_Hub\Components\Common\ltaLog"
		{
			$areaPath ="RecHub.Components.ltalog"
		}
		"Receivables_Hub\Components"
		{
			$areaPath = "RecHub.ComponentsCollection"
		}
		
		"Receivables_Hub\CSR Research"
		{
			$areaPath ="RecHub.CSRResearch"
		}
		"Receivables_Hub\File Cleanup Service"
		{
			$areaPath ="RecHub.FileCleanupSvc"
		}
		"Receivables_Hub\Data Import Toolkit\Data Import Client Service"
		{
			$areaPath = "RecHub.DataImportToolkit.DataImportClientSvc"
			
		}
		"Receivables_Hub\Data Import Toolkit\Data Import Demo Client"
		{
			$areaPath = "RecHub.DataImportToolkit.DemoClient"
			
		}
		"Receivables_Hub\Data Import Toolkit\Data Import ICON Client Base"
		{
			$areaPath ="RecHub.DataImportToolkit.ICONClientBase"
		}
		"Receivables_Hub\Data Import Toolkit\Data Import ICON XClient"
		{
			$areaPath = "RecHub.DataImportToolkit.ICONXClient"
		}
		"Receivables_Hub\Data Import Toolkit\Data Import Toolkit Services"
		{
			$areaPath ="RecHub.DataImportToolkit.Services"
		}
		"Receivables_Hub\Data Import Toolkit\Data Import WCF Lib"
		{
			$areaPath = "RecHub.DataImportToolkit.WCFLib"
		}
		"Receivables_Hub\Data Import Toolkit\Data Import XClient Base"
		{
			$areaPath = "RecHub.DataImportToolkit.XClientBase"
		}
		"Receivables_Hub\File Import Toolkit\File Import Client Lib"
		{
			$areaPath = "RecHub.FileImportToolKit.ClientLib"
		}
		"Receivables_Hub\File Import Toolkit\File Import Client Service"
		{
			$areaPath = "RecHub.FileImportToolkit.ClientSvc"
		}
		"Receivables_Hub\File Import Toolkit\File Import ICON XClient"
		{
			$areaPath = "RecHub.FileImportToolkit.ICONXClient"
		}
		"Receivables_Hub\File Import Toolkit\File Import ItemProcessing XClient"
		{
			$areaPath = "RecHub.FileImportToolkit.ItemProcessingXClient"
		}
		"Receivables_Hub\File Import Toolkit\File Import Toolkit Services"
		{
			$areaPath ="RecHub.FileImportToolkit.Services"
		}
		"Receivables_Hub\File Import Toolkit\File Import WCF Lib"
		{
			$areaPath =""
		}
		"Receivables_Hub\OLF\OLF Hyland Image Import Service"
		{
			$areaPath = "RecHub.OLF.HylandImageImportSvc"
		}
		"Receivables_Hub\ICON\ICON Services"
		{
			$areaPath ="RecHub.ICON.Services"
		}
		"Receivables_Hub\ICON\ICON Common"
		{
			$areaPath ="Receivables_Hub\ICON\ICON Services"	
		}
		"Receivables_Hub\OLF\OLF Image Services"
		{
			$areaPath = "RecHub.OLF.ImageSvc"
		}
		"Receivables_Hub\OLF\OLF Services"
		{
			$areaPath = "RecHub.OLF.Services"
		}
	   "Receivables_Hub\Online"
	   {
	   		$areaPath = "RecHub.Online"
	   }
	   "Receivables_Hub\Online Administration"
	   {
	   		$areaPath = "RecHub.Online.Administration"
	   }
	   
	   "Receivables_Hub\Online Logon"
	   {
	   		$areaPath = "RecHub.Online.Logon"
	   }
	   "Receivables_Hub\Online Decisioning API Services"
	   {
	   		$areaPath = "RecHub.Online.Decisioning.Services"
	   }
	}   
	

    # Local Variables            
    $TFSBuild = "${env:ProgramFiles(x86)}"  + "\Microsoft Visual Studio 11.0\Common7\IDE\TFSBuild.exe";            
         
	$BuildArgs = @{            
        FilePath = $TFSBuild            
        ArgumentList = "start", "/collection:http://mosittfs02:8080/tfs/wfs", ("/builddefinition:Receivables_Hub\"   +  $areaPath)
    }        
          
	Start-Process @BuildArgs -Wait -NoNewWindow
	
	$BldNumberOrig = "Bld_" + (Get-Date –f yy.MM.dd);
	$DailyBuildNumber = 20;
	$BldCreated = false;
	while( !$BldCreated )
	{
		$BldNumber = $BldNumberOrig + "." + $DailyBuildNumber;
		$TestFolder = ("c:\DropBox\" +  $BldNumber + "\" + $areaPath);
		if((Test-Path $TestFolder) -eq $true )
		{
			$BldCreated= $true;
			$BuildFolder = "\\omafs01\commondev\QA\RecHub_Build_Location\";
			$YearFolder = (Get-Date -Format yyyy);
			$BuildFolder += $YearFolder;
			$BuildFolder += "\";
			$BuildFolder += "1.05";
			$BuildFolder += "\";
			$BuildFolder += $BldNumber;
			$BuildFolder += "\";
			$BuildFolder += $areaPath;
			New-Item $BuildFolder -type directory | Out-Null;
		}
		else
		{
			$DailyBuildNumber--;
		}
	}
	

	RecursiveCopy $TestFolder  $BuildFolder
	$local:BuildNumber.Value = $BldNumber;
	    
}
function BuildDirectories()
{
	param
	(
		[object] $BuildFolder
	)
	
	



	$LTABuildFolder = $BuildFolder + "\\LTA";
	$LTABuildBinFolder = $LTABuildFolder + "\\bin";
	
	
	#build the bin2 folder
	$LTABuildBin2Folder = $LTABuildFolder + "\\bin2";
	Write Host "Building Bin2 Folder"
	New-Item  $LTABuildBin2Folder -type directory
	
	Copy-Item ($SourceCode + "\Ini\IPOnline.ini") $LTABuildBin2Folder\.
	Copy-Item ($SourceCode + "\Ini\IPOnline.ini.app") $LTABuildBin2Folder\.
	Copy-Item ($SourceCode + "\Ini\IPOnline.ini.file") $LTABuildBin2Folder\.
	Copy-Item ($SourceCode + "\Ini\IPOnline.ini.web") $LTABuildBin2Folder\.
	if($buildcode -ne "Build105INI" ) 
	{
		
		
		
		Copy-Item ($SourceCode + "\Codebase\Components\ipoCrypto\bin\Release\ipoCrypto.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\Components\ipoDB\bin\Release\ipoDB.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\Components\ipoLib\bin\Release\ipoLib.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\Components\ipoLog\bin\Release\ipoLog.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\BusinessCommon\ipoDAL\bin\Release\ipoDAL.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\BusinessCommon\ipoCommon\bin\Release\ipoCommon.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\BusinessCommon\ipoImages\bin\Release\ipoImages.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\BusinessCommon\ipoPICS\bin\Release\ipoPICS.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\Apps\EncryptIni\bin\Release\ipoEncryptIni.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\FileCleanup\FileCleanupAPI\bin\Release\FileCleanupAPI.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\FileCleanup\FileCleanupSvc\bin\Release\FileCleanupSvc.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLDecisioning\CheckDTD\bin\Release\CheckDTD.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFConsole\bin\Release\OLFConsole.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFConsole\OLFConsole.exe.config") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFServicesClient\bin\Release\OLFServicesClient.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFImageAPI\bin\Release\OLFImageAPI.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFImageSvc\bin\Release\OLFImageSvc.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFImageImport\bin\Release\OLFImageImportSvc.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\ImportImages\bin\Release\ImportImages.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\PartitionManager\bin\Release\PartitionManager.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\PartitionManager\bin\Release\Interop.CBOCrypto.dll") $LTABuildBin2Folder\.
	
		Copy-Item ("\\omafs01\commondev\CheckBOX\ThirdPartyProducts\Hyland\9.2.0.9\wfs\Hyland.types.dll")  $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFImageAPI\bin\Release\OLFImageAPI.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\Consolidator\ICONSvc\bin\Release\IconSvc.exe") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\Consolidator\ICONSvc\bin\Release\ICONAPI.dll") $LTABuildBin2Folder\.
		Copy-Item ($SourceCode + "\Codebase\OTIS\OTISConsole\bin\Release\Merged\OTISConsole.exe") $LTABuildBin2Folder\.
		
		
		New-Item  $LTABuildFolder -type directory
		
		# build the bin folder
		Write Host "Building Bin Folder"
		New-Item  $LTABuildBinFolder -type directory
		Copy-Item ($SourceCode + "\Support\bin\Microsoft\sc.exe") $LTABuildBinFolder\.
		if($buildcode -eq "Build10108" ) 
		{
			Copy-Item ($SourceCode + "\Support\bin\WFS\CBOCrypto.dll") $LTABuildBinFolder\.
		}
	


		#build the OLDescision folder

		Write Host "OLDescision Folder"
		$LTABuildOLDescisionFolder = $LTABuildFolder + "\\wwwroot\OLDecisioningServices";
		$LTABuildOLDescisionFolderBin = $LTABuildFolder + "\\wwwroot\OLDecisioningServices\bin";

		New-Item  $LTABuildOLDescisionFolder -type directory
		New-Item  $LTABuildOLDescisionFolderBin -type directory

		Copy-Item ($SourceCode + "\Codebase\OLDecisioning\OLDecisioningServices\*.asmx") $LTABuildOLDescisionFolder\.
		Copy-Item ($SourceCode + "\Codebase\OLDecisioning\OLDecisioningServices\*.config") $LTABuildOLDescisionFolder\.
		Copy-Item ($SourceCode + "\Codebase\OLDecisioning\OLDecisioningServices\bin\*.dll") $LTABuildOLDescisionFolderBin\.
		Copy-Item ($SourceCode + "\Codebase\OLDecisioning\OLDecisioningServices\bin\*.exe")$LTABuildOLDescisionFolderBin\.

		#build the IPLogon folder

		Write Host "IPLOGON Folder"
		$LTABuildIPLOGONFolder = $LTABuildFolder + "\\wwwroot\IPLogon\";
		$LTABuildIPLOGONFolderBin = $LTABuildFolder + "\\wwwroot\IPLogon\bin";

		New-Item  $LTABuildIPLOGONFolder -type directory
		New-Item  $LTABuildIPLOGONFolderBin -type directory



		RecursiveCopy ($SourceCode + "\Codebase\OLLogon\IPLogon\") $LTABuildIPLOGONFolder "*.gif" ;
		RecursiveCopy ($SourceCode + "\Codebase\OLLogon\IPLogon\") $LTABuildIPLOGONFolder "*.css" ;
		RecursiveCopy ($SourceCode + "\Codebase\OLLogon\IPLogon\") $LTABuildIPLOGONFolder "*.aspx" ;
		RecursiveCopy ($SourceCode + "\Codebase\OLLogon\IPLogon\") $LTABuildIPLOGONFolder "*.config" ;
		RecursiveCopy ($SourceCode + "\Codebase\OLLogon\IPLogon\") $LTABuildIPLOGONFolder "*.xsl" ;
		RecursiveCopy ($SourceCode + "\Codebase\OLLogon\IPLogon\") $LTABuildIPLOGONFolder "*.js" ;
		RecursiveCopy ($SourceCode + "\Codebase\OLLogon\IPLogon\") $LTABuildIPLOGONFolder "*.jpg" ;
		RecursiveCopy ($SourceCode + "\Codebase\OLLogon\IPLogon\") $LTABuildIPLOGONFolder "*.png" ;
		Copy-Item ($SourceCode + "\Codebase\OLLogon\IPLogon\*.asmx") $LTABuildIPLOGONFolder\.
		Copy-Item ($SourceCode + "\Codebase\OLLogon\IPLogon\*.config") $LTABuildIPLOGONFolder\.
		Copy-Item ($SourceCode + "\Codebase\OLLogon\IPLogon\bin\*.dll")  $LTABuildIPLOGONFolderBin\.
		Copy-Item ($SourceCode + "\Codebase\OLLogon\IPLogon\bin\*.exe")  $LTABuildIPLOGONFolderBin\.


		#build the OLLogonServices folder

		Write Host "OLLogonServices Folder"
		$LTABuildOLLogonServicesFolder = $LTABuildFolder + "\\wwwroot\OLLogonServices";
		$LTABuildOLLogonServicesFolderBin = $LTABuildFolder + "\\wwwroot\OLLogonServices\bin";

		New-Item  $LTABuildOLLogonServicesFolder -type directory
		New-Item  $LTABuildOLLogonServicesFolderBin -type directory


		Copy-Item ($SourceCode + "\Codebase\OLLogon\OLLogonServices\*.asmx") $LTABuildOLLogonServicesFolder\.
		Copy-Item ($SourceCode + "\Codebase\OLLogon\OLLogonServices\*.config") $LTABuildOLLogonServicesFolder\.
		Copy-Item ($SourceCode + "\Codebase\OLLogon\OLLogonServices\bin\*.dll") $LTABuildOLLogonServicesFolderBin\.
		Copy-Item ($SourceCode + "\Codebase\OLLogon\OLLogonServices\bin\*.exe") $LTABuildOLLogonServicesFolderBin\.



		#build the IPonline folder

		Write Host "IPonline Folder"
		$LTABuildIPOnlineFolder = $LTABuildFolder + "\\wwwroot\IPOnline\";
		$LTABuildIPOnlineFolderBin = $LTABuildFolder + "\\wwwroot\IPOnline\bin";

		New-Item  $LTABuildIPOnlineFolder -type directory
		New-Item  $LTABuildIPOnlineFolderBin -type directory


		Copy-Item ($SourceCode + "\Codebase\Online\IPOnline\*.asmx") $LTABuildIPOnlineFolder\.
		RecursiveCopy ($SourceCode + "\Codebase\Online\IPOnline\") $LTABuildIPOnlineFolder "*.gif" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\IPOnline\") $LTABuildIPOnlineFolder "*.css" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\IPOnline\") $LTABuildIPOnlineFolder "*.aspx" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\IPOnline\") $LTABuildIPOnlineFolder "*.config" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\IPOnline\") $LTABuildIPOnlineFolder "*.xsl" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\IPOnline\") $LTABuildIPOnlineFolder "*.js" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\IPOnline\") $LTABuildIPOnlineFolder "*.jpg" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\IPOnline\") $LTABuildIPOnlineFolder "*.png" ;
		Copy-Item ($SourceCode + "\Codebase\Online\IPOnline\bin\*.dll") $LTABuildIPOnlineFolderBin\.

		#build the OLAdmin folder

		Write Host "OLAdmin Folder"
		$LTABuildOLAdminFolder = $LTABuildFolder + "\\wwwroot\OLAdmin\";
		$LTABuildOLAdminFolderBin = $LTABuildFolder + "\\wwwroot\OLAdmin\bin";
		$LTABuildOLAdminFolderStyles = $LTABuildFolder + "\\wwwroot\OLAdmin\Styles\Admin";

		New-Item  $LTABuildOLAdminFolder -type directory
		New-Item  $LTABuildOLAdminFolderBin -type directory
		New-Item  $LTABuildOLAdminFolderStyles -type directory

		RecursiveCopy ($SourceCode + "\Codebase\Online\OLAdmin\") $LTABuildOLAdminFolder "*.gif" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\OLAdmin\") $LTABuildOLAdminFolder "*.css" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\OLAdmin\") $LTABuildOLAdminFolder "*.aspx" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\OLAdmin\") $LTABuildOLAdminFolder "*.config" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\OLAdmin\") $LTABuildOLAdminFolder "*.xsl" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\OLAdmin\") $LTABuildOLAdminFolder "*.js" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\OLAdmin\") $LTABuildOLAdminFolder "*.jpg" ;
		RecursiveCopy ($SourceCode + "\Codebase\Online\OLAdmin\") $LTABuildOLAdminFolder "*.png" ;
		Copy-Item ($SourceCode + "\Codebase\Online\OLAdmin\bin\*.dll") $LTABuildOLAdminFolderBin\.
		Copy-Item ($SourceCode + "\Codebase\Online\OLAdmin\Styles\Admin\*.html") $LTABuildOLAdminFolderStyles\.



		#build the OLServices folder

		Write Host "OLServices Folder"
		$LTABuildOLServicesFolder = $LTABuildFolder + "\\wwwroot\OLServices";
		$LTABuildOLServicesFolderBin = $LTABuildFolder + "\\wwwroot\OLServices\bin";

		New-Item  $LTABuildOLServicesFolder -type directory
		New-Item  $LTABuildOLServicesFolderBin -type directory


		Copy-Item ($SourceCode + "\Codebase\Online\OLServices\*.asmx") $LTABuildOLServicesFolder\.
		Copy-Item ($SourceCode + "\Codebase\Online\OLServices\*.config") $LTABuildOLServicesFolder\.
		Copy-Item ($SourceCode + "\Codebase\Online\OLServices\bin\*.dll") $LTABuildOLServicesFolderBin\.


		#build the OLFServices folder

		Write Host "OLFServices Folder"
		$LTABuildOLFServicesFolder = $LTABuildFolder + "\\wwwroot\OLFServices";
		$LTABuildOLFServicesFolderBin = $LTABuildFolder + "\\wwwroot\OLFServices\bin";

		New-Item  $LTABuildOLFServicesFolder -type directory
		New-Item  $LTABuildOLFServicesFolderBin -type directory


		Copy-Item ($SourceCode + "\Codebase\OLF\OLFServices\*.svc") $LTABuildOLFServicesFolder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFServices\*.config") $LTABuildOLFServicesFolder\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFServices\bin\*.dll") $LTABuildOLFServicesFolderBin\.


		#build the OLFImageAPI folder

		Write Host "OLFImageAPI Folder"
		$LTABuildOLFImageAPIResource = $LTABuildFolder + "\\data\ServiceStyles\Online\resource";
		$LTABuildOLFImageAPITemplates = $LTABuildFolder + "\\data\ServiceStyles\Online\templates";

		New-Item  $LTABuildOLFImageAPIResource -type directory
		New-Item  $LTABuildOLFImageAPITemplates -type directory


		Copy-Item ($SourceCode + "\Codebase\OLF\OLFImageAPI\resource\*.xsl") $LTABuildOLFImageAPITemplates\.
		Copy-Item ($SourceCode + "\Codebase\OLF\OLFImageAPI\resource\ImageNotAvailable.jpg") $LTABuildOLFImageAPIResource\.


		#build the SSIS Templates folder

		Write Host "SSIS Templates Folder"
		$LTABuildSSISTempaltes= $LTABuildFolder + "\\database\SSIS";
		New-Item  $LTABuildSSISTempaltes -type directory
		Copy-Item ($SourceCode + "\Database\SSIS\LegacyDataMigration\*.ps1") $LTABuildSSISTempaltes\.
		Copy-Item ($SourceCode + "\Database\SSIS\LegacyDataMigration\*.sql") $LTABuildSSISTempaltes\.


		#build the OLFICONServices folder

		Write Host "OLFICONServices Folder"
		$LTABuildOLFICONServicesFolder = $LTABuildFolder + "\\wwwroot\OLFICONServices";
		$LTABuildOLFICONServicesFolderBin = $LTABuildFolder + "\\wwwroot\OLFICONServices\bin";

		New-Item  $LTABuildOLFICONServicesFolder -type directory
		New-Item  $LTABuildOLFICONServicesFolderBin -type directory


		Copy-Item ($SourceCode + "\Codebase\Consolidator\OLFICONServices\*.asmx") $LTABuildOLFICONServicesFolder\.
		Copy-Item ($SourceCode + "\Codebase\Consolidator\OLFICONServices\*.config") $LTABuildOLFICONServicesFolder\.
		Copy-Item ($SourceCode + "\Codebase\Consolidator\OLFICONServices\bin\*.dll") $LTABuildOLFICONServicesFolderBin\.
		Copy-Item ($SourceCode + "\Codebase\Consolidator\OLFICONServices\bin\Xceed.Compression.dll") $LTABuildOLFICONServicesFolderBin\.
		Copy-Item ($SourceCode + "\Codebase\Consolidator\OLFICONServices\bin\Xceed.FileSystem.dll") $LTABuildOLFICONServicesFolderBin\.
		Copy-Item ($SourceCode + "\Codebase\Consolidator\OLFICONServices\bin\Xceed.Zip.dll") $LTABuildOLFICONServicesFolderBin\.
		
		#build the CSRResearch folder

		Write Host "CSRResearch Folder"
		$LTABuildCSRResearchFolder = $LTABuildFolder + "\\wwwroot\CSRResearch";
		$LTABuildCSRResearchFolderBin = $LTABuildFolder + "\\wwwroot\CSRResearch\bin";
		$LTABuildCSRResearchFolderScripts = $LTABuildFolder + "\\wwwroot\CSRResearch\scripts";
		$LTABuildCSRResearchFolderStyles = $LTABuildFolder + "\\wwwroot\CSRResearch\styles";

		New-Item  $LTABuildCSRResearchFolder -type directory
		New-Item  $LTABuildCSRResearchFolderBin -type directory
		New-Item  $LTABuildCSRResearchFolderScripts -type directory
		New-Item  $LTABuildCSRResearchFolderStyles -type directory

		Copy-Item ($SourceCode + "\Codebase\Research\CSRResearch\Default.aspx") $LTABuildCSRResearchFolder\.
		Copy-Item ($SourceCode + "\Codebase\Research\CSRResearch\web.config") $LTABuildCSRResearchFolder\.
		Copy-Item ($SourceCode + "\Codebase\Research\CSRResearch\bin\*.dll") $LTABuildCSRResearchFolderBin\.
		Copy-Item ($SourceCode + "\Codebase\Research\CSRResearch\styles\*") $LTABuildCSRResearchFolderStyles -Recurse
		Copy-Item ($SourceCode + "\Codebase\Research\CSRResearch\scripts\*.aspx") $LTABuildCSRResearchFolderScripts\.

		#build the InstallScripts folder

		Write Host "InstallScripts Folder"
		$LTABuildInstallScriptsFolder = $LTABuildFolder + "\\Support\InstallScripts";

		New-Item  $LTABuildInstallScriptsFolder -type directory
		Copy-Item ($SourceCode + "\support\InstallScripts\*.vbs") $LTABuildInstallScriptsFolder\.
		Copy-Item ($SourceCode + "\support\InstallScripts\*.vbs") $LTABuildFolder\.


		#build the UtilScripts folder

		Write Host "UtilScripts Folder"
		$LTABuildUtilScriptsFolder = $LTABuildFolder + "\\scripts";

		New-Item  $LTABuildUtilScriptsFolder -type directory
		Copy-Item ($SourceCode + "\support\SupportScripts\*.vbs") $LTABuildUtilScriptsFolder\.




		#build the Powershell folder

		Write Host "Powershell Folder"
		$LTABuildDBFolder = $LTABuildFolder + "\\Database";
		Copy-Item ($SourceCode + "\Database\SupportScripts\InstallPatch.ps1") $LTABuildDBFolder\.
	}
	
}


function GetTFSSourceCode()
{

	#get the source code
	Write-Host "Delete Workspace"
	$TFS.VCS.DeleteWorkspace($workspaceName + $computer, $tfs.AuthenticatedUserName);
	if ($buildcode -ne "Build10108" ) 
	{
		Write-Host "Delete Directory"
		Remove-Item -Recurse -Force $SourceCode
	}
	Write-Host "Create Workspace"
	$workspace = $TFS.VCS.CreateWorkSpace($workspaceName + $computer, $tfs.AuthenticatedUserName, $workspaceComment);
	Write-Host "Map Workspace"
	$workspace.Map($serverSource, $SourceCode)
	Write-Host "Get Workspace"
	$TFS.VCS.GetWorkSpace($SourceCode)
	Write-Host "Get SourceCode"
	$workspace.Get()
	
}
# Select environment MENU
Write-Host "Choose your build option."
Write-Host "1. Build 1.01.08"
Write-Host "2. Build 1.04"
Write-Host "3. Build  1.05 Ini "
Write-Host "4. Build  DB Code "
Write-Host "5. Build  Consolidtor VB Code "
Write-Host "6. Build 1.02"
Write-Host "7. Build 1.05"
Write-Host " "
$a = Read-Host "Select 1-7: "
 
Write-Host " "
 
switch ($a) 
    { 
        1 {
           "** 1.01.08 **";
           $buildCode = "Build10108";
		   $SourceCode = "\\omaoltabuild\OLTABuild1.01.08"
		   $workspaceName = "1.01.08TFS"
		   $serverSource = "$/Receivables_Hub/1.01.08"
		   $workspaceComment =  "1.01.08 Workspace for all Build"
           break;
          } 
        2 {
           "**1.04  **";
           $buildCode = "Build104";
		   $SourceCode = "c:\1.04BuildTFSRecHub"
		   $workspaceName = "1.04TFS"
		   $serverSource = "$/Receivables_Hub/1.04"
		   $workspaceComment =  "1.04 Workspace for all Build"
           break;
          } 
        3 {
           "** 1.05 INI **";
           $buildCode = "Build105INI";
		   $workspaceName = "1.05INI"
		   $serverSource = "$/Receivables_Hub/Main/Ini/"
		   $workspaceComment =  "Main Workspace for Ini"
           break;
          } 
		 4 {
           "** Build DB Code **";
           $buildCode = "BuildDBCode";
           break;
          } 
		 5 {
           "** Build Consolidator (VB) Code **";
           $buildCode = "BuildConsolidatorVBCode";
		   $SourceCode = "\\omaoltabuild\ConsoldiatorVB"
		   $workspaceName = "ConsoldiatorVBTFS"
		   $serverSource = "$/IntegraPAY_Online/Main"
		   $workspaceComment =  "ConsoldiatorVB Workspace for all Build"
           break;
          } 
		 6 {
           "**1.02  **";
           $buildCode = "Build102";
		   $SourceCode = "c:\1.02BuildTFSRecHub"
		   $workspaceName = "1.02TFS"
		   $serverSource = "$/Receivables_Hub/1.02"
		   $workspaceComment =  "1.02 Workspace for all Build"
           break;
          } 
		  7 {
           "**1.05  **";
           $buildCode = "Build105";
		  
           break;
          } 
        default {
          "** The selection could not be determined **";
          break;
          }
}

$SourceCode = $SourceCode   + $env:username 
Write-Host $buildCode



#Connect to the TFS Server
$TFS = Get-TFS $TFSServer;

if ($buildCode -ne "Build105")
{
	GetTFSSourceCode
}


Write-Host "Prompt for wait for build"

#now go ahead an build it

Write-Host "Build the code"
$builtWithTfs = 0
switch ($buildCode)
{
	"Build105" 
		{
			$WorkItemCollection = $TFS.WIT.Query("SELECT [System.Id] FROM WorkItems WHERE [System.AreaPath] under 'Receivables_Hub'  AND [Microsoft.VSTS.CMMI.TaskType] != 'DB Change' AND [System.IterationPath] under 'Receivables_Hub\1.05' AND  [System.State] = 'Resolved'  AND [WFS.WorkItemStatus] = 'Ready For Build' ORDER BY [System.Id]")
			
			
			$builtWithTfs = 1

		}	
	"Build105INI" 
		{
			$WorkItemCollection = $TFS.WIT.Query("SELECT [System.Id] FROM WorkItems WHERE [System.AreaPath] under 'Receivables_Hub\Ini Files'  AND [Microsoft.VSTS.CMMI.TaskType] != 'DB Change' AND [System.IterationPath] under 'Receivables_Hub\1.05' AND  [System.State] = 'Resolved'  AND [WFS.WorkItemStatus] = 'Ready For Build' ORDER BY [System.Id]")

		}
	"Build102" 
		{
			Build-VisualStudioSolution

			$WorkItemCollection = $TFS.WIT.Query("SELECT [System.Id] FROM WorkItems WHERE [System.AreaPath] under 'Receivables_Hub'  AND [Microsoft.VSTS.CMMI.TaskType] != 'DB Change' AND [System.IterationPath] under 'Receivables_Hub\1.02' AND  [System.State] = 'Resolved'  AND [WFS.WorkItemStatus] = 'Ready For Build' ORDER BY [System.Id]")

		}
	"Build104" 
		{
			Build-VisualStudioSolution

			$WorkItemCollection = $TFS.WIT.Query("SELECT [System.Id] FROM WorkItems WHERE [System.AreaPath] under 'Receivables_Hub'  AND [Microsoft.VSTS.CMMI.TaskType] != 'DB Change' AND [System.IterationPath] under 'Receivables_Hub\1.04' AND  [System.State] = 'Resolved'  AND [WFS.WorkItemStatus] = 'Ready For Build' ORDER BY [System.Id]")

		}
	"Build10108" 
		{
			#now go ahead an build it

			Write-Host "Please log on on to the omaoltabuild build server. Got to directory.\\omaoltabuild\OLTABuild1.01.08\. When you are done building press any key to continue this script. Thanks!! "

			$input = Read-Host "Please log on on to the omaoltabuild build server. Got to directory.\\omaoltabuild\OLTABuild1.01.08\. When you are done building. Press ok to continue the script. Thanks!!"
			


			$WorkItemCollection = $TFS.WIT.Query("SELECT [System.Id] FROM WorkItems WHERE [System.AreaPath] under 'Receivables_Hub'  AND [Microsoft.VSTS.CMMI.TaskType] != 'DB Change' AND [System.IterationPath] under 'Receivables_Hub\1.01\1.01.08' AND  [System.State] = 'Resolved'  AND [WFS.WorkItemStatus] = 'Ready For Build' ORDER BY [System.Id]")


		}
	"BuildDBCode" 
		{
		}
	"BuildConsolidatorVBCode" 
		{
		}
	default:
		{
			Write-Host "Error this is really bad!. Should never happen!!"
		}
}

if( $WorkItemCollection.Count -gt 0 )
{
	if ($builtWithTfs -eq 0)
	{
		CreateBuildFolder ([ref]$BuildFolder) ([ref]$BuildNumber);
	
	
	
		Write-Host "Build Number       " $BuildNumber;
		Write-Host;

		BuildDirectories($BuildFolder)
	}

	$EmailBody = "<html><body><table><tr colspan=""3""><td>Build Number: </td><td><b>"+ $BuildNumber +"</b></td></tr><tr><th>Work Item</th><th>Release</th><th>Description</th></tr>";
	for( $i=0;$i -lt $WorkItemCollection.Count; $i++ )
	{
		$WorkItem = $TFS.WIT.GetWorkItem($WorkItemCollection.Item($i).Id);
	
		if ($builtWithTfs -eq 1)
		{
			Build-TFSBuild([ref]$BuildNumber);
			Write-Host "Build Number       " $BuildNumber;
		}

		$EmailBody += "<tr><td><b>" + $WorkItemCollection.Item($i).Id.ToString() + "</b></td><td>" + $ReleaseVersion + "</td><td>" + $WorkItemCollection.Item($i).Title + "</td>";

		
		Write-Host;
		Write-Host "Work Item      " $WorkItem.Id
		Write-Host "Build Folder  " $BuildFolder
	

		foreach( $Field in $WorkItem.Fields )
		{
			if( $Field.ReferenceName -eq "Microsoft.VSTS.Build.IntegrationBuild" )
			{
				$Field.Value = $BuildNumber;
			}
			if( $Field.ReferenceName -eq "WFS.WorkItemStatus" )
			{
				$Field.Value = "Build Complete";
			}
			if( $Field.ReferenceName -eq "System.AssignedTo" -and $local:AssignToQA -eq $true )
			{
				$Field.Value = "Steve Eckhart";
			}
#			if( $Field.ReferenceName -eq "System.State" )
#			{
#				$Field.Value = "Active";
#			}
		}
		$WorkItem.Save();
		AssignToQA $WorkItem;
	}
	
	$message = New-Object System.Net.Mail.MailMessage;
	$FromAddress = new-object System.Net.Mail.MailAddress("OLTA Build Completion@wausaufs.com")
	$message.Subject = ("OLTA Software Build Completion Announcement " + (Get-Date –f "yyyy-MM-dd HH:mm:ss"));
	$message.From = $FromAddress
	$message.To.Add("Joel Caples <joel.caples@wausaufs.com>");
	$message.To.Add("Wayne Schwarz <Wayne.Schwarz@wausaufs.com>");
	$message.To.Add("Steve Eckhart <Steve.Eckhart@wausaufs.com>");
	$message.To.Add("Mike Elwood <mike.elwood@wausaufs.com>");
	$message.To.Add("Joy Sucha <joy.sucha@wausaufs.com>");
	$message.To.Add("Paul Troya <PTroya@Wausaufs.com>");
	$message.CC.Add("John Boehm <john.boehm@wausaufs.com>");
	$message.IsBodyHTML = $true;
	$message.Body = $EmailBody + "</table></body></html>";
	$smtp = New-Object Net.Mail.SmtpClient("172.25.0.31")
	$smtp.Send($message)
}


    