::********************************************************
::Step 1
::********************************************************
"C:\Program Files\Microsoft SDKs\Windows\v6.0A\bin\ildasm.exe" ..\bin\Hyland\7.2.1.157\Hyland.Types.dll /out:Hyland.Types.il

::********************************************************
::Step 2: Re-Assemble using your strong-name key
::********************************************************
"C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727\ilasm.exe" Hyland.Types.il /res:Hyland.Types.res /dll /key:..\..\keys\ipoKeyPair.snk /out:..\bin\Hyland\7.2.1.157\Hyland.Types.wfs.dll

::********************************************************
::for verification you can use following command,
::********************************************************
"C:\Program Files\Microsoft SDKs\Windows\v6.0A\bin\sn.exe" -vf Hyland.Types.wfs.dll

::********************************************************
::Clean Up
::********************************************************
Del Hyland.Types.il
Del Hyland.Types.res
