﻿<system.identityModel.services>
  <federationConfiguration>
    <cookieHandler path="/" 
                   requireSsl="true" 
                   name="idsrvauth" />
    
    <wsFederation passiveRedirectEnabled="true" 
                  issuer="https://@@RAAM_APP_SERVER@@/Wfs.Raam.IdentityServer/issue/wsfed" 
                  realm="https://@@APP_SERVER@@/FrameworkServices/" 
                  requireHttps="true" />
    
  </federationConfiguration>
</system.identityModel.services>