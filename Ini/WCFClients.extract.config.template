﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="wfs.raam.trustedsubsystem.credential" type="Wfs.Raam.Core.Configuration.TrustedSubsystemCredentialSection, Wfs.Raam.Core" />
    <section name="system.identityModel" type="System.IdentityModel.Configuration.SystemIdentityModelSection, System.IdentityModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
    <section name="wfs.raam.core" type="Wfs.Raam.Core.Configuration.CoreSettingsSection, Wfs.Raam.Core" />
  </configSections>

  <wfs.raam.core applicationcontextswitchenabled="true" serversidetokencachingenabled="true" />

  <system.identityModel>
    <identityConfiguration saveBootstrapContext="true">

      <audienceUris>
        <add value="urn:Wfs.AppServices" />
      </audienceUris>

      <certificateValidation certificateValidationMode="PeerOrChainTrust" revocationMode="NoCheck" />

      <issuerNameRegistry type="System.IdentityModel.Tokens.ConfigurationBasedIssuerNameRegistry, System.IdentityModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089">
        <trustedIssuers>
          <add thumbprint="@@RAAM_THUMBPRINT@@" name="https://wfs.raam.com/trust" />
        </trustedIssuers>
      </issuerNameRegistry>

    </identityConfiguration>
  </system.identityModel>

  <wfs.raam.trustedsubsystem.credential encryptedUsername="@@RAAM_SYSTEM_USER@@"
                                        encryptedPassword="@@RAAM_SYSTEM_PASSWORD@@"
                                        issuerAddress="https://@@RAAM_APP_SERVER@@/Wfs.Raam.IdentityServer/issue/wstrust/mixed/username"
                                        cacheToken="true">
    <relyingParties>
      <relyingParty realm="urn:Wfs.AppServices" contract="WFS.RecHub.OLFServices.IOLFService"/>
      <relyingParty realm="urn:Wfs.AppServices" contract="WFS.RecHub.ExternalLogon.Common.IExternalLogonService"/>
      <relyingParty realm="urn:Wfs.AppServices" contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IAuthorizationPolicyContract" />
      <relyingParty realm="urn:Wfs.AppServices" contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IApplicationContract" />
      <relyingParty realm="urn:Wfs.AppServices" contract="Wfs.Raam.Service.IdentityServer.Contracts.ServiceContracts.IFederationContract" />
    </relyingParties>
  </wfs.raam.trustedsubsystem.credential>
  
  <system.serviceModel>
    <bindings>

      <ws2007FederationHttpBinding>
        <!-- Standard RAAM-Enabled HTTPS Binding -->
        <binding name="WfsRaamBinding" maxReceivedMessageSize="67108864" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="TransportWithMessageCredential">
            <message establishSecurityContext="false" issuedKeyType="BearerKey" />
          </security>
        </binding>
      </ws2007FederationHttpBinding>

      <wsHttpBinding>
        <binding name="wsHTTP">
          <security mode="Transport">
            <transport clientCredentialType="None"></transport>
          </security>
        </binding>
      </wsHttpBinding>
    </bindings>

    <client>
      <!-- OLF Service -->
      <endpoint address="https://@@APP_SERVER@@/OLFServices/OLFService.svc"
                contract="WFS.RecHub.OLFServices.IOLFService"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- External Logon Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubExternalLogonService/ExternalLogonService.svc"
                contract="WFS.RecHub.ExternalLogon.Common.IExternalLogonService"
                binding="wsHttpBinding" bindingConfiguration="wsHTTP" />

      <!-- RAAM Application Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.Authorization/ApplicationService.svc"
                contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IApplicationContract"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM Authorization Policy Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.Authorization/AuthorizationPolicyService.svc"
                contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IAuthorizationPolicyContract"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM Federation Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.IdentityServer/FederationService.svc"
                contract="Wfs.Raam.Service.IdentityServer.Contracts.ServiceContracts.IFederationContract"
                binding="wsHttpBinding" bindingConfiguration="wsHTTP" />
    </client>
  </system.serviceModel>
</configuration>