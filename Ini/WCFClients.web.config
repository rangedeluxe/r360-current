﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="system.identityModel" type="System.IdentityModel.Configuration.SystemIdentityModelSection, System.IdentityModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
    <section name="wfs.raam.trustedsubsystem.credential" type="Wfs.Raam.Core.Configuration.TrustedSubsystemCredentialSection, Wfs.Raam.Core" />
  </configSections>

  <system.identityModel>
    <identityConfiguration saveBootstrapContext="true">
      <issuerNameRegistry type="System.IdentityModel.Tokens.ConfigurationBasedIssuerNameRegistry, System.IdentityModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089">
        <trustedIssuers>
          <add thumbprint="@@RAAM_THUMBPRINT@@" name="https://wfs.raam.com/trust" />
        </trustedIssuers>
      </issuerNameRegistry>
    </identityConfiguration>
  </system.identityModel>

  <wfs.raam.trustedsubsystem.credential encryptedUsername="@@RAAM_SYSTEM_USER@@"
                                        encryptedPassword="@@RAAM_SYSTEM_PASSWORD@@"
                                        issuerAddress="https://@@RAAM_APP_SERVER@@/Wfs.Raam.IdentityServer/issue/wstrust/mixed/username"
                                        cacheToken="true">
    <relyingParties>

      <relyingParty realm="urn:Wfs.AppServices"   contract="framework.services.common.IBrandingService" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="framework.services.common.ITabService" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="framework.services.common.ILanguageService" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="Framework.SharedContracts.ISessionNotification" />

      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.IR360Services" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.HubConfig.IHubConfig" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.HubReport.Common.IHubReportService" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.BillingExtracts.Common.IBillingExtracts" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.EventSubmission.Common.IEventSubmissionService" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="ExtractScheduleService.IExtractScheduleService" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.SessionMaintenance.Common.ISessionMaintenance" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.PayerCommon.Interfaces.IPayerService" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.ISystemServices" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.ICBXServices" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.IImageServices" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.IRemitterServices" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.IReportServices" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.IUserServices" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.OLFServices.IOLFOnlineService"/>
      <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.OLFServices.IOLFService"/>
	  <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.IExceptionServices"/>
	  <relyingParty realm="urn:Wfs.AppServices"   contract="WFS.RecHub.R360Services.Common.IPostDepositExceptionServices"/>

      <relyingParty realm="urn:Wfs.AppServices"   contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IAuthorizationPolicyContract" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IEntityContract" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IResourceContract" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IApplicationContract" />
      <relyingParty realm="urn:Wfs.AppServices"   contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IUserContract" />
      <relyingParty realm="urn:Wfs.AppServices" contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IUserActivityContract" />
    </relyingParties>
  </wfs.raam.trustedsubsystem.credential>

  <system.serviceModel>

	<extensions>
		<bindingExtensions>
			<add name="WS2007FederationStreamHttpBinding" type="WFS.RecHub.R360Shared.Bindings.WS2007FederationStreamHttpBindingElement, R360Shared" />
		</bindingExtensions>
	</extensions>
  
    <bindings>
	  <!-- Stream-Enabled OLF Binding -->
	  <WS2007FederationStreamHttpBinding>
		  <binding name="WfsRaamStreamBinding" maxReceivedMessageSize="67108864" messageEncoding="Text" 
			closeTimeout="01:00:00"	openTimeout="01:00:00" receiveTimeout="01:00:00" sendTimeout="01:00:00">
			  <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
			  <security mode="TransportWithMessageCredential">
				<message establishSecurityContext="false" issuedKeyType="BearerKey" />
			  </security>
		  </binding>
	  </WS2007FederationStreamHttpBinding>
	
      <ws2007FederationHttpBinding>
        <!-- Standard RAAM-Enabled HTTPS Binding -->
		<binding name="WfsRaamBinding" maxReceivedMessageSize="2147483647" maxBufferPoolSize="2147483647" messageEncoding="Text" 
			closeTimeout="00:20:00"	openTimeout="00:20:00" receiveTimeout="00:20:00" sendTimeout="00:20:00" >
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="TransportWithMessageCredential">
            <message establishSecurityContext="false" issuedKeyType="BearerKey" />
          </security>
        </binding>

        <!-- RAAM-Enabled HTTPS Binding for Images (MTOM) -->
        <binding name="WfsRaamMtomBinding" maxReceivedMessageSize="67108864" messageEncoding="Mtom">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="TransportWithMessageCredential">
            <message establishSecurityContext="false" issuedKeyType="BearerKey" />
          </security>
        </binding>

      </ws2007FederationHttpBinding>

      <wsHttpBinding>
        <binding name="wsHttp" maxReceivedMessageSize="2147483647">
          <security mode="Transport">
            <transport clientCredentialType="None" />
          </security>
        </binding>
      </wsHttpBinding>
    </bindings>

    <client>
      <!-- Billing Extract Service -->
      <endpoint address="https://@@APP_SERVER@@:9500/RecHubBillingExtractsService/BillingExtracts.svc"
                contract="WFS.RecHub.BillingExtracts.Common.IBillingExtracts"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Event Submission Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubEventSubmissionService/EventSubmissionService.svc"
                contract="WFS.RecHub.EventSubmission.Common.IEventSubmissionService"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Hub Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/R360Services.svc"
                contract="WFS.RecHub.R360Services.Common.IR360Services"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />
				
	  <!-- R360 Service for Post-Deposit Exceptions -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/PostDepositExceptionServices.svc"
                contract="WFS.RecHub.R360Services.Common.IPostDepositExceptionServices"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- R360 Service CBX Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/CBXServices.svc"
                contract="WFS.RecHub.R360Services.Common.ICBXServices"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- R360 Service Image Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/ImageServices.svc"
                contract="WFS.RecHub.R360Services.Common.IImageServices"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- R360 Service Remitter Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/RemitterServices.svc"
                contract="WFS.RecHub.R360Services.Common.IRemitterServices"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- R360 Service Report Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/ReportServices.svc"
                contract="WFS.RecHub.R360Services.Common.IReportServices"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- R360 Service System Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/SystemServices.svc"
                contract="WFS.RecHub.R360Services.Common.ISystemServices"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />
		
	  <!-- R360 Exception Services -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/ExceptionServices.svc"
                contract="WFS.RecHub.R360Services.Common.IExceptionServices"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />
				
				
      <!-- R360 Service User Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubServices/UserServices.svc"
                contract="WFS.RecHub.R360Services.Common.IUserServices"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Hub Config Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubConfigService/HubConfig.svc"
                contract="WFS.RecHub.HubConfig.IHubConfig"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Hub Reports Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubReportingService/ReportService.svc"
                contract="WFS.RecHub.HubReport.Common.IHubReportService"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Extract Scheduler Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubExtractScheduleService/ExtractScheduleService.svc"
                contract="ExtractScheduleService.IExtractScheduleService"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- OLF Service -->
      <endpoint address="https://@@APP_SERVER@@/OLFServices/OLFService.svc"
                contract="WFS.RecHub.OLFServices.IOLFService"
                binding="WS2007FederationStreamHttpBinding" bindingConfiguration="WfsRaamStreamBinding" />

      <!-- OLF Online Service -->
      <endpoint address="https://@@APP_SERVER@@/OLFServices/OLFOnlineService.svc"
                contract="WFS.RecHub.OLFServices.IOLFOnlineService"
                binding="WS2007FederationStreamHttpBinding" bindingConfiguration="WfsRaamStreamBinding" />
      
      <!-- Payer Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubPayerService/PayerService.svc"
                contract="WFS.RecHub.PayerCommon.Interfaces.IPayerService"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Session Maintenance Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubSessionMaintenanceService/SessionMaintenanceService.svc"
                contract="WFS.RecHub.SessionMaintenance.Common.ISessionMaintenance"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM Authorization Policy Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.Authorization/AuthorizationPolicyService.svc"
                contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IAuthorizationPolicyContract"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM Entity Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.Authorization/EntityService.svc"
                contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IEntityContract"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM Resource Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.Authorization/ResourceService.svc"
                contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IResourceContract"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM Application Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.Authorization/ApplicationService.svc"
                contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IApplicationContract"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM User Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.Authorization/UserService.svc"
                contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IUserContract"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM User Activity Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.Authorization/UserActivityService.svc"
                contract="Wfs.Raam.Service.Authorization.Contracts.ServiceContracts.IUserActivityContract"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- RAAM Federation Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.IdentityServer/FederationService.svc"
                contract="Wfs.Raam.Service.IdentityServer.Contracts.ServiceContracts.IFederationContract"
                binding="wsHttpBinding" bindingConfiguration="wsHttp" />

      <!-- RAAM Token Caching Service -->
      <endpoint address="https://@@RAAM_APP_SERVER@@/Wfs.Raam.Service.TokenCache/TokenCacheService.svc"
                contract="Wfs.Raam.Service.TokenCache.Contracts.ServiceContracts.ITokenCacheContract"
                binding="wsHttpBinding" bindingConfiguration="wsHttp" />

      <!-- Framework Branding Service -->
      <endpoint address="https://@@APP_SERVER@@/FrameworkServices/Branding.svc"
                contract="framework.services.common.IBrandingService"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Framework Tab/Menu Service -->
      <endpoint address="https://@@APP_SERVER@@/FrameworkServices/Tab.svc"
                contract="framework.services.common.ITabService"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Framework Language Service -->
      <endpoint address="https://@@APP_SERVER@@/FrameworkServices/Language.svc"
                contract="framework.services.common.ILanguageService"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" />

      <!-- Framework Contract - R360 Session Maintenance Service -->
      <endpoint address="https://@@APP_SERVER@@/RecHubSessionMaintenanceService/SessionMaintenanceFrameworkService.svc"
                contract="Framework.SharedContracts.ISessionNotification"
                binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding"
                name="R360SessionNotification" />

    </client>
  </system.serviceModel>
</configuration>