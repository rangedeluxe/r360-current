﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.WebTesting;

namespace IPOnline_Performance_Tests
{
    public class RemoveRequest : WebTestRequestPlugin
    {
        public override void PostRequest(object sender, PostRequestEventArgs e)
        {
            List<WebTestRequest> remove = new List<WebTestRequest>();

            foreach (WebTestRequest dependent in e.Request.DependentRequests)
            {
                if (dependent.Url.EndsWith(".css") || dependent.Url.EndsWith(".js"))
                    remove.Add(dependent);
            }

            foreach (WebTestRequest dependent in remove)
            {
                e.Request.DependentRequests.Remove(dependent);
            }
        }
    }
}
