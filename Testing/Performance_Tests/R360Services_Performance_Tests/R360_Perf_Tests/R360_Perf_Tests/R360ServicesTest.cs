﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Claims;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

namespace R360_Perf_Tests
{
    [TestClass]
    public class R360ServicesTest
    {
        private string sid;
        private string sessionID;
        private Guid organizationID;
        private string function;
        private string mode;
        private DateTime summaryDate;
        private readonly R360ServiceManager _R360ServiceManager = new R360ServiceManager();

        [TestInitialize]
        public void Initalize()
        {
            sid = ConfigurationManager.AppSettings["SID"];
            sessionID = ConfigurationManager.AppSettings["SessionID"];
            organizationID = Guid.Parse(ConfigurationManager.AppSettings["OrganizationID"]);
            function = ConfigurationManager.AppSettings["Function"];
            mode = ConfigurationManager.AppSettings["Mode"];
            summaryDate = DateTime.Parse(ConfigurationManager.AppSettings["SummaryDate"]);

            ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.Sid, sid) }));
            //ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.SessionId, sessionID) }));
        }

        [TestMethod]
        public void GetClientAccounts()
        {
            try
            {
                ClientAccountsResponse response = _R360ServiceManager.GetClientAccounts();

                if (response.Status == StatusCode.SUCCESS)
                    return;
                else
                    throw new Exception("Fail Return Code");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [TestMethod]
        public void GetClientAccountSummary()
        {
            try
            {
                SummariesResponse response = _R360ServiceManager.GetClientAccountSummary(summaryDate);

                if (response.Status == StatusCode.SUCCESS)
                    return;
                else
                    throw new Exception("Fail Return Code");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //[TestMethod]
        //public void GetPaymentTypeTotals()
        //{
        //    try
        //    {
        //        cChartsResponse response = _R360ServiceManager.GetPaymentTypeTotals(Guid.Empty, organizationID, summaryDate);

        //        if (response.Status == StatusCode.SUCCESS)
        //            return;
        //        else
        //            throw new Exception("Fail Return Code");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //[TestMethod]
        //public void GetExceptionTypeSummary()
        //{
        //    try
        //    {
        //        cExceptionTypeSummaryResponse response = _R360ServiceManager.GetExceptionTypeSummary(organizationID, organizationID);

        //        if (response.Status == StatusCode.SUCCESS)
        //            return;
        //        else
        //            throw new Exception("Fail Return Code");

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        [TestMethod]
        public void GetClientAccountsForOrganization()
        {
            try
            {
                ClientAccountsResponse response = _R360ServiceManager.GetClientAccountsForOrganization(organizationID);

                if (response.Status == StatusCode.SUCCESS)
                    return;
                else
                    throw new Exception("Fail Return Code");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [TestMethod]
        public void CheckUserPermissionsForFunction()
        {
            try
            {
                PermissionsResponse response = _R360ServiceManager.CheckUserPermissionsForFunction(function, mode);

                if (response.Status == StatusCode.SUCCESS)
                    return;
                else
                    throw new Exception("Fail Return Code");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
