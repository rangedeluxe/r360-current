﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvancedSearchUnitTest
{
    [TestClass()]
    public class AdvSearchUnitTest : SqlDatabaseTestClass
    {

        public AdvSearchUnitTest()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SelectChksCourtesyAmt_FilterCourtesyAmt_Success_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvSearchUnitTest));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition1;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition2;
            this.SelectChksCourtesyAmt_FilterCourtesyAmt_SuccessData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            SelectChksCourtesyAmt_FilterCourtesyAmt_Success_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition1 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            scalarValueCondition2 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            // 
            // SelectChksCourtesyAmt_FilterCourtesyAmt_Success_TestAction
            // 
            SelectChksCourtesyAmt_FilterCourtesyAmt_Success_TestAction.Conditions.Add(scalarValueCondition1);
            SelectChksCourtesyAmt_FilterCourtesyAmt_Success_TestAction.Conditions.Add(scalarValueCondition2);
            resources.ApplyResources(SelectChksCourtesyAmt_FilterCourtesyAmt_Success_TestAction, "SelectChksCourtesyAmt_FilterCourtesyAmt_Success_TestAction");
            // 
            // scalarValueCondition1
            // 
            scalarValueCondition1.ColumnNumber = 1;
            scalarValueCondition1.Enabled = true;
            scalarValueCondition1.ExpectedValue = "58";
            scalarValueCondition1.Name = "scalarValueCondition1";
            scalarValueCondition1.NullExpected = false;
            scalarValueCondition1.ResultSet = 3;
            scalarValueCondition1.RowNumber = 1;
            // 
            // scalarValueCondition2
            // 
            scalarValueCondition2.ColumnNumber = 4;
            scalarValueCondition2.Enabled = true;
            scalarValueCondition2.ExpectedValue = "76936.80";
            scalarValueCondition2.Name = "scalarValueCondition2";
            scalarValueCondition2.NullExpected = false;
            scalarValueCondition2.ResultSet = 3;
            scalarValueCondition2.RowNumber = 1;
            // 
            // SelectChksCourtesyAmt_FilterCourtesyAmt_SuccessData
            // 
            this.SelectChksCourtesyAmt_FilterCourtesyAmt_SuccessData.PosttestAction = null;
            this.SelectChksCourtesyAmt_FilterCourtesyAmt_SuccessData.PretestAction = null;
            this.SelectChksCourtesyAmt_FilterCourtesyAmt_SuccessData.TestAction = SelectChksCourtesyAmt_FilterCourtesyAmt_Success_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void SelectChksCourtesyAmt_FilterCourtesyAmt_Success()
        {
            SqlDatabaseTestActions testActions = this.SelectChksCourtesyAmt_FilterCourtesyAmt_SuccessData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            try
            {
                // Execute the test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
                SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            }
            finally
            {
                // Execute the post-test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
                SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
            }
        }
        private SqlDatabaseTestActions SelectChksCourtesyAmt_FilterCourtesyAmt_SuccessData;
    }
}
