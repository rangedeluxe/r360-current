﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvancedSearchUnitTest
{
    [TestClass()]
    public class SelectStubsAmount_FilterStubsAmountJ_Returns13RowsSuccess : SqlDatabaseTestClass
    {

        public SelectStubsAmount_FilterStubsAmountJ_Returns13RowsSuccess()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SelStubsAmt_FilterStubsAmt_Success_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectStubsAmount_FilterStubsAmountJ_Returns13RowsSuccess));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.RowCountCondition rowCountIs13;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition XMLRowCountIs13;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition FirstChecksAccountIs03108116;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SelStubsAmt_FilterStubsAmt_Success_PretestAction;
            this.SelStubsAmt_FilterStubsAmt_SuccessData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            SelStubsAmt_FilterStubsAmt_Success_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            rowCountIs13 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.RowCountCondition();
            XMLRowCountIs13 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            FirstChecksAccountIs03108116 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            SelStubsAmt_FilterStubsAmt_Success_PretestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            // 
            // SelStubsAmt_FilterStubsAmt_Success_TestAction
            // 
            SelStubsAmt_FilterStubsAmt_Success_TestAction.Conditions.Add(rowCountIs13);
            SelStubsAmt_FilterStubsAmt_Success_TestAction.Conditions.Add(XMLRowCountIs13);
            SelStubsAmt_FilterStubsAmt_Success_TestAction.Conditions.Add(FirstChecksAccountIs03108116);
            resources.ApplyResources(SelStubsAmt_FilterStubsAmt_Success_TestAction, "SelStubsAmt_FilterStubsAmt_Success_TestAction");
            // 
            // rowCountIs13
            // 
            rowCountIs13.Enabled = true;
            rowCountIs13.Name = "rowCountIs13";
            rowCountIs13.ResultSet = 1;
            rowCountIs13.RowCount = 13;
            // 
            // XMLRowCountIs13
            // 
            XMLRowCountIs13.ColumnNumber = 1;
            XMLRowCountIs13.Enabled = true;
            XMLRowCountIs13.ExpectedValue = "13";
            XMLRowCountIs13.Name = "XMLRowCountIs13";
            XMLRowCountIs13.NullExpected = false;
            XMLRowCountIs13.ResultSet = 3;
            XMLRowCountIs13.RowNumber = 1;
            // 
            // FirstChecksAccountIs03108116
            // 
            FirstChecksAccountIs03108116.ColumnNumber = 16;
            FirstChecksAccountIs03108116.Enabled = true;
            FirstChecksAccountIs03108116.ExpectedValue = "03108116";
            FirstChecksAccountIs03108116.Name = "FirstChecksAccountIs03108116";
            FirstChecksAccountIs03108116.NullExpected = false;
            FirstChecksAccountIs03108116.ResultSet = 1;
            FirstChecksAccountIs03108116.RowNumber = 1;
            // 
            // SelStubsAmt_FilterStubsAmt_Success_PretestAction
            // 
            resources.ApplyResources(SelStubsAmt_FilterStubsAmt_Success_PretestAction, "SelStubsAmt_FilterStubsAmt_Success_PretestAction");
            // 
            // SelStubsAmt_FilterStubsAmt_SuccessData
            // 
            this.SelStubsAmt_FilterStubsAmt_SuccessData.PosttestAction = null;
            this.SelStubsAmt_FilterStubsAmt_SuccessData.PretestAction = SelStubsAmt_FilterStubsAmt_Success_PretestAction;
            this.SelStubsAmt_FilterStubsAmt_SuccessData.TestAction = SelStubsAmt_FilterStubsAmt_Success_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void SelStubsAmt_FilterStubsAmt_Success()
        {
            SqlDatabaseTestActions testActions = this.SelStubsAmt_FilterStubsAmt_SuccessData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            try
            {
                // Execute the test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
                SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            }
            finally
            {
                // Execute the post-test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
                SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
            }
        }
        private SqlDatabaseTestActions SelStubsAmt_FilterStubsAmt_SuccessData;
    }
}
