﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvancedSearchUnitTest
{
    [TestClass()]
    public class AdvSearch_NoSelectFields_1FilterField_Returns57Rows : SqlDatabaseTestClass
    {

        public AdvSearch_NoSelectFields_1FilterField_Returns57Rows()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction NoSelectFlds_StubAmtFilter_Success_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdvSearch_NoSelectFields_1FilterField_Returns57Rows));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition TotalRecordsEqual57;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition CheckTotal;
            this.NoSelectFlds_StubAmtFilter_SuccessData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            NoSelectFlds_StubAmtFilter_Success_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            TotalRecordsEqual57 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            CheckTotal = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            // 
            // NoSelectFlds_StubAmtFilter_Success_TestAction
            // 
            NoSelectFlds_StubAmtFilter_Success_TestAction.Conditions.Add(TotalRecordsEqual57);
            NoSelectFlds_StubAmtFilter_Success_TestAction.Conditions.Add(CheckTotal);
            resources.ApplyResources(NoSelectFlds_StubAmtFilter_Success_TestAction, "NoSelectFlds_StubAmtFilter_Success_TestAction");
            // 
            // TotalRecordsEqual57
            // 
            TotalRecordsEqual57.ColumnNumber = 1;
            TotalRecordsEqual57.Enabled = true;
            TotalRecordsEqual57.ExpectedValue = "57";
            TotalRecordsEqual57.Name = "TotalRecordsEqual57";
            TotalRecordsEqual57.NullExpected = false;
            TotalRecordsEqual57.ResultSet = 3;
            TotalRecordsEqual57.RowNumber = 1;
            // 
            // CheckTotal
            // 
            CheckTotal.ColumnNumber = 4;
            CheckTotal.Enabled = true;
            CheckTotal.ExpectedValue = "76903.58";
            CheckTotal.Name = "CheckTotal";
            CheckTotal.NullExpected = false;
            CheckTotal.ResultSet = 3;
            CheckTotal.RowNumber = 1;
            // 
            // NoSelectFlds_StubAmtFilter_SuccessData
            // 
            this.NoSelectFlds_StubAmtFilter_SuccessData.PosttestAction = null;
            this.NoSelectFlds_StubAmtFilter_SuccessData.PretestAction = null;
            this.NoSelectFlds_StubAmtFilter_SuccessData.TestAction = NoSelectFlds_StubAmtFilter_Success_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void NoSelectFlds_StubAmtFilter_Success()
        {
            SqlDatabaseTestActions testActions = this.NoSelectFlds_StubAmtFilter_SuccessData;
            //
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            try
            {
                //
                // Execute the test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
                SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            }
            finally
            {
                // Execute the post-test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
                SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
            }
        }
        private SqlDatabaseTestActions NoSelectFlds_StubAmtFilter_SuccessData;
    }
}
