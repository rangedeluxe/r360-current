﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvancedSearchUnitTest
{
    [TestClass()]
    public class SelectStubsAmt_SortStubsAmount_Return13rows : SqlDatabaseTestClass
    {

        public SelectStubsAmt_SortStubsAmount_Return13rows()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SelectStubsAmt_SortStubsAmt_Success_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectStubsAmt_SortStubsAmount_Return13rows));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.RowCountCondition ResultSetIs13Rows;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition ChecksAccountIs43825618;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition ReportTitleIsINVAmount;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition StubsAmount_01Is1809454;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SelectStubsAmt_SortStubsAmt_Success_PretestAction;
            this.SelectStubsAmt_SortStubsAmt_SuccessData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            SelectStubsAmt_SortStubsAmt_Success_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            ResultSetIs13Rows = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.RowCountCondition();
            ChecksAccountIs43825618 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            ReportTitleIsINVAmount = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            StubsAmount_01Is1809454 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            SelectStubsAmt_SortStubsAmt_Success_PretestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            // 
            // SelectStubsAmt_SortStubsAmt_Success_TestAction
            // 
            SelectStubsAmt_SortStubsAmt_Success_TestAction.Conditions.Add(ResultSetIs13Rows);
            SelectStubsAmt_SortStubsAmt_Success_TestAction.Conditions.Add(ChecksAccountIs43825618);
            SelectStubsAmt_SortStubsAmt_Success_TestAction.Conditions.Add(ReportTitleIsINVAmount);
            SelectStubsAmt_SortStubsAmt_Success_TestAction.Conditions.Add(StubsAmount_01Is1809454);
            resources.ApplyResources(SelectStubsAmt_SortStubsAmt_Success_TestAction, "SelectStubsAmt_SortStubsAmt_Success_TestAction");
            // 
            // ResultSetIs13Rows
            // 
            ResultSetIs13Rows.Enabled = true;
            ResultSetIs13Rows.Name = "ResultSetIs13Rows";
            ResultSetIs13Rows.ResultSet = 1;
            ResultSetIs13Rows.RowCount = 13;
            // 
            // ChecksAccountIs43825618
            // 
            ChecksAccountIs43825618.ColumnNumber = 16;
            ChecksAccountIs43825618.Enabled = true;
            ChecksAccountIs43825618.ExpectedValue = "43825618";
            ChecksAccountIs43825618.Name = "ChecksAccountIs43825618";
            ChecksAccountIs43825618.NullExpected = false;
            ChecksAccountIs43825618.ResultSet = 1;
            ChecksAccountIs43825618.RowNumber = 1;
            // 
            // ReportTitleIsINVAmount
            // 
            ReportTitleIsINVAmount.ColumnNumber = 4;
            ReportTitleIsINVAmount.Enabled = true;
            ReportTitleIsINVAmount.ExpectedValue = "[INV].Amount";
            ReportTitleIsINVAmount.Name = "ReportTitleIsINVAmount";
            ReportTitleIsINVAmount.NullExpected = false;
            ReportTitleIsINVAmount.ResultSet = 4;
            ReportTitleIsINVAmount.RowNumber = 1;
            // 
            // StubsAmount_01Is1809454
            // 
            StubsAmount_01Is1809454.ColumnNumber = 29;
            StubsAmount_01Is1809454.Enabled = true;
            StubsAmount_01Is1809454.ExpectedValue = "18094.54";
            StubsAmount_01Is1809454.Name = "StubsAmount_01Is1809454";
            StubsAmount_01Is1809454.NullExpected = false;
            StubsAmount_01Is1809454.ResultSet = 1;
            StubsAmount_01Is1809454.RowNumber = 13;
            // 
            // SelectStubsAmt_SortStubsAmt_Success_PretestAction
            // 
            resources.ApplyResources(SelectStubsAmt_SortStubsAmt_Success_PretestAction, "SelectStubsAmt_SortStubsAmt_Success_PretestAction");
            // 
            // SelectStubsAmt_SortStubsAmt_SuccessData
            // 
            this.SelectStubsAmt_SortStubsAmt_SuccessData.PosttestAction = null;
            this.SelectStubsAmt_SortStubsAmt_SuccessData.PretestAction = SelectStubsAmt_SortStubsAmt_Success_PretestAction;
            this.SelectStubsAmt_SortStubsAmt_SuccessData.TestAction = SelectStubsAmt_SortStubsAmt_Success_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void SelectStubsAmt_SortStubsAmt_Success()
        {
            SqlDatabaseTestActions testActions = this.SelectStubsAmt_SortStubsAmt_SuccessData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            try
            {
                // Execute the test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
                SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            }
            finally
            {
                // Execute the post-test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
                SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
            }
        }
        private SqlDatabaseTestActions SelectStubsAmt_SortStubsAmt_SuccessData;
    }
}
