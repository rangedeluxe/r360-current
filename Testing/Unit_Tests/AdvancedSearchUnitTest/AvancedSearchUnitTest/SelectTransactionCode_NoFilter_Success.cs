﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvancedSearchUnitTest
{
    [TestClass()]
    public class SelectTransactionCode_NoFilter_Success : SqlDatabaseTestClass
    {

        public SelectTransactionCode_NoFilter_Success()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SelectTranCode_NoFilter_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SelectTranCode_NoFilter_PretestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction SelectTranCode_NoFilter_PosttestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectTransactionCode_NoFilter_Success));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition TranCodeIs2197;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition CheckTotIs7693680;
            this.SelectTranCode_NoFilterData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            SelectTranCode_NoFilter_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            SelectTranCode_NoFilter_PretestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            SelectTranCode_NoFilter_PosttestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            TranCodeIs2197 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            CheckTotIs7693680 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            // 
            // SelectTranCode_NoFilterData
            // 
            this.SelectTranCode_NoFilterData.PosttestAction = SelectTranCode_NoFilter_PosttestAction;
            this.SelectTranCode_NoFilterData.PretestAction = SelectTranCode_NoFilter_PretestAction;
            this.SelectTranCode_NoFilterData.TestAction = SelectTranCode_NoFilter_TestAction;
            // 
            // SelectTranCode_NoFilter_TestAction
            // 
            SelectTranCode_NoFilter_TestAction.Conditions.Add(TranCodeIs2197);
            SelectTranCode_NoFilter_TestAction.Conditions.Add(CheckTotIs7693680);
            resources.ApplyResources(SelectTranCode_NoFilter_TestAction, "SelectTranCode_NoFilter_TestAction");
            // 
            // SelectTranCode_NoFilter_PretestAction
            // 
            resources.ApplyResources(SelectTranCode_NoFilter_PretestAction, "SelectTranCode_NoFilter_PretestAction");
            // 
            // SelectTranCode_NoFilter_PosttestAction
            // 
            resources.ApplyResources(SelectTranCode_NoFilter_PosttestAction, "SelectTranCode_NoFilter_PosttestAction");
            // 
            // TranCodeIs2197
            // 
            TranCodeIs2197.ColumnNumber = 14;
            TranCodeIs2197.Enabled = true;
            TranCodeIs2197.ExpectedValue = "2197";
            TranCodeIs2197.Name = "TranCodeIs2197";
            TranCodeIs2197.NullExpected = false;
            TranCodeIs2197.ResultSet = 1;
            TranCodeIs2197.RowNumber = 42;
            // 
            // CheckTotIs7693680
            // 
            CheckTotIs7693680.ColumnNumber = 4;
            CheckTotIs7693680.Enabled = true;
            CheckTotIs7693680.ExpectedValue = "76936.80";
            CheckTotIs7693680.Name = "CheckTotIs7693680";
            CheckTotIs7693680.NullExpected = false;
            CheckTotIs7693680.ResultSet = 3;
            CheckTotIs7693680.RowNumber = 1;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void SelectTranCode_NoFilter()
        {
            SqlDatabaseTestActions testActions = this.SelectTranCode_NoFilterData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            try
            {
                // Execute the test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
                SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            }
            finally
            {
                // Execute the post-test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
                SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
            }
        }
        private SqlDatabaseTestActions SelectTranCode_NoFilterData;
    }
}
