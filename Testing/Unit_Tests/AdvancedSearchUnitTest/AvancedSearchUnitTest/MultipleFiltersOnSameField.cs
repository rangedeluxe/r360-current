﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvancedSearchUnitTest
{
    [TestClass()]
    public class MultipleFiltersOnSameField : SqlDatabaseTestClass
    {

        public MultipleFiltersOnSameField()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction MutipleFiltersOnSameField_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultipleFiltersOnSameField));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition TotRecordsIs18;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition ChecksAcountRow1IS04258908;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition StubsAmountRow18Is44308;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition DocumentCountIs47;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition CheckTotalIs1094138;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction MutipleFiltersOnSameField_PretestAction;
            this.MutipleFiltersOnSameFieldData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            MutipleFiltersOnSameField_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            TotRecordsIs18 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            ChecksAcountRow1IS04258908 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            StubsAmountRow18Is44308 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            DocumentCountIs47 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            CheckTotalIs1094138 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            MutipleFiltersOnSameField_PretestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            // 
            // MutipleFiltersOnSameField_TestAction
            // 
            MutipleFiltersOnSameField_TestAction.Conditions.Add(TotRecordsIs18);
            MutipleFiltersOnSameField_TestAction.Conditions.Add(ChecksAcountRow1IS04258908);
            MutipleFiltersOnSameField_TestAction.Conditions.Add(StubsAmountRow18Is44308);
            MutipleFiltersOnSameField_TestAction.Conditions.Add(DocumentCountIs47);
            MutipleFiltersOnSameField_TestAction.Conditions.Add(CheckTotalIs1094138);
            resources.ApplyResources(MutipleFiltersOnSameField_TestAction, "MutipleFiltersOnSameField_TestAction");
            // 
            // TotRecordsIs18
            // 
            TotRecordsIs18.ColumnNumber = 1;
            TotRecordsIs18.Enabled = true;
            TotRecordsIs18.ExpectedValue = "18";
            TotRecordsIs18.Name = "TotRecordsIs18";
            TotRecordsIs18.NullExpected = false;
            TotRecordsIs18.ResultSet = 3;
            TotRecordsIs18.RowNumber = 1;
            // 
            // ChecksAcountRow1IS04258908
            // 
            ChecksAcountRow1IS04258908.ColumnNumber = 16;
            ChecksAcountRow1IS04258908.Enabled = true;
            ChecksAcountRow1IS04258908.ExpectedValue = "04258908";
            ChecksAcountRow1IS04258908.Name = "ChecksAcountRow1IS04258908";
            ChecksAcountRow1IS04258908.NullExpected = false;
            ChecksAcountRow1IS04258908.ResultSet = 1;
            ChecksAcountRow1IS04258908.RowNumber = 1;
            // 
            // StubsAmountRow18Is44308
            // 
            StubsAmountRow18Is44308.ColumnNumber = 29;
            StubsAmountRow18Is44308.Enabled = true;
            StubsAmountRow18Is44308.ExpectedValue = "443.08";
            StubsAmountRow18Is44308.Name = "StubsAmountRow18Is44308";
            StubsAmountRow18Is44308.NullExpected = false;
            StubsAmountRow18Is44308.ResultSet = 1;
            StubsAmountRow18Is44308.RowNumber = 18;
            // 
            // DocumentCountIs47
            // 
            DocumentCountIs47.ColumnNumber = 2;
            DocumentCountIs47.Enabled = true;
            DocumentCountIs47.ExpectedValue = "47";
            DocumentCountIs47.Name = "DocumentCountIs47";
            DocumentCountIs47.NullExpected = false;
            DocumentCountIs47.ResultSet = 3;
            DocumentCountIs47.RowNumber = 1;
            // 
            // CheckTotalIs1094138
            // 
            CheckTotalIs1094138.ColumnNumber = 4;
            CheckTotalIs1094138.Enabled = true;
            CheckTotalIs1094138.ExpectedValue = "10941.38";
            CheckTotalIs1094138.Name = "CheckTotalIs1094138";
            CheckTotalIs1094138.NullExpected = false;
            CheckTotalIs1094138.ResultSet = 3;
            CheckTotalIs1094138.RowNumber = 1;
            // 
            // MutipleFiltersOnSameField_PretestAction
            // 
            resources.ApplyResources(MutipleFiltersOnSameField_PretestAction, "MutipleFiltersOnSameField_PretestAction");
            // 
            // MutipleFiltersOnSameFieldData
            // 
            this.MutipleFiltersOnSameFieldData.PosttestAction = null;
            this.MutipleFiltersOnSameFieldData.PretestAction = MutipleFiltersOnSameField_PretestAction;
            this.MutipleFiltersOnSameFieldData.TestAction = MutipleFiltersOnSameField_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void MutipleFiltersOnSameField()
        {
            SqlDatabaseTestActions testActions = this.MutipleFiltersOnSameFieldData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            try
            {
                // Execute the test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
                SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            }
            finally
            {
                // Execute the post-test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
                SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
            }
        }
        private SqlDatabaseTestActions MutipleFiltersOnSameFieldData;
    }
}
