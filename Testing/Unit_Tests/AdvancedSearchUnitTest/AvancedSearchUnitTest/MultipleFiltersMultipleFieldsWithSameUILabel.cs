﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AvancedSearchUnitTest
{
    [TestClass()]
    public class MultipleFiltersMultipleFieldsWithSameUILabel : SqlDatabaseTestClass
    {

        public MultipleFiltersMultipleFieldsWithSameUILabel()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction MultFiltersMultFieldsWithSameUILabel_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.RowCountCondition RowCountIs18;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction MultFiltersMultFieldsWithSameUILabel_PretestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction MultFiltersMultFieldsWithSameUILabel_PosttestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MultipleFiltersMultipleFieldsWithSameUILabel));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition TwoINVAmountColumnsExist;
            this.MultFiltersMultFieldsWithSameUILabelData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            MultFiltersMultFieldsWithSameUILabel_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            RowCountIs18 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.RowCountCondition();
            MultFiltersMultFieldsWithSameUILabel_PretestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            MultFiltersMultFieldsWithSameUILabel_PosttestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            TwoINVAmountColumnsExist = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            // 
            // MultFiltersMultFieldsWithSameUILabelData
            // 
            this.MultFiltersMultFieldsWithSameUILabelData.PosttestAction = MultFiltersMultFieldsWithSameUILabel_PosttestAction;
            this.MultFiltersMultFieldsWithSameUILabelData.PretestAction = MultFiltersMultFieldsWithSameUILabel_PretestAction;
            this.MultFiltersMultFieldsWithSameUILabelData.TestAction = MultFiltersMultFieldsWithSameUILabel_TestAction;
            // 
            // MultFiltersMultFieldsWithSameUILabel_TestAction
            // 
            MultFiltersMultFieldsWithSameUILabel_TestAction.Conditions.Add(RowCountIs18);
            MultFiltersMultFieldsWithSameUILabel_TestAction.Conditions.Add(TwoINVAmountColumnsExist);
            resources.ApplyResources(MultFiltersMultFieldsWithSameUILabel_TestAction, "MultFiltersMultFieldsWithSameUILabel_TestAction");
            // 
            // RowCountIs18
            // 
            RowCountIs18.Enabled = true;
            RowCountIs18.Name = "RowCountIs18";
            RowCountIs18.ResultSet = 1;
            RowCountIs18.RowCount = 18;
            // 
            // MultFiltersMultFieldsWithSameUILabel_PretestAction
            // 
            resources.ApplyResources(MultFiltersMultFieldsWithSameUILabel_PretestAction, "MultFiltersMultFieldsWithSameUILabel_PretestAction");
            // 
            // MultFiltersMultFieldsWithSameUILabel_PosttestAction
            // 
            resources.ApplyResources(MultFiltersMultFieldsWithSameUILabel_PosttestAction, "MultFiltersMultFieldsWithSameUILabel_PosttestAction");
            // 
            // TwoINVAmountColumnsExist
            // 
            TwoINVAmountColumnsExist.ColumnNumber = 4;
            TwoINVAmountColumnsExist.Enabled = true;
            TwoINVAmountColumnsExist.ExpectedValue = "[INV].Amount";
            TwoINVAmountColumnsExist.Name = "TwoINVAmountColumnsExist";
            TwoINVAmountColumnsExist.NullExpected = false;
            TwoINVAmountColumnsExist.ResultSet = 4;
            TwoINVAmountColumnsExist.RowNumber = 2;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void MultFiltersMultFieldsWithSameUILabel()
        {
            SqlDatabaseTestActions testActions = this.MultFiltersMultFieldsWithSameUILabelData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            try
            {
                // Execute the test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
                SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            }
            finally
            {
                // Execute the post-test script
                // 
                System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
                SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
            }
        }
        private SqlDatabaseTestActions MultFiltersMultFieldsWithSameUILabelData;
    }
}
