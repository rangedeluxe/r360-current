﻿--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ClientAccount_Search
(
	@parmSearchRequest	XML,
	@parmSearchTotals	XML OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/18/2009
*
* Purpose: Query Data Entry Details fact table for matching records
*
* Modification History
* 04/18/2009 CR 25817 JPB	Created
* 10/16/2009 CR 27952 JPB	Added = to Check Amount compares.
* 10/16/2009 CR 27953 JPB	Added Check Number filter back in. 
* 10/16/2009 CR 27954 JPB	Fixed sort direction.
* 10/16/2009 CR 27801 JPB	Advanced Filters (<WHERE>) not working correctly.
* 02/22/2010 CR 29079 JPB	Should only return rows <= the 
*							SiteCodes.CurrentProcessingDate for the Lockbox 
*							being searched.
* 03/11/2010 CR 29180 JPB	Changed initial select/update to just a select.
*							(Performance improvement.)
* 03/22/2010 CR 29240 JPB	Updated to fix ambiguous column name error.
* 04/13/2010 CR 29292 JPB	Re-write to improve performance.
* 04/13/2010 CR 29345 JPB	When a column is listed in the advanced selection and
*							the selection list, an error was thrown.
* 06/23/2010 CR 29960 JPB	**Returning too many rows when no data entry are requested,
*							causing the check amount total and check count to be
*							incorrect.
*							**Viewing Days not working correct. Now get the value from
*							from the dimLockboxView so the most current setting
*							is used.
*							**A sort was not being done on the result set 
*							returned to the application.
* 08/23/2010 CR 30451 JPB	Major overall to correct check/de combining and compares.
* 08/25/2010 CR 30452 JPB	Fixed compares and data converisons, added code
*			 CR 30386 JPB	from CR 29961 View Days correction. (This was done
*							in 1.00.06.02, this code is reverted from 1.00.04.00.
* 09/25/2010 CR 31163 JPB	Application timing out when check amount was added to
*							the advanced filter. Also discoverd that the data type
*							was not properly account for on the check/stubs advanced
*							filter options.
* 12/23/2010 CR 32159 JMC	Modified search to be able to handle values in 
*							olta.factDataEntryDetails.DataEntryValue that contain
*							ticks (').
* 01/03/2011 CR 32189 JPB	Fixed dimRemitters table misspelling.
* 10/28/2011 CR 47367 JPB	Speed improvements.
* 02/09/2012 CR 51364 MEL	Speed improvements.
* 03/25/2012 CR 51512 JPB	Corrected CheckCount return value. Only delete non
*							matching records from temp table when DE included in
*							WHERE.
* 03/29/2012 CR 51640 WJS	Change DimRemitter from INNER JOIN To LEFT JOIN
* 03/29/2012 CR 51648 WJS	Allowed other checks field to display not just remittername only one excluded is checkSequence
* 04/01/2012 CR 51681 WJS   Make it so stubs.amount is now stored in a bit field called StubsAmountOccured. This will allow us to look for field in money type,
*							 but allow inner join on factDataEntryDetails to still work
*							 Always return remitter name as ChecksRemitterName, this allows to display on UI
* 04/03/2012 CR 51782 WJS	Need to add to sort order/1.03.01 FP items.
* 04/06/2012 CR 51912 JNE	Add missing recompile option in SQL Command.
* 04/18/2012 CR 52171 JPB	Not all DE Fields displayed. (FP:52170)
* 04/30/2012 CR 52389 JPB	Stub.Amount values not always inlcuded in the result set.
* 08/10/2012 CR 54159 JPB	Added BatchNumber support.
* 12/05/2012 WI 70675 JPB	Added Deposit Date Key for factDataEntryDetail query (FP:CR 56462)
* 12/05/2012 WI 70980 JPB	Update lockbox search order to be more discrete (FP:CR 56220)
* 12/05/2012 WI 71128 JPB	Same column name in result set (FP:70079)
* 05/14/2013 WI 90758 JBS	Update to 2.0 release.  Change Schema to RecHubData
*							Renamed proc From usp_LockBoxSearch
*							Renamed SiteCode to SiteCodeID
*							Change references:  LockBox to ClientAccount,
*							Customer to Organization, Processing to Immutable.
*							Removed references to dimRemitters table and replaced with factChecks columns
*							FP 110058.
* 06/08/2013 WI 90081 JPB	Added Payment Source and Payment Type to result set.
* 08/30/2013 WI 112726 CEJ	Alter usp_ClientAccount_Search to return payment source and payment type long name
* 11/05/2013 WI 121416 JPB	factChecks.NumericRoutingNumber not included in #tmpMatchingResults insert statement.
* 11/21/2013 WI 123202 JBS	Remove check on @iProcessDEData > 0 as a determining factor to build.
*							If no rows match selected dataEntry columns we still want to create Results for request columns.
* 12/17/2013 WI 125457 JBS	Changed Where clauses to only exclude reserved names from the 
*							Checks table to return all expected columns in result set
* 03/18/2014 WI 133284 JBS	Add DDA as optional input for Filtering and returning column
* 06/25/2014 WI 142872 JBS	Adding RAAM logic.  Adding SessionID to XML.  Removing obsolete variables and logic.
*							Changed BatchID to BIGINT and BatchSourceKey to SMALLINT
* 07/09/2014 WI 142872 KLC	Changed from LogonEntityID to workgroup's EntityID
* 07/18/2014 WI 142872 JBS	Corrected truncation of DDA output
* 08/19/2014 WI 159559 JMC	Modified to ORDER BY SourceBatchID instead of BatchID
* 08/19/2014 WI	159573 JBS	Remove need for Table name to determine DDA input.
* 09/23/2014 WI	167521 SAS	Added ImportTypeShortName column
* 09/29/2014 WI 167088 CEJ	Change RecHubData.usp_ClientAccount_Search to take OLWorkGroupID rather than OLClientAccountKey
* 09/30/2014 WI	167521 SAS	Added comma after ImportTypeShortName column
* 10/24/2014 WI	174074 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175149 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/07/2014 WI 176350 SAS  Add BatchSourceShortName and ImportTypeShortName to resultset.  		
*							Removed BatchSourceName
* 11/18/2014 WI 178429 JPB	Use 256 for DE alphnumeric fields instead of FldLength.
* 11/20/2014 WI 179166 JBS  Added support for Bracketed Title on DisplayName. FIX Bug 179048 Add in MarkSense Logic
* 12/16/2014 WI 181977 CEJ	Change usp_ClientAccount_Search to include $0.00 payments in the payment count
* 12/17/2014 WI 182353 CEJ	Fix usp_ClientAccount_Search to consistently remove table prefix before comparing Display Names
* 03/05/2015 WI 194258 JBS	Add brackets to DE Column names to handle special characters.
* 03/25/2015 WI 194934 JBS  Added View Ahead logic.  ALso made some corrections to the Bracketing logic where the Where clause
*							comes into play.  Also other fine tuning and corrections.
* 04/29/2015 WI 200581 JBS  Handle DataType 6 input the same as Amount column for Checks and Stub search logic.  
* 04/14/2015 WI 200583 JSF  Bug 194518:R360 User Activity Report - Advanced Search - Search parameters not in audit message
* 05/11/2015 WI 212416 BLR  Removed audit in this SP. We're auditing in the code now.
* 05/20/2015 WI 71714  JBS	Adding Stub Sequence to final sorting.  Also adding in check to not send back DisplayFinal items. Also Bug 216145 typo
* 05/22/2015 WI 213687 MGE  FP Add code to usp_LockboxSearch to include columns with the same display name but different fldnames   Bug 202574
* 06/02/2015 WI 213687 JBS	Bug 216651. Correcting if Where clause fields are empty so we don't build bad DE fields
* 06/08/2015 WI 213687 JBS	Bug 217280. Add in missing StubsBatchSequence loading.
* 07/24/2015 WI 224359 JBS	Adding logic to add in dimWorkgroupDataEntryColumns and remove dimDataEntryColumn and ClientAccountDataEntry
* 07/29/2015 WI 224359 MGE	Updating Joe's work to change two where clause from AND to OR in the section that inserts into #tmpDEResultsPivot
* 08/10/2015 WI 227836 MGE	Updated search to return UILable instead of SourceDisplayName
* 08/12/2015 WI 227832 MGE  As of this change the stored procedure picks up fields with names = the UILable name chosen but still returns separate fields to UI.
* 08/20/2015 WI 227832 MGE	Changes to allow a single Where clause for PMT feilds to 1) not error out, and to properly filter the rows.
* 08/21/2015 WI 230304 MGE  Fix issue with multiple columns with same UILabel not showing consistently
* 08/25/2015 WI 232008 MGE	Fix issue with where clause being applied at fldname level instead of UILabel
* 08/26/2015 WI 232008 MGE	Fix issue whth where clause when column not included in select fields
* 08/26/2015 WI 232008 MGE	Adjusting for some things I didn't get into source correctly (one was a cut & paste error)
* 09/04/2015 WI 234038 MGE	Tuning to prevent timeouts at Band of Edwardsville (applicable generally)
* 09/09/2015 WI 234681 MGE	Corrections to calculations for result too large  (Bug 234589)
* 09/11/2015 WI 234818 MGE	Corrections to the number of lines being display when there are no stub fields selected (Bug 234590) 
* 09/15/2015 WI 235438 MGE  Correction to BatchPaymentSource and sorts.  Bug 234887
* 09/16/2015 WI 236049 MGE	Moved join to get batch source short name and import type short name to final query.
* 09/29/2015 WI 238222 TWE  Filter by BatchSourceKey failed
* 10/05/2015 WI 238920 MGE	Fix DEWhere logic when there are multiple FldNames for same UILabel and more than 1 operator on the same UILabel
* 10/08/2015 WI 240055 MGE  Fix CheckSequence (aka payment sequence) filter; force recompile of insert into #tmpDEResultsPivot.
* 10/13/2015 WI 240625 MGE	Fix sorting of DEWhere for the construction of Delete from #tmpDEResultsPivot statement
* 11/19/2015 WI 246528 MGE	Fix so factChecks.TransactionCode will display data in the search results 
***********************************************************************************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @CheckSearch			VARCHAR(MAX),
		@SelectColumns			VARCHAR(MAX),
		@SQLSearchCommand		VARCHAR(MAX),
		@SQLCommand				VARCHAR(MAX),
		@TransactionTable		VARCHAR(35),
		@dtDepositDateStart		DATETIME,
		@dtDepositDateEnd		DATETIME,
		@dtDepositDateKeyStart	INT,			--CR 51364
		@dtDepositDateKeyStop	INT,			--CR 51364
		@iSourceBatchIDFrom		BIGINT,
		@iSourceBatchIDTo		BIGINT,
		@iBatchNumberFrom		INT,
		@iBatchNumberTo			INT,
		@BatchPaymentTypeKey	TINYINT,
		@BatchSourceKey			SMALLINT,
		@vcAmountFrom			VARCHAR(100),
		@vcAmountTo				VARCHAR(100),
		@vcSessionID			VARCHAR(40),
 		@vcClientAccountID		INT,
		@vcSerialNumber			VARCHAR(30),
		@vcPaginateRS			VARCHAR(5),
		@iPaginateRS			SMALLINT,
		@iRecordsPerPage		INT,
		@iStartRecord			INT,
		@vcSortBy				VARCHAR(512),
		@vcSortDirection		VARCHAR(4),
		@vcSortDisplayName		VARCHAR(64),
		@iDisplayScannedChecks	BIT,
		@iDisplayCOTSOnly		BIT,
		@vcDEColumnNames		VARCHAR(MAX),
		@vcDBColumnsWithCast	VARCHAR(MAX),	/* new for fixed data typing */
		@iDEColumnCount			INT,
		@DEInSort				BIT,			/* CR 51782 JPB 04/05/12 */
		@dtCurrentProcessingDate	DATETIME,		--CR 29079
		--used for pivoting/merging records
		@iProcessDEData				INT,
		@iCurrentCheckRecordID		INT,
		@iCurrentBankKey			INT,
		@iCurrentOrganizationKey	INT,
		@iCurrentClientAccountKey	INT,
		@iCurrentImmutableDateKey	INT,
		@iCurrentDepositDateKey		INT,
		@iCurrentBatchID			BIGINT,
		@iCurrentTransactionID		INT,
		@iDETransactionCounter		INT,
		@iMaxDEInfoItems			INT,
		@iSRRecordCount				INT,
		@iSRDocumentCount			INT,
		@iSRCheckCount				INT,
		@mSRCheckTotal				MONEY,
		@SQLWhereCommand			VARCHAR(MAX),
		@SQLFillTmpDims				VARCHAR(MAX),		-- CR 51364
		@Loop						INT,
		@DEWhereCount				INT,
		@MatchingUILabelCount		INT,				-- WI 232008
		@SavedDisplayName			VARCHAR(270),		-- WI 232008
		@SavedOperator				VARCHAR(32),		-- WI 238920
		@SavedDataValue				VARCHAR(80),		-- WI 238920	
		@StubsDEColumnCount			INT,				-- Bug 234590
		@TooManyRows				BIT,
		@factDataEntryDetailsMaxRowCount INT,
		@totalMatchRowCount				BIGINT,
		@totalMatchMaxRowCount			BIGINT,
		@totalMatchWithDEMaxRowCount	BIGINT,
		@ActualMatchRows				BIGINT,
		--used to determine 'base' column names
		@vcCheckAmountCN		VARCHAR(30),
		@vcCheckSerialNumberCN	VARCHAR(30),
		@vcCheckAccountNumberCN VARCHAR(30),
		@vcCheckRTCN			VARCHAR(30),
		@bMarkSense					BIT,
		@Today					VARCHAR(10);			-- CR 51364

BEGIN TRY

	SET @Today = CONVERT(VARCHAR,GETDATE(),101); -- Function is nondeterministic so save as variable and use variable.
	/* max number rows that will be processed */
	SET @factDataEntryDetailsMaxRowCount = 480000;	--Value has been set to 80 thousand
	SET @totalMatchMaxRowCount = 200000;
	SET @totalMatchWithDEMaxRowCount = 2560000000;  -- Value has been set to 560 million

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults')) 
		DROP TABLE #tmpMatchingResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults_TEMP')) 
		DROP TABLE #tmpMatchingResults_TEMP;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDEResults')) 
		DROP TABLE #tmpDEResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDEResultsPivot')) 
		DROP TABLE #tmpDEResultsPivot;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#pivotCheckInfo')) 
		DROP TABLE #pivotCheckInfo;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#pivotStubInfo')) 
		DROP TABLE #pivotStubInfo;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEInfoFields')) 
		DROP TABLE #DEInfoFields;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEInfoFieldsExpanded')) 
        DROP TABLE #DEInfoFieldsExpanded
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEWhere')) 
		DROP TABLE #DEWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#ExpandedDEWhere')) 
		DROP TABLE #ExpandedDEWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#OrderedExpandedDEWhere')) 
		DROP TABLE #OrderedExpandedDEWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDimensions')) 
		DROP TABLE #tmpDimensions;

	--Create a temp table to hold the identifing information for the records that match
	CREATE TABLE #tmpMatchingResults
	(
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchSourceKey		SMALLINT, -- WI 90081
		BatchPaymentTypeKey	TINYINT, -- WI 90081
		BatchID				BIGINT,		-- WI 142872
		SourceBatchID		BIGINT,		-- WI 142872
		BatchNumber			INT,
		TransactionID		INT,
		TxnSequence			INT,
		CheckCount			INT,
		DocumentCount		INT,
		StubCount			INT,
		OMRCount			INT,
		CheckAmount			MONEY,
		BankID				INT,
		ClientAccountID		INT,
		SerialNumber		VARCHAR(30),
		numeric_serial		BIGINT,
		BatchSequence		INT,
		StubsBatchSequence	INT,			-- WI 71714
		ChecksCheckSequence INT,
		StubsStubSequence	INT,			-- WI 71714
		RT					VARCHAR(30),
		numeric_rt			BIGINT,
		AccountNumber		VARCHAR(30),
		RemitterName		VARCHAR(60),
		Account				VARCHAR(30),
		RoutingNumber		VARCHAR(30),
		DDA					VARCHAR(35),
		BatchSourceShortName VARCHAR(30),
		ImportTypeShortName  VARCHAR(30),
		TransactionCode		VARCHAR(30)
	);
		
	CREATE CLUSTERED INDEX IDX_#tmpMatchingResults ON #tmpMatchingResults
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID);

		--Create a temp table to hold the identifing information for the records that match
	CREATE TABLE #tmpMatchingResults_TEMP
	(
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchSourceKey		SMALLINT, -- WI 90081
		BatchPaymentTypeKey	TINYINT, -- WI 90081
		BatchID				BIGINT,		-- WI 142872
		SourceBatchID		BIGINT,		-- WI 142872
		BatchNumber			INT,
		TransactionID		INT,
		TxnSequence			INT,
		CheckCount			INT,
		DocumentCount		INT,
		StubCount			INT,
		OMRCount			INT,
		CheckAmount			MONEY,
		BankID				INT,
		ClientAccountID		INT,
		SerialNumber		VARCHAR(30),
		numeric_serial		BIGINT,
		BatchSequence		INT,
		StubsBatchSequence	INT,			-- WI 71714
		ChecksCheckSequence INT,
		--StubsStubSequence	INT,			-- WI 71714  MGE Took out since it is also defined below
		RT					VARCHAR(30),
		numeric_rt			BIGINT,
		AccountNumber		VARCHAR(30),
		RemitterName		VARCHAR(60),
		Account				VARCHAR(30),
		RoutingNumber		VARCHAR(30),
		StubsStubSequence	INT,
		DDA					VARCHAR(35),
		BatchSourceShortName VARCHAR(30),
		ImportTypeShortName  VARCHAR(30),
		TransactionCode		VARCHAR(30)
	);
		
	CREATE CLUSTERED INDEX IDX_#tmpMatchingResults_TEMP ON #tmpMatchingResults_TEMP
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,TxnSequence,BatchSequence);

	--Create a temp table to hold the matched data entry records
	CREATE TABLE #tmpDEResults
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				BIGINT,
		SourceBatchID		BIGINT,
		TransactionID		INT,
		BatchSequence		INT,
		StubSequence		INT,			-- WI 71714
		TableName			VARCHAR(36),
		BatchSourceKey		SMALLINT,
		FldName				VARCHAR(140),	-- WI 194258
		DataType			INT,
		StubsAmountOccured	BIT,
	--	FldLength			INT,
		DataEntryValue		VARCHAR(80),
		WorkgroupDataEntryColumnKey	BIGINT,
		DataEntryValueMoney MONEY,
		DataEntryValueDateTime DATETIME
	);
	SET @SQLCommand = 'ALTER TABLE #tmpDEResults ADD CONSTRAINT PK_#tmpDEResults_' + REPLACE(CAST(NEWID() AS VARCHAR(36)),'-','_') + ' PRIMARY KEY NONCLUSTERED (RowID)'
	EXEC (@SQLCommand);

	CREATE CLUSTERED INDEX IDX_#tmpDEResults ON #tmpDEResults
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID);

	--Create a temp table to hold the matched data entry records
	CREATE TABLE #tmpDEResultsPivot
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				INT,
		TransactionID		INT,
		BatchSequence		INT, 
		StubSequence		INT		-- WI 71714
	);
	SET @SQLCommand = 'ALTER TABLE #tmpDEResultsPivot ADD CONSTRAINT PK_#tmpDEResultsPivot_' + REPLACE(CAST(NEWID() AS VARCHAR(36)),'-','_') + ' PRIMARY KEY NONCLUSTERED (RowID)'
	EXEC (@SQLCommand);

	CREATE CLUSTERED INDEX IDX_#tmpDEResultsPivot ON #tmpDEResultsPivot
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,BatchSequence);

	CREATE TABLE #pivotCheckInfo
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				INT,
		TransactionID		INT,
		BatchSequence		INT, 
		StubSequence		INT		-- WI 71714
	);
	SET @SQLCommand = 'ALTER TABLE #pivotCheckInfo ADD CONSTRAINT PK_#pivotCheckInfo_' + REPLACE(CAST(NEWID() AS VARCHAR(36)),'-','_') + ' PRIMARY KEY NONCLUSTERED (RowID)'
	EXEC (@SQLCommand);

	CREATE CLUSTERED INDEX IDX_#pivotCheckInfo ON #pivotCheckInfo
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,BatchSequence);

	CREATE TABLE #pivotStubInfo
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				INT,
		TransactionID		INT,
		BatchSequence		INT, 
		StubSequence		INT		-- WI 71714
	);
	SET @SQLCommand = 'ALTER TABLE #pivotStubInfo ADD CONSTRAINT PK_#pivotStubInfo_' + REPLACE(CAST(NEWID() AS VARCHAR(36)),'-','_') + ' PRIMARY KEY NONCLUSTERED (RowID)'
	EXEC (@SQLCommand);
	CREATE CLUSTERED INDEX IDX_#pivotStubInfo ON #pivotStubInfo
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID);	-- WI 71714 removed BatchSequence
	
	CREATE TABLE #DEInfoFields 
	(
		RowID				INT IDENTITY(1,1) NOT NULL,			
		TableType			BIT,
		TableName			VARCHAR(36),
		FldName				VARCHAR(265),		-- WI 194258
		FinalName			VARCHAR(270),
		DisplayName			VARCHAR(256),
		OrgDisplayName		VARCHAR(256),		
		DisplayFinal		BIT DEFAULT(1),
		StubsAmountOccured	BIT,
		DataType			SMALLINT,
		BatchSourceKey		SMALLINT,
		WorkgroupDataEntryColumnKey	BIGINT
	);

	CREATE CLUSTERED INDEX IDX_#DEInfoFields ON #DEInfoFields
	(TableName,FldName);
	
	CREATE TABLE #DEInfoFieldsExpanded
	(
		RowID			INT IDENTITY(1,1) NOT NULL,			
		TableType		BIT,
		TableName		VARCHAR(36),
		FldName			VARCHAR(265),	-- WI 194258
		FinalName		VARCHAR(270),
		DisplayName		VARCHAR(256),
		DisplayFinal	BIT DEFAULT(1),
		DataType		SMALLINT,
		BatchSourceKey	SMALLINT,
		WorkgroupDataEntryColumnKey	BIGINT,
		ClientAccountKey	INT,
		StubsAmountOccured	BIT
	);
	
	CREATE CLUSTERED INDEX IDX_#DEInfoFieldsExpanded ON #DEInfoFieldsExpanded
	(TableName,FldName,WorkgroupDataEntryColumnKey);
	
	CREATE TABLE #DEWhere
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		TableType			BIT,
		TableName			VARCHAR(36),
		ColumnName			VARCHAR(265),
		DisplayName			VARCHAR(265),
		CombinedName		VARCHAR(270),
		Operator			VARCHAR(32),
		BatchSourceKey		SMALLINT,
		StubsAmountOccured	BIT,
		DataType			INT,
		DataValue			VARCHAR(80)
	);

	CREATE CLUSTERED INDEX IDX_#DEWhere ON #DEWhere
	(CombinedName);

	CREATE TABLE #ExpandedDEWhere
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		TableType			BIT,
		TableName			VARCHAR(36),
		ColumnName			VARCHAR(265),
		DisplayName			VARCHAR(265),
		CombinedName		VARCHAR(270),
		Operator			VARCHAR(32),
		BatchSourceKey		SMALLINT,
		StubsAmountOccured	BIT,
	 	DataType			INT,
		DataValue			VARCHAR(80)
	);

	CREATE CLUSTERED INDEX IDX_#DEWhere ON #ExpandedDEWhere
	(CombinedName);

	CREATE TABLE #OrderedExpandedDEWhere
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		TableType			BIT,
		TableName			VARCHAR(36),
		ColumnName			VARCHAR(265),
		DisplayName			VARCHAR(265),
		CombinedName		VARCHAR(270),
		Operator			VARCHAR(32),
		BatchSourceKey		SMALLINT,
		StubsAmountOccured	BIT,
	 	DataType			INT,
		DataValue			VARCHAR(80)
	);

	CREATE CLUSTERED INDEX IDX_#DEWhere2 ON #OrderedExpandedDEWhere
	(Operator, DataValue, CombinedName);

	--Create temp table to store dimensional data
	CREATE TABLE #tmpDimensions
	(
		SiteBankID			INT,   
		EntityID			INT,
		SiteClientAccountID	INT, 
		ClientAccountKey	INT, 
		SiteCodeID			INT,
		LBMostRecent		BIT,
		StartDateKey		INT,
		SiteCurProcDateKey	INT,
		ViewAhead			BIT
	);


	SET @vcDEColumnNames = '';
	SET @vcDBColumnsWithCast = '';

	/* CR 51782 JPB 04/05/12 Set DEInSort if de fields in sort clause */
	IF EXISTS( 
				SELECT	1 
				FROM	@parmSearchRequest.nodes('/Root/SortBy') SearchRequest(att) 
				WHERE	SearchRequest.att.value('.','varchar(512)') LIKE 'Checks%'				--  TODO Make sure we do not need to add in our reserved column names like RT etc to exclude.
						OR SearchRequest.att.value('.','varchar(512)') LIKE 'Stubs%'
						OR SearchRequest.att.value('.','varchar(512)') = 'Stubs.Amount'
			)
		SET @DEInSort = 1
	ELSE SET @DEInSort = 0;
	/* CR 51782 JPB 04/05/12 Set DEInSort if de fields in sort clause */

		--setup the where clause table for de fields
	INSERT INTO #DEWhere 
	(
		TableType,
		TableName,
		ColumnName,
		CombinedName,
		Operator,
		BatchSourceKey,
		DataValue,
		DataType,
		StubsAmountOccured,
		DisplayName
	)
	SELECT 
		SR.TableType,
		SR.TableName,
		SR.FieldName,
		SR.TableName+SR.FieldName,
		SR.Operator,
		SR.BatchSourceKey,
		SR.DEValue,
		SR.DataType,
		SR.StubsAmountOccured,
		SR.DisplayName
	FROM	(
			SELECT	
				CASE UPPER(SearchRequest.att.value('@tablename', 'varchar(20)'))
												WHEN 'CHECKS' THEN 1
					ELSE 0
				END AS TableType,
				SearchRequest.att.value('@tablename', 'varchar(20)') AS TableName,
				CASE   -- UPPER(SearchRequest.att.value('@fieldname', 'varchar(256)'))
					WHEN UPPER(SearchRequest.att.value('@fieldname', 'varchar(256)')) IN ('AMOUNT', 'RT', 'ACCOUNT', 'SERIAL') 
						AND UPPER(SearchRequest.att.value('@tablename', 'varchar(20)')) = 'CHECKS' 
						AND SearchRequest.att.value('@batchsourcekey', 'smallint') = 255  THEN SearchRequest.att.value('@fieldname', 'varchar(256)')
						ELSE
						'['+SearchRequest.att.value('@fieldname', 'varchar(256)')+']'
					END AS FieldName,
					SearchRequest.att.value('@operator', 'varchar(20)') as Operator,
				SearchRequest.att.value('@batchsourcekey', 'smallint') as BatchSourceKey,
					SearchRequest.att.value('@value', 'varchar(128)') as DEValue,
					SearchRequest.att.value('@datatype', 'int') AS DataType,
					CASE 
					WHEN SearchRequest.att.value('@tablename', 'varchar(128)') = 'Stubs' AND UPPER(SearchRequest.att.value('@fieldname', 'varchar(128)')) = 'AMOUNT' THEN 1
						ELSE 0
					END AS StubsAmountOccured,
				SearchRequest.att.value('@reporttitle', 'varchar(256)') AS DisplayName				
			FROM	
				@parmSearchRequest.nodes('/Root/WhereClause/field') SearchRequest(att)
			WHERE 
				SearchRequest.att.value('@datatype', 'int') <> -1	--- This takes care of bogus empty rows in the Where clause
				AND SearchRequest.att.value('@batchsourcekey', 'smallint') <> 255   -- Eliminates reserved fields
		) SR ;
	SET @DEWhereCount = @@ROWCOUNT;


	--set the column names that we will use going forward
	IF EXISTS(SELECT 1 FROM @parmSearchRequest.nodes('/Root/SelectFields/field') SearchRequest(att) )
	BEGIN
		SELECT	@vcCheckAmountCN = 'ChecksAmount',
				@vcCheckSerialNumberCN = 'ChecksSerial',
				@vcCheckAccountNumberCN = 'ChecksAccount',
				@vcCheckRTCN = 'ChecksRT';
	END
	ELSE
	BEGIN
		SELECT	@vcCheckAmountCN = 'Amount',
				@vcCheckSerialNumberCN = 'SerialNumber',
				@vcCheckAccountNumberCN = 'AccountNumber',
				@vcCheckRTCN = 'RT';
	END

	----Get the filter parameters, this will be used in the search WHERE clause
	SELECT	@dtDepositDateStart = SR.DepositDateStart,
			@dtDepositDateEnd = SR.DepositDateEnd,
			@iSourceBatchIDFrom = SR.BatchIDFrom,
			@iSourceBatchIDTo = SR.BatchIDTo,
			@iBatchNumberFrom = SR.BatchNumberFrom,
			@iBatchNumberTo = SR.BatchNumberTo,
			@vcAmountFrom = SR.AmountFrom,
			@vcAmountTo = SR.AmountTo,
			@vcSessionID = SR.SessionID,
			@vcClientAccountID = SR.ClientAccountID,
			@vcSerialNumber = SR.SerialNumber,
			@iRecordsPerPage = SR.RecordsPerPage,
			@vcPaginateRS = SR.PaginateRS,
			@iStartRecord = StartRecord,
			@vcSortBy = SortBy,
			@vcSortDisplayName = COALESCE(SortDisplayName,''),
			@vcSortDirection = SortDirection,
			@iDisplayScannedChecks = DisplayScannedChecks,
			@iDisplayCOTSOnly = DisplayCOTSOnly,
			@bMarkSense = MarkSense,				-- WI 179166
			@BatchPaymentTypeKey = 
				CASE 
					WHEN LEN(SR.BatchPaymentTypeKey) > 0 THEN CAST(SR.BatchPaymentTypeKey AS TINYINT)
					ELSE 255
			END,
			@BatchSourceKey = 
				CASE 
					WHEN LEN(SR.BatchSourceKey) > 0 THEN CAST(SR.BatchSourceKey AS SMALLINT)
					ELSE 255		-- This will Equate to ALL BatchSources
			END
	FROM	(
				SELECT	SearchRequest.att.query('DateFrom').value('.', 'datetime') AS DepositDateStart,
						SearchRequest.att.query('DateTo').value('.', 'datetime') AS DepositDateEnd,
						SearchRequest.att.query('BatchIDFrom').value('.', 'bigint') AS BatchIDFrom,
						SearchRequest.att.query('BatchIDTo').value('.', 'bigint') AS BatchIDTo,
						SearchRequest.att.query('BatchNumberFrom').value('.', 'int') AS BatchNumberFrom,
						SearchRequest.att.query('BatchNumberTo').value('.', 'int') AS BatchNumberTo,
						REPLACE(SearchRequest.att.query('AmountFrom').value('.', 'varchar(100)'),',','') AS AmountFrom,
						REPLACE(SearchRequest.att.query('AmountTo').value('.', 'varchar(100)'),',','') AS AmountTo,
						SearchRequest.att.query('ClientAccountID').value('.', 'int') AS ClientAccountID,
						SearchRequest.att.query('SessionID').value('.', 'varchar(40)') AS SessionID,
						SearchRequest.att.query('Serial').value('.', 'varchar(40)') AS SerialNumber,
						SearchRequest.att.query('RecordsPerPage').value('.','int') AS RecordsPerPage,
						SearchRequest.att.query('PaginateRS').value('.','varchar(5)') AS PaginateRS,
						SearchRequest.att.query('StartRecord').value('.','int') AS StartRecord,
						SearchRequest.att.query('SortBy').value('.', 'varchar(512)') AS SortBy,
						SearchRequest.att.query('SortByDir').value('.','varchar(4)') AS SortDirection, --CR 27954 10/16/09 JPB
						SearchRequest.att.query('SortByDisplayName').value('.', 'varchar(64)') AS SortDisplayName,
						COALESCE(SearchRequest.att.value('@DisplayScannedCheck', 'bit'),1) AS DisplayScannedChecks,
						CASE
							WHEN UPPER(SearchRequest.att.query('COTSOnly').value('.','VARCHAR(5)')) = 'FALSE' THEN 0
							ELSE 1
						END AS DisplayCOTSOnly,
						COALESCE(SearchRequest.att.query('MarkSenseOnly').value('.','bit'),0) AS MarkSense,     -- WI 179166   
						SearchRequest.att.query('BatchPaymentTypeKey').value('.', 'VARCHAR(3)') AS BatchPaymentTypeKey,
						SearchRequest.att.query('BatchSourceKey').value('.', 'VARCHAR(5)') AS BatchSourceKey
				FROM	@parmSearchRequest.nodes('/Root') SearchRequest(att)
			) SR;

	--Clean up Batch IDs
	IF @iSourceBatchIDFrom = 0
		SET @iSourceBatchIDFrom = -1;
	IF @iSourceBatchIDTo = 0
		SET @iSourceBatchIDTo = 9223372036854775807; -- BIGINT max value

	--Clean up Batch Numberss
	IF @iBatchNumberFrom = 0
		SET @iBatchNumberFrom = -1;
	IF @iBatchNumberTo = 0
		SET @iBatchNumberTo = 999999999; 
	
	--Clean up pagition information
	IF @iRecordsPerPage <= 0
		SET @iRecordsPerPage = 999999999;
	IF @iStartRecord <= 0
		SET @iStartRecord = 1;

	--SELECT '#DEInfoFields', * FROM 	#DEInfoFields		-- DEBUG ONLY
	
	--Cleanup sort direction
	IF LEN(@vcSortDirection) = 0
		SET @vcSortDirection = 'ASC';
		
	--Set pagation information
	IF UPPER(@vcPaginateRS) = 'TRUE' OR @vcPaginateRS = '1'
		SET @iPaginateRS = 1;
	ELSE
		SET @iPaginateRS = 0;
		
	Set @dtDepositDateKeyStart = CONVERT(VARCHAR,(@dtDepositDateStart),112);	--CR 51364
	Set @dtDepositDateKeyStop = CONVERT(VARCHAR,(@dtDepositDateEnd),112);   	--CR 51364
	
	BEGIN TRY
		
		-- WI 194934  Adding the viewAhead bit and Enddatekey for use later to determine viewahead permission
		INSERT into #tmpDimensions 
		(   
			SiteBankID, 
			EntityID,
			SiteClientAccountID, 
			ClientAccountKey, 
			SiteCodeID,
			LBMostRecent,
			StartDateKey,
			SiteCurProcDateKey,
			ViewAhead
		)
		SELECT 
			RecHubUser.SessionClientAccountEntitlements.SiteBankID, 
			RecHubUser.SessionClientAccountEntitlements.EntityID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID, 
			RecHubData.dimClientAccounts.ClientAccountKey, 
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.MostRecent,
			RecHubUser.SessionClientAccountEntitlements.StartDateKey,
			RecHubUser.SessionClientAccountEntitlements.EndDateKey,
			RecHubUser.SessionClientAccountEntitlements.ViewAhead
		FROM RecHubUser.SessionClientAccountEntitlements
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
				AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
				AND RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		WHERE 
			RecHubUser.SessionClientAccountEntitlements.SessionID = @vcSessionID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID =
			CASE 
			WHEN (LEN(@vcClientAccountID) > 0 AND @vcClientAccountID <> 0) THEN @vcClientAccountID
			ELSE	RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			END;

	END TRY
	BEGIN CATCH
		--error inserting into temp table, stop the process here
		--get the error information so it can be passed to RAISERROR
		--this will pass control to main CATCH block
		DECLARE @ErrorMessageTmpDims    NVARCHAR(4000),
				@ErrorSeverityTmpDims   INT,
				@ErrorStateTmpDims      INT;
		SELECT	@ErrorMessageTmpDims = ERROR_MESSAGE()+ 'Inserting into #tmpDimensions table',
				@ErrorSeverityTmpDims = ERROR_SEVERITY(),
				@ErrorStateTmpDims = ERROR_STATE();
		RAISERROR(@ErrorMessageTmpDims,@ErrorSeverityTmpDims,@ErrorStateTmpDims);
	END CATCH

 --SELECT '#TmpDimensions ', * FROM #tmpDimensions		--  DEBUG ONLY

	--  MOVING DOWN SECTION OF CODE Begin
	-- WI 71714  Combined the collection of Items by combining the Where conditions
	INSERT INTO #DEInfoFields (TableType,TableName,FldName,StubsAmountOccured,DataType,DisplayName,OrgDisplayName,BatchSourceKey)
	SELECT	DISTINCT 
			CASE UPPER(SearchRequest.att.value('@tablename', 'varchar(20)'))
				WHEN 'CHECKS' THEN 1
				ELSE 0
			END AS TableType,
			SearchRequest.att.value('@tablename', 'varchar(20)') AS TableName,
			CASE   -- UPPER(SearchRequest.att.value('@fieldname', 'varchar(256)'))
				WHEN UPPER(SearchRequest.att.value('@fieldname', 'varchar(256)')) IN ('AMOUNT', 'RT', 'ACCOUNT', 'SERIAL', 'TRANSACTIONCODE') 
					AND UPPER(SearchRequest.att.value('@tablename', 'varchar(20)')) = 'CHECKS' 
					--AND SearchRequest.att.value('@batchsourcekey', 'smallint') = 255  
					THEN SearchRequest.att.value('@fieldname', 'varchar(256)')
				ELSE
			'['+SearchRequest.att.value('@fieldname', 'varchar(256)')+']'
			END AS FldName,
			CASE 
				WHEN SearchRequest.att.value('@tablename', 'varchar(128)') = 'Stubs' AND UPPER(SearchRequest.att.value('@fieldname', 'varchar(128)')) = 'AMOUNT' THEN 1
				ELSE 0
			END AS StubsAmountOccured,
			SearchRequest.att.value('@datatype', 'int') AS DataType,
			REPLACE(REPLACE(SearchRequest.att.value('@reporttitle', 'varchar(265)'),'[INV].',''),'[PMT].','') AS DisplayName,    -- FP WI122800  -- TODO reduce when labels reduce
			SearchRequest.att.value('@reporttitle', 'varchar(256)') AS OrgDisplayName,
			SearchRequest.att.value('@batchsourcekey', 'smallint') AS BatchSourceKey				
	FROM 	
		@parmSearchRequest.nodes('/Root/SelectFields/field') SearchRequest(att)
	WHERE	
		UPPER(SearchRequest.att.value('@tablename', 'varchar(20)')) = 'STUBS' 
		OR (UPPER(SearchRequest.att.value('@tablename', 'varchar(20)')) = 'CHECKS' 
			--CR 51648 WJS Allow other checks fields the only want you want to exclude is checksequence
			AND UPPER(SearchRequest.att.value('@fieldname', 'varchar(256)')) <> 'CHECKSEQUENCE');
	SET @iDEColumnCount = @@ROWCOUNT;
	--SELECT '#DEInfoFields From XML SelectFields', * FROM 	#DEInfoFields		-- DEBUG ONLY


/************************ MOVED Code for Adding DEWhere fields to before the Bug 213687 section AND before adding WorkgroupDEKey  MGE 08/27/2015 ************/	
	--finally get items in the where clause that are not in the selected fields list, but make sure we do not add any check columns
	;WITH DEFI AS
	(
		SELECT	DISTINCT 
			CASE UPPER(SearchRequest.att.value('@tablename', 'varchar(20)'))
				WHEN 'CHECKS' THEN 1
				ELSE 0
			END AS TableType,
			SearchRequest.att.value('@tablename', 'varchar(20)') AS TableName,
			CASE  -- UPPER(SearchRequest.att.value('@fieldname', 'varchar(256)'))				-- not sure if this is really need as we should have them excluded with Where clause
				WHEN UPPER(SearchRequest.att.value('@fieldname', 'varchar(256)')) IN ('AMOUNT', 'RT', 'ACCOUNT', 'SERIAL', 'TRANSACTIONCODE') 
					AND UPPER(SearchRequest.att.value('@tablename', 'varchar(20)')) = 'CHECKS' 
					--AND SearchRequest.att.value('@batchsourcekey', 'smallint') = 255  
					THEN SearchRequest.att.value('@fieldname', 'varchar(256)')
				ELSE
			'['+SearchRequest.att.value('@fieldname', 'varchar(256)')+']'
			END AS FldName,
			CASE 
				WHEN SearchRequest.att.value('@tablename', 'varchar(20)') = 'Stubs' AND UPPER(SearchRequest.att.value('@fieldname', 'varchar(256)')) = 'AMOUNT' THEN 1
				ELSE 0
			END AS StubsAmountOccured,
			SearchRequest.att.value('@datatype', 'int') AS DataType,
			0 AS DisplayFinal,
			REPLACE(REPLACE(SearchRequest.att.value('@reporttitle', 'varchar(265)'),'[INV].',''),'[PMT].','') AS DisplayName,    -- FP WI122800
			SearchRequest.att.value('@reporttitle', 'varchar(265)') AS OrgDisplayName,
			SearchRequest.att.value('@batchsourcekey', 'smallint') AS BatchSourceKey				
		FROM 	@parmSearchRequest.nodes('/Root/WhereClause/field') SearchRequest(att)
		WHERE	
				((UPPER(SearchRequest.att.value('@tablename', 'varchar(20)')) = 'CHECKS' 
					AND SearchRequest.att.value('@fieldname', 'varchar(256)') NOT IN ('AMOUNT', 'RT', 'ACCOUNT', 'SERIAL')) 
					OR (UPPER(SearchRequest.att.value('@tablename', 'varchar(20)')) <> 'CHECKS'))
			AND SearchRequest.att.value('@batchsourcekey', 'smallint') <> 255   -- These are the reserved fields that are constants
			AND SearchRequest.att.value('@datatype', 'int') <> -1    --- This takes care of bogus empty rows in the Where clause
	)
	INSERT INTO #DEInfoFields (TableType,TableName,FldName,DataType,DisplayFinal,DisplayName,StubsAmountOccured,OrgDisplayName,BatchSourceKey)
	SELECT TableType,TableName,FldName,DataType,DisplayFinal,DisplayName,StubsAmountOccured,OrgDisplayName,BatchSourceKey
	FROM DEFI 
	WHERE	NOT EXISTS
			(	
				SELECT	DEInfo.TableName,
						DEInfo.FldName
				FROM	#DEInfoFields DEInfo
				WHERE	DEInfo.TableName = DEFI.TableName
						--AND REPLACE(REPLACE(DEInfo.FldName, '[', ''), ']', '') = DEFI.FldName	
						AND DEInfo.FldName = DEFI.FldName
						AND DEInfo.DisplayName = DEFI.DisplayName
			);
	--select 'DEInfoWithWhereFields', * from #DEInfoFields			--DEBUG ONLY
	UPDATE #DEInfoFields SET WorkgroupDataEntryColumnKey = RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
	FROM #DEInfoFields 
	INNER JOIN RecHubData.dimWorkgroupDataEntryColumns 
		ON RecHubData.dimWorkgroupDataEntryColumns.UILabel = #DEInfoFields.DisplayName
		AND RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = #DEInfoFields.BatchSourceKey
		AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = #DEInfoFields.TableType
		AND RecHubData.dimWorkgroupDataEntryColumns.DataType = #DEInfoFields.DataType
	INNER JOIN #tmpDimensions ON
		RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = #tmpDimensions.SiteClientAccountID
		AND RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = #tmpDimensions.SiteBankID

		/* Section Added for Bug 213687 *******************************************************************************/
--select 'DEInfoBefore', * from #DEInfoFields			--DEBUG ONLY
	INSERT INTO #DEInfoFields (TableType,TableName,FldName,StubsAmountOccured,DataType,DisplayName,OrgDisplayName,BatchSourceKey,WorkgroupDataEntryColumnKey,DisplayFinal)
	SELECT Distinct
		RecHubData.dimWorkgroupDataEntryColumns.IsCheck,
		CASE  RecHubData.dimWorkgroupDataEntryColumns.IsCheck
			WHEN 1 THEN 'Checks'
			ELSE	'Stubs'
		END
		AS TableName,
		'[' + RecHubData.dimWorkgroupDataEntryColumns.FieldName + ']', 
		0 AS StubsAmountOccured,
		RecHubData.dimWorkgroupDataEntryColumns.DataType,
		RecHubData.dimWorkgroupDataEntryColumns.UILabel,
		#DEInfoFields.OrgDisplayName AS OrgDisplayName,				
		RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey,
		RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey,
		#DEInfoFields.DisplayFinal												--Make the same as the field we're matching on
	FROM 
		RecHubData.dimWorkgroupDataEntryColumns
		INNER JOIN #tmpDimensions ON
			RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = #tmpDimensions.SiteClientAccountID
			AND RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = #tmpDimensions.SiteBankID
		LEFT JOIN #DEInfoFields ON 	
			UPPER(RecHubData.dimWorkgroupDataEntryColumns.UILabel) = UPPER(#DEInfoFields.DisplayName)
			AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = #DEInfoFields.TableType
			AND ((RecHubData.dimWorkgroupDataEntryColumns.DataType = #DEInfoFields.DataType) OR 
				(#DEInfoFields.TableName = 'Stubs' AND #DEInfoFields.FldName = 'Amount'))
	WHERE 
		UPPER(RecHubData.dimWorkgroupDataEntryColumns.UILabel) = UPPER(#DEInfoFields.DisplayName)
		AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = #DEInfoFields.TableType
		AND RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey NOT IN (SELECT WorkgroupDataEntryColumnKey FROM #DEInfoFields Where WorkgroupDataEntryColumnKey IS NOT NULL);
		

	

--select 'DEInfoAfter', * from #DEInfoFields			--DEBUG ONLY

/* End of Section Added for Bug 213687 *************************************************************************************/

	SELECT @iDEColumnCount = Count(*) from #DEInfoFields;
	
	/* WI 70079 JPB 11/28/12 - Create the final result set column name */
	UPDATE #DEInfoFields
	SET 
		FinalName = 
			CASE 
				WHEN UPPER(TableName) = 'CHECKS' AND UPPER(FldName) IN ('AMOUNT','ACCOUNT','RT','SERIAL','TRANSACTIONCODE','[AMOUNT]','[ACCOUNT]','[RT]','[SERIAL]','[TRANSACTIONCODE]')
					THEN TableName + REPLACE(REPLACE(FldName, '[', ''), ']', '')
				ELSE '['+TableName + REPLACE(REPLACE(FldName, '[', ''), ']', '') + '_' + RIGHT('0'+CAST(RowID AS VARCHAR),3)+']'    -- WI194934 for 100+ columns
			END
	FROM #DEInfoFields;

	-- MOVING DOWN SECTION OF CODE END
	--Select @BatchSourceKey AS 'BatchSource FilterKey'				--DEBUG ONLY

	INSERT INTO #DEInfoFieldsExpanded 
	(TableName, FldName, FinalName, DisplayFinal, DataType, TableType, WorkgroupDataEntryColumnKey, StubsAmountOccured, DisplayName, BatchSourceKey)
	SELECT DISTINCT
			DEIF.TableName,
			DEIF.FldName,
			DEIF.FinalName,
			DEIF.DisplayFinal,
			DEIF.DataType,
			DEIF.TableType,
		RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey,
			DEIF.StubsAmountOccured,
		DEIF.DisplayName,
		DEIF.BatchSourceKey
	FROM	
		RecHubData.dimWorkgroupDataEntryColumns
		INNER JOIN #tmpDimensions ON
			RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = #tmpDimensions.SiteBankID
			AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = #tmpDimensions.SiteClientAccountID		
			INNER JOIN #DEInfoFields DEIF 
			ON RecHubData.dimWorkgroupDataEntryColumns.IsCheck = DEIF.TableType
			AND RecHubData.dimWorkgroupDataEntryColumns.FieldName = REPLACE(REPLACE(DEIF.FldName, '[', ''), ']', '')    
			--AND RecHubData.dimWorkgroupDataEntryColumns.SourceDisplayName = DEIF.DisplayName		---  This will need to change when we use UILabel
			AND (	
				(RecHubData.dimWorkgroupDataEntryColumns.UILabel IS NULL 
					AND	UPPER(RecHubData.dimWorkgroupDataEntryColumns.SourceDisplayName) = UPPER(DEIF.DisplayName))    ---  This will need to change when we use UILabel
			OR
				(RecHubData.dimWorkgroupDataEntryColumns.UILabel IS NOT NULL		
					AND UPPER(RecHubData.dimWorkgroupDataEntryColumns.UILabel) = UPPER(DEIF.DisplayName))
			)			
			AND RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = 
                     CASE 
                       WHEN @BatchSourceKey <> 255  THEN @BatchSourceKey --    Applying batchsource filter to DE fields list
                       ELSE RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey
                     END
			AND RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = DEIF.BatchSourceKey
			AND (RecHubData.dimWorkgroupDataEntryColumns.DataType = DEIF.DataType OR 
				DEIF.TableName = 'Stubs' AND DEIF.FldName = 'Amount');			--MGE --CR 52389 JPB 04/30/12
		 
	--Select ' #DEInfoFieldsExpanded	' , * from #DEInfoFieldsExpanded order by WorkgroupDataEntryColumnKey	--DEBUG ONLY	
	--SELECT '#DEInfoFields', * FROM 	#DEInfoFields		-- DEBUG ONLY

	--SELECT @vcSortBy AS '@vcSortBy BEFORE cleanup'							--	DEBUG ONLY
	--SELECT LEFT(@vcSortBy,CHARINDEX('.',@vcSortBy)-1) AS 'TablenameComp'		--	DEBUG ONLY
	--SELECT RIGHT(@vcSortBy,LEN(@vcSortBy)-CHARINDEX('.',@vcSortBy)) AS 'FldNameComp'	--	DEBUG ONLY
	--SELECT @vcSortDisplayName as '@vcSortDisplayName'							--	DEBUG ONLY

	IF @BatchSourceKey <> 255   -- Making DEInfoFields match DEIfnoFieldExpanded based on batchsourcekey filter
       BEGIN;
           DELETE FROM #DEInfoFields
           WHERE BatchSourceKey <> @BatchSourceKey;
       END;

	--Clean up sort by
	IF CHARINDEX('.',@vcSortBy) > 0
	BEGIN
		SELECT	@vcSortBy = FinalName
		FROM	#DEInfoFields
		WHERE	TableName = LEFT(@vcSortBy,CHARINDEX('.',@vcSortBy)-1)
				--AND FldName = RIGHT(@vcSortBy,LEN(@vcSortBy)-CHARINDEX('.',@vcSortBy))
				AND REPLACE(REPLACE(FldName, '[', ''), ']', '') = RIGHT(@vcSortBy,LEN(@vcSortBy)-CHARINDEX('.',@vcSortBy))
				AND OrgDisplayName = @vcSortDisplayName;									-- 179166
		IF( @@ROWCOUNT  = 0 )	
			SET @vcSortBy = REPLACE(@vcSortBy,'.','');
	END
	--SELECT @vcSortBy AS '@vcSortBy after cleanup'							--	DEBUG ONLY
	IF @DEWhereCount > 0
	BEGIN
		UPDATE #DEWhere
			SET CombinedName = DE.FinalName
		FROM #DEInfoFields DE
		WHERE #DEWhere.TableType = DE.TableType AND #DEWhere.ColumnName = DE.FldName;
	END
	--Select '#DEWhere' ,* from #DEWhere					--DEBUG ONLY	
	
	--Get check search fields
	--WI 200581 Add Datatype = 6 to case statement to handle decimal data against INT columns on factchecks table
	SET @CheckSearch = ''
	SELECT @CheckSearch=@CheckSearch + ' RecHubData.factChecks.' 
			+CASE FieldName
				WHEN 'RT' THEN 'RoutingNumber'
				ELSE FieldName
			END
			+CASE Operator 
				WHEN 'Equals' THEN '=' +CASE WHEN FieldName = 'Amount' OR DataType = 6 THEN SR.DEValue ELSE QUOTENAME(SR.DEValue,CHAR(39)) END
				WHEN 'Is Greater Than' THEN ' > ' +CASE WHEN FieldName = 'Amount' OR DataType = 6 THEN SR.DEValue ELSE QUOTENAME(SR.DEValue,CHAR(39)) END
				WHEN 'Is Less Than' THEN ' < ' +CASE WHEN FieldName = 'Amount' OR DataType = 6 THEN SR.DEValue ELSE QUOTENAME(SR.DEValue,CHAR(39)) END
				WHEN 'Begins With' THEN ' LIKE '+QUOTENAME(SR.DEValue+'%',CHAR(39))
				WHEN 'Contains' THEN ' LIKE '+QUOTENAME('%'+SR.DEValue+'%',CHAR(39))
				WHEN 'Ends With' THEN ' LIKE '+QUOTENAME('%'+SR.DEValue,CHAR(39))
				ELSE Operator       
			END+' AND'
	FROM ( 
			SELECT	SearchRequest.att.value('@tablename', 'varchar(20)') AS TableName,
					SearchRequest.att.value('@fieldname', 'varchar(128)') AS FieldName,
					SearchRequest.att.value('@datatype', 'int') AS DataType,
					SearchRequest.att.value('@operator', 'varchar(20)') as Operator,
					SearchRequest.att.value('@value', 'varchar(128)') as DEValue
			FROM	@parmSearchRequest.nodes('/Root/WhereClause/field') SearchRequest(att)
			WHERE	SearchRequest.att.value('@tablename', 'varchar(20)') = 'Checks'
			AND		SearchRequest.att.value('@fieldname', 'varchar(128)') IN ('Amount', 'Account', 'RT', 'SERIAL', 'CheckSequence')
		   ) SR;
	--	WI 133284
	--  Add DDA field to check search need to use dimDDAs table
	SELECT @CheckSearch=@CheckSearch + ' RecHubData.dimDDAs.' + FieldName
			+CASE Operator 
				WHEN 'Equals' THEN '=' +QUOTENAME(SR.DEValue,CHAR(39)) 
				WHEN 'Begins With' THEN ' LIKE '+QUOTENAME(SR.DEValue+'%',CHAR(39))
				WHEN 'Contains' THEN ' LIKE '+QUOTENAME('%'+SR.DEValue+'%',CHAR(39))
				WHEN 'Ends With' THEN ' LIKE '+QUOTENAME('%'+SR.DEValue,CHAR(39))
				ELSE Operator       
			END+' AND'
	FROM ( 
			SELECT	SearchRequest.att.value('@tablename', 'varchar(20)') AS TableName,
					SearchRequest.att.value('@fieldname', 'varchar(128)') AS FieldName,
					SearchRequest.att.value('@datatype', 'int') AS DataType,
					SearchRequest.att.value('@operator', 'varchar(20)') as Operator,
					SearchRequest.att.value('@value', 'varchar(128)') as DEValue
			FROM	@parmSearchRequest.nodes('/Root/WhereClause/field') SearchRequest(att)
			WHERE	SearchRequest.att.value('@fieldname', 'varchar(128)') = 'DDA'	-- WI159573
		   ) SR;
	--Add amounts if defined
	--CR 27952 JPB 10/16/09 Added = to each statement
	IF LEN(@vcAmountFrom) > 0
	BEGIN
		IF LEN(@CheckSearch) > 0
			SET @CheckSearch = @CheckSearch + ' RecHubData.factChecks.Amount >= ' + @vcAmountFrom + ' AND';
		ELSE SET @CheckSearch = 'RecHubData.factChecks.Amount >= ' + @vcAmountFrom + ' AND';
	END
	IF LEN(@vcAmountTo) > 0
	BEGIN
		IF LEN(@CheckSearch) > 0
			SET @CheckSearch = @CheckSearch + ' RecHubData.factChecks.Amount <= ' + @vcAmountTo + ' AND';
		ELSE SET @CheckSearch = 'RecHubData.factChecks.Amount <= ' + @vcAmountTo + ' AND';
	END
	--Add Check Serial Number if defined
	--Serial Number CR 27953 10/16/09 JPB
	IF LEN(@vcSerialNumber) > 0 
	BEGIN
		IF LEN(@CheckSearch) > 0 --SerialNumber NOT LIKE @vcSerialNumber OR SerialNumber IS NULL
			SET @CheckSearch = @CheckSearch + ' RecHubData.factChecks.Serial LIKE ' + CHAR(39) + '%' + @vcSerialNumber + '%' + CHAR(39) + ' AND';
		ELSE SET @CheckSearch = 'RecHubData.factChecks.Serial LIKE ' + CHAR(39) + '%' + @vcSerialNumber + '%' + CHAR(39) + ' AND';
	END

	--Removing trailing 'AND'
	IF LEN(@CheckSearch) > 0
		SET @CheckSearch = SUBSTRING(@CheckSearch,1,LEN(@CheckSearch)-3);

	--select @CheckSearch	AS ChkSearch	--Debug only
	--Create base transaction search command

	SET @SQLSearchCommand = '
	INSERT INTO #tmpMatchingResults_TEMP
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchSourceKey,BatchPaymentTypeKey,BatchID,SourceBatchID,BatchNumber,TransactionID,TxnSequence,CheckCount,DocumentCount,StubCount,OMRCount,CheckAmount,
	BankID,ClientAccountID,SerialNumber,numeric_serial,BatchSequence,ChecksCheckSequence,RemitterName,Account,RoutingNumber,numeric_rt,DDA,TransactionCode) --,BatchSourceShortName,ImportTypeShortName)	
			SELECT	RecHubData.factTransactionSummary.BankKey,
					RecHubData.factTransactionSummary.OrganizationKey,
					RecHubData.factTransactionSummary.ClientAccountKey,
					RecHubData.factTransactionSummary.ImmutableDateKey,
					RecHubData.factTransactionSummary.DepositDateKey,
					RecHubData.factTransactionSummary.BatchSourceKey,
					RecHubData.factTransactionSummary.BatchPaymentTypeKey,
					RecHubData.factTransactionSummary.BatchID,
					RecHubData.factTransactionSummary.SourceBatchID,
					RecHubData.factTransactionSummary.BatchNumber,
					RecHubData.factTransactionSummary.TransactionID,
					RecHubData.factTransactionSummary.TxnSequence,
					CheckCount,
					CASE ' + CONVERT(char(1),@iDisplayScannedChecks) + '
						WHEN 0 THEN DocumentCount - ScannedCheckCount
						ELSE DocumentCount
					END AS DocumentCount,
					StubCount,
					OMRCount,
					RecHubData.factChecks.Amount,
					SiteBankID,
					SiteClientAccountID,
					RecHubData.factChecks.Serial,
					RecHubData.factChecks.NumericSerial,
					RecHubData.factChecks.BatchSequence,
					RecHubData.factChecks.CheckSequence,
					RecHubData.factChecks.RemitterName,
					RecHubData.factChecks.Account,
					RecHubData.factChecks.RoutingNumber,
					RecHubData.factChecks.NumericRoutingNumber,
					RecHubData.dimDDAs.DDA,
					RecHubData.factChecks.TransactionCode
					--RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
					--RecHubData.dimImportTypes.ShortName AS ImportTypeShortName'
					;

			SET @SQLSearchCommand = @SQLSearchCommand + '
			FROM	#tmpDimensions
					INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.ClientAccountKey = #tmpDimensions.ClientAccountKey'

			SET @SQLSearchCommand = @SQLSearchCommand + '
					LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
							AND RecHubData.factChecks.BatchID = RecHubData.factTransactionSummary.BatchID
							AND RecHubData.factChecks.TransactionID = RecHubData.factTransactionSummary.TransactionID
							AND RecHubData.factChecks.IsDeleted = 0';

			--	WI 133284
				SET @SQLSearchCommand = @SQLSearchCommand + '
					LEFT OUTER JOIN RecHubData.dimDDAs ON RecHubData.factChecks.DDAKey = RecHubData.dimDDAs.DDAKey';
			--  WI 167521
			--SET @SQLSearchCommand = @SQLSearchCommand + '
			--		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey ' 

			--SET @SQLSearchCommand = @SQLSearchCommand + '
			--		INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey ';
					
			SET @SQLSearchCommand = @SQLSearchCommand + 
			'
			WHERE	RecHubData.factTransactionSummary.DepositDateKey  >= StartDateKey' + ' 
					AND ((RecHubData.factTransactionSummary.DepositDateKey  <= SiteCurProcDateKey AND ViewAhead = 0 ) OR (ViewAhead = 1))' + ' 
					AND RecHubData.factTransactionSummary.DepositDateKey >= ' + CONVERT(VARCHAR,@dtDepositDateKeyStart) + '
					AND RecHubData.factTransactionSummary.DepositDateKey <= ' + CONVERT(VARCHAR,@dtDepositDateKeyStop) + '
					AND RecHubData.factTransactionSummary.SourceBatchID >= ' + CONVERT(VARCHAR,@iSourceBatchIDFrom) + '
					AND RecHubData.factTransactionSummary.SourceBatchID <= ' + CONVERT(VARCHAR,@iSourceBatchIDTo) + '
					AND RecHubData.factTransactionSummary.BatchNumber >= ' + CONVERT(VARCHAR,@iBatchNumberFrom) + '
					AND RecHubData.factTransactionSummary.BatchNumber <= ' + CONVERT(VARCHAR,@iBatchNumberTo) + '
					AND RecHubData.factTransactionSummary.DepositStatus >= 850
					AND RecHubData.factTransactionSummary.OMRCount >= '+ CONVERT(VARCHAR,@bMarkSense);
			IF( @BatchPaymentTypeKey <> 255 )
				SET @SQLSearchCommand = @SQLSearchCommand + '
					AND RecHubData.factTransactionSummary.BatchPaymentTypeKey = ' + CAST(@BatchPaymentTypeKey AS VARCHAR);
			IF( @BatchSourceKey <> 255 )
				SET @SQLSearchCommand = @SQLSearchCommand + '
					AND RecHubData.factTransactionSummary.BatchSourceKey = ' + CAST(@BatchSourceKey AS VARCHAR);
			SET @SQLSearchCommand = @SQLSearchCommand + '
					AND RecHubData.factTransactionSummary.IsDeleted = 0';
		IF LEN(@CheckSearch) > 0
		SET @SQLSearchCommand = @SQLSearchCommand + '
		AND ' + @CheckSearch;
		
		SET @SQLSearchCommand = @SQLSearchCommand + '
		OPTION (MAXDOP 1)';

	--SELECT 'INSERT INTO #tmpMatchingResults_TEMP'  -- DEBUG ONLY
	--PRINT @SQLSearchCommand				-- DEBUG ONLY

	EXEC(@SQLSearchCommand);
	SET @totalMatchRowCount = @@ROWCOUNT;

	--select '#tmpMatchingResults_TEMP ', * from #tmpMatchingResults_TEMP			--  DEBUG ONLY
	SELECT @StubsDEColumnCount = Coalesce(Count(*),0) from #DEInfoFields WHERE TableName like 'Stubs%';			
	--SET @SQLSearchCommand = '
	IF @StubsDEColumnCount = 0
		-- Just copy rows from #tmpMatchingResults
		INSERT INTO #tmpMatchingResults
		(BankKey,
		OrganizationKey,
		ClientAccountKey,
		ImmutableDateKey,
		DepositDateKey,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		TransactionID,
		TxnSequence,
		CheckCount,
		DocumentCount,
		StubCount,
		OMRCount,
		CheckAmount,
		BankID,
		ClientAccountID,
		SerialNumber,
		numeric_serial,
		BatchSequence,
		StubsBatchSequence,
		ChecksCheckSequence,
		StubsStubSequence,
		RemitterName,
		Account,
		RoutingNumber,
		numeric_rt,
		DDA,
		BatchSourceShortName,
		ImportTypeShortName,
		TransactionCode)	
	SELECT	
		#tmpMatchingResults_TEMP.BankKey,
		#tmpMatchingResults_TEMP.OrganizationKey,
		#tmpMatchingResults_TEMP.ClientAccountKey,
		#tmpMatchingResults_TEMP.ImmutableDateKey,
		#tmpMatchingResults_TEMP.DepositDateKey,
		#tmpMatchingResults_TEMP.BatchSourceKey,
		#tmpMatchingResults_TEMP.BatchPaymentTypeKey,
		#tmpMatchingResults_TEMP.BatchID,
		#tmpMatchingResults_TEMP.SourceBatchID,
		#tmpMatchingResults_TEMP.BatchNumber,
		#tmpMatchingResults_TEMP.TransactionID,
		#tmpMatchingResults_TEMP.TxnSequence,
		#tmpMatchingResults_TEMP.CheckCount,
		#tmpMatchingResults_TEMP.DocumentCount,
		#tmpMatchingResults_TEMP.StubCount,
		#tmpMatchingResults_TEMP.OMRCount,
		#tmpMatchingResults_TEMP.CheckAmount,
		#tmpMatchingResults_TEMP.BankID,
		#tmpMatchingResults_TEMP.ClientAccountID,
		#tmpMatchingResults_TEMP.SerialNumber,
		#tmpMatchingResults_TEMP.numeric_serial,
		#tmpMatchingResults_TEMP.BatchSequence,
		NULL AS StubsBatchSequence,								--	Mo stubs included in select since no fields were requested
		#tmpMatchingResults_TEMP.ChecksCheckSequence,
		0 AS StubsStubSequence,									--	Mo stubs included in select since no fields were requested
		#tmpMatchingResults_TEMP.RemitterName,
		#tmpMatchingResults_TEMP.Account,
		#tmpMatchingResults_TEMP.RoutingNumber,
		#tmpMatchingResults_TEMP.numeric_rt,		
		#tmpMatchingResults_TEMP.DDA,
		#tmpMatchingResults_TEMP.BatchSourceShortName,
		#tmpMatchingResults_TEMP.ImportTypeShortName,
		#tmpMatchingResults_TEMP.TransactionCode
	FROM	
		#tmpMatchingResults_TEMP	
	ELSE
	INSERT INTO #tmpMatchingResults
		(BankKey,
		OrganizationKey,
		ClientAccountKey,
		ImmutableDateKey,
		DepositDateKey,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		TransactionID,
		TxnSequence,
		CheckCount,
		DocumentCount,
		StubCount,
		OMRCount,
		CheckAmount,
		BankID,
		ClientAccountID,
		SerialNumber,
		numeric_serial,
		BatchSequence,
		StubsBatchSequence,
		ChecksCheckSequence,
		StubsStubSequence,
		RemitterName,
		Account,
		RoutingNumber,
		numeric_rt,
		DDA,
		BatchSourceShortName,
		ImportTypeShortName,
		TransactionCode)	
	SELECT	
		#tmpMatchingResults_TEMP.BankKey,
		#tmpMatchingResults_TEMP.OrganizationKey,
		#tmpMatchingResults_TEMP.ClientAccountKey,
		#tmpMatchingResults_TEMP.ImmutableDateKey,
		#tmpMatchingResults_TEMP.DepositDateKey,
		#tmpMatchingResults_TEMP.BatchSourceKey,
		#tmpMatchingResults_TEMP.BatchPaymentTypeKey,
		#tmpMatchingResults_TEMP.BatchID,
		#tmpMatchingResults_TEMP.SourceBatchID,
		#tmpMatchingResults_TEMP.BatchNumber,
		#tmpMatchingResults_TEMP.TransactionID,
		#tmpMatchingResults_TEMP.TxnSequence,
		#tmpMatchingResults_TEMP.CheckCount,
		#tmpMatchingResults_TEMP.DocumentCount,
		#tmpMatchingResults_TEMP.StubCount,
		#tmpMatchingResults_TEMP.OMRCount,
		#tmpMatchingResults_TEMP.CheckAmount,
		#tmpMatchingResults_TEMP.BankID,
		#tmpMatchingResults_TEMP.ClientAccountID,
		#tmpMatchingResults_TEMP.SerialNumber,
		#tmpMatchingResults_TEMP.numeric_serial,
		#tmpMatchingResults_TEMP.BatchSequence,
		RecHubData.factStubs.BatchSequence AS StubsBatchSequence,
		#tmpMatchingResults_TEMP.ChecksCheckSequence,
		COALESCE(RecHubData.factStubs.StubSequence, 0) AS StubsStubSequence,
		#tmpMatchingResults_TEMP.RemitterName,
		#tmpMatchingResults_TEMP.Account,
		#tmpMatchingResults_TEMP.RoutingNumber,
		#tmpMatchingResults_TEMP.numeric_rt,		
		#tmpMatchingResults_TEMP.DDA,
		#tmpMatchingResults_TEMP.BatchSourceShortName,
		#tmpMatchingResults_TEMP.ImportTypeShortName,
		#tmpMatchingResults_TEMP.TransactionCode
	FROM	
		#tmpMatchingResults_TEMP
			LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = #tmpMatchingResults_TEMP.DepositDateKey
				AND RecHubData.factStubs.BatchID = #tmpMatchingResults_TEMP.BatchID
				AND RecHubData.factStubs.TransactionID = #tmpMatchingResults_TEMP.TransactionID
				AND RecHubData.factStubs.TxnSequence = #tmpMatchingResults_TEMP.TxnSequence
				AND RecHubData.factStubs.IsDeleted = 0
				--AND #tmpMatchingResults_TEMP.BatchSequence IS NOT NULL
	OPTION (MAXDOP 1);

	--select '#tmpMatchingResults -After Adding FactStubs ', * from #tmpMatchingResults	 --  DEBUG ONLY


	SET @iProcessDEData = 0	;				--MGE 3/20/2012 for case where there is no filter
	--If matching records where found, build the final select statement
	IF @totalMatchRowCount > 0 
	BEGIN /* Records found */
		--Get data entry columns if they were requested
		IF @iDEColumnCount > 0
		BEGIN /* @iDEColumnCount > 0 */
			--Store the DE Results so we can join them to the final result set
			--I.E. get all of the de column data, still in the vertical view
			IF( @DEWhereCount > 0 )
			BEGIN --IF( @DEWhereCount > 0 )
				--SELECT '@DEWhereCount - After matching results'; SELECT @DEWhereCount;  -- DEBUG ONLY
				SET @SQLCommand = '';
				BEGIN
					SELECT	@SQLCommand=@SQLCommand + '			
					SELECT	RecHubData.factDataEntryDetails.BankKey,
							RecHubData.factDataEntryDetails.OrganizationKey,
							RecHubData.factDataEntryDetails.ClientAccountKey,
							RecHubData.factDataEntryDetails.ImmutableDateKey,
							RecHubData.factDataEntryDetails.DepositDateKey,
							RecHubData.factDataEntryDetails.BatchID,
							RecHubData.factDataEntryDetails.SourceBatchID,
							RecHubData.factDataEntryDetails.TransactionID,
							RecHubData.factDataEntryDetails.BatchSequence,
							SR.StubsStubSequence,
							DEIF.TableName,
							DEIF.FldName,
							DEIF.DataType,
							DEIF.StubsAmountOccured,
						--	DEIF.FldLength,
							RecHubData.factDataEntryDetails.DataEntryValue,
							RecHubData.factDataEntryDetails.WorkGroupDataEntryColumnKey,
							RecHubData.factDataEntryDetails.DataEntryValueMoney,
							RecHubData.factDataEntryDetails.DataEntryValueDateTime
					FROM	#DEInfoFieldsExpanded DEIF
							INNER JOIN RecHubData.factDataEntryDetails ON RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = DEIF.WorkGroupDataEntryColumnKey
							INNER JOIN #tmpMatchingResults  SR
									ON RecHubData.factDataEntryDetails.DepositDateKey = SR.DepositDateKey
									AND RecHubData.factDataEntryDetails.BatchID = SR.BatchID
									AND RecHubData.factDataEntryDetails.TransactionID = SR.TransactionID
									AND (RecHubData.factDataEntryDetails.BatchSequence = SR.StubsBatchSequence) 
										OR (RecHubData.factDataEntryDetails.BatchSequence = SR.BatchSequence)'
							
				END		--END IF BEGIN
			
				SET @SQLCommand = @SQLCommand + '
				WHERE RecHubData.factDataEntryDetails.IsDeleted = 0
				 ORDER BY RecHubData.factDataEntryDetails.BankKey,
						RecHubData.factDataEntryDetails.OrganizationKey,
						RecHubData.factDataEntryDetails.ClientAccountKey,
						RecHubData.factDataEntryDetails.DepositDateKey,
						RecHubData.factDataEntryDetails.BatchID,
						RecHubData.factDataEntryDetails.TransactionID';
		
				SET @SQLCommand = @SQLCommand + ' OPTION (RECOMPILE, MAXDOP 1)'; 	
				

				-- debug only	(next six lines)
				--SELECT DEIF.TableName, #DEWhere.TableName, DEIF.FldName, #DEWhere.ColumnName, * 
				--FROM	#DEInfoFieldsExpanded DEIF
				--		INNER JOIN #DEWhere ON DEIF.TableName = #DEWhere.TableName
				--					AND DEIF.FldName = #DEWhere.ColumnName;      -- DEBUG ONLY

				--SELECT ' INSERT INTO #tmpDEResults  '    --DEBUG ONLY
				--PRINT  @SQLCommand			--DEBUG ONLY
					
				INSERT INTO #tmpDEResults	
				(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,
				BatchID,SourceBatchID,TransactionID,BatchSequence,StubSequence,TableName,FldName,DataType, StubsAmountOccured,
				DataEntryValue,WorkgroupDataEntryColumnKey, DataEntryValueMoney,	DataEntryValueDateTime)
				EXEC(@SQLCommand);					
				SET @iProcessDEData = @@ROWCOUNT;

				--SELECT '#tmpDEResults ', * FROM  #tmpDEResults                 --DEBUG ONLY
			
				;WITH tmpDistinctRows AS
				(
					SELECT	DISTINCT 
							#tmpDEResults.BankKey,
							#tmpDEResults.OrganizationKey,
							#tmpDEResults.ClientAccountKey,
							#tmpDEResults.ImmutableDateKey,
							#tmpDEResults.DepositDateKey,
							#tmpDEResults.BatchID,
							#tmpDEResults.TransactionID,
							#tmpDEResults.BatchSequence,
							#tmpDEResults.StubSequence			-- WI 71714  
					FROM	#tmpDEResults
				)
				Insert into #tmpDEResults
					(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,SourceBatchID,TransactionID,BatchSequence,StubSequence,
					TableName,FldName,DataType, DataEntryValue, WorkgroupDataEntryColumnKey, DataEntryValueMoney, DataEntryValueDateTime)
				SELECT	tDR.BankKey,
						tDR.OrganizationKey,
						tDR.ClientAccountKey,
						tDR.ImmutableDateKey,
						tDR.DepositDateKey,
						tDR.BatchID,
						RecHubData.factDataEntryDetails.SourceBatchID,
						tDR.TransactionID,
						RecHubData.factDataEntryDetails.BatchSequence,
						tDR.StubSequence,
						DEIF.TableName,
						DEIF.FldName,
						DEIF.DataType,
						--DEIF.FldLength,
						RecHubData.factDataEntryDetails.DataEntryValue,
						RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey,
						RecHubData.factDataEntryDetails.DataEntryValueMoney,
						RecHubData.factDataEntryDetails.DataEntryValueDateTime
				FROM	RecHubData.factDataEntryDetails
						INNER JOIN tmpDistinctRows tDR ON RecHubData.factDataEntryDetails.DepositDateKey = tDR.DepositDateKey
							AND RecHubData.factDataEntryDetails.BatchID = tDR.BatchID
							AND RecHubData.factDataEntryDetails.TransactionID = tDR.TransactionID
							AND RecHubData.factDataEntryDetails.BatchSequence = tDR.BatchSequence	-- WI194934 Make sure we match at this level
						INNER JOIN #DEInfoFieldsExpanded DEIF ON RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = DEIF.WorkgroupDataEntryColumnKey
				WHERE DEIF.FldName NOT IN (SELECT Distinct ColumnName FROM #DEWhere);

			--SELECT '#tmpDEResults - After distinct rows ', * FROM  #tmpDEResults                 --DEBUG ONLY
					
			END --IF( @DEWhereCount > 0 )
		ELSE  --@DEWhereCount
			BEGIN --IF( @DEWhereCount > 0 )
		--select sum(stubcount) from #tmpMatchingResults				--Debug ONLY
				SELECT @totalMatchRowCount = @totalMatchRowCount * SUM(StubCount) FROM #tmpMatchingResults_TEMP;
				SELECT @totalMatchRowCount = @totalMatchRowCount * COUNT(*) FROM #DEInfoFieldsExpanded;
		/* IF Removed 04/26/12 JPB */
		--				IF( (SELECT COUNT(*) FROM #DEInfoFields) > 1 )
		--					SET @totalMatchRowCount = (@totalMatchRowCount * 10) * 1.9
		----select @totalMatchRowCount, @totalMatchWithDEMaxRowCount		--Debug ONLY
				IF( @totalMatchRowCount < @totalMatchWithDEMaxRowCount )
				BEGIN
					SET @SQLCommand = ''
					SELECT	@SQLCommand=@SQLCommand + '	
					SELECT	RecHubData.factDataEntryDetails.BankKey,
							RecHubData.factDataEntryDetails.OrganizationKey,
							RecHubData.factDataEntryDetails.ClientAccountKey,
							RecHubData.factDataEntryDetails.ImmutableDateKey,
							RecHubData.factDataEntryDetails.DepositDateKey,
							RecHubData.factDataEntryDetails.BatchID,
							RecHubData.factDataEntryDetails.SourceBatchID,
							RecHubData.factDataEntryDetails.TransactionID,
							RecHubData.factDataEntryDetails.BatchSequence,
							#tmpMatchingResults.StubsStubSequence,
							DEIF.TableName,
							DEIF.FldName,
							DEIF.DataType,
							RecHubData.factDataEntryDetails.DataEntryValue,
							RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey,
							RecHubData.factDataEntryDetails.DataEntryValueMoney,
							RecHubData.factDataEntryDetails.DataEntryValueDateTime
					FROM	#tmpMatchingResults
							INNER JOIN  RecHubData.factDataEntryDetails 
								ON	RecHubData.factDataEntryDetails.DepositDateKey = #tmpMatchingResults.DepositDateKey
									AND RecHubData.factDataEntryDetails.BatchID = #tmpMatchingResults.BatchID
									AND RecHubData.factDataEntryDetails.TransactionID = #tmpMatchingResults.TransactionID
									AND (RecHubData.factDataEntryDetails.BatchSequence = #tmpMatchingResults.StubsBatchSequence) 
										OR (RecHubData.factDataEntryDetails.BatchSequence = #tmpMatchingResults.BatchSequence )
							INNER JOIN #DEInfoFieldsExpanded DEIF ON RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = DEIF.WorkgroupDataEntryColumnKey
					WHERE	RecHubData.factDataEntryDetails.IsDeleted = 0';	
					SET @SQLSearchCommand = @SQLSearchCommand + '
						OPTION (RECOMPILE, MAXDOP 1)';	

					--SELECT ' Where @totalMatchRowCount < @totalMatchWithDEMaxRowCount   '   -- debug only 
					--PRINT @SQLCommand						--Debug ONLY	
								
					INSERT INTO #tmpDEResults	
						(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,SourceBatchID,TransactionID,BatchSequence,StubSequence,
						TableName,FldName,DataType,DataEntryValue,WorkgroupDataEntryColumnKey,DataEntryValueMoney,DataEntryValueDateTime)
					EXEC (@SQLCommand);
					SET @iProcessDEData = @@ROWCOUNT;

					--SELECT '#tmpDEResults  With No Where clause ', * FROM  #tmpDEResults                 --DEBUG ONLY
				END
				ELSE /* Total number of rows to process is too large, so set the count greater then max + 1 */ 
					SET @iProcessDEData = @factDataEntryDetailsMaxRowCount + 1;
			END --IF( @DEWhereCount > 0 )
			
			/* continue on only if we ended up with data entry rows to handle */
			/* CR 51782 JPB 04/05/12 Added OR @DEInSort = 1 OR @DEWhereCount > 0 */
			/* WI 123202 REmoving the Check for @iProcessDEData > 0 */
			IF( @iProcessDEData < @factDataEntryDetailsMaxRowCount OR @DEInSort = 1 OR @DEWhereCount > 0 ) 
			BEGIN /* IF( @iProcessDEData < @factDataEntryDetailsMaxRowCount ) */
				SET @TooManyRows = 0;
				--create the alter statement and get a list of the columns for the final select statement
				SET @SQLCommand = 'ALTER TABLE #tmpDEResultsPivot Add ';
				SELECT	@SQLCommand = @SQLCommand + SDEI.FinalName 
						+ CASE DataType
							WHEN 1 THEN ' VARCHAR(256) NULL'
							WHEN 6 THEN
								CASE StubsAmountOccured 
									WHEN 1 THEN ' MONEY NULL'
									ELSE  ' FLOAT NULL'
								END 
							WHEN 7 THEN ' MONEY NULL'
							WHEN 11 THEN ' DATETIME'
						END
						+ ','
				FROM	(
							SELECT	DISTINCT DEI.TableName AS TableName, 
									DEI.FldName AS FldName,
									DEI.FinalName AS FinalName, 
									MAX(DEI.DataType) AS DataType, 
									DEI.StubsAmountOccured AS StubsAmountOccured
							FROM	#DEInfoFieldsExpanded DEI
							WHERE	UPPER(DEI.FldName) <> 'CHECKSEQUENCE'
							GROUP BY 
									DEI.TableName,
									DEI.FldName,
									DEI.FinalName,
									DEI.StubsAmountOccured
						) SDEI;
				IF @@ROWCOUNT > 0
				BEGIN
					--clean up the SQL command
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);

					--now alter the temp table
					EXEC(@SQLCommand) ;

					--set the column names ordered by table, column name for the final result set
					SET @vcDEColumnNames = '';
					SET @vcDBColumnsWithCast = '';
					;WITH DEColumnList AS
					(
						SELECT DISTINCT TableName,FldName,DataType,StubsAmountOccured,FinalName
						FROM	#DEInfoFieldsExpanded
						WHERE	
							(DisplayFinal = 1 AND TableType = 0)
							OR (DisplayFinal = 1 AND TableType = 1 AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL'))
					)
					SELECT	@vcDEColumnNames = @vcDEColumnNames + FinalName  + ', ',
							@vcDBColumnsWithCast = @vcDBColumnsWithCast 
							+ CASE DataType 
								WHEN 6 THEN 
									CASE StubsAmountOccured 
									WHEN 1 THEN  'CAST(' + FinalName + ' AS MONEY)' 
									ELSE  'CAST(' + FinalName + ' AS FLOAT)' 
								END  
								WHEN 7 THEN 'CAST(' + FinalName + ' AS MONEY)' 
								WHEN 11 THEN 'CONVERT(VARCHAR(80),CAST(' + FinalName +' AS DATETIME),101)'
								ELSE FinalName 
							END
							+ ' AS ' + FinalName + ','
					FROM	DEColumnList
					ORDER BY TableName,FldName;

					--clean up the DE column list
					IF LEN(@vcDEColumnNames) > 0
						SET @vcDEColumnNames = SUBSTRING(@vcDEColumnNames,1,LEN(@vcDEColumnNames)-1);
					IF LEN(@vcDBColumnsWithCast) > 0
						SET @vcDBColumnsWithCast = SUBSTRING(@vcDBColumnsWithCast,1,LEN(@vcDBColumnsWithCast)-1);
				END	

				/* Alter the Check/Stub pivot tables to match the final restults */
				SET @SQLCommand = 'ALTER TABLE #pivotCheckInfo Add ';
				SELECT	@SQLCommand = @SQLCommand + SDEI.FinalName 
						+ CASE DataType
							WHEN 1 THEN ' VARCHAR(256) NULL'
							WHEN 6 THEN 
									CASE SDEI.StubsAmountOccured 
												WHEN 1 THEN ' MONEY NULL '
												ELSE  ' FLOAT NULL '
									END
							WHEN 7 THEN ' MONEY NULL'
							WHEN 11 THEN ' DATETIME'
						END
						+ ','
				FROM	(
							SELECT	DISTINCT DEI.TableName AS TableName, 
									DEI.FldName AS FldName,
									DEI.FinalName AS FinalName, 
									MAX(DEI.DataType) AS DataType, 
									DEI.StubsAmountOccured AS StubsAmountOccured
							FROM	#DEInfoFieldsExpanded DEI
							WHERE	
									DEI.TableType = 1
									AND UPPER(DEI.FldName) <> 'CHECKSEQUENCE' 
							GROUP BY 
									DEI.TableName,
									DEI.FldName,
									DEI.FinalName,
									DEI.StubsAmountOccured
						) SDEI;
						
				IF @@ROWCOUNT > 0
				BEGIN
					--clean up the SQL command
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
					--now alter the temp table

					--SELECT 'ALTER TABLE #pivotCheckInfo Add '  -- DEBUG ONLY
					--PRINT @SQLCommand						-- DEBUG ONLY

					EXEC(@SQLCommand); 
				END
				
				SET @SQLCommand = 'ALTER TABLE #pivotStubInfo Add ';
				SELECT	@SQLCommand = @SQLCommand + SDEI.FinalName 
						+ CASE DataType
							WHEN 1 THEN ' VARCHAR(256) NULL'
							WHEN 6 THEN 
									CASE SDEI.StubsAmountOccured 
												WHEN 1 THEN ' MONEY NULL '
												ELSE  ' FLOAT NULL '
									END
							WHEN 7 THEN ' MONEY NULL'
							WHEN 11 THEN ' DATETIME'
						END
						+ ','
				FROM	(
							SELECT	DISTINCT DEI.TableName AS TableName, 
									DEI.FldName AS FldName,
									DEI.FinalName AS FinalName, 
									MAX(DEI.DataType) AS DataType, 
									DEI.StubsAmountOccured AS StubsAmountOccured
							FROM	#DEInfoFieldsExpanded DEI 
							WHERE	DEI.TableType = 0  
							GROUP BY 
									DEI.TableName,
									DEI.FldName,
									DEI.Finalname,
									DEI.StubsAmountOccured
						) SDEI;
						
				IF @@ROWCOUNT > 0
				BEGIN
					--clean up the SQL command
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
					--now alter the temp table

					--SELECT 'ALTER TABLE #pivotStubInfo Add '  -- DEBUG ONLY
					--PRINT @SQLCommand						-- DEBUG ONLY	

					EXEC(@SQLCommand); 
				END

				/* Now pivot the data entry information into the correct temp table. */
				SET @SQLCommand = 'INSERT INTO #pivotCheckInfo(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,';
				
				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.FinalName AS FinalName,
							DEI.DataType AS DataType,
							DEI.StubsAmountOccured AS StubsAmountOccured
					FROM	#DEInfoFields DEI
					WHERE	
							TableType  = 1
							--AND DisplayFinal = 1  -- eliminate the Where columns  MGE IT's too soon to eliminate them
							AND UPPER(REPLACE(REPLACE(DEI.FldName, '[', ''), ']', '')) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL')
				)
				SELECT	@SQLCommand = @SQLCommand + FinalName + ','
				FROM DBColumns;
				
				SET @SQLCommand = @SQLCommand + '
						BatchSequence,StubSequence)';
				
				SET @SQLCommand = @SQLCommand + '
				SELECT 	#tmpDEResults.BankKey,
						#tmpDEResults.OrganizationKey,
						#tmpDEResults.ClientAccountKey,
						#tmpDEResults.ImmutableDateKey,
						#tmpDEResults.DepositDateKey,
						#tmpDEResults.BatchID,
						#tmpDEResults.TransactionID,';

				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.FinalName AS FinalName,
							DEI.DisplayName AS DisplayName,
							DEI.DataType AS DataType,
							DEI.StubsAmountOccured AS StubsAmountOccured
					FROM	#DEInfoFields DEI
					WHERE	TableType = 1
							--AND DisplayFinal = 1  -- eliminate the Where columns	MGE IT's too soon to eliminate them
							AND UPPER(REPLACE(REPLACE(DEI.FldName, '[', ''), ']', '')) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL')
				)
				SELECT  @SQLCommand = @SQLCommand + 
						' MIN(CASE WHEN #DEInfoFieldsExpanded.TableName='+QUOTENAME(TableName,CHAR(39))+' AND #DEInfoFieldsExpanded.FldName='+QUOTENAME(FldName,CHAR(39))+' AND #DEInfoFieldsExpanded.DisplayName='+QUOTENAME(DisplayName,CHAR(39))+' THEN '
							 +CASE DataType
								WHEN 1 THEN '#tmpDEResults.DataEntryValue '
								WHEN 6 THEN 
									CASE StubsAmountOccured 
												WHEN 1 THEN 'RecHubData.#tmpDEResults.DataEntryValueMoney '
												ELSE  'CAST(#tmpDEResults.DataEntryValue AS FLOAT) '
									END
								
								WHEN 7 THEN 'RecHubData.#tmpDEResults.DataEntryValueMoney '
								WHEN 11 THEN 'RecHubData.#tmpDEResults.DataEntryValueDateTime '
							END+'ELSE NULL END) AS  '+FinalName+','
				FROM DBColumns;
				--IF LEN(@SQLCommand) > 0				--MGE 3/5/2011	
				--	SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1)	--MGE 3/5/2011
				--Added DepositDateKey filter below									--MGE 3/9/2011
				SELECT  @SQLCommand = @SQLCommand +  '#tmpDEResults.BatchSequence,#tmpDEResults.StubSequence
				 FROM	#tmpDEResults
						INNER JOIN #DEInfoFieldsExpanded ON #tmpDEResults.WorkgroupDataEntryColumnKey = #DEInfoFieldsExpanded.WorkgroupDataEntryColumnKey
							AND #DEInfoFieldsExpanded.TableType = 1
				GROUP BY #tmpDEResults.BankKey,#tmpDEResults.OrganizationKey,#tmpDEResults.ClientAccountKey,#tmpDEResults.ImmutableDateKey,#tmpDEResults.DepositDateKey,#tmpDEResults.BatchID,#tmpDEResults.TransactionID,#tmpDEResults.BatchSequence,#tmpDEResults.StubSequence
				';		
				SET @SQLCommand = @SQLCommand + '				
				OPTION (RECOMPILE, MAXDOP 1)';											--MGE Prevent Parallelism 3/19/12
				EXEC(@SQLCommand);

				--PRINT @SQLCommand										--DEBUG ONLY	--MGE 3/8/12
				--SELECT '#pivotCheckInfo ', * FROM #pivotCheckInfo		--DEBUG ONLY

				SET @SQLCommand = 'INSERT INTO #pivotStubInfo(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,';
				
				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.FinalName AS FinalName,
							DEI.DataType AS DataType
					FROM	#DEInfoFields DEI
					WHERE	TableType = 0
							--AND DisplayFinal = 1  -- eliminate the Where columns	MGE too soon to eliminate
				)
				SELECT	@SQLCommand = @SQLCommand + FinalName + ','
				FROM DBColumns;
				
				SET @SQLCommand = @SQLCommand + '
						BatchSequence,StubSequence)';
				
				SET @SQLCommand = @SQLCommand + '
				SELECT 	#tmpDEResults.BankKey,
						#tmpDEResults.OrganizationKey,
						#tmpDEResults.ClientAccountKey,
						#tmpDEResults.ImmutableDateKey,
						#tmpDEResults.DepositDateKey,
						#tmpDEResults.BatchID,
						#tmpDEResults.TransactionID,';

				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.FinalName AS FinalName,
							DEI.DisplayName AS DisplayName,
							DEI.DataType AS DataType,
							DEI.StubsAmountOccured AS StubsAmountOccured
					FROM	#DEInfoFields DEI
					WHERE	TableType = 0
							--AND DisplayFinal = 1  -- eliminate the Where columns	MGE too soon to eliminate
				)
				SELECT  @SQLCommand = @SQLCommand + 
						'MIN(CASE WHEN #DEInfoFieldsExpanded.TableName='+QUOTENAME(TableName,CHAR(39))+' AND #DEInfoFieldsExpanded.FldName='+QUOTENAME(FldName,CHAR(39))+' AND #DEInfoFieldsExpanded.DisplayName='+QUOTENAME(DisplayName,CHAR(39))+' THEN '
							 +CASE DataType
								WHEN 1 THEN '#tmpDEResults.DataEntryValue '
								WHEN 6 THEN 
									CASE StubsAmountOccured 
												WHEN 1 THEN '#tmpDEResults.DataEntryValueMoney '
												ELSE  'CAST(#tmpDEResults.DataEntryValue AS FLOAT) '
									END
								WHEN 7 THEN '#tmpDEResults.DataEntryValueMoney '
								WHEN 11 THEN '#tmpDEResults.DataEntryValueDateTime '
							END+'ELSE NULL END) AS  '+FinalName+','
				FROM DBColumns;
							
				SELECT  @SQLCommand = @SQLCommand +  '
						#tmpDEResults.BatchSequence,#tmpDEResults.StubSequence
				FROM	#tmpDEResults
						
						INNER JOIN #DEInfoFieldsExpanded ON #tmpDEResults.WorkgroupDataEntryColumnKey = #DEInfoFieldsExpanded.WorkgroupDataEntryColumnKey
							AND #DEInfoFieldsExpanded.TableType = 0
				GROUP BY #tmpDEResults.BankKey,#tmpDEResults.OrganizationKey,#tmpDEResults.ClientAccountKey,#tmpDEResults.ImmutableDateKey,#tmpDEResults.DepositDateKey,#tmpDEResults.BatchID,#tmpDEResults.TransactionID,#tmpDEResults.BatchSequence,#tmpDEResults.StubSequence
				';

				SET @SQLCommand = @SQLCommand + '				
				OPTION (MAXDOP 1)';											--MGE Prevent Parallelism 3/19/12
				
				EXEC(@SQLCommand);
				SET @iDETransactionCounter = @iDETransactionCounter + 1;

				--PRINT @SQLCommand										--DEBUG ONLY	 
				--SELECT '#pivotStubInfo ', * FROM #pivotStubInfo			--DEBUG ONLY

				SET @SQLCommand = '
				INSERT INTO #tmpDEResultsPivot (BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,';
				
				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.FinalName AS FinalName
					FROM	#DEInfoFields DEI
					WHERE	
							--(DisplayFinal = 1 AND TableType = 0)  --  eliminate the Where columns	MGE Its too soon to eliminate them
							(TableType = 0)							
							--OR (DisplayFinal = 1 AND TableType = 1 AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL'))
							OR (TableType = 1 AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL'))
				)
				SELECT @SQLCommand = @SQLCommand + FinalName+','
				FROM DBColumns;
				
				SET @SQLCommand = @SQLCommand + 'BatchSequence,StubSequence)
				SELECT	COALESCE(#pivotCheckInfo.BankKey,#pivotStubInfo.BankKey) AS BankKey,
						COALESCE(#pivotCheckInfo.OrganizationKey,#pivotStubInfo.OrganizationKey) AS OrganizationKey,
						COALESCE(#pivotCheckInfo.ClientAccountKey,#pivotStubInfo.ClientAccountKey) AS ClientAccountKey,
						COALESCE(#pivotCheckInfo.ImmutableDateKey,#pivotStubInfo.ImmutableDateKey) AS ImmutableDateKey,
						COALESCE(#pivotCheckInfo.DepositDateKey,#pivotStubInfo.DepositDateKey) AS DepositDateKey,
						COALESCE(#pivotCheckInfo.BatchID,#pivotStubInfo.BatchID) AS BatchID,
						COALESCE(#pivotCheckInfo.TransactionID,#pivotStubInfo.TransactionID) AS TransactionID,';
						
				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldNamem,
							DEI.FinalName AS FinalName
					FROM	#DEInfoFields DEI
					WHERE	
							--(DisplayFinal = 1 AND TableType = 0)  --  eliminate the Where columns	MGE Its too soon to eliminate them
							(TableType = 0)							
							--OR (DisplayFinal = 1 AND TableType = 1 AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL'))
							OR (TableType = 1 AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL'))
				)
				SELECT @SQLCommand = @SQLCommand + 
					+CASE UPPER(TableName)
						WHEN 'CHECKS' THEN '#pivotCheckInfo.'
						--WHEN 'CHECKSDATAENTRY' THEN '#pivotCheckInfo.'
						WHEN 'STUBS' THEN '#pivotStubInfo.'
						--WHEN 'STUBSDATAENTRY' THEN '#pivotStubInfo.'
					END+FinalName+','
				FROM DBColumns;
						
				SELECT @SQLCommand = @SQLCommand + '
						COALESCE(#pivotCheckInfo.BatchSequence,#pivotStubInfo.BatchSequence) AS BatchSequence, COALESCE(#pivotCheckInfo.StubSequence,#pivotStubInfo.StubSequence) AS StubSequence
				FROM #pivotCheckInfo
					FULL OUTER JOIN #pivotStubInfo ON #pivotStubInfo.BankKey = #pivotCheckInfo.BankKey
						AND #pivotStubInfo.OrganizationKey = #pivotCheckInfo.OrganizationKey
						AND #pivotStubInfo.ClientAccountKey = #pivotCheckInfo.ClientAccountKey
						AND #pivotStubInfo.ImmutableDateKey = #pivotCheckInfo.ImmutableDateKey
						AND #pivotStubInfo.DepositDateKey = #pivotCheckInfo.DepositDateKey
						AND #pivotStubInfo.BatchID = #pivotCheckInfo.BatchID
						AND #pivotStubInfo.TransactionID = #pivotCheckInfo.TransactionID
						AND #pivotStubInfo.StubSequence = #pivotCheckInfo.StubSequence
						 ';
				
				SET @SQLCommand = @SQLCommand + '				
				OPTION (RECOMPILE, MAXDOP 1)';								--MGE Prevent Parallelism 3/19/12; Force Recompile 10/8/2015

				--PRINT @SQLCommand											--DEBUG ONLY

				EXEC(@SQLCommand);

				--SELECT '#tmpDEResultsPivot ', * FROM  #tmpDEResultsPivot;	-- DEBUG ONLY

				/*	When muliple items are in the where, we have to do a AND. The process of creating the temp
					table is an OR. Delete all records that do not match the actual request. Only do the next step
					if there are more then 1 item in the where. 
				*/
				--PRINT '@DEWhere COUNT = ' + CAST(@DEWhereCount AS CHAR(2));						--DEBUG ONLY
				--SELECT '#DEWhere input to Expanded DEWhere', * from #DEWhere						--DEBUG ONLY
				IF( @DEWhereCount >= 1 )
				BEGIN
					/********************** Put all matching UILabels in #DEWhere, which isn't used after this section of code - Bug 230509 *****/
					SELECT @MatchingUILabelCount = 
						(
						SELECT COUNT(*)
						FROM #DEWHERE INNER JOIN #DEInfoFields 
								ON #DEInfoFields.OrgDisplayName = #DEWhere.DisplayName
									AND #DEInfoFields.TableType = #DEWhere.TableType
									AND #DEInfoFields.DataType = #DEWhere.DataType
						)
					--PRINT '@MatchingUILabelCount = ' + CAST(@MatchingUILabelCount AS CHAR(2));		--DEBUG ONLY
				IF @MatchingUILabelCount >= 1	
					INSERT INTO #ExpandedDEWhere (TableType, TableName,ColumnName,DisplayName,CombinedName,Operator,BatchSourceKey,StubsAmountOccured, DataType,DataValue)
						SELECT #DEInfoFields.TableType, #DEWhere.TableName, #DEInfoFields.FldName AS ColumnName, #DEInfoFields.OrgDisplayName AS DisplayName, 
							#DEInfoFields.FinalName AS CombinedName, #DEWhere.Operator, #DEInfoFields.BatchSourceKey, #DEWhere.StubsAmountOccured, #DEInfoFields.DataType, 
							#DEWhere.DataValue
							FROM #DEWhere INNER JOIN #DEInfoFields 
									ON #DEInfoFields.OrgDisplayName = #DEWhere.DisplayName
									AND #DEInfoFields.TableType = #DEWhere.TableType
									AND #DEInfoFields.DataType = #DEWhere.DataType
						ORDER BY #DEInfoFields.OrgDisplayName, Operator
					--Select '#ExpandedDEWhere', * from #ExpandedDEWhere								--DEBUG ONLY
				END		-- END IF BEGIN

				--SELECT 'DEInfo used for Expanded DEWhere',* FROM #DEInfoFields				--DEBUG ONLY
				--SELECT 'Expanded DEWhere', * FROM #ExpandedDEWhere							--DEBUG ONLY
				
				INSERT INTO #OrderedExpandedDEWhere (TableType, TableName,ColumnName,DisplayName,CombinedName,Operator,BatchSourceKey,StubsAmountOccured, DataType,DataValue)
				SELECT TableType, TableName, ColumnName, DisplayName, CombinedName, Operator, BatchSourceKey, StubsAmountOccured, DataType, DataValue
				FROM #ExpandedDEWhere
				ORDER BY Operator, DataValue, CombinedName;

				--SELECT * FROM #OrderedExpandedDEWhere										--DEBUG ONLY
								
				/******************************** NOW BUILD BUILD & APPLY THE WHERE CLAUSE TO THE RESULT SET **************/

				SELECT @DEWhereCount = Count(*) FROM #ExpandedDEWhere
				--PRINT '@DEWhereCount = ' + CAST(@DEWhereCount AS CHAR(2));		--DEBUG ONLY
				IF @DEWhereCount >= 1
				BEGIN
					SET @Loop = 1;
					SET @SQLCommand = '(';
					SELECT 
						@SavedDisplayName = DisplayName,
						@SavedOperator = Operator, 
						@SavedDataValue = DataValue FROM #OrderedExpandedDEWhere WHERE RowID = 1;
					WHILE @Loop <= @DEWhereCount
					BEGIN
						IF @Loop > 1
						BEGIN
							IF (SELECT DisplayName FROM #OrderedExpandedDEWhere WHERE RowID = @Loop) = @SavedDisplayName
							AND (SELECT Operator FROM #OrderedExpandedDEWhere WHERE RowID = @Loop) = @SavedOperator
							AND (SELECT DataValue FROM #OrderedExpandedDEWhere WHERE RowID = @Loop) = @SavedDataValue
									SELECT @SQLCommand=@SQLCommand +' OR ' 
								ELSE
									SELECT @SQLCommand=@SQLCommand + ') AND (';
							SELECT 
								@SavedDisplayName = DisplayName,
								@SavedOperator = Operator, 
								@SavedDataValue = DataValue
							FROM #OrderedExpandedDEWhere 
							WHERE RowID = @Loop;
						END
						SELECT	@SQLCommand=@SQLCommand + CombinedName
							+CASE Operator
								WHEN 'Equals' THEN ' = ' 
									+CASE DataType 
										WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
										WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
										ELSE DataValue 
									END /* Can be numeric data types, but alpha/date still need ticks */
								WHEN 'Is Greater Than' THEN ' > ' 
									+CASE DataType 
										WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
										WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
										ELSE DataValue 
									END /* Can be numeric data types, but alpha/date still need ticks */
								WHEN 'Is Less Than' THEN ' < ' 
									+CASE DataType 
										WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
										WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
										ELSE DataValue 
									END /* Can be numeric data types, but alpha/date still need ticks */
								WHEN 'Begins With' THEN ' LIKE '+QUOTENAME(DataValue+'%',CHAR(39))
								WHEN 'Contains' THEN ' LIKE '+QUOTENAME('%'+DataValue+'%',CHAR(39))
								WHEN 'Ends With' THEN ' LIKE '+QUOTENAME('%'+DataValue,CHAR(39))
							END			--END CASE
						FROM #ExpandedDEWhere WHERE RowID = @Loop;
						SET @Loop = @Loop + 1;

					--PRINT @Loop;												--DEBUG ONLY
					--PRINT @SavedDisplayName;									--DEBUG ONLY
					--PRINT @SQLCommand;										--DEBUG ONLY

					END		-- END WHILE
					SET @SQLCommand = @SQLCommand +')'
					SET @SQLCommand = 'DELETE FROM #tmpDEResultsPivot WHERE RowID NOT IN (SELECT RowID FROM #tmpDEResultsPivot WHERE ' + @SQLCommand + ')';

					--Print 'Delete command from #tmpDEResultsPivot - This is result of Where clause '	-- Debug only
					--Print @SQLCommand																	-- Debug Only

					EXEC(@SQLCommand);
				END			--END IF BEGIN
				--remove any none matching records from the #tmpMatchingResults table since we do a left join to this table later
				/* CR 51512 JPB 03/25/12 - only remove if there were DE items included in the WHERE clause */
				IF( @DEWhereCount > 0 ) 
					DELETE 
					FROM	#tmpMatchingResults 
					WHERE	NOT EXISTS
							(
								SELECT	BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID
								FROM	#tmpDEResultsPivot
								WHERE	#tmpMatchingResults.BankKey = #tmpDEResultsPivot.BankKey
										AND #tmpMatchingResults.OrganizationKey = #tmpDEResultsPivot.OrganizationKey
										AND #tmpMatchingResults.ClientAccountKey = #tmpDEResultsPivot.ClientAccountKey
										AND #tmpMatchingResults.ImmutableDateKey = #tmpDEResultsPivot.ImmutableDateKey
										AND #tmpMatchingResults.DepositDateKey = #tmpDEResultsPivot.DepositDateKey
										AND #tmpMatchingResults.BatchID = #tmpDEResultsPivot.BatchID
										AND #tmpMatchingResults.TransactionID = #tmpDEResultsPivot.TransactionID
							);
			END	/* IF( @iProcessDEData < @factDataEntryDetailsMaxRowCount ) */
			ELSE
			BEGIN 
				IF( @iProcessDEData >= @factDataEntryDetailsMaxRowCount )
					SET @TooManyRows = 1;
				ELSE
				BEGIN
					/* remove any none matching records from the #tmpMatchingResults table since we do a left join to this table later */
					/* but not if there are no rows in #tmpMatchingResults         MGE 03/21/2012                                      */
					IF @iProcessDEData > 0
					BEGIN
						DELETE 
						FROM	#tmpMatchingResults 
						WHERE	NOT EXISTS
								(
									SELECT	BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID
									FROM	#tmpDEResultsPivot
									WHERE	#tmpMatchingResults.BankKey = #tmpDEResultsPivot.BankKey
											AND #tmpMatchingResults.OrganizationKey = #tmpDEResultsPivot.OrganizationKey
											AND #tmpMatchingResults.ClientAccountKey = #tmpDEResultsPivot.ClientAccountKey
											AND #tmpMatchingResults.ImmutableDateKey = #tmpDEResultsPivot.ImmutableDateKey
											AND #tmpMatchingResults.DepositDateKey = #tmpDEResultsPivot.DepositDateKey
											AND #tmpMatchingResults.BatchID = #tmpDEResultsPivot.BatchID
											AND #tmpMatchingResults.TransactionID = #tmpDEResultsPivot.TransactionID
								);
					END
				END
			END 
		END /*  WI 123202*/
		/* Check the total number of rows that are in the matching result set */
		IF( (SELECT COUNT(*) FROM #tmpMatchingResults) > @totalMatchMaxRowCount )
			BEGIN
				SET @TooManyRows = 1;
				--Set @ActualMatchRows = (select count (*) FROM #tmpMatchingResults)
				--print 'Actual Matching Rows: '
				--print @ActualMatchRows
				--print 'Max Matching Rows: '
				--Print @totalMatchMaxRowCount
				--(select COUNT(*), @totalMatchMaxRowCount FROM #tmpMatchingResults)		-- DEBUG ONLY MGE 03/19/2012	
			END
	
		IF( @TooManyRows = 1 )
		BEGIN
			SELECT @iSRRecordCount = -99, @iSRDocumentCount = -99, @iSRCheckCount = -99, @mSRCheckTotal = 0.00;
		END
		ELSE
		BEGIN		
			--This is the total information before the result sets are joined
			SELECT	@iSRDocumentCount = SUM(COALESCE(DocumentCount,0))
			FROM	#tmpMatchingResults_TEMP;
			/* CR 51512 JPB 03/25/12 - Get the Check count */
			;WITH CheckInfo AS
			(
				SELECT	DISTINCT BankKey,ClientAccountKey,OrganizationKey,DepositDateKey,ImmutableDateKey,BatchID,TransactionID,BatchSequence,
						COALESCE(CheckCount,0) AS CheckCount,
						COALESCE(CheckAmount,0.00) AS CheckAmount --CR 29963
				FROM	#tmpMatchingResults
				WHERE	CheckCount <> 0
			)
			SELECT	@iSRCheckCount = COALESCE(COUNT(*),0),
					@mSRCheckTotal = SUM(COALESCE(CheckAmount,0.00)) --CR 29963
			FROM	CheckInfo;
			
			--COTS
			IF @iDisplayCOTSOnly = 1
				DELETE FROM #tmpMatchingResults WHERE CheckCount <> 0;
				
			--this is the total number of available to the user
			SELECT	@iSRRecordCount = COUNT(*)
			FROM	#tmpMatchingResults
					LEFT JOIN #tmpDEResultsPivot ON 
							#tmpDEResultsPivot.BankKey = #tmpMatchingResults.BankKey
							AND #tmpDEResultsPivot.OrganizationKey = #tmpMatchingResults.OrganizationKey
							AND #tmpDEResultsPivot.ClientAccountKey = #tmpMatchingResults.ClientAccountKey
							AND #tmpDEResultsPivot.ImmutableDateKey = #tmpMatchingResults.ImmutableDateKey
							AND #tmpDEResultsPivot.DepositDateKey = #tmpMatchingResults.DepositDateKey
							AND #tmpDEResultsPivot.BatchID = #tmpMatchingResults.BatchID
							AND #tmpDEResultsPivot.TransactionID = #tmpMatchingResults.TransactionID
							AND #tmpDEResultsPivot.StubSequence = #tmpMatchingResults.StubsStubSequence;

			--Use a CTE to create a sorted record set with server side paging
			--Adapted from http://www.sqlservercentral.com/articles/Advanced+Querying/3181/
			SET @SQLCommand = ';with FinalResultSet AS ( SELECT ';
			IF LEN(@vcSortBy) > 0
			BEGIN 
				IF UPPER(@vcSortBy) = 'BATCHDEPOSITDATE' -- some of the columns will be 'special' and have to be accounted for
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.DepositDateKey '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBy = 'BATCHBATCHID'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.SourceBatchID '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBy = 'BATCHSOURCEBATCHID'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.SourceBatchID '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBy = 'BATCHBATCHNUMBER'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.BatchNumber '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF UPPER(@vcSortBy) = 'TRANSACTIONSTRANSACTIONID'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.TransactionID '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF UPPER(@vcSortBy) = 'TRANSACTIONSTXNSEQUENCE'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY TxnSequence '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF UPPER(@vcSortBy) = 'CHECKSAMOUNT'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY CheckAmount '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBy = 'CHECKSSERIAL'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY numeric_serial '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBy = 'CHECKSRT'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY numeric_rt '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBy = 'CHECKSACCOUNT'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY Account ' + @vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBy = 'CHECKSREMITTERNAME'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY RemitterName ' + @vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBY = 'BATCHPAYMENTSOURCE' -- WI 90081
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY RecHubData.dimBatchSources.LongName '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBY = 'BATCHPAYMENTTYPE' -- WI 90081
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY RecHubData.dimBatchPaymentTypes.LongName '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
				ELSE IF @vcSortBy = 'CHECKSTRANSACTIONCODE'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY TransactionCode' +@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
			ELSE
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY '+@vcSortBy+' '+@vcSortDirection+',#tmpMatchingResults.BankID,#tmpMatchingResults.ClientAccountID,#tmpMatchingResults.DepositDateKey,#tmpMatchingResults.ImmutableDateKey,#tmpMatchingResults.SourceBatchID,#tmpMatchingResults.TransactionID,TxnSequence,#tmpMatchingResults.StubsStubSequence) AS RecID,';
			END
			ELSE 
			BEGIN
				SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.BankKey '+@vcSortDirection+',
														#tmpMatchingResults.OrganizationKey,
														#tmpMatchingResults.ClientAccountKey,
														#tmpMatchingResults.DepositDateKey,
														#tmpMatchingResults.ImmutableDateKey,
														#tmpMatchingResults.SourceBatchID,
														#tmpMatchingResults.TransactionID,
														TxnSequence,
														#tmpMatchingResults.StubsStubSequence) AS RecID,';
			END

			SET @SQLCommand = @SQLCommand + 
				'	BankID,
					ClientAccountID,		
					CONVERT(VARCHAR(10),#tmpMatchingResults.DepositDateKey, 101) AS Deposit_Date,
					RecHubData.dimBatchSources.LongName AS PaymentSource,
					RecHubData.dimBatchPaymentTypes.LongName AS PaymentType,
					#tmpMatchingResults.BatchID,
					#tmpMatchingResults.SourceBatchID,
					#tmpMatchingResults.BatchNumber,
					#tmpMatchingResults.ImmutableDateKey AS PICSDate,
					#tmpMatchingResults.TransactionID,
					#tmpMatchingResults.TxnSequence,
					CheckAmount,
					SerialNumber,
					numeric_serial,
					#tmpMatchingResults.numeric_rt,
					#tmpMatchingResults.DDA,
					dimBatchSources.ShortName AS BatchSourceShortName,
					dimImportTypes.ShortName AS ImportTypeShortName,';
			IF @vcSortBy = 'CHECKSRT' OR @vcSortBy = 'CHECKSACCOUNT' OR 	@vcSortBy = 'CHECKSREMITTERNAME'
				SET @SQLCommand = @SQLCommand +	'Account,
					#tmpMatchingResults.RoutingNumber,
					#tmpMatchingResults.RemitterName,';
			SET @SQLCommand = @SQLCommand +' #tmpMatchingResults.BatchSequence,
					CheckCount,
					DocumentCount,
					StubCount,
					#tmpMatchingResults.OMRCount AS MarkSenseDocumentCount,
					ChecksCheckSequence,
					StubsStubSequence,
					#tmpMatchingResults.BankKey,
					#tmpMatchingResults.OrganizationKey,
					#tmpMatchingResults.ClientAccountKey,
					#tmpMatchingResults.ImmutableDateKey,
					#tmpMatchingResults.DepositDateKey ';
			
			IF LEN(@vcDEColumnNames) > 0
				SET @SQLCommand = @SQLCommand + ', ' + @vcDEColumnNames;
				
			SET @SQLCommand = @SQLCommand + ' 
			FROM	#tmpMatchingResults 
					LEFT JOIN #tmpDEResultsPivot ON #tmpDEResultsPivot.BankKey = #tmpMatchingResults.BankKey
						AND #tmpDEResultsPivot.OrganizationKey = #tmpMatchingResults.OrganizationKey
						AND #tmpDEResultsPivot.ClientAccountKey = #tmpMatchingResults.ClientAccountKey
						AND #tmpDEResultsPivot.ImmutableDateKey = #tmpMatchingResults.ImmutableDateKey
						AND #tmpDEResultsPivot.DepositDateKey = #tmpMatchingResults.DepositDateKey
						AND #tmpDEResultsPivot.BatchID = #tmpMatchingResults.BatchID
						AND #tmpDEResultsPivot.TransactionID = #tmpMatchingResults.TransactionID
						AND #tmpDEResultsPivot.StubSequence = #tmpMatchingResults.StubsStubSequence
					INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = #tmpMatchingResults.BatchSourceKey
					INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
					INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = #tmpMatchingResults.BatchPaymentTypeKey';
			
			
			SET @SQLCommand = @SQLCommand + ') SELECT BankID,
					ClientAccountID,
					CONVERT(VARCHAR(10),CAST(CONVERT(VARCHAR(10),Deposit_Date, 101) AS DATETIME),101) AS Deposit_Date, 	
					PaymentSource,
					PaymentType,
					FinalResultSet.BatchID,
					FinalResultSet.SourceBatchID,
					FinalResultSet.BatchNumber,
					PICSDate,
					FinalResultSet.TransactionID,
					FinalResultSet.TxnSequence,
					CheckAmount AS ' + @vcCheckAmountCN + ',
					SerialNumber AS ' + @vcCheckSerialNumberCN + ',
					RecHubData.factChecks.TransactionCode AS ChecksTransactionCode,
					numeric_serial,
					RecHubData.factChecks.Account AS ' + @vcCheckAccountNumberCN + ',
					RecHubData.factChecks.RoutingNumber AS ' + @vcCheckRTCN + ',
					numeric_rt,
					DDA,
					BatchSourceShortName,
					ImportTypeShortName,
					RecHubData.factChecks.RemitterName AS ChecksRemitterName,
					FinalResultSet.BatchSequence,
					CheckCount,
					DocumentCount,
					StubCount,
					MarkSenseDocumentCount,
					ChecksCheckSequence'  
					--StubsStubSequence'	-- WI 71714  this is here but leaving out until requested to include in results
					-- This needs to be fixed to work with the new image tables. ImageInfoXML.value(' + CHAR(39) + '(Images/Image/@FileSize)[1]' + CHAR(39) + ', ' + CHAR(39) + 'int' + CHAR(39) + ') AS ImageSize ';

			IF LEN(@vcDBColumnsWithCast) > 0
				SET @SQLCommand = @SQLCommand + ', ' + @vcDBColumnsWithCast;
			SET @SQLCommand = @SQLCommand + ' 
			FROM	FinalResultSet 
					LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = FinalResultSet.DepositDateKey
						AND RecHubData.factChecks.BatchID = FinalResultSet.BatchID
						AND RecHubData.factChecks.TransactionID = FinalResultSet.TransactionID 
						AND (RecHubData.factChecks.CheckSequence = FinalResultSet.ChecksCheckSequence 
							OR FinalResultSet.ChecksCheckSequence is Null)
						AND RecHubData.factChecks.IsDeleted = 0 ';

			IF @iPaginateRS = 0
				SET @SQLCommand = @SQLCommand + 'WHERE RecID >= ' + CAST(@iStartRecord AS VARCHAR(10));
			ELSE SET @SQLCommand = @SQLCommand + 'WHERE RecID BETWEEN ' + CAST(@iStartRecord AS VARCHAR(10)) + ' AND ' + CAST((@iStartRecord+@iRecordsPerPage-1) AS VARCHAR(10));
			SET @SQLCommand = @SQLCommand + ' ORDER BY RecID';

			--Print 'Final command For FinalResultSet'    	--	Debug only
			--Print @SQLCommand								--	Debug Only

			EXEC(@SQLCommand);
			IF @@ROWCOUNT = 0 -- no rows are actually returned, so 0 out the values
				SELECT @iSRRecordCount = 0, @iSRDocumentCount = 0, @iSRCheckCount = 0, @mSRCheckTotal = 0.00;
		END
		--final info needed for the return XML
		-- WI 125457 Changed Where clause to only exclude reserved names from the Checks table
		UPDATE #DEInfoFields
			SET FldName = REPLACE(REPLACE(FldName,'[',''),']','');		-- WI 194258 Removing before we send back
		
		SET @parmSearchTotals = 
			(
				SELECT	'Results' AS "@Name",
						@iSRRecordCount AS "@TotalRecords",
						@iSRDocumentCount AS "@DocumentCount",
						@iSRCheckCount AS "@CheckCount",
						@mSRCheckTotal AS "@CheckTotal",
						(
							SELECT	TableName AS '@tablename',
									FldName AS '@fieldname',
									DataType AS '@datatype',
									--REPLACE(DisplayName,CHAR(39),CHAR(39)+CHAR(39)) AS '@reporttitle',
									OrgDisplayName AS '@reporttitle',
									RIGHT('0'+CAST(RowID AS VARCHAR),3) AS '@ColID'		-- WI194934 for 100+ columns
							FROM #DEInfoFields
							WHERE (UPPER(TableName) = 'STUBS' OR
								(UPPER(TableName) = 'CHECKS' AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL','TRANSACTIONCODE')))
								AND DisplayFinal = 1
							FOR XML PATH('field'), ROOT('SelectFields'), TYPE
						)
				FOR XML PATH('RecordSet'), ROOT('Page') 
			);
	END	/* Records found */
	ELSE
	BEGIN /* No records found */
		UPDATE #DEInfoFields
			SET FldName = REPLACE(REPLACE(FldName,'[',''),']','');		-- WI 194258 Removing before we send back
		
		SELECT @iSRRecordCount = 0, @iSRDocumentCount = 0, @iSRCheckCount = 0, @mSRCheckTotal = 0.00;
		-- WI 125457 Changed Where clause to only exclude reserved names from the Checks table
		SET @parmSearchTotals = 
		(
			SELECT	'Results' AS "@Name",
					@iSRRecordCount AS "@TotalRecords",
					@iSRDocumentCount AS "@DocumentCount",
					@iSRCheckCount AS "@CheckCount",
					@mSRCheckTotal AS "@CheckTotal",
					(
						SELECT	TableName AS '@tablename',
								FldName AS '@fieldname',
								DataType AS '@datatype',
								OrgDisplayName AS '@reporttitle',
								RIGHT('0'+CAST(RowID AS VARCHAR),3) AS '@ColID'		-- WI194934 for 100+ columns
						FROM #DEInfoFields
						WHERE (UPPER(TableName) = 'STUBS' OR
							(UPPER(TableName) = 'CHECKS' AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL','TRANSACTIONCODE')))
							AND DisplayFinal = 1
						FOR XML PATH('field'), ROOT('SelectFields'), TYPE
					)
			FOR XML PATH('RecordSet'), ROOT('Page') 
		);
	END
	
	--Drop the temp tables
	DROP TABLE #tmpMatchingResults;
	DROP TABLE #tmpMatchingResults_TEMP;
	DROP TABLE #tmpDEResults;
	DROP TABLE #tmpDEResultsPivot;
	DROP TABLE #pivotCheckInfo;
	DROP TABLE #pivotStubInfo;
	DROP TABLE #DEInfoFields;
	DROP TABLE #DEInfoFieldsExpanded;
	DROP TABLE #DEWhere;
	DROP TABLE #OrderedExpandedDEWhere;
	DROP TABLE #tmpDimensions;	
END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults')) 
		DROP TABLE #tmpMatchingResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults_TEMP')) 
		DROP TABLE #tmpMatchingResults_TEMP;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDEResults')) 
		DROP TABLE #tmpDEResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDEResultsPivot')) 
		DROP TABLE #tmpDEResultsPivot;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#pivotCheckInfo')) 
		DROP TABLE #pivotCheckInfo;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#pivotStubInfo')) 
		DROP TABLE #pivotStubInfo;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEInfoFields')) 
		DROP TABLE #DEInfoFields;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEInfoFieldsExpanded')) 
		DROP TABLE #DEInfoFieldsExpanded;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEWhere')) 
		DROP TABLE #DEWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#ExpandedDEWhere')) 
		DROP TABLE #ExpandedDEWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#OrderedExpandedDEWhere')) 
		DROP TABLE #OrderedExpandedDEWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDimensions')) 
		DROP TABLE #tmpDimensions;
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
