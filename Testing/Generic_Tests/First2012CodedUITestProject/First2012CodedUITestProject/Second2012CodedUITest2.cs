﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace First2012CodedUITestProject
{
    /// <summary>
    /// Summary description for CodedUITest2
    /// </summary>
    [CodedUITest]
    public class Second2012CodedUITest2
    {
        public Second2012CodedUITest2()
        {
        }

        // The following is used for DataDriven Coded UI Tests for this Example is SQL Server 2008 R2
       // [DataSource("System.Data.SqlClient", "Data Source=B4HG5S1;Initial Catalog=OLTA_Test_Database2;Integrated Security=True", "FirstDataCodedUITbl01", DataAccessMethod.Sequential), TestMethod]

        //[TestMethod]
        //[TestCategory("CodedUITests"), TestCategory("Daily"), TestCategory("Bing")]


        public void SecondCodedUITest02DBMethod1()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463

            //this.UIMap.SecondRecordedMethod2Params.UIGoogleWindowsInterneWindowUrl = TestContext.DataRow["URL"].ToString();
            //this.UIMap.SecondRecordedMethod2Params.UIEnteryoursearchtermEditText = TestContext.DataRow["SearchFld01"].ToString();

            
            this.UIMap.SecondRecordedMethod2();

        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize()
        {        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
            //BrowserWindow.CurrentBrowser = "Firefox";  // Note - This will default to your current version of FireFox in your Default Profile.
            //BrowserWindow.CurrentBrowser = "chrome";
            //BrowserWindow.CurrentBrowser = "IE";
            BrowserWindow.CurrentBrowser = "Firefox 3.5.7";
            //BrowserWindow.CurrentBrowser = "Firefox 3.6.28 (en-US)";
            //BrowserWindow.CurrentBrowser = "Firefox 12.0 (en-US)";

        }

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //    // For more information on generated code, see http://go.microsoft.com/fwlink/?LinkId=179463
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
