## This will create the WfsApps\ folder structure under c:\

$mainPath = "c:\WfsApps\"
if (!(Test-Path -Path $mainPath )) {
        New-Item -ItemType directory -Path $mainPath
}

$subPath = "bin\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "Framework\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "Logs\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "ProcessXMLDirectory\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\bin\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\bin2\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\Attachments\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ACHImport\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ACHImport\00_Input\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ACHImport\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ACHImport\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ACHImport\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ACHImport\04_Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ACHImport\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ACHImport\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

######
$subPath = "RecHub\Data\DataImport\Generic\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Generic\00 Input\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Generic\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Generic\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Generic\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Generic\04_Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Generic\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Generic\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

#######

$subPath = "RecHub\Data\DataImport\ICONBatchImport_00\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONBatchImport_00\00_Input\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONBatchImport_00\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONBatchImport_00\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONBatchImport_00\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONBatchImport_00\04_Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONBatchImport_00\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONBatchImport_00\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

###
$subPath = "RecHub\Data\DataImport\ICONClientSetupImport_00\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONClientSetupImport_00\00_Input\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONClientSetupImport_00\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONClientSetupImport_00\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONClientSetupImport_00\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONClientSetupImport_00\04_Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONClientSetupImport_00\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\ICONClientSetupImport_00\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

###

$subPath = "RecHub\Data\DataImport\Wire\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Wire\00_Input\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Wire\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Wire\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Wire\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Wire\04 Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Wire\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\DataImport\Wire\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

##

$subPath = "RecHub\Data\EDM\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\EDM\Definitions\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

## 

$subPath = "RecHub\Data\FileImport\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\FileImport\IntegraPAYFileImport_00\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\FileImport\IntegraPAYFileImport_00\00_Input"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\IntegraPAYFileImport_00\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\IntegraPAYFileImport_00\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\IntegraPAYFileImport_00\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\IntegraPAYFileImport_00\04_Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\IntegraPAYFileImport_00\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\IntegraPAYFileImport_00\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

####

$subPath = "RecHub\Data\FileImport\ExtractDrop_00\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\FileImport\ExtractDrop_00\00_Input"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\ExtractDrop_00\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\ExtractDrop_00\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\ExtractDrop_00\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\ExtractDrop_00\04_Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\ExtractDrop_00\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\ExtractDrop_00\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

####

$subPath = "RecHub\Data\FileImport\RPSFileImport_00\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\FileImport\RPSFileImport_00\00_Input"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\RPSFileImport_00\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\RPSFileImport_00\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\RPSFileImport_00\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\RPSFileImport_00\04_Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\RPSFileImport_00\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\RPSFileImport_00\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

###
$subPath = "RecHub\Data\FileImport\Samples\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

##
$subPath = "RecHub\Data\FileImport\SimpleFileDrop_00\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\Data\FileImport\SimpleFileDrop_00\00_Input"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\SimpleFileDrop_00\01_InProcess\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\SimpleFileDrop_00\02_PendingResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\SimpleFileDrop_00\03_Response\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\SimpleFileDrop_00\04_Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\SimpleFileDrop_00\98_InputError\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
$subPath = "RecHub\Data\FileImport\SimpleFileDrop_00\99_ErrorResponse\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

##
$subPath = "RecHub\Data\LogFiles\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

##
$subPath = "RecHub\DitDataGenerator\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\DitDataGenerator\Archive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\DitDataGenerator\ICONBatchImport 00"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}


##
$subPath = "RecHub\ExtractDesignManager\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "RecHub\ExtractDesignManager\bin2\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}


$subPath = "RecHub\ExtractDesignManager\image\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}


$subPath = "RecHub\ExtractDesignManager\Logs"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}


$subPath = "RecHub\ExtractDesignManager\Output\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}


$subPath = "RecHub\ExtractDesignManager\XML\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

###

$subPath = "RecHub\Img\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

##

$subPath = "RecHub\ini\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

##

$subPath = "RecHub\logs\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}
##
$subPath = "RecHub\OnlineArchive\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

##
$subPath = "SaveXMLFileDirectory\"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}

$subPath = "Utilities"
if (!(Test-Path -Path "$mainPath$subPath")) {
        New-Item -ItemType directory -Path "$mainPath$subPath"
}





