﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR  45986 WJS 10/14/2011 Initial Release
******************************************************************************/
namespace FiServImageTestApp
{
    public partial class CustomImageTestApp : Form
    {
        public CustomImageTestApp()
        {
            InitializeComponent();
        }

        private void GetImage_Click(object sender, EventArgs e)
        {
            DateTime pProcessingDate = new DateTime(); // TODO: Initialize to an appropriate value
            int pLbxNo = 0; // TODO: Initialize to an appropriate value
            int pBatchNo = 0; // TODO: Initialize to an appropriate value
            int pBankId = 0; // TODO: Initialize to an appropriate value
            int pLocId = 0; // TODO: Initialize to an appropriate value
            int pTransaction = 0; // TODO: Initialize to an appropriate value
            int pBatchSeqNo = 0; // TODO: Initialize to an appropriate value
            RetrievalObjectClass[] actual;
            
            FiServCustomImageService client = new FiServCustomImageService("http://localhost:60880/FiServCustomImageService.svc");
           
            actual = client.RetrieveImage(pProcessingDate, pLbxNo, pBatchNo, pBankId, pLocId, pTransaction, pBatchSeqNo);

            StringBuilder message = new StringBuilder();
            message.AppendFormat("Got a image back with an imageName {0}. A imageformat of {1}. A Batch Sequence of {2}. A imageType of {3}", actual[0].ImageFilePath, actual[0].ImageFormat, actual[0].BatchSeqNo, actual[0].ImageType);
            MessageBox.Show(message.ToString());

        }
    }
}
