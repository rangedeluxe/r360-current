﻿param
(
	[parameter(Mandatory = $true)][string] $DBServer,
	[parameter(Mandatory = $true)][string] $DBName,
	[parameter(Mandatory = $true)][int] $BankID,
	[parameter(Mandatory = $true)][int] $LockboxID,
	[parameter(Mandatory = $true)][int] $BatchID,
	[parameter(Mandatory = $true)][datetime] $ProcessingDate,
    [parameter(Mandatory = $true)][datetime] $DepositDate,
    [parameter(Mandatory = $true)][string] $BatchSource,
	[string] $UserID = $null,
	[string] $Password = $null
)

################################################################################
################################################################################
#
#
# NOT TO BE RELEASED OUTSIDE OF R360 Dev/QA.
#
#
################################################################################
################################################################################

################################################################################
# 04/02/2019 R360-15995	MGE 0.00	Created.
# 
#
#
#
################################################################################

################################################################################
# Ideally the SQL code in this script would be SPs. However, this is a test data
# generator and will moved from system to system in Dev/QA. Keeping the SQL code
# here will make this easier. 
#
# The SQL statements were tested/debugged in SSMS and simply copied. This
# was done to make it easier to read/troubleshoot/modify.
#################################################################################
cls
#Import-Module -Name $PSScriptRoot\DeadlineRollover_SQL -Force ;

$ScriptName = "DeleteintegraPAYBatch";
$ScriptVerison = "1.00";

################################################################################
# Write message to log file.
################################################################################
function Write-Log([String] $local:LogInformation)
{
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($LogFile) -eq $null )
		{
			$LogFile = Get-Location + "\" + $LogFile;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $LogFile -Encoding unicode -Append;
		}
	}
	end
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $local:LogInformation | Out-File -FilePath $LogFile -Encoding unicode -Append;
	}
}

################################################################################
# Open the database connection
################################################################################
function Create-DatabaseConnection([ref] $local:dbConnection,[string] $local:DBServer,[string] $local:DBName,[string] $local:UserID,[string] $local:Password)
{
	if( $local:UserID.Length -gt 0 )
	{
		$local:dbConnectionOptions = [string]::Format("Data Source={0}; Initial Catalog={1};UId={2};Pwd={3}",$local:DBServer,$local:DBName,$local:UserID,$local:Password);
	}
	else
	{
		$local:dbConnectionOptions = [string]::Format("Data Source={0}; Initial Catalog={1};Integrated Security=SSPI",$local:DBServer,$local:DBName);
	}
	$local:dbConnection.value = New-Object System.Data.SqlClient.SqlConnection($local:dbConnectionOptions);

	#Setup a handler so the print/raiseerror information from the SP is written to the results file.
	#Adapted from http://sqlskills.com/blogs/jonathan/post/Capturing-InfoMessage-Output-%28PRINT-RAISERROR%29-from-SQL-Server-using-PowerShell.aspx
	$handler = [System.Data.SqlClient.SqlInfoMessageEventHandler]{
		param($sender, $event) 
		Write-Log $event.Message 
		if( $event.Message.Contains("Error") -or $event.Message.Contains("failed" ) )
		{
			$Results = $false;
		}
	};
	$local:dbConnection.value.add_InfoMessage($handler);
	$local:dbConnection.value.FireInfoMessageEventOnUserErrors = $true;
}

################################################################################
# Open the database connection
################################################################################
function Open-DatabaseConnection([System.Data.SqlClient.SqlConnection] $local:dbConnection)
{
	try
	{
		$local:dbConnection.Open();
		$Results = $true;
	}
	catch
	{
		Write-Log "Error Opening Database Connection";
		Write-Log $_.Exception.Message;
		$Results = $false;
	}
	return $Results;
}

################################################################################
# ExecuteSQLStatement
################################################################################
function ExecuteSQLStatement([System.Data.SqlClient.SqlConnection] $local:dbConnection,[string] $local:SQL,[ref] $local:SQLResults)
{
	$local:Results = $true;
	$local:dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$local:dbCommand.Connection = $local:dbConnection;
	$local:dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$local:dbCommand.CommandType = [System.Data.CommandType]::Text;
	$local:dbCommand.CommandText = $local:SQL;
	try
	{
		$local:SQLResults.value = $local:dbCommand.ExecuteNonQuery();
	}
	catch
	{
		$local:Results = $false;
		Write-Log "Error executing SQL Command";
		Write-Log $_.Exception.Message;
	}
	return $local:Results;
}

################################################################################
# Main
################################################################################
[System.Data.SqlClient.SqlConnection] $dbConnection = $null;

[int] $local:SQLResults = 0;

$ScriptPath = [System.IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Definition);
$LogFile = Join-Path -Path $ScriptPath -ChildPath ([string]::Format("{0}_{1}.txt",[System.IO.Path]::GetFilenameWithoutExtension($MyInvocation.MyCommand.Definition),(Get-Date –f yyyyMMddHHmmss)));

Write-Log ([string]::Format("{0} - Version {1}",$ScriptName,$ScriptVerison) );

$StopWatch = [System.Diagnostics.Stopwatch]::StartNew();
Create-DatabaseConnection ([ref]$dbConnection) $DBServer $DBName $UserID $Password;
#Open connection for setup processing
if( (Open-DatabaseConnection $local:dbConnection) )
{
	Write-Log ([string]::Format("Successfully connected to {0}.{1}",$DBServer,$DBName));
	Write-Host ([string]::Format("Successfully connected to {0}.{1}",$DBServer,$DBName));
	Write-Log "Processing integraPAY Batch Delete";
	Write-Host "Processing integraPAY Batch Delete...";
	
    $local:SQL = ([string]::Format("EXECUTE RecHub.usp_CDSQueue_Ins_Batch {0}, {1}, {2}, '{3}', '{4}', 4, {5}", $BankID, $LockboxID, $BatchID, $ProcessingDate, $DepositDate, $BatchSource));
    #Write-Host $local:SQL
    ExecuteSQLStatement $local:dbConnection $local:SQL ([ref]$local:SQLResults) | Out-Null ;
    #Write-Host $local:SQLResults

    if ($local:SQLResults -eq -1)
    {
	    Write-Log "Delete row added to CDSQueue";	
	    Write-Host "Delete row added to CDSQueue";	
	}
    else
    {
        Write-Log "Failed to add delete row to CDSQueue: $local:SQLResults"
        Write-Host "Failed to add delete row to CDSQueue: $local:SQLResults"
    }
	
$dbConnection.Close();
	$StopWatch.Stop();
	$TimeSpan = [TimeSpan]::FromMilliseconds($StopWatch.ElapsedMilliseconds);
	Write-Host ([string]::Format("Completed in {0:00}:{1:00}:{2:00}.{3:000}",$TimeSpan.Hours,$TimeSpan.Minutes,$TimeSpan.Seconds,$TimeSpan.Milliseconds));
}
else
{
	Write-Host ([string]::Format("Could not open connection to {0}.{1}",$DBServer,$DBName));
}