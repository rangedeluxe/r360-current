function UpdateRolloverDatesSQL()
{
return [string] "SET NOCOUNT ON;

;with DatedSchedule AS
(
	SELECT 
		Schedule.BankID, 
		Schedule.LockboxID,
		DATEPART(dw,GETDATE()) AS TodaysDOW,
		Schedule.[DayOfWeek],
		DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] AS DOWDiff,
		CASE 
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = 0 THEN CONVERT(DATE,GETDATE() + 7)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = 1 THEN CONVERT(DATE,GETDATE() + 6)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = 2 THEN CONVERT(DATE,GETDATE() + 5)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = 3 THEN CONVERT(DATE,GETDATE() + 4)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = 4 THEN CONVERT(DATE,GETDATE() + 3)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = 5 THEN CONVERT(DATE,GETDATE() + 2)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = 6 THEN CONVERT(DATE,GETDATE() + 1)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = -1 THEN CONVERT(DATE,GETDATE() + 1)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = -2 THEN CONVERT(DATE,GETDATE() + 2)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = -3 THEN CONVERT(DATE,GETDATE() + 3)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = -4 THEN CONVERT(DATE,GETDATE() + 4)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = -5 THEN CONVERT(DATE,GETDATE() + 5)
			WHEN DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] = -6 THEN CONVERT(DATE,GETDATE() + 6)
		END AS DateToRollTo,
		CONVERT(CHAR(8), ScheduleTime, 108) AS Deadline,
		CONVERT(CHAR(8), GETDATE(), 108) AS CurrentTime

	FROM Lockbox 
	INNER JOIN Schedule ON Schedule.BankID = Lockbox.BankID AND Schedule.LockboxID = Lockbox.LockboxID
	INNER JOIN ScheduleActivities ON ScheduleActivities.ActivityID = Schedule.ActivityID
	AND ScheduleActivities.Description = 'OLDDecisionDeadline'
),
	Batches AS
	(select 
		ProcessingDate, 
		DepositDate, 
		GlobalBatchID, 
		BatchID, 
		Batch.BankID, 
		Batch.LockboxID, 
		DecisionStatus, 
		DepositStatus, 
		Convert(CHAR(8), ScheduleTime, 108) AS TodaysDeadline,
		CONVERT(CHAR(8), GETDATE(), 108) AS TimeOfDay,
		DATEPART(dw,GETDATE()) - Schedule.[DayOfWeek] AS DOWDif,
		Schedule.[DayOfWeek]
	from Batch 
	INNER JOIN Schedule 
		ON Schedule.BankID = Batch.BankID
		AND Schedule.LockboxID = Batch.LockboxID
		AND Schedule.[DayofWeek] = DATEPART(dw,ProcessingDate)
	WHERE DepositStatus IN (240,241)
	AND (CONVERT(DATE,Batch.ProcessingDate) < CONVERT(DATE,GETDATE())) 
		OR (CONVERT(DATE,ProcessingDate) = CONVERT(DATE,GETDATE()) AND (CONVERT(CHAR(8), GETDATE(), 108) >= Convert(CHAR(8), ScheduleTime, 108) ))
	) 
	UPDATE BATCH SET ProcessingDate = DateToRollTo, DepositDate = DateToRollTo 
	FROM Batch
	INNER JOIN Batches ON Batches.GlobalBatchID = Batch.GlobalBatchID
	INNER JOIN DatedSchedule
	ON DatedSchedule.BankID = Batches.BankID
		AND DatedSchedule.LockboxID = Batches.LockboxID
	WHERE DateToRollTo IN (SELECT TOP 1 DateToRollTo FROM DatedSchedule WHERE DateToRollTo > CONVERT(Date,GetDate()) ORDER BY DateToRollTo ASC)"
}