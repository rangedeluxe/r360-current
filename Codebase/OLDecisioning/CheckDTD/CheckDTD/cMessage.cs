using System;

namespace WFS.integraPAY.Online.CheckDTD {

    /// <summary>
    /// 
    /// </summary>
    public enum MessageType {
         Error = 0
        ,Information = 1
        ,Warning = 2
    }


    /// <summary>
    /// 
    /// </summary>
    public enum MessageImportance {
         Essential = 0
        ,Verbose = 1
        ,Debug = 2
    }

    /// <summary>
    /// 
    /// </summary>
    internal class cMessage {

        private string _Message;
        private MessageType _MessageType;
        private MessageImportance _MessageImportance;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageType"></param>
        public cMessage(string message
                      , MessageType messageType
                      , MessageImportance messageImportance) {

            this.message=message;
            this.messageType=messageType;
        }

        /// <summary>
        /// 
        /// </summary>
        public string message {
            get { return(_Message); }
            set { _Message = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public MessageType messageType {
            get { return(_MessageType); }
            set { _MessageType = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public MessageImportance messageImportance{
            get { return(_MessageImportance); }
            set { _MessageImportance = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() {

            const string FORMAT_DATE_TIME_LOGICAL = "yyyy-MM-dd HH:mm:ss";

            string strOutputString=DateTime.Now.ToString(FORMAT_DATE_TIME_LOGICAL);

            switch(this.messageType) {
                case MessageType.Warning:
                    strOutputString+=" WARNING: " + this.message;
                    break;
                case MessageType.Error:
                    strOutputString+=" ERROR: " + this.message;
                    break;
                default:
                    strOutputString+= " " + this.message;
                    break;
            }

            return strOutputString; 
        }
    }
}
