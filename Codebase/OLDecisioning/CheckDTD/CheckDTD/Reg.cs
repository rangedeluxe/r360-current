using System;
using System.Security.Permissions;
using Microsoft.Win32;

namespace WFS.integraPAY.Online.CheckDTD {

	/// <summary>
	/// Summary description for Reg.
	/// </summary>
	public class Reg {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string getSetting(string appName, 
                                        string section, 
                                        string key, 
                                        string defaultValue) {

            string strRetVal="";

            try {

                string strAppKeyName = @"Software\" + appName + @"\" + section;

                // new Microsoft.Win32 Registry Key
                RegistryKey regKey;

                regKey = Registry.CurrentUser.OpenSubKey(strAppKeyName);

                if(regKey==null) {
                    Registry.CurrentUser.CreateSubKey(strAppKeyName);
                    regKey = Registry.CurrentUser.OpenSubKey(strAppKeyName);
                }

                object objValue=regKey.GetValue(key);
                if(objValue != null) {
                    strRetVal=objValue.ToString();
                } else {
                    strRetVal=defaultValue;
                }
            }
            catch(Exception e) {
                Console.WriteLine(e.Message);
                strRetVal=defaultValue;
            }

            return strRetVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appName"></param>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="setting"></param>
        public static bool saveSetting(string appName
                                     , string section
                                     , string key
                                     , string setting) {

            bool bolRetVal=false;

            try {
                string strAppKeyName = @"Software\" + appName + @"\" + section;

                // new Microsoft.Win32 Registry Key
                RegistryKey regKey;

                regKey = Registry.CurrentUser.OpenSubKey(strAppKeyName);

                if(regKey==null) {
                    Registry.CurrentUser.CreateSubKey(strAppKeyName);
                }               
                regKey = Registry.CurrentUser.OpenSubKey(@"Software\" + appName + @"\" + section, true);

                regKey.SetValue(key, setting);
                bolRetVal=true;
            }
            catch(Exception e) {
                Console.WriteLine(e.Message);
                bolRetVal=false;
            }

            return(bolRetVal);
        }
	}
}
