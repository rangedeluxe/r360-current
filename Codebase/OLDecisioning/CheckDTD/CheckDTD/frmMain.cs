using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Xml;
using System.Xml.Schema;

namespace WFS.integraPAY.Online.CheckDTD {

	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form {

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDTDPath;
        private System.Windows.Forms.TextBox txtXmlPath;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.ListBox lstOutput;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFileMask;

        //private bool m_bolCancel = false;
        private bool _InProcess = false;

        private string APP_NAME = "CheckDTD";

        /// <summary>
        /// 
        /// </summary>
		public frmMain() {
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing) {
			if(disposing) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDTDPath = new System.Windows.Forms.TextBox();
            this.txtXmlPath = new System.Windows.Forms.TextBox();
            this.btnGo = new System.Windows.Forms.Button();
            this.lstOutput = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFileMask = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(16, 40);
            this.label1.Name = "label1";
            this.label1.TabIndex = 0;
            this.label1.Text = "DTD Path";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 16);
            this.label2.Name = "label2";
            this.label2.TabIndex = 1;
            this.label2.Text = "Xml Path";
            // 
            // txtDTDPath
            // 
            this.txtDTDPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
                | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDTDPath.Location = new System.Drawing.Point(128, 40);
            this.txtDTDPath.Name = "txtDTDPath";
            this.txtDTDPath.Size = new System.Drawing.Size(360, 20);
            this.txtDTDPath.TabIndex = 1;
            this.txtDTDPath.Text = "";
            // 
            // txtXmlPath
            // 
            this.txtXmlPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
                | System.Windows.Forms.AnchorStyles.Right)));
            this.txtXmlPath.Location = new System.Drawing.Point(128, 16);
            this.txtXmlPath.Name = "txtXmlPath";
            this.txtXmlPath.Size = new System.Drawing.Size(360, 20);
            this.txtXmlPath.TabIndex = 0;
            this.txtXmlPath.Text = "";
            // 
            // btnGo
            // 
            this.btnGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGo.Location = new System.Drawing.Point(504, 32);
            this.btnGo.Name = "btnGo";
            this.btnGo.TabIndex = 3;
            this.btnGo.Text = "Go!";
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // lstOutput
            // 
            this.lstOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstOutput.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.lstOutput.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            this.lstOutput.ItemHeight = 14;
            this.lstOutput.Location = new System.Drawing.Point(0, 0);
            this.lstOutput.Name = "lstOutput";
            this.lstOutput.Size = new System.Drawing.Size(592, 162);
            this.lstOutput.TabIndex = 4;
            this.lstOutput.DoubleClick += new System.EventHandler(this.lstOutput_DoubleClick);
            this.lstOutput.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lstOutput_DrawItem);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtFileMask);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtDTDPath);
            this.panel1.Controls.Add(this.txtXmlPath);
            this.panel1.Controls.Add(this.btnGo);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(592, 104);
            this.panel1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(16, 64);
            this.label3.Name = "label3";
            this.label3.TabIndex = 5;
            this.label3.Text = "File Mask:";
            // 
            // txtFileMask
            // 
            this.txtFileMask.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
                | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileMask.Location = new System.Drawing.Point(128, 64);
            this.txtFileMask.Name = "txtFileMask";
            this.txtFileMask.Size = new System.Drawing.Size(128, 20);
            this.txtFileMask.TabIndex = 2;
            this.txtFileMask.Text = "*.xml";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lstOutput);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 104);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(592, 162);
            this.panel2.TabIndex = 7;
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(592, 266);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmMain";
            this.Text = "DTD Validator";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Closed += new System.EventHandler(this.frmMain_Closed);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			Application.Run(new frmMain());
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGo_Click(object sender, System.EventArgs e) {
            if(!_InProcess) {
                _InProcess = true;
                btnGo.Text = "Cancel";
                Go();
                btnGo.Text = "Go";
                _InProcess = false;
            } else {
                _InProcess = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Go() {

            this.lstOutput.Items.Clear();
            string[] files = System.IO.Directory.GetFiles(this.txtXmlPath.Text, this.txtFileMask.Text);
            string strTempFile;
            TextWriter tw;
            System.Xml.XmlDocument doc;

            foreach(string file in files) {

                if(!_InProcess) {
                    break;
                }

                try {

                    outputMessage("Processing File: " + file, "", MessageType.Information, MessageImportance.Essential);

                    doc = new System.Xml.XmlDocument();
                    doc.Load(file);

                    strTempFile = System.IO.Path.GetTempFileName();
                    tw = new System.IO.StreamWriter(strTempFile, false);    

                    if(doc.DocumentType == null) {
                        tw.WriteLine("<!DOCTYPE " + doc.DocumentElement.Name + " SYSTEM \"" + Path.Combine(this.txtDTDPath.Text, doc.DocumentElement.Name + ".dtd") + "\">");
                    }

                    tw.Write(doc.InnerXml);
                    tw.Flush();
                    tw.Close();

                    ValidateXml(strTempFile);

                    File.Delete(strTempFile);

                } catch(Exception ex) {
                    outputMessage(ex.Message, "", MessageType.Error, MessageImportance.Essential);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlFileName"></param>
        private void ValidateXml(string xmlFileName) {

            //
            // http://msdn2.microsoft.com/en-us/library/z2adhb2f.aspx
            //

            // Set the validation settings.
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.DTD;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationHandler);

            // Create the XmlReader object.
            XmlReader reader = XmlReader.Create(xmlFileName, settings);


            // Parse the file. 
            while (reader.Read()) ;

            outputMessage("Validation finished", "", MessageType.Information, MessageImportance.Essential);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void ValidationHandler(object sender, ValidationEventArgs args) {
            outputMessage("*Validation error: " + args.Message, "", MessageType.Error, MessageImportance.Essential);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        private void outputMessage(string message, 
                                   string src, 
                                   MessageType messageType, 
                                   MessageImportance messageImportance) {

            cMessage objMessage = new cMessage(message
                                            , messageType
                                            , messageImportance);

            if(messageImportance==MessageImportance.Essential) {

                this.lstOutput.Items.Add(objMessage);
                this.lstOutput.TopIndex = this.lstOutput.Items.Count-1;

                Console.WriteLine(objMessage.ToString());
            }

            Application.DoEvents();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstOutput_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e) {

			e.DrawBackground();
			e.DrawFocusRectangle();

            Brush myBrush = Brushes.Black;

            if(e.Index>0) {
                cMessage objMessage=(cMessage)this.lstOutput.Items[e.Index];

                switch(objMessage.messageType) {
                    case MessageType.Error:
                        myBrush=Brushes.Red;
                        break;
                    case MessageType.Warning:
                        myBrush=Brushes.DarkGoldenrod;
                        break;
                }

			    e.Graphics.DrawString(lstOutput.Items[e.Index].ToString()
                                    , e.Font
                                    , myBrush
                                    , e.Bounds);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstOutput_DoubleClick(object sender, System.EventArgs e) {

            frmDisplayText frmOutput = new frmDisplayText();
            frmOutput.txtOutput.Text = lstOutput.SelectedItem.ToString();

            cMessage objMessage = (cMessage)lstOutput.SelectedItem;
                switch(objMessage.messageType) {
                    case MessageType.Error:
                        frmOutput.txtOutput.ForeColor=System.Drawing.Color.Red;
                        break;
                    case MessageType.Warning:
                        frmOutput.txtOutput.ForeColor=System.Drawing.Color.DarkGoldenrod;
                        break;
                    case MessageType.Information:
                        frmOutput.txtOutput.ForeColor=System.Drawing.Color.Black;
                        break;
            }

            frmOutput.Show();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Load(object sender, System.EventArgs e) {

            if(Reg.getSetting(APP_NAME, "Settings", "XmlPath", "").Length > 0) {
                this.txtXmlPath.Text = Reg.getSetting(APP_NAME, "Settings", "XmlPath", "");
            }

            if(Reg.getSetting(APP_NAME, "Settings", "DTDPath", "").Length > 0) {
                this.txtDTDPath.Text = Reg.getSetting(APP_NAME, "Settings", "DTDPath", "");
            }

            if(Reg.getSetting(APP_NAME, "Settings", "FileMask", "").Length > 0) {
                this.txtFileMask.Text = Reg.getSetting(APP_NAME, "Settings", "FileMask", "");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Closed(object sender, System.EventArgs e) {
            Reg.saveSetting(APP_NAME, "Settings", "XmlPath", this.txtXmlPath.Text);
            Reg.saveSetting(APP_NAME, "Settings", "DTDPath", this.txtDTDPath.Text);
            Reg.saveSetting(APP_NAME, "Settings", "FileMask", this.txtFileMask.Text);
        }
	}
}
