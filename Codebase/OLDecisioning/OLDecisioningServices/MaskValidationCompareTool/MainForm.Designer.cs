﻿namespace MaskValidationCompareTool {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnValidateMask = new System.Windows.Forms.Button();
            this.lvMain = new System.Windows.Forms.ListView();
            this.colMaskIn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colCPPResult = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDotNetResult = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colResultsMatch = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colMsg = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.btnValidateOne = new System.Windows.Forms.Button();
            this.txtMask = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtResultsMatch = new System.Windows.Forms.TextBox();
            this.txtDotNetResult = new System.Windows.Forms.TextBox();
            this.txtCPPResult = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnValidateMask
            // 
            this.btnValidateMask.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnValidateMask.Location = new System.Drawing.Point(12, 681);
            this.btnValidateMask.Margin = new System.Windows.Forms.Padding(4);
            this.btnValidateMask.Name = "btnValidateMask";
            this.btnValidateMask.Size = new System.Drawing.Size(119, 28);
            this.btnValidateMask.TabIndex = 2;
            this.btnValidateMask.Text = "Validate All";
            this.btnValidateMask.UseVisualStyleBackColor = true;
            this.btnValidateMask.Click += new System.EventHandler(this.btnValidateMask_Click);
            // 
            // lvMain
            // 
            this.lvMain.AllowColumnReorder = true;
            this.lvMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvMain.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colMaskIn,
            this.colValue,
            this.colCPPResult,
            this.colDotNetResult,
            this.colResultsMatch,
            this.colMsg});
            this.lvMain.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvMain.FullRowSelect = true;
            this.lvMain.GridLines = true;
            this.lvMain.Location = new System.Drawing.Point(4, 2);
            this.lvMain.Margin = new System.Windows.Forms.Padding(4);
            this.lvMain.Name = "lvMain";
            this.lvMain.Size = new System.Drawing.Size(1303, 534);
            this.lvMain.TabIndex = 4;
            this.lvMain.UseCompatibleStateImageBehavior = false;
            this.lvMain.View = System.Windows.Forms.View.Details;
            this.lvMain.Click += new System.EventHandler(this.lvMain_Click);
            // 
            // colMaskIn
            // 
            this.colMaskIn.Text = "Mask";
            this.colMaskIn.Width = 164;
            // 
            // colValue
            // 
            this.colValue.Text = "Value";
            this.colValue.Width = 195;
            // 
            // colCPPResult
            // 
            this.colCPPResult.Text = "CPP Result";
            this.colCPPResult.Width = 99;
            // 
            // colDotNetResult
            // 
            this.colDotNetResult.Text = "DotNet Result";
            this.colDotNetResult.Width = 113;
            // 
            // colResultsMatch
            // 
            this.colResultsMatch.Text = "Results Match?";
            this.colResultsMatch.Width = 114;
            // 
            // colMsg
            // 
            this.colMsg.Text = "Message";
            this.colMsg.Width = 285;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Mask";
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(232, 47);
            this.txtValue.Margin = new System.Windows.Forms.Padding(4);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(248, 22);
            this.txtValue.TabIndex = 6;
            // 
            // btnValidateOne
            // 
            this.btnValidateOne.Location = new System.Drawing.Point(8, 80);
            this.btnValidateOne.Margin = new System.Windows.Forms.Padding(4);
            this.btnValidateOne.Name = "btnValidateOne";
            this.btnValidateOne.Size = new System.Drawing.Size(119, 28);
            this.btnValidateOne.TabIndex = 7;
            this.btnValidateOne.Text = "Validate This";
            this.btnValidateOne.UseVisualStyleBackColor = true;
            this.btnValidateOne.Click += new System.EventHandler(this.btnValidateOne_Click);
            // 
            // txtMask
            // 
            this.txtMask.Location = new System.Drawing.Point(8, 46);
            this.txtMask.Margin = new System.Windows.Forms.Padding(4);
            this.txtMask.Name = "txtMask";
            this.txtMask.Size = new System.Drawing.Size(215, 22);
            this.txtMask.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(233, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Value";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtMsg);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtResultsMatch);
            this.groupBox1.Controls.Add(this.txtDotNetResult);
            this.groupBox1.Controls.Add(this.txtCPPResult);
            this.groupBox1.Controls.Add(this.txtMask);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtValue);
            this.groupBox1.Controls.Add(this.btnValidateOne);
            this.groupBox1.Location = new System.Drawing.Point(4, 545);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1304, 116);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(931, 27);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 17);
            this.label6.TabIndex = 17;
            this.label6.Text = "Message";
            // 
            // txtMsg
            // 
            this.txtMsg.Location = new System.Drawing.Point(931, 47);
            this.txtMsg.Margin = new System.Windows.Forms.Padding(4);
            this.txtMsg.Multiline = true;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.ReadOnly = true;
            this.txtMsg.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMsg.Size = new System.Drawing.Size(364, 61);
            this.txtMsg.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(763, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Results Match?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(628, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "DotNet Result";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(485, 26);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "CPP Result";
            // 
            // txtResultsMatch
            // 
            this.txtResultsMatch.Location = new System.Drawing.Point(767, 46);
            this.txtResultsMatch.Margin = new System.Windows.Forms.Padding(4);
            this.txtResultsMatch.Name = "txtResultsMatch";
            this.txtResultsMatch.ReadOnly = true;
            this.txtResultsMatch.Size = new System.Drawing.Size(155, 22);
            this.txtResultsMatch.TabIndex = 12;
            // 
            // txtDotNetResult
            // 
            this.txtDotNetResult.Location = new System.Drawing.Point(628, 46);
            this.txtDotNetResult.Margin = new System.Windows.Forms.Padding(4);
            this.txtDotNetResult.Name = "txtDotNetResult";
            this.txtDotNetResult.ReadOnly = true;
            this.txtDotNetResult.Size = new System.Drawing.Size(129, 22);
            this.txtDotNetResult.TabIndex = 11;
            // 
            // txtCPPResult
            // 
            this.txtCPPResult.Location = new System.Drawing.Point(489, 46);
            this.txtCPPResult.Margin = new System.Windows.Forms.Padding(4);
            this.txtCPPResult.Name = "txtCPPResult";
            this.txtCPPResult.ReadOnly = true;
            this.txtCPPResult.Size = new System.Drawing.Size(129, 22);
            this.txtCPPResult.TabIndex = 10;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1308, 724);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lvMain);
            this.Controls.Add(this.btnValidateMask);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "integraPAY Mask Validator";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnValidateMask;
        private System.Windows.Forms.ListView lvMain;
        private System.Windows.Forms.ColumnHeader colMaskIn;
        private System.Windows.Forms.ColumnHeader colValue;
        private System.Windows.Forms.ColumnHeader colCPPResult;
        private System.Windows.Forms.ColumnHeader colDotNetResult;
        private System.Windows.Forms.ColumnHeader colResultsMatch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Button btnValidateOne;
        private System.Windows.Forms.TextBox txtMask;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtResultsMatch;
        private System.Windows.Forms.TextBox txtDotNetResult;
        private System.Windows.Forms.TextBox txtCPPResult;
        private System.Windows.Forms.ColumnHeader colMsg;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMsg;
    }
}

