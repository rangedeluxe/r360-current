﻿using System;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using WFS.RecHub.MaskValidation;

namespace MaskValidationCompareTool
{
    public partial class MainForm : Form
    {
        [DllImport("ValidateMask.DLL", EntryPoint = "_IsValidMaskedValue", CallingConvention = CallingConvention.Cdecl)]
        private static extern int IsValidMaskedValue(string mask, string value);

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnValidateMask_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvMain.Items)
            {
                var cppResult = IsValidMaskedValue(item.SubItems[0].Text, item.SubItems[1].Text) != 0;
                var dotNetResult = MaskValidator.ValidateMask(item.SubItems[0].Text, item.SubItems[1].Text);

                item.UseItemStyleForSubItems = false;
                item.SubItems[2].Text = cppResult ? "Pass" : "Fail";
                item.SubItems[3].Text = dotNetResult.Matched ? "Pass" : "Fail";
                item.SubItems[4].ForeColor = cppResult == dotNetResult.Matched ? Color.Green : Color.Red;
                item.SubItems[4].Text = cppResult == dotNetResult.Matched ? "Match" : "Did Not Match";
                item.SubItems[5].Text = dotNetResult.Message;
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            LoadValues();
        }

        private void LoadValues()
        {
            var appSettings = ConfigurationManager.AppSettings;

            var lstMasks = appSettings.AllKeys
                .Where(key => key.StartsWith("Mask", StringComparison.InvariantCultureIgnoreCase))
                .Select(key => appSettings[key]);

            var lstValues = appSettings.AllKeys
                .Where(key => key.StartsWith("Value", StringComparison.InvariantCultureIgnoreCase))
                .Select(key => appSettings[key])
                .ToList();

            foreach (var mask in lstMasks)
            {
                foreach (var value in lstValues)
                {
                    AddItem(mask, value);
                }
            }
        }

        private void AddItem(string mask, string value)
        {
            var item = new ListViewItem(mask);
            item.SubItems.Add(value);
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add("-");
            item.SubItems.Add(string.Empty);
            lvMain.Items.Add(item);
        }

        private void btnValidateOne_Click(object sender, EventArgs e)
        {
            var cppResult = IsValidMaskedValue(txtMask.Text, txtValue.Text) != 0;
            var dotNetResult = MaskValidator.ValidateMask(txtMask.Text, txtValue.Text);

            txtCPPResult.Text = cppResult ? "Pass" : "Fail";
            txtDotNetResult.Text = dotNetResult.Matched ? "Pass" : "Fail";
            txtResultsMatch.Text = cppResult == dotNetResult.Matched ? "Match" : "Did Not Match";
            txtResultsMatch.ForeColor = cppResult == dotNetResult.Matched ? Color.Green : Color.Red;
            txtMsg.Text = dotNetResult.Message;
        }

        private void lvMain_Click(object sender, EventArgs e)
        {
            txtMask.Text = lvMain.SelectedItems[0].Text;
            txtValue.Text = lvMain.SelectedItems[0].SubItems[1].Text;
            txtCPPResult.Text = lvMain.SelectedItems[0].SubItems[2].Text;
            txtDotNetResult.Text = lvMain.SelectedItems[0].SubItems[3].Text;
            txtResultsMatch.Text = lvMain.SelectedItems[0].SubItems[4].Text;
            txtMsg.Text = lvMain.SelectedItems[0].SubItems[5].Text;
        }
    }
}
