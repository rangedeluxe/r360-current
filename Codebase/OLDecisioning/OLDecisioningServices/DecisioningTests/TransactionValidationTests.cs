﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OLDecisioningShared.DTO;
using System;
using System.Collections.Generic;
using WFS.integraPAY.Online.OLDecisioningServicesAPI;
using WFS.RecHub.Common;
using WFS.RecHub.OLDecisioningServicesAPI;
using System.Linq;

namespace DecisioningTests
{
    [TestClass]
    public class TransactionValidationTests
    {
        private const string DefaultFieldName = "Name";
        private const int DefaultFieldLength = 10;
        private const string DefaultFieldTitle = "DefaultTitle";
        private const string DefaultTableName = "Table";

        private void AssertIsValid(ValidationResponseDto response)
        {
            Assert.IsTrue(response.IsValid);
        }
        private void AssertIsInvalid(ValidationResponseDto response, params ValidationErrorType[] expectedErrors)
        {
            Assert.IsTrue(response.Errors.Any(x => expectedErrors.Contains(x.Type)));

            if (expectedErrors.Length == 0)
                Assert.Fail("Wait, why is this supposed to be invalid but not have any errors?");
        }
        private DecisioningDataEntryItemFieldDto CreateDataEntryField(DataEntryDataType dataType, string value,
            string fieldName = DefaultFieldName, string tableName = DefaultTableName)
        {
            return new DecisioningDataEntryItemFieldDto
            {
                FieldName = fieldName,
                TableName = tableName,
                DataType = dataType,
                Value = value,
            };
        }
        private DecisioningDataEntrySetupDto CreateDataEntrySetup(DataEntryDataType dataType,
            string fieldName = DefaultFieldName, string tableName = DefaultTableName, string title = DefaultFieldTitle,
            int length = DefaultFieldLength, bool isRequired = false, string mask = null)
        {
            return new DecisioningDataEntrySetupDto
            {
                FieldName = fieldName,
                TableName = tableName,
                DataType = dataType,
                Title = title,
                Length = length,
                IsRequired = isRequired,
                Mask = mask,
            };
        }
        private DecisioningStubDto CreateStub(IEnumerable<DecisioningDataEntryItemFieldDto> dataEntryFields)
        {
            return new DecisioningStubDto
            {
                DataEntry = dataEntryFields,
            };
        }
        private DecisioningTransactionDto CreateTransaction(IEnumerable<DecisioningStubDto> stubs,
            int transactionStatus = 0)
        {
            return new DecisioningTransactionDto
            {
                Stubs = stubs,
                TransactionStatus = transactionStatus,
            };
        }
        private DecisioningTransactionDto CreateTransactionWithSingleDataEntry(
            DecisioningDataEntryItemFieldDto dataEntryField)
        {
            return CreateTransaction(stubs: new[]
            {
                CreateStub(dataEntryFields: new[] {dataEntryField}),
            });
        }
        private DecisioningDataEntrySetupDto GetMatchingDataEntrySetup(DecisioningDataEntryItemFieldDto dataEntryField)
        {
            return new DecisioningDataEntrySetupDto
            {
                DataType = dataEntryField.DataType,
                FieldName = dataEntryField.FieldName,
                TableName = dataEntryField.TableName,
                Length = DefaultFieldLength,
            };
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validation_ShouldThrowError_OnNullInput_ForTransaction()
        {
            new TransactionValidationService().Validate(null, new DecisioningDataEntrySetupDto[] {});
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Validation_ShouldThrowError_OnNullInput_ForSetupFields()
        {
            new TransactionValidationService().Validate(new DecisioningTransactionDto(), null);
        }

        [TestMethod]
        public void Validation_ShouldPass_OnValidInput()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransaction(stubs: new[]
            {
                CreateStub(dataEntryFields: new DecisioningDataEntryItemFieldDto[] {}),
            });
            var setupfields = new DecisioningDataEntrySetupDto[] {};

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_ShouldFail_OnLongLength_ForAlphaType()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Alphanumeric, value: "111111"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Alphanumeric, length: 5),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsInvalid(result, ValidationErrorType.MaxLengthExceeded);
        }

        [TestMethod]
        public void Validation_ShouldFail_OnNullSetupField()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Alphanumeric, value: "11111"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Alphanumeric, length: 5, tableName: "Table1"),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsInvalid(result, ValidationErrorType.SetupFieldNotFound);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Numeric_Field_Contains_Letter()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Numeric, value: "20p");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsInvalid(result, ValidationErrorType.InvalidNumericValue);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Numeric_Field_Leading_Zero()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Numeric, value: "0218");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Numeric_Field_Decimal()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Numeric, value: "54.25");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Numeric_Field_Following_Space()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Numeric, value: "20 ");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Numeric_Leading_Following_Space()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Numeric, value: " 458");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Numeric_Leading_Negative_Value()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Numeric, value: "-8");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_Valid_Numeric_Field()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Numeric, value: "11111"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Numeric, length: 5),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsValid(result);
        }
        [TestMethod]
        public void Validation_ShouldFail_OnLongLength_For_NumericField()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Numeric, value: "111111"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Numeric, length: 5),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsInvalid(result, ValidationErrorType.MaxLengthExceeded);
        }
        [TestMethod]
        public void Validation_ShouldPass_OnValidLength_ForAlphaField()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Alphanumeric, value: "11111"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Alphanumeric, length: 5),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Currency_Field_No_Decimal()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: "200");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Currency_Field_Only_Decimal()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: ".20");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Currency_Negative_Value()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: "-5");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Currency_Field()
        {
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Currency, value: "11.70"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Currency, length: 20),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Currency_Field_Too_Many_Decimals()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: "1.2.5");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsInvalid(result, ValidationErrorType.InvalidCurrencyValue);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Currency_Field_Leading_Zero()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: "02.50");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Currency_Field_Only_Decimal()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: ".");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsInvalid(result, ValidationErrorType.InvalidCurrencyValue);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Currency_Field_Only_Dash()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: "-");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsInvalid(result, ValidationErrorType.InvalidCurrencyValue);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Currency_Field_Empty_String()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: "");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsInvalid(result, ValidationErrorType.InvalidCurrencyValue);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Currency_Field_Decimal_With_Nothing_Following()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: "1.");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsInvalid(result, ValidationErrorType.InvalidCurrencyValue);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Currency_Field_Contains_A_Letter()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Currency, value: "1a");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsInvalid(result, ValidationErrorType.InvalidCurrencyValue);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Currency_Field_Too_Many_Decimal_Places()
        {
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Currency, value: "11.511"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Currency, length: 20),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsInvalid(result, ValidationErrorType.InvalidCurrencyValue);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Date_Field_In_YYYY_MM_DD()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Date, value: "2016/10/08");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Date_Field_Using_Dashes()
        {
            var validator = new TransactionValidationService();
            var dataEntryField = CreateDataEntryField(DataEntryDataType.Date, value: "10-08-2016");
            var transaction = CreateTransactionWithSingleDataEntry(dataEntryField);
            var setupfields = new[] {GetMatchingDataEntrySetup(dataEntryField)};

            var result = validator.Validate(transaction, setupfields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Pass_On_Valid_Date_Field()
        {
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Date, value: "2/9/2017"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Date, length: 12),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_Should_Fail_On_Invalid_Date_Field()
        {
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Date, value: "2/9//2017"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Date, length: 12),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsInvalid(result, ValidationErrorType.InvalidDateTimeValue);
        }

        [TestMethod]
        public void Validation_ShouldPass_OnRequiredField()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Numeric, value: "1"));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Numeric, isRequired: true, length: 20),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_ShouldFail_OnRequiredField_ForEmptyString()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Numeric, value: ""));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Numeric, isRequired: true),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsInvalid(result,
                ValidationErrorType.RequiredFieldBlank,
                ValidationErrorType.InvalidNumericValue);
        }

        [TestMethod]
        public void Validation_ShouldFail_OnRequiredField_ForNullValue()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Numeric, value: null));
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Numeric, isRequired: true),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsInvalid(result, ValidationErrorType.RequiredFieldBlank);
        }

        [TestMethod]
        public void Validation_ShouldPass_OnRequiredField_ForNullValueAndRejectedStatus()
        {
            // Arrange
            var validator = new TransactionValidationService();
            var transaction = CreateTransaction(
                transactionStatus: 4,
                stubs: new[]
                {
                    CreateStub(dataEntryFields: new[]
                    {
                        CreateDataEntryField(DataEntryDataType.Numeric, value: null),
                    }),
                });
            var setupfields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Numeric, isRequired: true),
            };

            // Act
            var result = validator.Validate(transaction, setupfields);

            // Assert
            AssertIsValid(result);
        }
        [TestMethod]
        public void Validation_ShouldFail_OnStringField_WhoseValueDoesNotMatchMask()
        {
            // Arrange
            var setupFields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Alphanumeric, mask: "###")
            };
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Alphanumeric, value: "abc"));

            // Act
            var result = new TransactionValidationService().Validate(transaction, setupFields);

            // Assert
            AssertIsInvalid(result, ValidationErrorType.MaskValidationFailed);
        }

        [TestMethod]
        public void Validation_ShouldFail_OnCurrencyField_WhoseValueExceedsMax()
        {
            var setupFields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Currency, length: 20)
            };
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Currency, value: "900000000000000.01"));

            var result = new TransactionValidationService().Validate(transaction, setupFields);

            AssertIsInvalid(result, ValidationErrorType.MaxCurrencyValueExceeded);
        }

        [TestMethod]
        public void Validation_ShouldFail_OnCurrencyField_WhoseValueExceedsMin()
        {
            var setupFields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Currency, length: 20)
            };
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Currency, value: "-900000000000000.01"));

            var result = new TransactionValidationService().Validate(transaction, setupFields);

            AssertIsInvalid(result, ValidationErrorType.MinCurrencyValueExceeded);
        }

        [TestMethod]
        public void Validation_ShouldSucceed_OnCurrencyField_WhoseValueIsGreaterThanMin()
        {
            var setupFields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Currency, length: 20)
            };
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Currency, value: "-900000000000000.00"));

            var result = new TransactionValidationService().Validate(transaction, setupFields);

            AssertIsValid(result);
        }

        [TestMethod]
        public void Validation_ShouldSucceed_OnCurrencyField_WhoseValueIsLessThanMax()
        {
            var setupFields = new[]
            {
                CreateDataEntrySetup(DataEntryDataType.Currency, length: 20)
            };
            var transaction = CreateTransactionWithSingleDataEntry(
                CreateDataEntryField(DataEntryDataType.Currency, value: "900000000000000.00"));

            var result = new TransactionValidationService().Validate(transaction, setupFields);

            AssertIsValid(result);
        }
    }
}
