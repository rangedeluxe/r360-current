﻿using System;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.OLDecisioningServicesAPI.DataProviders;
using WFS.RecHub.OLDecisioningServicesAPI.Repositories;
using WFS.RecHub.R360Shared;
using WFS.RecHub.OLDecisioningServicesAPI;
using WFS.RecHub.OlfRequestCommon;

namespace DecisioningTests
{
    [TestClass]
    public class DecisioningTests
    {
        private readonly DecisioningRepository _repo;
        private readonly Guid _testerSid = Guid.Parse("c9c825cb-eabc-41bb-be10-26c8b0d4b6f7");

        private R360ServiceContext _serviceContext = R360ServiceContext.Current;

        private enum DepositStatusValues
        {
            PendingDecisioning = 240,
            InDecisioning = 241
        };

        private IDecisioningDataProvider RepositoryFromJsonContent(byte[] content)
        {
            return RepositoryFromJsonContent(Encoding.Default.GetString(content));
        }

        private IDecisioningDataProvider RepositoryFromJsonContent(string content)
        {
            return new MockDecisioningDataProvider(content,
                (data) => { }
            );
        }

        public DecisioningTests()
        {
            _repo = new DecisioningRepository(new MockServiceContext(), new MockDecisioningDataProvider());
        }


        [TestMethod]
        public void Decisioning_ShouldReturn_Results()
        {
            var response = _repo.GetAll();
            Assert.AreEqual(response.Batches.Count(), 3);
        }

        [TestMethod]
        public void Decisioning_Should_Return_BatchDetails()
        {
            var response = _repo.GetBatchDetails(100);
            Assert.AreEqual(response.Data.WorkgroupId, 99);
        }

        [TestMethod]
        public void Decisioning_Should_Return_BatchTransactions()
        {
            var response = _repo.GetBatchDetails(101);
            var transactions = response.Data.Transactions;
            Assert.AreEqual(transactions.Count(), 2);
        }

        [TestMethod]
        public void Decisioning_Should_Return_BatchImageDetails()
        {
            var response = _repo.GetBatchImageDetails(101,1);
            Assert.AreEqual(response.Data.BatchId, 100);
            Assert.AreEqual(response.Data.DisplayModeForCheck, 0);
        }

        [TestMethod]
        public void MockDecisioningDataProvider_CheckOutBatch_ShouldSetFirstBatchStatusToInDecisioning()
        {
            var testRepo = RepositoryFromJsonContent(Properties.Resources.FirstUnlockedSecondLocked);

            testRepo.CheckOutBatch(100, _testerSid, "Test", "Lock", "");
            var firstTransaction = testRepo.GetPendingBatches().First();
            Assert.AreEqual(firstTransaction.DepositStatus, (int)DepositStatusValues.InDecisioning);
        }

        [TestMethod]
        public void MockDecisioningDataProvider_ResetBatch_ShouldSetSecondBatchStatusToPendingDecisioning()
        {
            var testRepo = RepositoryFromJsonContent(Properties.Resources.FirstUnlockedSecondLocked);

            testRepo.ResetBatch(101);
            var secondTransaction = testRepo.GetPendingBatches().Skip(1).First();
            Assert.AreEqual(secondTransaction.DepositStatus, (int)DepositStatusValues.PendingDecisioning);
        }

        [TestMethod]
        public void DecisioningAPI_CheckOutBatch_ShouldSetFirstBatchStatusToInDecisioning()
        {
            var repository = new DecisioningRepository(_serviceContext,
                RepositoryFromJsonContent(Properties.Resources.FirstUnlockedSecondLocked));
            var decisioningApi = new DecisioningApi(_serviceContext, repository);

            decisioningApi.CheckOutBatch(100, _testerSid, "Test", "Lock", "");
            var firstTransaction = decisioningApi.GetPendingBatches().Batches.First();
            Assert.AreEqual(firstTransaction.DepositStatus, (int)DepositStatusValues.InDecisioning);
        }

        [TestMethod]
        public void DecisioningAPI_ResetBatch_ShouldSetSecondBatchStatusToPendingDecisioning()
        {
            var repository = new DecisioningRepository(_serviceContext,
                RepositoryFromJsonContent(Properties.Resources.FirstUnlockedSecondLocked));
            var decisioningApi = new DecisioningApi(_serviceContext, repository);

            decisioningApi.ResetBatch(101);
            var secondTransaction = decisioningApi.GetPendingBatches().Batches.Skip(1).First();
            Assert.AreEqual(secondTransaction.DepositStatus, (int)DepositStatusValues.PendingDecisioning);
        }

        [TestMethod]
        public void DecisioningAPI_GetBatchDetails_ShouldContainTransactions()
        {
            var repository = new DecisioningRepository(_serviceContext,
                RepositoryFromJsonContent(Properties.Resources.FirstUnlockedSecondLocked));
            var decisioningApi = new DecisioningApi(_serviceContext, repository);
            var response = decisioningApi.GetBatchDetails(100);

            Assert.IsTrue(response.Data.Transactions.Count() > 0);
        }

        [TestMethod]
        public void DecisioningAPI_GetBatchImageDetails_ShouldContainPaymentsAndDocuments()
        {
            var repository = new DecisioningRepository(_serviceContext,
                RepositoryFromJsonContent(Properties.Resources.FirstUnlockedSecondLocked));
            var decisioningApi = new DecisioningApi(_serviceContext, repository);
            var response = decisioningApi.GetBatchImageDetails(100, 2);

            Assert.IsTrue(response.Data.Payments.Count() > 0);
            Assert.IsTrue(response.Data.Documents.Count() > 0);
        }
    }
}
