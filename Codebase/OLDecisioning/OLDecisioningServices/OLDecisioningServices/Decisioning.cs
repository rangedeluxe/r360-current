using System;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.integraPAY.Online.OLDecisioningServicesAPI.DataProviders;
using WFS.RecHub.OLDecisioningServicesAPI;
using WFS.RecHub.OLDecisioningShared;
using WFS.RecHub.R360Shared;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.OLDecisioningServicesAPI.Repositories;

namespace WFS.RecHub.OLDecisioningServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Decisioning" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Decisioning.svc or Decisioning.svc.cs at the Solution Explorer and start debugging.
    public class Decisioning : LTAService, IDecisioning
    {
        public OLDBaseGenericResponse<string> Ping()
        {
            return new OLDBaseGenericResponse<string> { Data = "Pong" };
        }

        public PendingBatchesResponse GetPendingBatches()
        {
            return FailOnError((decisioningApi) =>
                    ((DecisioningApi)decisioningApi).GetPendingBatches());
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            return FailOnError((decisioningApi) =>
                    ((DecisioningApi)decisioningApi).GetBatches(bankId, workgroupId));
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            return FailOnError(decisioningApi => ((DecisioningApi)decisioningApi).GetBatchDetails(globalBatchId));
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            return FailOnError(decisioningApi => ((DecisioningApi)decisioningApi).GetTransactionDetails(globalBatchId, transactionid));
        }

        public OLDBaseGenericResponse<InProcessExceptionImageRequestDto> GetBatchImageDetails(int globalBatchId, int transactionId)
        {
            return FailOnError(decisioningApi => ((DecisioningApi) decisioningApi).GetBatchImageDetails(globalBatchId, transactionId));
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new DecisioningApi(serviceContext, new DecisioningRepository(serviceContext, new DatabaseDataProvider()));
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(long globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            return FailOnError(api => ((DecisioningApi)api).CheckOutBatch(globalBatchId, sid, firstName, lastName, login));
        }

        public OLDBaseGenericResponse<bool> ResetBatch(long globalBatchId)
        {
            return FailOnError(api => ((DecisioningApi)api).ResetBatch(globalBatchId));
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            return FailOnError(api => ((DecisioningApi)api).UpdateTransaction(dto));
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            return FailOnError(api => ((DecisioningApi)api).CompleteBatch(dto));
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            return FailOnError(decisioningApi => ((DecisioningApi)decisioningApi).GetBatch(globalBatchId));
        }
    }

}