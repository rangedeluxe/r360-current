﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace OLDecisioningShared.DTO
{
    [DataContract]
    public class DecisioningBatchDto
    {
        [DataMember]
        [MapFrom("BankID")]
        public int BankId { get; set; }

        [DataMember]
        [MapFrom("CustomerID")]
        public int CustomerId { get; set; }

        [DataMember]
        public string Entity { get; set; }

        [DataMember]
        [MapFrom("LockboxID")]
        public int WorkgroupId { get; set; }

        [DataMember]
        public string WorkgroupName { get; set; }

        [DataMember]
        [MapFrom("SourceShortname")]
        public string PaymentSource { get; set; }

        [DataMember]
        public DateTime DepositDate { get; set; }

        [DataMember]
        [MapFrom("ProcessingDate")]
        public DateTime ProcessDate { get; set; }

        [DataMember]
        [MapFrom("DecisioningDeadline")]
        public DateTime? DeadLine { get; set; }

        [DataMember]
        public double? MinutesUntilDeadLine { get; set; }

        [DataMember]
        [MapFrom("BatchID")]
        public int BatchId { get; set; }

        [DataMember]
        public int DepositStatus { get; set; }

        [DataMember]
        public string DepositStatusDisplayName { get; set; }

        [DataMember]
        [MapFrom("ItemsInBatch")]
        public int NumberOfTransactions { get; set; }

        [DataMember]
        [MapFrom("RAAMSID")]
        public Guid? SID { get; set; }

        [DataMember]
        [MapFrom("LogonName")]
        public string User { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        [MapFrom("GlobalBatchID")]
        public int GlobalBatchId { get; set; }

        [DataMember]
        public Guid? BatchResetID { get; set; }

        [DataMember]
        public int Balancing { get; set; }

        [DataMember]
        public bool IsCommingled { get; set; }

        [DataMember]
        public bool IsImageExchange { get; set; }

        [DataMember]
        public int UseCar { get; set; }

        [DataMember]
        public bool UserCanLock { get; set; }

        [DataMember]
        public int LocalTimeZoneBias { get; set; }

        [DataMember]
        public IEnumerable<DecisioningTransactionDto> Transactions { get; set; }

        [DataMember]
        public IEnumerable<int> TransactionIds { get; set; }

        [DataMember]
        public IEnumerable<DecisioningDataEntrySetupDto> DESetupList { get; set; }
    }
}
