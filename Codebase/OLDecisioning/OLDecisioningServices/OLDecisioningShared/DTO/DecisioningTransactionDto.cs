﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace OLDecisioningShared.DTO
{

    [DataContract]
    public class DecisioningTransactionDto
    {
        [DataMember]
        public int TransactionId { get; set; }
        
        [DataMember]
        public int GlobalBatchId { get; set; }

        [DataMember]
        [MapFrom("Sequence")]
        public int TransactionSequence { get; set; }

        [DataMember]
        public Decimal Amount { get; set; }

        [DataMember]
        public string RT { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string CheckTraceRef { get; set; }

        [DataMember]
        public string Payer { get; set; }
        
        [DataMember]
        [MapFrom("DecisionSourceStatus")]
        public int TransactionStatus { get; set; }

        [DataMember]
        public DecisioningBatchDto ParentBatch { get; set; }

        [DataMember]
        public IEnumerable<DecisioningPaymentDto> Payments { get; set; }

        [DataMember]
        public IEnumerable<DecisioningStubDto> Stubs { get; set; }
    }
}
