﻿namespace OLDecisioningShared.DTO
{
    public class DecisioningDataEntryDto
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string Value { get; set; }
        /// <summary>
        /// This class is returned to the client JavaScript. Stick with
        /// ints instead of enums for now, rather than change all the JS.
        /// </summary>
        public int DataType { get; set; }
    }
}
