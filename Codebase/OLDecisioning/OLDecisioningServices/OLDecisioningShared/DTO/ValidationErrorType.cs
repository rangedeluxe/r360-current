﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLDecisioningShared.DTO
{
    public enum ValidationErrorType
    {
        SetupFieldNotFound = 0,
        RequiredFieldBlank = 1,
        MaxLengthExceeded = 2,
        InvalidNumericValue = 3,
        InvalidCurrencyValue = 4,
        InvalidDateTimeValue = 5,
        MaskValidationFailed = 6,
        MaxCurrencyValueExceeded = 7,
        MinCurrencyValueExceeded = 8
    }
}
