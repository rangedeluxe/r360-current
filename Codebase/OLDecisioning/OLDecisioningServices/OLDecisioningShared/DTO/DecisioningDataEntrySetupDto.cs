﻿using System;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;
using WFS.RecHub.Common;

namespace OLDecisioningShared.DTO
{
    public class DecisioningDataEntrySetupDto
    {
        public Guid DESetupFieldID { get; set; }
        public string TableName { get; set; }
        [MapFrom("FldName")]
        public string FieldName { get; set; }
        [MapFrom("FldDataTypeEnum")]
        public DataEntryDataType DataType { get; set; }
        [MapFrom("FldLength")]
        public int Length { get; set; }
        public string Title { get; set; }
        [MapFrom("TypeRequired")]
        public bool IsRequired { get; set; }
        [MapFrom("MaskIn")]
        public string Mask { get; set; }
    }
}