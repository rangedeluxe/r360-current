﻿using System.Collections.Generic;

namespace OLDecisioningShared.DTO
{
    public class ValidationResponseDto
    {
        public ValidationResponseDto()
        {
            Errors = new List<ValidationErrorDto>();
        }

        public bool IsValid { get; set; }
        public List<ValidationErrorDto> Errors { get; set; }
    }
}
