﻿using System;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;
using WFS.RecHub.Common;

namespace OLDecisioningShared.DTO
{
    public class DecisioningDataEntryItemFieldDto
    {
        public Guid DEItemRowDataID { get; set; }
        public int GlobalCheckID { get; set; }
        public Guid DEItemFieldDataID { get; set; }
        public Guid DESetupFieldID { get; set; }
        [MapFrom("fldvalue")]
        public string Value { get; set; }
        public int FieldDecisionStatus { get; set; }
        public Guid LockboxDecisioningReasonId { get; set; }
        public string DecisioningReasonDesc { get; set; }
        public string TableName { get; set; }
        [MapFrom("FldName")]
        public string FieldName { get; set; }
        [MapFrom("FldLength")]
        public int FieldLength { get; set; }
        [MapFrom("FldDataTypeEnum")]
        public DataEntryDataType DataType { get; set; }
        public string Title { get; set; }
    }
}
