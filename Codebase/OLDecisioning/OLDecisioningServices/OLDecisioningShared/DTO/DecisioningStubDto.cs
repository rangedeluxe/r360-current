﻿using System;
using System.Collections.Generic;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace OLDecisioningShared.DTO
{
    public class DecisioningStubDto
    {
        public Guid DEItemRowDataID { get; set; }
        public decimal Amount { get; set; }
        [MapFrom("AccountNumber")]
        public string Account { get; set; }
        public int BatchSequence { get; set; }
        public IEnumerable<DecisioningDataEntryItemFieldDto> DataEntry { get; set; }
    }
}
