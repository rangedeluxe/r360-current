﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLDecisioningShared.DTO
{
    
    public class ValidationErrorDto
    {
        public string FieldTitle { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int MaxLength { get; set; }
        public string Value { get; set; }
        public int RowIndex { get; set; }
        public Guid DEItemRowDataID { get; set; }

        public ValidationErrorType Type { get; set; }

    }
}
