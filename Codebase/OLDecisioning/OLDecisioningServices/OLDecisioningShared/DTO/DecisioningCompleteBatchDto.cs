﻿using System;
using System.Runtime.Serialization;

namespace OLDecisioningShared.DTO
{
    [DataContract]
    public class DecisioningCompleteBatchDto
    {
        [DataMember]
        public Guid batchResetId { get; set; }
        [DataMember]
        public int globalBatchId { get; set; }
    }
}
