﻿using System;
using System.ServiceModel;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.OlfRequestCommon;


namespace WFS.RecHub.OLDecisioningShared
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDecisioning" in both code and config file together.
    //[ServiceContract(Namespace = "urn:wausaufs.com:services:HubService")]
    [ServiceContract(Namespace = "urn:wausaufs.com:services:OLDecisioning")]
    public interface IDecisioning
    {
        [OperationContract]
        OLDBaseGenericResponse<string> Ping();

        [OperationContract]
        PendingBatchesResponse GetPendingBatches();

        [OperationContract]
        PendingBatchesResponse GetBatches(long bankId, long workgroupId);
        [OperationContract]
        OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId);
        [OperationContract]
        OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId);

        [OperationContract]
        OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid);

        [OperationContract]
        OLDBaseGenericResponse<bool> CheckOutBatch(long globalBatchId, Guid sid, string firstName, string lastName, string login);

        [OperationContract]
        OLDBaseGenericResponse<bool> ResetBatch(long globalBatchId);

        [OperationContract]
        UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto);

        [OperationContract]
        OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto);

        [OperationContract]
        OLDBaseGenericResponse<InProcessExceptionImageRequestDto> GetBatchImageDetails(int globalBatchId, int transactionId);
    }
}
