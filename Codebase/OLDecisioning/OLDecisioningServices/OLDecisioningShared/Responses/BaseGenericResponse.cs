﻿using System.Runtime.Serialization;
using WFS.RecHub.R360Shared;

namespace OLDecisioningShared.Responses
{
    [DataContract]
    public class OLDBaseGenericResponse<T> : BaseResponse
    {
        public OLDBaseGenericResponse(){ }
        [DataMember]
        public T Data { get; set; }
    }
}
