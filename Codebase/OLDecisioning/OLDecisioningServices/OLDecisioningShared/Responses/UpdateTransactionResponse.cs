﻿using OLDecisioningShared.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace OLDecisioningShared.Responses
{
    public class UpdateTransactionResponse : OLDBaseGenericResponse<bool>
    {
        public ValidationResponseDto ValidationResults { get; set; }
    }
}
