﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using OLDecisioningShared.DTO;
using WFS.RecHub.R360Shared;

namespace OLDecisioningShared.Responses
{
    [DataContract]
    public class PendingBatchesResponse : BaseResponse
    {
        [DataMember]
        public IEnumerable<DecisioningBatchDto> Batches { get; set; }

    }
}
