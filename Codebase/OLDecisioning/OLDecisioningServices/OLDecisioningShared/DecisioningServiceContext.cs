﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using Wfs.Raam.Core.Services;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;

namespace OLDecisioningShared
{
    [ServiceContract(Namespace = ServiceConstants.ServiceNamespaces.Root)]
    [System.Runtime.Serialization.DataContractAttribute(Namespace = "urn:wausaufs.com:services:OLDecisioning")]
    public class DecisioningServiceContext : IServiceContext
    {
        /// <summary>
        /// Unique name of object stored in headers
        /// </summary>
        private const string HeaderServiceContext = "DecisioningServiceContext";

        [DataMember]
        public string SiteKey { get; set; }

        private SimpleClaim[] _RAAMClaims = null;
        public SimpleClaim[] RAAMClaims
        {

            get
            {
                if (_RAAMClaims == null)
                {
                    // Load ambient claims as simple claims for service to use...
                    CurrentPrincipal principal = WSFederatedAuthentication.CurrentPrincipal;
                    _RAAMClaims = principal.Identity.Claims.Select(cv => new SimpleClaim() { Type = cv.Type, Value = cv.Value }).ToArray();
                }
                return _RAAMClaims;
            }
        }

        public static DecisioningServiceContext Current
        {
            get
            {
                // determine if servicecontext was sent in service headers
                if (OperationContext.Current != null)
                {
                    int intIndexOfHeader = OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderServiceContext, ServiceConstants.ServiceNamespaces.Root);

                    if (intIndexOfHeader >= 0)
                    {
                        // Return the header value
                        return OperationContext.Current.IncomingMessageHeaders.GetHeader<DecisioningServiceContext>(intIndexOfHeader);
                    }
                }

                // Not found in header - return a blank context
                return new DecisioningServiceContext(string.Empty);
            }
        }

        public static void Init()
        {
            Init(string.Empty);
        }

        private DecisioningServiceContext(string siteKey)
        {
            SiteKey = siteKey;
        }
        public static void Init(string siteKey)
        {
            var context = new DecisioningServiceContext(siteKey);

            MessageHeader<DecisioningServiceContext> header = new MessageHeader<DecisioningServiceContext>(context);
            if (OperationContext.Current != null)
                OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader(HeaderServiceContext, ServiceConstants.ServiceNamespaces.Root));
        }

        public Guid GetSessionID()
        {
            if (RAAMClaims != null)
            {
                var sidClaim = RAAMClaims.FirstOrDefault(o => o.Type == ClaimTypes.Sid);
                Guid sid;
                if (sidClaim != null && Guid.TryParse(sidClaim.Value, out sid))
                    return sid;
            }

            throw new InvalidSessionException();
        }

        public Guid GetSID()
        {
            if (RAAMClaims != null)
            {
                var sidClaim = RAAMClaims.FirstOrDefault(o => o.Type == ClaimTypes.Sid);
                Guid sid;
                if (sidClaim != null && Guid.TryParse(sidClaim.Value, out sid))
                    return sid;
            }

            throw new InvalidSessionException();
        }

        public string GetLoginNameForAuditing()
        {
            if (RAAMClaims != null)
            {
                return RAAMClaims.FirstOrDefault(o => o.Type == ClaimTypes.Name)?.Value ?? "Name Not Defined";
            }

            throw new InvalidSessionException();
        }
    }
}
