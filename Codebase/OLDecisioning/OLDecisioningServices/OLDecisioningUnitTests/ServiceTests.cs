﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.OLDecisioningServicesClient;
using WFS.RecHub.Common;
using System.Collections.Generic;

namespace OLDecisioningUnitTests
{
	[TestClass]
	public class ServiceTests
	{
		[TestMethod]
		public void PingTest()
		{
			IOLDecisioningSvcClient client = OLDecisioningSvcClient.CreateClient("IPOnline", "UnitTester");

			Assert.IsTrue(client.Ping().Success);
		}

		[TestMethod]
		public void GetBatchTest()
		{
			foreach (long globalBatchId in new long[] { 1086899017, 28938870, 2235634, 453780526 })
			{
				IOLDecisioningSvcClient client = OLDecisioningSvcClient.CreateClient("IPOnline", "UnitTester");

				var response = client.GetBatch(globalBatchId);
				Assert.IsTrue(response.Success);
				Assert.AreEqual(globalBatchId, response.GlobalBatchID);
			}
		}

		[TestMethod]
		public void UpdateBatchTest()
		{
			long globalBatchId = 0;
			BatchTransaction[] transactions = new BatchTransaction[] {
				new BatchTransaction()
				{
					TransactionID = 1,
					TxnMode = 1,
					BalancingAction = InvoiceBalancingActions.Undefined,
					Checks = new List<TransactionCheckItem>(),
					DecisionSourceStatus = DecisionSrcStatusTypes.Undefined,
					DestinationBatchTypeID = Guid.Empty,
					DestinationLockboxKey = Guid.Empty,
					DestLockboxIdentifier = string.Empty,
					Documents = new List<TransactionDocumentItem>(),
					Sequence = 42,
					Stubs = new List<TransactionStubItem>()
				}
			};

			IOLDecisioningSvcClient client = OLDecisioningSvcClient.CreateClient("IPOnline", "UnitTester");
			var response = client.UpdateBatch(globalBatchId, transactions);

			Assert.IsTrue(response.Success);
		}

	}
}
