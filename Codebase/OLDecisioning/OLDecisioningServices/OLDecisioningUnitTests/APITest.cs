﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.OLDecisioningServicesAPI;
using System.Xml.Linq;
using System.Xml;
using System.Diagnostics;

namespace OLDecisioningUnitTests
{
	[TestClass]
	public class APITests
	{
		[TestMethod]
		public void APIGetBatchesPendingTest()
		{
			BatchDecisioning api = new BatchDecisioning("IPOnline");

			long lngBankID = 1;
			long lngCustomerID = -1;
			long lngLockboxID = 9210;

			// Build request
			Guid requestId = Guid.NewGuid();
			XNode request = new XElement("ReqGetBatchesPendingDecisioning",
				new XAttribute("ReqID", requestId),
				new XElement("Bank",
					new XAttribute("BankID", lngBankID),
					new XElement("Customer",
						new XAttribute("CustomerID", lngCustomerID),
						new XElement("Lockbox",
							new XAttribute("LockboxID", lngLockboxID)
						)
					)
				)
			);

			var response = api.GetBatchesPendingDecisioning(request.Xml()).X();
		}

		[TestMethod]
		public void APIGetBatchTest()
		{
			BatchDecisioning api = new BatchDecisioning("IPOnline");

			long globalBatchId = 1086899017;

			// Build request
			Guid requestId = Guid.NewGuid();
			XNode request = new XElement("ReqGetBatch",
				new XAttribute("ReqID", requestId),
				new XElement("Batch",
					new XAttribute("GlobalBatchID", globalBatchId)
				)
			);

			var response = api.GetBatch(request.Xml()).X();
		}
	}

	public static class XmlDocumentExtensions
	{
		[DebuggerStepThrough]
		public static XmlDocument Xml(this XNode xDocument)
		{
			var xmlDocument = new XmlDocument();
			using (var xmlReader = xDocument.CreateReader())
			{
				xmlDocument.Load(xmlReader);
			}
			return xmlDocument;
		}

		[DebuggerStepThrough]
		public static XDocument X(this XmlNode xmlDocument)
		{
			using (var nodeReader = new XmlNodeReader(xmlDocument))
			{
				nodeReader.MoveToContent();
				return XDocument.Load(nodeReader);
			}
		}
	}

}
