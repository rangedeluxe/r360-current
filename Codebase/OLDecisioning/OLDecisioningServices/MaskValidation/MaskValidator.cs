﻿using System;
using System.Text.RegularExpressions;
using WFS.RecHub.MaskValidation.DTO;
using WFS.RecHub.MaskValidation.Helpers;

namespace WFS.RecHub.MaskValidation
{
    public static class MaskValidator
    {
        // this walks the mask that was input by the user and
        // dynamically generates a regex mask used to validate the input.
        //
        // optionalMaskHelper will pick up optional fields deliminated by [ ]
        // 
        // GroupingMaskHelper will pick up grouping fieldds deliminated by { }
        //
        // pick up the repeating option and save for future use. indicated by * followed by a number
        //   currently the code will only pick up a repeating option up to 9.

       
        private static MaskDto SetMaskOptionalElements(string mask)
        {
            var maskDto = new MaskDto
            {
                OriginalMask = mask,
                RegExpressionMask = "^",
                RepeatNumberOfTimes = 1,
                MaskIndex = 0
            };
            
            var theMaskArray = mask.ToCharArray();
            int theTempIndex = 0;

            do
            {
                switch (theMaskArray[maskDto.MaskIndex].ToString())
                {
                    case "[":
                        maskDto = LocateOptional(theMaskArray, theMaskArray[maskDto.MaskIndex].ToString(), maskDto);
                        break;

                    case "{":
                        maskDto = LocateGroupEntry(theMaskArray, theMaskArray[maskDto.MaskIndex].ToString(), maskDto);
                        break;

                    case "*":
                        maskDto = LocateRepeating(theMaskArray, theMaskArray[maskDto.MaskIndex].ToString(), maskDto);
                        break;
                    default:
                        // these are the required field matches
                        maskDto = RequiredStringMatch(theMaskArray[maskDto.MaskIndex].ToString(), maskDto);
                        break;
                }


                if (theTempIndex == maskDto.MaskIndex)
                    maskDto.MaskIndex = 9999;
                theTempIndex = maskDto.MaskIndex;

            } while (maskDto.MaskIndex < theMaskArray.Length);
            
            maskDto.RegExpressionMask = maskDto.RegExpressionMask + @"$";

            return maskDto;
        }

        private static MaskDto LocateOptional(char[] maskCharArray, string maskCharIn, MaskDto maskDto)
        {
            var maskDtoCopy = maskDto;
            string regExpressionMask = maskDtoCopy.RegExpressionMask;

            string theWholeMask = maskDto.OriginalMask;
            var tempSetOfValues = theWholeMask.Substring(maskDto.MaskIndex, theWholeMask.Length - maskDto.MaskIndex );
            int first = tempSetOfValues.IndexOf("[");
            int stringLength = tempSetOfValues.IndexOf("]") + 1;

            var abc = tempSetOfValues.Substring(first, stringLength);

            var maskDtoNew = new MaskDto
            {
                RegExpressionMask = String.Empty,
                MaskIndex = 0,
                OriginalMask = abc,
                RepeatNumberOfTimes = 1
            };

            OptionalMaskHelper omh = new OptionalMaskHelper();
            var bdc = omh.SetMaskOptional(maskDtoNew);

            //maskDtoCopy.MaskIndex++;
            maskDtoCopy.RegExpressionMask = maskDtoCopy.RegExpressionMask + bdc.RegExpressionMask;
            maskDtoCopy.MaskIndex = maskDtoCopy.MaskIndex + bdc.MaskIndex;
            return maskDtoCopy;
        }

        private static MaskDto LocateGroupEntry(char[] maskCharArray, string maskCharIn, MaskDto maskDto)
        {
            var maskDtoCopy = maskDto;
            string regExpressionMask = maskDtoCopy.RegExpressionMask;

            string theWholeMask = maskDto.OriginalMask;
            var tempSetOfValues = theWholeMask.Substring(maskDto.MaskIndex, theWholeMask.Length - maskDto.MaskIndex);
            int first = tempSetOfValues.IndexOf("{");
            int stringLength = tempSetOfValues.IndexOf("}") + 1;

            var abc = tempSetOfValues.Substring(first, stringLength);

            var maskDtoNew = new MaskDto
            {
                RegExpressionMask = String.Empty,
                MaskIndex = 0,
                OriginalMask = abc,
                RepeatNumberOfTimes = 1
            };

            GroupingMaskHelper groupingMaskHelper = new GroupingMaskHelper();
            var bdc = groupingMaskHelper.SetMaskGrouping(maskDtoNew);

            maskDtoCopy.RegExpressionMask = maskDtoCopy.RegExpressionMask + bdc.RegExpressionMask;
            maskDtoCopy.MaskIndex = maskDtoCopy.MaskIndex + bdc.MaskIndex;
            return maskDtoCopy;
        }

        private static MaskDto LocateRepeating(char[] maskCharArray, string maskCharIn, MaskDto maskDto)
        {
            var maskDtoCopy = maskDto;
            string regExpressionMask = maskDtoCopy.RegExpressionMask;

            string theWholeMask = maskDto.OriginalMask;
            var tempSetOfValues = theWholeMask.Substring(maskDto.MaskIndex, theWholeMask.Length - maskDto.MaskIndex);

            var maskDtoNew = new MaskDto
            {
                RegExpressionMask = String.Empty,
                MaskIndex = 0,
                OriginalMask = tempSetOfValues,
                RepeatNumberOfTimes = 1
            };

            RepeatHelper repeatHelper = new RepeatHelper();
            var bdc = repeatHelper.SetMaskOptional(maskDtoNew);

            maskDtoCopy.RegExpressionMask = maskDtoCopy.RegExpressionMask + bdc.RegExpressionMask;
            maskDtoCopy.MaskIndex = maskDtoCopy.MaskIndex + bdc.MaskIndex;
            maskDtoCopy.RepeatNumberOfTimes = bdc.RepeatNumberOfTimes;
            return maskDtoCopy;
        }

        private static MaskDto RequiredStringMatch(string maskChar,  MaskDto maskDtoIn)
        {
            var maskDtoCopy = maskDtoIn;
            //these are the required field matches
            string regExpressionMask = maskDtoCopy.RegExpressionMask;

            switch (maskChar)
            {
                case "#":
                    regExpressionMask = regExpressionMask + @"[0-9]" + SetRepeating(maskDtoCopy.RepeatNumberOfTimes );
                    break;

                case "?":
                    regExpressionMask = regExpressionMask + @"[a-zA-Z]" + SetRepeating(maskDtoCopy.RepeatNumberOfTimes);
                    break;

                case "!":
                case "@":
                    regExpressionMask = regExpressionMask + @"[0-9a-zA-Z!]" + SetRepeating(maskDtoCopy.RepeatNumberOfTimes);
                    break;

                case "&":
                case "~":
                    regExpressionMask = regExpressionMask + @"[a-zA-Z]" + SetRepeating(maskDtoCopy.RepeatNumberOfTimes);
                    break;

                case "-":
                    regExpressionMask = regExpressionMask + @"[-]" + SetRepeating(maskDtoCopy.RepeatNumberOfTimes);
                    break;

                case " ":
                    regExpressionMask = regExpressionMask + @"[\s]" + SetRepeating(maskDtoCopy.RepeatNumberOfTimes);
                    break;
            }

            maskDtoCopy.RepeatNumberOfTimes = 1;
            maskDtoCopy.RegExpressionMask = regExpressionMask;
            maskDtoCopy.MaskIndex++;

            return maskDtoCopy;
        }

        private static string SetRepeating(int repeatTimes)
        {
            if (repeatTimes == 0)
            {
                //repeat 0 or many times
                return "*";
            }
            else
            {
                return "{" + repeatTimes + "}";
            }
        }

        public static ValidationResult ValidateMask(string mask, string value)
        {
            if (string.IsNullOrWhiteSpace(mask))
                return new ValidationResult(true, "No mask defined");

            if (string.IsNullOrWhiteSpace(value))
                return new ValidationResult(true, "Blank value, will be handled by required-field logic");

            //create string to represent the optional elements
            var maskTranslated = SetMaskOptionalElements(mask.Trim());
            Regex rgx = new Regex(maskTranslated.RegExpressionMask);

            return rgx.IsMatch(value.Trim()) ? new ValidationResult(true, "Successful match") : 
                new ValidationResult(false, "Match failed");
        }
    }
}
