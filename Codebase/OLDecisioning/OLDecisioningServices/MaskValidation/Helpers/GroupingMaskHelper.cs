﻿using System;
using System.Text.RegularExpressions;
using WFS.RecHub.MaskValidation.DTO;

namespace WFS.RecHub.MaskValidation.Helpers
{
    public class GroupingMaskHelper
    {
        //inside the grouping indicators an optional field is possible
        //
        //look for the optional indicators [ ] and call the OptionalMaskHelper to convert them.

        public  MaskDto SetMaskGrouping(MaskDto mask)
        {
            var maskDto = mask;
            var tempMask = ReplaceCommas(maskDto.OriginalMask);

            string regExpressionMask = maskDto.RegExpressionMask;

            int optionalStart = tempMask.IndexOf("[");

            if (optionalStart == -1)
            {
                maskDto.MaskIndex = tempMask.Length;
                maskDto.RegExpressionMask = regExpressionMask + "(" + RemoveCaseHelper.RemoveCaseSensitive(tempMask.Substring(1,tempMask.Length - 2)) + ")";
                return maskDto;
            }

            int theTempIndex = maskDto.MaskIndex;
            maskDto.MaskIndex++;

            var theFields = tempMask.Split('|');

            for (int i = 0; i < theFields.Length; i++)
            {
                if (i == 0)
                {
                    regExpressionMask = regExpressionMask + LocateFields(theFields[i]);
                }
                else
                {
                    regExpressionMask = regExpressionMask + "|" + LocateFields(theFields[i]);
                }

                theTempIndex = theTempIndex + theFields[i].Length + 1;
            }

            regExpressionMask =  RemoveCaseHelper.RemoveCaseSensitive(regExpressionMask);

            maskDto.RegExpressionMask = "(" + regExpressionMask + ")";
            maskDto.MaskIndex = maskDto.MaskIndex + theTempIndex - 2;

            return maskDto;
        }

        private string ReplaceCommas(string theMask)
        {
            string regExMask = String.Empty;
            var theMaskArray = theMask.ToCharArray();
            bool optionalField = false;

            for (int i = 0; i < theMaskArray.Length; i++)
            {
                switch (theMaskArray[i].ToString())
                {
                    case "[":
                        regExMask = regExMask + theMaskArray[i];
                        optionalField = true;
                        break;
                    case "]":
                        regExMask = regExMask + theMaskArray[i];
                        optionalField = false;
                        break;
                    case ",":
                        if (optionalField)
                        {
                            regExMask = regExMask + theMaskArray[i];
                        }
                        else
                        {
                            regExMask = regExMask + "|";
                        }
                        break;
                    default:
                        regExMask = regExMask + theMaskArray[i];
                        break;
                        
                }
            }

            return regExMask;
        }

        private string LocateFields(string theMask)
        {
            var maskDtoNew = new MaskDto
            {
                RegExpressionMask = String.Empty,
                MaskIndex = 0,
                OriginalMask = theMask,
                RepeatNumberOfTimes = 1
            };
            string regExMask = String.Empty;
            int maskIndex = 0;   

            var theMaskArray = theMask.ToCharArray();
            do
            {
                switch (theMaskArray[maskIndex].ToString())
                {
                    case "[":
                        maskDtoNew.OriginalMask = theMask.Substring(maskIndex, theMaskArray.Length - maskIndex);
                        maskDtoNew.RegExpressionMask = regExMask;
                        maskDtoNew = LocateOptional(maskDtoNew);
                        maskIndex = maskIndex + maskDtoNew.MaskIndex;
                        regExMask = maskDtoNew.RegExpressionMask;
                        break;

                    case "{":                 //skip the first {
                        maskIndex++;
                        break;

                    case "}":
                        regExMask = "(" + regExMask + ")";
                        maskIndex++;
                        break;

                    default:
                        // now go create array of valid values.
                        regExMask = regExMask + theMaskArray[maskIndex];
                        maskIndex++;
                        break;
                }

            } while (maskIndex < theMaskArray.Length);

            return regExMask;
        }

        private static MaskDto LocateOptional(MaskDto maskDto)
        {
            var maskDtoCopy = maskDto;
            string regExpressionMask = maskDtoCopy.RegExpressionMask;

            string theWholeMask = maskDto.OriginalMask;
            var tempSetOfValues = theWholeMask.Substring(maskDto.MaskIndex, theWholeMask.Length - maskDto.MaskIndex);
            int first = tempSetOfValues.IndexOf("[");
            int stringLength = tempSetOfValues.IndexOf("]") + 1;

            var abc = tempSetOfValues.Substring(first, stringLength);

            var maskDtoNew = new MaskDto
            {
                RegExpressionMask = String.Empty,
                MaskIndex = 0,
                OriginalMask = abc,
                RepeatNumberOfTimes = 1
            };

            OptionalMaskHelper optionalMaskHelper = new OptionalMaskHelper();
            var bdc = optionalMaskHelper.SetMaskOptional(maskDtoNew);

            maskDtoCopy.RegExpressionMask = maskDtoCopy.RegExpressionMask + bdc.RegExpressionMask;
            maskDtoCopy.MaskIndex = maskDtoCopy.MaskIndex + stringLength;
            return maskDtoCopy;
        }
    }
}
