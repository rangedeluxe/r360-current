﻿using System;
using System.Text.RegularExpressions;

namespace WFS.RecHub.MaskValidation.Helpers
{
    //this walks through the mask
    //converting all alpha char to upper and lower case valid
    //  if regex mask of [] is found skip converting that mask
    public static class RemoveCaseHelper
    {
        public static string RemoveCaseSensitive(string maskIn)
        {
            Regex rgx = new Regex("[a-zA-Z]");
            string newMask = String.Empty;
            var maskArray = maskIn.ToCharArray();
            bool skipCaseCheck = false;

            for (int i = 0; i < maskArray.Length; i++)
            {
                switch (maskArray[i].ToString())
                {
                    case "[":
                        skipCaseCheck = true;
                        newMask = newMask + maskArray[i];
                        break;
                    case "]":
                        skipCaseCheck = false;
                        newMask = newMask + maskArray[i];
                        break;
                    default:

                        if (rgx.IsMatch(maskArray[i].ToString()) && skipCaseCheck == false)
                        {
                            newMask = newMask + "[" + maskArray[i].ToString().ToLower() + maskArray[i].ToString().ToUpper() + "]";
                        }
                        else
                        {
                            newMask = newMask + maskArray[i];
                        }
                        break;
                }

            }

            return newMask;
        }
    }
}
