﻿
using System;
using WFS.RecHub.MaskValidation.DTO;

namespace WFS.RecHub.MaskValidation.Helpers
{
    public class OptionalMaskHelper
    {
        public  MaskDto SetMaskOptional(MaskDto mask)
        {
            var maskDto = mask;

            maskDto.MaskIndex++;

            string regExpressionMask = maskDto.RegExpressionMask + "(";

            int theTempIndex = maskDto.MaskIndex;

            var theMaskArray = maskDto.OriginalMask.ToCharArray();
            do
            {
                switch (theMaskArray[maskDto.MaskIndex].ToString())
                {
                    case "#":
                        regExpressionMask = regExpressionMask + @"[0-9]";
                        maskDto.MaskIndex++;
                        break;

                    case "?":
                        regExpressionMask = regExpressionMask + @"[a-zA-Z]";
                        maskDto.MaskIndex++;
                        break;

                    case "-":
                        regExpressionMask = regExpressionMask + @"[-]";
                        maskDto.MaskIndex++;
                        break;

                    case "!":
                    case "@":
                        regExpressionMask = regExpressionMask + @"[0-9a-zA-Z!]";
                        maskDto.MaskIndex++;
                        break;

                    case "&":
                    case "~":
                        regExpressionMask = regExpressionMask + @"[a-zA-Z]";
                        maskDto.MaskIndex++;
                        break;

                    case " ":
                        regExpressionMask = regExpressionMask + @"[\s]";
                        maskDto.MaskIndex++;
                        break;

                    case "]":
                        regExpressionMask = regExpressionMask + SetRepeating(maskDto.RepeatNumberOfTimes) + ")?";
                        maskDto.MaskIndex++;
                        break;

                    case "*":
                        maskDto = LocateRepeating(theMaskArray, theMaskArray[maskDto.MaskIndex].ToString(), maskDto);
                        break;

                    default:
                        // now go create array of valid values.
                        regExpressionMask = regExpressionMask + LocateValidEntry(maskDto.MaskIndex, maskDto.OriginalMask);
                        maskDto.MaskIndex = maskDto.MaskIndex + maskDto.OriginalMask.Length - 2;
                        maskDto.RepeatNumberOfTimes = 1;
                        break;
                }

            } while (maskDto.MaskIndex < maskDto.OriginalMask.Length );

            maskDto.RegExpressionMask = regExpressionMask;

            return maskDto;
        }
        
        private  string LocateValidEntry(int theIndex, string theWholeMask)
        {
            var tempSetOfValues = theWholeMask.Substring(theIndex, theWholeMask.Length - theIndex - 1);
            var theValues = tempSetOfValues.Split(']');
            return "(" + RemoveCaseHelper.RemoveCaseSensitive(tempSetOfValues.Replace(',', '|')) + ")";
        }

        private static MaskDto LocateRepeating(char[] maskCharArray, string maskCharIn, MaskDto maskDto)
        {
            var maskDtoCopy = maskDto;
            string regExpressionMask = maskDtoCopy.RegExpressionMask;

            string theWholeMask = maskDto.OriginalMask;
            var tempSetOfValues = theWholeMask.Substring(maskDto.MaskIndex, theWholeMask.Length - maskDto.MaskIndex);

            var maskDtoNew = new MaskDto
            {
                RegExpressionMask = String.Empty,
                MaskIndex = 0,
                OriginalMask = tempSetOfValues,
                RepeatNumberOfTimes = 1
            };

            RepeatHelper repeatHelper = new RepeatHelper();
            var bdc = repeatHelper.SetMaskOptional(maskDtoNew);

            maskDtoCopy.RegExpressionMask = maskDtoCopy.RegExpressionMask + bdc.RegExpressionMask;
            maskDtoCopy.MaskIndex = maskDtoCopy.MaskIndex + bdc.MaskIndex;
            maskDtoCopy.RepeatNumberOfTimes = bdc.RepeatNumberOfTimes;
            return maskDtoCopy;
        }

        private static string SetRepeating(int repeatTimes)
        {
            if (repeatTimes == 0)
            {
                //repeat 0 or many times
                return "*";
            }
            else
            {
                return "{" + repeatTimes + "}";
            }
        }
    }
}
