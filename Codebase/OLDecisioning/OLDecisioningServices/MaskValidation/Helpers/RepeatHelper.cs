﻿

using WFS.RecHub.MaskValidation.DTO;

namespace WFS.RecHub.MaskValidation.Helpers
{
    public class RepeatHelper
    {
        public MaskDto SetMaskOptional(MaskDto mask)
        {
            var maskDto = mask;

            string regExpressionMask = maskDto.RegExpressionMask; 

            int theTempIndex = maskDto.MaskIndex;

            var theMaskArray = maskDto.OriginalMask.ToCharArray();

            int repeatNumberOfTimes;
            int.TryParse(theMaskArray[maskDto.MaskIndex + 1].ToString(), out repeatNumberOfTimes);
            if (repeatNumberOfTimes == 0)
            {
                //multiplier unlimited
                maskDto.MaskIndex = maskDto.MaskIndex + 1;
                maskDto.RepeatNumberOfTimes = repeatNumberOfTimes;
            }
            else
            {
                maskDto.RepeatNumberOfTimes = repeatNumberOfTimes;
                string maskRepeatingDigits = theMaskArray[maskDto.MaskIndex + 1].ToString() + theMaskArray[maskDto.MaskIndex + 2].ToString();
                int.TryParse((theMaskArray[maskDto.MaskIndex + 1].ToString() + theMaskArray[maskDto.MaskIndex + 2].ToString()), out repeatNumberOfTimes);
                if (repeatNumberOfTimes > 0)
                {
                    //multiplier 10 to 99
                    maskDto.MaskIndex = maskDto.MaskIndex + 3;
                    maskDto.RepeatNumberOfTimes = repeatNumberOfTimes;
                }
                else
                {
                    //multiplier 1 to 9
                    maskDto.MaskIndex = maskDto.MaskIndex + 2;
                }
            }

            maskDto.RegExpressionMask = regExpressionMask;

            return maskDto;
        }

    }
}
