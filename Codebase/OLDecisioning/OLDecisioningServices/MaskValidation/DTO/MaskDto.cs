﻿

using System.Collections.Generic;

namespace WFS.RecHub.MaskValidation.DTO
{
    public class MaskDto
    {
        public string OriginalMask { get; set; }
        public string RegExpressionMask { get; set; }
        public int MaskIndex { get; set; }
        public int RepeatNumberOfTimes { get; set; }
    }
}
