﻿namespace WFS.RecHub.MaskValidation
{
    public struct ValidationResult
    {
        public ValidationResult(bool matched, string message)
        {
            Matched = matched;
            Message = message;
        }

        public string Message { get; }
        public bool Matched { get; }
    }
}