﻿using System.Runtime.InteropServices;

namespace MaskValidationRegressionTests
{
    public static class LegacyMaskValidator
    {
        [DllImport("ValidateMask.DLL", EntryPoint = "_IsValidMaskedValue", CallingConvention = CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool IsValidMaskedValue(string mask, string value);
    }
}