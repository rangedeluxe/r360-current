﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.MaskValidation;

namespace MaskValidationRegressionTests
{
    /// <summary>
    /// Tests for the new mask validator written in C#. In many cases, these tests
    /// also compare the behavior of the legacy mask validator DLL from integraPAY
    /// against the behavior of the new mask validator.
    /// </summary>
    [TestClass]
    public class MaskValidatorRegressionTests
    {
        [TestInitialize]
        public void SetUp()
        {
            UseNewLibraryOnly = false;
        }

        private bool UseNewLibraryOnly { get; set; }

        private void AssertAcceptsAsciiOnly(string mask, string template)
        {
            // Don't check every possible Unicode code point, because that would make the test too slow.
            // But at least get a representative sample.
            AssertInvalid(mask: mask, valueFrom: 0x0000, valueTo: 0x001F, template: template);
            AssertInvalid(mask: mask, valueFrom: 0x007F, valueTo: 0x0FFF, template: template);

            var sampleValues = new[]
            {
                "Ⓔ", // U+24BA CIRCLED LATIN CAPITAL LETTER E
                "꘥", // U+A625 VAI DIGIT FIVE
                "𐐝", // U+01041D DESERET CAPITAL LETTER ES
                "𝟡", // U+01D7E1 MATHEMATICAL DOUBLE-STRUCK DIGIT NINE
            };
            foreach (var value in sampleValues)
                AssertInvalid(mask: mask, value: value, expectedMessage: string.Format(template, value));
        }
        private void AssertInvalid(string mask, int valueFrom, int valueTo, string template)
        {
            AssertValidity(mask: mask, valueFrom: valueFrom, valueTo: valueTo, shouldBeValid: false, template: template);
        }
        private void AssertInvalid(string mask, string value, string expectedMessage)
        {
            AssertValidity(mask: mask, value: value, shouldBeValid: false, expectedMessage: expectedMessage);
        }
        private void AssertValid(string mask, int valueFrom, int valueTo, string template)
        {
            AssertValidity(mask: mask, valueFrom: valueFrom, valueTo: valueTo, shouldBeValid: true, template: template);
        }
        private void AssertValid(string mask, string value, string expectedMessage)
        {
            AssertValidity(mask: mask, value: value, shouldBeValid: true, expectedMessage: expectedMessage);
        }
        private void AssertValidity(string mask, int valueFrom, int valueTo, bool shouldBeValid, string template)
        {
            for (var utf32 = valueFrom; utf32 <= valueTo; ++utf32)
            {
                var codePoint = char.ConvertFromUtf32(utf32);
                // If either the legacy or new library would consider the mask or value to be
                // whitespace-only, add a literal-character prefix and suffix, so the character
                // of interest isn't immediately trimmed. But only do this for whitespace-only
                // strings, because if the code point is a combining mark, it would combine with
                // the prefix and make it really messy to check the resulting message.
                Func<char, bool> isConsideredWhitespaceByEitherLibrary = c => char.IsWhiteSpace(c) || c <= 0x1F;
                var needsDelimiter =
                    mask.All(isConsideredWhitespaceByEitherLibrary) ||
                    codePoint.All(isConsideredWhitespaceByEitherLibrary);
                var delimiter = needsDelimiter ? "-" : "";
                var targetMask = $"{delimiter}{mask}{delimiter}";
                var targetValue = $"{delimiter}{codePoint}{delimiter}";
                var targetExpectedMessage = string.Format(template, codePoint);
                AssertValidity(mask: targetMask, value: targetValue, shouldBeValid: shouldBeValid,
                    expectedMessage: targetExpectedMessage);
            }
        }
        private void AssertValidity(string mask, string value, bool shouldBeValid, string expectedMessage)
        {
            var legacyIsValid = LegacyMaskValidator.IsValidMaskedValue(mask, value);
            var currentResult = MaskValidator.ValidateMask(mask, value);

            Assert.AreEqual(expectedMessage, currentResult.Message, "Expected message");

            var expected = GetDescription(shouldBeValid, shouldBeValid, expectedMessage);
            var actual = GetDescription(legacyIsValid, currentResult.Matched, currentResult.Message);
            Assert.AreEqual(expected, actual, $"Mask:{Stringify(mask)} Value:{Stringify(value)}");
        }
        private string GetDescription(bool legacyIsValid, bool currentIsValid, string comparisonMessage)
        {
            return UseNewLibraryOnly
                ? $"{currentIsValid}:{comparisonMessage}"
                : $"Legacy:{legacyIsValid} Current:{currentIsValid}:{comparisonMessage}";
        }
        private string Stringify(string value)
        {
            if (value == null)
                return "null";
            if (value.Length == 1)
                return $"'{value}' (U+{(int) value[0]:X4})";
            return $"'{value}'";
        }

        [TestMethod]
        public void NullMask_AlwaysValid()
        {
            AssertValid(mask: null, value: null, expectedMessage: "No mask defined");
            AssertValid(mask: null, value: "", expectedMessage: "No mask defined");
            AssertValid(mask: null, value: "ABCD !@#", expectedMessage: "No mask defined");
        }
        [TestMethod]
        public void EmptyMask_AlwaysValid()
        {
            AssertValid(mask: "", value: null, expectedMessage: "No mask defined");
            AssertValid(mask: "", value: "", expectedMessage: "No mask defined");
            AssertValid(mask: "", value: "ABCD !@#", expectedMessage: "No mask defined");
        }
        [TestMethod]
        public void WhitespaceMask_AlwaysValid()
        {
            AssertValid(mask: "\t ", value: null, expectedMessage: "No mask defined");
            AssertValid(mask: "\t ", value: "", expectedMessage: "No mask defined");
            AssertValid(mask: "\t ", value: "ABCD !@#", expectedMessage: "No mask defined");
        }
        [TestMethod]
        public void NullValue_AlwaysValid()
        {
            AssertValid(mask: "-", value: null, expectedMessage: "Blank value, will be handled by required-field logic");
            AssertValid(mask: "#", value: null, expectedMessage: "Blank value, will be handled by required-field logic");
            AssertValid(mask: "ABC!", value: null, expectedMessage: "Blank value, will be handled by required-field logic");
        }
        [TestMethod]
        public void EmptyValue_AlwaysValid()
        {
            AssertValid(mask: "-", value: "", expectedMessage: "Blank value, will be handled by required-field logic");
            AssertValid(mask: "#", value: "", expectedMessage: "Blank value, will be handled by required-field logic");
            AssertValid(mask: "ABC!", value: "", expectedMessage: "Blank value, will be handled by required-field logic");
        }
        [TestMethod]
        public void WhitespaceValue_AlwaysValid()
        {
            AssertValid(mask: "-", value: "\t ", expectedMessage: "Blank value, will be handled by required-field logic");
            AssertValid(mask: "#", value: "\t ", expectedMessage: "Blank value, will be handled by required-field logic");
            AssertValid(mask: "ABC!", value: "\t ", expectedMessage: "Blank value, will be handled by required-field logic");
        }
        [TestMethod]
        public void LeadingAndTrailingSpaces_AreIgnored()
        {
            AssertValid(mask: " - ", value: "-", expectedMessage: "Successful match");
            AssertValid(mask: "-", value: " - ", expectedMessage: "Successful match");
        }
        [TestMethod]
        public void InternalSpaces_AreSignificant()
        {
            AssertInvalid(mask: "--", value: "- -", expectedMessage: "Match failed");
            AssertInvalid(mask: "- -", value: "--", expectedMessage: "Match failed");
        }
        [TestMethod]
        public void LiteralHyphenInMask_RequiresLiteralHyphen()
        {
            AssertInvalid(mask: "-", value: "0", expectedMessage: "Match failed");
            AssertInvalid(mask: "-", value: "A", expectedMessage: "Match failed");
            AssertInvalid(mask: "-", value: "--", expectedMessage: "Match failed");
            AssertValid(mask: "-", value: "-", expectedMessage: "Successful match");
        }

        [Ignore]
        [TestMethod]
        public void LiteralUnicodeCharacterInMask_AcceptedByNewLibrary()
        {
            // This isn't part of the spec for the validation feature, but it makes the code
            // much more orthogonal.

            UseNewLibraryOnly = true;

            // From the Basic Multilingual Plane (encoded with a single "char"):
            // U+256C BOX DRAWINGS DOUBLE VERTICAL AND HORIZONTAL
            AssertInvalid(mask: "╬", value: "A", expectedMessage: "Match failed");
            AssertValid(mask: "╬", value: "╬", expectedMessage: "Successful match");

            // From a supplementary plane:
            // U+01D74F MATHEMATICAL BOLD ITALIC PARTIAL DIFFERENTIAL
            AssertInvalid(mask: "𝝏", value: "A", expectedMessage: "Match failed");
            AssertValid(mask: "𝝏", value: "𝝏", expectedMessage: "Successful match");
        }
        [TestMethod]
        public void NumberSignInMask_RequiresAsciiDigit()
        {
            AssertInvalid(mask: "#", value: "-", expectedMessage: "Match failed");
            AssertInvalid(mask: "#", value: "!", expectedMessage: "Match failed");
            AssertInvalid(mask: "#", value: "A", expectedMessage: "Match failed");

            AssertValid(mask: "#", value: "0", expectedMessage: "Successful match");
            AssertInvalid(mask: "#", value: "00", expectedMessage: "Match failed");
            AssertInvalid(mask: "#", value: "000", expectedMessage: "Match failed");
            AssertValid(mask: "#", value: "9", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void OptionalLetter()
        {
            AssertValid(mask: "[?]", value: "a", expectedMessage: "Successful match");
            AssertValid(mask: "[??]", value: "ab", expectedMessage: "Successful match");
            AssertValid(mask: "[???]", value: "abc", expectedMessage: "Successful match");
            AssertValid(mask: "[???]", value: " ", expectedMessage: "Blank value, will be handled by required-field logic");
        }

        [TestMethod]
        public void NumberSignInMaskOptional_RequiresAsciiDigit()
        {
            AssertInvalid(mask: "[?]", value: "1", expectedMessage: "Match failed");

            AssertInvalid(mask: "[???]", value: "a", expectedMessage: "Match failed");
            AssertInvalid(mask: "[???]", value: "ab", expectedMessage: "Match failed");
            AssertInvalid(mask: "[???]", value: "abcd", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void NumberSignMaskWithOptionalNumber_RequiresAsciiDigit()
        {
            AssertValid(mask: "#[#]", value: "0", expectedMessage: "Successful match");
            AssertValid(mask: "#[#]", value: "9", expectedMessage: "Successful match");
            AssertValid(mask: "#[#]", value: "99", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void NumberSignInMaskOptionalNumber_RequiresAsciiDigit()
        {
            AssertInvalid(mask: "#[#]", value: "-", expectedMessage: "Match failed");
            AssertInvalid(mask: "#[#]", value: "!", expectedMessage: "Match failed");
            AssertInvalid(mask: "#[#]", value: "A", expectedMessage: "Match failed");

            // only 1 digit is required
            AssertValid(mask: "#[#][#]", value: "0", expectedMessage: "Successful match");
            AssertValid(mask: "#[#][#]", value: "01", expectedMessage: "Successful match");
            AssertValid(mask: "#[#][#]", value: "012", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void ZipCode_Validtestss()
        {
            AssertValid(mask: "#####[-####]", value: "12345", expectedMessage: "Successful match");
            AssertValid(mask: "#####[-####]", value: "12345-4321", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void ZipCode_RequiresAsciiDigit()
        {
            AssertInvalid(mask: "#####[-####]", value: "1", expectedMessage: "Match failed");
            AssertInvalid(mask: "#####[-####]", value: "12345-", expectedMessage: "Match failed");
            AssertInvalid(mask: "#####[-####]", value: "12345-1", expectedMessage: "Match failed");
            AssertInvalid(mask: "#####[-####]", value: "12345-12", expectedMessage: "Match failed");
            AssertInvalid(mask: "#####[-####]", value: "12345-123", expectedMessage: "Match failed");
            AssertInvalid(mask: "#####[-####]", value: "12345-12345", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void VariousMasksForTesting()
        {
            AssertValid(mask: "###{AA,BB}##", value: "123aa56", expectedMessage: "Successful match");
            AssertValid(mask: "###{AA,BB}", value: "123aa", expectedMessage: "Successful match");
            AssertValid(mask: "{AA,BB}###", value: "aa123", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void VariousMasksForTesting01()
        {
            AssertValid(mask: "{QA,RC[2]}?", value: "rc2a", expectedMessage: "Successful match");
            AssertValid(mask: "{QA,RC[2]}", value: "rc2", expectedMessage: "Successful match");
            AssertValid(mask: "{QA,RC[2]}", value: "rc", expectedMessage: "Successful match");
            AssertValid(mask: "{QA,RC[2]}", value: "qa", expectedMessage: "Successful match");
            AssertValid(mask: "*3#?", value: "123a", expectedMessage: "Successful match");
            AssertValid(mask: "*3#{AA,BB}*2#", value: "123aa21", expectedMessage: "Successful match");
            AssertValid(mask: "{AA,BB}[0,1,2]", value: "AA", expectedMessage: "Successful match");
            AssertValid(mask: "{AA,BB}[0,1,2]", value: "AA0", expectedMessage: "Successful match");
            AssertValid(mask: "{AA,BB}[0,1,2]", value: "AA2", expectedMessage: "Successful match");

            AssertValid(mask: "[0,1,2]{AA,BB}", value: "1Aa", expectedMessage: "Successful match");
            AssertValid(mask: "{AA,BB}*2#*3?", value: "Aa12cvv", expectedMessage: "Successful match");
        }


        [TestMethod]
        public void OnlyAllowSpecificValues_RequiresAsciiDigit()
        {
            AssertValid(mask: "[001,004]", value: "001", expectedMessage: "Successful match");
            AssertValid(mask: "[001,004]", value: "004", expectedMessage: "Successful match");
            AssertInvalid(mask: "[001,004]", value: "4", expectedMessage: "Match failed");
            AssertValid(mask: "[001,004][002,005]", value: "004002", expectedMessage: "Successful match");
            AssertInvalid(mask: "[001,004][002,005]", value: "004003", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void OptionalValuesAllowSpecificValues()
        {
            AssertValid(mask: "[A,N]", value: "a", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void GroupingValuesOption1()
        {
            AssertValid(mask: "{Red,Green}", value: "Red", expectedMessage: "Successful match");
            AssertValid(mask: "{Red,Green}", value: "Green", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void GroupingValuesAutomobile()
        {
            // { Auto[mobile],Car}
            AssertValid(mask: "{Auto[mobile],Car}", value: "Car", expectedMessage: "Successful match");
            AssertValid(mask: "{Auto[mobile],Car}", value: "car", expectedMessage: "Successful match");
            AssertValid(mask: "{Car,Auto[mobile]}", value: "Car", expectedMessage: "Successful match");
            AssertInvalid(mask: "{Car,Auto[mobile]}", value: "Jeep", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void GroupingValuesAutomobileOption01()
        {
            // { Auto[mobile],Car}
            AssertValid(mask: "{Auto[mobile],Car}", value: "Auto", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void GroupingValuesAutomobileOption02()
        {
            AssertValid(mask: "{Auto[mobile],Car}", value: "Automobile", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void GroupingValuesAutomobileOption03()
        {
            AssertValid(mask: "{Auto[mobile],Car}", value: "AutoMobile", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void GroupingValuesOption3()
        {
            AssertValid(mask: "{Red,Green}", value: "Green", expectedMessage: "Successful match");
            AssertValid(mask: "{Red,Green}", value: "Red", expectedMessage: "Successful match");
            AssertValid(mask: "{Red,Green}", value: "red", expectedMessage: "Successful match");
            AssertValid(mask: "{Red,Green,Blue}", value: "blue", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void GroupingValuesInvalidOption2()
        {
            AssertInvalid(mask: "{Red,Green}", value: "Blue", expectedMessage: "Match failed");
           // AssertInvalid(mask: "{Red,Green}", value: "red", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void FixedMaskPlusGroupingValues()
        {
            AssertValid(mask: "#{1,4}", value: "04", expectedMessage: "Successful match");
            AssertValid(mask: "#{1,4}", value: "01", expectedMessage: "Successful match");
            AssertInvalid(mask: "#{1,4}", value: "0", expectedMessage: "Match failed");
            AssertInvalid(mask: "#{1,4}", value: "15", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void ZipCodeMaskRepeatingValid()
        {
            AssertValid(mask: "*5#[-*4#]", value: "12345", expectedMessage: "Successful match");
            AssertValid(mask: "*5#[-*4#]", value: "12345-1234", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void MaskRepeatingValid()
        {
            AssertValid(mask: "*5#", value: "12345", expectedMessage: "Successful match");
            AssertValid(mask: "*#", value: "123", expectedMessage: "Successful match");
            AssertValid(mask: "*#", value: "123456", expectedMessage: "Successful match");
            AssertValid(mask: "*#", value: "1234567890123456", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void MaskRepeatingValid2()
        {
            AssertValid(mask: "*15#", value: "123451234512345", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void MaskRepeatingValid3()
        {
            AssertValid(mask: "@[*#]", value: "A12345", expectedMessage: "Successful match");
            AssertValid(mask: "@[*#]", value: "A", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void ZipCodeMaskRepeatingInValid()
        {
            AssertInvalid(mask: "*5#[-*4#]", value: "1234", expectedMessage: "Match failed");
            AssertInvalid(mask: "*5#[-*4#]", value: "12345-123", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void FixedMaskRepeating()
        {
            AssertValid(mask: "*3#", value: "123", expectedMessage: "Successful match");
            AssertInvalid(mask: "3#", value: "15", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void FixedMaskRepeating2()
        {
            AssertValid(mask: "*3?", value: "abc", expectedMessage: "Successful match");
           
            AssertInvalid(mask: "3?", value: "15", expectedMessage: "Match failed");
            //*3?@
        }

        [TestMethod]
        public void FixedMaskRepeating3()
        {
            AssertValid(mask: "???@", value: "abc9", expectedMessage: "Successful match");
            AssertValid(mask: "*3?@", value: "abc9", expectedMessage: "Successful match");
        }

        [TestMethod]
        public void GroupingValuesPlusOptional()
        {
            AssertValid(mask: "{00[1,4],11[2,5]}", value: "004", expectedMessage: "Successful match");
            AssertValid(mask: "{01,4}", value: "01", expectedMessage: "Successful match");
            AssertInvalid(mask: "{1,4}", value: "0", expectedMessage: "Match failed");
            AssertInvalid(mask: "{1,4}", value: "15", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void GroupingValuesWithOptional()
        {
            AssertValid(mask: "{00[1,4]}", value: "00", expectedMessage: "Successful match");
            AssertValid(mask: "{00[1,4]}", value: "001", expectedMessage: "Successful match");
            AssertValid(mask: "{00[1,4]}", value: "004", expectedMessage: "Successful match");
            AssertInvalid(mask: "{00[1,4]}", value: "005", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void GroupingValuesWithSelections()
        {
            AssertInvalid(mask: "{Red,Green}", value: "Blue", expectedMessage: "Match failed");
            AssertValid(mask: "{Red,Green}", value: "", expectedMessage: "Blank value, will be handled by required-field logic");
        }

        [TestMethod]
        public void SpecialMaskCharValues_RequiresAsciiDigit()
        {
            AssertValid(mask: "!", value: "1", expectedMessage: "Successful match");
            AssertValid(mask: "!", value: "a", expectedMessage: "Successful match");
            AssertValid(mask: "! !", value: "a a", expectedMessage: "Successful match");
            AssertValid(mask: "![ ]!", value: "aa", expectedMessage: "Successful match");
            AssertValid(mask: "![!]", value: "a", expectedMessage: "Successful match");

            AssertValid(mask: "@", value: "1", expectedMessage: "Successful match");
            AssertValid(mask: "@", value: "a", expectedMessage: "Successful match");
            AssertValid(mask: "@[@]", value: "a", expectedMessage: "Successful match");

            AssertValid(mask: "&", value: "a", expectedMessage: "Successful match");
            AssertValid(mask: "&[&]", value: "a", expectedMessage: "Successful match");
            AssertInvalid(mask: "&", value: "4", expectedMessage: "Match failed");

            AssertValid(mask: "~", value: "a", expectedMessage: "Successful match");
            AssertValid(mask: "~[~]", value: "a", expectedMessage: "Successful match");
            AssertInvalid(mask: "~", value: "4", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void NumberSignInMask_InNewLibrary_AcceptsAsciiOnly()
        {
            UseNewLibraryOnly = true;
            AssertAcceptsAsciiOnly("#", "Match failed");
        }
        [TestMethod]
        public void QuestionMarkInMask_RequiresAsciiLetter()
        {
            AssertInvalid(mask: "?", valueFrom: 0, valueTo: 'A' - 1, template: "Match failed");
            AssertValid(mask: "?", valueFrom: 'A', valueTo: 'Z', template: "Successful match");
            AssertInvalid(mask: "?", valueFrom: 'Z' + 1, valueTo: 'a' - 1, template: "Match failed");
            AssertValid(mask: "?", valueFrom: 'a', valueTo: 'z', template: "Successful match");
            AssertInvalid(mask: "?", valueFrom: 'z' + 1, valueTo: 0x7F, template: "Match failed");
        }
        [TestMethod]
        public void QuestionMarkInMask_InNewLibrary_AcceptsAsciiOnly()
        {
            UseNewLibraryOnly = true;
            AssertAcceptsAsciiOnly("?", "Match failed");
        }

        [TestMethod]
        public void ShorterThanMask()
        {
            AssertInvalid(mask: "---", value: "--", expectedMessage: "Match failed");
            AssertInvalid(mask: "###", value: "12", expectedMessage: "Match failed");
        }

        [TestMethod]
        public void LongerThanMask()
        {
            AssertInvalid(mask: "---", value: "----", expectedMessage: "Match failed");
            AssertInvalid(mask: "###", value: "1234", expectedMessage: "Match failed");
        }
    }
}
