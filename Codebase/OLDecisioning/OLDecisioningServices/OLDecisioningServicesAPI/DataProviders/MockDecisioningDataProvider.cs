﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.Common;
using WFS.RecHub.OlfRequestCommon;

namespace WFS.RecHub.OLDecisioningServicesAPI.DataProviders
{
    /// <summary>
    /// 
    /// </summary>
    public class MockDecisioningDataProvider : IDecisioningDataProvider
    {
        private const string EXCEPTIONSFILE = "Resources\\Exceptions.json";
        private readonly List<DecisioningBatchDto> _data;
        private Action<string> _saveAction;
        public MockDecisioningDataProvider() : this(GetFileContent(EXCEPTIONSFILE),
                (content) => File.WriteAllText(GetFullPath(EXCEPTIONSFILE), content))
        {
        }

        public MockDecisioningDataProvider(string exceptionData, Action<string> saveAction)
        {
            _data = JsonConvert.DeserializeObject<List<DecisioningBatchDto>>(exceptionData);
            _saveAction = saveAction;
        }

        private static string GetFileContent(string fileName)
        {
            var fullFileName = GetFullPath(fileName);
            return File.ReadAllText(fullFileName);
        }

        private static string GetFullPath(string fileName)
        {
            var assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            return new Uri(Path.Combine(assemblyFolder, fileName)).LocalPath;
        }

        public IEnumerable<DecisioningBatchDto> GetPendingBatches()
        {
            //this just sets the display name for deposit status
            var result = _data.Select(x =>
            {
                x.DepositStatusDisplayName = GetDepositStatusDisplayName(x.DepositStatus);
                return x;
            }).ToList();
            return result;
        }

        public IEnumerable<DecisioningBatchDto> GetBatches(long bankId, long workgroupId)
        {
            throw new NotImplementedException();
        }

        public DecisioningBatchDto GetBatchDetails(long globalBatchId)
        {
            var result = _data.FirstOrDefault(x => x.GlobalBatchId == globalBatchId);
            result.DepositStatusDisplayName = GetDepositStatusDisplayName(result.DepositStatus);
            return result;
        }

        //This needs to be pulling from the DB instead of a enum in this library
        public string GetDepositStatusDisplayName(int depositStatus)
        {
            if (depositStatus == (int)DepositStatusValues.DepositPendingDecision) return "Pending Decisioning";
            return depositStatus == (int)DepositStatusValues.DepositInDecision ? "In Decisioning" : "";
        }

        public bool CheckOutBatch(long globalBatchID, Guid sid, string firstName, string lastName, string login)
        {
            var batch = _data.FirstOrDefault(x => x.GlobalBatchId == globalBatchID);

            if (batch != null)
            {
                batch.SID = sid;
                batch.FirstName = firstName;
                batch.DepositStatus = 241;
                batch.LastName = lastName;
                _saveAction(JsonConvert.SerializeObject(_data));
            }
            else
            {
                throw new Exception($"Batch not found for GlobalBatchID: {globalBatchID}");
            }
            return true;
        }

        public bool ResetBatch(long globalBatchID)
        {
            var batch = _data.FirstOrDefault(x => x.GlobalBatchId == globalBatchID);

            if (batch != null)
            {
                batch.SID = null;
                batch.DepositStatus = 240;
                _saveAction(JsonConvert.SerializeObject(_data));
            }
            else
            {
                throw new Exception($"Batch not found for GlobalBatchID: {globalBatchID}");
            }
            return true;
        }

        public bool UpdateTransaction(DecisioningTransactionDto dto)
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            throw new NotImplementedException();
        }

        public InProcessExceptionImageRequestDto GetBatchImageDetails(long globalBatchId, int transactionId)
        {
            var result = _data.FirstOrDefault(x => x.GlobalBatchId == globalBatchId);
            result.DepositStatusDisplayName = GetDepositStatusDisplayName(result.DepositStatus);
            var result2 = new InProcessExceptionImageRequestDto();
            result2.GlobalBatchId = result.GlobalBatchId;
            result2.BatchId = result.BatchId;
            result2.Amount = 123.45m;
            result2.BankId = result.BankId;
            result2.BatchNumber = result.BatchId;
            result2.DepositDate = result.DepositDate;
            result2.DisplayModeForCheck = 0;
            result2.DisplayModeForDocument = 0;
            result2.DisplayRemitterNameInPdf = true;
            result2.PaymentSource = result.PaymentSource;
            result2.ProcessingDate = result.DepositDate;
            result2.TransactionId = transactionId;
            result2.TransactionSequence = 2;
            result2.Workgroup = result.WorkgroupName;
            result2.WorkgroupId = result.WorkgroupId;
            result2.Payments = CreatePaymentSet();
            result2.Documents = CreateDocumentSet();

            var result3 = new InProcessExceptionImageRequestDto();
            result3.GlobalBatchId = result2.GlobalBatchId;
            result3.BatchId = result2.BatchId;
            result3.Amount = result2.Amount;
            result3.BankId = result2.BankId;
            result3.BatchNumber = result2.BatchNumber;
            result3.DepositDate = result2.DepositDate;
            result3.DisplayModeForCheck = result2.DisplayModeForCheck;
            result3.DisplayModeForDocument = result2.DisplayModeForDocument;
            result3.DisplayRemitterNameInPdf = result2.DisplayRemitterNameInPdf;
            result3.PaymentSource = result2.PaymentSource;
            result3.ProcessingDate = result2.ProcessingDate;
            result3.TransactionId = result2.TransactionId;
            result3.TransactionSequence = result2.TransactionSequence;
            result3.Workgroup = result2.Workgroup;
            result3.WorkgroupId = result2.WorkgroupId;
            result3.Payments = result2.Payments;
            result3.Documents = result2.Documents;
            return result3;
        }

        private List<InProcessExceptionPaymentImageRequestDto> CreatePaymentSet()
        {
            List<InProcessExceptionPaymentImageRequestDto> payments = new List<InProcessExceptionPaymentImageRequestDto>
            {
                new InProcessExceptionPaymentImageRequestDto()
                {
                    GlobalCheckID = 100101,
                    CheckSequence = 1,
                    BatchSequence = 2,
                    RT = "RT 01",
                    Account = "theAccount",
                    Serial = "theSerial",
                    TransactionCode = "theTranCode",
                    Amount = 12345.67m,
                    PayeeID = 25,
                    PayeeName = "George",
                    TraceNumber = "trace number",
                    RemitterName = "theRemitter",
                    DecisioningReasonID = Guid.NewGuid(),
                    DecisioningReasonDesc = "the very short description"
                }
            };
            var set2 = new InProcessExceptionPaymentImageRequestDto()
            {
                GlobalCheckID = payments[0].GlobalCheckID,
                CheckSequence = payments[0].CheckSequence,
                BatchSequence = payments[0].BatchSequence,
                RT = payments[0].RT,
                Account = payments[0].Account,
                Serial = payments[0].Serial,
                TransactionCode = payments[0].TransactionCode,
                Amount = payments[0].Amount + 125,
                PayeeID = payments[0].PayeeID,
                PayeeName = payments[0].PayeeName,
                TraceNumber = payments[0].TraceNumber,
                RemitterName = payments[0].RemitterName,
                DecisioningReasonID = payments[0].DecisioningReasonID,
                DecisioningReasonDesc = payments[0].DecisioningReasonDesc
            };

            payments.Add(set2);
            return payments;
        }

        private List<InProcessExceptionDocumentImageRequestDto> CreateDocumentSet()
        {
            List<InProcessExceptionDocumentImageRequestDto> documents = new List<InProcessExceptionDocumentImageRequestDto>
            {
                new InProcessExceptionDocumentImageRequestDto()
                {
                    GlobalDocumentId = 100101,
                    DocumentSequence = 1,
                    BatchSequence = 2,
                    FileDescriptor = "IN",
                    ImageHeight = 2250,
                    ImageWidth = 80,
                    IsColor = false,
                    TraceNumber = "trace number",
                    Description = "the very short description",
                    PicsDate = "19981231"
                }
            };
            var set2 = new InProcessExceptionDocumentImageRequestDto()
            {
                GlobalDocumentId = documents[0].GlobalDocumentId,
                DocumentSequence = documents[0].DocumentSequence,
                BatchSequence = documents[0].BatchSequence,
                FileDescriptor = documents[0].FileDescriptor,
                ImageHeight = documents[0].ImageHeight,
                ImageWidth = documents[0].ImageWidth,
                IsColor = documents[0].IsColor,
                TraceNumber = documents[0].TraceNumber,
                Description = documents[0].Description,
                PicsDate = documents[0].PicsDate
            };
            documents.Add(set2);
            return documents;
        }

        public IEnumerable<DecisioningDataEntrySetupDto> GetSetupFields(int globalbatchid)
        {
            throw new NotImplementedException();
        }

        public DecisioningTransactionDto GetTransactionDetails(long globalBatchId, long tranasctionid)
        {
            throw new NotImplementedException();
        }

        public DecisioningBatchDto GetBatch(long globalBatchId)
        {
            throw new NotImplementedException();
        }
    }
}
