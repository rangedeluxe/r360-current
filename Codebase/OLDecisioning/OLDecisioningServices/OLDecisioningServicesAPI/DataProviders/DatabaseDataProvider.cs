﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.OLDecisioningServicesAPI.DataProviders;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common.Log;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.OLDecisioningServicesAPI;
using WFS.RecHub.R360Shared;

namespace WFS.integraPAY.Online.OLDecisioningServicesAPI.DataProviders
{
    public class DatabaseDataProvider : IDecisioningDataProvider
    {
        private string _siteKey = "iponline";
        private readonly cDecisioningDAL _dal;
        private cEventLog _eventLog;
        private OLDecisioningOptions _serverOptions;
        public DatabaseDataProvider()
        {
            _dal = new cDecisioningDAL(_siteKey);
            _serverOptions = new OLDecisioningOptions(_siteKey);
            _eventLog = new cEventLog(_serverOptions.logFilePath
                , _serverOptions.logFileMaxSize
                , (MessageImportance)_serverOptions.loggingDepth);
        }

        /// <summary>
        /// Gets all the batches for a specified bankId and workgroupId
        /// with status DepositInDecision and DepositPendingDecision
        /// </summary>
        /// <param name="bankId">Bank id for batches requested</param>
        /// <param name="workgroupId">Workgroup id for batches requested</param>
        /// <returns>List of batches</returns>
        public IEnumerable<DecisioningBatchDto> GetBatches(long bankId, long workgroupId)
        {
            DataTable pendingDt = null;
            var resp = _dal.GetBatches(bankId, workgroupId, DepositStatusValues.DepositPendingDecision, out pendingDt);

            var batches = new List<DecisioningBatchDto>();
            //first get batches pending decisioning
            if (resp)
            {
                var list = pendingDt.ToObjectList<DecisioningBatchDto>();
                foreach (var b in list)
                {
                    b.DepositStatusDisplayName = GetDepositStatusDisplayName(b.DepositStatus);
                    CalculateDeadlineMinutes(b);
                }
                batches.AddRange(list);
            }

            //now grab in-decisioning
            DataTable inDecResults = null;
            var inDecResp = _dal.GetBatches(bankId, workgroupId, DepositStatusValues.DepositInDecision, out inDecResults);
            if (!inDecResp) return batches;
            var indec = inDecResults.ToObjectList<DecisioningBatchDto>();
            foreach (var batch in indec)
            {
                DataTable indecBatchDt = null;
                batch.DepositStatusDisplayName = GetDepositStatusDisplayName(batch.DepositStatus);
                CalculateDeadlineMinutes(batch);
                if (!_dal.GetDecisioningBatch(batch.GlobalBatchId, out indecBatchDt)) continue;
                var row = indecBatchDt.Rows[0];
                batch.SID = row.GetGuid("RAAMSID");
                batch.User = row.GetString("LogonName");
                batch.FirstName = row.GetString("FirstName");
                batch.LastName = row.GetString("LastName");
                batch.PaymentSource = row.GetString("SourceShortname");
            }
            batches.AddRange(indec);
            return batches;
        }

        /// <summary>
        /// Gets all the batches with status DepositInDecision and DepositPendingDecision
        /// </summary>
        /// <returns>List of batches</returns>
        public IEnumerable<DecisioningBatchDto> GetPendingBatches()
        {
            DataTable pendingDt = null;
            var result = _dal.GetBatches(-1, -1, DepositStatusValues.DepositPendingDecision, out pendingDt);
            if (!result) return null;

            //first get batches pending decisioning
            var batches = pendingDt.ToObjectList<DecisioningBatchDto>();
            foreach (var batch in batches)
            {
                batch.DepositStatusDisplayName = GetDepositStatusDisplayName(batch.DepositStatus);
                CalculateDeadlineMinutes(batch);
            }

            //now grab in-decisioning
            DataTable inDecResults = null;
            var inDecResp = _dal.GetBatches(-1, -1, DepositStatusValues.DepositInDecision, out inDecResults);
            if (!inDecResp) return batches;
            var indec = inDecResults.ToObjectList<DecisioningBatchDto>();

            foreach (var batch in indec)
            {
                DataTable indecBatchDt = null;
                batch.DepositStatusDisplayName = GetDepositStatusDisplayName(batch.DepositStatus);
                CalculateDeadlineMinutes(batch);
                var resPendBatch = _dal.GetDecisioningBatch(batch.GlobalBatchId, out indecBatchDt);
                if (!resPendBatch || indecBatchDt.Rows.Count <= 0) continue;
                var row = indecBatchDt.Rows[0];
                batch.SID = row.GetGuid("RAAMSID");
                batch.User = row.GetString("LogonName");
                batch.FirstName = row.GetString("FirstName");
                batch.LastName = row.GetString("LastName");
                batch.PaymentSource = row.GetString("SourceShortname");
            }
            batches.AddRange(indec);

            // filter the deadline expired batches
            batches = batches
                .Where(x => !x.MinutesUntilDeadLine.HasValue || x.MinutesUntilDeadLine.Value >= 0)
                .ToList();

            return batches;
        }

        /// <summary>
        /// Calculates deadline and MinutesUntilDeadLine in GMT
        /// </summary>
        /// <param name="batch"></param>
        private void CalculateDeadlineMinutes(DecisioningBatchDto batch)
        {
            if (batch.DeadLine.HasValue)
            {
                //MinutesUntilDeadLine should be used for any and all calculations, otherwise deadline (time type) will 
                //become an issue whenever deadline +/- timezonebias gets it past midnight. Deadline should only be used
                //for displaying purposes
                batch.MinutesUntilDeadLine = (batch.DeadLine?.TimeOfDay.TotalMinutes + batch.LocalTimeZoneBias) -
                                             DateTime.UtcNow.TimeOfDay.TotalMinutes;
               
                batch.DeadLine = batch.DeadLine?.AddMinutes(batch.LocalTimeZoneBias);
            }
        }

        private string GetDepositStatusDisplayName(int depositStatus)
        {
            if (depositStatus == (int)DepositStatusValues.DepositPendingDecision) return "Pending Decisioning";
            return depositStatus == (int)DepositStatusValues.DepositInDecision ? "In Decisioning" : "";
        }

        /// <summary>
        /// Gets details for a batch such as Transactions, checks, stubs and de fields
        /// </summary>
        /// <param name="globalBatchId">Global batch id for requested batch</param>
        /// <returns></returns>
        public DecisioningBatchDto GetBatchDetails(long globalBatchId)
        {
            var result = PopulateBatchData(globalBatchId);

            // Get all Transactions
            DataTable transactionsDt = null;
            _dal.GetTransactions(result.GlobalBatchId, out transactionsDt);
            var transactions = transactionsDt.ToObjectList<DecisioningTransactionDto>();

            // All transaction details
            var translist = new List<DecisioningTransactionDto>();
            foreach (var t in transactions)
                translist.Add(PopulateTransactionData(globalBatchId, t.TransactionId));
            result.Transactions = translist;

            // Transaction children data.
            foreach (var t in result.Transactions)
            {
                t.Payments = PopulatePaymentData(globalBatchId, t.TransactionId);
                t.Stubs = PopulateStubData(globalBatchId, t.TransactionId);
            }

            return result;
        }

        public DecisioningBatchDto GetBatch(long globalBatchId)
        {
            var result = PopulateBatchData(globalBatchId);
            return result;
        }

        private DecisioningBatchDto PopulateBatchData(long globalbatchid)
        {
            // Batch data.
            DataTable resultdt = null;
            _dal.GetBatchDetails(globalbatchid, out resultdt);
            var result = resultdt?.ToObjectList<DecisioningBatchDto>()?.FirstOrDefault();
            CalculateDeadlineMinutes(result);
            result.DepositStatusDisplayName = GetDepositStatusDisplayName(result.DepositStatus);
            if (result.DepositStatus == (int)DepositStatusValues.DepositInDecision)
            {
                DataTable batchDecisioning = null;
                _dal.GetDecisioningBatch(result.GlobalBatchId, out batchDecisioning);
                var decRow = batchDecisioning.Rows[0];
                result.SID = decRow.GetGuid("RAAMSID");
                result.User = decRow.GetString("LogonName");
                result.FirstName = decRow.GetString("FirstName");
                result.LastName = decRow.GetString("LastName");
                result.BatchResetID = decRow.GetGuid("BatchResetID");
            }

            // DE Setup List.
            DataTable dt;
            _dal.GetDESetupFields(result.GlobalBatchId, out dt);
            var delist = dt.ToObjectList<DecisioningDataEntrySetupDto>();
            var olddelist = dt.ToObjectList<cDESetupField>();
            result.DESetupList = delist;

            // Transaction IDs, but no data.
            DataTable transactionsDt;
            _dal.GetTransactions(globalbatchid, out transactionsDt);
            var tids = new List<int>();
            foreach (DataRow r in transactionsDt.Rows)
                tids.Add(r.GetInteger("TransactionID"));
            result.TransactionIds = tids;

            return result;
        }

        private DecisioningTransactionDto PopulateTransactionData(long globalbatchid, long transactionid)
        {
            DataTable transactionsDt = null;
            _dal.GetTransaction(globalbatchid, transactionid, out transactionsDt);
            var transaction = transactionsDt?.ToObjectList<DecisioningTransactionDto>().FirstOrDefault();
            return transaction;
        }

        private List<DecisioningPaymentDto> PopulatePaymentData(long globalbatchid, long transactionid)
        {
            var payments = new List<DecisioningPaymentDto>();

            // Data entry list
            DataTable dt;
            _dal.GetDESetupFields(globalbatchid, out dt);
            var delist = dt.ToObjectList<DecisioningDataEntrySetupDto>();
            var olddelist = dt.ToObjectList<cDESetupField>();

            // Get all Checks
            var checks = _dal.GetChecks(globalbatchid, transactionid, olddelist, out dt);
            var checksdelist = delist.Where(x => x.TableName.Contains("Checks"));
            payments.AddRange(dt.ToObjectList<DecisioningPaymentDto>());

            // Get All checks DE fields.
            for (var i = 0; i < payments.Count(); i++)
            {
                var p = payments.ElementAt(i);
                var dedata = new List<DecisioningDataEntryDto>();
                foreach (var de in checksdelist)
                {
                    dedata.Add(new DecisioningDataEntryDto()
                    {
                        DataType = (int)de.DataType,
                        FieldName = de.FieldName,
                        TableName = de.TableName,
                        Value = dt.Rows[i].GetString(de.TableName + "_" + de.FieldName)
                    });
                }
                p.DataEntry = dedata;
            }

            return payments;
        }

        private List<DecisioningStubDto> PopulateStubData(long globalbatchid, long transactionid)
        {
            var stubs = new List<DecisioningStubDto>();

            // Data entry list
            DataTable dt;
            _dal.GetDESetupFields(globalbatchid, out dt);
            var delist = dt.ToObjectList<DecisioningDataEntrySetupDto>();
            var olddelist = dt.ToObjectList<cDESetupField>();

            // Get all Stubs
            _dal.GetStubs(globalbatchid, transactionid, olddelist, out dt);
            var stubsdelist = delist.Where(x => x.TableName.Contains("Stubs"));
            stubs.AddRange(dt.ToObjectList<DecisioningStubDto>());

            // Get all stub DE definitions (for exception reasons)
            var stubsdedefinitions = _dal.GetDEItemFields(globalbatchid, transactionid, DocumentTypes.Stub, out dt);
            var devalues = dt.ToObjectList<DecisioningDataEntryItemFieldDto>();

            // Get All stubs DE fields.
            foreach (var s in stubs)
                s.DataEntry = devalues.Where(x => x.DEItemRowDataID == s.DEItemRowDataID);

            return stubs;
        }

        public DecisioningTransactionDto GetTransactionDetails(long globalbatchid, long transactionid)
        {
            var transaction = PopulateTransactionData(globalbatchid, transactionid);
            transaction.ParentBatch = PopulateBatchData(globalbatchid);
            transaction.Payments = PopulatePaymentData(globalbatchid, transactionid);
            transaction.Stubs = PopulateStubData(globalbatchid, transactionid);
            return transaction;
        }

        public InProcessExceptionImageRequestDto GetBatchImageDetails(long globalBatchId, int transactionId)
        {
            _eventLog.logEvent("Entering GetBatchImageDetails globalBatchId=" + globalBatchId + " transactionId=" + transactionId
                                , this.GetType().Name
                                , MessageType.Information
                                , MessageImportance.Debug);
            var result = new InProcessExceptionImageRequestDto();

            DataTable dt;
            var delist = new List<cDESetupField>();

            // Get all Checks
            if (_dal.GetChecks(globalBatchId, transactionId, delist, out dt))
            {
                result.Payments = dt.ToObjectList<InProcessExceptionPaymentImageRequestDto>();
            }
            else
            {
                _eventLog.logEvent("Sql read failed for Checks:  globalBatchId=" + globalBatchId + " transactionId=" + transactionId
                    , this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            }

            // Get all Documents; 
            if (_dal.GetDocuments(globalBatchId, transactionId, out dt))
            {
                result.Documents = dt.ToObjectList<InProcessExceptionDocumentImageRequestDto>();
            }
            else
            {
                _eventLog.logEvent("Sql read failed for Documents:  globalBatchId=" + globalBatchId + " transactionId=" + transactionId
                    , this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            }

            return result;
        }


        /// <summary>
        /// Locks a batch to a single user
        /// </summary>
        /// <param name="globalBatchId">batch to be locked</param>
        /// <param name="sid">User's Unique ID</param>
        /// <param name="firstName">User's first name</param>
        /// <param name="lastName">User's last name</param>
        /// <param name="login">User's login</param>
        /// <returns>Success or Failure</returns>
        public bool CheckOutBatch(long globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            var returnVal = false;
            if (!_dal.lockBatch(globalBatchId)) return returnVal;
            var batch = GetBatchDetails(globalBatchId);
            _dal.BeginTrans();
            try
            {
                var affectedrows = 0;
                if (_dal.InsertDecisioningBatches(globalBatchId, batch.NumberOfTransactions, login, firstName, lastName, sid, out affectedrows) && affectedrows >= 0)
                {
                    var updatedrows = 0;
                    returnVal = _dal.UpdateBatchDepositStatus(globalBatchId, DepositStatusValues.DepositInDecision, out updatedrows);
                    _dal.CommitTrans();

                }
                _dal.RollbackTrans();
            }
            catch (Exception)
            {
                _dal.RollbackTrans();
                throw;
            }
            _dal.unlockBatch(globalBatchId);
            return returnVal;
        }
        public bool ResetBatch(long globalBatchId)
        {
            var retVal = false;
            _dal.BeginTrans();
            try
            {
                if (_dal.lockBatch(globalBatchId))
                {
                    var batch = GetBatchDetails(globalBatchId);
                    if (batch.BatchResetID != null)
                    {
                        var affectedrows = 0;
                        retVal = _dal.ResetDecisioningBatch(batch.BatchResetID.Value, out affectedrows);
                        var batchReset = 0;
                        retVal = _dal.UpdateBatchDepositStatus(globalBatchId,
                            DepositStatusValues.DepositPendingDecision, out batchReset);
                        _dal.CommitTrans();
                    }
                    _dal.unlockBatch(globalBatchId);
                }
            }
            catch (Exception)
            {
                retVal = false;
                _dal.RollbackTrans();
                throw;
            }
            return retVal;
        }

        public bool UpdateTransaction(DecisioningTransactionDto dto)
        {
            //updates the transaction status 
            int affectedRows = 0;
            var batch = GetBatchDetails(dto.GlobalBatchId);
            if (_dal.UpdateTxnDecisionStatus(dto.GlobalBatchId, dto.TransactionId,
                (DecisionSrcStatusTypes)dto.TransactionStatus, Guid.Empty, out affectedRows))
            {
                WriteToAuditFile(AuditFileEventTypes.TransactionRejected, "Batch " + batch.BatchId + " Transaction "
                                + dto.TransactionId + " Decision Source Status was set to " +
                                ((DecisionSrcStatusTypes)dto.TransactionStatus).ToString() + ".",
                                batch.BankId, batch.WorkgroupId, batch.BatchId, batch.ProcessDate, batch.User);
            }

            // Gather a list of stubs to be deleted. (The stub is not contained in the DTO, but was in the DB call).
            var deletestubs = batch.Transactions.FirstOrDefault(x => x.TransactionId == dto.TransactionId)
                .Stubs.Where(x => !dto.Stubs.Any(y => y.DEItemRowDataID == x.DEItemRowDataID));
            foreach (var del in deletestubs)
            {
                int affected;
                // Delete the stub.
                _dal.DeleteStub(del.DEItemRowDataID, out affected);
                // Update vertical table.
                _dal.DeleteVerticalStub(del.DEItemRowDataID, out affected);
                // Audit the event.
                WriteToAuditFile(AuditFileEventTypes.StubDeleted
                    , "Batch " + batch.BatchId.ToString() + " Transaction "
                                + dto.TransactionId.ToString()
                                + " Stub " + del.BatchSequence.ToString() + " was deleted."
                    , batch.BankId
                    , batch.WorkgroupId
                    , batch.BatchId
                    , batch.ProcessDate
                    , batch.User);
            }

            // Update the rest of the stubs.
            foreach (var stub in dto.Stubs)
            {
                var rows = 0;
                DataTable dt;
                int returnval;

                // No null values initial check.
                foreach (var de in stub.DataEntry)
                    de.Value = de.Value ?? string.Empty;

                // Gather up our DE data.
                var defields = stub.DataEntry
                    .Select(x => new cDEField(x.TableName, x.FieldName, x.Value, x.DataType, FieldActions.Update))
                    .ToList();
                var desetupfields = stub.DataEntry
                    .Select(x => new cDESetupField(x.TableName, x.FieldName, x.DataType))
                    .ToList();

                // Insert the new stub if we need to.
                if (stub.DEItemRowDataID == Guid.Empty)
                {
                    stub.DEItemRowDataID = InsertNewStub(batch.GlobalBatchId, dto.TransactionId);

                    WriteToAuditFile(AuditFileEventTypes.BatchSaved
                                    , "Batch " + batch.BatchId.ToString() + " Transaction "
                                               + dto.TransactionId.ToString()
                                               + " stub was created."
                                    , batch.BankId
                                    , batch.WorkgroupId
                                    , batch.BatchId
                                    , batch.ProcessDate
                                    , batch.User);
                }

                // Grab the stub from the DB for validation.
                var success = _dal.GetStub(stub.DEItemRowDataID, out dt);
                if (!success || !dt.HasData())
                    return false;

                // Update the stubs.
                success = _dal.UpdateStubs(stub.DEItemRowDataID, defields, desetupfields, out rows);
                if (!success)
                    return false;

                // Now for the stubs DE.
                success = _dal.UpdateStubsDE(stub.DEItemRowDataID, defields, desetupfields, out rows);
                if (!success)
                    return false;

                // Loop through for the vertical table.
                foreach (var de in stub.DataEntry)
                {
                    _dal.UpdateVertStubDE(dto.GlobalBatchId, dto.TransactionId, stub.DEItemRowDataID,
                        de.TableName, de.FieldName, de.Value, (FieldDecisionStatusTypes)de.FieldDecisionStatus, out returnval);
                    if (returnval == 0)
                        _dal.InsertVertStubDE(stub.DEItemRowDataID, de.TableName, de.FieldName, de.Value, out returnval);
                }

                // Handle balancing (future story)
                //var batch = GetBatchDetails(dto.GlobalBatchId);
                //var transaction = batch.Transactions.FirstOrDefault(x => x.TransactionId == dto.TransactionId);
            }
            return true;
        }

        private Guid InsertNewStub(int globalbatchid, int transactionid)
        {
            bool bolRetVal;
            int intRowsAffected;

            long lngGlobalStubID = GetNewGlobalId();
            Guid guidDEItemRowDataID = Guid.NewGuid();

            long lngBatchSequence = GetNextBatchSequence(globalbatchid);

            bolRetVal = _dal.InsertStubsBalancingDE(globalbatchid, transactionid, lngGlobalStubID, lngBatchSequence, 0m, out intRowsAffected) && intRowsAffected >= 0;
            if (!bolRetVal)
                return Guid.Empty;

            bolRetVal = _dal.InsertVertStubsBalancingRowDE(guidDEItemRowDataID, globalbatchid, transactionid, lngGlobalStubID, lngBatchSequence, out intRowsAffected);
            if (!bolRetVal)
                return Guid.Empty;

            return guidDEItemRowDataID;
        }

        /// <summary>
        /// Look, this is terrible code. But the logic was taken
        /// from BatchDecisioning.cs:1716.
        /// </summary>
        /// <returns></returns>
        private int GetNewGlobalId()
        {
            Random random = new Random();
            DataTable dt;
            var newid = -1;

            while (newid == -1)
            {
                var lngGlobalID = random.Next(100000000, 2147483647);
                if (_dal.GetNewGlobalID("Stubs", "GlobalStubID", lngGlobalID, out dt))
                    if (!dt.HasData())
                        newid = lngGlobalID;
            }
            return (newid);
        }

        /// <summary>
        /// Copied straight from BatchDecisioning.cs:1738
        /// </summary>
        /// <param name="GlobalBatchID"></param>
        /// <returns></returns>
        private long GetNextBatchSequence(long GlobalBatchID)
        {
            bool bolRetVal = false;
            DataTable dt;
            DataRow dr;
            long lngMaxBatchSequence = 0;
            bolRetVal = _dal.GetBatchMaxBatchSequence(GlobalBatchID, out dt);

            if (bolRetVal && (dt.Rows.Count > 0))
            {
                dr = dt.Rows[0];
                long lngTmpSequence = 0;
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    lngTmpSequence = 0;
                    switch (dc.ColumnName.ToLower())
                    {
                        case ("maxchecksbatchsequence"):
                        case ("maxstubsbatchsequence"):
                        case ("maxdocumentsbatchsequence"):
                            lngTmpSequence = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            if (lngTmpSequence > lngMaxBatchSequence)
                                lngMaxBatchSequence = lngTmpSequence;
                            break;
                    }
                }
            }
            return (lngMaxBatchSequence + 1);
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            int rows = 0;
            var batch = GetBatchDetails(dto.globalBatchId);
            var retVal = new OLDBaseGenericResponse<bool>
            {
                Data = false,
                Errors = new List<string>(),
                Status = StatusCode.FAIL
            };
            //uncomment the following lines when we have
            //UI validation and we are switching fields statuses
            ////var retVal = ValidatePedingItems(batch);
            //if (retVal.Errors.Count != 0)
            //{
            //    retVal.Status = StatusCode.FAIL;
            //    return retVal;
            //}
            var status = GetDepositStatus(batch);
            _dal.BeginTrans();
            //this was in the old function, so we do it here too.
            _dal.setMICRVerifyStatus(batch.GlobalBatchId);
            _dal.calculateBatch(batch.GlobalBatchId, status);

            WriteToAuditFile(AuditFileEventTypes.BatchCompleted
                , "Batch " + batch.BatchId + " was set to " + status + "."
                , batch.BankId
                , batch.WorkgroupId
                , batch.BatchId
                , batch.ProcessDate, batch.User);
            //done
            var result = _dal.CompleteDecisioningBatch(dto.batchResetId, out rows);
            var statusresult = _dal.UpdateBatchStatus(batch.GlobalBatchId, DecisionStatusTypes.DecisionComplete, out var batchesUpdated);
            if (result && statusresult)
            {
                _dal.CommitTrans();
                retVal.Status = StatusCode.SUCCESS;
            }
            else
            {
                _dal.RollbackTrans();
                retVal.Status = StatusCode.FAIL;
                retVal.Errors.Add("There was an error completing the batch.");
            }
            return retVal;
        }

        private OLDBaseGenericResponse<bool> ValidatePedingItems(DecisioningBatchDto batch)
        {
            DataTable dt;
            var ret = new OLDBaseGenericResponse<bool> { Data = false, Errors = new List<string>() };
            if (_dal.GetPendingTransactionCount(batch.GlobalBatchId, out dt) && dt.Rows.Count > 0)
            {
                var dr = dt.Rows[0];
                var pendingTransactionCount = dr.GetInteger("PendingTransactionCount");
                if (pendingTransactionCount > 0)
                    ret.Errors.Add("Cannot complete a Batch containing Transactions requiring decisioning.");
            }
            //comment out when/if we do Destination Lockboxes for Commingling
            //if (_dal.GetUndefinedDestLbxCount(batch.GlobalBatchId, out dt) && dt.Rows.Count > 0)
            //{
            //    var dr = dt.Rows[0];
            //    var rejectedDeItemCount = dr.GetInteger("UndefinedDestLbxCount");
            //    if (rejectedDeItemCount > 0)
            //    {
            //        ret.Errors.Add("Cannot complete a Batch containing pending Commingled Transactions that do " +
            //                       "not have a Destination Lockbox defined.");
            //    }
            //}
            //TODO: Add balancing validation
            if (ret.Errors.Count == 0) ret.Data = true;
            return ret;
        }

        private DepositStatusValues GetDepositStatus(DecisioningBatchDto batch)
        {
            DepositStatusValues newStatus;
            bool bolRetVal;
            var unkilledCheckCount = 0;
            DataRow dr;
            DataTable dt;
            bolRetVal = _dal.GetUnKilledCheckCount(batch.GlobalBatchId, out dt);

            if (dt.HasData())
            {
                dr = dt.Rows[0];
                unkilledCheckCount = dr.GetInteger("theCount");
            }
            var rejectedTransactionCount = 0;
            _dal.GetRejectedTransactionCount(batch.GlobalBatchId, out dt);
            if (dt.HasData())
            {
                dr = dt.Rows[0];
                rejectedTransactionCount = dr.GetInteger("theCount");
            }
            if (batch.IsCommingled)
            {
                newStatus = DepositStatusValues.DepositPendingSplit;
            }
            else
            {
                if (unkilledCheckCount == 0)
                {
                    if (batch.IsImageExchange)
                    {
                        newStatus = rejectedTransactionCount > 0 ?
                            DepositStatusValues.PendingDecisionSplit : DepositStatusValues.PendingPrint;
                    }
                    else
                    {
                        newStatus = DepositStatusValues.PendingEncode;
                    }
                }
                else
                {
                    newStatus = batch.UseCar > 0 ? DepositStatusValues.PendingCAR : DepositStatusValues.PendingKFI;
                }
            }
            return newStatus;
        }
        /// <summary>
        /// Writes to audit file
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="eventDesc"></param>
        /// <param name="bankID"></param>
        /// <param name="lockboxID"></param>
        /// <param name="batchID"></param>
        /// <param name="processingDate"></param>
        /// <param name="logonName"></param>
        private void WriteToAuditFile(AuditFileEventTypes eventType
                                    , string eventDesc
                                    , long bankID
                                    , long lockboxID
                                    , long batchID
                                    , DateTime processingDate, string logonName)
        {
            try
            {
                eventDesc = eventDesc + "[LogonName=" + logonName + ";]";
                _dal.InsertAuditFile(eventType,
                                               eventDesc,
                                               bankID,
                                               lockboxID,
                                               batchID,
                                               processingDate);
            }
            catch (Exception e)
            {
                _eventLog.logError(e);
                _eventLog.logEvent("Unable to write to event[" + eventDesc + "] to the AuditFile."
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        public IEnumerable<DecisioningDataEntrySetupDto> GetSetupFields(int globalbatchid)
        {
            DataTable dt;
            _dal.GetDESetupFields(globalbatchid, out dt);
            return dt.ToObjectList<DecisioningDataEntrySetupDto>();
        }
    }
}
