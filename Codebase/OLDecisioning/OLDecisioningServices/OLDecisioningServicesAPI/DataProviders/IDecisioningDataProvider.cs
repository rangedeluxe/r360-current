﻿using System;
using System.Collections.Generic;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.OlfRequestCommon;

namespace WFS.RecHub.OLDecisioningServicesAPI.DataProviders
{
    public interface IDecisioningDataProvider
    {
        IEnumerable<DecisioningBatchDto> GetPendingBatches();
        IEnumerable<DecisioningBatchDto> GetBatches(long bankId, long workgroupId);
        DecisioningBatchDto GetBatchDetails(long globalBatchId);
        DecisioningBatchDto GetBatch(long globalBatchId);
        DecisioningTransactionDto GetTransactionDetails(long globalBatchId, long tranasctionid);
        bool CheckOutBatch(long globalBatchId, Guid sid, string firstName, string lastName, string login);
        bool ResetBatch(long globalBatchId);
        bool UpdateTransaction(DecisioningTransactionDto dto);
        InProcessExceptionImageRequestDto GetBatchImageDetails(long globalBatchId, int transactionId);
        OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto);
        IEnumerable<DecisioningDataEntrySetupDto> GetSetupFields(int globalbatchid);
    }
}
