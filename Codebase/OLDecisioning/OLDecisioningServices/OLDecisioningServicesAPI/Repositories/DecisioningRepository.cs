﻿using System;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.OLDecisioningServicesAPI.DataProviders;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.OLDecisioningServicesAPI.Repositories
{
    public class DecisioningRepository : IDecisioningRepository
    {
        private readonly IDecisioningDataProvider _dataProvider;
        private readonly TransactionValidationService _validationservice;

        private IServiceContext _serviceContext;
        public DecisioningRepository(IServiceContext context, IDecisioningDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
            _serviceContext = context;
            _validationservice = new TransactionValidationService();
        }
        public PendingBatchesResponse GetAll()
        {
            var response = new PendingBatchesResponse
            {
                Batches = _dataProvider.GetPendingBatches(),
                Status = StatusCode.SUCCESS
            };
            return response;
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            var response = new PendingBatchesResponse
            {
                Batches = _dataProvider.GetBatches(bankId, workgroupId),
                Status = StatusCode.SUCCESS
            };
            return response;
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            var response = new OLDBaseGenericResponse<DecisioningBatchDto>
            {
                Data = _dataProvider.GetBatchDetails(globalBatchId),
                Status = StatusCode.SUCCESS
            };
            return response;
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            var response = new OLDBaseGenericResponse<DecisioningTransactionDto>
            {
                Data = _dataProvider.GetTransactionDetails(globalBatchId, transactionid),
                Status = StatusCode.SUCCESS
            };
            return response;
        }

        public OLDBaseGenericResponse<InProcessExceptionImageRequestDto> GetBatchImageDetails(int globalBatchId, int transactionId)
        {
            var response = new OLDBaseGenericResponse<InProcessExceptionImageRequestDto>
            {
                Data = _dataProvider.GetBatchImageDetails(globalBatchId, transactionId),
                Status = StatusCode.SUCCESS
            };
            return response;
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(long globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            var response = new OLDBaseGenericResponse<bool>
            {
                Data = _dataProvider.CheckOutBatch(globalBatchId, sid, firstName, lastName, login)
            };
            return response;
        }

        public OLDBaseGenericResponse<bool> ResetBatch(long globalBatchId)
        {
            var response = new OLDBaseGenericResponse<bool> {Data = _dataProvider.ResetBatch(globalBatchId)};
            return response;
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            var response = new UpdateTransactionResponse();

            // First, validate the request.
            var setupfields = _dataProvider.GetSetupFields(dto.GlobalBatchId);
            var valresponse = _validationservice.Validate(dto, setupfields);
            response.ValidationResults = valresponse;
            if (!valresponse.IsValid)
            {
                // Validation failed, so we're going to set our errors and kick out.
                response.Status = StatusCode.FAIL;
                return response;
            }

            // Update the transaction.
            response.Data = _dataProvider.UpdateTransaction(dto);

            return response;
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            var response = _dataProvider.CompleteBatch(dto);
            return response;
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            var response = new OLDBaseGenericResponse<DecisioningBatchDto>
            {
                Data = _dataProvider.GetBatch(globalBatchId),
                Status = StatusCode.SUCCESS
            };
            return response;
        }
    }
}
