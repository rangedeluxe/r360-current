﻿using System;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.OlfRequestCommon;

namespace WFS.RecHub.OLDecisioningServicesAPI.Repositories
{
    public interface IDecisioningRepository
    {
        PendingBatchesResponse GetAll();
        PendingBatchesResponse GetBatches(long bankId, long workgroupId);
        OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId);
        OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId);
        OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid);
        OLDBaseGenericResponse<bool> CheckOutBatch(long globalBatchId, Guid sid, string firstName, string lastName, string login);
        OLDBaseGenericResponse<bool> ResetBatch(long globalBatchId);
        UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto);
        OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto);
        OLDBaseGenericResponse<InProcessExceptionImageRequestDto> GetBatchImageDetails(int globalBatchId, int transactionId);
    }
}
