using System.Collections;
using System.Collections.Specialized;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
*    Module: ipoDecisioningOptions
*  Filename: ipoDecisioningOptions.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 12257 JMC 07/28/2005
*   -Initial release version.
* CR 28522 JMC 01/06/2010
*    - Class now inherits from cSiteOptions
* CR 33199 JCS 11/09/2011
*    - Added class constructor with input parameter SiteKey.
*    - Modified class to either use settings from SiteKey section or default to
*      [ipoDecisioningAPI] section when SiteKey is not used.
* WI 96283 CRG 04/11/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLDecisioningServicesAPI
******************************************************************************/
namespace WFS.RecHub.OLDecisioningServicesAPI {

	/// <summary>
	/// Summary description for ipoDecisioningOptions.
	/// </summary>
	public class OLDecisioningOptions : cSiteOptions {

        private string _XmlSavePath = string.Empty;

        /// <summary>
        /// 
        /// </summary>
		public OLDecisioningOptions() : base(ipoLib.INISECTION_DECISIONING_API) {

            // Use log file path from INISECTION_DECISIONING_API section
            StringCollection colSiteOptions = ipoINILib.GetINISection(ipoLib.INISECTION_DECISIONING_API);
            string strKey;
            string strValue;

            const string XML_SAVE_PATH = "XmlSavePath";

            System.Collections.IEnumerator myEnumerator;

            //Load section options.
            myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {

                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if(strKey.ToLower() == XML_SAVE_PATH.ToLower()) {
                    _XmlSavePath = ipoLib.cleanPath(strValue); 
                }
            }
		}
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SiteKey"></param>
		public OLDecisioningOptions(string SiteKey) : base(SiteKey) {
            // Use log file path from SiteKey section
            _XmlSavePath = base.DecisioiningAPIXmlSavePath;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string XmlSavePath {
            get {
                return (_XmlSavePath);
            }
        }
	}
}
