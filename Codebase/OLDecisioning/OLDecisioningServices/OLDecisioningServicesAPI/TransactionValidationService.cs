using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using OLDecisioningShared.DTO;
using WFS.RecHub.Common;
using WFS.RecHub.MaskValidation;

namespace WFS.RecHub.OLDecisioningServicesAPI
{
    /// <summary>
    /// Class provides logic to validation the input of a transaction.  Shouldn't really depend on anything,
    /// and I wouldn't expect this needs to be mocked out.
    /// </summary>
    public class TransactionValidationService
    {
        public ValidationResponseDto Validate(DecisioningTransactionDto updatedtransaction, IEnumerable<DecisioningDataEntrySetupDto> setupFields)
        {
            var output = new ValidationResponseDto();

            // Quick validate our input.
            if (updatedtransaction == null)
                throw new ArgumentNullException(nameof(updatedtransaction), "Transaction is required for validation.");
            if (setupFields == null)
                throw new ArgumentNullException(nameof(setupFields), "Setup fields is required for validation.");

            var setupFieldList = setupFields.ToList();

            // Validate stubs
            for (int i = 0; i < updatedtransaction.Stubs.Count(); i++)
            {
                var stub = updatedtransaction.Stubs.ElementAt(i);
                foreach (var de in stub.DataEntry)
                {
                    var setup = setupFieldList.FirstOrDefault(x => x.FieldName == de.FieldName && x.TableName == de.TableName && x.DataType == de.DataType);

                    var validationErrors = ValidateStubField(updatedtransaction, setup, de.Value);
                    foreach (var validationErrorType in validationErrors)
                    {
                        var validationErrorDto = new ValidationErrorDto
                        {
                            Type = validationErrorType,
                            DEItemRowDataID = stub.DEItemRowDataID,
                            RowIndex = i,
                            FieldTitle = de.Title,
                            Value = de.Value
                        };

                        // There are some ValidationErrorDto properties that we only set for certain errors.
                        if (validationErrorDto.Type == ValidationErrorType.SetupFieldNotFound)
                            validationErrorDto.FieldName = de.FieldName;
                        if (validationErrorDto.Type == ValidationErrorType.MaxLengthExceeded)
                            validationErrorDto.MaxLength = setup?.Length ?? 0;

                        output.Errors.Add(validationErrorDto);
                    }
                }
            }

            output.IsValid = !output.Errors.Any();
            return output;
        }
        private IEnumerable<ValidationErrorType> ValidateStubField(DecisioningTransactionDto updatedtransaction,
            DecisioningDataEntrySetupDto setup, string value)
        {
            // Case - If we have a DE record that doesn't have a setup field.
            if (setup == null)
            {
                yield return ValidationErrorType.SetupFieldNotFound;
                yield break;
            }

            // Case - If we have a required field on a non-rejected transaction and it's empty.
            if (setup.IsRequired && updatedtransaction.TransactionStatus != 4 && string.IsNullOrWhiteSpace(value))
            {
                yield return ValidationErrorType.RequiredFieldBlank;
            }

            // Case - If we have a DE record that exceeds its maxlength.
            if ((setup.DataType == DataEntryDataType.Alphanumeric ||
                 setup.DataType == DataEntryDataType.Numeric ||
                 setup.DataType == DataEntryDataType.Currency ||
                 setup.DataType == DataEntryDataType.Date)
                && value != null && value.Length > setup.Length)
            {
                yield return ValidationErrorType.MaxLengthExceeded;
            }

            // Case - data-type validation
            if (value != null)
            {
                var dataTypeError = ValidateDataType(setup.DataType, value);
                if (dataTypeError.HasValue)
                    yield return dataTypeError.Value;
            }

            // Case - mask validation
            var maskResult = MaskValidator.ValidateMask(setup.Mask, value);
            if (!maskResult.Matched)
            {
                yield return ValidationErrorType.MaskValidationFailed;
            }
        }
        private ValidationErrorType? ValidateDataType(DataEntryDataType dataType, [NotNull] string value)
        {
            var dataTypeError = DataEntryDataTypeValidation.ValidateInputValue(dataType, value);
            switch (dataTypeError)
            {
                case DataEntryDataTypeValidationError.InvalidNumericValue:
                    return ValidationErrorType.InvalidNumericValue;
                case DataEntryDataTypeValidationError.InvalidCurrencyValue:
                    return ValidationErrorType.InvalidCurrencyValue;
                case DataEntryDataTypeValidationError.MaxCurrencyValueExceeded:
                    return ValidationErrorType.MaxCurrencyValueExceeded;
                case DataEntryDataTypeValidationError.MinCurrencyValueExceeded:
                    return ValidationErrorType.MinCurrencyValueExceeded;
                case DataEntryDataTypeValidationError.InvalidDateTimeValue:
                    return ValidationErrorType.InvalidDateTimeValue;
                default:
                    return null;
            }
        }
    }
}