﻿using OLDecisioningShared.Responses;
using System;
using System.Runtime.CompilerServices;
using OLDecisioningShared.DTO;
using WFS.integraPAY.Online.OLDecisioningServicesAPI.DataProviders;
using WFS.RecHub.Common;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.OLDecisioningServicesAPI.Repositories;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.OLDecisioningServicesAPI
{
    public class DecisioningApi : APIBase
    {
        private readonly IDecisioningRepository _repository;
        
        #region API Wrappers

        private T Do<T>(Func<T> operation, [CallerMemberName] string procName = null)
           where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T { Status = StatusCode.FAIL };
            try
            {
                // Perform operation (no pre-conditions. Decisioning sits on a different db)
                return operation();
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }

        #endregion

        public DecisioningApi(IServiceContext serviceContext) : base(serviceContext)
        {
            _repository = new DecisioningRepository(serviceContext, new DatabaseDataProvider());
        }

        public DecisioningApi(IServiceContext serviceContext, IDecisioningRepository repository) : base(serviceContext)
        {
            _repository = repository;
        }

      
        public PendingBatchesResponse GetPendingBatches()
        {
            return Do<PendingBatchesResponse>(() =>
            {
                return _repository.GetAll();
            });
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            return Do<UpdateTransactionResponse>(() =>
            {
                return _repository.UpdateTransaction(dto);
            });
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            return Do<PendingBatchesResponse>(() =>
            {
                return _repository.GetBatches(bankId, workgroupId);
            });
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            return Do<OLDBaseGenericResponse<DecisioningBatchDto>>(() =>
            {
                return _repository.GetBatchDetails(globalBatchId);
            });
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            return Do<OLDBaseGenericResponse<DecisioningBatchDto>>(() =>
            {
                return _repository.GetBatch(globalBatchId);
            });
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            return Do<OLDBaseGenericResponse<DecisioningTransactionDto>>(() =>
            {
                return _repository.GetTransactionDetails(globalBatchId, transactionid);
            });

        }

        public OLDBaseGenericResponse<InProcessExceptionImageRequestDto> GetBatchImageDetails(int globalBatchId, int transactionId)
        {
            return Do<OLDBaseGenericResponse<InProcessExceptionImageRequestDto>>(() =>
            {
                return _repository.GetBatchImageDetails(globalBatchId, transactionId);
            });
        }

        protected override bool ValidateSID(out int userID)
        {
            throw new NotImplementedException("The OLDecisioning Service does not validate SIDs");
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(long globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            return Do(() =>
            {
                return _repository.CheckOutBatch(globalBatchId, sid, firstName, lastName, login);
            });
        }

        public OLDBaseGenericResponse<bool> ResetBatch(long globalBatchId)
        {
            return Do(() =>
            {
                return _repository.ResetBatch(globalBatchId);
            });
        }

        protected override ActivityCodes RequestType { get; }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            return Do(() => _repository.CompleteBatch(dto));
        }
    }
}
