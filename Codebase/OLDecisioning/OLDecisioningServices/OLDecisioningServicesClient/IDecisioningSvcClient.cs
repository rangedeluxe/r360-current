﻿using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OlfRequestCommon;

namespace WFS.RecHub.OLDecisioningServicesClient
{
    public interface IDecisioningSvcClient
    {
        OLDBaseGenericResponse<string> Ping();

        UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto);

        PendingBatchesResponse GetAll();

        PendingBatchesResponse GetBatches(long bankId, long workgroupId);

        OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId);
        OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId);
        OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid);

        OLDBaseGenericResponse<InProcessExceptionImageRequestDto> GetBatchImageDetails(int globalBatchId, int transactionId);

        OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login);

        OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId);

        OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto);
    }
}
