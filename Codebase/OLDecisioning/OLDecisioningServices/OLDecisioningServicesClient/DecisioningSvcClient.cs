﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using OLDecisioningShared;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.OLDecisioningShared;
using WFS.LTA.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.OlfRequestCommon;

namespace WFS.RecHub.OLDecisioningServicesClient
{
    public class DecisioningSvcClient : IDecisioningSvcClient
    {
        private readonly IDecisioning _decisioningService = null;
        private readonly string _siteKey;
        private readonly ltaLog _log;

        public DecisioningSvcClient(string siteKey, ltaLog log) : this(siteKey, log, R360ServiceFactory.Create<IDecisioning>())
        {
        }

        public DecisioningSvcClient(string siteKey, ltaLog log, IDecisioning decisioningService)
        {
            _siteKey = siteKey;
            _log = log;
            _decisioningService = decisioningService;
        }

        protected virtual T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
        {
            // All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
            try
            {
                using (new OperationContextScope((IContextChannel)_decisioningService))
                {
                    // Set context inside of operation scope
                    DecisioningServiceContext.Init(_siteKey);

                    return operation();
                }
            }
            catch (Exception ex)
            {
                _log.logError(ex, this.GetType().Name, methodName);
                throw;
            }
        }

        public OLDBaseGenericResponse<string> Ping()
        {
            return Do(() => _decisioningService.Ping());
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            return Do(() => _decisioningService.UpdateTransaction(dto));
        }

        public PendingBatchesResponse GetAll()
        {
            return Do(() => _decisioningService.GetPendingBatches());
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            return Do(() => _decisioningService.GetBatches(bankId, workgroupId));
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            return Do(() => _decisioningService.GetBatchDetails(globalBatchId));
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            return Do(() => _decisioningService.GetBatch(globalBatchId));
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            return Do(() => _decisioningService.GetTransactionDetails(globalBatchId, transactionid));
        }

        public OLDBaseGenericResponse<InProcessExceptionImageRequestDto> GetBatchImageDetails(int globalBatchId, int transactionId)
        {
            return Do(() => _decisioningService.GetBatchImageDetails(globalBatchId, transactionId));
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            return Do(() => _decisioningService.CheckOutBatch(globalBatchId, sid, firstName, lastName, login));
        }

        public OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId)
        {
            return Do(() => _decisioningService.ResetBatch(globalBatchId));
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            return Do(() => _decisioningService.CompleteBatch(dto));
        }
    }
}
