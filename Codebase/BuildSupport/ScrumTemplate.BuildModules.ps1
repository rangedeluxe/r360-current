﻿function Get-TFS {
    param ([string] $ServerName = $(Throw 'serverName is required'))
    # load the required dll
    [void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.Client");

    $propertiesToAdd = (
        ('VCS', 'Microsoft.TeamFoundation.VersionControl.Client', 'Microsoft.TeamFoundation.VersionControl.Client.VersionControlServer'),
        ('WIT', 'Microsoft.TeamFoundation.WorkItemTracking.Client', 'Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItemStore'),
        ('BS', 'Microsoft.TeamFoundation.Build.Client', 'Microsoft.TeamFoundation.Build.Client.IBuildServer'),
        ('CSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.ICommonStructureService'),
        ('GSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.IGroupSecurityService')
    );

    # fetch the TFS instance, but add some useful properties to make life easier
    # Make sure to "promote" it to a psobject now to make later modification easier
    [psobject] $tfs = [Microsoft.TeamFoundation.Client.TeamFoundationServerFactory]::GetServer($ServerName);
    foreach ($entry in $propertiesToAdd)
    {
        $scriptBlock = '
            [System.Reflection.Assembly]::LoadWithPartialName("{0}") > $null
            $this.GetService([{1}])
        ' -f $entry[1],$entry[2];
        $tfs | add-member scriptproperty $entry[0] $ExecutionContext.InvokeCommand.NewScriptBlock($scriptBlock);
    }
    return $tfs;
}

function EnsureDir {
    param ([string] $path,
            [bool] $cleanPath = $False)
    if(!(Test-Path -Path $path)) {
        New-Item $path -type directory | Out-Null;
    }
    else {
        if($cleanPath) {
            Remove-Item -Recurse -Force (Join-Path $path "*")|Out-Null;
        }
    }
    return;
}

################################################################################
# Determine if a child WI is complete
################################################################################
function IsChildComplete
{
    param
    (
        [int] $local:ChildID
    )
    $local:Completed = $false;
    $local:ChildWorkItem = $TFS.WIT.GetWorkItem($local:ChildID);

    if( $local:ChildWorkItem.State -eq "Resolved" -or $local:ChildWorkItem.State -eq "Closed" )
    {
        $local:Completed = $true;
    }
    return $local:Completed;
}

################################################################################
# Determine if all childern of the parent WI are complete
################################################################################
function IsParentComplete
{
    param
    (
        [int] $local:ParentID
    )

    $local:Complete = $true;
    $local:ParentWorkItem = $TFS.WIT.GetWorkItem($local:ParentID);
    foreach( $local:Link in $local:ParentWorkItem.Links)
    {
        if( $local:Link.BaseType -eq "RelatedLink")
        {
            if( $local:Link.LinkTypeEnd.Name -eq "Child" )
            {
                if( (IsChildComplete $Local:Link.RelatedWorkItemId) -eq $false )
                {
                    $local:Complete = $false;
                    break;
                }
            }
        }
    }
    if( $local:Complete -eq $true )
    {
        foreach( $local:Field in $local:ParentWorkItem.Fields )
        {
            if( $local:Field.ReferenceName -eq "System.AssignedTo" )
            {
                $local:Field.Value = "Steve Eckhart";
            }
        }
        $local:ParentWorkItem.Save();
    }
}

################################################################################
# Reassign the WI to QA if all childern of the parent are complete
################################################################################
function AssignToQA
{
    param
    (
        [object] $local:WorkItem
    )
    foreach( $local:Link in $local:WorkItem.Links )
    {
        if( $local:Link.BaseType -eq "RelatedLink" )
        {
            if( $local:Link.LinkTypeEnd.Name -eq "Parent" )
            {
                IsParentComplete $local:Link.RelatedWorkItemId
            }
        }
    }
}
<#
export-modulemember -function Get-TFS;
export-modulemember -function EnsureDir;
export-modulemember -function IsChildComplete;
export-modulemember -function IsParentComplete;
export-modulemember -function AssignToQA;
##>