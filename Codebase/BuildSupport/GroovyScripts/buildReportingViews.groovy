def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildReportingViews(jobItems)

}

def buildReportingViews(jobItems) {

  def binariesFolder = "${jobItems.Workspace}\\binaries\\ReportingViews"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Reporting Views..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\UI\\UISolution\\UISolution.sln")

  echo "Building Reporting views solution..."
  runMSBuild('2017','.\\Codebase\\UI\\UISolution\\UISolution.sln',"${binariesFolder}")

  //gatherReportingViewArtifacts(binariesFolder, deploymentFolder)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\ReportingViews\\', 'Codebase\\BuildSupport\\PublishMap.ReportingViews.xml')

}

def gatherReportingViewArtifacts(binariesFolder, deploymentFolder) {
  echo "Creating ${deploymentFolder} ReportingView deployment artifacts in workspace..."

  dir("${binariesFolder}/_PublishedWebsites/ReportingViews/bin") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\bin"
  }

  dir("${binariesFolder}/_PublishedWebsites/ReportingViews") {
    bat "copy Global.asax ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\Global.asax"
    bat "copy Error.aspx ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\Error.aspx"
    bat "copy Error.html ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\Error.html"
    bat "copy web.config ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\web.config.oem"
    bat "copy web.config.template ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\web.config.template"
    bat "copy identity.web.config.template ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\identity.web.config.template"
    bat "copy identityservices.web.config.template ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\identityservices.web.config.template"
    bat "copy webmachinekey.config.template ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\webmachinekey.config.template"
  }

  dir("${binariesFolder}/_PublishedWebsites/ReportingViews/Views") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\Views"
  }

  dir("${binariesFolder}/_PublishedWebsites/ReportingViews/Assets") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\RecHubReportingViews\\Assets"
  }
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecReportingViews"
	def artifactoryFilesToUpload = """{
          "files": [
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/ReportingViews/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubReportingViews/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/ReportingViews/bin/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubReportingViews/bin/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/ReportingViews/Views/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubReportingViews/Views/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/ReportingViews/Assets/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubReportingViews/Assets/",
              "recursive": false
            }
          ]
        }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;