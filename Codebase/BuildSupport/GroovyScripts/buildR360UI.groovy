def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\R360UI"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Setting up nuget packages..."
  dir('Codebase/R360UI') {
    bat "npm i"
    bat "npm run build"
  }
  
  //powershell './Codebase/BuildSupport/StampVersion/StampVersion.exe "${ENV:CustomBuildNumber}" "${workspace}\\Codebase"'
  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\R360UI\\R360UI.sln")
  
  echo "Building R360UI solution..."
  runMSBuild('2017','.\\Codebase\\R360UI\\R360UI.sln',"${binariesFolder}")

  gatherR360UIArtifacts(binariesFolder, deploymentFolder)

  if (jobItems.PublishToArtifactory) {
    publishToArtifactory(jobItems)
  }
  
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/R360UI"
  def artifactoryFilesToUpload = """{
        "files": [
          {
            "pattern": "${jobItems.BranchPath}/wwwroot/R360UI/*.*",
            "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/R360UI/",
            "recursive": false
          },
          {
            "pattern": "${jobItems.BranchPath}/wwwroot/R360UI/bin/*.*",
            "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/R360UI/bin/",
            "recursive": false
          },
          {
            "pattern": "${jobItems.BranchPath}/wwwroot/R360UI/bin/roslyn/*.*",
            "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/R360UI/bin/roslyn/",
            "recursive": false
          },
          {
            "pattern": "${jobItems.BranchPath}/wwwroot/R360UI/assets/*.*",
            "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/R360UI/assets/",
            "recursive": false
          }
        ]
      }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def gatherR360UIArtifacts(binariesFolder, deploymentFolder) {
  echo "Creating ${deploymentFolder} R360UI deployment artifacts in workspace..."

  dir("${binariesFolder}") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\wwwroot\\R360UI\\bin"
  }

  dir("${binariesFolder}/roslyn") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\wwwroot\\R360UI\\bin\\roslyn"
    bat "xcopy /y /i *.exe ${deploymentFolder}\\wwwroot\\R360UI\\bin\\roslyn"
  }

  dir("${binariesFolder}/_PublishedWebsites/R360UI") {
    bat "xcopy /y /i *.template ${deploymentFolder}\\wwwroot\\R360UI"
    bat "xcopy /y /i *.asax ${deploymentFolder}\\wwwroot\\R360UI"
  }

  dir('Codebase/R360UI/dist') {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\R360UI"
  }

  dir('Codebase/R360UI/src/assets') {
    bat "xcopy /y /i *.json* ${deploymentFolder}\\wwwroot\\R360UI\\assets"
  }
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;