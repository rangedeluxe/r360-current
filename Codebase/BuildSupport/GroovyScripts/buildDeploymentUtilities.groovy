def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Building Deployment Utilities solution..."
  runMSBuild('2017','.\\Codebase\\Apps\\DeploymentUtilities\\DeploymentUtilities.sln',"${binariesFolder}")

  String[] testFiles = ["${binariesFolder}\\DeploymentUnitTests.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'Binaries', 'Codebase\\BuildSupport\\PublishMap.DeploymentUtilities.xml')
  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;