def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\FileImportServices"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build File Import Services..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\FileImport\\FileImportSolution\\FileImportSolution.sln")

  echo "Building solution..."
  runMSBuild('2017','.\\Codebase\\FileImport\\FileImportSolution\\FileImportSolution.sln',"${binariesFolder}")

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\FileImportServices\\', 'Codebase\\BuildSupport\\PublishMap.FileImportSolution.xml')
  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;