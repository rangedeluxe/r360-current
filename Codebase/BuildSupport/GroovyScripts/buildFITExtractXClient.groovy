def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildXClient(jobItems)
}

def buildXClient(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\FITExtractXClient"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Building FIT solution..."
  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\FileImport\\fitExtractXClient\\fitExtractXClient.sln")
  runMSBuild('2017','.\\Codebase\\FileImport\\fitExtractXClient\\fitExtractXClient.sln',"${binariesFolder}")

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\FITExtractXClient\\', 'Codebase\\BuildSupport\\PublishMap.fitExtractXClient.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;