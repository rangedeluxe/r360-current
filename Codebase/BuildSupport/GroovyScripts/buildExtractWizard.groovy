def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  buildExtractWizard(jobItems)
}

def buildExtractWizard(jobItems) {

  def binariesFolder = "${jobItems.Workspace}\\binaries\\ExtractWizard"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Extract Wizard..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\DataOutput\\ExtractWizard\\ExtractWizard.sln")

  echo "Building Extract Wizard solution..."
  //def buildSolution = ".\\Codebase\\DataOutput\\ExtractWizard\\ExtractWizard.sln"
  //def executeBuild = "\"${tool '15.0 (VS2017)'}\\msbuild.exe\" "
  /*def properties = """
      /p:SkipInvalidConfigurations=true 
      /p:Configuration=Release 
      \"/p:Platform=Any CPU\" 
      /p:Version=${jobItems.VersionToBuild}
      /p:OutDir=${binariesFolder} ${buildSolution}
    """
    */
  //def properties = "/p:SkipInvalidConfigurations=true /p:Configuration=Release \"/p:Platform=Any CPU\" /p:AssemblyVersion=${jobItems.VersionToBuild} /p:OutDir=${binariesFolder} ${buildSolution}"
    
  //executeBuild = executeBuild + properties
  //echo "MSBuild command is - ${executeBuild}"

  //bat executeBuild
  
  runMSBuild('2017','.\\Codebase\\DataOutput\\ExtractWizard\\ExtractWizard.sln',"${binariesFolder}")

  String[] testFiles = ["${binariesFolder}\\Test.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\ExtractWizard\\', 'Codebase\\BuildSupport\\PublishMap.ExtractWizard.xml')
  
}



def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;