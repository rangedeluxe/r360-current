def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\HubReportService"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Hub Report Service..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\R360Services\\Reporting\\ReportingSolution\\ReportingSolution.sln")

  echo "Building solution..."
  
  runMSBuild('2017','.\\Codebase\\R360Services\\Reporting\\ReportingSolution\\ReportingSolution.sln',"${binariesFolder}")

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\HubReportService\\', 'Codebase\\BuildSupport\\PublishMap.ReportingSolution.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;