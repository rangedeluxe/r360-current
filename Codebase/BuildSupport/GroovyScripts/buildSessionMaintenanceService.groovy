def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\SessionMaintenanceService"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Session Maintenance Services..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\R360Services\\SessionMaintenance\\SessionMaintenanceSolution\\SessionMaintenanceSolution.sln")

  echo "Building solution..."
  runMSBuild('2017','.\\Codebase\\R360Services\\SessionMaintenance\\SessionMaintenanceSolution\\SessionMaintenanceSolution.sln',"${binariesFolder}")

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\SessionMaintenanceService\\', 'Codebase\\BuildSupport\\PublishMap.SessionMaintenanceSolution.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;