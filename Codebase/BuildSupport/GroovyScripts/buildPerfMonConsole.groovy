def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\Diagnostics\\PerfMonConsole\\PerfMonConsole.sln"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build PerfMonConsole ..."
 
  echo "Building PerfMonConsole ..."

  commonScripts.buildMSProject(solution, binariesFolder)


  def artifactoryFilesToUpload = """{ 
	"files": [
	  {
		  "pattern": "${binariesFolder}\\PerfMonConsole.exe",
		  "target": "tmsa.r360/${jobItems.BranchToBuild}/Utilities/PerfMonConsole/",
		  "recursive": false
	  },
	  {
		  "pattern": "${binariesFolder}\\PerfMonConsole.exe.config",
		  "target": "tmsa.r360/${jobItems.BranchToBuild}/Utilities/PerfMonConsole/",
		  "recursive": false
	  },
	  {
		  "pattern": ".\\Codebase\\Diagnostics\\PerfMonConsole\\*.txt",
		  "target": "tmsa.r360/${jobItems.BranchToBuild}/Utilities/PerfMonConsole/",
		  "recursive": false
	  }
	]
  }"""

  commonScripts.uploadToArtifactory(artifactoryFilesToUpload)
  echo "Upload of PerfMonConsole to Artifactory complete."
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;