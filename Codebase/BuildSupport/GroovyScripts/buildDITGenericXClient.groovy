def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\DataImport\\ditGenericXClient\\DITGenericImport.sln"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build DIT Generic XClient..."

  commonScripts.restoreNuget(solution)

  echo "Building DIT Generic XClient..."

  commonScripts.buildMSProject(solution, binariesFolder)

  echo "Running unit tests for DIT Generic XClient..."

  String[] testFiles = ["${binariesFolder}\\UnitTestGenericImport.dll"]
  runMSTest('2017', testFiles)

  commonScripts.poshPublishToArtifactory(jobItems, '\\Binaries\\', 'Codebase\\BuildSupport\\PublishMap.DITGenericImport.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;