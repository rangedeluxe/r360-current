def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\TestApps\\DitDataGenerator\\DitDataGenerator.sln"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build Image Generator test application..."
  powershell "./Codebase/BuildSupport/StampVersion/StampVersion.exe ${jobItems.BuildNumber} ${jobItems.Workspace}\\Codebase\\TestApps\\DitDataGenerator"
  commonScripts.restoreNuget(solution)

  echo "Building Image Generator test application..."

  commonScripts.buildMSProject(solution, binariesFolder)

  commonScripts.poshPublishToArtifactory(jobItems, '\\Binaries\\', 'Codebase\\BuildSupport\\PublishMap.DitDataGenerator.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;