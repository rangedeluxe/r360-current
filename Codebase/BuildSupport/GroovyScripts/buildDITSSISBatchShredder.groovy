def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\DataImport\\DataImportSolution\\DataImportSolution.sln"
  def project = ".\\Codebase\\DataImport\\BatchShredder\\BatchShredder.csproj"
  def testProject = ".\\Codebase\\DataImport\\BatchShredderTests\\BatchShredderTests.csproj"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build DIT Batch Shredder for SSIS..."

  commonScripts.restoreNuget(solution)

  echo "Building DIT Batch Shredder for SSIS..."

  commonScripts.buildMSProject(project, binariesFolder)
  commonScripts.buildMSProject(testProject, binariesFolder)

  String[] testFiles = ["${binariesFolder}\\BatchShredderTests.dll"]
  runMSTest('2017', testFiles)

  commonScripts.poshPublishToArtifactory(jobItems, '\\Binaries\\', 'Codebase\\BuildSupport\\PublishMap.BatchShredder.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;