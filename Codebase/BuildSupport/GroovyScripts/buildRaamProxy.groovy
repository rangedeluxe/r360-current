def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildRaamProxy(jobItems)

}

def buildRaamProxy(jobItems) {

  def binariesFolder = "${jobItems.Workspace}\\binaries\\RaamProxy"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Hub Views..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\UI\\UISolution\\UISolution.sln")

  echo "Building Hub solution..."
  runMSBuild('2017','.\\Codebase\\UI\\UISolution\\UISolution.sln',"${binariesFolder}")

  //gatherHubViewArtifacts(binariesFolder, deploymentFolder)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\RaamProxy\\', 'Codebase\\BuildSupport\\PublishMap.RaamProxy.xml')

}

def gatherHubViewArtifacts(binariesFolder, deploymentFolder) {
  echo "Creating ${deploymentFolder} HubView deployment artifacts in workspace..."

  dir("${binariesFolder}/_PublishedWebsites/RaamProxy/bin") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\bin"
  }

  dir("${binariesFolder}/_PublishedWebsites/RaamProxy") {
    bat "copy Global.asax ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\Global.asax"
    bat "copy Error.aspx ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\Error.aspx"
    bat "copy Error.html ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\Error.html"
    bat "copy web.config ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\web.config.oem"
    bat "copy web.config.template ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\web.config.template"
    bat "copy identity.web.config.template ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\identity.web.config.template"
    bat "copy identityservices.web.config.template ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\identityservices.web.config.template"
    bat "copy webmachinekey.config.template ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\webmachinekey.config.template"
  }

  dir("${binariesFolder}/_PublishedWebsites/RaamProxy/Views") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\Views"
  }

  dir("${binariesFolder}/_PublishedWebsites/RaamProxy/Assets") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\RecHubRaamProxy\\Assets"
  }
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubRaamProxy"
	def artifactoryFilesToUpload = """{
          "files": [
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/RaamProxy/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubRaamProxy/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/RaamProxy/bin/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubRaamProxy/bin/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/RaamProxy/Views/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubRaamProxy/Views/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/RaamProxy/Assets/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubRaamProxy/Assets/",
              "recursive": false
            }
          ]
        }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;