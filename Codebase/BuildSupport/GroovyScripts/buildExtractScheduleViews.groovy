def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildExtractSchedulerViews(jobItems)

}

def buildExtractSchedulerViews(jobItems) {

  def binariesFolder = "${jobItems.Workspace}\\binaries\\ExtractSchedulerViews"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build ExtractScheduler Views..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\UI\\UISolution\\UISolution.sln")

  echo "Building ExtractScheduler solution..."
  runMSBuild('2017','.\\Codebase\\UI\\UISolution\\UISolution.sln',"${binariesFolder}")

  //gatherExtractSchedulerViewArtifacts(binariesFolder, deploymentFolder)

   getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\ExtractSchedulerViews\\', 'Codebase\\BuildSupport\\PublishMap.ExtractSchedulerViews.xml')

}

def gatherExtractSchedulerViewArtifacts(binariesFolder, deploymentFolder) {
  echo "Creating ${deploymentFolder} ExtractSchedulerView deployment artifacts in workspace..."

  dir("${binariesFolder}/_PublishedWebsites/ExtractSchedulerViews/bin") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\bin"
  }

  dir("${binariesFolder}/_PublishedWebsites/ExtractSchedulerViews") {
    bat "copy Global.asax ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\Global.asax"
    bat "copy Error.aspx ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\Error.aspx"
    bat "copy Error.html ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\Error.html"
    bat "copy web.config ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\web.config.oem"
    bat "copy web.config.template ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\web.config.template"
    bat "copy identity.web.config.template ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\identity.web.config.template"
    bat "copy identityservices.web.config.template ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\identityservices.web.config.template"
    bat "copy webmachinekey.config.template ${deploymentFolder}\\wwwroot\\ExtractSchedulerViews\\webmachinekey.config.template"
  }

  dir("${binariesFolder}/_PublishedWebsites/ExtractSchedulerViews/Views") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\ExtractSchedulerviews\\Views"
  }

  dir("${binariesFolder}/_PublishedWebsites/ExtractSchedulerViews/Assets") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\ExtractSchedulerviews\\Assets"
  }
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews"
	def artifactoryFilesToUpload = """{
          "files": [
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews/bin/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews/bin/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews/Views/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews/Views/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews/Assets/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/ExtractSchedulerViews/Assets/",
              "recursive": false
            }
          ]
        }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;