def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Configuration files..."

  //configuration build is just a move from workspace to artifactory
  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\ini\\', 'Codebase\\BuildSupport\\PublishMap.R360ConfigurationFiles.xml')
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;