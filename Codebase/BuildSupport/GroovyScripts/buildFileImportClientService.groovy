def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildFileImportClientService(jobItems)
}

def buildFileImportClientService(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\FileImportClientService"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  def commonScript = getCommonScripts(jobItems.Workspace)
  commonScript.cleanupFolder(binariesFolder)
  commonScript.cleanupFolder(deploymentFolder)

  echo "Starting to build FileImportClientLib Solution..."

  commonScript.restoreNuget(".\\Codebase\\FileImport\\FileImportSolution\\FileImportSolution.sln")

  echo "Building FileImportClientLib solution..."
  runMSBuild('2017','.\\Codebase\\FileImport\\FileImportSolution\\FileImportSolution.sln',"${binariesFolder}")

  commonScript.poshPublishToArtifactory(jobItems,'\\Binaries\\FileImportClientService\\', 'Codebase\\BuildSupport\\PublishMap.FileImportClientSvc.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;