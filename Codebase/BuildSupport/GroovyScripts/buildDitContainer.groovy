import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

def build(jobItems) {
  // First step - we build all the stuff we need to.
  //  We're building into '../Deploy/res' because this wil automatically include
  //  all of the files into the deploy.exe.
  dir('Codebase/DitContainer/ACHService/ACHService') {
    bat "dotnet build -c release"
    bat "dotnet publish ACHService.csproj -c Release -r win-x64 -o ..\\..\\Deploy\\res\\ACHService --self-contained"
  }

  runTests(jobItems.Workspace)

  // Update the package.json file with the current version number
  //updateAppVersion(jobItems)
  // Copy in our dependencies
  dir('Codebase/DitContainer') {
    bat "mkdir Deploy\\res\\Dependencies"
    bat "xcopy /Y Dependencies Deploy\\res\\Dependencies"
  }

  // now we build everything into a deployment exe.
  dir('Codebase/DitContainer/Deploy') {
    bat "npm i"
    bat "npm run build"
  }

  powershell "Rename-Item 'Codebase\\DitContainer\\Deploy\\dist\\InstallDitContainer 1.0.0.exe' 'InstallDitContainer.exe'"

  dir('Codebase/DitContainer/Deploy/dist') {
    def artifactoryFilesToUpload = """{
        "files": [
              {
                  "pattern": "InstallDitContainer.exe",
                  "target": "tmsa.r360/${jobItems.BranchToBuild}/DitContainer/${jobItems.BranchPath}/",
                  "recursive": false
              }
            ]
            
        }"""
    getCommonScripts(jobItems.Workspace).uploadToArtifactory(artifactoryFilesToUpload)
    echo "Upload of DitContainer to Artifactory complete."
  }
}

def runTests(workspace) {
  
    dir('Codebase/DitContainer/ACHService/ACHService.Tests') {
      bat "dotnet build ACHService.Tests.csproj -c release"
    }
    script {
      String[] testFiles = ["${jobItems.Workspace}\\Codebase\\DitContainer\\ACHService\\ACHService.Tests\\bin\\release\\netcoreapp2.1\\ACHService.Tests.dll"]
      runMSTest('2017', testFiles)
    }
    step([$class: 'MSTestPublisher', testResultsFile:"**/*.trx", failOnError: true, keepLongStdio: true])
}

def getCommonScripts(workspace) {
  def commonScripts = load "${jobItems.Workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

//readJSON, writeJSON would work a whole lot better, but they are not enabled
def updateAppVersion(jobItems) {
  echo "Updating ${jobItems.Workspace}\\Codebase\\DitContainer\\Deploy\\package.json"
  def jsonData = readFile(file:"${jobItems.Workspace}\\Codebase\\DitContainer\\Deploy\\package.json")
  def jsonParser = new groovy.json.JsonSlurperClassic()
  def packageSettings = jsonParser.parseText(jsonData)
  packageSettings.version = jobItems.VersionToBuild
  echo "after change packageSettings ${packageSettings}"
  def jsonPackageSetting = JsonOutput.toJson(packageSettings)
  echo "jsonPackageSetting ${jsonPackageSetting}"
  def jsonText = JsonOutput.prettyPrint(jsonPackageSetting)
  echo "jsonText ${jsonText}"
	echo "Write ${jobItems.Workspace}\\Codebase\\DitContainer\\Deploy\\package.json"
  writeFile(file:"${jobItems.Workspace}\\Codebase\\DitContainer\\Deploy\\package.json", text: jsonText)
  echo "writefile complete, leaving updateAppVersion"
}


return this;