def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\ExtractProcessorService"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Extract Processor Service..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\DataOutput\\ExtractProcessSvc\\ExtractProcessSvc.sln")

  echo "Building solution..."
  
  runMSBuild('2017','.\\Codebase\\DataOutput\\ExtractProcessSvc\\ExtractProcessSvc.sln',"${binariesFolder}")

  String[] testFiles = ["${binariesFolder}\\UnitTestExtractprocessSvc.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\ExtractProcessorService\\', 'Codebase\\BuildSupport\\PublishMap.ExtractProcessorService.xml')
  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;