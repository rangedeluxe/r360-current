def runMSTest(testFileList) {
	def testFiles = ""
	for (String testFile : testFileList) {
		testFiles += "\"" + testFile + "\" ";
	}
	def executeTest = "\"C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Enterprise\\Common7\\IDE\\CommonExtensions\\Microsoft\\TestWindow\\vstest.console.exe\" " + testFiles
	bat executeTest
}
return this;