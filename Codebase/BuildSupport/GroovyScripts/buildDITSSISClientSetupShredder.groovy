def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\DataImport\\DataImportSolution\\DataImportSolution.sln"
  def project = ".\\Codebase\\DataImport\\ClientSetupShredder\\ClientSetupShredder.csproj"
  def testProject = ".\\Codebase\\DataImport\\ClientSetupShredderTests\\ClientSetupShredderTests.csproj"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build DIT Client Setup Shredder for SSIS..."

  commonScripts.restoreNuget(solution)

  echo "Building DIT Client Setup Shredder for SSIS..."

  commonScripts.buildMSProject(project, binariesFolder)
  commonScripts.buildMSProject(testProject, binariesFolder)

  String[] testFiles = ["${binariesFolder}\\ClientSetupShredderTests.dll"]
  runMSTest('2017', testFiles)

  commonScripts.poshPublishToArtifactory(jobItems, '\\Binaries\\', 'Codebase\\BuildSupport\\PublishMap.ClientSetupShredder.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;