def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildExternalLogonService(jobItems)
}

def buildExternalLogonService(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\ExternalLogonService"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build ExternalLogonService Solution..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\ExternalLogon\\ExternalLogin.Solution\\ExternalLogon.sln")

  echo "Building ExternalLogonService solution..."
  runMSBuild('2017','.\\Codebase\\ExternalLogon\\ExternalLogin.Solution\\ExternalLogon.sln',"${binariesFolder}")

  echo "Running ExternalLogonService unit tests..."
  String[] testFiles = ["${binariesFolder}\\WFS.RecHub.ExternalLogin.Core.Test.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\ExternalLogonService\\', 'Codebase\\BuildSupport\\PublishMap.ExternalLogon.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;