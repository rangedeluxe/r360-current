import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic

def build(jobItems) {
  // First step - we build all the stuff we need to.
  //  We're building into '../Deploy/res' because this wil automatically include
  //  all of the files into the deploy.exe.
  dir('Codebase/TestApps/ACHDataGenerator/ACHDataGenerator') {
    bat "dotnet build -c release"
    bat "dotnet publish ACHDataGenerator.csproj -c release -r win-x64 -o .\\Deploy --self-contained"
  }

  runTests(jobItems.Workspace)

  dir('Codebase/TestApps/ACHDataGenerator/ACHDataGenerator/Deploy') {
    def artifactoryFilesToUpload = """{
        "files": [
              {
                  "pattern": "*.exe",
                  "target": "tmsa.r360/${jobItems.BranchToBuild}/Utilities/ACHDataGenerator/",
                  "recursive": false
              },
              {
                  "pattern": "*.dll",
                  "target": "tmsa.r360/${jobItems.BranchToBuild}/Utilities/ACHDataGenerator/",
                  "recursive": false
              },
              {
                  "pattern": "*.json",
                  "target": "tmsa.r360/${jobItems.BranchToBuild}/Utilities/ACHDataGenerator/",
                  "recursive": false
              },
              {
                  "pattern": "*.config",
                  "target": "tmsa.r360/${jobItems.BranchToBuild}/Utilities/ACHDataGenerator/",
                  "recursive": false
              }		  
            ]
            
        }"""
    getCommonScripts(jobItems.Workspace).uploadToArtifactory(artifactoryFilesToUpload)
    echo "Upload of ACH Data Generator to Artifactory complete."
  }
}

def runTests(workspace) {
  
    dir('Codebase/TestApps/ACHDataGenerator/ACHDataGenerator.Test') {
      bat "dotnet build ACHDataGenerator.Test.csproj -c release"
    }
    script {
      String[] testFiles = ["${jobItems.Workspace}\\Codebase\\TestApps\\ACHDataGenerator\\ACHDataGenerator.Test\\bin\\Release\\netcoreapp2.1\\ACHDataGenerator.Test.dll"]
      runMSTest('2017', testFiles)
    }
    step([$class: 'MSTestPublisher', testResultsFile:"**/*.trx", failOnError: true, keepLongStdio: true])
}

def getCommonScripts(workspace) {
  def commonScripts = load "${jobItems.Workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

//readJSON, writeJSON would work a whole lot better, but they are not enabled
def updateAppVersion(jobItems) {
  echo "Updating ${jobItems.Workspace}\\Codebase\\DitContainer\\Deploy\\package.json"
  def jsonData = readFile(file:"${jobItems.Workspace}\\Codebase\\DitContainer\\Deploy\\package.json")
  def jsonParser = new groovy.json.JsonSlurperClassic()
  def packageSettings = jsonParser.parseText(jsonData)
  packageSettings.version = jobItems.VersionToBuild
  echo "after change packageSettings ${packageSettings}"
  def jsonPackageSetting = JsonOutput.toJson(packageSettings)
  echo "jsonPackageSetting ${jsonPackageSetting}"
  def jsonText = JsonOutput.prettyPrint(jsonPackageSetting)
  echo "jsonText ${jsonText}"
	echo "Write ${jobItems.Workspace}\\Codebase\\DitContainer\\Deploy\\package.json"
  writeFile(file:"${jobItems.Workspace}\\Codebase\\DitContainer\\Deploy\\package.json", text: jsonText)
  echo "writefile complete, leaving updateAppVersion"
}


return this;