def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildDataImportSolution(jobItems)
}

def buildDataImportSolution(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\DataImportSolution"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build DataImportToolkit Solution..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\DataImport\\DataImportSolution\\DataImportSolution.sln")

  echo "Building DataImportToolkit solution..."
  runMSBuild('2017','.\\Codebase\\DataImport\\DataImportSolution\\DataImportSolution.sln',"${binariesFolder}")

  echo "Running DataImportToolkit unit tests..."
  String[] testFiles = ["${binariesFolder}\\ClientUnitTests.dll","${binariesFolder}\\DataImportServicesDal_IntegrationTests.dll","${binariesFolder}\\BatchShredderTests.dll","${binariesFolder}\\DataImportClientUnitTests.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\DataImportSolution\\', 'Codebase\\BuildSupport\\PublishMap.DataImportSolution.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;