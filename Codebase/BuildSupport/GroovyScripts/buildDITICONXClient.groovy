def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildDitICONXClient(jobItems)
}

def buildDitICONXClient(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\DitICONXClient"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build DIT ICON XClient Solution..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\DataImport\\ditICONXClient\\ditICONXClient.sln")

  echo "Building DitACHImport solution..."
  //Codebase\DataImport\ditICONXClient\ditICONXClient.sln -Verbosity Detailed
  runMSBuild('2017','.\\Codebase\\DataImport\\ditICONXClient\\ditICONXClient.sln',"${binariesFolder}")

  echo "Running DIT ICON XClient unit tests..."
  String[] testFiles = ["${binariesFolder}\\DITICONClientTest.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\DitICONXClient\\', 'Codebase\\BuildSupport\\PublishMap.ditICONXClient.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;