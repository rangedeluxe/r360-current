def getReleaseInfo(releaseFilePath, branch) {
	def releaseConfigFile = releaseFilePath
	def jsonData = readFile(file:releaseConfigFile)
	def jsonParser = new groovy.json.JsonSlurperClassic()
	def releaseConfig = jsonParser.parseText(jsonData)
	def branchConfig = releaseConfig.Branches.find{it.Branch.equalsIgnoreCase(branch)}
	if (branchConfig != null){
		println "Found branch ${branch}.  Returning release information..."
		return [ProductName:releaseConfig.ProductName, Major:branchConfig.Major, Minor:branchConfig.Minor, HotFix:branchConfig.HotFix, Patch:branchConfig.Patch]
	}
	
	println "Branch ${branch} not found in release.json file.  Returning null..."
	return null
}

def getVersionInfo(versionFilePath) {
	def versionConfigFile = versionFilePath
	def jsonData = readFile(file:versionConfigFile)
	def jsonParser = new groovy.json.JsonSlurperClassic()
	def versionConfig = jsonParser.parseText(jsonData)

	return [ProductName:versionConfig.ProductName, Major:versionConfig.Major, Minor:versionConfig.Minor, HotFix:versionConfig.HotFix, Patch:versionConfig.Patch]

}
return this;