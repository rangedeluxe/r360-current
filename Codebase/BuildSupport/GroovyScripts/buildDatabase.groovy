def build(jobItems) {
  echo "Starting to build Database..."
  echo "workspace is ${workspace}"

  def binariesFolder = "${jobItems.Workspace}\\binaries\\DatabaseBuild"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def stagingFolder = "${jobItems.Workspace}\\Objects"
  def buildApp = "${jobItems.Workspace}\\binaries\\DatabaseBuild\\DatabaseBuild.exe"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Building DatabaseBuild solution..."
  runMSBuild('2017','.\\Codebase\\DatabaseBuild\\DatabaseBuild.sln',"${binariesFolder}")

  echo "Building R360 objects"
  powershell """${buildApp} `
      -WorkspaceFolder '${jobItems.Workspace}\\Database' `
      -VersionNumber '${jobItems.VersionToBuild}' `
      -StagingFolder '${stagingFolder}\\R360'
    """
  if (fileExists("${stagingFolder}\\R360")) {
    echo "Pushing R360 objects"
    getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,"Objects\\R360", 'Codebase\\BuildSupport\\PublishMap.Database.xml')
  }

  echo "Building R360 RAAM objects"
  powershell """${buildApp} `
      -WorkspaceFolder '${jobItems.Workspace}\\Database\\RAAM' `
      -VersionNumber '${jobItems.VersionToBuild}' `
      -StagingFolder '${stagingFolder}\\RAAM'
    """
  if (fileExists("${stagingFolder}\\RAAM")) {
    echo "Pushing R360 RAAM objects"
    getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,"Objects\\RAAM", 'Codebase\\BuildSupport\\PublishMap.Database.xml')
  }
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}


return this;