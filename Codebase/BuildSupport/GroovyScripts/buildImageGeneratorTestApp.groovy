def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\TestApps\\ImageGenerator\\ImageGenerator.sln"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build Image Generator test application..."
  commonScripts.restoreNuget("${solution}")

  echo "Building Image Generator test application..."
  
  commonScripts.buildMSProject(solution, binariesFolder)

  String[] testFiles = ["${binariesFolder}\\ImageGenerators.UnitTests.dll","${binariesFolder}\\ImageGenerators.IntegrationTests.dll"]
  runMSTest('2017', testFiles)

  commonScripts.poshPublishToArtifactory(jobItems, '\\Binaries\\', 'Codebase\\BuildSupport\\PublishMap.ImageGenerator.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;