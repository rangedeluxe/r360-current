def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\PostDepositBusinessRules_SSIS"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Database\\SSIS\\DataImportIntegrationServices\\DataImportIntegrationServices.sln"
  def project = ".\\Database\\SSIS\\DataImportIntegrationServices\\PostDepositBusinessRules.Ssis\\PostDepositBusinessRules.Ssis.csproj"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Post-Deposit Business Rules for SSIS..."

  getCommonScripts(jobItems.Workspace).restoreNuget(solution)

  echo "Building Business Rules SSIS Project..."

  getCommonScripts(jobItems.Workspace).buildMSProject(project, binariesFolder)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems, '\\Binaries\\PostDepositBusinessRules_SSIS\\', 'Codebase\\BuildSupport\\PublishMap.PostDepositBusinessRules.Ssis.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;