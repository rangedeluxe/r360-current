def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  buildOLDecisioningServices(jobItems)
}

def buildOLDecisioningServices(jobItems) {

  def binariesFolder = "${jobItems.Workspace}\\binaries\\OLDecisioningServices"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\OLDecisioning\\OLDecisioningServices\\OLDecisioningServices.sln")

  echo "Building OLDecisionServices solution..."
  runMSBuild('2017','.\\Codebase\\OLDecisioning\\OLDecisioningServices\\OLDecisioningServices.sln',"${binariesFolder}")

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\OLDecisioningServices\\', 'Codebase\\BuildSupport\\PublishMap.OLDecisioningServices.xml')
  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;