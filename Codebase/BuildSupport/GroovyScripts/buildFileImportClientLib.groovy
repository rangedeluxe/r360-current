def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildFileImportClientLib(jobItems)
}

def buildFileImportClientLib(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\FileImportClientLib"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  def commonScript = getCommonScripts(jobItems.Workspace)
  commonScript.cleanupFolder(binariesFolder)
  commonScript.cleanupFolder(deploymentFolder)

  echo "Starting to build FileImportClientLib Solution..."

  echo "Building FileImportClientLib solution..."
  runMSBuild('2017','.\\Codebase\\FileImport\\FileImportClientLib\\FileImportClientLib.sln',"${binariesFolder}")

  commonScript.poshPublishToArtifactory(jobItems,'\\Binaries\\FileImportClient\\', 'Codebase\\BuildSupport\\PublishMap.FileImportClientLib.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;