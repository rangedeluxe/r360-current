def createReleasePackage(jobItems) {
  echo "Starting to create release package..."
  
  powershell """D:\\PowerShellScripts\\packageRelease.ps1 `
     -versions '${jobItems.BranchPath}' `
     -apikey '${jobItems.ApiKey}' `
     -Branch '${jobItems.BranchToBuild}'
  """

  publishToArtifactory(jobItems)
}

def packageReleaseNotes(jobItems)
{
  echo "Go to artifactory packages folder, find this packaged release version, and download it"
  addReleaseNotes(jobItems)
}

def addReleaseNotes(jobItems) {
  def commonScriptRoutines = getCommonScripts(jobItems.Workspace)

  def downloadFolder = "${jobItems.BranchToBuild}/Packages/"
  def zipFileName = "Release-${jobItems.BranchPath}.zip"
  def zipFile ="${downloadFolder}${zipFileName}"
  def artifactoryZipFile = "tmsa.r360/${zipFile}"
  def downloadPattern = artifactoryZipFile
  def downloadTarget = "${jobItems.Workspace}"
  
  commonScriptRoutines.downloadFromArtifactory(downloadPattern, downloadTarget)

  echo "Unzip the release package and copy in the release notes folder from bitbucket"
 
  def releaseNotesFolder = "${jobItems.Workspace}\\ReleaseNotes\\${jobItems.BranchPath}"
  def newZipFile = "R360_${jobItems.VersionToBuild}.zip"

  echo "Release notes folder is ${releaseNotesFolder}"

  powershell """
    Compress-Archive -Path ${releaseNotesFolder}\\* -Update -DestinationPath ${zipFile} 
    Rename-Item -Path ${zipFile} -NewName ${newZipFile}
  """

  def packagesFolder = "${jobItems.BranchToBuild}/Packages/WithReleaseNotes"
  def uploadFileSpec = """{
        "files": [
          {
            "pattern": "${jobItems.BranchToBuild}/Packages/*.zip",
            "target": "tmsa.r360/${packagesFolder}/",
            "recursive": false
          }
        ]
      }"""

  //def artifactoryUriToDelete = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}"
  def artifactoryUriToDelete = "http://artifactory.deluxe.com/tmsa.r360/${packagesFolder}"
  def artifactoryUriToUpload = "http://artifactory.deluxe.com/tmsa.r360/${packagesFolder}"

  commonScriptRoutines.deleteFromArtifactory(artifactoryUriToDelete)
  commonScriptRoutines.uploadToArtifactory(uploadFileSpec)

  echo "Send fancy message"

  def artifactoryLink = "(<a href=${artifactoryUriToUpload}>View Release Package</a>)"
  def message = "R360 package with release notes is 'Ready To GO'!  The package can be found here ${artifactoryLink}"
  commonScriptRoutines.notify('Packaged and Ready', message )
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/Packages/"
	def artifactoryFilesToUpload = """{
      "files": [
            {
                "pattern": ".\\*.zip",
                "target": "tmsa.r360/${jobItems.BranchToBuild}/Packages/"
            }
          ]
           
      }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}
return this;