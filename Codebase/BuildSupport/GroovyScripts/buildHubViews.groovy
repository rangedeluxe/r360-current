def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildHubViews(jobItems)

}

def buildHubViews(jobItems) {

  def binariesFolder = "${jobItems.Workspace}\\binaries\\HubViews"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Hub Views..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\UI\\UISolution\\UISolution.sln")

  echo "Building Hub Views..."
  runMSBuild('2017','.\\Codebase\\UI\\UISolution\\UISolution.sln',"${binariesFolder}")

  echo "Run Hub Views unit tests..."
  String[] testFiles = ["${binariesFolder}\\HubViews.UnitTests.dll"]
  runMSTest('2017', testFiles)
  
  //gatherHubViewArtifacts(binariesFolder, deploymentFolder)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\HubViews\\', 'Codebase\\BuildSupport\\PublishMap.HubViews.xml')

}

def gatherHubViewArtifacts(binariesFolder, deploymentFolder) {
  echo "Creating ${deploymentFolder} HubView deployment artifacts in workspace..."

  dir("${binariesFolder}/_PublishedWebsites/HubViews/bin") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\wwwroot\\RecHubViews\\bin"
  }

  dir("${binariesFolder}/_PublishedWebsites/HubViews") {
    bat "copy Global.asax ${deploymentFolder}\\wwwroot\\RecHubViews\\Global.asax"
    bat "copy Error.aspx ${deploymentFolder}\\wwwroot\\RecHubViews\\Error.aspx"
    bat "copy Error.html ${deploymentFolder}\\wwwroot\\RecHubViews\\Error.html"
    bat "copy web.config ${deploymentFolder}\\wwwroot\\RecHubViews\\web.config.oem"
    bat "copy web.config.template ${deploymentFolder}\\wwwroot\\RecHubViews\\web.config.template"
    bat "copy identity.web.config.template ${deploymentFolder}\\wwwroot\\RecHubViews\\identity.web.config.template"
    bat "copy identityservices.web.config.template ${deploymentFolder}\\wwwroot\\RecHubViews\\identityservices.web.config.template"
    bat "copy webmachinekey.config.template ${deploymentFolder}\\wwwroot\\RecHubViews\\webmachinekey.config.template"
  }

  dir("${binariesFolder}/_PublishedWebsites/HubViews/Views") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\Hubviews\\Views"
  }

  dir("${binariesFolder}/_PublishedWebsites/HubViews/Assets") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\Hubviews\\Assets"
  }
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubViews"
	def artifactoryFilesToUpload = """{
          "files": [
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/RecHubViews/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubViews/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/RecHubViews/bin/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubViews/bin/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/RecHubViews/Views/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubViews/Views/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/RecHubViews/Assets/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubViews/Assets/",
              "recursive": false
            }
          ]
        }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;