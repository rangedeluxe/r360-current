def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildDitACHImport(jobItems)
}

def buildDitACHImport(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\DitACHImport"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build DitACHImport Solution..."

  echo "Building DitACHImport solution..."
  runMSBuild('2017','.\\Codebase\\DataImport\\ditACHXClient\\DITACHImport.sln',"${binariesFolder}")

  echo "Running DitACHImport unit tests..."
  String[] testFiles = ["${binariesFolder}\\ACHUnitTestProject.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\DitACHImport\\', 'Codebase\\BuildSupport\\PublishMap.DITACHImport.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;