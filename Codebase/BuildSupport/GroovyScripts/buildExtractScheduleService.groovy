def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  def binariesFolder = "${jobItems.Workspace}\\binaries\\ExtractScheduleService"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Extract Schedule Service..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\R360Services\\ExctractSchedule\\ExtractScheduleSolution\\ExtractScheduleSolution.sln")

  echo "Building Extract Schedule Service solution..."
  runMSBuild('2017','.\\Codebase\\R360Services\\ExctractSchedule\\ExtractScheduleSolution\\ExtractScheduleSolution.sln"',"${binariesFolder}")

  String[] testFiles = ["${binariesFolder}\\ExtractScheduleTest.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\ExtractScheduleService\\', 'Codebase\\BuildSupport\\PublishMap.ExtractScheduleSolution.xml')

}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;