def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildXClient(jobItems)
}

def buildXClient(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\FITItemProcessingXClient"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)
  
  echo "Building solution..."
  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\FileImport\\FileImportSolution\\FileImportSolution.sln")

  
  runMSBuild('2017','.\\Codebase\\FileImport\\FileImportSolution\\FileImportSolution.sln',"${binariesFolder}")

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\FITItemProcessingXClient\\', 'Codebase\\BuildSupport\\PublishMap.fitItemProcessingXClient.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;