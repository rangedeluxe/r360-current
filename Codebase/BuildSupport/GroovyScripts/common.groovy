def getJobItems() {
  return [
      BranchToBuild : "",
      BranchPath : "",
      VersionToBuild : "",
      Workspace : "",
      PublishToArtifactory : false,
      BuildNumber : "",
      ApiKey : ""
  ]

}

def getDeployItems(jobItems) {
  
  return [
      R360Version : jobItems.BranchPath,
      FrameworkVersion : "",
      RAAMVErsion : "",
      R360Branch : jobItems.BranchToBuild,
      FrameworkBranch : "",
      RAAMBranch : "",
      DeployToEnvironment : "",
      ApiKey : jobItems.ApiKey
  ]

}

def setJobItems(workspace, params, customBuildNumber, apiKey) {
  //commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"

  echo "Setting up the job items object"

  jobItems = getJobItems()
  
  workspace = "${workspace}"
  def scriptPath = "${workspace}\\Codebase\\BuildSupport\\GroovyScripts"
  def defaultVersion = "${customBuildNumber}"
  apiKey = "${apiKey}"
  echo "defaultVersion is ${defaultVersion}"
  echo "api key is ${apiKey}"
  def startupInfo = jobStartup(workspace, scriptPath, params, defaultVersion)
  
  branchPath = startupInfo.BranchPath
  branchToBuild = startupInfo.BranchToBuild 
  versionToBuild = startupInfo.VersionToBuild

  jobItems.Workspace = workspace
  jobItems.BranchToBuild = startupInfo.BranchToBuild
  jobItems.BranchPath = startupInfo.BranchPath
  jobItems.VersionToBuild = startupInfo.VersionToBuild
  jobItems.PublishToArtifactory = params.Publish
  jobItems.BuildNumber = defaultVersion
  jobItems.ApiKey = apiKey

  echo "jobItems.ApiKey = ${jobItems.ApiKey}"
  echo "Returning job items object"
  return jobItems;

}

def jobStartup(workspace, scriptPath, params, defaultVersion) {

	def productName = "RecHub"
	def commonScripts = load "${scriptPath}\\getReleaseInfo.groovy"
	def branchToBuild = ""
  def versionToBuild = ""
  def branchPath = ""

	if (params?.BranchToBuild != null) 
		branchToBuild = "${params?.BranchToBuild.toLowerCase()}"

	//releaseInfo = commonScripts.getReleaseInfo("${workspace}\\Codebase\\BuildSupport\\Release.json", branchToBuild)
  releaseInfo = commonScripts.getVersionInfo("${workspace}\\Codebase\\BuildSupport\\Version.json")

	if (releaseInfo != null) {
		branchPath = "${releaseInfo.ProductName}${releaseInfo.Major}.${releaseInfo.Minor}.${releaseInfo.HotFix}"
		versionToBuild = "${releaseInfo.Major}.${releaseInfo.Minor}.${releaseInfo.HotFix}"
		
		if (releaseInfo.Patch != "00") {
			branchPath = branchPath + "." + releaseInfo.Patch
			versionToBuild = versionToBuild + "." + releaseInfo.Patch
		}
	}
	else {
		branchPath = "${productName}${branchToBuild}" 
		versionToBuild = "${defaultVersion}"
		echo "No release config info found, so defaulting to ${versionToBuild}"
	}

	//if the version parameter is passed in, then we'll override the version from the release.config file
	if (params?.Version != null && params?.Version != '') {
		echo "Version is being overriden with ${params?.Version}"
		versionToBuild = "${params?.Version}"
		branchPath = "${productName}${versionToBuild}"
	}

	echo "Branch path - ${branchPath}" 
	echo "Running against branch ${branchToBuild}" 
	echo "Version To Build - ${versionToBuild}"
    return [
        BranchPath : branchPath,
        BranchToBuild : branchToBuild,
        VersionToBuild : versionToBuild
    ]
}

def cleanupFolder(folder) {
  if (fileExists("${folder}")) {
      echo "Folder ${folder} exists, deleting it..."
      bat "rmdir /S /Q ${folder}"
  } else {
      echo "Folder ${folder} doesn't exist"
  }
}

def publishToArtifactory(workspace, location, filesToUpload, deploy = true) {
  if (deploy) {
    echo "Running publish to artificatory step..."

    deleteFromArtifactory(location)
    uploadToArtifactory(filesToUpload)
    
  }
}

def deleteFromArtifactory(deleteUri) {
  echo "Deleting ${deleteUri} from  artifactory..."
  
  withEnv(["LOCATION=${deleteUri}"]) {
    powershell '''
      $location = "$env:LOCATION"
      $token = "$ENV:JenkinsDeploySupportArtifactoryAPIToken"
      Write-Host "location = $location"
      $authheader = @{"X-JFrog-Art-Api"=$token}
      $uri = new-object system.uri($location)
      Invoke-RestMethod -uri $uri -Headers $authheader -Method Delete
    '''
  }

  echo "Delete from artifactory finished..."

}

def uploadToArtifactory(uploadFileSpec) {
  def server = Artifactory.server 'Artifactory'
  server.upload spec: uploadFileSpec, failNoOp: true
}

def downloadFromArtifactory(downloadPattern, downloadTarget) {
  echo "Download pattern is ${downloadPattern}"
  echo "Download target is ${downloadTarget}"

  def server = Artifactory.server 'Artifactory'
  def downloadSpec = """{
      "files": [
            {
                "pattern": "${downloadPattern}",
                "target": "${downloadTarget}/"
            }
          ]
           
      }"""
  server.download spec: downloadSpec, failNoOp: true

}

def poshPublishToArtifactory(jobItems, binSource, publishMap) {

  if (jobItems.PublishToArtifactory) {

    echo "Publishing to artifactory..."
    //def publishScript = "D:\\PowerShellScripts\\GitPublishApp_Pipeline"
    def publishScript = "${jobItems.Workspace}\\Codebase\\BuildSupport\\PowershellScripts\\GitPublishApp_Pipeline "
    powershell """${publishScript} `
      -WorkspaceFolder '${jobItems.Workspace}' `
      -BuildNumber '${jobItems.BuildNumber}' `
      -BinSource '${binSource}' `
      -ConfigFile D:\\PowerShellScripts\\GitPublishApp_R360_Current.config `
      -Branch '${jobItems.BranchToBuild}' `
      -DeploymentMap ${publishMap} `
      -JFrogBranch '${jobItems.BranchToBuild}'
    """
  }
}

def restoreNuget(Solution) {
  echo "Restoring NuGet packages..."
  powershell "d:\\Tools\\nuget.exe restore '${Solution}' -Verbosity Detailed"
  /*powershell '''
      nuget locals all -list
      nuget locals all -clear
      nuget restore '${Solution}' -Source http://artifactory.deluxe.com/api/nuget/deluxe.nuget
  '''*/
}

def buildMSProject(projectFullPath, outputPath) {
  /*powershell '''
      & "C:/Program Files (x86)/Microsoft Visual Studio/2017/BuildTools/MSBuild/15.0/Bin/MSBuild.exe" $env:SOLUTION_FILE /p:Configuration=Release /p:PublishProfile=Release.pubxml /p:DeployOnBuild=true /verbosity:normal
    '''*/
  def executeBuild = "\"${tool '15.0 (VS2017)'}\\msbuild.exe\" "
  executeBuild = executeBuild + "/p:Configuration=Release \"/p:Platform=Any CPU\" /p:OutputPath=${outputPath} ${projectFullPath}"
  bat executeBuild
}

def notify(status, message) {
  office365ConnectorSend (
      color:  '00994c',
      webhookUrl: 'https://outlook.office.com/webhook/8cbc3577-61db-4289-9470-9feea2b241b6@1f7c1878-7408-4f86-b429-cf17f96a717c/JenkinsCI/f90f7936154a45ea9d47b8a507bf0963/774801a1-6bb0-4acc-bc02-54b2900dea6f',
      message: message,
      status: status
  )
}

def getActiveApplicationNames(modulesFileWithPath) {
	def jsonData = readFile(file:modulesFileWithPath)
	def jsonParser = new groovy.json.JsonSlurperClassic()
	def data = jsonParser.parseText(jsonData)
  def applications = data.Applications.findAll{ it.Active == true }
	if (applications != null) {
    applications.each { println it}
    println "We have ${applications.size()} active applications"
		return applications.Name.sort()
	}

	println "No application modules found"
	return []
}

def getApplicationInfo(applicationName, modulesFileWithPath) {
	def jsonData = readFile(file:modulesFileWithPath)
	def jsonParser = new groovy.json.JsonSlurperClassic()
	def data = jsonParser.parseText(jsonData)
	def application = data.Applications.find{it.Name.equalsIgnoreCase(applicationName)}
	if (application != null){
		println "Found application ${applicationName}.  Returning script information..."
		return application
	}
	
	println "Application ${applicationName} not found in modules.json file.  Returning null..."
	return null
}

return this;