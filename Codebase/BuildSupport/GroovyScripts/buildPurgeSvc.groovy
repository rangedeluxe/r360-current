def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\Purge\\PurgeSvc\\PurgeSvc.sln"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build PurgeSvc..."
  powershell "./Codebase/BuildSupport/StampVersion/StampVersion.exe ${jobItems.BuildNumber} ${jobItems.Workspace}\\Codebase\\Purge\\PurgeSvc"

  echo "Building PurgeSvc..."

  commonScripts.buildMSProject(solution, binariesFolder)

  commonScripts.poshPublishToArtifactory(jobItems, '\\Binaries\\', 'Codebase\\BuildSupport\\PublishMap.PurgeSvc.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;