def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\ICONServices"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build ICON Services..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\ICON\\ICONServices\\ICONServices.sln")

  echo "Building solution..."
  
  runMSBuild('2017','.\\Codebase\\ICON\\ICONServices\\ICONServices.sln',"${binariesFolder}")
  
  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\ICONServices\\', 'Codebase\\BuildSupport\\PublishMap.ICONServices.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;