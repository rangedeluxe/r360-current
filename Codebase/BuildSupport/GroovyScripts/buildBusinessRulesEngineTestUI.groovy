def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\R360Services\\Hub\\HubSolution\\HubSolution.sln"
  def project = ".\\Codebase\\R360Services\\PostDepositValidationTestUi\\PostDepositValidationTestUi.csproj"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build Business Rules Engine Test UI..."

  commonScripts.restoreNuget(solution)

  echo "Building Business Rules Engine..."

  commonScripts.buildMSProject(project, binariesFolder)

  commonScripts.poshPublishToArtifactory(jobItems, '\\Binaries\\', 'Codebase\\BuildSupport\\PublishMap.PostDepositValidationTestUi.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;