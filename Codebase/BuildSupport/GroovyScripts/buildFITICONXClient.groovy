def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildXClient(jobItems)
}

def buildXClient(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\FITICONXClient"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Building FIT solution..."
  runMSBuild('2017','.\\Codebase\\FileImport\\fitICONXClient\\fitICONXClient.sln',"${binariesFolder}")

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\FITICONXClient\\', 'Codebase\\BuildSupport\\PublishMap.fitICONXClient.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;