def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\CustomImage"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build CustomImage..."
  getCommonScripts(jobItems.Workspace).restoreNuget('.\\Codebase\\CustomImage\\CustomImageAsmx.sln')
  runMSBuild('2017','.\\Codebase\\CustomImage\\CustomImageAsmx.sln',"${binariesFolder}")

  //String[] testFiles = ["${binariesFolder}\\Test.dll"]
  //runMSTest('2017', testFiles)

  if (jobItems.PublishToArtifactory) {
    gatherArtifacts(binariesFolder, deploymentFolder)
    publishToArtifactory(jobItems)
  }
    
}

def gatherArtifacts(binariesFolder, deploymentFolder) {
  echo "Creating ${deploymentFolder} deployment artifacts in workspace..."

  dir("${binariesFolder}") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\bin2\\customimages"
    bat "copy CustomImageWSAsmx.dll.config ${deploymentFolder}\\bin2\\customimages\\CustomImageWSAsmx.dll.config.oem"
  }

  dir("${binariesFolder}/_PublishedWebsites/CustomImageRetrievalWebAsmx/bin") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\Utilities\\MockViewpointe\\bin"
  }

  dir("${binariesFolder}/_PublishedWebsites/CustomImageRetrievalWebAsmx") {
    bat "xcopy /y /i *.asmx ${deploymentFolder}\\Utilities\\MockViewpointe"
    bat "copy web.config ${deploymentFolder}\\Utilities\\MockViewpointe\\web.config.oem"
  }
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/FILE/${jobItems.BranchPath}/bin2/customimages"
  def artifactoryFilesToUpload = """{
        "files": [
          {
            "pattern": "${jobItems.BranchPath}/bin2/customimages/*.*",
            "target": "tmsa.r360/${jobItems.BranchToBuild}/FILE/${jobItems.BranchPath}/bin2/customimages/",
            "recursive": false
          },
          {
            "pattern": "${jobItems.BranchPath}/Utilities/MockViewpointe/(*)",
            "target": "tmsa.r360/${jobItems.BranchToBuild}/Utilities/MockViewpointe/{1}",
            "recursive": true,
            "flat": false
          }
        ]
      }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  

}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;