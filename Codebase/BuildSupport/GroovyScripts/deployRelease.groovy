def deployReleasePackage(deployItems) {
  echo "Starting to deploy release package..."
 
  dir('Support/DeployUtilities/Deployer') {
    bat "npm i"

    powershell """node index.js --r360version '${deployItems.R360Version}' `
     --frameworkversion '${deployItems.FrameworkVersion}' `
     --raamversion '${deployItems.RAAMVersion}' `
     --apikey '${deployItems.ApiKey}' `
     --tiers '${deployItems.DeployToEnvironment}' `
     --raambranch '${deployItems.RAAMBranch}' `
     --r360branch '${deployItems.R360Branch}' `
     --frameworkbranch '${deployItems.FrameworkBranch}'
  """
  }
 
}

return this;