def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildHubSolution(jobItems)
}

def buildHubSolution(jobItems) {
  def binariesFolder = "${jobItems.Workspace}\\binaries\\HubSolution"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Hub Solution..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\R360Services\\Hub\\HubSolution\\HubSolution.sln")

  echo "Building Hub solution..."
  runMSBuild('2017','.\\Codebase\\R360Services\\Hub\\HubSolution\\HubSolution.sln',"${binariesFolder}")

  String[] testFiles = ["${binariesFolder}\\R360Tests.dll"]
  runMSTest('2017', testFiles)
  //*** just going to use the publish map for now and come back to direct artifactory upload later ***
  //gatherRecHubServiceArtifacts(binariesFolder, deploymentFolder)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\HubSolution\\', 'Codebase\\BuildSupport\\PublishMap.HubSolution.xml')
  
}

def gatherRecHubServiceArtifacts(binariesFolder, deploymentFolder) {

  echo "Creating ${deploymentFolder} RecHubServices deployment artifacts in workspace..."

  dir("${binariesFolder}") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\wwwroot\\RecHubServices\\bin"
  }

  dir("${binariesFolder}/_PublishedWebsites/R360Services") {
    bat "xcopy /y /i *.template ${deploymentFolder}\\wwwroot\\RecHubServices"
    bat "copy web.config.template ${deploymentFolder}\\wwwroot\\RecHubServices\\web.config.template"
    bat "copy identity.app.config.template ${deploymentFolder}\\wwwroot\\RecHubServices\\identity.app.config.template"
  }
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/APP/${jobItems.BranchPath}/wwwroot/RecHubServices"
	def	artifactoryFilesToUpload = """{
      "files": [
        {
          "pattern": "${jobItems.BranchPath}/wwwroot/RecHubServices/*.*",
          "target": "tmsa.r360/${jobItems.BranchToBuild}/APP/${jobItems.BranchPath}/wwwroot/RecHubServices/",
          "recursive": false
        },
        {
          "pattern": "${jobItems.BranchPath}/wwwroot/RecHubServices/bin/*.*",
          "target": "tmsa.r360/${jobItems.BranchToBuild}/APP/${jobItems.BranchPath}/wwwroot/RecHubServices/bin/",
          "recursive": false
        }
      ]
    }"""

  getCommonScripts(workspace).publishToArtifactory(workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}
return this;