def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\OLFImageService"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build OLF Image Service..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\OLF\\OLFImageSvc\\OLFImageSvc.sln")

  echo "Building solution..."
  runMSBuild('2017','.\\Codebase\\OLF\\OLFImageSvc\\OLFImageSvc.sln',"${binariesFolder}")
  
  echo "Running unit tests..."
  
  String[] testFiles = ["${binariesFolder}\\ipoImages_UnitTest.dll","${binariesFolder}\\ItemProcDAL_UnitTest.dll",
    "${binariesFolder}\\LocateImageTests.dll","${binariesFolder}\\OLFImageTests.dll",
    "${binariesFolder}\\OLFServicesDAL_UnitTests.dll", "${binariesFolder}\\OLFServicesExternal.Test.dll", 
    "${binariesFolder}\\SystemIOTests.dll"]

  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\OLFImageService\\', 'Codebase\\BuildSupport\\PublishMap.OLFImageSvc.xml')
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;