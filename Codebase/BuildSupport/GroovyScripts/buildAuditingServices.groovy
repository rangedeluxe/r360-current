def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\AuditingServices"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Auditing Services..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\R360Services\\AuditingServices\\AuditingServicesSolution\\AuditingServices.sln")

  echo "Building solution..."
  
  runMSBuild('2017','.\\Codebase\\R360Services\\AuditingServices\\AuditingServicesSolution\\AuditingServices.sln',"${binariesFolder}")

  String[] testFiles = ["${binariesFolder}\\AuditingUnitTest.dll"]
  runMSTest('2017', testFiles)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\AuditingServices\\', 'Codebase\\BuildSupport\\PublishMap.AuditingServices.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;