def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  def publishFolder = "${jobItems.Workspace}\\Codebase\\R360Services\\R360API\\AdminAPI\\bin\\Publish"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  dir('Codebase/R360Services/R360API/AdminAPI') {
    bat "dotnet build -c release"
    bat "dotnet publish AdminAPI.csproj -c Release -o bin\\Publish"
  }
  gatherAdminAPIArtifacts(publishFolder,deploymentFolder)
  if (jobItems.PublishToArtifactory) {
    publishToArtifactory(jobItems)
  }
}

def runTests(workspace, branchPath) {
    dir('Codebase/R360Services/AdminAPI/AdminAPI.Tests') {
      bat "dotnet build AdminAPI.Tests.csproj -c Release -o bin\\Release"
    }
    script {
      String[] testFiles = ["${workspace}\\Codebase\\R360Services\\R360API\\AdminAPI.Tests\\bin\\Release\\AdminAPI.Tests.dll"]
      runMSTest('2017', testFiles)
    }
    step([$class: 'MSTestPublisher', testResultsFile:"**/*.trx", failOnError: true, keepLongStdio: true])
}

def gatherAdminAPIArtifacts(publishFolder, deploymentFolder) {
  echo "Creating ${deploymentFolder} AdminAPI deployment artifacts in workspace..."
  dir("${publishFolder}") {
    bat "xcopy /s /y /i *.* ${deploymentFolder}\\wwwroot\\AdminAPI"
  }
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/APP/${jobItems.BranchPath}/wwwroot/AdminAPI"
  def artifactoryFilesToUpload = """{
        "files": [
          {
            "pattern": "${jobItems.BranchPath}/wwwroot/AdminAPI/*.*",
            "target": "tmsa.r360/${jobItems.BranchToBuild}/APP/${jobItems.BranchPath}/wwwroot/AdminAPI/",
            "recursive": false
          },
		  {
            "pattern": "${jobItems.BranchPath}/wwwroot/AdminAPI/runtimes/win/lib/netstandard2.0/*.*",
            "target": "tmsa.r360/${jobItems.BranchToBuild}/APP/${jobItems.BranchPath}/wwwroot/AdminAPI/runtimes/win/lib/netstandard2.0/",
            "recursive": false
          }
        ]
      }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;