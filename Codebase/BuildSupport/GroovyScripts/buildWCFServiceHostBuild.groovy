def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"
  def solution = ".\\Codebase\\Apps\\WCFServiceHost\\WCFServiceHost.sln"
  def commonScripts = getCommonScripts(jobItems.Workspace)

  commonScripts.cleanupFolder(binariesFolder)
  commonScripts.cleanupFolder(deploymentFolder)

  echo "Starting to build WCF Service Host..."

  commonScripts.restoreNuget(solution)

  echo "Building WCF Service Host..."

  commonScripts.buildMSProject(solution, binariesFolder)

  commonScripts.poshPublishToArtifactory(jobItems, '\\Binaries\\', 'Codebase\\BuildSupport\\PublishMap.WCFServiceHost.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;