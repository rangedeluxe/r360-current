def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"
  
  def binariesFolder = "${jobItems.Workspace}\\binaries\\HubConfigService"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Hub Config Service..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\R360Services\\HubConfigService\\HubConfigServiceSolution\\HubConfigService.sln")

  echo "Building solution..."
  
  runMSBuild('2017','.\\Codebase\\R360Services\\HubConfigService\\HubConfigServiceSolution\\HubConfigService.sln',"${binariesFolder}")

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\HubConfigService\\', 'Codebase\\BuildSupport\\PublishMap.HubConfigService.xml')
  
}


def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;