def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  def binariesFolder = "${jobItems.Workspace}\\binaries\\DITBAI2WireTransfer"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Building solution..."
  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\DataImport\\ditWireXClient\\DITBAI2WireTransferImport.sln")
  runMSBuild('2017','.\\Codebase\\DataImport\\ditWireXClient\\DITBAI2WireTransferImport.sln',"${binariesFolder}")

  echo "Running unit tests..."
  String[] testFiles = ["${binariesFolder}\\WireTransferTest.dll"]
  runMSTest('2017', testFiles)


  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\DITBAI2WireTransfer\\', 'Codebase\\BuildSupport\\PublishMap.DITBAI2WireTransferImport.xml')
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;