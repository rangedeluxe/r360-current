def build(jobItems) {
  echo "${jobItems.Workspace}"
  echo "${jobItems.BranchPath}"

  buildHubConfigViews(jobItems)

}

def buildHubConfigViews(jobItems) {

  def binariesFolder = "${jobItems.Workspace}\\binaries\\HubConfigViews"
  def deploymentFolder = "${jobItems.Workspace}\\${jobItems.BranchPath}"

  getCommonScripts(jobItems.Workspace).cleanupFolder(binariesFolder)
  getCommonScripts(jobItems.Workspace).cleanupFolder(deploymentFolder)

  echo "Starting to build Hub Config Views..."

  getCommonScripts(jobItems.Workspace).restoreNuget(".\\Codebase\\UI\\UISolution\\UISolution.sln")

  echo "Building Hub config views solution..."
  runMSBuild('2017','.\\Codebase\\UI\\UISolution\\UISolution.sln',"${binariesFolder}")

  //gatherHubViewArtifacts(binariesFolder, deploymentFolder)

  getCommonScripts(jobItems.Workspace).poshPublishToArtifactory(jobItems,'\\Binaries\\HubConfigViews\\', 'Codebase\\BuildSupport\\PublishMap.HubConfigViews.xml')

}

def gatherHubViewArtifacts(binariesFolder, deploymentFolder) {
  echo "Creating ${deploymentFolder} Hub config View deployment artifacts in workspace..."

  dir("${binariesFolder}/_PublishedWebsites/HubConfigViews/bin") {
    bat "xcopy /y /i *.dll ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\bin"
  }

  dir("${binariesFolder}/_PublishedWebsites/HubConfigViews") {
    bat "copy Global.asax ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\Global.asax"
    bat "copy Error.aspx ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\Error.aspx"
    bat "copy Error.html ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\Error.html"
    bat "copy web.config ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\web.config.oem"
    bat "copy web.config.template ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\web.config.template"
    bat "copy identity.web.config.template ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\identity.web.config.template"
    bat "copy identityservices.web.config.template ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\identityservices.web.config.template"
    bat "copy webmachinekey.config.template ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\webmachinekey.config.template"
  }

  dir("${binariesFolder}/_PublishedWebsites/HubConfigViews/Views") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\Views"
  }

  dir("${binariesFolder}/_PublishedWebsites/HubConfigViews/Assets") {
    bat "xcopy /y /i *.* ${deploymentFolder}\\wwwroot\\RecHubConfigViews\\Assets"
  }
}

def publishToArtifactory(jobItems) {
  def artifactoryLocation = "http://artifactory.deluxe.com/tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubConfigViews"
	def artifactoryFilesToUpload = """{
          "files": [
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/HubConfigViews/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubConfigViews/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/HubConfigViews/bin/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubConfigViews/bin/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/HubConfigViews/Views/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubConfigViews/Views/",
              "recursive": false
            },
            {
              "pattern": "${jobItems.BranchPath}/wwwroot/HubConfigViews/Assets/*.*",
              "target": "tmsa.r360/${jobItems.BranchToBuild}/WEB/${jobItems.BranchPath}/wwwroot/RecHubConfigViews/Assets/",
              "recursive": false
            }
          ]
        }"""

  getCommonScripts(jobItems.Workspace).publishToArtifactory(jobItems.Workspace, artifactoryLocation, artifactoryFilesToUpload, true)  
}

def getCommonScripts(workspace) {
  def commonScripts = load "${workspace}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
  return commonScripts;
}

return this;