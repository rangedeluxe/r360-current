# Look for R360 work items that are in the specified module and "Ready for Build", mark them
# as "Build Complete", and set the Build Indicator field to the given build number string.
# For use by external build scripts that already do their own building and e-mailing, but
# that want to mark R360 work items (if present) as complete. E.g., RAAM and Framework.
#
# This script loads .NET DLLs, which behaves oddly if you try to do it more than once
# in the same PowerShell session. Recommended usage is to launch this script in a new
# PowerShell session, even if you're calling it from PowerShell. Example usage:
#
#   powershell -file .\MarkWorkItemsBuildComplete.ps1 "Framework" "Framework_15.08.18.2"

param (
    [string] $ModuleName,
    [string] $BuildNumber
)
$ErrorActionPreference = 'Stop'

$TFSServer = "http://TFSPROD:8080/tfs/wfs";
$TeamProjectName = "Receivables360 Online"

Write-Host "Module name: $ModuleName"
Write-Host "Build number: $BuildNumber"
Write-Host "TFS server: $TFSServer"
Write-Host "Team project: $TeamProjectName"
Write-Host

$ScriptDirectory = Split-Path $MyInvocation.MyCommand.Path
Import-Module (Join-Path $ScriptDirectory ".\ScrumTemplate.BuildModules.ps1");
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.WorkItemTracking.Client") | Out-Null

function GetWorkItemIdsToUpdate($TFS) {
    $WIQL = "
        SELECT [System.Id]
        FROM WorkItemLinks
        WHERE
            [Source].[System.TeamProject] = '$TeamProjectName' AND
            [Source].[WFS.Version] = 'Current' AND
            [System.Links.LinkType] = 'System.LinkTypes.Hierarchy-Forward' AND
            [Target].[System.WorkItemType] = 'Task' AND
            [Target].[WFS.Modules] = '$ModuleName' AND
            [Target].[WFS.WorkItemStatus] = 'Ready for Build'
        MODE(MustContain)
    "

    $Query = New-Object Microsoft.TeamFoundation.WorkItemTracking.Client.Query($TFS.WIT, $WIQL)
    [int[]] $TaskIds = $Query.RunLinkQuery() | ?{
        $_.SourceId -ne 0    # filter out the parent nodes; we only want the child nodes
    } | %{
        $_.TargetId          # return the task IDs
    }

    $TaskIds
}

function GetWorkItems($TFS, $Ids) {
    if ($Ids -ne $Null) {
        $IdsAsString = [String]::Join(",", $Ids)
        if ($IdsAsString -ne '') {
            $WIQL = "SELECT [System.Id] FROM WorkItems WHERE [System.Id] IN ($IdsAsString)"
            $Tasks = $TFS.WIT.Query($WIQL)
        }
    }

    $Tasks
}

function UpdateTask($TFS, $Task) {
    Write-Host "Updating task $($Task.Id): $($Task.Title)"

    $Task.Open()
    $Task.Fields['WFS.BuildIndicator'].Value = $BuildNumber
    $Task.Fields['WFS.WorkItemStatus'].Value = "Build Complete"
    $Task.Save()
}

$TFS = Get-TFS $TFSServer
Write-Host "Looking for $TeamProjectName work items to update..."
$TaskIds = GetWorkItemIdsToUpdate $TFS
$Tasks = GetWorkItems $TFS $TaskIds
if ($Tasks) {
    $Tasks | %{
        UpdateTask $TFS $_
    }
} else {
    Write-Host "No work items to update."
}