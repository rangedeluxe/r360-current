﻿param (
    [string] $buildNumber = "1.0.0.0",
    [string] $binSource = "",
    [string] $deployMap = "",
    [string] $buildName,
    [string] $ManualBuildRequestedBy = "",
    [string] $buildBaseDir = $Null,
    [string] $ErrorMessage = $Null,
    [string] $BuildUri = $Null, # e.g. vstfs:///Build/Build/17080
    [switch] $AlwaysDeploy,
    [string] $EmailList
)

$ErrorActionPreference = 'Stop'

$TFSServer = "http://TFSPROD:8080/tfs/wfs";
$TeamProjectName = "Receivables360 Online"
$ILMergeExe = "C:\Tools\ILMerge_2.12.0803\ILMerge.exe";
$snExe = "C:\Tools\sn.exe";
$SmtpServer = "relay.wausaufs.com"

# Load modules and assemblies
$ScriptDirectory = Split-Path $MyInvocation.MyCommand.Path
Import-Module (Join-Path $ScriptDirectory ".\ScrumTemplate.BuildModules.ps1");
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.VersionControl.Client") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.WorkItemTracking.Client") | Out-Null
Add-Type -AssemblyName System.Web

# Information about the build definition

function GetBuildNameSegments {
    # 'QA - DIT XClient ACH'
    # 'Dev - DIT XClient ACH'
    # '2.01 - DIT XClient ACH'
    $buildName.Split("-", 2) | %{ $_.Trim() }
}

function GetBuildBranchName {
    # 'Main', 'Dev', '2.01', etc.
    $Name = (GetBuildNameSegments)[0]
    if ($Name -eq "QA") {
        $Name = "Main"
    }
    $Name
}

function GetBuildVersionDisplayName {
    # 'Current', 'Dev', '2.01', etc.
    $Name = (GetBuildNameSegments)[0]
    if ($Name -eq "QA") {
        $Name = "Current"
    }
    $Name
}

function GetWorkItemVersion {
    # 'Current', '2.01', etc. - matches the "Version" work item field in TFS
    $Name = (GetBuildNameSegments)[0]
    if (($Name -eq "Dev") -or ($Name -eq "QA")) {
        $Name = "Current"
    }
    $Name
}

function IsDevBranch {
    (GetBuildNameSegments)[0] -eq "Dev"
}

function GetTfsPathForBuildBranch {
    $BranchName = GetBuildBranchName

    # Handle cases where we weren't consistent with the version number
    if ($BranchName -eq "2.01") {
        # The branch corresponding to work item Version="2.01" is in $/.../Release 2.01.01.
        $BranchName = "2.01.01"
    }

    if ($BranchName -match "^\d") {
        # Numbered branch - find it under the "Releases" folder
        "$/Receivables360 Online/Releases/Release $BranchName"
    } else {
        # Named branch - find it directly under Receivables360 Online
        "$/Receivables360 Online/$BranchName"
    }
}

# Utility functions

function GetBuildPageUrl {
    if ($BuildUri) {
        $BuildId = ([Uri] $BuildUri).Segments | Select-Object -Last 1
        "http://tfsprod:8080/tfs/WFS/Receivables360%20Online/_build#_a=summary&buildId=$BuildId"
    }
}

function GetChangesetOfLastBuildWithStatus($TFS, $Status) {
    $BuildDetailSpec = $TFS.BS.CreateBuildDetailSpec($TeamProjectName, $buildName)
    $BuildDetailSpec.Status = $Status
    $BuildDetailSpec.QueryOrder = "StartTimeDescending"
    $BuildDetailSpec.MaxBuildsPerDefinition = 1
    $BuildDetailSpec.QueryOptions = "None"  # optimization: don't return extra metadata
    $LastSuccessfulBuild = $TFS.BS.QueryBuilds($BuildDetailSpec).Builds
    $SourceGetVersion = $LastSuccessfulBuild.SourceGetVersion
    if ($SourceGetVersion) {
        # "C12345" -> 12345
        [int]$SourceGetVersion.Trim("C")
    } else {
        0
    }
}

function GetChangesetOfLastSuccessfulBuild($TFS) {
    $LastSucceeded = GetChangesetOfLastBuildWithStatus $TFS "Succeeded"
    $LastPartiallySucceeded = GetChangesetOfLastBuildWithStatus $TFS "PartiallySucceeded"
    ($LastSucceeded, $LastPartiallySucceeded | Measure-Object -Maximum).Maximum
}

function GetChangesetAndTaskIdTuplesSince($TFS, $MinChangesetId) {
    $Branch = GetTfsPathForBuildBranch
    $RecursionTypeFull = "Full"
    $QueryHistoryParameters = New-Object Microsoft.TeamFoundation.VersionControl.Client.QueryHistoryParameters($Branch, $RecursionTypeFull)
    $QueryHistoryParameters.VersionStart = New-Object Microsoft.TeamFoundation.VersionControl.Client.ChangesetVersionSpec($MinChangesetId)
    $History = $TFS.VCS.QueryHistory($QueryHistoryParameters)
    $History | %{
        $Changeset = $_
        $_.AssociatedWorkItems | ?{ $_.WorkItemType -eq "Task" } | %{
            New-Object PSObject -Property @{ Changeset = $Changeset; TaskId = $_.Id }
        }
    }
}

function GetChangesetsAndTaskIdsToBuild($TFS, $ModuleNames) {
    $DisplayModuleNames = [System.String]::Join(" or ", ($ModuleNames | %{ "'$_'" }))
    $ModuleNamesForWiql = [System.String]::Join(", ", ($ModuleNames | %{ "'" + ($_ -replace "'", "''") + "'" }))

    if ($ManualBuildRequestedBy) {
        Write-Host "Looking for 'Ready For Build' tasks for $DisplayModuleNames"
        $TaskFilter = "[Target].[WFS.WorkItemStatus] = 'Ready For Build'"
        $ChangesetAndTaskIdTuples = $Null
    } else {
        $LastSuccessfulBuild = GetChangesetOfLastSuccessfulBuild $TFS
        $MinChangesetId = $LastSuccessfulBuild + 1
        Write-Host "Looking for changesets associated with $DisplayModuleNames with ID >= C$MinChangesetId"
        $ChangesetAndTaskIdTuples = GetChangesetAndTaskIdTuplesSince $TFS $MinChangesetId
        if ($ChangesetAndTaskIdTuples -eq $null) {
            Write-Host "Query returned no results"
            return New-Object PSObject -Property @{ Changesets = @(); TaskIds = @() }
        }
        $TaskIdsCheckedIn = $ChangesetAndTaskIdTuples | %{ $_.TaskId } | Sort-Object -Unique
        $IDs = [String]::Join(",", $TaskIdsCheckedIn)
        if ($IDs -eq '') { $IDs = "-1" }  # Avoid errors due to "IN ()"
        Write-Host "Candidate IDs: $IDs"
        $TaskFilter = "[Target].[System.Id] IN ($IDs)"
    }

    $WIQL = "
        SELECT [System.Id], [System.Links.LinkType], [System.WorkItemType], [System.Title], [System.AssignedTo], [System.State]
        FROM WorkItemLinks
        WHERE
            [Source].[System.TeamProject] = '$TeamProjectName' AND
            [Source].[WFS.Version] = '$(GetWorkItemVersion)' AND
            [System.Links.LinkType] = 'System.LinkTypes.Hierarchy-Forward' AND
            [Target].[System.WorkItemType] = 'Task' AND
            [Target].[WFS.Modules] IN ($ModuleNamesForWiql) AND
            $TaskFilter
        ORDER BY [System.Id]
        MODE(MustContain)
    "

    $Query = New-Object Microsoft.TeamFoundation.WorkItemTracking.Client.Query($TFS.WIT, $WIQL)
    [int[]] $TaskIds = $Query.RunLinkQuery() | ?{
        $_.SourceId -ne 0    # filter out the parent nodes; we only want the child nodes
    } | %{
        $_.TargetId          # return the task IDs
    }
    if (($ChangesetAndTaskIdTuples -ne $Null) -and ($TaskIds -ne $Null)) {
        $Changesets = $ChangesetAndTaskIdTuples | ?{ $TaskIds.Contains($_.TaskId) } | %{ $_.Changeset } | Sort-Object ChangesetId -Unique
    } else {
        $Changesets = @()
    }
    return New-Object PSObject -Property @{ Changesets = $Changesets; TaskIds = $TaskIds }
}

function GetChangesetsAndTasksToBuild($TFS, $ModuleNames) {
    $ChangesetsAndTaskIdsToBuild = GetChangesetsAndTaskIdsToBuild $TFS $ModuleNames
    $TaskIdsToBuild = $ChangesetsAndTaskIdsToBuild.TaskIds
    $Tasks = @()
    if ($TaskIdsToBuild -ne $null) {
        $IDs = [String]::Join(",", $TaskIdsToBuild)
        if ($IDs -ne '') {
            $WIQL = "SELECT [System.Id] FROM WorkItems WHERE [System.Id] IN ($IDs)"
            $Tasks = $TFS.WIT.Query($WIQL)
        }
    }
    return New-Object PSObject -Property @{ Changesets = $ChangesetsAndTaskIdsToBuild.Changesets; Tasks = $Tasks }
}

function GetParentId($WorkItem) {
    $ParentLink = $WorkItem.WorkItemLinks | ?{ $_.LinkTypeEnd.Name -eq 'Parent' }
    $ParentLink.TargetId
}

# Temp-directory functions

function GetTempDirectory($UniqueId) {
    # Expand the path because of known PowerShell bug with DOS short filenames:
    # https://connect.microsoft.com/PowerShell/feedbackdetail/view/874645/dos-style-8-3-shortnames-that-resolve-to-actual-shorter-long-names-results-in-an-erroneous-itemnotfoundexception
    $TempDirectory = [System.IO.Path]::GetFullPath($env:Tmp)
    Join-Path $TempDirectory "BuildTemp.$UniqueId"
}

function CleanUpOldTempDirectories {
    try {
        $Mask = GetTempDirectory "*"
        Write-Host "Looking for old temp directories in $Mask"
        $Timeout = [System.TimeSpan]::FromHours(4)
        Get-ChildItem $Mask -Directory | ?{
            ([System.DateTime]::Now - $_.CreationTime) -ge $Timeout
        } | %{
            Write-Host "Trying to remove old temp directory $_"
            Remove-Item $_ -Recurse -Force -ErrorAction SilentlyContinue
        }
    } catch {
        Write-Host "Warning: received error '$($_.Exception)' trying to remove temp directory. Continuing."
    }
}

function CreateTempDirectory {
    $Directory = GetTempDirectory ([System.Guid]::NewGuid())
    New-Item $Directory -Type Directory | Out-Null
    $Directory
}

# Deployment functions

function GetBuildNumberFolderName($ModuleName) {
    $Parts = $buildNumber.Split('.')
    $FirstParts = $Parts | Select-Object -First ($Parts.Count - 1)
    $LastPart = $Parts | Select-Object -Last 1
    [System.String]::Join(".", $FirstParts) + "\$ModuleName.$LastPart"
}

function GetSprintName($TFS) {
    $TeamProject = $TFS.CSS.GetProjectFromName($TeamProjectName)
    $LifecycleUri = $TFS.CSS.ListStructures($TeamProject.Uri) | ?{ $_.StructureType -eq 'ProjectLifecycle' } | %{ $_.Uri }
    $Xml = $TFS.CSS.GetNodesXml($LifecycleUri, $True)
    $CurrentSprint = $Xml.Node.Children.Node |
        ?{ $_.StartDate -and ([DateTime] $_.StartDate -le (Get-Date)) } |
        Sort-Object StartDate |
        Select-Object -Last 1
    $CurrentSprint.Name -replace " ", "_"
}

function GetPublishDirs($TFS, $ModuleName) {
    $BuildDir = Join-Path (Join-Path $buildBaseDir (Get-Date -Format yyyy)) (GetBuildVersionDisplayName)
    if (IsDevBranch) {
        @(
            (Join-Path $BuildDir "Current_Build")
        )
    } else {
        # The first directory should be the one for this individual build.
        @(
            (Join-Path $BuildDir (GetBuildNumberFolderName $ModuleName))
            (Join-Path $BuildDir (GetSprintName $TFS))
            (Join-Path $BuildDir "Current_Hotfix")
            (Join-Path $BuildDir "Current_Build")
        )
    }
}

function GetModuleDeployDirs($ModuleName, $OutputDirectory, $TierList) {
    if ($TierList) {
        $Tiers = $TierList.Split(',') | %{ Join-Path $_.Trim() "RecHub2.1" }
    } else {
        $Tiers = @("Other\$ModuleName")
    }
    $ModuleDeployDirs = $Tiers | %{
        Join-Path $OutputDirectory $_
    }
    $ModuleDeployDirs
}

function ProcessTopLevelFilter($filter, $sourceDir, $deployDirs) {
    $sourcePath = Join-Path $sourceDir "*";
    if ($filter -ne $null) {
        foreach ($deployDir in $deployDirs) {
            $destDir = Join-Path $deployDir $filter.DestPath;
            EnsureDir $destDir;
            if($filter.Rename -ne $null) {
                Get-ChildItem -Path $sourcePath -filter $filter.Pattern|% {Copy-Item -Path (Join-Path $sourceDir $_.name) -destination (Join-Path $destDir ($filter.Rename -replace '<file>',$_.name)) -Force};
            }
            else {
                Copy-Item -Path $sourcePath -Destination $destDir -include $filter.Pattern -Force;
            }
        }
    }
}

function ProcessTopLevelFilters($ModuleName, $filters, $sourceDir, $OutputDirectory) {
    foreach ($filter in $source.Filter) {
        $ModuleDeployDirs = GetModuleDeployDirs $ModuleName $OutputDirectory (GetTier $filter)
        ProcessTopLevelFilter $filter $sourceDir $ModuleDeployDirs
    }
}

function ProcessDestDirectory($dest, $recursive, $sourceDir, $deployDirs) {
    $sourcePath = Join-Path $sourceDir "*";
    $includePat = @();
    $excludePat = @();
    foreach ($filter in $dest.Filter) {
        if ($filter.Pattern -ne $null) {
            $includePat += $filter.Pattern;
        }
    }
    foreach ($filter in $dest.Exclude) {
        if ($filter.Pattern -ne $null) {
            $excludePat += $filter.Pattern;
        }
    }
    if ($includePat.Count -ne 0) {
        foreach ($deployDir in $deployDirs) {
            $destDir = Join-Path $deployDir $dest.Path;
            EnsureDir $destDir;
            if ($recursive -ne $null) {
                foreach ($curSubDir in (Get-ChildItem -Path $sourceDir -Recurse| ?{$_.PSIsContainer})) {
                    $curDest = (Join-Path $destDir ($curSubDir.FullName -replace ("^" + [regex]::escape($sourceDir)), ""));
                    EnsureDir $curDest;
                    Copy-Item -Force -Path (Join-Path $curSubDir.FullName "*") -destination $curDest -include $includePat -exclude $excludePat;
                }
            }
            Copy-Item -Force -Path $sourcePath -Destination $destDir -include $includePat -exclude $excludePat;
        }
    }
}

function ProcessDestDirectories($ModuleName, $dests, $recursive, $sourceDir, $OutputDirectory) {
    foreach ($dest in $dests) {
        $ModuleDeployDirs = GetModuleDeployDirs $ModuleName $OutputDirectory (GetTier $Dest)
        ProcessDestDirectory $dest $recursive $sourceDir $ModuleDeployDirs
    }
}

function ProcessILMerge($merge, $binSource, $sourceDir, $deployDirs) {
    $sourcePath = Join-Path $sourceDir "*";
    $mergeFiles = @();
    $mergeSign = "";
    $mergePlatform = "v4,C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319";

    if($merge.KeyFile) {
        $mergeSign = "/keyfile:""" + (Join-Path $binSource $merge.KeyFile) + """";
    }

    if($merge.Framework) {
        if($merge.Framework -eq "4.0") {
            $mergePlatform = "v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0";
        }
        elseif ($merge.Framework -eq "4.6") {
            $mergePlatform = "v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6";
        }
    }

    if($merge -ne $null) {
        foreach($filter in $merge.Filter) {
            if($filter -ne $null) {
                $files = Get-ChildItem -path $sourcePath -include $filter.Pattern|%{Join-Path $sourceDir $_.Name};
                foreach($file in $files) {
                    $mergeFiles += $file;
                }
            }
        }

        $mergeList = """" + ($mergeFiles -Join """ """) + """";
        $logDir = Join-Path ($deployDirs | Select-Object -First 1) "..\Logs"
        $logFile = Join-Path $logDir "MergeLog.txt";
        $tempDir = Join-Path $sourceDir "Temp";
        $tempFile = Join-Path $tempDir (split-path $merge.DestFile -Leaf);

        EnsureDir $logDir;
        EnsureDir $tempDir $True;

        if($mergeFiles.Count -ne 0) {
            $args = """/targetplatform:$mergePlatform"" /ndebug /closed $mergeList $mergeSign /out:""$tempFile"" /log:""$logFile"" /allowDup";
            $oProc = Start-Process $ilmergeExe -Args $args  -PassThru;
            Wait-Process -inputObject $oProc;
            if($merge.KeyFile) {
                $oProc = Start-Process $snExe -Args ("-R ""$tempFile""" + $merge.KeyFile + """") -PassThru;
                Wait-Process -inputObject $oProc;
            }
            if ((Test-Path $tempFile) -and ((Get-Item $tempFile).Length -gt 0)) {
                foreach($deployDir in $deployDirs) {
                    $destFile = Join-Path $deployDir $merge.DestFile;
                    EnsureDir (Split-Path -parent $destFile);
                    Copy-Item -Path $tempFile -Destination $destFile;
                }
                Write-Host "ILMerge Completed Successfully";
            }
            else {
                throw New-Object System.Exception("IL Merge Failed (see logs at: ""$logFile"")")
            }
        }
    }
}

function ProcessILMerges($ModuleName, $merges, $binSource, $sourceDir, $OutputDirectory) {
    foreach($merge in $merges) {
        $ModuleDeployDirs = GetModuleDeployDirs $ModuleName $OutputDirectory (GetTier $Merge)
        ProcessILMerge $merge $binSource $sourceDir $ModuleDeployDirs
    }
}

function GetTier($Node) {
    while ($Node -and !($Node.Tier)) {
        $Node = $Node.ParentNode
    }
    $Node.Tier
}

function BuildTempDirectory($TFS, $ModuleName, $ModulePublishMap, $OutputDirectory) {
    if (!(Test-Path -Path $binSource)) {
        throw New-Object System.Exception("Binaries path not found ($binSource)");
    }

    Write-Host "Building temp directory: $OutputDirectory"

    foreach ($source in $ModulePublishMap.Source) {
        $sourceDir = [System.IO.Path]::GetFullPath((Join-Path $binSource $source.BuildDir));
        ProcessTopLevelFilters $ModuleName $source.Filter $sourceDir $OutputDirectory
        ProcessDestDirectories $ModuleName $source.Dest $source.Recursive $sourceDir $OutputDirectory
        ProcessILMerges $ModuleName $source.Merge $binSource $sourceDir $OutputDirectory
    }

    Get-ChildItem $OutputDirectory -File -Recurse | ?{
        $_.IsReadOnly
    } | %{
        Write-Host "Clearing read-only flag: $_"
        $_.IsReadOnly = $False
    }
}

function Publish($TempDirectory, $PublishDirs) {
    $PublishDirs | %{
        Write-Host "Publishing to: $_"
        $SourcePath = Join-Path $TempDirectory "*"
        $TargetPath = $_
        New-Item $TargetPath -Type Directory -Force | Out-Null
        Copy-Item $SourcePath $TargetPath -Recurse -Force
    }
}

# Task-update functions

function UpdateTasks($WorkItemCollection) {
    if (IsDevBranch) {
        return;
    }

    Write-Host "Updating tasks"
    foreach ($Task in $WorkItemCollection) {
        $Task.Open()
        $Task.Fields['System.History'].Value = "Deployed to QA in $buildNumber"
        $Task.Fields['WFS.BuildIndicator'].Value = $buildNumber
        $Task.Fields['WFS.WorkItemStatus'].Value = "Build Complete"
        $Task.Save()
    }
}

# E-mail functions

function HtmlEncode($text) {
    [System.Web.HttpUtility]::HtmlEncode($text)
}

function GetEmailRecipients {
    if ($EmailList) {
        $EmailList.Split(',')
    } elseif ($ErrorMessage) {
        @("R360Developers@wausaufs.com")
    } else {
        @("R360DevelopmentTeam@wausaufs.com")
    }
}

function GetChildDeveloperTaskIdsForParentId($TFS, $ParentId) {
    $WIQL = "
        SELECT [System.Id]
        FROM WorkItemLinks
        WHERE
            [Source].[System.Id] = $ParentId AND
            [System.Links.LinkType] = 'System.LinkTypes.Hierarchy-Forward' AND
            [Target].[System.WorkItemType] = 'Task' AND
            [Target].[Microsoft.VSTS.Common.Activity] <> 'Testing'
        ORDER BY [System.Id]
        MODE(MustContain)
    "
    $Query = New-Object Microsoft.TeamFoundation.WorkItemTracking.Client.Query($TFS.WIT, $WIQL)
    $Query.RunLinkQuery() | ?{
        $_.SourceId -ne 0    # filter out the parent nodes; we only want the child nodes
    } | %{
        $_.TargetId          # return the task IDs
    }
}

function GetParentTypeDisplayName($Parent) {
    $TypeName = $Parent.Type.Name
    if ($TypeName -eq "Product Backlog Item") {
        "PBI"
    } else {
        $TypeName
    }
}

function GetTaskStatusString($Task) {
    $StatusString = $Task.Fields['WFS.WorkItemStatus'].Value
    if ($StatusString -eq '') {
        $StatusString = $Task.State
        if ($StatusString -eq 'Done') {
            $StatusString = 'In Progress'
        }
    }
    if (($StatusString -eq 'Build Complete') -or ($StatusString -eq 'Ready for Test')) {
        "<span style='color: #00AA00'>$StatusString</span>"
    } else {
        $StatusString
    }
}

function GetEmailBody($TFS, $Tasks, $Changesets, $BuiltModules) {
    $FontStyle = "font-family: Segoe UI; font-size: 80%"
    $Table = "<table cellspacing='0' cellpadding='0' style='$FontStyle'>"
    $SpacerCell = "<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>"
    $Star = "&#9733;"

    $Body = "<html><body style='$FontStyle'>`n"
    if ($ErrorMessage) {
        $Body += "<p style='color: red; white-space: pre-wrap'>Error message:<br>$(HtmlEncode $ErrorMessage)</p>`n"
    } else {
        $Body += "A new R360 $(GetBuildVersionDisplayName) build is ready to be installed.<br><br>`n"
    }
    $Body += "$Table`n"
    $Body += "<tr><td valign='top'>Version:</td>$SpacerCell<td valign='top'>$(GetBuildVersionDisplayName)</td></tr>`n"
    $Body += "<tr><td valign='top'>Build:</td>$SpacerCell<td valign='top'>"
    $BuildPageUrl = GetBuildPageUrl
    if ($BuildPageUrl) {
        $Body += "<a href='$BuildPageUrl'>"
    }
    $Body += "$BuildName $BuildNumber"
    if ($BuildPageUrl) {
        $Body += "</a>"
    }
    $Body += "</td></tr>`n"
    if ($BuiltModules) {
        $Body += "<tr><td valign='top'>Module:</td>$SpacerCell<td valign='top'>`n"
        $BuiltModules | %{
            $Module = $_

            $Body += "  <div>"
            $Body += $Module.Name

            $PublishDir = $Module.PublishDir
            if ($PublishDir -and (!$ErrorMessage)) {
                $Body += " <a href='file:///$($PublishDir.Replace(' ', '%20'))' style='font-family: Wingdings; text-decoration: none'>1</a>"
            }
            $Body += "</div>`n"
        }
        $Body += "</td></tr>`n"
    }
    $Body += "<tr><td>&nbsp;</td></tr>"
    $Body += "</table>`n"
    $Parents = $Tasks | %{ GetParentId $_ } | Sort-Object -Unique
    foreach ($ParentId in $Parents) {
        $Parent = $TFS.WIT.GetWorkItem($ParentId)
        $Body += "<div style='background: #CCC; color: #000; font-size: 120%'>`n"
        $Body += "  <b>$(GetParentTypeDisplayName($Parent)) #$($ParentId): $(HtmlEncode($Parent.Title))</b>`n"
        $Body += "</div>`n"
        $Body += "$Table`n"
        $TaskIds = GetChildDeveloperTaskIdsForParentId $TFS $ParentId
        foreach ($TaskId in $TaskIds) {
            $Task = $TFS.WIT.GetWorkItem($TaskId)
            $Starred = $Tasks | ?{ $_.Id -eq $Task.Id }
            $IsErrorRow = $ErrorMessage -and $Starred
            if ($IsErrorRow) {
                $Body += "<tr style='color: red'>`n"
            } else {
                $Body += "<tr>`n"
            }
            $Body += "<td valign='top' style='white-space:nowrap'>"
            if ($IsErrorRow) {
                $Body += "<b>Build Failed</b>"
            } else {
                $Body += "$(GetTaskStatusString($Task))"
            }
            $Body += "</td>$SpacerCell`n"
            $Body += "<td valign='top' style='white-space:nowrap'>$($Task.Fields['WFS.Modules'].Value)</td>$SpacerCell`n"
            $Body += "<td valign='top'>#$($Task.Id): $(HtmlEncode($Task.Title))"
            if ($Starred) {
                $Body += "&nbsp;&nbsp;<span style='color: #FF9900'>$Star</span>"
            }
            $Body += "</td>`n"
            $Body += "</tr>`n"
        }
        $Body += "<tr><td>&nbsp;</td></tr>"
        $Body += "</table>`n"
    }
    $Body += "<br><br><div style='border-top: 1px solid #CCC; font-size: 80%; font-style: italic'>`n"
    if ($ErrorMessage) {
        $Body += "$Star indicates tasks that were included in this build.`n"
    } else {
        $Body += "$Star indicates tasks that were newly deployed in this build.`n"
    }
    $Body += "<span style='color: #AAA'>`n"
    if ($ManualBuildRequestedBy) {
        $Body += "<br><br>This build was manually queued by $ManualBuildRequestedBy.`n"
    } else {
        $Changesets | %{
            $Body += "<br><br>C$($_.ChangesetId) $(HtmlEncode $_.Comment) ($(HtmlEncode $_.CommitterDisplayName))`n"
        }
    }
    $Body += "</span></div>`n"
    $Body += "</body></html>"
    $Body
}

function SendEmailNotification($TFS, $Tasks, $Changesets, $BuiltModules) {
    if ((IsDevBranch) -and !$ErrorMessage) {
        return;
    }

    if ($ErrorMessage) {
        Write-Host "Sending 'failure' e-mail notification"
    } else {
        Write-Host "Sending 'success' e-mail notification"
    }

    $Recipients = GetEmailRecipients
    $Body = GetEmailBody $TFS $Tasks $Changesets $BuiltModules

    $Message = New-Object System.Net.Mail.MailMessage
    $Message.Subject = "R360 $(GetBuildVersionDisplayName) Build"
    if ($ErrorMessage) {
        $Message.Subject += " FAILED"
    }
    $Message.From = New-Object System.Net.Mail.MailAddress("R360 Build R360BuildCompletion@wausaufs.com")
    foreach($Recipient in $Recipients) {
        $Message.To.Add($Recipient)
    }
    $Message.IsBodyHTML = $True
    $Message.Body = $Body
    $Smtp = New-Object Net.Mail.SmtpClient($SmtpServer)
    $Smtp.Send($Message)
}

# Determining which modules belong to this build

function GetKnownModules([xml] $DeployData) {
    $DeployData.PublishMap.Module | %{
        @{ ModuleName = $_.Name; ModulePublishMap = $_ }
    }
}

function GetModulesToPublish($Modules, $Tasks) {
    if ($AlwaysDeploy -or (IsDevBranch)) {
        # Publish all modules defined for this build
        $Modules
    } else {
        # Publish only the modules that actually appear in our tasks
        $TaskModules = $Tasks | %{ $_.Fields["WFS.Modules"].Value } | Sort-Object -Unique
		$Modules | ?{ $TaskModules -contains $_.ModuleName -or $_.ModuleName -eq 'AlwaysDeploy'} 
    }
}

# Main program

if (!$buildBaseDir) {
    if (IsDevBranch) {
        $buildBaseDir = "\\sanserver\ProductDevelopment\R360\DevBuilds\"
    } else {
        $buildBaseDir = "\\sanserver\ProductDevelopment\Legacy\QA Pending\RecHub_Build_Location\"
    }
}

Write-Host "Build Number: $buildNumber";
Write-Host "Binaries Path: $binSource";
Write-Host "Deploy Map: $deployMap";
Write-Host "Build Name: $buildName";
Write-Host "Build Base Path: $buildBaseDir";
Write-Host "TFS Server: $TFSServer";
if ($BuildUri) { Write-Host "Build URI: $BuildUri" }
if ($TestMode) { Write-Host "Operating in test mode" }
if ($ErrorMessage) { Write-Host "Build Error: $ErrorMessage" }
Write-Host

$TFS = Get-TFS $TFSServer
$Modules = GetKnownModules (Get-Content $deployMap)
$ChangesetsAndTasks = GetChangesetsAndTasksToBuild $TFS ($Modules | %{ $_.ModuleName })
$BuiltModules = @()
$ShouldSendEmail = $True
$AlwaysDeployModule  = $Modules | ?{ $_.ModuleName -eq 'AlwaysDeploy' }
if (!$ErrorMessage) {
    $ForceDeploy = $AlwaysDeploy -or (IsDevBranch) -or $AlwaysDeployModule
    if (($ChangesetsAndTasks.Tasks.Count -eq 0) -and (!$ForceDeploy)) {
        Write-Host "No tasks found to deploy"
        $ShouldSendEmail = $False
    } else {
        try {
            $ChangesetsAndTasks.Tasks | %{ Write-Host "Task: $($_.Id) $($_.Title) ($($_.Fields['WFS.Modules'].Value))" }
            $ChangesetsAndTasks.Changesets | %{ Write-Host "Changeset: C$($_.ChangesetId) $($_.Comment) ($($_.CommitterDisplayName))"}

            $ModulesToPublish = GetModulesToPublish $Modules $ChangesetsAndTasks.Tasks
            if ($ModulesToPublish) {
                Write-Host ("Publishing modules: " + [System.String]::Join(", ", ($ModulesToPublish | %{ $_.ModuleName } )))
            } else {
                throw "Unexpected error: No matching modules to publish"
            }

            CleanUpOldTempDirectories
            $ModulesToPublish | %{
                $ModuleName = $_.ModuleName
                $ModulePublishMap = $_.ModulePublishMap

                $PublishDirs = GetPublishDirs $TFS $ModuleName
                $BuiltModules += @{
                    Name = $ModuleName
                    PublishDir = $PublishDirs | Select-Object -First 1
                }

                $TempDirectory = CreateTempDirectory

                BuildTempDirectory $TFS $ModuleName $ModulePublishMap $TempDirectory

                Publish $TempDirectory $PublishDirs
            }

            UpdateTasks $ChangesetsAndTasks.Tasks
        } catch {
            $ErrorMessage = $_.Exception.Message
            Write-Host "Exception: $($_.Exception)"
        }
    }
}

if ($ShouldSendEmail) {
    try {
        # If we have an error message (either passed into the script, or from the 'catch' above), this will react appropriately.
        SendEmailNotification $TFS $ChangesetsAndTasks.Tasks $ChangesetsAndTasks.Changesets $BuiltModules
    } catch {
        Write-Host "Warning: Unable to send e-mail. $($_.Exception)"
    }
}

if ($ErrorMessage) {
    # If we got an error inside this script, we need to make sure to fail the build.
    $Host.UI.WriteErrorLine($ErrorMessage)
    Exit 1
}