﻿#$ErrorActionPreference = "Stop"
param 
(
	[parameter(Mandatory = $true)][string] $ProjectConfigFile = "",
	[parameter(Mandatory = $true)][string] $BuildNumber = "",
	[parameter(Mandatory = $true)][string] $WorkspaceFolder = "",
	[parameter(Mandatory = $true)][string] $SharePointVar = "",
	[string] $SinceCommit = "",
	[string] $DatabaseFolder = "Database",
	[string] $Version= "Current",
	[string] $Branch = "Dev"
)

$ScriptName = "GitDatabaseBuild";
$ScriptVerison = "3.01"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
################################################################################
# 10/11/2016 WI XXXXXX JPB	0.00	Created.
# 12/09/2016 PT #134097217 JPB 0.12 Re-factored main report function to make it faster.
# 04/05/2017 PT #XXXXXXXXX JPB 0.13 Make sure there is a valid commit ID.
# 03/16/2018 PT #156018711 JPB 3.00 Updated to format only, files to Staging folder on build server.
# 09/24/2018 PT #158427298 JPB 3.01 Ignore any changes in the BuildSupport folder, read the release
#									config from the json file
################################################################################
Import-Module -Name $PSScriptRoot\GitLibrary -Force;
Import-Module -Name $PSScriptRoot\PivotalTrackerLibrary -Force;

################################################################################
# Write-AppConsole
# Display message with script name/version
################################################################################
function Write-AppConsole([String] $local:ConsoleInformation)
{
	begin
	{
	}
	process
	{
		if( $_ -ne $null )
		{
			Write-Host ([string]::Format("{0}[{1}] {2}",$ScriptName,$ScriptVerison,$_));
		}
	}
	end
	{
		Write-Host ([string]::Format("{0}[{1}] {2}",$ScriptName,$ScriptVerison,$local:ConsoleInformation));
	}
}

################################################################################
# Add-HTMLNotification-Header
# Create a HTML report header of the items that were processed
################################################################################
function Add-HTMLNotification-Header([System.Text.StringBuilder]$local:BuildResults,[string]$local:ReportTitle,[string]$local:BuildNumber,[string]$local:Version,[string]$local:Branch)
{
	#Create HTML document with a table
	$local:BuildResults.Append("<html><body><table style=""width:100%"">") | Out-Null;
	
	#Add a report header to the table
	$local:BuildResults.Append("<tr>") | Out-Null;
	$local:BuildResults.AppendFormat("<td colspan=""4"" style=""text-align:center""><h1>{0}</h1></td>",$local:ReportTitle) | Out-Null;
	$local:BuildResults.Append("</tr>") | Out-Null;

	$local:BuildResults.Append("<tr>") | Out-Null;
	$local:BuildResults.AppendFormat("<td colspan=""4"" style=""text-align:center""><h2>Version: {0}, Branch: {1}</h2></td>",$local:Version,$local:Branch) | Out-Null;
	$local:BuildResults.Append("</tr>") | Out-Null;

	$local:BuildResults.Append("<tr>") | Out-Null;
	$local:BuildResults.AppendFormat("<td colspan=""4"" style=""text-align:center""><h3>Build Number {0}</h3></td>",$local:BuildNumber) | Out-Null;
	$local:BuildResults.Append("</tr>") | Out-Null;
	
	$local:BuildResults.Append("<tr>") | Out-Null;
	$local:BuildResults.AppendFormat("<td colspan=""4"" style=""text-align:center""><h3>Build completed at {0}</h3></td>",(Get-Date -Format "MM/dd/yyyy HH:mm:ss")) | Out-Null;
	$local:BuildResults.Append("</tr>") | Out-Null;

	$local:BuildResults.Append("<table border=""1"" style=""width:100%"">") | Out-Null;
}

################################################################################
# Add-HTMLNotification-CommitComments
# Add the commit comments to the HTML report
################################################################################
function Add-HTMLNotification-CommitComments([System.Text.StringBuilder]$local:BuildResults,[string]$local:CommitComments,[string]$local:SHA1)
{
	$local:BuildResults.AppendFormat("<tr ><td colspan=""2"" align=""center"" bgcolor=""#FFFFB0""><b>Commit</b><br>{0}</td><td colspan=""2"" bgcolor=""#FFFFB0"">{1}</td></tr>",$local:SHA1,$local:CommitComments) | Out-Null;
}

################################################################################
# Add-HTMLNotification-CommitComments
# Add the commit comments to the HTML report
################################################################################
function Add-HTMLNotification-FileNameRow([System.Text.StringBuilder]$local:BuildResults)
{
	$local:BuildResults.AppendFormat("<tr><td colspan=""2"" align=""right""><br><b>Affected File(s)</b></td><td colspan=""2"">") | Out-Null;
}

################################################################################
# Add-HTMLNotification-CommitComments
# Add the filename to the HTML report
################################################################################
function Add-HTMLNotification-FileName($local:BuildResults,[string]$local:FileName)
{
	$local:BuildResults.AppendFormat("{0}<br />",$local:FileName) | Out-Null;
}

################################################################################
# Close-HTMLNotification-FileNameRow
# Close out the file row HTML report
################################################################################
function Close-HTMLNotification-FileNameRow([System.Text.StringBuilder]$local:BuildResults)
{
	$local:BuildResults.AppendFormat("</td></tr>")  | Out-Null;
}

################################################################################
# Add-HTMLNotification-Issue
# Add commit info to the HTML report
################################################################################
function Add-HTMLNotification-Issue([System.Text.StringBuilder]$local:BuildResults,[int]$local:IssueID,[string]$local:IssueTitle)
{
	$local:BuildResults.AppendFormat("<tr><td colspan=""2"" align=""right"">{0}</td><td colspan=""2"">{1}</td></tr>", $local:IssueID, $local:IssueTitle) | Out-Null;
}

################################################################################
# Get-PTIssueInfoLocal
# Get the issue info from Pivotal Tracker
################################################################################
function Get-PTIssueInfoLocal([int]$local:IssueID)
{
	$local:Story = Get-PTStory $local:IssueID;
	$local:Title = $local:Story.name;
	return $local:Title;
}

################################################################################
# Test-Create-Folder
# Create a folder if it does not exist
################################################################################
function Test-Create-Folder($local:FolderName)
{
	if( !(Test-Path $local:FolderName) )
	{
		New-Item $local:FolderName -type directory | Out-Null;
	}
}

################################################################################
# Format-SQLFile 
# Call the format SQL PoSH and copy to the final formatted folder.
################################################################################
function Format-SQLFile([string] $local:SourceFile,[string] $local:StagingFile,[string] $local:BuildNumber,[int] $local:IssueID,[string] $local:SHA1)
{
	$local:SourceFileName = [System.IO.Path]::GetFileName($local:SourceFile)
	$local:SourceFolder = [System.IO.Path]::GetDirectoryName($local:SourceFile)
	$local:StagingFileName = [System.IO.Path]::GetFileName($local:StagingFile)
	$local:StagingFolder = [System.IO.Path]::GetDirectoryName($local:StagingFile)
	&$PSScriptRoot\CreateReleaseScriptV3.PS1 -sourcefile "$local:SourceFileName" -sourcefolder "$local:SourceFolder" -destinationFile "$local:StagingFileName" -destinationFolder "$local:StagingFolder" -Schema $local:Schema -IssueID $local:IssueID -BuildNumber $local:BuildNumber -CreateBackup 0 -ChangeSetID $local:SHA1;
}

################################################################################
# Format-XMLFile 
# Add attriutes to a XML file and copy from the build folder to the final formatted folder.
################################################################################
function Format-XMLFile([string] $local:SourceFileName,[string] $local:StagingFileName, [string] $local:BuildNumber,[int] $local:IssueID,[string] $local:SHA1)
{
	$local:XML = New-Object "System.Xml.XmlDocument";
	$local:XML.Load($local:SourceFileName);
	$IssueAttribute = $XML.CreateAttribute("IssueID");
	$IssueAttribute.Value = $local:IssueID;
	$local:XML.FirstChild.Attributes.Append($IssueAttribute) | Out-Null;
	$local:BuildAttribute = $local:XML.CreateAttribute("BuildNumber");
	$local:BuildAttribute.Value = $local:BuildNumber;
	$local:XML.FirstChild.Attributes.Append($local:BuildAttribute) | Out-Null;
	$ChangeSetAttribute = $XML.CreateAttribute("ChangeID");
	$ChangeSetAttribute.Value = $local:SHA1;
	$XML.FirstChild.Attributes.Append($ChangeSetAttribute) | Out-Null;
	$local:ScriptNameAttribute = $local:XML.CreateAttribute("ScriptName");
	$local:ScriptNameAttribute.Value = [System.IO.Path]::GetFileName($local:SourceFileName);
	$local:XML.FirstChild.Attributes.Append($local:ScriptNameAttribute) | Out-Null;
	$local:XML.Save($local:StagingFileName);
}

################################################################################
# ProcessCommit
# Process the files in a commmit
################################################################################
function ProcessCommit([string]$local:WorkspaceFolder,[string]$local:DatabaseFolderName,[string] $local:BuildNumber,$local:Commit,[System.Text.StringBuilder]$local:ReportBody)
{
	#Array for the commit list
	$local:FileNameList = New-Object System.Collections.ArrayList;
	
	Get-FileNamesFromSHA1 ([ref] $local:FileNameList) $local:Commit.SHA1;
	
	$local:FileNameAdded = $false;
	if( $local:FileNameList.Count -gt 0 )
	{
		Write-AppConsole ([string]::Format("Processing commit {0} with {1} file(s).",$local:Commit.SHA1,$local:FileNameList.Count));
		foreach( $local:FileName in $local:FileNameList)
		{
			#No need to deal with the database testing application
			if( $local:FileName.ToUpper().Contains("DATABASETEST") )
			{
				continue;
			}
			#Ignore the build support folder
			if( [System.IO.Path]::GetDirectoryName($local:FileName).ToUpper().Contains("BUILDSUPPORT") )
			{
				continue;
			}
			#$local:StagingFileName = $local:FileName.Replace($local:DatabaseFolderName,"Build");
			$local:StagingFileName = $local:FileName -replace ([string]::Format("^(.*?)\b{0}\b",$local:DatabaseFolderName)) ,"Build";
			$local:SourceFile = Join-Path -Path $local:WorkspaceFolder -ChildPath $local:FileName;
			$local:StagingFile = Join-Path -Path $local:WorkspaceFolder -ChildPath $StagingFileName;
			Test-Create-Folder ([System.IO.Path]::GetDirectoryName($local:StagingFile));
			
			#skip if the file is not there, this happens when a file is delete from the repo,
			#the file name still exists in the commit
			if( !(Test-Path $local:SourceFile) )
			{
				continue;
			}

			$local:IssueIDs = New-Object System.Collections.ArrayList;
			[string] $local:SystemType="";
			$local:CommitText = Get-IssueIDsFromSHA1 $local:Commit.SHA1 ([ref] $local:SystemType) ([ref] $local:IssueIDs);
			#Remove merged commits from process.
			if( $local:CommitText.ToUpper().StartsWith("MERGE") )
			{
				continue;
			}

			if( $local:IssueIDs.Count -gt 0 )
			{
				$local:IssueID = $local:IssueIDs[0];
				$local:IssueTitle = Get-PTIssueInfoLocal $local:IssueID;
			}
			else 
			{
				$local:IssueID = -1;
				$local:IssueTitle = "Story ID not linked";
			}

			#Add the file to the HTML report
			if( !$local:FileNameAdded )
			{
				Add-HTMLNotification-CommitComments $local:ReportBody $local:CommitText $local:Commit.SHA1;
				if( $local:IssueID -ne -1 ) 
				{
					Add-HTMLNotification-Issue $local:ReportBody $local:IssueID $local:IssueTitle;
				}
				Add-HTMLNotification-FileNameRow $local:ReportBody;
				$local:FileNameAdded = $true;
			}			
			Add-HTMLNotification-FileName $local:ReportBody $local:FileName;
			
			switch( [System.IO.Path]::GetExtension($local:FileName).ToUpper() )
			{
				{@(".CONFIG",".DTSX",".RDL",".PS1") -contains $_}
				{
					Write-AppConsole ([string]::Format("Copying {0}",[System.IO.Path]::GetFileName($local:FileName)));
					Copy-Item "$local:SourceFile" "$local:StagingFile" -Force | Out-Null;
					break;
				}
				".XML"
				{
					Write-AppConsole ([string]::Format("Formatting {0}",[System.IO.Path]::GetFileName($local:FileName)));
					Format-XMLFile "$local:SourceFile" "$local:StagingFile" $local:BuildNumber $local:IssueID $local:Commit.SHA1;
					break;
				}
				default
				{
					Write-AppConsole ([string]::Format("Formatting {0}",[System.IO.Path]::GetFileName($local:FileName)));
					Format-SQLFile "$local:SourceFile" "$local:StagingFile" $local:BuildNumber $local:IssueID $local:SHA1;
					Write-AppConsole "default";
					break;
				}
			}
		}
	}
	Close-HTMLNotification-FileNameRow $local:ReportBody;
}

################################################################################
# ProcessCommits
# Get a list of commints in the database folder and process them.
################################################################################
function ProcessCommits([string] $local:WorkspaceFolder,[string] $local:DatabaseFolder,[string] $local:BuildNumber,[string] $local:SinceCommit,[string]$local:SharePointVar,[string] $local:ProjectConfigFile,[string] $local:Version,[string] $local:Branch)
{
	$local:ReportHTML = New-Object System.Text.StringBuilder; 
	$local:ReportBody = New-Object System.Text.StringBuilder;
	
	$local:CommitList = Get-BuildListByPath $DatabaseFolder $local:SinceCommit $true $true;

	if( $local:CommitList.Count -gt 0 )
	{
		foreach( $local:Commit in $local:CommitList )
		{
			ProcessCommit $local:WorkspaceFolder $local:DatabaseFolder $local:BuildNumber $local:Commit $local:ReportBody;
		}

		#Load the project config XML
		$local:ProjectConfigXML = New-Object "System.Xml.XmlDocument";
		$local:ProjectConfigXML.Load($local:ProjectConfigFile);
		
		
		Add-HTMLNotification-Header $local:ReportHTML $local:ProjectConfigXML.BuildConfig.Report.Title $local:BuildNumber $local:Version $local:Branch;
		Write-AppConsole ([string]::Format("Saving Build Report to {0}",$local:SharePointVar));
		$local:ReportHTML.Append($local:ReportBody);
		$local:ReportHTML.ToString() | Set-Content (Join-Path -Path $local:WorkspaceFolder -ChildPath ($local:BuildNumber + ".html")) | Out-Null;
		
		
		$local:NotificationXML=[string]::Format("<SharePointNotification><ProjectName>{0}</ProjectName><Version>{1}</Version><Branch>{2}</Branch><NotificationFile>{3}</NotificationFile></SharePointNotification>",
			$local:ProjectConfigXML.BuildConfig.Report.SharePoint.ProjectName,
			$local:Version,
			$local:Branch,
			(Join-Path -Path $local:WorkspaceFolder -ChildPath ($local:BuildNumber + ".html"))
			);
		[Environment]::SetEnvironmentVariable($local:SharePointVar, $local:NotificationXML, "User");
		
	}
}

################################################################################
#
# Main
#
################################################################################
cls
$StopWatch = [System.Diagnostics.Stopwatch]::StartNew();

if( $SinceCommit.Length -eq 0 -or $SinceCommit -eq $null )
{
	$SinceCommit = $Env:GIT_PREVIOUS_SUCCESSFUL_COMMIT;
}

Write-AppConsole ([string]::Format("Build Number           : {0}",$BuildNumber));
Write-AppConsole ([string]::Format("Version                : {0}",$Version));
Write-AppConsole ([string]::Format("Branch                 : {0}",$Branch));
Write-AppConsole ([string]::Format("Workspace folder       : {0}",$WorkspaceFolder));
Write-AppConsole ([string]::Format("Database folder        : {0}",$DatabaseFolder));
Write-AppConsole ([string]::Format("Project Config File    : {0}",$ProjectConfigFile));
Write-AppConsole ([string]::Format("Share Point Var        : {0}",$SharePointVar));
Write-AppConsole ([string]::Format("Building since commit  : {0}",$SinceCommit));

#Create a temp directory to be used for the builds
$TempDirectory = Join-Path -Path $WorkspaceFolder -ChildPath "Build";
Write-AppConsole ([string]::Format("Created staging folder : {0}",$TempDirectory));
New-Item $TempDirectory -type directory | Out-Null;

ProcessCommits $WorkspaceFolder $DatabaseFolder $BuildNumber $local:SinceCommit $SharePointVar $ProjectConfigFile $Version $Branch;

$StopWatch.Stop();
$TimeSpan = [TimeSpan]::FromMilliseconds($StopWatch.ElapsedMilliseconds);
Write-AppConsole ([string]::Format("Completed in {0:00}:{1:00}:{2:00}.{3:000}",$TimeSpan.Hours,$TimeSpan.Minutes,$TimeSpan.Seconds,$TimeSpan.Milliseconds));