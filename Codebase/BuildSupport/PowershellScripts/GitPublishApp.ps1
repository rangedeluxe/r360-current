param 
(
	[parameter(Mandatory = $true)][string] $BuildNumber = "",
	[parameter(Mandatory = $true)][string] $BinSource = "",
	[parameter(Mandatory = $true)][string] $ConfigFile = "",
	[parameter(Mandatory = $true)][string] $DeploymentMap = "",
	[parameter(Mandatory = $true)][string] $WorkspaceFolder = "",
    [string] $HotFixBranch = "",
	[string] $Version= "Current",
    [string] $Branch = "QA",
    [string] $ReleaseConfigPath,
    [bool] $DeployJFrog = $true,
    [string] $JFrogApiKey = $ENV:JenkinsDeploySupportArtifactoryAPIToken,
    [string] $JFrogProj = "tmsa.r360",
    [string] $JFrogBranch = "dev"
)

$ScriptName = "GitPublishApp";
$ScriptVerison = "1.06";
#$ErrorActionPreference = "Stop"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

################################################################################
# 07/18/2016 WI XXXXXX JPB	1.0	Created.
# 12/08/2016 PT #134820329 CJ 1.01	Fail build if dir/file does not exist.
# 08/16/2016 PT #138525851 JPB	1.02 Added framework verisons 4.5.1, 4.5.2 and 4.6.1
# 04/04/2018 PT #156018711 JPB 	1.04 Changes to support Database deployment maps.
#								Merged Brandon's JFrog (Artifactory) changes in.
# 05/02/2018 PT #156853484 JPB	1.04.01 Added support for version number to come from
#								config file for publish path.
# 09/04/2018 PT #159747137 JPB	1.05 Get config file from build support folder.
# 09/24/2018 PT #158427298 JPB 	1.06 Read the release config from the json file
################################################################################


################################################################################
# Import modules needed for this script.
################################################################################
Import-Module -Name (Join-Path $PSScriptRoot "PivotalTrackerLibrary") -Force;

################################################################################
# Write-AppConsole
# Process/copy destination folders
################################################################################
function Write-AppConsole([String] $local:ConsoleInformation)
{
	begin
	{
	}
	process
	{
		if( $_ -ne $null )
		{
			Write-Host ([string]::Format("{0}[{1}] {2}",$ScriptName,$ScriptVerison,$_));
		}
	}
	end
	{
		Write-Host ([string]::Format("{0}[{1}] {2}",$ScriptName,$ScriptVerison,$local:ConsoleInformation));
	}
}

################################################################################
# Publish a File to JFrog Artifactory
################################################################################
function JFrog-File($local:filepath,$local:destpath,$local:version) {
    $namewithverison = $local:destpath
    $location = "http://artifactory.deluxe.com/" + $script:JFrogProj + "/$JFrogBranch/" + $namewithverison
    $uri = new-object system.uri($location)
    $authheader = @{"X-JFrog-Art-Api"=$script:JFrogApiKey}    
    $content = invoke-webrequest -uri $uri -infile $local:filepath -method put -headers $authheader -contenttype "application/json"
}



################################################################################
# Copy-Build
# Process/copy destination folders
################################################################################
function Copy-Build($local:TempDirectory, $local:PublishDir) 
{
	Write-AppConsole ([string]::Format("Publishing to  : {0}",$local:PublishDir));
	$local:SourcePath = Join-Path $local:TempDirectory "*"
	$local:TargetPath = $local:PublishDir;
	New-Item $local:TargetPath -Type Directory -Force | Out-Null;
	Copy-Item $local:SourcePath $local:TargetPath -Recurse -Force;
    if ($script:DeployJFrog -eq $true) {
        Write-AppConsole "JFrog Enabled, Publishing to Artifactory..."
        $absroot = $local:TempDirectory
        Write-AppConsole $absroot
        $allfiles = Get-ChildItem $local:TempDirectory -Recurse -Force | Where { ! $_.PSIsContainer }
        $allfiles | % { 
            $relpath = $_.FullName.Substring($absroot.Length + 1)
            $abspath = $_.FullName
            Write-AppConsole ("JFrog Publishing File : " + $relpath)
            JFrog-File $abspath $relpath $script:BuildNumber
        }
    }
}

################################################################################
# Copy-DestinationDirectory
# Process/copy destination directory
################################################################################
function Copy-DestinationDirectory([System.Xml.XmlElement] $local:Destination, [string] $local:Recursive, [string] $local:SourceDir,[array] $local:DeployDirs) 
{
    $local:SourcePath = Join-Path $local:SourceDir "*";
    $local:IncludePat = @();
    $local:ExcludePat = @();
    foreach ($local:Filter in $local:Destination.Filter) 
	{
        if ($local:Filter.Pattern -ne $null) 
		{
            $local:IncludePat += $local:Filter.Pattern;
        }
    }
    foreach ($local:Filter in $local:Destination.Exclude) 
	{
        if ($local:Filter.Pattern -ne $null) {
            $local:excludePat += $local:Filter.Pattern;
        }
    }
	
    if ($local:IncludePat.Count -ne 0) 
	{
        foreach ($local:DeployDir in $local:DeployDirs) 
		{
            $local:FoundCount=0;
            $local:DestDir = Join-Path $local:DeployDir $local:Destination.Path;
            New-Folder $local:DestDir;
            if ([bool] $local:Recursive -ne $null -and $local:Recursive.Length -gt 0 ) 
			{
                foreach ($local:CurSubDir in (Get-ChildItem -Path $local:SourceDir -Recurse | ?{$_.PSIsContainer})) 
				{
                    $local:CurDest = (Join-Path $local:DestDir ($local:CurSubDir.FullName -replace ("^" + [regex]::escape([string] $local:SourceDir)), ""));
                    New-Folder $local:CurDest;
                    [PSObject[]] $local:FilesFoundFromTo = FilePathsFromIncludesExcludes $local:IncludePat $local:ExcludePat $local:CurSubDir.FullName $true;
                    $local:FoundCount+=$local:FilesFoundFromTo.Count;
                    $local:FilesFoundFromTo | ? { $_ } | % {
                        $finalpath = (Join-Path $local:CurDest $_.destFilename)
                        Copy-Item -Path $_.sourcePath -destination $finalpath -Force
                    };
                }
            }
            FilePathsFromIncludesExcludes $local:IncludePat $local:ExcludePat $local:SourceDir ($local:FoundCount -gt 0 )|% {Copy-Item -Path $_.sourcePath -destination (Join-Path $local:DestDir $_.destFilename) -Force};
        }
    }
}

################################################################################
# Copy-DestinationDirectories
# Process/copy destination directories
################################################################################
function Copy-DestinationDirectories([string]$local:ModuleName,$local:Destinations,[string] $local:Recursive,[string] $local:SourceDir,[string] $local:OutputDirectory) 
{
    foreach( $local:Destination in $local:Destinations )  
	{
        $local:ModuleDeployDirs = Get-ModuleDeployDirs $local:ModuleName $local:OutputDirectory (Get-Tier $local:Destination)
        Copy-DestinationDirectory $local:Destination $local:Recursive $local:SourceDir $local:ModuleDeployDirs;
    }
}

################################################################################
# FilePathsFromFilter
# Gets the paths for the files that fit the filter and gives destination name
################################################################################
function FilePathsFromFilter([System.Xml.XmlElement] $local:Filter,[string] $local:SourceDir)
{
    $local:files = Get-ChildItem -Path $local:SourceDir -Filter $local:Filter.Pattern|
        % {
            $local:DestName=$_.Name;
            if($local:Filter.Rename -ne $null) 
            {
                $local:DestName=$local:Filter.Rename -replace '<file>',$_.Name;
            }
            [PSCustomObject]@{
                sourcePath = $_.FullName;
                destFilename = $local:DestName;
            }
          };

#    if($local:files.Count -eq 0) 
#    {
#        throw New-Object System.Exception("No files found for filter pattern: $($local:Filter.Pattern) in path: $local:SourceDir");
#    }
    return $local:files;
}

################################################################################
# FilePathsFromIncludesExcludes
# Gets the paths for the files that fit the filter and gives destination name
################################################################################
function FilePathsFromIncludesExcludes([string[]] $local:includePats, [string[]] $local:excludePats, [string] $local:SourceDir, [bool]$local:Optional=$false)
{
    $local:files = Get-ChildItem -File -Path $(Join-Path $local:SourceDir "*") -Include $local:includePats -Exclude $local:excludePats|% {
        [PSCustomObject]@{
            sourcePath = $_.FullName;
            destFilename = $_.Name;
        }
    };

    if($local:files.Count -eq 0 -and !$local:Optional) 
    {
        throw New-Object System.Exception("No files found in path: $local:SourceDir that fit patterns: $($local:includePats -join ", ") excluding patterns: $($local:excludePats -join ", ")");
    }
    return $local:files;
}

################################################################################
# Copy-TopLevelFilter
# Copy file using top level filter
################################################################################
function Copy-TopLevelFilter([System.Xml.XmlElement] $local:Filter,[string] $local:SourceDir,[array] $local:DeployDirs) 
{
    $local:SourcePath = Join-Path $local:SourceDir "*";
    if( $local:Filter -ne $null )
	{
		foreach ($local:DeployDir in $local:DeployDirs) 
		{
			$local:DestDir = Join-Path $local:DeployDir $local:Filter.DestPath;
			New-Folder $local:DestDir;
            FilePathsFromFilter $local:Filter $local:SourceDir | % {
                $finalpath = (Join-Path $local:DestDir $_.destFilename)
                Copy-Item -Path $_.sourcePath -destination $finalpath -Force
            };
		}
    }
}

################################################################################
# Copy-TopLevelFilters
# Process/copy top level filters
################################################################################
function Copy-TopLevelFilters([string]$local:ModuleName,[System.Xml.XmlElement] $local:Source,[string] $local:SourceDir,[string] $local:OutputDirectory) 
{
	#Database build will not have all folders listed in the deployment map
	if( Test-Path $local:SourceDir)
	{
	    foreach ($local:Filter in $local:Source.Filter) 
		{
	        $local:ModuleDeployDirs = Get-ModuleDeployDirs $local:ModuleName $local:OutputDirectory (Get-Tier $local:Filter)
	        Copy-TopLevelFilter $local:Filter $local:SourceDir $local:ModuleDeployDirs
	    }
	}
}

################################################################################
# Get-ModuleDeployDirs
# Get a module's deployment folder info
################################################################################
function Get-ModuleDeployDirs([string]$local:ModuleName,[string] $local:OutputDirectory,[string] $local:TierList) 
{
	if( $PublishFolder.ToUpper().Contains("[@TIER]") )
	{
	    if ($local:TierList) 
		{
            $local:Tiers = $local:TierList.Split(',') | ForEach-Object {
                $TierName = $_.Trim();
                $TierPath = $PublishFolder.Replace("[@TIER]", $TierName);
                $TierPath
            }
	    } 
		else 
		{
	        $local:Tiers = @("Other\$ModuleName")
	    }
	}
	else 
	{
		$local:Tiers = $PublishFolder;
	}
    	$local:ModuleDeployDirs = $local:Tiers | %{
        	Join-Path $local:OutputDirectory $_
    }
    return $local:ModuleDeployDirs;
}

################################################################################
# Get-TempDirectory
# Get temp folder and add unique ID
################################################################################
function Get-TempDirectory([string] $local:UniqueId) 
{
    # Expand the path because of known PowerShell bug with DOS short filenames:
    # https://connect.microsoft.com/PowerShell/feedbackdetail/view/874645/dos-style-8-3-shortnames-that-resolve-to-actual-shorter-long-names-results-in-an-erroneous-itemnotfoundexception
    $local:TempDirectory = [System.IO.Path]::GetFullPath($env:Tmp)
    return Join-Path $local:TempDirectory "BuildTemp.$UniqueId"
}

################################################################################
# Get-Module
# Get module file belongs to
################################################################################
function Get-Module([System.Xml.XmlElement] $local:Node) 
{
    $local:ModuleName = "unknown";
    while( $local:Node -and !($local:Node.LocalName -eq "module") -and $local:Node.ParentNode -isnot [System.Xml.XmlDocument]) 
	{
        $local:Node = $local:Node.ParentNode;
    }
    if($local:Node.LocalName -like "module") 
    {
        $local:ModuleName = $local:Node.Name;
    }
    return $local:ModuleName
}

################################################################################
# Get-Tier
# Get tier file belongs to
################################################################################
function Get-Tier([System.Xml.XmlElement] $local:Node) 
{
    $local:CurrentNode = $local:Node;
    while( $local:CurrentNode -and !($local:CurrentNode.Tier)) 
	{
        $local:CurrentNode = $local:CurrentNode.ParentNode;
    }
    if($local:CurrentNode.Tier) 
    {
        $local:Tier = $local:CurrentNode.Tier;
    }
    else 
    {
        $local:Tier = "Other\$(Get-Module $local:Node)";
    }
    return $local:Tier
}

################################################################################
# Merge-Assemblies
# Process all assmeblies that need to be merge into the output file
################################################################################
function Merge-Assemblies([xml] $local:ConfigXML,[string] $local:ModuleName,$local:Merges,[string] $local:BinSource,[string] $local:SourceDir,[string] $local:OutputDirectory,[string] $local:WorkspaceFolder) 
{
    foreach($local:Merge in $local:Merges) 
	{
        $ModuleDeployDirs = Get-ModuleDeployDirs $local:ModuleName $local:OutputDirectory (Get-Tier $local:Merge)
        Merge-Assembly $local:ConfigXML $local:Merge $local:BinSource $local:SourceDir $local:ModuleDeployDirs $local:WorkspaceFolder;
    }
}

################################################################################
# Merge-Assembly
# Merge assembly by callign ILMerge
################################################################################
function Merge-Assembly([xml]$local:ConfigXML,[System.Xml.XmlElement] $local:MergeXML,[string] $local:BinSource,[string] $local:SourceDir,[array] $local:DeployDirs,[string] $local:WorkspaceFolder)
{
    $local:SourcePath = Join-Path $local:SourceDir "*";
    $local:MergeFiles = @();
    $local:MergeSign = "";
    $local:MergePlatform = "v4,C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319";
	
    if($local:MergeXML.KeyFile) 
	{
        $local:MergeSign = "/keyfile:""" + (Join-Path $local:BinSource $local:MergeXML.KeyFile) + """";
    }

    if($local:MergeXML.Framework) 
	{
		switch( $local:MergeXML.Framework )
		{
        	"4.0"
			{
	            $local:MergePlatform = "v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0";
	        }
			"4.5"
			{
	            $local:MergePlatform = "v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5";
			}
			"4.5.1"
			{
	            $local:MergePlatform = "v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5.1";
			}
			"4.5.2"
			{
	            $local:MergePlatform = "v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5.2";
			}
        	"4.6"
			{
	            $local:MergePlatform = "v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6";
	        }
        	"4.6.1"
			{
	            $local:MergePlatform = "v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.6.1";
	        }
		}
    }


    if( $local:MergeXML -ne $null ) 
	{
        foreach( $local:Filter in $local:MergeXML.Filter ) 
		{
            if($local:Filter -ne $null) 
			{
                $local:FilterPattern = $local:Filter.Pattern;
                $local:PatternPath = Join-Path $local:SourceDir $local:FilterPattern;
                if(-Not (Test-Path $local:PatternPath))
                {
                    throw New-Object System.Exception("No files matching the specified ILMerge file pattern ($local:FilterPattern) were found.");
                }

               	foreach($local:FileFromTo in FilePathsFromFilter $local:Filter $local:SourcePath) 
				{
                    $local:MergeFiles += $local:FileFromTo.sourcePath;
                }
            }
        }

        $local:MergeList = """" + ($local:MergeFiles -Join """ """) + """";
        $local:TempDir = Join-Path $local:SourceDir "Temp";
        $local:TempFile = Join-Path $local:TempDir (split-path $local:MergeXML.DestFile -Leaf);

        New-Folder $local:TempDir $True;

        if($local:MergeFiles.Count -ne 0) 
		{
            $local:Args = """/targetplatform:$local:MergePlatform"" /ndebug /closed $local:MergeList $local:MergeSign /out:""$local:TempFile"" /allowDup /log";
            $local:cmd = "& ""$($local:ConfigXML.Config.Tools.ILMerge)"" $local:Args";
            Invoke-Expression "$local:cmd 2>&1"|%{Write-Host("[IL Merge]  $_")};
            if($local:MergeXML.KeyFile) 
			{
                $loal:oProc = Start-Process $local:ConfigXML.Config.Tools.sn -Args ("-R ""$local:TempFile""" + $local:MergeXML.KeyFile + """") -PassThru;
                Wait-Process -inputObject $local:oProc;
            }
            if ((Test-Path $local:TempFile) -and ((Get-Item $local:TempFile).Length -gt 0)) 
			{
                foreach( $local:DeployDir in $local:DeployDirs ) 
				{
                    $local:DestFile = Join-Path $local:DeployDir $local:MergeXML.DestFile;
                    New-Folder (Split-Path -parent $local:DestFile);
                    Copy-Item -Path $local:TempFile -Destination $local:DestFile;
                }
                Write-AppConsole "ILMerge Completed Successfully";
            }
            else 
			{
                throw New-Object System.Exception("IL Merge Failed")
            }
        }
    }
}

################################################################################
# New-BuildTempDirectory
# Create a new build temp directory
################################################################################
function New-BuildModule([xml] $local:ConfigXML,[System.Xml.XmlElement] $local:PublishMap, [string] $local:BinSource, [string] $local:OutputDirectory,[string] $local:WorkspaceFolder)
{

    Write-AppConsole ([string]::Format("Building temp directory: {0}",$local:OutputDirectory));

	foreach( $local:Module in $local:PublishMap.Module )
	{
		Write-AppConsole ([string]::Format("Building module {0} for tier {1} ",$local:Module.Name,$local:Module.Tier));
		foreach( $local:Source in $local:Module.Source )
		{
			$local:SourceDir = [System.IO.Path]::GetFullPath((Join-Path $local:BinSource $local:Source.BuildDir));
	        Copy-TopLevelFilters $local:ModuleName $local:Source $local:SourceDir $local:OutputDirectory;
	        Copy-DestinationDirectories $local:ModuleName $local:Source.Dest $local:Source.Recursive $local:SourceDir $local:OutputDirectory;
	        Merge-Assemblies $local:ConfigXML $local:ModuleName $local:Source.Merge $local:BinSource $local:SourceDir $local:OutputDirectory $local:WorkspaceFolder;
		}
	}
    Get-ChildItem $local:OutputDirectory -File -Recurse | ?{
        $_.IsReadOnly
    } | %{
        Write-AppConsole "Clearing read-only flag: $_"
        $_.IsReadOnly = $False
    }
    Write-AppConsole ([string]::Format("Build Complete"));
}

################################################################################
# New-Folder
# Create a new folder if it does not exist, clean if requested
################################################################################
function New-Folder([string] $local:Path,[bool] $local:CleanPath = $False)
{
    if( !(Test-Path -Path $local:Path) ) 
	{
        New-Item $local:Path -type directory | Out-Null;
    }
    else 
	{
        if( $local:CleanPath ) 
		{
            Remove-Item -Recurse -Force (Join-Path $local:Path "*")|Out-Null;
        }
    }
}

################################################################################
# New-TempDirectory
# Create a new temp directory
################################################################################
function New-TempDirectory 
{
    $local:Directory = Get-TempDirectory ([System.Guid]::NewGuid())
    New-Item $local:Directory -Type Directory | Out-Null
    return $local:Directory
}

################################################################################
# Main
################################################################################
cls
$StopWatch = [System.Diagnostics.Stopwatch]::StartNew();

#Setup var to hold path replacement variables
$PathDictionary = @{};

#Combine the workspace folder to get a full path
$BinSource = Join-Path -Path $WorkspaceFolder -ChildPath $BinSource;
$DeploymentMap = Join-Path -Path $WorkspaceFolder -ChildPath $DeploymentMap

if( !(Test-Path $ConfigFile) )
{
	Write-AppConsole ([string]::Format("Cannot find config file {0}. Aborting build...",$ConfigFile)) -ForegroundColor Red;
	return 1;
}

if( !(Test-Path $DeploymentMap) )
{
	Write-AppConsole ([string]::Format("Cannot find deployment map {0}. Aborting build...",$DeploymentMap)) -ForegroundColor Red;
	return 1;
}

if( !(Test-Path $BinSource) )
{
	Write-AppConsole ([string]::Format("Cannot find bin folder {0}. Aborting build...",$BinSource)) -ForegroundColor Red;
	return 1;
}

#Load the config file
$ConfigXML = New-Object "System.Xml.XmlDocument";
$ConfigXML.Load($ConfigFile);


#Load the deployment file
$DeploymentMapXML = New-Object "System.Xml.XmlDocument";
$DeploymentMapXML.Load($DeploymentMap);

$BaseBuildFolderNode = $ConfigXML.Config.Output.SelectSingleNode("$Branch"+"BuildFolder");
$BaseBuildFolder = $BaseBuildFolderNode.InnerText;

if ($HotFixBranch.Length -ne 0 )
{
     $BaseBuildFolder += ("_" + $HotFixBranch);
}

$PathDictionary.Add("[@VERSION]",$Version);
$PathDictionary.Add("[@BRANCH]",$Branch);
$PathDictionary.Add("[@YEAR]",(Get-Date).year);

if( $BaseBuildFolder.ToUpper().Contains("[@SPRINT]") )
{
	$PTProjectInfo = Get-PTProjectInfo "R360 - Backlog";
	$PathDictionary.Add("[@SPRINT]",$PTProjectInfo.current_iteration_number);
}


$PublishFolder = $ConfigXML.Config.Output.PublishFolder;

#See if there is a Release.config in the folder where the deployment map is used. 
#If so, use that as the ReleaseConfigPath
if ($ReleaseConfigPath.Length -le 0 ) 
{
	$BuildSupportPath = [System.IO.Path]::GetDirectoryName($DeploymentMap);
	$ReleaseConfig = Join-Path -Path $BuildSupportPath -ChildPath "Release.json";
	if( (Test-Path $ReleaseConfig) )
	{
		$ReleaseConfigPath = $ReleaseConfig;
	}
}

#See if there is a Release.config in the folder where the deployment map is used. 
#If so, use that as the ReleaseConfigPath
if ($ReleaseConfigPath.Length -le 0 ) 
{
	$BuildSupportPath = [System.IO.Path]::GetDirectoryName($DeploymentMap);
	$ReleaseConfig = Join-Path -Path $BuildSupportPath -ChildPath "Release.json";
	if( (Test-Path $ReleaseConfig) )
	{
		$ReleaseConfigPath = $ReleaseConfig;
	}
}

if( !(Test-Path $ReleaseConfigPath) )
{
	Write-AppConsole ([string]::Format("Cannot find release config file {0}. Aborting build...",$ReleaseConfigPath)) -ForegroundColor Red;
	return 1;
}

if ($ReleaseConfigPath) 
{
	$ReleaseConfig = Get-Content -Raw -Path $ReleaseConfigPath | ConvertFrom-Json;
	$BranchConfig = $ReleaseConfig | Select-Object -ExpandProperty Branches | Where-Object Branch -eq $JFrogBranch

	$HotfixVersion = @(
	        $BranchConfig.Major
	        $BranchConfig.Minor
	        $BranchConfig.HotFix
	    ) -join ".";	
	
	$PathDictionary.Add("[@PRODUCTNAME]",$ReleaseConfig.ProductName);
	$PathDictionary.Add("[@RELEASEMAJOR]",$BranchConfig.Major);
	$PathDictionary.Add("[@RELEASEMINOR]",$BranchConfig.Minor);
	$PathDictionary.Add("[@RELEASEHOTFIX]",$BranchConfig.HotFix);
	$PathDictionary.Add("[@PATCH]",$BranchConfig.Patch);
	$PathDictionary.Add("[@MAJOR]",$BranchConfig.Major);
	$PathDictionary.Add("[@MINOR]",$BranchConfig.Minor);
	$PathDictionary.Add("[@HOTFIX]",$BranchConfig.HotFix);
}


#replace path variables with real values
foreach( $PathDictionaryKey in $PathDictionary.keys )
{
	$BaseBuildFolder = $BaseBuildFolder.Replace($PathDictionaryKey,$PathDictionary[$PathDictionaryKey]);
	$PublishFolder = $PublishFolder.Replace($PathDictionaryKey,$PathDictionary[$PathDictionaryKey]);
}


#write out what is happening
Write-AppConsole ([string]::Format("Build Number       : {0}",$BuildNumber));
Write-AppConsole ([string]::Format("Binaries Path      : {0}",$BinSource));
Write-AppConsole ([string]::Format("Deployment Map     : {0}",[System.IO.Path]::GetFileName($DeploymentMap)));
Write-AppConsole ([string]::Format("Build Base Path    : {0}",$BaseBuildFolder));
Write-AppConsole ([string]::Format("Publish Path       : {0}",$PublishFolder));
Write-AppConsole ([string]::Format("Publish to JFrog   : {0}",$DeployJFrog));
if ($HotfixVersion) {
	Write-AppConsole ([string]::Format("Hotfix Version     : {0}", $HotfixVersion));
}
Write-AppConsole ([string]::Format("Publish Path   : {0}",$PublishFolder));

#Create a temp directory to be used for the builds
#$TempDirectory = New-TempDirectory;
$TempDirectory = Join-Path $WorkspaceFolder "Staging";
if( !(Test-Path $TempDirectory) )
{
	Write-AppConsole ([string]::Format("Created working temp directory {0}",$TempDirectory));
	New-Item $TempDirectory -type directory | Out-Null;
}

#Create the build
New-BuildModule $ConfigXML $DeploymentMapXML.PublishMap $BinSource $TempDirectory $WorkspaceFolder;

Copy-Build $TempDirectory $BaseBuildFolder;


$StopWatch.Stop();
$TimeSpan = [TimeSpan]::FromMilliseconds($StopWatch.ElapsedMilliseconds);
Write-AppConsole ([string]::Format("Completed in {0:00}:{1:00}:{2:00}.{3:000}",$TimeSpan.Hours,$TimeSpan.Minutes,$TimeSpan.Seconds,$TimeSpan.Milliseconds));
