Param(
  [string[]]$versions = @(),
  [string]$apikey = $ENV:JenkinsDeploySupportArtifactoryAPIToken,
  [string]$branch = 'dev'
)

add-type -assembly system.io.compression.filesystem

# Change these based on your repository.
$R360_PROJECT = "tmsa.r360/$branch"
$SHARED_PROJECT = "tmsa.shared/$branch"
$REPOSITORY_ROOT = "https://artifactory.deluxe.com/api/storage/"
$DOWNLOAD_ROOT = "https://artifactory.deluxe.com:443/"
$API_ROOT = "http://artifactory.deluxe.com/"

# Do not change these.
$QUERY_FIND_FILES = "?list&deep=1&listFolders=0"

# This script reads from arifactory and produces a ZIP file containing everything the release needs.
# Just pass in the version, like 'RecHub2.03.03' (this needs to match up to the RecHub folder inside of artifactory)
# and the API user and Key to communicate with artifactory.

function jfrog($local:filepath, $local:destpath, $project) {
    $location = $API_ROOT + $project + "/" + $local:destpath
    $uri = new-object system.uri($location)
    $authheader = @{"X-JFrog-Art-Api"=$script:apikey}
    write-host "jfrog: uploading file $location"
	[System.Net.ServicePointManager]::MaxServicePointIdleTime = 216000
    $content = invoke-webrequest -UseBasicParsing -uri $uri -infile $local:filepath -method put -headers $authheader -contenttype "application/json" -TimeoutSec 216000
}

function get-json ($local:url) {
    $uri = new-object system.uri($local:url)
    $authheader = @{"X-JFrog-Art-Api"=$script:apikey}
    write-host "json: invoking:" $local:url
    return (invoke-webrequest -UseBasicParsing -uri $uri -method "get" -headers $authheader) | convertfrom-json
}

function get-file ($local:tier, $local:project, $local:url, $local:destfolder) {
    $uri = new-object system.uri($DOWNLOAD_ROOT + $local:project + $local:tier + $local:url)
    $authheader = @{"X-JFrog-Art-Api"=$script:apikey}
    write-host "downloading file:" $uri
    
    # make sure the path exists.
    $path = join-path $local:destfolder $local:url
    $dir = split-path $path
    if (!(test-path -path $dir)) {
        new-item -itemtype "directory" -path $dir
    }

    invoke-webrequest -UseBasicParsing -uri $uri -outfile $path -headers $authheader
}

function list-jfrog-folders ($local:subdir, $local:project) {
    $url = $REPOSITORY_ROOT + $local:project + $local:subdir
    $root = get-json $url
    return $root.children
}

function list-jfrog-files ($local:project, $local:subdir) {
    $url = $REPOSITORY_ROOT + $local:project + $local:subdir + "/" + $QUERY_FIND_FILES
    $files = get-json $url
    return $files
}

# Validation first.
if ($versions.length -eq 0 -or $apikey -eq "") {
    write-host "Error: -version and -apikey are both required."
    return 1;
}

# For each version!
$versions | % {
    $version = $_;
    $project = $R360_PROJECT
    if ($version -match "Framework" -or $version -match "RAAM") {
        $project = $SHARED_PROJECT
    }

    # Grab all of our tiers inside of the root. Should include folders like "WEB", "APP", etc.
    $tiers = (list-jfrog-folders "/" $project) | where { $_.folder -eq $true }

    # Create an output directory where all of our release files will go.
    $dirname = "Release-" + $version
    new-item $dirname -itemtype "directory"
    
    # Now we grab all of the files in the main tiers.
    $tiers | where { $_ -ne "Packages" } | % {
        $tier = $_.uri;
        write-host "gathering files for tier:" $tier
        $files = (list-jfrog-files $project $tier).files | where { ($_.uri.startswith("/" + $version + "/") -or $tier -eq '/Utilities') }
        write-host "downloading files for tier:" $tier
        $files | % {
            get-file $tier $project $_.uri ($dirname + $tier)
        }
    }
    
    # Move the deployment scripts to the release (parent) folder.
    $deploypath = $dirname + "/DEPLOY/" + $version + "/*"
    write-host "moving deployment utilities to the root"
    if ((test-path -path $deploypath)) {
        copy-item $deploypath $dirname -recurse -force
        write-host "deleting deploy folder"
        remove-item ($dirname + "/DEPLOY") -force -recurse
    }
    else {
        write-host "did not find any deloyment folder for this release"
    }
    
    # Now zip up the folder.
    write-host "zipping up release folder $dirname"
    $compressionLevel = [System.IO.Compression.CompressionLevel]::Optimal
    [System.IO.Compression.ZipFile]::CreateFromDirectory($dirname, "$dirname.zip", $compressionLevel, $false)
    
    # Frog up the release file.
    #write-host "frogging zip file"
    #jfrog ($dirname + ".zip") ("Packages/" + $dirname + ".zip") $project
    
    # Clean up the folder that we already zipped.
    #write-host "deleting temp release folder"
    #remove-item $dirname -force -recurse
    
    write-host "all donezo."
}
