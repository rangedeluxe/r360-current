#!groovy
def commonScripts
def jobItems
def createReleasePackage = "Create Release Package"
def packageReleaseNotes = "Package Release Notes"

@Library('common') _
pipeline {
    agent {
        // This is a label we added to 'mosqatfsbs03.wausaufs.nwk'.
        // This means we can run on any machine that has this label - currently only mosqatfsbs03.wausaufs.nwk.
		label 'QA_Pipeline'
	}
	parameters {
		gitParameter branchFilter: 'origin/(.*)', 
			sortMode: 'ASCENDING_SMART', 
			listSize: '10', 
			selectedValue: 'DEFAULT', 
			description: 'Which branch would you like to build?', 
			defaultValue: 'DEV', name: 'BranchToBuild', 
			type: 'PT_BRANCH', 
			useRepository: 'http://bitbucket.deluxe.com/scm/WFS/r360-current.git/'
		choice(choices: 
			[
				"${createReleasePackage}",
				"${packageReleaseNotes}"
			], description: 'What function would you like to perform?', name: 'Function')
	}
	environment {
		CustomBuildNumber = VersionNumber(projectStartDate: '1969-12-31', versionNumberString: '${BUILD_YEAR, XX}.${BUILD_MONTH, XX}.${BUILD_DAY, XX}.${BUILDS_TODAY, XXX}', versionPrefix: '', worstResultForIncrement: 'NOT_BUILT')
		JenkinsDeploySupportArtifactoryAPIToken = credentials('JenkinsDeploySupportArtifactoryAPIToken')
	}	
	options	{
		buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '100'))
		timestamps()
	}
	
	stages {
		stage('Job Startup') {
			steps { 
				echo "Running job startup step..."
				
				script {
					commonScripts = load "${WORKSPACE}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
					jobItems = commonScripts.setJobItems(WORKSPACE, params, CustomBuildNumber, JenkinsDeploySupportArtifactoryAPIToken)
					
					notify.buildNotification('r360', "started", false, "$JOB_NAME #${BUILD_NUMBER} Function: '$params.Function' Build ${jobItems.VersionToBuild} started for branch ${jobItems.BranchToBuild} (<a href=${BUILD_URL}>View build</a>)")
				}
				
				cleanWs()
			}
		}

		stage("Check out") {
			
			steps {
				checkout poll: false, 
				scm: [$class: 'GitSCM', 
					branches: [[name: "${jobItems.BranchToBuild}"]], 
					doGenerateSubmoduleConfigurations: false, 
					extensions: [
						[$class: 'CloneOption', timeout: 120],
						[$class: 'CleanBeforeCheckout'],
						[$class: 'SparseCheckoutPaths', 
							sparseCheckoutPaths:[
							]
						]
					],
					userRemoteConfigs: [[credentialsId: '0039de23-b91f-4f24-af68-81190d23a31b', url: 'http://bitbucket.deluxe.com/scm/WFS/r360-current.git/']],
					submoduleCfg: []
				]
			}
		}	

		stage("Create Release Package") {
			when {
				expression { params.Function == "${createReleasePackage}" }
			}
			steps {

				script {
					def buildScript = load "${WORKSPACE}\\Codebase\\BuildSupport\\GroovyScripts\\createRelease.groovy"
					buildScript.createReleasePackage(jobItems)
				}
				
			}
		}

		stage("Package Release Notes") {
			when {
				expression { params.Function == "${packageReleaseNotes}" }
			}
			steps {

				script {
					def buildScript = load "${WORKSPACE}\\Codebase\\BuildSupport\\GroovyScripts\\createRelease.groovy"
					buildScript.packageReleaseNotes(jobItems)
				}
				
			}
		}
	}
		
	post {
		failure {
			script {
				notify.buildNotification("r360", "failed", false, "${JOB_NAME} #${BUILD_NUMBER} Function: '$params.Function' Build ${jobItems.VersionToBuild} failed for branch ${jobItems.BranchToBuild}. Duration: ${currentBuild.durationString.replace(' and counting', '')} (<a href=${BUILD_URL}>View build</a>)")
			}
			
		}
		unstable {
			script {
				notify.buildNotification("r360", "unstable", false, "${JOB_NAME} #${BUILD_NUMBER} Function: '$params.Function'  Build ${jobItems.VersionToBuild} succeeded but one or more unit tests failed for branch ${jobItems.BranchToBuild}. Duration: ${currentBuild.durationString.replace(' and counting', '')} (<a href=${BUILD_URL}>View build</a>)")
			}
			
		}
		success {
			script {
				notify.buildNotification("r360", "succeeded", false, "${JOB_NAME} #${BUILD_NUMBER} Function: '$params.Function' Build ${jobItems.VersionToBuild} succeeded for branch ${jobItems.BranchToBuild}. Duration: ${currentBuild.durationString.replace(' and counting', '')} (<a href=${BUILD_URL}>View build</a>)")
			}
			
		}
	}
}