#!groovy
def commonScripts
def jobItems
def selectedApplication
def applicationInfo
def applicationNames
def modulesFile

@Library('common') _
pipeline {
    agent {
        // This is a label we added to 'mosqatfsbs03.wausaufs.nwk'.
        // This means we can run on any machine that has this label - currently only mosqatfsbs03.wausaufs.nwk.
		label 'QA_Pipeline'
	}
	
	parameters {
		
		gitParameter branchFilter: 'origin/(.*)', 
			sortMode: 'ASCENDING_SMART', 
			listSize: '10', 
			selectedValue: 'DEFAULT', 
			description: 'Which branch would you like to build?', 
			defaultValue: 'DEV', name: 'BranchToBuild', 
			type: 'PT_BRANCH', 
			useRepository: 'http://bitbucket.deluxe.com/scm/WFS/r360-current.git/'
		string(defaultValue: '', description: 'Override the version with whatever you want for this build (i.e. 1.00.00).  Leave it blank if you want it to default.', name: 'Version')
		booleanParam(defaultValue: true, description: 'Publish to artifactory (True/False)?', name: 'Publish')
	}
	environment {
		CustomBuildNumber = VersionNumber(projectStartDate: '1969-12-31', versionNumberString: '${BUILD_YEAR, XX}.${BUILD_MONTH, XX}.${BUILD_DAY, XX}.${BUILDS_TODAY, XXX}', versionPrefix: '', worstResultForIncrement: 'NOT_BUILT')
		JenkinsDeploySupportArtifactoryAPIToken = credentials('JenkinsDeploySupportArtifactoryAPIToken')
	}	
	options	{
		buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '100'))
		timestamps()
	}
	
	stages {
		stage('Job Startup') {
			steps { 
				echo "Running job startup step..."
				
				script {
					commonScripts = load "${WORKSPACE}\\Codebase\\BuildSupport\\GroovyScripts\\common.groovy"
					jobItems = commonScripts.setJobItems(WORKSPACE, params, CustomBuildNumber, JenkinsDeploySupportArtifactoryAPIToken)

					modulesFile = "${WORKSPACE}\\Codebase\\BuildSupport\\modules.json"
					applicationNames = commonScripts.getActiveApplicationNames(modulesFile)

					if (applicationNames.size() > 0) {
						def appList = ["Build All"] + applicationNames
						timeout(time: 60, unit: 'SECONDS') {							
							selectedApplication = input message: 'Please choose an application module to build', ok: 'Build',
								parameters: [choice(name: 'Application', choices: appList, 
								description: 'Which of the available application modules would you like to build?')]

							echo "The selected application is...${selectedApplication}"
						}

					}
					else {
						selectedApplication = "None"
						currentBuild.result = "ABORTED"
    					error('No Active applications to build.  Aborting here.')
    					echo 'Further code will not be executed'
					}
					
					notify.buildNotification('r360', "started", false, "$JOB_NAME #${BUILD_NUMBER} Application: '${selectedApplication}' Build ${jobItems.VersionToBuild} started for branch ${jobItems.BranchToBuild} (<a href=${BUILD_URL}>View build</a>)")
				}
				
				cleanWs()
			}
		}

		stage("Check out") {
			
			steps {
				checkout poll: false, 
				scm: [$class: 'GitSCM', 
					branches: [[name: "${jobItems.BranchToBuild}"]], 
					doGenerateSubmoduleConfigurations: false, 
					extensions: [
						[$class: 'CloneOption', timeout: 120],
						[$class: 'CleanBeforeCheckout'],
						[$class: 'SparseCheckoutPaths', 
							sparseCheckoutPaths:[
							]
						]
					],
					userRemoteConfigs: [[credentialsId: '0039de23-b91f-4f24-af68-81190d23a31b', url: 'http://bitbucket.deluxe.com/scm/WFS/r360-current.git/']],
					submoduleCfg: []
				]
			}
		}	

		stage ("Build Application Module") {
			steps {
				script {
					if (selectedApplication != "Build All") {
						applicationInfo = commonScripts.getApplicationInfo(selectedApplication, modulesFile)
						echo "=====>Building application '${applicationInfo.Name}'<====="

						def buildScript = load "${WORKSPACE}\\Codebase\\BuildSupport\\GroovyScripts\\${applicationInfo.Script}"
						buildScript.build(jobItems)
					}
					else {
						echo "Build All Selected"
						for (String applicationName :  applicationNames) {
							applicationInfo = commonScripts.getApplicationInfo(applicationName, modulesFile)

							echo "=====>Building application '${applicationInfo.Name}'<====="

							def buildScript = load "${WORKSPACE}\\Codebase\\BuildSupport\\GroovyScripts\\${applicationInfo.Script}"
							buildScript.build(jobItems)
						}
					}
				}
			}
		}
	}
	post {
		failure {
			script {
				notify.buildNotification("r360", "failed", false, "${JOB_NAME} #${BUILD_NUMBER} Application: '${selectedApplication}' Build ${jobItems.VersionToBuild} failed for branch ${jobItems.BranchToBuild}. Duration: ${currentBuild.durationString.replace(' and counting', '')} (<a href=${BUILD_URL}>View build</a>)")
			}
			
		}
		unstable {
			script {
				notify.buildNotification("r360", "unstable", false, "${JOB_NAME} #${BUILD_NUMBER} Application: '${selectedApplication}'  Build ${jobItems.VersionToBuild} succeeded but one or more unit tests failed for branch ${jobItems.BranchToBuild}. Duration: ${currentBuild.durationString.replace(' and counting', '')} (<a href=${BUILD_URL}>View build</a>)")
			}
			
		}
		success {
			script {
				notify.buildNotification("r360",, "succeeded", false, "${JOB_NAME} #${BUILD_NUMBER} Application: '${selectedApplication}' Build ${jobItems.VersionToBuild} succeeded for branch ${jobItems.BranchToBuild}. Duration: ${currentBuild.durationString.replace(' and counting', '')} (<a href=${BUILD_URL}>View build</a>)")
			}
			
		}
	}
}