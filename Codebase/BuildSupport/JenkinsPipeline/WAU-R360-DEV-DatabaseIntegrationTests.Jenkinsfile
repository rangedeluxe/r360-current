#!groovy
import groovy.json.JsonOutput
//default branch to build
def branchToBuild = "DEV"
def branchPath = ""
def commonScripts

@Library('common') _
pipeline {
    agent {
        // This is a label we added to 'mosqatfsbs03.wausaufs.nwk'.
        // This means we can run on any machine that has this label - currently only mosqatfsbs03.wausaufs.nwk.
		label 'QA_Pipeline'
	}
	options	{
		buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '10'))
		timestamps()
	}
	environment {
	    notificationRoom = 'r360'
	}
	stages {
	    stage('Job Startup') {
	        steps { 
				script {
	            echo "Running job startup step..."
				notify.started('r360')
				echo "Running against branch DEV"
				if (params?.BranchToBuild != null) {
				    branchToBuild = "Dev"
					}
				cleanWs()
				}
	        }
	    }
	    stage('Check out') {
	        steps { 
	            echo "Running checkout step..."
				checkout poll: false, 
				scm: [$class: 'GitSCM', 
					branches: [[name: "*/Dev"]], 
					doGenerateSubmoduleConfigurations: false, 
					extensions: [
						[$class: 'CloneOption', timeout: 120],
						[$class: 'CleanBeforeCheckout'],
						[$class: 'SparseCheckoutPaths', 
							sparseCheckoutPaths:[
							]
						]
					],
					userRemoteConfigs: [[credentialsId: '0039de23-b91f-4f24-af68-81190d23a31b', url: 'http://bitbucket.deluxe.com:7990/scm/WFS/r360-current.git/']],
					submoduleCfg: []
				]
	        }
	    }
		stage('Build setup') {
			steps {
				powershell 'd:\\Tools\\nuget.exe restore .\\Codebase\\TestApps\\DatabaseTests\\DatabaseTests.sln -Verbosity Detailed'
			}
		}
		stage('Build') {
			steps {
			    dir('Codebase/TestApps/DatabaseTests') {
			        bat "dotnet publish -c Release -r win-x64 -o ${WORKSPACE}\\binaries"
			    }
			}
		}
		stage('Run Tests on SQL 2012') {
		    steps {
				script {
					executeTest = "\"${tool '15.0 (VS2017)'}\\..\\..\\..\\Common7\\IDE\\CommonExtensions\\Microsoft\\TestWindow\\vstest.console.exe\" "
					executeTest += "${WORKSPACE}\\binaries\\DatabaseTests.dll"
					executeTest += " /Enablecodecoverage /Logger:trx /Settings:${WORKSPACE}\\Codebase\\TestApps\\DatabaseTests\\Jenkins.2012.runsettings"
					bat executeTest
				}
				step([$class: 'MSTestPublisher', testResultsFile:"**/*.trx", failOnError: true, keepLongStdio: true])
		    }
		}
		stage('Run Tests on SQL 2016') {
		    steps {
				script {
					executeTest = "\"${tool '15.0 (VS2017)'}\\..\\..\\..\\Common7\\IDE\\CommonExtensions\\Microsoft\\TestWindow\\vstest.console.exe\" "
					executeTest += "${WORKSPACE}\\binaries\\DatabaseTests.dll"
					executeTest += " /Enablecodecoverage /Logger:trx /Settings:${WORKSPACE}\\Codebase\\TestApps\\DatabaseTests\\Jenkins.2016.runsettings"
					bat executeTest
				}
				step([$class: 'MSTestPublisher', testResultsFile:"**/*.trx", failOnError: true, keepLongStdio: true])
		    }
		}
	}
	post {
		failure {
			script {
				notify.failed('r360')
			}
		}
		unstable {
			script {
				notify.unstable('r360')
			}	
		}
		success {
			script {
				notify.succeeded('r360')
			}
		}
	}
}