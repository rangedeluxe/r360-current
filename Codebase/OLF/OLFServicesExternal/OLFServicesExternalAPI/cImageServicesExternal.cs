﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Crypto;
using WFS.RecHub.ExternalLogon.API;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.OLFServices;
using WFS.RecHub.R360Shared;
using WFS.RecHub.Common.Log;
using WFS.RecHub.DAL;

using Wfs.Raam.Core;
using Wfs.Raam.Core.Services;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 02/10/2015
*
* Purpose: Main logic for OLFServicesExternal methods
*       
*
* Modification History
* WI 187501 TWE 02/10/2015
*  - Initial 
* WI 197638 TWE 03/24/2015
*    modify to allow legacy version of imageRPS to call old contract
* WI 197809 TWE 03/25/2015
*    modify to pass in paymentsource
*******************************************************************************/

namespace WFS.RecHub.OLFServicesExternalAPI
{
    public class cImageServicesExternal : LTAService, IExternalLogonService, IDisposable
    {
        // Track whether Dispose has been called.
        private bool disposed = false;

        private Guid _SessionID = Guid.Empty;
        private string _SiteKey = string.Empty;
        private cOLFExternalSiteOptions _ServerOptions = null;
        private cEventLog _EventLog = null;
        private readonly cSessionDAL _sessiondal;
        private readonly cCrypto3DES _crypto;


        public cImageServicesExternal()          
        {
            _crypto = new cCrypto3DES();
        }

        public cImageServicesExternal(Guid SessionIDValue, 
                              string SiteKeyValue, 
                              NameValueCollection ServerVariables) 
        {
            _crypto = new cCrypto3DES();
            _SessionID = SessionIDValue;
            _SiteKey = SiteKeyValue;
            _sessiondal = new cSessionDAL(_SiteKey);
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog eventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(serverOptions.logFilePath,
                                              serverOptions.logFileMaxSize,
                                              (MessageImportance)serverOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        /// <summary>
        /// Object that contains ICON Web Service options from the local .ini file.
        /// </summary>
        protected cOLFExternalSiteOptions serverOptions
        {
            get
            {
                if (_ServerOptions == null)
                {
                    _ServerOptions = new cOLFExternalSiteOptions(_SiteKey);
                }

                return _ServerOptions;
            }
        }

       
        private OLFServiceClient olfService = null;
        protected OLFServiceClient OLFService
        {
            get
            {
                if (olfService == null)
                {
                    olfService = new OLFServiceClient(_SiteKey);
                }
                return olfService;
            }
            set
            {
                olfService = value;
            }
        }

        private BaseResponse LogOnError(Func<APIBase, BaseResponse> operation)
        {
            return FailOnError<BaseResponse>((oapi) =>
            {
                var response = (BaseResponse)operation(oapi);
                response.Errors.ForEach(err =>
                {
                    EventLog.logWarning(err, "External Logon", MessageImportance.Essential);
                });
                return response;
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new RAAMExternalLogon(
                new WFS.RecHub.R360Shared.ConfigHelpers.Logger(
                    x => EventLog.logEvent(x, MessageImportance.Essential),
                    x => EventLog.logError(new Exception(x))),
                serviceContext);
        }

        public WFS.RecHub.ExternalLogon.Common.PingResponse Ping()
        {
            return (PingResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.Ping();
            });
        }

        public ExternalLogonResponse PermissionExternalLogon(PermissionLogonRequest request)
        {
            return (ExternalLogonResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.PermissionExternalLogon(request);
            });
        }

        public SessionResponse ValidateSession(SessionRequest request)
        {
            return (SessionResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.ValidateSession(request);
            });
        }

        public SessionResponse EndSession(SessionRequest request)
        {
            return (SessionResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.EndSession(request);
            });
        }

        public ExternalLogonResponse ExternalLogon(ExternalLogonRequest request)
        {
            return (ExternalLogonResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.ExternalLogon(request);
            });
        }

        public string Ping2()
        {
            eventLog.logEvent("Request-->Ping: ", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
            return "Pong";
        }

        public bool GetSession(string logonName, string password, out Guid sessionID)
        {
            bool rtrnFlag = false;
            string sRetVal = string.Empty;
            sessionID = Guid.Empty;

            eventLog.logEvent("Request-->GetSession: ", this.GetType().Name, MessageType.Information, MessageImportance.Debug);

            //make sure the claims are attached
            rtrnFlag = AttachClaims(logonName, password, out sessionID);
       
            if (rtrnFlag)
            {
                eventLog.logEvent("Current User Claims: \r\n" + WSFederatedAuthentication.ClaimsAccess.GetClaims().Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
            }
            else
            {
                eventLog.logEvent("Request-->GetSession: Failed, no claims attached.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            }

            return rtrnFlag;
        }

        public bool EndSession()
        {
            bool rtrnFlag = false;
            eventLog.logEvent("Request-->EndSession: ", this.GetType().Name, MessageType.Information, MessageImportance.Debug);

            var request = new SessionRequest()
            {
                Session = _SessionID
            };

            var endSessionResponse = EndSession(request);

            if (endSessionResponse.Status == StatusCode.SUCCESS)
            {
                rtrnFlag = true;
            }

            return rtrnFlag;
        }

        public bool GetImage(   
                             string logonName, 
                             string password,
                             int BankID,
                             int LockboxID,
                             int ImmutableDateKey,
                             int DepositDateKey,
                             long BatchID,
                             int BatchSequence,
                             string paymentSource,
                             bool IsCheck,
                             out byte[][] ImageList,
                             out string FileDescriptor,
                             out string FileExtension,
                             out Guid sessionID)
        {
            bool rtrnFlag = false;
            ImageList = null;
            FileDescriptor = string.Empty;
            FileExtension = string.Empty;
            sessionID = Guid.Empty;

            //make sure the claims are attached
            rtrnFlag = AttachClaims(logonName, password, out sessionID);
            string importTypeShortName = ConfigurationManager.AppSettings["DefaultImportType"];

            if (rtrnFlag)
            {
                rtrnFlag = OLFService.GetImage(BankID,                   //<--
                                               LockboxID,                //<--
                                               ImmutableDateKey,        //<--
                                               DepositDateKey,
                                               0,                        //<--No true batch id
                                               BatchSequence,            //<--
                                               IsCheck,                  //<--
                                               0,                        //<--
                                               BatchID,                  //<--use BatchID input (actually sourcebatchid)
                                               paymentSource,            //<--Required
                                               importTypeShortName,      //<--New
                                               out ImageList,            //<--
                                               out FileDescriptor,       //<--
                                               out FileExtension);       //<--
            }        
            return rtrnFlag;
        }
       
        private bool AttachClaims(string logonName, string password, out Guid sessionID)
        {
            bool rtrnFlag = false;
            string sRetVal = string.Empty;

            string encLogonName = string.Empty;
            string encPassword = string.Empty;
            string versionCheck = ConfigurationManager.AppSettings["PreviousVersionAllowed"];

            if (versionCheck != null && versionCheck.ToUpper() == "TRUE")
            {
                //For the legacy version pick up the credentials from the config file
                _crypto.Decrypt(ConfigurationManager.AppSettings["LogonName"], out encLogonName);
                _crypto.Decrypt(ConfigurationManager.AppSettings["Password"], out encPassword);
                eventLog.logEvent("Use Credentials from web.config ", this.GetType().Name, MessageType.Information, MessageImportance.Essential);
            }
            else
            {
                encLogonName = logonName;
                encPassword = password;
            }

            if (!WSFederatedAuthentication.IsAuthenticated || WSFederatedAuthentication.ClaimsAccess.GetClaim(WfsClaimTypes.SessionId) == null)
            {
                rtrnFlag = ProcessLogon(encLogonName, encPassword, out sRetVal, out sessionID);
            }
            else
            {
                if (WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.UserName) == logonName)
                {
                    sessionID = new Guid(WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.SessionId));
                    rtrnFlag = true;
                }
                else
                {
                    rtrnFlag = ProcessLogon(encLogonName, encPassword, out sRetVal, out sessionID);
                }
            }
            return rtrnFlag;
        }

        private bool ProcessLogon(string encLogonName, string encPassword, out string sRetVal, out Guid sessionID)
        {
            bool rtrnFlag = false;
            sRetVal = string.Empty;

            string applicatoinURI = ConfigurationManager.AppSettings["ApplicationURI"];
            string wcfConfigLocation = ConfigurationManager.AppSettings["WCFConfigLocation"];

            int userID = 0;
            string message = string.Empty;
            sessionID = Guid.Empty;


            //go authenticate the logon

            try
            {
                rtrnFlag = Logon(encLogonName, encPassword, out userID, out sessionID, out message);
                if (rtrnFlag == false)
                {
                    sRetVal = "RAAM Authentication Failed";
                    eventLog.logEvent(sRetVal
                                , this.GetType().Name
                                , MessageType.Warning
                                , MessageImportance.Essential);
                }
            }
            catch (System.Exception ex)
            {
                eventLog.logEvent("Exception in Logon: " + ex.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);

                sRetVal = "Unable to Log into RAAM";
            }
            return rtrnFlag;
        }

        //private string DecryptIt(string encString)
        //{
        //    string sTmp;
        //    _crypto.Decrypt(encString, out sTmp);
        //    return sTmp;
        //}

        private bool Logon(            
            string uername,
            string password,
            out int userID,
            out Guid sessionID,
            out string userMessage)
        {            
            var request = new ExternalLogonRequest()
            {
                Username = uername,
                Password = password,
                Entity = string.Empty,
                IPAddress = Environment.MachineName
            };

            var response = ExternalLogon(request);  

            // If we couldn't log in, we'll kick out early.
            if (response == null || response.Status == StatusCode.FAIL || response.Session == null)
            {
                userID = 0;
                sessionID = Guid.Empty;
                userMessage = "Logon Failed";
                return false;
            }

            userID = response.UserId;
            sessionID = response.Session;
            userMessage = "Logon Successful";
            return true;
        }

        #region Dispose
        

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_sessiondal != null)
                    {
                        _sessiondal.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }
        
        #endregion
    }
}
