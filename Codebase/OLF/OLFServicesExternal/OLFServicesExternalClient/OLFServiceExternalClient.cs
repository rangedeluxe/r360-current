﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Web;

using WFS.RecHub.OLFServicesExternal;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 02/10/2015
*
* Purpose: Main logic for OLFServicesExternal methods
*       
*
* Modification History
* WI 187501 TWE 02/10/2015
*  - Initial 
* WI 197638 TWE 03/24/2015
*    modify to allow legacy version of imageRPS to call old contract
* WI 197809 TWE 03/25/2015
*    modify to pass in paymentsource
*******************************************************************************/

namespace WFS.RecHub.OLFServicesExternalClient
{
    public class OLFServiceExternalClient
    {
        private readonly IOLFServiceExternal _OLFServiceExternal = null;

        private Guid _SessionID;

        private string siteKey = "IPOnline";

        public OLFServiceExternalClient()
        {
            ChannelFactory<IOLFServiceExternal> factory = new ChannelFactory<IOLFServiceExternal>("BasicHttpBinding_IOLFServiceExternal");

            _OLFServiceExternal = factory.CreateChannel();
        }

        public string Ping()
        {
            using (new OperationContextScope((IContextChannel)_OLFServiceExternal))
            {
                return _OLFServiceExternal.Ping();
            }        
        }

        public bool GetSession(string logonName,
                               string password,
                               out Guid SessionID)
        {

            bool bolRetVal;

            try
            {
                using (new OperationContextScope((IContextChannel)_OLFServiceExternal))
                {

                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", this.siteKey);

                    bolRetVal = _OLFServiceExternal.GetSession(logonName,
                                                       password,
                                                       out SessionID);
                }
            }
            catch (FaultException<InvalidSiteKeyFault>)
            {
                throw (new InvalidSiteKeyException());
            }
            catch (FaultException<ServerFaultException> ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "GetSession");
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "GetSession");
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "GetSession");
                throw;
            }

            return (bolRetVal);
        }

        public bool EndSession(Guid SessionID)
        {

            bool bolRetVal;

            try
            {
                using (new OperationContextScope((IContextChannel)_OLFServiceExternal))
                {

                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SessionID", SessionID.ToString());
                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", this.siteKey);

                    bolRetVal = _OLFServiceExternal.EndSession();
                }
            }
            catch (FaultException<InvalidSiteKeyFault>)
            {
                throw (new InvalidSiteKeyException());
            }
            catch (FaultException<InvalidSessionIDFault>)
            {
                throw (new InvalidSessionIDException());
            }
            catch (FaultException<ServerFaultException> ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "EndSession");
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "EndSession");
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "EndSession");
                throw;
            }

            return (bolRetVal);
        }

        public bool GetImage(string encLogonName,
                             string encPassword,
                             int BankID,
                             int LockboxID,
                             int ImmutableDateKey,
                             int DepositDateKey,
                             long BatchID,
                             int BatchSequence,
                             bool IsCheck,
                             string paymentSource,
                             out byte[][] ImageList,
                             out string FileDescriptor,
                             out string FileExtension,
                             out Guid sessionID)
        {

            bool bolImageGot;

            try
            {
                using (new OperationContextScope((IContextChannel)_OLFServiceExternal))
                {

                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SessionID", _SessionID.ToString());
                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", this.siteKey);

                    bolImageGot = _OLFServiceExternal.GetImage(    
                                                        encLogonName, 
                                                        encPassword,
                                                        BankID,
                                                        LockboxID,
                                                        ImmutableDateKey,
                                                        DepositDateKey,
                                                        BatchID,
                                                        BatchSequence,
                                                        IsCheck,
                                                        paymentSource,
                                                        out ImageList,
                                                        out FileDescriptor,
                                                        out FileExtension,
                                                        out sessionID);
                }
            }
            catch (FaultException<InvalidSiteKeyFault>)
            {
                throw (new InvalidSiteKeyException());
            }
            catch (FaultException<InvalidSessionIDFault>)
            {
                throw (new InvalidSessionIDException());
            }
            catch (FaultException<SessionExpiredFault>)
            {
                throw (new SessionExpiredException());
            }
            catch (FaultException<InvalidSessionFault>)
            {
                throw (new InvalidSessionException());
            }
            catch (FaultException<ImageNotFoundInDbFault>)
            {
                throw (new ImageNotFoundInDbException(BankID, LockboxID, ImmutableDateKey, BatchID, BatchSequence));
            }
            catch (FaultException<ImageNotFoundOnDiskFault>)
            {
                throw (new ImageNotFoundOnDiskException(BankID, LockboxID, ImmutableDateKey, BatchID, BatchSequence));
            }
            catch (FaultException<ServerFaultException> ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "GetImage");
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "GetImage");
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                //EventLog.logError(ex, this.GetType().Name, "GetImage");
                throw (new Exception(ex.Message, ex));
            }

            return (bolImageGot);
        }
    }
}
