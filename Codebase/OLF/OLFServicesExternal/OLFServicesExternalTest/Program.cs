﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using WFS.RecHub.OLFServicesExternalClient;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 02/10/2015
*
* Purpose: Main logic for OLFServicesExternal methods
*       
*
* Modification History
* WI 187501 TWE 02/10/2015
*  - Initial 
* WI 197638 TWE 03/24/2015
*    modify to allow legacy version of imageRPS to call old contract
* WI 197809 TWE 03/25/2015
*    modify to pass in paymentsource
*******************************************************************************/

namespace OLFServicesExternalTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            Ping();

            string encLogonName = "OLFuser";              
            string encPassword = "Wausau#1";  

            Guid _sessionID;
            GetSession( encLogonName, encPassword, out _sessionID);

            EndSession(_sessionID);

            encLogonName = "FITuser";              //FITuser
            GetSession(encLogonName, encPassword, out _sessionID);

            EndSession(_sessionID);

            encLogonName = "OLFuser";              //OLFuser
            GetSession(encLogonName, encPassword, out _sessionID);

            EndSession(_sessionID);

            int BankID = 999;
            int LockboxID = 2791;
            int ImmutableDateKey = 20141210;
            int DepositDateKey = 20141210;
            long BatchID = 2854;
            int BatchSequence = 2;
            bool IsCheck = true;
            int SiteID = 999;
            string paymentSource = "imageRPS-Michael";
            byte[][] ImageList;
            string FileDescriptor;
            string FileExtension;
            Guid sessionID;

            GetImage(encLogonName, encPassword,
                BankID,
                LockboxID,
                ImmutableDateKey,
                DepositDateKey,
                BatchID,
                BatchSequence,
                IsCheck,
                SiteID,
                paymentSource,
               out  ImageList,
               out  FileDescriptor,
               out  FileExtension,
               out  sessionID);

            EndSession(_sessionID);

            GetImage(encLogonName, encPassword,
                BankID,
                LockboxID,
                ImmutableDateKey,
                DepositDateKey,
                BatchID,
                BatchSequence,
                IsCheck,
                SiteID,
                paymentSource,
               out  ImageList,
               out  FileDescriptor,
               out  FileExtension,
               out  sessionID);

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue . . .");
            Console.ReadKey(true);

            int abc = 1;
        }
       

        static void Ping()
        {
            Console.WriteLine("Request: Ping");
            OLFServiceExternalClient _olf = new OLFServiceExternalClient();
            Console.WriteLine("Response:  " + _olf.Ping());
            Console.WriteLine("");
        }

        static void GetSession(string logonName, string password, out Guid sessionID)
        {
            sessionID = Guid.Empty;
            Console.WriteLine("Request: GetSession");
            OLFServiceExternalClient _olf = new OLFServiceExternalClient();
            _olf.GetSession(logonName, password, out sessionID);
            Console.WriteLine("Response:  SessionID>>" + sessionID.ToString() + "<<");
            Console.WriteLine("");
        }

        static void EndSession(Guid sessionID)
        {
            Console.WriteLine("Request: EndSession");
            OLFServiceExternalClient _olf = new OLFServiceExternalClient();
            if (_olf.EndSession(sessionID))
            {
                Console.WriteLine("Response:  Session terminated " + sessionID.ToString());
            }
            else
            {
                Console.WriteLine("Response:  Session not found " + sessionID.ToString());
            }
            Console.WriteLine("");
        }

        static void GetImage(string encLogonName, string encPassword,int BankID,
                      int LockboxID,
                      int ImmutableDateKey,
                      int DepositDateKey,
                      long BatchID,
                      int BatchSequence,
                      bool IsCheck,
                      int SiteID,
                      string paymentSource,
                      out byte[][] ImageList,
                      out string FileDescriptor,
                      out string FileExtension,
                      out Guid sessionID)
        {
            ImageList = null;
             FileDescriptor = string.Empty;
             FileExtension = string.Empty;
            OLFServiceExternalClient _olf = new OLFServiceExternalClient();

            Console.WriteLine("Request: GetImage");
            _olf.GetImage( 
                       encLogonName,
                       encPassword,
                       BankID,
                       LockboxID,
                       ImmutableDateKey,
                       DepositDateKey,
                       BatchID,
                       BatchSequence,
                       IsCheck,
                       paymentSource,
                      out  ImageList,
                      out  FileDescriptor,
                      out  FileExtension,
                      out sessionID);
             Console.WriteLine("Response: from GetImage SessionID>>" + sessionID.ToString() + "<<");
             Console.WriteLine("");
        }

        public static void Pause()
        {
            Console.WriteLine("Press any key to continue . . .");
            Console.ReadKey(true);
        }


    }
}
