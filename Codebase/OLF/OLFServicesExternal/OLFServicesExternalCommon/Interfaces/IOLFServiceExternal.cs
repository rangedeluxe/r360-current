﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 02/10/2015
*
* Purpose: Main logic for OLFServicesExternal methods
*       
*
* Modification History
* WI 187501 TWE 02/10/2015
*  - Initial 
* WI 197638 TWE 03/24/2015
*    modify to allow legacy version of imageRPS to call old contract
*******************************************************************************/

namespace WFS.RecHub.OLFServicesExternal
{
    //**************************************
    // OLFServiceExternal
    //**************************************
    [ServiceContract]
    public interface IOLFServiceExternal
    {

        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "GetSession")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool GetSession(string LogonName,
                        string Password,
                        out Guid SessionID);

        [OperationContract(Action = "EndSession")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool EndSession();

        [OperationContract(Action = "GetImage")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ImageNotFoundInDbFault))]
        [FaultContract(typeof(ImageNotFoundOnDiskFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool GetImage(string encLogonName,
                      string encPassword,
                      int BankID,
                      int LockboxID,
                      int ImmutableDateKey,
                      int DepositDateKey,
                      long BatchID,
                      int BatchSequence,
                      bool IsCheck,
                      string PaymentSource,
                      out byte[][] ImageList,
                      out string FileDescriptor,
                      out string FileExtension,
                      out Guid sessionID);
    }


    //**************************************
    // Common Data Contracts
    //**************************************
    [DataContract]
    public class ServerFaultException
    {

        [DataMember]
        public string errorcode;

        [DataMember]
        public string message;

        [DataMember]
        public string details;
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "InvalidSiteKeyFault", Namespace = "http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSiteKeyFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged
    {

        public InvalidSiteKeyFault(int category, string source, string message)
            : base(category, (int)OLFServiceErrorCodes.InvalidSiteKey, source, message)
        {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "InvalidSessionIDFault", Namespace = "http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSessionIDFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged
    {

        public InvalidSessionIDFault(int category, string source, string message)
            : base(category, (int)OLFServiceErrorCodes.InvalidSessionID, source, message)
        {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "SessionExpiredFault", Namespace = "http://www.wfs.com/Security/v1.0.0")]
    public partial class SessionExpiredFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged
    {

        public SessionExpiredFault(int category, string source, string message)
            : base(category, (int)OLFServiceErrorCodes.SessionExpired, source, message)
        {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "InvalidSessionFault", Namespace = "http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSessionFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged
    {

        public InvalidSessionFault(int category, string source, string message)
            : base(category, (int)OLFServiceErrorCodes.InvalidSession, source, message)
        {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "ImageNotFoundInDbFault", Namespace = "http://www.wfs.com/Security/v1.0.0")]
    public partial class ImageNotFoundInDbFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged
    {

        public ImageNotFoundInDbFault(int category, string source, string message)
            : base(category, (int)OLFServiceErrorCodes.ImageNotFoundInDb, source, message)
        {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name = "ImageNotFoundOnDiskFault", Namespace = "http://www.wfs.com/Security/v1.0.0")]
    public partial class ImageNotFoundOnDiskFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged
    {

        public ImageNotFoundOnDiskFault(int category, string source, string message)
            : base(category, (int)OLFServiceErrorCodes.ImageNotFoundOnDisk, source, message)
        {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

}
