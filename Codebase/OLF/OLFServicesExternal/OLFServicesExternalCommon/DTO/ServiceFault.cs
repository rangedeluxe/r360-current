﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 02/10/2015
*
* Purpose: Main logic for OLFServicesExternal methods
*       
*
* Modification History
* WI 187501 TWE 02/10/2015
*  - Initial 
*
*******************************************************************************/

namespace WFS.RecHub.OLFServicesExternal
{
    /// </code>
    /// </example>
    /// <seealso cref="Wfs.ServiceModel.Service.ExceptionShield"/>
    [DataContract(Namespace = "http://Wfs.ServiceModel", Name = "ServiceFault")]
    public class ServiceFault
    {

        private int m_category = 0;
        private int m_errorCode = 0;
        private string m_source = null;
        private string m_message = null;
        private ExceptionDetail m_detail = null;

        /// <summary>
        /// Default constructor initializes a new instance with no data set
        /// </summary>
        public ServiceFault()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceFault"/> class 
        /// without error code and source information.
        /// </summary>
        /// <param name="message">
        /// The human readable description of the exception.  Ensure that 
        /// this does not include any information that might be useful to an 
        /// attacker.
        /// </param>
        /// <remarks>
        /// Use this constructor for errors that do not have a specific error code 
        /// and source associated with them (typically unknown exceptions).
        /// </remarks>
        public ServiceFault(string message)
            : this(0, 0, null, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceFault"/> class 
        /// without error code information.
        /// </summary>
        /// <param name="source">Source application or component</param>
        /// <param name="message">
        /// The human readable description of the exception.  Ensure that 
        /// this does not include any information that might be useful to an 
        /// attacker.
        /// </param>
        /// <remarks>
        /// Use this constructor for errors that do not have a specific error code 
        /// associated with them (typically unknown exceptions).
        /// </remarks>
        public ServiceFault(string source, string message)
            : this(0, 0, source, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceFault"/> class 
        /// </summary>
        /// <param name="category">Error or event application defined category</param>
        /// <param name="errorCode">Application error code</param>
        /// <param name="source">Source application or component</param>
        /// <param name="message">
        /// The human readable description of the error.  Ensure that 
        /// this does not include any information that might be useful to an 
        /// attacker.
        /// </param>
        public ServiceFault(int category, int errorCode, string source, string message)
        {
            Category = category;
            ErrorCode = errorCode;
            Source = source;
            Message = message;
        }

        /// <summary>
        /// Gets or sets the error category of the exception.
        /// </summary>
        /// <value>The category of the exception.</value>
        [DataMember]
        public int Category
        {

            get { return m_category; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Category");
                }
                m_category = value;
            }
        }

        /// <summary>
        /// Gets or sets the error code of the exception.
        /// </summary>
        /// <value>The error code of the exception.</value>
        [DataMember]
        public int ErrorCode
        {

            get
            {
                return m_errorCode;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("ErrorCode");
                }
                m_errorCode = value;
            }
        }

        /// <summary>
        /// Gets or sets the source application or component property
        /// </summary>
        /// <value>source string</value>
        [DataMember]
        public string Source
        {
            get
            {
                return m_source;
            }
            set
            {
                m_source = value;
            }
        }

        /// <summary>
        /// Gets or sets the human readable description of the exception.
        /// </summary>
        /// <value>A message string.</value>
        /// <remarks>
        /// This value can also be used as the fault reason for this fault.
        /// It is the caller's responsibility to ensure that the value of this
        /// property does not contain any confidential or security related 
        /// information.
        /// </remarks>
        [DataMember]
        public string Message
        {
            get
            {
                return m_message;
            }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("Message");
                }
                m_message = value;
            }
        }

        /// <summary>
        /// Get or sets the detail exception data associated with this fault
        /// </summary>
        /// <remarks>
        /// This is used when show exception details in faults is enabled to support both
        /// getting exception detail information and keeping the typed faults exception intact
        /// at the same time.
        /// </remarks>
        [DataMember]
        public ExceptionDetail ExceptionDetail
        {
            get { return m_detail; }
            set { m_detail = value; }
        }

        /// <summary>
        /// Returns a string that represents this <see cref="ServiceFault"/>.
        /// When the exception detail property is set the formatted string 
        /// will include the detail information.
        /// </summary>
        /// <returns>
        /// A formatted string representing the fault.
        /// </returns>
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();

            sb.Append("Service Fault: ");

            if (m_category != 0 || m_errorCode != 0)
            {
                sb.AppendFormat("Error={0}-{1} ", m_category, m_errorCode);
            }

            if (!String.IsNullOrEmpty(m_source))
            {
                sb.AppendFormat("Source={0} ", m_source);
            }

            sb.Append(m_message);

            if (m_detail != null)
            {
                sb.AppendLine();
                sb.Append(m_detail.ToString());
            }

            return sb.ToString();
        }
    }
}
