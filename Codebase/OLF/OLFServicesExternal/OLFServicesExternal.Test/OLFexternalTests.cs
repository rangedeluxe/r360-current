﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.OLFServicesExternal;
using WFS.RecHub.OLFServicesExternalAPI;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 02/10/2015
*
* Purpose: Main logic for OLFServicesExternal methods
*       
*
* Modification History
* WI 187501 TWE 02/10/2015
*  - Initial 
* WI 197638 TWE 03/24/2015
*    modify to allow legacy version of imageRPS to call old contract
*******************************************************************************/

namespace WFS.RecHub.OLFServicesExternal.Test
{
    [TestClass]
    public class OLFexternalTests : IDisposable
    {
        // Track whether Dispose has been called.
        private bool disposed = false;

        private readonly cImageServicesExternal _OLFexternal;

        public OLFexternalTests()
        {
            _OLFexternal = new cImageServicesExternal();
        }

        [TestMethod]
        public void TestPing1()
        {
            var result = _OLFexternal.Ping2();
            Assert.IsTrue(result == "Pong");
        }

        [TestMethod]
        public void TestGetSessionFail01()
        {
            string userLogon = "";
            string userPassword = "";
            Guid sessionID;
            //var request = 
            var result = _OLFexternal.GetSession(userLogon, userPassword, out sessionID);

            Assert.IsFalse(result);
            Assert.IsTrue(sessionID == Guid.Empty);
        }

        [TestMethod, Ignore]
        public void TestGetSessionSuccess01()
        {
            string userLogon = "OLFuser";
            string userPassword = "Wausau#1";
            Guid sessionID;

            var result = _OLFexternal.GetSession(userLogon, userPassword, out sessionID);

            Assert.IsTrue(result);
            Assert.IsTrue(sessionID != Guid.Empty);
        }

        [TestMethod, Ignore]
        public void TestGetSessionSuccess02()
        {
            string userLogon = "OLFuser";
            string userPassword = "Wausau#1";

            Guid sessionID;
            var result = _OLFexternal.GetSession(userLogon, userPassword, out sessionID);

            string userLogon2 = "OLFuser";
            string userPassword2 = "Wausau#1";
            Guid sessionID2;
            var result2 = _OLFexternal.GetSession(userLogon2, userPassword2, out sessionID2);

            Assert.IsTrue(result);
            Assert.IsTrue(result2);
            Assert.AreEqual(sessionID.ToString(), sessionID2.ToString());
        }

        [TestMethod, Ignore]
        public void TestGetSessionSuccess03()
        {
            string userLogon = "OLFuser";
            string userPassword = "Wausau#1";
            Guid sessionID;
            var result = _OLFexternal.GetSession(userLogon, userPassword, out sessionID);

            string userLogon2 = "OLFuser2";
            string userPassword2 = "Wausau#1";
            Guid sessionID2;
            var result2 = _OLFexternal.GetSession(userLogon2, userPassword2, out sessionID2);

            Assert.IsTrue(result);
            Assert.IsTrue(result2);
            Assert.AreNotEqual(sessionID.ToString(), sessionID2.ToString());
        }

        //[TestMethod]
        //public void TestEndSessionSuccess01()
        //{
        //    string userLogon = "lRntgiRWB9w=";
        //    string userEntity = "qiD1YeLeYuQ=";
        //    string userPassword = "T9AyWrk1+zU4gktpQh6viw==";
        //    Guid sessionID;
        //    var result = _OLFexternal.GetSession(userEntity, userLogon, userPassword, out sessionID);

        //    var result2 = _OLFexternal.EndSession();
           
        //    Assert.IsTrue(result);
            
        //    //Assert.AreNotEqual(sessionID.ToString(), sessionID2.ToString());
        //}

        #region Dispose
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_OLFexternal != null)
                    {
                        _OLFexternal.Dispose();
                    }                   
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }
        #endregion
    }
}
