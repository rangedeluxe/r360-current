﻿

namespace WFS.RecHub.OlfRequestCommon
{
    public class InProcessExceptionDocumentImageRequestDto
    {
        public int GlobalDocumentId { get; set; }
        public int DocumentSequence { get; set; }
        public int BatchSequence { get; set; }
        public string FileDescriptor { get; set; }
        public double ImageHeight { get; set; }
        public double ImageWidth { get; set; }
        public bool IsColor { get; set; }
        public string TraceNumber { get; set; }
        public string Description { get; set; }
        public string PicsDate { get; set; }
        public int SiteCodeId { get; set; }
    }
}
