﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WFS.RecHub.OlfRequestCommon
{
    [DataContract]
    public class InProcessExceptionImageRequestDto
    {
        //  The following are the preference settings passed into OLF
        [DataMember]
        public bool DisplayRemitterNameInPdf { get; set; }
        [DataMember]
        public int DisplayModeForCheck { get; set; }
        [DataMember]
        public int DisplayModeForDocument { get; set; }
        //-----------------------------------------------------------

        //  Transaction Keys
        [DataMember]
        public string Workgroup { get; set; }
        [DataMember]
        public string PaymentSource { get; set; }
        [DataMember]
        public int WorkgroupId { get; set; }
        [DataMember]
        public DateTime ProcessingDate { get; set; }
        [DataMember]
        public DateTime DepositDate { get; set; }
        [DataMember]
        public int BankId { get; set; }
        [DataMember]
        public int TransactionId { get; set; }
        [DataMember]
        public int TransactionSequence { get; set; }
        [DataMember]
        public int BatchId { get; set; }
        [DataMember]
        public int BatchNumber { get; set; }
        [DataMember]
        public int GlobalBatchId { get; set; }
        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public IEnumerable<InProcessExceptionPaymentImageRequestDto> Payments { get; set; }
        [DataMember]
        public IEnumerable<InProcessExceptionDocumentImageRequestDto> Documents { get; set; }
    }
}