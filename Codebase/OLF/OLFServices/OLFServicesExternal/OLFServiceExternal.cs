﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using System.Configuration;

using WFS.RecHub.Common;
using WFS.RecHub.Common.Crypto;
using WFS.RecHub.OLFServicesExternalAPI;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 02/10/2015
*
* Purpose: Main logic for OLFServicesExternal methods
*       
*
* Modification History
* WI 187501 TWE 02/10/2015
*  - Initial 
* WI 197638 TWE 03/24/2015
*    modify to allow legacy version of imageRPS to call old contract
* WI 197809 TWE 03/25/2015
*    modify to pass in paymentsource
*******************************************************************************/
namespace WFS.RecHub.OLFServicesExternal
{

    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    // http://msdn.microsoft.com/en-us/library/aa702682(v=VS.90).aspx
    // http://forums.silverlight.net/forums/p/189399/436335.aspx
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class OLFServiceExternal : _ServiceBase, IOLFServiceExternal
    {
        //Constructor 
        public OLFServiceExternal()
        {
        }

        // Ping
        public string Ping()
        {
            using (cImageServicesExternal objImageServices = new cImageServicesExternal())
            {
                return objImageServices.Ping2();
            }
        }

        public bool GetSession(string logonName, string password, out Guid sessionID)
        {

            bool bolRetVal;
            Guid gidSessionID;
            string strTemp;
            ServerFaultException error;

            try
            {

                EventLog.logEvent("GetSession:", this.GetType().Name, MessageImportance.Debug);
                if (GetHeader(HTTPHEADER_SITEKEY, out strTemp))
                {
                    _SiteKey = strTemp;
                }
                else
                {
                    throw (new InvalidSiteKeyException());
                }

                NameValueCollection nvcContext;
                if (HttpContext.Current == null)
                {
                    nvcContext = GetWCFContext();
                }
                else
                {
                    nvcContext = HttpContext.Current.Request.ServerVariables;
                }

                using (cImageServicesExternal objImageServices = new cImageServicesExternal(Guid.Empty, _SiteKey, nvcContext))
                {
                    bolRetVal = objImageServices.GetSession(logonName, password, out gidSessionID);
                }
            }
            catch (InvalidSiteKeyException ex)
            {
                InvalidSiteKeyFault f = new InvalidSiteKeyFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSiteKeyFault>(f, f.Message);
            }
            catch (FaultException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "GetSession", "A general exception occurred while executing GetSession() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }

            sessionID = gidSessionID;

            return (bolRetVal);
        }

        public bool EndSession()
        {

            bool bolRetVal;
            Guid gidSessionID;
            string strTemp;
            Guid gidTemp;
            ServerFaultException error;

            try
            {
                EventLog.logEvent("EndSession:", this.GetType().Name, MessageImportance.Debug);

                if (GetHeader(HTTPHEADER_SITEKEY, out strTemp))
                {
                    _SiteKey = strTemp;
                }
                else
                {
                    throw (new InvalidSiteKeyException());
                }

                if (GetHeader(HTTPHEADER_SESSIONID, out gidTemp))
                {
                    gidSessionID = gidTemp;
                }
                else
                {
                    throw (new InvalidSessionIDException());
                }

                NameValueCollection nvcContext;
                if (HttpContext.Current == null)
                {
                    nvcContext = GetWCFContext();
                }
                else
                {
                    nvcContext = HttpContext.Current.Request.ServerVariables;
                }
                using (cImageServicesExternal objImageServices = new cImageServicesExternal(gidSessionID, _SiteKey, nvcContext))
                {
                    bolRetVal = objImageServices.EndSession();
                }
            }
            catch (InvalidSiteKeyException ex)
            {
                InvalidSiteKeyFault f = new InvalidSiteKeyFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSiteKeyFault>(f, f.Message);
            }
            catch (InvalidSessionIDException ex)
            {
                InvalidSessionIDFault f = new InvalidSessionIDFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSessionIDFault>(f, f.Message);
            }
            catch (FaultException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "EndSession", "A general exception occurred while executing EndSession() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }

            return (bolRetVal);
        }



        public bool GetImage(  
                            string logonName,
                            string password,
                            int BankID,
                            int LockboxID,
                            int ImmutableDateKey,
                            int DepositDateKey,
                            long BatchID,
                            int BatchSequence,
                            bool IsCheck,
                            string paymentSource,
                            out byte[][] ImageList,
                            out string FileDescriptor,
                            out string FileExtension,
                            out Guid sessionID)
        {
            bool bolRetVal;

            byte[][] arImageList;
            string strFileDescriptor;
            string strFileExtension;
            ServerFaultException error;

            Guid gidTemp;
            string strTemp;

            Guid gidSessionID = Guid.Empty;

            try
            {
                EventLog.logEvent("GetImage: " +
                    " LogonName=\"" + logonName + "\"" +
                    //", encPassword=" + password +
                    ", BankID=" + BankID +
                    ", LockboxID=" + LockboxID +
                    ", ImmutableDateKey=" + ImmutableDateKey +
                    ", DepositDateKey=" + DepositDateKey +
                    ", BatchID=" + BatchID +
                    ", BatchSequence=" + BatchSequence +
                    ", IsCheck=" + IsCheck +
                    ", paymentSource=\"" + paymentSource + "\"",
                    this.GetType().Name, MessageImportance.Debug);

                if (GetHeader(HTTPHEADER_SITEKEY, out strTemp))
                {
                    _SiteKey = strTemp;
                }
                else
                {
                    throw (new InvalidSiteKeyException());
                }

                if (GetHeader(HTTPHEADER_SESSIONID, out gidTemp))
                {
                    gidSessionID = gidTemp;
                }
                else
                {
                    throw (new InvalidSessionIDException());
                }

                NameValueCollection nvcContext;
                if (HttpContext.Current == null)
                {
                    nvcContext = GetWCFContext();
                }
                else
                {
                    nvcContext = HttpContext.Current.Request.ServerVariables;
                }
                using (cImageServicesExternal objImageServices = new cImageServicesExternal(gidSessionID, _SiteKey, nvcContext))
                {

                    bolRetVal = objImageServices.GetImage(logonName,
                                                          password,
                                                          BankID,
                                                          LockboxID,
                                                          ImmutableDateKey,
                                                          DepositDateKey,
                                                          BatchID,
                                                          BatchSequence,
                                                          paymentSource,
                                                          IsCheck,
                                                          out arImageList,
                                                          out strFileDescriptor,
                                                          out strFileExtension,
                                                          out sessionID);
                }

                if (!bolRetVal)
                {
                    arImageList = null; ;
                    strFileExtension = string.Empty;
                }



                ImageList = arImageList;
                FileDescriptor = strFileDescriptor;
                FileExtension = strFileExtension;
            }
            catch (InvalidSiteKeyException ex)
            {
                InvalidSiteKeyFault f = new InvalidSiteKeyFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSiteKeyFault>(f, f.Message);
            }
            catch (InvalidSessionIDException ex)
            {
                InvalidSessionIDFault f = new InvalidSessionIDFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSessionIDFault>(f, f.Message);
            }
            catch (SessionExpiredException ex)
            {
                SessionExpiredFault f = new SessionExpiredFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<SessionExpiredFault>(f, f.Message);
            }
            catch (InvalidSessionException ex)
            {
                InvalidSessionFault f = new InvalidSessionFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSessionFault>(f, f.Message);
            }
            catch (ImageNotFoundInDbException ex)
            {
                ImageNotFoundInDbFault f = new ImageNotFoundInDbFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<ImageNotFoundInDbFault>(f, f.Message);
            }
            catch (ImageNotFoundOnDiskException ex)
            {
                ImageNotFoundOnDiskFault f = new ImageNotFoundOnDiskFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<ImageNotFoundOnDiskFault>(f, f.Message);
            }
            catch (FaultException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "GetImage", "A general exception occurred while executing GetImage() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }

            return (bolRetVal);
        }
              
        private static NameValueCollection GetWCFContext()
        {
            NameValueCollection nvcAns;
            nvcAns = new NameValueCollection();
            if (OperationContext.Current.Channel.RemoteAddress != null)
            {
                nvcAns.Add("REMOTE_ADDR", OperationContext.Current.Channel.RemoteAddress.ToString());
            }
            else
            {
                nvcAns.Add("REMOTE_ADDR", Environment.MachineName);
            }
            nvcAns.Add("HTTP_USER_AGENT", OperationContext.Current.Host.Description.Name);
            nvcAns.Add("SERVER_SOFTWARE", OperationContext.Current.Host.Description.ServiceType.Name);
            return nvcAns;
        }

        private bool GetHeader(string Key, out string Value)
        {

            if (WebOperationContext.Current.IncomingRequest.Headers[Key] != null &&
               WebOperationContext.Current.IncomingRequest.Headers[Key].Length > 0)
            {
                Value = WebOperationContext.Current.IncomingRequest.Headers[Key];
                return (true);
            }
            else
            {
                Value = string.Empty;
                return (false);
            }
        }

        private bool GetHeader(string Key, out Guid Value)
        {

            Guid gidTemp;

            if (WebOperationContext.Current.IncomingRequest.Headers[Key] != null &&
               ipoLib.TryParse(WebOperationContext.Current.IncomingRequest.Headers[Key], out gidTemp))
            {
                Value = gidTemp;
                return (true);
            }
            else
            {
                Value = Guid.Empty;
                return (false);
            }
        }

        /// <summary>
        /// Validate User
        /// </summary>
        /// <param name="LogonName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public bool ValidateUser(string LogonName, string Password)
        {

            return true;

        }
    }
}

