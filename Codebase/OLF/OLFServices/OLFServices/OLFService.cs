﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.OLFServicesAPI;
using WFS.RecHub.R360Shared;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/02/2011 
*   -New Service module
* CR 33857 JNE 07/15/2011
*   -Move Common Code to _ServiceBase class
*   -Add logging when exception is thrown
* CR 45986 WJS 11/23/2001
*	-Add ability pass in SiteID
* CR 51010 JMC 03/07/2012
*   -Changed MaxReceivedMessageSize from 64 mb (67108864 bytes) to 2 gb 
*    (2147483647 bytes)
* CR 52588 twe 10/15/2012
*    IF URL starts with https force security mode transport to use SSL (port 443)
* CR 54779 CEJ 10/25/2012 
*  - Change the image service to allow it to be hosted as a Windows service
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 143450 RDS 05/22/2014	Move Binding configuration to config files
* WI 143275 SAS 05/21/2014
*   Changes done to change the data type of BatchID from int to long
* WI 169137 SAS 10/01/2014
*   Changes done to change the getImage function signature
* WI 175209 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176356 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName  
******************************************************************************/
namespace WFS.RecHub.OLFServices
{

    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    // http://msdn.microsoft.com/en-us/library/aa702682(v=VS.90).aspx
    // http://forums.silverlight.net/forums/p/189399/436335.aspx
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class OLFService : _ServiceBase, IOLFService
    {

        private IOLFService _ImageService = null;
        private const string INISECTION_IMAGEFILESERVICES = "ImageFileServices";

        //Constructor 
        public OLFService()
        {

            string strTemp = "";
            int intTemp;
            ServerFaultException error;

            try
            {
                strTemp = ipoINILib.IniReadValue(cImageServices.ServiceKey, INIKEY_INTERMEDIATESERVER);

                if (strTemp != null && strTemp.Length > 0)
                { // Make sure result is not null or empty.
                    if (int.TryParse(strTemp, out intTemp))
                    {
                        if (intTemp == 1)
                        {
                            _IntermediateServer = true;
                        }
                        else
                        {
                            _IntermediateServer = false;
                        }
                    }
                    else
                    {
                        _IntermediateServer = false;
                    }
                }

            }
            catch (Exception ex)
            {
                error = BuildIniReadException(ex, "OLFService", cImageServices.ServiceKey, INIKEY_INTERMEDIATESERVER);
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }

        // Ping
        public string Ping()
        {

            ServerFaultException error;

            try
            {

                if (_IntermediateServer)
                {

                    // Setup Communication Channel
                    OpenCommunication();

                    // Call method
                    return _ImageService.Ping();

                }
                else
                {
                    return "Pong";
                }

            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "Ping", "A general exception occurred while executing Ping() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }

        public bool GetSession(string LogonName, string Password, out Guid SessionID)
        {

            bool bolRetVal;
            Guid gidSessionID;
            string strTemp;
            ServerFaultException error;

            try
            {

                if (_IntermediateServer)
                {

                    // Setup Communication Channel
                    OpenCommunication();

                    // Call method
                    return _ImageService.GetSession(LogonName, Password, out SessionID);
                }
                else
                {
                    if (GetHeader(HTTPHEADER_SITEKEY, out strTemp))
                    {
                        _SiteKey = strTemp;
                    }
                    else
                    {
                        throw (new InvalidSiteKeyException());
                    }

                    using (cImageServices objImageServices = new cImageServices(Guid.Empty, _SiteKey))
                    {
                        bolRetVal = objImageServices.GetSession(LogonName, Password, out gidSessionID);
                    }
                }

            }
            catch (InvalidSiteKeyException ex)
            {
                InvalidSiteKeyFault f = new InvalidSiteKeyFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSiteKeyFault>(f, f.Message);
            }
            catch (FaultException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "GetSession", "A general exception occurred while executing GetSession() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }

            SessionID = gidSessionID;

            return (bolRetVal);
        }

        public bool EndSession()
        {

            bool bolRetVal;
            Guid gidSessionID;
            string strTemp;
            Guid gidTemp;
            ServerFaultException error;

            try
            {

                if (_IntermediateServer)
                {

                    // Setup Communication Channel
                    OpenCommunication();

                    // Call method
                    return _ImageService.EndSession();

                }
                else
                {

                    if (GetHeader(HTTPHEADER_SITEKEY, out strTemp))
                    {
                        _SiteKey = strTemp;
                    }
                    else
                    {
                        throw (new InvalidSiteKeyException());
                    }

                    if (GetHeader(HTTPHEADER_SESSIONID, out gidTemp))
                    {
                        gidSessionID = gidTemp;
                    }
                    else
                    {
                        throw (new InvalidSessionIDException());
                    }

                    using (cImageServices objImageServices = new cImageServices(gidSessionID, _SiteKey))
                    {
                        bolRetVal = objImageServices.EndSession();
                    }
                }

            }
            catch (InvalidSiteKeyException ex)
            {
                InvalidSiteKeyFault f = new InvalidSiteKeyFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSiteKeyFault>(f, f.Message);
            }
            catch (InvalidSessionIDException ex)
            {
                InvalidSessionIDFault f = new InvalidSessionIDFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSessionIDFault>(f, f.Message);
            }
            catch (FaultException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "EndSession", "A general exception occurred while executing EndSession() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }

            return (bolRetVal);
        }

        public bool GetImage(int BankID,
                             int LockboxID,
                             int ImmutableDateKey,
                             int DepositDateKey,
                             long BatchID,
                             int BatchSequence,
                             bool IsCheck,
                             int SiteID,
                             long SourceBatchID,
                             string BatchSourceShortName,
                             string ImportTypeShortName,
                             int PaymentTypeId,
                             out byte[][] ImageList,
                             out string FileDescriptor,
                             out string FileExtension)
        {

            bool bolRetVal;

            byte[][] arImageList;
            string strFileDescriptor;
            string strFileExtension;
            ServerFaultException error;

            Guid gidTemp;
            string strTemp;

            Guid gidSessionID = Guid.Empty;

            try
            {
                EventLog.logEvent("GetImage: " +
                    ", BankID=" + BankID +
                    ", LockboxID=" + LockboxID +
                    ", ImmutableDateKey=" + ImmutableDateKey +
                    ", DepositDateKey=" + DepositDateKey +
                    ", BatchID=" + BatchID +
                    ", BatchSequence=" + BatchSequence +
                    ", IsCheck=" + IsCheck +
                    ", SiteID=" + SiteID +
                    ", SourceBatchID=" + SourceBatchID +
                    ", BatchSourceShortName=" + BatchSourceShortName +
                    ", ImportTypeShortName=" + ImportTypeShortName +
                    ", PaymentTypeId=" + PaymentTypeId,
                    this.GetType().Name, MessageImportance.Debug);


                if (GetHeader(HTTPHEADER_SITEKEY, out strTemp))
                {
                    _SiteKey = strTemp;
                }
                else
                {
                    throw (new InvalidSiteKeyException());
                }

                if (GetHeader(HTTPHEADER_SESSIONID, out gidTemp))
                {
                    gidSessionID = gidTemp;
                }
                else
                {
                    throw (new InvalidSessionIDException());
                }

                if (_IntermediateServer)
                {

                    // Set up Communication Channel
                    OpenCommunication();
                    
                    using (new OperationContextScope((IContextChannel)_ImageService))
                    {
                        WebOperationContext.Current.OutgoingRequest.Headers.Add("SessionID", gidSessionID.ToString());
                        WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", _SiteKey);

                        //Call Method
                        bolRetVal = _ImageService.GetImage(BankID,
                                                            LockboxID,
                                                            ImmutableDateKey,
                                                            DepositDateKey,
                                                            BatchID,
                                                            BatchSequence,
                                                            IsCheck,
                                                            SiteID,
                                                            SourceBatchID,
                                                            BatchSourceShortName,
                                                            ImportTypeShortName,
                                                            PaymentTypeId,
                                                            out arImageList,
                                                            out strFileDescriptor,
                                                            out strFileExtension);
                    }
                }
                else
                {
                    using (cImageServices objImageServices = new cImageServices(gidSessionID, _SiteKey))
                    {

                        bolRetVal = objImageServices.GetImage(BankID,
                                                              LockboxID,
                                                              ImmutableDateKey,
                                                              DepositDateKey,
                                                              BatchID,
                                                              BatchSequence,
                                                              IsCheck,
                                                              SiteID,
                                                              SourceBatchID,
                                                              BatchSourceShortName,
                                                              ImportTypeShortName,
                                                              PaymentTypeId,
                                                              out arImageList,
                                                              out strFileDescriptor,
                                                              out strFileExtension);
                    }
                }

                if (!bolRetVal)
                {
                    arImageList = null; ;
                    strFileExtension = string.Empty;
                }

                ImageList = arImageList;
                FileDescriptor = strFileDescriptor;
                FileExtension = strFileExtension;

            }
            catch (InvalidSiteKeyException ex)
            {
                InvalidSiteKeyFault f = new InvalidSiteKeyFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSiteKeyFault>(f, f.Message);
            }
            catch (InvalidSessionIDException ex)
            {
                InvalidSessionIDFault f = new InvalidSessionIDFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSessionIDFault>(f, f.Message);
            }
            catch (SessionExpiredException ex)
            {
                SessionExpiredFault f = new SessionExpiredFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<SessionExpiredFault>(f, f.Message);
            }
            catch (InvalidSessionException ex)
            {
                InvalidSessionFault f = new InvalidSessionFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSessionFault>(f, f.Message);
            }
            catch (ImageNotFoundInDbException ex)
            {
                ImageNotFoundInDbFault f = new ImageNotFoundInDbFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<ImageNotFoundInDbFault>(f, f.Message);
            }
            catch (ImageNotFoundOnDiskException ex)
            {
                ImageNotFoundOnDiskFault f = new ImageNotFoundOnDiskFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<ImageNotFoundOnDiskFault>(f, f.Message);
            }
            catch (FaultException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "GetImage", "A general exception occurred while executing GetImage() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }

            return (bolRetVal);
        }

        public bool DeleteBatchImages(
                            int BankID,
                            int ProcessingDateKey,
                            int SiteID,
                            string importTypeShortName,
                            string batchPaymentSourceName,
                            long SourceBatchID)
        {

            bool bolRetVal;

            byte[][] arImageList;
            string strFileDescriptor;
            string strFileExtension;
            ServerFaultException error;

            Guid gidTemp;
            string strTemp;

            Guid gidSessionID = Guid.Empty;

            try
            {
                if (GetHeader(HTTPHEADER_SITEKEY, out strTemp))
                {
                    _SiteKey = strTemp;
                }
                else
                {
                    throw (new InvalidSiteKeyException());
                }

                if (GetHeader(HTTPHEADER_SESSIONID, out gidTemp))
                {
                    gidSessionID = gidTemp;
                }
                else
                {
                    throw (new InvalidSessionIDException());
                }

                if (_IntermediateServer)
                {

                    // Set up Communication Channel
                    OpenCommunication();

                    using (new OperationContextScope((IContextChannel)_ImageService))
                    {
                        WebOperationContext.Current.OutgoingRequest.Headers.Add("SessionID", gidSessionID.ToString());
                        WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", _SiteKey);

                        //Call Method
                        bolRetVal = _ImageService.DeleteBatchImages(BankID,
                                                            ProcessingDateKey,
                                                            SiteID,
                                                            importTypeShortName,
                                                            batchPaymentSourceName,
                                                            SourceBatchID);
                    }
                }
                else
                {
                    using (cImageServices objImageServices = new cImageServices(gidSessionID, _SiteKey))
                    {

                        bolRetVal = objImageServices.DeleteBatchImages(BankID,
                                                              ProcessingDateKey,
                                                              SiteID,
                                                              importTypeShortName,
                                                              batchPaymentSourceName,
                                                              SourceBatchID);
                    }
                }

                if (!bolRetVal)
                {
                    arImageList = null; ;
                    strFileExtension = string.Empty;
                }
            }
            catch (InvalidSiteKeyException ex)
            {
                InvalidSiteKeyFault f = new InvalidSiteKeyFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSiteKeyFault>(f, f.Message);
            }
            catch (InvalidSessionIDException ex)
            {
                InvalidSessionIDFault f = new InvalidSessionIDFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSessionIDFault>(f, f.Message);
            }
            catch (SessionExpiredException ex)
            {
                SessionExpiredFault f = new SessionExpiredFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<SessionExpiredFault>(f, f.Message);
            }
            catch (InvalidSessionException ex)
            {
                InvalidSessionFault f = new InvalidSessionFault(0, this.GetType().Name, ex.Message);
                throw new FaultException<InvalidSessionFault>(f, f.Message);
            }
            catch (FaultException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                error = BuildGeneralException(ex, "GetImage", "A general exception occurred while executing GetImage() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }

            return (bolRetVal);
        }

        private static NameValueCollection GetWCFContext()
        {
            NameValueCollection nvcAns;
            nvcAns = new NameValueCollection();
            if (OperationContext.Current.Channel.RemoteAddress != null)
            {
                nvcAns.Add("REMOTE_ADDR", OperationContext.Current.Channel.RemoteAddress.ToString());
            }
            else
            {
                nvcAns.Add("REMOTE_ADDR", Environment.MachineName);
            }
            nvcAns.Add("HTTP_USER_AGENT", OperationContext.Current.Host.Description.Name);
            nvcAns.Add("SERVER_SOFTWARE", OperationContext.Current.Host.Description.ServiceType.Name);
            return nvcAns;
        }

        // Opens the communication channel to service
        private void OpenCommunication()
        {
            try
            {
                if (LogManager.IsDefault) LogManager.Logger = EventLog.IPOtoILogger(this.GetType().Name);
                _ImageService = R360ServiceFactory.Create<IOLFService>();
            }
            catch (FaultException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                ServerFaultException error = BuildCommunicationsChannelException(ex, "OpenCommunication", "An undefined exception occurred.");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }

        private bool GetHeader(string Key, out string Value)
        {
            if (WebOperationContext.Current.IncomingRequest.Headers[Key] != null &&
               WebOperationContext.Current.IncomingRequest.Headers[Key].Length > 0)
            {
                Value = WebOperationContext.Current.IncomingRequest.Headers[Key];
                return (true);
            }
            else
            {
                Value = string.Empty;
                return (false);
            }
        }

        private bool GetHeader(string Key, out Guid Value)
        {
            Guid gidTemp;

            if (WebOperationContext.Current.IncomingRequest.Headers[Key] != null &&
               ipoLib.TryParse(WebOperationContext.Current.IncomingRequest.Headers[Key], out gidTemp))
            {
                Value = gidTemp;
                return (true);
            }
            else
            {
                Value = Guid.Empty;
                return (false);
            }
        }

    }
}

