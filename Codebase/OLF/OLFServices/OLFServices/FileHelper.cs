﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace WFS.RecHub.OLFServices
{
    public class FileHelper
    {
        private readonly string _filePath;
        private readonly FileInfo _file;

        public FileHelper(string filePath)
        {
            _filePath = filePath;
            _file = new FileInfo(_filePath);
        }

        public virtual bool IsFileLocked()
        {
            FileStream stream = null;

            try
            {
                stream = _file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the _file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //_file is not locked
            return false;
        }
    }
}