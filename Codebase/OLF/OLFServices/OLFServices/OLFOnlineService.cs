﻿using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Activation;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.R360Shared;
using System.Data;
using System.Threading;
using WFS.RecHub.OLFServices.DataTransferObjects;
using System.Web;
using WFS.RecHub.OLFServicesAPI;
using System.Collections.Generic;
using WFS.RecHub.OLFServicesAPI.helper;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/02/2011 
*    -Modified naming convention for module-level variables.
*    -Added directive to allow AspNet compatibility.
* CR 32188 JMC 03/30/2010
*     -Modified GetImageSize() to get the OnlineColorMode before the call to 
*      GetImageSize()
*     -Modified getRelativeImagePath() to get the OnlineColorMode before the 
*      call to GetImageSize()
* CR 33857 JNE 07/15/2011
*     - Move Common Code to _ServiceBase class
*     - Add logging when exception is thrown
* CR 51010 JMC 03/07/2012
*    -Changed MaxReceivedMessageSize from 64 mb (67108864 bytes) to 2 gb 
*     (2147483647 bytes)
* CR 52588 twe 10/15/2012
*    IF URL starts with https force security mode transport to use SSL (port 443)
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 143450 RDS 05/22/2014	Move Binding configuration to config files
*
* WI 143450 RDS 05/22/2014	Move Binding configuration to config files
*  -Added a new function GetImageSizeForACHOrWirePayment which returns image size as 1 
*   when the payment type is ACH or Wire Transfer.
*  -Update function GetImageSize to givea call to GetImageSizeForACHOrWirePayment
*  when image size reterived is zero
*  
* WI 158932 SAS 08/18/2014	
* Added function to return the image size for ACH and Wire Transfer Payment Type
* WI 167463 SAS 09/23/2014
*   Change done for retrieving image relative path.
* WI 175209 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176356 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName 
*   ******************************************************************************/
namespace WFS.RecHub.OLFServices
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class OLFOnlineService : _ServiceBase, IOLFOnlineService {
        private IOLFOnlineService _ImageService = null;
       
        private string GetBasePath(string FolderKey) {
            string strBasePath;
            if(FolderKey != null) {
                switch(FolderKey.ToLower().Trim()) {
                    case "onlinearchive":
                        strBasePath = this.SiteOptions.OnlineImagePath;
                        break;
                    default:
                        strBasePath = this.SiteOptions.imagePath;
                        break;
                }
            } else {
                strBasePath = this.SiteOptions.imagePath;
            }
            return(strBasePath);
        }
        
        //Constructor 
        public OLFOnlineService() { 
           String strTemp = "";
           int intTemp = 0;
           ServerFaultException error;
           try{
                strTemp = ipoINILib.IniReadValue("OnlineFileServices", INIKEY_INTERMEDIATESERVER);
                if(strTemp != null && strTemp.Length > 0){ // Make sure result is not null or empty.
                    if(int.TryParse(strTemp, out intTemp)) {
                        if(intTemp == 1) {
                            _IntermediateServer = true;
                        }else{
                            _IntermediateServer = false;
                        }
                    }else{
                        _IntermediateServer = false;
                    }
                }
            }catch(Exception ex){
                error = BuildIniReadException(ex,"OLFOnlineService","OnlineFileServices",INIKEY_INTERMEDIATESERVER);
                throw new FaultException<ServerFaultException>(error, error.message);
           }
           try{
                strTemp = ipoINILib.IniReadValue("OnlineFileServices",INIKEY_CHUNKSIZE);
                if ((strTemp != null) && (strTemp.Length>0)){
                    if(int.TryParse(strTemp, out intTemp)) {
                        _ChunkSize = intTemp;
                    }
                }
           }catch(Exception ex){
                error = BuildIniReadException(ex,"OLFOnlineService","OnlineFileServices",INIKEY_CHUNKSIZE);
                throw new FaultException<ServerFaultException>(error, error.message);
           }
        }

        // Put File       
        public void PutFile(UploadItem Item)
        {
            _SiteKey = Item.FileMetaData.SiteKey;
            ServerFaultException error;
            if (_IntermediateServer)
            {
                // Setup Communication Channel
                OpenCommunication();
                //Call Method
                _ImageService.PutFile(Item);
            }
            else
            {
                if (Item.FileMetaData.FileName == null)
                {
                    Exception ex = new Exception("Missing FileName");
                    error = BuildGeneralException(ex, "PutFile", "Missing Filename");
                    throw new FaultException<ServerFaultException>(error, error.message);
                 }
                 string FullPathName = Path.Combine(GetBasePath(Item.FileMetaData.FolderKey), Item.FileMetaData.FileName);
                if (FileExists(Item.FileMetaData))
                {
                    Exception ex = new Exception("Destination file " + FullPathName + " already exists.");
                    error = BuildGeneralException(ex, "PutFile", "File already exists");
                    throw new FaultException<ServerFaultException>(error, error.message);
                  }
                  FileStream TargetStream = null;
                  Stream SourceStream = Item.data;
                  
                try
                {
                    using (TargetStream = new FileStream(FullPathName, FileMode.Create, FileAccess.ReadWrite))
                    {
                        //read from input stream in chunks set by ini file and save to output stream
                        byte[] buffer = new byte[_ChunkSize];
                        int count = 0;
                        while ((count = SourceStream.Read(buffer, 0, _ChunkSize)) > 0)
                        {
                            TargetStream.Write(buffer, 0, count);
                        }
                        TargetStream.Close();
                        //TargetStream.Dispose();
                        SourceStream.Close();
                        SourceStream.Dispose();
                     }
                }
                catch (Exception ex)
                {
                    error = BuildGeneralException(ex, "PutFile", ex.Message.ToString());
                        throw new FaultException<ServerFaultException>(error, error.message);
                    }
             }
        }
        
        // Get File        
        public Stream GetFile(FileMetaData MetaData)
        {
            _SiteKey = MetaData.SiteKey;

            ServerFaultException error;        
            FileStream ReturnStream;
            if (_IntermediateServer){
                // Setup Communication Channel
                OpenCommunication();
                //Set Parameters
                WFS.RecHub.OLFServices.FileMetaData TempMetaData = new WFS.RecHub.OLFServices.FileMetaData();
                TempMetaData.EmulationID  = MetaData.EmulationID;
                TempMetaData.FileName = MetaData.FileName;
                TempMetaData.SessionID = MetaData.SessionID;
                TempMetaData.SiteKey = MetaData.SiteKey;
                TempMetaData.FolderKey = MetaData.FolderKey;
                //Call Method
                return _ImageService.GetFile(TempMetaData);
            }else{
                String FullPathName = Path.Combine(GetBasePath(MetaData.FolderKey), MetaData.FileName);
                if (FileExists(MetaData)){
                    try{
                        ReturnStream = File.OpenRead(FullPathName);
                        return ReturnStream;
                     }catch (IOException ex){
                        error = BuildGeneralException(ex,"GetFile","An exception thrown while opening file");
                        throw new FaultException<ServerFaultException>(error, error.message);
                     }
                  }else{
                    Exception ex = new Exception("File "+ FullPathName +" does not exist.");
                    error = BuildGeneralException(ex,"GetFile","File does not exist");
                    throw new FaultException<ServerFaultException>(error, error.message);
                  }
            }     
        }
        // Check if File exists
        public bool FileExists(FileMetaData MetaData)
        {
            _SiteKey = MetaData.SiteKey;
            ServerFaultException error;
            if (_IntermediateServer) {
                // Setup Communication Channel
                OpenCommunication();
                //Call Method
                return _ImageService.FileExists(MetaData);
            }else{
                string FullPathName = Path.Combine(GetBasePath(MetaData.FolderKey), MetaData.FileName);
                try{
                    return File.Exists(FullPathName);
                }catch(Exception ex){
                    error = BuildGeneralException(ex,"FileExists","Error checking if file "+ FullPathName +" exists.");
                    throw new FaultException<ServerFaultException>(error, error.message);
                }
             }
        }
        //Get File Size
        public long GetFileSize(FileMetaData MetaData)
        {
            _SiteKey = MetaData.SiteKey;
            ServerFaultException error;
            if (_IntermediateServer) {
                // Setup Communication Channel
                OpenCommunication();
                //Set Parameters
                WFS.RecHub.OLFServices.FileMetaData TempMetaData = new WFS.RecHub.OLFServices.FileMetaData();
                TempMetaData.EmulationID  = MetaData.EmulationID;
                TempMetaData.FileName = MetaData.FileName;
                TempMetaData.SessionID = MetaData.SessionID;
                TempMetaData.SiteKey = MetaData.SiteKey;
                TempMetaData.FolderKey = MetaData.FolderKey;
                // Call method
                return _ImageService.GetFileSize(TempMetaData);
            }else{
                string FullPathName = Path.Combine(GetBasePath(MetaData.FolderKey), MetaData.FileName);

                EventLog.logEvent(string.Format("GetFileSize: FileName:{0}, FolderKey:{1}, FullPath: {2}", MetaData.FileName, MetaData.FolderKey, FullPathName), 
                    "OLFOnlineService",  MessageImportance.Debug);

                if (FileExists(MetaData)){
                    try{
                        FileInfo fi = new FileInfo(FullPathName);
                        return fi.Length;
                    }catch(Exception ex){
                        error = BuildGeneralException(ex,"GetFileSize","Error while getting file size of "+ MetaData.FileName +".");
                        throw new FaultException<ServerFaultException>(error, error.message);
                    }
                }else{
                    Exception ex = new Exception("File "+ FullPathName +" does not exist.");
                    error = BuildGeneralException(ex,"GetFileSize","File does not exist");
                    throw new FaultException<ServerFaultException>(error, error.message);
                }
            }
        }
        // Ping
        public string Ping()
        {
           ServerFaultException error;
           EventLog.logEvent("Ping Requested", MessageImportance.Debug);
           try{
               if (_IntermediateServer){
                    // Setup Communication Channel
                    OpenCommunication();
                    // Call method
                    return _ImageService.Ping();
               }else{
                    return "Pong";
               }
            } catch(Exception ex) {
                error = BuildGeneralException(ex, "Ping", "A general exception occurred while executing Ping() method");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }
        // Opens the communication channel to service
        public void OpenCommunication() 
        {
			try
			{
				if (LogManager.IsDefault) LogManager.Logger = EventLog.IPOtoILogger(this.GetType().Name);
				_ImageService = R360ServiceFactory.Create<IOLFOnlineService>();
			}
			catch (FaultException ex)
			{
				throw;
			}
			catch (Exception ex)
			{
				ServerFaultException error = BuildCommunicationsChannelException(ex, "OpenCommunication", "An undefined exception occurred.");
				throw new FaultException<ServerFaultException>(error, error.message);
			}
        }    

        /// <summary>
        /// Returns the Image Size for ACH and Wire Transfer Payment Type
        /// </summary>
        /// <param name="MetaData"></param>
        /// <param name="PICSDate"></param>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="BatchID"></param>
        /// <returns></returns>
        private long GetImageSizeForACHOrWirePayment(FileMetaData MetaData,                                 
                                 string PICSDate,
                                 int BankID,
                                 int LockboxID,
                                 long BatchID)
        {
            long lngRetVal = 0;
            Int16 iPaymentTypeKey = -1;
            WFS.RecHub.DAL.cItemProcDAL ItemProcDAL;
            ServerFaultException error;
            try
            {
                ItemProcDAL = new WFS.RecHub.DAL.cItemProcDAL(MetaData.SiteKey);
                DateTime dtDepositDate;
                dtDepositDate = DateTime.ParseExact(PICSDate, "yyyyMMdd", null);
                DataTable dt = new DataTable();
                if (ItemProcDAL.GetPaymentTypeKey(BankID,
                                                   LockboxID,
                                                  dtDepositDate,
                                                   BatchID,
                                                   out dt) && dt.Rows.Count > 0)
                iPaymentTypeKey = Convert.ToInt16(dt.Rows[0]["BatchPaymentTypeKey"]);
                if ((BatchPaymentType)iPaymentTypeKey == BatchPaymentType.ACH || (BatchPaymentType)iPaymentTypeKey == BatchPaymentType.WIRE)
                    lngRetVal= 1;                
                
            }
            catch(Exception ex)
            {
                lngRetVal = 0;
                error = BuildGeneralException(ex, "GetImageSizeForACHOrWirePayment", "Error in GetImageSizeForACHOrWirePayment");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
            return lngRetVal;
        }    

        
        public long GetImageSize(FileMetaData MetaData,
                                 byte OnlineColorMode,
                                 string PICSDate, 
                                 int BankID, 
                                 int LockboxID, 
                                 long BatchID, 
                                 int Item, 
                                 string ImageType, 
                                 short Page,
                                 long sourceBatchID,
                                 string BatchSourceShortName,
                                 string ImportTypeShortName)
        {

            long lngRetVal = 0;
            string strFileName = string.Empty;
            WFS.RecHub.Common.cPICS objPICS; 
            WFS.RecHub.Common.cFILEGROUP objFILEGROUP;
            WFS.RecHub.DAL.cItemProcDAL ItemProcDAL;

            var siteoptions = new cSiteOptions(MetaData.SiteKey);

            EventLog.logEvent("Image Path: " + siteoptions.imagePath, "OLFOnlineService", MessageImportance.Debug);

            EventLog.logEvent(string.Format("GetImageSize: FolderKey:{0}, ColorMode:{1}, PICSDate:{2}, BankID:{3}, LockboxID:{4}, BatchID:{5}, Item:{6}, ImageType:{7}, Page:{8}, SourceBatchID:{9}, BatchSourceShortName:{10}, ImportTypeShortName:{11}",
                MetaData.FolderKey, OnlineColorMode, PICSDate, BankID, LockboxID, BatchID, Item, ImageType, Page, sourceBatchID, BatchSourceShortName, ImportTypeShortName), 
                "OLFOnlineService", MessageImportance.Essential);
           
            ServerFaultException error;
            try {
                if (_IntermediateServer) {
                    // Setup Communication Channel
                    OpenCommunication();
                    //Set Parameters
                    WFS.RecHub.OLFServices.FileMetaData TempMetaData = new WFS.RecHub.OLFServices.FileMetaData();
                    TempMetaData.EmulationID  = MetaData.EmulationID;
                    TempMetaData.FileName = MetaData.FileName;
                    TempMetaData.SessionID = MetaData.SessionID;
                    TempMetaData.SiteKey = MetaData.SiteKey;
                    TempMetaData.FolderKey = MetaData.FolderKey;
                    // Call method
                    return _ImageService.GetImageSize(TempMetaData,
                                                     OnlineColorMode,
                                                     PICSDate, 
                                                     BankID, 
                                                     LockboxID, 
                                                     BatchID, 
                                                     Item, 
                                                     ImageType, 
                                                     Page,
                                                     sourceBatchID,
                                                     BatchSourceShortName,
                                                     ImportTypeShortName);
                }else{
                    _SiteKey = MetaData.SiteKey;
                    if (siteoptions.imageStorageMode == ImageStorageMode.FILEGROUP)
                    {
                         objFILEGROUP = new WFS.RecHub.Common.cFILEGROUP(MetaData.SiteKey);
                         ItemProcDAL = new WFS.RecHub.DAL.cItemProcDAL(MetaData.SiteKey);
                        // Define default image storage location
                         string strRootImageLocation = siteoptions.imagePath;
                        DataTable dt = null;
                        // Lookup lockbox
                        if (ItemProcDAL.GetLockbox(BankID,LockboxID,out dt)){
                            if (dt != null && dt.Rows.Count>0 && dt.Rows[0]["FileGroup"] != DBNull.Value){
                                string strFileGroup = dt.Rows[0]["FileGroup"].ToString();
                                if (!string.IsNullOrWhiteSpace(strFileGroup)){
                                    if (Directory.Exists(strFileGroup)){
                                        strRootImageLocation = strFileGroup;
                                    }
                                }
                            }
                        }
                        if (dt != null){
                            dt.Dispose();
                        }

                        strFileName = objFILEGROUP.getRelativeImagePath(OnlineColorMode,
                                                                   PICSDate,
                                                                   BankID,
                                                                   LockboxID,
                                                                   BatchID,
                                                                   Item,
                                                                   ImageType,
                                                                   Page,
                                                                   strRootImageLocation,
                                                                   sourceBatchID,
                                                                   BatchSourceShortName,
                                                                   ImportTypeShortName);
                        if(strFileName.Length > 0 && File.Exists(Path.Combine(strRootImageLocation, strFileName))) {
                            FileInfo fi = new FileInfo(Path.Combine(strRootImageLocation, strFileName));
                            lngRetVal = fi.Length;
                        } else {
                            lngRetVal = 0;
                        }

                    }else{
                        objPICS = new WFS.RecHub.Common.cPICS(MetaData.SiteKey);

                        strFileName = objPICS.getRelativeImagePath(OnlineColorMode,
                                                                   PICSDate,
                                                                   BankID,
                                                                   LockboxID,
                                                                   BatchID,
                                                                   Item,
                                                                   ImageType,
                                                                   Page,
                                                                   siteoptions.imagePath,
                                                                   sourceBatchID,
                                                                   BatchSourceShortName,
                                                                   ImportTypeShortName);
                        if (strFileName.Length > 0 && File.Exists(Path.Combine(siteoptions.imagePath, strFileName)))
                        {
                            FileInfo fi = new FileInfo(Path.Combine(siteoptions.imagePath, strFileName));
                            lngRetVal = fi.Length;
                        } else {
                            lngRetVal = 0;
                        }
                    }
                    
                }
                if(lngRetVal==0)
                {
                    lngRetVal = GetImageSizeForACHOrWirePayment(MetaData, PICSDate, BankID, LockboxID, BatchID);
                }

            } catch(Exception ex) {
                lngRetVal = 0;
                error = BuildGeneralException(ex, "GetImageSize", "Error in GetImageSize");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
            return(lngRetVal);
        }

        public Stream GetImageJobFileAsAttachment(FileMetaData MetaData, Guid OnlineImageQueueID) {
            string strFileName;
            ServerFaultException error;
            try {
                if (_IntermediateServer) {
                    // Setup Communication Channel
                    OpenCommunication();
                    //Set Parameters
                    WFS.RecHub.OLFServices.FileMetaData TempMetaData = new WFS.RecHub.OLFServices.FileMetaData();
                    TempMetaData.EmulationID  = MetaData.EmulationID;
                    TempMetaData.FileName = MetaData.FileName;
                    TempMetaData.SessionID = MetaData.SessionID;
                    TempMetaData.SiteKey = MetaData.SiteKey;
                    TempMetaData.FolderKey = MetaData.FolderKey;
                    // Call method
                    return _ImageService.GetImageJobFileAsAttachment(TempMetaData,
                                                                    OnlineImageQueueID);
                } else {
                    _SiteKey = MetaData.SiteKey;
                    if(SiteOptions.OnlineImagePath.Length <= 0) {
                        throw(new Exception("OnlineImagePath not defined or not available."));
                    }
                    if(GetImageJobSize(MetaData, OnlineImageQueueID) > 0) {
                        // File Name
                        if(GetImageJobFileName(OnlineImageQueueID, out strFileName)) {
                            var attempts = 0;
                            var helper = new FileHelper(strFileName);
                            while (helper.IsFileLocked())
                            {
                                attempts++;
                                Thread.Sleep(TimeSpan.FromSeconds(1));
                                if (attempts > 10) break;
                            }

                            return File.Open(strFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite); 


                        } else {
                            throw(new Exception("File not found."));
                        }
                    } else {
                        return(null);
                    }
                }
            } catch(Exception ex) {
                error = BuildGeneralException(ex, "GetImageJobFileAsAttachment", "Error in GetImageJobFileAsAttachment");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }

        public long GetImageJobSize(FileMetaData MetaData, Guid OnlineImageQueueID) {
            ServerFaultException error;
            long lngRetVal = 0;
            string strFileName;
            try {
                if (_IntermediateServer) {
                    // Setup Communication Channel
                    OpenCommunication();
                    //Set Parameters
                    WFS.RecHub.OLFServices.FileMetaData TempMetaData = new WFS.RecHub.OLFServices.FileMetaData();
                    TempMetaData.EmulationID  = MetaData.EmulationID;
                    TempMetaData.FileName = MetaData.FileName;
                    TempMetaData.SessionID = MetaData.SessionID;
                    TempMetaData.SiteKey = MetaData.SiteKey;
                    TempMetaData.FolderKey = MetaData.FolderKey;
                    // Call method
                    return _ImageService.GetImageJobSize(TempMetaData, OnlineImageQueueID);
                } else {
                    _SiteKey = MetaData.SiteKey;
                    if(GetImageJobFileName(OnlineImageQueueID, out strFileName)) {
                        FileInfo fi = new FileInfo(strFileName);
                        lngRetVal = fi.Length;
                    } else {
                        lngRetVal = 0;
                    }
                }
            } catch(Exception ex) {
                lngRetVal = 0;
                error = BuildGeneralException(ex, "GetImageJobSize", "Error in GetImageJobSize");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
            return(lngRetVal);
        }

        public Stream GetCENDSFileAsAttachment(FileMetaData MetaData, string FilePath) {
            ServerFaultException error;
            string strFileName;
            try {
                if (_IntermediateServer) {
                    // Setup Communication Channel
                    OpenCommunication();
                    //Set Parameters
                    WFS.RecHub.OLFServices.FileMetaData TempMetaData = new WFS.RecHub.OLFServices.FileMetaData();
                    TempMetaData.EmulationID  = MetaData.EmulationID;
                    TempMetaData.FileName = MetaData.FileName;
                    TempMetaData.SessionID = MetaData.SessionID;
                    TempMetaData.SiteKey = MetaData.SiteKey;
                    TempMetaData.FolderKey = MetaData.FolderKey;
                    // Call method
                    return _ImageService.GetCENDSFileAsAttachment(TempMetaData, FilePath);
                } else {
                    _SiteKey = MetaData.SiteKey;
                    if(SiteOptions.CENDSPath.Length <= 0) {
                        throw(new Exception("CENDSPath not defined or not available."));
                    }
                    if(GetCENDSFileSize(MetaData, FilePath) > 0) {

                        // File Name
                        if(GetCENDSFileName(FilePath, out strFileName)) {
                            //MemoryStream ms = new MemoryStream(File.ReadAllBytes(strFileName));
                            //File.ReadAllBytes(Path.Combine(SiteOptions.OnlineImagePath, strFileName));
                            return(File.OpenRead(strFileName));
                        } else {
                            throw(new Exception("File not found."));
                        }

                    } else {
                        return(null);
                    }
                }
            } catch(Exception ex) {
                error = BuildGeneralException(ex, "GetCENDSFileAsAttachment", "Error in GetCENDSFileAsAttachment");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }

        public long GetCENDSFileSize(FileMetaData MetaData,string FilePath) {
            ServerFaultException error;
            long lngRetVal = 0;
            string strFileName;
            try {
                if (_IntermediateServer) {
                    // Setup Communication Channel
                    OpenCommunication();
                    //Set Parameters
                    WFS.RecHub.OLFServices.FileMetaData TempMetaData = new WFS.RecHub.OLFServices.FileMetaData();
                    TempMetaData.EmulationID  = MetaData.EmulationID;
                    TempMetaData.FileName = MetaData.FileName;
                    TempMetaData.SessionID = MetaData.SessionID;
                    TempMetaData.SiteKey = MetaData.SiteKey;
                    TempMetaData.FolderKey = MetaData.FolderKey;
                    // Call method
                    return _ImageService.GetCENDSFileSize(TempMetaData, FilePath);
                } else {
                    _SiteKey = MetaData.SiteKey;

                    if(GetCENDSFileName(FilePath, out strFileName)) {
                        FileInfo fi = new FileInfo(strFileName);
                        lngRetVal = fi.Length;
                    } else {
                        lngRetVal = 0;
                    }
                }
            } catch(Exception ex) {
                error = BuildGeneralException(ex, "GetCENDSFileSize", "Error in GetCENDSFileSize");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
            return(lngRetVal);
        }

        public ImageResponseDto DownloadSingleImage(SingleImageRequestDto request)
        {
            try
            {
                if (_IntermediateServer)
                {
                    // Setup Communication Channel
                    OpenCommunication();
                    // Call method
                    return _ImageService.DownloadSingleImage(request);
                }
                else
                {
                    // First do some Auth and Session Stuff.
                    
                    //Guid emulationID = new Guid(metaData.EmulationID);
                    EventLog.logEvent("Download Single Image", "DownloadSingleImage", MessageImportance.Debug);
                    return new cImageServices(request.SessionId, request.SiteKey).GetSingleImage(request);
                }
            }
            catch (Exception e)
            {
                var error = BuildGeneralException(e, "DownloadSingleImage", "Error in Download Single Image");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }
        public ImageResponseDto DownloadImage(FileMetaData metaData, ImageRequestDto request)
        {
            try
            {
                if (_IntermediateServer)
                {
                    // Setup Communication Channel
                    OpenCommunication();
                    //Set Parameters
                    FileMetaData TempMetaData = new FileMetaData();
                    TempMetaData.EmulationID = metaData.EmulationID;
                    TempMetaData.FileName = metaData.FileName;
                    TempMetaData.SessionID = metaData.SessionID;
                    TempMetaData.SiteKey = metaData.SiteKey;
                    TempMetaData.FolderKey = metaData.FolderKey;

                    // Call method
                    return _ImageService.DownloadImage(TempMetaData, request);
                }
                else
                {
                    // First do some Auth and Session Stuff.
                    _SiteKey = metaData.SiteKey;
                    Guid sessionID = new Guid(metaData.SessionID);
                    Guid emulationID = new Guid(metaData.EmulationID);
                    EventLog.logEvent("Entered Download Image", "DOWNLOAD-IMAGE", MessageImportance.Debug);
                    return new cImageServices(sessionID, _SiteKey).DownloadImage(request);
                }
            }
            catch (Exception ex)
            {
                var error = BuildGeneralException(ex, "DownloadImage", "Error in Download Image");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }

        public OlfResponseDto ImagesExist(ImageRequestDto request)
        {
            _SiteKey = request.SiteKey;
            ServerFaultException error;
            if (_IntermediateServer)
            {
                // Setup Communication Channel
                OpenCommunication();
                //Call Method
                return _ImageService.ImagesExist(request);
            }
            else
            {
                try
                {
                    LocateImages locateImages = new LocateImages(EventLog, SiteOptions);
                    return locateImages.AnyImagesExist(request);
                }
                catch (Exception ex)
                {
                    error = BuildGeneralException(ex, "ImagesExist", "Error checking if any images exist.");
                    throw new FaultException<ServerFaultException>(error, error.message);
                }
            }
        }

        public OlfResponseDto BatchSequenceImagesExist(ImageRequestDto request)
        {
            _SiteKey = request.SiteKey;
            ServerFaultException error;
            if (_IntermediateServer)
            {
                // Setup Communication Channel
                OpenCommunication();
                //Call Method
                return _ImageService.BatchSequenceImagesExist(request);
            }
            else
            {
                try
                {
                    LocateImages locateImages = new LocateImages(EventLog, SiteOptions);
                    return locateImages.BatchSequenceImagesExist(request);
                }
                catch (Exception ex)
                {
                    error = BuildGeneralException(ex, "ImagesExist", "Error checking if any images exist.");
                    throw new FaultException<ServerFaultException>(error, error.message);
                }
            }
        }


        private bool GetImageJobFileName(Guid OnlineImageQueueID, out string FileName) {
            ServerFaultException error;
            string strFileDir;
            string[] arFiles;
            string strFileName = string.Empty;
            FileName = string.Empty;
            bool bolRetVal = false;
            try{
                strFileDir = SiteOptions.OnlineImagePath;
                arFiles = Directory.GetFiles(SiteOptions.OnlineImagePath,  OnlineImageQueueID.ToString() + ".*");
                foreach(string strTemp in arFiles) {
                    if(!strTemp.ToLower().EndsWith(".xml") && !strTemp.ToLower().EndsWith(".klg")) {
                        strFileName = strTemp;
                        bolRetVal = true;
                        break;
                    }
                }
                if(bolRetVal) {
                    FileName = Path.Combine(SiteOptions.OnlineImagePath, strFileName);
                } 
            }catch(Exception ex){
                error = BuildGeneralException(ex, "GetImageJobFileName", "Error in GetImageJobFileName");
                throw new FaultException<ServerFaultException>(error, error.message);
            }            
            return(bolRetVal);
        }

        private bool GetCENDSFileName(string RelativeFilePath, out string FullFilePath) {
            ServerFaultException error;
            string strFileName = string.Empty;
            FullFilePath = string.Empty;
            bool bReturn = false;
            try{
                strFileName = Path.Combine(SiteOptions.CENDSPath, RelativeFilePath);
                if(File.Exists(strFileName)) {
                    FullFilePath = strFileName;
                    bReturn = true;
                } 
            }catch(Exception ex){
                error = BuildGeneralException(ex, "GetCENDSFileName", "Error in GetCENDSFileName");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
            return(bReturn);
        }

        public IEnumerable<SingleImageRequestDto> GetAvailablePaymentsImages(IEnumerable<ItemRequestDto> payments, string sitekey, Guid sessionid)
        {
            try
            {
                if (_IntermediateServer)
                {
                    // Setup Communication Channel
                    OpenCommunication();
                    // Call method
                    return _ImageService.GetAvailablePaymentsImages(payments, sitekey, sessionid);
                }
                else
                {
                    // First do some Auth and Session Stuff.

                    //Guid emulationID = new Guid(metaData.EmulationID);
                    EventLog.logEvent("GetAvailablePaymentsImages", "GetAvailablePaymentsImages", MessageImportance.Debug);
                    return new cImageServices(sessionid, sitekey).GetAvailableItemImages(payments, true);
                }
            }
            catch (Exception e)
            {
                var error = BuildGeneralException(e, "DownloadSingleImage", "Error in Download Single Image");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }

        public IEnumerable<SingleImageRequestDto> GetAvailableStubsImages(IEnumerable<ItemRequestDto> stubs, string sitekey, Guid sessionid)
        {
            try
            {
                if (_IntermediateServer)
                {
                    // Setup Communication Channel
                    OpenCommunication();
                    // Call method
                    return _ImageService.GetAvailableStubsImages(stubs, sitekey, sessionid);
                }
                else
                {
                    // First do some Auth and Session Stuff.

                    //Guid emulationID = new Guid(metaData.EmulationID);
                    EventLog.logEvent("GetAvailableStubsImages", "GetAvailableStubsImages", MessageImportance.Debug);
                    return new cImageServices(sessionid, sitekey).GetAvailableItemImages(stubs, false);
                }
            }
            catch (Exception e)
            {
                var error = BuildGeneralException(e, "DownloadSingleImage", "Error in Download Single Image");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }

        public IEnumerable<SingleImageRequestDto> GetAvailableDocumentImages(IEnumerable<ItemRequestDto> docs,
            string sitekey, Guid sessionid)
        {
            try
            {
                if (_IntermediateServer)
                {
                    // Setup Communication Channel
                    OpenCommunication();
                    // Call method
                    return _ImageService.GetAvailableDocumentImages(docs, sitekey, sessionid);
                }
                EventLog.logEvent("GetAvailableStubsImages", "GetAvailableStubsImages", MessageImportance.Debug);
                return new cImageServices(sessionid, sitekey).GetAvailableItemImages(docs, false);

            }
            catch (Exception e)
            {
                var error = BuildGeneralException(e, "GetAvailableDocumentImages", "Error in Download Single Image");
                throw new FaultException<ServerFaultException>(error, error.message);
            }
        }
    }      
}
