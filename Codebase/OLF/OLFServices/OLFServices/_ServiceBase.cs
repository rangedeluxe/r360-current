using System;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Jason Efken
* Date: 7/15/2011
*
* Purpose: Base Class for OLFOnlineService and OLFService
*
* Modification History
* CR 33857 JNE 07/15/2011
*     - Move Common Code from OLFOnlineService & OLFService to this class
*     - Add logging when exception is thrown
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
******************************************************************************/

namespace WFS.RecHub.OLFServices {
     public class _ServiceBase{
        protected cSiteOptions _SiteOptions = null;
        protected cEventLog _EventLog = null;
        protected string _SiteKey = null;
        protected bool _IntermediateServer = false;
        protected int _ChunkSize = 4096;

        protected const string INIKEY_INTERMEDIATESERVER = "IntermediateServer";
        protected const string INIKEY_CHUNKSIZE = "ChunkSize";
        protected const string INIKEY_SERVERLOCATION = "ServerLocation";
        protected const string HTTPHEADER_SITEKEY = "SiteKey";
        protected const string HTTPHEADER_SESSIONID = "SessionID";

        protected _ServiceBase() {
        }

        protected cSiteOptions SiteOptions{
            get{
                if (_SiteOptions == null){
                    _SiteOptions = new cSiteOptions(_SiteKey);
                }
                return _SiteOptions;
            }
        }
        protected cEventLog EventLog{
            get{
                if (_EventLog == null){
                    _EventLog = new cEventLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (MessageImportance)SiteOptions.loggingDepth);
                }
                return _EventLog;
            }
        }
        

        protected ServerFaultException BuildGeneralException(Exception ex, string procName, string Msg) {
            EventLog.logEvent("A general exception occurred", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(OLFServiceErrorCodes.UndefinedError, Msg, Msg));
        }

        protected ServerFaultException BuildIniReadException(Exception ex, string procName, string section, string key) {
            EventLog.logEvent("Unable to read ini setting: [" + section + "]." + key, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(OLFServiceErrorCodes.ConfigurationError, 
                                             "A configuration error occurred at the server",
                                             "A configuration error occurred at the server"));
        }

        protected ServerFaultException BuildCommunicationsChannelException(Exception ex, string procName, string Msg) {
            EventLog.logEvent("Unable to establish communications channel - " + Msg, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(OLFServiceErrorCodes.CommunicationError, 
                                             "A communications channel occurred at the server",
                                             "A communications channel occurred at the server"));
        }

        private static ServerFaultException BuildServerFaultException(OLFServiceErrorCodes ErrorCode, string Msg, string Details) {

            ServerFaultException objError;

            objError = new ServerFaultException();
            objError.errorcode = "Error";
            objError.message = ((int)ErrorCode).ToString() + " - " + Msg;
            objError.details = Details;

            return(objError);
        }
    }    
}
