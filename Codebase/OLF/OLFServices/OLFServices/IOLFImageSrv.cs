﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Services;
using System.Web.Services.Protocols;

using WFS.integraPAY.Online.Common;
using WFS.integraPAY.Online.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/02/2011 
*    -Added IOLFService interface.
* CR 32188 JMC 03/30/2010
*   -Modified GetImageSize() to accept OnlineColorMode parameter to eliminate
*    the call to the database.
* CR 45986 WJS 11/23/2001
*	- Add ability pass in SiteID
* WI 199732 TWE 04/06/2015
*   Remove ProcessingDateKey;  Add ImmutableDateKey, DepositDateKey
******************************************************************************/
namespace WFS.integraPAY.Online.OLFServices {

    //**************************************
    // OLFOnlineService
    //**************************************
    // NOTE: If you change the interface name "IOLFOnlineService" here, you must also update the reference to "IOLFOnlineService" in Web.config.
    [ServiceContract]
    public interface IOLFOnlineService {

        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "PutFile")]
        [FaultContract(typeof(ServerFaultException))]
        void PutFile(UploadItem Item);

        [OperationContract(Action = "GetFile")]
        [FaultContract(typeof(ServerFaultException))]
        Stream GetFile(FileMetaData MetaData);

        [OperationContract(Action = "GetFileSize")]
        [FaultContract(typeof(ServerFaultException))]
        long GetFileSize(FileMetaData MetaData);

        [OperationContract(Action = "FileExists")]
        [FaultContract(typeof(ServerFaultException))]
        bool FileExists(FileMetaData MetaData);

        [OperationContract(Action = "GetImageSize")]
        long GetImageSize(FileMetaData MetaData, byte OnlineColorMode, string PICSDate, int BankID, int LockboxID, long BatchID, int Item, string ImageType, short Page);

        [OperationContract(Action = "GetImageJobFileAsAttachment")]
        Stream GetImageJobFileAsAttachment(FileMetaData MetaData, Guid OnlineImageQueueID);

        [OperationContract(Action = "GetImageJobSize")]
        long GetImageJobSize(FileMetaData MetaData, Guid OnlineImageQueueID);

        [OperationContract(Action = "GetCENDSFileAsAttachment")]
        Stream GetCENDSFileAsAttachment(FileMetaData MetaData, string FilePath);

        [OperationContract(Action = "GetCENDSFileSize")]
        long GetCENDSFileSize(FileMetaData MetaData, string FilePath);

        [OperationContract(Action = "DownloadImage")]
        ImageResponseDto DownloadImage(FileMetaData metaData, ImageRequestDto request);
    }

    //**************************************
    // OLFService
    //**************************************
    [ServiceContract]
    public interface IOLFService {

        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "GetSession")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool GetSession(string LogonName,
                        string Password,
                        out Guid SessionID);

        [OperationContract(Action = "EndSession")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool EndSession();

        [OperationContract(Action = "GetImage")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ImageNotFoundInDbFault))]
        [FaultContract(typeof(ImageNotFoundOnDiskFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool GetImage(string encLogonName,
                      string encPassword,
                      int BankID,
                      int LockboxID,
                      int ImmutableDateKey,
                      int DepositDateKey,
                      int BatchID,
                      int BatchSequence,
                      bool IsCheck,
                      string PaymentSource,
                      out byte[][] ImageList,
                      out string FileDescriptor,
                      out string FileExtension,
                      out Guid sessionID);
    }

    /* Message Contracts */
    [MessageContract]
    public class UploadItem {

        [MessageHeader(MustUnderstand = true)]
        public FileMetaData FileMetaData;

        [MessageBodyMember]
        public Stream data;
    }

    //**************************************
    // OLFConsolidatorService
    //**************************************
    [ServiceContract]
    public interface IOLFConsolidatorService {

        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "PutFile")]
        [FaultContract(typeof(ServerFaultException))]
        void PutFile(UploadItem Item);

        [OperationContract(Action = "GetFile")]
        [FaultContract(typeof(ServerFaultException))]
        Stream GetFile(FileMetaData MetaData);

        [OperationContract(Action = "GetFileSize")]
        [FaultContract(typeof(ServerFaultException))]
        long GetFileSize(FileMetaData MetaData);

        [OperationContract(Action = "FileExists")]
        [FaultContract(typeof(ServerFaultException))]
        bool FileExists(FileMetaData MetaData);
    }

    //**************************************
    // Common Data Contracts
    //**************************************
    [DataContract]
    public class ServerFaultException {

        [DataMember]
        public string errorcode;

        [DataMember]
        public string message;

        [DataMember]
        public string details;
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSiteKeyFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSiteKeyFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSiteKeyFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.InvalidSiteKey, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSessionIDFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSessionIDFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSessionIDFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.InvalidSessionID, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SessionExpiredFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class SessionExpiredFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public SessionExpiredFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.SessionExpired, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSessionFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSessionFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSessionFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.InvalidSession, source, message) {
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ImageNotFoundInDbFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class ImageNotFoundInDbFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public ImageNotFoundInDbFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.ImageNotFoundInDb, source, message) {
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ImageNotFoundOnDiskFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class ImageNotFoundOnDiskFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public ImageNotFoundOnDiskFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.ImageNotFoundOnDisk, source, message) {
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [DataContract]
    public class FileMetaData {

        [DataMember]
        public string SessionID;

        [DataMember]
        public string SiteKey;

        [DataMember]
        public string EmulationID;

        [DataMember]
        public string FileName;

        [DataMember]
        public string FolderKey;
    }
}
