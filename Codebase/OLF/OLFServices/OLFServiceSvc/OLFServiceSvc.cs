﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Collections;
using System.Text;
using WFS.RecHub.Common.Log;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* WI 98043 WJS 04/19/2013
*   -Initial Version.
******************************************************************************/
namespace WFS.RecHub.OLFServices.OLFServiceSvc 
{
    public partial class OLFServiceSvc : ServiceBase
    {

        //move this to ipoLib at some point
        public const string INISECTION_OLFSERVICE_SERVICE = "OLFServiceService";
        private System.ComponentModel.IContainer components = null;
        private cEventLog _EventLog = null;
        private cOLFServiceSvcOptions _ServerOptions = null;
        private cWCFHostBase _whbWebHost = null;

        /// <summary>
        /// 
        /// </summary>
        private cEventLog eventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(serverOptions.logFilePath
                                                , serverOptions.logFileMaxSize
                                                , (MessageImportance)serverOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        public OLFServiceSvc()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        private cOLFServiceSvcOptions serverOptions
        {
            get
            {
                try
                {
                    if (_ServerOptions == null)
                    {
                        _ServerOptions = new cOLFServiceSvcOptions(INISECTION_OLFSERVICE_SERVICE);
                    }
                }
                catch { }

                return _ServerOptions;
            }
        }
        protected override void OnStart(string[] args)
        {
            eventLog.logEvent("Attempting to host WCF OLFService. Value of HostOLFService is:" + serverOptions.Host_WCF_OLFServices.ToString(), "OLFServiceServices",
                       MessageType.Information, MessageImportance.Debug);
            string[] asConfigError = serverOptions.CheckSettings();
            if (asConfigError.Length > 0)
            {
                foreach (string sCurError in asConfigError)
                {
                    eventLog.logEvent(sCurError, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                }
                base.Stop();
            }
            else if (serverOptions.Host_WCF_OLFServices)
            {

                eventLog.logEvent("Attempting to host WCF OLFService", "OLFServiceServices",
                        MessageType.Information, MessageImportance.Debug);
                try
                {
                    Dictionary<string, object> dctHostedItems = new Dictionary<string, object>();

                    if (serverOptions.OLFService_URL != "")
                    {
                        dctHostedItems.Add(serverOptions.OLFService_URL, new OLFService());
                    }
                    if (serverOptions.OLFOnlineService_URL != "")
                    {
                        dctHostedItems.Add(serverOptions.OLFOnlineService_URL, new OLFOnlineService());
                    }

                    eventLog.logEvent("OLF Service URL to host: " + serverOptions.OLFService_URL, "OLFServiceServices",
                               MessageType.Information, MessageImportance.Debug);
                    eventLog.logEvent("OLF Online Service URL to host: " + serverOptions.OLFOnlineService_URL, "OLFServiceServices",
                           MessageType.Information, MessageImportance.Debug);
                    _whbWebHost = new cWCFHostBase(dctHostedItems);
                    _whbWebHost.LogEvent += new dlgLogEvent(_whbWebHost_LogEvent);
                    _whbWebHost.Open();
                    eventLog.logEvent("Hosting WCF OLF Service", "OLFServiceServices",
                            MessageType.Information, MessageImportance.Essential);
                }
                catch (Exception ex)
                {
                    eventLog.logEvent(string.Format("Failed to host WCF OLFServices: {0}", ex.Message),
                            "OLFServiceServices", MessageType.Error, MessageImportance.Essential);
                }
            }

            eventLog.logEvent("OLFService Service started normally."
                            , this.GetType().Name
                            , MessageImportance.Essential);
        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (_whbWebHost != null)
            {
                _whbWebHost.Close();
                _whbWebHost.Dispose();
            }
            base.Dispose(disposing);
        }
        protected override void OnStop()
        {


        

            if (_whbWebHost != null)
            {
                _whbWebHost.Close();
                _whbWebHost.Dispose();
                _whbWebHost = null;
            }
        }
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.ServiceName = "OLFService Service";
        }
        void _whbWebHost_LogEvent(string sMessage, string sSource, MessageType mstEventType, MessageImportance msiEventImportance)
        {
            eventLog.logEvent(sMessage, sSource, mstEventType, msiEventImportance);
        }
    }
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new OLFServiceSvc() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
