﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* WI 98043 WJS 04/19/2013
*   -Initial Version.
******************************************************************************/
namespace WFS.RecHub.OLFServices.OLFServiceSvc 
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OLFServiceServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.OLFServiceServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // OLFServiceServiceProcessInstaller
            // 
            this.OLFServiceServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.OLFServiceServiceProcessInstaller.Password = null;
            this.OLFServiceServiceProcessInstaller.Username = null;
            // 
            // OLFServiceServiceInstaller
            // 
            this.OLFServiceServiceInstaller.ServiceName = "WFS OLF Service Service";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.OLFServiceServiceProcessInstaller,
            this.OLFServiceServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller OLFServiceServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller OLFServiceServiceInstaller;
    }
}