using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   
*
* Purpose:  
*
* Modification History
* WI 98043 WJS 04/19/2013
*   -Initial Version.
******************************************************************************/
namespace WFS.RecHub.OLFServices.OLFServiceSvc
{

	/// <summary>
	/// Summary description for cOLFServiceSvcOptions.
	/// </summary>
	public class cOLFServiceSvcOptions : cSiteOptions {

        private const string INIKEY_HOST_WCF_OLFSERVICES = "HostWCFOLFServices";
        private const string INIKEY_OLFSERVICE_URL = "OLFService_URL";
        private const string INIKEY_OLFONLINESERVICE_URL = "OLFONLINEService_URL";

        //************************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteKey"></param>
        //************************************************************************
		public cOLFServiceSvcOptions(string siteKey) : base(siteKey) {

            StringCollection colSiteOptions = ipoINILib.GetINISection(siteKey);
            string strKey;
            string strValue;



            System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {
                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if (strKey.ToLower() == INIKEY_HOST_WCF_OLFSERVICES.ToLower()) {
                    List<string> lstTrueValues = new List<string>(new string[] {
                            "true", "t", "yes", "1"});
                    Host_WCF_OLFServices = lstTrueValues.Contains(strValue.ToLower());
                }
                else if (strKey.ToLower() == INIKEY_OLFSERVICE_URL.ToLower()) {
                    OLFService_URL = strValue;
                }
                else if (strKey.ToLower() == INIKEY_OLFONLINESERVICE_URL.ToLower()) {
                    OLFOnlineService_URL = strValue;
                }
            }
		}


        //************************************************************************
        /// <author>Charlie Johnson</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public bool Host_WCF_OLFServices {
            get;
            set;
        }

        //************************************************************************
        /// <author>Charlie Johnson</author> 
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public string OLFService_URL {
            get;
            set;
        }

        //************************************************************************
        /// <author>Charlie Johnson</author> 
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public string OLFOnlineService_URL {
            get;
            set;
        }

        //************************************************************************
        /// <author>Charlie Johnson</author> 
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public string[] CheckSettings() {
            List<string> lstErrors = new List<string>();

            if (Host_WCF_OLFServices) {
                if (OLFService_URL != "") {
                    if (!Uri.IsWellFormedUriString(OLFService_URL, UriKind.Absolute)) {
                        lstErrors.Add(string.Format("{0} is an invalid URL", INIKEY_OLFSERVICE_URL));
                    }
                }
                if (OLFOnlineService_URL != "") {
                    if (!Uri.IsWellFormedUriString(OLFOnlineService_URL, UriKind.Absolute)) {
                        lstErrors.Add(string.Format("{0} is an invalid URL", INIKEY_OLFONLINESERVICE_URL));
                    }
                }
                if (OLFOnlineService_URL == "" && OLFService_URL == "") {
                    lstErrors.Add("The WCF Host setting is on but no Service URL is specified");
                }
            }
            return lstErrors.ToArray();
        }
	}
}
