﻿
using System.Runtime.Serialization;

namespace WFS.RecHub.OLFServices.DataTransferObjects
{
    [DataContract]
    public class ImageResponseDto
    {
        [DataMember]
        public byte[] FrontImageData { get; set; }

        [DataMember]
        public byte[] BackImageData { get; set; }

        [DataMember]
        public bool IsSuccessful { get; set; }

        [DataMember]
        public string[] Messages { get; set; }
    }
}
