﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WFS.RecHub.OLFServices.DataTransferObjects
{
    [DataContract]
    public class ImageRequestDto
    {
        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public int WorkgroupId { get; set; }

        [DataMember]
        public DateTime DepositDate { get; set; }

        [DataMember]
        public long BatchId { get; set; }

        [DataMember]
        public long SourceBatchId { get; set; }

        [DataMember]
        public int BatchSequence { get; set; }

        [DataMember]
        public int TransactionId { get; set; }

        [DataMember]
        public bool IsCheck { get; set; }

        [DataMember]
        public string SiteKey { get; set; }

        [DataMember]
        public bool DisplayRemitterName { get; set; }

        [DataMember]
        public bool DisplayBatchId { get; set; }

        [DataMember]
        public bool DisplayScannedCheck { get; set; }

        [DataMember]
        public Guid SessionId { get; set; }

        [DataMember]
        public int PicsDate { get; set; }

        [DataMember]
        public string WorkgroupName { get; set; }

        [DataMember]
        public string BatchPaymentSource { get; set; }

        [DataMember]
        public string ImportTypeShortName { get; set; }

        //For story R360-9330 - Image Icons Aren't Appearing when ImageStorageTyp is set to 3 on rechub.ini file
        //[DataMember]
        //public string FileGroup { get; set; }

    }
}
