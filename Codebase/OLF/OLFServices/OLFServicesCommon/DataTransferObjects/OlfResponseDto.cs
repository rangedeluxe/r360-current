﻿
using System.Runtime.Serialization;

namespace WFS.RecHub.OLFServices.DataTransferObjects
{
    [DataContract]
    public class OlfResponseDto
    {
        [DataMember]
        public bool ImagesAvailable { get; set; }
        [DataMember]
        public bool IsSuccessful { get; set; }

        [DataMember]
        public string[] Messages { get; set; }
    }
}
