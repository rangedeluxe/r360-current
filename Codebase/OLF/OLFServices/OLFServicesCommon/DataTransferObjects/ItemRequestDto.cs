﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.OLFServices.DataTransferObjects
{
    public class ItemRequestDto
    {
        public long BatchId { get; set; }
        public int BatchSequence { get; set; }
        public DateTime DepositDate { get; set; }
    }
}
