﻿using System;
using System.Runtime.Serialization;
using WFS.RecHub.Common;

namespace WFS.RecHub.OLFServices.DataTransferObjects
{
    [DataContract]
    public class SingleImageRequestDto
    {
        [DataMember]
        public long BatchId { get; set; }
        [DataMember]
        public int BatchSequence { get; set; }
        [DataMember]
        public DateTime DepositDate { get; set; }
        [DataMember]
        public bool IsCheck { get; set; }
        [DataMember]
        public string SiteKey { get; set; }
        [DataMember]
        public Guid SessionId { get; set; }
        [DataMember]
        public OLFImageDisplayMode DisplayMode { get; set; }

    }
}
