﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFS.RecHub.OLFServices.Interfaces
{
    public interface IDownloadable<TRequest, TResponse>
    {
        TResponse Download(TRequest request);
    }
}
