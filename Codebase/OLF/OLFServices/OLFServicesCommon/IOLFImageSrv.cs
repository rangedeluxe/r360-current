﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using WFS.RecHub.Common;
using WFS.RecHub.OLFServices.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/02/2011 
*    -Added IOLFService interface.
* CR 32188 JMC 03/30/2010
*   -Modified GetImageSize() to accept OnlineColorMode parameter to eliminate
*    the call to the database.
* CR 45986 WJS 11/23/2001
*	- Add ability pass in SiteID
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 143275 SAS 05/21/2014
*   Changes done to change the datatype of BatchID from int to long
* WI 167463 SAS 09/23/2014
*   Change done for retrieving image relative path.
* WI 169141 SAS 10/01/2014
*   Change done for retrieving image relative path.
* WI 175212 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176358 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName  
* WI 187546 TWE 02/04/2015
*    add DeleteBatch method
******************************************************************************/
namespace WFS.RecHub.OLFServices {

    //**************************************
    // OLFOnlineService
    //**************************************
    // NOTE: If you change the interface name "IOLFOnlineService" here, you must also update the reference to "IOLFOnlineService" in Web.config.
    [ServiceContract]
    public interface IOLFOnlineService {

        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "PutFile")]
        [FaultContract(typeof(ServerFaultException))]
        void PutFile(UploadItem Item);

        [OperationContract(Action = "GetFile")]
        [FaultContract(typeof(ServerFaultException))]
        Stream GetFile(FileMetaData MetaData);

        [OperationContract(Action = "GetFileSize")]
        [FaultContract(typeof(ServerFaultException))]
        long GetFileSize(FileMetaData MetaData);

        [OperationContract(Action = "FileExists")]
        [FaultContract(typeof(ServerFaultException))]
        bool FileExists(FileMetaData MetaData);

        [OperationContract(Action = "ImagesExist")]
        [FaultContract(typeof(ServerFaultException))]
        OlfResponseDto ImagesExist(ImageRequestDto request);

        [OperationContract(Action = "BatchSequenceImagesExist")]
        [FaultContract(typeof(ServerFaultException))]
        OlfResponseDto BatchSequenceImagesExist(ImageRequestDto request);

        [OperationContract(Action = "GetImageSize")]
        long GetImageSize(FileMetaData MetaData, byte OnlineColorMode, string PICSDate, int BankID, int LockboxID, long BatchID, int Item, string ImageType, short Page, long SourceBatchID, string BatchSourceShortName, string ImportTypeShortName);
        

        [OperationContract(Action = "GetImageJobFileAsAttachment")]
        Stream GetImageJobFileAsAttachment(FileMetaData MetaData, Guid OnlineImageQueueID);

        [OperationContract(Action = "GetImageJobSize")]
        long GetImageJobSize(FileMetaData MetaData, Guid OnlineImageQueueID);

        [OperationContract(Action = "GetCENDSFileAsAttachment")]
        Stream GetCENDSFileAsAttachment(FileMetaData MetaData, string FilePath);

        [OperationContract(Action = "GetCENDSFileSize")]
        long GetCENDSFileSize(FileMetaData MetaData, string FilePath);

        [OperationContract(Action = "DownloadImage")]
        ImageResponseDto DownloadImage(FileMetaData metaData, ImageRequestDto request);
        [OperationContract(Action = "DownloadSingleImage")]
        ImageResponseDto DownloadSingleImage(SingleImageRequestDto request);

        [OperationContract]
        IEnumerable<SingleImageRequestDto> GetAvailablePaymentsImages(IEnumerable<ItemRequestDto> payments, string sitekey, Guid sessionid);

        [OperationContract]
        IEnumerable<SingleImageRequestDto> GetAvailableStubsImages(IEnumerable<ItemRequestDto> stubs, string sitekey, Guid sessionid);
        [OperationContract]
        IEnumerable<SingleImageRequestDto>  GetAvailableDocumentImages(IEnumerable<ItemRequestDto> docs, string sitekey, Guid sessionid);
    }

    //**************************************
    // OLFService
    //**************************************
    [ServiceContract]
    public interface IOLFService {

        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "GetSession")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool GetSession(string LogonName,
                        string Password,
                        out Guid SessionID);

        [OperationContract(Action = "EndSession")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool EndSession();

        [OperationContract(Action = "GetImage")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ImageNotFoundInDbFault))]
        [FaultContract(typeof(ImageNotFoundOnDiskFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool GetImage(int BankID,
                      int LockboxID,
                      int ImmutableDateKey,
                      int DepositDateKey,
                      long BatchID,
                      int BatchSequence,
                      bool IsCheck,
                      int SiteID,
                      long SourceBatchID,
                      string BatchSourceShortName, 
                      string ImportTypeShortName,
                      int PaymentTypeId,
                      out byte[][] ImageList,
                      out string FileDescriptor, 
                      out string FileExtension);

        [OperationContract(Action = "DeleteBatchImages")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        //[FaultContract(typeof(ImageNotFoundInDbFault))]
        //[FaultContract(typeof(ImageNotFoundOnDiskFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool DeleteBatchImages(
                      int clientID,
                      int ProcessingDateKey,
                      int SiteID,
                      string ImportTypeShortName,
                      string batchPaymentSourceName,
                      long SourceBatchID
            );
    }

    /* Message Contracts */
    [MessageContract]
    public class UploadItem {

        [MessageHeader(MustUnderstand = true)]
        public FileMetaData FileMetaData;

        [MessageBodyMember]
        public Stream data;
    }


    //**************************************
    // Common Data Contracts
    //**************************************
    [DataContract]
    public class ServerFaultException {

        [DataMember]
        public string errorcode;

        [DataMember]
        public string message;

        [DataMember]
        public string details;
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSiteKeyFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSiteKeyFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSiteKeyFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.InvalidSiteKey, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSessionIDFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSessionIDFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSessionIDFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.InvalidSessionID, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SessionExpiredFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class SessionExpiredFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public SessionExpiredFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.SessionExpired, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSessionFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSessionFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSessionFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.InvalidSession, source, message) {
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ImageNotFoundInDbFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class ImageNotFoundInDbFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public ImageNotFoundInDbFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.ImageNotFoundInDb, source, message) {
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ImageNotFoundOnDiskFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class ImageNotFoundOnDiskFault : ServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public ImageNotFoundOnDiskFault(int category, string source, string message) : base(category, (int)OLFServiceErrorCodes.ImageNotFoundOnDisk, source, message) {
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [DataContract]
    public class FileMetaData {

        [DataMember]
        public string SessionID;

        [DataMember]
        public string SiteKey;

        [DataMember]
        public string EmulationID;

        [DataMember]
        public string FileName;

        [DataMember]
        public string FolderKey;
    }
}
