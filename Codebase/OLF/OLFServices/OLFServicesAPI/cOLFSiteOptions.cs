using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: Site Options
*
* Modification History
* CR 32562 JMC 03/02/2011 
*   -Initial version.
* CR 32263 WJS 08/12/2011
*   -Removed cOLFSeviceOptions moved into OLFPreferences
* WI 96240 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServicesAPI
******************************************************************************/

namespace WFS.RecHub.OLFServicesAPI {

    public class cOLFSiteOptions : cSiteOptions {

        public cOLFSiteOptions(string vSiteKey) : base(vSiteKey) {

        }
    }
}
 
