﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.DAL;
using WFS.RecHub.DAL.OLFServicesDAL;
using WFS.RecHub.OLFImageAPI;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServices.Interfaces;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: Image Services
*
* Modification History
* CR 32562 JMC 03/02/2011 
*   -Initial version.
* CR 45627 JMC 07/12/2011
*   -Added a Using{} clause when accessing ipoImages to ensure Dispose() is 
*    called.
* CR 32263 WJS 08/12/2011
*   -Removed cOLFSeviceOptions moved into OLFPreferences
* CR 45986 WJS 11/23/2001
*	-Add ability pass in SiteID
* CR 50564 JMC 02/24/2012
*   -Removed call to ipoImages.SetSiteID().
* CR 52959 JNE 09/28/2012
*   -GetImage - Added FileGroup code. 
* WI 96240 CRG 04/10/2013
*   -Changed the Namespace, Framework, Usings and Flower Boxes for OLFServicesAPI
* WI 117402 JMC 10/16/2013
*   -Modified GetItem to no longer access the database for Check/Document details
* WI 117833 JMC 10/18/2013
*   -Modified ipoImages and OLFServicesAPI to accept empty File Descriptors.    
* WI 143419 DJW 6/2/2013
*   BatchID to Long, and BatchId to SourceBatchID conversion
* WI 162631 BLR 09/02/2014
*   Populated DepositDateKey in the GetItem() function.
* WI 169107 SAS 10/01/2014
*   Updated GetImage to add SourceBatchID and ImportTypeShortName
* WI 175207 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176354 SAS 11/06/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName 
* WI 187546 TWE 02/05/2015
*    Add BatchDelete call to OLFservices
******************************************************************************/

namespace WFS.RecHub.OLFServicesAPI
{

    public class cImageServices : IDisposable
    {

        public const string ServiceKey = "ImageFileServices";

        private cOLFSiteOptions _OLFSiteOptions = null;
        private cEventLog _EventLog = null;

        private string _SiteKey = string.Empty;
        private cSession _AppSession = null;
        private Guid _SessionID = Guid.Empty;
        private cOLFServicesDAL _ItemProcDAL = null;

        // Track whether Dispose has been called.
        private bool disposed = false;

        public cImageServices(Guid SessionIDValue,
            string SiteKeyValue)
        {

            _SessionID = SessionIDValue;
            _SiteKey = SiteKeyValue;
        }

        private cOLFSiteOptions OLFSiteOptions
        {
            get
            {
                if (_OLFSiteOptions == null)
                {
                    _OLFSiteOptions = new cOLFSiteOptions(_SiteKey);
                }
                return (_OLFSiteOptions);
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        private cEventLog EventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(OLFSiteOptions.logFilePath,
                        OLFSiteOptions.logFileMaxSize,
                        (MessageImportance)OLFSiteOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        private cSession AppSession
        {
            get
            {
                if (_AppSession == null)
                {
                    _AppSession = new cSession();
                }

                return (_AppSession);
            }
        }

        private cOLFServicesDAL ItemProcDAL
        {
            get
            {
                if (_ItemProcDAL == null)
                {
                    _ItemProcDAL = new cOLFServicesDAL(_SiteKey);
                }
                return (_ItemProcDAL);
            }
        }

        public bool GetSession(string LogonName,
            string Password,
            out Guid SessionID)
        {

            bool bolRetVal = true;
            Guid gidSessionID = Guid.Empty;

            try
            {

                _SessionID = R360ServiceContext.Current.GetSessionID();

            }
            catch (AccountDisabledException ex)
            {
                _SessionID = Guid.Empty;
                throw;
            }
            catch (MaxLogonAttemptsExceededException ex)
            {
                _SessionID = Guid.Empty;
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex);
                _SessionID = Guid.Empty;
                SessionID = Guid.Empty;
                bolRetVal = false;
            }
            SessionID = _SessionID;
            return (bolRetVal);
        }

        public bool EndSession()
        {

            cSessionDAL objSessionDAL;
            DataTable dt;
            bool bolRetVal;

            try
            {
                using (objSessionDAL = new cSessionDAL(_SiteKey))
                {
                    bolRetVal = objSessionDAL.EndSession(_SessionID, out dt);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex);
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        public long GetImageSize(int BankID,
            int LockboxID,
            int ImmutableDateKey,
            int depositDateKey,
            long BatchID,
            int BatchSequence,
            bool IsCheck,
            int SiteID,
            long SourceBatchID,
            string BatchSourceShortName,
            string ImportTypeShortName,
            int PaymentTypeId)
        {
            // Gather the root image location.
            DataTable dt = null;
            string strRootImageLocation = OLFSiteOptions.imagePath;
            if (OLFSiteOptions.imageStorageMode == ImageStorageMode.FILEGROUP)
            {
                if (ItemProcDAL.GetLockbox(BankID, LockboxID, out dt))
                {
                    if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["FileGroup"] != DBNull.Value)
                    {
                        string strFileGroup = dt.Rows[0]["FileGroup"].ToString();
                        if (!string.IsNullOrWhiteSpace(strFileGroup) && strFileGroup != "null")
                        {
                            if (Directory.Exists(strFileGroup))
                            {
                                strRootImageLocation = strFileGroup;
                            }
                        }
                    }
                }
                if (dt != null)
                {
                    dt.Dispose();
                }
            }

            // Gather the image path.
            string path;
            using (cipoImages ipoImages =
                    new cipoImages(_SiteKey, OLFSiteOptions.imageStorageMode, strRootImageLocation))
            {
                IItem item;
                GetItem(BankID, LockboxID, ImmutableDateKey, depositDateKey, BatchID,
                    BatchSequence, SourceBatchID, BatchSourceShortName, ImportTypeShortName, IsCheck, PaymentTypeId, out item);
                path = ipoImages.GetRelativeImagePath(item, 0);
                var fullpath = Path.Combine(strRootImageLocation, path);

                // Check the path for data, and check for ACH/WIRE.
                if (string.IsNullOrWhiteSpace(fullpath) || !File.Exists(fullpath))
                {
                    var imageResponse = ipoImages.GetImageForACHOrWirePayment(item);
                    if (imageResponse.IsSuccessful)
                        return imageResponse.ImageBytes.Length;
                    else
                        return 0;
                }
                return new FileInfo(fullpath).Length;
            }
        }

        public bool GetImage(int BankID,
            int LockboxID,
            int ImmutableDateKey,
            int depositDateKey,
            long BatchID,
            int BatchSequence,
            bool IsCheck,
            int SiteID,
            long SourceBatchID,
            string BatchSourceShortName,
            string ImportTypeShortName,
            int PaymentTypeId,
            out byte[][] ImageList,
            out string FileDescriptor,
            out string FileExtension)
        {

            IItem objItem;
            List<byte[]> arImageList;
            string strFileDescriptor;
            string strFileExtension;
            cSessionDAL objSessionDAL;
            int intRecords;
            DataTable dt = null;

            bool bolImagesWereFound;

            bool bolRetVal;

            try
            {
                // Define default image storage location
                string strRootImageLocation = OLFSiteOptions.imagePath;
                //Lookup FileGroup location if exists
                if (OLFSiteOptions.imageStorageMode == ImageStorageMode.FILEGROUP)
                {
                    // Lookup lockbox
                    if (ItemProcDAL.GetLockbox(BankID, LockboxID, out dt))
                    {
                        if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["FileGroup"] != DBNull.Value)
                        {
                            string strFileGroup = dt.Rows[0]["FileGroup"].ToString();
                            if (!string.IsNullOrWhiteSpace(strFileGroup) && strFileGroup != "null")
                            {
                                if (Directory.Exists(strFileGroup))
                                {
                                    strRootImageLocation = strFileGroup;
                                }
                            }
                        }
                    }
                    if (dt != null)
                    {
                        dt.Dispose();
                    }
                }


                using (cipoImages ipoImages =
                    new cipoImages(_SiteKey, OLFSiteOptions.imageStorageMode, strRootImageLocation))
                {
                    EventLog.logEvent("GetItem:  BankID=" + BankID.ToString() +
                                      " LockboxID=" + LockboxID.ToString() +
                                      " ImmutableDateKey=" + ImmutableDateKey.ToString() +
                                      " DepositDateKey=" + depositDateKey.ToString() +
                                      " BatchID=" + BatchID.ToString() +
                                      " BatchSequence=" + BatchSequence.ToString() +
                                      " SourceBatchID=" + SourceBatchID.ToString() +
                                      " BatchSourceShortName=" + BatchSourceShortName +
                                      " ImportTypeShortName=" + ImportTypeShortName +
                                      " PaymentTypeID=" + PaymentTypeId.ToString()
                        , MessageImportance.Debug);

                    if (GetItem(BankID,
                        LockboxID,
                        ImmutableDateKey,
                        depositDateKey,
                        BatchID,
                        BatchSequence,
                        SourceBatchID,
                        BatchSourceShortName,
                        ImportTypeShortName,
                        IsCheck,
                        PaymentTypeId,
                        out objItem))
                    {
                        EventLog.logEvent("GetItem-Success:  "
                            , MessageImportance.Debug);

                        arImageList = new List<byte[]>();
                        FileExtension = string.Empty;
                        FileDescriptor = string.Empty;
                        bolImagesWereFound = false;

                        for (int intPage = 0; intPage <= 1; ++intPage)
                        {
                            var response = ipoImages.GetImage(objItem, intPage, out strFileDescriptor,
                                out strFileExtension);
                            if (response.IsSuccessful)
                            {
                                if (!IsCheck && strFileDescriptor == "C")
                                {
                                    arImageList.Add(new byte[0]);
                                }
                                else
                                {
                                    arImageList.Add(response.ImageBytes);

                                    FileExtension = strFileExtension;

                                    if (string.IsNullOrEmpty(objItem.FileDescriptor))
                                    {
                                        FileDescriptor = strFileDescriptor;
                                    }
                                    else
                                    {
                                        FileDescriptor = objItem.FileDescriptor;
                                    }

                                    using (objSessionDAL = new cSessionDAL(_SiteKey))
                                    {
                                        objSessionDAL.InsertSessionActivityLog(
                                            _SessionID,
                                            this.GetType().Module.Name,
                                            (intPage == 0
                                                ? ActivityCodes.acViewImageFront
                                                : ActivityCodes.acViewImageBack),
                                            Guid.Empty,
                                            1,
                                            BankID,
                                            LockboxID,
                                            out intRecords);
                                    }

                                    bolImagesWereFound = true;
                                }

                            }
                            else
                            {
                                arImageList.Add(new byte[0]);
                            }
                        }

                        ImageList = arImageList.ToArray();

                        bolRetVal = true;

                    }
                    else
                    {
                        throw (new ImageNotFoundInDbException(BankID, LockboxID, ImmutableDateKey, BatchID,
                            BatchSequence));
                    }
                } //using

                if (!bolImagesWereFound)
                {
                    throw (new ImageNotFoundOnDiskException(BankID, LockboxID, ImmutableDateKey, BatchID,
                        BatchSequence));
                }

                // Update the session record to indicate that the Session is active.
                //LogonAPI.LogSessionAccess(_SessionID, this.GetType().Module.Name);

            }
            catch (SessionExpiredException)
            {
                throw;
            }
            catch (InvalidSessionException)
            {
                throw;
            }
            catch (ImageNotFoundInDbException ex)
            {
                EventLog.logWarning(ex.Message,
                    this.GetType().ToString(),
                    MessageImportance.Verbose);
                throw;
            }
            catch (ImageNotFoundOnDiskException ex)
            {
                EventLog.logWarning(ex.Message,
                    this.GetType().ToString(),
                    MessageImportance.Verbose);

                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetImage");
                ImageList = null;
                FileDescriptor = string.Empty;
                FileExtension = string.Empty;
                bolRetVal = false;
            }

            return bolRetVal;
        }

        public bool DeleteBatchImages(int clientID,
            int ProcessingDateKey,
            int SiteID,
            string importTypeShortName,
            string batchPaymentSourceName,
            long SourceBatchID
        )
        {
            bool bolRetVal = false;
            DataTable dt = null;
            string folderPathToBatch = String.Empty;

            try
            {

                // Define default image storage location
                string strRootImageLocation = OLFSiteOptions.imagePath;
                //Lookup FileGroup location if exists
                if (OLFSiteOptions.imageStorageMode == ImageStorageMode.FILEGROUP)
                {
                    // Lookup lockbox
                    if (ItemProcDAL.GetLockbox(SiteID, clientID, out dt))
                    {
                        if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["FileGroup"] != DBNull.Value)
                        {
                            string strFileGroup = dt.Rows[0]["FileGroup"].ToString();
                            if (!string.IsNullOrWhiteSpace(strFileGroup) && strFileGroup != "null")
                            {
                                if (Directory.Exists(strFileGroup))
                                {
                                    strRootImageLocation = strFileGroup;
                                }
                            }
                        }
                    }
                    if (dt != null)
                    {
                        dt.Dispose();
                    }
                    cFILEGROUP filegroup = new cFILEGROUP(_SiteKey);
                    folderPathToBatch = filegroup.GetImagePath(importTypeShortName,
                        batchPaymentSourceName,
                        strRootImageLocation,
                        ProcessingDateKey.ToString(),
                        SiteID,
                        clientID);
                }
                else
                {
                    cPICS pics = new cPICS(_SiteKey);
                    folderPathToBatch = pics.GetImagePath(importTypeShortName,
                        batchPaymentSourceName,
                        strRootImageLocation,
                        ProcessingDateKey.ToString(),
                        SiteID,
                        clientID);
                }

                EventLog.logEvent("Folder Path:  " + folderPathToBatch,
                    this.GetType().Name,
                    MessageType.Information,
                    MessageImportance.Debug);

                if (Directory.Exists(folderPathToBatch))
                {
                    // get the list of files to delete
                    List<string> fileEntries = Directory.GetFiles(folderPathToBatch)
                        .Where(x => x.StartsWith(folderPathToBatch + "\\" + SourceBatchID.ToString() + "_")).ToList();

                    EventLog.logEvent("Count of files to Delete:  " + fileEntries.Count.ToString(),
                        this.GetType().Name,
                        MessageType.Information,
                        MessageImportance.Debug);

                    foreach (string fileName in fileEntries)
                    {
                        File.Delete(fileName); //now delete each file
                    }

                    // if no other batches under the folder, then delete the folder
                    if (Directory.GetFiles(folderPathToBatch).Count() == 0)
                    {
                        Directory.Delete(folderPathToBatch);
                    }

                    bolRetVal = true;
                }
                else
                {
                    EventLog.logEvent("Folder Path doesn't exist:  " + folderPathToBatch,
                        this.GetType().Name,
                        MessageType.Error,
                        MessageImportance.Essential);
                    bolRetVal = false;
                }

            }
            catch (SessionExpiredException)
            {
                throw;
            }
            catch (InvalidSessionException)
            {
                throw;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "DeleteBatchImages");
                bolRetVal = false;
            }

            return bolRetVal;
        }

        public IEnumerable<SingleImageRequestDto> GetAvailableItemImages(IEnumerable<ItemRequestDto> items, bool ischecks)
        {
            var output = new List<SingleImageRequestDto>();
            foreach (var p in items)
            {
                // Grab the image data for the payment.
                DataTable dt = null;
                ItemProcDAL.GetImageData(p.BatchId, p.DepositDate, p.BatchSequence, ischecks,
                    out dt);
                cImage img;
                img = new cCheck { DepositDate = p.DepositDate, BatchSequence = p.BatchSequence };

                // Kick out early if the data wasn't found.
                if (dt.Rows.Count <= 0)
                {
                    EventLog.logEvent("Image data not found for Check BatchId: " +
                                      p.BatchId + "BatchSequence: " + p.BatchSequence,
                        GetType().Name,
                        MessageType.Error,
                        MessageImportance.Essential);
                    continue;
                }
                var row = dt.Rows[0];
                img.LoadDataRow(ref row);

                // Check to see if the image exists, kick out early if not.
                if (GetImageSize(img.BankID, img.LockboxID, int.Parse(img.PICSDate), int.Parse(p.DepositDate.ToString("yyyyMMdd")),
                    p.BatchId, p.BatchSequence, ischecks, 0, img.SourceBatchID, img.BatchSourceShortName, img.ImportTypeShortName,
                    img.PaymentType) == 0)
                    continue;
                
                // Finally add the item, as we found a valid image.
                output.Add(new SingleImageRequestDto()
                {
                    BatchId = p.BatchId,
                    BatchSequence = p.BatchSequence,
                    DepositDate = p.DepositDate,
                    IsCheck = ischecks
                });
            }
            return output;
        }

        public ImageResponseDto DownloadImage(ImageRequestDto request)
        {
            var imageDisplayModes = ItemProcDAL.GetImageDisplayModes(request.BankId, request.WorkgroupId);

            IDownloadable<ImageRequestDto, ImageResponseDto> imageDownload =
                new OLFImageAPI.ImageDownload(imageDisplayModes);
            request.SessionId = this._SessionID;
            return imageDownload.Download(request);
        }

        private bool GetItem(int BankID,
            int LockboxID,
            int immutableDateKey,
            int depositDateKey,
            long BatchID,
            int BatchSequence,
            long SourceBatchID,
            string BatchSourceShortName,
            string ImportTypeShortName,
            bool IsCheck,
            int PaymentTypeId,
            out IItem Image)
        {

            bool bolRetVal;
            IItem objItem;

            try
            {

                if (IsCheck)
                {
                    objItem = new cCheck();
                    ((cCheck)objItem).BankID = BankID;
                    ((cCheck)objItem).LockboxID = LockboxID;
                    ((cCheck)objItem).ProcessingDateKey = immutableDateKey;
                    ((cCheck)objItem).BatchID = BatchID;
                    ((cCheck)objItem).BatchSequence = BatchSequence;
                    ((cCheck)objItem).DepositDate = DateTime.ParseExact(depositDateKey.ToString(), "yyyyMMdd", null);
                    ((cCheck)objItem).SourceBatchID = SourceBatchID;
                    ((cCheck)objItem).BatchSourceShortName = BatchSourceShortName;
                    ((cCheck)objItem).ImportTypeShortName = ImportTypeShortName;
                    ((cCheck)objItem).PaymentType = PaymentTypeId;
                }
                else
                {
                    objItem = new cDocument();
                    ((cDocument)objItem).BankID = BankID;
                    ((cDocument)objItem).LockboxID = LockboxID;
                    ((cDocument)objItem).ProcessingDateKey = immutableDateKey;
                    ((cDocument)objItem).BatchID = BatchID;
                    ((cDocument)objItem).BatchSequence = BatchSequence;
                    ((cDocument)objItem).DepositDate =
                        DateTime.ParseExact(depositDateKey.ToString(), "yyyyMMdd", null);
                    ((cDocument)objItem).SourceBatchID = SourceBatchID;
                    ((cDocument)objItem).BatchSourceShortName = BatchSourceShortName;
                    ((cDocument)objItem).ImportTypeShortName = ImportTypeShortName;
                    ((cDocument)objItem).PaymentType = PaymentTypeId;
                }


                Image = objItem;

                bolRetVal = true;

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetItem");
                bolRetVal = false;
                Image = null;
            }

            return (bolRetVal);
        }


        #region Dispose   

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_ItemProcDAL != null)
                    {
                        _ItemProcDAL.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }

        // Use interop to call the method necessary
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~cImageServices()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }

        #endregion

        public ImageResponseDto GetSingleImage(SingleImageRequestDto request)
        {

            DataTable dt = null;
            ItemProcDAL.GetImageData(request.BatchId, request.DepositDate, request.BatchSequence, request.IsCheck,
                out dt);
            cImage img;
            if (request.IsCheck)
            {
                img = new cCheck { DepositDate = request.DepositDate,BatchSequence = request.BatchSequence};
            }
            else
            {
                img = new cDocument { DepositDate = request.DepositDate, BatchSequence = request.BatchSequence };
            }
            if (dt.Rows.Count <= 0)
            {
                EventLog.logEvent("Image data not found for BatchId: " +
                                  request.BatchId + "BatchSequence: " + request.BatchSequence,
                    GetType().Name,
                    MessageType.Error,
                    MessageImportance.Essential);
                return new ImageResponseDto
                {
                    FrontImageData = null,
                    IsSuccessful = false,
                    Messages = new[] { "Image data not found" }
                };
            }
            var row = dt.Rows[0];
            img.LoadDataRow(ref row);
            var imageDisplayModes = ItemProcDAL.GetImageDisplayModes(img.BankID, img.LockboxID);

            var imageDwnld = new ImageDownload(imageDisplayModes);
            var result = imageDwnld.DownLoadSingleImage(request, (IItem)img);

            byte[] pngBytes;
            if (result?.FrontImageData == null)
            {
                EventLog.logEvent("Image Data was not found for:" +
                    "BankId:" + img.BankID + " BatchId:" + img.BatchID +
                    " Batch Sequence:" + img.BatchSequence + " Deposit Date"
                    ,
                    GetType().Name,
                    MessageType.Error,
                    MessageImportance.Essential);
                return result;
            }
            using (MemoryStream inStream = new MemoryStream(result.FrontImageData))
            using (MemoryStream outStream = new MemoryStream())
            {
                Image.FromStream(inStream).Save(outStream, System.Drawing.Imaging.ImageFormat.Png);
                pngBytes = outStream.ToArray();
            }
            result.FrontImageData = pngBytes;

            if (result?.BackImageData != null)
            {
                using (MemoryStream inStream = new MemoryStream(result.BackImageData))
                using (MemoryStream outStream = new MemoryStream())
                {
                    Image.FromStream(inStream).Save(outStream, System.Drawing.Imaging.ImageFormat.Png);
                    pngBytes = outStream.ToArray();
                }
                result.BackImageData = pngBytes;
            }
            return result;

        }
    }
}
