﻿using System;
using System.IO;
using System.Linq;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.OLFServices.DataTransferObjects;

namespace WFS.RecHub.OLFServicesAPI.helper
{
    public class LocateImages
    {
        private readonly cEventLog _eventLog;
        private readonly cSiteOptions _siteoptions;
        private readonly ISystemIo _systemIo = null;

        public LocateImages(cEventLog eventLog, cSiteOptions siteoptions)
        {
            _eventLog = eventLog;
            _siteoptions = siteoptions;
            _systemIo = new ApplicationBlocks.Common.SystemIo();
        }

        public LocateImages(cEventLog eventLog, cSiteOptions siteoptions, ISystemIo systemIo)
        {
            _eventLog = eventLog;
            _siteoptions = siteoptions;
            _systemIo = systemIo;
        }

        public OlfResponseDto AnyImagesExist(ImageRequestDto request)
        {
            if (!_siteoptions.CheckImageExists)
            {
                _eventLog?.logEvent("CheckImageExists set to off, returning images are there. LocateImages.AnyImagesExist", MessageImportance.Debug);
                return new OlfResponseDto() { ImagesAvailable = true, IsSuccessful = true };
            }

            _eventLog?.logEvent("Image Path: " + _siteoptions.imagePath, "LocateImages.AnyImagesExist", MessageImportance.Debug);
           
            string fullImagePath =  SetFullImagePath(request);
            _eventLog?.logEvent("Full Image Path: " + fullImagePath, "LocateImages.AnyImagesExist", MessageImportance.Debug);
            if (!_systemIo.Exists(fullImagePath))
            {
                _eventLog?.logEvent("No folder found containing images", "LocateImages.AnyImagesExist", MessageImportance.Debug);
                return new OlfResponseDto(){ImagesAvailable = false, IsSuccessful = true};
            }

            string[] imageFiles = _systemIo.GetFiles(fullImagePath, request.SourceBatchId.ToString()+"*");

            if (imageFiles.Length < 1)
            {
                _eventLog?.logEvent("No images for " + request.SourceBatchId + " found in folder", "LocateImages.AnyImagesExist", MessageImportance.Debug);
                return new OlfResponseDto() { ImagesAvailable = false, IsSuccessful = true };
            }
            _eventLog?.logEvent("For " + request.SourceBatchId + " found " + imageFiles.Length + " images in folder", "LocateImages.AnyImagesExist", MessageImportance.Debug);

            return new OlfResponseDto(){ImagesAvailable = true, IsSuccessful = true};
        }

        public OlfResponseDto BatchSequenceImagesExist(ImageRequestDto request)
        {
            if (!_siteoptions.CheckImageExists)
            {
                _eventLog?.logEvent("CheckImageExists set to off, returning images are there. LocateImages.BatchSequenceImagesExist", MessageImportance.Debug);
                return new OlfResponseDto() { ImagesAvailable = true, IsSuccessful = true };
            }

            _eventLog?.logEvent("Image Path: " + _siteoptions.imagePath, "LocateImages.BatchSequenceImagesExist", MessageImportance.Debug);

            string fullImagePath = SetFullImagePath(request);
            _eventLog?.logEvent("Full Image Path: " + fullImagePath, "LocateImages.BatchSequenceImagesExist", MessageImportance.Debug);
            if (!_systemIo.Exists(fullImagePath))
            {
                _eventLog?.logEvent("No folder found containing images", "LocateImages.BatchSequenceImagesExist", MessageImportance.Debug);
                return new OlfResponseDto() { ImagesAvailable = false, IsSuccessful = true };
            }

            //If the request includes a transactionId > 0, check for images that belong only to the transaction
            var filePattern = request.SourceBatchId.ToString() + "_" + request.BatchSequence.ToString() + "_*";

            string[] imageFiles = null;

            if (request.IsCheck)
            {
                imageFiles = _systemIo.EnumerateFiles(fullImagePath, filePattern).Where(s => s.Contains("_C_"))
                    .ToArray();
            }
            else
            {
                imageFiles = _systemIo.EnumerateFiles(fullImagePath, filePattern).Where(s => !s.Contains("_C_"))
                    .ToArray();
            }

            if (imageFiles.Length < 1)
            {
                _eventLog?.logEvent("No images for " + request.SourceBatchId + " found in folder", "LocateImages.BatchSequenceImagesExist", MessageImportance.Debug);
                return new OlfResponseDto() { ImagesAvailable = false, IsSuccessful = true };
            }
            _eventLog?.logEvent("For " + request.SourceBatchId + " found " + imageFiles.Length + " images in folder", "LocateImages.BatchSequenceImagesExist", MessageImportance.Debug);

            return new OlfResponseDto() { ImagesAvailable = true, IsSuccessful = true };
        }

        private string SetFullImagePath(ImageRequestDto request)
        {
            _eventLog?.logEvent($"imagePath: {_siteoptions.imagePath}", "LocateImages.SetFullImagePath", MessageImportance.Debug);
            _eventLog?.logEvent($"PicsDate: {request.PicsDate}", "LocateImages.SetFullImagePath", MessageImportance.Debug);
            _eventLog?.logEvent($"BankId: {request.BankId}", "LocateImages.SetFullImagePath", MessageImportance.Debug);
            _eventLog?.logEvent($"WorkgroupId: {request.WorkgroupId}", "LocateImages.SetFullImagePath", MessageImportance.Debug);
            _eventLog?.logEvent($"ImportTypeShortName: {request.ImportTypeShortName}", "LocateImages.SetFullImagePath", MessageImportance.Debug);
            _eventLog?.logEvent($"BatchPaymentSource: {request.BatchPaymentSource}", "LocateImages.SetFullImagePath", MessageImportance.Debug);

            return _systemIo.Combine(_siteoptions.imagePath, 
                                request.BatchPaymentSource,
                                request.PicsDate.ToString(), 
                                request.BankId.ToString(),
                                request.WorkgroupId.ToString(),
                                request.ImportTypeShortName);
            //For story R360-9330 - Image Icons Aren't Appearing when ImageStorageTyp is set to 3 on rechub.ini file
            //return _systemIo.Combine(!string.IsNullOrEmpty(request.FileGroup) ? request.FileGroup : _siteoptions.imagePath,
            //    request.BatchPaymentSource,
            //    request.PicsDate.ToString(),
            //    request.BankId.ToString(),
            //    request.WorkgroupId.ToString());
        }
    }
}
