﻿using System;
using System.IO;
using System.ServiceModel;
using WFS.RecHub.Common;
using WFS.RecHub.OLFServices;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: Services Client Base for OLF Services Client
*
* Modification History
* CR 51010 JMC 03/07/2012
*    - Changed MaxReceivedMessageSize from 64 mb (67108864 bytes) to 2 gb 
*      (2147483647 bytes)
* WI 96332 CRG 04/11/2013
*    - Change the NameSpace, Framework, Using's and Flower Boxes for OLFServicesClient
* WI 145504 BLR 06/04/2014
*    - Added FolderKey to the Metadata of all methods.
* WI 143450 RDS 05/22/2014	Move Binding configuration to config files
******************************************************************************/
namespace WFS.RecHub.OLFServicesClient
{
    public abstract class _ServicesClientBase : _DMPObjectRoot {
        
        public String OnlineArchivePath = ipoINILib.IniReadValue("ipoImageSvc","OnlineArchivePath");
        public IOLFOnlineService OLFOnlineService = null;  
        public FileMetaData MetaData = new FileMetaData();
        public UploadItem UploadItem = new UploadItem();      
        
        public _ServicesClientBase(string vSiteKey) : base(vSiteKey) { }

        public string Ping()
        {
            return OLFOnlineService.Ping();
        }
        
        public bool FileExists(string FileName, string FolderKey)
        {
            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.SessionID = "";
            MetaData.EmulationID = "";
            MetaData.FileName = FileName;
            // WI 145504 : Just including the FolderKey.
            MetaData.FolderKey = FolderKey;
            try{
                bool bFileExists;
                bFileExists =  OLFOnlineService.FileExists(MetaData);
                return bFileExists;
            }catch (FaultException<ServerFaultException> ex){ // Log Error
                EventLog.logError(ex, this.GetType().Name, "FileExists");
                return (false);
            };
            
        }
        
        public long GetFileSize(string FileName, string FolderKey)
        {
            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.SessionID = "";
            MetaData.EmulationID = "";
            MetaData.FileName = FileName;
            // WI 145504 : Just including the FolderKey.
            MetaData.FolderKey = FolderKey;
            try{
                long lFileSize = 0;
                lFileSize =  OLFOnlineService.GetFileSize(MetaData);
                return lFileSize;
            }catch (FaultException<ServerFaultException> ex){
                EventLog.logError(ex, this.GetType().Name, "FileName");
                return 0;
            }
        }
        
        public Stream GetFile(string DownloadFile, string FolderKey)
        {
            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.SessionID = "";
            MetaData.EmulationID = "";
            MetaData.FileName = DownloadFile;
            // WI 145504 : Just including the FolderKey.
            MetaData.FolderKey = FolderKey;
            try{
                Stream strmFile = null;
                strmFile =  OLFOnlineService.GetFile(MetaData);
                return strmFile;
            }catch (FaultException<ServerFaultException> ex){
                EventLog.logError(ex, this.GetType().Name, "GetFile");
                return null;
            }
        }
        
        public void PutStream(String DestinationFileName, Stream Data, string FolderKey)
        {
            FileMetaData MetaData = new FileMetaData();
            MetaData.FileName = DestinationFileName;
            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.FolderKey = FolderKey;
            UploadItem.data = Data;
            UploadItem.FileMetaData = MetaData; 
            try{
                OLFOnlineService.PutFile(UploadItem);
                
            }catch (FaultException<ServerFaultException> ex){
                EventLog.logError(ex, this.GetType().Name, "PutStream");
            }
        }
        public void PutFile(String DestinationFileName, String SourceFileName, string FolderKey)
        {
            if (File.Exists(SourceFileName))
            {
               Stream SourceStream = null;
               try{
                SourceStream = File.OpenRead(SourceFileName);
               }catch (Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "PutFile"); 
               }
               PutStream(DestinationFileName, SourceStream, FolderKey); 
               SourceStream.Close();
               //SourceStream.Dispose();
            }else{
                string Message = "File "+SourceFileName+" does not exist.";
                EventLog.logWarning(Message,MessageImportance.Essential);    
            }
        }
    }

  
}
