﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using WFS.RecHub.Common;
using WFS.RecHub.OLFServices;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: OLF Online Client
*
* Modification History
* CR 32188 JMC 03/30/2010
*   -Modified GetImageSize() to accept OnlineColorMode parameter to eliminate
*    the call to the database.
* CR 52588 twe 10/15/2012
*    IF URL starts with https force security mode transport to use SSL (port 443)
* WI 96332 CRG 04/11/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for OLFServicesClient
* WI 143450 RDS 05/22/2014	Move Binding configuration to config files
* WI 167467 SAS 09/23/2014
*   Change done for retrieving image relative path.
* WI 175213 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName    
* WI 176359 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName 
******************************************************************************/
namespace WFS.RecHub.OLFServicesClient
{

    public class OLFOnlineClient : _ServicesClientBase, IOLFOnlineClient
    {

        public OLFOnlineClient(string vSiteKey) : base(vSiteKey)
        {
            if (LogManager.IsDefault) LogManager.Logger = EventLog.IPOtoILogger(this.GetType().Name);
            OLFOnlineService = R360ServiceFactory.Create<IOLFOnlineService>();
        }

        public long GetImageSize(FileMetaData MetaData,
                                 byte OnlineColorMode,
                                 string PICSDate,
                                 int BankID,
                                 int LockboxID,
                                 long BatchID,
                                 int Item,
                                 string ImageType,
                                 short Page,
                                 long SourceBatchID,
                                 string BatchSourceShortName,
                                 string ImportTypeShortName)
        {

            long lngRetVal;

            if (OLFOnlineService != null)
            {
                lngRetVal = OLFOnlineService.GetImageSize(MetaData, OnlineColorMode, PICSDate, BankID, LockboxID, BatchID, Item, ImageType, Page, SourceBatchID, BatchSourceShortName, ImportTypeShortName);
            }
            else {
                lngRetVal = 0;
            }

            return (lngRetVal);
        }

        public IEnumerable<SingleImageRequestDto> GetAvailablePaymentsImages(IEnumerable<ItemRequestDto> payments, string sitekey, Guid sessionid)
        {
            return OLFOnlineService != null
                ? OLFOnlineService.GetAvailablePaymentsImages(payments, sitekey, sessionid)
                : null;
        }

        public IEnumerable<SingleImageRequestDto> GetAvailableStubsImages(IEnumerable<ItemRequestDto> stubs, string sitekey, Guid sessionid)
        {
            return OLFOnlineService != null
                ? OLFOnlineService.GetAvailableStubsImages(stubs, sitekey, sessionid)
                : null;
        }

        public IEnumerable<SingleImageRequestDto> GetAvailableDocumentImages(IEnumerable<ItemRequestDto> stubs, string sitekey, Guid sessionid)
        {
            return OLFOnlineService != null ? OLFOnlineService.GetAvailableDocumentImages(stubs, sitekey, sessionid): null;
        }

        public Stream GetImageJobFileAsAttachment(Guid OnlineImageQueueID)
        {

            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.SessionID = "";
            MetaData.EmulationID = "";

            try
            {
                Stream strmFile = null;
                if (OLFOnlineService != null)
                {
                    strmFile = OLFOnlineService.GetImageJobFileAsAttachment(MetaData, OnlineImageQueueID);
                }
                return strmFile;
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetFile");
                return null;
            }
        }

        public long GetImageJobSize(FileMetaData MetaData, Guid OnlineImageQueueID)
        {

            long lngRetVal;

            if (OLFOnlineService != null)
            {
                lngRetVal = OLFOnlineService.GetImageJobSize(MetaData, OnlineImageQueueID);
            }
            else {
                lngRetVal = 0;
            }

            return (lngRetVal);
        }

        public Stream GetCENDSFileAsAttachment(string FilePath)
        {

            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.SessionID = "";
            MetaData.EmulationID = "";

            try
            {
                Stream strmFile = null;
                if (OLFOnlineService != null)
                {
                    strmFile = OLFOnlineService.GetCENDSFileAsAttachment(MetaData, FilePath);
                }
                return strmFile;
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetFile");
                return null;
            }
        }

        public long GetCENDSFileSize(FileMetaData MetaData,
                                     string FilePath)
        {

            long lngRetVal;

            if (OLFOnlineService != null)
            {
                lngRetVal = OLFOnlineService.GetCENDSFileSize(MetaData, FilePath);
            }
            else {
                lngRetVal = 0;
            }

            return (lngRetVal);
        }


        public ImageResponseDto DownloadSingleImage(SingleImageRequestDto request)
        {
            try
            {
                if (OLFOnlineService == null)
                {
                    return new ImageResponseDto
                    {
                        IsSuccessful = false,
                        Messages = new List<string>() { "There was an issue downloading the image.  The online image service is not configured properly." }.ToArray()
                    };
                }
                return OLFOnlineService.DownloadSingleImage(request);
            }
            catch (FaultException<ServerFaultException> e)
            {
                EventLog.logError(e, this.GetType().Name, "DownloadSingleImage");
                return new ImageResponseDto
                {
                    IsSuccessful = false,
                    Messages = new List<string>() { "There was an issue downloading the check image." }.ToArray()
                };
            }
    }

        public ImageResponseDto DownloadImage(FileMetaData metaData, ImageRequestDto request)
        {
            try
            {

                if (OLFOnlineService != null)
                    return OLFOnlineService.DownloadImage(metaData, request);
                else
                    return new ImageResponseDto
                    {
                        IsSuccessful = false,
                        Messages = new List<string>() { "There was an issue downloading the image.  The online image service is not configured properly." }.ToArray()
                    };

            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DownloadImage");
                return new ImageResponseDto { IsSuccessful = false, Messages = new List<string>() { "There was an issue downloading the image." }.ToArray() };
            }
        }

        public OlfResponseDto ImagesExist(ImageRequestDto request)
        {
            try
            {
                if (OLFOnlineService != null)
                    return OLFOnlineService.ImagesExist(request);
                else
                    return new OlfResponseDto
                    {
                        IsSuccessful = false,
                        Messages = new List<string>() { "There was an issue locating an image.  The online image service is not configured properly." }.ToArray()
                    };
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, "ImagesExist");
                return new OlfResponseDto { IsSuccessful = false, Messages = new List<string>() { "There was an issue locating the image." }.ToArray() };
            }
        }
        public OlfResponseDto BatchSequenceImagesExist(ImageRequestDto request)
        {
            try
            {
                if (OLFOnlineService != null)
                    return OLFOnlineService.BatchSequenceImagesExist(request);
                else
                    return new OlfResponseDto
                    {
                        IsSuccessful = false,
                        Messages = new List<string>() { "There was an issue locating an batch sequence image.  The online image service is not configured properly." }.ToArray()
                    };
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, "BatchSequenceImagesExist");
                return new OlfResponseDto { IsSuccessful = false, Messages = new List<string>() { "There was an issue locating the batch sequence image." }.ToArray() };
            }
        }
    }
}