﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WFS.integraPAY.Online.Common;
using WFS.integraPAY.Online.OLFServicesClient;
using System.ServiceModel;

namespace WFS.integraPAY.Online.OLFServicesClient
{
    public abstract class OLFServicesClientBase : _DMPObjectRoot {
        
        public String OnlineArchivePath = ipoINILib.IniReadValue("ipoImageSvc","OnlineArchivePath");
        public BasicHttpBinding basicHTTPBinding = new BasicHttpBinding();
        public EndpointAddress endpointaddress = null;
        public IOLFOnlineService OLFOnlineService = null;  
        public IOLFConsolidatorService OLFConsolidatorService = null;  
        public string BasePath = "";
        public WFS.integraPAY.Online.OLFServicesClient.FileMetaData MetaData = new WFS.integraPAY.Online.OLFServicesClient.FileMetaData();
        public WFS.integraPAY.Online.OLFServicesClient.UploadItem UploadItem = new  WFS.integraPAY.Online.OLFServicesClient.UploadItem();      
        
        public abstract void SetEndPointAddress();
        
        public abstract void CreateChannel();
        
        public OLFServicesClientBase(string vSiteKey) : base(vSiteKey) { 
            basicHTTPBinding.MaxReceivedMessageSize = 67108864;
            basicHTTPBinding.MessageEncoding = WSMessageEncoding.Mtom;
            basicHTTPBinding.TransferMode = TransferMode.Streamed;
            SetEndPointAddress();
            CreateChannel();
        }
        
        public string Ping()
        {
           if (OLFOnlineService != null){
            return OLFOnlineService.Ping();
           }else{
            return OLFConsolidatorService.Ping();
           }
        }
        
        public bool FileExists(string FileName, string FolderKey)
        {
            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.SessionID = "";
            MetaData.EmulationID = "";
            MetaData.FileName = FileName;
            try{
                bool bFileExists;
                if (OLFOnlineService != null){
                    bFileExists =  OLFOnlineService.FileExists(MetaData);
                }else{
                    bFileExists =  OLFConsolidatorService.FileExists(MetaData);
                }
                return bFileExists;
            }catch (FaultException<WFS.integraPAY.Online.OLFServicesClient.ServerFaultException> ex){ // Log Error
                EventLog.logError(ex, this.GetType().Name, "FileExists");
                return (false);
            };
            
        }
        
        public long GetFileSize(string FileName, string FolderKey)
        {
            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.SessionID = "";
            MetaData.EmulationID = "";
            MetaData.FileName = FileName;
            try{
                long lFileSize = 0;
                if (OLFOnlineService != null){
                    lFileSize =  OLFOnlineService.GetFileSize(MetaData);
                }else{
                    lFileSize =  OLFConsolidatorService.GetFileSize(MetaData);
                }
                return lFileSize;
            }catch (FaultException<WFS.integraPAY.Online.OLFServicesClient.ServerFaultException> ex){
                EventLog.logError(ex, this.GetType().Name, "FileName");
                return 0;
            }
        }
        
        public Stream GetFile(string DownloadFile, string FolderKey)
        {
            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.SessionID = "";
            MetaData.EmulationID = "";
            MetaData.FileName = DownloadFile;
            try{
                Stream strmFile = null;
                if (OLFOnlineService != null){
                    strmFile =  OLFOnlineService.GetFile(MetaData);
                }else{
                    strmFile =  OLFConsolidatorService.GetFile(MetaData);
                }
                return strmFile;
            }catch (FaultException<WFS.integraPAY.Online.OLFServicesClient.ServerFaultException> ex){
                EventLog.logError(ex, this.GetType().Name, "GetFile");
                return null;
            }
        }
        
        public void PutStream(String DestinationFileName, Stream Data, string FolderKey)
        {
            WFS.integraPAY.Online.OLFServicesClient.FileMetaData MetaData = new WFS.integraPAY.Online.OLFServicesClient.FileMetaData();
            MetaData.FileName = DestinationFileName;
            MetaData.SiteKey = SiteOptions.siteKey;
            MetaData.FolderKey = FolderKey;
            UploadItem.data = Data;
            UploadItem.FileMetaData = MetaData; 
            try{
                if (OLFOnlineService != null){
                    OLFOnlineService.PutFile(UploadItem);
                }else{
                    OLFConsolidatorService.PutFile(UploadItem);
                }
                
            }catch (FaultException<WFS.integraPAY.Online.OLFServicesClient.ServerFaultException> ex){
                EventLog.logError(ex, this.GetType().Name, "PutStream");
            }
        }
        public void PutFile(String DestinationFileName, String SourceFileName, string FolderKey)
        {
            if (File.Exists(SourceFileName))
            {
               Stream SourceStream = null;
               try{
                SourceStream = File.OpenRead(SourceFileName);
               }catch (Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "PutFile"); 
               }
               PutStream(DestinationFileName, SourceStream, FolderKey); 
               SourceStream.Close();
               SourceStream.Dispose();
            }else{
                string Message = "File "+SourceFileName+" does not exist.";
                EventLog.logWarning(Message,cEventLog.MessageImportance.Essential);    
            }
        }

    }
}
