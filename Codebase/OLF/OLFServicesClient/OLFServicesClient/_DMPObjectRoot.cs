﻿using System;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: DMP Object Root Class
*
* Modification History
* CR 32562 JMC 03/02/2011 
*    -Reduced the scope of exposed function where possible.    
* WI 96332 CRG 04/11/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for OLFServicesClient
******************************************************************************/
namespace WFS.RecHub.OLFServicesClient
{
    /// <summary>
    /// ipo Image Service Object Root class.
    /// </summary>   
    public abstract class _DMPObjectRoot
    {
        private cSiteOptions _SiteOptions = null;
        private cEventLog _EventLog = null;
        private string _SiteKey;

        public _DMPObjectRoot(string vSiteKey){
            _SiteKey = vSiteKey;
        }
        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        protected string SiteKey{
           get { return _SiteKey; }
        }
        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected cSiteOptions SiteOptions{
            get{
                if (_SiteOptions == null){
                    _SiteOptions = new cSiteOptions(this.SiteKey);
                }
                return _SiteOptions;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog EventLog{
            get{
                if (_EventLog == null){
                    _EventLog = new cEventLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (MessageImportance)SiteOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output messages.
        /// </summary>
        /// <param name="message">Test message to be logged.</param>
        /// <param name="src">Source of the event.</param>
        /// <param name="messageType">MessageType(Information, Warning, Error)</param>
        /// <param name="messageImportance">MessageImportance(Essential, Verbose, Debug)
        /// </param>
        protected void object_outputMessage(string message
                                          , string src
                                          , MessageType messageType
                                          , MessageImportance messageImportance) {

            EventLog.logEvent(message
                            , src
                            , messageType
                            , messageImportance);
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output errors.
        /// </summary>
        /// <param name="e"></param>
        protected void Object_OutputError(Exception e) {
            EventLog.logError(e);
        }

    }
}
