﻿namespace WFS.integraPAY.Online.OLFServicesClient {
    using System.Runtime.Serialization;
    using System;
    using System.IO;    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FileMetaData", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
    [System.SerializableAttribute()]
    public partial class FileMetaData : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string EmulationIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FileNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SessionIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SiteKeyField;

        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FolderKeyField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string EmulationID {
            get {
                return this.EmulationIDField;
            }
            set {
                if ((object.ReferenceEquals(this.EmulationIDField, value) != true)) {
                    this.EmulationIDField = value;
                    this.RaisePropertyChanged("EmulationID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FileName {
            get {
                return this.FileNameField;
            }
            set {
                if ((object.ReferenceEquals(this.FileNameField, value) != true)) {
                    this.FileNameField = value;
                    this.RaisePropertyChanged("FileName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SessionID {
            get {
                return this.SessionIDField;
            }
            set {
                if ((object.ReferenceEquals(this.SessionIDField, value) != true)) {
                    this.SessionIDField = value;
                    this.RaisePropertyChanged("SessionID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SiteKey {
            get {
                return this.SiteKeyField;
            }
            set {
                if ((object.ReferenceEquals(this.SiteKeyField, value) != true)) {
                    this.SiteKeyField = value;
                    this.RaisePropertyChanged("SiteKey");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FolderKey {
            get {
                return this.FolderKeyField;
            }
            set {
                if ((object.ReferenceEquals(this.FolderKeyField, value) != true)) {
                    this.FolderKeyField = value;
                    this.RaisePropertyChanged("FolderKey");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
    [System.SerializableAttribute()]
    public partial class ServerFaultException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string detailsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorcodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string messageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string details {
            get {
                return this.detailsField;
            }
            set {
                if ((object.ReferenceEquals(this.detailsField, value) != true)) {
                    this.detailsField = value;
                    this.RaisePropertyChanged("details");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorcode {
            get {
                return this.errorcodeField;
            }
            set {
                if ((object.ReferenceEquals(this.errorcodeField, value) != true)) {
                    this.errorcodeField = value;
                    this.RaisePropertyChanged("errorcode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string message {
            get {
                return this.messageField;
            }
            set {
                if ((object.ReferenceEquals(this.messageField, value) != true)) {
                    this.messageField = value;
                    this.RaisePropertyChanged("message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="IOLFOnlineService")]
    public interface IOLFOnlineService {
        
        // CODEGEN: Generating message contract since the operation PutFile is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="PutFile", ReplyAction="http://tempuri.org/IOLFImageSrv/PutFileResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.integraPAY.Online.OLFServicesClient.ServerFaultException), Action="http://tempuri.org/IOLFImageSrv/PutFileServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
        WFS.integraPAY.Online.OLFServicesClient.PutFileResponse PutFile(WFS.integraPAY.Online.OLFServicesClient.UploadItem request);
        
        [System.ServiceModel.OperationContractAttribute(Action="GetFile", ReplyAction="http://tempuri.org/IOLFImageSrv/OLFGetFileResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.integraPAY.Online.OLFServicesClient.ServerFaultException), Action="http://tempuri.org/IOLFImageSrv/OLFGetFileServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
        System.IO.Stream GetFile(WFS.integraPAY.Online.OLFServicesClient.FileMetaData MetaData);
        
        [System.ServiceModel.OperationContractAttribute(Action="GetFileSize", ReplyAction="http://tempuri.org/IOLFImageSrv/GetFileSizeResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.integraPAY.Online.OLFServicesClient.ServerFaultException), Action="http://tempuri.org/IOLFImageSrv/GetFileSizeServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
        long GetFileSize(WFS.integraPAY.Online.OLFServicesClient.FileMetaData MetaData);
        
        [System.ServiceModel.OperationContractAttribute(Action="FileExists", ReplyAction="http://tempuri.org/IOLFImageSrv/FileExistsResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.integraPAY.Online.OLFServicesClient.ServerFaultException), Action="http://tempuri.org/IOLFImageSrv/FileExistsServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
        bool FileExists(WFS.integraPAY.Online.OLFServicesClient.FileMetaData MetaData);

        [System.ServiceModel.OperationContractAttribute(Action="GetImageSize", ReplyAction="http://tempuri.org/IOLFImageSrv/GetImageSizeResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.integraPAY.Online.OLFServicesClient.ServerFaultException), Action="http://tempuri.org/IOLFImageSrv/GetImageSizeServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
        long GetImageSize(FileMetaData MetaData, string PICSDate, int BankID, int LockboxID, long BatchID, int Item, string ImageType, short Page);

        [System.ServiceModel.OperationContractAttribute(Action="GetImageJobFileAsAttachment", ReplyAction="http://tempuri.org/IOLFImageSrv/GetImageJobFileAsAttachmentResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.integraPAY.Online.OLFServicesClient.ServerFaultException), Action="http://tempuri.org/IOLFImageSrv/GetImageJobFileAsAttachmentServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
        System.IO.Stream GetImageJobFileAsAttachment(FileMetaData MetaData, Guid OnlineImageQueueID);

        [System.ServiceModel.OperationContractAttribute(Action="GetImageJobSize", ReplyAction="http://tempuri.org/IOLFImageSrv/GetImageJobSizeResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.integraPAY.Online.OLFServicesClient.ServerFaultException), Action="http://tempuri.org/IOLFImageSrv/GetImageJobSizeServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.integraPAY.Online.OLFServices")]
        long GetImageJobSize(FileMetaData MetaData, Guid OnlineImageQueueID);
        
        [System.ServiceModel.OperationContractAttribute(Action="Ping", ReplyAction="http://tempuri.org/IOLFImageSrv/PingResponse")]
        string Ping();
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UploadItem", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class UploadItem {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public WFS.integraPAY.Online.OLFServicesClient.FileMetaData FileMetaData;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public System.IO.Stream data;
        
        public UploadItem() {
        }
        
        public UploadItem(WFS.integraPAY.Online.OLFServicesClient.FileMetaData FileMetaData, System.IO.Stream data) {
            this.FileMetaData = FileMetaData;
            this.data = data;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class PutFileResponse {
        
        public PutFileResponse() {
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    public interface IOLFOnlineServiceChannel : WFS.integraPAY.Online.OLFServicesClient.IOLFOnlineService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
    public partial class OLFOnlineService : System.ServiceModel.ClientBase<WFS.integraPAY.Online.OLFServicesClient.IOLFOnlineService>, WFS.integraPAY.Online.OLFServicesClient.IOLFOnlineService {
        
        public OLFOnlineService() {
        }
        
        public OLFOnlineService(string endpointConfigurationName) : 
               base(endpointConfigurationName) {
        }
        
        public OLFOnlineService(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public OLFOnlineService(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public OLFOnlineService(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        WFS.integraPAY.Online.OLFServicesClient.PutFileResponse WFS.integraPAY.Online.OLFServicesClient.IOLFOnlineService.PutFile(WFS.integraPAY.Online.OLFServicesClient.UploadItem request) {
            return base.Channel.PutFile(request);
        }
        
        public void PutFile(WFS.integraPAY.Online.OLFServicesClient.FileMetaData FileMetaData, System.IO.Stream data) {
            WFS.integraPAY.Online.OLFServicesClient.UploadItem inValue = new WFS.integraPAY.Online.OLFServicesClient.UploadItem();
            inValue.FileMetaData = FileMetaData;
            inValue.data = data;
            WFS.integraPAY.Online.OLFServicesClient.PutFileResponse retVal = ((WFS.integraPAY.Online.OLFServicesClient.IOLFOnlineService)(this)).PutFile(inValue);
        }
        
        public System.IO.Stream GetFile(WFS.integraPAY.Online.OLFServicesClient.FileMetaData MetaData) {
            return base.Channel.GetFile(MetaData);
        }
        
        public long GetFileSize(WFS.integraPAY.Online.OLFServicesClient.FileMetaData MetaData) {
            return base.Channel.GetFileSize(MetaData);
        }
        
        public bool FileExists(WFS.integraPAY.Online.OLFServicesClient.FileMetaData MetaData) {
            return base.Channel.FileExists(MetaData);
        }

        public long GetImageSize(FileMetaData MetaData, string PICSDate, int BankID, int LockboxID, long BatchID, int Item, string ImageType, short Page) {
            return(base.Channel.GetImageSize(MetaData, PICSDate, BankID, LockboxID, BatchID, Item, ImageType, Page));
        }

        public Stream GetImageJobFileAsAttachment(FileMetaData MetaData, Guid OnlineImageQueueID) {
            return(base.Channel.GetImageJobFileAsAttachment(MetaData, OnlineImageQueueID));
        }

        public long GetImageJobSize(FileMetaData MetaData, Guid OnlineImageQueueID) {
            return(base.Channel.GetImageJobSize(MetaData, OnlineImageQueueID));
        }
        
        public string Ping() {
            return base.Channel.Ping();
        }
    }
}
