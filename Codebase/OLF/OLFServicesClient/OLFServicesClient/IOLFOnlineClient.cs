﻿using System;
using System.IO;
using WFS.RecHub.OLFServices;
using WFS.RecHub.OLFServices.DataTransferObjects;

namespace WFS.RecHub.OLFServicesClient
{
    public interface IOLFOnlineClient
    {
        long GetImageSize(FileMetaData MetaData, byte OnlineColorMode, string PICSDate, int BankID, int LockboxID, long BatchID,
            int Item, string ImageType, short Page, long SourceBatchID, string BatchSourceShortName, string ImportTypeShortName);

        Stream GetImageJobFileAsAttachment(Guid OnlineImageQueueID);
        long GetImageJobSize(FileMetaData MetaData, Guid OnlineImageQueueID);
        Stream GetCENDSFileAsAttachment(string FilePath);
        long GetCENDSFileSize(FileMetaData MetaData, string FilePath);
        ImageResponseDto DownloadImage(FileMetaData metaData, ImageRequestDto request);
        ImageResponseDto DownloadSingleImage(SingleImageRequestDto request);
        OlfResponseDto ImagesExist(ImageRequestDto request);
    }
}
