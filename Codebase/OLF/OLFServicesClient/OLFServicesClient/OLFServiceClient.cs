﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Web;
using WFS.RecHub.Common;
using WFS.RecHub.OLFServices;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/02/2011 
*    -New Client module for the OLFServices.OLFService WCF service.
*    -Exposes Ping(), GetSession(), EndSession(), and GetImage() methods
* CR 45986 WJS 11/23/2001
*    -Add ability pass in SiteID
* CR 51010 JMC 03/07/2012
*    -Changed MaxReceivedMessageSize from 64 mb (67108864 bytes) to 2 gb 
*     (2147483647 bytes)
* CR 52588 twe 10/15/2012
*    IF URL starts with https force security mode transport to use SSL (port 443)
* WI 96332 CRG 04/11/2013
*    Change the NameSpace, Framework, Using's and Flower Boxes for OLFServicesClient
* WI 143450 RDS 05/22/2014	Move Binding configuration to config files
* WI 143442 DJW 05/30/2014
*     Updated to handle batch collisions (BatchID to long, and added BatchSourceID)
* WI 169147 SAS 10/01/2014
*   Changes done to change the getImage function signature  
* WI 175213 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName 
* WI 176359 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName  
******************************************************************************/
namespace WFS.RecHub.OLFServicesClient {

    public class OLFServiceClient : _DMPObjectRoot 
	{
        private readonly IOLFService _OLFService = null;  

        private Guid _SessionID;

        public OLFServiceClient(string vSiteKey) : base (vSiteKey)
		{
			if (LogManager.IsDefault) LogManager.Logger = EventLog.IPOtoILogger(this.GetType().Name);
			_OLFService = R360ServiceFactory.Create<IOLFService>();

			// Extract default SessionID from ambient claims
			_SessionID = R360ServiceContext.Current.GetSessionID();
        }

        public string Ping() 
        {
            using (new OperationContextScope((IContextChannel)_OLFService))
            {
                return _OLFService.Ping();
            }
        }

        public bool GetSession(string LogonName,
                               string Password,
                               out Guid SessionID) {

            bool bolRetVal;

            try {
                using (new OperationContextScope((IContextChannel) _OLFService)) {

                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", this.SiteKey);

                    bolRetVal = _OLFService.GetSession(LogonName,
                                                       Password,
                                                       out SessionID);
                }
            } catch(FaultException<InvalidSiteKeyFault>) {
                throw(new InvalidSiteKeyException());
            } catch (FaultException<ServerFaultException> ex) {
                EventLog.logError(ex, this.GetType().Name, "GetSession");
                throw(new Exception(ex.Message, ex));
            } catch(FaultException ex) {
                EventLog.logError(ex, this.GetType().Name, "GetSession");
                throw(new Exception(ex.Message, ex));
            } catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "GetSession");
                throw;
            }

            return(bolRetVal);
        }

        public bool EndSession(Guid SessionID) {

            bool bolRetVal;

            try {
                using (new OperationContextScope((IContextChannel) _OLFService)) {

                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SessionID", _SessionID.ToString());
                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", this.SiteKey);

                    bolRetVal = _OLFService.EndSession();
                }
            } catch(FaultException<InvalidSiteKeyFault>) {
                throw(new InvalidSiteKeyException());
            } catch(FaultException<InvalidSessionIDFault>) {
                throw(new InvalidSessionIDException());
            } catch (FaultException<ServerFaultException> ex) {
                EventLog.logError(ex, this.GetType().Name, "EndSession");
                throw(new Exception(ex.Message, ex));
            } catch(FaultException ex) {
                EventLog.logError(ex, this.GetType().Name, "EndSession");
                throw(new Exception(ex.Message, ex));
            } catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "EndSession");
                throw;
            }

            return(bolRetVal);
        }

        public void SetSession(Guid SessionIDValue) {
            _SessionID = SessionIDValue;
        }

        public bool GetImage(int BankID,
                            int LockboxID,
                            int ImmutableDateKey,
                            int DepositDateKey,
                            long BatchID,
                            int BatchSequence,
                            bool IsCheck,
                            int SiteID,
                            long SourceBatchID,
                            string BatchSourceShortName,
                            string ImportTypeShortName,
                            out byte[][] ImageList,
                            out string FileDescriptor,
                            out string FileExtension)
        {
            EventLog.logEvent("GetImage: " +
                    ", BankID=" + BankID +
                    ", LockboxID=" + LockboxID +
                    ", ImmutableDateKey=" + ImmutableDateKey +
                    ", DepositDateKey=" + DepositDateKey +
                    ", BatchID=" + BatchID +
                    ", BatchSequence=" + BatchSequence +
                    ", SiteID=" + SiteID +
                    ", SourceBatchID=" + SourceBatchID +
                    ", BatchSequence=" + BatchSequence +
                    ", BatchSourceShortName=" + BatchSourceShortName,
                    this.GetType().Name, MessageImportance.Debug);

            return GetImage(BankID,
                             LockboxID,
                             ImmutableDateKey,
                             DepositDateKey,
                             BatchID,
                             BatchSequence,
                             IsCheck,
                             SiteID,
                             SourceBatchID,
                             BatchSourceShortName,
                             ImportTypeShortName,
                             -1,
                             out ImageList,
                             out FileDescriptor,
                             out FileExtension);
        }

        public bool GetImage(int BankID,
                             int LockboxID,
                             int ImmutableDateKey,
                             int DepositDateKey,
                             long BatchID,
                             int BatchSequence,
                             bool IsCheck,
                             int SiteID,
                             long SourceBatchID,
                             string BatchSourceShortName,
                             string ImportTypeShortName,
                             int PaymentTypeId,
                             out byte[][] ImageList,
                             out string FileDescriptor, 
                             out string FileExtension) {

            bool bolImageGot;

            try {
                using (new OperationContextScope((IContextChannel) _OLFService)) {

                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SessionID", _SessionID.ToString());
                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", this.SiteKey);

                    bolImageGot =  _OLFService.GetImage(BankID,
                                                        LockboxID,
                                                        ImmutableDateKey,
                                                        DepositDateKey,
                                                        BatchID,
                                                        BatchSequence,
                                                        IsCheck,
                                                        SiteID,
                                                        SourceBatchID,
                                                        BatchSourceShortName,
                                                        ImportTypeShortName,
                                                        PaymentTypeId,
                                                        out ImageList,
                                                        out FileDescriptor, 
                                                        out FileExtension);
                }
            } catch(FaultException<InvalidSiteKeyFault>) {
                throw(new InvalidSiteKeyException());
            } catch(FaultException<InvalidSessionIDFault>) {
                throw(new InvalidSessionIDException());
            } catch(FaultException<SessionExpiredFault>) {
                throw(new SessionExpiredException());
            } catch(FaultException<InvalidSessionFault>) {
                throw(new InvalidSessionException());
            } catch(FaultException<ImageNotFoundInDbFault>) {
                throw (new ImageNotFoundInDbException(BankID, LockboxID, ImmutableDateKey, BatchID, BatchSequence));
            } catch(FaultException<ImageNotFoundOnDiskFault>) {
                throw (new ImageNotFoundOnDiskException(BankID, LockboxID, ImmutableDateKey, BatchID, BatchSequence));
            } catch(FaultException<ServerFaultException> ex) {
                EventLog.logError(ex, this.GetType().Name, "GetImage");
                throw(new Exception(ex.Message, ex));
            } catch(FaultException ex) {
                EventLog.logError(ex, this.GetType().Name, "GetImage");
                throw(new Exception(ex.Message, ex));
            } catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "GetImage");
                throw(new Exception(ex.Message, ex));
            }

            return(bolImageGot);
        }

        public bool DeleteBatchImages(
                             int clientID,
                             int ProcessingDateKey,
                             int SiteID,
                             string importTypeShortName,
                             string batchPaymentSourceName,
                             long SourceBatchID)
        {

            bool bolImageGot;

            try
            {
                using (new OperationContextScope((IContextChannel)_OLFService))
                {

                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SessionID", _SessionID.ToString());
                    WebOperationContext.Current.OutgoingRequest.Headers.Add("SiteKey", this.SiteKey);

                    bolImageGot = _OLFService.DeleteBatchImages(
                                                        clientID,
                                                        ProcessingDateKey,
                                                        SiteID,
                                                        importTypeShortName,
                                                        batchPaymentSourceName,
                                                        SourceBatchID);
                }
            }
            catch (FaultException<InvalidSiteKeyFault>)
            {
                throw (new InvalidSiteKeyException());
            }
            catch (FaultException<InvalidSessionIDFault>)
            {
                throw (new InvalidSessionIDException());
            }
            catch (FaultException<SessionExpiredFault>)
            {
                throw (new SessionExpiredException());
            }
            catch (FaultException<InvalidSessionFault>)
            {
                throw (new InvalidSessionException());
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteBatchImages");
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteBatchImages");
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteBatchImages");
                throw (new Exception(ex.Message, ex));
            }

            return (bolImageGot);
        }
    }
}