﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using Microsoft.QualityTools.Testing.Fakes;
using WFS.RecHub.OLFImageAPI;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.DAL.Fakes;
using WFS.RecHub.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.OLFImageAPI.Fakes;

namespace OLFImageTests
{
    [TestClass]
    public class ImageJobManagerTests
    {
        private ImageDownload imageDownload;
        private ImageRequestDto request;

        [TestInitialize]
        public void Setup()
        {
            imageDownload = new ImageDownload(new ImageDisplayModes());
            request = new ImageRequestDto
            {
                SiteKey = "IPOnline",
                IsCheck = true,
                BankId = 1,
                WorkgroupId = 4350,
                BatchId = 137,
                DepositDate = new DateTime(2006, 01, 12),
                BatchSequence = 1,
                TransactionId = 1,
                SessionId = Guid.NewGuid()
            };

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ImageDownload_DownloadWithNullRequestContext_ThrowsArgumentNullException()
        {

            //arrange

            //act
            imageDownload.Download(null);

            //assert
            Assert.Fail("This test should have failed with an 'ArgumentNullException'");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ImageDownload_ToItemListWithNullRequestContext_ThrowsArgumentNullException()
        {

            //arrange

            //act
            imageDownload.ToItemList(null);

            //assert
            Assert.Fail("This test should have failed with an 'ArgumentNullException'");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ImageDownload_ToItemListWithOutSiteKey_ThrowsArgumentNullException()
        {

            //arrange
            request.SiteKey = null;

            //act
            var actual = imageDownload.ToItemList(request);

            //assert
            Assert.Fail("This test should have failed with an 'ArgumentNullException'");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ImageDownload_ToItemListWithOutSessionId_ThrowsArgumentNullException()
        {

            //arrange
            request.SessionId = Guid.Empty;

            //act
            var actual = imageDownload.ToItemList(request);

            //assert
            Assert.Fail("This test should have failed with an 'ArgumentException'");
        }

        [TestMethod]
        [Ignore]
        public void ImageDownload_ToItemListForValidCheck_ReturnsCheckItem()
        {
            using (ShimsContext.Create())
            {
                //arrange
                ShimcItemProcDAL.AllInstances.GetCheckGuidInt32Int32Int64DateTimeInt32Int32DataTableOut =
                    (cItemProcDAL itemProcDAL, Guid sessionId, int bankId, int workgroupId, long batchId, DateTime depositDate, int transactiondId, int batchSequence, out DataTable dataTable) =>
                    {
                        dataTable = GetDataRow().Table;
                        return true;
                    };

                //act
                var actual = imageDownload.ToItemList(request);

                //assert
                Assert.IsNotNull(actual[0]);
                Assert.IsInstanceOfType(actual[0], typeof(cCheck));
            }

        }

        [TestMethod]
        [Ignore]
        public void ImageDownload_ToItemListForValidDocument_ReturnsDocumentItem()
        {
            using (ShimsContext.Create())
            {
                //arrange
                request.IsCheck = false;
                ShimcItemProcDAL.AllInstances.GetDocumentGuidInt32Int32DateTimeInt64Int32Int32BooleanDataTableOut =
                    (cItemProcDAL itemProcDAL, Guid sessionId, int bankId, int workgroupId, DateTime depositdatekey, long batchId, int transactionid, int batchSequence, bool something, out DataTable dataTable) =>
                    {
                        dataTable = GetDataRow().Table;
                        return true;
                    };

                //act
                var actual = imageDownload.ToItemList(request);

                //assert
                Assert.IsNotNull(actual[0]);
                Assert.IsInstanceOfType(actual[0], typeof(cDocument));
            }

        }

        [TestCleanup]
        public void TearDown()
        {
            request = null;
            imageDownload = null;
        }

        private DataRow GetDataRow()
        {
            using (DataTable dt = GetDataTable())
            {
                var row = dt.NewRow();

                row["BankId"] = 1;
                row["LockboxId"] = 4530;
                row["ProcessingDateKey"] = 20060112;

                dt.Rows.Add(row);
                return dt.Rows[0];
            }
        }

        private DataTable GetDataTable()
        {
            using (DataTable dt = new DataTable())
            {
                dt.Columns.Add("BankId", typeof(int));
                dt.Columns.Add("LockboxId", typeof(int));
                dt.Columns.Add("BatchId", typeof(int));
                dt.Columns.Add("BatchSequence", typeof(int));
                dt.Columns.Add("BatchSiteCode", typeof(int));
                dt.Columns.Add("ProcessingDateKey", typeof(int));
                dt.Columns.Add("PicsDate", typeof(int));

                return dt;
            }
        }
    }
}
