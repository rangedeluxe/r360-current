﻿using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.OLFImageAPI;

namespace OLFImageTests
{
    [TestClass]
    public class JobRequestTests
    {
        /// <summary>
        /// Non-abstract descendant of <see cref="_JobRequest"/> for use in tests.
        /// </summary>
        private class FakeJobRequest : _JobRequest
        {
        }

        private ImageDisplayModes ImageSidesBoth
            => new ImageDisplayModes(OLFImageDisplayMode.BothSides, OLFImageDisplayMode.BothSides);
        private ImageDisplayModes ImageSidesFront
            => new ImageDisplayModes(OLFImageDisplayMode.FrontOnly, OLFImageDisplayMode.FrontOnly);
        private ImageDisplayModes ImageSidesMixed
            => new ImageDisplayModes(OLFImageDisplayMode.FrontOnly, OLFImageDisplayMode.BothSides);

        private static void CheckImageDisplayModes(_JobRequest.JobRequestOptions options,
            OLFImageDisplayMode check, OLFImageDisplayMode document)
        {
            Assert.AreEqual(check, options.checkImageDisplayMode, "CheckImageDisplayMode");
            Assert.AreEqual(document, options.documentImageDisplayMode, "DocumentImageDisplayMode");
        }
        private static void CheckImageDisplayModes(_JobRequest.JobRequestOptions options,
            ImageDisplayModes expectedModes)
        {
            CheckImageDisplayModes(options,
                check: expectedModes.CheckImageDisplayMode,
                document: expectedModes.DocumentImageDisplayMode);
        }
        private static XElement CreateDefaultOptionsElement()
        {
            var optionsElement = new XElement("jobRequestOptions");
            optionsElement.SetAttributeValue("displayScannedCheck", "True");
            optionsElement.SetAttributeValue("returnType", "ZIP");
            return optionsElement;
        }
        private _JobRequest.JobRequestOptions ParseOptions(XElement jobRequestOptionsElement,
            ImageDisplayModes defaultDisplayModes)
        {
            // Wrap the jobRequestOptions element in a jobRequest element
            Assert.AreEqual("jobRequestOptions", jobRequestOptionsElement.Name,
                $"Incorrect XML element passed to {nameof(ParseOptions)}");

            var jobRequestElement = new XElement("jobRequest", jobRequestOptionsElement);
            return ParseOptionsFromJobRequestXElement(jobRequestElement, defaultDisplayModes);
        }
        private _JobRequest.JobRequestOptions ParseOptionsFromJobRequestXElement(XElement jobRequestXElement,
            ImageDisplayModes defaultDisplayModes)
        {
            // Convert the XElement to an XmlElement
            Assert.AreEqual("jobRequest", jobRequestXElement.Name,
                $"Incorrect XML element passed to {nameof(ParseOptionsFromJobRequestXElement)}");

            var document = new XmlDocument();
            document.LoadXml(jobRequestXElement.ToString());
            return ParseOptionsFromJobRequestXmlElement(document.DocumentElement, defaultDisplayModes);
        }
        private _JobRequest.JobRequestOptions ParseOptionsFromJobRequestXmlElement(XmlElement jobRequestXmlElement,
            ImageDisplayModes defaultDisplayModes)
        {
            // Pass the XmlElement to getJobRequestOptions
            Assert.AreEqual("jobRequest", jobRequestXmlElement.Name,
                $"Incorrect XML element passed to {nameof(ParseOptionsFromJobRequestXmlElement)}");

            _JobRequest.JobRequestOptions options;

            var jobRequest = new FakeJobRequest();
            jobRequest.getJobRequestOptions(jobRequestXmlElement, () => defaultDisplayModes, out options);

            return options;
        }

        [TestMethod]
        public void GetJobRequestOptions_NoDisplayModesInXml_PreferencesAreBothSides()
        {
            var optionsElement = CreateDefaultOptionsElement();
            var options = ParseOptions(optionsElement, ImageSidesBoth);
            CheckImageDisplayModes(options, ImageSidesBoth);
        }
        [TestMethod]
        public void GetJobRequestOptions_NoDisplayModesInXml_PreferencesAreFrontOnly()
        {
            var optionsElement = CreateDefaultOptionsElement();
            var options = ParseOptions(optionsElement, ImageSidesFront);
            CheckImageDisplayModes(options, ImageSidesFront);
        }
        [TestMethod]
        public void GetJobRequestOptions_NoDisplayModesInXml_PreferencesAreDifferent()
        {
            var optionsElement = CreateDefaultOptionsElement();
            var options = ParseOptions(optionsElement, ImageSidesMixed);
            CheckImageDisplayModes(options, ImageSidesMixed);
        }
        [TestMethod]
        public void GetJobRequestOptions_CheckDisplayModeInXml()
        {
            var optionsElement = CreateDefaultOptionsElement();
            optionsElement.SetAttributeValue("checkImageDisplayMode", (int) OLFImageDisplayMode.FrontOnly);

            var options = ParseOptions(optionsElement, ImageSidesBoth);
            CheckImageDisplayModes(options,
                check: OLFImageDisplayMode.FrontOnly,
                document: OLFImageDisplayMode.BothSides);
        }
        [TestMethod]
        public void GetJobRequestOptions_DocumentDisplayModeInXml()
        {
            var optionsElement = CreateDefaultOptionsElement();
            optionsElement.SetAttributeValue("documentImageDisplayMode", (int) OLFImageDisplayMode.FrontOnly);

            var options = ParseOptions(optionsElement, ImageSidesBoth);
            CheckImageDisplayModes(options,
                check: OLFImageDisplayMode.BothSides,
                document: OLFImageDisplayMode.FrontOnly);
        }
    }
}