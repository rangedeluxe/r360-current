﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.util;
using Org.BouncyCastle.Cms;
using WFS.RecHub.OLFImageAPI;
using OLFImageTests.Helpers;

namespace OLFImageTests {
    [TestClass]
    public class JobRequestSearchResultsTests {

        [TestMethod]
        public void JobRequestSearchResults_Check_GenerateSurrogateImage_WIRE_ImageShouldGenerate() {
            string resultingFileName=string.Empty;
            TestHelper.StartWithCleanImageFolder();
            using (cJobRequestSearchResults_TestWrapper wrapper = new cJobRequestSearchResults_TestWrapper() {siteKey = "Online"}) {
                resultingFileName = wrapper.Explose_GenerateSurrogateImage(
                        XMLToDataRow(Properties.Resources.GenerateSurrogateImage_TestSchema, Properties.Resources.GenerateSurrogateImage_Wire_TestData), 
                        TestHelper.ImagePath,
                        true);
            }
            Assert.AreNotEqual(resultingFileName, string.Empty);
            Assert.IsTrue(File.Exists(Path.Combine(TestHelper.ImagePath, resultingFileName)));
            TestHelper.StartWithCleanImageFolder();
        }

        [TestMethod]
        public void JobRequestSearchResults_Document_GenerateSurrogateImage_WIRE_ImageShouldGenerate() {
            string resultingFileName = string.Empty;
            TestHelper.StartWithCleanImageFolder();
            using (cJobRequestSearchResults_TestWrapper wrapper = new cJobRequestSearchResults_TestWrapper() { siteKey = "Online" }) {
                resultingFileName = wrapper.Explose_GenerateSurrogateImage(
                        XMLToDataRow(Properties.Resources.GenerateSurrogateImage_TestSchema, Properties.Resources.GenerateSurrogateImage_Wire_TestData), 
                        TestHelper.ImagePath,
                        false);
            }
            Assert.AreNotEqual(resultingFileName, string.Empty);
            Assert.IsTrue(File.Exists(Path.Combine(TestHelper.ImagePath, resultingFileName)));
            TestHelper.StartWithCleanImageFolder();
        }

        [TestMethod]
        public void JobRequestSearchResults_Check_GenerateSurrogateImage_Invalid_ImageShouldNotGenerate() {
            string resultingFileName = string.Empty;
            TestHelper.StartWithCleanImageFolder();
            using (cJobRequestSearchResults_TestWrapper wrapper = new cJobRequestSearchResults_TestWrapper() { siteKey = "Online" }) {
                resultingFileName = wrapper.Explose_GenerateSurrogateImage(
                        XMLToDataRow(Properties.Resources.GenerateSurrogateImage_TestSchema, Properties.Resources.GenerateSurrogateImage_InvalidPaymentType_TestData), 
                        TestHelper.ImagePath,
                        true);
            }
            Assert.AreEqual(resultingFileName, string.Empty);
            TestHelper.StartWithCleanImageFolder();
        }

        [TestMethod]
        public void JobRequestSearchResults_Document_GenerateSurrogateImage_Invalid_ImageShouldNotGenerate() {
            string resultingFileName = string.Empty;
            TestHelper.StartWithCleanImageFolder();
            using (cJobRequestSearchResults_TestWrapper wrapper = new cJobRequestSearchResults_TestWrapper() { siteKey = "Online" }) {
                resultingFileName = wrapper.Explose_GenerateSurrogateImage(
                        XMLToDataRow(Properties.Resources.GenerateSurrogateImage_TestSchema, Properties.Resources.GenerateSurrogateImage_InvalidPaymentType_TestData), 
                        TestHelper.ImagePath,
                        false);
            }
            Assert.AreEqual(resultingFileName, string.Empty);
            TestHelper.StartWithCleanImageFolder();
        }

        private DataRow XMLToDataRow(string xsdData, string xmlData) {
            DataSet tempDataSet = new DataSet();
            tempDataSet.ReadXmlSchema(new StringReader(xsdData));
            tempDataSet.ReadXml(new StringReader(xmlData));
            DataRow result = tempDataSet.Tables[0].Rows[0];
            return result;
        }
    }

    public class cJobRequestSearchResults_TestWrapper : cJobRequestSearchResults {
        public string Explose_GenerateSurrogateImage(DataRow imageRow, string outputImagePath, bool isCheck) {
            return GenerateSurrogateImage(imageRow, outputImagePath, isCheck);
        }
    }
}
