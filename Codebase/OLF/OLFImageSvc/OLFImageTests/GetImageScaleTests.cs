﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.OLFImageAPI;

namespace OLFImageTests
{
    [TestClass]
    public class GetImageScaleTests
    {
        [TestInitialize]
        public void SetUp()
        {
            BoundsWidth = 0;
            BoundsHeight = 0;
            ImageWidth = 0;
            ImageHeight = 0;
            ScaleWidth = 0;
            ScaleHeight = 0;
        }

        private float BoundsWidth { get; set; }
        private float BoundsHeight { get; set; }
        private float ImageWidth { get; set; }
        private float ImageHeight { get; set; }
        private float ScaleWidth { get; set; }
        private float ScaleHeight { get; set; }

        private GetImageScaleTests ImageSize(float width, float height)
        {
            ImageWidth = width;
            ImageHeight = height;
            return this;
        }
        private GetImageScaleTests InBounds(float width, float height)
        {
            BoundsWidth = width;
            BoundsHeight = height;
            return this;
        }
        private void IsScaledTo(float expectedScaledWidth, float expectedScaledHeight)
        {
            float scaledWidth;
            float scaledHeight;
            ImageRpsReportBase.GetImageScale(ImageWidth, ImageHeight, BoundsWidth, BoundsHeight,
                out scaledWidth, out scaledHeight);
            Assert.AreEqual(expectedScaledWidth, scaledWidth, 0.001, "ScaledWidth");
            Assert.AreEqual(expectedScaledHeight, scaledHeight, 0.001, "ScaledHeight");
        }

        // Basic cases: square bounds

        [TestMethod]
        public void SquareBounds_SquareImage_SameSize()
        {
            ImageSize(10, 10).InBounds(10, 10).IsScaledTo(10, 10);
        }
        [TestMethod]
        public void SquareBounds_SquareImage_Smaller()
        {
            ImageSize(5, 5).InBounds(10, 10).IsScaledTo(10, 10);
        }
        [TestMethod]
        public void SquareBounds_SquareImage_Larger()
        {
            ImageSize(15, 15).InBounds(10, 10).IsScaledTo(10, 10);
        }
        [TestMethod]
        public void SquareBounds_WideImage_SameWidth()
        {
            ImageSize(10, 5).InBounds(10, 10).IsScaledTo(10, 5);
        }
        [TestMethod]
        public void SquareBounds_WideImage_Smaller()
        {
            ImageSize(5, 2.5f).InBounds(10, 10).IsScaledTo(10, 5);
        }
        [TestMethod]
        public void SquareBounds_WideImage_Larger()
        {
            ImageSize(15, 7.5f).InBounds(10, 10).IsScaledTo(10, 5);
        }
        [TestMethod]
        public void SquareBounds_TallImage_SameHeight()
        {
            ImageSize(5, 10).InBounds(10, 10).IsScaledTo(5, 10);
        }
        [TestMethod]
        public void SquareBounds_TallImage_Smaller()
        {
            ImageSize(2.5f, 5).InBounds(10, 10).IsScaledTo(5, 10);
        }
        [TestMethod]
        public void SquareBounds_TallImage_Larger()
        {
            ImageSize(7.5f, 15).InBounds(10, 10).IsScaledTo(5, 10);
        }

        // More interesting cases: non-square bounds.
        // These probably don't need to test all the "larger/smaller/same size" cases;
        // that's covered above. Here we're more about the shape.

        [TestMethod]
        public void HalfPageBounds_WideImage()
        {
            ImageSize(5, 2.5f).InBounds(8.5f, 5.5f).IsScaledTo(8.5f, 4.25f);
        }
        [TestMethod]
        public void HalfPageBounds_ImageSlightlyWiderThanBounds()
        {
            ImageSize(9, 5.5f).InBounds(8.5f, 5.5f).IsScaledTo(8.5f, 5.194f);
        }
        [TestMethod]
        public void HalfPageBounds_ImageSlightlyWiderThanTall()
        {
            ImageSize(6, 5.5f).InBounds(8.5f, 5.5f).IsScaledTo(6, 5.5f);
        }
        [TestMethod]
        public void HalfPageBounds_SquareImage()
        {
            ImageSize(6, 6).InBounds(8.5f, 5.5f).IsScaledTo(5.5f, 5.5f);
        }
        [TestMethod]
        public void HalfPageBounds_ImageSlightlyTallerThanWide()
        {
            ImageSize(5.5f, 6).InBounds(8.5f, 5.5f).IsScaledTo(5.042f, 5.5f);
        }
        [TestMethod]
        public void HalfPageBounds_TallImage()
        {
            ImageSize(2.5f, 5).InBounds(8.5f, 5.5f).IsScaledTo(2.75f, 5.5f);
        }
    }
}