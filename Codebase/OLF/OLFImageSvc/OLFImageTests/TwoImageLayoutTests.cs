﻿using iTextSharp.text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.OLFImageAPI;

namespace OLFImageTests
{
    [TestClass]
    public class TwoImageLayoutTests
    {
        // Real PDF coordinates are typically 72 pixels per inch, but here we'll
        // use 100 pixels per inch to make it easier to visualize
        private const float PageWidth = 850;
        private const float PageHeight = 1100;
        private const float LeftMargin = 100.25f;
        private const float RightMargin = 100.25f;
        private const float HeaderHeight = 299.75f;
        private const float FooterHeight = 149.75f;
        private const float VerticalSpaceBetweenImages = 50.25f;

        [TestInitialize]
        public void SetUp()
        {
            ContentArea = new RectangleReadOnly(
                llx: LeftMargin, lly: FooterHeight,
                urx: PageWidth - RightMargin, ury: PageHeight - HeaderHeight);
            Layout = new TwoImageLayout(ContentArea, VerticalSpaceBetweenImages);
        }

        private RectangleReadOnly ContentArea { get; set; }
        private TwoImageLayout Layout { get; set; }

        [TestMethod]
        public void FirstImageRectangle()
        {
            // Top of page: 1100
            // Top of content area: 800.25
            // Bottom of content area: 149.75
            // Bottom of page: 0
            // The space from 800.25..149.75 (height=650.5) should be divided into
            // two 300.125-high rectangles with a margin of 50.25 between them.

            // Use iTextSharp backwards coordinates (Y=0 at the bottom of the page, positive=up)
            // and upside-down rectangle (lower-left coordinates, then upper-right coordinates).
            var imageRectangle = Layout.GetImageRectangle();
            Assert.AreEqual(ContentArea.Left, imageRectangle.Left, "Left");
            Assert.AreEqual(ContentArea.Right, imageRectangle.Right, "Right");
            Assert.AreEqual(800.25f, imageRectangle.Top, "Top");
            Assert.AreEqual(500.125f, imageRectangle.Bottom, "Bottom");
        }
        [TestMethod]
        public void SecondImageRectangle()
        {
            const float firstImageHeight = 200.5f;

            Layout.AddImage(firstImageHeight);

            // Use iTextSharp backwards coordinates (Y=0 at the bottom of the page, positive=up)
            // and upside-down rectangle (lower-left coordinates, then upper-right coordinates).
            var imageRectangle = Layout.GetImageRectangle();
            // imageRectangle should start HeaderHeight + firstImageHeight + VerticalSpaceBetweenImages
            // from the top of the page (so at upside-down Y = 1100 - 299.75 - 200.5 - 50.25),
            // and go to the bottom of the page's content area.
            Assert.AreEqual(ContentArea.Left, imageRectangle.Left, "Left");
            Assert.AreEqual(ContentArea.Right, imageRectangle.Right, "Right");
            Assert.AreEqual(549.5f, imageRectangle.Top, "Top");
            Assert.AreEqual(149.75f, imageRectangle.Bottom, "Bottom");
        }
    }
}