﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLFImageTests.Helpers
{
    public static class TestHelper
    {

        public static string ImagePath
        {
            get { return @".\TestImages";  }
        }

        public static void StartWithCleanImageFolder()
        {
            if (Directory.Exists(ImagePath))
            {
                Directory.Delete(ImagePath, true);
            }
            Directory.CreateDirectory(ImagePath);
        }
    }
}
