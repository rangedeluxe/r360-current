﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Fakes;

namespace OLFImageTests
{
    [TestClass]
    public class PdfUtilityTests
    {
        cParsedAddendaCollection parsedAddendas;

        [TestInitialize]
        public void Setup()
        {
            parsedAddendas = new cParsedAddendaCollection()
            {
                new cParsedAddenda {BatchSequence = 2, DisplayName = "amount", FieldName = "amount", FieldValue = "22.56", UILabel = "Amount" },
                new cParsedAddenda {BatchSequence = 2, DisplayName = "account number", FieldName = "accountnumber", FieldValue = "234567", UILabel = "Account Number" },
                new cParsedAddenda {BatchSequence = 1, DisplayName = "amount", FieldName = "amount", FieldValue = "11.56", UILabel = "Amount" },
                new cParsedAddenda {BatchSequence = 1, DisplayName = "account number", FieldName = "accountnumber", FieldValue = "123456", UILabel = "Account Number" }
            };
        }

        [TestMethod]
        public void Can_GeneratePdfByteArray_FromImageData_ForACH()
        {
            //arrange
            var imageData = new cImageData("unittest");
            var itemRequest = new StubIItem();

            //act
            var actual =  imageData.ToACHPaymentPdfBytes(itemRequest);

            //assert
            Assert.IsNotNull(actual);
            Assert.IsTrue(actual.Length > 0);
        }

        [TestMethod]
        public void Can_FormatParsedAddendaField_ToCurrency()
        {
            //arrange
            var expected = "$9,999,999.99";
            var parsedAddenda = new cParsedAddenda
            {
                DataType = 7,
                FieldName = "amount",
                FieldValue = "9999999.99"
            };

            //act
            var actual = parsedAddenda.Format().FieldValue;

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WillNot_FormatParsedAddendaField_ToCurrency()
        {
            //arrange
            var expected = "9999999.99";
            var parsedAddenda = new cParsedAddenda
            {
                FieldName = "amount",
                FieldValue = "9999999.99"
            };

            //act
            var actual = parsedAddenda.Format().FieldValue;

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Can_AddACHRawAddendaData_ToPdf()
        {
            //arrange
            var document = new Document();
            var rawPaymentDataArrayList = new ArrayList()
            {
                "line 1",
                "line2"
            };

            //act
            document.AddACHRawAddenda(rawPaymentDataArrayList);

            //assert
            AssertACHRawAddenda(document, rawPaymentDataArrayList);
        }

        [TestMethod]
        public void Can_AddACHParsedAddendaData_ToPdfTable()
        {
            //arrange
            var table = new Table();

            //act
            table = table.AddACHParsedAddenda(parsedAddendas, 1);

            //assert
            Assert.AreEqual(2, table.Rows.Count);
            Assert.AreEqual(4, table.Columns.Count);
        }

        [TestMethod]
        public void Can_AddACHParsedAddendaData_ToNullPdfTable()
        {
            //arrange
            Table table = null;

            //act
            table = table.AddACHParsedAddenda(parsedAddendas, 1);

            //assert
            Assert.IsNotNull(table);
            Assert.AreEqual(2, table.Rows.Count);
            Assert.AreEqual(4, table.Columns.Count);
        }

        [TestMethod]
        public void Will_AddLineBreak_WhenStringIsTooLong()
        {
            //arrange
            var expected = $"ThisIsAVeryLongTextM{Chars.LF}essageThatNeedsToBeS{Chars.LF}plitAcrossLines";

            //act
            var actual = "ThisIsAVeryLongTextMessageThatNeedsToBeSplitAcrossLines".FormatText();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Will_AddLineBreak_WhenCellTextExceedsSizeOfTheColumn()
        {
            //arrange
            var table = new Table();
            var column = table.AddColumn(Unit.FromInch(.5));
            table.AddRow();
            var cell = column[0];

            var expected = new List<DocumentObject>
            {
                new Text { Content = "ThisIsAVeryLongTextM" },
                new Character { SymbolName = SymbolName.LineBreak },
                new Text { Content = "essageThatNeedsToBeS"},
                new Character { SymbolName = SymbolName.LineBreak },
                new Text { Content = "plitAcrossLines" }
            };

            //act
            cell.AddCellText("ThisIsAVeryLongTextMessageThatNeedsToBeSplitAcrossLines");

            //assert
            AssertCellText(cell, expected);
        }
        
        [TestMethod]
        public void Will_MaintainSpaces_InText()
        {
            //arrange
            var document = new Document();
            var myText = "PleaseKeepMy        Spaces";
            var expected = myText.Replace(Chars.Space, Chars.NonBreakableSpace);

            //act
            document.AddParagraphWithNoBreakSpaces(myText, string.Empty);

            //assert
            AssertParagraphText(document.LastSection.Elements[0] as Paragraph, expected);
        }

        [TestMethod]
        public void Can_Add_ACHTable()
        {
            //arrange
            var document = new Document();

            //act
            var actual = document.AddACHTable();

            //assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(4, actual.Columns.Count);
        }

        [TestMethod]
        public void Can_GetFieldValue_FromDictionary()
        {
            //arrange
            var fields = new Dictionary<string, string>
            {
                { "Amount", "24.56" },
                { "Account", "1234567" }
            };

            //act
            var actual = fields.GetFieldValue("Account");

            //assert
            Assert.AreEqual("1234567", actual);
        }

        private void AssertCellText(Cell cell, IList<DocumentObject> expected)
        {
            //assert
            AssertCell(cell);

            var paragraph = cell.Elements[0] as Paragraph;
            Assert.AreEqual(expected.Count, paragraph.Elements.Count);
            for (int i = 0; i < expected.Count; i++)
            {
                if (paragraph.Elements[i] is Text)
                {
                    Assert.IsInstanceOfType(expected[i], typeof(Text));
                    Assert.AreEqual((expected[i] as Text).Content, (paragraph.Elements[i] as Text).Content);
                }

                if (paragraph.Elements[i] is Character)
                {
                    Assert.IsInstanceOfType(expected[i], typeof(Character));
                    Assert.AreEqual((expected[i] as Character).SymbolName, (paragraph.Elements[i] as Character).SymbolName);
                }
            }
        }

        private void AssertCellText(Cell cell, string expected)
        {
            //assert
            AssertCell(cell);

            var paragraph = cell.Elements[0] as Paragraph;
            AssertParagraphText(paragraph, expected);
        }

        private void AssertCell(Cell cell)
        {
            //assert
            Assert.AreEqual(1, cell.Elements.Count);
            Assert.IsInstanceOfType(cell.Elements[0], typeof(Paragraph));
        }

        private void AssertParagraphText(Paragraph paragraph, string expected)
        {
            Assert.IsNotNull(paragraph);
            Assert.AreEqual(1, paragraph.Elements.Count);
            Assert.IsInstanceOfType(paragraph.Elements[0], typeof(Text));

            var text = paragraph.Elements[0] as Text;
            Assert.AreEqual(expected, text.Content, true);
        }

        private void AssertACHRawAddenda(Document document, ArrayList rawPaymentData)
        {
            Assert.IsNotNull(document);
            var elements = document.LastSection.Elements;
            //should be three paragraphs
            Assert.AreEqual(3, elements.Count);
            Assert.IsInstanceOfType(elements[0], typeof(Paragraph));
            Assert.IsInstanceOfType(elements[1], typeof(Paragraph));
            Assert.IsInstanceOfType(elements[2], typeof(Paragraph));

            var paragraphs = new List<Paragraph>
            {
                elements[0] as Paragraph,
                elements[1] as Paragraph,
                elements[2] as Paragraph,
            };

            var text = new List<Text>();
            paragraphs.ToList().ForEach(p =>
            {
                foreach(var element in p.Elements)
                {
                    if (element is Text)
                        text.Add(element as Text);
                }
            });

            //validate header text exists
            Assert.IsTrue(TextExists(text, ipoImageCommon.ACH_RAW_ADDENDA));

            //validate that the raw addenda data exists
            foreach(var rawPaymentDataLine in rawPaymentData)
            {
                Assert.IsTrue(TextExists(text, rawPaymentDataLine.ToString().Replace(' ', Chars.NonBreakableSpace)));
            }
        }

        private bool TextExists(IList<Text> textList, string value)
        {
            var result = textList.Where(t => string.Equals(t.Content, value));
            return result.FirstOrDefault() != null;
        }
    }
}
