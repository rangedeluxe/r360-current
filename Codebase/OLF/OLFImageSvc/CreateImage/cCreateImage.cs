using System;
using WFS.RecHub.OLFImageAPI;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Application: RecHub Online Image Creation Command-Line Tool.
*       Class: cCreateImage
*    Filename: cCreateImage.cs
*      Author: Joel Caples
*
* Revisions:
* JMC 05/15/2003
*   -Created class
* JMC 11/20/2003
*   -First major enhancement : 
*       1.)Added Strong-Naming to the assembly.
*       2.)Changed the namespace of the class to narrow the scope.
*       3.)Modified Comments to utilize C# Xml format.
*       4.)Application now instantiates the Job Manager
*          object of the Image server object.  Function calls have been 
*          modified slightly.
* CR 7074 JMC 03/22//2004
*   -Modified calls to "CreateImagePDF" to include "Page" parameter (Defaulted to -1).
* CR 50733 JMC 03/04/2012
*   -Removed reference to Cleanup Service API.
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
******************************************************************************/
namespace DMP.ipo.ipoImage {

    /// <author>Joel Caples</author>
    /// <summary>
    /// InegraPAY Online Image Creation Command-Line Tool
    /// </summary>
	class cCreateImage {

        /// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args) {

            cImageJobManager objImageJobManager;

            switch(args.Length) {
                case 1:
                    if (args[0].ToUpper().Trim() == "PROCESSARCHIVES") {
                        objImageJobManager = new cImageJobManager();
                        objImageJobManager.processArchives();
                        objImageJobManager=null;
                    } else { 
                        usage(); 
                    }
                    break;
                default:
                    // PROCESSARCHIVES
                    objImageJobManager = new cImageJobManager();
                    objImageJobManager.processArchives();
                    objImageJobManager=null;
                    break;
            }
		}

        /// <summary>
        /// Outputs proper methods for calling the console app.
        /// The "CREATEFILES" and "PROCESSARCHIVES" directives
        /// have been intentionally left out since these functions
        /// should generally not be run manually unless debugging.
        /// </summary>
        private static void usage() {

            Console.Write( "\nUsage :\n"
                         + "    ipoCreatePDF PROCESSARCHIVES\n"
                         + "    ipoCreatePDF PURGE\n\n"
                         + "Examples :\n"
                         + "    ipoCreatePDF PROCESSARCHIVES\n"
                         + "    ipoCreatePDF PURGE\n");
        }
	}
}
