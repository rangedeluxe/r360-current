﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using iTextSharp.text.pdf;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.ItemProcDAL;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServices.Interfaces;

namespace WFS.RecHub.OLFImageAPI
{
    public class ImageDownload : IDownloadable<ImageRequestDto, ImageResponseDto>
    {
        private readonly ImageDisplayModes _imageDisplayModes;

        public ImageDownload(ImageDisplayModes imageDisplayModes)
        {
            _imageDisplayModes = imageDisplayModes;
        }

        public ImageResponseDto Download(ImageRequestDto request)
        {
            Validate(request);
            // Init initial values. nothing special.
            var imagerequest = new cJobRequestViewSpecified();
            imagerequest.siteKey = request.SiteKey;
            imagerequest.sessionID = request.SessionId;

            byte[] data;
            using (var stream = new MemoryStream())
            {
                var document = new ciTextSharpDoc();
                var writer = PdfWriter.GetInstance(document, stream);

                document.Open();

                // First we need some options. These should probably come from the database?
                var options = new _JobRequest.JobRequestOptions
                {
                    checkImageDisplayMode = _imageDisplayModes.CheckImageDisplayMode,
                    displayRemitterNameInPDF = request.DisplayRemitterName,
                    displayScannedCheck = request.DisplayScannedCheck,
                    documentImageDisplayMode = _imageDisplayModes.DocumentImageDisplayMode,
                    ImageFilterOption = OLFImageFilterOption.All,
                    includeImages = true,
                    returnType = "pdf",
                    displayBatchID = request.DisplayBatchId,
                    olCLientAccountLabel_Override = request.WorkgroupId + " - " + request.WorkgroupName
                };

                var items = ToItemList(request);
                // Done with our IItems, now to process the PDF.
                imagerequest.appendImagesToPdf(ref items, ref document, options, options.displayBatchID, writer);

                // Done!
                stream.Flush();
                
                document.Cleanup(writer, imagerequest.serverOptions);

                data = stream.ToArray();
                stream.Close();
            }

            return new ImageResponseDto {FrontImageData = data, IsSuccessful = true};
        }

        public IItem[] ToItemList(ImageRequestDto request)
        {
            Validate(request);

            var date = request.PicsDate;

            var items = new List<IItem>();
            cImage item;
            var dal = new cItemProcDAL(request.SiteKey);
            DataTable dt;
            var success = request.IsCheck
                ? dal.GetCheck(request.SessionId, request.BankId, request.WorkgroupId, request.BatchId,
                    request.DepositDate, request.TransactionId, request.BatchSequence, out dt)
                : dal.GetDocument(request.SessionId, request.BankId, request.WorkgroupId, request.DepositDate,
                    request.BatchId, request.TransactionId, request.BatchSequence, true, out dt);

            // Convert the DT to the IItem.
            if (request.IsCheck)
                item = new cCheck();
            else
                item = new cDocument();

            if (dt == null || dt.Rows == null || dt.Rows.Count == 0)
                return items.ToArray();
               
            var row = dt.Rows[0];
            item.LoadDataRow(ref row);
            items.Add((IItem) item);
            return items.ToArray();
        }

        private void Validate(ImageRequestDto request)
        {
            if (request == null)
                throw new ArgumentNullException("Request context must be provided.");

            if (string.IsNullOrEmpty(request.SiteKey))
                throw new ArgumentNullException("SiteKey must be configured.");

            if (request.SessionId == Guid.Empty)
                throw new ArgumentException("Session must be configured.");
        }

        public ImageResponseDto DownLoadSingleImage(SingleImageRequestDto request, IItem imageData)
        {
            byte[] frontImage;
            byte[] backImage;
            // 0 means front. Pretty great enum names, huh?
            request.DisplayMode = OLFImageDisplayMode.Undefined;
            var result = GetImage(request, imageData, out frontImage);
            // 1 means back. We're not setting our 'result' here because back images don't always exist.
            request.DisplayMode = OLFImageDisplayMode.BothSides;
            GetImage(request, imageData, out backImage);
            return new ImageResponseDto { FrontImageData = frontImage, BackImageData = backImage, IsSuccessful = result };
        }

        private bool GetImage(SingleImageRequestDto request, IItem image,  out byte[] resultImage)
        {
            var siteOptions = new cSiteOptions(request.SiteKey);
            // Define default image storage location
            var strRootImageLocation = siteOptions.imagePath;
            //Lookup FileGroup location if exists
            if (siteOptions.imageStorageMode == ImageStorageMode.FILEGROUP)
            {
                // Lookup lockbox
                var itemProcDal = new cItemProcDAL(request.SiteKey);
                WorkgroupDTO workgroup = itemProcDal.GetWorkgroup(image.BankID, image.LockboxID);

                if (!string.IsNullOrWhiteSpace(workgroup?.FileGroup))
                {
                    if (Directory.Exists(workgroup.FileGroup))
                    {
                        strRootImageLocation = workgroup.FileGroup;
                    }
                }
            }
            bool bRet = false;
            using (cipoImages ipoImages =
                new cipoImages(request.SiteKey, siteOptions.imageStorageMode, strRootImageLocation))
            {
                var response = ipoImages.GetImage((IItem)image, (int)request.DisplayMode);
                bRet = response.IsSuccessful;
                resultImage = response.ImageBytes;
            }
            return bRet;

        }

    }
}