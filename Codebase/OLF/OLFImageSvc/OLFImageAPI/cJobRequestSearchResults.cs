using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web.UI;
using System.Xml;
using WFS.RecHub.Common;
using System.Text;
using WFS.RecHub.ItemProcDAL;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using CustomImageAPI.Models;
using WFS.RecHub.ApplicationBlocks.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     11/20/2003
*
* Purpose:  IntegraPAY Online Image Search Results.
*
* Modification History
*   01/14/2009 CR 99999 ABC 
*       - Initial release.
*   11/20/2003 JMC 
*       - Created Class.
*   03/17/2004 CR 7017 JMC
*       - Modified "processPDFJob" function to extract  criteria node from Xml 
*         instructions.  
*       - Modified "createSearchResultsXMLDoc function to append search criteria
*         to output Xml.
*       - Removed guts of "outputPdfAsPdfTable" function and ported code to new
*         "cSearchResultsPdf" class
*   03/03/2005 CR 10537 JMC
*       - Modified processPdfJob method to extract Search Parameters from the
*         request Xml.  From these parameters, the OLLockboxID is expected, from
*         which the BankID and LockboxID can be extracted for logging.
*       - processPdfJob now logs "Print View" Pdf's in the SessionActivityLog.
*       - Modified processZipJob method to extract Search Parameters from the
*         request Xml.  From these parameters, the OLLockboxID is expected, from
*         which the BankID and LockboxID can be extracted for logging.
*       - processZipJob now logs "Download Csv" in the SessionActivityLog.
*       - processZipJob now logs "Download Xml" in the SessionActivityLog.
*       - processZipJob now logs "Download Html" in the SessionActivityLog.
*       - processZipJob now logs "Zip download" in the SessionActivityLog.
*       - processZipJob now logs "Download Image Fronts" in the SessionActivityLog.
*       - processZipJob now logs "Download Image Backs" in the SessionActivityLog.
*       - Modified createSearchResultsXMLDoc function to output # of image fronts
*         and backs.
*       - Modified csrxFillJaggedArrays method to use the new cTransaction class
*       -Removed old code pertaining to CR# 7728
*       - Modified csrxBuildTransactionNodes method to output # of image fronts
*         and backs.
*       - Modified csrxBuildCheckNodes method to output # of image fronts
*         and backs.
*       - Modified csrxBuildDocumentNodes method to output # of image fronts
*         and backs.
*       - Changed return type of outputXmlDoc function to bool.
*       - Change return type of outputText function to bool.
*       - Change return type of outputPdfAsPdfTable function to bool.
*   05/23/2008 CR 23581 JCS
*       - Replace page parameter with checkImageDisplayMode, documentImageDisplayMode in createSearchResultsXMLDoc
*       - Replace page parameter with checkImageDisplayMode, documentImageDisplayMode in csrxBuildTransactionNodes
*       - processPdfJob now passes parameters for checkImageDisplayMode and documentImageDisplayMode
*       - processZipJob now passes parameters for checkImageDisplayMode and documentImageDisplayMode
*       - csrxBuildTransactionNodes now takes parameters checkImageDisplayMode, documentImageDisplayMode.
*       - Pass checkImageDisplay mode to csrxBuildCheckNodes.
*       - Pass documentImageDisplay mode to csrxBuildDocumentNodes.
* CR 45627 JMC 07/12/2011
*    -Added a Using{} clause when accessing ipoImages to ensure Dispose() is 
*     called.
*  CR 47058 WJS 10/6/2011
*   - Update to centralize point for XML Resource
* CR 48475 WJS 11/24/2011
*	- Move using ipoImages call to BuildTransactionModes so once instance createed fro checks and documents
*	- Passed in output image path for Hyland to allow hyland to store images
*	- Create shared logic for pics/hyland for file existence
* CR 50734 JMC 03/02/2012
*   -Elminated unused outputPdfAsHtml functionality.
*   -Removed old ItemProc code.
*   -Added missing attribute handling to csrxBuildTransactionNodes() method.
* CR 50642 JNE 03/20/2012
*   -Added use of SerialNumber when calling GetTransactionChecks in 
*   -In csrxBuildDocumentNodes moved bFileExist outside 2nd if statement.  If the image
*    already exists we still want it associated in the html file.
* CR 51697 JNE 04/09/2012
*   -In outputHtml and outputText - Created PathDocument from Document for speedier transformations.
* CR 52276 JNE 06/20/2012
*   -Moved processing from app server to image server (this class).
*   -createSearchResultsXMLDoc now includes doing the actually search, output datatable results and create supporting xml.
*   -methods used to create html, xml and csv have been adjusted to use the datatable and supporting xml.
*   -Added css branding for html
* CR 54169 JNE 08/08/2012
*   -createSearchResultsXMLDoc - Added DisplayBatchID, BatchNumber, BatchIDFrom,BatchIDTo, BatchNumberFrom, BatchNumberTo
*   -BuildCriteriaText - BatchIDFrom,BatchIDTo, BatchNumberFrom, BatchNumberTo
* CR 53949 JNE 09/19/2012
*   -OutputHTML - Added currency formating and special labels for BatchNumber and Check Amount.
* CR 52959 JNE 10/01/2012
*   -createSearchResultsXMLDoc - Added FileGroup code when determining root image path. 
* WI  85208 JMC 01/10/2013
*   -Added logic to account for column names that are different than what was expected
*    from the search.  This was done to account for an issue where more than one field was
*    selected to be returned that pointed to the same DE Table and Field name but with different
*    display names.
* WI 70358 JMC 01/17/2013
*   -Corrected an issue where "Serial" was not being properly supressed from the
*    Search Results output.
* WI 96037 CRG 04/10/2013
*   -Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 71734 CRG 04/10/2013
*   -Replace XCeed.net with native .Net zip compression
* WI 116654 TWE 10/10/2013
*   -Rename LockboxID to ClientAccountID
* WI 116943 TWE 10/11/2013
*   -Rename Lockbox to ClientAccount
* WI 117165 TWE 10/14/2013
*   -Correct the output zip file name on the ZipFile function
* WI 121801 MLH 11/07/2013
*   -Fixed outputText so the CSV line values are concatenated in the same order as the Search definition
* WI 123291 TWE 11/22/2013
*   -(FP) Add ini setting to control use of Curly brackets in file name (csv)
* WI 122110 MLH 11/22/2013
*   -Fixed DisplayFields not populating downloaded files from Account Search.  Fixed several other bugs where
*       DataRow[colName] was checking for null threw exception
*       If a fieldName isn't available, write out empty value, otherwise wrong data was getting into the wrong
*       columns.
*       Implemented outputMessage & outputError eventHandlers on pdf class
*       Broke out some lengthy htmlWriter code and added regions
* WI 121672 MLH 11/26/2013
*   -Added OLClientAccount DisplayLabel formatted with DDA  See WI 121666 Attachment
*    for v2.0 limitations.
*   -Removed USE_ITEMPROC code
* WI 127110 EAS 01/15/2014
*   -Remove Table Name from appending to Column Name 
* WI 128752 TWE 02/05/2014
*   -Add Table Name to Field name to create Column Name
* WI 130157 TWE 02/19/2014
*   -imageExist flag was set incorrectly
* WI 134804 TWE 04/02/2014
*   -Add DDA to Account Search, add to PDF View
* WI 71542 BLR 05/07/2014
*   -Template directory built from the request's sitekey 
* WI 143274 SAS 05/21/2014
*   -Changes done to change the datatype of BatchID from int to long
* WI 148949 DJW 06/20/2014
*   -Changes done to add SessionID for RAAM
* WI 157074 JMC 08/20/2014
*   -Corrected criteria display labels
* WI 155445 SAS 09/12/2014
*   -Modified to add PaymentType and PaymentSource constant  
* WI 185936 and 185937 JSF 01/26/2015
*   -Batch ID column using incorrect database column
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI
{

    /// <summary>
    /// ipo Image Service "Search Results" job object class.
    /// </summary>
    public class cJobRequestSearchResults : _TransactionalJobRequest, _IJobRequest
    {

        /// <summary>
        /// Class Constructor
        /// </summary>
        public cJobRequestSearchResults()
        {
        }

        /// <summary>
        /// Public interface for the class.
        ///   jobRequestOptions 
        ///   ------------------------------------------------------------------------
        ///       displayScannedCheck = "True, False" 
        ///           returnType = "ZIP", "PDF"
        ///           includeImages = "True, False"
        ///       
        ///           outputFormat = "TEXT", "HTML", "PDF", "XML"
        ///
        ///               "TEXT" options  -
        ///                   fileType = "DELIMITED, FIXED" - NOT IMPLEMENTED
        ///                   delimiter = Any single-character string 
        ///                       ex:(",", "|", ";"). 
        ///                   fileExtension = Any valid file extension NO DOT!
        ///                       ex:("txt", "csv", "dat", "doc").
        ///
        ///               "HTML" options -
        ///                   layout = "printView" 
        ///                   includeImageRefs = "true" - NOT IMPLEMENTED
        ///
        ///               "PDF" options
        ///                   layout = "pdfTable", "html"
        ///                   includeImages = "True", "false" - NOT IMPLEMENTED
        ///
        /// </summary>
        public void processJob()
        {

            jobRequestEventArgs ea = new jobRequestEventArgs();
            changeStatus(OLFJobStatus.Processing, ea);

            JobRequestOptions sctJobRequestOptions;

            try
            {
                var jobRequestNode = nodeJobRequest();
                var getImageDisplayModes = new Lazy<ImageDisplayModes>(() =>
                {
                    int bankId;
                    int workgroupId;
                    int.TryParse(jobRequestNode.SelectSingleNode("BankID")?.InnerText ?? "", out bankId);
                    int.TryParse(jobRequestNode.SelectSingleNode("ClientAccountID")?.InnerText ?? "", out workgroupId);
                    return OlfServicesDal.GetImageDisplayModes(bankId, workgroupId);
                });
                getJobRequestOptions(jobRequestNode, () => getImageDisplayModes.Value, out sctJobRequestOptions);

                switch (sctJobRequestOptions.returnType)
                {
                    case OUTPUT_FORMAT_PDF:
                        {
                            processPdfJob(sctJobRequestOptions, out ea.fileSize);
                            break;
                        }
                    case OUTPUT_FORMAT_ZIP:
                        {
                            ProcessZipJob(sctJobRequestOptions, out ea.fileSize);
                            break;
                        }
                    default:
                        {
                            throw (new Exception("Invalid return type specified : " + sctJobRequestOptions.returnType));
                        }
                }
                changeStatus(OLFJobStatus.Completed, ea);
            }
            catch (Exception e)
            {
                ea.Message = e.Message;
                ea.src = e.Source;
                ea.fileSize = 0;
                changeStatus(OLFJobStatus.Error, ea);
            }
            finally
            {
                try
                {
                    if (Directory.Exists(this.outputRootPath))
                    {
                        System.IO.Directory.Delete(this.outputRootPath, true);
                    }
                }
                catch (Exception e)
                {
                    eventLog.logWarning("Unable to delete temporary output directory : "
                                      + this.outputRootPath + "; " + e.Message
                                       , this.GetType().Name
                                       , MessageImportance.Essential);
                }
            }
        }

        /// <summary>
        /// Creates a Pdf that contains all data for the given Search request.
        /// </summary>
        /// <param name="sctJobRequestOptions">Job request options.</param>
        /// <param name="fileSize">Returns size of the completed file.</param>
        private void processPdfJob(JobRequestOptions sctJobRequestOptions, out long fileSize)
        {

            XmlNode nodeJobRequestOptions;
            XmlNode nodeSearchParms;
            XmlDocument docXml;
            DataTable dt = null;
            string strFileName;
            int intImageFrontCount = 0;
            int intImageBackCount = 0;

            fileSize = 0;

            nodeJobRequestOptions = nodeJobRequest().SelectSingleNode(XML_ELE_JOB_REQ_OPTIONS);
            nodeSearchParms = nodeJobRequest();

            createSearchResultsXMLDoc(ref nodeSearchParms
                                    , sctJobRequestOptions.displayScannedCheck
                                    , false
                                    , sctJobRequestOptions.checkImageDisplayMode
                                    , sctJobRequestOptions.documentImageDisplayMode
                                    , out docXml
                                    , out dt
                                    , out intImageFrontCount
                                    , out intImageBackCount
                                    , sctJobRequestOptions.olCLientAccountLabel_Override);

            strFileName = this.serverOptions.onlineArchivePath
                          + "\\"
                          + this.jobID
                          + "."
                          + FILE_EXT_PDF;

            outputPdfAsPdfTable(ref docXml, ref dt, strFileName);
            LogActivity(ActivityCodes.acSearchResultsPdfView, this.sessionID, this.jobID, 1);
            fileSize = ipoLib.LOF(strFileName);

        }

        /// <summary>
        /// Creates a zip file containing all images as well as other files
        /// relating to given Search results.
        /// </summary>
        /// <param name="sctJobRequestOptions">Job request options.</param>
        /// <param name="fileSize">Returns size of the completed file.</param>
        private void ProcessZipJob(JobRequestOptions sctJobRequestOptions,
                                   out long fileSize)
        {
            XmlNode nodeJobRequestOptions;
            XmlNode nodeJobOutputs;
            XmlNode nodeSearchParms;
            XmlDocument docXml;
            XmlNode nodeOutputOptions;

            DataTable dt = null;

            string strFileExtension = FILE_EXT_CSV;
            string strFileName;

            fileSize = 0;
            int intImageFrontCount = 0;
            int intImageBackCount = 0;

            nodeJobRequestOptions = nodeJobRequest().SelectSingleNode(XML_ELE_JOB_REQ_OPTIONS);
            nodeSearchParms = nodeJobRequest();
            nodeJobOutputs = nodeJobRequestOptions.SelectSingleNode(XML_ELE_OUTPUTS);

            // build directory
            buildOutputPath();

            createSearchResultsXMLDoc(ref nodeSearchParms
                                    , sctJobRequestOptions.displayScannedCheck
                                    , sctJobRequestOptions.includeImages
                                    , sctJobRequestOptions.checkImageDisplayMode
                                    , sctJobRequestOptions.documentImageDisplayMode
                                    , out docXml
                                    , out dt
                                    , out intImageFrontCount
                                    , out intImageBackCount
                                    , sctJobRequestOptions.olCLientAccountLabel_Override);
            // Must have output chosen and search results
            if (nodeJobOutputs.ChildNodes.Count > 0 && dt != null && dt.Rows.Count > 0)
            {
                int siteBankId;
                int siteWorkgroupId;
                var rowsCount = dt.Rows.Count;
                try
                {
                    siteBankId = int.Parse(nodeSearchParms.SelectSingleNode("BankID")?.InnerText ?? "");
                    siteWorkgroupId = int.Parse(nodeSearchParms.SelectSingleNode("ClientAccountID")?.InnerText ?? "");
                }
                catch
                {
                    siteBankId = -1;
                    siteWorkgroupId = -1;
                }

                for (int i = 0; i < nodeJobOutputs.ChildNodes.Count; ++i)
                {
                    switch (nodeJobOutputs.ChildNodes[i].Attributes[XML_ATTR_OUTPUT_FORMAT].Value)
                    {
                        case OUTPUT_FORMAT_TEXT:

                            //*************************************
                            //Determine file extension
                            //*************************************
                            try
                            {
                                nodeOutputOptions = nodeJobOutputs.ChildNodes[i].SelectSingleNode(XML_ELE_TEXT_OPTIONS);

                                try { strFileExtension = nodeOutputOptions.Attributes[XML_ATTR_TEXT_FILE_EXT].Value; }
                                catch { strFileExtension = FILE_EXT_CSV; }
                            }
                            catch
                            {
                                eventLog.logWarning("Text options node ["
                                                    + XML_ELE_TEXT_OPTIONS
                                                    + "] not found on input doc as expected."
                                                    , this.GetType().Name
                                                    , MessageImportance.Essential);
                            }
                            //*************************************

                            if (outputText(ref docXml, ref dt, strFileExtension))
                            {
                                LogActivity(ActivityCodes.acSearchResultsDownloadCsv, this.sessionID, this.jobID, rowsCount
                                          , siteBankId, siteWorkgroupId);
                            }
                            break;

                        case OUTPUT_FORMAT_XML:
                            if (outputXmlDoc(ref docXml, ref dt))
                            {
                                LogActivity(ActivityCodes.acSearchResultsDownloadXml, this.sessionID, this.jobID, rowsCount
                                          , siteBankId, siteWorkgroupId);
                            }
                            break;

                        case OUTPUT_FORMAT_HTML:
                            if (outputHtml(ref docXml, ref dt))
                            {
                                LogActivity(ActivityCodes.acSearchResultsDownloadHtml, this.sessionID, this.jobID, rowsCount
                                          , siteBankId, siteWorkgroupId);
                            }
                            break;


                    }   //switch
                }   //for

                strFileName = getAvailableFileName(ipoLib.cleanPath(serverOptions.onlineArchivePath) + this.jobID, FILE_EXT_ZIP);
                ZipFile.CreateFromDirectory(this.outputRootPath, strFileName);

                fileSize = ipoLib.LOF(strFileName);

                LogActivity(ActivityCodes.acSearchResultsZipDownload, this.sessionID, this.jobID
                          , intImageFrontCount + intImageBackCount
                          , siteBankId, siteWorkgroupId);

                LogActivity(ActivityCodes.acDownloadImageFront, this.sessionID, this.jobID
                          , intImageFrontCount
                          , siteBankId, siteWorkgroupId);

                LogActivity(ActivityCodes.acDownloadImageBack, this.sessionID, this.jobID
                          , intImageBackCount
                          , siteBankId, siteWorkgroupId);
            }   //if
        }

        /// <summary>
        /// Creates an datatable containing Search results and supporting xml document.  
        /// </summary>
        /// <param name="nodeSearchParms">
        /// Node containing search criteria.
        /// </param>
        /// <param name="displayScannedCheck">
        /// Boolean indicating whether documents of type 'SC' should be supressed.
        /// </param>
        /// <param name="includeImages">
        /// Boolean indicating whether images should be extracted when creating the Xml document.
        /// </param>
        /// <param name="checkImageDisplayMode"></param>
        /// <param name="documentImageDisplayMode"></param>
        /// <param name="docXml">Support Xml Document</param>
        /// <param name="dtSearch">Search Results Datatable</param>
        /// <param name="ImageFrontCount"></param>
        /// <param name="ImageBackCount"></param>
        protected void createSearchResultsXMLDoc(ref XmlNode nodeSearchParms
                                             , bool displayScannedCheck
                                             , bool includeImages
                                             , OLFImageDisplayMode checkImageDisplayMode
                                             , OLFImageDisplayMode documentImageDisplayMode
                                             , out XmlDocument docXml
                                             , out DataTable dtSearch
                                             , out int ImageFrontCount
                                             , out int ImageBackCount
                                             , string olClientAccountDisplayOverride)
        {
            #region Variables
            DataTable dt;
            DataTable dtChecks;
            DataTable dtDocuments;

            cOLLockbox objOLLockbox;

            XmlDocument docSearchTotals;
            XmlDocument docReqXml;

            XmlNodeList nlWhereClause;

            XmlNode nodeSearchTemp;
            XmlNode nodeRoot;
            XmlNode nodeSelectFields = null;
            XmlNode node;
            XmlNode subNode;
            XmlNode nodeTemp;
            XmlNode nodeSearchCriteria;
            XmlNode nodeDateFrom;
            XmlNode nodeDateTo;

            int intOLWorkgroupID;

            Decimal decCheckTotal = 0;
            Decimal decTemp = 0;
            Decimal decAmountFrom = 0;
            Decimal decAmountTo = 0;

            DateTime dtdrDepositDate = DateTime.MinValue;
            DateTime dtTemp = DateTime.MinValue;

            string strCriteriaText = string.Empty;
            string strBatchIDFrom = string.Empty;
            string strBatchIDTo = string.Empty;
            string strBatchNumberFrom = string.Empty;
            string strBatchNumberTo = string.Empty;
            string strSerial = string.Empty;
            string strLockboxLongName = string.Empty;
            string strfieldName = string.Empty;
            string surrogateImageFile;

            int intBankID = -1;
            int intLockboxID = -1;
            int intTotalRecords = 0;
            int intDocumentCount = 0;
            int intCheckCount = 0;
            int intTemp = 0;
            long longTemp = 0;

            int intdrBankID = 0;
            int intdrLockboxID = 0;
            long longdrBatchID = 0;
            int intdrBatchNumber = 0;
            int intdrTransactionID = 0;
            int intCheckStartPage = 0;
            int intCheckEndPage = 0;
            int intDocumentStartPage = 0;
            int intDocumentEndPage = 0;

            bool bolCOTSOnly = false;
            bool bolTemp = false;
            bool bolMarkSense = false;
            bool bolRetVal = false;
            bool bolDisplayBatchID = false;

            dtSearch = null;
            ImageFrontCount = 0;
            ImageBackCount = 0;
            #endregion

            //Create output document
            docXml = ipoXmlLib.GetXMLDoc("searchXml");
            docXml.PreserveWhitespace = true;

            //Create Document for Search Input
            docReqXml = new XmlDocument();
            nodeRoot = docReqXml.CreateNode(XmlNodeType.Element, "Root", docReqXml.NamespaceURI);
            docReqXml.AppendChild(nodeRoot);
            ipoXmlLib.addAttribute(docReqXml.DocumentElement, "ReqID", Guid.NewGuid().ToString());
            ipoXmlLib.addAttribute(docReqXml.DocumentElement, "ReqDateTime", DateTime.Now.ToString());


            #region Build up the datatble to pass to the 'Search' Stored Procedure
            foreach (XmlNode nodeChildTemp in nodeSearchParms.ChildNodes)
            {
                nodeSearchTemp = docReqXml.ImportNode(nodeChildTemp, true);
                docReqXml.DocumentElement.AppendChild(nodeSearchTemp);
            }

            //BankID
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("BankID");
            if (nodeTemp != null && int.TryParse(nodeTemp.InnerText, out intTemp))
            {
                intBankID = intTemp;
            }

            //CAID
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("ClientAccountID");
            if (nodeTemp != null && int.TryParse(nodeTemp.InnerText, out intTemp))
            {
                intLockboxID = intTemp;
            }

            // olClientAccountDisplayOverride is from the Job Request XML, if it's not there,
            // then populate the ClientAccount the old way...
            if (String.IsNullOrEmpty(olClientAccountDisplayOverride))
            {
                olClientAccountDisplayOverride += docReqXml.DocumentElement.SelectSingleNode("WorkgroupSelectionLabel").InnerText;
            }

            //BatchIDFrom
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("BatchIDFrom");
            if (nodeTemp != null && long.TryParse(nodeTemp.InnerText, out longTemp))
            {
                strBatchIDFrom = longTemp.ToString();
            }

            //BatchIDTo
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("BatchIDTo");
            if (nodeTemp != null && long.TryParse(nodeTemp.InnerText, out longTemp))
            {
                strBatchIDTo = longTemp.ToString();
            }

            //BatchNumberFrom
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("BatchNumberFrom");
            if (nodeTemp != null && int.TryParse(nodeTemp.InnerText, out intTemp))
            {
                strBatchNumberFrom = intTemp.ToString();
            }

            //BatchNumberTo
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("BatchNumberTo");
            if (nodeTemp != null && int.TryParse(nodeTemp.InnerText, out intTemp))
            {
                strBatchNumberTo = intTemp.ToString();
            }
            //Serial
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("Serial");
            if (nodeTemp != null)
            {
                strSerial = nodeTemp.InnerText;
            }

            //Amount From
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("AmountFrom");
            if (nodeTemp != null && Decimal.TryParse(nodeTemp.InnerText, out decTemp))
            {
                decAmountFrom = decTemp;
            }

            //Amount To
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("AmountTo");
            if (nodeTemp != null && Decimal.TryParse(nodeTemp.InnerText, out decTemp))
            {
                decAmountTo = decTemp;
            }

            //Determine COTS
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("COTSOnly");
            if (nodeTemp == null)
            {
                bolCOTSOnly = false;
            }
            else
            {
                if (bool.TryParse(nodeTemp.InnerText, out bolTemp))
                {
                    bolCOTSOnly = bolTemp;
                }
                else
                {
                    bolCOTSOnly = false;
                }
            }

            //Mark Sense
            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("MarkSenseOnly");
            if (nodeTemp == null)
            {
                bolMarkSense = false;
            }
            else
            {
                if (bool.TryParse(nodeTemp.InnerText, out bolTemp))
                {
                    bolMarkSense = bolTemp;
                }
                else
                {
                    bolMarkSense = false;
                }
            }

            nodeDateFrom = docReqXml.DocumentElement.SelectSingleNode("DateFrom");
            nodeDateTo = docReqXml.DocumentElement.SelectSingleNode("DateTo");

            // Add the SELECT fields & the WHERE clause
            nodeSelectFields = docReqXml.DocumentElement.SelectSingleNode("SelectFields");
            nlWhereClause = docReqXml.DocumentElement.SelectNodes("WhereClause/field");

            nodeTemp = docReqXml.DocumentElement.SelectSingleNode("displaybatchid");
            if (nodeTemp == null)
            {
                bolDisplayBatchID = false;
            }
            else
            {
                if (bool.TryParse(nodeTemp.InnerText, out bolTemp))
                {
                    bolDisplayBatchID = bolTemp;
                }
                else
                {
                    bolDisplayBatchID = false;
                }
            }

            //Append column information to docXml
            node = ipoXmlLib.addElement((docXml.DocumentElement), "Recordset", string.Empty);
            ipoXmlLib.addAttribute(node, "Name", "Columns");

            var defaults = new[]
            {
                new { FieldName="Batch.Deposit_Date", Title="Deposit Date", Type="11" },
                new { FieldName="Batch.BatchNumber", Title="Batch", Type="6" },
                new { FieldName="Batch.PaymentSource", Title="Payment Source", Type="1" },
                new { FieldName="Batch.PaymentType", Title="Payment Type", Type="1" },
                new { FieldName="Transactions.TxnSequence", Title="Transaction", Type="6" },
                new { FieldName="Batch.SourceBatchID", Title="Batch ID", Type="6" },
                new { FieldName="Batch.BatchNumber", Title="Batch Number", Type="6" },
            }.ToList();


            if (nodeSelectFields.HasChildNodes)
            {
                var nodestodelete = new List<XmlNode>();
                foreach (XmlNode childNode in nodeSelectFields.ChildNodes)
                {
                    // Don't include the standard defaults.
                    var fieldname = childNode.SelectSingleNode("@fieldname").InnerText;
                    var type = childNode.SelectSingleNode("@datatype").InnerText;
                    var title = childNode.SelectSingleNode("@reporttitle").InnerText;

                    var del = defaults.Any(x => x.FieldName == fieldname && x.Title == title && x.Type == type);
                    if (del)
                    {
                        nodestodelete.Add(childNode);
                    }

                    if (!(bolCOTSOnly && childNode.SelectSingleNode("@tablename").InnerText.ToLower().StartsWith("checks")))
                    {
                        subNode = ipoXmlLib.addElement(node, "Record", string.Empty);
                        ipoXmlLib.addAttribute(subNode, "TableName", childNode.SelectSingleNode("@tablename").InnerText);
                        ipoXmlLib.addAttribute(subNode, "fieldName", childNode.SelectSingleNode("@fieldname").InnerText);
                        ipoXmlLib.addAttribute(subNode, "dataTypeEnum", childNode.SelectSingleNode("@datatype").InnerText);
                        ipoXmlLib.addAttribute(subNode, "displayName", childNode.SelectSingleNode("@reporttitle").InnerText);
                    }
                }

                foreach (var n in nodestodelete)
                    nodeSelectFields.RemoveChild(n);
            }
            else
            {
                if (!bolCOTSOnly)
                {

                    subNode = ipoXmlLib.addElement(node, "Record", string.Empty);
                    ipoXmlLib.addAttribute(subNode, "TableName", "");
                    ipoXmlLib.addAttribute(subNode, "fieldName", XML_ATTR_PAYMENTBATCHSEQUENCE);
                    ipoXmlLib.addAttribute(subNode, "dataTypeEnum", ((int)DMPFieldTypes.FLOAT).ToString());
                    ipoXmlLib.addAttribute(subNode, "displayName", "Payment Batch Sequence");
                    ipoXmlLib.addAttribute(subNode, "SortOrder", "1");

                    subNode = ipoXmlLib.addElement(node, "Record", string.Empty);
                    ipoXmlLib.addAttribute(subNode, "TableName", "");
                    ipoXmlLib.addAttribute(subNode, "fieldName", XML_ATTR_AMOUNT);
                    ipoXmlLib.addAttribute(subNode, "dataTypeEnum", ((int)DMPFieldTypes.CURRENCY).ToString());
                    ipoXmlLib.addAttribute(subNode, "displayName", "Check Amount");
                    ipoXmlLib.addAttribute(subNode, "SortOrder", "2");

                    subNode = ipoXmlLib.addElement(node, "Record", string.Empty);
                    ipoXmlLib.addAttribute(subNode, "TableName", "");
                    ipoXmlLib.addAttribute(subNode, "fieldName", XML_ATTR_RT);
                    ipoXmlLib.addAttribute(subNode, "dataTypeEnum", ((int)DMPFieldTypes.STRING).ToString());
                    ipoXmlLib.addAttribute(subNode, "displayName", "R/T");
                    ipoXmlLib.addAttribute(subNode, "SortOrder", "3");

                    subNode = ipoXmlLib.addElement(node, "Record", string.Empty);
                    ipoXmlLib.addAttribute(subNode, "TableName", "");
                    ipoXmlLib.addAttribute(subNode, "fieldName", XML_ATTR_ACCOUNT_NUMBER);
                    ipoXmlLib.addAttribute(subNode, "dataTypeEnum", ((int)DMPFieldTypes.STRING).ToString());
                    ipoXmlLib.addAttribute(subNode, "displayName", "Account Number");
                    ipoXmlLib.addAttribute(subNode, "SortOrder", "4");

                    subNode = ipoXmlLib.addElement(node, "Record", string.Empty);
                    ipoXmlLib.addAttribute(subNode, "TableName", "");
                    ipoXmlLib.addAttribute(subNode, "fieldName", XML_ATTR_SERIAL_NUMBER);
                    ipoXmlLib.addAttribute(subNode, "dataTypeEnum", ((int)DMPFieldTypes.STRING).ToString());
                    ipoXmlLib.addAttribute(subNode, "displayName", "Check Number");
                    ipoXmlLib.addAttribute(subNode, "SortOrder", "5");

                    subNode = ipoXmlLib.addElement(node, "Record", string.Empty);
                    ipoXmlLib.addAttribute(subNode, "TableName", "");
                    ipoXmlLib.addAttribute(subNode, "fieldName", XML_ATTR_DDA);
                    ipoXmlLib.addAttribute(subNode, "dataTypeEnum", ((int)DMPFieldTypes.STRING).ToString());
                    ipoXmlLib.addAttribute(subNode, "displayName", "DDA");
                    ipoXmlLib.addAttribute(subNode, "SortOrder", "6");

                    subNode = ipoXmlLib.addElement(node, "Record", string.Empty);
                    ipoXmlLib.addAttribute(subNode, "TableName", "");
                    ipoXmlLib.addAttribute(subNode, "fieldName", XML_ATTR_PAYER);
                    ipoXmlLib.addAttribute(subNode, "dataTypeEnum", ((int)DMPFieldTypes.STRING).ToString());
                    ipoXmlLib.addAttribute(subNode, "displayName", "Payer");
                    ipoXmlLib.addAttribute(subNode, "SortOrder", "7");
                }


                // Build up advanced search parameters
                foreach (XmlNode childNode in nlWhereClause)
                {
                    if (childNode.Attributes.GetNamedItem("tablename") != null)
                    {
                        if (!(bolCOTSOnly && childNode.Attributes.GetNamedItem("tablename").InnerText.ToLower().StartsWith("checks")))
                        {
                            strfieldName = childNode.Attributes.GetNamedItem("fieldname") != null ? childNode.Attributes.GetNamedItem("fieldname").InnerText : string.Empty;
                            if (!(strfieldName.Contains(XML_ATTR_SERIAL_NUMBER)
                                || strfieldName.Contains(XML_ATTR_ACCOUNT_NUMBER)
                                || strfieldName.Contains(XML_ATTR_RT)
                                || strfieldName.Contains(XML_ATTR_DDA)
                                || strfieldName.Contains(XML_ATTR_AMOUNT)
                                || strfieldName == string.Empty))
                            {

                                subNode = ipoXmlLib.addElement(node, "Record", childNode.InnerText);
                                ipoXmlLib.addAttribute(subNode, "TableName", childNode.Attributes.GetNamedItem("tablename").InnerText);
                                ipoXmlLib.addAttribute(subNode, "fieldName", strfieldName);

                                if (childNode.Attributes.GetNamedItem("datatype") != null)
                                {
                                    ipoXmlLib.addAttribute(subNode, "dataTypeEnum", childNode.Attributes.GetNamedItem("datatype").InnerText);
                                }
                                if (childNode.Attributes.GetNamedItem("reporttitle") != null)
                                {
                                    ipoXmlLib.addAttribute(subNode, "displayName", childNode.Attributes.GetNamedItem("reporttitle").InnerText);
                                }
                                if (childNode.Attributes.GetNamedItem("operator") != null)
                                {
                                    ipoXmlLib.addAttribute(subNode, "Operator", childNode.Attributes.GetNamedItem("operator").InnerText);
                                }
                                if (childNode.Attributes.GetNamedItem("value") != null)
                                {
                                    ipoXmlLib.addAttribute(subNode, "Value", childNode.Attributes.GetNamedItem("value").InnerText);
                                }
                            }
                        }   // !(bolCOTSOnly
                    }       // Check tablename not null
                }           //  Advanced search parameters 
            }               // Build up SP xml for Search

            var transformer = new AdvancedSearchRequestXmlTransformer();
            var request = transformer.ToDTO(docReqXml);
            var requestTrans = new AdvancedSearchRequestDataTableTransformer();
            // Convert to datatable for the proc;
            var tables = requestTrans.ToDataTable(request);
            #endregion
            
            //Run Search
            if (SearchDAL.LockboxSearch(tables, out docSearchTotals, out dt))
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    dtSearch = dt;

                    if (docSearchTotals != null)
                    {
                        nodeTemp = docSearchTotals.SelectSingleNode("/Page/RecordSet[@Name='Results']/@TotalRecords");
                        if (nodeTemp != null && int.TryParse(nodeTemp.InnerText, out intTemp))
                        {
                            intTotalRecords = intTemp;
                        }
                        nodeTemp = docSearchTotals.SelectSingleNode("/Page/RecordSet[@Name='Results']/@DocumentCount");
                        if (nodeTemp != null && int.TryParse(nodeTemp.InnerText, out intTemp))
                        {
                            intDocumentCount = intTemp;
                        }
                        nodeTemp = docSearchTotals.SelectSingleNode("/Page/RecordSet[@Name='Results']/@CheckCount");
                        if (nodeTemp != null && int.TryParse(nodeTemp.InnerText, out intTemp))
                        {
                            intCheckCount = intTemp;
                        }
                        nodeTemp = docSearchTotals.SelectSingleNode("/Page/RecordSet[@Name='Results']/@CheckTotal");
                        if (nodeTemp != null && decimal.TryParse(nodeTemp.InnerText, out decTemp))
                        {
                            decCheckTotal = decTemp;
                        }
                    }

                    AppendColumnNames(docXml, docSearchTotals);
                }
            }


            //Get Check/Document Images
            if (dtSearch != null)
            {
                if (includeImages)
                {
                    // set pages
                    establishPageRanges(checkImageDisplayMode, out intCheckStartPage, out intCheckEndPage);
                    establishPageRanges(documentImageDisplayMode, out intDocumentStartPage, out intDocumentEndPage);


                    dtSearch.Columns.Add("CheckDataDictionary", typeof(Dictionary<string, Dictionary<string, string>>));
                    dtSearch.Columns.Add("DocumentDataDictionary", typeof(Dictionary<string, Dictionary<string, string>>));

                    foreach (DataRow dr in dtSearch.Rows)
                    {
                        if (int.TryParse(dr["BankID"].ToString(), out intTemp))
                        {
                            intdrBankID = intTemp;
                        }
                        if (int.TryParse(dr["ClientAccountID"].ToString(), out intTemp))
                        {
                            intdrLockboxID = intTemp;
                        }
                        if (DateTime.TryParse(dr["Deposit_Date"].ToString(), out dtTemp))
                        {
                            dtdrDepositDate = dtTemp;
                        }
                        if (long.TryParse(dr["BatchID"].ToString(), out longTemp))
                        {
                            longdrBatchID = longTemp;
                        }
                        if (int.TryParse(dr["BatchNumber"].ToString(), out intTemp))
                        {
                            intdrBatchNumber = intTemp;
                        }
                        if (int.TryParse(dr["TransactionID"].ToString(), out intTemp))
                        {
                            intdrTransactionID = intTemp;
                        }
                        // Define default image storage location
                        string strRootImageLocation = siteOptions.imagePath;
                        //Lookup FileGroup location if exists
                        if (siteOptions.imageStorageMode == ImageStorageMode.FILEGROUP)
                        {
                            // Lookup workgroup
                            WorkgroupDTO workgroup = ItemProcDAL.GetWorkgroup(intBankID, intLockboxID);
                            if (workgroup!=null)
                            {
                                if (!string.IsNullOrWhiteSpace(workgroup.FileGroup))
                                {
                                    if (Directory.Exists(workgroup.FileGroup))
                                    {
                                        strRootImageLocation = workgroup.FileGroup;
                                    }
                                    else
                                    {
                                        // Invalid directory log error.
                                        eventLog.logError(new eImageDirectoryNotFoundException(workgroup.FileGroup));
                                    }
                                }
                            }
                            if (dt != null)
                            {
                                dt.Dispose();
                            }
                        }

                        // Image location may change per lockbox due to FileGroups
                        using (cipoImages ipoImages = new cipoImages(this.siteKey, siteOptions.imageStorageMode, strRootImageLocation))
                        {

                            //Load transaction object
                            cTransaction transaction = new cTransaction(intdrBankID, intdrLockboxID, dtdrDepositDate, longdrBatchID, intdrBatchNumber, intdrTransactionID, 0);

                            // Get Checks for given transaction
                            bolRetVal = ItemProcDAL.GetTransactionChecks(base.sessionID, intdrBankID,
                                                                        intdrLockboxID,
                                                                        dtdrDepositDate,
                                                                        longdrBatchID,
                                                                        intdrTransactionID,
                                                                        out dtChecks);


                            Dictionary<string, Dictionary<string, string>> dctChecks = new Dictionary<string, Dictionary<string, string>>();

                            if (bolRetVal && dtChecks != null && dtChecks.Rows.Count > 0)
                            {
                                int intCheckCnt = 0;
                                string strCheckKey = string.Empty;
                                string strCheckFilePath = string.Empty;
                                string strNewFileName = string.Empty;
                                // Add check information to output datatable
                                foreach (DataRow drChecks in dtChecks.Rows)
                                {
                                    if (drChecks["BatchSequence"].ToString() == dr["BatchSequence"].ToString())
                                    {
                                        intCheckCnt += 1;

                                        //Steps required to use ipoimage.GetRelativeImagePath -- should be more generic.
                                        DataRow drTempChecks = drChecks;
                                        cCheck oCheck = new cCheck();
                                        oCheck.LoadDataRow(ref drTempChecks);

                                        for (int intCurrentPage = intCheckStartPage; intCurrentPage < intCheckEndPage + 1; ++intCurrentPage)
                                        {
                                            //reset variables
                                            strCheckFilePath = string.Empty;
                                            strNewFileName = string.Empty;
                                            Dictionary<string, string> dctCheckFields = new Dictionary<string, string>();
                                            // add data
                                            dctCheckFields.Add("SerialNumber", drChecks["Serial"].ToString());
                                            dctCheckFields.Add("RT", drChecks["RT"].ToString());
                                            dctCheckFields.Add("BatchSequence", drChecks["BatchSequence"].ToString());
                                            dctCheckFields.Add("Amount", drChecks["Amount"].ToString());
                                            dctCheckFields.Add("AccountNumber", drChecks["Account"].ToString());
                                            dctCheckFields.Add("DDA", drChecks["DDA"].ToString());

                                            // add image
                                            ipoImages.SetOutputImagePath(this.outputImagePath);
                                            strCheckFilePath = ipoImages.GetRelativeImagePath(oCheck, intCurrentPage);

                                            //**************************************************************
                                            try
                                            {
                                                bool bFileExists = false;
                                                //* Consolidate directory structure to rename the Image file.
                                                if (ipoImages.GetImageStorageMode() == ImageStorageMode.HYLAND)
                                                {
                                                    if (File.Exists(Path.Combine(this.outputImagePath, strCheckFilePath)))
                                                    {
                                                        strNewFileName = strCheckFilePath;
                                                        bFileExists = true;
                                                    }

                                                }
                                                else
                                                {   //PICS or Custom
                                                    if (ipoImages.GetImageStorageMode() == ImageStorageMode.CUSTOM)
                                                    {
                                                        strNewFileName = TryGetCustomImageDetails(ipoImages, oCheck, intCurrentPage)
                                                            .GetResult(new ImageDetails(string.Empty)).RelativePath;
                                                        bFileExists = !string.IsNullOrEmpty(strNewFileName);
                                                    }
                                                    else
                                                    {
                                                        strNewFileName = strCheckFilePath.Replace("\\", "_");

                                                        if (File.Exists(Path.Combine(strRootImageLocation, strCheckFilePath)))
                                                        {
                                                            bFileExists = true;
                                                            if (
                                                                !(File.Exists(Path.Combine(this.outputImagePath,
                                                                    strNewFileName))))
                                                            {
                                                                File.Copy(
                                                                    Path.Combine(strRootImageLocation, strCheckFilePath),
                                                                    Path.Combine(this.outputImagePath, strNewFileName));
                                                            }
                                                        }
                                                    }
                                                    if (!bFileExists)
                                                    {
                                                        strNewFileName = GenerateSurrogateImage(
                                                                drChecks, this.outputImagePath, true);
                                                        bFileExists = strNewFileName != string.Empty;
                                                    }
                                                }
                                                if (bFileExists)
                                                {
                                                    switch (intCurrentPage)
                                                    {
                                                        case cPICS.PAGE_FRONT:
                                                            ++ImageFrontCount;
                                                            break;
                                                        case cPICS.PAGE_BACK:
                                                            ++ImageBackCount;
                                                            break;
                                                    }
                                                    dctCheckFields.Add(XML_ATTR_IMAGE_PATH, Path.Combine(this.relativeImagePath.Replace("\\", "/"), strNewFileName));
                                                }
                                                else
                                                {
                                                    if (intCurrentPage == cPICS.PAGE_FRONT)
                                                    {
                                                        eventLog.logError(new eImageNotFoundException(strCheckFilePath));
                                                        dctCheckFields.Add(XML_ELE_CHECK_ERROR, "Check image not available.");
                                                    }
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                eventLog.logError(e);
                                            }

                                            // add key
                                            strCheckKey = intCheckCnt.ToString() + "-" + intCurrentPage.ToString();
                                            dctChecks.Add(strCheckKey, dctCheckFields);
                                        }// end For
                                    } // end if
                                } // end foreach
                            } // end if
                            dr["CheckDataDictionary"] = dctChecks;

                            // Get Documents for given transaction
                            if (transaction.TransactionID > -1)
                            {
                                bolRetVal = ItemProcDAL.GetTransactionDocuments(base.sessionID, transaction, displayScannedCheck, out dtDocuments);
                            }
                            else
                            {
                                bolRetVal = ItemProcDAL.GetTransactionDocumentsBySequence(base.sessionID, transaction, displayScannedCheck, out dtDocuments);
                            }


                            Dictionary<string, Dictionary<string, string>> dctDocuments = new Dictionary<string, Dictionary<string, string>>();

                            if (bolRetVal && dtDocuments != null && dtDocuments.Rows.Count > 0)
                            {
                                int intDocumentCnt = 0;
                                string strDocumentKey = string.Empty;
                                string strDocumentFilePath = string.Empty;
                                string strNewFileName = string.Empty;
                                foreach (DataRow drDocument in dtDocuments.Rows)
                                {
                                    intDocumentCnt += 1;

                                    //Steps required to use ipoimage.GetRelativeImagePath -- should be more generic.
                                    DataRow drTempDocument = drDocument;
                                    cDocument oDocument = new cDocument();
                                    oDocument.LoadDataRow(ref drTempDocument);

                                    for (int intCurrentPage = intDocumentStartPage; intCurrentPage < intDocumentEndPage + 1; ++intCurrentPage)
                                    {
                                        //reset variables
                                        strDocumentFilePath = string.Empty;
                                        strNewFileName = string.Empty;
                                        Dictionary<string, string> dctDocumentFields = new Dictionary<string, string>();

                                        ipoImages.SetOutputImagePath(this.outputImagePath);
                                        strDocumentFilePath = ipoImages.GetRelativeImagePath(oDocument, intCurrentPage);
                                        try
                                        {
                                            bool bFileExists = false;
                                            //* Consolidate directory structure to rename the Image file.
                                            if (ipoImages.GetImageStorageMode() == ImageStorageMode.HYLAND)
                                            {
                                                if (File.Exists(Path.Combine(this.outputImagePath, strDocumentFilePath)))
                                                {
                                                    strNewFileName = strDocumentFilePath;
                                                    bFileExists = true;
                                                }

                                            }
                                            else
                                            { //PICS or Custom
                                                if (ipoImages.GetImageStorageMode() == ImageStorageMode.CUSTOM)
                                                {
                                                    strNewFileName = TryGetCustomImageDetails(ipoImages, oDocument, intCurrentPage)
                                                        .GetResult(new ImageDetails(string.Empty)).RelativePath;
                                                    bFileExists = !string.IsNullOrEmpty(strNewFileName);
                                                }
                                                else
                                                {
                                                    strNewFileName = strDocumentFilePath.Replace("\\", "_");

                                                    // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                                                    // replaces IsColor
                                                    if (
                                                        (strNewFileName.Contains("_FG") || strNewFileName.Contains("_FC"))
                                                        && (strNewFileName.ToLower().EndsWith("." + FILE_EXT_TIF)))
                                                    {
                                                        strNewFileName = strNewFileName.Substring(0
                                                            , strNewFileName.Length - 4) + "." + FILE_EXT_JPG;
                                                    }

                                                    if (File.Exists(Path.Combine(strRootImageLocation, strDocumentFilePath)))
                                                    {
                                                        if (!(File.Exists(Path.Combine(this.outputImagePath, strNewFileName))))
                                                        {
                                                            File.Copy(Path.Combine(strRootImageLocation, strDocumentFilePath)
                                                                , Path.Combine(this.outputImagePath, strNewFileName));

                                                        }
                                                        bFileExists = true;
                                                    } // if exists
                                                }
                                                if (!bFileExists)//try and get surrogate then if the image doesn't exist
                                                {
                                                    strNewFileName = GenerateSurrogateImage(
                                                                drDocument, this.outputImagePath, false);
                                                    bFileExists = strNewFileName != string.Empty;
                                                }
                                                
                                            } //else pics or custom
                                            if (bFileExists)
                                            {
                                                switch (intCurrentPage)
                                                {
                                                    case cPICS.PAGE_FRONT:
                                                        ++ImageFrontCount;
                                                        break;
                                                    case cPICS.PAGE_BACK:
                                                        ++ImageBackCount;
                                                        break;
                                                }
                                                dctDocumentFields.Add(XML_ATTR_IMAGE_PATH, this.relativeImagePath.Replace("\\", "/") + strNewFileName);
                                            }
                                            else
                                            {
                                                if (intCurrentPage == cPICS.PAGE_FRONT)
                                                {
                                                    eventLog.logError(new eImageNotFoundException(strDocumentFilePath));
                                                    dctDocumentFields.Add(XML_ATTR_DOCUMENT_ERROR, "Document image not available.");
                                                }
                                            }

                                        }
                                        catch (Exception e)
                                        {
                                            eventLog.logError(e);
                                            strNewFileName = "";
                                        }
                                        // add key
                                        strDocumentKey = intDocumentCnt.ToString() + "-" + intCurrentPage.ToString();
                                        dctDocuments.Add(strDocumentKey, dctDocumentFields);
                                    } // for
                                } // foreach
                            } // if
                            dr["DocumentDataDictionary"] = dctDocuments;
                        } // using cipoimage
                    } // foreach through dtSearch

                } // if include images
            } // if dtSearch

            //Append Search Criteria and Totals
            nodeSearchCriteria = ipoXmlLib.addElement(docXml.DocumentElement, "Recordset", string.Empty);
            ipoXmlLib.addAttribute(nodeSearchCriteria, "Name", "searchCriteria");

            if (!bolCOTSOnly)
            {
                nodeTemp = ipoXmlLib.addElement(nodeSearchCriteria, "Record", string.Empty);
                ipoXmlLib.addAttribute(nodeTemp, "displayTitle", "Check Count");
                ipoXmlLib.addAttribute(nodeTemp, "displayText", intCheckCount.ToString());

                nodeTemp = ipoXmlLib.addElement(nodeSearchCriteria, "Record", string.Empty);
                ipoXmlLib.addAttribute(nodeTemp, "displayTitle", "Check Total");
                ipoXmlLib.addAttribute(nodeTemp, "displayText", decCheckTotal.ToString("C"));
            }

            nodeTemp = ipoXmlLib.addElement(nodeSearchCriteria, "Record", string.Empty);
            ipoXmlLib.addAttribute(nodeTemp, "displayTitle", "Search Criteria");
            try
            {
                strCriteriaText = BuildCriteriaText(intLockboxID, olClientAccountDisplayOverride, nodeDateFrom.InnerText, nodeDateTo.InnerText, strBatchIDFrom, strBatchIDTo, strBatchNumberFrom, strBatchNumberTo, strSerial, decAmountFrom, decAmountTo, bolCOTSOnly,
                bolMarkSense, nlWhereClause);
            }
            catch (Exception)
            {
                strCriteriaText = string.Empty;
            }
            ipoXmlLib.addAttribute(nodeTemp, "displayText", strCriteriaText);
        }

        private PossibleResult<ImageDetails> TryGetCustomImageDetails(cipoImages imageContext, IItem imageItem, int intCurrentPage)
        {
            try
            {
                var customImage = imageContext.TryGetCustomImage(imageItem, intCurrentPage).GetResult(new CustomImage());
                if (customImage.Details == null || customImage.Data == null) return Result.None<ImageDetails>();

                var newFileName = customImage.Details.RelativePath.Replace("\\", "_").Replace(":", "_");
                File.WriteAllBytes(Path.Combine(this.outputImagePath, newFileName), customImage.Data);
                return Result.Real(customImage.Details);
            }
            catch(Exception e)//just want to capture any exception here and log it and then return back a None result
            {
                eventLog.logError(e);
                return Result.None<ImageDetails>();
            }
        }

        protected IItem RecordToItem(DataRow imageRow, bool isCheck) {
            IItem resultItem;
            if (isCheck) {
                resultItem = new cCheck();
                ((cCheck) resultItem).LoadDataRow(ref imageRow);
            }
            else {
                resultItem = new cDocument();
                ((cDocument)resultItem).LoadDataRow(ref imageRow);
            }
            return resultItem;
        }

        protected ImageResponse GenerateImage(ipoEmbeddedImage imageGenerator, bool isCheck, IItem item, BatchPaymentType paymentType) { 
            byte[] resultingImage;
            var response = new ImageResponse();

            if (isCheck) {
                response.IsSuccessful = imageGenerator.GetCheckImage(item, paymentType, out resultingImage);
                response.ImageBytes = resultingImage;
            }
            else {
                response = imageGenerator.GetDocumentImage(item, paymentType);
            }
            return response;
        }

        protected string GenerateSurrogateImage(DataRow imageRow, string outputImagePath, bool isCheck) {
            string surrogateFileName = "";
            string keyData = "Failed to parse check record";
            var validPaymentTypes = new []{
                    BatchPaymentType.ACH,
                    BatchPaymentType.WIRE};
            var imageResponse = new ImageResponse();

            try {
                using (var emb = new ipoEmbeddedImage(this.siteKey)) {
                    var paymentType = (BatchPaymentType)Enum.Parse(typeof(BatchPaymentType), imageRow["PaymentType"].ToString().ToUpper());
                    if (validPaymentTypes.Contains(paymentType)) {
                        var imageItem = RecordToItem(imageRow, isCheck);
                        
                        keyData = string.Format(
                            "BankID {0}, LockboxID {1}, DepositDate {2:MM/dd/yyyy}, SourceBatchID {3}, BatchSequence {4}",
                            imageItem.BankID, imageItem.LockboxID, imageItem.DepositDate, imageItem.SourceBatchID, imageItem.BatchSequence);
                        imageResponse = GenerateImage(emb, isCheck, imageItem, paymentType);
                        if (imageResponse.IsSuccessful) {
                            if (!Directory.Exists(outputImagePath)) {
                                Directory.CreateDirectory(Path.GetDirectoryName(outputImagePath));
                            }

                            var fileType = string.IsNullOrEmpty(imageResponse.FileType)
                                ? "tif"
                                : imageResponse.FileType.TrimStart('.');
                            string newImageFileName = string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}.{7}",
                                imageItem.ImportTypeShortName,
                                imageItem.BankID,
                                imageItem.LockboxID,
                                imageItem.DepositDate.ToString("yyyyMMdd"),
                                imageItem.SourceBatchID,
                                imageItem.BatchSequence,
                                imageItem.FileDescriptor,
                                fileType);
                            var fullImageFileName = Path.Combine(outputImagePath, newImageFileName);
                            eventLog.logEvent(string.Format("Writing Surrogate to: {0}", fullImageFileName),
                                "cJobRequestSearchResults.GenerateSurrogateImage", MessageImportance.Essential);
                            File.WriteAllBytes(fullImageFileName, imageResponse.ImageBytes);
                            surrogateFileName = Path.GetFileName(fullImageFileName);
                        }
                        else {
                            eventLog.logError(
                                new Exception(string.Format("Unable to generate {0} image for: {1}",
                                    imageRow["PaymentType"].ToString(), keyData)));
                        }
                    }
                }
            }
            catch (Exception ex) {
                eventLog.logError(new Exception(string.Format("Unable to generate {0} image for: {1}", imageRow["PaymentType"].ToString(), keyData), ex));
            }
            return surrogateFileName;
        }
        
        private static string getAttributeValue(XmlNode node, string attributeName)
        {
            string retVal = string.Empty;
            try
            {
                if (node.Attributes.GetNamedItem(attributeName) != null)
                    retVal = node.Attributes.GetNamedItem(attributeName).Value;
            }
            catch { }
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outXml">
        ///     Xml that is going to be sent returned by the function to the Search results page.
        /// </param>
        /// <param name="docQueryRespXml">
        ///     Xml that was produced from the Lockbox Search stored procedure.  
        ///     This Xml contains summary results and column IDs sent back by the server.
        /// </param>
        private static void AppendColumnNames(XmlDocument outXml, XmlDocument docQueryRespXml)
        {
            string strColumnName;
            string strFieldName;
            string strDisplayName;
            XmlNode nodeSelectField;
            XmlAttribute attColID;

            string strTableName;
            XmlAttribute attColumnName;
            XmlNodeList nodColumns;
            string xPathCriteria = string.Empty;

            // These are all the columns to include in the report
            nodColumns = outXml.DocumentElement.SelectNodes("/searchXml/Recordset[@Name='Columns']/Record");
            foreach (XmlNode nodeCol in nodColumns)
            {
                strTableName = getAttributeValue(nodeCol, "TableName");
                strFieldName = getAttributeValue(nodeCol, "fieldName");
                strDisplayName = getAttributeValue(nodeCol, "displayName");

                strColumnName = strTableName + strFieldName;
                xPathCriteria = "/Page/RecordSet[@Name='Results']/SelectFields/field[@tablename='" + strTableName + "' and @fieldname='" + strFieldName + "' and @reporttitle='" + strDisplayName + "']";

                // if it exists in the QueryResults/SelectFields list, then we need to fix the column name naming convention 
                // to match what comes from the SP: <tablename><fieldName>_<colID> (colID only if specified)
                nodeSelectField = docQueryRespXml.SelectSingleNode(xPathCriteria);
                if (nodeSelectField != null)
                {
                    attColID = nodeSelectField.Attributes["ColID"];
                    if (attColID != null)
                    {
                        strColumnName += "_" + attColID.Value;
                    }
                }
                if (strColumnName != string.Empty)
                {
                    attColumnName = outXml.CreateAttribute("ColumnName");
                    attColumnName.Value = strColumnName;
                    nodeCol.Attributes.Append(attColumnName);
                }
            }       // end foreach
        }

        private static string BuildCriteriaText(int LockboxID,
                                                string DisplayName,
                                                string DepositDateFrom,
                                                string DepositDateTo,
                                                string BatchIDFrom,
                                                string BatchIDTo,
                                                string BatchNumberFrom,
                                                string BatchNumberTo,
                                                string Serial,
                                                Decimal AmountFrom,
                                                Decimal AmountTo,
                                                bool COTSOnly,
                                                bool MarkSenseOnly,
                                                XmlNodeList AdvancedSearchParms)
        {

            //Return value
            string strSearchCriteria = string.Empty;

            //Advanced Search Criteria
            string strReportTitle = string.Empty;
            string strOperator = string.Empty;
            string strValue = string.Empty;

            try
            {
                //Client Account
                strSearchCriteria = "Workgroup = " + DisplayName + "; ";

                //Deposit Dates
                if (DepositDateFrom.Length > 0 && DepositDateTo.Length > 0)
                {
                    strSearchCriteria += "Deposit Date Is Greater Than or Equal to " + DepositDateFrom + " and Less Than or Equal to " + DepositDateTo + "; ";
                }
                else if (DepositDateFrom.Length > 0)
                {
                    strSearchCriteria += "Deposit Date Is Greater Than or Equal to " + DepositDateFrom + "; ";
                }
                else if (DepositDateTo.Length > 0)
                {
                    strSearchCriteria += "Deposit Date Is Less Than or Equal to " + DepositDateTo + "; ";
                }

                //Batch ID Range
                if (BatchIDFrom.Length > 0 && BatchIDTo.Length > 0)
                {
                    strSearchCriteria += "Batch ID Is Greater Than or Equal to " + BatchIDFrom + " and Less Than or Equal to " + BatchIDTo + "; ";
                }
                else if (BatchIDFrom.Length > 0)
                {
                    strSearchCriteria += "Batch ID Is Greater Than or Equal to " + BatchIDFrom + "; ";
                }
                else if (BatchIDTo.Length > 0)
                {
                    strSearchCriteria += "Batch ID Is Less Than or Equal to " + BatchIDTo + "; ";
                }

                //Batch Number Range
                if (BatchNumberFrom.Length > 0 && BatchNumberTo.Length > 0)
                {
                    strSearchCriteria += "Batch Number Is Greater Than or Equal to " + BatchNumberFrom + " and Less Than or Equal to " + BatchNumberTo + "; ";
                }
                else if (BatchNumberFrom.Length > 0)
                {
                    strSearchCriteria += "Batch Number Is Greater Than or Equal to " + BatchNumberFrom + "; ";
                }
                else if (BatchNumberTo.Length > 0)
                {
                    strSearchCriteria += "Batch Number Is Less Than or Equal to " + BatchNumberTo + "; ";
                }

                //Check Number
                if (Serial.Length > 0)
                {
                    strSearchCriteria += "Check Number Contains " + Serial + "; ";
                }

                //Check Amount Range
                if (AmountFrom > 0 && AmountTo > 0)
                {
                    strSearchCriteria += "Check Amount Is Greater Than or Equal to " + AmountFrom.ToString("C") + " and Less Than or Equal to " + AmountTo.ToString("C") + "; ";
                }
                else if (AmountFrom > 0)
                {
                    strSearchCriteria += "Check Amount Is Greater Than or Equal to " + AmountFrom.ToString("C") + "; ";
                }
                else if (AmountTo > 0)
                {
                    strSearchCriteria += "Check Amount Is Less Than or Equal to " + AmountTo.ToString("C") + "; ";
                }

                //COTS Only
                if (COTSOnly)
                {
                    strSearchCriteria += "Display Correspondence Only Transactions; ";
                }

                //Mark Sense Only
                if (MarkSenseOnly)
                {
                    strSearchCriteria += "Display Mark Sense Transactions Only; ";
                }

                //Loop through search Advanced Search Criteria
                if (AdvancedSearchParms != null)
                {
                    foreach (XmlNode childNode in AdvancedSearchParms)
                    {
                        if ((childNode.Attributes.GetNamedItem("reporttitle") != null))
                        {
                            strReportTitle = childNode.Attributes.GetNamedItem("reporttitle").InnerText;
                        } if ((childNode.Attributes.GetNamedItem("operator") != null))
                        {
                            strOperator = childNode.Attributes.GetNamedItem("operator").InnerText;
                        } if ((childNode.Attributes.GetNamedItem("value") != null))
                        {
                            strValue = childNode.Attributes.GetNamedItem("value").InnerText;
                        }

                        if ((strReportTitle.Length > 0) && (strOperator.Length > 0) && (strValue.Length > 0))
                        {
                            strSearchCriteria += strReportTitle + " " + strOperator + " " + strValue + "; ";
                        }
                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }

            return (strSearchCriteria);
        }

        /// <summary>
        /// 
        /// </summary>
        private void buildOutputPath()
        {
            try { Directory.CreateDirectory(this.outputRootPath); }
            catch { }
            try { Directory.CreateDirectory(this.outputImagePath); }
            catch { }
        }

        /// <summary>
        /// 
        /// </summary>
        private string outputRootPath
        {
            get
            {
                string strFullOutputPath = ipoLib.cleanPath(this.serverOptions.onlineArchivePath);
                strFullOutputPath += this.jobID;
                strFullOutputPath += "\\";

                return strFullOutputPath;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private string outputImagePath
        { get { return (ipoLib.cleanPath(this.outputRootPath) + this.relativeImagePath); } }


        /// <summary>
        /// 
        /// </summary>
        private string relativeImagePath
        { get { return ("images\\"); } }


        /// <summary>
        /// Accepts Search Results Datatable and supporting Xml document and outputs it to a xml file.
        /// </summary>
        /// <param name="docXml">Supporting Xml document</param>
        /// <param name="dt">Search Results DataTable</param>
        private bool outputXmlDoc(ref XmlDocument docXml, ref DataTable dt)
        {
            string strFileName;
            XmlNode nodeDisplayColumns;
            XmlNode nodeSearchCriteria;
            int iColCnt = 0;

            try
            {
                nodeDisplayColumns = docXml.SelectSingleNode(XML_ELE_SEARCH_XML_ROOT + "/Recordset[@Name='Columns']");
                nodeSearchCriteria = docXml.SelectSingleNode(XML_ELE_SEARCH_XML_ROOT + "/Recordset[@Name='" + XML_ELE_SEARCH_CRITERIA + "']");
                strFileName = getAvailableFileName(this.outputRootPath + this.jobID, FILE_EXT_XML);
                iColCnt = dt.Columns.Count;
                var hsetFieldNames = new HashSet<string>();
                //Preload with default field names
                hsetFieldNames.Add(XML_ATTR_BANK_ID);
                hsetFieldNames.Add(XML_ATTR_LOCKBOX_ID);
                hsetFieldNames.Add(XML_ATTR_BATCH_ID);
                hsetFieldNames.Add(XML_ATTR_DEPOSIT_DATE);
                hsetFieldNames.Add(XML_ATTR_TRANSACTION_ID);

                using (StreamWriter sw = new StreamWriter(strFileName, false, System.Text.Encoding.ASCII))
                {
                    using (XmlTextWriter xml = new XmlTextWriter(sw))
                    {
                        xml.WriteStartDocument();
                        // root tag
                        xml.WriteStartElement(XML_ELE_SEARCH_XML_ROOT);
                        xml.WriteWhitespace("\n");

                        // Column Headers
                        xml.WriteStartElement(XML_ELE_OPTIONAL_COLUMNS);
                        xml.WriteWhitespace("\n");
                        //write columns
                        foreach (XmlNode nodeRecord in nodeDisplayColumns.SelectNodes("Record"))
                        {
                            xml.WriteStartElement(XML_ELE_OPTIONAL_COLUMN);
                            // AttributeString inserts and not appends hence the reverse order
                            xml.WriteAttributeString(XML_ATTR_FIELD_NAME, nodeRecord.Attributes[XML_ATTR_FIELD_NAME].Value.ToString());
                            xml.WriteAttributeString(XML_ATTR_DATA_TYPE_ENUM, nodeRecord.Attributes[XML_ATTR_DATA_TYPE_ENUM].Value.ToString());
                            xml.WriteAttributeString(XML_ATTR_DISPLAY_NAME, nodeRecord.Attributes[XML_ATTR_DISPLAY_NAME].Value.ToString());

                            xml.WriteEndElement(); // XML_ELE_OPTIONAL_COLUMN
                            xml.WriteWhitespace("\n");

                            //save fieldnames in hashset for lookup later (no dups)
                            if (!(hsetFieldNames.Contains(nodeRecord.Attributes["ColumnName"].Value.ToString())))
                            {
                                hsetFieldNames.Add(nodeRecord.Attributes["ColumnName"].Value.ToString());
                            }
                        }
                        xml.WriteEndElement(); // XML_ELE_OPTIONAL_COLUMNS
                        xml.WriteWhitespace("\n");

                        // Data
                        xml.WriteStartElement(XML_ELE_SEARCH_RESULTS);
                        xml.WriteWhitespace("\n");

                        foreach (DataRow dr in dt.Rows)
                        {
                            xml.WriteStartElement(XML_ELE_DATA_ROW);
                            xml.WriteWhitespace("\n");
                            //write columns
                            for (int i = 0; i < iColCnt; i++)
                            {
                                string strColumnName = dr.Table.Columns[i].ColumnName.ToString();
                                // Only write fields that are in the fieldnames hashset.
                                if (hsetFieldNames.Contains(strColumnName) && !strColumnName.Equals(XML_ATTR_SOURCE_BATCH_ID))
                                {
                                    xml.WriteStartElement("Fld");
                                    xml.WriteAttributeString("ID", strColumnName);
                                    if (strColumnName.Equals(XML_ATTR_BATCH_ID))
                                    {
                                        string tempValue = dr[XML_ATTR_SOURCE_BATCH_ID].ToString();
                                        xml.WriteValue(tempValue);
                                    }
                                    else
                                    {
                                        xml.WriteValue(dr[i].ToString());
                                    }
                                    xml.WriteEndElement(); //Fld
                                    xml.WriteWhitespace("\n");
                                }
                                // Write Image Data
                                // Checks
                                if (strColumnName == "CheckDataDictionary")
                                {
                                    Dictionary<string, Dictionary<string, string>> dctChecks = (Dictionary<string, Dictionary<string, string>>)dr[i];
                                    foreach (KeyValuePair<string, Dictionary<string, string>> entry in dctChecks)
                                    {
                                        xml.WriteStartElement("check");
                                        Dictionary<string, string> dctCheckFields = entry.Value;
                                        foreach (KeyValuePair<string, string> checkFieldsEntry in dctCheckFields.Reverse())
                                        {
                                            xml.WriteAttributeString(checkFieldsEntry.Key, checkFieldsEntry.Value);
                                        }
                                        xml.WriteEndElement(); // check
                                        xml.WriteWhitespace("\n");
                                    }
                                }
                                // Documents
                                if (strColumnName == "DocumentDataDictionary")
                                {
                                    Dictionary<string, Dictionary<string, string>> dctDocuments = (Dictionary<string, Dictionary<string, string>>)dr[i];
                                    foreach (KeyValuePair<string, Dictionary<string, string>> entry in dctDocuments)
                                    {
                                        xml.WriteStartElement("document");
                                        Dictionary<string, string> dctDocumentFields = entry.Value;
                                        foreach (KeyValuePair<string, string> documentFieldsEntry in dctDocumentFields.Reverse())
                                        {
                                            xml.WriteAttributeString(documentFieldsEntry.Key, documentFieldsEntry.Value);
                                        }
                                        xml.WriteEndElement(); // document
                                        xml.WriteWhitespace("\n");
                                    }
                                }
                            }
                            xml.WriteEndElement(); // XML_ELE_DATA_ROW
                            xml.WriteWhitespace("\n");
                        }
                        xml.WriteEndElement(); // XML_ELE_SEARCH_RESULTS
                        xml.WriteWhitespace("\n");

                        //Search criteria
                        xml.WriteStartElement(XML_ELE_SEARCH_CRITERIA);
                        xml.WriteWhitespace("\n");
                        foreach (XmlNode nodeRecord in nodeSearchCriteria.SelectNodes("Record"))
                        {
                            xml.WriteStartElement(XML_ELE_SEARCH_CRITERIA_ROW);
                            xml.WriteAttributeString(XML_ATTR_DISPLAY_TITLE, nodeRecord.Attributes[XML_ATTR_DISPLAY_TITLE].Value.ToString());
                            xml.WriteAttributeString(XML_ATTR_DISPLAY_TEXT, nodeRecord.Attributes[XML_ATTR_DISPLAY_TEXT].Value.ToString());
                            xml.WriteEndElement(); // XML_ELE_OPTIONAL_COLUMN
                            xml.WriteWhitespace("\n");
                        }
                        xml.WriteEndElement(); // XML_ELE_SEARCH_CRITERIA
                        xml.WriteWhitespace("\n");

                        xml.WriteEndElement(); // XML_ELE_SEARCH_XML_ROOT
                        xml.WriteEndDocument();
                    }
                }
                return (true);
            }
            catch (Exception e)
            {
                eventLog.logError(e);
                return (false);
            }
        }

        /// <summary>
        /// Accepts Search Results Xml document an outputs a formatted Html 
        /// document containing them.
        /// </summary>
        /// <param name="docXml">Supporting Xml document</param>
        /// <param name="dt">Search Results datatable </param>
        private bool outputHtml(ref XmlDocument docXml, ref DataTable dt)
        {

            int iColCnt = 0;
            int iRowCnt = 0;
            string strFileName;
            XmlNode nodeDisplayColumns;
            XmlNode nodeSearchCriteria;
            string strImageSvcCssFullFileName = string.Empty;
            DataColumnCollection dtColumns;

            try
            {
                strFileName = getAvailableFileName(this.outputRootPath + checkForBrackets(this.jobID), FILE_EXT_HTML);

                // WI 71542 : Using the sitekey from the request, not the server's sitekey.
                strImageSvcCssFullFileName = serverOptions.GetIpoTemplatesDirectory(this.siteKey) + CSS_SEARCH_OUTPUT_HTML;
                nodeDisplayColumns = docXml.SelectSingleNode(XML_ELE_SEARCH_XML_ROOT + "/Recordset[@Name='Columns']");
                nodeSearchCriteria = docXml.SelectSingleNode(XML_ELE_SEARCH_XML_ROOT + "/Recordset[@Name='" + XML_ELE_SEARCH_CRITERIA + "']");
                iColCnt = dt.Columns.Count;
                dtColumns = dt.Columns;
                var hsetFieldNames = new HashSet<string>();

                using (StreamWriter sw = new StreamWriter(strFileName, false, System.Text.Encoding.ASCII))
                {
                    using (HtmlTextWriter html = new HtmlTextWriter(sw))
                    {
                        html.WriteFullBeginTag("html");
                        html.WriteLine();
                        // Write header 
                        html.Indent++;
                        html.WriteFullBeginTag("head");
                        html.WriteLine();
                        // title
                        html.Indent++;
                        html.WriteFullBeginTag("title");
                        html.Write("Search Results");
                        html.WriteEndTag("title");
                        html.WriteLine();

                        html.WriteFullBeginTag("style");
                        html.WriteLine();
                        html.Indent++;

                        // Check if style file exists
                        if (File.Exists(strImageSvcCssFullFileName))
                        {
                            string strline = string.Empty;
                            using (StreamReader StyleFile = new StreamReader(strImageSvcCssFullFileName))
                            {
                                while (!StyleFile.EndOfStream)
                                {
                                    strline = StyleFile.ReadLine();
                                    // Ignore empty lines.
                                    if (strline.Length > 0)
                                    {
                                        html.Write(strline);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Add default style if style file doesn't exist
                            writeHTML_defaultCSS(html);
                        }

                        html.Indent--;
                        html.WriteEndTag("style");
                        html.WriteLine();
                        html.Indent--;

                        html.WriteEndTag("head");
                        html.WriteLine();

                        // Write Body 
                        html.WriteFullBeginTag("body");
                        html.WriteLine();

                        // use table to display 'Search Results'
                        html.Indent++;
                        html.WriteBeginTag("table");
                        html.WriteAttribute("align", "center");
                        html.WriteAttribute("border", "0");
                        html.WriteAttribute("width", "98%");
                        html.WriteAttribute("cellpadding", "0");
                        html.WriteAttribute("cellspacing", "0");
                        html.Write(HtmlTextWriter.TagRightChar);
                        html.WriteLine();

                        html.Indent++;
                        html.WriteFullBeginTag("tr");
                        html.WriteLine();

                        html.Indent++;
                        html.WriteBeginTag("td");
                        html.WriteAttribute("align", "left");
                        html.WriteAttribute("class", "contenttitle");
                        html.Write(HtmlTextWriter.TagRightChar);
                        html.WriteLine();
                        html.Indent++;
                        html.Write("Search Results");
                        html.WriteLine();
                        html.WriteBreak();
                        html.Indent--;
                        html.WriteEndTag("td");
                        html.WriteLine();
                        html.Indent--;

                        html.WriteEndTag("tr");
                        html.WriteLine();
                        html.Indent--;

                        html.WriteEndTag("table");
                        html.WriteLine();

                        //Table 1

                        html.WriteBeginTag("table");
                        html.WriteAttribute("align", "center");
                        html.WriteAttribute("border", "0");
                        html.WriteAttribute("width", "98%");
                        html.WriteAttribute("cellpadding", "0");
                        html.WriteAttribute("cellspacing", "0");
                        html.Write(HtmlTextWriter.TagRightChar);
                        html.WriteLine();

                        html.Indent++;
                        html.WriteFullBeginTag("tr");
                        html.WriteLine();

                        html.Indent++;
                        html.WriteBeginTag("td");
                        html.WriteAttribute("align", "left");
                        html.WriteAttribute("class", "contentsubtitle");
                        html.Write(HtmlTextWriter.TagRightChar);
                        html.WriteLine();
                        html.Indent++;

                        //Table 2
                        html.WriteBeginTag("table");
                        html.WriteAttribute("align", "center");
                        html.WriteAttribute("border", "0");
                        html.WriteAttribute("width", "98%");
                        html.WriteAttribute("cellpadding", "0");
                        html.WriteAttribute("cellspacing", "0");
                        html.Write(HtmlTextWriter.TagRightChar);
                        html.WriteLine();

                        html.Indent++;
                        html.WriteFullBeginTag("tr");
                        html.WriteLine();

                        html.Indent++;
                        html.WriteFullBeginTag("td");
                        html.WriteLine();
                        html.Indent++;

                        //Table 3
                        html.WriteBeginTag("table");
                        html.WriteAttribute("border", "0");
                        html.WriteAttribute("width", "100%");
                        html.WriteAttribute("cellpadding", "2");
                        html.WriteAttribute("cellspacing", "1");
                        html.Write(HtmlTextWriter.TagRightChar);
                        html.WriteLine();

                        html.Indent++;
                        html.WriteFullBeginTag("tr");
                        html.WriteLine();

                        html.Indent++;

                        // Write table column headers & capture the database ColumnNames to match with DataSet
                        foreach (XmlNode nodeRecord in nodeDisplayColumns.SelectNodes("Record"))
                        {

                            hsetFieldNames.Add(nodeRecord.Attributes["ColumnName"].Value.ToString());

                            // Per CR 53949 - Custom titles for BatchNumber and Check Amount
                            string strDisplayName = nodeRecord.Attributes[XML_ATTR_DISPLAY_NAME].Value.ToString();
                            switch (strDisplayName)
                            {
                                case "BatchNumber":
                                    strDisplayName = "Batch";
                                    break;
                                case "Check Amount":
                                    strDisplayName = "Payment Amount";
                                    break;
                                default:
                                    break;
                            }
                            writeHTML_cellHeader(strDisplayName, html);
                        }

                        // Checks Heading
                        // Add report header for Checks
                        writeHTML_cellHeader("Checks", html);
                        writeHTML_cellHeader("Documents", html);


                        html.Indent--;
                        html.WriteEndTag("tr");
                        html.WriteLine();
                        html.Indent--;
                        html.Indent++;

                        // Write out each Line of data, using only the columns from hsetFieldNames
                        List<string> unavailableColumns = new List<string>();
                        foreach (DataRow dr in dt.Rows)
                        {
                            string strColumnName = string.Empty;
                            iRowCnt++;
                            html.WriteBeginTag("tr");
                            html.WriteAttribute("class", (iRowCnt % 2 == 0 ? "evenrow" : "oddrow"));
                            html.Write(HtmlTextWriter.TagRightChar);
                            html.WriteLine();

                            // Only write columns that are listed in hsetFieldNames & datatable
                            // If for some reason, the column isn't in the DataTable, then log it only once in the eventLog,
                            // keep track of unavailable column in this list:  unavailableColumns.
                            foreach (string columnName in hsetFieldNames)
                            {
                                if (dt.Columns.Contains(columnName))
                                {
                                    writeHTML_cellData(html, columnName, dr, dt.Columns[columnName].DataType);
                                }
                                else if (columnName.Contains("."))
                                {
                                    writeHTML_cellData(html, columnName.Split('.')[1], dr, dt.Columns[columnName.Split('.')[1]].DataType);
                                }
                                else
                                {
                                    writeHTML_cellData(html, "&nbsp;", "reportdata");
                                    logUnavailableColumns(unavailableColumns, columnName, "cJobRequestSearchResults.outputHtml");
                                }
                            }

                            // Make sure there is a Dictionary object in the DataTable before adding to report.
                            if (dt.Columns.Contains("CheckDataDictionary"))
                            {



                                strColumnName = "CheckDataDictionary";
                                html.Indent++;
                                html.WriteBeginTag("td");
                                html.WriteAttribute("class", "reportdata");
                                html.Write(HtmlTextWriter.TagRightChar);
                                html.WriteLine();
                                Dictionary<string, Dictionary<string, string>> dctChecks = (Dictionary<string, Dictionary<string, string>>)dr[strColumnName];
                                foreach (KeyValuePair<string, Dictionary<string, string>> entry in dctChecks)
                                {
                                    Dictionary<string, string> dctCheckFields = entry.Value;

                                    foreach (KeyValuePair<string, string> checkFieldsEntry in dctCheckFields)
                                    {
                                        if (checkFieldsEntry.Key == XML_ELE_CHECK_ERROR)
                                        {
                                            html.Indent++;
                                            html.WriteBeginTag("div");
                                            html.WriteAttribute("class", "errMsg");
                                            html.Write(HtmlTextWriter.TagRightChar);
                                            html.Write(checkFieldsEntry.Value);
                                            html.WriteEndTag("div");
                                            html.WriteLine();
                                            html.Indent--;
                                        }
                                        else if (checkFieldsEntry.Key == XML_ATTR_IMAGE_PATH)
                                        {
                                            //create link
                                            html.Indent++;
                                            html.WriteBeginTag(HtmlTextWriterTag.A.ToString());
                                            html.WriteAttribute(HtmlTextWriterAttribute.Href.ToString(), checkFieldsEntry.Value);
                                            html.Write(HtmlTextWriter.TagRightChar);
                                            html.Write("Check");
                                            html.WriteEndTag(HtmlTextWriterTag.A.ToString());
                                            html.WriteLine();
                                            html.Indent--;
                                        }
                                    }
                                }
                                html.WriteEndTag("td");
                                html.WriteLine();
                                html.Indent--;
                            }

                            // Documents
                            if (dt.Columns.Contains("DocumentDataDictionary"))
                            {

                                strColumnName = "DocumentDataDictionary";
                                html.Indent++;
                                html.WriteBeginTag("td");
                                html.WriteAttribute("class", "reportdata");
                                html.Write(HtmlTextWriter.TagRightChar);
                                html.WriteLine();
                                Dictionary<string, Dictionary<string, string>> dctDocuments = (Dictionary<string, Dictionary<string, string>>)dr[strColumnName];
                                foreach (KeyValuePair<string, Dictionary<string, string>> entry in dctDocuments)
                                {
                                    Dictionary<string, string> dctDocumentFields = entry.Value;
                                    foreach (KeyValuePair<string, string> documentFieldsEntry in dctDocumentFields)
                                    {
                                        if (documentFieldsEntry.Key == XML_ATTR_DOCUMENT_ERROR)
                                        {
                                            html.Indent++;
                                            html.WriteBeginTag("div");
                                            html.WriteAttribute("class", "errMsg");
                                            html.Write(HtmlTextWriter.TagRightChar);
                                            html.Write(documentFieldsEntry.Value);
                                            html.WriteEndTag("div");
                                            html.WriteLine();
                                            html.Indent--;
                                        }
                                        else
                                        {
                                            //create link
                                            html.Indent++;
                                            html.WriteBeginTag(HtmlTextWriterTag.A.ToString());
                                            html.WriteAttribute(HtmlTextWriterAttribute.Href.ToString(), documentFieldsEntry.Value);
                                            html.Write(HtmlTextWriter.TagRightChar);
                                            html.Write("Document");
                                            html.WriteEndTag(HtmlTextWriterTag.A.ToString());
                                            html.WriteLine();
                                            html.Indent--;
                                        }
                                    }
                                }
                                html.WriteEndTag("td");
                                html.WriteLine();
                                html.Indent--;
                            }
                            html.WriteEndTag("tr");
                            html.WriteLine();
                        }

                        #region Write closing HTML Structure

                        html.Indent--;

                        html.WriteEndTag("table");
                        html.WriteLine();
                        html.Indent--;
                        // end of table 3

                        html.WriteEndTag("td");
                        html.WriteLine();
                        html.Indent--;

                        html.WriteEndTag("tr");
                        html.WriteLine();
                        html.Indent--;

                        html.WriteEndTag("table");
                        html.WriteLine();
                        html.Indent--;
                        // end of table 2

                        html.WriteEndTag("td");
                        html.WriteLine();
                        html.Indent--;

                        html.WriteEndTag("tr");
                        html.WriteLine();
                        html.Indent--;

                        html.WriteEndTag("table");
                        html.WriteLine();
                        html.Indent--;
                        // end of table 1                 

                        html.WriteEndTag("body");
                        html.WriteLine();
                        html.Indent--;

                        html.WriteEndTag("html");
                        html.WriteLine();
                        #endregion
                    }
                }

                eventLog.logEvent("File written : " + strFileName
                                , this.GetType().Name
                                , MessageType.Information
                                , MessageImportance.Verbose);

                return (true);
            }
            catch (Exception e)
            {
                eventLog.logError(e);

                return (false);
            }
        }


        private void logUnavailableColumns(List<string> unavailableColumnList, string currentColumn, string Source)
        {

            if (!unavailableColumnList.Contains(currentColumn))
            {
                unavailableColumnList.Add(currentColumn);
                eventLog.logEvent("Requested output column not contained in search results : " + currentColumn, this.GetType().Name, MessageType.Warning, MessageImportance.Debug);
            }
        }


        #region HTML Helper Functions
        private void writeHTML_cellHeader(string displayName, HtmlTextWriter reportHTML)
        {
            reportHTML.WriteBeginTag("td");
            reportHTML.WriteAttribute("align", "left");
            reportHTML.WriteAttribute("class", "reportheading");
            reportHTML.Write(HtmlTextWriter.TagRightChar);
            reportHTML.Write(displayName);
            reportHTML.WriteEndTag("td");
            reportHTML.WriteLine();
        }

        private void writeHTML_cellData(HtmlTextWriter reportHTML, string cellValue, string cssClass)
        {
            reportHTML.Indent++;
            reportHTML.WriteBeginTag("td");
            reportHTML.WriteAttribute("class", cssClass);
            reportHTML.Write(HtmlTextWriter.TagRightChar);
            reportHTML.Write(cellValue);
            reportHTML.WriteEndTag("td");
            reportHTML.WriteLine();
            reportHTML.Indent--;
        }

        private void writeHTML_cellData(HtmlTextWriter reportHTML, string columnName, DataRow dataRow, Type dataType)
        {
            // use default spacer in case the data is null
            // and assume formatting for string dataType
            string columnValue = "&nbsp;";
            string attributeAlign = "left";

            if (!DBNull.Value.Equals(dataRow[columnName]))
            {
                if (dataType == System.Type.GetType("System.Decimal"))
                {
                    attributeAlign = "right";
                    columnValue = ((decimal)dataRow[columnName]).ToString("C");
                }
                else
                {
                    columnValue = dataRow[columnName].ToString();
                }
            }

            // Write the <td></td>
            reportHTML.Indent++;
            reportHTML.WriteBeginTag("td");
            reportHTML.WriteAttribute("align", attributeAlign);
            reportHTML.WriteAttribute("class", "reportdata");
            reportHTML.Write(HtmlTextWriter.TagRightChar);
            reportHTML.Write(columnValue);
            reportHTML.WriteEndTag("td");
            reportHTML.WriteLine();
            reportHTML.Indent--;
        }

        // If stylesheet specified in ini file doesn't exist, this CSS is used.
        private void writeHTML_defaultCSS(HtmlTextWriter reportHTML)
        {
            reportHTML.Write("body {font-family: arial, helvetica, sans-serif; font-size: 11px; font-weight: normal; color: #000000; background-color : #ffffff; margin : 0px }");
            reportHTML.Write("p { font-family :  arial, helvetica, sans-serif; font-size : 11px; font-weight : normal; color : #666666; background-color : #FFFFFF; }");

            reportHTML.Write(".copyright { vertical-align : middle; font-family : arial, helvetica, sans-serif; font-size: 8pt; font-weight : normal; color : #000000; text-decoration : none; }");
            reportHTML.Write(".copyright A { font-family : arial, helvetica, sans-serif; font-weight : normal; font-size : 8pt; color : #6c276a; text-decoration : underline; }");
            reportHTML.Write(".copyright A:LINK { color : #6c276a; }");
            reportHTML.Write(".copyright A:VISITED { color : #6c276a; }");
            reportHTML.Write(".copyright A:HOVER { color : #6c276a; }");

            /*Content Title*/
            reportHTML.Write(".contenttitle { font-family : arial, helvetica, sans-serif; font-size: 16px; font-weight : bold; color : #000000; text-decoration : none; background-color : #F3F3F3; }");
            reportHTML.Write(".contentsubtitle { font-family : arial, helvetica, sans-serif; font-size: 12px; font-weight : normal; font-style : normal; color : #000000; text-decoration : none; }");
            reportHTML.Write(".contentbreak { background-color : #333399; }");

            /*Reports*/
            reportHTML.Write(".reportmain { background-color : #DDDDDD; }");
            reportHTML.Write(".reportsub { background-color : #DDDDDD; }");
            reportHTML.Write(".reportcaption { font-family : arial, helvetica, sans-serif; font-size : 14px; font-weight : bold; color : #000000; text-decoration : none; text-align : left; background-color : #FFFFFF; }");
            reportHTML.Write(".recordcount { font-family : arial, helvetica, sans-serif; font-size: 11px; font-weight : bold; color : #A9A9A9; text-decoration : none; background-color : #FFFFFF; }");

            reportHTML.Write(".reportheading { background-color : #000000; font-family : arial, helvetica, sans-serif; font-size: 12px; font-weight : bold; color : #F3F3F3; text-decoration : none; }");
            reportHTML.Write(".reportheading A:LINK { color : #F3F3F3; text-decoration : underline; }");
            reportHTML.Write(".reportheading A:VISITED { color : #F3F3F3; text-decoration : underline; }");
            reportHTML.Write(".reportheading A:HOVER { color : #F3F3F3; text-decoration : underline; }");

            reportHTML.Write(".reportdata { font-family : arial, helvetica, sans-serif; font-size: 12px; font-weight : normal; color : #000000; text-decoration : none; }");
            reportHTML.Write(".reportdata A:LINK { color : #6c276a; text-decoration : underline; }");
            reportHTML.Write(".reportdata A:VISITED {	color : #6c276a; }");
            reportHTML.Write(".reportdata A:HOVER { color : #6c276a; }");

            reportHTML.Write(".reportfooter { font-family : arial, helvetica, sans-serif; font-size: 12px; font-weight : bold; color : #000000; text-decoration : none; background-color : #FFFFFF; }");
            reportHTML.Write(".reportfooter A { font-family : arial, helvetica, sans-serif; font-size: 11px; font-weight : bold; color : #333399; text-decoration : underline; background-color : #FFFFFF; }");
            reportHTML.Write(".reportfooter A:LINK { color : #6c276a; }");
            reportHTML.Write(".reportfooter A:VISITED { color : #6c276a; }");
            reportHTML.Write(".reportfooter A:HOVER { color : #6c276a; }");

            reportHTML.Write(".evenrow { background-color : #FFFFFF; }");
            reportHTML.Write(".oddrow { background-color : #E0E0E0; }");

            reportHTML.Write("div.errMsg { font-weight:bold; color:Red; }");

        }
        #endregion


        /// <summary>
        /// Accepts Search Results Datatable and supporting Xml document and outputs a delimited text 
        /// document containing them.
        /// </summary>
        /// <param name="docXml">Supporting Xml document</param>
        /// <param name="dt">Search Results DataTable</param>
        /// <param name="fileExtension">Extension to save the filename with.</param>
        private bool outputText(ref XmlDocument docXml, ref DataTable dt, string fileExtension)
        {
            string strFileName;
            XmlNode nodedisplayColumns;
            StringBuilder sbLine = new StringBuilder();
            DataColumnCollection dtColumns;

            try
            {

                // this is searchXML
                nodedisplayColumns = docXml.SelectSingleNode(XML_ELE_SEARCH_XML_ROOT + "/Recordset");

                if (fileExtension.ToLower() == "csv")
                {
                    strFileName = getAvailableFileName(this.outputRootPath + checkForBrackets(this.jobID), fileExtension);
                }
                else
                {
                    strFileName = getAvailableFileName(this.outputRootPath + this.jobID, fileExtension);
                }

                var hsetFieldNames = new HashSet<string>();

                using (StreamWriter sw = new StreamWriter(strFileName, false))
                {


                    foreach (XmlNode nodeRecord in nodedisplayColumns.SelectNodes("Record"))
                    {
                        //save specially formatted ColumnNames returned from SP for getting the values from DataRows
                        hsetFieldNames.Add(nodeRecord.Attributes["ColumnName"].Value.ToString());
                        //write CSV's column headers
                        sw.Write(nodeRecord.Attributes[XML_ATTR_DISPLAY_NAME].Value.ToString());
                        if (nodeRecord != nodedisplayColumns.LastChild)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.Write(sw.NewLine);

                    dtColumns = dt.Columns;
                    List<string> unavailableColumns = new List<string>();
                    foreach (DataRow dr in dt.Rows)
                    {
                        sbLine.Clear();

                        // Build up each line of values in the same order as the Hashset.
                        // Use the ColumnCollection on the DataTable to make sure the Column exists.
                        foreach (string columnName in hsetFieldNames)
                        {
                            if (dt.Columns.Contains(columnName))
                            {
                                if (!Convert.IsDBNull(dr[columnName].ToString()))
                                {
                                    sbLine.Append($"\"{dr[columnName].ToString()}\"");
                                }
                            }
                            else if (columnName.Contains("."))
                            {
                                sbLine.Append($"\"{dr[columnName.Split('.')[1]].ToString()}\"");
                            }
                            else
                            {
                                // If a column is not contained in dt.Columns, only log it once, otherwise will end up with
                                // an error message for the same column name = dt.Rows.Count
                                logUnavailableColumns(unavailableColumns, columnName, "cJobRequestSearchResults.outputText");
                            }
                            sbLine.Append(",");
                        }
                        // Last item in the line, remove the comma, then write it to the stream
                        sbLine.Remove(sbLine.Length - 1, 1);
                        sw.Write(sbLine.ToString());
                        sw.Write(sw.NewLine);
                    }
                }

                eventLog.logEvent("File written : " + strFileName
                                , this.GetType().Name
                                , MessageType.Information
                                , MessageImportance.Verbose);

                return (true);
            }
            catch (Exception e)
            {
                eventLog.logError(e);
                return (false);
            }
        }


        private string checkForBrackets(Guid jobID)
        {
            if (serverOptions.isCurlyBracketsEnabled == true)
            {
                return jobID.ToString("B");
            }
            return jobID.ToString();
        }


        /// <summary>
        /// Accepts supporting Xml document, and search results datatable and outputs a "Print View" Pdf 
        /// document using itextsharp.
        /// </summary>
        /// <param name="docXml">Supporting Xml document</param>
        /// <param name="dt">Search Results datatable</param>
        /// <param name="fileName">Name of file to be saved to the hard drive.</param>
        private bool outputPdfAsPdfTable(ref XmlDocument docXml, ref DataTable dt, string fileName)
        {
            cSearchResultsPdf objSearchResultsPdf = new cSearchResultsPdf();
            objSearchResultsPdf.outputMessage += new outputMessageEventHandler(base.object_outputMessage);
            objSearchResultsPdf.outputError += new outputErrorEventHandler(base.object_outputError);
            objSearchResultsPdf.outputPdfAsTable(ref docXml, ref dt, fileName);
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseFileName"></param>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        private string getAvailableFileName(string baseFileName
                                          , string fileExtension)
        {
            string strRetVal = baseFileName;

            if (fileExtension.Length > 0) { strRetVal += "." + fileExtension; }
            int i = 1;

            if (File.Exists(strRetVal))
            {
                try
                {
                    while (true)
                    {
                        strRetVal = baseFileName + "_" + i.ToString();
                        if (fileExtension.Length > 0) { strRetVal += "." + fileExtension; }

                        if (File.Exists(strRetVal))
                        { ++i; }
                        else
                        { break; }
                    }
                }
                catch (Exception e) { eventLog.logError(e); }
            }

            return (strRetVal);
        }
    }
}
