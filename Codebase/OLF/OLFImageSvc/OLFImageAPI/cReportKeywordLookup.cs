﻿using System;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb (for previous author)
* Date:     04/12/2013
*
* Purpose:  Report Keyword Lookup
*
* Modification History
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI
{
    class cReportKeywordLookup
    {
        private const char _dividerChar = '|';

        public cReportKeywordLookup()
        {
        }
        public cReportKeywordLookup(int iBatchSequence, string sKeyword)
        {
            Keyword = sKeyword;
            BatchSequence = iBatchSequence;
           

        }
        public int BatchSequence
        {
            get;
            set;
        }
        public string Keyword
        {
            get;
            set;
        }
        public string BuildKeyword
        {
            get
            {
                StringBuilder sbKeyWord = new StringBuilder();
                sbKeyWord.Append(BatchSequence.ToString());
                sbKeyWord.Append(_dividerChar.ToString());
                sbKeyWord.Append(Keyword);
                return sbKeyWord.ToString();
            }

        }
        public bool AssignElements(string key)
        {
            int batchSeq = -1;
            bool bRetval =true;
            string[] keyArray;
            keyArray = key.Split(_dividerChar);
            if (keyArray.Length == 2)
            {
                if (Int32.TryParse(keyArray[0], out batchSeq))
                {
                    BatchSequence = batchSeq;
                    Keyword = keyArray[1];
                }
                else
                {
                    bRetval= false;
                }
            }
            return bRetval;
        }
    }
}
