using iTextSharp.text;

namespace WFS.RecHub.OLFImageAPI
{
    public class SingleImageLayout : IImageLayout
    {
        public SingleImageLayout(RectangleReadOnly contentArea)
        {
            ContentArea = contentArea;
        }

        private RectangleReadOnly ContentArea { get; set; }

        public void AddImage(float imageHeight)
        {
            // Nothing to do
        }
        public RectangleReadOnly GetImageRectangle()
        {
            return ContentArea;
        }
    }
}