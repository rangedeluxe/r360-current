﻿using System;
using System.IO;
using System.Xml;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     06/14/2005
*
* Purpose:  
*
* Modification History
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 166933 SAS 09/19/2014
*   Change done for ImageRPS Report
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

	/// <summary>
	/// Summary description for cJobRequestViewSpecified.
	/// </summary>
	internal class cJobRequestReport : _JobRequest, _IJobRequest {

		public cJobRequestReport() {
		}

        public void processJob() {
            string completedFilePath = string.Empty; 
            XmlNode nodeReport;
            string sReportName =string.Empty;
            long lngLOF  =0;
            XmlAttribute attribute;
            try{
                jobRequestEventArgs ea = new jobRequestEventArgs();
                changeStatus(OLFJobStatus.Processing, ea);
                XmlNode nodeReports=nodeJobRequest().SelectSingleNode("Reports");
                completedFilePath = serverOptions.onlineArchivePath
                                        + this.jobID
                                        + "." + FILE_EXT_PDF;
                if(!(nodeReports==null) && (nodeReports.ChildNodes.Count>0)) {
                    for(int reportIndex=0;reportIndex<nodeReports.ChildNodes.Count;++reportIndex) {
                        nodeReport=nodeReports.ChildNodes[reportIndex];
                        attribute = nodeReport.Attributes["ReportName"];
                        if (attribute != null)
                        {
                            sReportName = attribute.Value;
                        }
                        switch (sReportName){
                            case "ItemReport":
                                ImageRpsItemReport ItemReport = new ImageRpsItemReport(this.siteKey,this.sessionID);
                                ItemReport.RunReport(nodeReport, completedFilePath);
                                break;
                            case "TransactionReport":
                                ImageRpsTransactionReport TransactionReport = new ImageRpsTransactionReport(this.siteKey, this.sessionID);
                                TransactionReport.RunReport(nodeReport, completedFilePath);
                                break;
                        }
                        eventLog.logEvent("PDF Document created : " + completedFilePath
                                            , this.GetType().Name
                                            , MessageImportance.Essential);
                        eventLog.logEvent("Completed Processing Job : " + this.jobID
                                            , this.GetType().Name
                                            , MessageImportance.Essential);
                    }
                    lngLOF = ipoLib.LOF(completedFilePath);
                    ea.fileSize = lngLOF;
                    changeStatus(OLFJobStatus.Completed, ea);
                }
                else
                {
                    eventLog.logEvent("Could not find item in Reports element" 
                                            , this.GetType().Name
                                            , MessageImportance.Essential);
                    changeStatus(OLFJobStatus.Error, ea);
                }
                
                /*if((reports != null) && (reports.Length > 0)) {
                    for(int i=0;i<reports.Length;++i) {
                        LogActivity(ActivityCodes.acViewSingle, this.sessionID, this.jobID, 1, reports[i].BankID, reports[i].LockboxID);
                    }
                }*/
              
            }catch(Exception e) {
                if (File.Exists(completedFilePath)){
                     File.Delete(completedFilePath); 
                }
                eventLog.logError(e);
            }
        }
    }
}
