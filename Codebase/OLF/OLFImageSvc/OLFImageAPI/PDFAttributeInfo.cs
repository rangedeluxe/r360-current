﻿using iTextSharp.text;
using iTextSharp.text.pdf;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     03/06/2012
*
* Purpose:
*
* Modification History
* CR 50733 JMC 03/06/2012
*    - Change Font objects to be declared as BaseFont.  This was to conform
*      to the newer version of iTextSharp (5.2.0)
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI
{
    class PDFAttributeInfo
    {
        public PDFAttributeInfo()
        {
            BF = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            BFHeading = BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            FontBold12 = FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD, new BaseColor(0, 0, 0));
            FontBold10 = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD, new BaseColor(0, 0, 0));
            FontUnderlineBold14 = FontFactory.GetFont(FontFactory.HELVETICA, 14, Font.BOLD | Font.UNDERLINE, new BaseColor(0, 0, 0));

            Font10 = FontFactory.GetFont(FontFactory.HELVETICA, 10, new BaseColor(0, 0, 0));
            Font9 = FontFactory.GetFont(FontFactory.HELVETICA, 9);
            FontSymbol = FontFactory.GetFont(FontFactory.ZAPFDINGBATS, 6);
            Font4 = FontFactory.GetFont(FontFactory.HELVETICA, 4);
        }

        /// <summary>
        /// Distance from the top of the page to where the "Item Fields:" and
        /// "Mark Sense Fields:" section starts.
        /// </summary>
        public int ItemFieldsDistanceFromTop { get; set; }
        /// <summary>
        /// Distance from the top of the page to where the "Item Information:" and
        /// "Pass I Information:" section starts.
        /// </summary>
        public int ItemInformationDistanceFromTop { get; set; }
        /// <summary>
        /// Distance from the top of the page to where the next piece of content should begin.
        /// Should generally be used as "<see cref="Bottom"/> + <see cref="CurrentDistanceFromBottom"/>".
        /// </summary>
        public int CurrentDistanceFromBottom { get; set; }
        /// <summary>
        /// Distance from the top of the page to where the next piece of content should begin.
        /// Should generally be used as "<see cref="Top"/> - <see cref="CurrentDistanceFromTop"/>".
        /// </summary>
        public int CurrentDistanceFromTop { get; set; }
        public int PageNo { get; set; }
        /// <summary>
        /// Y-coordinate of the top of the page.
        /// This is in iTextSharp backwards coordinates (Y=0 at the bottom of the page, positive=up).
        /// </summary>
        public float Top { get; set; }
        /// <summary>
        /// Y-coordinate of the bottom of the page.
        /// This is in iTextSharp backwards coordinates (Y=0 at the bottom of the page, positive=up).
        /// </summary>
        public float Bottom { get; set; }
        public float Left { get; set; }
        public float RightColumn { get; set; }
        public float Right { get; set; }
        public float Center { get; set; }
        public BaseFont BF { get; set; }
        public BaseFont BFHeading { get; set; }
        public Font FontUnderlineBold14 { get; set; }
        public Font FontBold12 { get; set; }
        public Font FontBold10 { get; set; }
        public Font Font10 { get; set; }
        public Font Font9 { get; set; }
        public Font FontSymbol { get; set; }
        public Font Font4 { get; set; }
    }
}
