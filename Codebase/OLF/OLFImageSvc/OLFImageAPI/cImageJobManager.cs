using System;
using System.Collections;
using System.IO;
using System.Threading;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.Common.Log;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     05/15/2003
*
* Purpose:  IntegraPAY Online Services Image Job Manager.
*
* Modification History
*   05/15/2003 JMC
*       -Created class
*   11/20/2003 JMC
*       -First major enhancement : 
*         1.)Added Strong-Naming to the assembly.
*         2.)Changed the namespace of the class to narrow the scope.
*         3.)Modified Comments to utilize C# Xml commenting.
*         4.)Job Manager is now the sole entry-point into the application.  Class
*            now contains hooks for createImagePdf();.
*         5.)Moved "ProcessDatFile" function to "processXMLFileJobs".  Module
*            now process Xml files rather than flat-text files.
*         6.)Module now manages job status in the database.  Module is notified
*            by job threads by the IJobRequest_OnJobStatusChanged event handler.
*         7.)Since multiple jobs can be encapsulated within a single file, this
*            module is now responsible for deleting the instruction Xml file.
*            It keeps track of all currently processing threads in a private 
*            collection, and will delete the instruction file once all threads
*            have completed.
*   03/22/2004 CR 7074 JMC
*       - Added "Page" parameter to createImagePDF functions.  Added "Page" parameter to call
*         to "createImagePDF" functions.
*   06/01/2004 CR 7728 JMC
*       - Added "View Specified" constants.
*       - Added "View Specified" to driver routine
*       - Added additional comments to function headers.
*       - Removed old commented code.
*   01/18/2005 CR 11337 JMC
*       - Added ImageFilterOption to calls to createImagePDF functions.
*   03/03/2005 CR 10537 JMC
*       - Added XML_ATTR_SESSION_ID constant
*       - Modified processXMLFileJobs method to extract SessionID from the 
*         request Xml.
*   06/14/2005 CR 11465 JMC
*	    - Modified class to use the server options MaxThreads value rather than the
*         hardcoded value of 10.
*   11/02/2005 CR 14209 JMC
*       - Added code to assign SetDeadlockPriority property to database object
*         after creation in IJobRequest_OnJobStatusChanged method.
*   11/04/2005 CR 14211 JMC
*       - Added code to assign QueryRetryAttempts property to database object
*         after creation in IJobRequest_OnJobStatusChanged method.
*   05/20/2009 CR 23584 MEH
*       - integraPAY Online Interface Into Hyland IMS Archive
*   03/22/2011 CR 33495 WJS
*      - Lock job Status from submitting only request at a time
*   10/05/2011 CR 33196 JNE
*       - Modified IJobRequest_OnJobStatusChanged to update database based 
*          on sitekey passed in with image request.
*   08/08/2012 CR 54169 JNE
*       - createImagePDF - Add BatchNumber & DisplayBatchID    
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 121672 MLH 11/26/2013
*   Added OLClientAccount DisplayLabel formatted with DDA  See WI 121666 Attachment
*   for v2.0 limitations.
*   Removed USE_ITEMPROC code
* WI 143274 SAS 05/21/2014
*   Changes done to change the datatype of BatchID from int to long
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

    /// <summary>
    ///  Description: ipo Image Service Job Manager object.  Public entry point 
    ///  for the application.
    /// </summary>
	public class cImageJobManager
	{
        private cImageServiceOptions _ServerOptions = null;
        private cEventLog _EventLog =null;
        private Thread[] _ProcessThreads;

        /// <summary>
        /// Collection of files currently being processed.
        /// </summary>
        private SortedList _FilesInProcess = new SortedList();

		#region Private Constants

        //***** Xml document root
        private const string XML_ELE_SERVICE_REQUEST_ROOT   = "ipoServiceRequest";

        //***** Job Request Parms
        private const string XML_ELE_JOB_REQUEST_ROOT       = "jobRequest";
        private const string XML_ATTR_JOB_REQUEST_TYPE      = "jobRequestType";

        //***** Search Output Global Batch Root Node
        private const string XML_ELE_GLOBAL_BATCH           = "globalBatch";

        //***** Search Output Transaction Root Node
        private const string XML_ELE_TRANSACTION            = "transaction";
    
        //***** Job Request Parms
        private const string XML_ATTR_JOB_ID                = "jobID";
        private const string XML_ATTR_SESSION_ID            = "sessionID";
        private const string XML_ATTR_SITE_KEY              = "siteKey";

        private const string PROCESS_FILE_EXT_XML           = "xml";
        private const string PROCESS_FILE_EXT_KLG           = "klg";
        private const string PROCESS_FILE_EXT_ZIP           = "zip";
        private const string PROCESS_FILE_EXT_PDF           = "pdf";

        private const string JOB_REQ_TRANSACTION            = "TRANSACTION";
        private const string JOB_REQ_BATCH                  = "BATCH";
        private const string JOB_REQ_SEARCH_RESULTS         = "SEARCH_RESULTS";
        private const string JOB_REQ_VIEW_SPECIFIED         = "VIEW_SPECIFIED";
        private const string JOB_REQ_IN_PROCESS_EXCEPTION   = "IN_PROCESS_EXCEPTION";

        private const string JOB_REQ_REPORT                 = "REPORT";

		#endregion

        /// <summary>
        /// Structure containing details about an input job request file.
        /// </summary>
        private struct FileInProcess
        {
            public string fileName;
            public int jobQuantity;
            public int jobsCompleted;
        }

        /// <summary>
        /// Public constructor.
        /// </summary>
		public cImageJobManager() 
        {
            _ProcessThreads = new Thread[serverOptions.MaxThreads];
		}


        /// <summary>
        /// Object that contains Image Service options from the local .ini file.
        /// </summary>
        private cImageServiceOptions serverOptions
        {
            get
            {
                if (_ServerOptions == null) { 
                    _ServerOptions = new cImageServiceOptions(ipoLib.INISECTION_IMAGE_SERVICE);
                }

                return _ServerOptions;
            }
        }

        /// <summary>
        /// EventLog object used to write processing information as the 
        /// application runs.
        /// </summary>
        private cEventLog eventLog
        {
            get
            {
                if (_EventLog == null) { 
                    _EventLog = new cEventLog(serverOptions.logFilePath
                                                , serverOptions.logFileMaxSize
                                                , (MessageImportance) serverOptions.loggingDepth); 
                }

                return _EventLog;
            }
        }

        /// <summary>
        /// Utility helper function that moves a file from one location to 
        /// another.  Function will attempt 10 times before giving up.
        /// </summary>
        /// <param name="fileName">Name of file to be processed.</param>
        /// <param name="newFileName"></param>
        /// <author name="Joel Caples"></author>
        private void moveFile(string fileName
                            , string newFileName)
        {
            bool bolFileMoved=false;
            int intTries = 10;

            while(!(bolFileMoved))
            {
                try
                {
                    if (File.Exists(newFileName))
                        { File.Delete(newFileName); }

                    File.Move(fileName, newFileName);

                    bolFileMoved= true;
                }
                catch
                {
                    //If the file is still owned by the process that is moving it to the directory, 
                    // this function will have to wait in order to process it.
                    intTries--;
                    if (intTries <1)
                        { break; }
                    else
                    {
                        eventLog.logEvent("Cannot access file.  Sleeping 1/2 second before attempting again.", this.GetType().Name, MessageImportance.Verbose);
                        System.Threading.Thread.Sleep(500); 
                    }
                }
            }
        }

        /// <summary>
        /// Searches onlineArchive directory for any new Xml files, and processes 
        /// each according to Xml instructions.
        /// </summary>
        public void processArchives()
        {
            bool bolStillProcessing=true;

            try
            {
                eventLog.logEvent("Processing archives in directory '" 
                                + serverOptions.onlineArchivePath + "'."
                                , this.GetType().Name
                                , MessageImportance.Essential);

                while(bolStillProcessing)
                {                   
                    foreach(string strFile in System.IO.Directory.GetFiles(serverOptions.onlineArchivePath
                                                                         , "*." + PROCESS_FILE_EXT_XML)) 
                    {
                        if(File.Exists(strFile))
                            { processXMLFile(strFile); }
                    }
                    waitForThreadsToProcess(ref bolStillProcessing);
                }   // while

                eventLog.logEvent("Done processing archives.", this.GetType().Name, MessageImportance.Essential);
            }
            catch(Exception e)
            {
                eventLog.logEvent("Error processing archives : " + e.Message
                                , e.Source
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }


        /// <summary>
        /// Prepares a job file for processing.
        /// </summary>
        private void processXMLFile(string datFile)
        {
            string strNewFileName;

            try {

                if(datFile.ToLower().EndsWith("." + PROCESS_FILE_EXT_XML.ToLower()))
                    { strNewFileName=datFile.Remove(datFile.Length-3, 3) + PROCESS_FILE_EXT_KLG; }
                else
                    { strNewFileName=datFile+"." + PROCESS_FILE_EXT_KLG; }

                if (File.Exists(datFile))
                {
                    eventLog.logEvent("Processing job file : " + datFile
                                    , this.GetType().Name
                                    , MessageImportance.Essential);

                    moveFile(datFile
                        , strNewFileName);

                        processXMLFileJobs(strNewFileName);

                    eventLog.logEvent("Job file submitted: " + datFile
                                    , this.GetType().Name
                                    , MessageImportance.Essential);
                }
            }
            catch(Exception e1)
            {
                try {
                    if(File.Exists(datFile))
                    {
                        File.Delete(datFile);
                    }
                }
                catch(Exception e2) {
                    eventLog.logError(e2);
                }

                throw;
            }
        }


        /// <summary>
        /// Opens a job file, and processes each job it contains.
        /// </summary>
        /// <param name="xmlFileName">File name to be processed.</param>
        private void processXMLFileJobs(string xmlFileName)
        {
            _IJobRequest objImageCreator=null;
            string strSiteKey="";
            string strJobRequestType="";
            Guid gidJobID = Guid.Empty;
            Guid gidSessionID;
            Guid gidTemp;

            XmlDocument doc=null;

            try {
                doc = new XmlDocument();
                doc.Load(xmlFileName);
            }
            catch(Exception e) {
                eventLog.logEvent("Cannot load Xml document: " + e.Message
                                , this.GetType().Name
                                , MessageType.Error
                                , MessageImportance.Essential);
                return;
            }

            System.Xml.XmlNodeList nlJobs = doc.SelectNodes(XML_ELE_SERVICE_REQUEST_ROOT
                                                          + "/" 
                                                          + XML_ELE_JOB_REQUEST_ROOT);

            if(nlJobs.Count > 0) {

                FileInProcess sctFileInProcess = new FileInProcess();

                sctFileInProcess.fileName = xmlFileName;
                sctFileInProcess.jobQuantity = nlJobs.Count;
                sctFileInProcess.jobsCompleted = 0;
                _FilesInProcess.Add(xmlFileName, sctFileInProcess);

                foreach(XmlNode nodeJobRequest in nlJobs) {

                    try {
                    
                        if(ipoLib.TryParse(nodeJobRequest.Attributes[XML_ATTR_JOB_ID].Value, out gidTemp)) {
                            gidJobID = gidTemp;
                        } else {
                            gidJobID = Guid.Empty;
                            throw(new Exception("Invalid Job ID: " + nodeJobRequest.Attributes[XML_ATTR_JOB_ID].Value));
                        }
                    
                        strSiteKey = nodeJobRequest.Attributes[XML_ATTR_SITE_KEY].Value;
                        strJobRequestType = nodeJobRequest.Attributes[XML_ATTR_JOB_REQUEST_TYPE].Value;
                        gidSessionID = new Guid(nodeJobRequest.Attributes[XML_ATTR_SESSION_ID].Value);
                    }
                    catch{
                        throw(new Exception("Required Job request attributes were not readable." 
                                          + "  Required attributes are " + XML_ATTR_JOB_ID + ", " 
                                          + XML_ATTR_SITE_KEY + ", " + XML_ATTR_JOB_REQUEST_TYPE
                                          + " and " 
                                          + XML_ATTR_SESSION_ID + "."));
                    }

                    switch(strJobRequestType) {
                        case JOB_REQ_TRANSACTION : 
                            objImageCreator = new cJobRequestViewAll();
                            break;
                        case JOB_REQ_BATCH : 
                            objImageCreator = new cJobRequestViewAll();
                            break;
                        case JOB_REQ_SEARCH_RESULTS :
                            objImageCreator = new cJobRequestSearchResults();
                            break;
                        case JOB_REQ_VIEW_SPECIFIED :
                            objImageCreator = new cJobRequestViewSpecified();
                            break;
                        case JOB_REQ_REPORT :
                            objImageCreator = new cJobRequestReport();
                            break;
                        case JOB_REQ_IN_PROCESS_EXCEPTION:
                            objImageCreator = new CJobRequestInProcessException();
                            break;
                        default:
                            break;
                    }

                    objImageCreator.fileName = xmlFileName;
                    objImageCreator.jobID = gidJobID;
                    objImageCreator.siteKey = strSiteKey;
                    objImageCreator.XMLDoc = xmlFileName;
                    objImageCreator.sessionID = gidSessionID;
                    objImageCreator.jobStatusChangedEvent += new jobStatusChangedEventHandler(IJobRequest_OnJobStatusChanged);

                    submitJob(objImageCreator);

                }   //for
            }   //if
            else {
                // Delete the file if it contains no jobs.
                if(File.Exists(xmlFileName)){
                    File.Delete(xmlFileName); 
                }           
            }
        }   //function

        /// <summary>
        /// Event handler that updates the database with the current job status
        /// for the reporting job.
        /// </summary>
        private void IJobRequest_OnJobStatusChanged(Object sender
                                                  , _JobRequest.jobRequestEventArgs e)
        {
            _IJobRequest objJobRequest = (_IJobRequest)sender;

            cSiteOptions objSiteOptions = new cSiteOptions(objJobRequest.siteKey);
            cItemProcDAL objItemProcDAL = new cItemProcDAL(objJobRequest.siteKey);//CR 33196 JNE 10/5/2011
            //CR 33495 WJS 3/22/2011
            lock (this)
            {
                switch (objJobRequest.status)
                {
                    case (OLFJobStatus.Processing):
                        {
                            objItemProcDAL.UpdateJobStatus(objJobRequest.jobID,
                                                        objJobRequest.status,
                                                        0);

                            eventLog.logEvent("Processing Job : " + objJobRequest.jobID
                                            , this.GetType().Name
                                            , MessageImportance.Essential);
                            break;
                        }
                    case (OLFJobStatus.Completed):
                        {

                            objItemProcDAL.UpdateJobStatus(objJobRequest.jobID,
                                                        objJobRequest.status,
                                                        e.fileSize);
                            eventLog.logEvent("Job completed successfully : " + objJobRequest.jobID
                                            , this.GetType().Name
                                            , MessageImportance.Essential);
                            incrementFileJobs(objJobRequest.fileName);
                            break;
                        }
                    case (OLFJobStatus.Error):
                        {

                            objItemProcDAL.UpdateJobStatusError(objJobRequest.jobID, e.Message);

                            eventLog.logEvent("Job failed due to errors : " + objJobRequest.jobID
                                            , this.GetType().Name
                                            , MessageImportance.Essential);
                            eventLog.logEvent(e.Message, e.src, MessageType.Error
                                            , MessageImportance.Essential);
                            incrementFileJobs(objJobRequest.fileName);
                            break;
                        }
                }
            }
        }


        /// <summary>
        /// An input job file can contain multiple jobs.  This function determines
        /// whether all jobs from a given job file have completed.  If all jobs
        /// have finished, this function will delete the file itself.
        /// </summary>
        /// <param name="fileName">
        /// name of file being processed, also used as the key
        /// of the collection
        /// </param>
        private void incrementFileJobs(string fileName)
        {
            FileInProcess sctFileInProcess = (FileInProcess) _FilesInProcess[fileName];

            ++sctFileInProcess.jobsCompleted;

            _FilesInProcess[fileName] = sctFileInProcess;

            if((sctFileInProcess.jobsCompleted == sctFileInProcess.jobQuantity) &&
            (File.Exists(sctFileInProcess.fileName)))
                { File.Delete(sctFileInProcess.fileName); }           
        }

        /// <summary>
        /// Creates a thread and adds it to the local collection of processing 
        /// jobs.
        /// </summary>
        /// <param name="jobRequest">
        /// Abstracted job request object to be added to 
        /// the thread queue.
        /// </param>
        private void submitJob(_IJobRequest jobRequest)
        {
            bool bolSpotFound=false;

            while(!(bolSpotFound))
            {
                for(int i=0;i<serverOptions.MaxThreads;++i)
                {
                    if (_ProcessThreads[i]==null)
                    {
                        bolSpotFound=true;
                    } // if
                    else if (!(_ProcessThreads[i].IsAlive))
                    {
                        _ProcessThreads[i]=null;
                        bolSpotFound=true; 
                    } // if

                    if (bolSpotFound)
                    {
                        eventLog.logEvent("Submitting XML job : " + jobRequest.fileName + " [" + i.ToString() + "]"
                                        , this.GetType().Name
                                        , MessageImportance.Essential);

                        _ProcessThreads[i]=new Thread(new ThreadStart(jobRequest.processJob));
                        _ProcessThreads[i].IsBackground=true;
                        _ProcessThreads[i].Start();

                        break;
                    } // if
                } // for
            } // while
        }


        /// <summary>
        /// Determines if any thread is still processing.  If any jobs
        /// are outstanding then sleep 1/2 second and exit the 
        /// function.
        /// </summary>
        /// <param name="bolStillProcessing">
        /// Out variable that answers the question of whether or not there are any 
        /// jobs currently processing.
        /// </param>
        private void waitForThreadsToProcess(ref bool bolStillProcessing)
        {
            for(int i=0;i<serverOptions.MaxThreads;++i)
            { 
                if (_ProcessThreads[i] == null)
                    { if (i+1==serverOptions.MaxThreads) { bolStillProcessing=false; } }
                else
                {
                    if (_ProcessThreads[i].IsAlive)
                    {
                        System.Threading.Thread.Sleep(500);
                        break;
                    }
                    else
                    {
                        _ProcessThreads[i].Join();
                        _ProcessThreads[i] = null;

                        eventLog.logEvent("Thread " + " [" + i.ToString() + "] was terminated."
                                        , this.GetType().Name
                                        , MessageImportance.Verbose);

                        if (i+1==serverOptions.MaxThreads) { bolStillProcessing=false; }
                    }
                }   //if

            }   // for

        }   // waitForThreadsToProcess

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SiteKey"></param>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="GlobalBatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchID"></param>
        /// <param name="BatchNumber"></param>
        /// <param name="DisplayScannedCheck"></param>
        /// <param name="DisplayBatchID"></param>
        /// <param name="DisplayRemitterNameInPDF"></param>
        /// <param name="CheckImageDisplayMode"></param>
        /// <param name="DocumentImageDisplayMode"></param>
        /// <param name="lbColorMode"></param>
        public void createImagePDF(string SiteKey,
                                   int BankID, 
                                   int LockboxID, 
                                   int GlobalBatchID, 
                                   DateTime DepositDate, 
                                   long BatchID,
                                   int BatchNumber, 
                                   bool DisplayScannedCheck,
                                   bool DisplayBatchID, 
                                   bool DisplayRemitterNameInPDF, 
                                   int CheckImageDisplayMode, 
                                   int DocumentImageDisplayMode,
                                   // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                                   WorkgroupColorMode lbColorMode) {

            cJobRequestViewAll objViewAll = new cJobRequestViewAll();
            objViewAll.siteKey = SiteKey;
            objViewAll.createImagePDF(BankID, 
                                      LockboxID, 
                                      DepositDate, 
                                      BatchID,
                                      BatchNumber, 
                                      DisplayScannedCheck,
                                      DisplayBatchID, 
                                      OLFImageFilterOption.All, 
                                      DisplayRemitterNameInPDF, 
                                      (OLFImageDisplayMode)CheckImageDisplayMode, 
                                      (OLFImageDisplayMode)DocumentImageDisplayMode,
                                      // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                                      lbColorMode);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="SiteKey"></param>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="GlobalBatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchID"></param>
        /// <param name="BatchNumber"></param>
        /// <param name="TransactionID"></param>
        /// <param name="TxnSequence"></param>
        /// <param name="DisplayScannedCheck"></param>
        /// <param name="DisplayBatchID"></param>
        /// <param name="DisplayRemitterNameInPDF"></param>
        /// <param name="CheckImageDisplayMode"></param>
        /// <param name="DocumentImageDisplayMode"></param>
        /// <param name="lbColorMode"></param>
        public void createImagePDF(string SiteKey,
                                   int BankID, 
                                   int LockboxID, 
                                   int GlobalBatchID, 
                                   DateTime DepositDate, 
                                   long BatchID,
                                   int BatchNumber, 
                                   int TransactionID, 
                                   int TxnSequence,
                                   bool DisplayScannedCheck,
                                   bool DisplayBatchID, 
                                   bool DisplayRemitterNameInPDF, 
                                   int CheckImageDisplayMode, 
                                   int DocumentImageDisplayMode,
                                   // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                                   WorkgroupColorMode lbColorMode)
        {
            cJobRequestViewAll objViewAll = new cJobRequestViewAll();
            objViewAll.siteKey = SiteKey;
            objViewAll.createImagePDF(BankID,
                                      LockboxID,
                                      DepositDate,
                                      BatchID,
                                      BatchNumber,
                                      TransactionID,
                                      TxnSequence, 
                                      DisplayScannedCheck,
                                      DisplayBatchID, 
                                      OLFImageFilterOption.All, 
                                      DisplayRemitterNameInPDF, 
                                      (OLFImageDisplayMode)CheckImageDisplayMode, 
                                      (OLFImageDisplayMode)DocumentImageDisplayMode,
                                      // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                                      lbColorMode);
        }

    }
}
