using System;
using System.Data;
using System.IO;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using WFS.RecHub.Common;
using System.Collections.Generic;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Author
* Date:     1/14/2009
*
* Purpose:  IntegraPAY Online Image Service Search Results PDF class.
*
* Modification History
*   01/14/2009 CR 99999 ABC 
*       - Initial release.
*   01/14/2009 CR 99999 XXX
*       - Initial release.
*   03/17/2004 CR 7017 JMC
*       - Created class to encapsulate Search Results Pdf output functionality.
*   12/08/2004 CR 10789 JMC
*       - Added check for datatypeenum when appending rows to the Pdf table.  Currency fields
*         will now be formatted with dollar signs and 2 decimal places.  Currency fields will
*         NOT be right justified due to limitations with the 3d party Pdf builder component.
* CR 50734 JMC 03/02/2012
*   -Refactored class to use iTextSharp instead of ABCPdf.net
* CR 52276 JNE 07/18/2012
*   - Added datatable parameter to outputPdfAsTable used when calling writeData.
*   - Modified writeData method to use datatable instead of node list for rows.
* WI 85208 JMC 01/10/2013
*   -Added Column Name Constant.
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 122110 MLH 11/22/2013
*   Fixed DisplayFields not populating downloaded files from Account Search.
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

    internal static class SearchResultsPdfSupport {

        private const string BASE_FONT = BaseFont.TIMES_ROMAN;
        private const string BASE_FONT_ENCODING = BaseFont.CP1252;
        private const float BASE_FONT_SIZE = 8;

        private const int SHADE_COLOR_R = 200;
        private const int SHADE_COLOR_G = 200;
        private const int SHADE_COLOR_B = 220;

        private const int NOSHADE_COLOR_R = 255;
        private const int NOSHADE_COLOR_G = 255;
        private const int NOSHADE_COLOR_B = 255;

        public const int HORIZONTAL_MARGIN_WIDTH = 5;
        public const int VERTICAL_MARGIN_HEIGHT = 50;

        public const float PHRASE_SPACING = 10;

        public static BaseColor ShadedColor {
            get {
                return(new BaseColor(SHADE_COLOR_R, SHADE_COLOR_G, SHADE_COLOR_B));
            }
        }

        public static BaseColor UnShadedColor {
            get {
                return(new BaseColor(NOSHADE_COLOR_R, NOSHADE_COLOR_G, NOSHADE_COLOR_B));
            }
        }

        public static Font ReportFont {
            get {
                BaseFont objBaseFont = BaseFont.CreateFont(SearchResultsPdfSupport.BASE_FONT, 
                                                           SearchResultsPdfSupport.BASE_FONT_ENCODING, false);
                Font objFont = new Font(objBaseFont, 
                                        SearchResultsPdfSupport.BASE_FONT_SIZE, Font.NORMAL);
                return(objFont);
            }
        }
    }

    /// <author>Joel Caples</author>
    /// <summary>
    /// 
    /// </summary>
    internal class cSearchResultsPdf : IDisposable 
    {
        // Track whether Dispose has been called.
        private bool disposed = false;

        private const string XML_ELE_OPTIONAL_COLUMN = "displayColumn";
        private const string XML_ATTR_FIELD_NAME = "fieldName";
        private const string XML_ATTR_COLUMN_NAME = "ColumnName";
        private const string XML_ATTR_DATA_TYPE_ENUM = "dataTypeEnum";
        private const string XML_ATTR_DISPLAY_NAME = "displayName";

        private const string XSL_SEARCH_OUTPUT_PP = "ipo_search_output_pp.xsl";

        private const string XML_ATTR_DISPLAY_TITLE = "displayTitle";
        private const string XML_ATTR_DISPLAY_TEXT = "displayText";

        private Document _iTextSharpDoc;
        private PdfPTable _PDFTable;

        private bool _Shade=false;


        /// <summary>
        /// Event handler to output informational text.
        /// </summary>
        public event outputMessageEventHandler outputMessage;

        /// <summary>
        /// EventHandler to output error information.
        /// </summary>
        public event outputErrorEventHandler outputError;


        /// <summary>
        /// 
        /// </summary>
        public cSearchResultsPdf() {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void onOutputError(System.Exception e) {
            if (outputError != null) {
                outputError(e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        private void onOutputMessage(string message
                                   , string src
                                   , MessageType messageType
                                   , MessageImportance messageImportance) {

            if (outputMessage != null) {
                outputMessage(message, 
                              src, 
                              messageType, 
                              messageImportance);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="docXml"></param>
        /// <param name="dt"></param>
        /// <param name="fileName"></param>
        public void outputPdfAsTable(ref XmlDocument docXml, ref DataTable dt, string fileName) {

            System.Xml.XmlNodeList nlCols;
            System.Xml.XmlNodeList nlSearchCriteria;

            nlCols = docXml.SelectNodes("/searchXml/Recordset[@Name='Columns']/Record");
            nlSearchCriteria = docXml.SelectNodes("/searchXml/Recordset[@Name='searchCriteria']/Record");            

            PdfWriter objPdfWriter = null;
            MyPageEventHandler e;

            try {

                // Create the Pdf
                _iTextSharpDoc = new Document(new Rectangle(PageSize.LETTER.Height, 
                                                            PageSize.LETTER.Width), 
                                              SearchResultsPdfSupport.HORIZONTAL_MARGIN_WIDTH, 
                                              SearchResultsPdfSupport.HORIZONTAL_MARGIN_WIDTH, 
                                              SearchResultsPdfSupport.VERTICAL_MARGIN_HEIGHT, 
                                              SearchResultsPdfSupport.VERTICAL_MARGIN_HEIGHT);

                // Create a new PdfWriter object, specifying the output stream
                using(FileStream objFS = new FileStream(fileName, FileMode.Create)) {

                    objPdfWriter = PdfWriter.GetInstance(_iTextSharpDoc, objFS);

                    // instantiate the custom PdfPageEventHelper
                    e = new MyPageEventHandler();

                    e.nlCols = nlCols;

                    // and add it to the PdfWriter
                    objPdfWriter.PageEvent = e;
                    createTable(fileName, nlCols);
                    writeData(ref dt, ref nlCols);
                    writeTrailer(ref nlCols, ref nlSearchCriteria);
                    writePdf(fileName);
                }

            } catch(Exception ex) {
                onOutputError(ex);
            } finally {
                if(objPdfWriter != null) {
                    objPdfWriter.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Cols"></param>
        private void createTable(string FileName, XmlNodeList Cols) {

            try {

                // Open the Document for writing
                _iTextSharpDoc.Open();

                _PDFTable = new PdfPTable(Cols.Count);
                _PDFTable.TotalWidth = _iTextSharpDoc.PageSize.Width-(SearchResultsPdfSupport.HORIZONTAL_MARGIN_WIDTH*2);
                _PDFTable.LockedWidth = true;
                _PDFTable.DefaultCell.BorderWidth = 0;

                System.Collections.Generic.List<float> arWidths = new System.Collections.Generic.List<float>();
                foreach(XmlNode node in Cols) {
                    arWidths.Add(50);
                }
                _PDFTable.SetWidths(arWidths.ToArray());

                ////// ensure columns are padded
                ////_PDFTable.Padding = 5;

            } catch(Exception ex) {
                onOutputError(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="nlCols"></param>
        private void writeData(ref DataTable dt, 
                               ref XmlNodeList nlCols) {

            string strFieldName;
            string strFieldValue;
            string strDataTypeEnum;
            List<string> unavailableColumns = new List<string>();

            XmlNode nodeCol;

            // Append Headers & Data
            formatRow();
            DataColumnCollection dtColumns = dt.Columns;
            foreach (DataRow dr in dt.Rows){
                for (int k = 0; k < nlCols.Count; k++) {
                    nodeCol = nlCols.Item(k);
                    strFieldName = nodeCol.Attributes["ColumnName"].Value;
                    strDataTypeEnum = nodeCol.Attributes[XML_ATTR_DATA_TYPE_ENUM].Value;
                    if ( dt.Columns.Contains(strFieldName) ) {
                        strFieldValue = dr[strFieldName].ToString();
                    }
                    else if (strFieldName.Contains("."))
                    {
                        strFieldValue = dr[strFieldName.Split('.')[1]].ToString();
                    }
                    else {
                        strFieldValue = string.Empty;
                        logUnavailableColumns(unavailableColumns, strFieldName, "cSearchResultsPdf.writeData");
                    }
                    if(_Shade) {
                        _PDFTable.DefaultCell.BackgroundColor = SearchResultsPdfSupport.ShadedColor;
                    } else {
                        _PDFTable.DefaultCell.BackgroundColor = SearchResultsPdfSupport.UnShadedColor;
                    }

                    if(strFieldValue.Length>0) {
                        switch(strDataTypeEnum) {
                            case "7":
                                _PDFTable.AddCell(new Phrase("$ " + Convert.ToDecimal(strFieldValue).ToString("###,###,###,###.00"), SearchResultsPdfSupport.ReportFont));
                                break;
                            default:
                                _PDFTable.AddCell(new Phrase(strFieldValue, SearchResultsPdfSupport.ReportFont));
                                break;
                        }
                    } else {
                        _PDFTable.AddCell(new Phrase(string.Empty, SearchResultsPdfSupport.ReportFont));
                    }
                } // for  (columns)
                formatRow();
            } //for each
   
            _iTextSharpDoc.Add(_PDFTable);
        }

        private void logUnavailableColumns(List<string> unavailableColumnList, string currentColumn, string Source) {

            if (!unavailableColumnList.Contains(currentColumn)) {
                unavailableColumnList.Add(currentColumn);
                onOutputMessage("Requested output column not contained in search results : " + currentColumn, Source, MessageType.Warning, MessageImportance.Debug);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nlCols"></param>
        /// <param name="nlSearchCriteria"></param>
        private void writeTrailer(ref XmlNodeList nlCols,
                                  ref XmlNodeList nlSearchCriteria) {

            string strDisplayTitle;
            string strDisplayText;

            Chunk chnkDisplayTitle;
            Chunk chnkDisplayText;
            Phrase p2;

            XmlNode nodeTemp;

            PdfPTable objTable = new PdfPTable(1);
            objTable.TotalWidth = _iTextSharpDoc.PageSize.Width-(SearchResultsPdfSupport.HORIZONTAL_MARGIN_WIDTH*2);
            objTable.LockedWidth = true;
            objTable.DefaultCell.BorderWidth = 0;
            objTable.DefaultCell.PaddingTop = 5;

            if(nlSearchCriteria.Count > 0) {

                for(int i=0;i<nlSearchCriteria.Count;++i) {

                    if(i==0) {
                        objTable.DefaultCell.BorderWidthTop = 1;
                    } else {
                        objTable.DefaultCell.BorderWidthTop = 0;
                    }

                    if(i==nlSearchCriteria.Count-1) {
                        objTable.DefaultCell.BorderWidthBottom = 1;
                    } else {
                        objTable.DefaultCell.BorderWidthBottom = 0;
                    }

                    nodeTemp = nlSearchCriteria[i];

                    try { 
                        strDisplayTitle=nodeTemp.Attributes[XML_ATTR_DISPLAY_TITLE].Value + ":";
                    } catch {
                        strDisplayTitle="";
                    }

                    try { 
                        strDisplayText=nodeTemp.Attributes[XML_ATTR_DISPLAY_TEXT].Value;
                    } catch {
                        strDisplayText="";
                    }

                    chnkDisplayTitle = new Chunk(strDisplayTitle + "  ", SearchResultsPdfSupport.ReportFont);
                    chnkDisplayText = new Chunk(strDisplayText, SearchResultsPdfSupport.ReportFont);

                    p2 = new Phrase(SearchResultsPdfSupport.PHRASE_SPACING);
                    p2.Add(chnkDisplayTitle);
                    p2.Add(chnkDisplayText);

                    objTable.AddCell(p2);
                    objTable.CompleteRow();
                }
            }

            _iTextSharpDoc.Add(objTable);
        }


        /// <summary>
        /// Formats Pdf document and Outputs file to the filesystem.
        /// </summary>
        /// <param name="fileName"></param>
        private void writePdf(string fileName) {

            onOutputMessage("Writing Pdf document : " + fileName, 
                            string.Empty, 
                            MessageType.Information, 
                            MessageImportance.Verbose);


            _iTextSharpDoc.Close();

            onOutputMessage("File written : " + fileName, 
                            string.Empty, 
                            MessageType.Information, 
                            MessageImportance.Verbose);
        }

        /// <summary>
        /// 
        /// </summary>
        private void formatRow() {
            _Shade = !_Shade;
        }

        #region Dispose
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_iTextSharpDoc != null)
                    {
                        _iTextSharpDoc.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }
        #endregion

    }


    /// <summary>
    /// We use a __single__ Image instance that's assigned __once__;
    /// the image bytes added **ONCE** to the PDF file. If you create 
    /// separate Image instances in OnEndPage()/OnEndPage(), for example,
    /// you'll end up with a much bigger file size.
    /// </summary>
    public class MyPageEventHandler : PdfPageEventHelper {

        private const string XML_ATTR_DISPLAY_NAME = "displayName";

        /// <summary>
        /// 
        /// </summary>
        public XmlNodeList nlCols;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="document"></param>
        public override void OnEndPage(PdfWriter writer, Document document) {

            string strHeaderText;
            XmlNode nodeCol;

            // cell height 
            float cellHeight = document.TopMargin;

            // PDF document size      
            Rectangle page = document.PageSize;

            // create two column table
            PdfPTable head = new PdfPTable(nlCols.Count);
            head.DefaultCell.BorderWidth = 0;
            head.DefaultCell.BorderWidthTop = 1;
            head.DefaultCell.BorderWidthBottom = 1;
            head.DefaultCell.PaddingTop = 5;
            //head.DefaultCell.BackgroundColor = SearchResultsPdfSupport.ShadedColor;
            head.TotalWidth = page.Width - (SearchResultsPdfSupport.HORIZONTAL_MARGIN_WIDTH*2);

            for(int j=0;j<nlCols.Count;++j) {
                nodeCol = nlCols.Item(j);
                strHeaderText = nodeCol.Attributes[XML_ATTR_DISPLAY_NAME].Value;
                head.AddCell(new Phrase(strHeaderText, SearchResultsPdfSupport.ReportFont));
            }

            // since the table header is implemented using a PdfPTable, we call
            // WriteSelectedRows(), which requires absolute positions!
            head.WriteSelectedRows(
                0, 
                -1,                                                 // first/last row; -1 flags all write all rows
                SearchResultsPdfSupport.HORIZONTAL_MARGIN_WIDTH,    // left offset
                // ** bottom** yPos of the table
                page.Height - cellHeight + head.TotalHeight,
                writer.DirectContent
            );
        }
    }
}
