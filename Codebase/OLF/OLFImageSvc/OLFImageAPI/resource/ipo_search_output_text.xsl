<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
    <!ENTITY copy "&#169;">
    <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>

<xsl:template match="searchResults">

    <xsl:for-each select="/searchXml/displayColumns/displayColumn">
	    <xsl:value-of select="@displayName"/>
        <xsl:choose>
	        <xsl:when test="count(/searchXml/displayColumns/displayColumn) = position()">
                <xsl:text></xsl:text>
  	        </xsl:when>
	        <xsl:otherwise>
                <xsl:text>,</xsl:text>
	        </xsl:otherwise>
        </xsl:choose>
    </xsl:for-each>

    <xsl:text>&#13;&#10;</xsl:text>

	<xsl:for-each select="dataRow">
	    <xsl:variable name="dr_curr"><xsl:value-of select="position()"/></xsl:variable>
        <xsl:for-each select="/searchXml/displayColumns/displayColumn">
			<xsl:variable name="fld"><xsl:value-of select="@fieldName"/></xsl:variable>

            <xsl:variable name="dataTypeEnum"><xsl:value-of select="@dataTypeEnum"/></xsl:variable>
            <xsl:choose>
                <xsl:when test="$dataTypeEnum='7'"><xsl:value-of select="format-number(//dataRow[position() = number($dr_curr)]/Fld[@ID=$fld], '$ #0.00')"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="//dataRow[position() = number($dr_curr)]/Fld[@ID=$fld]"/></xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
	            <xsl:when test="count(/searchXml/displayColumns/displayColumn) = position()">
                    <xsl:text></xsl:text>
  	            </xsl:when>
	            <xsl:otherwise>
                    <xsl:text>,</xsl:text>
	            </xsl:otherwise>
            </xsl:choose>
		</xsl:for-each>
        <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

</xsl:template>

</xsl:stylesheet>

