<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
    <!ENTITY copy "&#169;">
    <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>

<xsl:template match="searchResults">
	<html>
	    <head>
		    <title>Search Results</title>
            <style>
							body {font-family: arial, helvetica, sans-serif; font-size: 11px; font-weight: normal; color: #000000; background-color : #ffffff; margin : 0px }
							p { font-family :  arial, helvetica, sans-serif; font-size : 11px; font-weight : normal; color : #666666; background-color : #FFFFFF; }

							.copyright { vertical-align : middle; font-family : arial, helvetica, sans-serif; font-size: 8pt; font-weight : normal; color : #000000; text-decoration : none; }
							.copyright A { font-family : arial, helvetica, sans-serif; font-weight : normal; font-size : 8pt; color : #6c276a; text-decoration : underline; }
							.copyright A:LINK { color : #6c276a; }
							.copyright A:VISITED { color : #6c276a; }
							.copyright A:HOVER { color : #6c276a; }

                /*Content Title*/
							.contenttitle { font-family : arial, helvetica, sans-serif; font-size: 16px; font-weight : bold; color : #000000; text-decoration : none; background-color : #F3F3F3; }
							.contentsubtitle { font-family : arial, helvetica, sans-serif; font-size: 12px; font-weight : normal; font-style : normal; color : #000000; text-decoration : none; }
							.contentbreak { background-color : #333399; }

							/*Reports*/
							.reportmain { background-color : #DDDDDD; }
							.reportsub { background-color : #DDDDDD; }
							.reportcaption { font-family : arial, helvetica, sans-serif; font-size : 14px; font-weight : bold; color : #000000; text-decoration : none; text-align : left; background-color : #FFFFFF; }
							.recordcount { font-family : arial, helvetica, sans-serif; font-size: 11px; font-weight : bold; color : #A9A9A9; text-decoration : none; background-color : #FFFFFF; }

							.reportheading { background-color : #000000; font-family : arial, helvetica, sans-serif; font-size: 12px; font-weight : bold; color : #F3F3F3; text-decoration : none; }
							.reportheading A:LINK { color : #F3F3F3; text-decoration : underline; }
							.reportheading A:VISITED { color : #F3F3F3; text-decoration : underline; }
							.reportheading A:HOVER { color : #F3F3F3; text-decoration : underline; }

							.reportdata { font-family : arial, helvetica, sans-serif; font-size: 12px; font-weight : normal; color : #000000; text-decoration : none; }
							.reportdata A:LINK { color : #6c276a; text-decoration : underline; }
							.reportdata A:VISITED {	color : #6c276a; }
							.reportdata A:HOVER { color : #6c276a; }

							.reportfooter { font-family : arial, helvetica, sans-serif; font-size: 12px; font-weight : bold; color : #000000; text-decoration : none; background-color : #FFFFFF; }
							.reportfooter A { font-family : arial, helvetica, sans-serif; font-size: 11px; font-weight : bold; color : #333399; text-decoration : underline; background-color : #FFFFFF; }
							.reportfooter A:LINK { color : #6c276a; }
							.reportfooter A:VISITED { color : #6c276a; }
							.reportfooter A:HOVER { color : #6c276a; }

							.evenrow { background-color : #FFFFFF; }
							.oddrow { background-color : #E0E0E0; }

							div.errMsg { font-weight:bold; color:Red; }
            </style>
	    </head>
        <body>

		<p/>

    		<table align="center" border="0" width="98%" cellpadding="0" cellspacing="0">
			    <tr>
				    <td align="left" class="contenttitle">
					    Search Results
				    </td>
			    </tr>
			</table>

			<p />

    		<table align="center" border="0" width="98%" cellpadding="0" cellspacing="0">
			    <tr>
				    <td align="left" class="contentsubtitle">
	                    <table align="center" width="98%" cellpadding="0" cellspacing="0" border="0" class="reportmain">
		                    <tr>
			                    <td>
        				            <table width="100%" cellpadding="2" cellspacing="1" border="0">
				                        <tr name="trColHeaders">
    				                        <xsl:apply-templates select="displayColumns"/>
                                            <xsl:for-each select="/searchXml/displayColumns/displayColumn">
	                                            <td class="reportheading"><xsl:value-of select="@displayName"/></td>
                                            </xsl:for-each>
				                        </tr>
	                                    <xsl:for-each select="dataRow">
	                                        <xsl:variable name="dr_curr"><xsl:value-of select="position()"/></xsl:variable>
		                                    <tr>

			                                    <xsl:if test='position() mod 2 = 0'>
				                                    <xsl:attribute name="class">evenrow</xsl:attribute>
			                                    </xsl:if>
			                                    <xsl:if test='position() mod 2 != 0'>
				                                    <xsl:attribute name="class">oddrow</xsl:attribute>
			                                    </xsl:if>

                                                <xsl:for-each select="/searchXml/displayColumns/displayColumn">
			                                        <xsl:variable name="fld"><xsl:value-of select="@fieldName"/></xsl:variable>
			                                        <!--td class="reportdata"><xsl:value-of select="//dataRow[position() = number($dr_curr)]/@*[name() = $fld]"/></td-->
			                                        <td class="reportdata"><xsl:value-of select="//dataRow[position() = number($dr_curr)]/Fld[@ID=$fld]"/></td>
		                                        </xsl:for-each>
		                                    </tr>
                                        </xsl:for-each>
                                    </table>
                                </td>
                            </tr>
                        </table>
				    </td>
			    </tr>           
		    </table>

		    <p/>

		    <table align="center" width="98%">
			    <tr>
				    <td align="left" valign="top">
					    <a href="#top"><!--img src="../Styles/Online/Images/top.gif" border="0" alt="Back to Top"/--></a>
				    </td>
			    </tr>
			    <tr>
				    <td class="copyright">

				    </td>
			    </tr>
		    </table>

        </body>
	</html>
</xsl:template>

</xsl:stylesheet>
