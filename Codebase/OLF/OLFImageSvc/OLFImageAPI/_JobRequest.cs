using System;
using System.Data;
using System.Xml;
using WFS.RecHub.Common;
////******************************************
////* Removed due to cancellation of CR# 7728
////******************************************
////using DMP.ipo.Services;
////******************************************

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     11/20/2003
*
* Purpose:  IntegraPAY Online Services Image Creation Server Object.
*
* Modification History
*   11/20/2003 CR JMC 
*       - Created Class.
*   03/17/2004 CR 7017 JMC
*       - Added constants for Xml Search Criteria nodes.
*   03/22/2004 CR 7074 JMC
*       - Added "Page" node constant, Added Page property to JobRequestOptions struct, added
*         default value(-1) for page when retrieving values from Xml instructions.
*   06/01/2004 CR 7728 JMC
*       - Added several functions that were only used in a single class, but with new 
*         requirements
*         are useful in multiple classes that inherit from the class.
*       - Added Remote image retrieval common functionality.
*       - Added references to System.Data, System.IO, System.Collections, and
*         DMP.ipo.Services.
*       - Removed ImageNotAvailable functionality.  Moved to cABCPdf3.cs.
*       - Added additional comments to function headers.
*       - Removed ProcessingInstructions structure.  Moved to _TransactionalJobRequest.
*       - Added functionality to package image objects and retrieve images from the 
*         new IRIS web service.
*       - Changed "Serial Number" label to read "Check Number". 
*   09/08/2004 CR 9345 JMC
*       - Changed "Serial Number" label to read "Check Number". 
*   01/18/2005 CR 11337 JMC
*       - Added IMAGE_FILTER_OPTION enumeration
*       - Added ImageFilterOption property to JobRequestOptions struct.
*       - Modified getJobRequestOptions function to set the value for ImageFilterOption.
*   03/03/2005 CR 10537 JMC
*       - Added XML_ATTR_SESSION_ID constant
*       - Added XML_ELE_SEARCH_PARMS constant
*       - Added protected cOLLockbox class
*       - Added public sessionID property
*       - Added protected determineBatchLockbox method
*       - Added protected getOLLockbox method
*   05/13/2008 CR 23661 JCS
*       - Added XML_ATTR_DISPLAY_REMITTER_IN_PDF constant.
*       - Added displayRemitterNameInPDF to JobRequestOptions struct.
*       - Modified getJobRequestOptions function to set the value for displayRemitterNameInPDF.
*   05/22/2008 CR 23581 JCS
*       - Removed XML_ATTR_PAGE constant and added constants XML_ATTR_CHECK_DISPLAY_MODE, XML_ATTR_DOCUMENT_DISPLAY_MODE
*       - Remove page property from JobRequestOptions struct and replaced with checkImageDisplayMode, documentImageDisplayMode
*       - Modified getJobRequestOptions function to set the value for checkImageDisplayMode, documentImageDisplayMode
*   10/5/2011 CR 46882 WJS
*       - Change embed resource name to correct path
*   07/18/2012 CR 52276 JNE
*       - Added CSS_SEARCH_OUTPUT_HTML constant.
*   08/08/2012 CR 54169 JNE 
*       - Added XML_ATTR_BATCH_NUMBER, XML_ELE_BATCH, XML_ELE_TRANSACTION constants    
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 118024 TWE 10/21/2013
*       -FP Added displayBatchID
* WI 121672 MLH 11/26/2013
*   Added OLClientAccount DisplayLabel formatted with DDA  See WI 121666 Attachment
*   for v2.0 limitations.
*   Removed USE_ITEMPROC code
* WI 134804 TWE 04/02/2014
*   Add DDA to Account Search, add to PDF View
* WI 157074 JMC 08/20/2014
*   -Modified OLLockbox object to include DisplayLabel property.
* WI 164152 SAS 09/08/2014
*   -Modified to set the Check and Document display mode as one sided for ACH 
*   -and Wire Transfer payment types
* WI 155445 SAS 09/12/2014
*   -Modified to add PaymentType and PaymentSource constant
* WI 182148 SAS 12/16/2014
*   -Modified to add case for Batch node
* WI 185936 and 185937 JSF 01/26/2015
*   -Batch ID column using incorrect database column
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

    public delegate void jobStatusChangedEventHandler(Object source, _JobRequest.jobRequestEventArgs e);

    /// <summary>
    /// ipo Image Service Job Request base class.
    /// </summary>
	public abstract class _JobRequest : _DMPObjectRoot {

		#region Private Constants

        //***** Xml document root
        private const string XML_ELE_SERVICE_REQUEST_ROOT   = "ipoServiceRequest";

        //***** Job Request Parms
        private const string XML_ELE_JOB_REQUEST_ROOT       = "jobRequest";
        private const string XML_ATTR_JOB_REQUEST_TYPE      = "jobRequestType";

        //***** Job Request Parms
        protected const string XML_ATTR_JOB_ID                = "jobID";
        protected const string XML_ATTR_SITE_KEY              = "siteKey";

        //***** Job Request Options
        protected const string XML_ELE_JOB_REQ_OPTIONS                  = "jobRequestOptions";
        protected const string XML_ATTR_DISPLAY_SC                      = "displayScannedCheck";
        protected const string XML_ATTR_INCLUDE_IMAGES                  = "includeImages";
        protected const string XML_ATTR_RETURN_TYPE                     = "returnType";
        protected const string XML_ATTR_SESSION_ID                      = "sessionID";
        protected const string XML_ATTR_DISPLAY_REMITTER_IN_PDF         = "displayRemitterNameInPDF";
        protected const string XML_ATTR_CHECK_DISPLAY_MODE              = "checkImageDisplayMode";
        protected const string XML_ATTR_DOCUMENT_DISPLAY_MODE           = "documentImageDisplayMode";
        protected const string XML_ATTR_DISPLAY_BATCH_ID                = "displayBatchID";
        protected const string XML_ATTR_OLCLIENTACCOUNT_DISPLAYLABEL    = "olClientAccountDisplayOverride";

        //***** Outputs Node
        protected const string XML_ELE_OUTPUTS                = "outputs";

        //***** Output Options
        protected const string XML_ELE_OUTPUT                 = "output";
        protected const string XML_ATTR_OUTPUT_FORMAT         = "outputFormat";

        //***** Text Options
        protected const string XML_ELE_TEXT_OPTIONS           = "textOptions";
        protected const string XML_ATTR_TEXT_FILE_EXT         = "fileExtension";

        //***** Html Options
        protected const string XML_ELE_HTML_OPTIONS           = "htmlOptions";

        //***** Xml Options
        protected const string XML_ELE_XML_OPTIONS            = "xmlOptions";

        //***** Pdf Options
        protected const string XML_ELE_PDF_OPTIONS            = "pdfOptions";
        protected const string XML_ELE_PDF_LAYOUT             = "layout";

        //***** Search Results Node
        protected const string XML_ELE_SEARCH_RESULTS         = "searchResults";

        //***** Optional Columns Header Node
        protected const string XML_ELE_OPTIONAL_COLUMNS       = "displayColumns";

        //***** Search Criteria Header Node
        protected const string XML_ELE_SEARCH_CRITERIA        = "searchCriteria";
        protected const string XML_ELE_SEARCH_PARMS           = "searchParms";
        protected const string XML_ELE_SEARCH_CRITERIA_ROW    = "criteriaRow";
        protected const string XML_ATTR_DISPLAY_TITLE         = "displayTitle";
        protected const string XML_ATTR_DISPLAY_TEXT          = "displayText";


        //***** Optional Columns
        protected const string XML_ELE_OPTIONAL_COLUMN        = "displayColumn";
        protected const string XML_ATTR_FIELD_NAME            = "fieldName";
        protected const string XML_ATTR_DATA_TYPE_ENUM        = "dataTypeEnum";
        protected const string XML_ATTR_DISPLAY_NAME          = "displayName";

        //***** Data Rows Header Node
        protected const string XML_ELE_DATA_ROWS              = "dataRows";

        //***** Data Rows
        protected const string XML_ELE_DATA_ROW               = "dataRow";

        protected const string XML_ATTR_LOCKBOX_ID            = "LockboxID";      
        protected const string XML_ATTR_DEPOSITDATE           = "DepositDate";
        protected const string XML_ATTR_BANK_ID               = "BankID";      
        protected const string XML_ATTR_TRANSACTION_ID        = "TransactionID";      
        protected const string XML_ATTR_DEPOSIT_DATE          = "Deposit_Date";
        protected const string XML_ATTR_BATCH_ID              = "BatchID";
        protected const string XML_ATTR_SOURCE_BATCH_ID       = "SourceBatchID";
        protected const string XML_ATTR_BATCH_NUMBER          = "BatchNumber";
        protected const string XML_ATTR_PAYMENT_TYPE          = "PaymentType";
        protected const string XML_ATTR_PAYMENT_SOURCE        = "PaymentSource";
        protected const string XML_ATTR_TXN_SEQUENCE          = "TxnSequence";
        protected const string XML_ATTR_AMOUNT                = "Amount";
        protected const string XML_ATTR_RT                    = "RT";
        protected const string XML_ATTR_DDA                   = "DDA";
        protected const string XML_ATTR_PAYER                 = "Payer";
        protected const string XML_ATTR_PAYMENTBATCHSEQUENCE  = "ChecksBatchSequence";
        protected const string XML_ATTR_INVOICEBATCHSEQUENCE  = "StubsBatchSequence";
        protected const string XML_ATTR_ACCOUNT_NUMBER        = "AccountNumber";
        protected const string XML_ATTR_SERIAL                = "Serial";
        protected const string XML_ATTR_SERIAL_NUMBER         = "SerialNumber";
        protected const string XML_ATTR_BATCH_SEQUENCE        = "BatchSequence";

        //***** Search Output Root Node
        protected const string XML_ELE_SEARCH_XML_ROOT        = "searchXml";

        //***** Search Output Check Root Node
        protected const string XML_ELE_CHECK                  = "check";
        protected const string XML_ATTR_IMAGE_PATH            = "imagePath";

        //***** Search Output Document Root Node
        protected const string XML_ELE_DOCUMENT               = "document";
        protected const string XML_ATTR_DOCUMENT_ERROR        = "documentError";

        protected const string XML_ELE_BATCHES                = "Batches";
        protected const string XML_ELE_BATCH                  = "batch";
         protected const string XML_ELE_TRANSACTIONS           = "transactions";
        protected const string XML_ELE_TRANSACTION            = "transaction";

        protected const string XML_ELE_CHECK_ERROR            = "checkError";
        protected const string XML_ELE_IMAGE_PATH             = XML_ATTR_IMAGE_PATH;

        protected const string JOB_REQ_TYPE_TRANSACTIONS      = "TRANSACTIONS";
        protected const string JOB_REQ_TYPE_SEARCH_RESULTS    = "SEARCH_RESULTS";

        protected const string RELATIVE_IMAGE_PATH            = "images\\";

        protected const string XSL_EMBEDDED_PATH              = "WFS.RecHub.OLFImageAPI.resource";
        protected const string XSL_SEARCH_OUTPUT_TEXT         = "ipo_search_output_text.xsl";
        protected const string XSL_SEARCH_OUTPUT_HTML         = "ipo_search_output_html.xsl";
        protected const string XSL_SEARCH_OUTPUT_PP           = "ipo_search_output_pp.xsl";
        protected const string CSS_SEARCH_OUTPUT_HTML         = "ipo_search_output_html.css";
 
        protected const string XSL_NAMESPACE_PREFIX           = "xsl";
        protected const string XSL_NAMESPACE_URI              = "http://www.w3.org/1999/XSL/Transform";

        protected const string FILE_EXT_PDF                   = "pdf";
        protected const string FILE_EXT_ZIP                   = "zip";
        protected const string FILE_EXT_JPG                   = "jpg";
        protected const string FILE_EXT_XML                   = "xml";
        protected const string FILE_EXT_HTML                  = "html";
        protected const string FILE_EXT_TIF                   = "tif";
        protected const string FILE_EXT_CSV                   = "csv";

        protected const string OUTPUT_FORMAT_ZIP              = "ZIP";
        protected const string OUTPUT_FORMAT_TEXT             = "TEXT";
        protected const string OUTPUT_FORMAT_XML              = "XML";
        protected const string OUTPUT_FORMAT_HTML             = "HTML";
        protected const string OUTPUT_FORMAT_PDF              = "PDF";

        protected const string COL_TRANSACTION_ID_LABEL       = "Transaction";
        protected const string COL_DEPOSIT_DATE_LABEL         = "Deposit Date";
        protected const string COL_BATCH_ID_LABEL             = "Batch";
        protected const string COL_RT_LABEL                   = "R/T";
        protected const string COL_AMOUNT_LABEL               = "Amount";
        protected const string COL_ACCOUNT_LABEL              = "Account";
        protected const string COL_SERIAL_LABEL               = "Check Number";
        protected const string COL_CHECKS_LABEL               = "Checks";
        protected const string COL_DOCS_LABEL                 = "Documents";

        protected const int COL_TRANSACTION_ID_DT_ENUM        = 1;
        protected const int COL_DEPOSIT_DATE_DT_ENUM          = 11;
        protected const int COL_BATCH_ID_DT_ENUM              = 1;
        protected const int COL_RT_DT_ENUM                    = 1;
        protected const int COL_AMOUNT_DT_ENUM                = 1;
        protected const int COL_ACCOUNT_DT_ENUM               = 1;
        protected const int COL_SERIAL_DT_ENUM                = 1;
        protected const string FORMAT_DATE_TIME_LOGICAL       = "yyyyMMddHHmmss";

        protected const string PDF_LAYOUT_HTML                = "html";
        protected const string PDF_LAYOUT_PDFTABLE            = "pdfTable";

		#endregion

        public event jobStatusChangedEventHandler jobStatusChangedEvent;

        private Guid _JobID = Guid.Empty;
        private Guid _SessionID;
        private string _XMLDoc = "";
        private string _FileName = "";
        private OLFJobStatus _JobStatus = OLFJobStatus.Pending;

        protected class cOLLockbox {

            private int _OLWorkgroupID;
            private int _BankID;
            private int _LockboxID;
            private string _DisplayLabel;

            public cOLLockbox() {
            }

            public int OLWorkgroupID {
                get {
                    return (_OLWorkgroupID);
                }
                set {
                    _OLWorkgroupID = value;
                }
            }

            public int bankID {
                get { return(_BankID); }
                set { _BankID = value; }
            }

            public int lockboxID {
                get { return(_LockboxID); }
                set { _LockboxID = value; }
            }

            public string DisplayLabel {
                get {
                    return (_DisplayLabel);
                }
                set {
                    _DisplayLabel = value;
                }
            }
        }


        /// <summary>
        /// Structure that contains global instructions for the job.
        /// NOTE: olClientAccountDisplayLabel is a bandaid for 2.0 (REQ 121666)
        /// </summary>
        public struct JobRequestOptions {
            public bool displayScannedCheck;
            public string returnType;
            public bool includeImages;
            public OLFImageFilterOption ImageFilterOption;
            public bool displayRemitterNameInPDF;
            public bool displayBatchID;
            public OLFImageDisplayMode checkImageDisplayMode;
            public OLFImageDisplayMode documentImageDisplayMode;
            public string olCLientAccountLabel_Override;
        }

        /// <summary>
        /// Locates and returns the root node for the current job.
        /// </summary>
        /// <returns>Root node for the current job.</returns>
        protected XmlNode nodeJobRequest() {

            try {
                XmlDocument doc = new XmlDocument();
                doc.Load(this.XMLDoc);

                XmlNode nodeRetVal = null;

                //******* ipoServiceRequest Node *************
                XmlNodeList elemList = doc.GetElementsByTagName(XML_ELE_SERVICE_REQUEST_ROOT);
                string strXPath = "/"
                                + XML_ELE_SERVICE_REQUEST_ROOT 
                                + "/" 
                                + XML_ELE_JOB_REQUEST_ROOT 
                                + "[@" 
                                + XML_ATTR_JOB_ID 
                                + "='" 
                                + this.jobID 
                                + "']";

                nodeRetVal = doc.SelectSingleNode(strXPath);

                return(nodeRetVal);
            }
            catch(Exception e)
            {
                eventLog.logError(e);
                return(null);
            }
        }


        /// <summary>
        /// Loads a JobRequestOptions structure with values contained in the Xml
        /// instructions document.
        /// </summary>
        /// <param name="nodeJobRequest">Root node of the current job.</param>
        /// <param name="jobRequestOptions">
        /// Structure of options for the current job.
        /// </param>
        public void getJobRequestOptions(XmlNode nodeJobRequest, Func<ImageDisplayModes> getDefaultDisplayModes,
            out JobRequestOptions jobRequestOptions)
        {
            XmlNode nodeJobRequestOptions;
            JobRequestOptions sctJobRequestOptions = new JobRequestOptions();

            int intTemp;
            bool bolTemp;
            bool blnACHOrWireTransfer = false;

            nodeJobRequestOptions = nodeJobRequest.SelectSingleNode(XML_ELE_JOB_REQ_OPTIONS);
            blnACHOrWireTransfer = IsACHOrWirePayment(nodeJobRequest);
            sctJobRequestOptions.displayScannedCheck = System.Convert.ToBoolean(nodeJobRequestOptions.Attributes[XML_ATTR_DISPLAY_SC].Value);

            try {
                if(nodeJobRequestOptions.Attributes[XML_ATTR_DISPLAY_REMITTER_IN_PDF] != null) {
                    if(int.TryParse(nodeJobRequestOptions.Attributes[XML_ATTR_DISPLAY_REMITTER_IN_PDF].Value, out intTemp)) {
                        sctJobRequestOptions.displayRemitterNameInPDF = (intTemp > 0);
                    } else if(bool.TryParse(nodeJobRequestOptions.Attributes[XML_ATTR_DISPLAY_REMITTER_IN_PDF].Value, out bolTemp)) {
                        sctJobRequestOptions.displayRemitterNameInPDF = bolTemp;
                    } else {
                        eventLog.logEvent("Invalid preference value detected; [" + XML_ATTR_DISPLAY_REMITTER_IN_PDF + " = '" + nodeJobRequestOptions.Attributes[XML_ATTR_DISPLAY_REMITTER_IN_PDF].Value + "'", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        sctJobRequestOptions.displayRemitterNameInPDF = false;
                    }
                } else {
                    sctJobRequestOptions.displayRemitterNameInPDF = false;
                }
            } catch { sctJobRequestOptions.displayRemitterNameInPDF = false; }

            try {
                sctJobRequestOptions.includeImages = System.Convert.ToBoolean(nodeJobRequestOptions.Attributes[XML_ATTR_INCLUDE_IMAGES].Value);
            } catch { sctJobRequestOptions.includeImages = true; }

            sctJobRequestOptions.checkImageDisplayMode = ParseImageDisplayMode(
                blnACHOrWireTransfer, nodeJobRequestOptions, XML_ATTR_CHECK_DISPLAY_MODE,
                () => getDefaultDisplayModes().CheckImageDisplayMode);

            sctJobRequestOptions.documentImageDisplayMode = ParseImageDisplayMode(
                blnACHOrWireTransfer, nodeJobRequestOptions, XML_ATTR_DOCUMENT_DISPLAY_MODE,
                () => getDefaultDisplayModes().DocumentImageDisplayMode);

            try {
                switch(nodeJobRequestOptions.Attributes["imageFilterOption"].Value) {
                    case "ChecksOnly":
                        sctJobRequestOptions.ImageFilterOption = OLFImageFilterOption.ChecksOnly;
                        break;
                    case "DocumentsOnly":
                        sctJobRequestOptions.ImageFilterOption = OLFImageFilterOption.DocsOnly;
                        break;
                    default:
                        sctJobRequestOptions.ImageFilterOption = OLFImageFilterOption.All;
                        break;

                }
            } catch{ 
                sctJobRequestOptions.ImageFilterOption = OLFImageFilterOption.All;
            }

            try
            {
                sctJobRequestOptions.displayBatchID = System.Convert.ToBoolean(nodeJobRequestOptions.Attributes[XML_ATTR_DISPLAY_BATCH_ID].Value);
            }
            catch { sctJobRequestOptions.displayBatchID = false; }

            if (nodeJobRequestOptions.Attributes.GetNamedItem(XML_ATTR_OLCLIENTACCOUNT_DISPLAYLABEL) != null)
                sctJobRequestOptions.olCLientAccountLabel_Override = nodeJobRequestOptions.Attributes.GetNamedItem(XML_ATTR_OLCLIENTACCOUNT_DISPLAYLABEL).Value; 


            sctJobRequestOptions.returnType = nodeJobRequestOptions.Attributes[XML_ATTR_RETURN_TYPE].Value;

            jobRequestOptions = sctJobRequestOptions;
        }
        private OLFImageDisplayMode ParseImageDisplayMode(bool isElectronicTransfer,
            XmlNode nodeJobRequestOptions, string attributeName, Func<OLFImageDisplayMode> getDefaultDisplayMode)
        {
            if (isElectronicTransfer)
                return OLFImageDisplayMode.FrontOnly;

            switch (Convert.ToInt32(nodeJobRequestOptions?.Attributes?[attributeName]?.Value))
            {
                case (int) OLFImageDisplayMode.BothSides:
                    return OLFImageDisplayMode.BothSides;
                case (int) OLFImageDisplayMode.FrontOnly:
                    return OLFImageDisplayMode.FrontOnly;
                default:
                    return getDefaultDisplayMode();
            }
        }

        /// <summary>
        /// Returns true if the payment is ACH or Wire Transfer
        /// </summary>
        /// <param name="nodeJobRequest"></param>
        /// <returns></returns>
        protected bool IsACHOrWirePayment(XmlNode nodeJobRequest)
        {
            bool bRetVal = false;
            int intBankID;
            int intLockboxID;
            DateTime dteDepositDate;
            long longBatchID;
            DataTable dt = new DataTable();
            try
            {

                if ((GetVariableValues(nodeJobRequest.SelectSingleNode("images"), out intBankID, out intLockboxID, out longBatchID, out dteDepositDate)) ||
                    (GetVariableValues(nodeJobRequest.SelectSingleNode(XML_ELE_BATCHES), out intBankID, out intLockboxID, out longBatchID, out dteDepositDate)) ||
                    (GetVariableValues(nodeJobRequest.SelectSingleNode(XML_ELE_TRANSACTIONS), out intBankID, out intLockboxID, out longBatchID, out dteDepositDate)))
                {
                    if (ItemProcDAL.GetPaymentTypeKey(intBankID,
                                                      intLockboxID,
                                                      dteDepositDate,
                                                      longBatchID,
                                                      out dt) && dt.Rows.Count > 0)
                        bRetVal = ((BatchPaymentType)Convert.ToInt16(dt.Rows[0]["BatchPaymentTypeKey"]) == BatchPaymentType.ACH || (BatchPaymentType)Convert.ToInt16(dt.Rows[0]["BatchPaymentTypeKey"]) == BatchPaymentType.WIRE);

                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return bRetVal;
        }

        /// <summary>
        /// Get All values of required parameter
        /// </summary>
        /// <param name="nodeRequests"></param>
        /// <param name="intBankID"></param>
        /// <param name="intLockBoxID"></param>
        /// <param name="longBatchID"></param>
        /// <param name="dteDepositDate"></param>
        /// <returns></returns>
        protected bool GetVariableValues(XmlNode nodeRequests, out int intBankID, out int intLockBoxID, out long longBatchID, out DateTime dteDepositDate)
        {
            intBankID = -1;
            intLockBoxID = -1;
            longBatchID = -1;
            dteDepositDate = DateTime.Now;
            int intTemp;
            long longTemp;
            DateTime dteTemp;
            bool bRetVal = false;

            XmlNode nodeRequest;
            try
            {
                if (!(nodeRequests == null) && (nodeRequests.ChildNodes.Count > 0))
                {
                    for (int intIndex = 0; intIndex < nodeRequests.ChildNodes.Count; ++intIndex)
                    {
                        nodeRequest = nodeRequests.ChildNodes[intIndex];
                        switch (nodeRequest.Name.ToLower())
                        {
                            case XML_ELE_BATCH:
                            case XML_ELE_CHECK:
                            case XML_ELE_DOCUMENT:
                            case XML_ELE_TRANSACTION:
                                if (nodeRequest.Attributes["BankID"] != null && int.TryParse(nodeRequest.Attributes["BankID"].Value, out intTemp))
                                {
                                    intBankID = intTemp;
                                }
                                else
                                {
                                    throw (new Exception("Invalid BankID"));
                                }
                                if (nodeRequest.Attributes["LockboxID"] != null && int.TryParse(nodeRequest.Attributes["LockboxID"].Value, out intTemp))
                                {
                                    intLockBoxID = intTemp;
                                }
                                else
                                {
                                    throw (new Exception("Invalid LockboxID"));
                                }

                                if (nodeRequest.Attributes["BatchID"] != null && long.TryParse(nodeRequest.Attributes["BatchID"].Value, out longTemp))
                                {
                                    longBatchID = longTemp;
                                }
                                else
                                {
                                    throw (new Exception("Invalid BatchID"));
                                }

                                if (nodeRequest.Attributes["DepositDate"] != null && DateTime.TryParse(nodeRequest.Attributes["DepositDate"].Value, out dteTemp))
                                {
                                    dteDepositDate = dteTemp;
                                }
                                else
                                {
                                    throw (new Exception("Invalid DepositDate"));
                                }
                                bRetVal = true;
                                break;
                        }
                        if (bRetVal)
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return bRetVal;

        }
        /// <summary>
        /// Indicates the current processing status of the job.
        /// </summary>
        public OLFJobStatus status {
            get { return _JobStatus; }
        }


        /// <summary>
        /// Fires an event that indicates that the current processing status of 
        /// the job has changed.
        /// </summary>
        /// <param name="jobStatusType"></param>
        /// <param name="e"></param>
        public void changeStatus(OLFJobStatus jobStatusType
                               , jobRequestEventArgs e) {
            _JobStatus = jobStatusType;

            if(jobStatusChangedEvent !=null) {
                jobStatusChangedEvent(this, e);
            }
        }


        /// <summary>
        /// The job ID of the current job.
        /// </summary>
        public Guid jobID {

            get { return _JobID; }
            set { _JobID = value; }
        }

        public Guid sessionID {

            get { return (_SessionID); }
            set { _SessionID = value; }
        }

        /// <summary>
        /// Xml instructions document that must be assigned to the job obejct 
        /// prior to calling the processJob() method which starts the thread.
        /// </summary>
        public string XMLDoc {
            set { _XMLDoc = value; }
            get { return _XMLDoc; }
        }


        /// <summary>
        /// Name of the file that the job will eventually produce.
        /// </summary>
        public string fileName {
            set { _FileName = value; }
            get { return _FileName; }
        }       


        /// <summary>
        /// Event argument object that contains processing details.
        /// </summary>
        public class jobRequestEventArgs : EventArgs {
            public long fileSize = 0;
            public string Message = "";
            public string src = "";
        }


        /// <summary>
        /// Utility function used to assign a start and end page range 
        /// based on the page IMAGE_DISPLAY_MODE parameter value.
        /// </summary>
        /// <param name="page">Page variable sent by the request.</param>
        /// <param name="startPage">Start of page range</param>
        /// <param name="endPage">End of page range/</param>
        protected static void establishPageRanges(OLFImageDisplayMode page
                                         , out int startPage
                                         , out int endPage) {
            switch (page) {
                case OLFImageDisplayMode.BothSides:
                    startPage = 0;
                    endPage = 1;
                    break;
                case OLFImageDisplayMode.FrontOnly:
                    startPage = 0;
                    endPage = 0;
                    break;
                default:
                    startPage = 0;
                    endPage = 1;
                    break;
            }
        }   //establishPageRanges

        protected bool getOLLockbox(int OLWorkgroupID, out cOLLockbox OLLockbox) {

            DataTable dt;
            bool bolRetVal;
            cOLLockbox objOLLockbox =  null;

            bolRetVal = ItemProcDAL.GetOLLockbox(OLWorkgroupID, out dt);

            objOLLockbox = new cOLLockbox();

            if (bolRetVal && (dt.Rows.Count > 0)) {
                DataRow dr = dt.Rows[0];

                objOLLockbox.bankID = ipoLib.NVL(ref dr, "BankID", -1);
                objOLLockbox.lockboxID = ipoLib.NVL(ref dr, "LockboxID", -1);
                objOLLockbox.OLWorkgroupID = ipoLib.NVL(ref dr, "OLWorkgroupID", -1);
                objOLLockbox.DisplayLabel = ipoLib.NVL(ref dr, "DisplayLabel", string.Empty);

                bolRetVal = true;

            } else {
                objOLLockbox.bankID = -1;
                objOLLockbox.lockboxID = -1;
                objOLLockbox.OLWorkgroupID = -1;

                bolRetVal = false;
            }

            dt.Dispose();
    
            OLLockbox = objOLLockbox;
            return(bolRetVal);
        }
    }
}
