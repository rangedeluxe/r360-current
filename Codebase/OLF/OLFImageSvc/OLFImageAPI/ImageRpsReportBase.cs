using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using WFS.RecHub.Common;
using System.Globalization;
using System.Linq;
using WFS.RecHub.DAL.OLFServicesDAL;
using WFS.RecHub.ItemProcDAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:
* Date:
*
* Purpose:
*
* Modification History
* CR 33192 J E 04/07/2011
*   - New file
* CR 45627 JMC 07/12/2011
*   - Added a Using{} clause when accessing ipoImages to ensure Dispose() is
*     called.
* CR 49316 WJS 02/08/2012
*   - Remove documentTypes
*     Use sourceProcessingDate for display
*     Fixed columns on report to match
* CR 54252 JNE 08/08/2012
*   - Add BatchNumber to class instead of looking in hash table.
* CR 52959 JNE 10/01/2012
*   - AddImages - Added FileGroup Code when determining image root path.
* WI 96037 CRG 04/10/2013
*   - Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 71542 BLR 05/07/2014
*   - Using the request's sitekey for the resource directory.
* WI 143274 SAS 05/21/2014
*   Changes done to change the datatype of BatchID from int to long
* * WI 166933 SAS 09/19/2014
*   Change done for ImageRPS Report
* WI 217655 TWE 06/10/2015
*   Only pick up account number from data entry fields (not stub record)
******************************************************************************/

namespace WFS.RecHub.OLFImageAPI
{
    internal abstract class ImageRpsReportBase : _DMPObjectRoot
    {
        protected const string IMG_IMAGE_NOT_AVAILABLE = "ImageNotAvailable.jpg";

        private PDFAttributeInfo _reportAttribInfo = null;
        private Guid _SessionID;
        private Hashtable _htCheckData = null;
        private Hashtable _htStubData   = null;
        private Hashtable _htFactBatchData = null;
        private Hashtable _htFactItemData = null;
        private Hashtable _htDataEntryTable =null;
        private Hashtable _htMarkSenseDataEntryTable = null;
        private SortedDictionary<int, ReportDocType> _batchSequenceElement = null;
        private readonly ImageDisplayModeCache _imageDisplayModeCache;

        private enum  ReportDocType
        {
            CHECK =0,
            DOCUMENT =1,
            STUB = 2
        };

        protected ImageRpsReportBase(string siteKey, Guid guidSessionID)
        {
            this.siteKey = siteKey;
            BatchSequence = -1;
            _SessionID = guidSessionID;

            ReportDateTime = DateTime.Now;
            _imageDisplayModeCache = new ImageDisplayModeCache(OlfServicesDal);
        }

        private DateTime ReportDateTime { get; set; }
        protected abstract string ReportTitle { get; }

        private bool GatherReportData(XmlNode nodeReport)
        {
            try
            {
                if (!GatherChecksData())
                    return false;

                if (!GatherStubData())
                    return false;

                if (!GatherBatchData())
                    return false;

                if (!GatherBatchInformation())
                    return false;

                if (!GatherItemData())
                    return false;

                if (!GatherDataEntryData())
                    return false;
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "GatherReportData");
                throw;
            }
            return true;
        }

        private bool GatherChecksData()
        {
            bool bReturn = false;
            DataTable dtChecks = null;
            cCheck check = null;
            int batchSequence = 0;
            DataRow rowItem = null;
            try
            {
                if (ItemProcDAL.GetCheckData(_SessionID, BankID, LockboxID, BatchID,DepositDate, TransactionID, out dtChecks))
                {
                    if (dtChecks != null )
                    {
                        foreach (DataRow dr in dtChecks.Rows)
                        {
                            if (Int32.TryParse(dr["BatchSequence"].ToString(), out batchSequence))
                            {
                                check = new cCheck();
                                //can not pass foreach by ref
                                rowItem= dr;
                                check.LoadDataRow(ref rowItem);
                                HTCheckData.Add(batchSequence, check);
                                BatchSequenceElement.Add(batchSequence, ReportDocType.CHECK);
                            }
                        }
                    }

                    bReturn = true;
                }
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "GatherChecksData");
                throw;
            }
            finally
            {
                if (dtChecks != null)
                {
                    dtChecks.Dispose();
                }
            }

            return bReturn;
        }
        private bool GatherStubData()
        {
            bool bReturn = false;
            // Retrieve from factStubs
            DataTable dtStubsTable = null;
            DataRow rowItem = null;
            cStub stub = null;
            int batchSequence = 0;
            try
            {
                if (ItemProcDAL.GetStubData(_SessionID,BankID, LockboxID, BatchID, DepositDate, TransactionID,out dtStubsTable))
                {
                    if (dtStubsTable != null)
                    {
                        foreach (DataRow dr in dtStubsTable.Rows)
                        {
                            if (Int32.TryParse(dr["BatchSequence"].ToString(), out batchSequence))
                            {
                                stub = new  cStub();
                                //can not pass foreach by ref
                                rowItem = dr;
                                stub.LoadDataRow(ref rowItem);
                                HTStubData.Add(stub.BatchSequence, stub);
                                BatchSequenceElement.Add(stub.BatchSequence, ReportDocType.STUB);
                            }
                        }
                    }

                    bReturn = true;
                }
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "GatherStubData");
                throw;
            }
            finally
            {
                if (dtStubsTable != null)
                {
                    dtStubsTable.Dispose();
                }
            }
            return bReturn;
        }

        private bool GatherBatchInformation()
        {
            DataTable dt;

            if (!BatchDataDAL.GetBatchInformation(_SessionID, BatchID, DepositDate, out dt)
                || dt == null
                || dt.Rows == null
                || dt.Rows.Count == 0)
                return false;

            var row = dt.Rows[0];
            ImmutableDate = DateTime.ParseExact(row["PICSDate"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);

            return true;
        }

        private bool GatherBatchData()
        {
            bool bReturn = false;
            // Retrieve from dimBatchDataSetupFields & factBatchData
            DataTable dtBatchData = null;
            try
            {
                if (BatchDataDAL.GetBatchData(_SessionID,BankID, LockboxID, BatchID, DepositDate, out dtBatchData))
                {
                    if (dtBatchData != null )
                    {
                        foreach (DataRow dr in dtBatchData.Rows)
                        {
                            HTFactBatchData.Add(dr["KeyWord"].ToString(), dr["DataValue"].ToString());
                        }
                    }
                    bReturn = true;
                }
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "GatherBatchData");
                throw;
            }
            finally
            {
                if (dtBatchData != null)
                {
                    dtBatchData.Dispose();
                }
            }
            return bReturn;
        }
        private bool GatherItemData()
        {
            bool bReturn = false;
            // Retrieve from dimItemDataSetupFields & factItemData
            DataTable dtItemData = null;
            try
            {
                if (BatchSequence > -1)
                {
                    bReturn = BatchDataDAL.GetItemDataByItem(_SessionID,
                                                              BankID,
                                                              LockboxID,
                                                              BatchID,
                                                              DepositDate,
                                                              BatchSequence,
                                                              out dtItemData);
                }
                else
                {
                    bReturn = BatchDataDAL.GetItemDataByTransaction(_SessionID,
                                                                    BankID,
                                                                    LockboxID,
                                                                    BatchID,
                                                                    DepositDate,
                                                                    TransactionID,
                                                                    out dtItemData);
                }

                if(bReturn) {
                    if (dtItemData != null && dtItemData.Rows.Count > 0)
                    {
                        long sourceBatchId;
                        long.TryParse(dtItemData.Rows[0]["SourceBatchID"].ToString(), out sourceBatchId);
                        SourceBatchID = sourceBatchId;

                        foreach (DataRow dr in dtItemData.Rows)
                        {
                            HTFactItemData.Add(BuildKeyWordValue(dr), dr["DataValue"].ToString());
                        }
                    }
                    bReturn = true;
                }
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "GatherItemData");
                throw;
            }
            finally
            {
                if (dtItemData != null)
                {
                    dtItemData.Dispose();
                }
            }
            return bReturn;
        }
        private bool GatherDataEntryData()
        {
            bool bReturn = false;
            // Retrieve from dataentry
            DataTable dtDataEntryData = null;
            string keyWordValue = string.Empty;
            try
            {
                if (ItemProcDAL.GetDataEntryData(_SessionID,BankID, LockboxID, BatchID, DepositDate, TransactionID, BatchSequence, out dtDataEntryData))
                {
                    if (dtDataEntryData != null && dtDataEntryData.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtDataEntryData.Rows)
                        {
                            keyWordValue = BuildKeyWordValue(dr);
                            if (!HTDataEntryData.ContainsKey(keyWordValue))
                            {
                                HTDataEntryData.Add(keyWordValue, dr["DataValue"].ToString());
                            }
                            else
                            {
                                eventLog.logEvent("A duplicate dataEntry value was detected. Keyword is: " + keyWordValue, MessageImportance.Verbose);
                            }
                            if (!HTMarkSenseDataEntryTable.ContainsKey(keyWordValue))
                            {
                                HTMarkSenseDataEntryTable.Add(keyWordValue, dr["MarkSense"].ToString());
                            }
                            else
                            {
                                eventLog.logEvent("A duplicate dataEntry(mark sense) value was detected. Keyword is: " + keyWordValue, MessageImportance.Verbose);
                            }
                        }
                    }
                    bReturn = true;
                }
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "GatherDataEntryData");
                throw;
            }
            finally
            {
                if (dtDataEntryData != null) 
                {
                    dtDataEntryData.Dispose();
                }
            }
            return bReturn;
        }

        public bool StartReport(XmlNode nodeReport, out Document document, out PdfWriter writer, out string strTempFileName)
        {
            bool bReturn = false;

            strTempFileName = string.Empty;
            try
            {
                document = new Document();
                strTempFileName = System.IO.Path.GetTempFileName();
                writer = PdfWriter.GetInstance(document, new FileStream(strTempFileName, FileMode.Create));
                document.AddTitle("title");
                document.AddAuthor("author");

                document.Open();

                PDFAttribInfo.Top = document.Top;
                PDFAttribInfo.Bottom = document.Bottom;
                PDFAttribInfo.Left = document.Left-20;
                PDFAttribInfo.Right = document.Right;
                PDFAttribInfo.Center = (PDFAttribInfo.Right - PDFAttribInfo.Left) / 2;
                PDFAttribInfo.RightColumn = PDFAttribInfo.Center + 30;

                bReturn = true;
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "StartReport");
                throw;
            }
            return bReturn;
        }

        public bool EndReport( Document document, string strTempFileName, string completedFilePath)
        {
            bool bReturn =false;
            try
            {
                document.Close();
                if (File.Exists(completedFilePath))
                {
                    File.Delete(completedFilePath);
                }
                File.Copy(strTempFileName, completedFilePath);
                File.Delete(strTempFileName);
                bReturn = true;
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "EndReport");
                throw;
            }
            return bReturn;
        }
        protected void RunReport(XmlNode nodeReport, Document document, PdfWriter writer)
        {
            try
            {
                if (!GatherReportData(nodeReport))
                {
                    throw new InvalidOperationException("Unable to Gather Report Data");
                }

                if (BatchSequence > 0)
                {
                    BuildItemReport(BatchSequence, true, document, writer);
                }
                else
                {
                    var isFirstItem = true;
                    foreach (int batchSeq in BatchSequenceElement.Keys)
                    {
                        var success = BuildItemReport(batchSeq, isFirstItem, document, writer);
                        if (!success)
                        {
                            //an item report failed. break out of the loop
                            break;
                        }
                        isFirstItem = false;
                    }
                }
            }
            catch (Exception e)
            {
                DisplayError(writer.DirectContent, e.Message);
                eventLog.logError(e, this.GetType().Name, "RunReport");
            }
        }
        private void DisplayError(PdfContentByte cbDetail, string message)
        {
            AddPageHeaderAndFooter(cbDetail, false, 1);
            PDFAttribInfo.PageNo++;
            PDFAttribInfo.CurrentDistanceFromTop = 40;
            cbDetail.BeginText();
            cbDetail.SetFontAndSize(PDFAttribInfo.BF, 10);
            cbDetail.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "An error occured while generating report: " + message, PDFAttribInfo.Left, PDFAttribInfo.Top - PDFAttribInfo.CurrentDistanceFromTop, 0);
            cbDetail.SetFontAndSize(PDFAttribInfo.BF, 10);
            PDFAttribInfo.CurrentDistanceFromTop = 60;
            cbDetail.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Contact the system administrator", PDFAttribInfo.Left, PDFAttribInfo.Top - PDFAttribInfo.CurrentDistanceFromTop, 0);

            cbDetail.EndText();
        }
        private bool BuildItemReport(int batchSeq, bool isFirstItem, Document document, PdfWriter writer)
        {
            bool bReturn = false;
            bool bIsCheck = false;
            bool bIsStub = false;

            if (!isFirstItem)
            {
                document.NewPage();
            }
            if (BatchSequenceElement.Count > 0)
            {
                 bIsStub = BatchSequenceElement[batchSeq] == ReportDocType.STUB ? true : false;
                 bIsCheck = BatchSequenceElement[batchSeq] == ReportDocType.CHECK ? true : false;
            }
            if (LayOutReport(bIsCheck, bIsStub, batchSeq, document, writer))
            {
                bReturn = true;
            }
            else
            {
                bReturn = false;
                eventLog.logEvent("Unable to Layout Report Data", MessageImportance.Verbose);
            }
            return bReturn;
        }

        private bool LayOutReport(bool bIsCheck, bool bIsStub, int batchSeq, Document document, PdfWriter writer)
        {
            bool bReturn = false;
            PdfContentByte cbDetail;
            try
            {
                cbDetail = writer.DirectContent;

                AddImages(document, cbDetail, bIsCheck,  batchSeq);

                AddPageHeaderAndFooter( cbDetail, bIsCheck, batchSeq);
                //amount always uses original batchSequence. Just factItemData needs to get special stub
                AddTopInformation(cbDetail, batchSeq, GetAmount(bIsCheck, batchSeq));
                AddVariableFieldsInformation(cbDetail, bIsCheck, batchSeq  );
                AddItemInformation(cbDetail, bIsCheck, batchSeq);
                AddPass1Information(cbDetail, batchSeq, bIsCheck);
                if (bIsCheck)
                {
                    AddPass2Information(cbDetail, batchSeq);
                }

                bReturn  =true;
            }
            catch (Exception ex)
            {
                eventLog.logError(ex, this.GetType().Name, "LayOutReport:" + ex.Message);
                throw;
            }

            return bReturn;
        }
        private Tuple<IItem, ImageItemType> GetItemAndType(bool bIsCheck, int batchSeq, out int bankId, out int lockboxId)
        {
            bankId = -1;
            lockboxId = -1;

            if (bIsCheck)
            {
                if (HTCheckData.ContainsKey(batchSeq))
                {
                    var check = (cCheck) HTCheckData[batchSeq];
                    bankId = check.BankID;
                    lockboxId = check.LockboxID;
                    return new Tuple<IItem, ImageItemType>(check, ImageItemType.Check);
                }
            }
            else
            {
                if (HTStubData.ContainsKey(batchSeq))
                {
                    var stub = (cStub) HTStubData[batchSeq];
                    stub.BatchSequence = stub.DocumentBatchSequence;
                    bankId = stub.BankID;
                    lockboxId = stub.LockboxID;

                    // Only include a page for images if the stub has a corresponding
                    // factDocuments record.
                    return stub.HasDocument
                        ? new Tuple<IItem, ImageItemType>(stub, ImageItemType.Document)
                        : null;
                }
            }

            return null;
        }
        private string GetRootImageLocation(int bankId, int lockboxId)
        {
            string strRootImageLocation = siteOptions.imagePath;
            if (siteOptions.imageStorageMode == ImageStorageMode.FILEGROUP)
            {
                // Lookup workgroup
                WorkgroupDTO workgroup = ItemProcDAL.GetWorkgroup(bankId, lockboxId);
                if (workgroup != null)
                {
                    if (!string.IsNullOrWhiteSpace(workgroup.FileGroup))
                    {
                        if (Directory.Exists(workgroup.FileGroup))
                        {
                            strRootImageLocation = workgroup.FileGroup;
                        }
                        else
                        {
                            eventLog.logError(new eImageDirectoryNotFoundException(workgroup.FileGroup));
                        }
                    }
                }
            }
            return strRootImageLocation;
        }
        private byte[] GetImageBytes(cipoImages ipoImages, IItem imageItem, int currentPage)
        {
            var response = ipoImages.GetImage(imageItem, currentPage);
            return response.IsSuccessful ? response.ImageBytes : null;
        }
        private void AddImages(Document document, PdfContentByte pdf, bool bIsCheck, int batchSeq)
        {
            float fScaleWidth = 0;
            float fScaleHeight = 0;
            Image jpg1 = null;

            int intbankid;
            int intlockboxid;

            var imageItemAndType = GetItemAndType(bIsCheck, batchSeq, out intbankid, out intlockboxid);
            if (imageItemAndType != null)
            {
                var imageDisplayModes = _imageDisplayModeCache.GetImageDisplayModes(intbankid, intlockboxid);
                var imageDisplayMode = imageDisplayModes.GetImageDisplayMode(imageItemAndType.Item2);

                //we have images
                AddPageHeaderAndFooter(pdf, bIsCheck, batchSeq);

                // Get image path
                var strRootImageLocation = GetRootImageLocation(intbankid, intlockboxid);

                using(cipoImages ipoImages = new cipoImages(this.siteKey, siteOptions.imageStorageMode,strRootImageLocation))
                {
                    var imageItem = imageItemAndType.Item1;
                    var imagesAsBytes = (
                        from pageNumber in imageDisplayMode.GetPageNumbers()
                        let bytes = GetImageBytes(ipoImages, imageItem, pageNumber)
                        where bytes != null
                        select bytes
                        ).ToList();

                    if (imagesAsBytes.Any())
                    {
                        var contentArea = new RectangleReadOnly(
                            llx: PDFAttribInfo.Left, lly: PDFAttribInfo.Bottom + PDFAttribInfo.CurrentDistanceFromBottom,
                            urx: PDFAttribInfo.Right, ury: PDFAttribInfo.Top - PDFAttribInfo.CurrentDistanceFromTop);
                        IImageLayout layout = imagesAsBytes.Count == 1
                            ? (IImageLayout) new SingleImageLayout(contentArea)
                            : new TwoImageLayout(contentArea, 18);

                        foreach (var imageAsBytes in imagesAsBytes)
                        {
                            jpg1 = Image.GetInstance(imageAsBytes);
                            if (jpg1 != null)
                            {
                                var boundingBox = layout.GetImageRectangle();

                                GetImageScale(jpg1.Width, jpg1.Height, boundingBox.Width, boundingBox.Height,
                                    out fScaleWidth, out fScaleHeight);
                                jpg1.ScaleAbsolute(fScaleWidth, fScaleHeight);

                                // SetAbsolutePosition sets the *lower*-left coordinate.
                                var imageLeft = boundingBox.Left;
                                var imageBottom = boundingBox.Top - fScaleHeight;
                                jpg1.SetAbsolutePosition(imageLeft, imageBottom);
                                pdf.AddImage(jpg1);

                                layout.AddImage(fScaleHeight);
                            }
                        }
                    }
                    else
                    {
                        AddImageNotFound(pdf,
                            PDFAttribInfo.Left, PDFAttribInfo.Top - PDFAttribInfo.CurrentDistanceFromTop);
                    }
                }   //using
                //we have images found create a new page for those images
                document.NewPage();
            }
        }
        private void AddImageNotFound(PdfContentByte pdf, float startXPosition, float startYPosition)
        {
            // WI 71542 : Using the sitekey from the request, not the server's sitekey.
            var fallbackImageProvider = new SiteKeyFallbackImageProvider(siteKey, serverOptions);
            var image = Image.GetInstance(fallbackImageProvider.LoadImageNotFound());
            if (image != null)
            {
                const float RECTHEIGHT = 250;
                const float RECTWIDTH = 250;
                image.ScaleAbsolute(RECTWIDTH, RECTHEIGHT);
                // SetAbsolutePosition takes the bottom-left corner
                image.SetAbsolutePosition(startXPosition, startYPosition - RECTHEIGHT);
                pdf.AddImage(image);
            }
        }
        private void AddTopInformation(PdfContentByte cbDetail, int batchSeq, string sAmountReturned)
        {
            try
            {
                int heightWriteSelectedRows = 0;
                string sAmount = string.Empty;
                string sAppliedAmount = string.Empty;
                int numberOfColumns = 4;
                PdfPTable aTable = new PdfPTable(numberOfColumns);
                PdfPCell cell = new PdfPCell();

                aTable.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                aTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                aTable.TotalWidth = PDFAttribInfo.Right - PDFAttribInfo.Left;
                aTable.WidthPercentage = 90;

                cell.Border = 0 ;
                cell.Phrase = new Phrase("Applied Amount:", PDFAttribInfo.FontBold10);
                aTable.AddCell(cell);
                cell.Phrase = new Phrase(sAmountReturned, PDFAttribInfo.Font10);
                aTable.AddCell(cell);
                cell.Phrase = new Phrase("Applied Amount Operator:", PDFAttribInfo.FontBold10);
                aTable.AddCell(cell);
                cell.Phrase = new Phrase(GetHashTableValueItem(HTFactItemData, batchSeq, "Applied OpID"), PDFAttribInfo.Font10);
                aTable.AddCell(cell);

                cell.Phrase = new Phrase("Document Audit Trail:", PDFAttribInfo.FontBold10);
                aTable.AddCell(cell);
                cell.Phrase = new Phrase(GetHashTableValueItem(HTFactItemData, batchSeq, "Audit Trail"), PDFAttribInfo.Font10);
                cell.Colspan = 3;
                aTable.AddCell(cell);

                //bottom part of table
                cell.Colspan = 1;
                //WJS this field will never get filled in the DAT file currently.
                cell.Phrase = new Phrase("ARC Return Audit:", PDFAttribInfo.FontBold10);
                aTable.AddCell(cell);
                cell.Phrase = new Phrase(GetHashTableValueItem(HTFactItemData, batchSeq, "ACH Returns Audit Trail"), PDFAttribInfo.Font10);
                cell.Colspan = 3;
                aTable.AddCell(cell);

                cell.Phrase = new Phrase("Reject Reason:", PDFAttribInfo.FontBold10);
                cell.Colspan = 1;
                aTable.AddCell(cell);
                cell.Phrase = new Phrase(GetHashTableValueItem(HTFactItemData, batchSeq, "Reject Reason"), PDFAttribInfo.Font9);
                cell.Colspan = 3;
                aTable.AddCell(cell);
                cell.Phrase = new Phrase("", PDFAttribInfo.Font10);

                heightWriteSelectedRows =  (int)aTable.WriteSelectedRows(0, -1, PDFAttribInfo.Left, PDFAttribInfo.Top - PDFAttribInfo.CurrentDistanceFromTop, cbDetail);
                PDFAttribInfo.CurrentDistanceFromTop = (int)PDFAttribInfo.Top - heightWriteSelectedRows;
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "AddTopInformation");
                throw;
            }
        }
        private void AddVariableFieldsInformation(PdfContentByte cbDetail, bool bIsCheck, int batchSeq)
        {
            try
            {
                bool valueOfMarkSense = false;
                string markSenseValue = string.Empty;
                StringBuilder sbMsg = new StringBuilder();
                int markSenseCount = 1;
                int itemFieldCount = 1;
                string batchSeqStr = string.Empty;
                Dictionary<string, string> markSenseFieldInfo = new Dictionary<string, string>();
                Dictionary<string, string> dataEntryFieldInfo = new Dictionary<string, string>();
                cCheck check = null;
                string sAccountNum = GetAccountNumber(bIsCheck, batchSeq);
                cReportKeywordLookup dataEntryReportLookup = new cReportKeywordLookup();
                cReportKeywordLookup markSenseReportLookup = new cReportKeywordLookup();

                //add certain very important key words if they exist
                if (!String.IsNullOrEmpty(sAccountNum))
                {
                    sbMsg.Append(itemFieldCount.ToString("D2") + " Account Number");
                    dataEntryFieldInfo.Add(sbMsg.ToString(), sAccountNum);
                    itemFieldCount++;
                }
                if (bIsCheck && HTCheckData.ContainsKey(batchSeq))
                {
                    check = (cCheck) HTCheckData[batchSeq];
                    if (!String.IsNullOrEmpty(check.RT))
                    {
                        sbMsg.Length = 0;
                        sbMsg.Append(itemFieldCount.ToString("D2") + " RT ");
                        dataEntryFieldInfo.Add(sbMsg.ToString(), check.RT);
                        itemFieldCount++;
                    }
                    if (!String.IsNullOrEmpty(check.Serial))
                    {
                        sbMsg.Length = 0;
                        sbMsg.Append(itemFieldCount.ToString("D2") + " Serial ");
                        dataEntryFieldInfo.Add(sbMsg.ToString(), check.Serial);
                        itemFieldCount++;
                    }
                }

                foreach (string key in HTDataEntryData.Keys)
                {
                    sbMsg.Length=0;
                    if (dataEntryReportLookup.AssignElements(key) && markSenseReportLookup.AssignElements(key))
                    {
                        if (dataEntryReportLookup.BatchSequence == batchSeq)
                        {
                            //we found the batch sequence we are working on
                             if (HTMarkSenseDataEntryTable.ContainsKey(key))
                             {
                                if (bool.TryParse(_htMarkSenseDataEntryTable[key].ToString(), out valueOfMarkSense))
                                {
                                    if (valueOfMarkSense)
                                    {
                                        //it is mark sense
                                        sbMsg.Append(markSenseCount.ToString("D2") + dataEntryReportLookup.Keyword);

                                        markSenseFieldInfo.Add(sbMsg.ToString(), HTDataEntryData[key].ToString());
                                        markSenseCount++;
                                    }
                                    else
                                    {
                                        //not mark sense
                                        sbMsg.Append(itemFieldCount.ToString("D2") + " " + dataEntryReportLookup.Keyword);
                                        dataEntryFieldInfo.Add(sbMsg.ToString(), HTDataEntryData[key].ToString());
                                        itemFieldCount++;
                                    }
                                }
                            }
                        }
                    }
                }
                PDFAttribInfo.ItemFieldsDistanceFromTop = PDFAttribInfo.CurrentDistanceFromTop;
                FillOutGrid(cbDetail, "Item Fields:", dataEntryFieldInfo, false);
                var currentDistanceFromTop = PDFAttribInfo.CurrentDistanceFromTop;
                PDFAttribInfo.CurrentDistanceFromTop = PDFAttribInfo.ItemFieldsDistanceFromTop;
                FillOutGrid(cbDetail, "Mark Sense Fields:", markSenseFieldInfo, true);
                if (currentDistanceFromTop > PDFAttribInfo.CurrentDistanceFromTop)
                {
                    //mark sense is not as big as Item Fields
                    PDFAttribInfo.CurrentDistanceFromTop = currentDistanceFromTop;
                }
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "AddVariableFieldsInformation");
                throw;
            }
        }

        private void AddItemInformation(PdfContentByte cbDetail, bool bIsCheck, int batchSeq)
        {
            try
            {
                // build Item Information Dictionary
                Dictionary<string, string> itemInformation = new Dictionary<string, string>();

                itemInformation.Add("Batch ID", SourceBatchID.ToString());
                itemInformation.Add("Batch Number", BatchNumber.ToString());
                itemInformation.Add("Transaction Number", GetTransactionNumber(bIsCheck, batchSeq));
                itemInformation.Add("Client ID", LockboxID.ToString());
                itemInformation.Add("Document Group ID", GetHashTableValueItem(HTFactBatchData, "Doc Grp ID"));
                itemInformation.Add("Document ID", GetHashTableValueItem(HTFactItemData, batchSeq, "Document ID"));

                itemInformation.Add("Document Type", GetHashTableValueItem(HTFactItemData, batchSeq, "Document Type"));

                itemInformation.Add("Work Type", GetHashTableValueItem(HTFactBatchData, "Work Type"));
                itemInformation.Add("Work Flow ID", GetHashTableValueItem(HTFactBatchData, "Work Flow ID"));
                itemInformation.Add("Consolidation Number", GetHashTableValueItem(HTFactBatchData, "Consolidation Number"));
                itemInformation.Add("Consolidation Date", GetHashTableValueItem(HTFactBatchData, "Consolidation Date"));
                itemInformation.Add("Reject job", GetHashTableValueItem(HTFactItemData, batchSeq, "Reject Job"));
                itemInformation.Add("ARC Info", GetHashTableValueItem(HTFactItemData, batchSeq, "ARC Reason"));

                PDFAttribInfo.ItemInformationDistanceFromTop = PDFAttribInfo.CurrentDistanceFromTop;
                FillOutGrid(cbDetail, "Item Information:", itemInformation, false);
                //Write table to Detail object
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "AddItemInformation");
                throw;
            }
        }
        private void AddPass1Information(PdfContentByte cbDetail, int batchSeq, bool bIsCheck)
        {
            try
            {
                // build Pass 1 Information Dictionary
                Dictionary<string, string> pass1Information = new Dictionary<string, string>();
                pass1Information.Add("Station/Operator", GetHashTableValueItem(HTFactBatchData, "P1 Station ID") + " / " + GetHashTableValueItem(HTFactBatchData, "P1 Operator ID"));
                pass1Information.Add("Process Date", SourceProcessingDate.ToString("MM/dd/yyyy"));
                pass1Information.Add("Receive Date", GetHashTableValueItem(HTFactBatchData, "Receive Date"));
                pass1Information.Add("System Date", ImmutableDate.ToString("MM/dd/yyyy"));
                pass1Information.Add("Process Type", GetHashTableValueItem(HTFactBatchData, "Process Type"));
                String SequenceNumber = GetSequenceNumber(bIsCheck, batchSeq);
                pass1Information.Add("Pass I Sequence", SequenceNumber);
                //add one item for formatting
                pass1Information.Add(string.Empty, string.Empty);
                PDFAttribInfo.CurrentDistanceFromTop = PDFAttribInfo.ItemInformationDistanceFromTop;
                FillOutGrid(cbDetail, "Pass I Information:", pass1Information, true);
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "AddPass1Information");
                throw;
            }
        }
        private void AddPass2Information(PdfContentByte cbDetail, int batchSeq)
        {
            try
            {
                // build Pass 2 Information Dictionary
                Dictionary<string, string> Pass2Information = new Dictionary<string, string>();
                Pass2Information.Add("Station/Operator", GetHashTableValueItem(HTFactBatchData, "P2 Station ID") + " / " + GetHashTableValueItem(HTFactBatchData, "P2 Operator ID"));
                Pass2Information.Add("Pocket Cut ID (blk-pkt#-cut#)", GetHashTableValueItem(HTFactItemData, batchSeq, "Pocket Cut ID"));
                Pass2Information.Add("Pocket Sequence", GetHashTableValueItem(HTFactItemData, batchSeq, "P2 Pocket Seq Num"));
                Pass2Information.Add("Deposit Date", DepositDate.ToString("MM/dd/yyyy"));
                Pass2Information.Add("Pass II Sequence", GetHashTableValueItem(HTFactItemData, batchSeq, "P2 Sequence"));

                FillOutGrid(cbDetail, "Pass II Information:", Pass2Information, true);
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "AddPass2Information");
                throw;
            }
        }

        private void AddPageHeaderAndFooter(PdfContentByte cbDetail, bool bIsCheck, int batchSeq)
        {
            try
            {
                AddPageHeader(cbDetail, bIsCheck, batchSeq);
                AddPageFooter(cbDetail);
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "AddPageHeaderAndFooter");
                throw;
            }
        }
        private void AddPageFooter(PdfContentByte cbDetail)
        {
            PDFAttribInfo.PageNo++;

            const int pageNumberFontSize = 10;
            cbDetail.BeginText();
            cbDetail.SetFontAndSize(PDFAttribInfo.BFHeading, pageNumberFontSize);
            cbDetail.ShowTextAligned(
                PdfContentByte.ALIGN_RIGHT, "Page " + PDFAttribInfo.PageNo,
                PDFAttribInfo.Right, PDFAttribInfo.Bottom + pageNumberFontSize, 0);
            cbDetail.EndText();

            PDFAttribInfo.CurrentDistanceFromBottom = pageNumberFontSize + 18;
        }
        private string GetLogonName()
        {
            string result = null;

            DataTable dataTable;
            if (SessionDAL.GetSession(_SessionID, out dataTable) &&
                dataTable != null &&
                dataTable.Rows.Count > 0)
            {
                result = dataTable.Rows[0]["LogonName"] as string;
            }

            return result ?? "unknown user";
        }
        protected virtual IEnumerable<string> GetPageSubheaders(bool bIsCheck, int batchSeq)
        {
            yield return string.Format("Deposit Date: {0:MM/dd/yyyy}", DepositDate);
            yield return string.Format("Batch ID: {0}", SourceBatchID);
            yield return string.Format("Batch Number: {0}", BatchNumber);
            yield return string.Format("Transaction Number: {0}", GetTransactionNumber(bIsCheck, batchSeq));
        }
        private void AddPageHeader(PdfContentByte cbDetail, bool bIsCheck, int batchSeq)
        {
            PDFAttribInfo.CurrentDistanceFromTop = 0;
            var aTable = new PdfPTable(1)
            {
                TotalWidth = PDFAttribInfo.Right - PDFAttribInfo.Left,
                WidthPercentage = 100,
            };

            var titleCell = new PdfPCell(new Phrase(ReportTitle, PDFAttribInfo.FontBold12))
            {
                Border = 0,
                HorizontalAlignment = Element.ALIGN_CENTER
            };
            aTable.AddCell(titleCell);

            var reportCreated = string.Format("Report created {0:MM/dd/yyyy} by {1}", ReportDateTime, GetLogonName());
            var reportCreatedCell = new PdfPCell(new Phrase(reportCreated, PDFAttribInfo.Font10))
            {
                Border = 0,
                BorderWidthBottom = 0.5f,
                HorizontalAlignment = Element.ALIGN_CENTER,
            };
            aTable.AddCell(reportCreatedCell);

            foreach (var subheaderText in GetPageSubheaders(bIsCheck, batchSeq))
            {
                var cell = new PdfPCell(new Phrase(subheaderText, PDFAttribInfo.Font10)) {Border = 0};
                aTable.AddCell(cell);
            }

            PDFAttribInfo.CurrentDistanceFromTop = (int)PDFAttribInfo.Top - (int)aTable.WriteSelectedRows(0, -1, PDFAttribInfo.Left, PDFAttribInfo.Top - PDFAttribInfo.CurrentDistanceFromTop, cbDetail);
            PDFAttribInfo.CurrentDistanceFromTop += 27;
        }

        public static void GetImageScale(float imageWidth, float imageHeight,
            float maxWidth, float maxHeight, out float scaledWidth, out float scaledHeight)
        {
            var horizontalScale = maxWidth / imageWidth;
            var verticalScale = maxHeight / imageHeight;

            var useHorizontalScale = (imageHeight * horizontalScale) <= maxHeight;
            var scale = useHorizontalScale ? horizontalScale : verticalScale;

            scaledWidth = imageWidth * scale;
            scaledHeight = imageHeight * scale;
        }
        #region Helper Methods

        private void FillOutGrid(PdfContentByte cbDetail, string header, Dictionary<string, string> info, bool alignRight)
        {
             PdfPCell rowData;
             PdfPCell headerCell;
             PdfPTable aTable;
             PdfPCell rowLabel;
             float xPos = 0;
             int iNumberOfColumns = 2;

             //populate info about grid
             rowLabel = new PdfPCell(new Phrase());
             rowLabel.HorizontalAlignment = Element.ALIGN_LEFT;
             rowLabel.Border = 0;

             rowData = new PdfPCell(new Phrase());
             rowData.HorizontalAlignment = Element.ALIGN_RIGHT;
             rowData.Border = 0;

             // Add header
             aTable = new PdfPTable(iNumberOfColumns);
             aTable.TotalWidth = (PDFAttribInfo.Right - PDFAttribInfo.Left) / 2;
             aTable.WidthPercentage = 90;
             aTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
             aTable.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;

             headerCell = new PdfPCell(new Phrase(header, PDFAttribInfo.FontBold12));
             headerCell.Colspan = 2;
             headerCell.Border = 0;
             headerCell.BorderWidthBottom = 1;
             aTable.AddCell(headerCell);

             foreach (KeyValuePair<string, string> pair in info)
             {
                 rowLabel.Phrase = new Phrase(pair.Key, PDFAttribInfo.Font9);
                 aTable.AddCell(rowLabel);
                 rowData.Phrase = new Phrase(pair.Value, PDFAttribInfo.Font9);
                 aTable.AddCell(rowData);
             }
             if (alignRight) {
                xPos = PDFAttribInfo.RightColumn;
             } else  {
                xPos = PDFAttribInfo.Left;
             }

             PDFAttribInfo.CurrentDistanceFromTop = (int)PDFAttribInfo.Top - (int)aTable.WriteSelectedRows(0, -1, xPos, PDFAttribInfo.Top - PDFAttribInfo.CurrentDistanceFromTop, cbDetail);
        }
        private  string GetValueItem(DataTable table, string columnToLookFor)
        {
            string innerStr = string.Empty;
            DataRow row =  null;
            if (table != null && table.Rows.Count > 0)
            {
                row = table.Rows[0];
                if (row.Table.Columns.Contains(columnToLookFor))
                {
                    innerStr = table.Rows[0][columnToLookFor].ToString(); 
                }
            }
            return innerStr;
        }
        private string GetHashTableValueItem(Hashtable htTable, int batchSequence, string keyword)
        {
            cReportKeywordLookup reportKeywordLookup = new cReportKeywordLookup(batchSequence, keyword);
            return GetHashTableValueItem(htTable, reportKeywordLookup.BuildKeyword);
        }
        private string GetHashTableValueItem(Hashtable htTable, string keyword)
        {
            string value = string.Empty;
            if (htTable.ContainsKey(keyword))
            {
                value = htTable[keyword].ToString();
            }

            return value;
        }

        private string BuildKeyWordValue(DataRow dr)
        {
            string keywordValue= string.Empty;
            int batchSequence =0;
            string keyword  = string.Empty;
            cReportKeywordLookup reportKeywordLookup = null;
            if (Int32.TryParse(dr["BatchSequence"].ToString(), out batchSequence))
            {
                keyword = dr["KeyWord"].ToString();
                reportKeywordLookup = new cReportKeywordLookup(batchSequence, keyword);
                keywordValue = reportKeywordLookup.BuildKeyword;
            }
            return keywordValue;
        }

        private string GetAmount(bool bIsCheck, int batchSeq)
        {
            string sAmount = string.Empty;
            cCheck check = null;
            cStub  stub = null;

            if (bIsCheck)
            {
                if (HTCheckData.ContainsKey(batchSeq))
                {
                    check = (cCheck)HTCheckData[batchSeq];
                    sAmount = check.Amount.ToString();
                }
            }
            else
            {
                if (HTStubData.ContainsKey(batchSeq))
                {
                    stub = (cStub)HTStubData[batchSeq];
                    sAmount = stub.Amount.ToString();
                }
            }
            if (!String.IsNullOrEmpty(sAmount))
            {
                //format it properly
                sAmount = Convert.ToDouble(sAmount).ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
            }
            return sAmount;
        }
        private  string GetTransactionNumber(bool bIsCheck, int batchSeq)
        {
            string sTransNum =string.Empty;
            cCheck check = null;
            cStub stub = null;

            if (bIsCheck)
            {
                if (HTCheckData.ContainsKey(batchSeq))
                {
                    check = (cCheck) HTCheckData[batchSeq];
                    sTransNum = check.TxnSequence.ToString();
                }
            }
            else
            {
                if (HTStubData.ContainsKey(batchSeq))
                {
                    stub = (cStub)HTStubData[batchSeq];
                    sTransNum = stub.TxnSequence.ToString();
                }
            }
            return sTransNum;
        }

        protected string GetSequenceNumber(bool bIsCheck, int batchSeq)
        {
            string sTransNum = string.Empty;
            cCheck check = null;
            cStub stub = null;

            if (bIsCheck)
            {
                if (HTCheckData.ContainsKey(batchSeq))
                {
                    check = (cCheck)HTCheckData[batchSeq];
                    sTransNum = check.BatchSequence.ToString();
                }
            }
            else
            {
                if (HTStubData.ContainsKey(batchSeq))
                {
                    stub = (cStub)HTStubData[batchSeq];
                    sTransNum = stub.BatchSequence.ToString();
                }
            }
            return sTransNum;
        }

        private string GetSourceProcessingDateKey(bool bIsCheck, int batchSeq)
        {
            DateTime dteTemp;
            cCheck check = null;
            cStub stub = null;

            if (bIsCheck)
            {
                if (HTCheckData.ContainsKey(batchSeq))
                {
                    check = (cCheck)HTCheckData[batchSeq];
                    if (DateTime.TryParseExact(check.SourceProcessingDateKey.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out dteTemp))
                    {
                        SourceProcessingDate = dteTemp;
                    }
                    else
                    {
                        eventLog.logEvent("Failed to convert sourceProcessingDateKey for check", MessageImportance.Essential);
                    }
                }
            }
            else
            {
                if (HTStubData.ContainsKey(batchSeq))
                {
                    stub = (cStub)HTStubData[batchSeq];
                    if (DateTime.TryParseExact(stub.SourceProcessingDateKey.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out dteTemp))
                    {
                        SourceProcessingDate = dteTemp;
                    }
                    else
                    {
                        eventLog.logEvent("Failed to convert sourceProcessingDateKey for stub", MessageImportance.Essential);
                    }
                }
            }
            return SourceProcessingDate.ToString("MM/dd/yyyy");
        }

        private  string GetAccountNumber(bool bIsCheck, int batchSeq)
        {
            eventLog.logEvent("GetAccountNumber:  bIsCheck=" + bIsCheck.ToString(), this.GetType().Name, MessageType.Information, MessageImportance.Debug);
            string sAccountNum =string.Empty;
            cCheck check = null;

            if (bIsCheck)
            {
                if (HTCheckData.ContainsKey(batchSeq))
                {
                    check = (cCheck) HTCheckData[batchSeq];
                    sAccountNum = check.Account.ToString();
                }
            }

            return sAccountNum;
        }

        internal PDFAttributeInfo PDFAttribInfo
        {
            get
            {
                if (_reportAttribInfo == null)
                {
                    _reportAttribInfo = new PDFAttributeInfo();
                }

                return _reportAttribInfo;
            }
        }

        internal Hashtable HTFactItemData
        {
            get
            {
                if (_htFactItemData == null)
                {
                    _htFactItemData = new Hashtable();
                }
                return (_htFactItemData);
            }
        }
        internal Hashtable HTFactBatchData
        {
            get
            {
                if (_htFactBatchData == null)
                {
                    _htFactBatchData = new Hashtable();
                }
                return (_htFactBatchData);
            }
        }

        internal Hashtable HTCheckData
        {
            get
            {
                if (_htCheckData == null)
                {
                    _htCheckData = new Hashtable();
                }
                return (_htCheckData);
            }
        }
        internal Hashtable HTStubData
        {
            get
            {
                if (_htStubData == null)
                {
                    _htStubData = new Hashtable();
                }
                return (_htStubData);
            }
        }

        internal Hashtable HTMarkSenseDataEntryTable
        {
            get
            {
                if (_htMarkSenseDataEntryTable == null)
                {
                    _htMarkSenseDataEntryTable = new Hashtable();
                }
                return (_htMarkSenseDataEntryTable);
            }
        }

        internal Hashtable HTDataEntryData
        {
            get
            {
                if (_htDataEntryTable == null)
                {
                    _htDataEntryTable = new Hashtable();
                }
                return (_htDataEntryTable);
            }
        }

        private SortedDictionary<int, ReportDocType> BatchSequenceElement
        {
            get
            {
                if (_batchSequenceElement == null)
                {
                    _batchSequenceElement = new SortedDictionary<int, ReportDocType>();
                }
                return (_batchSequenceElement);
            }
        }
        protected int LockboxID
        {
            get;
            set;
        }
        protected int BankID
        {
            get;
            set;
        }
        protected long BatchID
        {
            get;
            set;
        }
        protected long SourceBatchID
        {
            get;
            set;
        }
        protected int BatchNumber
        {
            get;
            set;
        }
        protected int TransactionID
        {
            get;
            set;
        }
        protected int BatchSequence
        {
            get;
            set;
        }
        public int DepositDateKey
        {
            get { return (Convert.ToInt32(DepositDate.ToString("yyyyMMdd"))); }
        }
        protected DateTime DepositDate
        {
            get;
            set;
        }
        public int ProcessingDateKey
        {
            get { return (Convert.ToInt32(ProcessingDate.ToString("yyyyMMdd"))); }
        }
        protected DateTime ProcessingDate
        {
            get;
            set;
        }
        public int SourceProcessingDateKey
        {
            get { return (Convert.ToInt32(SourceProcessingDate.ToString("yyyyMMdd"))); }
        }
        protected DateTime SourceProcessingDate
        {
            get;
            set;
        }

        public DateTime ImmutableDate { get; set; }

        protected void GetBatchParams(XmlNode nodeReport)
        {
            int iTemp = 0;
            long lTemp = 0;
            DateTime dteTemp;
            try
            {
                if (nodeReport.Attributes["BankID"] != null && int.TryParse(nodeReport.Attributes["BankID"].Value, out iTemp))
                {
                    BankID = iTemp;
                }
                else
                {
                    throw (new Exception("Invalid BankID"));
                }

                if (nodeReport.Attributes["LockboxID"] != null && int.TryParse(nodeReport.Attributes["LockboxID"].Value, out iTemp))
                {
                    LockboxID = iTemp;
                }
                else
                {
                    throw (new Exception("Invalid LockboxID"));
                }

                if (nodeReport.Attributes["BatchID"] != null && long.TryParse(nodeReport.Attributes["BatchID"].Value, out lTemp))
                {
                    BatchID = lTemp;
                }
                else
                {
                    throw (new Exception("Invalid BatchID"));
                }

                if (nodeReport.Attributes["BatchNumber"] != null && int.TryParse(nodeReport.Attributes["BatchNumber"].Value, out iTemp))
                {
                    BatchNumber = iTemp;
                } else
                {
                    throw (new Exception("Invalid BatchNumber"));
                }

                if (nodeReport.Attributes["DepositDate"] != null && DateTime.TryParse(nodeReport.Attributes["DepositDate"].Value, out dteTemp))
                {
                    DepositDate = dteTemp;
                }
                else
                {
                    throw (new Exception("Invalid DepositDate"));
                }
                if (nodeReport.Attributes["ProcessingDate"] != null && DateTime.TryParse(nodeReport.Attributes["ProcessingDate"].Value, out dteTemp))
                {
                    ProcessingDate = dteTemp;
                }
                else
                {
                    throw (new Exception("Invalid ProcessingDate"));
                }

                if (nodeReport.Attributes["TransactionID"] != null && int.TryParse(nodeReport.Attributes["TransactionID"].Value, out iTemp))
                {
                    TransactionID = iTemp;
                }
                else
                {
                    throw (new Exception("Invalid TransactionID"));
                }
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "GetBatchParams");
                throw;
            }
        }
        #endregion
    }
}
