using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     06/01/2004
*
* Purpose:  Transaction Job Request base class.
*
* Modification History
*   01/14/2009 CR 99999 ABC 
*       - Initial release.
*   06/01/2004 CR 7728 JMC
*       - Created Class
*   03/03/2005 CR 10537 JMC
*       - Replaced ProcessingInstructions structure with cTransaction class
*       - Modified GetTransactionChecks method to use new cTransaction class
*       - Added protected getBatchTransactions method
*   05/10/2005 CR 12276 JMC
*	    - Added Sequence property to the cTransaction class.
*	    - Added IComparable interface to the cTransaction class.  The sort is on
*         GlobalBatchID, Transactions.Sequence.
*   11/9/2011 CR 47984 WJS
*		- Add method GetBatchChecks and GetBatchDocuments 
*   11/18/2011 CR 48387 WJS
*		- Add filter on GetBatchChecks and GetBatchDocuments
*	03/20/2012 CR 50642 JNE
*	    - Overloaded GetTransactionChecks to include SerialNumber as input
*	    - used to separate out the images for multi-check transaction
*	07/18/2012 CR 52276 JNE
*	    - GetTransactionChecks (..., SerialNumber) not used by zip download so method was removed.
*   08/08/2012 CR 54169 JNE
*       - getBatchTransactions - Add BatchNumber    
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices*
* WI 143274 SAS 05/21/2014
*   Changes done to change the datatype of BatchID from int to long
* WI 148949 DJW 06/20/2014
*   Changes done to add SessionID for RAAM
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

    /// <summary>
    /// Base class used for job classes that work at either the GlobalBatch or
    /// Transactional level.
    /// </summary>
    public abstract class _TransactionalJobRequest : _JobRequest {



        /// <author>Joel Caples</author>
        /// <summary>
        /// Retrieves an arry of checks belonging to a specified Batch or 
        /// Transaction.
        /// </summary>
        /// <param name="transaction">
        /// Structure containing Batch/Transaction ID's.
        /// </param>
        /// <returns>Array of cCheck objects.</returns>
        protected cCheck[] GetTransactionChecks(cTransaction transaction) {

            cCheck check;
            DataTable dt;
            bool bolRetVal;

            ArrayList checks=new ArrayList();


            bolRetVal = ItemProcDAL.GetTransactionChecks(base.sessionID,transaction.BankID, 
                                                         transaction.LockboxID, 
                                                         transaction.DepositDate, 
                                                         transaction.BatchID, 
                                                         transaction.TransactionID,            
                                                         out dt);

            if (bolRetVal && (dt.Rows.Count > 0)) {

                for(int j=0;j<dt.Rows.Count;++j) {

                    System.Data.DataRow dr = dt.Rows[j];

                    check=new cCheck();
                    check.LoadDataRow(ref dr);
                    checks.Add(check);
                }
            }

            dt.Dispose();

            return((cCheck[])checks.ToArray(typeof(cCheck)));
        }
        /// <author>Joel Caples</author>
        /// <summary>
        /// Retrieves an arry of documents belonging to a specified Batch or 
        /// Transaction.
        /// </summary>
        /// <param name="transaction">
        /// Structure containing Batch/Transaction ID's.
        /// </param>
        /// <param name="displayScannedCheck">
        /// Value indicating whether to supress documents with a type of 'SC'.
        /// </param>
        /// <returns>Array of cDocument objects.</returns>
        protected cDocument[] GetTransactionDocuments(cTransaction transaction
                                                    , bool displayScannedCheck) {

            cDocument document;
            DataTable dt;
            bool bolRetVal;
            DataRow dr;


            if(transaction.TransactionID > -1) {
                bolRetVal = ItemProcDAL.GetTransactionDocuments(base.sessionID, transaction, 
                                                                displayScannedCheck, 
                                                                out dt);
            } else {
                bolRetVal = ItemProcDAL.GetTransactionDocumentsBySequence(base.sessionID, transaction, 
                                                                          displayScannedCheck, 
                                                                          out dt);
            }


            ArrayList documents=new ArrayList();

            if (bolRetVal && (dt.Rows.Count > 0)) {

                for(int j=0;j<dt.Rows.Count;++j) {

                    dr = dt.Rows[j];

                    document=new cDocument();
                    document.LoadDataRow(ref dr);
                    documents.Add(document);
                }
            }

            dt.Dispose();

            return((cDocument[])documents.ToArray(typeof(cDocument)));
        }



        protected bool getBatchTransactions(int BankID, 
                                            int LockboxID, 
                                            DateTime DepositDate, 
                                            long BatchID, 
                                            out ArrayList batchTransactions) {
            DataTable dt;
            bool bolRetVal;
            cTransaction procTemp;           

            batchTransactions = new ArrayList();

            bolRetVal = ItemProcDAL.GetBatchTransactions(base.sessionID, BankID, LockboxID, DepositDate, BatchID, out dt);

            if (bolRetVal && (dt.Rows.Count > 0)) {
                for(int j=0;j<dt.Rows.Count;++j) {
                    DataRow dr = dt.Rows[j];

                    procTemp = new cTransaction(ipoLib.NVL(ref dr, "BankID", -1),
                                                ipoLib.NVL(ref dr, "LockboxID", -1),
                                                //ipoLib.NVL(ref dr, "GlobalBatchID", -1),
                                                ipoLib.NVL(ref dr, "DepositDate", DateTime.MinValue),
                                                ipoLib.NVL(ref dr, "BatchID", (long)-1),
                                                ipoLib.NVL(ref dr, "BatchNumber", -1),                                                
                                                ipoLib.NVL(ref dr, "TransactionId", -1),
                                                ipoLib.NVL(ref dr, "TxnSequence", -1));

                    batchTransactions.Add(procTemp);
                }
            } else {
                bolRetVal = false;
            }

            dt.Dispose();
    
            return(bolRetVal);
        }

        /// <summary>
        /// Retrieves an array of checks belonging to a specified Batch 
        /// </summary>
        /// <param name="document">
        /// Structure containing Batch
        /// </param>
        /// <param name="transactionList"></param>
        /// <returns>Array of cCheck objects.</returns>
        protected cCheck[] GetBatchChecks(cDocument document, List<int> transactionList)
        {


            ArrayList checks = new ArrayList();
           
            DataTable dt = null;
            try
            {
                bool bolRetVal;
                cCheck check;

                bolRetVal = ItemProcDAL.GetBatchChecks(base.sessionID,document.BankID,
                                                             document.LockboxID,
                                                             document.DepositDate,
                                                             document.BatchID,
                                                             out dt);
                if (bolRetVal && (dt.Rows.Count > 0))
                {

                    for (int j = 0; j < dt.Rows.Count; ++j)
                    {

                        DataRow dr = dt.Rows[j];
                        int transactionID = (int)dr["TransactionID"];
                        if (transactionList.Contains(transactionID))
                        {
                            check = new cCheck();
                            check.LoadDataRow(ref dr);
                            checks.Add(check);
                        }
                    }
                }
            }
            finally{
                if (dt != null)
                {
                    dt.Dispose();
                }

            }

            return ((cCheck[])checks.ToArray(typeof(cCheck)));
        }

        /// <summary>
        /// Retrieves an arrya of documents belonging to a specified Batch 
        /// </summary>
        /// <param name="doc">
        /// Structure containing Batch
        /// </param>
        /// <param name="displayScannedCheck">
        /// Value indicating whether to supress documents with a type of 'SC'.
        /// </param>
        ///  <param name="transactionList"></param>
        /// <returns>Array of cDocument objects.</returns>
        protected cDocument[] GetBatchDocuments(cDocument doc
                                                    , bool displayScannedCheck
                                                    , List<int> transactionList)
        {

            ArrayList documents = new ArrayList();
            DataTable dt = null;
            bool bolRetVal;
            try{
                
                    cDocument document;
                    DataRow dr;
                    bolRetVal = ItemProcDAL.GetBatchDocuments(base.sessionID, doc.BankID,
                                                             doc.LockboxID,
                                                             doc.DepositDate,
                                                             doc.BatchID,
                                                                    displayScannedCheck,
                                                                    out dt);
                    

                    if (bolRetVal && (dt.Rows.Count > 0))
                    {

                        for (int j = 0; j < dt.Rows.Count; ++j)
                        {

                            dr = dt.Rows[j];
                            int transactionID = (int)dr["TransactionID"];
                            if (transactionList.Contains(transactionID))
                            {
                                document = new cDocument();
                                document.LoadDataRow(ref dr);
                                documents.Add(document);
                            }
                        }
                   }
             } finally {
                if (dt != null)
                {
                    dt.Dispose();
                }

            }

            return ((cDocument[])documents.ToArray(typeof(cDocument)));
        }
    }
}
