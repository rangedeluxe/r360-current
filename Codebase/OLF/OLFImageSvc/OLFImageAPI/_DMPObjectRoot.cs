using System;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.Common.Log;
using WFS.RecHub.DAL.OLFServicesDAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     11/20/2003
*
* Purpose:  IntegraPAY Online Services Image Creation Server Object.
*
* Modification History
* JMC 11/20/2003
*    -Initial release.
* CR 7728 JMC 06/01/2004
*    -Added module-level PICS object for consumption by classes inheriting from the
*     class.
*    -Added additional comments to function headers.
* CR 10537 JMC 03/03/2005
*    -Added activityType constants
*    -Added LogActivity methods to log to the SessionActivityLog table.
* CR 14209 JMC 11/02/2005
*    -Added code to assign SetDeadlockPriority property to database object
*     after creation.
* CR 14211 JMC 11/04/2005
*    -Added code to assign QueryRetryAttempts property to database object
*     after creation.
* CR 23584 MEH 05/20/2009
*    -integraPAY Online Interface Into Hyland IMS Archive
* CR 32562 JMC 03/02/2011 
*    -Modified InsertSessionActivityLog parameter list order to match changes
*     in the DAL.
* CR 45627 JMC 07/12/2011
*    -Removed module-level ipoImages object.
* CR 52276 JNE 05/15/2012
*    -Added SearchDAL 
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

    /// <summary>
    /// ipo Image Service Object Root class.
    /// </summary>
	public abstract class _DMPObjectRoot : IDisposable
	{
        // Track whether Dispose has been called.
        private bool disposed = false;

        private cImageServiceOptions _ServerOptions = null;
        private cSiteOptions _SiteOptions = null;
        private cEventLog _EventLog = null;

        protected cItemProcDAL _ItemProcDAL = null;
        protected cBatchDataDAL _BatchDataDAL = null;
        private cOLFServicesDAL _olfServicesDal;
        protected cSessionDAL _SessionDAL = null;
        protected cSearchDAL _SearchDAL = null;

        private string _SiteKey;
        
        /// <summary>
        /// Name of the section header in the local .ini file that contains Image
        /// Service options.
        /// </summary>
        public string serverKey
        {
            get { return ipoLib.INISECTION_IMAGE_SERVICE; }
        }

        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        public string siteKey
        {
            set { _SiteKey = value; }
            get { return _SiteKey; }
        }

        /// <summary>
        /// Object that contains Image Service options from the local .ini file.
        /// </summary>
        internal cImageServiceOptions serverOptions
        {
            get
            {
                if (_ServerOptions == null) { 
                    _ServerOptions = new cImageServiceOptions(this.serverKey); 
                }

                return _ServerOptions;
            }
        }


        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        internal cSiteOptions siteOptions
        {
            get
            {
                if (_SiteOptions == null)
                    { _SiteOptions = new cSiteOptions(this.siteKey); }

                return _SiteOptions;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        internal cEventLog eventLog
        {
            get
            {
                if (_EventLog == null) { 
                    _EventLog = new cEventLog(siteOptions.logFilePath
                                                , siteOptions.logFileMaxSize
                                                , (MessageImportance)siteOptions.loggingDepth); 
                }

                return _EventLog;
            }
        }

        protected cItemProcDAL ItemProcDAL {
            get {
                if(_ItemProcDAL == null) {
                    _ItemProcDAL = new cItemProcDAL(this.siteKey);
                }
                return(_ItemProcDAL);
            }
        }

        protected cBatchDataDAL BatchDataDAL {
            get {
                if (_BatchDataDAL == null) {
                    _BatchDataDAL = new cBatchDataDAL(this.siteKey);
                }
                return (_BatchDataDAL);
            }
        }

        protected cOLFServicesDAL OlfServicesDal => _olfServicesDal ?? (_olfServicesDal = new cOLFServicesDAL(siteKey));

        protected cSessionDAL SessionDAL {
            get {
                if(_SessionDAL == null) {
                    _SessionDAL = new cSessionDAL(this.siteKey);
                }
                return(_SessionDAL);
            }
        }

        protected cSearchDAL SearchDAL {
            get {
                if(_SearchDAL == null) {
                    _SearchDAL = new cSearchDAL(this.siteKey);
                }
                return(_SearchDAL);
            }
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output messages.
        /// </summary>
        /// <param name="message">Test message to be logged.</param>
        /// <param name="src">Source of the event.</param>
        /// <param name="messageType">MessageType(Information, Warning, Error)</param>
        /// <param name="messageImportance">MessageImportance(Essential, Verbose, Debug)
        /// </param>
        protected void object_outputMessage(string message
                                          , string src
                                          , MessageType messageType
                                          , MessageImportance messageImportance)
        {
            eventLog.logEvent(message
                            , src
                            , messageType
                            , messageImportance);
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output errors.
        /// </summary>
        /// <param name="e"></param>
        protected void object_outputError(Exception e)
        {
            eventLog.logError(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="activityCode"></param>
        /// <param name="sessionID"></param>
        /// <param name="onlineImageQueueID"></param>
        /// <param name="itemCount"></param>
        protected void LogActivity(ActivityCodes activityCode, Guid sessionID, Guid onlineImageQueueID
                                 , int itemCount) {

            int intRowsAffected;

            if(itemCount > 0) {
                SessionDAL.InsertSessionActivityLog(sessionID, 
                                                    "ipoImage", 
                                                    activityCode, 
                                                    onlineImageQueueID, 
                                                    itemCount, 
                                                    -1, 
                                                    -1,
                                                    out intRowsAffected);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="activityCode"></param>
        /// <param name="sessionID"></param>
        /// <param name="onlineImageQueueID"></param>
        /// <param name="itemCount"></param>
        /// <param name="bankID"></param>
        /// <param name="lockboxID"></param>
        protected void LogActivity(ActivityCodes activityCode, Guid sessionID, Guid onlineImageQueueID
                                 , int itemCount, int bankID, int lockboxID) {
            
            int intRowsAffected;

            if(itemCount > 0) {
                SessionDAL.InsertSessionActivityLog(sessionID, 
                                                    "ipoImage", 
                                                    activityCode, 
                                                    onlineImageQueueID,
                                                    itemCount, 
                                                    bankID, 
                                                    lockboxID, 
                                                    out intRowsAffected);
            }
        }

        #region Dispose
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_ItemProcDAL != null)
                    {
                        _ItemProcDAL.Dispose();
                    }
                    if (_SearchDAL != null)
                    {
                        _SearchDAL.Dispose();
                    }
                    if (_SessionDAL != null)
                    {
                        _SessionDAL.Dispose();
                    }
                    if (_BatchDataDAL != null)
                    {
                        _BatchDataDAL.Dispose();
                    }
                    _olfServicesDal?.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }
        #endregion
    }
}
