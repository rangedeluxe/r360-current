using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     05/15/2003
*
* Purpose:  IntegraPAY Online Image service image not found error handler
*
* Modification History
*   05/15/2003 CR JMC
*       - Created class
*   11/20/2003 CR JMC
*       - First major enhancement : 
*          1.)Added Strong-Naming to the assembly.
*          2.)Changed the namespace of the class to narrow the scope.
*          3.)Modified Comments to utilize C# Xml format.
*          4.)Changed scope of eImageNotFoundException, and 
*             eInvalidImageException to be internal.
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {
    /*******************************************************************************
    * Application: InegraPAY Online Services Image Creation Server Object
    *       Class: eImageNotFoundException
    *    Filename: ipoException.cs
    *      Author: 
    *
    * Revisions:
    ******************************************************************************/
    /// <author>Joel Caples</author>
    /// <exception cref="eImageNotFoundException">
    /// ipo Image Service eImageNotFoundException object class.
    /// </exception>
    [Serializable]
    internal class eImageDirectoryNotFoundException : Exception
    {
        private string _DirectoryPath;

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="directoryPath"></param>
        //************************************************************************
        public eImageDirectoryNotFoundException(string directoryPath)
        {
            _DirectoryPath = directoryPath;
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public override string Message
        {
            get { 
                return ("Image Directory specified was either not found or permission to "
                      + "access file was denied : " + _DirectoryPath); 
            }
        }
    }

    /*******************************************************************************
    * Application: InegraPAY Online Services Image Creation Server Object
    *       Class: eImageNotFoundException
    *    Filename: ipoException.cs
    *      Author: 
    *
    * Revisions:
    ******************************************************************************/
    /// <author>Joel Caples</author>
    /// <exception cref="eImageNotFoundException">
    /// ipo Image Service eImageNotFoundException object class.
    /// </exception>
    [Serializable]
    internal class eImageNotFoundException : Exception
    {
        private string _FilePath;

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="filePath"></param>
        //************************************************************************
        public eImageNotFoundException(string filePath)
        {
            _FilePath = filePath;
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public override string Message
        {
            get { 
                return ("Image specified was either not found or permission to "
                      + "access file was denied : " + _FilePath); 
            }
        }
    }



    /*******************************************************************************
    * Application: InegraPAY Online Services Image Creation Server Object
    *       Class: eInvalidImageException
    *    Filename: ipoException.cs
    *      Author: Joel Caples
    *
    * Revisions:
    * 05.15.2003 Joel Caples   
    *   -Created class
    * 11.20.2003 Joel Caples
    *   -First major enhancement : 
    *       1.)Added Strong-Naming to the assembly.
    *       2.)Changed the namespace of the class to narrow the scope.
    *       3.)Modified Comments to utilize C# Xml format.
    ******************************************************************************/
    /// <author>Joel Caples</author>
    /// <exception cref="eInvalidImageException">
    /// ipo Image Service eInvalidImageException object class.
    /// </exception>
    [Serializable]
    internal class eInvalidImageException : Exception
    {
        private string _FilePath;
		private string _ImageType;

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="imageType"></param>
        //************************************************************************
        public eInvalidImageException(string filePath
                                    , string imageType)
        {
            _FilePath = filePath;
			_ImageType = imageType;
		}

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        //************************************************************************
		public eInvalidImageException(string filePath)
		{
			_FilePath = filePath;
			_FilePath = "Unknown type";
		}

        //************************************************************************
        /// <author>Mark Horwich</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public eInvalidImageException()
		{
			_FilePath = "Unknown path";
			_FilePath = "Unknown type";
		}        

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public override string Message
        {
            get { 
                return ("Unable to open image file.  Image may be corrupted or"
                      + " of an invalid file format (" + _ImageType + ") : " 
                      + _FilePath); 
            }
        }
    }
}
