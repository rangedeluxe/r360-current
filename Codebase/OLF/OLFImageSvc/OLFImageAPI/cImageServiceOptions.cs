using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     06/14/2005
*
* Purpose:  IntegraPAY Online Image Service options class.
*
* Modification History
*   06/14/2005 CR 11465 JMC
*       - Modified class to extract MaxThreads from the local .ini file and expose
*         it's value as a property.
* CR 28522 JMC 01/06/2010
*   - Class now inherits from cSiteOptions
* CR 54779 CEJ 10/23/2012 
*   - Change the image service to allow it to be hosted as a Windows service
* WI 98042 WJS 4/19/2013
*   - Remove hosting of OLF Servie from image service
* WI 96037 CRG 04/10/2013
*   - Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 123291 TWE 11/22/2013
*   - (FP) Add ini setting to control use of Curly brackets in file name (csv)
* WI 71542 BLR 05/07/2014    
*   - Removed ipoTemplatesDir & ipoResourcesDir, these are now request-level,
*     not server-level.   
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

	/// <summary>
	/// Summary description for ipoImageServerOptions.
	/// </summary>
	public class cImageServiceOptions : cSiteOptions {

        private string _OnlineArchivePath = "";
        private long _MaxThreads = 10;
        private bool _CurlyBrackets = false;

        private const string INIKEY_ONLINE_ARCHIVE_PATH = "OnlineArchivePath";
        private const string INIKEY_IPO_TEMPLATES_DIR = "ipoTemplatesDir";
        private const string INIKEY_IPO_RESOURCE_DIR = "ipoResourceDir";
        private const string INIKEY_MAX_THREADS = "MaxThreads";
        const string INIKEY_USECURLYBRACKETS = "UseCurlyBrackets";

        //************************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteKey"></param>
        //************************************************************************
		public cImageServiceOptions(string siteKey) : base(siteKey) 
        {
            StringCollection colSiteOptions = ipoINILib.GetINISection(siteKey);
            string strKey;
            string strValue;
            long lngMaxThreads;

            System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {
                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if(strKey.ToLower() == INIKEY_ONLINE_ARCHIVE_PATH.ToLower()) {
                    _OnlineArchivePath = ipoLib.cleanPath(strValue); 
                }
                else if(strKey.ToLower() == INIKEY_MAX_THREADS.ToLower()) {
                    try {
                        lngMaxThreads = System.Convert.ToInt64(strValue); 
                        if(lngMaxThreads > 0) {
                            _MaxThreads = lngMaxThreads;
                        } else {
                            _MaxThreads = 10;
                        }
                    }
                    catch { _MaxThreads = 10; }
                }
                else
                    if (strKey.ToLower() == INIKEY_USECURLYBRACKETS.ToLower())
                    {
                        if (strValue.ToLower() == "true")
                        {
                            _CurlyBrackets = true;
                        }
                        else
                        {
                            _CurlyBrackets = false;
                        }
                    }
            }
		}


        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// Returns path where Online generated image files are stored based on 
        /// the local INI file.
        /// </summary>
        //************************************************************************
        public string onlineArchivePath {
            get {
                return (_OnlineArchivePath);
            }
        }

        /// <summary>
        /// WI 71542 : Directory can now exist multiple times in the INI file.
        /// </summary>
        /// <param name="sitekey"></param>
        /// <returns></returns>
        public string GetIpoTemplatesDirectory(string sitekey)
        {
            return ipoINILib.IniReadValue(sitekey, INIKEY_IPO_TEMPLATES_DIR);
        }

        /// <summary>
        /// WI 71542 : Directory can now exist multiple times in the INI file.
        /// </summary>
        /// <param name="sitekey"></param>
        /// <returns></returns>
        public string GetIpoResourcesDirectory(string sitekey)
        {
            return ipoINILib.IniReadValue(sitekey, INIKEY_IPO_RESOURCE_DIR);
        }

        //************************************************************************
        /// <author>Joel Caples</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public long MaxThreads {
            get {
                return(_MaxThreads);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool isCurlyBracketsEnabled
        {
            get
            {
                if (_CurlyBrackets)
                {
                    return true;
                }
                return false;
            }
        }
	}
}
