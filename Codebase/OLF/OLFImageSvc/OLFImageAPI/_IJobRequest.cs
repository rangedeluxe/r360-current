using System;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     11/20/2003
*
* Purpose:  IntegraPAY Online Services Image Creation Server Object.
*
* Modification History
*   11/20/2003 JMC
*       -Created class
*   03/03/2005 CR 10537 JMC
*       -Added sessionID Guid property
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

    /// <author>Joel Caples</author>
    /// <summary>
    /// ipo Image Service Job Request interface.
    /// </summary>
	internal interface _IJobRequest
	{
        event jobStatusChangedEventHandler jobStatusChangedEvent;
        void processJob();

        string XMLDoc
        {
            set;
            get;
        }

        Guid sessionID
        {
            set;
            get;
        }

        string siteKey
        {
            set;
            get;
        }

        Guid jobID
        {
            set;
            get;
        }

        string fileName
        {
            set;
            get;
        }

        OLFJobStatus status
        {
            get;
        }
	}
}
