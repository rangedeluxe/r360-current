using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Xml;
using iTextSharp.text.pdf;
using WFS.RecHub.Common;
using WFS.RecHub.ItemProcDAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     06/01/2004
*
* Purpose:  IntegraPAY Online Image View Specified Image.
*
* Modification History
* 01/14/2009 CR 99999 ABC 
*    -Initial release.
* 06/01/2004 CR 7728 JMC
*    -NEW CLASS - used for "View Single" requirement.
* 03/03/2005 CR 10537 JMC
*    -Modified processJob method to log Activities.
*    -Removed commented code from appendImagesToPdf function.
*    -Modified appendImagesToPdf function to return out parm from 
*     appendImage function.
* 05/23/2008 CR 23661 JCS
*    -Modified function appendImagesToPdf to optionally include RemitterName in check details
* 05/23/2008 CR 24043 JCS
*    -Modified appendImagesToPdf function to display check front and back on single page.
* 05/20/2009 CR 23584 MEH
*    -integraPAY Online Interface Into Hyland IMS Archive
* CR 32562 JMC 03/02/2011 
*    -Changed type of local images object from cImage[] to IItem[]
*    -Modified buildImageArrays() method to accept IItem[] parameter instead of cImage[] parameter.
*    -Modified appendImagesToPdf() method to accept IItem[] parameter instead of cImage[] parameter.
* CR 45627 JMC 07/12/2011
*    -Added a Using{} clause when accessing ipoImages to ensure Dispose() is 
*     called.
* CR 50750 JNE 03/01/2012
*    -Removed (bRet) in cJobRequestViewSpecified in appendImagesToPdf 
* CR 50733 JMC 03/04/2012
*    -Refactored object to use iTextSharp instead of ABCPdf.Net
*    -Removed unused ItemProc code.
* CR 54169 JNE 08/08/2012
*    -processJob - Added bDisplayBatchID
*    -appendImagesToPdf - Added displayBatchID
* CR 52959 JNE 10/01/2012
*    -appendImagesToPdf - Added FileGroup code when determining root image path. 
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 121672 MLH 11/26/2013
*   Added OLClientAccount DisplayLabel formatted with DDA  See WI 121666 Attachment
*   for v2.0 limitations.
*   Removed USE_ITEMPROC code
* WI 71542 BLR 05/07/2014
*   Using the request's sitekey for Templates and Resources directories.
*
* WI 143274 SAS 05/21/2014
*   Changes done to change the datatype of BatchID from int to long
* WI 148949 DJW 06/20/2014
*   Changes done to add SessionID for RAAM
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI
{

    /// <summary>
    /// Summary description for cJobRequestViewSpecified.
    /// </summary>
    internal class cJobRequestViewSpecified : _JobRequest, _IJobRequest
    {

        PdfWriter pdfWriter;

        public cJobRequestViewSpecified()
        {
        }

        public void processJob()
        {

            string completedFilePath = serverOptions.onlineArchivePath
                                        + this.jobID
                                        + "." + FILE_EXT_PDF;

            JobRequestOptions sctJobRequestOptions;
            IItem[] images = null;
            jobRequestEventArgs ea;
            XmlNode nodeImages;
            ciTextSharpDoc objiTextSharpDoc = null;
            long lngLOF;
            bool bDisplayBatchID = false;
            bool bTemp = false;

            try
            {
                getJobRequestOptions(nodeJobRequest(),
                    () => new ImageDisplayModes(OLFImageDisplayMode.BothSides, OLFImageDisplayMode.BothSides),
                    out sctJobRequestOptions);

                ea = new jobRequestEventArgs();
                changeStatus(OLFJobStatus.Processing, ea);

                nodeImages = nodeJobRequest().SelectSingleNode("images");

                // Set displayBatchID from node
                if (!(nodeImages == null))
                {
                    if (nodeImages.SelectSingleNode("Check") != null)
                    {
                        if ((nodeImages.SelectSingleNode("Check").Attributes["DisplayBatchID"] != null)
                            && (bool.TryParse(nodeImages.SelectSingleNode("Check").Attributes["DisplayBatchID"].Value, out bTemp)))
                        {
                            bDisplayBatchID = bTemp;
                        }
                    }
                    else if (nodeImages.SelectSingleNode("Document") != null)
                    {
                        if ((nodeImages.SelectSingleNode("Document").Attributes["DisplayBatchID"] != null)
                            && (bool.TryParse(nodeImages.SelectSingleNode("Document").Attributes["DisplayBatchID"].Value, out bTemp)))
                        {
                            bDisplayBatchID = bTemp;
                        }
                    }
                }
                // Create the Pdf
                objiTextSharpDoc = new ciTextSharpDoc();
                objiTextSharpDoc.outputError += new outputErrorEventHandler(object_outputError);

                // Create a new PdfWriter object, specifying the output stream
                using (var output = new FileStream(completedFilePath, FileMode.Create))
                {
                    pdfWriter = PdfWriter.GetInstance(objiTextSharpDoc, output);

                    // Open the Document for writing
                    objiTextSharpDoc.Open();

                    if (nodeImages != null && (nodeImages.ChildNodes.Count > 0))
                    {
                        buildImageArrays(ref nodeImages, out images);
                    }

                    if (images != null)
                    {
                        appendImagesToPdf(ref images, ref objiTextSharpDoc, sctJobRequestOptions, bDisplayBatchID);
                    }

                    // Save PDF Document
                    objiTextSharpDoc.Close();
                }

                eventLog.logEvent("PDF Document created : " + completedFilePath
                                , this.GetType().Name
                                , MessageImportance.Essential);
                eventLog.logEvent("Completed Processing Job : " + this.jobID
                                , this.GetType().Name
                                , MessageImportance.Essential);

                if ((images != null) && (images.Length > 0))
                {
                    for (int i = 0; i < images.Length; ++i)
                    {
                        LogActivity(ActivityCodes.acViewSingle, this.sessionID, this.jobID, 1, images[i].BankID, images[i].LockboxID);
                    }
                }

                lngLOF = ipoLib.LOF(completedFilePath);

                ea.fileSize = lngLOF;
                changeStatus(OLFJobStatus.Completed, ea);

            }
            catch (Exception e)
            {

                eventLog.logError(e);

                try
                {
                    if (objiTextSharpDoc != null)
                    {
                        objiTextSharpDoc.Close();
                    }
                }
                catch { }

                try
                {
                    if (File.Exists(completedFilePath))
                    {
                        File.Delete(completedFilePath);
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeImages"></param>
        /// <param name="images">
        ///     An array of type cImage is required to pass to the remote image service.
        /// </param>
        private void buildImageArrays(ref XmlNode nodeImages
                                    , out IItem[] images)
        {

            bool bolRetVal;

            cCheck check;
            cDocument document;
            DataTable dt;
            XmlNode nodeImage;

            DataRow dr;

            int intBankID;
            int intLockboxID;
            DateTime dteDepositDate;
            long longBatchID;
            int intTransactionID;
            int intBatchSequence;
            bool bolDisplayScannedCheck;

            int intTemp;
            long longTemp;
            DateTime dteTemp;
            images = null;

            ArrayList objImages = new ArrayList();

            for (int imageIndex = 0; imageIndex < nodeImages.ChildNodes.Count; ++imageIndex)
            {

                nodeImage = nodeImages.ChildNodes[imageIndex];

                switch (nodeImage.Name.ToLower())
                {
                    case "check":

                        if (nodeImage.Attributes["BankID"] != null && int.TryParse(nodeImage.Attributes["BankID"].Value, out intTemp))
                        {
                            intBankID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BankID"));
                        }

                        if (nodeImage.Attributes["LockboxID"] != null && int.TryParse(nodeImage.Attributes["LockboxID"].Value, out intTemp))
                        {
                            intLockboxID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid LockboxID"));
                        }

                        if (nodeImage.Attributes["BatchID"] != null && long.TryParse(nodeImage.Attributes["BatchID"].Value, out longTemp))
                        {
                            longBatchID = longTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchID"));
                        }

                        if (nodeImage.Attributes["DepositDate"] != null && DateTime.TryParse(nodeImage.Attributes["DepositDate"].Value, out dteTemp))
                        {
                            dteDepositDate = dteTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid DepositDate"));
                        }

                        if (nodeImage.Attributes["TransactionID"] != null && int.TryParse(nodeImage.Attributes["TransactionID"].Value, out intTemp))
                        {
                            intTransactionID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid TransactionID"));
                        }

                        if (nodeImage.Attributes["BatchSequence"] != null && int.TryParse(nodeImage.Attributes["BatchSequence"].Value, out intTemp))
                        {
                            intBatchSequence = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchSequence"));
                        }

                        bolRetVal = ItemProcDAL.GetCheck(base.sessionID, intBankID, intLockboxID, longBatchID, dteDepositDate, intTransactionID, intBatchSequence, out dt);

                        if (bolRetVal && (dt.Rows.Count > 0))
                        {

                            dr = dt.Rows[0];

                            check = new cCheck();
                            check.LoadDataRow(ref dr);
                            objImages.Add(check);
                        }

                        dt.Dispose();

                        break;

                    case "document":

                        if (nodeImage.Attributes["BankID"] != null && int.TryParse(nodeImage.Attributes["BankID"].Value, out intTemp))
                        {
                            intBankID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BankID specified"));
                        }

                        if (nodeImage.Attributes["LockboxID"] != null && int.TryParse(nodeImage.Attributes["LockboxID"].Value, out intTemp))
                        {
                            intLockboxID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid LockboxID specified"));
                        }

                        if (nodeImage.Attributes["DepositDate"] != null && DateTime.TryParse(nodeImage.Attributes["DepositDate"].Value, out dteTemp))
                        {
                            dteDepositDate = dteTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid Deposit Date specified"));
                        }

                        if (nodeImage.Attributes["BatchID"] != null && long.TryParse(nodeImage.Attributes["BatchID"].Value, out longTemp))
                        {
                            longBatchID = longTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchID specified"));
                        }

                        if (nodeImage.Attributes["TransactionID"] != null && int.TryParse(nodeImage.Attributes["TransactionID"].Value, out intTemp))
                        {
                            intTransactionID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid TransactionID specified"));
                        }

                        if (nodeImage.Attributes["BatchSequence"] != null && int.TryParse(nodeImage.Attributes["BatchSequence"].Value, out intTemp))
                        {
                            intBatchSequence = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchSequence specified"));
                        }

                        if (nodeImage.Attributes["DisplayScannedCheck"] != null)
                        {
                            bolDisplayScannedCheck = (nodeImage.Attributes["DisplayScannedCheck"].Value.Trim() == "1");
                        }
                        else
                        {
                            throw (new Exception("Invalid Display Scanned Check value specified"));
                        }

                        bolRetVal = ItemProcDAL.GetDocument(base.sessionID, intBankID, intLockboxID, dteDepositDate, longBatchID, intTransactionID, intBatchSequence, bolDisplayScannedCheck, out dt);

                        if (bolRetVal && (dt.Rows.Count > 0))
                        {

                            dr = dt.Rows[0];

                            document = new cDocument();
                            document.LoadDataRow(ref dr);
                            objImages.Add(document);
                        }

                        dt.Dispose();

                        break;
                }   // Switch
            }   // For

            images = (IItem[])objImages.ToArray(typeof(IItem));

        }   // buildImageArrays

        /// <summary>
        /// 
        /// </summary>
        /// <param name="images"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="sctJobRequestOptions"></param>
        /// <param name="displayBatchID"></param>
        internal void appendImagesToPdf(ref IItem[] images
                                     , ref ciTextSharpDoc pdfDoc
                                     , JobRequestOptions sctJobRequestOptions
                                     , bool displayBatchID, PdfWriter writer = null)
        {

            string strObjectType;
            cCheck objCheck;
            cDocument objDocument;
            int intStartPage;
            int intEndPage;
            // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
            //string strImageFilePath;
            byte[] objBytes;
            bool bolPlaceholderAppended;
            bool bolImageFrontAppended = false;

            int intImageFrontCount = 0;
            int intImageBackCount = 0;
            OLFImageDisplayMode page = OLFImageDisplayMode.BothSides;

            if (writer != null)
                pdfWriter = writer;

            if (images.Length > 0)
            {
                // Define default image storage location
                string strRootImageLocation = siteOptions.imagePath;
                //Lookup FileGroup location if exists
                if (siteOptions.imageStorageMode == ImageStorageMode.FILEGROUP)
                {
                    // Lookup lockbox
                    WorkgroupDTO workgroup = ItemProcDAL.GetWorkgroup(images[0].BankID, images[0].LockboxID);

                    if (workgroup != null)
                    {
                        if (!string.IsNullOrWhiteSpace(workgroup.FileGroup))
                        {
                            if (Directory.Exists(workgroup.FileGroup))
                            {
                                strRootImageLocation = workgroup.FileGroup;
                            }
                            else
                            {
                                // Invalid directory log error.
                                eventLog.logError(new eImageDirectoryNotFoundException(strRootImageLocation));
                            }
                        }
                    }
                }

                using (cipoImages ipoImages = new cipoImages(this.siteKey, siteOptions.imageStorageMode, strRootImageLocation))
                {

                    for (int j = 0; j < images.Length; ++j)
                    {

                        // set image page options
                        strObjectType = images[j].GetType().ToString();
                        switch (strObjectType)
                        {
                            case ("WFS.RecHub.Common.cCheck"):
                                page = sctJobRequestOptions.checkImageDisplayMode;
                                break;

                            case ("WFS.RecHub.Common.cDocument"):
                                page = sctJobRequestOptions.documentImageDisplayMode;
                                break;
                        }

                        establishPageRanges(page
                                          , out intStartPage
                                          , out intEndPage);

                        for (int intCurrentPage = intStartPage; intCurrentPage < intEndPage + 1; ++intCurrentPage)
                        {

                            // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                            var response = ipoImages.GetImage(images[j], intCurrentPage);

                            //strImageFilePath=PICS.getImagePath(images[j]
                            //                                 , intCurrentPage
                            //                                 , siteOptions.imagePath);

                            bolPlaceholderAppended = false;

                            switch (strObjectType)
                            {
                                case ("WFS.RecHub.Common.cCheck"):

                                    //if (pdfDoc.appendCheckImage(strImageFilePath
                                    // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive

                                    // WI 71542 : Using the sitekey from the request, not the server's sitekey.
                                    if ((pdfDoc.appendCheckImage(response.ImageBytes
                                            , new SiteKeyFallbackImageProvider(siteKey, serverOptions)
                                            , (intCurrentPage == cPICS.PAGE_FRONT)
                                            , (intCurrentPage == cPICS.PAGE_BACK)
                                            , out bolPlaceholderAppended)))
                                    {


                                        switch (intCurrentPage)
                                        {
                                            case cPICS.PAGE_FRONT:
                                                bolImageFrontAppended = true;
                                                if (intEndPage == 0)
                                                {
                                                    objCheck = (cCheck)images[j];
                                                    pdfDoc.appendCheckDetails(objCheck, sctJobRequestOptions.displayRemitterNameInPDF, displayBatchID, sctJobRequestOptions.olCLientAccountLabel_Override);
                                                }
                                                break;

                                            case cPICS.PAGE_BACK:
                                                objCheck = (cCheck)images[j];
                                                pdfDoc.appendCheckDetails(objCheck, sctJobRequestOptions.displayRemitterNameInPDF, displayBatchID, sctJobRequestOptions.olCLientAccountLabel_Override);
                                                break;
                                        }

                                    }
                                    else
                                    {
                                        // image insert failed
                                        if ((intCurrentPage == cPICS.PAGE_BACK) && (bolImageFrontAppended || bolPlaceholderAppended))
                                        {
                                            objCheck = (cCheck)images[j];
                                            pdfDoc.appendCheckDetails(objCheck, sctJobRequestOptions.displayRemitterNameInPDF, displayBatchID, sctJobRequestOptions.olCLientAccountLabel_Override);
                                        }
                                        else if (bolPlaceholderAppended)
                                        {
                                            objCheck = (cCheck)images[j];
                                            pdfDoc.appendCheckDetails(objCheck, sctJobRequestOptions.displayRemitterNameInPDF, displayBatchID, sctJobRequestOptions.olCLientAccountLabel_Override);
                                        }
                                    }

                                    break;
                                case ("WFS.RecHub.Common.cDocument"):

                                    // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                                    //if (pdfDoc.appendImage(strImageFilePath
                                    //        , this.serverOptions.ipoResourceDir
                                    //        , (intCurrentPage == cPICS.PAGE_FRONT)
                                    //        , out bolPlaceholderAppended)) 

                                    // WI 71542 : Using the sitekey from the request, not the server's sitekey.
                                    
                                    if (pdfDoc.AppendImage(response,
                                        new SiteKeyFallbackImageProvider(siteKey, serverOptions),
                                        (intCurrentPage == cPICS.PAGE_FRONT),
                                        out bolPlaceholderAppended, pdfWriter))
                                    {
                                        objDocument = (cDocument)images[j];
                                        pdfDoc.appendDocumentDetails(objDocument, displayBatchID, sctJobRequestOptions.olCLientAccountLabel_Override);
                                    }
                                    break;
                            }   // switch

                            if (!bolPlaceholderAppended)
                            {
                                switch (intCurrentPage)
                                {
                                    case cPICS.PAGE_FRONT:
                                        ++intImageFrontCount;
                                        break;
                                    case cPICS.PAGE_BACK:
                                        ++intImageBackCount;
                                        break;
                                }
                            }
                        }   // for

                        if (intImageFrontCount > 0)
                        {
                            base.LogActivity(ActivityCodes.acViewImageFront
                                        , this.sessionID
                                        , this.jobID
                                        , intImageFrontCount
                                        , images[j].BankID
                                        , images[j].LockboxID);
                        }

                        if (intImageBackCount > 0)
                        {
                            base.LogActivity(ActivityCodes.acViewImageBack
                                        , this.sessionID
                                        , this.jobID
                                        , intImageBackCount
                                        , images[j].BankID
                                        , images[j].LockboxID);
                        }

                    }   // for
                }   // using
            }   //if
        }
    }
}

