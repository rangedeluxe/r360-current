﻿namespace WFS.RecHub.OLFImageAPI
{
    public interface IFallbackImageProvider
    {
        /// <summary>
        /// Loads the appropriate "Image Not Found" image.
        /// </summary>
        byte[] LoadImageNotFound();
    }
}