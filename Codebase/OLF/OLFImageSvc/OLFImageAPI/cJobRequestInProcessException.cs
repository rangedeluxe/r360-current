﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.Common;
using iTextSharp.text.pdf;

namespace WFS.RecHub.OLFImageAPI
{
    internal class CJobRequestInProcessException : _JobRequest, _IJobRequest
    {
        PdfWriter objPdfWriter = null;

        public CJobRequestInProcessException()
        {
        }

        public void processJob()
        {
            long lengthOfFile;
            JobRequestOptions sctJobRequestOptions = new JobRequestOptions();

            // Set status to "Processing".
            var ea = new jobRequestEventArgs();
            changeStatus(OLFJobStatus.Processing, ea);
            try
            {
                InProcessExceptionImageRequestDto inProcessDto = RetrieveInProcessExceptionDto();

                CreatePdf(inProcessDto, 
                    sctJobRequestOptions, 
                    out lengthOfFile);

                // Set status to "Completed".
                ea = new jobRequestEventArgs();
                ea.fileSize = lengthOfFile;
                changeStatus(OLFJobStatus.Completed, ea);
            }
            catch (Exception e)
            {
                // Set status to "Error".
                ea = new jobRequestEventArgs();
                ea.Message = e.Message;
                ea.src = e.Source;
                changeStatus(OLFJobStatus.Error, ea);
            }
        }

        private InProcessExceptionImageRequestDto RetrieveInProcessExceptionDto()
        {
            eventLog.logEvent("Ready to deserialize : ", this.GetType().Name, MessageImportance.Debug);

            XmlNode contractChildNode = nodeJobRequest().SelectSingleNode("contract").FirstChild;
            XElement contractChildElement;

            // Create a reader and move to the content.
            using (XmlNodeReader nodeReader = new XmlNodeReader(contractChildNode))
            {
                nodeReader.MoveToContent();
                contractChildElement = XElement.Load(nodeReader);
            }

            //Now deserialize the DataContractNode
            InProcessExceptionImageRequestDto response;
            using (XmlReader reader = contractChildElement.CreateReader())
            {
                var serializer = new DataContractSerializer(typeof(InProcessExceptionImageRequestDto));
                response = (InProcessExceptionImageRequestDto)serializer.ReadObject(reader, true);
            }

            eventLog.logEvent("Deserialize complete: ", this.GetType().Name, MessageImportance.Debug);
            return response;
        }
        
        private void CreatePdf(InProcessExceptionImageRequestDto inProcessDto, JobRequestOptions sctJobRequestOptions, out long lengthOfFile)
        {

            string completedFilePath = string.Empty;
            ciTextSharpDoc objiTextSharpDoc = null;
            
            lengthOfFile = 0;

            eventLog.logEvent("Start to create PDF : ", this.GetType().Name, MessageImportance.Debug);

            try
            {
                completedFilePath = CreateCompletedFilePath();

                // Create the Pdf
                objiTextSharpDoc = new ciTextSharpDoc();

                using (FileStream objFS = new FileStream(completedFilePath, FileMode.Create))
                {
                    objPdfWriter = PdfWriter.GetInstance(objiTextSharpDoc, objFS);

                    // Open the Document for writing
                    objiTextSharpDoc.Open();

                    objiTextSharpDoc.outputError += new outputErrorEventHandler(object_outputError);
                    // Define default image storage location
                    string strRootImageLocation = siteOptions.imagePath;

                    using (cipoImages ipoImages = new cipoImages(this.siteKey, siteOptions.imageStorageMode,
                        strRootImageLocation))
                    {
                        // process the checks (payments)
                        if (inProcessDto.Payments.Any())
                        {
                            ProcessPaymentImages(inProcessDto, ipoImages, ref objiTextSharpDoc);
                        }

                        // process the documents
                        if (inProcessDto.Documents.Any())
                        {
                            ProcessDocumentImages(inProcessDto, ipoImages, ref objiTextSharpDoc);
                        }

                    }

                    // Save PDF Document
                    objiTextSharpDoc.Close();
                    eventLog.logEvent("PDF Document created : " + completedFilePath
                        , this.GetType().Name
                        , MessageImportance.Essential);
                }
            }
            catch (Exception ex)
            {
                eventLog.logEvent("Create PDF failed : ", this.GetType().Name, MessageImportance.Debug);
                if ((!string.IsNullOrEmpty(completedFilePath)) && File.Exists(completedFilePath))
                {
                    File.Delete(completedFilePath);
                }

                throw;
            }
        }
        
        private void ProcessPaymentImages(InProcessExceptionImageRequestDto inProcessDto, 
            cipoImages ipoImages,
            ref ciTextSharpDoc pdfDoc)
        {
            int imageFrontCount = 0;
            int imageBackCount = 0;
            int imageTotalFrontCount = 0;
            int imageTotalBackCount = 0;

            cCheck check = new cCheck();
            check.DepositDate = inProcessDto.DepositDate;
            check.ProcessingDateKey = Convert.ToInt32(inProcessDto.DepositDate.ToString("yyyyMMdd"));
            check.TransactionID = inProcessDto.TransactionId;
            check.TxnSequence = inProcessDto.TransactionSequence;
            check.SourceBatchID = inProcessDto.BatchId;
            check.BatchNumber = inProcessDto.BatchId;   //batch number and batchid are the same
            check.LockboxID = inProcessDto.WorkgroupId;
            check.BankID = inProcessDto.BankId;

            foreach (var payment in inProcessDto.Payments)
            {
                check.BatchSequence = payment.BatchSequence;
                check.RT = payment.RT;
                check.Account = payment.Account;
                check.Serial = payment.Serial;
                check.Amount = payment.Amount;
                check.RemitterName = payment.RemitterName;
                check.BatchSiteCode = payment.SiteCodeId;

                AppendCheck(check,
                    LocateDisplayMode(inProcessDto.DisplayModeForCheck),
                    ref pdfDoc,
                    inProcessDto.DisplayRemitterNameInPdf,    //display remitter name on pdf
                    false,    // the batch number and batch id are the same in this case
                    ipoImages,
                    out imageFrontCount,
                    out imageBackCount,
                    inProcessDto.Workgroup);
                imageTotalBackCount = imageTotalBackCount + imageBackCount;
                imageTotalFrontCount = imageTotalFrontCount + imageFrontCount;
            }
            if (imageTotalFrontCount > 0)
            {
                base.LogActivity(ActivityCodes.acViewImageFront
                            , this.sessionID
                            , this.jobID
                            , imageTotalFrontCount
                            , inProcessDto.BankId
                            , inProcessDto.WorkgroupId);
            }

            if (imageTotalBackCount > 0)
            {
                base.LogActivity(ActivityCodes.acViewImageBack
                            , this.sessionID
                            , this.jobID
                            , imageTotalBackCount
                            , inProcessDto.BankId
                            , inProcessDto.WorkgroupId);
            }
        }

        private void ProcessDocumentImages(InProcessExceptionImageRequestDto inProcessDto,
            cipoImages ipoImages,
            ref ciTextSharpDoc pdfDoc)
        {
            int imageFrontCount = 0;
            int imageBackCount = 0;
            int imageTotalFrontCount = 0;
            int imageTotalBackCount = 0;

            cDocument document = new cDocument();
            document.BankID = inProcessDto.BankId;
            document.LockboxID = inProcessDto.WorkgroupId;
            document.BatchID = inProcessDto.BatchId;
            document.BatchNumber = inProcessDto.BatchId;
            document.DepositDate = inProcessDto.DepositDate;
            document.ProcessingDateKey = Convert.ToInt32(inProcessDto.DepositDate.ToString("yyyyMMdd"));
            document.TransactionID = inProcessDto.TransactionId;
            document.TxnSequence = inProcessDto.TransactionSequence;
            document.SourceBatchID = inProcessDto.BatchId;
            

            foreach (var stubs in inProcessDto.Documents)
            {
                document.DocumentSequence = stubs.DocumentSequence;
                document.BatchSequence = stubs.BatchSequence;
                document.FileDescriptor = stubs.FileDescriptor;
                document.BatchSiteCode = stubs.SiteCodeId;

                AppendDocument(document,
                    LocateDisplayMode(inProcessDto.DisplayModeForDocument),
                    false,                                  //displayBatchID
                    ref pdfDoc,
                    ipoImages,
                    out imageFrontCount,
                    out imageBackCount,
                    inProcessDto.Workgroup);
                imageTotalBackCount = imageTotalBackCount + imageBackCount;
                imageTotalFrontCount = imageTotalFrontCount + imageFrontCount;
            }
            if (imageTotalFrontCount > 0)
            {
                base.LogActivity(ActivityCodes.acViewImageFront
                            , this.sessionID
                            , this.jobID
                            , imageTotalFrontCount
                            , inProcessDto.BankId
                            , inProcessDto.WorkgroupId);
            }

            if (imageTotalBackCount > 0)
            {
                base.LogActivity(ActivityCodes.acViewImageBack
                            , this.sessionID
                            , this.jobID
                            , imageTotalBackCount
                            , inProcessDto.BankId
                            , inProcessDto.WorkgroupId);
            }
        }

        private OLFImageDisplayMode LocateDisplayMode(int displayMode)
        {
            switch (displayMode)
            {
                case (int)OLFImageDisplayMode.BothSides:
                    return OLFImageDisplayMode.BothSides;
                case (int)OLFImageDisplayMode.FrontOnly:
                    return OLFImageDisplayMode.FrontOnly;
                default:
                    return OLFImageDisplayMode.Undefined;
            }
        }

        private string CreateCompletedFilePath()
        {
            return  serverOptions.onlineArchivePath
                                    + this.jobID
                                    + "."
                                    + FILE_EXT_PDF;
        }

        private void AppendCheck(cCheck check
                               , OLFImageDisplayMode page
                               , ref ciTextSharpDoc pdfDoc
                               , bool displayRemitterNameInPDF
                               , bool displayBatchID
                               , cipoImages ipoImages
                               , out int ImageFrontCount
                               , out int ImageBackCount
                               , string olClientAccountDisplayOverride)
        {
            int intStartPage;
            int intEndPage;
            byte[] objBytes;
            bool bolPlaceholderAppended = false; 
            bool bolImageFrontAppended = false;

            ImageFrontCount = 0;
            ImageBackCount = 0;

            establishPageRanges(page
                              , out intStartPage
                              , out intEndPage);


            for (int intCurrentPage = intStartPage; intCurrentPage < intEndPage + 1; ++intCurrentPage)
            {
                var response = ipoImages.GetImage(check, intCurrentPage);

                if ((pdfDoc.appendCheckImage(response.ImageBytes
                        , new SiteKeyFallbackImageProvider(siteKey, serverOptions)
                        , (intCurrentPage == cPICS.PAGE_FRONT)
                        , (intCurrentPage == cPICS.PAGE_BACK)
                        , out bolPlaceholderAppended)))
                {
                    switch (intCurrentPage)
                    {
                        case cPICS.PAGE_FRONT:
                            bolImageFrontAppended = true;
                            if (intEndPage == 0)
                            {
                                pdfDoc.appendCheckDetails(check, displayRemitterNameInPDF, displayBatchID, olClientAccountDisplayOverride);
                            }
                            break;

                        case cPICS.PAGE_BACK:
                            pdfDoc.appendCheckDetails(check, displayRemitterNameInPDF, displayBatchID, olClientAccountDisplayOverride);
                            break;
                    }
                }
                else
                {
                    // image insert failed
                    if ((intCurrentPage == cPICS.PAGE_BACK) && (bolImageFrontAppended || bolPlaceholderAppended))
                    {
                        pdfDoc.appendCheckDetails(check, displayRemitterNameInPDF, displayBatchID, olClientAccountDisplayOverride);
                    }
                    else if (bolPlaceholderAppended)
                    {
                        pdfDoc.appendCheckDetails(check, displayRemitterNameInPDF, displayBatchID, olClientAccountDisplayOverride);
                    }
                }

                if (!bolPlaceholderAppended)
                {
                    switch (intCurrentPage)
                    {
                        case cPICS.PAGE_FRONT:
                            ++ImageFrontCount;
                            break;
                        case cPICS.PAGE_BACK:
                            ++ImageBackCount;
                            break;
                    }
                }
            }   //for

            pdfDoc.NewPage();
        }   

        private void AppendDocument(cDocument document
                                  , OLFImageDisplayMode page
                                  , bool displayBatchID
                                  , ref ciTextSharpDoc pdfDoc
                                  , cipoImages ipoImages
                                  , out int imageFrontCount
                                  , out int imageBackCount
                                  , string olClientAccountDisplayOverride)
        {

            int intStartPage;
            int intEndPage;
            byte[] objBytes;
            bool bolPlaceholderAppended;

            imageFrontCount = 0;
            imageBackCount = 0;

            establishPageRanges(page
                              , out intStartPage
                              , out intEndPage);

            for (int intCurrentPage = intStartPage; intCurrentPage < intEndPage + 1; ++intCurrentPage)
            {
                var response = ipoImages.GetImage(document, intCurrentPage);

                if (pdfDoc.AppendImage(response,
                    new SiteKeyFallbackImageProvider(siteKey, serverOptions),
                    (intCurrentPage == cPICS.PAGE_FRONT),
                    out bolPlaceholderAppended, objPdfWriter))
                {
                    pdfDoc.appendDocumentDetails(document, displayBatchID, olClientAccountDisplayOverride);

                    if (!bolPlaceholderAppended)
                    {
                        switch (intCurrentPage)
                        {
                            case cPICS.PAGE_FRONT:
                                ++imageFrontCount;
                                break;
                            case cPICS.PAGE_BACK:
                                ++imageBackCount;
                                break;
                        }
                    }

                }   //if
            }   //for

            pdfDoc.NewPage();
        }
    }
}
