﻿using System;
using System.Collections.Generic;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb (for previous author)
* Date:     04/12/2013
*
* Purpose:  Item Report
*
* Modification History
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 166933 SAS 09/19/2014
*   Change done for ImageRPS Report
******************************************************************************/

namespace WFS.RecHub.OLFImageAPI
{
    internal class ImageRpsItemReport : ImageRpsReportBase
    {
        public ImageRpsItemReport(string sSiteKey, Guid guidSessionID)
            : base(sSiteKey, guidSessionID)
        {
        }

        protected override string ReportTitle
        {
            get { return "ImageRPS Item Report"; }
        }

        protected override IEnumerable<string> GetPageSubheaders(bool bIsCheck, int batchSeq)
        {
            foreach (var subheader in base.GetPageSubheaders(bIsCheck, batchSeq))
                yield return subheader;

            var sequenceNumber = GetSequenceNumber(bIsCheck, batchSeq);
            yield return string.Format("Sequence Number: {0}", sequenceNumber);
        }
        public bool RunReport(XmlNode nodeReport, string completedFilePath)
        {
            bool bReturn = false;
            string strTempFileName = "";
            Document document = null;
            PdfWriter pdfWriter = null;
            int batchSequence = 0;
            int iTemp;
            try
            {
                StartReport(nodeReport, out document, out pdfWriter, out strTempFileName);
                //get batch sequence now everything else will be gotten later
                if (nodeReport.Attributes["BatchSequence"] != null
                    && int.TryParse(nodeReport.Attributes["BatchSequence"].Value.ToString(), out iTemp))
                {
                    batchSequence = iTemp;
                }
                else
                {
                    throw (new Exception("Invalid BatchSequence"));
                }
                BatchSequence = batchSequence;
                GetBatchParams(nodeReport);
                RunReport(nodeReport, document, pdfWriter);
                EndReport(document, strTempFileName, completedFilePath);
            }
            catch (Exception e)
            {
                eventLog.logError(e, this.GetType().Name, "RunReport");
                throw;
            }
            return bReturn;
        }
    }
}
