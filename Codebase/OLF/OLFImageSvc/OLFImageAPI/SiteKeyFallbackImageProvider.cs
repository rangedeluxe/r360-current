﻿using System.IO;
using WFS.RecHub.Common;

namespace WFS.RecHub.OLFImageAPI
{
    public class SiteKeyFallbackImageProvider : IFallbackImageProvider
    {
        private const string CompiledResourcePath = "WFS.RecHub.OLFImageAPI.resource";
        private const string ImageNotAvailableFileName = "ImageNotAvailable.jpg";

        public SiteKeyFallbackImageProvider(string siteKey, cImageServiceOptions serverOptions)
        {
            SiteKey = siteKey;
            ServerOptions = serverOptions;
        }

        private cImageServiceOptions ServerOptions { get; set; }
        private string SiteKey { get; set; }

        /// <summary>
        /// Loads the "Image Not Found" image from the ipoResourceDir path specified in
        /// RecHub.ini for the given siteKey. If that path doesn't exist or doesn't contain
        /// an "Image Not Found" image, fall back to a default image from compiled resources.
        /// </summary>
        public byte[] LoadImageNotFound()
        {
            var resourceDir = ServerOptions.GetIpoResourcesDirectory(SiteKey);
            var brandedPath = Path.Combine(resourceDir, ImageNotAvailableFileName);
            return File.Exists(brandedPath)
                ? File.ReadAllBytes(brandedPath)
                : LoadDefault();
        }
        private byte[] LoadDefault()
        {
            const string compiledResourceIdentifier = CompiledResourcePath + "." + ImageNotAvailableFileName;
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            using (var objStream = ipoLib.getStreamResource(compiledResourceIdentifier, assembly))
            {
                var streamLength = (int) objStream.Length;
                var bytes = new byte[streamLength];
                objStream.Read(bytes, 0, streamLength);
                return bytes;
            }
        }
    }
}