using iTextSharp.text;

namespace WFS.RecHub.OLFImageAPI
{
    public interface IImageLayout
    {
        /// <summary>
        /// Adds an image to the layout, and prepares for the next <see cref="GetImageRectangle"/> call.
        /// </summary>
        /// <param name="imageHeight"></param>
        void AddImage(float imageHeight);

        /// <summary>
        /// Gets the bounding box for the next image.
        /// </summary>
        RectangleReadOnly GetImageRectangle();
    }
}