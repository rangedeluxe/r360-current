using System;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     03/04/2012
*
* Purpose:  LTA Pdf generation library.  Uses iTextSharp.
*
* Modification History
* CR 50733 JMC 03/04/2012
*    - New class replaces obsolete cABCPdf3.cs
* CR 54169 JNE 08/08/2012
*    - appendCheckDetails, appendDocumentDetails - Add BatchNumber and displayBatchID
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 71734 CRG 04/10/2013
*   Replace XCeed.net with native .Net zip compression
* WI 116726 CEJ 10/10/2013
*   Alter text placed in PDF file by ciTextSharpDoc.appendDocumentDetails and appendCheckDetails
* WI 121672 MLH 11/26/2013
*   Added OLClientAccount DisplayLabel formatted with DDA  See WI 121666 Attachment
*   for v2.0 limitations.
*   Removed USE_ITEMPROC code
* WI 146612 DJW 06/27/2014
*    Changed "Client Account" to Workgroup. 
* WI 177157 SAS 11/11/2014
*    Changed check number to Check/Trace/Ref Number
* WI 179868 SAS 11/28/2014
*    Changes done for latest version of iTextSharp, funciton appendNoImagesFound() 
*    commented out.
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

    /// <summary>
    /// Inherited from the WebSuperGoo Document object.
    /// </summary>
	internal class ciTextSharpDoc : Document {

        private int _ImageCount=0;
        private double _PageVerticalOffset = 0;
        public event outputErrorEventHandler outputError;

        protected const string XSL_EMBEDDED_PATH = "WFS.RecHub.OLFImageAPI.resource";
        protected const string IMG_IMAGE_NOT_AVAILABLE = "ImageNotAvailable.jpg";

        protected const float IMG_CHECK_WIDTH_CONSTRAINT = 512;
        protected const float IMG_CHECK_HEIGHT_CONSTRAINT = 279;

        private const int PDF_PAGE_HEIGHT = 792;
        private const int PDF_PAGE_WIDTH = 612;
        private const int PDF_PAGE_MARGIN_LEFT = 50;
        private const int PDF_PAGE_MARGIN_TOP = 50;
        private const int PDF_IMAGE_PADDING_TOP = 25;
        private const int PDF_DETAILS_PADDING_TOP = 30;

        private const string RPT_FONT_NAME = "times-roman";

        private const string BASE_FONT = BaseFont.TIMES_ROMAN;
        private const string BASE_FONT_ENCODING = BaseFont.CP1252;
        private const float BASE_FONT_SIZE = 10;

        private const float PHRASE_SPACING = 10;

        private const string NO_IMAGES_FOUND_HTML = "<br><br><h1>No images could be located for the current request.</h1>";

        /// <summary>
        /// 
        /// </summary>
		public ciTextSharpDoc() {
            _ImageCount = 0;
		}

        /// <summary>
        /// 
        /// </summary>
        public static Font ReportFont {
            get {
                BaseFont objBaseFont = BaseFont.CreateFont(BASE_FONT, BASE_FONT_ENCODING, false);
                Font objFont = new Font(objBaseFont, BASE_FONT_SIZE, Font.NORMAL);
                return(objFont);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static Font ReportFontBold {
            get {
                return(FontFactory.GetFont(RPT_FONT_NAME, 10, iTextSharp.text.Font.BOLD));
            }
        }

//**********************************************************************************
//**********************************************************************************
//  Private Area Starts.
//**********************************************************************************
//**********************************************************************************

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iTextSharp_Img"></param>
        private void formatImage(ref Image iTextSharp_Img) {

            const double RECTHEIGHT=642;
            const double RECTWIDTH=512;

            this.formatImage(ref iTextSharp_Img, 0, RECTWIDTH, RECTHEIGHT, PDF_PAGE_MARGIN_TOP);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="img"></param>
        /// <param name="verticalPageOffset"></param>
        /// <param name="rectWidth"></param>
        /// <param name="rectHeight"></param>
        /// <param name="marginTop"></param>
        private void formatImage(ref Image img, 
                                 double verticalPageOffset, 
                                 double rectWidth, 
                                 double rectHeight, 
                                 int marginTop) {

            img.ScaleToFit((float)rectWidth, (float)rectHeight);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iTextSharp_Img"></param>
        /// <param name="imagePath"></param>
        private void addImage(ref Image iTextSharp_Img, 
                              string imagePath) {

            try {
                iTextSharp_Img = Image.GetInstance(imagePath);
                iTextSharp_Img.SetAbsolutePosition(((this.pageSize.Width - iTextSharp_Img.ScaledWidth) / 2), iTextSharp_Img.AbsoluteY);
                base.Add(iTextSharp_Img);
            } catch(Exception e) {
                if (outputError != null) {
                    outputError(e);
                }
            }

            ++_ImageCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iTextSharp_Img"></param>
        private void addImage(ref Image iTextSharp_Img) {

            try {
                iTextSharp_Img.SetAbsolutePosition(((this.pageSize.Width - iTextSharp_Img.ScaledWidth) / 2), iTextSharp_Img.AbsoluteY);
                base.Add(iTextSharp_Img);
            } catch(Exception e) {
                if (outputError != null) {
                    outputError(e);
                }
            }

            ++_ImageCount;
        }

        /// <summary>
        /// 
        /// </summary>
        public int imageCount {
            get { return _ImageCount; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imagePath"></param>
        private bool appendImage(string imagePath) {

            bool bolRetVal = false;
            
            // Create new Image reference
            if (System.IO.File.Exists(imagePath)) {

                Image objiTextSharp_Img;

                try {
                    objiTextSharp_Img = Image.GetInstance(imagePath);

                    // Add a new page to the document if existing pages
                    if (imageCount > 0) {
                        this.NewPage();
                        _PageVerticalOffset = 0;
                    }

                    formatImage(ref objiTextSharp_Img);
                    addImage(ref objiTextSharp_Img, imagePath);

                    bolRetVal = true;

                } catch(Exception e) {
                    throw;
                }

            } else {
                throw(new eImageNotFoundException(imagePath));
            }                    

            return(bolRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ImageBytes"></param>
        /// <returns></returns>
        private bool appendImage(byte[] ImageBytes) {

            bool bolRetVal = false;

            // Create new Image reference
            if(ImageBytes != null && ImageBytes.Length > 0) {

                try {

                    Image objiTextSharp_Img = Image.GetInstance(ImageBytes);

                    // Add a new page to the document if existing pages
                    if (imageCount > 0) {
                        this.NewPage();
                        _PageVerticalOffset = 0;
                    }

                    formatImage(ref objiTextSharp_Img);
                    addImage(ref objiTextSharp_Img);                    

                    bolRetVal = true;

                } catch(Exception e) {
                    throw;
                }

            } else {
                throw(new eImageNotFoundException("<Bytes>"));
            }

            return(bolRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ImageBytes"></param>
        /// <param name="addImageToCurrentPage"></param>
        /// <param name="rectWidth"></param>
        /// <param name="rectHeight"></param>
        /// <returns></returns>
        private bool appendImage(byte[] ImageBytes, bool addImageToCurrentPage, double rectWidth, double rectHeight ) {

            bool bolRetVal = false;
            int intPaddingTop = PDF_PAGE_MARGIN_TOP;

            // Create new Image reference
            if (ImageBytes != null) {

                Image objiTextSharp_Img;

                try {

                    objiTextSharp_Img = Image.GetInstance(ImageBytes);

                    // Add a new page to the document if existing pages
                    if ((imageCount > 0) && (!addImageToCurrentPage)) {
                        this.NewPage();
                        _PageVerticalOffset = 0;
                    }

                    if (addImageToCurrentPage) { 
                        intPaddingTop = PDF_IMAGE_PADDING_TOP; 
                    }

                    formatImage(ref objiTextSharp_Img, _PageVerticalOffset, rectWidth, rectHeight, intPaddingTop);
                    addImage(ref objiTextSharp_Img);

                    bolRetVal = true;

                } catch (Exception e) {
                    throw;
                }

            } else {
                throw (new eImageNotFoundException(string.Empty));
            }

            return (bolRetVal);
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="check"></param>
        /// <param name="displayRemitterName"></param>
        /// <param name="displayBatchID"></param>
        public void appendCheckDetails(cCheck check, bool displayRemitterName, bool displayBatchID, string olClientAccountDisplayOverride) {

            Chunk chnkDisplayTitle2=null;
            Chunk chnkDisplayText2=null;
            Chunk chnkDisplayTitle3=null;
            Chunk chnkDisplayText3=null;
            Phrase p2;
            Phrase p3;
            string clientAccountDisplayLabel = string.Empty;

            bool bolContinue = true;

            if (String.IsNullOrEmpty(olClientAccountDisplayOverride))
                olClientAccountDisplayOverride = check.LockboxID.ToString();

            // Create table with two columns.
            PdfPTable objTable = new PdfPTable(2);

            objTable.TotalWidth = this.PageSize.Width-(PDF_PAGE_MARGIN_LEFT*2);
            objTable.LockedWidth = true;
            objTable.DefaultCell.BorderWidth = 0;
            objTable.DefaultCell.Padding=0;
            objTable.DefaultCell.PaddingBottom=2;


            for(int i=0;i<5;++i) {
                p2 = new Phrase(ciTextSharpDoc.PHRASE_SPACING);
                p3 = new Phrase(ciTextSharpDoc.PHRASE_SPACING);
                switch(i) {
                    case 0:
                        chnkDisplayTitle2 = new Chunk("Deposit Date: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText2 = new Chunk(check.DepositDate.ToString("d"), ciTextSharpDoc.ReportFont);

                        chnkDisplayTitle3 = new Chunk("R/T: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText3 = new Chunk(check.RT, ciTextSharpDoc.ReportFont);
                        break;
                    case 1:
                        chnkDisplayTitle2 = new Chunk("Workgroup: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText2 = new Chunk(olClientAccountDisplayOverride, ciTextSharpDoc.ReportFont);

                        chnkDisplayTitle3 = new Chunk("Account Number: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText3 = new Chunk(check.Account, ciTextSharpDoc.ReportFont);
                        break;
                    case 2:
                        chnkDisplayTitle2 = new Chunk("Batch: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText2 = new Chunk(check.BatchNumber.ToString(), ciTextSharpDoc.ReportFont);

                        chnkDisplayTitle3 = new Chunk("Check/Trace/Ref Number: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText3 = new Chunk(check.Serial, ciTextSharpDoc.ReportFont);
                        break;
                    case 3:
                        chnkDisplayTitle2 = new Chunk("Transaction: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText2 = new Chunk(check.TxnSequence.ToString(), ciTextSharpDoc.ReportFont);

                        if (displayRemitterName) {
                            chnkDisplayTitle3 = new Chunk("Payer: ", ciTextSharpDoc.ReportFontBold);
                            chnkDisplayText3 = new Chunk(check.RemitterName, ciTextSharpDoc.ReportFont);
                        } else {
                            chnkDisplayTitle3 = new Chunk("Amount: ", ciTextSharpDoc.ReportFontBold);
                            chnkDisplayText3 = new Chunk(check.Amount.ToString("C"), ciTextSharpDoc.ReportFont);
                        }
                        break;
                    case 4:

                        if (displayRemitterName) {
                            
                            if (displayBatchID) {
                                chnkDisplayTitle2 = new Chunk("Batch ID: ", ciTextSharpDoc.ReportFontBold);
                                chnkDisplayText2 = new Chunk(check.SourceBatchID.ToString(), ciTextSharpDoc.ReportFont);
                            }else{
                                chnkDisplayTitle2 = new Chunk(string.Empty, ciTextSharpDoc.ReportFontBold);
                                chnkDisplayText2 = new Chunk(string.Empty, ciTextSharpDoc.ReportFont);
                            }

                            chnkDisplayTitle3 = new Chunk("Amount: ", ciTextSharpDoc.ReportFontBold);
                            chnkDisplayText3 = new Chunk(check.Amount.ToString("C"), ciTextSharpDoc.ReportFont);
                            
                            bolContinue = true;

                        } else {
                            if (displayBatchID){
                                chnkDisplayTitle2 = new Chunk("Batch ID: ", ciTextSharpDoc.ReportFontBold);
                                chnkDisplayText2 = new Chunk(check.SourceBatchID.ToString(), ciTextSharpDoc.ReportFont);
                                
                                chnkDisplayTitle3 = new Chunk(string.Empty, ciTextSharpDoc.ReportFontBold);
                                chnkDisplayText3 = new Chunk(string.Empty, ciTextSharpDoc.ReportFont);

                                bolContinue = true;
                            }else{
                                // if DisplayRemitterName is false and DisplayBatchID is false, then there will no fifth row, so exit the loop.
                                bolContinue = false;
                            }
                        }
                        break;
                }

                if(bolContinue) {

                    p2.Add(chnkDisplayTitle2);
                    p2.Add(chnkDisplayText2);

                    p3.Add(chnkDisplayTitle3);
                    p3.Add(chnkDisplayText3);

                    objTable.AddCell(p2);
                    objTable.AddCell(p3);

                    objTable.CompleteRow();
                }
            }

            this.Add(objTable);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="displayBatchID"></param>
        public void appendDocumentDetails(cDocument document, bool displayBatchID, string olClientAccountDisplayOverride) {

            Chunk chnkDisplayTitle=null;
            Chunk chnkDisplayText=null;
            Phrase p2;
            bool bolContinue = true;
            string clientAccountDisplayLabel = string.Empty;

            if (String.IsNullOrEmpty(olClientAccountDisplayOverride))
                olClientAccountDisplayOverride = document.LockboxID.ToString();

            // Create table with two columns.  For Documents, the second 
            // column will have no data in it.
            PdfPTable objTable = new PdfPTable(2);
            objTable.TotalWidth = this.PageSize.Width-(PDF_PAGE_MARGIN_LEFT*2);
            objTable.LockedWidth = true;
            objTable.DefaultCell.BorderWidth = 0;
            objTable.DefaultCell.Padding=0;
            objTable.DefaultCell.PaddingBottom=2;


            for(int i=0;i<5;++i) {
                p2 = new Phrase(SearchResultsPdfSupport.PHRASE_SPACING);
                switch(i) {
                    case 0:
                        chnkDisplayTitle = new Chunk("Deposit Date: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText = new Chunk(document.DepositDate.ToString("d"), ciTextSharpDoc.ReportFont);
                        break;
                    case 1:
                        chnkDisplayTitle = new Chunk("Workgroup: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText = new Chunk(olClientAccountDisplayOverride, ciTextSharpDoc.ReportFont);
                        break;
                    case 2:
                        chnkDisplayTitle = new Chunk("Batch: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText = new Chunk(document.BatchNumber.ToString(), ciTextSharpDoc.ReportFont);
                        break;
                    case 3:
                        chnkDisplayTitle = new Chunk("Transaction: ", ciTextSharpDoc.ReportFontBold);
                        chnkDisplayText = new Chunk(document.TxnSequence.ToString(), ciTextSharpDoc.ReportFont);
                        break;
                    case 4:
                        if (displayBatchID){
                            chnkDisplayTitle = new Chunk("Batch ID: ", ciTextSharpDoc.ReportFontBold);
                            chnkDisplayText = new Chunk(document.SourceBatchID.ToString(), ciTextSharpDoc.ReportFont);
                        }else{
                            bolContinue = false;
                        }
                        break;
                }
                if (bolContinue){
                    p2.Add(chnkDisplayTitle);
                    p2.Add(chnkDisplayText);
                
                    objTable.AddCell(p2);
                    objTable.CompleteRow();
                }
            }

            this.Add(objTable);
        }

        internal bool AppendPdf(ImageResponse imageResponse, PdfWriter writer)
        {
            var pdfReader = new PdfReader(imageResponse.ImageBytes);
            Int32 pages = pdfReader.NumberOfPages;
            for (Int32 pageNum = 1; pageNum <= pages; pageNum++)
            {
                this.NewPage();
                var page = writer.GetImportedPage(pdfReader, pageNum);
                var image = Image.GetInstance(page);
                formatImage(ref image);
                addImage(ref image);
            }

            return true;
        }

        internal bool AppendImage(ImageResponse imageResponse,
            IFallbackImageProvider fallbackImageProvider,
            bool addPlaceholderForMissingImage, 
            out bool appendedPlaceholder, PdfWriter writer = null)
        {
            try
            {
                if (imageResponse == null)
                    throw new eImageNotFoundException(string.Empty);

                if (!imageResponse.IsSuccessful)
                    throw new eImageNotFoundException(string.Empty);

                if (string.Equals(imageResponse.FileType, "pdf",
                    StringComparison.InvariantCultureIgnoreCase))
                {
                    this.AppendPdf(imageResponse, writer);
                    appendedPlaceholder = false;
                    return true;
                }

                this.appendImage(imageResponse.ImageBytes);
                appendedPlaceholder = false;
                return (true);
            }
            catch (eImageNotFoundException)
            {
                if (addPlaceholderForMissingImage)
                {
                    appendImageNotFound(fallbackImageProvider);
                    appendedPlaceholder = true;
                    return (true);
                }
                else
                {
                    appendedPlaceholder = false;
                    return (false);
                }
            }
        }

        internal bool appendImage(byte[] ImageBytes,
            IFallbackImageProvider fallbackImageProvider,
            bool addPlaceholderForMissingImage,
            out bool AppendedPlaceholder)
        {
            var imageResponse = new ImageResponse
            {
                IsSuccessful = true,
                ImageBytes = ImageBytes
            };

            return AppendImage(imageResponse, fallbackImageProvider, 
                addPlaceholderForMissingImage, out AppendedPlaceholder);
        }

        // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive
        internal bool appendCheckImage(byte[] ImageBytes,
            IFallbackImageProvider fallbackImageProvider,
            bool addPlaceholderForMissingImage,
            bool addImageToCurrentPage,
            out bool AppendedPlaceholder)
        {
            try {
                this.appendImage(ImageBytes, addImageToCurrentPage, IMG_CHECK_WIDTH_CONSTRAINT, IMG_CHECK_HEIGHT_CONSTRAINT);
                AppendedPlaceholder = false;
                return (true);
            } catch (eImageNotFoundException) {
                if (addPlaceholderForMissingImage) {
                    appendImageNotFound(fallbackImageProvider);
                    AppendedPlaceholder = true;
                    return (true);
                } else {
                    AppendedPlaceholder = false;
                    return (false);
                }
            }
        }

        private void appendImageNotFound(IFallbackImageProvider fallbackImageProvider)
        {
            try
            {
                var imageData = fallbackImageProvider.LoadImageNotFound();
                appendImage(imageData);
            }
            catch
            {
                Phrase objPhrase = new Phrase("Image not available.");
                Add(objPhrase);
            }
        }
    }
}
