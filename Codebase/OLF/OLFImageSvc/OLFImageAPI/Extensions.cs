﻿using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OLFServices.DataTransferObjects;
using static WFS.RecHub.OLFImageAPI._JobRequest;

namespace WFS.RecHub.OLFImageAPI
{
    public static class Extensions
    {
        internal static void Cleanup(this ciTextSharpDoc document, PdfWriter writer, cImageServiceOptions serviceOptions)
        {
            if (document == null)
                throw new ArgumentNullException("PdfDocument must be initialized");

            if (document.IsOpen())
            {
                if (writer.DidNotWriteAnyPages())//check and see if the writer has written anything to the document
                {
                    bool wasPlaceHolderAppended = false;
                    //if nothing has been written, then append
                    document.AppendImage(null,
                        new SiteKeyFallbackImageProvider(serviceOptions.siteKey, serviceOptions),
                        true, out wasPlaceHolderAppended, writer);
                }

                document.Close();
            }
        }
        internal static bool DidNotWriteAnyPages(this PdfWriter writer)
        {
            if (writer == null)
                throw new ArgumentNullException("PdfWriter must be initialized.");

            if (writer.PageEmpty && writer.CurrentPageNumber == 1)
                return true;

            return false;
        }
    }
}
