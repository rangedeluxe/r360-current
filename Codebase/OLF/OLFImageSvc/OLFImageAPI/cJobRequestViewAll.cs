using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using iTextSharp.text.pdf;
using WFS.RecHub.Common;
using WFS.RecHub.ItemProcDAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     05/15/2003
*
* Purpose:  IntegraPAY Online Image Service View All.
*
* Modification History
* 01/14/2009 CR 99999 ABC 
*    -Initial release.
* 05/15/2003 JMC
*    -Created class
* 11/20/2003 JMC
*    -First major enhancement : 
*     1.)Added Strong-Naming to the assembly.
*     2.)Changed the namespace of the class to narrow the scope.
*     3.)Modified Comments to utilize C# Xml format.
*     4.)Renamed this class/file.  Used to be cImageCreator.
* 03/22//2004 CR 7074 JMC
*    -Added "page" input parameter to "createImagePdf" functions.
*    -Added "page" parameter to calls to "appendBatch" function.
*    -Added "page" parameter to calls to "appendTransaction" function.
*    -Added "page" parameter to call to "createPDF" function.
*    -Added "page" input parameter to "createPDF" functions.
*    -Added "page" input parameter to "appendBatch" function.
*    -Added "page" input parameter to "appendTransaction" function.
*    -Added "page" input parameter to "appendTransactionImages" function.
*    -"appendTransactionImages" function will now append all specified pages
*     (0=Front, 1=Back, Anything else or -1=Both Front and Back).
*    -Modified logic that appends images, to supress image placeholder when
*     attempting to append image backs.
* 01/18/2005 CR 9310 JMC
*    -Added ImageFilterOption parameter to createImagePDF functions.
*    -Modified createPDF function to call appendNoImagesFound when
*     no images were appended to the Pdf.
*    -Modified appendBatch function to return count of images appended
*     to the Pdf.
*    -Modified appendTransaction and appendTransactionImages functions to 
*     return count of images appended to the Pdf.
* 03/03/2005 CR 10537 JMC
*    -Modified createImagePDF function to use the sctJobRequestOptions 
*     structure.
*    -Modified ProcessJob function to use the cTransaction class instead
*     of the ProcessingInstructions structure.
*    -Modified ProcessJob function to combine the Batch and Transaction
*     Arrays and filter out duplicate Transactions.
*    -Modified appendTransactionsToArray function to use the
*     sctJobRequestOptions structure.
*    -Modified createPDF function to use the
*     sctJobRequestOptions structure.
*    -Modified createPDF function to accept the sctJobRequestOptions 
*     structure instead of the individual options.
*    -Modified createPDF function to log activities.
*    -Modified appendTransactionImages to count and return image fronts
*     and image backs.
*    -Modified appendCheck function to count and return image fronts
*     and image backs.
*    -Modified appendDocument to count and return image fronts and image 
*     backs.
* 05/10/2005 CR 12276 JMC
*    -Modified processJob function to use an ArrayList instead of a normal
*     array.  This was done so we can use the Sort method which uses the
*     new IComparable interface which was added to the cTransaction object.
* 06/02/2005 CR 12653 JMC
*    -Modified processJob, appendBatchTransactionsToArray, and 
*     appendTransactionsToArray to use ArrayList collections instead of 
*     SortedLists.  This was necessary in order to not use the default sort
*     which was by the result of the cTransaction.ToString() function.  Doing
*     this allows sorting only when called explicitly.  A side-effect of not
*     using the SortedList is that it required writing a short piece of code
*     to loop through each item in the ArrayList to determine if a Transaction
*     already exists in the collection.
*    -The call to sort the array was moved to the appendBatchTransactionsToArray
*     function which will only sort those transactions that are part of a
*     "View All" batch.  Doing this will cause the resultant PDF document to
*     sort Transactions in the order that they exist in the parameter Xml
*     document except in the case of "View All" Batches.
* 06/22/2005 CR 12996 JMC
*    -Corrected Bank/Lockbox assignment for View-All Transactions
* 05/23/2008 CR 23661 JCS
*    -Added displayRemitterNameInPDF parameter to createImagePDF,createPDF functions.
*    -Modified functions appendTransactionImages, appendCheck to add Remitter Name when viewing checks
* 05/23/2008 CR 23581 JCS
*    -Added checkImageDisplayMode, documentImageDisplayMode parameters to createImagePDF, functions.
*    -Modified appendTransactionImages to use check/document display modes.
* 5/23/2008 CR 24043 JCS
*    -Modified appendCheck to display check front and rear images on same page.
* 05/20/2009 CR 23584 MEH
*    -integraPAY Online Interface Into Hyland IMS Archive
* CR 32562 JMC 03/02/2011 
*    -Modified appendCheck() to remove the cast from (cCheck) to (cCheck)
*    -Modified appendCheck() to remove the cast from (cDocument) to (cImage)
* CR 45627 JMC 07/12/2011
*    -Added a Using{} clause when accessing ipoImages to ensure Dispose() is 
*     called.
* CR 47984 WJS 11/8/2011
*    - Allow images to be called all at once for Hyland. So you can avoid multiple requests
*	 - Only dispose of ipoimages once for entire call as opposed to per image
*	 - Change appendTransactionImages to make it so get entire batch at once
* CR 48387 WJS 11/17/2011
*	 - Add multiple appendTransactionImages to be able to read cTransaction and cDocument
* CR 50536 WJS 02/29/2012
*	 - Allow check if multiple images fails for PICS
* CR 50750 JNE 03/01/2012 
*	 - Removed bRet in cJobRequestViewAll in appendImagesToPdf and in appendDocument
* CR 50733 JMC 03/04/2012
*    - Refactored object to use iTextSharp instead of ABCPdf.Net
*    - Removed unused ItemProc code. 
* CR 54169 JNE 08/08/2012
*    - createImagePDF - Added BatchNumber, displayBatchID
*    - processJob, createPDF, appendTransactionImages - Added bDisplayBatchID
*    - appendTransactionsToArray - Added BatchNumber
*    - appendCheck, appendDocument - Added displayBatchID
* CR 56004 JMC 09/25/2012
*    -Corrected issue where SessionActivityLog rows were being written for each 
*     iteration of the loop that adds images to 'View-All' Pdfs.
* CR 52959 JNE 10/01/2012
*    - createPDF - Added FileGroup code when determining root image path. 
* WI 81862 WJS 12/18/2012
*    -Added displayBatchID
* WI 96037 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFServices
* WI 117970 JMC 10/18/2013
*   -Modified OLFImageAPI to evaluate missing BatchID or BatchNumber as false
* WI 118024 TWE 10/21/2013
*       -FP Added displayBatchID
* WI 121672 MLH 11/26/2013
*   Added OLClientAccount DisplayLabel formatted with DDA  See WI 121666 Attachment
*   for v2.0 limitations.
*   Removed USE_ITEMPROC code
* WI 71542 BLR 05/07/2014
*   Using the request's sitekey, not the server's sitekey, for finding the
*   resource directory.  
* WI 143274 SAS 05/21/2014
*   Changes done to change the datatype of BatchID from int to long
* WI 164401 SAS 09/09/2014
*   Changes done to set the value for Display BatchID 
* WI 173286 SAS 10/20/2014
*   Changes done to set the value for Display BatchID while reading Batch nodes elements
******************************************************************************/
namespace WFS.RecHub.OLFImageAPI {

    /// <summary>
    /// ipo Image Service "View All" job object class
    /// </summary>
	internal class cJobRequestViewAll : _TransactionalJobRequest, _IJobRequest
    {
        PdfWriter objPdfWriter = null;

        /// <summary>
        /// Public Constructor
        /// </summary>
		public cJobRequestViewAll() {
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="BatchID"></param>
        /// <param name="BatchNumber"></param>
        /// <param name="displayScannedCheck"></param>
        /// <param name="displayBatchID"></param>
        /// <param name="imageFilterOption"></param>
        /// <param name="displayRemitterNameInPDF"></param>
        /// <param name="checkImageDisplayMode"></param>
        /// <param name="documentImageDisplayMode"></param>
        /// <param name="lbColorMode"></param>
        public void createImagePDF(int BankID, 
                                   int LockboxID, 
                                   DateTime DepositDate, 
                                   long BatchID,
                                   int BatchNumber, 
                                   bool displayScannedCheck,
                                   bool displayBatchID, 
                                   OLFImageFilterOption imageFilterOption, 
                                   bool displayRemitterNameInPDF, 
                                   OLFImageDisplayMode checkImageDisplayMode, 
                                   OLFImageDisplayMode documentImageDisplayMode,
                                   // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                                   WorkgroupColorMode lbColorMode) {

            createImagePDF(BankID, 
                           LockboxID, 
                           DepositDate, 
                           BatchID,
                           BatchNumber, 
                           -1, 
                           -1, 
                           displayScannedCheck,
                           displayBatchID, 
                           imageFilterOption, 
                           displayRemitterNameInPDF, 
                           checkImageDisplayMode,
                           documentImageDisplayMode,
                           // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                           lbColorMode);
        }

        public void createImagePDF(int BankID, 
                                   int LockboxID, 
                                   DateTime DepositDate, 
                                   long BatchID,
                                   int BatchNumber, 
                                   int TransactionID, 
                                   int TxnSequence, 
                                   bool displayScannedCheck,
                                   bool bDisplayBatchID, 
                                   OLFImageFilterOption imageFilterOption, 
                                   bool displayRemitterNameInPDF, 
                                   OLFImageDisplayMode checkImageDisplayMode, 
                                   OLFImageDisplayMode documentImageDisplayMode,
                                   // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                                   WorkgroupColorMode lbColorMode) {

            long lngLOF;
            JobRequestOptions sctJobRequestOptions = new JobRequestOptions();

            sctJobRequestOptions.displayScannedCheck = displayScannedCheck;
            sctJobRequestOptions.ImageFilterOption = imageFilterOption;
            sctJobRequestOptions.displayRemitterNameInPDF = displayRemitterNameInPDF;
            sctJobRequestOptions.includeImages = true;
            sctJobRequestOptions.checkImageDisplayMode = checkImageDisplayMode;
            sctJobRequestOptions.documentImageDisplayMode = documentImageDisplayMode;
            sctJobRequestOptions.returnType = "";

            try {

                this.jobID=Guid.Empty;
                cTransaction[] pi=new cTransaction[1];

                pi[0] = new cTransaction(BankID, LockboxID, DepositDate, BatchID, BatchNumber,TransactionID, TxnSequence);

                createPDF(pi
                        , sctJobRequestOptions
                        , bDisplayBatchID
                        , out lngLOF);

                Console.WriteLine("Pdf created: " + this.jobID.ToString());

            } catch(Exception e) {

                jobRequestEventArgs ea = new jobRequestEventArgs();
                ea.Message = e.Message;
                ea.src = e.Source;
                changeStatus(OLFJobStatus.Error, ea);
            }
        }


        /// <summary>
        /// Parses the input Xml document for Batches and Transactions to be
        /// processed.  The function then creates an array containing all Batch
        /// and transaction ID's.  A corresponding array containing all SiteCodes
        /// for each Batch's Lockbox.  These arrays are then used to retrieve all
        /// required images by Site, and create the Pdf.
        /// </summary>
        public void processJob()
        {
			ArrayList 					transactionArray;
            XmlNode                     nodeGlobalBatches;
            XmlNode                     nodeTransactions;
            JobRequestOptions           sctJobRequestOptions;
            long lngLOF;
            jobRequestEventArgs ea;
            SortedList slBoxCounts = new SortedList();

            try
            {
                
                // Set status to "Processing".
                ea = new jobRequestEventArgs();
                changeStatus(OLFJobStatus.Processing, ea);

                //Retrieve Batch nodes from the request Xml document.
                nodeGlobalBatches = nodeJobRequest().SelectSingleNode(XML_ELE_BATCHES);

                //Retrieve Transaction nodes from the request Xml document.
                nodeTransactions = nodeJobRequest().SelectSingleNode(XML_ELE_TRANSACTIONS);

        
                //Retrieve process-level options from the request Xml document.
                getJobRequestOptions(nodeJobRequest(),
                    () => new ImageDisplayModes(OLFImageDisplayMode.BothSides, OLFImageDisplayMode.BothSides),
                    out sctJobRequestOptions);

                //Set DisplayBatchID value
                if (sctJobRequestOptions.displayBatchID == false)
                {
                    if (DisplayBatchId(nodeGlobalBatches) || DisplayBatchId(nodeTransactions))
                        sctJobRequestOptions.displayBatchID = true;
                }

                transactionArray = new ArrayList();

                //Build an array containing a list of all nodes to be processed.
                appendBatchTransactionsToArray(ref nodeGlobalBatches
                                             , ref transactionArray);

                appendTransactionsToArray(ref nodeTransactions
                                        , ref transactionArray);


                //Create the Pdf.
                createPDF((cTransaction[])transactionArray.ToArray(typeof(cTransaction))
                        , sctJobRequestOptions
                        , sctJobRequestOptions.displayBatchID
                        , out lngLOF);

                // Set status to "Completed".
                ea = new jobRequestEventArgs();
                ea.fileSize = lngLOF;
                changeStatus(OLFJobStatus.Completed, ea);
            }
            catch(Exception e) {

                // Set status to "Error".
                ea = new jobRequestEventArgs();
                ea.Message = e.Message;
                ea.src = e.Source;
                changeStatus(OLFJobStatus.Error, ea);
            }
        }


        /// <summary>
        /// Check if the Display BatchID value is set in the request, the request
        /// can be either the transaction node or the batch node
        /// </summary>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private bool DisplayBatchId(XmlNode nodes)
        {
            bool bRetVal = false;
            bool bTemp;
            try
            {
                //Assign each Transaction node to an element of the instructions array.
                if ((nodes != null) && (nodes.ChildNodes.Count > 0))
                {
                    for (int i = 0; i < nodes.ChildNodes.Count; ++i)
                    {
                        XmlNode node = nodes.ChildNodes[i];
                        if (node.Attributes["DisplayBatchID"] != null && bool.TryParse(node.Attributes["DisplayBatchID"].Value, out bTemp))
                            return bTemp;                        
                    }
                }
            }
            catch
            {
                bRetVal = false;
            }
            return bRetVal;
        }
        /// <summary>
        /// Creates an array of BankID, LockboxID, GlobalBatchID, and TransactionID's to be
        /// included in the "View All" Pdf.
        /// </summary>
        /// <param name="nodeBatches"></param>
        /// <param name="transactionArray"></param>
        /// <returns></returns>
        private void appendBatchTransactionsToArray(ref XmlNode nodeBatches
                                                  , ref ArrayList transactionArray) {

            ArrayList arTransactions;
            bool bolTxnExists;
            cTransaction txnTemp;
            
            int intBankID;
            int intLockboxID;
            DateTime dteDepositDate;
            long longBatchID;
            int intTemp;
            long longTemp=0;

            DateTime dteTemp;
    
            try {

                //Assign each Batch node to an element of the instructions array.
                if(nodeBatches != null) { 
                    if (nodeBatches.ChildNodes.Count > 0) {
                        for (int i=0;i<nodeBatches.ChildNodes.Count;++i)
                        {
                            XmlNode nodeBatch = nodeBatches.ChildNodes[i];
                            eventLog.logEvent("Processing View All Batch Request for Bank : " + nodeBatch.Attributes[XML_ATTR_BANK_ID].Value + " Lockbox : " + nodeBatch.Attributes[XML_ATTR_LOCKBOX_ID].Value + " Batch : " + nodeBatch.Attributes[XML_ATTR_BATCH_ID].Value + " Deposit Date : " + nodeBatch.Attributes[XML_ATTR_DEPOSITDATE].Value, this.GetType().Name, MessageImportance.Debug);

                            try {

                                if(nodeBatch.Attributes["BankID"] != null && int.TryParse(nodeBatch.Attributes["BankID"].Value, out intTemp)) {
                                    intBankID = intTemp;
                                } else {
                                    throw(new Exception("Invalid BankID specified"));
                                }

                                if(nodeBatch.Attributes["LockboxID"] != null && int.TryParse(nodeBatch.Attributes["LockboxID"].Value, out intTemp)) {
                                    intLockboxID = intTemp;
                                } else {
                                    throw(new Exception("Invalid LockboxID specified"));
                                }

                                if(nodeBatch.Attributes["DepositDate"] != null && DateTime.TryParse(nodeBatch.Attributes["DepositDate"].Value, out dteTemp)) {
                                    dteDepositDate = dteTemp;
                                } else {
                                    throw(new Exception("Invalid Deposit Date specified"));
                                }

                                if (nodeBatch.Attributes["BatchID"] != null && long.TryParse(nodeBatch.Attributes["BatchID"].Value, out longTemp))
                                {
                                    longBatchID = longTemp;
                                } else {
                                    throw(new Exception("Invalid BatchID specified"));
                                }

                                if(getBatchTransactions(intBankID, intLockboxID, dteDepositDate, longBatchID, out arTransactions)) {
                                    if(arTransactions.Count >0) {
                                        arTransactions.Sort();
                                        for(int j=0;j<arTransactions.Count;++j) {
                                            if(transactionArray.Count>0) {
                                                bolTxnExists = false;
                                                for(int k=0;k<transactionArray.Count;++k) {

                                                    txnTemp = (cTransaction)transactionArray[k];

                                                    if(txnTemp.BankID == ((cTransaction)arTransactions[j]).BankID &&
                                                       txnTemp.LockboxID == ((cTransaction)arTransactions[j]).LockboxID &&
                                                       txnTemp.BatchID == ((cTransaction)arTransactions[j]).BatchID &&
                                                       txnTemp.DepositDate == ((cTransaction)arTransactions[j]).DepositDate &&
                                                       txnTemp.TransactionID == ((cTransaction)arTransactions[j]).TransactionID) {

                                                        bolTxnExists=true;
                                                        break;
                                                    }
                                                }
                                                if(!bolTxnExists) {
                                                    transactionArray.Add(arTransactions[j]);
                                                }
                                            } else {
                                                transactionArray.Add(arTransactions[j]);
                                            }
                                        }
                                    }
                                }
                            } catch(Exception ex) {  
                                eventLog.logEvent(ex.Message, this.GetType().Name, MessageType.Error, MessageImportance.Essential); 
                            }
                        }
                    }
                }

            } catch(Exception e) {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeTransactions"></param>
        /// <param name="transactionArray"></param>
        private void appendTransactionsToArray(ref XmlNode nodeTransactions
                                             , ref ArrayList transactionArray) {

            cTransaction procTemp;
            bool bolTxnExists;
            cTransaction txnTemp;

            int intBankID;
            int intLockboxID;
            DateTime dteDepositDate;
            long longBatchID;
            int intBatchNumber;
            int intTransactionID;
            int intTxnSequence;

            int intTemp;
            long longTemp = 0;
            DateTime dteTemp;

            try {

                //Assign each Transaction node to an element of the instructions array.
                if(nodeTransactions != null) { 
                    if (nodeTransactions.ChildNodes.Count > 0) {
                        for (int i=0;i<nodeTransactions.ChildNodes.Count;++i)
                        {
                            XmlNode nodeTransaction = nodeTransactions.ChildNodes[i];
                            if(nodeTransaction.Attributes["BankID"] != null && int.TryParse(nodeTransaction.Attributes["BankID"].Value, out intTemp)) {
                                intBankID = intTemp;
                            } else {
                                throw(new Exception("Invalid BankID specified"));
                            }

                            if(nodeTransaction.Attributes["LockboxID"] != null && int.TryParse(nodeTransaction.Attributes["LockboxID"].Value, out intTemp)) {
                                intLockboxID = intTemp;
                            } else {
                                throw(new Exception("Invalid LockboxID specified"));
                            }

                            if(nodeTransaction.Attributes["DepositDate"] != null && DateTime.TryParse(nodeTransaction.Attributes["DepositDate"].Value, out dteTemp)) {
                                dteDepositDate = dteTemp;
                            } else {
                                throw(new Exception("Invalid Deposit Date specified"));
                            }

                            if(nodeTransaction.Attributes["BatchID"] != null && long.TryParse(nodeTransaction.Attributes["BatchID"].Value, out longTemp)) {
                                longBatchID = longTemp;
                            } else {
                                throw(new Exception("Invalid BatchID specified"));
                            }
                            if(nodeTransaction.Attributes["BatchNumber"]!= null && int.TryParse(nodeTransaction.Attributes["BatchNumber"].Value, out intTemp)){
                                intBatchNumber = intTemp;
                            }else{
                                throw(new Exception("Invalid BatchNumber specified"));
                            }
                            if(nodeTransaction.Attributes["TransactionID"] != null && int.TryParse(nodeTransaction.Attributes["TransactionID"].Value, out intTemp)) {
                                intTransactionID = intTemp;
                            } else {
                                throw(new Exception("Invalid TransactionID specified"));
                            }

                            if(nodeTransaction.Attributes["TxnSequence"] != null && int.TryParse(nodeTransaction.Attributes["TxnSequence"].Value, out intTemp)) {
                                intTxnSequence = intTemp;
                            } else {
                                //throw(new Exception("Invalid TxnSequence specified"));
                                intTxnSequence = intTransactionID;
                            }

                            procTemp = new cTransaction(intBankID, intLockboxID, dteDepositDate, longBatchID, intBatchNumber, intTransactionID, intTxnSequence);

                            if(transactionArray.Count>0) {
                                bolTxnExists = false;
                                for(int k=0;k<transactionArray.Count;++k) {

                                    txnTemp = (cTransaction)transactionArray[k];

                                    if(txnTemp.BankID == procTemp.BankID &&
                                       txnTemp.LockboxID == procTemp.LockboxID &&
                                       txnTemp.BatchID == procTemp.BatchID &&
                                       txnTemp.DepositDate == procTemp.DepositDate &&
                                       txnTemp.TransactionID == procTemp.TransactionID) {

                                        bolTxnExists=true;

                                        break;
                                    }
                                    }

                                if(!bolTxnExists) {
                                    transactionArray.Add(procTemp);
                                }

                            } else {
                                transactionArray.Add(procTemp);
                            }
                        }
                    }
                }
            } catch(Exception e) {
                throw;
            }
        }

//**********************************************************************************
//** Job Request functions
//**********************************************************************************



        private bool AreAllTransactionForTheSameBatch(cDocument doc, cTransaction[] transactionArray, out List<int> transactionList)
        {
            bool bRtnval = false;
            transactionList = new List<int>();
            transactionList.Add(transactionArray[0].TransactionID);
            for (int i = 1; i < transactionArray.Length; i++)
            {
                if ((transactionArray[i].BankID != doc.BankID) ||
                    (transactionArray[i].LockboxID != doc.LockboxID) ||
                    (transactionArray[i].BatchID != doc.BatchID) ||
                    (transactionArray[i].DepositDate != doc.DepositDate))
                {
                    //a batch mis matches go ahead an loop individually
                    return bRtnval;
                }
                else
                {
                    transactionList.Add(transactionArray[i].TransactionID);
                }
            }
            //they all match
            bRtnval = true;
            return bRtnval;
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transactionArray"></param>
        /// <param name="sctJobRequestOptions"></param>
        /// <param name="bDisplayBatchID"></param>
        /// <param name="LOF"></param>
        private void createPDF(cTransaction[] transactionArray
                             , JobRequestOptions sctJobRequestOptions
                             , bool bDisplayBatchID
                             , out long LOF) {

            string completedFilePath = string.Empty;
            bool bGetAllImagesAtOnce;
            ciTextSharpDoc objiTextSharpDoc = null;
            
            try
            {
                completedFilePath = serverOptions.onlineArchivePath
                                         + this.jobID 
                                         + "." 
                                         + FILE_EXT_PDF;

                bGetAllImagesAtOnce = false;

                LOF = 0;

                // Create the Pdf
                objiTextSharpDoc = new ciTextSharpDoc();

                // Create a new PdfWriter object, specifying the output stream
                using(FileStream objFS = new FileStream(completedFilePath, FileMode.Create)) {
                    objPdfWriter = PdfWriter.GetInstance(objiTextSharpDoc, objFS);

                    // Open the Document for writing
                    objiTextSharpDoc.Open();
                
                    objiTextSharpDoc.outputError += new outputErrorEventHandler(object_outputError);

                    // Define default image storage location
                    string strRootImageLocation = siteOptions.imagePath;
                    //Lookup FileGroup location if exists
                    if (siteOptions.imageStorageMode == ImageStorageMode.FILEGROUP) {
                        WorkgroupDTO workgroup = ItemProcDAL.GetWorkgroup(transactionArray[0].BankID,
                            transactionArray[0].LockboxID);
                        // Lookup workgroup
                        if (workgroup!=null){
                            if (!string.IsNullOrWhiteSpace(workgroup.FileGroup)) {
                                if (Directory.Exists(workgroup.FileGroup)) {
                                    strRootImageLocation = workgroup.FileGroup;
                                }
                                else {
                                    // Invalid directory log error.
                                    eventLog.logError(new eImageDirectoryNotFoundException(strRootImageLocation));
                                }
                            }
                        }
                    }

                    using(cipoImages ipoImages = new cipoImages(this.siteKey, siteOptions.imageStorageMode, strRootImageLocation)) {

                        if (transactionArray.Length > 1)
                        {
                            List<int> transactionList = new List<int>();
                            cDocument document = new cDocument();
                            document.BankID = transactionArray[0].BankID;
                            document.LockboxID = transactionArray[0].LockboxID;
                            document.BatchID = transactionArray[0].BatchID;
                            document.BatchNumber = transactionArray[0].BatchNumber;
                            document.DepositDate = transactionArray[0].DepositDate;
                            document.BatchSequence = -1;
         
                            if (AreAllTransactionForTheSameBatch(document, transactionArray, out transactionList))
                            {
                                bGetAllImagesAtOnce = ipoImages.GetAllImages(document);
                                if (bGetAllImagesAtOnce) {
	                                //only pass in first transaction we will get all the image associated with entire batch

                                    
	                                appendTransactionImages(document
	                                                         , transactionList
	                                                         , sctJobRequestOptions.displayScannedCheck
	                                                         , sctJobRequestOptions.ImageFilterOption
	                                                         , ref objiTextSharpDoc
	                                                         , sctJobRequestOptions.displayRemitterNameInPDF
                                                             , bDisplayBatchID
	                                                         , sctJobRequestOptions.checkImageDisplayMode
	                                                         , sctJobRequestOptions.documentImageDisplayMode
	                                                         , ipoImages
                                                             , sctJobRequestOptions.olCLientAccountLabel_Override);
                        	    }
                            } else{
                                bGetAllImagesAtOnce = false;
                            }

                            //if you can get the images all at once then go get each image by itself.
                            if (!bGetAllImagesAtOnce)
                            {
                                //you have different batches you need to loop over individually
                                for (int i = 0;  i < transactionArray.Length; i++)
                                {
                                    appendTransactionImages(transactionArray[i]
                                                   , sctJobRequestOptions.displayScannedCheck
                                                   , sctJobRequestOptions.ImageFilterOption
                                                   , ref objiTextSharpDoc
                                                   , sctJobRequestOptions.displayRemitterNameInPDF
                                                   , bDisplayBatchID
                                                   , sctJobRequestOptions.checkImageDisplayMode
                                                   , sctJobRequestOptions.documentImageDisplayMode
                                                   , ipoImages
                                                   , sctJobRequestOptions.olCLientAccountLabel_Override);
                                }
                            }

                        }
                        else
                        {

                            appendTransactionImages(transactionArray[0]
                                                   , sctJobRequestOptions.displayScannedCheck
                                                   , sctJobRequestOptions.ImageFilterOption
                                                   , ref objiTextSharpDoc
                                                   , sctJobRequestOptions.displayRemitterNameInPDF
                                                   , bDisplayBatchID
                                                   , sctJobRequestOptions.checkImageDisplayMode
                                                   , sctJobRequestOptions.documentImageDisplayMode
                                                   , ipoImages
                                                   , sctJobRequestOptions.olCLientAccountLabel_Override);
                        }

                    
                     } // end of using 

                    // Save PDF Document
                    objiTextSharpDoc.Cleanup(objPdfWriter, this.serverOptions);

                    eventLog.logEvent("PDF Document created : " + completedFilePath
                                    , this.GetType().Name
                                    , MessageImportance.Essential);

                    LOF = ipoLib.LOF(completedFilePath);
                }
            }
            catch(Exception e)
            {
                eventLog.logEvent(e.Message, MessageImportance.Essential);
                if ((!string.IsNullOrEmpty(completedFilePath)) && File.Exists(completedFilePath)) {
                    File.Delete(completedFilePath); 
                }

                throw;
            }
            finally {

                if(objiTextSharpDoc != null) {
                    objiTextSharpDoc.Close();
                }

                if(objPdfWriter != null) {
                    objPdfWriter.Close();
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="displayScannedCheck"></param>
        /// <param name="imageFilterOption"></param>
        /// <param name="iTextSharpDoc"></param>
        /// <param name="displayRemitterNameInPDF"></param>
        /// <param name="displayBatchID"></param>
        /// <param name="checkImageDisplayMode"></param>
        /// <param name="documentImageDisplayMode"></param>
        /// <param name="ipoImages"></param>
        private void appendTransactionImages(cTransaction transaction
                                           , bool displayScannedCheck
                                           , OLFImageFilterOption imageFilterOption
                                           , ref ciTextSharpDoc iTextSharpDoc
                                           , bool displayRemitterNameInPDF
                                           , bool displayBatchID
                                           , OLFImageDisplayMode checkImageDisplayMode
                                           , OLFImageDisplayMode documentImageDisplayMode
                                           , cipoImages ipoImages
                                           , string olClientAccountDisplayOverride)
        {
            cCheck[] checks;
            cDocument[] documents;
            checks = GetTransactionChecks(transaction);
            documents = GetTransactionDocuments(transaction, displayScannedCheck);
            appendTransactionImages(checks, documents, imageFilterOption, ref iTextSharpDoc,
                                displayRemitterNameInPDF, displayBatchID, checkImageDisplayMode, documentImageDisplayMode, ipoImages,
                                 transaction.BankID, transaction.LockboxID, olClientAccountDisplayOverride);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="transactionList"></param>
        /// <param name="displayScannedCheck"></param>
        /// <param name="imageFilterOption"></param>
        /// <param name="iTextSharpDoc"></param>
        /// <param name="displayRemitterNameInPDF"></param>
        /// <param name="displayBatchID"></param>
        /// <param name="checkImageDisplayMode"></param>
        /// <param name="documentImageDisplayMode"></param>
        /// <param name="ipoImages"></param>
        private void appendTransactionImages(cDocument doc
                                           , List<int> transactionList
                                           , bool displayScannedCheck
                                           , OLFImageFilterOption imageFilterOption
                                           , ref ciTextSharpDoc iTextSharpDoc
                                           , bool displayRemitterNameInPDF
                                           , bool displayBatchID
                                           , OLFImageDisplayMode checkImageDisplayMode
                                           , OLFImageDisplayMode documentImageDisplayMode
                                           , cipoImages ipoImages
                                           , string olClientAccountDisplayOverride)
    {
            cCheck[] checks;
            cDocument[] documents;
            checks = GetBatchChecks(doc, transactionList);
            documents = GetBatchDocuments(doc, displayScannedCheck, transactionList);
            appendTransactionImages(checks, documents, imageFilterOption, ref iTextSharpDoc,
                                displayRemitterNameInPDF, displayBatchID, checkImageDisplayMode, documentImageDisplayMode, ipoImages,
                                doc.BankID, doc.LockboxID, olClientAccountDisplayOverride);
           
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="checks"></param>
        /// <param name="documents"></param>
        /// <param name="imageFilterOption"></param>
        /// <param name="iTextSharpDoc"></param>
        /// <param name="displayRemitterNameInPDF"></param>
        /// <param name="displayBatchID"></param>
        /// <param name="checkImageDisplayMode"></param>
        /// <param name="documentImageDisplayMode"></param>
        /// <param name="ipoImages"></param>
        /// <param name="bankID"></param>
        /// <param name="lockboxID"></param>
        private void appendTransactionImages(cCheck[] checks
                                           , cDocument[] documents
                                           , OLFImageFilterOption imageFilterOption
                                           , ref ciTextSharpDoc iTextSharpDoc
                                           , bool displayRemitterNameInPDF
                                           , bool displayBatchID
                                           , OLFImageDisplayMode checkImageDisplayMode
                                           , OLFImageDisplayMode documentImageDisplayMode
                                           , cipoImages ipoImages
                                           , int bankID
                                           , int lockboxID
                                           , string olClientAccountDisplayOverride) {

            int intImageFrontCount = 0;
            int intImageBackCount = 0;

			int intTotImageFrontCount = 0;
            int intTotImageBackCount = 0;

            int intCurrentCheckIndex=0;
            int intCurrentDocumentIndex=0;

           
            

            while(true) {

                if((intCurrentCheckIndex<checks.Length) && (intCurrentDocumentIndex<documents.Length))  {
                    if((checks[intCurrentCheckIndex].DepositDate != documents[intCurrentDocumentIndex].DepositDate) ||
                       (checks[intCurrentCheckIndex].BatchID != documents[intCurrentDocumentIndex].BatchID)) {
                        if((imageFilterOption == OLFImageFilterOption.All)||(imageFilterOption == OLFImageFilterOption.DocsOnly)) {
                            appendDocument(documents[intCurrentDocumentIndex], documentImageDisplayMode, displayBatchID, ref iTextSharpDoc, ipoImages, out intImageFrontCount, out intImageBackCount, olClientAccountDisplayOverride);
                            intTotImageFrontCount += intImageFrontCount;
                            intTotImageBackCount += intImageBackCount;
                        }
                        ++intCurrentDocumentIndex;

                    } else if((checks[intCurrentCheckIndex].DepositDate == documents[intCurrentDocumentIndex].DepositDate) &&
                              (checks[intCurrentCheckIndex].BatchID == documents[intCurrentDocumentIndex].BatchID)) {
                        if(checks[intCurrentCheckIndex].TransactionID > documents[intCurrentDocumentIndex].TransactionID) {
                            if((imageFilterOption == OLFImageFilterOption.All)||(imageFilterOption == OLFImageFilterOption.DocsOnly)) {
                                appendDocument(documents[intCurrentDocumentIndex], documentImageDisplayMode, displayBatchID, ref iTextSharpDoc, ipoImages, out intImageFrontCount, out intImageBackCount, olClientAccountDisplayOverride);
                                intTotImageFrontCount += intImageFrontCount;
                                intTotImageBackCount += intImageBackCount;
                            }
                            ++intCurrentDocumentIndex;
                        } else {
                            if((imageFilterOption == OLFImageFilterOption.All)||(imageFilterOption == OLFImageFilterOption.ChecksOnly)) {
                                appendCheck(checks[intCurrentCheckIndex], checkImageDisplayMode, ref iTextSharpDoc, displayRemitterNameInPDF, displayBatchID, ipoImages, out intImageFrontCount, out intImageBackCount, olClientAccountDisplayOverride);
                                intTotImageFrontCount += intImageFrontCount;
                                intTotImageBackCount += intImageBackCount;
                            }
                            ++intCurrentCheckIndex;
                        }
                    } else {
                        if((imageFilterOption == OLFImageFilterOption.All)||(imageFilterOption == OLFImageFilterOption.ChecksOnly)) {
                            appendCheck(checks[intCurrentCheckIndex], checkImageDisplayMode, ref iTextSharpDoc, displayRemitterNameInPDF, displayBatchID, ipoImages, out intImageFrontCount, out intImageBackCount, olClientAccountDisplayOverride);
                            intTotImageFrontCount += intImageFrontCount;
                            intTotImageBackCount += intImageBackCount;
                        }
                        ++intCurrentCheckIndex;
                    }
                } else if(intCurrentCheckIndex<checks.Length) {
                    if((imageFilterOption == OLFImageFilterOption.All)||(imageFilterOption == OLFImageFilterOption.ChecksOnly)) {
                        appendCheck(checks[intCurrentCheckIndex], checkImageDisplayMode, ref iTextSharpDoc, displayRemitterNameInPDF, displayBatchID, ipoImages, out intImageFrontCount, out intImageBackCount, olClientAccountDisplayOverride);
                        intTotImageFrontCount += intImageFrontCount;
                        intTotImageBackCount += intImageBackCount;
                    }
                    ++intCurrentCheckIndex;
                } else if(intCurrentDocumentIndex<documents.Length) {
                    if((imageFilterOption == OLFImageFilterOption.All)||(imageFilterOption == OLFImageFilterOption.DocsOnly)) {
                        appendDocument(documents[intCurrentDocumentIndex], documentImageDisplayMode, displayBatchID, ref iTextSharpDoc, ipoImages, out intImageFrontCount, out intImageBackCount, olClientAccountDisplayOverride);
                        intTotImageFrontCount += intImageFrontCount;
                        intTotImageBackCount += intImageBackCount;
                    }
                    ++intCurrentDocumentIndex;
                } else {
                    break;
                }

            }

            if (intTotImageFrontCount > 0)
            {
                base.LogActivity(ActivityCodes.acViewImageFront
                            , this.sessionID
                            , this.jobID
                            , intTotImageFrontCount
                            , bankID
                            , lockboxID);
            }

            if (intTotImageBackCount > 0)
            {
                base.LogActivity(ActivityCodes.acViewImageBack
                            , this.sessionID
                            , this.jobID
                            , intTotImageBackCount
                            , bankID
                            , lockboxID);
            }

            if (intTotImageFrontCount + intTotImageBackCount > 0)
            {
                base.LogActivity(ActivityCodes.acViewAll
                            , this.sessionID
                            , this.jobID
                            , intTotImageFrontCount + intTotImageBackCount
                            , bankID
                            , lockboxID);
            }

        }   //appendTransactionImages


        /// <summary>
        /// 
        /// </summary>
        /// <param name="check"></param>
        /// <param name="page"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="displayRemitterNameInPDF"></param>
        /// <param name="displayBatchID"></param>
        /// <param name="ipoImages"></param>
        /// <param name="ImageFrontCount"></param>
        /// <param name="ImageBackCount"></param>
        private void appendCheck(cCheck check
                               , OLFImageDisplayMode page
                               , ref ciTextSharpDoc pdfDoc
                               , bool displayRemitterNameInPDF
                               , bool displayBatchID
                               , cipoImages ipoImages
                               , out int ImageFrontCount
                               , out int ImageBackCount
                               , string olClientAccountDisplayOverride) {

            int intStartPage;
            int intEndPage;
            // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
            // ipoImages is now returning a memory stream, not just a file name
            //string strImageFilePath;
            byte[] objBytes;
            bool bolPlaceholderAppended = false; //CR 28347 9/30/2010 JNE
            bool bolImageFrontAppended = false;

            ImageFrontCount = 0;
            ImageBackCount = 0;

            establishPageRanges(page
                              , out intStartPage
                              , out intEndPage);

      
            for (int intCurrentPage = intStartPage; intCurrentPage < intEndPage + 1; ++intCurrentPage) {


                // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                var response = ipoImages.GetImage(check, intCurrentPage);

                // WI 71542 : Using the sitekey from the request, not the server's sitekey.
                if ((pdfDoc.appendCheckImage(response.ImageBytes
                        , new SiteKeyFallbackImageProvider(siteKey, serverOptions)
                        , (intCurrentPage == cPICS.PAGE_FRONT)
                        , (intCurrentPage == cPICS.PAGE_BACK)
                        , out bolPlaceholderAppended))) {


                    switch (intCurrentPage) {
                        case cPICS.PAGE_FRONT:
                            bolImageFrontAppended = true;
                            if (intEndPage == 0) {
                                pdfDoc.appendCheckDetails(check, displayRemitterNameInPDF, displayBatchID, olClientAccountDisplayOverride);
                            }
                            break;

                        case cPICS.PAGE_BACK:
                            pdfDoc.appendCheckDetails(check, displayRemitterNameInPDF, displayBatchID, olClientAccountDisplayOverride);
                            break;
                    }
                } else {
                    // image insert failed
                    if ((intCurrentPage == cPICS.PAGE_BACK) && (bolImageFrontAppended || bolPlaceholderAppended)) {
                        pdfDoc.appendCheckDetails(check, displayRemitterNameInPDF, displayBatchID, olClientAccountDisplayOverride);
                    } else if (bolPlaceholderAppended) {
                        pdfDoc.appendCheckDetails(check, displayRemitterNameInPDF, displayBatchID, olClientAccountDisplayOverride);
                    }
                }

                if (!bolPlaceholderAppended) {
                    switch (intCurrentPage) {
                        case cPICS.PAGE_FRONT:
                            ++ImageFrontCount;
                            break;
                        case cPICS.PAGE_BACK:
                            ++ImageBackCount;
                            break;
                    }
                }
            }   //for

            pdfDoc.NewPage();
            
        }   //appendAllImagePages


        /// <summary>
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="page"></param>
        /// <param name="displayBatchID"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="ipoImages"></param>
        /// <param name="ImageFrontCount"></param>
        /// <param name="ImageBackCount"></param>
        private void appendDocument(cDocument document
                                  , OLFImageDisplayMode page
                                  , bool displayBatchID
                                  , ref ciTextSharpDoc pdfDoc
                                  , cipoImages ipoImages
                                  , out int ImageFrontCount
                                  , out int ImageBackCount
                                  , string olClientAccountDisplayOverride) {

            int intStartPage;
            int intEndPage;
            // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
            //string strImageFilePath;
            byte[] objBytes;
            bool bolPlaceholderAppended;

            ImageFrontCount = 0;
            ImageBackCount = 0;

            establishPageRanges(page
                              , out intStartPage
                              , out intEndPage);
 
            for(int intCurrentPage=intStartPage;intCurrentPage<intEndPage+1;++intCurrentPage) {


                // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 
                var response = ipoImages.GetImage(document, intCurrentPage);

                // MEH CR 23584 05/20/2009 integraPAY Online Interface Into Hyland IMS Archive 

                // WI 71542 : We have this.SiteKey as well. (Instead of serverOptions.ipoResourceDir)

                if (pdfDoc.AppendImage(response,
                    new SiteKeyFallbackImageProvider(siteKey, serverOptions),
                    (intCurrentPage == cPICS.PAGE_FRONT),
                    out bolPlaceholderAppended, objPdfWriter))
                {
                    pdfDoc.appendDocumentDetails(document, displayBatchID, olClientAccountDisplayOverride);

                    if(!bolPlaceholderAppended) {
                        switch(intCurrentPage) {
                            case cPICS.PAGE_FRONT:
                                ++ImageFrontCount;
                                break;
                            case cPICS.PAGE_BACK:
                                ++ImageBackCount;
                                break;
                        }
                    }

                }   //if
            }   //for

            pdfDoc.NewPage();

        }   //appendAllImagePages

	}   //cJobRequestViewAll
}   //DMP.ipo.ipoImage
