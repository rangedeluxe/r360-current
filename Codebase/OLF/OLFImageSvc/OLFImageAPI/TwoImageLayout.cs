﻿using iTextSharp.text;

namespace WFS.RecHub.OLFImageAPI
{
    /// <summary>
    /// Given a bounding box, determines the appropriate per-image bounding boxes
    /// to fit two images (stacked vertically) into a PDF page.
    /// </summary>
    /// <remarks>
    /// Usage:
    /// This is used by ImageRpsReportBase. The process is:
    /// 1. The report asks for a bounding box for the first image.
    /// 2. The report calculates a scaling ratio to make the image fit in that bounding box.
    /// 3. The report draws the scaled image in the top-left of that bounding box.
    /// 4. The report tells this layout what height was actually used for the first image.
    /// 5. The report asks for a bounding box for the second image. This second bounding box
    ///    takes up the rest of the content area of the page.
    /// </remarks>
    public class TwoImageLayout : IImageLayout
    {
        /// <summary>
        /// Creates a <see cref="TwoImageLayout"/> for the given content rectangle
        /// and inter-image margin.
        /// </summary>
        /// <param name="contentArea">
        /// This is in iTextSharp backwards coordinates (Y=0 at the bottom of the page, positive=up).
        /// </param>
        /// <param name="verticalSpaceBetweenImages">
        /// Amount of vertical space to place between the two images.
        /// </param>
        public TwoImageLayout(RectangleReadOnly contentArea, float verticalSpaceBetweenImages)
        {
            ContentArea = contentArea;
            VerticalSpaceBetweenImages = verticalSpaceBetweenImages;

            ImageRectangle = CalculateFirstImageRectangle();
        }

        private RectangleReadOnly ContentArea { get; set; }
        private RectangleReadOnly ImageRectangle { get; set; }
        private float VerticalSpaceBetweenImages { get; set; }

        public void AddImage(float imageHeight)
        {
            ImageRectangle = CalculateSecondImageRectangle(imageHeight);
        }
        private RectangleReadOnly CalculateFirstImageRectangle()
        {
            var firstRectangleHeight = (ContentArea.Height - VerticalSpaceBetweenImages) / 2;
            return new RectangleReadOnly(
                llx: ContentArea.Left, lly: ContentArea.Top - firstRectangleHeight,
                urx: ContentArea.Right, ury: ContentArea.Top);
        }
        private RectangleReadOnly CalculateSecondImageRectangle(float firstImageHeight)
        {
            var secondRectangleTop = ContentArea.Top - firstImageHeight - VerticalSpaceBetweenImages;
            return new RectangleReadOnly(
                llx: ContentArea.Left, lly: ContentArea.Bottom,
                urx: ContentArea.Right, ury: secondRectangleTop);
        }
        /// <summary>
        /// Gets the bounding box for the next image.
        /// </summary>
        public RectangleReadOnly GetImageRectangle()
        {
            return ImageRectangle;
        }
    }
}