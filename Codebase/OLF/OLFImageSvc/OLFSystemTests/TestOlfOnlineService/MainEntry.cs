﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServicesClient;

namespace TestOlfOnlineService
{
    class MainEntry
    {
        static void Main(string[] args)
        {
            OLFOnlineClient olfOnlineClient = new OLFOnlineClient("IPOnline");
            var request = new ImageRequestDto()
            {
                SiteKey = "IPOnline",
                BatchPaymentSource = "IMAGERPSOMAHA",
                PicsDate = 20171111,
                BankId = 100,
                WorkgroupId = 3030
            };
            Console.WriteLine(" ");
            Console.WriteLine("Test 1");
            var response = olfOnlineClient.ImagesExist(request);

            EvaluateResponse(response);

            var request2 = new ImageRequestDto()
            {
                SiteKey = "IPOnline",
                BatchPaymentSource = "IMAGERPSOMAHA",
                PicsDate = 20171111,
                BankId = 100,
                WorkgroupId = 3
            };

            Console.WriteLine(" ");
            Console.WriteLine("Test 2");
            var response2 = olfOnlineClient.ImagesExist(request2);

            EvaluateResponse(response2);

            var request3 = new ImageRequestDto()
            {
                SiteKey = "IPOnline",
                BatchPaymentSource = "IMAGERPSOMAHA",
                PicsDate = 20171111,
                BankId = 100,
                WorkgroupId = 3030,
                SourceBatchId = 10201
            };
            Console.WriteLine(" ");
            Console.WriteLine("Test 3");
            var response3 = olfOnlineClient.ImagesExist(request3);

            EvaluateResponse(response3);

        }

        private static void EvaluateResponse(OlfResponseDto response)
        {
            if (response.IsSuccessful)
            {
                Console.WriteLine("Call was a success");
                if (response.ImagesAvailable)
                {
                    Console.WriteLine("images are available");
                }
                else
                {
                    Console.WriteLine("no images are available");
                }
            }
            else
            {
                Console.WriteLine("Call failed");
            }
        }
    }
}
