﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;

namespace LocateImageTests.Mocks
{
    public class MockSystemIo : ISystemIo
    {

        private bool _exists = false;
        private string[] _files;
        private string _combine;

        public MockSystemIo(bool exists, string[] files, string combine)
        {
            _exists = exists;
            _files = files;
            _combine = combine;
        }

        public bool Exists(string path)
        {
            return _exists;
        }

        public string[] GetFiles(string path, string searchPattern)
        {
            return _files;
        }

        public string[] EnumerateFiles(string path, string searchPattern)
        {
            return _files;
        }

        public string Combine(string image, string batchPaymentSource, string pictures, string bankId,
            string workgroupId, string importTypeShortName)
        {
            return _combine;
        }
    }
}
