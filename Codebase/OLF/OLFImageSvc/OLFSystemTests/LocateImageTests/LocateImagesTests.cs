﻿using System;
using LocateImageTests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServicesAPI.helper;
using WFS.RecHub.Common.Log;

namespace LocateImageTests
{
    [TestClass]
    public class LocateImagesTests
    {
        private ImageRequestDto _request;
        private OlfResponseDto _response;
        [TestMethod]
        public void TestMethodImagesAvailable()
        {
            cSiteOptions options = new cSiteOptions("UnitTest");

            string[] files = {"test.bmp", "test2.jpg", "test3.doc"};

            MockSystemIo systemIo = new MockSystemIo(true, files, "MockUnittest");
            LocateImages locateImages = new LocateImages(null, options, systemIo);

            _request = new ImageRequestDto
            {
                SiteKey = "IPOnline",
                IsCheck = true,
                BankId = 1,
                WorkgroupId = 4350,
                BatchId = 137,
                PicsDate = 20171111,
                BatchSequence = 1,
                TransactionId = 1,
                SessionId = Guid.NewGuid()
            };
            _response = locateImages.AnyImagesExist(_request);
            Assert.AreEqual(_response.IsSuccessful, true);
            Assert.AreEqual(_response.ImagesAvailable, true);
        }

        [TestMethod]
        public void TestMethodNoFiles()
        {
            cSiteOptions options = new cSiteOptions("UnitTest");

            string[] files = new string[0];

            MockSystemIo systemIo = new MockSystemIo(true, files, "MockUnittest");
            LocateImages locateImages = new LocateImages(null, options, systemIo);

            _request = new ImageRequestDto
            {
                SiteKey = "IPOnline",
                IsCheck = true,
                BankId = 1,
                WorkgroupId = 4350,
                BatchId = 137,
                PicsDate = 20171111,
                BatchSequence = 1,
                TransactionId = 1,
                SessionId = Guid.NewGuid()
            };
            _response = locateImages.AnyImagesExist(_request);
            Assert.AreEqual(_response.IsSuccessful, true);
            Assert.AreEqual(_response.ImagesAvailable, false);
        }
    }
}
