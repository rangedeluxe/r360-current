﻿using System;
using System.Collections.Generic;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.DAL.OLFServicesDAL;
using WFS.RecHub.DAL.OLFServicesDAL.Fakes;

namespace OLFServicesDAL_UnitTests
{
    [TestClass]
    public class ImageDisplayModeCacheTests
    {
        private void AssertImageDisplayModes(ImageDisplayModes imageDisplayModes,
            OLFImageDisplayMode expectedCheckImageDisplayMode, OLFImageDisplayMode expectedDocumentImageDisplayMode,
            string message)
        {
            Assert.AreEqual(expectedCheckImageDisplayMode, imageDisplayModes.CheckImageDisplayMode,
                "Check image display mode " + message);
            Assert.AreEqual(expectedDocumentImageDisplayMode, imageDisplayModes.DocumentImageDisplayMode,
                "Document image display mode " + message);
        }
        private ImageDisplayModeCache CreateCache(FakesDelegates.Func<int, int, ImageDisplayModes> getImageDisplayModes)
        {
            var stubGetter = new StubIGetImageDisplayModesDal
            {
                GetImageDisplayModesInt32Int32 = getImageDisplayModes
            };
            return new ImageDisplayModeCache(stubGetter);
        }

        [TestMethod]
        public void MultipleRequests()
        {
            var dictionary = new Dictionary<Tuple<int, int>, ImageDisplayModes>
            {
                [Tuple.Create(1, 5)] = new ImageDisplayModes(
                    OLFImageDisplayMode.FrontOnly, OLFImageDisplayMode.FrontOnly),
                [Tuple.Create(2, 5)] = new ImageDisplayModes(
                    OLFImageDisplayMode.BothSides, OLFImageDisplayMode.FrontOnly),
                [Tuple.Create(2, 6)] = new ImageDisplayModes(
                    OLFImageDisplayMode.BothSides, OLFImageDisplayMode.BothSides),
            };
            var cache = CreateCache(
                (siteBankId, siteWorkgroupId) => dictionary[Tuple.Create(siteBankId, siteWorkgroupId)]);

            AssertImageDisplayModes(cache.GetImageDisplayModes(1, 5),
                OLFImageDisplayMode.FrontOnly, OLFImageDisplayMode.FrontOnly, "First workgroup");
            AssertImageDisplayModes(cache.GetImageDisplayModes(2, 5),
                OLFImageDisplayMode.BothSides, OLFImageDisplayMode.FrontOnly, "Second workgroup");
            AssertImageDisplayModes(cache.GetImageDisplayModes(2, 6),
                OLFImageDisplayMode.BothSides, OLFImageDisplayMode.BothSides, "Third workgroup");
        }
        [TestMethod]
        public void RepeatedRequestsAreNotPassedThrough()
        {
            var log = new List<string>();
            var cache = CreateCache((siteBankId, siteWorkgroupId) =>
            {
                log.Add($"({siteBankId}, {siteWorkgroupId})");
                return new ImageDisplayModes();
            });

            cache.GetImageDisplayModes(1, 5);
            cache.GetImageDisplayModes(1, 6);
            cache.GetImageDisplayModes(2, 6);
            cache.GetImageDisplayModes(1, 5);

            Assert.AreEqual("(1, 5); (1, 6); (2, 6)", string.Join("; ", log), "Calls passed through to DAL");
        }
        [TestMethod]
        public void RepeatedRequestsReturnCachedValue()
        {
            var cache = CreateCache((siteBankId, siteWorkgroupId) =>
                siteBankId == 1 && siteWorkgroupId == 5
                    ? new ImageDisplayModes(OLFImageDisplayMode.BothSides, OLFImageDisplayMode.BothSides)
                    : new ImageDisplayModes(OLFImageDisplayMode.FrontOnly, OLFImageDisplayMode.FrontOnly));

            var workgroupOne = cache.GetImageDisplayModes(1, 5);
            var workgroupTwo = cache.GetImageDisplayModes(1, 6);
            var workgroupThree = cache.GetImageDisplayModes(2, 6);
            var workgroupOneRepeat = cache.GetImageDisplayModes(1, 5);

            AssertImageDisplayModes(workgroupOne,
                OLFImageDisplayMode.BothSides, OLFImageDisplayMode.BothSides, "Workgroup 1, first iteration");
            AssertImageDisplayModes(workgroupTwo,
                OLFImageDisplayMode.FrontOnly, OLFImageDisplayMode.FrontOnly, "Workgroup 2");
            AssertImageDisplayModes(workgroupThree,
                OLFImageDisplayMode.FrontOnly, OLFImageDisplayMode.FrontOnly, "Workgroup 3");
            AssertImageDisplayModes(workgroupOneRepeat,
                OLFImageDisplayMode.BothSides, OLFImageDisplayMode.BothSides, "Workgroup 1, second iteration");
        }
    }
}
