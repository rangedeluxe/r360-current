﻿using System;
using System.Data;
using System.IO;
using ipoImages_UnitTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.ItemProcDAL;

namespace ItemProcDAL_UnitTest {
    [TestClass]
    public class cItemProcDAL_UnitTest {
        [TestMethod]
        public void GetWorkgroup_NullFileGroup_Should_Return_Null() {
            var itemProcDALTestDumby = new ItemProcDAL_TestDumby("IPOnline") {
                WorkGroupDataTable=XMLToDataTable(Properties.Resources.GetWorkgroup_Null_Filegroup, 
                    Properties.Resources.GetWorkgroup_Table_Layout)
            };
            Assert.IsNull(itemProcDALTestDumby.GetWorkgroup(1,1));
        }
        
        [TestMethod]
        public void GetWorkgroup_NullDataSet_Should_Return_Null() {
            var itemProcDALTestDumby = new ItemProcDAL_TestDumby("IPOnline") { WorkGroupDataTable = null };
            Assert.IsNull(itemProcDALTestDumby.GetWorkgroup(1, 1));
        }

        [TestMethod]
        public void GetWorkgroup_GoodFileGroup_Should_Return_FileGroup() {
            var itemProcDALTestDumby = new ItemProcDAL_TestDumby("IPOnline") {
                WorkGroupDataTable = XMLToDataTable(Properties.Resources.GetWorkgroup_Good_Filegroup,
                    Properties.Resources.GetWorkgroup_Table_Layout)
            };
            Assert.IsTrue(@"E:\ImageStore\".Equals(itemProcDALTestDumby.GetWorkgroup(1,1).FileGroup));
        }

        private DataTable XMLToDataTable(string xmlData, string xsdData) {
            DataSet tempDataSet = new DataSet();
            tempDataSet.ReadXmlSchema(new StringReader(xsdData));
            tempDataSet.ReadXml(new StringReader(xmlData));
            return tempDataSet.Tables[0];
        }
    }
}
