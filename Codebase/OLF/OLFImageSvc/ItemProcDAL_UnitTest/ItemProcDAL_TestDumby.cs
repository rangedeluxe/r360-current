﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using WFS.RecHub.DAL;

namespace ipoImages_UnitTest {
    public class ItemProcDAL_TestDumby: cItemProcDAL {
        public ItemProcDAL_TestDumby(string siteKey) : base(siteKey) {
        }

        public DataTable WorkGroupDataTable { get; set; }

        public override bool GetLockbox(int BankID, int ClientAccountID, out DataTable dt) {
            bool result = true;

            dt = WorkGroupDataTable;
            return result;
        }
    }
}
