﻿using System;
using WFS.RecHub.OLFImageSvc;

namespace OLFImageServiceConsole
{
    public static class Program
    {
        public static void Main()
        {
            var worker = new OlfImageWorker();
            worker.Start();
            Console.WriteLine("OLF started. Press any key to stop.");
            Console.ReadKey(intercept: true);
            worker.Stop();
        }
    }
}
