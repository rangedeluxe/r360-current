using System.ComponentModel;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   
* Date:     
*
* Purpose:  
*
* Modification History
* WI 98021 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFImageSvc
******************************************************************************/
namespace WFS.RecHub.OLFImageSvc {

	/// <summary>
	/// Summary description for ProjectInstaller.
	/// </summary>
	[RunInstaller(true)]
	public class ProjectInstaller : System.Configuration.Install.Installer {

        private System.ServiceProcess.ServiceProcessInstaller ipoImageServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ipoImageServiceInstaller;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

        /// <summary>
        /// 
        /// </summary>
		public ProjectInstaller() {
			// This call is required by the Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing ) {
			if( disposing ) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ipoImageServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ipoImageServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ipoImageServiceProcessInstaller
            // 
            this.ipoImageServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ipoImageServiceProcessInstaller.Password = null;
            this.ipoImageServiceProcessInstaller.Username = null;
            // 
            // ipoImageServiceInstaller
            // 
            this.ipoImageServiceInstaller.DisplayName = "WFS IPOnline Image Service";
            this.ipoImageServiceInstaller.ServiceName = "WFS IPOnline Image Service";
            this.ipoImageServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ipoImageServiceProcessInstaller,
            this.ipoImageServiceInstaller});

        }
		#endregion
	}
}
