/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     11/20/2003
*
* Purpose:  IntegraPAY Online Image Service.
*
* Modification History
*   11/20/2003 JMC
*        - Created class
*   06/14/2005 CR 11465 JMC
*        - Modified class to log MaxThreads on startup.
*   11/02/2005 CR 14209 JMC
*        - Added SetDeadlockPriority to logfile output on service startup.
*   11/04/2005 CR 14211 JMC
*        - Added QueryRetryAttempts to logfile output on service startup.
* CR 54779 CEJ 10/23/2012
*   - Change the image service to allow it to be hosted as a Windows service
* WI 70276 JMC 12/05/2012
*   - Added ILMerge post-build event to the project so OLFImageImportSvc can be
*     released as a single executable.
* WI 98042 WJS 4/19/2013
*   - Remove hosting of OLF Servie from image service
* WI 98021 CRG 04/10/2013
*   - Change the NameSpace, Framework, Using's and Flower Boxes for OLFImageSvc
* WI 71542 BLR 05/07/2014
*   - Removed references to ipoTemplatesDir & ipoResourcesDir.
* WI 143450 RDS 05/22/2014	Move Binding configuration to config files
******************************************************************************/

namespace WFS.RecHub.OLFImageSvc
{
    /// <author>Joel Caples</author>
    /// <summary>
    /// ipo Image Service
    /// </summary>
    public class OLFImageService : System.ServiceProcess.ServiceBase
    {
        /// Required designer variable.
        private System.ComponentModel.Container components = null;

        private OlfImageWorker Worker { get; set; }

        public OLFImageService()
        {
            // This call is required by the Windows.Forms Component Designer.
            InitializeComponent();

            Worker = new OlfImageWorker();
        }

        /// <summary>
        /// The main entry point for the process
        /// </summary>
        private static void Main()
        {
            System.ServiceProcess.ServiceBase[] ServicesToRun;

            // More than one user Service may run within the same process. To add
            // another service to this process, change the following line to
            // create a second service object. For example,
            //
            //   ServicesToRun = New System.ServiceProcess.ServiceBase[]
            //      {new Service1(), new MySecondUserService()};
            //
            ServicesToRun = new System.ServiceProcess.ServiceBase[] {new OLFImageService()};

            System.ServiceProcess.ServiceBase.Run(ServicesToRun);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CanPauseAndContinue = true;
            this.ServiceName = "OLFImageSvc";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        protected override void OnStart(string[] args)
        {
            Worker.Start();
        }
        protected override void OnStop()
        {
            Worker.Stop();
        }
        protected override void OnPause()
        {
            Worker.Pause();
        }
        protected override void OnContinue()
        {
            Worker.Continue();
        }
    }
}
