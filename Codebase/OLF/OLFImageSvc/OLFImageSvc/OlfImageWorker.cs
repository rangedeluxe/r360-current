using System;
using System.IO;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.OLFImageAPI;

namespace WFS.RecHub.OLFImageSvc
{
    public class OlfImageWorker
    {
        public OlfImageWorker()
        {
            ServerOptions = new cImageServiceOptions(ipoLib.INISECTION_IMAGE_SERVICE);
            EventLog = new cEventLog(ServerOptions.logFilePath, ServerOptions.logFileMaxSize,
                (MessageImportance) ServerOptions.loggingDepth);

            try
            {
                if (!(Directory.Exists(ServerOptions.onlineArchivePath)))
                {
                    throw (new Exception("Specified Online Archive Path does not"
                                         + " exist.  Service cannot attach to"
                                         + " specified directory and will not"
                                         + " function properly."));
                }

                FileMonitor = new FileSystemWatcher(ServerOptions.onlineArchivePath, "*.xml")
                {
                    EnableRaisingEvents = false
                };
                FileMonitor.Created += FileMonitor_Changed;
            }
            catch (Exception e)
            {
                LogError(e);
            }
        }

        private cEventLog EventLog { get; set; }
        private FileSystemWatcher FileMonitor { get; set; }
        private cImageServiceOptions ServerOptions { get; set; }

        public void Continue()
        {
            try
            {
                FileMonitor.EnableRaisingEvents = true;
                Log("OLF Image Service resumed.");

                cImageJobManager objImage = new cImageJobManager();
                objImage.processArchives();
                objImage = null;
            }
            catch (Exception e)
            {
                LogError("OLF Image Service continued with errors : " + e.Message, e.Source);
            }
        }
        /// <summary>
        /// Event handler, detects a new or modified file in the onlineArchive
        /// directory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileMonitor_Changed(object sender,
            System.IO.FileSystemEventArgs e)
        {
            try
            {
                string ChangeType = e.ChangeType.ToString();

                //display a message box for the appropriate changetype.
                if ((ChangeType == "Created") || (ChangeType == "Changed"))
                {
                    Log(e.Name + " Created - File: " + e.FullPath + " " + e.ChangeType);

                    cImageJobManager objImage = new cImageJobManager();
                    objImage.processArchives();
                    objImage = null;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
        }
        private void Log(string message)
        {
            EventLog.logEvent(message, GetType().Name, MessageImportance.Essential);
        }
        private void LogError(Exception exception)
        {
            EventLog.logError(exception);
        }
        private void LogError(string message, string source)
        {
            EventLog.logEvent(message, source, MessageType.Error, MessageImportance.Essential);
        }
        public void Pause()
        {
            try
            {
                FileMonitor.EnableRaisingEvents = false;
                Log("OLF Image Service paused.");
            }
            catch (Exception e)
            {
                LogError("OLF Image Service paused with errors : " + e.Message, e.Source);
            }
        }
        public void Start()
        {
            try
            {
                FileMonitor.EnableRaisingEvents = true;

                Log("OLF Image Service started normally.");
                Log("    Online Archive Directory: " + ServerOptions.onlineArchivePath);
                Log("    Max Threads: " + ServerOptions.MaxThreads);
                Log("    Set Deadlock Priority: " + ServerOptions.SetDeadlockPriority);
                Log("    Query Retry Attempts: " + ServerOptions.QueryRetryAttempts);

                cImageJobManager objImage = new cImageJobManager();
                objImage.processArchives();
                objImage = null;
            }
            catch (Exception e)
            {
                LogError("OLF Image Service started with errors : " + e.Message, e.Source);
            }
        }
        public void Stop()
        {
            try
            {
                FileMonitor.EnableRaisingEvents = false;
                Log("OLF Image Service stopped.");
            }
            catch (Exception e)
            {
                LogError("OLF Image Service stopped with errors : " + e.Message, e.Source);
            }
        }
    }
}