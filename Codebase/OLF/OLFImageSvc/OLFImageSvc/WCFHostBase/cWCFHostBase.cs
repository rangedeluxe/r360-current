﻿

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using WFS.RecHub.Common;


/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     10/09/2012
*
* Purpose:  Provides a base class to aid in hosting WCF services.
*
* Modification History
* CR 54779 CEJ 10/09/2012 
*  - Image Tier --> IIS to WCF
* WI 98021 CRG 04/10/2013
*   Change the NameSpace, Framework, Using's and Flower Boxes for OLFImageSvc
* WI 143450 RDS 05/22/2014	Move Binding configuration to config files
*******************************************************************************/
namespace WFS.RecHub.OLFServices{
	////************************************************************************
	///// <author>Charlie Johnson</author> 
	///// <summary>
	///// Signature for the LogEvent
	///// </summary>
	////************************************************************************
	//public delegate void dlgLogEvent(string sMessage, string sSource, MessageType mstEventType,
	//			MessageImportance msiEventImportance);

	////************************************************************************
	///// <author>Charlie Johnson</author> 
	///// <summary>
	///// The class contains the functionality to host given objects at given endpoint URLs
	///// </summary>
	////************************************************************************
	//public class cWCFHostBase : IDisposable {
	//	private List<ServiceHost> _lstServer = new List<ServiceHost>();
	//	private bool _disposed;
	//	private List<string> _lstReferencePathes = new List<string>();
	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// This event is raised when the WCFHostBase has information to log
	//	/// </summary>
	//	//************************************************************************
	//	public event dlgLogEvent LogEvent;

	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// Create an instance to host a given object for a given endpoint URL
	//	/// </summary>
	//	//************************************************************************
	//	public cWCFHostBase(string uri, object hostedObject):
	//			this(new Dictionary<string, object> {{uri, hostedObject}}) {
	//	}

	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// Create an instance to host given objects for given endpoint URLs
	//	/// </summary>
	//	/// <param name="hostedItems">The keys are the endpoint URLs and the values is the hosted objects</param>
	//	//************************************************************************
	//	public cWCFHostBase(Dictionary<string, object> hostedItems) {
	//		try {
	//			foreach (KeyValuePair<string, object> hstCurPair in hostedItems) {
	//				BasicHttpBinding bhbBinding = new BasicHttpBinding();

	//				bhbBinding.MaxReceivedMessageSize = 2147483647;
	//				bhbBinding.MessageEncoding = WSMessageEncoding.Mtom;
	//				bhbBinding.TransferMode = TransferMode.Streamed;
	//				bhbBinding.ReaderQuotas.MaxArrayLength = 2147483647;
	//				bhbBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
	//				bhbBinding.ReaderQuotas.MaxDepth = 2147483647;
	//				bhbBinding.ReaderQuotas.MaxNameTableCharCount = 2147483647;
	//				bhbBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
	//				if (hstCurPair.Key.Trim().ToLower().StartsWith("https:")) {
	//					bhbBinding.Security.Mode = BasicHttpSecurityMode.Transport;
	//				}
	//				else {
	//					bhbBinding.Security.Mode = BasicHttpSecurityMode.None;
	//				}

	//				ServiceHost svhNewHost = new ServiceHost(hstCurPair.Value.GetType());
	//				svhNewHost.AddServiceEndpoint(GetContractInterface(hstCurPair.Value.GetType()), bhbBinding, hstCurPair.Key);
	//				_lstServer.Add(svhNewHost);
	//			_disposed = false;
	//			AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
	//			}
	//		}
	//		catch(Exception ex) {
	//			OnLogEvent(ex.Message, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
	//		}
	//	}

	//	private static Type GetContractInterface(Type typHostedType) {
	//		Type typAns = null;

	//		foreach(Type typCurInterface in typHostedType.GetInterfaces()) {
	//			if(typCurInterface.GetCustomAttributes(typeof(System.ServiceModel.ServiceContractAttribute), true).Count()>0) {
	//				typAns = typCurInterface;
	//				break;
	//			}
	//		}
	//		return typAns;
	//	}

	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// This method is for adding directories of supporting DLLs for the hosted object to the assembly
	//	/// </summary>
	//	//************************************************************************
	//	public void AddReferencePath(string sPath) {
	//		if (!_lstReferencePathes.Contains(sPath.ToLower())) {
	//			_lstReferencePathes.Add(sPath.ToLower());
	//		}
	//	}

	//	System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args) {
	//		Assembly asmAns = null;

	//		foreach (string sCurPath in _lstReferencePathes) {
	//			string sCurDllName = Path.Combine(sCurPath, string.Format("{0}.dll", new AssemblyName(args.Name).Name));
	//			if (File.Exists(sCurDllName)) {
	//				asmAns = Assembly.LoadFrom(sCurDllName);
	//				break;
	//			}
	//		}
	//		return asmAns;
	//	}

	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// Begins the broadcasting of the hosted object
	//	/// </summary>
	//	//************************************************************************
	//	public void Open() {
	//		try {
	//			foreach (ServiceHost svhCurEndPoint in _lstServer) {
	//				svhCurEndPoint.Open();
	//			}
	//		}
	//		catch (Exception ex) {
	//			OnLogEvent(ex.Message, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
	//		}
	//	}

	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// Ends the broadcasting of the hosted object and released the endpoint URL
	//	/// </summary>
	//	//************************************************************************
	//	public void Close() {
	//		try {
	//			foreach (ServiceHost svhCurEndPoint in new List<ServiceHost>(_lstServer)) {
	//				svhCurEndPoint.Close();
	//				_lstServer.Remove(svhCurEndPoint);
	//			}
	//		}
	//		catch (Exception ex) {
	//			OnLogEvent(ex.Message, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
	//		}
	//	}

	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// Raised the Log Event
	//	/// </summary>
	//	//************************************************************************
	//	protected void OnLogEvent(string message, string source, MessageType eventType,
	//			MessageImportance eventImportance) {
	//		if (LogEvent != null) {
	//			LogEvent(message, source, eventType, eventImportance);
	//		}
	//	}

	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// For IDisposable support
	//	/// </summary>
	//	//************************************************************************
	//	public void Dispose() {
	//		Dispose(true);
	//		GC.SuppressFinalize(this);
	//	}

	//	//************************************************************************
	//	/// <author>Charlie Johnson</author> 
	//	/// <summary>
	//	/// For IDisposable support
	//	/// </summary>
	//	//************************************************************************
	//	public void Dispose(bool disposing) {
	//		if (!_disposed) {
	//			if (disposing) {
	//				if (_lstServer != null) {
	//					if (_lstServer.Count() > 0) {
	//						foreach (ServiceHost svhCurItem in _lstServer) {
	//							try {
	//								if (svhCurItem.State != CommunicationState.Closed) {
	//									svhCurItem.Close();
	//								}
	//							}
	//							catch(Exception ex) {
	//								OnLogEvent(ex.Message, this.GetType().Name, MessageType.Warning, MessageImportance.Essential);
	//							}
	//						}
	//					_lstServer.Clear();
	//					}
	//				}
	//			}
	//		}
	//	}
	//}
}
