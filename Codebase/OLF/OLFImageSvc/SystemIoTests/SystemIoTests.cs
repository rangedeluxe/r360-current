﻿using WFS.RecHub.ApplicationBlocks.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SystemIoTests
{
    [TestClass]
    public class SystemIoTests
    {
        [TestMethod]
        public void TestIncludeBatchSourceInCombine()
        {
            var systemIo = new SystemIo();

            var fullPath = systemIo.Combine(@"D:\WfsApps\RecHub", "TestBatchSource", "Img", "1", "123","Test");
            Assert.AreEqual(@"D:\WfsApps\RecHub\TestBatchSource\Img\1\123", fullPath);
        }

        [TestMethod]
        public void TestDoNotIncludeBatchSourceInCombine()
        {
            var systemIo = new SystemIo();

            var fullPath = systemIo.Combine(@"D:\WfsApps\RecHub", "IntegraPAY-I1", "Img", "1", "123", "integraPAY");
            Assert.AreEqual(@"D:\WfsApps\RecHub\Img\1\123", fullPath);

        }
    }
}
