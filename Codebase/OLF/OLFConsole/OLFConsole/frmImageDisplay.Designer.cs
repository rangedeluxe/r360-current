﻿namespace WFS.RecHub.OLFConsole {
    
    partial class frmImageDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmImageDisplay));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLockboxID = new System.Windows.Forms.TextBox();
            this.txtBankID = new System.Windows.Forms.TextBox();
            this.txtBatchSequence = new System.Windows.Forms.TextBox();
            this.txtBatchID = new System.Windows.Forms.TextBox();
            this.txtImmutableDateKey = new System.Windows.Forms.TextBox();
            this.pbFront = new System.Windows.Forms.PictureBox();
            this.btnGetImage = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoDocument = new System.Windows.Forms.RadioButton();
            this.rdoCheck = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.pbBack = new System.Windows.Forms.PictureBox();
            this.txtLogonName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtImportTypeShortName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBatchSourceShortName = new System.Windows.Forms.TextBox();
            this.txtSourceBatchId = new System.Windows.Forms.TextBox();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.txtDepositDateKey = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbFront)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBack)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(63, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "BankID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "BatchSequence:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 123);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "BatchID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "ImmutableDateKey:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(47, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "LockboxID:";
            // 
            // txtLockboxID
            // 
            this.txtLockboxID.Location = new System.Drawing.Point(115, 42);
            this.txtLockboxID.Name = "txtLockboxID";
            this.txtLockboxID.Size = new System.Drawing.Size(88, 20);
            this.txtLockboxID.TabIndex = 3;
            // 
            // txtBankID
            // 
            this.txtBankID.Location = new System.Drawing.Point(115, 19);
            this.txtBankID.Name = "txtBankID";
            this.txtBankID.Size = new System.Drawing.Size(88, 20);
            this.txtBankID.TabIndex = 2;
            // 
            // txtBatchSequence
            // 
            this.txtBatchSequence.Location = new System.Drawing.Point(115, 144);
            this.txtBatchSequence.Name = "txtBatchSequence";
            this.txtBatchSequence.Size = new System.Drawing.Size(88, 20);
            this.txtBatchSequence.TabIndex = 6;
            // 
            // txtBatchID
            // 
            this.txtBatchID.Location = new System.Drawing.Point(115, 120);
            this.txtBatchID.Name = "txtBatchID";
            this.txtBatchID.Size = new System.Drawing.Size(88, 20);
            this.txtBatchID.TabIndex = 5;
            // 
            // txtImmutableDateKey
            // 
            this.txtImmutableDateKey.Location = new System.Drawing.Point(115, 64);
            this.txtImmutableDateKey.Name = "txtImmutableDateKey";
            this.txtImmutableDateKey.Size = new System.Drawing.Size(88, 20);
            this.txtImmutableDateKey.TabIndex = 4;
            // 
            // pbFront
            // 
            this.pbFront.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbFront.Location = new System.Drawing.Point(227, 9);
            this.pbFront.Name = "pbFront";
            this.pbFront.Size = new System.Drawing.Size(527, 225);
            this.pbFront.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFront.TabIndex = 10;
            this.pbFront.TabStop = false;
            // 
            // btnGetImage
            // 
            this.btnGetImage.Location = new System.Drawing.Point(140, 440);
            this.btnGetImage.Name = "btnGetImage";
            this.btnGetImage.Size = new System.Drawing.Size(75, 25);
            this.btnGetImage.TabIndex = 9;
            this.btnGetImage.Text = "Get Image";
            this.btnGetImage.UseVisualStyleBackColor = true;
            this.btnGetImage.Click += new System.EventHandler(this.btnGetImage_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoDocument);
            this.groupBox1.Controls.Add(this.rdoCheck);
            this.groupBox1.Location = new System.Drawing.Point(115, 275);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(88, 59);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // rdoDocument
            // 
            this.rdoDocument.AutoSize = true;
            this.rdoDocument.Location = new System.Drawing.Point(6, 33);
            this.rdoDocument.Name = "rdoDocument";
            this.rdoDocument.Size = new System.Drawing.Size(74, 17);
            this.rdoDocument.TabIndex = 8;
            this.rdoDocument.Text = "Document";
            this.rdoDocument.UseVisualStyleBackColor = true;
            // 
            // rdoCheck
            // 
            this.rdoCheck.AutoSize = true;
            this.rdoCheck.Checked = true;
            this.rdoCheck.Location = new System.Drawing.Point(6, 10);
            this.rdoCheck.Name = "rdoCheck";
            this.rdoCheck.Size = new System.Drawing.Size(56, 17);
            this.rdoCheck.TabIndex = 7;
            this.rdoCheck.TabStop = true;
            this.rdoCheck.Text = "Check";
            this.rdoCheck.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(75, 285);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Type:";
            // 
            // pbBack
            // 
            this.pbBack.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbBack.Location = new System.Drawing.Point(227, 240);
            this.pbBack.Name = "pbBack";
            this.pbBack.Size = new System.Drawing.Size(527, 225);
            this.pbBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbBack.TabIndex = 14;
            this.pbBack.TabStop = false;
            // 
            // txtLogonName
            // 
            this.txtLogonName.Location = new System.Drawing.Point(115, 19);
            this.txtLogonName.Name = "txtLogonName";
            this.txtLogonName.Size = new System.Drawing.Size(88, 20);
            this.txtLogonName.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Logon Name:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(115, 41);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(88, 20);
            this.txtPassword.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(53, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Password:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtLogonName);
            this.groupBox2.Controls.Add(this.txtPassword);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(209, 76);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtDepositDateKey);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtImportTypeShortName);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtBatchSourceShortName);
            this.groupBox3.Controls.Add(this.txtSourceBatchId);
            this.groupBox3.Controls.Add(this.txtBankID);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.groupBox1);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtLockboxID);
            this.groupBox3.Controls.Add(this.txtImmutableDateKey);
            this.groupBox3.Controls.Add(this.txtBatchSequence);
            this.groupBox3.Controls.Add(this.txtBatchID);
            this.groupBox3.Location = new System.Drawing.Point(12, 94);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(209, 340);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 219);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(91, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "ImportTypeName:";
            // 
            // txtImportTypeShortName
            // 
            this.txtImportTypeShortName.Location = new System.Drawing.Point(115, 219);
            this.txtImportTypeShortName.Name = "txtImportTypeShortName";
            this.txtImportTypeShortName.Size = new System.Drawing.Size(88, 20);
            this.txtImportTypeShortName.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "BatchSourceName:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 172);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "SourceBatchID:";
            // 
            // txtBatchSourceShortName
            // 
            this.txtBatchSourceShortName.Location = new System.Drawing.Point(115, 193);
            this.txtBatchSourceShortName.Name = "txtBatchSourceShortName";
            this.txtBatchSourceShortName.Size = new System.Drawing.Size(88, 20);
            this.txtBatchSourceShortName.TabIndex = 17;
            // 
            // txtSourceBatchId
            // 
            this.txtSourceBatchId.Location = new System.Drawing.Point(115, 169);
            this.txtSourceBatchId.Name = "txtSourceBatchId";
            this.txtSourceBatchId.Size = new System.Drawing.Size(88, 20);
            this.txtSourceBatchId.TabIndex = 16;
            // 
            // txtMsg
            // 
            this.txtMsg.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMsg.Location = new System.Drawing.Point(227, 471);
            this.txtMsg.Multiline = true;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.ReadOnly = true;
            this.txtMsg.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMsg.Size = new System.Drawing.Size(527, 73);
            this.txtMsg.TabIndex = 10;
            // 
            // txtDepositDateKey
            // 
            this.txtDepositDateKey.Location = new System.Drawing.Point(115, 90);
            this.txtDepositDateKey.Name = "txtDepositDateKey";
            this.txtDepositDateKey.Size = new System.Drawing.Size(88, 20);
            this.txtDepositDateKey.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 93);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "DepositDateKey:";
            // 
            // frmImageDisplay
            // 
            this.AcceptButton = this.btnGetImage;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 551);
            this.Controls.Add(this.txtMsg);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pbBack);
            this.Controls.Add(this.btnGetImage);
            this.Controls.Add(this.pbFront);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmImageDisplay";
            this.Text = "Long-Term Archive File Services Console";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmImageDisplay_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbFront)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBack)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLockboxID;
        private System.Windows.Forms.TextBox txtBankID;
        private System.Windows.Forms.TextBox txtBatchSequence;
        private System.Windows.Forms.TextBox txtBatchID;
        private System.Windows.Forms.TextBox txtImmutableDateKey;
        private System.Windows.Forms.PictureBox pbFront;
        private System.Windows.Forms.Button btnGetImage;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoCheck;
        private System.Windows.Forms.RadioButton rdoDocument;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pbBack;
        private System.Windows.Forms.TextBox txtLogonName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBatchSourceShortName;
        private System.Windows.Forms.TextBox txtSourceBatchId;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtImportTypeShortName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDepositDateKey;
    }
}