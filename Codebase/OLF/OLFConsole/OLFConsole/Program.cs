﻿using System;
using System.Configuration;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/07/2011 
*   -New file
* CR 47771 WJS 11/23/2011
*   -Add ability pass in SiteID
* CR 117400 JMC 10/16/2013
*   -Updated OLFConsole for 2.00
* WI 169148 SAS 10/01/2014
*   Changes done to change the getImage function signature    
* WI 175563 SAS 10/31/2014
*   Update ImportTypeShortName field to BatchSourceName   
* WI 176361 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName 
******************************************************************************/
namespace WFS.RecHub.OLFConsole {

    class Program {
    
        static void Main(string[] args) {

            string strFilePath = string.Empty;
            int intBankID = -1;
            int intSiteID = -1;
            int intLockboxID = -1;
            int intImmutableDateKey = -1;
            int intDepositDateKey = -1;
            int intBatchID = -1;
            int intBatchSequence = -1;
            long longSourceBatchID = -1;
            string strBatchSourceShortName = string.Empty;
            string strImportTypeShortName = string.Empty;
            bool bolIsCheck = false;
            string strLogonName = string.Empty;
            string strPassword = string.Empty;
            Guid gidSessionID = Guid.Empty;
            bool bolLaunchGUI = false;
            string strSiteKey;

            string strTemp;
            int intTemp;
            long longTemp;
            byte[][] arImageList;
            string strFileDescriptor;
            string strFileExtension;

            cImageClient objClient;

            try {

                if(args.Length > 0) {

                    if(args[0].Trim() == "-?" || args[0].Trim() == "/?") {
                        Usage();
                    } else {
                        foreach (string arg in args)
                        {
                            if (arg.ToLower().StartsWith("-destination="))
                            {
                                strFilePath = arg.Substring(("-destination=").Length);
                            }
                            else if (arg.ToLower().StartsWith("-bankid="))
                            {
                                strTemp = arg.Substring(("-bankid=").Length);
                                if (int.TryParse(strTemp, out intTemp))
                                {
                                    intBankID = intTemp;
                                }
                            }
                            else if (arg.ToLower().StartsWith("-lockboxid="))
                            {
                                strTemp = arg.Substring(("-lockboxid=").Length);
                                if (int.TryParse(strTemp, out intTemp))
                                {
                                    intLockboxID = intTemp;
                                }
                            }
                            else if (arg.ToLower().StartsWith("-immutabledatekey="))
                            {
                                strTemp = arg.Substring(("-immutabledatekey=").Length);
                                if (int.TryParse(strTemp, out intTemp))
                                {
                                    intImmutableDateKey = intTemp;
                                }
                            }
                            else if (arg.ToLower().StartsWith("-depositdatekey="))
                            {
                                strTemp = arg.Substring(("-depositdatekey=").Length);
                                if (int.TryParse(strTemp, out intTemp))
                                {
                                    intDepositDateKey = intTemp;
                                }
                            }
                            else if (arg.ToLower().StartsWith("-batchid="))
                            {
                                strTemp = arg.Substring(("-batchid=").Length);
                                if (int.TryParse(strTemp, out intTemp))
                                {
                                    intBatchID = intTemp;
                                }
                            }
                            else if (arg.ToLower().StartsWith("-batchsequence="))
                            {
                                strTemp = arg.Substring(("-batchsequence=").Length);
                                if (int.TryParse(strTemp, out intTemp))
                                {
                                    intBatchSequence = intTemp;
                                }
                            }
                            else if (arg.ToLower().StartsWith("-ischeck="))
                            {
                                strTemp = arg.Substring(("-ischeck=").Length);
                                bolIsCheck = (strTemp.Trim() == "1");
                            }
                            else if (arg.ToLower().StartsWith("-logonname="))
                            {
                                strLogonName = arg.Substring(("-logonname=").Length);
                            }
                            else if (arg.ToLower().StartsWith("-password="))
                            {
                                strPassword = arg.Substring(("-password=").Length);
                            }
                            else if (arg.ToLower().StartsWith("-gui"))
                            {
                                bolLaunchGUI = true;
                            }
                            else if (arg.ToLower().StartsWith("-siteid="))
                            {
                                strTemp = arg.Substring(("-siteid=").Length);
                                if (int.TryParse(strTemp, out intTemp))
                                {
                                    intSiteID = intTemp;
                                }
                            }
                            else if (arg.ToLower().StartsWith("-sourcebatchid="))
                            {
                                strTemp = arg.Substring(("-sourcebatchid=").Length);
                                if (long.TryParse(strTemp, out  longTemp))
                                {
                                    longSourceBatchID = longTemp;
                                }
                            }
                            else if (arg.ToLower().StartsWith("-batchsourceshortname="))
                            {
                                strBatchSourceShortName = arg.Substring(("-batchsourceshortname=").Length);
                            }
                            else if (arg.ToLower().StartsWith("-importtypeshortname="))
                            {
                                strImportTypeShortName = arg.Substring(("-importtypeshortname=").Length);
                            }
                        }

                        strSiteKey=ConfigurationManager.AppSettings["siteKey"];

                        if(bolLaunchGUI) {
                            LaunchGUI(strSiteKey,
                                      intBankID,
                                      intLockboxID,
                                      intImmutableDateKey,
                                      intDepositDateKey,
                                      intBatchID,
                                      intBatchSequence,                                      
                                      longSourceBatchID,
                                      strBatchSourceShortName,
                                      strImportTypeShortName,
                                      bolIsCheck,                                      
                                      intSiteID,
                                      strLogonName,
                                      strPassword);
                        } else {
                            if(intBankID > -1 && 
                               intLockboxID > -1 && 
                               intImmutableDateKey > -1 && 
                               intBatchID > -1 && 
                               intBatchSequence > -1 && 
                               strLogonName.Trim().Length > 0 && 
                               strPassword.Trim().Length > 0) {

                                objClient = new cImageClient(strSiteKey);

                                objClient.GetSession(strLogonName, strPassword);

                                if(strSiteKey != null && strSiteKey.Length > 0) {

                                    if(objClient.GetImage(intBankID,
                                                          intLockboxID,
                                                          intImmutableDateKey,
                                                          intDepositDateKey,
                                                          intBatchID,
                                                          intBatchSequence,
                                                          bolIsCheck,
                                                          intSiteID,
                                                          longSourceBatchID,
                                                          strBatchSourceShortName,
                                                          strImportTypeShortName,
                                                          out arImageList,
                                                          out strFileDescriptor, 
                                                          out strFileExtension)) {

                                        System.IO.FileStream fs = System.IO.File.Create(strFilePath);

                                        fs.Write(arImageList[0], 0, arImageList[0].Length);
                                        fs.Flush();

                                        fs.Dispose();

                                        Console.WriteLine("01 - Image was written successfully.");
                                    } else {
                                        Console.WriteLine("00 - Image request failed.");
                                    }

                                } else {
                                    InvalidSiteKey();
                                }

                               objClient.EndSession();

                            } else {
                                IncorrectSyntax();
                                Usage();
                            }
                        }
                    }
                } else {
                    IncorrectSyntax();
                    Usage();
                }
            } catch(Exception ex) {
                Console.WriteLine("Exception occurred: " + ex.Message);
            }
        }

        private static void LaunchGUI(string SiteKey,
                                      int BankID,
                                      int LockboxID,
                                      int ImmutableDateKey,
                                      int DepositDateKey,
                                      int BatchID,
                                      int BatchSequence,
                                      long SourceBatchID,
                                      string BatchSourceShortName,
                                      string ImportTypeShortName,
                                      bool IsCheck,
                                      int SiteID,
                                      string LogonName,
                                      string Password) {

            frmImageDisplay objForm = new frmImageDisplay(SiteKey,
                                                          BankID,
                                                          LockboxID,
                                                          ImmutableDateKey,
                                                          DepositDateKey,
                                                          BatchID,
                                                          BatchSequence,
                                                          SourceBatchID,
                                                          BatchSourceShortName,
                                                          ImportTypeShortName,
                                                          IsCheck,
                                                          SiteID,
                                                          LogonName,
                                                          Password);

            objForm.ShowDialog();
            objForm.Dispose();
        }

        private static void InvalidSiteKey() {
            Console.WriteLine("SiteKey must be defined in the local .config file as follows:");
            Console.WriteLine("");
            Console.WriteLine("<configuration>");
            Console.WriteLine("	<appSettings>");
            Console.WriteLine("		<add key=\"localIni\" value=\"D:\\IPOnline\\bin2\\IPOnline.ini\"/>");
            Console.WriteLine("		<add key=\"siteKey\" value=\"IPOnline\"/>");
            Console.WriteLine("	</appSettings>");
            Console.WriteLine("</configuration>");      
        }

        private static void IncorrectSyntax() {
            Console.WriteLine("The syntax of the command is incorrect.");
            Console.WriteLine("");
        }

        private static void Usage() {
            Console.WriteLine("To retrieve a file from OLFServices:");
            Console.WriteLine("");
            Console.WriteLine(" OLFClientUtil");
            Console.WriteLine("         -destination=<Destination>");
            Console.WriteLine("         -bankid=<BankID>");
            Console.WriteLine("         -lockboxid=<LockboxID>");
            Console.WriteLine("         -immutabledatekey=<ImmutableDateKey>");
            Console.WriteLine("         -depositdatekey=<DepositDateKey>");
            Console.WriteLine("         -batchid=<BatchID>");
            Console.WriteLine("         -batchsequence=<BatchSequence>");
            Console.WriteLine("         -sourcebatchid=<SourceBatchID>");
            Console.WriteLine("         -batchsourceshortname=<batchsourceshortname>");
            Console.WriteLine("         -importtypeshortname=<importtypeshortname>");
            Console.WriteLine("         -ischeck=<IsCheck>");
            Console.WriteLine("         -logonname=<LogonName>");
            Console.WriteLine("         -password=<Password>");
            Console.WriteLine("         [-GUI]");
            Console.WriteLine("");
            Console.WriteLine("  <Destination>          UNC path and file name to store the retrieved image.");
            Console.WriteLine("  <BankID>               BankID associated to the image being retrieved.");
            Console.WriteLine("  <LockboxID>            LockboxID associated to the image being retrieved.");
            Console.WriteLine("  <ImmutableDateKey>     Processing Date (in YYYYMMDD format) of the image");
            Console.WriteLine("                         being retrieved.");
            Console.WriteLine("  <DepositDateKey>       Deposit Date (in YYYYMMDD format) of the image");
            Console.WriteLine("                         being retrieved.");
            Console.WriteLine("  <BatchID>              BatchID associated to the image being retrieved.");
            Console.WriteLine("  <BatchSequence>        BatchSequence of the image being retrieve.");
            Console.WriteLine("  <SourceBatchID>        SourceBatchID of the image being retrieve.");
            Console.WriteLine("  <BatchSourceShortName> BatchSourceShortName of the image being retrieve.");
            Console.WriteLine("  <ImportTypeShortName>  ImportTypeShortName of the image being retrieve.");
            Console.WriteLine("  <IsCheck>              0=image being retrieved is not a Check (Default)");
            Console.WriteLine("                         1=image being retrieved is a Check");
            Console.WriteLine("  <LogonName>            Service user logon name");
            Console.WriteLine("  <Password>             Service user password");
            Console.WriteLine("  <GUI>                  0=Do not launch GUI");
            Console.WriteLine("                         1=Launch GUI.  Disregard <Destination> parm.");
        }
    }
}
