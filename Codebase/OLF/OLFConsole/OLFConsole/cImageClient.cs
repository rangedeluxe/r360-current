﻿using System;

using WFS.RecHub.Common;
using WFS.RecHub.OLFServicesClient;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     03/22/2009
*
* Purpose:  IPOnline common enumeration class.
*
* Modification History
* CR 32562 JMC 03/22/2011 
*   -New file.
* CR 45986 WJS 11/23/2011
*   -Add ability pass in SiteID
* CR 117400 JMC 10/16/2013
*   -Updated OLFConsole for 2.00
*  WI 169148 SAS 10/01/2014
*   Changes done to change the getImage function signature    
*  WI 175563 SAS 10/31/2014
*   Update ImportTypeShortName field to BatchSourceName 
*  WI 176361 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName 
******************************************************************************/
namespace WFS.RecHub.OLFConsole {

    internal class cImageClient {

        private string _SiteKey = string.Empty;
        private Guid _SessionID = Guid.Empty;
        private OLFServiceClient _ServiceClient = null;

        public event outputMessageEventHandler outputMessage;

        public cImageClient(string SiteKeyValue) {
            _SiteKey = SiteKeyValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        private void onOutputMessage(string message,
                                     string src,
                                     MessageType messageType,
                                     MessageImportance messageImportance) {

            if (outputMessage != null) {
                outputMessage(message,
                              src,
                              messageType,
                              messageImportance);
            }
        }

        public Guid SessionID {
            get {
                return(_SessionID);
            }
        }

        private OLFServiceClient ServiceClient {
            get {
                if(_ServiceClient == null) {
                    _ServiceClient = new OLFServiceClient(_SiteKey);
                }
                return(_ServiceClient);
            }
        }

        public void GetSession(string LogonName, string Password) {

            Guid gidSessionID;

            try {
                if(ServiceClient.GetSession(LogonName, Password, out gidSessionID)) {
                    _SessionID = gidSessionID;
                } else {
                    _SessionID = Guid.Empty;
                    throw(new Exception("Unable to validate User credentials."));
                }
            } catch(InvalidSiteKeyException) {
                throw;
            } catch(Exception) {
                throw;
            }
        }

        public void EndSession() {

            try {
                if(_SessionID != Guid.Empty) {
                    ServiceClient.SetSession(_SessionID);
                    ServiceClient.EndSession(_SessionID);
                }
            } catch(InvalidSiteKeyException) {
                throw;
            } catch(InvalidSessionIDException) {
                throw;
            } catch(Exception ex) {
                throw;
            }
        }

        public bool GetImage(int BankID, 
                             int LockboxID, 
                             int ImmutableDateKey, 
                             int DepositDateKey,
                             int BatchID, 
                             int BatchSequence, 
                             bool IsCheck,
                             int  SiteID,
                             long SourceBatchID,
                             string BatchSourceShortName,
                             string ImportTypeShortName,
                             out byte[][] ImageList,
                             out string FileDescriptor, 
                             out string FileExtension) {

            bool bolRetVal;

            byte[][] arImageList;
            string strFileDescriptor;
            string strFileExtension;

            try {

                ServiceClient.SetSession(SessionID);

                bolRetVal = ServiceClient.GetImage(BankID,
                                                   LockboxID,
                                                   ImmutableDateKey,
                                                   DepositDateKey,
                                                   BatchID,
                                                   BatchSequence,
                                                   IsCheck,
                                                   SiteID, 
                                                   SourceBatchID,
                                                   BatchSourceShortName,
                                                   ImportTypeShortName,
                                                   out arImageList,
                                                   out strFileDescriptor, 
                                                   out strFileExtension);

                if(bolRetVal) {
                    ImageList = arImageList;
                    FileDescriptor = strFileDescriptor;
                    FileExtension = strFileExtension;
                } else {
                    ImageList = null;
                    FileDescriptor = string.Empty;
                    FileExtension = string.Empty;
                    bolRetVal = false;
                }

            } catch(InvalidSiteKeyException) {
                throw;
            } catch(InvalidSessionIDException) {
                throw;
            } catch(SessionExpiredException ex) {
                onOutputMessage("Session has expired.  Application will reconnect on next request.", 
                                this.GetType().Name, 
                                MessageType.Information, 
                                MessageImportance.Essential);
                _SessionID = Guid.Empty;

                throw;
            } catch(InvalidSessionException) {
                throw;
            } catch(ImageNotFoundInDbException) {
                throw;
            } catch(ImageNotFoundOnDiskException) {
                throw;
            } catch(Exception) {
                throw;
            }

            return(bolRetVal);
        }
    }
}
