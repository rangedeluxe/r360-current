﻿using System;

using WFS.RecHub.Common;
using WFS.RecHub.OLFServicesExternalClient;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     
*
* Purpose:  IPOnline common enumeration class.
*
* Modification History
* WI 197638 TWE 03/24/2015
*    modify to allow legacy version of imageRPS to call old contract
******************************************************************************/

namespace WFS.RecHub.OLFConsoleExternal
{
    internal class cImageClient
    {

        private string _SiteKey = string.Empty;
        private Guid _SessionID = Guid.Empty;
        private OLFServiceExternalClient _ServiceClient = null;

        public event outputMessageEventHandler outputMessage;

        public cImageClient(string SiteKeyValue)
        {
            _SiteKey = SiteKeyValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        private void onOutputMessage(string message,
                                     string src,
                                     MessageType messageType,
                                     MessageImportance messageImportance)
        {

            if (outputMessage != null)
            {
                outputMessage(message,
                              src,
                              messageType,
                              messageImportance);
            }
        }

        public Guid SessionID
        {
            get
            {
                return (_SessionID);
            }
        }

        private OLFServiceExternalClient ServiceClient
        {
            get
            {
                if (_ServiceClient == null)
                {
                    _ServiceClient = new OLFServiceExternalClient();
                }
                return (_ServiceClient);
            }
        }

        public void GetSession(string LogonName, string Password)
        {

            Guid gidSessionID;

            try
            {

                if (ServiceClient.GetSession(LogonName, Password, out gidSessionID))
                {
                    _SessionID = gidSessionID;
                }
                else
                {
                    _SessionID = Guid.Empty;
                    throw (new Exception("Unable to validate User credentials."));
                }
            }
            catch (InvalidSiteKeyException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void EndSession()
        {

            try
            {
                if (_SessionID != Guid.Empty)
                {
                    //ServiceClient.SetSession(_SessionID);
                    ServiceClient.EndSession(_SessionID);
                }
            }
            catch (InvalidSiteKeyException)
            {
                throw;
            }
            catch (InvalidSessionIDException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool GetImage( 
                             string LogonName, 
                             string Password,
                             int BankID,
                             int LockboxID,
                             int ImmutableDateKey,
                             int DepositDateKey,
                             int BatchID,
                             int BatchSequence,
                             bool IsCheck,
                             string paymentSource,
                             out byte[][] ImageList,
                             out string FileDescriptor,
                             out string FileExtension,
                             out Guid sessionID)
        {

            bool bolRetVal;

            byte[][] arImageList;
            string strFileDescriptor;
            string strFileExtension;

            try
            {

                bolRetVal = ServiceClient.GetImage(LogonName,
                                                   Password,
                                                   BankID,
                                                   LockboxID,
                                                   ImmutableDateKey,
                                                   DepositDateKey,
                                                   BatchID,
                                                   BatchSequence,
                                                   IsCheck,
                                                   paymentSource,
                                                   out arImageList,
                                                   out strFileDescriptor,
                                                   out strFileExtension,
                                                   out sessionID);

                if (bolRetVal)
                {
                    ImageList = arImageList;
                    FileDescriptor = strFileDescriptor;
                    FileExtension = strFileExtension;
                }
                else
                {
                    ImageList = null;
                    FileDescriptor = string.Empty;
                    FileExtension = string.Empty;
                    bolRetVal = false;
                }

            }
            catch (InvalidSiteKeyException)
            {
                throw;
            }
            catch (InvalidSessionIDException)
            {
                throw;
            }
            catch (SessionExpiredException ex)
            {
                onOutputMessage("Session has expired.  Application will reconnect on next request.",
                                this.GetType().Name,
                                MessageType.Information,
                                MessageImportance.Essential);
                _SessionID = Guid.Empty;

                throw;
            }
            catch (InvalidSessionException)
            {
                throw;
            }
            catch (ImageNotFoundInDbException)
            {
                throw;
            }
            catch (ImageNotFoundOnDiskException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return (bolRetVal);
        }
    }
}
