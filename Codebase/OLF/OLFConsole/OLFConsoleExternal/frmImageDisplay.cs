﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 197638 TWE 03/24/2015
*    modify to allow legacy version of imageRPS to call old contract
******************************************************************************/
namespace WFS.RecHub.OLFConsoleExternal
{

    public partial class frmImageDisplay : Form
    {

        private string _SiteKey = string.Empty;
        private cImageClient _ImageClient = null;

        public frmImageDisplay(string SiteKey,                               
                               int BankID,
                               int LockboxID,
                               int ImmutableDateKey,
                               int DepositDateKey,
                               int BatchID,
                               int BatchSequence,
                               bool IsCheck,
                               string paymentSource,
                               string LogonName,
                               string Password)
        {

            InitializeComponent();

            try
            {

                _SiteKey = SiteKey;

                if (BankID >= 0)
                {
                    this.txtBankID.Text = BankID.ToString();
                }

                if (LockboxID >= 0)
                {
                    this.txtLockboxID.Text = LockboxID.ToString();
                }

                if (ImmutableDateKey >= 0)
                {
                    this.txtImmutableDateKey.Text = ImmutableDateKey.ToString();
                }

                if (DepositDateKey >= 0)
                {
                    this.txtDepositDateKey.Text = DepositDateKey.ToString();
                }

                if (BatchID >= 0)
                {
                    this.txtBatchID.Text = BatchID.ToString();
                }

                if (BatchSequence >= 0)
                {
                    this.txtBatchSequence.Text = BatchSequence.ToString();
                }
               
                this.rdoCheck.Checked = IsCheck;
                this.rdoDocument.Checked = !IsCheck;

                this.txtLogonName.Text = LogonName.Trim();
                this.txtPassword.Text = Password.Trim();
                this.txtPaymentSource.Text = paymentSource.Trim();

                if (ImageClient.SessionID == Guid.Empty && 
                    LogonName.Trim().Length > 0 && 
                    Password.Trim().Length > 0)
                {
                    ImageClient.GetSession(LogonName.Trim(), Password.Trim());
                }

                if (ImageClient.SessionID != Guid.Empty)
                {
                    if (BankID >= 0 && 
                        LockboxID >= 0 && 
                        ImmutableDateKey >= 0 &&
                        DepositDateKey >= 0 && 
                        BatchID >= 0 && 
                        BatchSequence >= 0 )
                    {
                        GetImage2(LogonName.Trim(), Password.Trim(), 
                            BankID, LockboxID, 
                            ImmutableDateKey, DepositDateKey, 
                            BatchID, BatchSequence, IsCheck, 
                            paymentSource, ImageClient.SessionID);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occurred: " + ex.Message);
                OnOutputMessage("Exception occurred: " + ex.Message, MessageType.Error);
            }
        }

        private cImageClient ImageClient
        {
            get
            {
                if (_ImageClient == null)
                {
                    _ImageClient = new cImageClient(_SiteKey);
                    _ImageClient.outputMessage += new outputMessageEventHandler(Object_OutputMessage);
                }
                return (_ImageClient);
            }
        }

        /// <summary>
        /// Event handler used to assign delegates from components
        /// that output messages.
        /// </summary>
        /// <param name="message">Test message to be logged.</param>
        /// <param name="src">Source of the event.</param>
        /// <param name="messageType">MessageType(Information, Warning, Error)</param>
        /// <param name="messageImportance">MessageImportance(Essential, Verbose, Debug)
        /// </param>
        private void Object_OutputMessage(string message,
                                          string src,
                                          MessageType messageType,
                                          MessageImportance messageImportance)
        {

            OnOutputMessage(message, messageType);
        }

        private void OnOutputMessage(string message, MessageType messageType)
        {

            string strMsg;

            switch (messageType)
            {
                case MessageType.Error:
                    strMsg = DateTime.Now.ToString("hhmmss") + " Error:   " + ipoLib.CRLF + message;
                    break;
                case MessageType.Warning:
                    strMsg = DateTime.Now.ToString("hhmmss") + " Warning: " + ipoLib.CRLF + message;
                    break;
                default:
                    strMsg = DateTime.Now.ToString("hhmmss") + " Info:   " + ipoLib.CRLF + message;
                    break;
            }

            if (this.txtMsg.Text.Length > 0)
            {
                this.txtMsg.AppendText(ipoLib.CRLF + strMsg);
            }
            else
            {
                this.txtMsg.AppendText(strMsg);
            }

            Console.WriteLine(strMsg);
        }

        private void btnGetImage_Click(object sender, EventArgs e)
        {

            try
            {

                GetImage();
                this.ActiveControl = this.txtBatchSequence;
                this.txtBatchSequence.SelectAll();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception occurred: " + ex.Message);
                OnOutputMessage("Exception occurred: " + ex.Message, MessageType.Error);
            }
        }

        private void GetImage()
        {
            string logonName = string.Empty;
            string password = string.Empty;
            int intBankID = -1;
            int intLockboxID = -1;
            int intImmutableDateKey = -1;
            int intDepositDateKey = -1;
            int intBatchID = -1;
            int intBatchSequence = -1;
            
            string paymentSource = string.Empty;
            Guid sessionID = Guid.Empty;

            int intTemp;
            long longTemp;
            string strMsg = string.Empty;

            if (int.TryParse(this.txtBankID.Text, out intTemp))
            {
                intBankID = intTemp;
            }
            else
            {
                strMsg = "BankID must be numeric.\r\n";
            }

            if (int.TryParse(this.txtLockboxID.Text, out intTemp))
            {
                intLockboxID = intTemp;
            }
            else
            {
                strMsg += "LockboxID must be numeric.\r\n";
            }

            if (int.TryParse(this.txtImmutableDateKey.Text, out intTemp))
            {
                intImmutableDateKey = intTemp;
            }
            else
            {
                strMsg += "ImmutableDateKey must be numeric.\r\n";
            }

            if (int.TryParse(this.txtDepositDateKey.Text, out intTemp))
            {
                intDepositDateKey = intTemp;
            }
            else
            {
                strMsg += "DepositDateKey must be numeric.\r\n";
            }

            if (int.TryParse(this.txtBatchID.Text, out intTemp))
            {
                intBatchID = intTemp;
            }
            else
            {
                strMsg += "BatchID must be numeric.\r\n";
            }

            if (int.TryParse(this.txtBatchSequence.Text, out intTemp))
            {
                intBatchSequence = intTemp;
            }
            else
            {
                strMsg += "BatchSequence must be numeric.\r\n";
            }
                       
            if (this.txtLogonName.Text.Trim().Length > 0)
            {
                logonName = this.txtLogonName.Text.Trim();
            }
            else
            {
                strMsg += "Logon Name must be entered.\r\n";
            }

            if (this.txtPassword.Text.Trim().Length > 0)
            {
                password = this.txtPassword.Text.Trim();
            }
            else
            {
                strMsg += "Password must be entered.\r\n";
            }

            if (this.txtPaymentSource.Text.Trim().Length > 0)
            {
                paymentSource = this.txtPaymentSource.Text.Trim();
            }
            else
            {
                strMsg += "Payment source must be entered.\r\n";
            }
            
            if (ImageClient.SessionID == Guid.Empty)
            {
                if (this.txtLogonName.Text.Trim().Length > 0 && this.txtPassword.Text.Trim().Length > 0)
                {
                    ImageClient.GetSession(this.txtLogonName.Text.Trim(), this.txtPassword.Text.Trim());
                    if (ImageClient.SessionID == Guid.Empty)
                    {
                        strMsg += "Could not establish a User Session.\r\n";
                    }
                }
                else
                {
                    strMsg += "UserID/Password not defined.\r\n";
                }
            }

            if (strMsg.Length == 0)
            {
                GetImage2(logonName, password, 
                    intBankID, intLockboxID, 
                    intImmutableDateKey, intDepositDateKey,
                    intBatchID, intBatchSequence, 
                    rdoCheck.Checked, paymentSource, sessionID);
            }
            else
            {
                MessageBox.Show(strMsg);
                OnOutputMessage(strMsg, MessageType.Information);
            }

        }

        private bool GetImage2(
                              string logonName, 
                              string password, 
                              int BankID,
                              int LockboxID,
                              int ImmutableDateKey,
                              int DepositDateKey,
                              int BatchID,
                              int BatchSequence,
                              bool IsCheck,
                              string paymentSource,
                              Guid sessionID)
        {

            byte[][] arImageList;
            string strFileDescriptor;
            string strFileExtension;
            MemoryStream ms;
            Image objImage;
            string strMsg;

            bool bolRetVal;

            this.pbFront.Image = null;
            this.pbBack.Image = null;

            try
            {
                

                if (ImageClient.GetImage(
                                        logonName,
                                        password, 
                                        BankID,
                                        LockboxID,
                                        ImmutableDateKey,
                                        DepositDateKey,
                                        BatchID,
                                        BatchSequence,
                                        IsCheck,
                                        paymentSource,
                                        out arImageList,
                                        out strFileDescriptor,
                                        out strFileExtension,
                                        out sessionID))
                {

                    if (arImageList != null)
                    {
                        if (arImageList.Length > 0 && arImageList[0].Length > 0)
                        {
                            ms = new MemoryStream(arImageList[0]);
                            objImage = Image.FromStream(ms);
                            this.pbFront.Image = objImage;
                        }

                        if (arImageList.Length > 1 && arImageList[1].Length > 0)
                        {
                            ms = new MemoryStream(arImageList[1]);
                            objImage = Image.FromStream(ms);
                            this.pbBack.Image = objImage;
                        }
                    }

                    strMsg = string.Empty;
                    strMsg += "SessionID: " + sessionID.ToString() + ipoLib.CRLF;
                    strMsg += "File Descriptor: " + strFileDescriptor + ipoLib.CRLF;
                    strMsg += "File Extension: " + strFileExtension;

                    OnOutputMessage(strMsg, MessageType.Information);

                    bolRetVal = true;

                }
                else
                {
                    MessageBox.Show("Image retrieval failed.");
                    OnOutputMessage("Image retrieval failed.", MessageType.Information);
                    bolRetVal = false;
                }
            }
            catch (InvalidSessionException ex)
            {
                OnOutputMessage(ex.Message, MessageType.Warning);
                bolRetVal = false;
            }
            catch (ImageNotFoundInDbException ex)
            {
                OnOutputMessage(ex.Message, MessageType.Warning);
                bolRetVal = false;
            }
            catch (ImageNotFoundOnDiskException ex)
            {
                OnOutputMessage(ex.Message, MessageType.Warning);
                bolRetVal = false;
            }
            catch (Exception ex)
            {
                OnOutputMessage(ex.Message, MessageType.Error);
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        private void frmImageDisplay_FormClosing(object sender, FormClosingEventArgs e)
        {
            ImageClient.EndSession();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.K))
            {

                this.txtLogonName.Text = "olf";
                this.txtPassword.Text = "uTz4VF$5";
                this.txtBankID.Text = "1";
                this.txtLockboxID.Text = "4533";
                this.txtImmutableDateKey.Text = "20060228";
                this.txtDepositDateKey.Text = "20060228";
                this.txtBatchID.Text = "681";
                this.txtBatchSequence.Text = "1";
                this.rdoCheck.Checked = true;                

                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}

