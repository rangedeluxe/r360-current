﻿using System;
using System.Collections;
using System.Collections.Specialized;
using WFS.RecHub.Common;
using WFS.RecHub.FilePurge.FilePurgeAPI;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     05/23/2013
*
* Purpose:  Image Purge Service.
*
* Modification History
* WI 96955 CRG 06/06/2013
*   Img Purge Svc Unit testing
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeConsole {
	class Program {
		/// <summary>
		/// Private collection of Purge objects.  One object exists in the 
		/// collection for every service section in the .ini file.
		/// </summary>
		private static System.Collections.SortedList _Purges;

		/// <summary>
		/// Mains the specified args.
		/// </summary>
		/// <param name="args">The args.</param>
		static void Main(string[] args) {

			// to demonstrate where the console output is going
			int argCount = args == null ? 0 : args.Length;
			if(argCount > 0) {
				if(args[0] == "/?") {
					Console.WriteLine("");
					Console.WriteLine("WFS R360 File Purge Console will manually execute the File Purge Process.");
					Console.WriteLine("");
				} else {
					Console.WriteLine("");
					Console.WriteLine("You specified {0} arguments:", argCount);
					for(int i = 0; i < argCount; i++) {
						Console.WriteLine("  {0}", args[i]);
					}

					Console.WriteLine("");
					Console.WriteLine("This application does not require any arguments");
					Console.WriteLine("");
				}
			} else {
				string strServiceName;
				string strServiceOptionsKey;
				if(_Purges == null) {
					_Purges = new SortedList();
					StringCollection colSiteOptions = ipoINILib.GetINISection("PurgeSections");
					System.Collections.IEnumerator myEnumerator = ((IEnumerable) colSiteOptions).GetEnumerator();
					while(myEnumerator.MoveNext()) {
						strServiceOptionsKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
						strServiceName = myEnumerator.Current.ToString().Substring(strServiceOptionsKey.Length + 1);
						cPurgeProcess objPurge = new cPurgeProcess(strServiceName, strServiceOptionsKey);
						_Purges.Add(strServiceOptionsKey, objPurge);
					}
				}

				if(_Purges.Count > 0) {
					foreach(cPurgeProcess objTemp in _Purges.Values) {
						objTemp.start();
					}
				}
			}
		}
	}
}
