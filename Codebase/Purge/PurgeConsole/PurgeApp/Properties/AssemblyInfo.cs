﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Rec Hub Image Purge Service Console")]
[assembly: AssemblyDescription("Purge image files to save disk space")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Wausau Financial Systems")]
[assembly: AssemblyProduct("Receivables Hub Long-Term Archive")]
[assembly: AssemblyCopyright("Copyright © Wausau Financial Systems 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]		

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("07d9f791-6411-4f45-a716-9aaf5eae2439")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
