﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.FilePurge.FolderProviders;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  Just some standard tests for the Notification Folder
*           Parsing classes.
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.Tests.FilePurgeAPI
{
    [TestClass]
    public class NotificationParsingTests
    {

        [TestMethod]
        public void TestValidNotificationFolder()
        {
            // Arrange
            var parser = new NotificationFolderParser();

            // Act
            var folder = parser.ParseFolder(@"20060228");

            // Assert
            Assert.AreEqual("20060228", folder.ImmutableDateString);
            Assert.AreEqual(null, folder.BankIDString);
            Assert.AreEqual(null, folder.ClientAccountIDString);
            Assert.AreEqual(null, folder.PaymentSource);
        }

        [TestMethod]
        public void TestValidNotificationEndedFolder()
        {
            // Arrange
            var parser = new NotificationFolderParser();

            // Act
            var folder = parser.ParseFolder(@"\20060228\");

            // Assert
            Assert.AreEqual("20060228", folder.ImmutableDateString);
            Assert.AreEqual(null, folder.BankIDString);
            Assert.AreEqual(null, folder.ClientAccountIDString);
            Assert.AreEqual(null, folder.PaymentSource);
        }
    }
}
