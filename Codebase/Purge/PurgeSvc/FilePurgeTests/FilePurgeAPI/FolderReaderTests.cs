﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.FilePurge.Utilities;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  Just some standard tests for the Reverse Folder Reader.
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.Tests.FilePurgeAPI
{
    [TestClass]
    public class FolderReaderTests
    {
        private ReverseFolderReader _reader;

        public FolderReaderTests() { }

        [TestMethod]
        public void TestValidImagePath()
        {
            // Arrange
            _reader = new ReverseFolderReader(@"c:\data\images\20060228\1\4035");

            // Act & Assert
            Assert.IsTrue(_reader.HasParentFolder);
            Assert.AreEqual("4035", _reader.ReadParentFolder());
            Assert.AreEqual("1", _reader.ReadParentFolder());
            Assert.AreEqual("20060228", _reader.ReadParentFolder());
            Assert.AreEqual("images", _reader.ReadParentFolder());
            Assert.IsTrue(_reader.HasParentFolder);
            Assert.AreEqual("data", _reader.ReadParentFolder());
            Assert.IsFalse(_reader.HasParentFolder);
            Assert.AreEqual(null, _reader.ReadParentFolder());
            Assert.AreEqual(null, _reader.ReadParentFolder());
        }

        [TestMethod]
        public void TestEndedImagePath()
        {
            // Arrange
            _reader = new ReverseFolderReader(@"c:\data\images\20060228\1\4035\");

            // Act & Assert
            Assert.IsTrue(_reader.HasParentFolder);
            Assert.AreEqual("4035", _reader.ReadParentFolder());
            Assert.AreEqual("1", _reader.ReadParentFolder());
            Assert.AreEqual("20060228", _reader.ReadParentFolder());
            Assert.AreEqual("images", _reader.ReadParentFolder());
            Assert.IsTrue(_reader.HasParentFolder);
            Assert.AreEqual("data", _reader.ReadParentFolder());
            Assert.IsFalse(_reader.HasParentFolder);
            Assert.AreEqual(null, _reader.ReadParentFolder());
            Assert.AreEqual(null, _reader.ReadParentFolder());
        }

        [TestMethod]
        public void TestRelativeImagePath()
        {
            // Arrange
            _reader = new ReverseFolderReader(@"20060228\1\4035\");

            // Act & Assert
            Assert.IsTrue(_reader.HasParentFolder);
            Assert.AreEqual("4035", _reader.ReadParentFolder());
            Assert.AreEqual("1", _reader.ReadParentFolder());
            Assert.AreEqual("20060228", _reader.ReadParentFolder());
            Assert.IsFalse(_reader.HasParentFolder);
            Assert.AreEqual(null, _reader.ReadParentFolder());
            Assert.AreEqual(null, _reader.ReadParentFolder());
        }
    }
}
