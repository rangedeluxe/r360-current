﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using WFS.RecHub.FilePurge.FolderProviders;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  Just some standard tests for the PICS Folder Provider.
*           Currently this is commented out, as the providers are dependent
*           on the file system, which makes mocking not an option.
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.Tests.FilePurgeAPI
{
    //[TestClass]
    public class PICSFolderProviderTests
    {
        public PICSFolderProviderTests()
        {
        }


        [TestMethod]
        public void TestImageDirectory()
        { 
            // Arrange
            var provider = new PICSFolderProvider();

            // Act
            var folders = provider.GetPurgeFolders(@"c:\data\images");

            // Assert
            Assert.IsTrue(folders.Any());
        }
    }
}
