using System.ComponentModel;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     06/06/2013
*
* Purpose:  Rec Hub Purge Service.
*
* Modification History
* WI 96951 CRG 06/06/2013
*   Img Purge Svc Code: Filter directories to walk based on saved XML
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeSvc {

	/// <summary>
	/// Summary description for ProjectInstaller.
	/// </summary>
	[RunInstaller(true)]
	public class ProjectInstaller : System.Configuration.Install.Installer {

		/// <summary>
		/// The ipo purge service process installer
		/// </summary>
		private System.ServiceProcess.ServiceProcessInstaller ipoPurgeServiceProcessInstaller;

		/// <summary>
		/// The ipo purge service installer
		/// </summary>
		private System.ServiceProcess.ServiceInstaller ipoPurgeServiceInstaller;

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="ProjectInstaller"/> class.
		/// </summary>
		public ProjectInstaller() {
			// This call is required by the Designer.
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing) {
			if(disposing) {
				if(components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.ipoPurgeServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
			this.ipoPurgeServiceInstaller = new System.ServiceProcess.ServiceInstaller();
			// 
			// ipoPurgeServiceProcessInstaller
			// 
			this.ipoPurgeServiceProcessInstaller.Password = null;
			this.ipoPurgeServiceProcessInstaller.Username = null;
			// 
			// ipoPurgeServiceInstaller
			// 
			this.ipoPurgeServiceInstaller.DisplayName = "WFS Image Purge Service";
			this.ipoPurgeServiceInstaller.ServiceName = "WFS Image Purge Service";
			// 
			// ProjectInstaller
			// 
			this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ipoPurgeServiceProcessInstaller,
            this.ipoPurgeServiceInstaller});

		}
		#endregion
	}
}
