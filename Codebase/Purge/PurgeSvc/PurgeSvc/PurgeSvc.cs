using System.Collections;
using System.Collections.Specialized;
using WFS.RecHub.Common;
using WFS.RecHub.FilePurge.FilePurgeAPI;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     06/06/2013
*
* Purpose:  Rec Hub Purge Service.
*
* Modification History
* WI 96951 CRG 06/06/2013
*   Img Purge Svc Code: Filter directories to walk based on saved XML
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeSvc {

	/// <summary>
	/// Purge Service
	/// </summary>
	/// <author>
	/// Calvin Glomb
	/// </author>
	public class PurgeService : System.ServiceProcess.ServiceBase {

		/// <summary>Required designer variable.</summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Private collection of Purge objects.  One object exists in the 
		/// collection for every service section in the .ini file.
		/// </summary>
		private System.Collections.SortedList _Purges;

		/// <summary>
		/// Public constructor
		/// </summary>
		public PurgeService() {
			// This call is required by the Windows.Forms Component Designer.
			InitializeComponent();
		}

		/// <summary>
		/// Starts service, and creates a purge process for all Purge sections
		/// in the local config file.
		/// </summary>
		/// <param name="args">Data passed by the start command.</param>
		protected override void OnStart(string[] args) {
			// pause to attach process for debugging, should be removed for release
			string strServiceOptionsKey;
			string strServiceName;
			if(_Purges == null) {
				_Purges = new SortedList();
				StringCollection colSiteOptions = ipoINILib.GetINISection("PurgeSections");
				System.Collections.IEnumerator myEnumerator = ((IEnumerable) colSiteOptions).GetEnumerator();
				while(myEnumerator.MoveNext()) {
					strServiceOptionsKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
					strServiceName = myEnumerator.Current.ToString().Substring(strServiceOptionsKey.Length + 1);
					cPurgeProcess objPurge = new cPurgeProcess(strServiceName, strServiceOptionsKey);
					_Purges.Add(strServiceOptionsKey, objPurge);
				}
			}

			if(_Purges.Count > 0) {
				foreach(cPurgeProcess objTemp in _Purges.Values) {
					objTemp.start();
					//Ideally, I would like to remove it from the collection if this 
					//fails, but I don't have a good way of getting to it's index 
					//without setting a bunch of variables.
				}
			}
		}

		/// <summary>
		/// Stop service and all living purge processes.
		/// </summary>
		protected override void OnStop() {
			cPurgeProcess objPurge;
			if(_Purges.Count > 0) {
				for(int i = _Purges.Count - 1; i >= 0; --i) {
					objPurge = (cPurgeProcess) _Purges.GetByIndex(i);
					objPurge.stop();
					_Purges.RemoveAt(i);
				}
			}

			_Purges = null;
		}

		/// <summary>
		/// Pauses service and all living purge processes.
		/// </summary>
		protected override void OnPause() {
			if(_Purges.Count > 0) {
				foreach(cPurgeProcess objPurge in _Purges.Values) {
					objPurge.pause();
				}
			}
		}

		/// <summary>
		/// Resumes service and all living purge processes.
		/// </summary>
		protected override void OnContinue() {
			if(_Purges.Count > 0) {
				foreach(cPurgeProcess objPurge in _Purges.Values) {
					objPurge.resume();
				}
			}
		}

		/// <summary>
		/// The main entry point for the process
		/// </summary>
		static void Main() {
			System.ServiceProcess.ServiceBase[] ServicesToRun;

			// More than one user Service may run within the same process. To add
			// another service to this process, change the following line to
			// create a second service object. For example,
			//
			//   ServicesToRun = New System.ServiceProcess.ServiceBase[] 
			//      {new Service1(), new MySecondUserService()};
			//
			ServicesToRun = new System.ServiceProcess.ServiceBase[] { new PurgeService() };
			System.ServiceProcess.ServiceBase.Run(ServicesToRun);
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.CanPauseAndContinue = true;
			this.ServiceName = "ImagePurgeService";
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing"></param>
		protected override void Dispose(bool disposing) {
			if(disposing) {
				if(components != null) {
					components.Dispose();
				}
			}

			base.Dispose(disposing);
		}
	}
}
