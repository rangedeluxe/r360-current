using System;
using System.Timers;
using WFS.LTA.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     05/23/2013
*
* Purpose:  Image Purge Service.
*
* Modification History
* WI 96951 CRG 06/06/2013
*    Img Purge Svc Code: Filter directories to walk based on saved XML
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeAPI
{
    /// <summary>
    /// 
    /// </summary>
    public class cPurgeProcess : _purgeBase
    {

        /// <summary>
        /// 
        /// </summary>
        private enum PurgeServiceState
        {
            Started,
            Paused,
            Stopped
        }

        /// <summary>
        /// The _ service state
        /// </summary>
        private PurgeServiceState _ServiceState = PurgeServiceState.Stopped;

        /// <summary>
        /// The _ process timer
        /// </summary>
        private Timer _ProcessTimer;

        /// <summary>
        /// The b first time
        /// </summary>
        private bool bFirstTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="cPurgeProcess" /> class.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="serviceOptionsKey">The service options key.</param>
        public cPurgeProcess(string serviceName, string serviceOptionsKey)
        {
            ServiceName = serviceName;
            ServiceOptionsKey = serviceOptionsKey;
        }

        /// <summary>
        /// Gets or sets the state of the service.
        /// </summary>
        /// <value>
        /// The state of the service.
        /// </value>
        private PurgeServiceState ServiceState
        {
            get
            {
                return _ServiceState;
            }
            set
            {
                _ServiceState = value;
            }
        }

        /// <summary>
        /// Starts the internal timer, and outputs processing options to the log.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.Exception">Purge folder is a required value.
        /// or Specified Purge folder: ServiceOptions.PurgeDirectory is not accessible.</exception>
        /// <author>
        /// Calvin Glomb
        /// </author>
        public bool start()
        {
            bool bolRetVal = true;
            cPurgeServices purgesvc = new cPurgeServices(ServiceName, ServiceOptionsKey);
            try
            {
                bFirstTime = true;
                _ProcessTimer = new System.Timers.Timer(ServiceOptions.PurgeInterval * 1000);
                _ProcessTimer.Elapsed += new ElapsedEventHandler(this.ProcessTimer_Tick);
                _ProcessTimer.AutoReset = true;
                _ProcessTimer.Enabled = true;
                _ProcessTimer.Start();
                ServiceState = PurgeServiceState.Started;
                eventLog.logEvent(ServiceName + " purge process started.", this.GetType().Name, LTAMessageImportance.Essential);
            }
            catch (Exception e)
            {
                eventLog.logEvent("An error occurred while starting " + ServiceName + " purge process: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                eventLog.logEvent(ServiceName + " was NOT started correctly and will not function", e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                eventLog.logEvent("Please correct the issues and restart the service", e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bolRetVal = false;
            }

            return bolRetVal;
        }

        /// <summary>
        /// Stops the internal timer.
        /// </summary>
        /// <author>
        /// Calvin Glomb
        /// </author>
        public void pause()
        {
            try
            {
                if (_ProcessTimer != null)
                {
                    _ProcessTimer.Stop();
                    ServiceState = PurgeServiceState.Paused;
                    eventLog.logEvent(ServiceName + " purge process paused.", this.GetType().Name, LTAMessageImportance.Essential);
                }
            }
            catch (Exception e)
            {
                eventLog.logEvent("An error occurred while pausing " + ServiceName + " purge process: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
            }
        }

        /// <summary>
        /// Re-starts the internal timer.
        /// </summary>
        /// <author>
        /// Calvin Glomb
        /// </author>
        public void resume()
        {
            try
            {
                if (_ProcessTimer != null)
                {
                    _ProcessTimer.Start();
                    ServiceState = PurgeServiceState.Started;
                    eventLog.logEvent(ServiceName + " purge process resumed.", this.GetType().Name, LTAMessageImportance.Essential);
                }
            }
            catch (Exception e)
            {
                eventLog.logEvent("An error occurred while resuming " + ServiceName + " purge process: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
            }
        }

        /// <summary>
        /// Stops the internal timer.
        /// </summary>
        /// <author>
        /// Calvin Glomb
        /// </author>
        public void stop()
        {
            try
            {
                if (_ProcessTimer != null)
                {
                    _ProcessTimer.Stop();
                    _ProcessTimer.AutoReset = false;
                    _ProcessTimer.Enabled = false;
                    _ProcessTimer = null;
                    ServiceState = PurgeServiceState.Stopped;
                    eventLog.logEvent(ServiceName + " purge process stopped.", this.GetType().Name, LTAMessageImportance.Essential);
                }
            }
            catch (Exception e)
            {
                eventLog.logEvent("An error occurred while stopping " + ServiceName + " purge process: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
            }
        }

        /// <summary>
        /// Runs the purge process when the timer ticks.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs" /> instance containing the event data.</param>
        /// <author>
        /// Calvin Glomb
        /// </author>
        private void ProcessTimer_Tick(object sender, System.Timers.ElapsedEventArgs e)
        {
            cPurgeServices purgesvc = new cPurgeServices(ServiceName, ServiceOptionsKey);
            try
            {
                _ProcessTimer.Stop();

                // compare current time with restart time
                if (DateTime.Compare(DateTime.Now.Date, ServiceOptions.TimeToPurge.Date) >= 0 || bFirstTime)
                {
                    if (!purgesvc.InitializeService())
                    {
                        throw (new Exception("Could not initialize"));
                    }

                    bFirstTime = false;
                    eventLog.logEvent(ServiceName + " purge process starting.", this.GetType().Name, LTAMessageImportance.Verbose);
                    ServiceOptions.TimeToPurge = ServiceOptions.TimeToPurge.AddDays(1);
                    purgesvc.PurgeFolders(ServiceOptions);
                    eventLog.logEvent(ServiceName + " purge process completed.", this.GetType().Name, LTAMessageImportance.Verbose);
                    purgesvc.PurgeNotificationFolders(ServiceOptions);
                    eventLog.logEvent(ServiceName + " purge notifications process completed.", this.GetType().Name, LTAMessageImportance.Verbose);

                }
            }
            catch (Exception ex)
            {
                eventLog.logEvent(ServiceName + " purge process terminated abnormally : " + ex.Message, ex.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
            }

            if (ServiceState == PurgeServiceState.Started)
            {
                _ProcessTimer.Start();
            }
        }

        /// <summary>
        /// Runs the purge process a single time.  This method can be used to
        /// call the application from an external assembly without using the
        /// service features.
        /// </summary>
        /// <author>
        /// Calvin Glomb
        /// </author>
        public void runOnce()
        {
            cPurgeServices purgesvc = new cPurgeServices(ServiceName, ServiceOptionsKey);
            ServiceState = PurgeServiceState.Started;
            try
            {
                if (!purgesvc.InitializeService())
                {
                    throw (new Exception("Could not initialize"));
                }

                eventLog.logEvent(ServiceName + " purge process starting.", this.GetType().Name, LTAMessageImportance.Verbose);
                purgesvc.PurgeFolders(ServiceOptions);
                eventLog.logEvent(ServiceName + " purge process completed.", this.GetType().Name, LTAMessageImportance.Verbose);
                purgesvc.PurgeNotificationFolders(ServiceOptions);
                eventLog.logEvent(ServiceName + " purge notifications process completed.", this.GetType().Name, LTAMessageImportance.Verbose);
            }
            catch (Exception e)
            {
                eventLog.logEvent("An error occurred while running " + ServiceName + " purge process once: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
            }

            ServiceState = PurgeServiceState.Stopped;
        }
    }
}
