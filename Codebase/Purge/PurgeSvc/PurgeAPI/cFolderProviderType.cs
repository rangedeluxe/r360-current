﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.FilePurge
{
    public enum cFolderProviderType
    {
        PICS,
        FILEGROUP,
        PAYMENTSOURCE,
        NONE
    }
}
