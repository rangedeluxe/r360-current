﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     05/23/2013
*
* Purpose:  Image Purge Service.
*
* Modification History
* WI 104252 CRG 06/06/2013
*    Img Purge Svc Code: Create Purge XML class
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeAPI {
	class cPurgeXML{
		/// <summary>
		/// The _bank ID
		/// </summary>
		private int _bankID;

		/// <summary>
		/// The _client account ID
		/// </summary>
		private int _clientAccountID;

		/// <summary>
		/// The _ earliest date purged
		/// </summary>
		private int _EarliestDatePurged;

		/// <summary>
		/// Initializes a new instance of the <see cref="cPurgeXML"/> class.
		/// </summary>
		/// <param name="bankID">The bank ID.</param>
		/// <param name="clientAccountID">The client account ID.</param>
		public cPurgeXML(int BankID, int ClientAccountID) {
			_bankID = BankID;
			_clientAccountID = ClientAccountID;
		}

		/// <summary>
		/// Gets or sets the bank ID.
		/// </summary>
		/// <value>
		/// The bank ID.
		/// </value>
		public int BankID {
			get {
				return _bankID;
			}
		}

		/// <summary>
		/// Gets or sets the client account ID.
		/// </summary>
		/// <value>
		/// The client account ID.
		/// </value>
		public int ClientAccountID {
			get {
				return _clientAccountID;
			}
		}

		/// <summary>
		/// Gets or sets the earliest date purged.
		/// </summary>
		/// <value>
		/// The earliest date purged.
		/// </value>
		public int EarliestDatePurged {
			get {
				return _EarliestDatePurged;
			}
			set {
				_EarliestDatePurged = value;
			}
		}
	}
}
