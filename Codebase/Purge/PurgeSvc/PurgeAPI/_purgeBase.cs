﻿using System;
using WFS.LTA.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     06/06/2013
*
* Purpose:  Image Purge Service.
*
* Modification History
* WI 96947 CRG 06/06/2013
*    Img Purge Svc Code: Read start-time config information/begin processing/sleep
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeAPI {
	/// <summary>
	/// 
	/// </summary>
	public abstract class _purgeBase {
		/// <summary>
		/// The _ site key
		/// </summary>
		private string _SiteKey = string.Empty;

		/// <summary>
		/// The _ service options
		/// </summary>
		private cPurgeServiceOptions _ServiceOptions = null;

		/// <summary>
		/// The _ service name
		/// </summary>
		private string _ServiceName = string.Empty;

		/// <summary>
		/// The _ service options key
		/// </summary>
		private string _ServiceOptionsKey = string.Empty;

		/// <summary>
		/// The _ event log
		/// </summary>
		private ltaLog _EventLog = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="_purgeBase"/> class.
		/// </summary>
		public _purgeBase() {
		}

		/// <summary>
		/// Gets or sets the name of the service.
		/// </summary>
		/// <value>
		/// The name of the service.
		/// </value>
		public string ServiceName {
			get {
				return _ServiceName;
			}
			set {
				_ServiceName = value;
			}
		}

		/// <summary>
		/// Gets or sets the service options.
		/// </summary>
		/// <value>
		/// The service options.
		/// </value>
		public cPurgeServiceOptions ServiceOptions {
			get {
				try {
					if(_ServiceOptions == null) {
						_ServiceOptions = new cPurgeServiceOptions(_ServiceOptionsKey);
					}
				} catch(Exception e) {
					throw (e);
				}

				return _ServiceOptions;
			}
		}

		/// <summary>
		/// Gets or sets the service options key.
		/// </summary>
		/// <value>
		/// The service options key.
		/// </value>
		public string ServiceOptionsKey {
			get {
				return _ServiceOptionsKey;
			}
			set {
				_ServiceOptionsKey = value;
			}
		}

		/// <summary>
		/// Gets or sets the site key.
		/// </summary>
		/// <value>
		/// The site key.
		/// </value>
		protected string SiteKey {
			get {
				return (_SiteKey);
			}
			set {
				_SiteKey = value;
			}
		}

		/// <summary>
		/// Logging object for the current instance.
		/// </summary>
		/// <value>
		/// The event log.
		/// </value>
		/// <author>
		/// Calvin Glomb
		/// </author>
		public ltaLog eventLog {
			get {
				if(_EventLog == null) {
					_EventLog = new ltaLog(ServiceOptions.logFilePath, ServiceOptions.logFileMaxSize, (LTAMessageImportance) ServiceOptions.loggingDepth);
				}

				return _EventLog;
			}
		}
	}
}
