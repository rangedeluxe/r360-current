using System;
using System.Collections;
using System.Collections.Specialized;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     06/06/2013
*
* Purpose:  Purge Service Options.
*
* Modification History
* WI 96945 CRG 06/06/2013
*    Img Purge Svc Code: Create start-up methods & Config File
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeAPI
{
    /// <author>Calvin Glomb</author>
    /// <summary>
    /// Purge service options
    /// </summary>
    public class cPurgeServiceOptions : cSiteOptions
    {

        /// <summary>
        /// The _ purge interval
        /// </summary>
        private int _PurgeInterval = 60;

        /// <summary>
        /// The _ max retries
        /// </summary>
        private int _MaxRetries = 3;

        /// <summary>
        /// The _ time to purge
        /// </summary>
        private DateTime _TimeToPurge = DateTime.Now;

        /// <summary>
        /// The purge folder
        /// </summary>
        private string _PurgePath = string.Empty;

        /// <summary>
        /// The purge folder
        /// </summary>
        private string _NotificationPath = string.Empty;

        /// <summary>
        /// The _ status XML file
        /// </summary>
        private string _LocalCache = string.Empty;

        /// <summary>
        /// Public Constructor.
        /// </summary>
        /// <param name="siteKey">ini key that contains the current Purge options.</param>
        /// <author>
        /// Calvin Glomb
        /// </author>
        public cPurgeServiceOptions(string siteKey)
            : base(siteKey)
        {
            LoadPurgeOptions(siteKey);
        }

        /// <summary>
        /// Returns the number of seconds that should elapse for the Image service
        /// should pause before scanning the directory specified by the
        /// OnlineArchive setting for files that require deletion.
        /// </summary>
        /// <value>
        /// The purge interval.
        /// </value>
        /// <author>
        /// Calvin Glomb
        /// </author>
        public int PurgeInterval
        {
            get
            {
                return (_PurgeInterval);
            }
            set
            {
                _PurgeInterval = value;
            }
        }

        /// <summary>
        /// Gets or sets the max retries.
        /// </summary>
        /// <value>
        /// The max retries.
        /// </value>
        public int MaxRetries
        {
            get
            {
                return (_MaxRetries);
            }
            set
            {
                _MaxRetries = value;
            }
        }

        /// <summary>
        /// Gets or sets the time to purge.
        /// </summary>
        /// <value>
        /// The time to purge.
        /// </value>
        public DateTime TimeToPurge
        {
            get
            {
                return _TimeToPurge;
            }
            set
            {
                if (_TimeToPurge == null)
                {
                    _TimeToPurge = DateTime.Now;
                }
                else
                {
                    _TimeToPurge = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the purge folder.
        /// </summary>
        /// <value>
        /// The purge folder.
        /// </value>
        public string PurgePath
        {
            get
            {
                return _PurgePath;
            }
            set
            {
                _PurgePath = value;
            }
        }

        /// <summary>
        /// Gets or sets the purge folder.
        /// </summary>
        /// <value>
        /// The purge folder.
        /// </value>
        public string NotificationPath
        {
            get
            {
                return _NotificationPath;
            }
            set
            {
                _NotificationPath = value;
            }
        }

        /// <summary>
        /// Gets or sets the status XML file.
        /// </summary>
        /// <value>
        /// The status XML file.
        /// </value>
        public string LocalCache
        {
            get
            {
                return _LocalCache;
            }
            set
            {
                _LocalCache = value;
            }
        }

        /// <summary>
        /// Reads the specified section from the local .ini file to retrieve
        /// processing options.
        /// </summary>
        /// <param name="siteKey">The site key.</param>
        /// <author>
        /// Calvin Glomb
        /// </author>
        private void LoadPurgeOptions(string siteKey)
        {
            string strKey = string.Empty;
            string strValue = string.Empty;
            int number = 0;
            StringCollection colSiteOptions = ipoINILib.GetINISection(siteKey);
            System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while (myEnumerator.MoveNext())
            {
                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length + 1);

                // purge interval is optional in the ini file
                if (strKey.ToLower() == "purgeinterval")
                {
                    if (int.TryParse(strValue, out number))
                    {
                        PurgeInterval = number;
                    }
                    else
                    {
                        PurgeInterval = 60; //10 seconds
                    }
                }
                else if (strKey.ToLower() == "maxretries")
                {
                    if (int.TryParse(strValue, out number))
                    {
                        MaxRetries = number;
                    }
                    else
                    {
                        MaxRetries = 3;
                    }

                }
                else if (strKey.ToLower() == "localcache")
                {
                    LocalCache = strValue;
                }
                else if (strKey.ToLower() == "purgepath")
                {
                    PurgePath = ipoLib.cleanPath(strValue);
                }
                else if (strKey.ToLower() == "cendspath")
                {
                    NotificationPath = ipoLib.cleanPath(strValue);
                }
            }
        }
    }
}

