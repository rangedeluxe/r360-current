﻿using System;
using System.IO;
using System.Linq;
using WFS.LTA.Common;
using WFS.RecHub.FilePurge.FolderProviders;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     06/07/2013
*
* Purpose:  Image Purge Service.
*
* Modification History
* WI 104251 CRG 06/06/2013
*    Img Purge Svc Code: Create Purge Folder class
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeAPI
{
    /// <summary>
    /// contains operations dealing with the IO to purge folders 
    /// </summary>
    public class cPurgeFolders : _purgeBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="cPurgeFolders"/> class.
        /// </summary>
        public cPurgeFolders()
        {
        }

        /// <summary>
        /// verify image folder to purge, by checking for proper file structure
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public bool VerifyPurgeFolder(string path)
        {
            bool bReturnValue = false;

            try
            {
                if (Directory.Exists(path))
                {
                    eventLog.logEvent("    Purge Folder exists at '" + path + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                    if (!ValidatePurgeFolder(path))
                    {
                        eventLog.logEvent("    Purge Folder contains Image folders'" + path + "'.", this.GetType().Name, LTAMessageImportance.Debug);
                        bReturnValue = true;
                    }
                    else
                    {
                        eventLog.logEvent("    Purge Folder contains invalid folders'" + path + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                        bReturnValue = false;
                    }
                }
                else
                {
                    eventLog.logEvent("Could not find purge folder '" + path + "'.", string.Empty, LTAMessageType.Error, LTAMessageImportance.Essential);
                    bReturnValue = false;
                }
            }
            catch (Exception e)
            {
                eventLog.logEvent("Cannot load purge options: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }

        /// <summary>
        /// Validates the purge folder, by size, is numeric and proper date.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        private bool ValidatePurgeFolder(string path)
        {
            bool bReturnValue = true;
            int value = 0;
            int today = Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd"));
            foreach (string datePath in Directory.GetDirectories(path))
            {
                DirectoryInfo dateInfo = new DirectoryInfo(datePath);
                if (dateInfo.Name.Length > 8 ||							// check size of folder name is to big 
                    dateInfo.Name.Length < 8 ||							// check size of folder name is too small
                    int.TryParse(dateInfo.Name, out value) == false ||	//is folder name numeric
                    value < 19000101 ||									// is less than 01/01/1900
                    value > today)
                {									// is in the future
                    eventLog.logEvent(path + " purge folder contains invalid folder " + datePath, string.Empty, LTAMessageType.Error, LTAMessageImportance.Verbose);
                    bReturnValue = false;
                }
            }

            return bReturnValue;
        }

        /// <summary>
        /// Removes the empty folders.
        /// </summary>
        /// <param name="root">The root.</param>
        public void RemoveEmptyFolders(DirectoryInfo root, DirectoryInfo baseroot)
        {
            // Depth-First approach
            var subfolders = root.GetDirectories();
            foreach (var subfolder in subfolders)
                RemoveEmptyFolders(subfolder, baseroot);

            // We don't want to delete our FileGroup folders, or Root images folder.
            if (baseroot.FullName != root.FullName && IsFolderEmpty(root.FullName))
            {
                try
                {
                    // Check to see if we have a thumbs.db.
                    var thumbspath = Path.Combine(root.FullName, "Thumbs.db");
                    if (File.Exists(thumbspath))
                        File.Delete(thumbspath);
                    root.Delete();
                }
                catch (Exception e)
                {
                    eventLog.logEvent(ServiceName + string.Format(" : ERROR Deleting Empty Folder {0} : ", root.FullName) + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                }
            }
        }

        /// <summary>
        /// Determines whether [is folder empty] [the specified path].
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>
        ///   <c>true</c> if [is folder empty] [the specified path]; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsFolderEmpty(string path)
        {
            return !Directory.GetDirectories(path).Any() 
                && !Directory.GetFiles(path)
                    .Where(x => !x.EndsWith("Thumbs.db"))
                    .Any();
        }

        /// <summary>
        /// Purges the folder. (will retry max retries times before giving up.
        /// </summary>
        /// <param name="folder">The folder.</param>
        /// <returns></returns>
        public bool PurgeFolder(PurgeFolder folder, bool expand)
        {
            bool bReturnValue = true;
            int maxRetries = ServiceOptions.MaxRetries;
            try
            {

                var foldername = folder.FullPathName;
                bool retry = false;
                int retrycount = 0;
                do
                {
                    try
                    {
                        Directory.Delete(foldername, true);
                        retry = false;
                        folder.Purged = true;
                    }
                    catch (IOException e)
                    {
                        retrycount++;
                        if (retrycount < maxRetries)
                        {
                            System.Threading.Thread.Sleep(500);
                            retry = true;
                        }
                        else
                        {
                            eventLog.logEvent("Retrying folder delete " + retrycount + ". " + e.Message, e.Source, LTAMessageType.Warning, LTAMessageImportance.Essential);
                            retry = false;
                            bReturnValue = false;
                        }
                    }
                    catch (Exception e)
                    {
                        eventLog.logEvent("Purge process error while retrying " + foldername + " : " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                        bReturnValue = false;
                    }
                }
                while (retry);
            }
            catch (Exception e)
            {
                eventLog.logEvent("Purge process error with " + folder.FullPathName + " : " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }
    }
}
