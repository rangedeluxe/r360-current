﻿
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  To provide a small model class to contain the data coming back 
*           from the purge queue database call. 
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.QueueProviders
{
    public class PurgeQueue
    {
        public int BankID { get; set; }
        public int ClientAccountID { get; set; }
        public int ImageRetentionDays { get; set; }
        public int BankKey { get; set; }
        public int ClientAccountKey { get; set; }
        public string FileGroup { get; set; }

        /// <summary>
        /// This date represents the last date in which a particular batch
        /// may have been created (ImmutableDateKey) in order to survive
        /// the current purge round.  This is what comes back from the
        /// database.
        /// </summary>
        public int BatchImmutableDateThreshold { get; set; }
    }
}
