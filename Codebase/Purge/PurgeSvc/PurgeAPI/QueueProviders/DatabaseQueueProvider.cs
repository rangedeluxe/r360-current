﻿using System;
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.DAL.FilePurge;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  To provide a simple layer around the cPurgeDAL, and to migrate
*           the parsing of the datarows from the out of the main service
*           logic.          
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.QueueProviders
{
    public class DatabaseQueueProvider : IQueueProvider, IDisposable
    {
        private readonly cPurgeDAL _purgedal;

        public DatabaseQueueProvider(string sitekey)
        {
            _purgedal = new cPurgeDAL(sitekey);
        }

        public IEnumerable<PurgeQueue> GetPurgeQueue()
        {
            DataTable datatable;
            var output = new List<PurgeQueue>();
            try
            {
                _purgedal.GetPurgeData(out datatable);
                foreach (DataRow row in datatable.Rows)
                {
                    var queue = new PurgeQueue();
                    queue.BankID = Convert.ToInt32(row["SiteBankID"]);
                    queue.BankKey = Convert.ToInt32(row["BankKey"]);
                    queue.ClientAccountID = Convert.ToInt32(row["SiteClientAccountID"]);
                    queue.ClientAccountKey = Convert.ToInt32(row["ClientAccountKey"]);
                    queue.BatchImmutableDateThreshold = Convert.ToInt32(row["DataDepositDateKey"]);
                    queue.FileGroup = row["FileGroup"] == null || row["FileGroup"] == DBNull.Value
                        ? string.Empty
                        : (string)row["FileGroup"];
                    output.Add(queue);
                }
            }
            catch (Exception) { }

            return output;
        }

        public void Dispose()
        {
            _purgedal.Dispose();
        }
    }
}
