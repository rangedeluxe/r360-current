﻿using System;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  This class tokenizes folder paths, and allows the parsers to 
*           read from back-to-front in order to support the batch source names,
*           which is optional, easily.
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.Utilities
{

    /// <summary>
    /// I know it's a bit strange, but this is how the Parsers work.
    /// Basically, designed to read back-to-front folder-by-folder of 
    /// an absolute or relative folder path.
    /// 
    /// Example: '20060228\1\4035', reads '4035', then '1', then '20060228', 
    /// then finally just null.
    /// </summary>
    public class ReverseFolderReader
    {
        private readonly string _path;
        private string _currentpath;

        public ReverseFolderReader(string path) 
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentOutOfRangeException("path");

            _path = path;
            _currentpath = path;
            HasParentFolder = true;
            if (_currentpath.EndsWith(@"\"))
                _currentpath = _currentpath.Substring(0, _currentpath.Length - 1);
        }

        public bool HasParentFolder { get; private set; }

        /// <summary>
        /// Reads the next token in the folder path.
        /// </summary>
        /// <returns></returns>
        public string ReadParentFolder()
        {
            if (!HasParentFolder)
                return null;

            var index = _currentpath.LastIndexOf('\\');
            string output;
            if (index < 0)
            {
                output = _currentpath;
                _currentpath = string.Empty;
                HasParentFolder = false;
            }
            else
            {
                output = _currentpath.Substring(index + 1);
                _currentpath = _currentpath.Substring(0, index);
            }

            // Double check to see if the current 'directory' is of 'c:' (no more parents).
            if (_currentpath.IndexOf('\\') == -1 &&
                (_currentpath.Contains(':') || string.IsNullOrWhiteSpace(_currentpath)))
                HasParentFolder = false;

            return output;
        }
    }
}
