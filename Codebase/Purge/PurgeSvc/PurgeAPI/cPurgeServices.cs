﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using WFS.LTA.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.DAL.FilePurge;
using WFS.RecHub.DTO;
using WFS.RecHub.FilePurge.FolderProviders;
using WFS.RecHub.FilePurge.QueueProviders;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     05/23/2013
*
* Purpose:  Image Purge Service.
*
* Modification History
* WI 96947  CRG 06/06/2013
*    - Img Purge Svc Code: Read start-time config information/begin processing/sleep
* WI 155206 BLR 07/22/2014
*    - Completely reworked the way this service reads the file system.  Severe
*      changes made to the PurgeFolders() method.
* WI 155630 BLR 07/24/2014
*    - Added an audit event call for a purge completion. 
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeAPI
{


    class cPurgeServices : _purgeBase
    {

        private readonly IFolderProvider _imagefolderprovider;
        private readonly IFolderProvider _notificationfolderprovider;
        private readonly IQueueProvider _purgequeueprovider;
        private readonly cPurgeDAL _purgedal;

        private cSystemSetupValues _SystemSetup = null;
        private SystemSetup retentionValues = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="cPurgeServices" /> class.
        /// </summary>
        public cPurgeServices()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="cPurgeServices" /> class.
        /// </summary>
        /// <param name="serviceName">Name of the service.</param>
        /// <param name="serviceOptionsKey">The service options key.</param>
        public cPurgeServices(string serviceName, string serviceOptionsKey)
        {
            ServiceName = serviceName;
            ServiceOptionsKey = serviceOptionsKey;
            _imagefolderprovider = FolderProviderFactory.CreateFromSiteOptions(serviceOptionsKey);
            _notificationfolderprovider = new NotificationFolderProvider();
            _purgequeueprovider = new DatabaseQueueProvider(serviceOptionsKey);
            _purgedal = new cPurgeDAL(serviceOptionsKey);
        }

        /// <summary>
        /// Gets the default system setup.
        /// </summary>
        /// <value>
        /// The default system setup.
        /// </value>
        protected cSystemSetupValues DefaultSystemSetup
        {
            get
            {
                return (_SystemSetup);
            }
        }

        /// <summary>
        /// Initializes the service.
        /// </summary>
        /// <returns></returns>
        public bool InitializeService()
        {
            bool bReturnValue = true;
            if (!LoadPurgeOptions())
            {
                bReturnValue = false;
            }

            if (!LoadDalParameters())
            {
                bReturnValue = false;
            }

            return bReturnValue;
        }

        /// <summary>
        /// Cleans the folder.
        /// </summary>
        /// <param name="ServiceOptions">The service options.</param>
        public void PurgeFolders(cPurgeServiceOptions ServiceOptions)
        {
            try
            {
                bool UpdatePurgeDate = true;
                cPurgeXMLFile xmlFile = new cPurgeXMLFile();
                cPurgeFolders folders = new cPurgeFolders();

                var purgequeuefolders = _purgequeueprovider.GetPurgeQueue();

                if (!purgequeuefolders.Any())
                {
                    // Purge queue empty? Then we have no work to do.
                    eventLog.logEvent(ServiceName + " Purge queue is empty; no work to do. ", this.GetType().Name, LTAMessageImportance.Essential);
                    return;
                }

                // This is a cache of the folders in each file group root.
                // We don't want to be parsing over and over again.
                var foldercache = new Dictionary<string, List<PurgeFolder>>();

                // Filter the folder list based on ImmutableDate, XMLDate, and ImageRetentionDays
                var purgefolders = new List<PurgeFolder>();
                foreach (var purgefolder in purgequeuefolders)
                {
                    // Using the PICS purgepath, unless filegroup is active and the workgroup has a valid filegroup defined.
                    var purgepath = ServiceOptions.PurgePath;
                    if (ServiceOptions.imageStorageMode == Common.ImageStorageMode.FILEGROUP
                        && !string.IsNullOrWhiteSpace(purgefolder.FileGroup))
                    {
                        purgepath = purgefolder.FileGroup;
                    }

                    // Validate our purge path.  This could be the FileGroup path or the Pics path,
                    // either way we want to check if it exists and toss up an error if it doesn't.
                    var message = string.Format("Purge Path: '{0}', BankID: {1}, ClientAccountID: {2}, ThresholdDate: {3}.", purgepath, purgefolder.BankID, purgefolder.ClientAccountID, purgefolder.BatchImmutableDateThreshold);
                    try
                    {
                        if (string.IsNullOrWhiteSpace(purgepath) || !Directory.Exists(purgepath))
                        {
                            eventLog.logWarning("Purge Path is NOT Valid. " + message + " Moving onto next purge folder...", LTAMessageImportance.Essential);
                            _purgedal.WriteSystemLevelEventLog("PROCESSINGEXCEPTION", "Image Purge Failed to find FileGroup/Root folder for " + message);
                            continue;
                        }
                    }
                    catch
                    {
                        // Means when calling Directory.Exists we may have hit a permission or generic I/O issue.
                        eventLog.logWarning("Purge Path is NOT Valid (IOException while checking the directory). " + message + " Moving onto next purge folder...", LTAMessageImportance.Essential);
                        _purgedal.WriteSystemLevelEventLog("PROCESSINGEXCEPTION", "Image Purge Failed to find FileGroup/Root folder for " + message);
                        continue;
                    }
                    

                    // Gather all of our data first.
                    List<PurgeFolder> allfolders;
                    if (foldercache.ContainsKey(purgepath))
                        allfolders = foldercache[purgepath];
                    else
                    {
                        allfolders = _imagefolderprovider.GetPurgeFolders(purgepath)
                            .ToList();
                        foldercache.Add(purgepath, allfolders);
                    }

                    // For each Record, grab all folders in the file system which match the bankid & clientaccountid
                    // and have an immutable date key <= the Batch Threshold from the database record.
                    // (and hasn't already been added to the 'to-purge' list).
                    var batchdatethreshold = DateTime.MinValue;
                    DateTime.TryParseExact(purgefolder.BatchImmutableDateThreshold.ToString(), FolderProviders.PurgeFolder.IMMUTABLE_DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out batchdatethreshold);

                    var folderstopurge = allfolders
                        .Where(x => x.BankID == purgefolder.BankID &&
                                    x.ClientAccountID == purgefolder.ClientAccountID &&
                                    x.ImmutableDate <= batchdatethreshold &&
                                    !purgefolders.Any(y => y.FullPathName == x.FullPathName));

                    if (folderstopurge.Count() > 0)
                        eventLog.logEvent(string.Format("Purge Queue Record: BankID:'{0}', ClientAccountID:'{1}', Threshold:'{2}' Found {3} folders to purge on the file system.", purgefolder.BankID, purgefolder.ClientAccountID, purgefolder.BatchImmutableDateThreshold, folderstopurge.Count()), LTAMessageImportance.Debug);

                    // Need this for future use.
                    foreach (var f in folderstopurge)
                    {
                        f.Queue = purgefolder;
                    }
                        

                    purgefolders.AddRange(folderstopurge);
                }

                // Now purgefolders contains the folders we actually want to purge.
                UpdatePurgeDate = true;
                foreach (var folder in purgefolders)
                {
                    if (!Directory.Exists(folder.FullPathName))
                    {
                        eventLog.logEvent(folder.FullPathName + " does not exist, skipping purge...", this.GetType().Name, LTAMessageImportance.Essential);
                        continue;
                    }

                    if (!this.PurgeFolder(folder, UpdatePurgeDate))
                    {
                        eventLog.logEvent(folder.FullPathName + " could not be purged.", this.GetType().Name, LTAMessageImportance.Essential);
                        UpdatePurgeDate = false;
                    }
                    else
                    {
                        eventLog.logEvent(folder.FullPathName + " Purged Successfully.", this.GetType().Name, LTAMessageImportance.Essential);
                    }
                }

                eventLog.logEvent(ServiceName + " Cleaning out Empty folders...", this.GetType().Name, LTAMessageImportance.Verbose);
                foreach (var pathpair in foldercache)
                {
                    var path = new DirectoryInfo(pathpair.Key);
                    folders.RemoveEmptyFolders(path, path);
                    eventLog.logEvent(ServiceName + " Finished Cleaning out Empty folders on path: " + path.FullName, this.GetType().Name, LTAMessageImportance.Verbose);
                }
                eventLog.logEvent(ServiceName + " purge process purging folder '" + ServiceOptions.PurgePath + "' is done.", this.GetType().Name, LTAMessageImportance.Verbose);

                // Finally we write out an audit event.
                eventLog.logEvent(ServiceName + ": Writing event audit...", LTAMessageImportance.Verbose);
                var numpurged = purgefolders.Count(x => x.Purged);
                if (_purgedal.WriteAuditEvent("Image Purge", numpurged, true))
                    eventLog.logEvent(ServiceName + ": Event audit written.", LTAMessageImportance.Verbose);
                else
                    eventLog.logEvent(ServiceName + ": Event audit failed to write to database.", LTAMessageImportance.Essential);
            }
            catch (Exception e)
            {
                eventLog.logEvent(ServiceName + " purge process error while purging subdirectories : " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
            }
        }

        private bool LoadPurgeOptions()
        {
            bool bReturnValue = true;
            try
            {
                eventLog.logEvent("    Purge Folder: " + this.ServiceOptions.PurgePath, this.GetType().Name, LTAMessageImportance.Verbose);
                eventLog.logEvent("    Notification Folder = '" + this.ServiceOptions.NotificationPath + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                eventLog.logEvent("    Purge Date: " + this.ServiceOptions.TimeToPurge, this.GetType().Name, LTAMessageImportance.Verbose);
            }
            catch (Exception e)
            {
                eventLog.logEvent(ServiceName + " cannot load purge options: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }

        private bool LoadDalParameters()
        {
            DataTable dt = null;
            bool bReturnValue = false;
            try
            {
                using (cSystemSetupDAL objDAL = new cSystemSetupDAL(ServiceOptionsKey))
                {
                    if (objDAL.GetSystemSetupBySection("Rec Hub Table Maintenance", out dt))
                    {
                        if (this.retentionValues == null)
                        {
                            this.retentionValues = new SystemSetup(SiteKey);
                        }

                        this.retentionValues.ProcessSystemSetupForPurge(dt);
                        _SystemSetup = this.retentionValues.GetValues();
                    }
                }

                // verify that the values were generated from the config files 
                eventLog.logEvent("    Default Data Retention Days = '" + this.DefaultSystemSetup.DefaultDataRetentionDays + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                eventLog.logEvent("    Default Image Retention Days = '" + this.DefaultSystemSetup.DefaultImageRetentionDays + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                eventLog.logEvent("    Max Retention Days = '" + this.DefaultSystemSetup.MaxRetentionDays + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                eventLog.logEvent("    Notifications Retention Days = '" + this.DefaultSystemSetup.NotificationsRetentionDays + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                eventLog.logEvent("    Purge History Retention Days = '" + this.DefaultSystemSetup.PurgeHistoryRetentionDays + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                eventLog.logEvent("    Queue Retention Days = '" + this.DefaultSystemSetup.QueueRetentionDays + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                bReturnValue = true;
            }
            catch (Exception e)
            {
                eventLog.logEvent(ServiceName + " cannot load purge parameters: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }

        private bool VerifyPurgeXMLFile()
        {
            bool bReturnValue = false;
            try
            {
                if (Directory.Exists(ServiceOptions.LocalCache))
                {
                    eventLog.logEvent("    Purge Cache Folder exists at '" + this.ServiceOptions.LocalCache + "'.", this.GetType().Name, LTAMessageImportance.Verbose);
                    bReturnValue = true;
                }

            }
            catch (Exception e)
            {
                eventLog.logEvent(ServiceName + " cannot load purge options: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }

        private bool GetDataTable(out DataTable dt)
        {
            dt = null;
            bool bReturnValue = false;
            try
            {
                using (cPurgeDAL objDAL = new cPurgeDAL(ServiceOptionsKey))
                {
                    bReturnValue = objDAL.GetPurgeData(out dt);
                }
            }
            catch (Exception e)
            {
                eventLog.logEvent(ServiceName + " cannot load purge parameters: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }

        public bool PurgeFolder(PurgeFolder folder, bool UpdatePurgeDate)
        {
            bool bReturnValue = false;
            try
            {
                string foldername = folder.FullPathName;

                // purge folder
                cPurgeFolders folders = new cPurgeFolders();
                if (folders.PurgeFolder(folder, true))
                {
                    // update purge history
                    using (cPurgeDAL objDAL = new cPurgeDAL(ServiceOptionsKey))
                    {
                        cPurgeHistory history = new cPurgeHistory();
                        // (2 means Image Purge, 1 means Data Purge)
                        history.Type = 2;
                        history.BankKey = folder.Queue.BankKey;
                        history.ClientAccountKey = folder.Queue.ClientAccountKey;
                        history.DataDepositDateKey = int.Parse(folder.ImmutableDateString);
                        history.DepositDateKey = int.Parse(folder.ImmutableDateString);
                        bReturnValue = objDAL.InsertPurgeHistory(history);
                    }

                    // log event
                    eventLog.logEvent(ServiceName + " removed " + foldername, this.GetType().Name, LTAMessageImportance.Verbose);
                    bReturnValue = true;
                }

            }
            catch (Exception e)
            {
                eventLog.logEvent(ServiceName + " purge process error while purging subdirectories : " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }

        public void PurgeNotificationFolders(cPurgeServiceOptions ServiceOptions)
        {
            try
            {
                cPurgeFolders folders = new cPurgeFolders();
                int retentionDate = Convert.ToInt32(DateTime.Now.Date.AddDays(-DefaultSystemSetup.NotificationsRetentionDays).ToString("yyyyMMdd"));
                var folderList = _notificationfolderprovider
                    .GetPurgeFolders(ServiceOptions.NotificationPath)
                    .ToList();

                foreach (var folder in folderList)
                {
                    if (int.Parse(folder.ImmutableDateString) <= retentionDate)
                    {
                        if (folders.PurgeFolder(folder, false))
                        {
                            eventLog.logEvent(ServiceName + " purged notification folder " + folder.FullPathName, this.GetType().Name, LTAMessageImportance.Debug);
                        }
                        else
                        {
                            eventLog.logEvent(ServiceName + " could not purge notification folder " + folder.FullPathName, this.GetType().Name, LTAMessageImportance.Essential);
                        }
                    }
                    else
                    {
                        eventLog.logEvent(ServiceName + " did not process notification folder " + folder.FullPathName, this.GetType().Name, LTAMessageImportance.Debug);
                    }
                }

                DirectoryInfo path = new DirectoryInfo(ServiceOptions.NotificationPath);
                eventLog.logEvent(ServiceName + " purge process purging notification folder '" + ServiceOptions.NotificationPath + "' is done.", this.GetType().Name, LTAMessageImportance.Verbose);

                // Finally we write out an audit event.
                eventLog.logEvent(ServiceName + ": Writing event audit...", LTAMessageImportance.Verbose);
                var numpurged = folderList.Count(x => x.Purged);
                if (_purgedal.WriteAuditEvent("Notification Purge", numpurged, false))
                    eventLog.logEvent(ServiceName + ": Event audit written.", LTAMessageImportance.Verbose);
                else
                    eventLog.logEvent(ServiceName + ": Event audit failed to write to database.", LTAMessageImportance.Essential);
            }
            catch (Exception e)
            {
                eventLog.logEvent(ServiceName + " purge process error while purging notification subdirectories : " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
            }
            finally
            {
            }
        }

    }
}

