﻿using System;
using System.Data;
using System.IO;
using System.Xml;
using WFS.LTA.Common;
using WFS.RecHub.DAL.FilePurge;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     06/06/2013
*
* Purpose:  Image Purge Service.
*           BLR - 01/29/2015 - XML CACHE FILES NO LONGER USED. 
*
* Modification History
* WI 104252 CRG 06/06/2013
*    Img Purge Svc Code: Create Purge XML class
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeAPI
{
    class cPurgeXMLFile : _purgeBase
    {

        public cPurgeXMLFile()
        {
        }

        /// <summary>
        /// Updates the cache files.
        /// </summary>
        /// <param name="ServiceOptions">The service options.</param>
        /// <returns></returns>
        public bool UpdateCacheFiles(cPurgeServiceOptions ServiceOptions)
        {
            DataTable dt = null;
            bool bReturnValue = true;
            string filename = string.Empty;
            cPurgeXML xmldata = null;
            try
            {
                using (cPurgeDAL objDAL = new cPurgeDAL(ServiceOptionsKey))
                {
                    if (objDAL.GetPurgeData(out dt))
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            xmldata = new cPurgeXML(Convert.ToInt32(dr["SiteBankID"]), Convert.ToInt32(dr["SiteClientAccountID"]));
                            bReturnValue = AddOrUpdateNode(xmldata, ServiceOptions);
                            if (bReturnValue == false)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        eventLog.logEvent(ServiceName + " cannot retrieve purge queue", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                        bReturnValue = false;
                    }
                }
            }
            catch (Exception e)
            {
                eventLog.logEvent(ServiceName + " cannot load purge options: " + e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }

        /// <summary>
        /// Gets the XML file.
        /// </summary>
        /// <param name="xmldoc">The xmldoc.</param>
        /// <param name="filename">The filename.</param>
        /// <returns></returns>
        private bool GetXMLFile(out XmlDocument xmldoc, string filename)
        {
            bool bReturnValue = false;
            XmlTextReader reader = null;
            XmlDocument doc = null;
            XmlNode docNode = null;
            XmlNode node = null;
            try
            {
                doc = new XmlDocument();
                if (!File.Exists(filename))
                {
                    docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                    doc.AppendChild(docNode);
                    node = doc.CreateElement("cache");
                    doc.AppendChild(node);
                    doc.Save(filename);
                }

                // read cache xml file
                reader = new XmlTextReader(filename);
                doc.Load(reader);
                reader.Close();
                xmldoc = doc;
                bReturnValue = true;
            }
            catch (Exception e)
            {
                eventLog.logEvent(e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
                xmldoc = null;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Dispose();
                }
            }

            return bReturnValue;
        }

        /// <summary>
        /// Gets the earliest date purged.
        /// </summary>
        /// <param name="xmldata">The xmldata.</param>
        /// <param name="ServiceOptions">The service options.</param>
        /// <returns></returns>
        public int GetEarliestDatePurged(cPurgeXML xmldata, cPurgeServiceOptions ServiceOptions)
        {
            int value = 0;
            string filename = Path.Combine(ServiceOptions.LocalCache, xmldata.BankID + ".xml");
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                if (GetXMLFile(out xmldoc, filename))
                {
                    XmlNode node;
                    XmlElement root = xmldoc.DocumentElement;
                    node = root.SelectSingleNode("//cache/folder[@SiteClientAccountID='" + xmldata.ClientAccountID + "']");
                    if (node == null)
                    {
                        value = 0;
                    }
                    else
                    {
                        if (node.Attributes["EarliestDatePurged"] == null || node.Attributes["EarliestDatePurged"].Value == "")
                        {
                            value = 0;
                        }
                        else
                        {
                            value = Convert.ToInt32(node.Attributes["EarliestDatePurged"].Value);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                eventLog.logEvent(e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                value = 0;
            }

            return value;
        }

        /// <summary>
        /// Adds the or update node.
        /// </summary>
        /// <param name="xmldata">The xmldata.</param>
        /// <param name="ServiceOptions">The service options.</param>
        public bool AddOrUpdateNode(cPurgeXML xmldata, cPurgeServiceOptions ServiceOptions)
        {
            bool bReturnValue = false;
            XmlDocument xmldoc = null;
            XmlNode oldNode = null;
            XmlElement root = null;
            XmlElement newNode = null;
            string filename = Path.Combine(ServiceOptions.LocalCache, xmldata.BankID + ".xml");
            try
            {
                xmldoc = new XmlDocument();
                if (GetXMLFile(out xmldoc, filename))
                {
                    root = xmldoc.DocumentElement;
                    oldNode = root.SelectSingleNode("//cache/folder[@SiteClientAccountID='" + xmldata.ClientAccountID + "']");
                    newNode = xmldoc.CreateElement("folder");
                    newNode.SetAttribute("SiteBankID", xmldata.BankID.ToString());
                    newNode.SetAttribute("SiteClientAccountID", xmldata.ClientAccountID.ToString());
                    if (oldNode == null)
                    {
                        newNode.SetAttribute("EarliestDatePurged", string.Empty);
                    }
                    else
                    {
                        if (oldNode.Attributes["EarliestDatePurged"].Value == string.Empty)
                        {
                            newNode.SetAttribute("EarliestDatePurged", xmldata.EarliestDatePurged.ToString());
                        }
                        else
                        {
                            if (Convert.ToInt32(oldNode.Attributes["EarliestDatePurged"].Value) < xmldata.EarliestDatePurged)
                            {
                                newNode.SetAttribute("EarliestDatePurged", xmldata.EarliestDatePurged.ToString());
                            }
                            else
                            {
                                newNode.SetAttribute("EarliestDatePurged", oldNode.Attributes["EarliestDatePurged"].Value);
                            }
                        }
                    }

                    if (oldNode == null)
                    {
                        xmldoc.DocumentElement.AppendChild(newNode);
                    }
                    else
                    {
                        xmldoc.DocumentElement.RemoveChild(oldNode);
                        xmldoc.DocumentElement.AppendChild(newNode);
                    }

                    xmldoc.Save(filename);
                    bReturnValue = true;
                }
                else
                {
                    eventLog.logEvent(ServiceName + " unable to retrieve xml file", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                    bReturnValue = false;
                }
            }
            catch (Exception e)
            {
                eventLog.logEvent(e.Message, e.Source, LTAMessageType.Error, LTAMessageImportance.Essential);
                bReturnValue = false;
            }

            return bReturnValue;
        }
    }
}
