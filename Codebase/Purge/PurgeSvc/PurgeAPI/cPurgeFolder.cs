﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin Glomb
* Date:     06/07/2013
*
* Purpose:  Image Purge Service.
*
* Modification History
* WI 104251 CRG 06/06/2013
*    Img Purge Svc Code: Create Purge Folder class
******************************************************************************/
namespace WFS.RecHub.FilePurge.FilePurgeAPI {
	/// <summary>
	/// 
	/// </summary>
	class cPurgeFolder {
		/// <summary>
		/// The _date of the folder
		/// </summary>
		private int _date;

		/// <summary>
		/// The _bank ID of the folder
		/// </summary>
		private int _bankID;

		/// <summary>
		/// The _client account ID of the folder
		/// </summary>
		private int _clientAccountID;

		/// <summary>
		/// The _folder path at the date level
		/// </summary>
		private string _folderName;

		/// <summary>
		/// Gets or sets the date.
		/// </summary>
		/// <value>
		/// The date.
		/// </value>
		public int LastPurgedDepositDate {
			get {
				return _date;
			}
			set {
				_date = value;
			}
		}

		/// <summary>
		/// Gets or sets the bank ID.
		/// </summary>
		/// <value>
		/// The bank ID.
		/// </value>
		public int BankID {
			get {
				return _bankID;
			}
			set {
				_bankID = value;
			}
		}

		/// <summary>
		/// Gets or sets the client account ID.
		/// </summary>
		/// <value>
		/// The client account ID.
		/// </value>
		public int ClientAccountID {
			get {
				return _clientAccountID;
			}
			set {
				_clientAccountID = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the folder.
		/// </summary>
		/// <value>
		/// The name of the folder.
		/// </value>
		public string FolderName {
			get {
				return _folderName;
			}
			set {
				_folderName = value;
			}
		}
	}
}