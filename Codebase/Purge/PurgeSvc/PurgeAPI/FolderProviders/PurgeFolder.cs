﻿using System;
using System.Globalization;
using WFS.RecHub.FilePurge.QueueProviders;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  To provide a simple model class to contain the data returned
*           from the parsers and the providers. Used throughout the purge
*           process. This replaces cPurgeFolder.
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.FolderProviders
{
    public class PurgeFolder
    {
        public const string IMMUTABLE_DATE_FORMAT = "yyyyMMdd";

        public PurgeFolder()
        {
            Purged = false;
        }

        private string _immutabledatestring;
        public string ImmutableDateString 
        {
            get { return _immutabledatestring; }
            set
            {
                _immutabledatestring = value;
                DateTime temp;
                if (DateTime.TryParseExact(value, IMMUTABLE_DATE_FORMAT, CultureInfo.InvariantCulture, DateTimeStyles.None, out temp))
                    ImmutableDate = temp;
            }
        }

        private string _bankidstring;
        public string BankIDString 
        {
            get { return _bankidstring; }
            set
            {
                _bankidstring = value;
                int temp;
                if (int.TryParse(value, out temp))
                    BankID = temp;
            }
        }

        private string _clientaccountidstring;
        public string ClientAccountIDString 
        {
            get { return _clientaccountidstring; }
            set
            {
                _clientaccountidstring = value;
                int temp;
                if (int.TryParse(value, out temp))
                    ClientAccountID = temp;
            }
        }

        public string FileGroup
        {
            get
            {
                return Queue == null
                    ? null
                    : Queue.FileGroup;
            }
        }

        public string RelativePath { get; set; }
        public DateTime ImmutableDate { get; private set; }
        public string PaymentSource { get; set; }
        public DateTime RetentionExpirationDate { get; set; }
        public int BankID { get; private set; }
        public int ClientAccountID { get; private set; }
        public string FullPathName { get; set; }
        public PurgeQueue Queue { get; set; }
        public bool Purged { get; set; }
    }
}
