﻿
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  To provide some base functionality for the parsers.
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.FolderProviders
{
    public class BaseFolderParser : IFolderParser
    {
        public BaseFolderParser() { }

        /// <summary>
        /// This base class attempts to parse simply the batch source name, if it exists.
        /// This is currently a shared component of both the PICS and the FILEGROUP storage
        /// structures.
        /// </summary>
        /// <param name="relativefolder"></param>
        /// <returns></returns>
        public virtual PurgeFolder ParseFolder(string relativefolder)
        {
            var output = new PurgeFolder();
            output.RelativePath = relativefolder;

            // Grab the first folder.
            if (relativefolder.StartsWith(@"\") && relativefolder.Length > 1)
               relativefolder = relativefolder.Substring(1);

            var index = relativefolder.IndexOf('\\');
            if (index < 0)
                return output;

            // Attempt to parse the directories name. This will tell us if there is a batch source name.
            // If we cannot parse an int, it's not an immutable date key (PICS) or a bank id (FILEGROUP).
            var path = relativefolder.Substring(0, index);
            int value;
            if (!int.TryParse(path, out value))
                output.PaymentSource = path;

            return output;
        }
    }
}