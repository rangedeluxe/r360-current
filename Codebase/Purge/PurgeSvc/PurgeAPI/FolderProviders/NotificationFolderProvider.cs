﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  To provide functionality to provide folder paths of the 
*           notification 'CENDS' folders. 
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.FolderProviders
{
    public class NotificationFolderProvider : BaseFolderProvider
    {
        public NotificationFolderProvider() 
            : base(new NotificationFolderParser())
        {
            
        }

        public override IEnumerable<PurgeFolder> GetPurgeFolders(string root)
        {
            // Override the base.  We don't need the child folders for notifications,
            // just the immutable date key.
            var allfolders = Directory.GetDirectories(root);
            return allfolders
                .Select(x => Parser.ParseFolder(x))
                .Where(x => x.ImmutableDate > DateTime.MinValue);
        }
    }
}
