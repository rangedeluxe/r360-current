﻿using System.Collections.Generic;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  To provide some base functionality for the folder providers.
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.FolderProviders
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BaseFolderProvider : IFolderProvider
    {
        protected IFolderParser Parser
        {
            get;
            private set;
        }

        public BaseFolderProvider(IFolderParser parser)
        {
            Parser = parser;
        }

        /// <summary>
        /// Returns all of the parsed purge folders from the root directory. 
        /// The IFolderParser (constructor) is in charge of parsing the folder paths.
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public virtual IEnumerable<PurgeFolder> GetPurgeFolders(string root)
        {
            // This base method just grabs all child folders recursively.
            var output = new List<PurgeFolder>();
            if (!Directory.Exists(root))
                return output;

            GetPurgeFolders(output, root, root);
                
            return output;
        }

        private void GetPurgeFolders(List<PurgeFolder> folders, string root, string current)
        {
            var rootdir = new DirectoryInfo(current);
            var children = rootdir.GetDirectories();
            foreach (var child in children)
            {
                var relativepath = child.FullName.Substring(root.Length);
                if (!string.IsNullOrWhiteSpace(relativepath))
                {
                    var folder = Parser.ParseFolder(relativepath);
                    // Seems Path.Combine goes crazy when starting your right-argument with a '\'.
                    if (relativepath.StartsWith(@"\"))
                        relativepath = relativepath.Substring(1);
                    folder.FullPathName = Path.Combine(root, relativepath);
                    folders.Add(folder);
                }
                    
                GetPurgeFolders(folders, root, child.FullName);
            }
        }
    }
}