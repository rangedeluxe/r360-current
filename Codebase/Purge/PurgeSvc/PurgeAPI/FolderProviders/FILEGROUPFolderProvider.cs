﻿using System;
using System.Collections.Generic;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Brandon Resheske
* Date:     07/22/2014
*
* Purpose:  To provide functionality to provide folder paths of the FILEGROUP
*           storage structure. 
*
* Modification History
* WI 155206 BLR 07/22/2014
*    - Initial Version
******************************************************************************/

namespace WFS.RecHub.FilePurge.FolderProviders
{
    public class FILEGROUPFolderProvider : BaseFolderProvider
    {
        public FILEGROUPFolderProvider() 
            : base(new FILEGROUPFolderParser())
        {

        }

        public override IEnumerable<PurgeFolder> GetPurgeFolders(string root)
        {
            var allfolders = base.GetPurgeFolders(root);
            return allfolders
                .Where(x => x.ImmutableDate > DateTime.MinValue);
        }
    }
}
