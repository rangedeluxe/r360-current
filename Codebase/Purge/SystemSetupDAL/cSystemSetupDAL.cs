﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin R Glomb
* Date:     05/21/2013
*
* Purpose: Provide data access to the system setup table
*
* Modification History
* WI 102413 CRG 05/21/2013
*	Purge Development - Admin Data Access layer for System Setup DAL
* WI 109747 EAS 07/30/2013
*   DAL components called with RecHubAdmin Connection Type 
******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// System Setup Data Access Layer class
	/// </summary>
	public class cSystemSetupDAL : _DALBase {

		/// <summary>
		/// Initializes a new instance of the <see cref="cSystemSetupDAL"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
		public cSystemSetupDAL(string vSiteKey)
			: base(vSiteKey) {
		}

        public cSystemSetupDAL(string vSiteKey, ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }

		/// <summary>
		/// Gets the system setup by section.
		/// </summary>
		/// <param name="Section">The section.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Result of database store procedure call
		/// </returns>
		public bool GetSystemSetupBySection(string Section, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSection", SqlDbType.VarChar, Section, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubConfig.usp_SystemSetup_Get_BySection", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetSystemSetupBySection(out DataTable dt)");
			}

			return bRetVal;
		}
	}
}