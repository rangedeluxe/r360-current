﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using WFS.RecHub.Common;
using WFS.RecHub.DTO;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Calvin R Glomb
* Date:     05/29/2013
*
* Purpose: Provide data access to the purge data
*
* Modification History
* WI 103542 CRG 05/29/2013
*	Img Purge Svc Code: Create Purge DAL
* WI 104062 CRG 06/07/2013
*	Img Purge Svc Code: Create PurgeHistory DAL
* WI 113839 TWE 09/11/2013
*   change rename parm name for RecHubSystem.usp_PurgeHistory_Ins
* WI 155630 BLR 07/24/2014
*    - Added an audit event call for a purge completion. 
******************************************************************************/
namespace WFS.RecHub.DAL.FilePurge
{

    /// <summary>
    /// Purge Data Access Layer class
    /// </summary>
    public class cPurgeDAL : _DALBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="cPurgeDAL"/> class.
        /// </summary>
        /// <param name="vSiteKey">The v site key.</param>
        public cPurgeDAL(string vSiteKey)
            : base(vSiteKey)
        {
            EventLog.logEvent(string.Format("Loaded new cPurgeDAL with siteKey : {0}", vSiteKey), MessageImportance.Debug);
        }

        /// <summary>
        /// Gets the purge data.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetPurgeData(out DataTable dt)
        {
            bool bRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPurgeType", SqlDbType.Int, 2, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRetVal = Database.executeProcedure("RecHubSystem.usp_PurgeQueue_Ins", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetPurgeData(out DataTable dt)");
            }

            return bRetVal;
        }

        /// <summary>
        /// Inserts the purge history.
        /// </summary>
        /// <param name="history">The history.</param>
        /// <returns></returns>
        public bool InsertPurgeHistory(cPurgeHistory history)
        {
            bool bRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPurgeHistoryType", SqlDbType.Int, history.Type, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBankKey", SqlDbType.Int, history.BankKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientAccountKey", SqlDbType.Int, history.ClientAccountKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, history.DataDepositDateKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmProcessingDateKey", SqlDbType.Int, history.DepositDateKey, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRetVal = Database.executeProcedure("RecHubSystem.usp_PurgeHistory_Ins", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertPurgeHistory(cPurgeHistory history)");
            }

            return bRetVal;
        }

        public bool WriteAuditEvent(string servicename, int numpurged, bool writehistorymessage)
        {
            bool bRetVal = false;
            List<SqlParameter> arParms = new List<SqlParameter>();

            try
            {
                var historymessage = writehistorymessage 
                    ? " View purge history table for more details." 
                    : string.Empty;
                var message = string.Format("{0} Completed on {1}, Folders Purged: {2}.{3}", servicename, DateTime.Now, numpurged, historymessage);
                arParms.Add(BuildParameter("@parmApplicationName", SqlDbType.VarChar, "PurgeService", ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, "Purge Completed", ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmAuditMessage", SqlDbType.VarChar, message, ParameterDirection.Input));

                SqlParameter[] parms = arParms.ToArray();

                bRetVal = Database.executeProcedure("RecHubCommon.usp_WFS_EventAudit_Ins", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "WriteAuditEvent(string servicename, int numpurged)");
            }
            return bRetVal;
        }

        public bool WriteSystemLevelEventLog(
            string eventName,
            string message
        )
        {
            SqlParameter[] objParms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubAlert.usp_EventLog_SystemLevel_Ins";

            var bolRetVal = true;

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, eventName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmMessage", SqlDbType.VarChar, message, ParameterDirection.Input));
                objParms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, objParms);
            }
            catch (Exception ex)
            {
                EventLog.logWarning(ex.Message, MessageImportance.Essential);
                EventLog.logWarning("Unable to execute procedure: " + PROCNAME, MessageImportance.Essential);
                bolRetVal = false;
            }

            return (bolRetVal);

        }
    }
}