﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Purge History 
*  Filename: cPurgeHistory.cs
*    Author: Calvin Glomb
*
* Revisions:
* ----------------------
* WI 104066 CRG 06/07/2013
*    Img Purge Svc Code: Create PurgeHistory DTO
******************************************************************************/
namespace WFS.RecHub.DTO {
	/// <summary>
	/// 
	/// </summary>
	public class cPurgeHistory {
		/// <summary>
		/// Initializes a new instance of the <see cref="cPurgeHistory"/> class.
		/// </summary>
		public cPurgeHistory() { }

		/// <summary>
		/// Gets or sets the ID.
		/// </summary>
		/// <value>
		/// The ID.
		/// </value>
		public int ID { get; set; }

		/// <summary>
		/// Gets or sets the type.
		/// </summary>
		/// <value>
		/// The type.
		/// </value>
		public int Type { get; set; }

		/// <summary>
		/// Gets or sets the bank key.
		/// </summary>
		/// <value>
		/// The bank key.
		/// </value>
		public int BankKey { get; set; }

		/// <summary>
		/// Gets or sets the client account key.
		/// </summary>
		/// <value>
		/// The client account key.
		/// </value>
		public int ClientAccountKey { get; set; }

		/// <summary>
		/// Gets or sets the data deposit date key.
		/// </summary>
		/// <value>
		/// The data deposit date key.
		/// </value>
		public int DataDepositDateKey { get; set; }

		/// <summary>
		/// Gets or sets the deposit date key.
		/// </summary>
		/// <value>
		/// The deposit date key.
		/// </value>
		public int DepositDateKey { get; set; }

		/// <summary>
		/// Gets or sets the batch ID.
		/// </summary>
		/// <value>
		/// The batch ID.
		/// </value>
		public int BatchID { get; set; }

		/// <summary>
		/// Gets or sets the batch number.
		/// </summary>
		/// <value>
		/// The batch number.
		/// </value>
		public int BatchNumber { get; set; }
	}
}

