﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Purge Queue 
*  Filename: cPurgeQueue.cs
*    Author: Calvin Glomb
*
* Revisions:
* ----------------------
* WI 103544 CRG 05/30/2013
*    Img Purge Svc Code: Create Purge DTO
******************************************************************************/
namespace WFS.RecHub.DTO {
	/// <summary>
	/// 
	/// </summary>
	public class cPurgeQueue {
		/// <summary>
		/// Initializes a new instance of the <see cref="cPurgeQueue"/> class.
		/// </summary>
		public cPurgeQueue() { }

		/// <summary>
		/// Gets or sets the ID.
		/// </summary>
		/// <value>
		/// The ID.
		/// </value>
		public int ID { get; set; }

		/// <summary>
		/// Gets or sets the bank key.
		/// </summary>
		/// <value>
		/// The bank key.
		/// </value>
		public int BankKey { get; set; }

		/// <summary>
		/// Gets or sets the client account.
		/// </summary>
		/// <value>
		/// The client account.
		/// </value>
		public int ClientAccountKey { get; set; }

		/// <summary>
		/// Gets or sets the site bank ID.
		/// </summary>
		/// <value>
		/// The site bank ID.
		/// </value>
		public int SiteBankID { get; set; }

		/// <summary>
		/// Gets or sets the site client account.
		/// </summary>
		/// <value>
		/// The site client account.
		/// </value>
		public int SiteClientAccountID { get; set; }

		/// <summary>
		/// Gets or sets the data deposit date key.
		/// </summary>
		/// <value>
		/// The data deposit date key.
		/// </value>
		public int DataDepositDateKey { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [data purged].
		/// </summary>
		/// <value>
		///   <c>true</c> if [data purged]; otherwise, <c>false</c>.
		/// </value>
		public bool DataPurged { get; set; }
	}
}
