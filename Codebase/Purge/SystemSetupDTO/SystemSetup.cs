﻿using System;
using System.Data;

/*********************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Calvin Glomb
* Date:     05/21/2013
*
* Purpose:  System Setup DTO.
*
* Modification History
* WI 102414 CRG 05/21/2013
*	Purge Development - Admin System Setup Service layer
*********************************************************************************/
namespace WFS.RecHub.DTO {
	/// <summary>
	/// 
	/// </summary>
	public class SystemSetup {

		/// <summary>
		/// The _systemsetupvalues
		/// </summary>
		private cSystemSetupValues _systemsetupvalues = null;

		// may need this for the purge service
		//private IList<cSystemSetupValue> _ILsystemsetupvalues = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="SystemSetup"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
		public SystemSetup(string vSiteKey) {
			_systemsetupvalues = new cSystemSetupValues(vSiteKey);
		}

		/// <summary>
		/// Gets the values.
		/// </summary>
		/// <returns></returns>
		public cSystemSetupValues GetValues() {
			return (_systemsetupvalues);
		}

		/// <summary>
		/// Determines the value.
		/// </summary>
		/// <param name="dr">The dr.</param>
		/// <returns></returns>
		private string DetermineValue(DataRow dr) {
			string strEffectiveSetting = string.Empty;
			if(dr.Table.Columns.Contains("Value") && dr["Value"].ToString().Length > 0) {
				strEffectiveSetting = dr["Value"].ToString();
			} else {
				strEffectiveSetting = String.Empty;
			}

			return (strEffectiveSetting);
		}

		/// <summary>
		/// Processes the system setup.
		/// </summary>
		/// <param name="dtSystemSetup">The dt system setup.</param>
		public void ProcessSystemSetupForPurge(DataTable dtSystemSetup) {
			int intTemp;
			foreach(DataRow dr in dtSystemSetup.Rows) {
				if(!dr.IsNull("SetupKey")) {
					switch(dr["SetupKey"].ToString().ToLower()) {
						case "defaultdataretentiondays":
							if(int.TryParse(this.DetermineValue(dr), out intTemp)) {
								_systemsetupvalues.DefaultDataRetentionDays = intTemp;
							}
							break;

						case "defaultimageretentiondays":
							if(int.TryParse(this.DetermineValue(dr), out intTemp)) {
								_systemsetupvalues.DefaultImageRetentionDays = intTemp;
							}
							break;
						case "maxretentiondays":
							if(int.TryParse(this.DetermineValue(dr), out intTemp)) {
								_systemsetupvalues.MaxRetentionDays = intTemp;
							}
							break;
						case "notificationsretentiondays":
							if(int.TryParse(this.DetermineValue(dr), out intTemp)) {
								_systemsetupvalues.NotificationsRetentionDays = intTemp;
							}
							break;
						case "purgehistoryretentiondays":
							if(int.TryParse(this.DetermineValue(dr), out intTemp)) {
								_systemsetupvalues.PurgeHistoryRetentionDays = intTemp;
							}
							break;
						case "queueretentiondays":
							if(int.TryParse(this.DetermineValue(dr), out intTemp)) {
								_systemsetupvalues.QueueRetentionDays = intTemp;
							}
							break;
					}
				}
			}
		}
	}
}
