﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: System Setup 
*  Filename: cSystemSetupValue.cs
*    Author: Calvin Glomb
*
* Revisions:
* ----------------------
* WI 102414 CRG 05/21/2013
*    Purge Development - Admin System Setup Service layer
******************************************************************************/
namespace WFS.RecHub.DTO
{
	/// <summary>
	/// 
	/// </summary>
    public class cSystemSetupValue
    {
		/// <summary>
		/// Initializes a new instance of the <see cref="cSystemSetupValue"/> class.
		/// </summary>
        public cSystemSetupValue(){}

		/// <summary>
		/// Gets or sets the section.
		/// </summary>
		/// <value>
		/// The section.
		/// </value>
	    public string Section{ get; set;}

		/// <summary>
		/// Gets or sets the setup key.
		/// </summary>
		/// <value>
		/// The setup key.
		/// </value>
		public string SetupKey { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>
		/// The value.
		/// </value>
		public string Value { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
        public string Description{ get ; set ;}

		/// <summary>
		/// Gets or sets the type of the data.
		/// </summary>
		/// <value>
		/// The type of the data.
		/// </value>
        public string DataType{ get ; set ;}

		/// <summary>
		/// Gets or sets the valid values.
		/// </summary>
		/// <value>
		/// The valid values.
		/// </value>
        public string ValidValues{ get ; set ;}
    }
}
