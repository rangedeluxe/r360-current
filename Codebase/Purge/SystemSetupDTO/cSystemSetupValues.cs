/*********************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Calvin Glomb	
* Date:     05/21/2013
*
* Purpose:  System Setup Values
*
* Modification History
* 05/21/2013 WI 102414 CRG 
*   Purge Development - Admin System Setup Service layer
*********************************************************************************/
namespace WFS.RecHub.DTO {
	/// <summary>
	/// 
	/// </summary>
	public class cSystemSetupValues {
		private int _DefaultDataRetentionDays;
		private int _DefaultImageRetentionDays;
		private int _MaxRetentionDays;
		private int _NotificationsRetentionDays;
		private int _PurgeHistoryRetentionDays;
		private int _QueueRetentionDays;
		private string _SiteKey = "";

		/// <summary>
		/// Initializes a new instance of the <see cref="cSystemSetupValues"/> class.
		/// </summary>
		/// <param name="siteKeyStr">The site key STR.</param>
		public cSystemSetupValues(string siteKeyStr) {
			_SiteKey = siteKeyStr;
		}

		/// <summary>
		/// Gets the site key.
		/// </summary>
		/// <value>
		/// The site key.
		/// </value>
		public string siteKey {
			get {
				return (_SiteKey);
			}
		}

		/// <summary>
		/// Gets or sets the default data retention days.
		/// </summary>
		/// <value>
		/// The default data retention days.
		/// </value>
		public int DefaultDataRetentionDays {
			get {
				return _DefaultDataRetentionDays;
			}
			set {
				_DefaultDataRetentionDays = value;
			}
		}

		/// <summary>
		/// Gets or sets the default image retention days.
		/// </summary>
		/// <value>
		/// The default image retention days.
		/// </value>
		public int DefaultImageRetentionDays {
			set {
				_DefaultImageRetentionDays = value;
			}
			get {
				return (_DefaultImageRetentionDays);
			}
		}

		/// <summary>
		/// Gets or sets the max retention days.
		/// </summary>
		/// <value>
		/// The max retention days.
		/// </value>
		public int MaxRetentionDays {
			set {
				_MaxRetentionDays = value;
			}
			get {
				return (_MaxRetentionDays);
			}
		}

		/// <summary>
		/// Gets or sets the notifications retention days.
		/// </summary>
		/// <value>
		/// The notifications retention days.
		/// </value>
		public int NotificationsRetentionDays {
			set {
				_NotificationsRetentionDays = value;
			}
			get {
				return (_NotificationsRetentionDays);
			}
		}

		/// <summary>
		/// Gets or sets the purge history retention days.
		/// </summary>
		/// <value>
		/// The purge history retention days.
		/// </value>
		public int PurgeHistoryRetentionDays {
			set {
				_PurgeHistoryRetentionDays = value;
			}
			get {
				return (_PurgeHistoryRetentionDays);
			}
		}

		/// <summary>
		/// Gets or sets the queue retention days.
		/// </summary>
		/// <value>
		/// The queue retention days.
		/// </value>
		public int QueueRetentionDays {
			set {
				_QueueRetentionDays = value;
			}
			get {
				return (_QueueRetentionDays);
			}
		}
	}
}
