delete from [Language].[Translations] where [PropertyId] IN (select [PropertyId] from [Language].[Property] where [Application] = 'ExceptionViews' and [PropertyGroup] = 'ExceptionSummary')
delete from [Language].[Property] where [Application] = 'ExceptionViews' and [PropertyGroup] = 'ExceptionSummary'

delete from [Language].[Translations] where [PropertyId] IN (select [PropertyId] from [Language].[Property] where [Application] = 'ExceptionViews' and [PropertyGroup] = 'ExceptionType')
delete from [Language].[Property] where [Application] = 'ExceptionViews' and [PropertyGroup] = 'ExceptionType'

delete from [Language].[Translations] where [PropertyId] IN (select [PropertyId] from [Language].[Property] where [Application] = 'ExceptionViews' and [PropertyGroup] = 'ExceptionStatus')
delete from [Language].[Property] where [Application] = 'ExceptionViews' and [PropertyGroup] = 'ExceptionStatus'

declare @recordId bigint

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Workgroup', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Workgroup', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Title', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Receivables Exceptions Summary', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('BatchId', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Batch Id', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('TransactionId', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Transaction Id', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Deadline', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Deadline', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('AmountDollar', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Amount ($)', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ViewLockedDetails', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'View Locked Details', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ItemCount', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Item Count:', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('LockedBy', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Locked by', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('At', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'At', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Unlock', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unlock', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('UnlockSuccess', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unlock successfull!', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('UnlockFail', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unlock failed!', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('0', 'ExceptionViews', 'ExceptionType')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'In Process', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('1', 'ExceptionViews', 'ExceptionType')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Post Process', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('0', 'ExceptionViews', 'ExceptionStatus')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unresolved', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('1', 'ExceptionViews', 'ExceptionStatus')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Pending', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('UnableToRetrieve', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unable to retrieve data.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('LockDetailTitle', 'ExceptionViews', 'ExceptionSummary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Exception Transaction Locked', @recordId, NULL)

