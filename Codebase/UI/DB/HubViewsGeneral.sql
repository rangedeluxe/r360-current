delete from [Language].[Translations] where [PropertyId] IN (select [PropertyId] from [Language].[Property] where [Application] = 'HubViews' and [PropertyGroup] = 'General')
delete from [Language].[Property] where [Application] = 'HubViews' and [PropertyGroup] = 'General'

declare @recordId bigint

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('DepositDate', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Deposit Date', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Print', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Print', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('AccountId', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Workgroup', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PaymentSource', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Payment Source', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PmtSource', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Pmt Source', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PaymentType', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Payment Type', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PmtType', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Pmt Type', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('BatchCount', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Batch Count', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('TransactionCount', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Transaction Count', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Total', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Total ($)', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('SelectGrouping', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Select Grouping...', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ViewDetails', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'View Details', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PageRefresh', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Refresh the page.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NoData', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'No data available.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Status', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Status', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('UnableToRetrieve', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unable to retrieve data.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ProcessingDate', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Processing Date', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Summary', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Summary', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Detail', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Detail', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Entity', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Entity', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('AccountNumbers', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Account Numbers', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('InternalServerError', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Internal server error.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('AmountDollar', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Amount ($)', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PaymentId', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Payment Id', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ExceptionId', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Exception Id', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NotAvailable', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Not Available', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('UserLoginEntity', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'User Login Entity', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('WorkgroupEntity', 'HubViews', 'General')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Workgroup Entity', @recordId, NULL)

