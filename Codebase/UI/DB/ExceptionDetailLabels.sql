delete from [Language].[Translations] where [PropertyId] IN (select [PropertyId] from [Language].[Property] where [Application] = 'ExceptionViews' and [PropertyGroup] = 'ExceptionDetail')
delete from [Language].[Property] where [Application] = 'ExceptionViews' and [PropertyGroup] = 'ExceptionDetail'

declare @recordId bigint

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Title', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Exception Detail', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('SourceType', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Source/Type', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ShowAllItems', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Show All Items', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ShowExceptionsOnly', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Show exceptions items only', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ShowAllFields', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Show All Fields', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ShowDecisionFieldsOnly', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Show decision fields only', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Image90r', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Rotate clockwise 90�', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Image90l', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Rotate counter clockwise 90�', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Image180', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Rotate 180�', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ImageBS', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'View backside if available.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ImageZoom', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Zoom In', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ImageFW', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Fit to width', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ImgageBW', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Toggle Black/White if available.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Accept', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Accept', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Reject', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Reject', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Skip', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Skip', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('AddNewItem', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Add new item', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('First', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'First image', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Next', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Next image', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Previous', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Previous image', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Last', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Last image', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ItemStatus', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Item status', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ItemInformation', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Item information', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ItemDelete', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Item delete', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('InfoTitle', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Payment Information', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NotesTitle', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Payment Item Notes', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PromptDeleteItem', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Delete Item?', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Confirm', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Confirm', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('BankId', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Bank Id', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('BatchId', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Batch Id', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('TransactionId', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Transaction Id', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('TransactionInformation', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Transaction Information', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('RejectFail', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Reject transaction failed.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('EndOfTransactions', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Selected group completed.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PromptRejectPaument', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Reject this payment?', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('AcceptFail', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Accept transaction failed.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Required', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'A value is required.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('MaxLength', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'The value exceeds max length of:', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NotFloat', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'The value is not a valid float.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NotCurrency', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'The value is not a valid currency.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NotDate', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'The value is not a valid date.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PromptRejectPayment', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Reject the payment?', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('BalancingError', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Transaction Out of Balance', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PromptBalancingDesiredIP', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'This Transaction is out of balance.  Do you want to continue with auto balancing steps?', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PromptBalancingDesiredPP', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'This Transaction is out of balance.  Do you want to accept without balancing?', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PromptBalancingRequired', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'This Transaction is out of balance.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PerformBalancing', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Press ''Yes'' to perform balancing on this Transaction.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('SkipBalancing', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Press ''No'' to continue without balancing.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ForceBalancing', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Press ''Yes'' to create a new balancing row.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('CancelBalancing', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Press ''No'' to return to the Transaction and correct the out of balance condition.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Exceptions', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Exceptions', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('RelatedItems', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Related Items', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PaymentItems', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Payment Items', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('GridLabelTotal', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Total ($)', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('UnlockFail', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unexpected error occurred when unlocking the exception transaction, please contact administrator.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('LockFail', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unexpected error occurred when locking the exception transaction, please contact administrator.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('LoadMessagesFailed', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unexpected error occurred when loading diplay contents, please contact administrator.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ViewTransactionFailed', 'ExceptionViews', 'ExceptionDetail')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unexpected error occurred when loading the transaction for viewing, please contact administrator.', @recordId, NULL)

