delete from [Language].[Translations] where [PropertyId] IN (select [PropertyId] from [Language].[Property] where [Application] = 'HubViews' and [PropertyGroup] = 'Dashboard')
delete from [Language].[Property] where [Application] = 'HubViews' and [PropertyGroup] = 'Dashboard'

declare @recordId bigint

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Title', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Dashboard', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Apply', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Apply', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('AllAccounts', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'All Accounts...', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('TotalsByPaymentType', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Totals by Payment Type', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('CountsByPaymentType', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Counts by Payment Type', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Exceptions', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Exceptions', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Resolved', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Resolved', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Unresolved', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Unresolved', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('TotalItems', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Total Transactions - ', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('TotalAmount', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Total Amount $', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Items', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, ' items - $', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NoAcounts', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'No accounts available.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NoUser', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'User profile could not be retrieved.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NoAccounts', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Accounts could not be retrieved.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NoAccountsAssigned', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'There are no Accounts in the system for you to view.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NoSummaryData', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Summary data could not be retrieved.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('NoExceptionData', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Exception data could not be retrieved.', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('PaymentTypeCountsAndAmounts', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Summary Counts and Amounts', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('ExceptionStatusByType', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Exception Status by Type', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('WorkgroupSelector', 'HubViews', 'Dashboard')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Entity/Workgroup', @recordId, NULL)

