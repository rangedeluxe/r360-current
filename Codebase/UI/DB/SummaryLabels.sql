delete from [Language].[Translations] where [PropertyId] IN (select [PropertyId] from [Language].[Property] where [Application] = 'HubViews' and [PropertyGroup] = 'Summary')
delete from [Language].[Property] where [Application] = 'HubViews' and [PropertyGroup] = 'Summary'

declare @recordId bigint

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('Title', 'HubViews', 'Summary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, 'Receivables Summary', @recordId, NULL)

insert into [Language].[Property] ([PropertyKey], [Application], [PropertyGroup]) values ('SubTitle', 'HubViews', 'Summary')
select @recordId = scope_identity()
insert into [Language].[Translations] ([LanguageId], [Value], [PropertyId], [Organization]) values (1, '(Showing All Entities/Workgroups)', @recordId, NULL)