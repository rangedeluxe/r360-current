param(
    [string]$folderName,      #Folder name (web virtual directory)
	[string]$webSite,         #If blank, this will default to the folderName

    [string]$sourcePath, #Used for testing only - leave blank in final scripts
    [string]$ilmergeExe  #Used for testing only - leave blank in final scripts
    )

#This Script performs standard deployment of the Build for an R360 UI Views project
Write-Host “Deploying R360 2.1 UI Views Build To QA Pending 2.1\<Current> Folder"
Write-Host "Script: " $MyInvocation.MyCommand.Path
$scriptPath = Split-Path -Parent -Path $MyInvocation.MyCommand.Path

#Source path is determined relative to where the script runs from... Which means this only works from a standard TFS Build setup...
$sourcePathRootDir = Join-Path $scriptPath "..\..\..\..\Binaries\"
$sourcePathRootDir = [System.IO.Path]::GetFullPath($sourcePathRootDir)

IF ($sourcePath -ne "")
{
    # Source Path has been overridden... Probably for Testing...
    $sourcePathRootDir = $sourcePath
}

Write-Host "Using Source Path Folder: " $sourcePathRootDir
$sourceFiles = Join-Path $sourcePathRootDir "*"

#Target path is hardcoded to the version (2.01)
$targetPathRootDir = "\\172.27.2.160\ProductDevelopment\Legacy\QA Pending\RecHub_Build_Location\2014\2.01\"
Write-Host "Copying To:          " $targetPathRootDir

#Current Build is a standard sub-folder
$targetPathCurrentDir = Join-Path $targetPathRootDir "Current_Build"
Write-Host "Target Current:      " $targetPathCurrentDir

#Also create a folder to hold this build - for now, just generate a unique build number (in the future, build number should be passed in...)
$today = Get-Date -format "yy.MM.dd"
$targetPathBuildNbrDir = Join-Path $targetPathRootDir ("Bld_" + $today + ".")
$bldNum = 1
While($true) {
	$targetPathBldNbrTest = $targetPathBuildNbrDir + $bldNum
	If (Test-Path $targetPathBldNbrTest) {
		$bldNum++; 
	}
	else {
		break;
	}
}

$targetPathBuildNbrDir = $targetPathBldNbrTest
Write-Host "Target Build Number: " $targetPathBuildNbrDir

$outputDirs = @($targetPathBuildNbrDir, $targetPathCurrentDir)

#Verify the caller specified the folder to use for the source / output
IF ($folderName -eq "")
{
    throw New-Object System.ArgumentException("[folderName] label was not specified", "folderName")
}
Write-Host "Folder: " $folderName

IF ($webSite -eq "")
{
	$webSite = $folderName
}

#Setup names / paths for configuration files
$webConfigName = "web.config.oem"

# Source is _PublishedWebsites\<FolderName>
$configSourceDir = Join-Path (Join-Path $sourcePathRootDir "_PublishedWebsites") $foldername
$configSourceFiles = Join-Path $configSourceDir "*"
$binSourceDir = Join-Path $configSourceDir "bin"
$binSourceFiles = Join-Path $binSourceDir "*"
$assetsSourceDir = Join-Path $configSourceDir "Assets"
$assetsSourceFiles = Join-Path $assetsSourceDir "*"
$viewsSourceDir = Join-Path $configSourceDir "Views"
$viewsSourceFiles = Join-Path $viewsSourceDir "*"

# Perform web application deployment?
Write-Host "Deploying Web Application..."
foreach ($outputDir in $outputDirs)
{
    # Use wwwroot\R360Services\<Folder Name> for web application output
    $targetDir = Join-Path (Join-Path (Join-Path $outputDir ("RecHub.UI_Views." + $folderName)) "wwwroot") $folderName
    Write-Host "Copying To:          " $targetDir
    New-Item -ItemType Directory -Force -Path $targetDir > $null  #Create directory before trying to copy files in...

    # web.config and Global.asax
    $configTarget = Join-Path $targetDir $webConfigName
    Copy-Item -path $configSourceFiles -destination $configTarget -include "web.config"

    Copy-Item -path $configSourceFiles -destination $targetDir -include "Global.asax"
        
    # bin
    $binTargetDir = Join-Path $targetDir "bin\"
    New-Item -ItemType Directory -Force -Path $binTargetDir > $null  #Create directory before trying to copy files in...
    Copy-Item -path $binSourceFiles -destination $binTargetDir -include "*.dll"

	# Assets
	IF (Test-Path $assetsSourceDir) {
	  $assetsTargetDir = Join-Path $targetDir "Assets\"
	  New-Item -ItemType Directory -Force -Path $assetsTargetDir > $null  #Create directory before trying to copy files in...
	  Copy-Item -path $assetsSourceFiles -destination $assetsTargetDir -Recurse -Force
	}
	
	# Views        
    $viewsTargetDir = Join-Path $targetDir "Views\"
    New-Item -ItemType Directory -Force -Path $viewsTargetDir > $null  #Create directory before trying to copy files in...
    Copy-Item -path $viewsSourceFiles -destination $viewsTargetDir -Recurse -Force
}
