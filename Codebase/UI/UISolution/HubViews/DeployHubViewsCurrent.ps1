#Shell to Common R360UIViews Deployment Script, with correct parameters
$thisScript = Split-Path $MyInvocation.MyCommand.Path 
$commonScript = Join-Path $thisScript "..\UIViewsDeployment.ps1"
$commonScript = [System.IO.Path]::GetFullPath($commonScript)

$commandWithParameters = "$commonScript" `
    + " -folderName:HubViews" `

Write-Host "Invoking Deployment Script with parameters: " $commandWithParameters

invoke-expression $commandWithParameters

Write-Host "Deployment Complete"
