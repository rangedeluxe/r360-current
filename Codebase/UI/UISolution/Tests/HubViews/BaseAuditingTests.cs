﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360WebShared.Services;

namespace Tests.HubViews
{
    [TestClass]
    public class BaseAuditingTests
    {
        private List<KeyValuePair<string, string>> _values;
        private IAuditFilter _filter;

        [TestInitialize]
        public void Initialize()
        {
            // Arrange (all test cases)

            _values = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("ID", "1"),
                new KeyValuePair<string, string>("EventRuleID", "2"),
                new KeyValuePair<string, string>("EventName", "System Failure"),
                new KeyValuePair<string, string>("SiteCodeID", "4"),
                new KeyValuePair<string, string>("SiteBankID", "5"),
                new KeyValuePair<string, string>("SiteClientAccountID", "6"),
                new KeyValuePair<string, string>("EventID", "7"),
                new KeyValuePair<string, string>("EntityID", "8"),
                new KeyValuePair<string, string>("InstanceID", "9"),
                new KeyValuePair<string, string>("DocumentTypeKey", "10"),
                new KeyValuePair<string, string>("IsReadOnly", "11"),
                new KeyValuePair<string, string>("NotificationTypeKey", "12"),
                new KeyValuePair<string, string>("BankKey", "13"),
                new KeyValuePair<string, string>("BatchSourceKey", "14"),
                new KeyValuePair<string, string>("ClientAccountKey", "15"),
                new KeyValuePair<string, string>("NotificationFileTypeKey", "16"),
            };
            _filter = new WfsAuditFilter();
        }

        // Removal of Keys

        [TestMethod]
        public void Auditing_Should_Remove_Id()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "ID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_EventRuleId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "EventRuleID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_EventId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "EventID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_EntityID()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "EntityID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_InstanceID()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "InstanceID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_DocumentTypeKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "DocumentTypeKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_IsReadOnly()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "IsReadOnly"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_NotificationTypeKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "NotificationTypeKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_BankKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "BankKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_BatchSourceKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "BatchSourceKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_ClientAccountKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "ClientAccountKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_NotificationFileTypeKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "NotificationFileTypeKey"));
        }

        // Removal of some non-case sensitive fields.
        [TestMethod]
        public void Auditing_Should_Remove_NotificationFileTypeKey_Non_Case_Sensitive()
        {
            // Arrange
            _values.Add(new KeyValuePair<string, string>("NotificAtioNfilETyPeKey", "40"));

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "NotificAtioNfilETyPeKey"));
        }

        // Replacement

        [TestMethod]
        public void Auditing_Should_Replace_SiteBankId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "SiteBankID"));
            Assert.IsTrue(_values.Any(x => x.Key == "BankID"));
        }
        [TestMethod]
        public void Auditing_Should_Replace_SiteClientAccountId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "SiteClientAccountID"));
            Assert.IsTrue(_values.Any(x => x.Key == "WorkgroupID"));
        }

        // No affect cases

        [TestMethod]
        public void Auditing_Should_Not_Affect_BatchNumber()
        {
            // Picked batchnumber out of random, may have to edit this in the future
            // if we want to alter this key.

            // Arrange
            _values.Add(new KeyValuePair<string, string>("BatchNumber", "10"));

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "BatchNumber" && x.Value == "10"));
        }
    }
}
