﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360WebShared.Services;
using WFS.RecHub.R360WebShared.Services.Fakes;

namespace UnitTests.Auditing
{
    [TestClass]
    public class BaseAuditingTests
    {
        private List<KeyValuePair<string, string>> _values;
        private IAuditFilter _filter;

        [TestInitialize]
        public void Initialize()
        {
            // Arrange (all test cases)

            _values = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("ID", "1"),
                new KeyValuePair<string, string>("EventRuleID", "2"),
                new KeyValuePair<string, string>("EventName", "System Failure"),
                new KeyValuePair<string, string>("SiteCodeID", "4"),
                new KeyValuePair<string, string>("SiteBankID", "5"),
                new KeyValuePair<string, string>("SiteClientAccountID", "6"),
                new KeyValuePair<string, string>("EventID", "7"),
                new KeyValuePair<string, string>("EntityID", "8"),
                new KeyValuePair<string, string>("InstanceID", "9"),
                new KeyValuePair<string, string>("DocumentTypeKey", "10"),
                new KeyValuePair<string, string>("IsReadOnly", "11"),
                new KeyValuePair<string, string>("NotificationTypeKey", "12"),
                new KeyValuePair<string, string>("BankKey", "13"),
                new KeyValuePair<string, string>("BatchSourceKey", "14"),
                new KeyValuePair<string, string>("ClientAccountKey", "15"),
                new KeyValuePair<string, string>("NotificationFileTypeKey", "16"),
                new KeyValuePair<string, string>("Batch", "17"),
                new KeyValuePair<string, string>("txtAction", "18"),
                new KeyValuePair<string, string>("hdtSecurity_Token", "19"),
                new KeyValuePair<string, string>("bank", "20"),
                new KeyValuePair<string, string>("lbx", "21"),
                new KeyValuePair<string, string>("fldDisplayRemitterNameInPDF", "22"),
                new KeyValuePair<string, string>("cboSortBy", "ClientAccountID~DESCENDING"),
                new KeyValuePair<string, string>("PaymentSource", "23"),
                new KeyValuePair<string, string>("PaymentSourceId", "23"),
                new KeyValuePair<string, string>("cboPaymentSource", "23"),
                new KeyValuePair<string, string>("PaymentType", "24"),
                new KeyValuePair<string, string>("hdnPaymentSource", "25"),
                new KeyValuePair<string, string>("hdnSomethingElse", "26"),
                new KeyValuePair<string, string>("hidPaymentSource", "25"),
                new KeyValuePair<string, string>("hidSomethingElse", "26"),
                new KeyValuePair<string, string>("txtSomeSillyName", "27"),
                new KeyValuePair<string, string>("fldSillyFieldName", "28"),
                new KeyValuePair<string, string>("cboSillyComboName", "29"),
                new KeyValuePair<string, string>("hdnBatchSourceIDPaymentSource", "30"),
                new KeyValuePair<string, string>("PredefSearchID", "ABCD"),
                new KeyValuePair<string, string>("PredefSearchID_Uncheck", "ABCD"),
                new KeyValuePair<string, string>("ReportID", "31"),
                new KeyValuePair<string, string>("Entity.id", "300301"),
                new KeyValuePair<string, string>("Entity.path", "300301"),
                new KeyValuePair<string, string>("Entity.id[1]", "300311"),
                new KeyValuePair<string, string>("Entity.label", "Tom's Bar"),
                new KeyValuePair<string, string>("parameterValues[1]", "300311"),
                new KeyValuePair<string, string>("parameterValues[2].Name", "300311"),
                new KeyValuePair<string, string>("parameterValues[3].ABC", "300311"),
                new KeyValuePair<string, string>("FIEntityID", "1"),
                new KeyValuePair<string, string>("lockboxPayerId", "1"),
                new KeyValuePair<string, string>("clientAccountId", "1"),
                new KeyValuePair<string, string>("siteClientAcctId", "1"),
                new KeyValuePair<string, string>("users[0]", "11212"),
                new KeyValuePair<string, string>("users[1]", "112121"),
                new KeyValuePair<string, string>("Entity.id", "100101|500"),
                new KeyValuePair<string, string>("selQueryName", "query"),
                new KeyValuePair<string, string>("chkValueOnOff", "checkbox"),
                new KeyValuePair<string, string>(null, null)
            };

            // Mocks for the dependencies
            var entitynameprovider = new StubIEntityNameProvider()
            {
                GetEntityNameInt32 = (entityid) =>
                {
                    return "EntityName";
                }
            };
            var sourcebatchidprovider = new StubISourceBatchIdProvider()
            {
                GetSourceBatchIdInt64 = (batchid) =>
                {
                    return 2020;
                }
            };
            var paymenttypeprovider = new StubIPaymentSourceProvider()
            {
                GetPaymentSourceNameInt32 = (PaymentSource) =>
                {
                    return "PaymentSource";
                },
                GetPaymentTypeNameInt32 = (PaymentType) =>
                {
                    return "PaymentType";
                }
            };
            var queryprovider = new StubISearchQueryProvider()
            {
                GetSearchQueryNameString = (queryid) =>
                {
                    return "QueryName";
                }
            };
            var reportprovider = new StubIReportProvider()
            {
                GetReportNameInt32 = (reportid) =>
                {
                    return "ReportName";
                }
            };

            // Finally gear everything up.
            _filter = new WfsAuditFilter(entitynameprovider, sourcebatchidprovider, 
                paymenttypeprovider, queryprovider, reportprovider);
        }

        // Removal of Keys

        [TestMethod]
        public void Auditing_Should_Remove_Id()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "ID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_EventRuleId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "EventRuleID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_EventId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "EventID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_InstanceID()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "InstanceID"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_DocumentTypeKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "DocumentTypeKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_IsReadOnly()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "IsReadOnly"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_NotificationTypeKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "NotificationTypeKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_BankKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "BankKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_BatchSourceKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "BatchSourceKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_ClientAccountKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "ClientAccountKey"));
        }
        [TestMethod]
        public void Auditing_Should_Remove_NotificationFileTypeKey()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "NotificationFileTypeKey"));
        }

        // Removal of some non-case sensitive fields.
        [TestMethod]
        public void Auditing_Should_Remove_NotificationFileTypeKey_Non_Case_Sensitive()
        {
            // Arrange
            _values.Add(new KeyValuePair<string, string>("NotificAtioNfilETyPeKey", "40"));

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "NotificAtioNfilETyPeKey"));
            Assert.IsFalse(_values.Any(x => x.Key == "NotificationFileTypeKey"));
        }

        [TestMethod]
        public void Auditing_Should_Remove_LockboxPayerId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "lockboxPayerId"));
        }

        [TestMethod]
        public void Auditing_Should_Remove_SecurityToken()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "hdtSecurity_Token"));
        }

        // Replacement

        [TestMethod]
        public void Auditing_Should_Replace_SiteBankId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "SiteBankID"));
            Assert.IsTrue(_values.Any(x => x.Key == "BankID"));
        }
        [TestMethod]
        public void Auditing_Should_Replace_SiteClientAccountId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "SiteClientAccountID"));
            Assert.IsTrue(_values.Any(x => x.Key == "WorkgroupID"));
        }

        [TestMethod]
        public void Auditing_Should_Replace_ClientAccountId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "clientAccountId"));
            Assert.IsTrue(_values.Any(x => x.Key == "WorkgroupID"));
        }

        [TestMethod]
        public void Auditing_Should_Replace_SiteClientAccId()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "SiteClientAccId"));
            Assert.IsTrue(_values.Any(x => x.Key == "WorkgroupID"));
        }

        [TestMethod]
        public void Auditing_Should_Replace_Bank()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "bank"));
            Assert.IsTrue(_values.Any(x => x.Key == "BankID"));
        }
        [TestMethod]
        public void Auditing_Should_Replace_Lbx()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "lbx"));
            Assert.IsTrue(_values.Any(x => x.Key == "WorkgroupID"));
        }
        [TestMethod]
        public void Auditing_Should_Replace_DisplayRemitterNameInPDF()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "fldDisplayRemitterNameInPDF"));
            Assert.IsTrue(_values.Any(x => x.Key == "DisplayPayerNameInPDF"));
        }

        // No affect cases

        [TestMethod]
        public void Auditing_Should_Not_Affect_BatchNumber()
        {
            // Picked batchnumber out of random, may have to edit this in the future
            // if we want to alter this key.

            // Arrange
            _values.Add(new KeyValuePair<string, string>("BatchNumber", "10"));

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "BatchNumber" && x.Value == "10"));
        }

        // Cases With Key Actions

        [TestMethod]
        public void Auditing_Should_Perform_PredefSearchID_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "PredefSearchID" && x.Value == "QueryName"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_PredefSearchID_Uncheck_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "PredefSearchID_Uncheck" && x.Value == "QueryName"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_ReportID_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "ReportID" && x.Value == "ReportName"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_BatchID_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "Batch" && x.Value == "2020"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_EntityID_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "EntityID" && x.Value == "EntityName"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_FIEntityID_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "FIEntityID" && x.Value == "EntityName"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_Entity_Property_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "Entity.id" && x.Value == "EntityName"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_Entity_Property_Filtering()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Count(x => x.Key.StartsWith("Entity.")) == 1);
        }

        [TestMethod]
        public void Auditing_Should_Perform_Entity_Piped_Workgroup_Filtering()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "WorkgroupSelection" && x.Value == "100101|500"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_User_Property_Filtering()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Count(x => x.Key.StartsWith("users[")) == 0);
        }

        [TestMethod]
        public void Auditing_Should_Perform_Report_Property_Filtering()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Count(x => x.Key.StartsWith("parameterValues[")) == 0);
        }

        [TestMethod]
        public void Auditing_Should_Perform_SortBy_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "SortBy" && !x.Value.Contains("ClientAccountID")));
        }

        [TestMethod]
        public void Auditing_Should_Perform_PaymentType_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "PaymentType" && x.Value == "PaymentType"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_PaymentSource_Action()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "PaymentSource" && x.Value == "PaymentSource"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_PaymentSourceId_Action()
        {
            _values.RemoveAll(x => x.Key == "PaymentSource");
            _values.RemoveAll(x => x.Key == "cboPaymentSource");

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "PaymentSourceId" && x.Value == "PaymentSource"));
        }

        [TestMethod]
        public void Auditing_Should_Perform_CBO_PaymentSource_Action()
        {
            // Arrange
            // We only want the 'cbo' for this test.
            _values.RemoveAll(x => x.Key == "PaymentSource");

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsTrue(_values.Any(x => x.Key == "PaymentSource" && x.Value == "PaymentSource"));
        }

        // Cases to break the key actions

        [TestMethod]
        public void Auditing_Should_Fail_To_Perform_BatchID_Action()
        {
            // Arrange
            _values.RemoveAll(x => x.Key == "BatchID");
            _values.Add(new KeyValuePair<string, string>("BatchID", "notanumber"));

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "BatchID" && x.Value == "2020"));
            Assert.IsTrue(_values.Any(x => x.Key == "BatchID" && x.Value == "notanumber"));
        }

        [TestMethod]
        public void Auditing_Should_Fail_To_Perform_EntityID_Action()
        {
            // Arrange
            _values.RemoveAll(x => x.Key == "EntityID");
            _values.Add(new KeyValuePair<string, string>("EntityID", "notanumber"));

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "EntityID" && x.Value == "2020"));
            Assert.IsTrue(_values.Any(x => x.Key == "EntityID" && x.Value == "notanumber"));
        }

        [TestMethod]
        public void Auditing_Should_Fail_To_Perform_PaymentType_Action()
        {
            // Arrange
            _values.RemoveAll(x => x.Key == "PaymentType");
            _values.Add(new KeyValuePair<string, string>("PaymentType", "notanumber"));

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "PaymentType" && x.Value == "PaymentType"));
            Assert.IsTrue(_values.Any(x => x.Key == "PaymentType" && x.Value == "notanumber"));
        }

        [TestMethod]
        public void Auditing_Should_Fail_To_Perform_PaymentSource_Action()
        {
            // Arrange
            _values.RemoveAll(x => x.Key == "PaymentSource");
            _values.RemoveAll(x => x.Key == "cboPaymentSource");
            _values.Add(new KeyValuePair<string, string>("PaymentSource", "notanumber"));

            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key == "PaymentSource" && x.Value == "PaymentSource"));
            Assert.IsTrue(_values.Any(x => x.Key == "PaymentSource" && x.Value == "notanumber"));
        }

        // Cases for Regex Actions
        [TestMethod]
        public void Auditing_Should_Remove_HDN_Fields()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key.StartsWith("hdn")));
        }

        [TestMethod]
        public void Auditing_Should_Remove_HID_Fields()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key.StartsWith("hid")));
        }

        [TestMethod]
        public void Auditing_Should_Rename_TXT_Fields()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key.StartsWith("txt")));
            Assert.IsTrue(_values.Any(x => x.Key == "SomeSillyName"));
        }

        [TestMethod]
        public void Auditing_Should_Rename_SEL_Fields()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key.StartsWith("sel")));
            Assert.IsTrue(_values.Any(x => x.Key == "QueryName"));
        }

        [TestMethod]
        public void Auditing_Should_Rename_CHK_Fields()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key.StartsWith("chk")));
            Assert.IsTrue(_values.Any(x => x.Key == "ValueOnOff"));
        }

        [TestMethod]
        public void Auditing_Should_Rename_FLD_Fields()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key.StartsWith("fld")));
            Assert.IsTrue(_values.Any(x => x.Key == "SillyFieldName"));
        }

        [TestMethod]
        public void Auditing_Should_Rename_CBO_Fields()
        {
            // Act
            _values = _filter.ApplyFilter(_values);

            // Assert
            Assert.IsFalse(_values.Any(x => x.Key.StartsWith("cbo")));
            Assert.IsTrue(_values.Any(x => x.Key == "SillyComboName"));
        }


        [TestMethod]
        public void Auditing_Should_Handle_Null_Pairs()
        {
            try
            {
                // Arrange and Act
                _values = _filter.ApplyFilter(_values);
            }
            catch (Exception e)
            {
                Assert.Fail();
            }
        }
    }
}
