﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StructureMap;
using Wfs.Logging;
using Moq;
using WFS.RecHub.R360Shared.Fakes;
using Microsoft.QualityTools.Testing.Fakes;
using HubConfigViews.Controllers;
using System.Web.Mvc;
using HubConfigViews.Models;
using WFS.RecHub.R360Services.R360ServicesServicesClient.Fakes;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace HubConfigViewsTests
{
    /// <summary>
    /// Added ignore, we need to decouple everything from the controller first.
    /// </summary>
    [TestClass, Ignore]
    public class UserPreferencesTests
    {
        public UserPreferencesModel model { get; set; }
        public UserPreferencesDTO dto { get; set; }
        [TestInitialize]
        public void SetUp()
        {
            ObjectFactory.Configure(x =>
            {
                x.For<ILogSource>().Use(new Mock<ILogSource>().Object);
            });

            model = new UserPreferencesModel
            {
                displayRemitterAccesible = true,
                displayRemitterNameInPDF = true,
                recordsPageAccesible = true,
                recordsPerPage = 20
            };
            dto = new UserPreferencesDTO
            {
                recordsPerPage = 20,
                displayRemitterNameinPdf = true
            };
        }

        [TestMethod]
        public void Should_Not_Access_RestoreDefaults()
        {
            using (ShimsContext.Create())
            {

                ShimR360Permissions.AllInstances.AllowedStringR360PermissionsActionType =
                    (permissions, s, parm3) => false;

                var controller = new UserPreferencesController();

                JsonResult result = controller.RestoreDefaults() as JsonResult;
                dynamic dresult = result.Data;
                Assert.AreEqual(dresult.HasErrors, true);

            }
        }
        [TestMethod]
        public void Should_Not_Access_SavePreferences()
        {
            using (ShimsContext.Create())
            {

                ShimR360Permissions.AllInstances.AllowedStringR360PermissionsActionType =
                    (permissions, s, parm3) => false;

                var controller = new UserPreferencesController();
                JsonResult result = controller.SavePreferences(model);
                dynamic dresult = result.Data;
                Assert.AreEqual(dresult.HasErrors, true);
            }
        }

        [TestMethod]
        public void Should_Fail_SavePreferences()
        {
            using (ShimsContext.Create())
            {

                ShimR360Permissions.AllInstances.AllowedStringR360PermissionsActionType =
                    (permissions, s, parm3) => true;
                //need to refactor this service
                ShimUserServiceManager.AllInstances.SetUserPreferencesUserPreferencesDTO = (serviceManager, dto) 
                    => new BaseGenericResponse<bool> {Status = StatusCode.FAIL};
                

                var controller = new UserPreferencesController();
                JsonResult result = controller.SavePreferences(model);
                dynamic dresult = result.Data;
                Assert.AreEqual(dresult.HasErrors, true);
            }
        }

        [TestMethod]
        public void Should_Fail_RestoreDefaultPreferences()
        {
            using (ShimsContext.Create())
            {

                ShimR360Permissions.AllInstances.AllowedStringR360PermissionsActionType =
                    (permissions, s, parm3) => true;
                //need to refactor this service
                ShimUserServiceManager.AllInstances.RestoreDefaultPreferences = (serviceManager)
                    => new BaseGenericResponse<UserPreferencesDTO> { Status = StatusCode.FAIL };

                var controller = new UserPreferencesController();
                JsonResult result = controller.SavePreferences(model);
                dynamic dresult = result.Data;
                Assert.AreEqual(dresult.HasErrors, true);
            }
        }
    }

}
