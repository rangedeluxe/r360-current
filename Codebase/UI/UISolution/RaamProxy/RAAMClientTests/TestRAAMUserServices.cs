﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360RaamClient.Fakes;

namespace RAAMClientTests
{
    [TestClass]
    public class TestRAAMUserServices
    {

        private IRaamClient client;

        [TestInitialize]
        public void setup()
        {

        }

        private IEnumerable<UserDto> GetUserList() 
        {
            var users = new List<UserDto>();
            for (var i = 0; i <10; i++) {
                users.Add(new UserDto {ID =1, Email = "user" + i + "@email.com", UserSID = Guid.NewGuid(), LoginId = "user" + i });
            }
            return users;
        }

        [TestMethod]
        public void TestGetUserEntitiesStub()
        {
            client = new StubIRaamClient()
            {
                  GetUserHierarchy = () => {
                        return GetUserList();
                 }
            };
        
            var users =  client.GetUserHierarchy();
            foreach (var user in users)
            {
                Debug.WriteLine(user.LoginId);
            }

            Assert.IsTrue(users != null && !users.IsEmpty());
        }

        public void TestGetUserEntitiesShim()
        {
          
        }

        
    }
}
