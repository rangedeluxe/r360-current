﻿using RaamProxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RaamProxy.Extensions;

namespace RaamProxy.EntityProviders
{
    public class RAAMEntityProvider : IEntityProvider
    {

        public bool GetEntitiesWithGroups(out List<Models.EntityModel> output)
        {
            if (!EntityModel.GetEntitiesWithGroups(out output))
                output = new List<EntityModel>();
            output = output.SortEntities(true)
                .ToList();
            return true;
        }

		public bool GetEntitiesWithUsers(int id, out List<Models.EntityModel> output)
		{
			if (!EntityModel.GetEntitiesWithUsers(id, out output))
				output = new List<EntityModel>();
			output = output.SortEntities(true)
				.ToList();
			return true;
		}

		public bool GetEntitiesWithRaamUsers(out List<Models.EntityModel> output)
		{
			if (!EntityModel.GetEntitiesWithRaamUsers(out output))
				output = new List<EntityModel>();
			output = output.SortEntities(true)
				.ToList();
			return true;
		}

		public bool GetIntegraPayOnlyEntities(out List<Models.EntityModel> output)
        {
            if (!EntityModel.GetIntegraPayOnlyEntities(out output))
                output = new List<EntityModel>();
            output = output.SortEntities(false)
                .ToList();
            return true;
        }

        public bool GetEntities(bool entitiesonly, out List<EntityModel> output)
        {
            if (!EntityModel.GetEntities(!entitiesonly, out output))
                output = new List<EntityModel>();
            output = output.SortEntities(false)
                .ToList();
            return true;
        }

        public bool GetEntities(int id, out List<EntityModel> output)
        {
            if (!EntityModel.GetEntities(id, out output))
                output = new List<EntityModel>();
            output = output.SortEntities(false)
                .ToList();
            return true;
        }

        public bool GetAllEntitiesWithUsers(out List<Models.EntityModel> output)
        {
            if (!EntityModel.GetAllEntitiesWithUsers(out output))
                output = new List<EntityModel>();
            output = output.SortEntities(true)
                .ToList();
            return true;
        }
    }
}