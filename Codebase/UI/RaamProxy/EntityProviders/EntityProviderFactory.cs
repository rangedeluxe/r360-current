﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaamProxy.EntityProviders
{
    public static class EntityProviderFactory
    {

        public static IEntityProvider GetEntityProvider()
        {
            var appsettings = System.Configuration.ConfigurationManager.AppSettings;
            var boolval = false;
            IEntityProvider output;

            if (appsettings.AllKeys.Contains("UseMock")
                && bool.TryParse(appsettings["UseMock"], out boolval)
                && boolval)
                output = new MockEntityProvider();
            else
                output = new RAAMEntityProvider();

            return output;
        }
    }
}