﻿using RaamProxy.MockData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RaamProxy.Extensions;

namespace RaamProxy.EntityProviders
{
    public class MockEntityProvider : IEntityProvider
    {
        public bool GetEntitiesWithGroups(out List<Models.EntityModel> output)
        {
            output = MockHelper.OutsourceData(true)
                .SortEntities(true)
                .ToList();
            return true;
        }

        public bool GetEntitiesWithUsers(int id, out List<Models.EntityModel> output)
        {
            output = MockHelper.OutsourceData(true)
                .SortEntities(true)
                .ToList();
            return true;
        }

		public bool GetEntitiesWithRaamUsers(out List<Models.EntityModel> output)
		{
			throw new NotImplementedException();
		}

		public bool GetIntegraPayOnlyEntities(out List<Models.EntityModel> output)
        {
            output = MockHelper.OutsourceData(false)
                .SortEntities(false)
                .ToList();
            return true;
        }

        public bool GetEntities(bool entitiesonly, out List<Models.EntityModel> output)
        {
            output = MockHelper.OutsourceData(entitiesonly)
                .SortEntities(false)
                .ToList();
            return true;
        }

        public bool GetEntities(int id, out List<Models.EntityModel> output)
        {
            output = MockHelper.OutsourceData(true)
                .SortEntities(false)
                .ToList();
            return true;
        }

        public bool GetAllEntitiesWithUsers(out List<Models.EntityModel> output)
        {
            output = MockHelper.OutsourceData(true).SortEntities(true).ToList();
            return true;
        }
    }
}