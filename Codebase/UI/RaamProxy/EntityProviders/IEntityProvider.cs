﻿using RaamProxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaamProxy.EntityProviders
{
    public interface IEntityProvider
    {
        bool GetEntitiesWithGroups(out List<EntityModel> output);
        bool GetEntitiesWithUsers(int id, out List<EntityModel> output);
		bool GetEntitiesWithRaamUsers(out List<EntityModel> output);
        bool GetIntegraPayOnlyEntities(out List<EntityModel> output);
        bool GetEntities(bool entitiesonly, out List<EntityModel> output);
        bool GetEntities(int id, out List<EntityModel> output);
        bool GetAllEntitiesWithUsers(out List<EntityModel> output);
    }
}
