﻿using RaamProxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaamProxy.MockData
{
  public class MockHelper
  {
    public static IList<EntityModel> OutsourceData(bool entitiesOnly)
    {
      IList<EntityModel> rvalue = new List<EntityModel>();

      var root = new EntityModel { id = "1", label = "Outsourcing", entityTypeCode = "AdminEntity" };
      rvalue.Add(root);

      // fi
      for (int i = 1; i < 7; i++)
      {
        var c = (i % 5 == 0 ? 150 : 5);
        root.items.Add(GetMockFILOBOrgData("An FI - " + i, c, entitiesOnly));
      }

      return rvalue;
    }

    public static IList<EntityModel> Workgroups(bool entitiesOnly)
    {
      IList<EntityModel> rvalue = new List<EntityModel>();

      var root = new EntityModel { id = "100", label = "Corporate ABC", entityTypeCode = "Corp" };
      rvalue.Add(root);

      // fi
      for (int i = 1; i < 15; i++)
      {
        root.items.Add(new EntityModel { id = i.ToString(), isNode = true, label = "Workgroup-" + i, icon = "/Assets/wfs/shim.gif" });
      }

      return rvalue;
    }

    public static IList<EntityModel> Workgroup(bool entitiesOnly)
    {
      IList<EntityModel> rvalue = new List<EntityModel>();

	  var root = new EntityModel { id = "100", label = "Corporate ABC", entityTypeCode = "Corp" };
      rvalue.Add(root);

      root.items.Add(new EntityModel { id = "2", isNode = true, label = "Workgroup-2", icon = "/Assets/wfs/shim.gif" });
      
      return rvalue;
    }

    private static EntityModel GetMockFIOrgData(string fiName, int c, bool entitiesOnly)
    {
      string[] orgs = new string[6] { "WFS", "ABC", "XYZ", "QRS", "TUV", "DEF" };
      string[] orgs2 = new string[8] { "Insurance", "Manufacturing", "Electronics", "Restaurant", "Engineering", "Clothing", "Medical", "Realty" };

      // fi
      var fiId = System.Guid.NewGuid().ToString();
	  var org = new EntityModel { id = fiId, label = fiName, entityTypeCode = "FI" };

      // corporates
      for (var j = 0; j < c; j++)
      {
        org.items.Add(CorpData(orgs[j % 6] + " " + orgs2[j % 8] + " - " + j, entitiesOnly));
      }

      return org;
    }

    private static EntityModel GetMockFILOBOrgData(string fiName, int c, bool entitiesOnly)
    {
      string[] orgs = new string[6] { "WFS", "ABC", "XYZ", "QRS", "TUV", "DEF" };
      string[] orgs2 = new string[8] { "Insurance", "Manufacturing", "Electronics", "Restaurant", "Engineering", "Clothing", "Medical", "Realty" };

      // fi
      var fiId = System.Guid.NewGuid().ToString();
	  var org = new EntityModel { id = fiId, label = fiName, entityTypeCode = "FI" };

      // corporates
      for (var j = 0; j < c; j++)
      {
        org.items.Add(CorpLOBData(orgs[j % 6] + " " + orgs2[j % 8] + " - " + j, entitiesOnly));
      }

      return org;
    }

    private static EntityModel CorpData(string corpName, bool entitiesOnly)
    {
		var root = new EntityModel { id = System.Guid.NewGuid().ToString(), label = corpName, entityTypeCode = "Corp" };

      if (!entitiesOnly)
      {
        for (int m = 1; m < 4; m++)
        {
          root.items.Add(new EntityModel { id = System.Guid.NewGuid().ToString(), isNode = true, label = "Workgroup-" + m, icon = "/Assets/wfs/shim.gif" });
        }
      }

      return root;
    }

    private static EntityModel CorpLOBData(string corpName, bool entitiesOnly)
    {
		var root = new EntityModel { id = System.Guid.NewGuid().ToString(), label = corpName, entityTypeCode = "Corp" };

      for (var j = 0; j < 4; j++)
      {
        var lobId = System.Guid.NewGuid().ToString();
		var lob = new EntityModel { id = lobId, label = "Line of Bus. - " + j, entityTypeCode = "LOB" };
        root.items.Add(lob);
        if (!entitiesOnly)
        {
          for (int m = 1; m < 4; m++)
          {
            lob.items.Add(new EntityModel { id = System.Guid.NewGuid().ToString(), isNode = true, label = "Workgroup-" + m, icon = "/Assets/wfs/shim.gif" });
          }
        }
      }

      return root;
    }
  }
}