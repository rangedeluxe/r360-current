﻿using RaamProxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RaamProxy.Extensions
{
    public static class EntityModelExtensions
    {
        /// <summary>
        /// Just a little method to sort our workgroups and Entities.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IEnumerable<EntityModel> SortEntities(this IEnumerable<EntityModel> entities, bool sortNodes)
        {
            // Need to first sort by if it's a workgroup or entity, then by the label.
            var ordered = entities.OrderBy(x =>
            {
                if (x.isNode)
                    return -1;
                else 
                    return 1;
            })
            .ThenBy(x => (!sortNodes && x.isNode) ? string.Empty : x.label);

            // Now we need to apply the same to the children.
            foreach (var e in ordered)
                e.items = e.items
					.SortEntities(sortNodes)
                    .ToList();
            return ordered;
        }
    }
}