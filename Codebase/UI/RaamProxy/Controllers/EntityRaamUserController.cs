﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RaamProxy.Models;
using WFS.RecHub.R360RaamClient;
using RaamProxy.MockData;
using RaamProxy.EntityProviders;

namespace RaamProxy.Controllers
{
    public class EntityRaamUserController : ApiController
    {

        // GET api/entity
		[Authorize]
        public IEnumerable<EntityModel> Get()
        {
            List<EntityModel> rvalue = null;

            if (!EntityProviderFactory.GetEntityProvider()
                .GetEntitiesWithRaamUsers(out rvalue))
                return new List<EntityModel>();

            return rvalue;
        }
    }
}
