﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Wfs.Logging;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;

namespace RaamProxy.Controllers
{
    public class EventTypeController : ApiController
    {
        // GET api/eventtype
		[Authorize]
        public IEnumerable<string> Get()
        {
            Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(EventTypeController));
            var client = new RaamClient(new ConfigHelpers.Logger(
                                               o => Logger.Log(LogLevel.Informational, o),
                                               o => Logger.Log(LogLevel.Warning, o)));
            return client.GetEventTypes();
        }
     
    }
}
