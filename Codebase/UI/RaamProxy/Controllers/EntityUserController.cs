﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RaamProxy.Models;
using WFS.RecHub.R360RaamClient;
using RaamProxy.MockData;
using RaamProxy.EntityProviders;

namespace RaamProxy.Controllers
{
    public class EntityUserController : ApiController
    {

        // GET api/entity/id
		[Authorize]
        public IEnumerable<EntityModel> Get(int id)
        {
            List<EntityModel> rvalue = null;

            if (!EntityProviderFactory.GetEntityProvider()
                .GetEntitiesWithUsers(id, out rvalue))
                return new List<EntityModel>();

            return rvalue;
        }
        // GET api/entity
        [Authorize]
        public IEnumerable<EntityModel> Get()
        {
            List<EntityModel> rvalue = null;
            if (!EntityProviderFactory.GetEntityProvider().GetAllEntitiesWithUsers(out rvalue))
                return new List<EntityModel>();
            return rvalue;
        }
    }
}
