﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Wfs.Logging;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;


namespace RaamProxy.Controllers
{
    public class EntityBreadcrumbController : ApiController
    {
        // GET api/entitybreadcrumb
		[Authorize]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/entitybreadcrumb/5
		[Authorize]
		public List<EntityBreadcrumbDTO> Get(int id)
        {

    		Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(EntityBreadcrumbController));
            var client = new RaamClient(new ConfigHelpers.Logger(
                                               o => Logger.Log(LogLevel.Informational, o),
                                               o => Logger.Log(LogLevel.Warning, o)));
            var ebc = new List<EntityBreadcrumbDTO>();
            var success = client.GetEntityBreadcrumbList(id, ebc, true);
            return ebc;
        }

        // POST api/entitybreadcrumb
		[Authorize]
		public void Post([FromBody]string value)
        {
        }

        // PUT api/entitybreadcrumb/5
		[Authorize]
		public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/entitybreadcrumb/5
		[Authorize]
		public void Delete(int id)
        {
        }
    }
}
