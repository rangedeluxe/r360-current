﻿using RaamProxy;
using RaamProxy.EntityProviders;
using RaamProxy.MockData;
using RaamProxy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RaamProxy.Controllers
{
	public class EntityController : ApiController
	{
		// GET api/entity
		[Authorize]
		public IEnumerable<EntityModel> Get(bool entitiesOnly)
		{
			List<EntityModel> rvalue = null;

			if (!EntityProviderFactory.GetEntityProvider()
				.GetEntities(entitiesOnly, out rvalue))
				return new List<EntityModel>();

			return rvalue;
		}

		// GET api/entity/5
		[Authorize]
		public List<EntityModel> Get(int id)
		{
			List<EntityModel> rvalue = null;

			if (!EntityProviderFactory.GetEntityProvider()
				.GetEntities(id, out rvalue))
				return new List<EntityModel>();


			return rvalue;
		}
	}
}
