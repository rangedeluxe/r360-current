﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RaamProxy.Models;
using RaamProxy.MockData;
using RaamProxy.EntityProviders;

namespace RaamProxy.Controllers
{
    public class EntityGroupController : ApiController
    {
		[Authorize]
        public IEnumerable<EntityModel> Get()
        {
            List<EntityModel> rvalue = null;

            if (!EntityProviderFactory.GetEntityProvider()
                .GetEntitiesWithGroups(out rvalue))
                return new List<EntityModel>();

            return rvalue;
        }
    }
}
