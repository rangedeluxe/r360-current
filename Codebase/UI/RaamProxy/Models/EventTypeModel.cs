﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wfs.Logging;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;

namespace RaamProxy.Models
{
    public class EventTypeModel
    {

        private static Wfs.Logging.ILogSource _logger = null;
        public static Wfs.Logging.ILogSource Logger
        {
            get
            {
                if (_logger == null)
                    _logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(EntityModel));
                return _logger;
            }
        }

        public IEnumerable<string> GetEventTypes()
        {
            var client = new RaamClient(new ConfigHelpers.Logger(
                                                o => Logger.Log(LogLevel.Informational, o),
                                                o => Logger.Log(LogLevel.Warning, o)));
            return client.GetEventTypes();
        }

    }
}