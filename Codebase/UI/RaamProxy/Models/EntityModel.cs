﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wfs.Logging;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.Common;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;

namespace RaamProxy.Models
{
    [Bind(Include = "id,label,icon,value,isNode,entityTypeCode,items")]
	public class EntityModel
	{
		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(EntityModel));
				return _logger;
			}
		}

		public string id { get; set; }

		public string label { get; set; }

		public string icon { get; set; }

		public long value { get; set; }
	
		public bool isNode { get; set; }

		public string entityTypeCode { get; set; }
	
		public IList<EntityModel> items { get; set; }

		public EntityModel()
		{
			items = new List<EntityModel>();
		}

		public static bool GetEntities(bool includeWorkgroups, out List<EntityModel> entities)
		{
			entities = new List<EntityModel>();
			try
			{
				var client = new RaamClient(new ConfigHelpers.Logger(
												o => Logger.Log(LogLevel.Informational, o),
												o => Logger.Log(LogLevel.Warning, o)));
				var raamEntities = client.GetAuthorizedEntitiesAndWorkgroups();

				List<WorkgroupNameDTO> workgroups = null;
				if (includeWorkgroups)
					workgroups = GetActiveWorkgroups(raamEntities);

				Logger.Log(LogLevel.Debug, "RAAM returned: {0}", raamEntities == null ? "<Null>" : raamEntities.Name);

				// Convert to local format
				ConvertToEntityModel(raamEntities, entities, includeWorkgroups, client, workgroups);

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(LogLevel.Error, ex.ToString());

				return false;
			}
		}

        /// <summary>
        /// Gets the entities.
        /// </summary>
        /// <param name="entityId">The entity id.</param>
        /// <param name="entities">The entities.</param>
        /// <returns></returns>
        public static bool GetEntities(int entityId, out List<EntityModel> entities)
        {
            entities = new List<EntityModel>();
            try
            {
                var client = new RaamClient(new ConfigHelpers.Logger(
                                                o => Logger.Log(LogLevel.Informational, o),
                                                o => Logger.Log(LogLevel.Warning, o)));
                var raamEntities = client.GetEntityHierarchy(entityId);
               
                Logger.Log(LogLevel.Debug, "RAAM returned: {0}", raamEntities == null ? "<Null>" : raamEntities.Name);

                // Convert to local format
                ConvertToEntityModelHierarchy(raamEntities, entities, client);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.ToString());

                return false;
            }
        }

        public static bool GetIntegraPayOnlyEntities(out List<EntityModel> entities)
        {
            entities = new List<EntityModel>();
            try
            {
                var client = new RaamClient(new ConfigHelpers.Logger(
                                                o => Logger.Log(LogLevel.Informational, o),
                                                o => Logger.Log(LogLevel.Warning, o)));
                var raamEntities = client.GetAuthorizedEntitiesAndWorkgroups();

                List<WorkgroupNameDTO> workgroups = GetIntegraPayOnlyWorkgroups(raamEntities);

                Logger.Log(LogLevel.Debug, "RAAM returned: {0}", raamEntities == null ? "<Null>" : raamEntities.Name);

                // Convert to local format
                ConvertToEntityModel(raamEntities, entities, true, client, workgroups);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.ToString());

                return false;
            }
        }

        public static bool GetAllEntitiesWithUsers(out List<EntityModel> entities)
        {
            entities = new List<EntityModel>();
            var hubUsers = new List<AuditUserDTO>();
            try
            {
                var raamClient = new RaamClient(new ConfigHelpers.Logger(
                                     o => Logger.Log(LogLevel.Informational, o),
                                     o => Logger.Log(LogLevel.Warning, o)));
                var r360Client = new R360ServiceManager();
                var raamEntities = raamClient.GetAuthorizedEntities();
                var raamUsers = raamClient.GetUserHierarchy().ToList();
                var response = r360Client.GetAuditUsers();
                if (response.Status == StatusCode.SUCCESS)
                {
                    hubUsers = response.Data;
                }
 

                Logger.Log(LogLevel.Debug, "RAAM returned: {0}", raamEntities == null ? "<Null>" : raamEntities.Name);
                Logger.Log(LogLevel.Debug, "RAAM returned: {0} Users", raamUsers == null ? 0 : raamUsers.Count);
                Logger.Log(LogLevel.Debug, "RecHub returned: {0} Users", hubUsers == null ? 0 : hubUsers.Count);

                // Filter out Users that are not in RecHub
                FilterRecHubUsers(hubUsers, raamUsers);
                // Sort Users by EntityId
                var sortedUsers = raamUsers.OrderBy(o => o.EntityId).ToList();

                // Convert to local format
                ConvertToEntityModel(raamEntities, entities, sortedUsers);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.ToString());

                return false;
            }
        }

        public static bool GetEntitiesWithUsers(int entityId, out List<EntityModel> entities)
        {
            entities = new List<EntityModel>();
            var hubUsers = new List<AuditUserDTO>();
            try
            {
                var raamClient = new RaamClient(new ConfigHelpers.Logger(
                                                o => Logger.Log(LogLevel.Informational, o),
                                                o => Logger.Log(LogLevel.Warning, o)));

                var r360Client = new R360ServiceManager();

                var raamEntities = raamClient.GetEntityHierarchy(entityId);
                
                
                var raamUsers = raamClient.GetUserHierarchy().ToList();

                // TEST - Remove When Done
                //Debug.WriteLine("----------------------------- RAAM Users ---------------------------------------");
                //foreach (var user in raamUsers)
                //{
                //   Debug.WriteLine(String.Format("Found User {0} - {1} {2} in Entity {3}", user.Email, user.Person.FirstName, user.Person.LastName, user.EntityId));
                //}

                var response = r360Client.GetAuditUsers();
                if (response.Status == StatusCode.SUCCESS)
                {
                    hubUsers = response.Data;
                }

                Logger.Log(LogLevel.Debug, "RAAM returned: {0}", raamEntities == null ? "<Null>" : raamEntities.Name);
                Logger.Log(LogLevel.Debug, "RAAM returned: {0} Users", raamUsers == null ? 0 : raamUsers.Count);
                Logger.Log(LogLevel.Debug, "RecHub returned: {0} Users", hubUsers == null ? 0 : hubUsers.Count);

                // Filter out Users that are not in RecHub
                FilterRecHubUsers(hubUsers, raamUsers);

                //Debug.WriteLine("----------------------------------- Scrubbed Users ----------------------------------");
                //foreach (var user in raamUsers)
                //{
                //    Debug.WriteLine(String.Format("Found User {0} - {1} {2} in Entity {3}", user.Email, user.Person.FirstName, user.Person.LastName, user.EntityId));
                //}

                // Sort Users by EntityId
                var sortedUsers = raamUsers.OrderBy(o => o.EntityId).ToList();

                // Convert to local format
                ConvertToEntityModel(raamEntities, entities, sortedUsers);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.ToString());

                return false;
            }
        }

		public static bool GetEntitiesWithRaamUsers(out List<EntityModel> entities)
		{
			entities = new List<EntityModel>();
			var hubUsers = new List<AuditUserDTO>();
			try
			{
				var raamClient = new RaamClient(new ConfigHelpers.Logger(
												o => Logger.Log(LogLevel.Informational, o),
												o => Logger.Log(LogLevel.Warning, o)));

				var r360Client = new R360ServiceManager();

                var raamEntities = raamClient.GetAuthorizedEntities();                
				var raamUsers = raamClient.GetUserHierarchy();

				Logger.Log(LogLevel.Debug, "RAAM returned: {0}", raamEntities == null ? "<Null>" : raamEntities.Name);
				Logger.Log(LogLevel.Debug, "RAAM returned: {0} Users", raamUsers == null ? 0 : raamUsers.Count());

				// Convert to local format
				ConvertToEntityModel(raamEntities, entities, raamUsers);

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(LogLevel.Error, ex.ToString());

				return false;
			}
		}


        public static bool GetEntitiesWithGroups(out List<EntityModel> entities)
        {
            entities = new List<EntityModel>();

            try
            {
                var client = new RaamClient(new ConfigHelpers.Logger(
                                                o => Logger.Log(LogLevel.Informational, o),
                                                o => Logger.Log(LogLevel.Warning, o)));

                //var raamEntities = client.GetAuthorizedEntities(entityId);
                var entityGroups = client.GetEntityGroupHierarchy();


                Logger.Log(LogLevel.Debug, "RAAM returned: {0}", entityGroups == null ? "<Null>" : entityGroups.Name);

                // Convert to local format
                ConvertToEntityModel(entityGroups, entities, client);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(LogLevel.Error, ex.ToString());

                return false;
            }
        }

		private static void ConvertToEntityModel(EntityDto raamEntity, List<EntityModel> entities, bool appendWorkgroups, RaamClient client, List<WorkgroupNameDTO> workgroups)
		{
			if (raamEntity == null)
				return;

			var entityModel = new EntityModel()
			{
				id = raamEntity.ID.ToString(),
				label = raamEntity.Name,
				isNode = false,
				entityTypeCode = raamEntity.EntityTypeCode,
                
			};

			List<EntityModel> assignedWorkgroups = null;
			if (appendWorkgroups && workgroups != null)
			{
				var resources = raamEntity.Resources;
				if (resources != null)
				{
					while (workgroups.Count > 0 && workgroups[0].EntityId == raamEntity.ID)
					{
						var wgExtRefID = RaamHelper.FormatWorkgroupRefID(workgroups[0].SiteBankID, workgroups[0].SiteWorkgroupID);
						var resource = resources.FirstOrDefault(o => o.ExternalReferenceID == wgExtRefID);
						if (resource != null)
						{
							if (assignedWorkgroups == null)
								assignedWorkgroups = new List<EntityModel>();
							assignedWorkgroups.Add(new EntityModel()
							{
								id = wgExtRefID,
								label = workgroups[0].DisplayLabel,
								icon = "/Assets/wfs/shim.gif",
								isNode = true
							});
						}
						workgroups.RemoveAt(0);
					}
				}
			}

			//add the entity children
			List<EntityModel> children = new List<EntityModel>();
			if (raamEntity.ChildEntities != null && raamEntity.ChildEntities.Count() > 0)
			{
				foreach (var child in raamEntity.ChildEntities)
				{
					ConvertToEntityModel(child, children, appendWorkgroups, client, workgroups);
				}
			}

			if (assignedWorkgroups != null && assignedWorkgroups.Count > 0)
				children.AddRange(assignedWorkgroups);

			entityModel.items = children;
			entities.Add(entityModel);
		}

        private static void ConvertToEntityModel(EntityDto raamEntity, List<EntityModel> entities, IEnumerable<UserDto> users)
        {
            if (raamEntity == null)
                return;

            var entityModel = new EntityModel()
            {
                id = raamEntity.ID.ToString(),
                label = raamEntity.Name,
                isNode = false,
                entityTypeCode = raamEntity.EntityTypeCode
            };

			var assignedUsers = users.Where(o => o.EntityId == raamEntity.ID).Select(o => new EntityModel()
			{
				id = o.UserSID.ToString(),
				label = o.Person.FirstName + " " + o.Person.LastName,
				icon = "/Assets/wfs/shim.gif",
				isNode = true,
				value = o.ID
			});

            //add the entity children
            List<EntityModel> children = new List<EntityModel>();
            if (raamEntity.ChildEntities != null && raamEntity.ChildEntities.Count() > 0)
            {
                foreach (var child in raamEntity.ChildEntities)
                {
                    ConvertToEntityModel(child, children, users);
                }
            }

            if (assignedUsers != null && assignedUsers.Count() > 0)
                children.AddRange(assignedUsers);

            entityModel.items = children;
            entities.Add(entityModel);

		}

        private static void ConvertToEntityModel(EntityDto raamEntity, List<EntityModel> entities, RaamClient client)
        {
            if (raamEntity == null)
                return;

            var entityModel = new EntityModel()
            {
                id = raamEntity.ID.ToString(),
                label = raamEntity.Name,
                isNode = false,
                entityTypeCode = raamEntity.EntityTypeCode
            };

            List<EntityModel> assignedGroups = null;
            if (raamEntity.Resources != null)
            {
                foreach (var group in raamEntity.Resources)
                {

                    if (assignedGroups == null)
                    {
                        assignedGroups = new List<EntityModel>();
                    }

                    assignedGroups.Add(new EntityModel()
                    {
                        id = group.EntityID.ToString() + "_" + group.ID.ToString(),
                        label = group.Name,
                        icon = "/Assets/wfs/shim.gif",
                        isNode = true,
                        value = group.ID
                    });
                }
            }

            //add the entity children
            List<EntityModel> children = new List<EntityModel>();
            if (raamEntity.ChildEntities != null && raamEntity.ChildEntities.Count() > 0)
            {
                foreach (var child in raamEntity.ChildEntities)
                {
                    if (child.IsActive)
                    {
                        ConvertToEntityModel(child, children, client);
                    }
                }
            }

            if (assignedGroups != null && assignedGroups.Count > 0)
                children.AddRange(assignedGroups);

            entityModel.items = children;
            entities.Add(entityModel);

        }

        private static void ConvertToEntityModelHierarchy(EntityDto raamEntity, List<EntityModel> entities, RaamClient client)
        {
            if (raamEntity == null)
                return;

            var entityModel = new EntityModel()
            {
                id = raamEntity.ID.ToString(),
                label = raamEntity.Name,
                isNode = false,
                entityTypeCode = raamEntity.EntityTypeCode
            };

             //add the entity children
            List<EntityModel> children = new List<EntityModel>();
            if (raamEntity.ChildEntities != null && raamEntity.ChildEntities.Count() > 0)
            {
                foreach (var child in raamEntity.ChildEntities)
                {
                    ConvertToEntityModelHierarchy(child, children, client);
                }
            }

            entityModel.items = children;
            entities.Add(entityModel);

        }

		private static List<WorkgroupNameDTO> GetActiveWorkgroups(EntityDto raamEntity)
		{
			var client = new R360ServiceManager();
			var response = client.GetActiveWorkgroups(ConvertToR360EntityDTO(raamEntity));
			if (response.Status != StatusCode.SUCCESS)
				throw new Exception("Error occurred getting active workgroups.");

			return response.Data as List<WorkgroupNameDTO>;
		}

        private static List<WorkgroupNameDTO> GetIntegraPayOnlyWorkgroups(EntityDto raamEntity)
        {
            var client = new R360ServiceManager();
            var response = client.GetIntegraPayOnlyWorkgroups(ConvertToR360EntityDTO(raamEntity));
            if (response.Status != StatusCode.SUCCESS)
                throw new Exception("Error occurred getting active workgroups.");

            return response.Data as List<WorkgroupNameDTO>;
        }

		private static EntityDTO ConvertToR360EntityDTO(EntityDto raamEntity)
		{
			return raamEntity == null ? null :
				new EntityDTO()
				{
					EntityId = raamEntity.ID,
					ChildEntities = raamEntity.ChildEntities == null ? null : raamEntity.ChildEntities.Select(o => ConvertToR360EntityDTO(o)).ToList()
				};
		}

        private static void FilterRecHubUsers(List<AuditUserDTO> hubUsers, List<UserDto> raamUsers)
        {
            for (int i = raamUsers.Count-1; i > -1; i--)
            {
                var userId = hubUsers.FirstOrDefault(c => c.SID == raamUsers[i].UserSID);
                if (userId != null)
                {
                    raamUsers[i].ID = userId.UserID;
                }
                else
                {
                    raamUsers.RemoveAt(i);
                }
            }
        }
	}
}