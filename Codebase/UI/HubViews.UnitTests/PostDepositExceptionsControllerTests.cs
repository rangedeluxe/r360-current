﻿using HubViews.Controllers;
using HubViews.Models;
using HubViews.UnitTests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Collections.Generic;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.UnitTests
{
    [TestClass]
    public class PostDepositExceptionsControllerTests
    {
        private PostDepositTransactionDetailRequestModel TransactionRequest => new PostDepositTransactionDetailRequestModel()
        {
            BatchId = 1,
            DepositDate = "5/4/2016",
            TransactionId = 1,
            RecordsTotal = 1
        };

        private PostDepositExceptionsController PopulateController(MockR360Services r360Services = null)
        {
            return new PostDepositExceptionsController(
                r360Services ?? new MockR360Services(),
                new MockRaamClient(),
                new MockContextContainer(),
                new MockPostDepositExceptionServices()
            );
        }

        [TestMethod]
        public void AutoMapperConfigurationIsValid()
        {
            PostDepositExceptionsController.CreateMapperConfiguration().AssertConfigurationIsValid();
        }
        [TestMethod]
        public void PostDepositExceptions_ShouldReturnView()
        {
            var controller = PopulateController();
            var view = controller.Index();
            Assert.IsNotNull(view);
        }

        [TestMethod]
        public void PostDepositExceptions_ShouldReturnValidTransactions()
        {
            var controller = PopulateController();
            var result = controller.GetTransactions(new Models.PostDepositTransactionRequestModel());
            var list = ((JArray)JObject.Parse(JsonConvert.SerializeObject(result.Data))["data"])
                .ToList();
            Assert.IsTrue(list.Any());
        }

        [TestMethod]
        public void PostDepositExceptionsDetails_ShouldReturnView()
        {
            var controller = PopulateController();
            var view = controller.Detail(TransactionRequest);
            Assert.IsNotNull(view);
        }

        [TestMethod]
        public void PostDepositExceptionsDetails_ShouldReturnValidTransaction()
        {
            var controller = PopulateController();
            var result = controller.GetTransactionDetail(TransactionRequest);
            var json = JObject.Parse(JsonConvert.SerializeObject(result.Data));
            var data = json["data"];
            Assert.AreEqual("Sailu-Never Forget", data["Workgroup"]);

        }

        [TestMethod]
        public void PostDepositExceptionsDetails_Automatically_Adds_Related_Item()
        {
            var controller = PopulateController();
            var transactionDetail = new PostDepositTransactionDetailResponseDto
            {
                Stubs = new List<PostDepositStubDetailDto>(),
                DataEntrySetupList = new[]
                {
                    new DataEntrySetupDto
                    {
                        DataType = 1,
                        FieldName = "test",
                        IsRequired = true,
                        TableName = "test",
                        Title = "test"
                    }
                }
            };
            Assert.IsFalse(transactionDetail.Stubs.Any());

            controller.AddNewRelatedItemIfRequired(transactionDetail);

            Assert.IsTrue(transactionDetail.Stubs.Any());
            Assert.IsTrue(transactionDetail.Stubs.First().DataEntryFields.Any());
            Assert.IsTrue(transactionDetail.Stubs.First().DataEntryFields.First().FieldName == "test");
        }

        [TestMethod]
        public void PostDepositExceptionsDetails_WhenStubPresent_ShouldNotAddRelatedItem()
        {
            var controller = PopulateController();
            var transactionDetail = new PostDepositTransactionDetailResponseDto
            {
                Stubs = new List<PostDepositStubDetailDto>
                {
                    //Create an empty stub
                    new PostDepositStubDetailDto()
                },
                DataEntrySetupList = new[]
                {
                    new DataEntrySetupDto
                    {
                        DataType = 1,
                        FieldName = "test",
                        IsRequired = true,
                        TableName = "test",
                        Title = "test"
                    }
                }
            };
            Assert.IsTrue(transactionDetail.Stubs.Any());
            controller.AddNewRelatedItemIfRequired(transactionDetail);
            //should not add any new stubs.
            Assert.AreEqual(1,transactionDetail.Stubs.Count());
        }

        [TestMethod]
        public void PostDepositExceptionsSummary_AuditMessage_ShouldPass()
        {

            var r360Services = new MockR360Services();
            var controller = PopulateController(r360Services);

            var view = controller.Index();

            Assert.AreEqual("Post-Deposit Exceptions Summary page was viewed by Peanut Butter", r360Services.UserAuditMessages[0].AuditMessage);
            Assert.AreEqual("Page View", r360Services.UserAuditMessages[0].EventType);
        }

        [TestMethod]
        public void PostDepositExceptionsTransactions_AuditMessage_ShouldPass()
        {

            var r360Services = new MockR360Services();
            var controller = PopulateController(r360Services);

            var results = controller.GetTransactions(new PostDepositTransactionRequestModel
            {
                Draw = 0,
                Length = 0,
                OrderBy = "BatchID",
                OrderByDirection = "ASC",
                Search = "",
                Start = 0
            });

            Assert.AreEqual("Peanut Butter viewed pending exception transactions on the Post-Deposit Exceptions Summary page.", r360Services.UserAuditMessages[0].AuditMessage);
            Assert.AreEqual("Post-Deposit Exceptions", r360Services.UserAuditMessages[0].EventType);
        }
    }
}