﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.R360Services.Common;

namespace HubViews.UnitTests.Mocks
{
    public class MockExceptionServices : IExceptionServices
    {
        public OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            return new OLDBaseGenericResponse<DecisioningBatchDto>()
            {
                Data = new DecisioningBatchDto()
                {
                    DeadLine = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0)
                }
            };
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            throw new NotImplementedException();
        }

        public PendingBatchesResponse GetPendingBatches()
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<string> Ping()
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId)
        {
            throw new NotImplementedException();
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            throw new NotImplementedException();
        }
    }
}
