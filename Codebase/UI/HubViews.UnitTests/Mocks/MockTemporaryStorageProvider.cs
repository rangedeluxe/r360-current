﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360WebShared.Services;

namespace HubViews.UnitTests.Mocks
{
    public class MockTemporaryStorageProvider : ITemporaryStorageProvider
    {
        public readonly List<KeyValuePair<string, object>> _storage;

        public MockTemporaryStorageProvider()
        {
            _storage = new List<KeyValuePair<string, object>>();
        }

        public object GetValue(string key)
        {
            var old = _storage.FirstOrDefault(x => x.Key == key);
            if (old.Equals(default(KeyValuePair<string, object>)))
                return null;
            return old.Value;
        }

        public void SetValue(string key, object value)
        {
            var old = _storage.FirstOrDefault(x => x.Key == key);
            if (!old.Equals(default(KeyValuePair<string, object>)))
                _storage.Remove(old);
            _storage.Add(new KeyValuePair<string, object>(key, value));
        }
    }
}
