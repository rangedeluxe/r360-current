﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;

namespace HubViews.UnitTests.Mocks
{
    public class MockRaamClient : IRaamClient
    {
        public bool CreateOrUpdateWorkgroupResource(int entityID, int siteBankID, int workgroupID, string name)
        {
            throw new NotImplementedException();
        }

        public EntityDto GetAncestorEntityHierarchy(int entityID)
        {
            throw new NotImplementedException();
        }

        public EntityDto GetAncestorFIEntity(int entityID)
        {
            throw new NotImplementedException();
        }

        public EntityDto GetAuthorizedEntities()
        {
            throw new NotImplementedException();
        }

        public EntityDto GetAuthorizedEntitiesAndWorkgroups()
        {
            throw new NotImplementedException();
        }

        public EntityDto GetEntitiesWithPermission(int rootEntityId, string asset, R360Permissions.ActionType action)
        {
            throw new NotImplementedException();
        }

        public string GetEntityBreadcrumb(int entityID)
        {
            throw new NotImplementedException();
        }

        public bool GetEntityBreadcrumbList(int entityId, List<EntityBreadcrumbDTO> entityBreadcrumbs, bool includeHierarchy)
        {
            throw new NotImplementedException();
        }

        public EntityDto GetEntityGroupHierarchy()
        {
            throw new NotImplementedException();
        }

        public EntityDto GetEntityHierarchy(int entityID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EntityDto> GetAuthorizedEntityList()
        {
            throw new NotImplementedException();
        }

        public string GetEntityName(int entityID)
        {
            throw new NotImplementedException();
        }

        public string GetNameFromSid(Guid sid)
        {
            return "Peanut Butter";
        }

        public IEnumerable<string> GetEventTypes()
        {
            throw new NotImplementedException();
        }

        public IList<EntityDto> GetFIEntities()
        {
            throw new NotImplementedException();
        }

        public EntityDto GetLoggedInEntity()
        {
            throw new NotImplementedException();
        }

        public UserDto GetUserByUserSID(Guid userSID)
        {
            return new UserDto()
            {
                LoginId = "NameLastName",
                Person = new PersonDto()
                {
                    FirstName = "Name",
                    LastName = "LastName"
                }
            };
        }

        public IEnumerable<UserDto> GetUserHierarchy()
        {
            throw new NotImplementedException();
        }

        public IList<int> GetUsersFIEntitiesIds()
        {
            throw new NotImplementedException();
        }

        public ResourceDto GetWorkgroupResource(int siteBankID, int workgroupID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ResourceDto> GetWorkgroupsForEntity(int entityid)
        {
            throw new NotImplementedException();
        }

        public void RemoveWorkgroupResource(int entityID, int siteBankID, int workgroupID)
        {
            throw new NotImplementedException();
        }
    }
}
