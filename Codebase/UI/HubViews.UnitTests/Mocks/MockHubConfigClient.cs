﻿using System;
using System.Collections.Generic;
using WFS.RecHub.HubConfig;
using WFS.RecHub.HubConfig.DTO;
using WFS.RecHub.R360Shared;

namespace HubViews.UnitTests.Mocks
{
    public class MockHubConfigClient : IHubConfig
    {
        public HubConfigResponse<int> AddBank(BankDto bank)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> AddEventRule(EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> AddPaymentSource(PaymentSourceDto paymentSource)
        {
            throw new NotImplementedException();
        }

        public BaseResponse AddUpdateEntity(RecHubEntityDto entity)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> AddWorkgroup(WorkgroupDto workgroup)
        {
            throw new NotImplementedException();
        }

        public BaseResponse DeletePaymentSource(PaymentSourceDto paymentSource)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<BankDto> GetBank(int bankKey)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<BankDto>> GetBanks()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<BankDto>> GetBanksForFI(int fiEntityID)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<BatchPaymentSubTypeDto>> GetBatchPaymentSubTypes()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<Tuple<int, string>>> GetBatchPaymentTypes()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<Tuple<int, string>>> GetBatchSources(int fiEntityID)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<DataEntryTemplateDto>> GetDataEntryTemplates()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<DocumentTypeDto>> GetDocumentTypes()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<ElectronicAccountDto> GetElectronicAccountByDDA(DDADto dda)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<RecHubEntityDto> GetEntity(int entityId)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<EventRuleDto> GetEventRule(int eventRuleID)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<EventRuleDto>> GetEventRules()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<EventDto>> GetEvents()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<BusinessRulesDto>> GetFieldValidationSettings(int siteBankID, int workgroupId)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> WriteAuditConfigReport(AuditConfigDto dto)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<NotificationTypeDto>> GetNotificationTypes()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<RecHubEntityDto> GetOrCreateEntityWorkgroupDefaults(int entityId)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<PaymentSourceDto> GetPaymentSource(int batchSourceKey)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<PaymentSourceDto>> GetPaymentSources()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<PreferenceDto>> GetPreferences()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> GetSystemMaxRetentionDays()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<Tuple<int, string>>> GetSystemTypes()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> GetUnassignedWorkgroupCount(int fiEntityID)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<WorkgroupDto>> GetUnassignedWorkgroups(int fiEntityID)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<Tuple<int, string>>> GetValidationTableTypes()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<WorkgroupDto> GetWorkgroup(int clientAccountKey, GetWorkgroupAdditionalData additionalData)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<WorkgroupDto> GetWorkgroupByWorkgroupAndBank(int siteClientAccountId, int siteBankId,
            GetWorkgroupAdditionalData additionalData)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<WorkgroupDto>> GetWorkgroups(int entityID)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> InsertNotificationType(NotificationTypeDto type)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<string> Ping()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> RemoveWorkgroupFromEntity(int clientAccountKey)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> SaveAssignments(List<WorkgroupDto> workgroups)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> UpdateBank(BankDto bank)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> UpdateDocumentType(DocumentTypeDto type)
        {
            throw new NotImplementedException();
        }

        public BaseResponse UpdateEventRule(EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> UpdateNotificationType(NotificationTypeDto type)
        {
            throw new NotImplementedException();
        }

        public BaseResponse UpdatePaymentSource(PaymentSourceDto paymentSource)
        {
            throw new NotImplementedException();
        }

        public BaseResponse UpdatePreferences(List<PreferenceDto> preferences)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> UpdateWorkgroup(WorkgroupDto workgroup)
        {
            throw new NotImplementedException();
        }
    }
}
