﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace HubViews.UnitTests.Mocks
{
    public class MockR360Services : IR360Services
    {
        public List<MockUserAuditMessage> UserAuditMessages = new List<MockUserAuditMessage>();

        public PermissionsResponse CheckUserPermissionsForFunction(string function, string mode)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> DeleteStoredQuery(Guid entity)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<List<WorkgroupNameDTO>> GetActiveWorkgroups(EntityDTO entity)
        {
            throw new NotImplementedException();
        }

        public AdvancedSearchResultsDto GetAdvancedSearch(AdvancedSearchDto requestContext)
        {
            var result = new AdvancedSearchResultsDto()
            {
                Metadata = new AdvancedSearchMetadata()
                {
                    CriteraString = "CriteriaString",
                    DisplayBatchId = true,
                    HasDocuments = true,
                    HasDownloadPermission = true,
                    HasPayments = true,
                    MaxRowsForDownload = 10,
                    WorkgroupId = 10,
                    WorkgroupName = "WorkgroupName"
                },
                Totals = new AdvancedSearchResponseDto()
                {
                    CheckCount = 1,
                    CheckTotal = 10,
                    DocumentCount = 1,
                    TotalRecords = 1
                },
                Results = new List<AdvancedSearchResultDto>()
                {
                    new AdvancedSearchResultDto()
                    {
                        BankId = 1,
                        BatchId = 1,
                        BatchNumber = 1,
                        BatchSequence = 1,
                        BatchSourceShortName = "BatchSource",
                        CheckCount = 1,
                        DepositDate = DateTime.Now,
                        DepositDateString = DateTime.Now.ToString("MM/dd/yyyy"),
                        DocumentCount = 1,
                        ImportTypeShortName = "ImportType",
                        IsMarkSense = true,
                        MarkSenseDocumentCount = 1,
                        PaymentAmount = 1,
                        PaymentSource = "PaymentSource",
                        PaymentSourceId = 1,
                        PaymentType = "PaymentType",
                        PaymentTypeId = 1,
                        PICSDate = DateTime.Now,
                        PICSDateInt = int.Parse(DateTime.Now.ToString("yyyyMMdd")),
                        ShowAllIcon = true,
                        ShowDocumentIcon = true,
                        ShowPaymentIcon = true,
                        SourceBatchId = 1,
                        TransactionId = 1,
                        TransactionSequence = 1,
                        WorkgroupId = 10,
                        DataEntry = new List<DataEntryDto>()
                        {
                            new DataEntryDto()
                            {
                                FieldName = "Amount",
                                Title = "Amount",
                                Type = ((int)AdvancedSearchWhereClauseDataType.Decimal).ToString(),
                                Value = "10.10"
                            }
                        }
                    },
                    new AdvancedSearchResultDto()
                    {
                        BankId = 1,
                        BatchId = 1,
                        BatchNumber = 1,
                        BatchSequence = 2,
                        BatchSourceShortName = "BatchSource",
                        CheckCount = 1,
                        DepositDate = DateTime.Now,
                        DepositDateString = DateTime.Now.ToString("MM/dd/yyyy"),
                        DocumentCount = 1,
                        ImportTypeShortName = "ImportType",
                        IsMarkSense = true,
                        MarkSenseDocumentCount = 1,
                        PaymentAmount = 1,
                        PaymentSource = "PaymentSource",
                        PaymentSourceId = 1,
                        PaymentType = "PaymentType",
                        PaymentTypeId = 1,
                        PICSDate = DateTime.Now,
                        PICSDateInt = int.Parse(DateTime.Now.ToString("yyyyMMdd")),
                        ShowAllIcon = true,
                        ShowDocumentIcon = true,
                        ShowPaymentIcon = true,
                        SourceBatchId = 1,
                        TransactionId = 1,
                        TransactionSequence = 1,
                        WorkgroupId = 10,
                        DataEntry = new List<DataEntryDto>()
                        {
                            new DataEntryDto()
                            {
                                FieldName = "Amount",
                                Title = "Amount",
                                Type = ((int)AdvancedSearchWhereClauseDataType.Decimal).ToString(),
                                Value = "5050.50"
                            }
                        }
                    }

                },
            };
            return result;
        }

        public BaseGenericResponse<List<AuditUserDTO>> GetAuditUsers()
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<string> GetBankName(int siteBankId)
        {
            return new BaseGenericResponse<string>()
            {
                Data = "BankName"
            };
        }

        public BatchDetailResponseDto GetBatchDetail(BatchDetailRequestDto requestContext)
        {
            throw new NotImplementedException();
        }

        public BatchSummaryResponseDto GetBatchSummary(BatchSummaryRequestDto requestContext)
        {
            throw new NotImplementedException();
        }

        public ClientAccountsResponse GetClientAccounts()
        {
            throw new NotImplementedException();
        }

        public ClientAccountsResponse GetClientAccountsForOrganization(Guid OLOrganizationID)
        {
            throw new NotImplementedException();
        }

        public SummariesResponse GetClientAccountSummary(DateTime summaryDate)
        {
            throw new NotImplementedException();
        }

        public DataEntryResponseDTO GetDataEntryFieldsForWorkgroup(DataEntrySearchDTO search)
        {
            var response = new DataEntryResponseDTO()
            {
                Status = StatusCode.SUCCESS,
                DataEntryFields = new List<DataEntryResponse>()
                {
                    new DataEntryResponse()
                    {
                        BatchSourceKey = 255,
                        DataType = (int)AdvancedSearchWhereClauseDataType.Decimal,
                        DisplayName = "Amount",
                        FldName = "Amount",
                        TableName = "Checks"
                    }
                }
            };
            return response;
        }

        public BaseGenericResponse<List<DDASummaryDataDto>> GetDDASummary(DateTime depositDate)
        {
            throw new NotImplementedException();
        }

        public ExceptionTypeSummaryResponse GetExceptionTypeSummary(EntityDTO entity, WorkgroupIDsDTO workgroup, DateTime datefrom, DateTime dateto)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<List<WorkgroupNameDTO>> GetIntegraPayOnlyWorkgroups(EntityDTO entity)
        {
            throw new NotImplementedException();
        }

        public NotificationResponseDto GetNotification(NotificationRequestDto request)
        {
            throw new NotImplementedException();
        }

        public NotificationsResponseDto GetNotifications(NotificationsRequestDto request)
        {
            throw new NotImplementedException();
        }

        public NotificationsFileTypesResponseDto GetNotificationsFileTypesResponse()
        {
            throw new NotImplementedException();
        }

        public PaymentSearchResponseDto GetPaymentSearch(PaymentSearchRequestDto request)
        {
            throw new NotImplementedException();
        }

        public InvoiceSearchResponseDto GetInvoiceSearch(InvoiceSearchRequestDto request)
        {
            throw new NotImplementedException();
        }

        public PaymentSourcesResponseDto GetPaymentSources()
        {
            throw new NotImplementedException();
        }

        public PaymentTypesResponseDto GetPaymentTypes()
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<long> GetSourceBatchID(long batchID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<List<AdvancedSearchStoredQueryDto>> GetStoredQueries()
        {
            return new BaseGenericResponse<List<AdvancedSearchStoredQueryDto>>()
            {
                Status = StatusCode.SUCCESS,
                Data = new List<AdvancedSearchStoredQueryDto>()
                {
                    new AdvancedSearchStoredQueryDto()
                    {
                        Id = Guid.NewGuid(),
                        Description = "Query",
                        IsDefault = true,
                        Name = "Query",
                    }
                }
            };
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> GetStoredQuery(Guid id)
        {
            return new BaseGenericResponse<AdvancedSearchStoredQueryDto>()
            {
                Status = StatusCode.SUCCESS,
                Data = new AdvancedSearchStoredQueryDto()
                {
                    Id = id,
                    Description = "Query",
                    IsDefault = true,
                    Name = "Query",
                }
            };
        }

        public TransactionDetailResponseDto GetTransactionDetail(TransactionDetailRequestDto requestContext)
        {
            throw new NotImplementedException();
        }

        public PingResponse Ping()
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> SaveStoredQuery(AdvancedSearchStoredQueryDto entity)
        {
            var response = new BaseGenericResponse<AdvancedSearchStoredQueryDto>()
            {
                Data = entity,
                Status = StatusCode.SUCCESS
            };
            response.Data.Id = Guid.NewGuid();
            return response;
        }

        public BaseResponse WriteAuditEvent(string eventName, string eventType, string applicationName, string description)
        {
            UserAuditMessages.Add(new MockUserAuditMessage
            {
                AuditMessage = description,
                EventType = eventType,
            });

            return new BaseResponse()
            {
                Status = StatusCode.SUCCESS
            };
        }

        public SummariesResponse GetClientAccountSummary(ReceivablesSummaryRequestDto request)
        {
            throw new NotImplementedException();
        }
    }
}
