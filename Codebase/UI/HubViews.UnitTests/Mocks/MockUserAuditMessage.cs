﻿namespace HubViews.UnitTests.Mocks
{
    public class MockUserAuditMessage
    {
        public string EventType { get; set; }
        public string AuditMessage { get; set; }
    }
}
