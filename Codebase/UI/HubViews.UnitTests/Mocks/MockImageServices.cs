﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.R360Services.Common;

namespace HubViews.UnitTests.Mocks
{
    public class MockImageServices : IImageServices
    {
        public Guid GeneratedGuid { get; set; }

        public MockImageServices()
        {
            GeneratedGuid = Guid.NewGuid();
        }

        public BaseGenericResponse<System.Data.DataSet> CheckImageJobStatus(Guid OnlineImageQueueID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<System.Data.DataSet> CreateSearchResultsJobRequest(System.Xml.XmlDocument SearchParms, System.Xml.XmlDocument JobParms)
        {
            var table = new DataTable();

            table.Columns.Add("OnlineImageQueueID", typeof(Guid));
            var row = table.NewRow();
            row["OnlineImageQueueID"] = GeneratedGuid;
            table.Rows.Add(row);

            var set = new DataSet();
            set.Tables.Add(table);

            return new BaseGenericResponse<DataSet>()
            {
                Data = set,
                Status = WFS.RecHub.R360Shared.StatusCode.SUCCESS
            };
        }

        public BaseGenericResponse<System.Data.DataSet> CreateViewAllJobRequest(System.Xml.XmlDocument Parms)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<System.Data.DataSet> CreateViewSelectedJobRequest(System.Xml.XmlDocument Parms)
        {
            var table = new DataTable();

            table.Columns.Add("OnlineImageQueueID", typeof(Guid));
            var row = table.NewRow();
            row["OnlineImageQueueID"] = GeneratedGuid;
            table.Rows.Add(row);

            var set = new DataSet();
            set.Tables.Add(table);

            return new BaseGenericResponse<DataSet>()
            {
                Data = set,
                Status = WFS.RecHub.R360Shared.StatusCode.SUCCESS
            };
        }
        public BaseGenericResponse<DataSet> CreateInProcessExceptionJobRequest(InProcessExceptionImageRequestDto request)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<System.Data.DataSet> CreateViewSpecifiedJobRequest(System.Xml.XmlDocument Parms)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<Stream> GetImageJobFileAsAttachment(Guid OnlineImageQueueID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<byte[]> GetImageJobFileAsBuffered(Guid OnlineImageQueueID)
        {
            throw new NotImplementedException();
        }

        public PingResponse Ping()
        {
            throw new NotImplementedException();
        }
    }
}
