﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.UnitTests.Mocks
{
    public class MockUserServices : IUserServices
    {
        public XmlDocumentResponse GetOLUserPreferences()
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetOnlineUserByID(int UserID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<WFS.RecHub.Common.cUserRAAM> GetUserByID(int UserID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<WFS.RecHub.Common.cOLLockboxes> GetUserLockboxes()
        {
            var data = new WFS.RecHub.Common.cOLLockboxes();
            data.Add(0, new cOLLockbox()
            {
                BankID = 1,
                LockboxID = 2,
                DisplayBatchID = true
            });
            return new BaseGenericResponse<WFS.RecHub.Common.cOLLockboxes>()
            {
                Data = data
            };
        }

        public BaseGenericResponse<UserPreferencesDTO> GetUserPreferences()
        {
            throw new NotImplementedException();
        }

        public PingResponse Ping()
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse RestoreDefaultOLPreferences()
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<UserPreferencesDTO> RestoreDefaultPreferences()
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse SetOLUserPreferences(XmlDocument Parms)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<bool> SetUserPreferences(UserPreferencesDTO Preferences)
        {
            throw new NotImplementedException();
        }
    }
}
