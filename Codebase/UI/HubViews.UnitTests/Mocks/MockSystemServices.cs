﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

namespace HubViews.UnitTests.Mocks
{
    public class MockSystemServices : ISystemServices
    {
        public XmlDocumentResponse GetBaseDocument()
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<cOLLockbox> GetLockBoxByAccountIDBankID(int BankID, int LockboxID)
        {
            return new BaseGenericResponse<cOLLockbox>()
            {
                Status = StatusCode.SUCCESS,
                Data = new cOLLockbox()
                {
                    BankID = BankID,
                    LockboxID = LockboxID,
                    LongName = "Workgroup"
                }
            };
        }

        public BaseGenericResponse<cOLLockbox> GetLockboxByID(int OLLockboxID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<cUserRAAM> GetSessionUser()
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetUserLockboxes(int UserID)
        {
            throw new NotImplementedException();
        }

        public PageActivityResponse InitPageActivity(string ScriptName, string QueryString)
        {
            throw new NotImplementedException();
        }

        public BaseResponse LogActivity(WFS.RecHub.Common.ActivityCodes ActivityCode, Guid OnlineImageQueueID, int ItemCount, int BankID, int LockboxID)
        {
            throw new NotImplementedException();
        }

        public PingResponse Ping()
        {
            throw new NotImplementedException();
        }

        public BaseResponse SetWebDeliveredFlag(Guid OnlineImageQueueID, bool InternetDelivered)
        {
            throw new NotImplementedException();
        }

        public BaseResponse UpdateActivityLockbox(long ActivityLogID, int BankID, int LockboxID)
        {
            throw new NotImplementedException();
        }
    }
}
