﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wfs.Logging;

namespace HubViews.UnitTests.Mocks
{
    public class MockLogger : ILogSource
    {
        public string SourceName
        {
            get
            {
                return "MockLogger";
            }
        }

        public void Initialize(Type sourceType)
        {
            
        }

        public void Initialize(string sourceName)
        {
            
        }

        public bool IsLoggable(LogLevel logLevel)
        {
            return true;
        }

        public void Log(LogEntry logEntry)
        {
            Log(logEntry.LogLevel, logEntry.Message);
        }

        public void Log(LogEntry logEntry, int logId)
        {
            Log(logEntry);
        }

        public void Log(LogLevel logLevel, string message)
        {
            Console.WriteLine($"{logLevel.ToString()} : {message}");
        }

        public void Log(LogLevel logLevel, string message, int logId)
        {
            Log(logLevel, message);
        }

        public void Log(LogLevel logLevel, string messageFormat, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void Log(LogLevel logLevel, string messageFormat, int logId, params object[] args)
        {
            throw new NotImplementedException();
        }
    }
}
