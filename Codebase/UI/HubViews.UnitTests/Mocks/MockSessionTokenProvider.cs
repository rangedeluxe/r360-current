﻿using WFS.RecHub.R360WebShared.Services;

namespace HubViews.UnitTests.Mocks
{
    public class MockSessionTokenProvider : ISessionTokenProvider
    {
        public string GenerateSessionToken(string session)
        {
            return "111-111";
        }
    }
}
