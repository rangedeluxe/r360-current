﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace HubViews.UnitTests.Mocks
{
    public class MockContextContainer : IContextContainer
    {
        public IServiceContext Current
        {
            get
            {
                return new MockServiceContext();
            }
        }
    }
}
