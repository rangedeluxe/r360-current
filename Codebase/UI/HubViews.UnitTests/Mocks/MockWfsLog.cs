﻿using System;
using System.Collections;
using Wfs.Logging;

namespace HubViews.UnitTests.Mocks
{
    public class MockWfsLog : IWfsLog
    {
        public void Log(WfsLogLevel level, string message, IEnumerable args = null, string source = null, Exception exception = null)
        {
            //just do nothing in the mock for logging
        }
    }
}
