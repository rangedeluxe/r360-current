﻿using System;
using WFS.RecHub.R360Shared;

namespace HubViews.UnitTests.Mocks
{
    public class MockServiceContext : IServiceContext
    {
        public SimpleClaim[] RAAMClaims
        {
            get
            {
                return new SimpleClaim[]
                {
                    new SimpleClaim()
                    {
                        Type = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid",
                        Value = Guid.NewGuid().ToString()
                    }
                };
            }
        }

        public string SiteKey
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public string GetLoginNameForAuditing()
        {
            throw new NotImplementedException();
        }
        public Guid GetSessionID()
        {
            return Guid.NewGuid();
        }

        public Guid GetSID()
        {
            return new Guid();
        }
    }
}
