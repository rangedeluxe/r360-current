﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace HubViews.UnitTests.Mocks
{
    public class MockPostDepositExceptionServices : IPostDepositExceptionServices
    {
        public BaseGenericResponse<PostDepositTransactionsResponseDto> GetPendingTransactions(
            PostDepositPendingTransactionsRequestDto request)
        {
            return new BaseGenericResponse<PostDepositTransactionsResponseDto>()
            {
                Data = new PostDepositTransactionsResponseDto()
                {
                    TotalFilteredRecords = 1,
                    TotalRecords = 1,
                    Transactions = new List<PostDepositTransactionResponseDto>()
                    {
                        new PostDepositTransactionResponseDto()
                        {
                            TransactionId = 1
                        }
                    }
                },
                Status = StatusCode.SUCCESS
            };
        }

        public BaseGenericResponse<PostDepositTransactionDetailResponseDto> GetTransaction(
            PostDepositTransactionRequestDto request)
        {
            return new BaseGenericResponse<PostDepositTransactionDetailResponseDto>
            {
                Data = new PostDepositTransactionDetailResponseDto
                {
                    DepositDate = new DateTime(2017, 2, 1),
                    TransactionId = 1,
                    BatchId = 1,
                    BankId = 1,
                    EntityBreadcrumb = "wfs\\mrc",
                    PaymentSource = "Integrasomething",
                    PaymentType = "something else",
                    EntityId = 1,
                    Workgroup = "Sailu-Never Forget",
                    WorkgroupId = 1,
                    Transactions =
                        new[]
                        {
                            new PostDepositExceptionTransactionDto
                            {
                                TransactionId = 1,
                                DepositDate = new DateTime(2017, 2, 1),
                                BatchId = 1
                            }
                        },
                }
            };
        }
        public BaseGenericResponse<PostDepositTransactionValidationResponseDto> GetTransactionValidationResults(
            PostDepositTransactionDetailResponseDto request)
        {
            return new BaseGenericResponse<PostDepositTransactionValidationResponseDto>()
            {
                Data = new PostDepositTransactionValidationResponseDto()
                {
                    StubDataEntryExceptions = Enumerable.Empty<PostDepositStubDataEntryExceptionDto>()
                }
            };
        }

        public BaseResponse LockTransaction(LockTransactionRequestDto request)
        {
            return new BaseResponse()
            {
                Status = StatusCode.SUCCESS
            };
        }

        public BaseResponse UnlockTransaction(LockTransactionRequestDto request)
        {
            return new BaseResponse()
            {
                Status = StatusCode.SUCCESS
            };
        }

        public BaseResponse UnlockTransactionWithoutOverride(LockTransactionRequestDto request)
        {
            return new BaseResponse()
            {
                Status = StatusCode.SUCCESS
            };
        }

        public BaseGenericResponse<PostDepositSavePayerResultDto> SavePayer(PostDepositSavePayerDto request)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<PostDepositTransactionSaveResultDto> SaveTransaction(
            PostDepositTransactionSaveDto request)
        {
            throw new NotImplementedException();
        }

        public BaseResponse AuditTransactionDetailView(PostDepositExceptionTransactionDto request)
        {
            return new BaseResponse()
            {
                Status = StatusCode.SUCCESS
            };

        }

        public BaseGenericResponse<bool> CompleteTransaction(PostDepositTransactionRequestDto request)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<PayerResponseDto> GetPayerList(PayerLookupRequestDto request)
        {
            throw new NotImplementedException();
        }
    }
}
