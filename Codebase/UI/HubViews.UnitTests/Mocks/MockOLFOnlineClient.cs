﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OLFServices;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServicesClient;

namespace HubViews.UnitTests.Mocks
{
    public class MockOLFOnlineClient : IOLFOnlineClient
    {
        public WFS.RecHub.OLFServices.DataTransferObjects.ImageResponseDto DownloadImage(WFS.RecHub.OLFServices.FileMetaData metaData, WFS.RecHub.OLFServices.DataTransferObjects.ImageRequestDto request)
        {
            throw new NotImplementedException();
        }

        public ImageResponseDto DownloadSingleImage(SingleImageRequestDto request)
        {
            throw new NotImplementedException();
        }

        public Stream GetCENDSFileAsAttachment(string FilePath)
        {
            throw new NotImplementedException();
        }

        public long GetCENDSFileSize(WFS.RecHub.OLFServices.FileMetaData MetaData, string FilePath)
        {
            throw new NotImplementedException();
        }

        public Stream GetImageJobFileAsAttachment(Guid OnlineImageQueueID)
        {
            throw new NotImplementedException();
        }

        public long GetImageJobSize(WFS.RecHub.OLFServices.FileMetaData MetaData, Guid OnlineImageQueueID)
        {
            throw new NotImplementedException();
        }

        public long GetImageSize(WFS.RecHub.OLFServices.FileMetaData MetaData, byte OnlineColorMode, string PICSDate, int BankID, int LockboxID, long BatchID, int Item, string ImageType, short Page, long SourceBatchID, string BatchSourceShortName, string ImportTypeShortName)
        {
            throw new NotImplementedException();
        }

        ImageResponseDto IOLFOnlineClient.DownloadImage(FileMetaData metaData, ImageRequestDto request)
        {
            throw new NotImplementedException();
        }

        ImageResponseDto IOLFOnlineClient.DownloadSingleImage(SingleImageRequestDto request)
        {
            throw new NotImplementedException();
        }

        Stream IOLFOnlineClient.GetCENDSFileAsAttachment(string FilePath)
        {
            throw new NotImplementedException();
        }

        long IOLFOnlineClient.GetCENDSFileSize(FileMetaData MetaData, string FilePath)
        {
            throw new NotImplementedException();
        }

        Stream IOLFOnlineClient.GetImageJobFileAsAttachment(Guid OnlineImageQueueID)
        {
            throw new NotImplementedException();
        }

        long IOLFOnlineClient.GetImageJobSize(FileMetaData MetaData, Guid OnlineImageQueueID)
        {
            throw new NotImplementedException();
        }

        long IOLFOnlineClient.GetImageSize(FileMetaData MetaData, byte OnlineColorMode, string PICSDate, int BankID, int LockboxID, long BatchID, int Item, string ImageType, short Page, long SourceBatchID, string BatchSourceShortName, string ImportTypeShortName)
        {
            throw new NotImplementedException();
        }

        OlfResponseDto IOLFOnlineClient.ImagesExist(ImageRequestDto request)
        {
            throw new NotImplementedException();
        }
    }
}
