﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.R360Services.Common;

namespace HubViews.UnitTests.Mocks
{
    public class MockReportServices : IReportServices
    {
        public BaseGenericResponse<DataSet> CreateReportJobRequest(XmlDocument Parms)
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetHOAParameters(XmlDocument Parms)
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetHOAPMADetail(XmlDocument Parms)
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetHOAPMASummary(XmlDocument Parms)
        {
            throw new NotImplementedException();
        }

        public PingResponse Ping()
        {
            throw new NotImplementedException();
        }
    }
}
