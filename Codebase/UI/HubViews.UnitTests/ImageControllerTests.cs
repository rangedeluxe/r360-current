﻿using CommonUtils.Web;
using HubViews.Controllers;
using HubViews.Models;
using HubViews.UnitTests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Wfs.Logging;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.OLFServicesClient.Fakes;
using WFS.RecHub.R360Services.Common.Fakes;

namespace HubViews.UnitTests
{
    [TestClass]
    public class ImageControllerTests
    {
        private readonly ImageController _controller;
        private readonly MockImageServices _imageservices;

        public ImageControllerTests()
        {
            _imageservices = new MockImageServices();
            _controller = new ImageController(
                _imageservices,
                new MockReportServices(),
                new MockOLFOnlineClient(),
                new MockHubConfigClient(),
                new MockContextContainer(),
                new MockUserServices(),
                new MockWfsLog()
            );
        }

        [TestMethod]
        public void ImageController_ShallInstanciate_Correctly()
        {
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void ImageController_ShallGenerate_AdvancedSearchSelectedPDF_Request()
        {
            // Arrange
            var id = Guid.NewGuid();
            _imageservices.GeneratedGuid = id;
            var req = new AdvancedSearchSelectedRequest()
            {
                SelectMode = "All",
                Transactions = new List<AdvancedSearchTransactionRequest>()
                {
                    new AdvancedSearchTransactionRequest()
                    {
                        BankId = 1,
                        BatchId = 2,
                        BatchNumber = 3,
                        DepositDate = "11/7/2016",
                        TransactionId = 4,
                        WorkgroupId = 5
                    }
                }
            };

            // Act
            var result = (JsonResult)_controller.AdvancedSearchPDFSelectedRequest(req);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(id.ToString(), obj.GetValue("InstanceId").Value<string>());
        }

        [TestMethod]
        public void ImageController_ShallGenerate_AdvancedSearchDownload_Request()
        {
            // Arrange
            var id = Guid.NewGuid();
            _imageservices.GeneratedGuid = id;
            var req = new AdvancedSearchRequestModel()
            {
                DateFrom = "11/7/2016",
                DateTo = "11/7/2016",
                OrderBy = "OrderBy",
                OrderDirection = "Direction",
                start = 1,
                length = 10,
                Workgroup = "1|2",
                SelectFields = new List<WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseDto>()
            };

            // Act
            var result = (JsonResult)_controller.AdvancedSearchDownloadRequest(req);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(id.ToString(), obj.GetValue("InstanceId").Value<string>());
        }

        [TestMethod]
        public void ImageController_ShallGenerate_AdvancedSearchDownload_Request_WithCriteria()
        {
            // Arrange
            var id = Guid.NewGuid();
            _imageservices.GeneratedGuid = id;
            var req = new AdvancedSearchRequestModel()
            {
                DateFrom = "11/7/2016",
                DateTo = "11/7/2016",
                OrderBy = "OrderBy",
                OrderDirection = "Direction",
                start = 1,
                length = 10,
                Workgroup = "1|2",
                SelectFields = new List<WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseDto>()
                {
                    new WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 255,
                        DataType = WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseDataType.String,
                        FieldName = "Amount",
                        IsDataEntry = true,
                        ReportTitle = "Amount",
                        Table = WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseTable.Checks
                    }
                },
                Criteria = new List<AdvancedSearchCriteria>()
                {
                    new AdvancedSearchCriteria()
                    {
                        Column = new WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseDto()
                        {
                            BatchSourceKey = 255,
                            DataType = WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseDataType.String,
                            FieldName = "Amount",
                            IsDataEntry = true,
                            ReportTitle = "Amount",
                            Table = WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseTable.Checks
                        },
                        Operator = WFS.RecHub.R360Services.Common.DTO.AdvancedSearchWhereClauseOperator.IsGreaterThan,
                        Value = "100"
                    }
                }
            };

            // Act
            var result = (JsonResult)_controller.AdvancedSearchDownloadRequest(req);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(id.ToString(), obj.GetValue("InstanceId").Value<string>());
        }

        [TestMethod]
        public void Can_CheckIfImagesExist()
        {
            //arrange
            var expected = true;
            
            var olfOnlineClient = new StubIOLFOnlineClient();
            olfOnlineClient.ImagesExistImageRequestDto = (imageRequest) => new OlfResponseDto
            {
                IsSuccessful = true,
                ImagesAvailable = true
            };

            var controller = new ImageController(
                _imageservices,
                new MockReportServices(),
                olfOnlineClient,
                new MockHubConfigClient(),
                new MockContextContainer(),
                new MockUserServices(),
                new MockWfsLog()
            );

            //act
            var result = controller.ImagesExist(GetImageViewModel());

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.IsInstanceOfType(result.Data, typeof(JSONResponse));
            var jsonResponse = result.Data as JSONResponse;
            Assert.IsInstanceOfType(jsonResponse.Data, typeof(OlfResponseDto));
            var olfResponse = jsonResponse.Data as OlfResponseDto;
            Assert.AreEqual(expected, olfResponse.IsSuccessful);
        }


        [TestMethod]
        public void Will_HandleError_WhenCallingImageService()
        {
            //arrange
            var expected = true;
            
            var olfOnlineClient = new StubIOLFOnlineClient();
            olfOnlineClient.ImagesExistImageRequestDto = (imageRequest) => throw new NullReferenceException();

            var controller = new ImageController(
                _imageservices,
                new MockReportServices(),
                olfOnlineClient,
                new MockHubConfigClient(),
                new MockContextContainer(),
                new MockUserServices(),
                new MockWfsLog()
            );

            //act
            var result = controller.ImagesExist(GetImageViewModel());

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.IsInstanceOfType(result.Data, typeof(JSONResponse));
            var jsonResponse = result.Data as JSONResponse;
            Assert.IsNull(jsonResponse.Data);
            Assert.AreEqual(expected, jsonResponse.HasErrors);
        }

        [TestMethod]
        public void Will_DefaultValues_WhenImageViewModelIsNotSet()
        {
            //arrange
            var expected = false;

            var olfOnlineClient = new StubIOLFOnlineClient();
            olfOnlineClient.ImagesExistImageRequestDto = (imageRequest) => new OlfResponseDto
            {
                IsSuccessful = false,
                ImagesAvailable = false
            };

            var imageViewModel = new ImageViewModel
            {
                BatchPaymentSource = "UnitTestPaymentSource",
                DepositDateString = DateTime.Now.ToString("yyyy-MM-dd")
            };

            var controller = new ImageController(
                _imageservices,
                new MockReportServices(),
                olfOnlineClient,
                new MockHubConfigClient(),
                new MockContextContainer(),
                new MockUserServices(),
                new MockWfsLog()
            );

            //act
            var result = controller.ImagesExist(imageViewModel);

            //assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Data);
            Assert.IsInstanceOfType(result.Data, typeof(JSONResponse));
            var jsonResponse = result.Data as JSONResponse;
            Assert.IsInstanceOfType(jsonResponse.Data, typeof(OlfResponseDto));
            var olfResponse = jsonResponse.Data as OlfResponseDto;
            Assert.AreEqual(expected, olfResponse.IsSuccessful);
        }

        public ImageViewModel GetImageViewModel()
        {
            return new ImageViewModel
            {
                BankId = 1,
                BatchId = 1,
                PICSDate = DateTime.Now.ToString("yyyyMMdd"),
                ImportTypeShortName = "UnitTestImportType",
                BatchPaymentSource = "UnitTestPaymentSource",
                DepositDateString = DateTime.Now.ToString("yyyy-MM-dd"),
                SourceBatchId = 1,
                TransactionId = 1,
                WorkgroupId = 1
            };
        }
    }
}
