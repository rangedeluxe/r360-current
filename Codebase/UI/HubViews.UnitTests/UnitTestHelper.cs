﻿using System;
using System.Linq;
using Microsoft.QualityTools.Testing.Fakes.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HubViews.UnitTests
{
    internal static class UnitTestHelper
    {
        internal static void AssertWasCalled<MT>(this StubBase<MT> mocking, string name, int count = 1) where MT : class
        {
            var observer = (StubObserver) mocking.InstanceObserver;
            Assert.AreEqual(count, observer.GetCalls().Count(call => call.StubbedMethod.Name == name));
        }

        internal static TMock MockFor<TMock>(this TMock mocking, Action<TMock> loader = null) where TMock : StubBase
        {
            mocking.InstanceObserver = new StubObserver();
            if (loader != null)
                loader(mocking);
            return mocking;
        }
    }
}