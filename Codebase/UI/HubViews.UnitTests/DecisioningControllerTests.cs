﻿using HubViews.Models;
using HubViews.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using OLDecisioningShared.DTO;
using Wfs.Logging;
using WFS.RecHub.HubConfig;
using HubViews.UnitTests.Mocks;
using System.Web.Mvc;

namespace HubViews.UnitTests
{
    [TestClass]
    public class DecisioningControllerTests
    {
        private readonly DecisioningController controller;

        public DecisioningControllerTests()
        {
            controller = new DecisioningController(
                new MockExceptionServices(), 
                new MockRaamClient(), 
                new MockR360Services(), 
                new MockContextContainer());
        }
        
        [TestMethod]
        public void PrintableView_ShouldFormatDeadline_ForValidTimeZone()
        {
            var result = (ViewResult)controller.AllTransactionsPrintableView(1, -300, "Central Daylight Time");
            var model = (DecisioningBatchModel)result.Model;

            Assert.AreEqual("07:00 am Central Daylight Time", model.DeadLine);
        }

        [TestMethod]
        public void PrintableView_ShouldFormatDeadline_ForInvalidTimeZone()
        {
            var result = (ViewResult)controller.AllTransactionsPrintableView(1, -300, "Central Daylight Time1");
            var model = (DecisioningBatchModel)result.Model;

            Assert.AreEqual("12:00 pm Coordinated Universal Time", model.DeadLine);
        }

        [TestMethod]
        public void CalculateTransactionTotals_ShouldPass()
        {
            var transaction = new DecisioningTransactionDto()
            {
                Payments = new[] {
                    NewPayment("First",   100.00m),
                    NewPayment("Second", 1000.00m),
                    NewPayment("Third",   100.00m),
                    NewPayment("Fourth",   10.10m),
                    NewPayment("Fifth",     5.03m),
                },
                Stubs = new[] {
                    NewStub("1st",   50.00m),
                    NewStub("2nd",    5.00m),
                    NewStub("3rd",   50.00m),
                    NewStub("4th", 1000.00m),
                    NewStub("5th",    0.10m)
                }
            };

            var expectedPaymentTotal = 1215.13m;
            var expectedStubTotal = 1105.10m;

            var totals = DecisioningController.CalculateTransactionTotals(transaction);

            Assert.AreEqual(expectedPaymentTotal, totals.PaymentTotal);
            Assert.AreEqual(expectedStubTotal, totals.StubsTotal);
        }

        [TestMethod]
        public void IsBalanced_ShouldPass_PaymentsEqualStubs()
        {
            DecisioningTransactionDto transaction = new DecisioningTransactionDto()
            {
                Payments = new DecisioningPaymentDto[] {
                    NewPayment("First", 1000.00m)
                },
                Stubs = new DecisioningStubDto[] {
                    NewStub("1st", 1000.00m)
                }
            };
            Assert.IsTrue(DecisioningController.IsBalanced(transaction));
        }

        [TestMethod]
        public void ValidTransactionBalance_ShouldPass_BalancedTransaction()
        {
            var transaction = new DecisioningTransactionDto()
            {
                Payments = new DecisioningPaymentDto[] {
                    NewPayment("First", 1000.00m)
                },
                Stubs = new DecisioningStubDto[] {
                    NewStub   ("First", 1000.00m)
                }
            };
            var businessRules = new WorkgroupBusinessRulesDto()
            {
                PreDepositBalancingRequired = true
            };
            Assert.IsTrue(DecisioningController.ValidTransactionBalance(businessRules, transaction));
        }

        [TestMethod]
        public void ValidTransactionBalance_ShouldFail_UnbalancedTransaction()
        {
            var transaction = new DecisioningTransactionDto()
            {
                Payments = new DecisioningPaymentDto[] {
                    NewPayment("First",    8.00m)
                },
                Stubs = new DecisioningStubDto[] {
                    NewStub   ("First", 1000.00m)
                }
            };
            var businessRules = new WorkgroupBusinessRulesDto()
            {
                PreDepositBalancingRequired = true
            };
            Assert.IsFalse(DecisioningController.ValidTransactionBalance(businessRules, transaction));
        }

        [TestMethod]
        public void ValidTransactionBalance_ShouldPass_NoBalancingRequired()
        {
            var transaction = new DecisioningTransactionDto()
            {
                Payments = new DecisioningPaymentDto[] {
                    NewPayment("First",    8.00m)
                },
                Stubs = new DecisioningStubDto[] {
                    NewStub   ("First", 1000.00m)
                }
            };
            var businessRules = new WorkgroupBusinessRulesDto()
            {
                PreDepositBalancingRequired = false
            };
            Assert.IsTrue(DecisioningController.ValidTransactionBalance(businessRules, transaction));
        }

        private DecisioningStubDto NewStub(string account, decimal amount)
        {
            return new DecisioningStubDto()
            {
                Account = account,
                Amount = amount,
                DataEntry = new DecisioningDataEntryItemFieldDto[] { },
                DEItemRowDataID = Guid.NewGuid()
            };
        }

        public static DecisioningPaymentDto NewPayment(string account, decimal amount)
        {
            return new DecisioningPaymentDto()
            {
                Account = account,
                Amount = amount,
                BatchSequence = 1,
                CheckSequence = 1,
                DataEntry = new DecisioningDataEntryDto[] { },
                DecisioningReasonDesc = "Testing",
                DecisioningReasonID = null,
                GlobalCheckID = 1,
                PayeeID = 1,
                PayeeName = "My Tester",
                RT = "112358",
                RemitterName = "Tester",
                Serial = "31415",
                TraceNumber = "00001",
                TransactionCode = "TEST"
            };
        }
    }
}
