using System;
using System.Collections.Generic;
using System.Linq;

namespace HubViews.UnitTests
{
    public static class TypeExtensions
    {
        /// <summary>
        /// <para>
        ///     If the given type is a collection (i.e., implements <see cref="IEnumerable{T}"/>),
        ///     returns the collection element type (the T from IEnumerable&lt;T&gt;).
        ///     Otherwise, returns null.
        /// </para>
        /// <para>
        ///     Strings are not considered collections even though they implement IEnumerable&lt;char&gt;.
        /// </para>
        /// </summary>
        public static Type GetCollectionItemType(this Type propertyType)
        {
            // Strings implement IEnumerable<char>, but we don't consider them collections
            if (propertyType == typeof(string))
                return null;

            return new[] {propertyType}.Concat(propertyType.GetInterfaces())
                .FirstOrDefault(IsIEnumerableOfT)
                ?.GetGenericArguments()[0];
        }
        private static bool IsIEnumerableOfT(Type intf)
        {
            return intf.IsGenericType && intf.GetGenericTypeDefinition() == typeof(IEnumerable<>);
        }
    }
}
