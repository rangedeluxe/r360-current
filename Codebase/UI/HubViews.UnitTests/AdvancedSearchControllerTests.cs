﻿using System;
using System.Collections.Generic;
using HubViews.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.R360Services.Common.DTO;
using HubViews.Models;
using HubViews.UnitTests.Mocks;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;

namespace HubViews.UnitTests
{

    [TestClass]
    public class AdvancedSearchControllerTests
    {
        private readonly AdvancedSearchController controller;
        //private readonly MockR360Services _r360services;
        //private readonly MockSystemServices _systemservices;
        //private readonly MockTemporaryStorageProvider _storage;
        //private readonly MockWfsLog _logger;

        public AdvancedSearchControllerTests()
        {
            controller = PopulateController();
        }

        private AdvancedSearchController PopulateController()
        {
            return new AdvancedSearchController(
                new MockR360Services(), 
                new MockSystemServices(), 
                new MockTemporaryStorageProvider(), 
                new MockWfsLog(), 
                new MockSessionTokenProvider(), 
                new MockContextContainer());
        }

        [TestMethod]
        public void AdvancedSearch_ShouldInitialize_Correctly()
        {
            // Do nothing, as we really just need to hit the constructor.
        }

        [TestMethod]
        public void AdvancedSearch_AutoMapperConfigurationIsValid()
        {
            AdvancedSearchController.CreateMapperConfiguration().AssertConfigurationIsValid();
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_ValidIndex()
        {
            // Act
            var result = controller.Index(null);

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_ValidResults()
        {
            // Act
            var result = controller.Results(null);

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_ValidQueryDialog()
        {
            // Act
            var result = controller.QueryDialog(null);

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldSave_ValidQuery()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchEditQueryRequestModel()
            {
                Workgroup = "10|10",
                DateFrom = date,
                DateTo = date,
                QueryName = "QueryName"
            };

            // Act
            var result = controller.SaveQuery(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(true, obj.Value<bool>("Success"));
        }

        [TestMethod]
        public void AdvancedSearch_ShouldNotSave_InvalidQuery()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchEditQueryRequestModel()
            {
                Workgroup = "10|10",
                DateFrom = date,
                DateTo = date
            };

            // Act
            var result = controller.SaveQuery(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(null, obj["Success"]);
            Assert.IsTrue(obj.Value<string>("Error").Length > 0);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_ValidAdvancedSearch()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                Workgroup = "10|10",
                DateFrom = date,
                DateTo = date
            };

            // Act
            var result = controller.GetAdvancedSearch(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(true, obj.Value<bool>("success"));
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_ValidAdvancedSearch_ForDataEntryCritera()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                Workgroup = "10|10",
                DateFrom = date,
                DateTo = date,
                Criteria = new List<AdvancedSearchCriteria>()
                {
                    new AdvancedSearchCriteria()
                    {
                        Column = new AdvancedSearchWhereClauseDto()
                        {
                            BatchSourceKey = 255,
                            FieldName = "Amount",
                            DataType = AdvancedSearchWhereClauseDataType.Decimal,
                            ReportTitle = "Amount",
                            IsDataEntry = true
                        },
                        Operator = AdvancedSearchWhereClauseOperator.IsGreaterThan,
                        Value = "10"
                    }
                }
            };

            // Act
            var result = controller.GetAdvancedSearch(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(true, obj.Value<bool>("success"));
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_ValidAdvancedSearch_ForStandardCritera()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                Workgroup = "10|10",
                DateFrom = date,
                DateTo = date,
                Criteria = new List<AdvancedSearchCriteria>()
                {
                    new AdvancedSearchCriteria()
                    {
                        Column = new AdvancedSearchWhereClauseDto()
                        {
                            BatchSourceKey = 255,
                            FieldName = "Amount",
                            DataType = AdvancedSearchWhereClauseDataType.Decimal,
                            ReportTitle = "Amount",
                            IsStandard = true,
                            StandardXmlColumnName = "Amount"
                        },
                        Operator = AdvancedSearchWhereClauseOperator.IsGreaterThan,
                        Value = "10"
                    },
                    new AdvancedSearchCriteria()
                    {
                        Column = new AdvancedSearchWhereClauseDto()
                        {
                            BatchSourceKey = 255,
                            FieldName = "Amount1",
                            DataType = AdvancedSearchWhereClauseDataType.Decimal,
                            ReportTitle = "Amount1",
                            IsStandard = true,
                            StandardXmlColumnName = "Amount1"
                        },
                        Operator = AdvancedSearchWhereClauseOperator.Equals,
                        Value = "10"
                    },
                }
            };

            // Act
            var result = controller.GetAdvancedSearch(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(true, obj.Value<bool>("success"));
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_InvalidAdvancedSearch_ForDateFrom()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                Workgroup = "10|10",
                DateTo = date
            };

            // Act
            var result = controller.GetAdvancedSearch(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(null, obj["success"]);
            Assert.IsTrue(obj.Value<string>("Error").Length > 0);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_InvalidAdvancedSearch_ForDateTo()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                Workgroup = "10|10",
                DateFrom = date
            };

            // Act
            var result = controller.GetAdvancedSearch(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(null, obj["success"]);
            Assert.IsTrue(obj.Value<string>("Error").Length > 0);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_InvalidAdvancedSearch_ForWorkgroup()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date
            };

            // Act
            var result = controller.GetAdvancedSearch(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(null, obj["success"]);
            Assert.IsTrue(obj.Value<string>("Error").Length > 0);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_InvalidAdvancedSearch_ForEntity()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10"
            };

            // Act
            var result = controller.GetAdvancedSearch(query);
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(null, obj["success"]);
            Assert.IsTrue(obj.Value<string>("Error").Length > 0);
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_ValidAdvancedSearchResponse()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10"
            };

            // Act
            controller.GetAdvancedSearch(query);
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions {DoNotQuoteFields = false, ExcludeCommasFromNumeric = false});
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.IsTrue(!contents.Contains("An error occurred"));
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_InvalidAdvancedSearchResponse_ForNoPriorSearch()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10"
            };

            // Act
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions { DoNotQuoteFields = false, ExcludeCommasFromNumeric = false });
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.IsTrue(contents.Contains("An error occurred"));
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_ValidAdvancedSearchResponse_ForDataEntryCurrency()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10",
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 255,
                        DataType = AdvancedSearchWhereClauseDataType.Decimal,
                        FieldName = "Amount",
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        ReportTitle = "Amount"
                    }
                }
            };

            // Act
            controller.GetAdvancedSearch(query);
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions { DoNotQuoteFields = false, ExcludeCommasFromNumeric = false });
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.IsTrue(!contents.Contains("An error occurred"));
            Assert.IsTrue(contents.Contains("Amount"));
            Assert.IsTrue(contents.Contains("$10.10"));
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_ValidAdvancedSearchResponse_ForDataEntryCurrency_WithCommas()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10",
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 255,
                        DataType = AdvancedSearchWhereClauseDataType.Decimal,
                        FieldName = "Amount",
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        ReportTitle = "Amount"
                    }
                }
            };

            // Act
            controller.GetAdvancedSearch(query);
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions { DoNotQuoteFields = false, ExcludeCommasFromNumeric = false });
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.IsTrue(!contents.Contains("An error occurred"));
            Assert.IsTrue(contents.Contains("Amount"));
            Assert.IsTrue(contents.Contains("$5,050.50"));
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_ValidAdvancedSearchResponse_ForDataEntryCurrency_WithoutCommas()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10",
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 255,
                        DataType = AdvancedSearchWhereClauseDataType.Decimal,
                        FieldName = "Amount",
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        ReportTitle = "Amount"
                    }
                }
            };

            // Act
            controller.GetAdvancedSearch(query);
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions { DoNotQuoteFields = true, ExcludeCommasFromNumeric = false });
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.IsTrue(!contents.Contains("An error occurred"));
            Assert.IsTrue(contents.Contains("Amount"));
            Assert.IsTrue(contents.Contains("$5,050.50"));
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_ValidAdvancedSearchResponse_ForDataEntryCurrency_WithQuotes()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10",
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 255,
                        DataType = AdvancedSearchWhereClauseDataType.Decimal,
                        FieldName = "Amount",
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        ReportTitle = "Amount"
                    }
                }
            };

            // Act
            controller.GetAdvancedSearch(query);
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions { DoNotQuoteFields = false, ExcludeCommasFromNumeric = false });
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.AreEqual(9,contents.IndexOf($"\""));
            Assert.IsTrue(!contents.Contains("An error occurred"));
            Assert.IsTrue(contents.Contains("Amount"));
            Assert.IsTrue(contents.Contains("$5,050.50"));
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_ValidAdvancedSearchResponse_ForDataEntryCurrency_WithoutQuotes()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10",
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 255,
                        DataType = AdvancedSearchWhereClauseDataType.Decimal,
                        FieldName = "Amount",
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        ReportTitle = "Amount"
                    }
                }
            };

            // Act
            controller.GetAdvancedSearch(query);
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions { DoNotQuoteFields = true, ExcludeCommasFromNumeric = false });
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.AreEqual(-1, contents.IndexOf($"\""));
            Assert.IsTrue(!contents.Contains("An error occurred"));
            Assert.IsTrue(contents.Contains("Amount"));
            Assert.IsTrue(contents.Contains("$5,050.50"));
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_ValidAdvancedSearchResponse_ForDataEntryCurrency_WithoutCommasQuotes()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10",
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 255,
                        DataType = AdvancedSearchWhereClauseDataType.Decimal,
                        FieldName = "Amount",
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        ReportTitle = "Amount"
                    }
                }
            };

            // Act
            controller.GetAdvancedSearch(query);
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions { DoNotQuoteFields = true, ExcludeCommasFromNumeric = true });
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.AreEqual(-1, contents.IndexOf($"\""));
            Assert.IsTrue(!contents.Contains("An error occurred"));
            Assert.IsTrue(contents.Contains("Amount"));
            Assert.IsTrue(contents.Contains("$5050.50"));
        }

        [TestMethod]
        public void DownloadAsText_ShouldReturn_ValidAdvancedSearchResponse_ForDataEntryCurrency_WithCommasQuotes()
        {
            // Arrange
            var date = "01/01/2016";
            var query = new AdvancedSearchRequestModel()
            {
                DateFrom = date,
                DateTo = date,
                Workgroup = "10|10",
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 255,
                        DataType = AdvancedSearchWhereClauseDataType.Decimal,
                        FieldName = "Amount",
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        ReportTitle = "Amount"
                    }
                }
            };

            // Act
            controller.GetAdvancedSearch(query);
            var result = (FileContentResult)controller.DownloadAsText(new AdvancedSearchDownloadAsTextOptions { DoNotQuoteFields = false, ExcludeCommasFromNumeric = false });
            var contents = System.Text.UTF8Encoding.UTF8.GetString(result.FileContents);

            // Assert
            Assert.IsTrue(contents.Length > 0);
            Assert.AreEqual(9, contents.IndexOf($"\""));
            Assert.IsTrue(!contents.Contains("An error occurred"));
            Assert.IsTrue(contents.Contains("Amount"));
            Assert.IsTrue(contents.Contains("$5,050.50"));
        }

        [TestMethod]
        public void GetAdvancedFindData_ShouldReturn_ValidData()
        {
            // Arrange
            var query = new AdvancedFindRequestModel()
            {
                Workgroup = "10|10"
            };

            // Act
            var result = controller.GetAdvancedFindData(query);
            var data = (List<AdvancedSearchWhereClauseDto>)result.Data;

            // Assert
            Assert.IsTrue(data.Any());
        }

        [TestMethod]
        public void GetStoredQueries_ShouldReturn_ValidData()
        {
            // Act
            var result = controller.GetStoredQueries();
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(true, obj.Value<bool>("success"));
        }

        [TestMethod]
        public void GetStoredQuery_ShouldReturn_ValidData()
        {
            // Act
            var result = controller.GetStoredQuery(Guid.NewGuid());
            var obj = JObject.Parse(JsonConvert.SerializeObject(result.Data));

            // Assert
            Assert.AreEqual(true, obj.Value<bool>("success"));
        }
    }
}
