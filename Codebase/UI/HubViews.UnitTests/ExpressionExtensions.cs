using System;
using System.Linq.Expressions;
using System.Reflection;

namespace HubViews.UnitTests
{
    /// <summary>
    /// Extension methods for <see cref="Expression{TDelegate}"/>, aka expression trees.
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Assuming the expression tree is a property-get expression, this returns the
        /// <see cref="PropertyInfo"/> for the property. Throws on error.
        /// </summary>
        public static PropertyInfo ParsePropertyInfo<TParameter, TResult>(
            this Expression<Func<TParameter, TResult>> expression)
        {
            // Try to diagnose errors we've run into before
            var unaryExpression = expression.Body as UnaryExpression;
            if (unaryExpression?.NodeType == ExpressionType.Convert)
            {
                var message =
                    $"Type mismatch: expression tree is trying to convert property access to {unaryExpression.Type.FullName}. " +
                    "Is type inference widening the type of the expression tree's expression?";
                throw new InvalidOperationException(message);
            }

            var member = (MemberExpression) expression.Body;
            var property = (PropertyInfo) member.Member;
            return property;
        }
    }
}
