﻿using System;
using System.Collections.Generic;
using HubViews.Controllers;
using HubViews.Models;
using HubViews.UnitTests.Mocks;
using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wfs.Logging;
using Wfs.Raam.Core;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Fakes;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Shared.Fakes;

namespace HubViews.UnitTests
{
    [TestClass]
    public class BatchSummaryControllerTests
    {
        private readonly BatchSummaryController _controller;
        private readonly MockWfsLog _logger;

        public BatchSummaryControllerTests()
        {
            _logger = new MockWfsLog();

            _controller = new BatchSummaryController(_logger);
        }

        [TestMethod]
        public void BatchSummary_TestGetBatches()
        {
            using (ShimsContext.Create())
            {
                var claims = new List<SimpleClaim>
                {
                    new SimpleClaim {Type = WfsClaimTypes.SessionId, Value = Guid.NewGuid().ToString()}
                };

                var session = ShimR360ServiceContext.AllInstances.RAAMClaimsGet =
                    (t) => claims.ToArray();

                // Stub the GetBatchSummary method
                var manager = new StubIR360Services().MockFor(mock =>
                    mock.GetBatchSummaryBatchSummaryRequestDto = BatchSummaryResponse =>
                    {
                        var response = new BatchSummaryResponseDto
                        {
                            Status = StatusCode.SUCCESS,
                            BatchSummaryList = new List<BatchSummaryDto>
                            {
                                new BatchSummaryDto
                                {
                                    BankId = 1,
                                    ClientAccountId = 1,
                                    BatchNumber = "11111",
                                    BatchID = 1,
                                    DepositDate = DateTime.Now,
                                    DocumentCount = 1,
                                    PaymentCount = 2,
                                    PaymentSource = "ACH",
                                    PaymentType = "ACH",
                                    SourceBatchID = 919191,
                                    TotalAmount = decimal.MinValue,
                                    TransactionCount = 2
                                }
                            }
                        };
                        return response;
                    });

                var model = new BatchSearchModel
                {
                    Workgroup = "TestBank/Test WG",
                    DateFrom = DateTime.Now.Subtract(new TimeSpan(30, 0, 0, 0)).ToShortDateString(),
                    DateTo = DateTime.Now.ToShortDateString(),
                    PaymentType = 1,
                    PaymentSource = 1
                };
                var logger = new InMemoryLog();
                var controller = new BatchSummaryController(logger);
                var actual = controller.GetBatches(model);
                Assert.IsTrue(actual.Data != null);
            }
        }

        [TestMethod]
        public void BatchSummary_TestGetPaymentTypes()
        {
            using (ShimsContext.Create())
            {
                var claims = new List<SimpleClaim>
                {
                    new SimpleClaim {Type = WfsClaimTypes.SessionId, Value = Guid.NewGuid().ToString()}
                };

                var session = ShimR360ServiceContext.AllInstances.RAAMClaimsGet =
                    (t) => claims.ToArray();

                // Shim the GetPaymentTypes method
                var manager = new StubIR360Services().MockFor(mock =>
                    mock.GetPaymentTypes = () =>
                    {
                        var response = new PaymentTypesResponseDto
                        {
                            Status = StatusCode.SUCCESS,
                            PaymentTypes = new List<PaymentTypeDto>
                            {
                                new PaymentTypeDto {LongName = "-- All --", PaymentTypeKey = 0},
                                new PaymentTypeDto {LongName = "Wire", PaymentTypeKey = 1},
                                new PaymentTypeDto {LongName = "ACH", PaymentTypeKey = 2},
                                new PaymentTypeDto {LongName = "Check", PaymentTypeKey = 3}
                            }
                        };
                        return response;
                    });

                var logger = new InMemoryLog();
                var controller = new BatchSummaryController(logger);
                var actual = controller.GetPaymentTypes();
                Assert.IsTrue(actual.Data != null);
            }
        }

        [TestMethod]
        public void BatchSummary_TestGetPaymentSources()
        {
            using (ShimsContext.Create())
            {
                var claims = new List<SimpleClaim>
                {
                    new SimpleClaim {Type = WfsClaimTypes.SessionId, Value = Guid.NewGuid().ToString()}
                };

                var session = ShimR360ServiceContext.AllInstances.RAAMClaimsGet =
                    (t) => claims.ToArray();

                // Shim the GetPaymentSources method
                var manager = new StubIR360Services().MockFor(mock =>
                    mock.GetPaymentSources = () =>
                    {
                        var response = new PaymentSourcesResponseDto
                        {
                            Status = StatusCode.SUCCESS,
                            PaymentSources = new List<PaymentSourceDto>
                            {
                                new PaymentSourceDto {LongName = "-- All --", PaymentSourceKey = 0},
                                new PaymentSourceDto {LongName = "Lockbox", PaymentSourceKey = 1},
                                new PaymentSourceDto {LongName = "ACH", PaymentSourceKey = 2},
                                new PaymentSourceDto {LongName = "IntegraPay", PaymentSourceKey = 3}
                            }
                        };
                        return response;
                    });

                var logger = new InMemoryLog();
                var controller = new BatchSummaryController(logger);
                var actual = controller.GetPaymentSources();
                Assert.IsTrue(actual.Data != null);
            }
        }

        [TestMethod]
        public void BatchSummary_TestGetBatchDetail()
        {

        }
    }
}