﻿using log4net.Config;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Wfs.Raam.Core;
using Wfs.Raam.TokenCache;

namespace HubConfigViews
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801
	public class MvcApplication : System.Web.HttpApplication
	{
		private static Wfs.Logging.ILogSource Logger;

		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);

			// get log4net config from web.config
			XmlConfigurator.Configure();
			ObjectFactory.Initialize(x =>
			{
				x.PullConfigurationFromAppConfig = true;
			});

			WSPassiveSessionConfiguration.Start();

			Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(MvcApplication));
		}

		public override void Init()
		{
			WSPassiveSessionConfiguration.Init();
			base.Init();
		}

		protected void Application_Error(object sender, EventArgs e)
		{
			HttpContext ctx = HttpContext.Current;
			if (ctx != null)
			{
				Exception exception = ctx.Server.GetLastError();
				if (exception != null)
				{
					Logger.Log(Wfs.Logging.LogLevel.Fatal, "Error: Unhandled Application Exception:\r\n  Offending URL: {0}\r\n  Exception: {1}",
						ctx.Request.Url.PathAndQuery, exception.ToString());

					// To let the page finish running we would clear the error - since we want our next line to run (the redirect) we need to do this...
					//ctx.Server.ClearError();

					// But really, we are just going to send them to our custom error page...
					//Response.Redirect("~/ErrorPage.aspx");
				}
				else
				{
					if (ctx.AllErrors != null && ctx.AllErrors.Length > 0)
					{
						Logger.Log(Wfs.Logging.LogLevel.Fatal, "Error: Unexpected exception during page lifecycle - displaying last error only\r\n  Offending URL: {0}\r\n  Exception: {1}",
							ctx.Request.Url.PathAndQuery, ctx.AllErrors[ctx.AllErrors.Length - 1]);
					}
					else
					{
						Logger.Log(Wfs.Logging.LogLevel.Fatal, "Error: Error event triggered with no error context - please look for previous exceptions...?\r\n  Offending URL: {0}",
							ctx.Request.Url.PathAndQuery);
					}
				}
			}
		}

        protected void Application_PreSendRequestHeaders()
        {
            var allowFrom = string.Empty;
            var principal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (principal != null)
            {
                var identity = principal.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    var allowFromClaim = identity.Claims.FirstOrDefault(c => c.Type == WfsClaimTypes.AllowFromUrlInFrame);
                    allowFrom = allowFromClaim == null ? "" : allowFromClaim.Value;
                }
            }

            if (!string.IsNullOrEmpty(allowFrom))
            {
                Response.Headers["X-Frame-Options"] = "ALLOW-FROM " + allowFrom;
                Response.Headers["Content-Security-Policy"] = "frame-ancestors " + allowFrom;
            }
            else
            {
                Response.Headers["X-Frame-Options"] = "SAMEORIGIN";
                Response.Headers["Content-Security-Policy"] = "frame-ancestors 'self'";
            }
        }
    }
}