﻿using System;

namespace WFS.RecHub.HubConfig.Core
{
    public enum SystemType
    {
        integraPAY,
        ImageRPS,
        DIT
    }
}