﻿using CommonUtils.Web;
using HubConfigViews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Controllers
{
	public class FieldValidationsController : BaseController
    {
        //
        // GET: /FieldValidations/

		[Authorize]
        public ActionResult Index()
        {
			StaticDescriptionModel[] validationTableTypes = null;
			PaymentSourceDto[] batchSources = null;
			StaticDescriptionModel[] batchPaymentTypes = null;
			BatchPaymentSubTypeModel[] batchPaymentSubTypes = null;
			List<WebErrors> webErrors = new List<WebErrors>();
			List<String> errors = null;
			bool success = true;

			var hasViewPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_UploadExceptions, R360Permissions.ActionType.View);
			var hasUpdatePerm = R360Permissions.Current.Allowed(R360Permissions.Perm_UploadExceptions, R360Permissions.ActionType.Manage);

			int fiEntityID = 0;/*TODO - will have to move this to GetFieldValidations to get sources available for entity*/

			if (!hasViewPerm)
			{
				return new EmptyResult();
			}
			else if (!StaticDescriptionModel.GetValidationTableTypes(out validationTableTypes, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occurred retrieving the validation table types." };
			}
			else if (!StaticDescriptionModel.GetBatchSources(fiEntityID, out batchSources, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occurred retrieving the batch sources." };
			}
			else if (!StaticDescriptionModel.GetBatchPaymentTypes(out batchPaymentTypes, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occurred retrieving the payment types." };
			}
			else if (!BatchPaymentSubTypeModel.GetBatchPaymentSubTypes(out batchPaymentSubTypes, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occurred retrieving the payment sub types." };
			}

			object model;
			if (success)
			{
				model = new
				{
					HasUpdatePerm = hasUpdatePerm,
					StaticDescriptions = new
					{
						ValidationTableTypes = validationTableTypes,
						BatchSources = batchSources,
						BatchPaymentTypes = batchPaymentTypes,
						BatchPaymentSubTypes = batchPaymentSubTypes,
					},
					Success = success,
					Errors = webErrors.Count == 0 ? null : webErrors
				};
			}
			else
			{
				model = new
				{
					Success = success,
					Errors = webErrors.Count == 0 ? null : webErrors
				};
			}

			return View(model);
        }

		[Authorize]
		public JsonResult GetFieldValidations(int siteBankID, int workgroupID)
		{
			// Audit the identifiers
			AuditFormFields("siteBankID", "workgroupID");

			BusinessRulesModel[] businessRules;
			List<string> errors = null;
			bool success = true;

			if (!BusinessRulesModel.GetFieldValidationSettings(siteBankID, workgroupID, out businessRules, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occurred saving the field validation settings." };
			}

			var response = new
			{
				Data = businessRules,
				HasErrors = !success,
				Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
			};
			return this.Json(response);
		}
    }
}
