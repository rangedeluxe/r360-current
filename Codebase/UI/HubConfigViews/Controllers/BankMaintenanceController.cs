﻿using CommonUtils.Web;
using HubConfigViews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Controllers
{
	public class BankMaintenanceController : BaseController
	{
		[Authorize]
		public ActionResult Index()
		{
			var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Bank, R360Permissions.ActionType.View);
			if (!hasPerm)
			{
				return new EmptyResult();
			}

			BankModel[] banks = null;
			List<string> errors = null;
			bool success = true;

			if (!BankModel.GetBanks(out banks, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occurred retrieving the Bank listing." };
			}
			
			var response = new
			{
				Banks = banks,
				FIs = FIEntityModel.GetFIEntities().ToArray(),
				CanAdd = R360Permissions.Current.Allowed(R360Permissions.Perm_Bank, R360Permissions.ActionType.Add),
				CanEdit = R360Permissions.Current.Allowed(R360Permissions.Perm_Bank, R360Permissions.ActionType.Update),
				Success = success,
				Errors = errors
			};

			return View(response);
		}
        [Authorize]
        [HttpPost]
        public JsonResult GetBankList()
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Bank, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return this.Json(null);
            }
            JSONResponse webResp = new JSONResponse();
            BankModel[] banks = null;
            List<string> errors = null;
            bool success = true;

            if (!BankModel.GetBanks(out banks, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    webResp.Errors = new List<WebErrors>(new List<WebErrors>(errors.Select(msg => new WebErrors() { Message = msg })));
            }
            webResp.HasErrors = !success;
            webResp.Data = banks;
            return this.Json(webResp);

        }
		[Authorize]
		public ActionResult EditBank(bool adding, int? bankKey)
		{
			var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Bank, adding ? R360Permissions.ActionType.Add : R360Permissions.ActionType.Update);

			BankModel bank = null;
			List<WebErrors> webErrors = new List<WebErrors>();
			List<String> errors = null;

			if (!hasPerm)
			{
				errors = new List<string>() { "Access Denied" };
			}
			else if (!adding)
			{
				if (!bankKey.HasValue)
				{
					errors = new List<string>() { "Error updating Bank, no ID specified." };
				}
				else if (!BankModel.GetBank(bankKey.Value, out bank, out errors))
				{
					if (errors == null || errors.Count == 0)
						errors = new List<string>() { "Error occurred retrieving the Bank for editing." };
				}
			}

			if (errors != null)
				webErrors.AddRange(errors.Select(o => new WebErrors() { Message = o }).ToList());

			var response = new
			{
				Bank = bank,
				FIs = FIEntityModel.GetFIEntities().ToArray(),
				Success = webErrors.Count == 0,
				Errors = webErrors.Count == 0 ? null : webErrors
			};

			return View(response);
		}

        [HttpPost]
		[Authorize]        
		public JsonResult Save([Bind(Include="BankKey, SiteBankID, MostRecent, BankName, FIEntityID")]
                               BankModel bank)
		{

			var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Bank, bank.BankKey <= 0 ? R360Permissions.ActionType.Add : R360Permissions.ActionType.Update);

			List<string> errors = null;

			if (!hasPerm)
			{
				errors = new List<string>() { "Access Denied" };
			}
			else if (!bank.Save(out errors))
			{
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occurred saving the Bank information." };
			}

			var response = new
			{
				HasErrors = errors != null && errors.Count > 0,
				Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
			};
			return this.Json(response);
		}
	}
}
