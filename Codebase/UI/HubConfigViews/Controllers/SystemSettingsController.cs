﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CommonUtils.Web;
using HubConfigViews.Models;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ApplicationBlocks.Common.Objects;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using System.Configuration;

namespace HubConfigViews.Controllers
{
    public class SystemSettingsController : BaseController
    {
        private const string MAX_PRINTABLE_ROWS_CONFIG = "MaxPrintableRowsGovernor";
        private const string MAX_PRINTABLE_ROWS_DEFAULT = "5000";

        private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(SystemSettingsController));

		[Authorize]
        public ActionResult Index()
        {
            if (!HasSystemSettingsViewPermission())
                return new EmptyResult();

            var resp = new {
                CanEditSystemSettings = HasSystemSettingsUpdatePermission()
            };

            return View(resp);
        }

		[Authorize]
        public ActionResult EditSystemSettings()
        {
            if (!HasSystemSettingsUpdatePermission())
                return new EmptyResult();

            var maxprintablerowstring = ConfigurationManager.AppSettings.AllKeys.Contains(MAX_PRINTABLE_ROWS_CONFIG)
                ? ConfigurationManager.AppSettings[MAX_PRINTABLE_ROWS_CONFIG]
                : MAX_PRINTABLE_ROWS_DEFAULT;

            int maxprintablerows;
            if (!int.TryParse(maxprintablerowstring, out maxprintablerows))
                maxprintablerows = int.Parse(MAX_PRINTABLE_ROWS_DEFAULT);

            return View(new
            {
                Preferences = GetPreferences(),
                MetaData = new
                {
                    MaxPrintableRowsGovernor = maxprintablerows
                }
            });
        }


		[Authorize]
        [HttpPost]
        public JsonResult GetPreferences()
        {
            bool success = true;
            List<PreferencesModel> preferences = null;
            List<string> errors = null;

            if (!HasSystemSettingsViewPermission())
                return this.Json(null);

            if (!PreferencesModel.GetPreferences(out preferences, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving the System Settings." };
            }

            var resp = new JSONResponse()
            {
                Data = preferences,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null,
            };

            return this.Json(resp);
        }

		[Authorize]
        [HttpPost]
        public JsonResult GetDocumentTypes()
        {
            if (!HasSystemSettingsViewPermission())
                return this.Json(null);

            bool success = true;
            List<string> errors = null;
            List<DocumentTypeDto> documenttypes = null;

            var hubConfigMgr = new HubConfigServicesManager();
            var docresponse = hubConfigMgr.GetDocumentTypes();
            if (docresponse.Status != StatusCode.SUCCESS)
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving the Document Types." };
            }
            documenttypes = docresponse.Data;

            var model = new
            {
                Data = documenttypes.ToArray(),
                CanEditDocuments = HasSystemSettingsUpdatePermission()
            };

            var resp = new JSONResponse()
            {
                Data = model,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null,
            };

            return Json(resp);
        }

		[Authorize]
        [HttpPost]
        public JsonResult GetNotificationTypes()
        {
            if (!HasSystemSettingsViewPermission())
                return this.Json(null);

            bool success = true;
            List<string> errors = null;
            List<NotificationTypeDto> notificationtypes = null;

            var hubConfigMgr = new HubConfigServicesManager();
            var docresponse = hubConfigMgr.GetNotificationTypes();
            if (docresponse.Status != StatusCode.SUCCESS)
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving the Notification Types." };
            }
            notificationtypes = docresponse.Data;

            var model = new
            {
                Data = notificationtypes.ToArray(),
                CanEditNotifications = HasSystemSettingsUpdatePermission()
            };

            var resp = new JSONResponse()
            {
                Data = model,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null,
            };

            return Json(resp);
        }

		[Authorize]
        public ActionResult EditDocumentType(int documenttypekey)
        {
            if (!HasSystemSettingsUpdatePermission())
                return this.Json(null);

            var hubConfigMgr = new HubConfigServicesManager();
            var docresponse = hubConfigMgr.GetDocumentTypes();
            var doc = docresponse.Data
                .FirstOrDefault(x => x.DocumentTypeKey == documenttypekey);

            return View(doc);
        }

		[Authorize]
        public ActionResult EditNotificationType(int? notificationfiletypekey)
        {
            if (!HasSystemSettingsUpdatePermission())
                return this.Json(null);

            var hubConfigMgr = new HubConfigServicesManager();
            var notresponse = hubConfigMgr.GetNotificationTypes();
            var not = notresponse.Data
                .FirstOrDefault(x => x.NotificationFileTypeKey == notificationfiletypekey);

            var model = new
            {
                Data = not,
                IsEdit = notificationfiletypekey.HasValue
            };

            return View(model);
        }

		[Authorize]
        [HttpPost]
        public JsonResult UpdateDocumentType([Bind(Include="FileDescriptor, Description, DocumentTypeKey, IsReadOnly")]
                                              DocumentTypeDto documenttype)
        {
            if (!HasSystemSettingsUpdatePermission())
                return this.Json(null);

            HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
            var resp = hubConfigMgr.UpdateDocumentType(documenttype);
            var webResponse = new JSONResponse()
            {
                Data = resp.Data,
                HasErrors = !resp.Data,
                Errors = resp.Errors != null ? resp.Errors.Select(o => new WebErrors() { Message = o }).ToList() : null,
            };
            return Json(webResponse);
        }

		[Authorize]
        [HttpPost]
        public JsonResult AddOrUpdateNotificationType([Bind(Include="FileType, FileTypeDescription, NotificationFileTypeKey, IsEdit")]
                                                      NotificationType notificationtype)
        {
            if (!HasSystemSettingsUpdatePermission())
                return this.Json(null);

            HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
            var previous = hubConfigMgr.GetNotificationTypes();
            if (previous.Status != StatusCode.SUCCESS)
                return this.Json(null);

            //Grab the notification by checking for existing Type, IsEdit will determine the operation
            var notification = previous.Data.FirstOrDefault(x =>  String.Equals(x.FileType, notificationtype.FileType, StringComparison.CurrentCultureIgnoreCase));
            if (notification != null && !notificationtype.IsEdit)
            {
                var response = new JSONResponse()
                {
                    Data = null,
                    HasErrors = true,
                    Errors = new List<WebErrors>()
                };
                response.Errors.Add(new WebErrors() { Message = "File type already exists in the application" });
                return Json(response);
            }
            HubConfigResponse<bool> resp;
            if (notification == null)
                resp = hubConfigMgr.InsertNotificationType(notificationtype.ToNotificationTypeDto());
            else
                resp = hubConfigMgr.UpdateNotificationType(notificationtype.ToNotificationTypeDto());

            var webResponse = new JSONResponse()
            {
                Data = resp.Data,
                HasErrors = !resp.Data,
                Errors = resp.Errors != null ? resp.Errors.Select(o => new WebErrors() { Message = o }).ToList() : null,
            };
            return Json(webResponse);
        }

		[Authorize]
        [HttpPost]
        public JsonResult UpdatePreferences(List<PreferencesModel> preferences)
        {
            JSONResponse webResponse = new JSONResponse();
            List<PreferenceDto> preferenceDtos = new List<PreferenceDto>();

            // Suppress the default audit, we'll be calling that manually later.
            base.IsAuditEventSuppressed = true;
            
            if (!HasSystemSettingsUpdatePermission())
                return this.Json(null);

            try
            {
                List<PreferencesModel> oldprefs;
                List<string> errors;
                PreferencesModel.GetPreferences(out oldprefs, out errors);

                // Building up a list of preferences that have actually changed.
                var diffs = new List<KeyValuePair<string, ObjectDifference>>();
                for (int i = 0; i < oldprefs.Count; i++)
                {
                    var pref = oldprefs[i];
                    var newpref = preferences.FirstOrDefault(x => x.Name == pref.Name);
                    if (newpref == null)
                        continue;

                    var diff = Utility.PropertyWiseObjectDiff(pref, newpref)
                        .FirstOrDefault(x => x.PropertyName == "DefaultSetting" || x.PropertyName == "IsOnline");
                    if (diff == null)
                        continue;

                    diffs.Add(new KeyValuePair<string, ObjectDifference>(pref.Name, diff));
                }

                var changedprefs = preferences.Where(x => diffs.Any(y => y.Key == x.Name))
                    .GroupBy(x => x.Name)
                    .Select(x => x.First());
                foreach (PreferencesModel model in changedprefs)
                {
                    preferenceDtos.Add(PreferencesModel.LoadFromModel(model));
                }

                HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
                var resp = hubConfigMgr.UpdatePreferences(preferenceDtos);

                if (resp.Status == WFS.RecHub.R360Shared.StatusCode.SUCCESS)
                    webResponse.HasErrors = false;
                else
                {
                    webResponse.HasErrors = true;
                    webResponse.Errors.AddRange(resp.Errors.Select(o => new WebErrors() { Message = o }).ToList());
                }

                // Build up the audit message.
                if (diffs.Count > 0)
                {
                    var app = Request.ApplicationPath.Trim('/');
                    var message = "SystemSettings/UpdatePreferences" + " Fields: ";
                    foreach (var diff in diffs)
                    {
                        message += diff.Key + "=" + diff.Value.NewValue;
                        if (diff.Key != diffs.Last().Key)
                            message += ", ";
                    }

                    var manager = new R360ServiceManager();
                    manager.WriteAuditEvent("Submitted / Retrieved Data", "Page View", app, message);
                }
            }
            catch (Exception ex)
            {
                webResponse.HasErrors = true;
                webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Save Failed." } };

                Logger.Log(Wfs.Logging.LogLevel.Error, "Error in PaymentSourceController: " + ex.Message);
            }

            return this.Json(webResponse);
        }

        private bool HasSystemSettingsViewPermission()
        {
           return R360Permissions.Current.Allowed(R360Permissions.Perm_SystemSettings, R360Permissions.ActionType.View);
        }

        private bool HasSystemSettingsUpdatePermission()
        {
            return R360Permissions.Current.Allowed(R360Permissions.Perm_SystemSettings, R360Permissions.ActionType.Update);
        }

    }
}
