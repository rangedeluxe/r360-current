﻿using CommonUtils.Web;
using HubConfigViews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wfs.Logging;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.HubConfig;
using WFS.RecHub.HubConfig.DTO;
using WFS.RecHub.HubConfig.Enums;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Controllers
{
    public class WorkgroupsController : BaseController
    {
        private const string AUDITAPPNAME = "WorkgroupMaintenance";
        private IRaamClient _raamClient;
        private IR360Services _r360Services;
        private static Guid CurrentSid
        {
            get
            {
                var sid = Guid.Parse(CommonUtils.AuthorizationManager.GetClaimValue(
                    "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid"));
                return sid;
            }
        }

        private UserDto SidToUser(Guid? sid)
        {
            var result = new UserDto()
            {
                LoginId = "",
                UserSID = Guid.Empty,
                Person = new PersonDto()
                {
                    FirstName = "",
                    LastName = ""
                }
            };

            if (sid != null)
            {
                result = _raamClient.GetUserByUserSID(sid.Value);
            }
            return result;
        }

        private string SidToName(Guid? sid)
        {
            var user = SidToUser(sid);

            return $"{user.Person.FirstName} {user.Person.LastName}".Trim();
        }
        private string GetBankName(int siteBankId)
        {
            var result = "";

            var bankNameData = _r360Services.GetBankName(siteBankId);

            if (bankNameData.Status == StatusCode.SUCCESS)
            {
                result = bankNameData.Data;
            }
            return result;
        }
        public WorkgroupsController()
        {
            _r360Services = new R360ServiceManager();
            Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(WorkgroupsController));
            _raamClient = new RaamClient(new ConfigHelpers.Logger(
                o => Logger.Log(LogLevel.Informational, o),
                o => Logger.Log(LogLevel.Warning, o)));

        }
        [Authorize]
        public ActionResult Index()
        {
            List<WebErrors> webErrors = new List<WebErrors>();

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.View);
            var canManage = success && R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.Update);
            var canViewAssociations = success && R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupAssociation, R360Permissions.ActionType.View);
            var canManageAssociations = success && R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupAssociation, R360Permissions.ActionType.Update);

            if (!success)
            {
                webErrors.Add(new WebErrors() { Message = "Access Denied" });
            }

            return View(new
            {
                CanManage = canManage,
                CanViewAssociations = canViewAssociations,
                CanManageAssociations = canManageAssociations,
                Success = success,
                Errors = webErrors.Count == 0 ? null : webErrors
            });
        }

        [Authorize]
        public JsonResult GetWorkgroups(int entityID)
        {
            BankModel[] banks = null;
            WorkgroupModel[] workgroups = null;
            List<string> errors = null;
            int unassignedCount = 0;

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.View);
            if (!success)
                errors = new List<string> { "Access Denied" };
            else
            {
                int fiEntityID = FIEntityModel.GetAncestorFIEntityID(entityID);
                if (fiEntityID == 0)
                {
                    success = false;
                    errors = new List<string>() { "Error occurred retrieving the workgroups.  Selected entity does not belong to a Financial Institution." };
                }
                else if (!BankModel.GetBanksForFI(fiEntityID, out banks, out errors))
                {
                    success = false;
                    if (errors == null || errors.Count == 0)
                        errors = new List<string>() { "Error occurred retrieving the Bank listing." };
                }
                else if (!WorkgroupModel.GetWorkgroups(entityID, out workgroups, out errors))
                {
                    success = false;
                    if (errors == null || errors.Count == 0)
                        errors = new List<string>() { "Error occured retrieving the workgroups." };
                }
                else if (!WorkgroupModel.GetUnassignedWorkgroupCount(fiEntityID, out unassignedCount, out errors))
                {
                    success = false;
                    if (errors == null || errors.Count == 0)
                        errors = new List<string>() { "Error occured retrieving the unassigned workgroup count." };
                }
            }

            var model = new JSONResponse()
            {
                Data = new
                {
                    Banks = banks,
                    Workgroups = workgroups,
                    UnassignedCount = unassignedCount
                },
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };
            return this.Json(model);
        }

        [Authorize]
        public JsonResult GetWorkGroupDEFields(int clientAccountKey, bool inactive)
        {
            List<String> errors = null;
            WorkgroupModel workgroup = null;
            DataEntryColumnModel[] results = { };
            bool success = true;
            if (!WorkgroupModel.GetWorkgroup(clientAccountKey, out workgroup, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving the workgroup." };
            }
            else
            {
                if (inactive)
                {
                    results = workgroup.DataEntryColumns.Where(x => x.IsActive).ToArray();
                }
                else
                {
                    results = workgroup.DataEntryColumns;
                }

            }

            var resp = new JSONResponse()
            {
                Data = results,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };
            return this.Json(resp);
        }

        [Authorize]
        public ActionResult WorkgroupDetails(bool adding, int entityID, int? clientAccountKey, string entityName)
        {
            // Audit the identifiers
            AuditFormFields("adding", "clientAccountKey");

            WorkgroupModel workgroup = null;
            BankModel[] banks = null;
            StaticDescriptionModel[] validationTableTypes = null;
            PaymentSourceDto[] batchSources = null;
            StaticDescriptionModel[] batchPaymentTypes = null;
            BatchPaymentSubTypeModel[] batchPaymentSubTypes = null;
            DataEntryTemplateModel[] deTemplates = null;
            int maxRetentionDays = 0;
            List<WebErrors> webErrors = new List<WebErrors>();
            List<String> errors = null;

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.View);
            var canManage = success && R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.Update);
            if (!success || (adding && !canManage))
            {
                webErrors.Add(new WebErrors() { Message = "Access Denied" });
                success = false;
            }
            else
            {
                if (!adding)
                {
                    if (!clientAccountKey.HasValue)
                    {
                        errors = new List<string>() { "Error updating workgroup, no workgroup specified." };
                        success = false;
                    }
                    else if (!WorkgroupModel.GetWorkgroup(clientAccountKey.Value, out workgroup, out errors))
                    {
                        success = false;
                        if (errors == null || errors.Count == 0)
                            errors = new List<string>() { "Error occurred retrieving the workgroup for editing." };
                    }
                    else if (workgroup == null)
                    {
                        success = false;
                        errors = new List<string>() { "Error updating workgroup.  The workgroup could not be found." };
                    }
                    else if (!workgroup.EntityID.HasValue || entityID != workgroup.EntityID.Value)
                    {
                        success = false;
                        errors = new List<string>() { "Data consistency error, current entity and workgroup's entity are different." };
                    }

                    if (workgroup != null)
                    {
                        workgroup.EntityName = entityName;
                    }
                }
                else if (entityID <= 0)
                {
                    success = false;
                    errors = new List<string>() { "Unable to add workgroup, no entity is present." };
                }

                if (success)
                {
                    int fiEntityID = FIEntityModel.GetAncestorFIEntityID(entityID);
                    if (fiEntityID == 0)
                    {
                        success = false;
                        errors = new List<string>() { "Unable to add/edit the workgroup.  Selected entity does not belong to a Financial Institution." };
                    }
                    else if (!BankModel.GetBanksForFI(fiEntityID, out banks, out errors))
                    {
                        success = false;
                        if (errors == null || errors.Count == 0)
                            errors = new List<string>() { "Error occurred retrieving the Bank listing." };
                    }
                    //else if (!StaticDescriptionModel.GetValidationTableTypes(out validationTableTypes, out errors))
                    //{
                    //    success = false;
                    //    if (errors == null || errors.Count == 0)
                    //        errors = new List<string>() { "Error occurred retrieving the validation table types." };
                    //}
                    else if (!StaticDescriptionModel.GetBatchSources(fiEntityID, out batchSources, out errors))
                    {
                        success = false;
                        if (errors == null || errors.Count == 0)
                            errors = new List<string>() { "Error occurred retrieving the batch sources." };
                    }
                    else if (!StaticDescriptionModel.GetBatchPaymentTypes(out batchPaymentTypes, out errors))
                    {
                        success = false;
                        if (errors == null || errors.Count == 0)
                            errors = new List<string>() { "Error occurred retrieving the payment types." };
                    }
                    else if (!BatchPaymentSubTypeModel.GetBatchPaymentSubTypes(out batchPaymentSubTypes, out errors))
                    {
                        success = false;
                        if (errors == null || errors.Count == 0)
                            errors = new List<string>() { "Error occurred retrieving the payment sub types." };
                    }
                    else if (!DataEntryTemplateModel.GetDataEntryTemplates(out deTemplates, out errors))
                    {
                        success = false;
                        if (errors == null || errors.Count == 0)
                            errors = new List<string>() { "Error occurred retrieving the data entry templates." };
                    }
                    else if (!SystemSettingsModel.GetMaxDataRetentionDays(out maxRetentionDays, out errors))
                    {
                        success = false;
                        if (errors == null || errors.Count == 0)
                            errors = new List<string>() { "Error occurred retrieving the system's default viewing days." };
                    }
                }

                if (errors != null)
                    webErrors.AddRange(errors.Select(o => new WebErrors() { Message = o }).ToList());

                if (success)
                {
                    var model = new
                    {
                        EntityID = adding ? entityID : workgroup.EntityID.Value,
                        EntityName = entityName,
                        Workgroup = workgroup,
                        Banks = banks,
                        StaticDescriptions = new
                        {
                            ImageDisplayModes = WorkgroupModel.ImageDisplayModes,
                            BalancingOptions = WorkgroupModel.BalancingOptions,
                            TableTypes = DataEntryColumnModel.TableTypes,
                            DataTypes = DataEntryColumnModel.DataTypes,
                            ValidationTableTypes = validationTableTypes,
                            BatchSources = batchSources,
                            BatchPaymentTypes = batchPaymentTypes,
                            BatchPaymentSubTypes = batchPaymentSubTypes,
                            FormatTypes = BusinessRulesModel.FormatTypes,
                            CDMethods = CheckDigitRoutineModel.CheckDigitMethods,
                            WeightDirections = CheckDigitRoutineModel.WeightPatternDirections,
                        },
                        DataEntryTemplates = deTemplates,
                        MaxRetentionDays = maxRetentionDays,
                        CanManage = canManage,
                        Success = success,
                        Errors = webErrors.Count == 0 ? null : webErrors
                    };

                    return View(model);
                }
            }

            return View(new
            {
                Success = success,
                Errors = webErrors.Count == 0 ? null : webErrors
            });
        }

        [Authorize]
        public JsonResult GetWorkgroup(int clientAccountKey)
        {
            // Audit the identifiers
            AuditFormFields("clientAccountKey");

            WorkgroupModel workgroup = null;
            List<string> errors = null;

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.View);
            if (!success)
            {
                errors = new List<string>() { "Access Denied" };
            }
            else if (!WorkgroupModel.GetWorkgroup(clientAccountKey, out workgroup, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occured retrieving the workgroup information." };
            }

            var model = new JSONResponse()
            {
                Data = workgroup,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };
            return this.Json(model);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Save([Bind(Include = "ClientAccountKey, SiteBankID, SiteClientAccountID, MostRecent, IsActive, DataRetentionDays, ImageRetentionDays, ShortName, LongName, FileGroup, EntityID, ViewingDays, MaximumSearchDays, CheckImageDisplayMode, DocumentImageDisplayMode, HOA, DisplayBatchID, InvoiceBalancing, DDAs, DataEntryColumns, DataEntrySetup, BillingAccount, BillingField1, BillingField2, EntityName, BusinessRules")]
                               WorkgroupModel workgroup)
        {
            List<string> errors = null;
            string infoMessage = null;

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.Update);
            if (!success)
            {
                errors = new List<string>() { "Access Denied" };
            }
            var auditType = workgroup.ClientAccountKey == 0 ? AuditOperations.INS:  AuditOperations.UPD;
            LogSave(workgroup, auditType);
            if (!workgroup.Save(out errors, out infoMessage))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred saving the workgroup information." };
            }
            

            var model = new
            {
                Data = workgroup,//pass back out the workgroup so we can select it
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null,
                InfoMessage = infoMessage
            };
            return this.Json(model);
        }

        private void LogSave(WorkgroupModel workgroup, AuditOperations auditType)
        {
            var manager = new HubConfigServicesManager();
            //This needs to be brough back when Invoice required is made visible again.
            //right now it's logging whenever a new wg is added and to avoid this im commenting it out
            /*
            var onOff = workgroup.BusinessRules != null && workgroup.BusinessRules.InvoiceRequired ? "on" : "off";
            manager.WriteAuditConfigReport(new AuditConfigDto
            {
                ApplicationName = AUDITAPPNAME,
                AuditType = auditType,
                Message = $"{SidToName(CurrentSid)} has turned {onOff} the \"Invoice Required\" Exception business" +
                          $" rule for  Bank: {workgroup.SiteBankID}-{GetBankName(workgroup.SiteBankID)}, Workgroup: " +
                          $"{workgroup.SiteClientAccountID}-{workgroup.LongName}.",
                AuditValue = workgroup.SiteClientAccountID.ToString()
            });
            */

            //this is not ideal. 
            //it gets a whole workgroup jus to find out whether or not our business rules have changed. 
            //We should either create a new sp and method to compare or 
            var oldwg = manager.GetWorkgroup(workgroup.ClientAccountKey, GetWorkgroupAdditionalData.BusinessRules);
            if (oldwg.Data != null && oldwg.Data.BusinessRules.PostDepositBalancingRequired ==
                workgroup.BusinessRules.PostDepositBalancingRequired &&
                oldwg.Data.BusinessRules.PostDepositPayerRequired ==
                workgroup.BusinessRules.PostDepositPayerRequired) return;

            var pdBalancingReq = workgroup.BusinessRules != null && 
                workgroup.BusinessRules.PostDepositBalancingRequired ? "on" : "off";
            manager.WriteAuditConfigReport(new AuditConfigDto
            {
                ApplicationName = AUDITAPPNAME,
                AuditType = auditType,
                Message = $"Post-Deposit Exceptions: {SidToName(CurrentSid)} has turned {pdBalancingReq} the \"Balancing Required\" post-deposit exception business" +
                          $" rule for  Bank: {workgroup.SiteBankID}-{GetBankName(workgroup.SiteBankID)}, Workgroup: " +
                          $"{workgroup.SiteClientAccountID}-{workgroup.LongName}.",
                AuditValue = workgroup.SiteClientAccountID.ToString()
            });

            var pdPayerReq = workgroup.BusinessRules != null &&
                                 workgroup.BusinessRules.PostDepositPayerRequired ? "on" : "off";
            manager.WriteAuditConfigReport(new AuditConfigDto
            {
                ApplicationName = AUDITAPPNAME,
                AuditType = auditType,
                Message = $"Post-Deposit Exceptions: {SidToName(CurrentSid)} has turned {pdPayerReq} the \"Payer Required\" post-deposit exception business" +
                          $" rule for  Bank: {workgroup.SiteBankID}-{GetBankName(workgroup.SiteBankID)}, Workgroup: " +
                          $"{workgroup.SiteClientAccountID}-{workgroup.LongName}.",
                AuditValue = workgroup.SiteClientAccountID.ToString()
            });
        }

        [HttpPost]
        [Authorize]
        public JsonResult RemoveWorkgroupFromEntity([Bind(Include = "ClientAccountKey")]
                                                    WorkgroupModel workgroup)
        {
            List<string> errors = null;
            bool partialSuccess = false;

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupAssociation, R360Permissions.ActionType.Update);
            if (!success)
            {
                errors = new List<string>() { "Access Denied" };
            }
            else if (!workgroup.RemoveWorkgroupFromEntity(out partialSuccess, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred removing the workgroup from the entity." };
            }

            var model = new
            {
                PartialSuccess = partialSuccess,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };
            return this.Json(model);
        }

        [HttpPost]
        [Authorize]
        public JsonResult VerifyDDANotAssociated(long allowedSiteBankID,
                                                 int allowedSiteClientAccountID,
                                                 [Bind(Include = "RT, DDA")]
                                                 DDAModel dda)
        {
            bool alreadyAssociated = true;
            List<string> errors = null;

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.Update);
            if (!success)
            {
                errors = new List<string>() { "Access Denied" };
            }
            else if (!dda.VerifyNotAssociated(allowedSiteBankID, allowedSiteClientAccountID, out alreadyAssociated, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occured verifying the DDA is not associated to another workgroup." };
            }

            var model = new JSONResponse()
            {
                Data = new { DDAIsAssociated = alreadyAssociated },
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };
            return this.Json(model);
        }

        [Authorize]
        public ActionResult AssignWorkgroups(int entityID, string entityName)
        {
            WorkgroupModel[] unassignedWorkgroups = null;
            List<WebErrors> webErrors = new List<WebErrors>();
            List<string> errors = null;

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupAssociation, R360Permissions.ActionType.View);
            var canAssociate = success && R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupAssociation, R360Permissions.ActionType.Update);
            if (!success)
            {
                errors = new List<string>() { "Access Denied" };
            }
            else
            {
                int fiEntityID = FIEntityModel.GetAncestorFIEntityID(entityID);
                if (fiEntityID == 0)
                {
                    success = false;
                    errors = new List<string>() { "Unable to assign workgroups.  Selected entity does not belong to a Financial Institution." };
                }
                else if (WorkgroupModel.GetUnassignedWorkgroups(fiEntityID, out unassignedWorkgroups, out errors))
                {
                    success = false;
                    if (errors == null || errors.Count == 0)
                        errors = new List<string>() { "Error occured retrieving the unassigned workgroups." };
                }
            }

            if (errors != null)
                webErrors.AddRange(errors.Select(o => new WebErrors() { Message = o }).ToList());

            var model = new
            {
                EntityID = entityID,
                EntityName = entityName,
                UnassignedWorkgroups = unassignedWorkgroups,
                CanAssociate = canAssociate,
                Success = success,
                Errors = webErrors.Count == 0 ? null : webErrors
            };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveAssignments(int entityID,
                                            [Bind(Include="ClientAccountKey, SiteBankID, SiteClientAccountID, MostRecent, IsActive, DataRetentionDays, ImageRetentionDays, ShortName, LongName, FileGroup, EntityID, ViewingDays, MaximumSearchDays, CheckImageDisplayMode, DocumentImageDisplayMode, HOA, DisplayBatchID, InvoiceBalancing, DDAs, DataEntryColumns, DataEntrySetup, BillingAccount, BillingField1, BillingField2")]
                                            WorkgroupModel[] workgroups)
        {
            List<string> errors = null;
            bool partialSuccess = false;
            string infoMessage = null;

            var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupAssociation, R360Permissions.ActionType.Update);
            if (!success)
            {
                errors = new List<string>() { "Access Denied" };
            }
            else if (!WorkgroupModel.SaveAssignments(entityID, workgroups, out partialSuccess, out errors, out infoMessage))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred assigning the workgroups." };
            }

            var model = new
            {
                PartialSuccess = partialSuccess,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null,
                InfoMessage = infoMessage
            };
            return this.Json(model);
        }
    }
}
