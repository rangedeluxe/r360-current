﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using CommonUtils.Web;
using HubConfigViews.Models;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;


namespace HubConfigViews.Controllers
{
    public class AlertsController : BaseController
    {

        private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(AlertsController));
        private RaamClient _raamClient;

        public AlertsController()
        {
            _raamClient = new RaamClient(new ConfigHelpers.Logger(x => Logger.Log(Wfs.Logging.LogLevel.Informational, x), x => Logger.Log(Wfs.Logging.LogLevel.Warning, x)));
        }
        private class EventPermission
        {
            public string Event { get; set; }
            public string Permission { get; set; }
        }
        private static readonly IReadOnlyList<EventPermission> EventPermissions = new List<EventPermission>(new EventPermission[] {
            new EventPermission { Event = "PROCESSINGEXCEPTION",    Permission = R360Permissions.Perm_AlertSystemException },
            new EventPermission { Event = "HIGHDOLLARPAYMENT",      Permission = R360Permissions.Perm_AlertHighDollar },
            new EventPermission { Event = "PAYERPAYMENTRECEIVED",   Permission = R360Permissions.Perm_AlertPaymentFromPayer },
            new EventPermission { Event = "EXTRACTCOMPLETE",        Permission = R360Permissions.Perm_AlertExtractComplete },
            new EventPermission { Event = "DECISIONPENDING",        Permission = R360Permissions.Perm_AlertDecisionPending },
            new EventPermission { Event = "FILEAVAILABLE",          Permission = R360Permissions.Perm_AlertFileRecieved },
        });

        [Authorize]
        public ActionResult Index()
        {

            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return new EmptyResult();
            }

            var model = new
            {
                CanViewAlerts = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.View),
                CanEditAlerts = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.Manage),
                CanViewNotifications = R360Permissions.Current.Allowed(R360Permissions.Perm_Notifications, R360Permissions.ActionType.View),
                CanDownload = R360Permissions.Current.Allowed(R360Permissions.Perm_AlertDownload, R360Permissions.ActionType.View),
            };
            return View(model);
        }

        [Authorize]
        public JsonResult AddAlert()
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.Manage);
            if (!hasPerm)
            {
                return Json(new
                {
                    Data = "",
                    HasErrors = true,
                    errors = new List<string>() { "" }
                });
            }

            bool success = true;
            List<EventModel> events = null;
            List<WorkgroupDto> workgroups = null;
            List<string> errors = null;

            if (!EventModel.GetEvents(out events, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving Events." };
            }

            foreach (var e in EventPermissions)
            {
                if (!R360Permissions.Current.Allowed(e.Permission, R360Permissions.ActionType.View))
                {
                    events.RemoveAll(x => x.Name.ToUpper() == e.Event);
                }
            }
            return Json(new
            {
                Data = new
                {
                    Events = events,
                    Workgroups = workgroups,
                    Labels = GetLabelsForPage("AddEventRule", true)
                },
                HasErrors = !success,
                Errors = errors?.Select(o => new WebErrors() { Message = o }).ToList()
            });
        }
        [Authorize]
        public JsonResult EditAlert(int eventRuleID)
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.Manage);
            if (!hasPerm)
            {
                return Json(new
                {
                    Data = "",
                    HasErrors = true,
                    errors = new List<string>() { "" }
                });
            }

            bool success = true;
            List<EventModel> events = null;
            List<string> errors = null;
            EventRuleModel eventRule = null;

            if (!EventRuleModel.GetEventRule(eventRuleID, out eventRule, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving Event Rule." };
            }

            if (!EventModel.GetEvents(out events, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving Events." };
            }

            foreach (var e in EventPermissions)
            {
                if (!R360Permissions.Current.Allowed(e.Permission, R360Permissions.ActionType.View))
                {
                    events.RemoveAll(x => x.Name.ToUpper() == e.Event);
                }
            }


            return Json(new
            {
                Data = new
                {
                    EventRule = eventRule,
                    Events = events,
                    AssignedUsers = eventRule.AssignedSIDs,
                    Labels = GetLabelsForPage("EditEventRule", true),
                    
                },
                HasErrors = !success,
                Errors = errors?.Select(o => new WebErrors() { Message = o }).ToList()
            });

        }

        [Authorize]
        public ActionResult AddEventRule()
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.Manage);
            if (!hasPerm)
            {
                return new EmptyResult();
            }

            bool success = true;
            List<EventModel> events = null;
            List<WorkgroupDto> workgroups = null;
            List<string> errors = null;

            if (!EventModel.GetEvents(out events, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving Events." };
            }

            foreach (var e in EventPermissions)
            {
                if (!R360Permissions.Current.Allowed(e.Permission, R360Permissions.ActionType.View))
                {
                    events.RemoveAll(x => x.Name.ToUpper() == e.Event);
                }
            }

            var resp = new JSONResponse()
            {
                Data = new
                {
                    Events = events,
                    Workgroups = workgroups,
                    Labels = GetLabelsForPage("AddEventRule", true)
                },
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };

            return View(resp);
        }

        [Authorize]
        public ActionResult EditEventRule(int eventRuleID)
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.Manage);
            if (!hasPerm)
            {
                return new EmptyResult();
            }

            bool success = true;
            List<EventModel> events = null;
            List<string> errors = null;
            EventRuleModel eventRule = null;

            if (!EventRuleModel.GetEventRule(eventRuleID, out eventRule, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving Event Rule." };
            }

            if (!EventModel.GetEvents(out events, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving Events." };
            }

            foreach (var e in EventPermissions)
            {
                if (!R360Permissions.Current.Allowed(e.Permission, R360Permissions.ActionType.View))
                {
                    events.RemoveAll(x => x.Name.ToUpper() == e.Event);
                }
            }

            //// Will need to preselect assigned Users
            //List<UserModel> assignedUsers = new List<UserModel>(); // users.FindAll(x => eventRule.AssignedSIDs.Exists(y => y.ToLower() == x.SID.ToLower()));

            ////users.RemoveAll(x => eventRule.AssignedSIDs.Exists(y => y.ToLower() == x.SID.ToLower()));
            var resp = new JSONResponse()
            {
                Data = new
                {
                    EventRule = eventRule,
                    Events = events,
                    AssignedUsers = eventRule.AssignedSIDs,
                    Labels = GetLabelsForPage("EditEventRule", true)
                },
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };

            return View(resp);
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetEventRules(bool inactive)
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return this.Json(null);
            }

            bool success = true;
            List<EventRuleModel> eventRules = null;
            List<string> errors = null;

            if (!EventRuleModel.GetEventRules(out eventRules, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving the Event Rules listing." };
            }

            if (eventRules != null)
            {
                foreach (var e in EventPermissions)
                {
                    if (!R360Permissions.Current.Allowed(e.Permission, R360Permissions.ActionType.View))
                    {
                        eventRules.RemoveAll(x => x.EventName.ToUpper() == e.Event);
                    }
                }

                if (inactive)
                {
                    eventRules.RemoveAll(x => x.IsActive == false);
                }
            }

            var resp = new JSONResponse()
            {
                Data = eventRules,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };

            return this.Json(resp);
        }

        [Authorize]
        [HttpPost]
        public JsonResult InsertEventRule(int eventID, string description, bool isActive, string eventOperator, string eventValue, int siteBankID, int siteClientAcctID, List<string> users)
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.Manage);
            if (!hasPerm)
            {
                return this.Json(null);
            }

            JSONResponse webResponse = new JSONResponse();
            try
            {

                EventRuleDto eventRule = new EventRuleDto()
                {
                    EventID = eventID,
                    Description = HttpUtility.HtmlEncode(description),
                    IsActive = isActive,
                    Operator = eventOperator,
                    EventValue = HttpUtility.HtmlEncode(eventValue),
                    SiteBankID = siteBankID,
                    SiteClientAccountID = siteClientAcctID,
                    UserRA3MSIDs = users
                };


                HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
                var resp = hubConfigMgr.AddEventRule(eventRule);

                if (resp.Status == WFS.RecHub.R360Shared.StatusCode.SUCCESS)
                    webResponse.HasErrors = false;
                else
                {
                    webResponse.HasErrors = true;
                    webResponse.Errors = new List<WebErrors>();
                    if (resp.Errors != null && resp.Errors.Count() > 0)
                        webResponse.Errors.AddRange(resp.Errors.Select(o => new WebErrors() { Message = o }).ToList());
                    else
                        webResponse.Errors.Add(new WebErrors() { Message = "Error occurred saving the alert information." });
                }
            }
            catch (Exception ex)
            {
                webResponse.HasErrors = true;
                webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Save Failed." } };

                Logger.Log(Wfs.Logging.LogLevel.Error, "Error in AlertsController: " + ex.ToString());
            }

            return this.Json(webResponse);
        }

        [Authorize]
        [HttpPost]
        public JsonResult UpdateEventRule(int eventRuleID, int eventID, string description, bool isActive, string eventOperator, string eventValue, int siteBankID, int siteClientAcctID, List<string> users)
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_Alerts, R360Permissions.ActionType.Manage);
            if (!hasPerm)
            {
                return this.Json(null);
            }

            JSONResponse webResponse = new JSONResponse();

            try
            {

                EventRuleDto eventRule = new EventRuleDto()
                {
                    EventRuleID = eventRuleID,
                    EventID = eventID,
                    Description = HttpUtility.HtmlEncode(description),
                    IsActive = isActive,
                    Operator = eventOperator,
                    EventValue = HttpUtility.HtmlEncode(eventValue),
                    SiteBankID = siteBankID,
                    SiteClientAccountID = siteClientAcctID,
                    UserRA3MSIDs = users
                };


                HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
                var resp = hubConfigMgr.UpdateEventRule(eventRule);

                if (resp.Status == WFS.RecHub.R360Shared.StatusCode.SUCCESS)
                    webResponse.HasErrors = false;
                else
                {
                    webResponse.HasErrors = true;
                    webResponse.Errors = new List<WebErrors>();
                    if (resp.Errors != null && resp.Errors.Count() > 0)
                        webResponse.Errors.AddRange(resp.Errors.Select(o => new WebErrors() { Message = o }).ToList());
                    else
                        webResponse.Errors.Add(new WebErrors() { Message = "Error occurred saving the alert information." });
                }
            }
            catch (Exception ex)
            {
                webResponse.HasErrors = true;
                webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Save Failed." } };

                Logger.Log(Wfs.Logging.LogLevel.Error, "Error in AlertsController: " + ex.ToString());
            }

            return this.Json(webResponse);
        }
    }
}
