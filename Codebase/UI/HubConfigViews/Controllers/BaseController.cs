﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Wfs.Logging;
using Wfs.Raam.Core.Helper;
using WFS.RecHub.R360WebShared;


namespace HubConfigViews.Controllers
{
    [NoCache]
	public abstract class BaseController : WfsSharedBaseController
	{
		private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(BaseController));

		public BaseController() : base()
		{
			if (ClaimsPrincipal.Current != null && ClaimsPrincipal.Current.Claims != null && ClaimsPrincipal.Current.Claims.Count() > 0)
			{
				Logger.Log(LogLevel.Debug, "Current User Claims: \r\n" + ClaimsPrincipal.Current.Claims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2));
			}
		}

        public Dictionary<string, string> GetLabelsForPage(string page, bool includeGeneral)
        {
            var pageLabels = new Dictionary<string, string>();

            // This switch will go away.  These will come from the DB.
            switch (page.ToLower())
            {
                case "addeventrule": 
                case "editeventrule":
                    pageLabels.Add("Title", "Alert");
                    pageLabels.Add("Add", "Add");
                    pageLabels.Add("Edit", "Edit");
                    pageLabels.Add("EventsLabel", "Events");
                    pageLabels.Add("Save", "Save");
                    pageLabels.Add("Cancel", "Cancel");
                    pageLabels.Add("MessageLabel", "Message");
                    pageLabels.Add("ActiveLabel", "Active");
                    pageLabels.Add("DescriptionLabel", "Description");
                    pageLabels.Add("WorkgroupLabel", "Workgroup");
                    pageLabels.Add("TableLabel", "Table");
                    pageLabels.Add("ColumnLabel", "Column");
                    pageLabels.Add("OperatorsLabel", "Operators");
                    pageLabels.Add("ValueLabel", "Value");
                    pageLabels.Add("UsersLabel", "Users");
                    pageLabels.Add("ErrorEventRequired", "Event is required.");
                    pageLabels.Add("ErrorWorkgroupRequired", "Workgroup is required.");
                    pageLabels.Add("ErrorEventOperatorRequired", "Operator is required.");
                    pageLabels.Add("ErrorEventValueRequired", "Value is required.");
                    pageLabels.Add("ErrorDescriptionMaxLength", "30 character maximum.");
                    pageLabels.Add("ErrorDescriptionRequired", "Description is required.");
                    break;
            }

            return pageLabels;
        }
	}
}