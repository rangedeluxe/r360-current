﻿using CommonUtils.Web;
using HubConfigViews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;
using WFS.RecHub.R360WebShared.Services;

namespace HubConfigViews.Controllers
{
	public class RecHubEntityMaintenanceController : BaseController
	{
		[Authorize]
		public ActionResult Index()
		{
			// Index has no data; Must select an entity from the Workgroup Selector first
			List<string> errors = null;
			var success = R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.View);
			if (!success)
				errors = new List<string> { "Access Denied" };
			var response = new
			{
				Success = success,
				Errors = errors,
			};

			return View(response);
		}

        [Authorize]
        public JsonResult GetOrCreateEntityWorkgroupDefaults(int entityId)
        {
            RecHubEntityModel entity = null;
            List<string> errors = null;
            bool success = true;
            bool notAssignable = false;

            if (R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.View))
            {
                int fiEntityID = FIEntityModel.GetAncestorFIEntityID(entityId);
                if (fiEntityID == 0)
                {
                    // No parent FI - cannot assign settings to this entity
                    notAssignable = true;
                }
                else if (!RecHubEntityModel.GetOrCreateEntityWorkgroupDefaults(entityId, out entity, out errors))
                {
                    success = false;
                    if (errors == null || errors.Count == 0)
                        errors = new List<string>() {"Error occurred retrieving the Receivables 360 Entity settings."};
                }
            }
            else
            {
                success = false;
                errors = new List<string>() {"Access Denied"};
            }

            var response = new
            {
                Entity = entity,
                Success = success,
                NotAssignable = notAssignable,
                Errors = errors != null ? errors.Select(o => new WebErrors() {Message = o}).ToList() : null
            };

            return this.Json(response);
        }

        [Authorize]
        public ActionResult EditEntity(int entityId)
        {
            RecHubEntityModel entity = null;
            int maxRetentionDays = 0;
            List<String> errors = null;
            bool success = true;

            if (R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.Update))
            {
                if (!RecHubEntityModel.GetOrCreateEntityWorkgroupDefaults(entityId, out entity, out errors))
                {
                    success = false;
                    if (errors == null || errors.Count == 0)
                        errors = new List<string>() { "Error occurred retrieving the Receivables 360 Entity settings for editing." };
                }
                else if (!SystemSettingsModel.GetMaxDataRetentionDays(out maxRetentionDays, out errors))
                {
                    success = false;
                    if (errors == null || errors.Count == 0)
                        errors = new List<string>() { "Error occurred retrieving the system's default viewing days." };
                }
                else if (entity == null)
                {
                    entity = new RecHubEntityModel() { EntityID = entityId };
                    entity.SetDefaultSettings();
                }
            }
            else
            {
                success = false;
                errors = new List<string>() { "Access Denied" };
            }

            var response = new
            {
                Entity = entity,
                MaxRetentionDays = maxRetentionDays,
                Success = success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };

            // Get the sessiontoken for CSRF attacks.
            var generator = new SessionTokenProvider();
            ViewBag.SessionToken = generator.GenerateSessionToken(R360ServiceContext.Current.GetSessionID().ToString());
            return View(response);
        }

        [HttpPost]
		[Authorize]
        [ValidateSessionToken("HashToken")]
		public JsonResult Save([Bind(Include="EntityID, DisplayBatchID, DocumentImageDisplayMode, MaximumSearchDays, PaymentImageDisplayMode, ViewingDays, BillingAccount, BillingField1, BillingField2")]
                               RecHubEntityModel entity)
		{
			

			List<string> errors = null;
			bool success = true;

			if (R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupMaintenance, R360Permissions.ActionType.Update))
			{
				if (!entity.Save(out errors))
				{
					success = false;
					if (errors == null || errors.Count == 0)
						errors = new List<string>() { "Error occurred saving the Receivables 360 Entity settings information." };
				}
			}
			else
			{
				success = false;
				errors = new List<string>() { "Access Denied" };
			}

			var response = new
			{
				HasErrors = !success,
				Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
			};
			return this.Json(response);
		}
	}
}
