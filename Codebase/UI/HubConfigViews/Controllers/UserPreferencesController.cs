﻿using CommonUtils.Web;
using HubConfigViews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;

namespace HubConfigViews.Controllers
{
    public class UserPreferencesController : BaseController
    {
        // GET: UserPreferences

        private UserServiceManager _R360UserService;
        protected UserServiceManager R360UserServiceClient
        {
            get
            {
                if (_R360UserService == null)
                {
                    _R360UserService = new UserServiceManager();
                }
                return _R360UserService;
            }
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_UserPreferences, R360Permissions.ActionType.View)]
        public ActionResult Index()
        {
            var prefs = getPreferences();

            var model = new JSONResponse()
            {
                Data = prefs,
                HasErrors = false,                
            };

            return View(model);
        }

        protected UserPreferencesModel getPreferences()
        {
            var resp = R360UserServiceClient.GetUserPreferences();
            UserPreferencesDTO dto = resp.Data;
            return new UserPreferencesModel(dto);
        }

        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_UserPreferences, R360Permissions.ActionType.View)]
        public JsonResult GetPreferences()
        {
            var data = getPreferences();
            return Json(data);
        }

        [HttpPost]
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_UserPreferences, R360Permissions.ActionType.View)]
        public JsonResult RestoreDefaults()
        {
            JSONResponse webResponse = new JSONResponse();
            if (!R360Permissions.Current.Allowed(R360Permissions.Perm_UserPreferences, R360Permissions.ActionType.View))
            {
                webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Access Denied." } };
                webResponse.HasErrors = true;
            }
            else
            {
                try
                {
                    var resp = R360UserServiceClient.RestoreDefaultPreferences();
                    if(resp.Status == StatusCode.SUCCESS)
                    {
                        webResponse.HasErrors = false;
                        webResponse.Data = resp.Data;
                    }
                    else
                    {
                        webResponse.HasErrors = true;
                        webResponse.Errors = new List<WebErrors>();
                        if (resp.Errors != null && resp.Errors.Count > 0)
                            webResponse.Errors.AddRange(resp.Errors.Select(o => new WebErrors() { Message = o }).ToList());
                        else
                            webResponse.Errors.Add(new WebErrors() { Message = "Error occurred restoring preferences" });
                    }
                }
                catch( Exception e)
                {
                    webResponse.HasErrors = true;
                    webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Restoring Preferences Failed." } };
                }
            }
            return Json(webResponse);
        }

        [HttpPost]
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_UserPreferences, R360Permissions.ActionType.View)]
        public JsonResult SavePreferences([Bind(Include = "recordsPageAccesible,displayRemitterAccesible,recordsPerPage,displayRemitterNameInPDF")] UserPreferencesModel prefs)
        {
            JSONResponse webResponse = new JSONResponse();

            if (!R360Permissions.Current.Allowed(R360Permissions.Perm_UserPreferences, R360Permissions.ActionType.View))
            {
                webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Access Denied." } };
                webResponse.HasErrors = true;
            }   
            else
            {
                var dto = new UserPreferencesDTO()
                {
                    displayRemitterNameinPdf = prefs.displayRemitterNameInPDF,
                    recordsPerPage = prefs.recordsPerPage
                };
                try
                {
                    var resp = R360UserServiceClient.SetUserPreferences(dto);
                    if (resp.Status == StatusCode.SUCCESS)
                    {
                        webResponse.HasErrors = false;
                    }
                    else
                    {
                        webResponse.HasErrors = true;
                        webResponse.Errors = new List<WebErrors>();
                        if (resp.Errors != null && resp.Errors.Count > 0)
                            webResponse.Errors.AddRange(resp.Errors.Select(o => new WebErrors() { Message = o }).ToList());
                        else
                            webResponse.Errors.Add(new WebErrors() { Message = "Error occurred saving preferences" });
                    }
                }
                catch (Exception e)
                {
                    webResponse.HasErrors = true;
                    webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Saving Preferences Failed." } };
                
                }
            }


            return Json(webResponse);
        }
    }
}