﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CommonUtils.Web;
using WFS.RecHub.HubConfig;
using WFS.RecHub.HubConfig.Core;
using HubConfigViews.Models;
using WFS.RecHub.R360Shared;


namespace HubConfigViews.Controllers
{
    public class PaymentSourceController : BaseController
    {
        #region Member Variables

        private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(PaymentSourceController));

        #endregion

        #region Methods

        #region Public

        #region Page Results

		[Authorize]
        public ActionResult Index()
        {

            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return new EmptyResult();
            }

            var model = new
            {
                CanModifyPaymentSources = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.Manage),
            };

            return View(model);
        }

		[Authorize]
        public ActionResult AddPaymentSource()
        {

            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return new EmptyResult();
            }

            PaymentSourceModel model = new PaymentSourceModel();
            model.SystemTypes = new SystemTypeModel().BuildListFromEnum();
            model.FIEntities = FIEntityModel.GetFIEntities();

            return View(model);
        }

        [HttpPost]
		[Authorize]
        public ActionResult EditPaymentSource([Bind(Include = "BatchSourceKey")]
                                              PaymentSourceDto incommingPayment)
        {

            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return new EmptyResult();
            }

			// Audit the identifiers only
			AuditFormFields("BatchSourceKey", "ShortName");

			HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
            HubConfigResponse<PaymentSourceDto> paymentSource = hubConfigMgr.GetPaymentSource(incommingPayment.BatchSourceKey);

            PaymentSourceModel model = new PaymentSourceModel();
            model.SystemTypes = new SystemTypeModel().BuildListFromEnum();
            model.FIEntities = FIEntityModel.GetFIEntities();
            model.PaymentSource = paymentSource.Data;

            return View(model);
        }

        [HttpPost]
		[Authorize]
        public ActionResult DeletePaymentSourceModal(PaymentSourceDto incommingPayment)
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return new EmptyResult();
            }

			// Audit the identifiers only
			AuditFormFields("BatchSourceKey", "ShortName");

			PaymentSourceModel model = new PaymentSourceModel();
            model.PaymentSource = incommingPayment;

            return View(model);
        }

        #endregion

        #region Data Results

		[Authorize]
        [HttpPost]
        public JsonResult GetPaymentSources()
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return this.Json(null);
            }

            JSONResponse webResponse = new JSONResponse();
            HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
            HubConfigResponse<List<PaymentSourceDto>> response = hubConfigMgr.GetPaymentSources();

            webResponse.Data = GetMatchedFIEntities(response.Data);

            return this.Json(webResponse);
        }

		[Authorize]
        [HttpPost]
        public JsonResult InsertPaymentSource(string shortName, string longName, bool isActive, string systemType, int fiEntityID)
        {

            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return this.Json(null);
            }

			// Audit the identifiers
			AuditFormFields("shortName");

			JSONResponse webResponse = new JSONResponse();

            try
            {
                PaymentSourceDto paymentSource = new PaymentSourceDto()
                {
                    ShortName = shortName,
                    LongName = longName,
                    IsActive = isActive,
                    SystemType = systemType,
                    FIEntityID = fiEntityID
                };

                HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
                var resp = hubConfigMgr.AddPaymentSource(paymentSource);

                if (resp.Status == WFS.RecHub.R360Shared.StatusCode.SUCCESS)
                    webResponse.HasErrors = false;
                else
                {
                    webResponse.HasErrors = true;
					webResponse.Errors = resp.Errors != null ? resp.Errors.Select(o => new WebErrors() { Message = o }).ToList() : null;
                }
            }
            catch (Exception ex)
            {
                webResponse.HasErrors = true;
                webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Save Failed." } };

                Logger.Log(Wfs.Logging.LogLevel.Error, "Error in PaymentSourceController: " + ex.Message);
            }

            return this.Json(webResponse); 
        }

		[Authorize]
        [HttpPost]
        public JsonResult UpdatePaymentSource(int batchSourceKey, string shortName, string longName, bool isActive, string systemType, int fiEntityID)
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return this.Json(null);
            }

			// Audit the identifiers
			AuditFormFields("batchSourceKey", "shortName");

			JSONResponse webResponse = new JSONResponse();

            try
            {
                PaymentSourceDto paymentSource = new PaymentSourceDto()
                {
                    BatchSourceKey = batchSourceKey,
                    ShortName = shortName,
                    LongName = longName,
                    IsActive = isActive,
                    SystemType = systemType,
                    FIEntityID = fiEntityID
                };

                HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
                var resp = hubConfigMgr.UpdatePaymentSource(paymentSource);

                if (resp.Status == WFS.RecHub.R360Shared.StatusCode.SUCCESS)
                    webResponse.HasErrors = false;
                else
                {
                    webResponse.HasErrors = true;
                    webResponse.Errors.AddRange(resp.Errors.Select(o => new WebErrors() { Message = o }).ToList());
                }
            }
            catch (Exception ex)
            {
                webResponse.HasErrors = true;
                webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Save Failed." } };

                Logger.Log(Wfs.Logging.LogLevel.Error, "Error in PaymentSourceController: " + ex.Message);
            }

            return this.Json(webResponse);
        }

		[Authorize]
        [HttpPost]
        public JsonResult DeletePaymentSource(int batchSourceKey)
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSource, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return this.Json(null);
            }

			// Audit the identifiers
			AuditFormFields("batchSourceKey");

			JSONResponse webResponse = new JSONResponse();

            try
            {
                PaymentSourceDto paymentSource = new PaymentSourceDto()
                {
                    BatchSourceKey = batchSourceKey
                };

                HubConfigServicesManager hubConfigMgr = new HubConfigServicesManager();
                var resp = hubConfigMgr.DeletePaymentSource(paymentSource);

                if (resp.Status == WFS.RecHub.R360Shared.StatusCode.SUCCESS)
                    webResponse.HasErrors = false;
                else
                {
                    webResponse.HasErrors = true;
                    webResponse.Errors = new List<WebErrors>();
                    webResponse.Errors.AddRange(resp.Errors.Select(o => new WebErrors() { Message = o }).ToList());
                }
            }
            catch (Exception ex)
            {
                webResponse.HasErrors = true;
                webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = "Delete Failed." } };

                Logger.Log(Wfs.Logging.LogLevel.Error, "Error in PaymentSourceController: " + ex.Message);
            }

            return this.Json(webResponse);
        }

        #endregion

        #endregion

        #region Private
        /// <summary>
        /// Returns a list of PaymentSources matched with FI Entities the user has access to and unmatched sources
        /// </summary>
        /// <param name="paymentSources"></param>
        /// <returns></returns>
        private List<PaymentSourceDto> GetMatchedFIEntities(List<PaymentSourceDto> paymentSources)
        {
            var entities = FIEntityModel.GetFIEntities().ToList();
            var allowedPaymentSource = new List<PaymentSourceDto>();
            foreach (var source in paymentSources)
            {
                //always add to the list if paymentsource has no assigned entity
                if (source.FIEntityID == 0)
                {
                    source.FIEntityName = string.Empty;
                    allowedPaymentSource.Add(source);
                }
                else
                {
                    //don't add to the list unless there is  a match with an entity
                    var entity = entities.FirstOrDefault(x => x.ID == source.FIEntityID);
                    if (entity != null)
                    {
                        source.FIEntityName = entity.Name;
                        allowedPaymentSource.Add(source);
                    }
                }

            }
            return allowedPaymentSource;
        }

        #endregion

        #endregion

    }
}
