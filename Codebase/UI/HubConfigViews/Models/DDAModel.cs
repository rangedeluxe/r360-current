﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.R360Shared;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class DDAModel
	{
		public string RT { get; set; }
		public string DDA { get; set; }

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(DDAModel));
				return _logger;
			}
		}

		public bool VerifyNotAssociated(long allowedSiteBankID, int allowedSiteClientAccountID, out bool ddaAlreadyAssociated, out List<string> errors)
		{
			errors = null;
			ddaAlreadyAssociated = true;//Assume it is, until we report that it is not

			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetElectronicAccountByDDA(LoadFromModel(this));
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				if (response.Data == null || (response.Data.SiteBankID == allowedSiteBankID && response.Data.SiteClientAccountID == allowedSiteClientAccountID))
					ddaAlreadyAssociated = false;

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in VerifyNotAssociated\r\n" + ex.ToString());
				return false;
			}
		}


		#region Helper Methods

		public static DDAModel LoadFromDto(DDADto ddaDto)
		{
			return new DDAModel()
			{
				RT = ddaDto.RT,
				DDA = ddaDto.DDA
			};
		}

		public static DDADto LoadFromModel(DDAModel ddaModel)
		{
			return new DDADto()
			{
				RT = ddaModel.RT,
				DDA = ddaModel.DDA
			};
		}
		#endregion
	}
}