﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Models
{
    public class EventModel
    {
        #region Member Variables

        private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(EventModel));

        #endregion

        #region Properties

        public int ID { get; set; }
        public string Name { get; set; }
        public string EventLongName { get; set; }
        public int Type { get; set; }
        public int Level { get; set; }
        public bool IsActive { get; set; }
        public string Message { get; set; }
        public string Schema { get; set; }
        public string Table { get; set; }
        public string Column { get; set; }
        public List<OperatorsModel> Operators { get; set; }

        #endregion

        #region Methods

        public static bool GetEvents(out List<EventModel> events, out List<string> errors)
        {
            errors = null;
            events = null;
            try
            {
                var manager = new HubConfigServicesManager();
                var response = manager.GetEvents();

                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                events = response.Data.Select(o => LoadFromDto(o)).ToList<EventModel>();

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetEvents\r\n" + ex.ToString());
                return false;
            }
        }

        #region Helper Methods

        public static EventModel LoadFromDto(EventDto eventRuleDto)
        {
            return new EventModel()
            {
                ID = eventRuleDto.ID,
                Name = eventRuleDto.Name,
                EventLongName = eventRuleDto.EventLongName,
                IsActive = eventRuleDto.IsActive,
                Type = eventRuleDto.Type,
                Level = eventRuleDto.Level,
                Message = eventRuleDto.Message,
                Schema = eventRuleDto.Schema,
                Table = eventRuleDto.Table,
                Column = eventRuleDto.Column,
                Operators = eventRuleDto.Operators.Select(o => new OperatorsModel {  ID = o, Name = o }).ToList<OperatorsModel>()
            };
        }

        public static EventDto LoadFromModel(EventModel eventModel)
        {
            return new EventDto()
            {
                ID = eventModel.ID,
                Name = eventModel.Name,
                IsActive = eventModel.IsActive,
                Type = eventModel.Type,
                Level = eventModel.Level,
                Message = eventModel.Message,
                Schema = eventModel.Schema,
                Table = eventModel.Table,
                Column = eventModel.Column,
                Operators = eventModel.Operators.Select(o => o.Name).ToList<string>()

            };
        }

        #endregion

        #endregion
    }

    public class OperatorsModel
    {
        public string ID  { get; set; }
        public string Name { get; set; }
    }
}