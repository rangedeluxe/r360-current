﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.HubConfig.Core;

namespace HubConfigViews.Models
{
    public class SystemTypeModel
    {
        public string Name { get; set; }
        public string ID { get; set; }

        public IEnumerable<SystemTypeModel> BuildListFromEnum()
        {
            List<SystemTypeModel> models = new List<SystemTypeModel>();
            IEnumerable<string> list = Enum.GetNames(typeof(SystemType)).Cast<String>().OrderBy(name=>name);

            foreach (string systemType in list)
            {
                models.Add(new SystemTypeModel() { Name = systemType, ID = systemType });
            }

            return models;
        }
    }
}