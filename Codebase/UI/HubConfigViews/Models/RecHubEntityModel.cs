﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.R360Shared;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class RecHubEntityModel
	{
		public int EntityID { get; set; }

		public bool DisplayBatchID { get; set; }
		public ImageDisplayModes PaymentImageDisplayMode { get; set; }
		public ImageDisplayModes DocumentImageDisplayMode { get; set; }
		public int ViewingDays { get; set; }
		public int MaximumSearchDays { get; set; }
		public string BillingAccount { get; set; }
		public string BillingField1 { get; set; }
		public string BillingField2 { get; set; }

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(RecHubEntityModel));
				return _logger;
			}
		}

        public static bool GetOrCreateEntityWorkgroupDefaults(int entityId, out RecHubEntityModel entity,
            out List<string> errors)
        {
            errors = null;
            entity = null;
            try
            {
                var manager = new HubConfigServicesManager();
                var response = manager.GetOrCreateEntityWorkgroupDefaults(entityId);
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                if (response.Data != null)
                    entity = LoadFromDto(response.Data);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetEntity\r\n" + ex.ToString());
                return false;
            }
        }

	    public bool Save(out List<string> errors)
		{
			errors = null;

			RecHubEntityDto entity = LoadFromModel(this);

			try
			{
				var manager = new HubConfigServicesManager();

				var response = manager.AddUpdateEntity(entity);
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in Save\r\n" + ex.ToString());
				return false;
			}
		}

		public void SetDefaultSettings()
		{
			DisplayBatchID = false;
			PaymentImageDisplayMode = ImageDisplayModes.UseInheritedSetting;
			DocumentImageDisplayMode = ImageDisplayModes.UseInheritedSetting;
			ViewingDays = 0; //TODO: Default to system setting?
			MaximumSearchDays = 0; //TODO: Default to system setting?
			BillingAccount = null;
			BillingField1 = null;
			BillingField2 = null;
		}

		#region Helper Methods

		public static RecHubEntityModel LoadFromDto(RecHubEntityDto entityDto)
		{
			return new RecHubEntityModel()
			{
				EntityID = entityDto.EntityID,
				DisplayBatchID = entityDto.DisplayBatchID,
				DocumentImageDisplayMode = entityDto.DocumentImageDisplayMode,
				MaximumSearchDays = entityDto.MaximumSearchDays,
				PaymentImageDisplayMode = entityDto.PaymentImageDisplayMode,
				ViewingDays = entityDto.ViewingDays,
				BillingAccount = entityDto.BillingAccount,
				BillingField1 = entityDto.BillingField1,
				BillingField2 = entityDto.BillingField2,
			};
		}

		public static RecHubEntityDto LoadFromModel(RecHubEntityModel entityModel)
		{
			return new RecHubEntityDto()
			{
				EntityID = entityModel.EntityID,
				DisplayBatchID = entityModel.DisplayBatchID,
				DocumentImageDisplayMode = entityModel.DocumentImageDisplayMode,
				MaximumSearchDays = entityModel.MaximumSearchDays,
				PaymentImageDisplayMode = entityModel.PaymentImageDisplayMode,
				ViewingDays = entityModel.ViewingDays,
				BillingAccount = entityModel.BillingAccount,
				BillingField1 = entityModel.BillingField1,
				BillingField2 = entityModel.BillingField2,
			};
		}

		#endregion
	}
}