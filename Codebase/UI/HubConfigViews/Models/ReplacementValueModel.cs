﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class ReplacementValueModel
	{
		public char Original { get; set; }
		public int? Replacement { get; set; }

		#region Helper Methods

		public static ReplacementValueModel LoadFromDto(KeyValuePair<char, int?> rvDto)
		{
			return new ReplacementValueModel()
			{
				Original = rvDto.Key,
				Replacement = rvDto.Value
			};
		}

		#endregion
	}
}