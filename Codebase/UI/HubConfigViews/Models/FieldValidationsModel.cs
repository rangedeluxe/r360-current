﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class FieldValidationsModel
	{
		public string FailureMessage { get; set; }
		public int ValidationTypeKey { get; set; }
		public string[] FieldValues { get; set; }
		public string ModificationDateTime { get; set; }
		public int ValueCount { get; set; }

		public static FieldValidationsModel LoadFromDto(FieldValidationsDto fvDto)
		{
			return new FieldValidationsModel()
			{
				FailureMessage = fvDto.FailureMessage,
				ValidationTypeKey = fvDto.ValidationTypeKey,
				FieldValues = fvDto.FieldValues == null ? null : fvDto.FieldValues.ToArray(),
				ModificationDateTime = fvDto.ModificationDateTime,
				ValueCount = fvDto.ValueCount
			};
		}

		public static FieldValidationsDto LoadFromModel(FieldValidationsModel fvModel)
		{
			return new FieldValidationsDto()
			{
				FailureMessage = fvModel.FailureMessage,
				ValidationTypeKey = fvModel.ValidationTypeKey,
				FieldValues = fvModel.FieldValues == null ? null : fvModel.FieldValues.ToList()
			};
		}
	}
}