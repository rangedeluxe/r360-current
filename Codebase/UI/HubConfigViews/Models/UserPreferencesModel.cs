﻿using WFS.RecHub.R360Services.Common.DTO;

namespace HubConfigViews.Models
{
    public class UserPreferencesModel
    {
        public bool recordsPageAccesible { get; set; }
        public bool displayRemitterAccesible { get; set; }
        public int recordsPerPage { get; set; }
        public bool displayRemitterNameInPDF { get; set; }
        public UserPreferencesModel() { }
        public UserPreferencesModel(UserPreferencesDTO dto)
        {
            displayRemitterAccesible = dto.displayRemitterNameinPdf == null ? false : true;
            recordsPageAccesible = dto.recordsPerPage == null ? false : true;
            displayRemitterNameInPDF = dto.displayRemitterNameinPdf.HasValue ? dto.displayRemitterNameinPdf.Value : false;
            recordsPerPage = dto.recordsPerPage.HasValue ? dto.recordsPerPage.Value : 0;
        }
    }
}