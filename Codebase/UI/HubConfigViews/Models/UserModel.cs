﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubConfigViews.Models
{
    public class UserModel
    {
        #region Member Variables

        private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(UserModel));

        #endregion

        #region Properties

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string SID { get; set; }
        public string Name { get { return LastName + ", " + FirstName + "  (" + Email + ")"; } }

        #endregion

        #region Methods

        public static bool GetUsersByEntityAssociation(int entityID, out List<UserModel> users, out List<string> errors)
        {
            errors = null;
            users = null;
            try
            {
                users = new List<UserModel>();
                users.Add(new UserModel() { ID = 2, FirstName = "Test", LastName = "User", Email = "testuser@wfs.com", UserName = "Test", SID = "D1306825-8B87-4DE2-9C52-E51880426367" });
                users.Add(new UserModel() { ID = 3, FirstName = "Test02", LastName = "User02", Email = "test0202user@wfs.com", UserName = "Test2", SID = "215B4AA3-9217-4B23-A1DE-8EF4AE4ECA09" });
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetUsersByWorkgroupAssociation\r\n" + ex.ToString());
                return false;
            }
        }

        #endregion

    }
}