﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class DataEntrySetupModel
	{
		public short? DeadLineDay { get; set; }
		public bool RequiresInvoice { get; set; }
		public BusinessRulesModel[] BusinessRules { get; set; }

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(DataEntrySetupModel));
				return _logger;
			}
		}

		#region Helper Methods

		public static DataEntrySetupModel LoadFromDto(DataEntrySetupDto deSetupDto)
		{
			return new DataEntrySetupModel()
			{
				DeadLineDay = deSetupDto.DeadLineDay,
				RequiresInvoice = deSetupDto.RequiresInvoice,
				BusinessRules = deSetupDto.BusinessRules == null ? null : deSetupDto.BusinessRules.ConvertAll(o => BusinessRulesModel.LoadFromDto(o)).ToArray()
			};
		}

		public static DataEntrySetupDto LoadFromModel(DataEntrySetupModel deSetupModel)
		{
			return new DataEntrySetupDto()
			{
				DeadLineDay = deSetupModel.DeadLineDay,
				RequiresInvoice = deSetupModel.RequiresInvoice,
				BusinessRules = deSetupModel.BusinessRules == null ? null : deSetupModel.BusinessRules.ToList().ConvertAll(o => BusinessRulesModel.LoadFromModel(o))
			};
		}
		#endregion
	}
}