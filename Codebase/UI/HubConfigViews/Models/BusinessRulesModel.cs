﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Models
{
	public class BusinessRulesModel
	{
		public string TableType { get; set; }
		public string FieldName { get; set; }
		public int? BatchSourceKey { get; set; }
		public int? BatchPaymentTypeKey { get; set; }
		public int? BatchPaymentSubTypeKey { get; set; }
		public int? ExceptionDisplayOrder { get; set; }
		public short MinLength { get; set; }
		public short MaxLength { get; set; }
		public bool Required { get; set; }
		public bool UserCanEdit { get; set; }
		public string RequiredMessage { get; set; }
		public string FormatType { get; set; }
		public string Mask { get; set; }
		public CheckDigitRoutineModel CheckDigitRoutine { get; set; }
		public FieldValidationsModel FieldValidations { get; set; }

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(WorkgroupModel));
				return _logger;
			}
		}

		public static bool GetFieldValidationSettings(int siteBankID, int workgroupId, out BusinessRulesModel[] businessRules, out List<string> errors)
		{
			errors = null;
			businessRules = null;
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetFieldValidationSettings(siteBankID, workgroupId);
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				businessRules = response.Data.Select(o => LoadFromDto(o)).ToArray();

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetWorkgroups\r\n" + ex.ToString());
				return false;
			}
		}


		#region Enum Helpers
		private static object _oLock = new object();

		private static Dictionary<FormatType, string> _formatTypeMapping = null;
		private static Dictionary<FormatType, string> FormatTypeMapping
		{
			get
			{
				if (_formatTypeMapping == null)
				{
					lock (_oLock)
					{
						if (_formatTypeMapping == null)
						{
							_formatTypeMapping = new Dictionary<FormatType, string>();
							FormatType[] formatTypes = (FormatType[])Enum.GetValues(typeof(FormatType));
							foreach (FormatType type in formatTypes)
							{
								//add additional translations here
								string description;
								switch (type)
								{
									case WFS.RecHub.HubConfig.FormatType.AlphanumericAndSpecialChar:
										description = "Alphanumeric and Special Characters";
										break;
									case WFS.RecHub.HubConfig.FormatType.CustomDate:
										description = "Custom Date";
										break;
									case WFS.RecHub.HubConfig.FormatType.NumberAndSpecialChar:
										description = "Numbers and Special Characters";
										break;
									case WFS.RecHub.HubConfig.FormatType.NumberDashes:
										description = "Numbers and Dashes";
										break;
									//case WFS.RecHub.HubConfig.FormatType.OtherAmount:
									//	description = "Other Amount";
									//	break;
									default:
										description = type.ToString();
										break;
								}
								_formatTypeMapping.Add(type, description);
							}
						}
					}
				}
				return _formatTypeMapping;
			}
		}

		public static string[] FormatTypes
		{
			get
			{
				return FormatTypeMapping.Values.ToArray();
			}
		}

		private static string GetFormatType(WFS.RecHub.HubConfig.FormatType formatType)
		{
			string desc;
			if (FormatTypeMapping.TryGetValue(formatType, out desc))
				return desc;
			return formatType.ToString();
		}

		private static WFS.RecHub.HubConfig.FormatType GetFormatType(string formatTypeDesc)
		{
			return FormatTypeMapping.FirstOrDefault(o => o.Value == formatTypeDesc).Key;
		}

		#endregion


		public static BusinessRulesModel LoadFromDto(BusinessRulesDto brDto)
		{
			return new BusinessRulesModel()
			{
				TableType = DataEntryColumnModel.GetTableTypeFromEnum(brDto.TableType),
				FieldName = brDto.FieldName,
				BatchSourceKey = brDto.BatchSourceKey,
				BatchPaymentTypeKey = brDto.BatchPaymentTypeKey,
				BatchPaymentSubTypeKey = brDto.BatchPaymentSubTypeKey,
				ExceptionDisplayOrder = brDto.ExceptionDisplayOrder,
				MinLength = brDto.MinLength,
				MaxLength = brDto.MaxLength,
				Required = brDto.Required,
				UserCanEdit = brDto.UserCanEdit,
				RequiredMessage = brDto.RequiredMessage,
				FormatType = GetFormatType(brDto.FormatType),
				Mask = brDto.Mask,
				CheckDigitRoutine = brDto.CheckDigitRoutine == null ? null : CheckDigitRoutineModel.LoadFromDto(brDto.CheckDigitRoutine),
				FieldValidations = brDto.FieldValidations == null ? null : FieldValidationsModel.LoadFromDto(brDto.FieldValidations)
			};
		}

		public static BusinessRulesDto LoadFromModel(BusinessRulesModel brModel)
		{
			ETableType tableType = DataEntryColumnModel.GetTableTypeFromSetup(brModel.TableType, brModel.FieldName);

			return new BusinessRulesDto()
			{
				TableType = tableType,
				FieldName = brModel.FieldName,
				BatchSourceKey = brModel.BatchSourceKey,
				BatchPaymentTypeKey = brModel.BatchPaymentTypeKey,
				BatchPaymentSubTypeKey = brModel.BatchPaymentSubTypeKey,
				ExceptionDisplayOrder = brModel.ExceptionDisplayOrder,
				MinLength = brModel.MinLength,
				MaxLength = brModel.MaxLength,
				Required = brModel.Required,
				UserCanEdit = brModel.UserCanEdit,
				RequiredMessage = brModel.RequiredMessage,
				FormatType = GetFormatType(brModel.FormatType),
				Mask = brModel.Mask,
				CheckDigitRoutine = brModel.CheckDigitRoutine == null ? null : CheckDigitRoutineModel.LoadFromModel(brModel.CheckDigitRoutine),
				FieldValidations = brModel.FieldValidations == null ? null : FieldValidationsModel.LoadFromModel(brModel.FieldValidations)
			};
		}
	}
}