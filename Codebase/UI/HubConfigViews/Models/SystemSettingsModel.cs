﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Models
{
    public class SystemSettingsModel
    {
        #region Member Variables

		private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(SystemSettingsModel));

        #endregion

        #region Properties

        #endregion

        #region Methods

        public static bool GetMaxDataRetentionDays(out int maxRetentionDays, out List<string> errors)
        {
			errors = null;
			maxRetentionDays = 0;
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetSystemMaxRetentionDays();
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				maxRetentionDays = response.Data;
				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetMaxDataRetentionDays\r\n" + ex.ToString());
				return false;
			}
        }

        #endregion

    }
}