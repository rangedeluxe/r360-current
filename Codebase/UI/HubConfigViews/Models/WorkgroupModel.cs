﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.R360Shared;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class WorkgroupModel
	{
		public int ClientAccountKey { get; set; }
		public int SiteBankID { get; set; }
		public int SiteClientAccountID { get; set; }
		public bool MostRecent { get; set; }
		public bool IsActive { get; set; }
		public short DataRetentionDays { get; set; }
		public short ImageRetentionDays { get; set; }
		public string ShortName { get; set; }
		public string LongName { get; set; }
		public string FileGroup { get; set; }

		public int? EntityID { get; set; }
        public string EntityName { get; set; }

		public int? ViewingDays { get; set; }
		public int? MaximumSearchDays { get; set; }
		public string CheckImageDisplayMode { get; set; }
		public string DocumentImageDisplayMode { get; set; }
		public bool HOA { get; set; }
		public bool? DisplayBatchID { get; set; }
		public string InvoiceBalancing { get; set; }


		public DDAModel[] DDAs { get; set; }
		public DataEntryColumnModel[] DataEntryColumns { get; set; }
		public DataEntrySetupModel DataEntrySetup { get; set; }

		public string BillingAccount { get; set; }
		public string BillingField1 { get; set; }
		public string BillingField2 { get; set; }
        public WorkgroupBusinessRulesDto BusinessRules { get; set; }

		private static string NoAccessAddInfo = "You have added a workgroup that your role is not authorized to view.  The add was successful, however to view the results, the authorization policy for that workgroup needs to be added to the appropriate role in Security Admin.";
		private static string NoAccessAssociateInfo = "You have associated one or more workgroup(s) that your role is not authorized to view.  The association was successful, however to view the results, the authorization policy for those workgroups need to be added to the appropriate role in Security Admin.";

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(WorkgroupModel));
				return _logger;
			}
		}

		public static bool GetWorkgroups(int entityID, out WorkgroupModel[] workgroups, out List<string> errors)
		{
			errors = null;
			workgroups = null;
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetWorkgroups(entityID);
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				workgroups = response.Data.Select(o => LoadFromDto(o)).ToArray();

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetWorkgroups\r\n" + ex.ToString());
				return false;
			}
		}

		public static bool GetUnassignedWorkgroupCount(int fiEntityID, out int unassignedCount, out List<string> errors)
		{
			errors = null;
			unassignedCount = 0;

			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetUnassignedWorkgroupCount(fiEntityID);
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				unassignedCount = response.Data;
				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetUnassignedWorkgroupCount\r\n" + ex.ToString());
				return false;
			}
		}

		public static bool GetUnassignedWorkgroups(int fiEntityID, out WorkgroupModel[] workgroups, out List<string> errors)
		{
			errors = null;
			workgroups = null;
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetUnassignedWorkgroups(fiEntityID);
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				workgroups = response.Data.Select(o => LoadFromDto(o)).ToArray();

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetUnassignedWorkgroups\r\n" + ex.ToString());
				return false;
			}
		}

		public static bool GetWorkgroup(int clientAccountKey, out WorkgroupModel workgroup, out List<string> errors)
		{
			errors = null;
			workgroup = null;

			try
			{
				var manager = new HubConfigServicesManager();
                var response = manager.GetWorkgroup(clientAccountKey,
                    GetWorkgroupAdditionalData.Ddas |
                    GetWorkgroupAdditionalData.DataEntryColumns |
                    GetWorkgroupAdditionalData.BusinessRules);
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				workgroup = LoadFromDto(response.Data);

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetWorkgroup\r\n" + ex.ToString());
				return false;
			}
		}

		public bool Save(out List<string> errors, out string infoMessage)
		{
			errors = null;
			infoMessage = null;

			WorkgroupDto workgroup = LoadFromModel(this);
            if(workgroup == null)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Failed to load workgroup from model.");
                return false;
            }

            try
			{
				var manager = new HubConfigServicesManager();
				HubConfigResponse<int> response = null;

				bool adding = workgroup.ClientAccountKey == 0;
				if (adding)
					response = manager.AddWorkgroup(workgroup);
				else
					response = manager.UpdateWorkgroup(workgroup);

				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}
				else if (adding)
				{
					if (!R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupVirtual, R360Permissions.ActionType.View))
						infoMessage = NoAccessAddInfo;
				}

				ClientAccountKey = response.Data;
				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in Save\r\n" + ex.ToString());
				return false;
			}
		}

		public bool RemoveWorkgroupFromEntity(out bool partialSuccess, out List<string> errors)
		{
			partialSuccess = false;
			errors = null;

            WorkgroupDto workgroup = LoadFromModel(this);
            if (workgroup == null)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Failed to load workgroup from model.");
                return false;
            }

            try
            {
                var manager = new HubConfigServicesManager();
				var response = manager.RemoveWorkgroupFromEntity(workgroup.ClientAccountKey);

				if (response.Status != StatusCode.SUCCESS)
				{
					partialSuccess = response.Data;
					errors = response.Errors;
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in RemoveWorkgroupFromEntity\r\n" + ex.ToString());
				return false;
			}
		}

		public static bool SaveAssignments(int entityID, WorkgroupModel[] workgroups, out bool partialSuccess, out List<string> errors, out string infoMessage)
		{
			partialSuccess = false;
			errors = null;
			infoMessage = null;

			List<WorkgroupDto> workgroupDtos = new List<WorkgroupDto>();
			foreach (var wg in workgroups)
			{
				var dto = LoadFromModel(wg);
                if (dto != null)
                {
                    dto.EntityID = entityID;
                    workgroupDtos.Add(dto);
                }
                else
                {
                    Logger.Log(Wfs.Logging.LogLevel.Warning, String.Format("Failed to load workgroup, \"{0}\", DTO from model.", wg.ShortName));
                }
            }

			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.SaveAssignments(workgroupDtos);

				if (response.Status != StatusCode.SUCCESS)
				{
					partialSuccess = response.Data;
					errors = response.Errors;
					return false;
				}
				else if (!R360Permissions.Current.Allowed(R360Permissions.Perm_WorkgroupVirtual, R360Permissions.ActionType.View))
					infoMessage = NoAccessAssociateInfo;
				
				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in SaveAssignments\r\n" + ex.ToString());
				return false;
			}
		}

		#region Helper Methods

		#region Enum Helpers
		private static object _oLock = new object();

		private static Dictionary<WFS.RecHub.HubConfig.ImageDisplayModes, string> _imageDisplayModeMapping = null;
		private static Dictionary<WFS.RecHub.HubConfig.ImageDisplayModes, string> ImageDisplayModeMapping
		{
			get
			{
				if (_imageDisplayModeMapping == null)
				{
					lock (_oLock)
					{
						if (_imageDisplayModeMapping == null)
						{
							_imageDisplayModeMapping = new Dictionary<ImageDisplayModes, string>();
							_imageDisplayModeMapping.Add(WFS.RecHub.HubConfig.ImageDisplayModes.UseInheritedSetting, "Use Entity Setting");
							_imageDisplayModeMapping.Add(WFS.RecHub.HubConfig.ImageDisplayModes.Both, "Front and Back");
							_imageDisplayModeMapping.Add(WFS.RecHub.HubConfig.ImageDisplayModes.FrontOnly, "Front only");
						}
					}
				}
				return _imageDisplayModeMapping;
			}
		}

		public static string[] ImageDisplayModes
		{
			get
			{
				return ImageDisplayModeMapping.Values.ToArray();
			}
		}

		private static string GetImageDisplayMode(WFS.RecHub.HubConfig.ImageDisplayModes imageDisplayMode)
		{
			string desc;
			if (ImageDisplayModeMapping.TryGetValue(imageDisplayMode, out desc))
				return desc;
			return imageDisplayMode.ToString();
		}

		private static WFS.RecHub.HubConfig.ImageDisplayModes GetImageDisplayMode(string imageDisplayModeDesc)
		{
			return ImageDisplayModeMapping.FirstOrDefault(o => o.Value == imageDisplayModeDesc).Key;
		}

		private static Dictionary<WFS.RecHub.HubConfig.BalancingOptions, string> _balancingOptionMapping = null;
		private static Dictionary<WFS.RecHub.HubConfig.BalancingOptions, string> BalancingOptionMapping
		{
			get
			{
				if (_balancingOptionMapping == null)
				{
					lock (_oLock)
					{
						if (_balancingOptionMapping == null)
						{
							_balancingOptionMapping = new Dictionary<BalancingOptions, string>();
							_balancingOptionMapping.Add(WFS.RecHub.HubConfig.BalancingOptions.NotRequired, "Not Required");
							_balancingOptionMapping.Add(WFS.RecHub.HubConfig.BalancingOptions.Desired, "Desired");
							_balancingOptionMapping.Add(WFS.RecHub.HubConfig.BalancingOptions.Required, "Required");
						}
					}
				}
				return _balancingOptionMapping;
			}
		}

		public static string[] BalancingOptions
		{
			get
			{
				return BalancingOptionMapping.Values.ToArray();
			}
		}

		private static string GetBalancingOption(WFS.RecHub.HubConfig.BalancingOptions balancingOption)
		{
			string desc;
			if (BalancingOptionMapping.TryGetValue(balancingOption, out desc))
				return desc;
			return balancingOption.ToString();
		}

		private static WFS.RecHub.HubConfig.BalancingOptions GetBalancingOption(string balancingOptionDesc)
		{
			return BalancingOptionMapping.FirstOrDefault(o => o.Value == balancingOptionDesc).Key;
		}
		#endregion


		public static WorkgroupModel LoadFromDto(WorkgroupDto workgroupDto)
		{
			return new WorkgroupModel()
			{
				ClientAccountKey = workgroupDto.ClientAccountKey,
				SiteBankID = workgroupDto.SiteBankID,
				SiteClientAccountID = workgroupDto.SiteClientAccountID,
				MostRecent = workgroupDto.MostRecent,
				IsActive = workgroupDto.IsActive == 1,
				DataRetentionDays = workgroupDto.DataRetentionDays,
				ImageRetentionDays = workgroupDto.ImageRetentionDays,
				ShortName = workgroupDto.ShortName,
				LongName = workgroupDto.LongName,
				FileGroup = workgroupDto.FileGroup,

				EntityID = workgroupDto.EntityID,
				ViewingDays = workgroupDto.ViewingDays,
				MaximumSearchDays = workgroupDto.MaximumSearchDays,
				CheckImageDisplayMode = GetImageDisplayMode(workgroupDto.CheckImageDisplayMode),
				DocumentImageDisplayMode = GetImageDisplayMode(workgroupDto.DocumentImageDisplayMode),
				HOA = workgroupDto.HOA,
				DisplayBatchID = workgroupDto.DisplayBatchID,
				InvoiceBalancing = GetBalancingOption(workgroupDto.InvoiceBalancing),

				DDAs = workgroupDto.DDAs == null ? null : workgroupDto.DDAs.ConvertAll(o => DDAModel.LoadFromDto(o)).ToArray(),
				DataEntryColumns = workgroupDto.DataEntryColumns == null ? null : workgroupDto.DataEntryColumns.ConvertAll(o => DataEntryColumnModel.LoadFromDto(o)).ToArray(),
				DataEntrySetup = workgroupDto.DataEntrySetup == null ? null : DataEntrySetupModel.LoadFromDto(workgroupDto.DataEntrySetup),

				BillingAccount = workgroupDto.BillingAccount,
				BillingField1 = workgroupDto.BillingField1,
				BillingField2 = workgroupDto.BillingField2,
                BusinessRules = workgroupDto.BusinessRules,
			};
		}

		public static WorkgroupDto LoadFromModel(WorkgroupModel workgroupModel)
		{
			return new WorkgroupDto()
			{
				ClientAccountKey = workgroupModel.ClientAccountKey,
				SiteBankID = workgroupModel.SiteBankID,
				SiteClientAccountID = workgroupModel.SiteClientAccountID,
				MostRecent = workgroupModel.MostRecent,
				IsActive = (short)(workgroupModel.IsActive ? 1 : 0),
				DataRetentionDays = workgroupModel.DataRetentionDays,
				ImageRetentionDays = workgroupModel.ImageRetentionDays,
				ShortName = workgroupModel.ShortName,
				LongName = workgroupModel.LongName,
				FileGroup = workgroupModel.FileGroup,

				EntityID = workgroupModel.EntityID,
                EntityName = workgroupModel.EntityName,
				ViewingDays = workgroupModel.ViewingDays,
				MaximumSearchDays = workgroupModel.MaximumSearchDays,
				CheckImageDisplayMode = GetImageDisplayMode(workgroupModel.CheckImageDisplayMode),
				DocumentImageDisplayMode = GetImageDisplayMode(workgroupModel.DocumentImageDisplayMode),
				HOA = workgroupModel.HOA,
				DisplayBatchID = workgroupModel.DisplayBatchID,
				InvoiceBalancing = GetBalancingOption(workgroupModel.InvoiceBalancing),

				DDAs = workgroupModel.DDAs == null ? null : workgroupModel.DDAs.ToList().ConvertAll(o => DDAModel.LoadFromModel(o)),
				DataEntryColumns = workgroupModel.DataEntryColumns == null ? null : workgroupModel.DataEntryColumns.ToList().ConvertAll(o => DataEntryColumnModel.LoadFromModel(o)),
				DataEntrySetup = workgroupModel.DataEntrySetup == null ? null : DataEntrySetupModel.LoadFromModel(workgroupModel.DataEntrySetup),

				BillingAccount = workgroupModel.BillingAccount,
				BillingField1 = workgroupModel.BillingField1,
				BillingField2 = workgroupModel.BillingField2,

                BusinessRules = workgroupModel.BusinessRules,
			};
		}
		#endregion
	}
}