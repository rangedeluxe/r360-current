﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.R360Shared;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class BankModel
	{
		public int BankKey { get; set; }
		public long SiteBankID { get; set; }
		public bool MostRecent { get; set; }
		public string BankName { get; set; }
		public int FIEntityID { get; set; }

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(BankModel));
				return _logger;
			}
		}

		public static bool GetBanksForFI(int fiEntityID, out BankModel[] banks, out List<string> errors)
		{
			errors = null;
			banks = null;
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetBanksForFI(fiEntityID);
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				banks = response.Data.Select(o => LoadFromDto(o)).ToArray();

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetBanksForFI\r\n" + ex.ToString());
				return false;
			}
		}

		public static bool GetBanks(out BankModel[] banks, out List<string> errors)
		{
			errors = null;
			banks = null;
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetBanks();
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				banks = response.Data.Select(o => LoadFromDto(o)).ToArray();

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetBanks\r\n" + ex.ToString());
				return false;
			}
		}

		public static bool GetBank(int bankKey, out BankModel bank, out List<string> errors)
		{
			errors = null;
			bank = null;

			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetBank(bankKey);
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				bank = LoadFromDto(response.Data);

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetBank\r\n" + ex.ToString());
				return false;
			}
		}

		public bool Save(out List<string> errors)
		{
			errors = null;

			BankDto bank = LoadFromModel(this);

			try
			{
				var manager = new HubConfigServicesManager();
				if (bank.BankKey == 0)
				{
					var response = manager.AddBank(bank);
					if (response.Status != StatusCode.SUCCESS)
					{
						errors = response.Errors;
						return false;
					}

					bank.BankKey = response.Data;
				}
				else
				{
					var response = manager.UpdateBank(bank);
					if (response.Status != StatusCode.SUCCESS)
					{
						errors = response.Errors;
						return false;
					}
				}

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in Save\r\n" + ex.ToString());
				return false;
			}
		}

		#region Helper Methods

		public static BankModel LoadFromDto(BankDto bankDto)
		{
			return new BankModel()
			{
				BankKey = bankDto.BankKey,
				SiteBankID = bankDto.SiteBankID,
				MostRecent = bankDto.MostRecent,
				BankName = bankDto.BankName,
				FIEntityID = bankDto.FIEntityID,
			};
		}

		public static BankDto LoadFromModel(BankModel bankModel)
		{
			return new BankDto()
			{
				BankKey = bankModel.BankKey,
				SiteBankID = bankModel.SiteBankID,
				MostRecent = bankModel.MostRecent,
				BankName = bankModel.BankName,
				FIEntityID = bankModel.FIEntityID,
			};
		}

		#endregion
	}
}