﻿using System;
using System.Collections.Generic;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
    public class PaymentSourceModel
    {
        public PaymentSourceDto PaymentSource { get; set; }
        public IEnumerable<SystemTypeModel> SystemTypes { get; set; }
        public IEnumerable<FIEntityModel> FIEntities { get; set; }
    }
}