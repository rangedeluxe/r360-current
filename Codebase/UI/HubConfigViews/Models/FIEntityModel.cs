﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Wfs.Logging;
using Wfs.Raam.Core;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using Wfs.Raam.Service.Authorization.Contracts.ServiceContracts;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Models
{
    public class FIEntityModel
    {
		private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(FIEntityModel));

        public int ID { get; set; }
        public string Name { get; set; }

		public static IEnumerable<FIEntityModel> GetFIEntities()
		{
			List<FIEntityModel> entities = null;
			try
			{
				var client = new RaamClient(new ConfigHelpers.Logger(
												o => Logger.Log(LogLevel.Informational, o),
												o => Logger.Log(LogLevel.Warning, o)));

				var raamEntities = client.GetFIEntities();

				// Convert to local format
				if (raamEntities != null)
				{
					entities = raamEntities.Select(o => new FIEntityModel() { ID = o.ID, Name = o.Name }).ToList();
					entities.Sort((o1, o2) => o1.Name.CompareTo(o2.Name));
				}
				else
					entities = new List<FIEntityModel>();

				Logger.Log(LogLevel.Debug, "FI List contains {0} entries", (object)entities.Count);
			}
			catch (Exception ex)
			{
				Logger.Log(LogLevel.Error, ex.ToString());

				// Return blank list on error...
				entities = new List<FIEntityModel>();
			}

			return entities;
		}

		public static int GetAncestorFIEntityID(int childID)
		{
			var client = new RaamClient(new ConfigHelpers.Logger(
											o => Logger.Log(LogLevel.Informational, o),
											o => Logger.Log(LogLevel.Warning, o)));

			var fiEntity = client.GetAncestorFIEntity(childID);

			return fiEntity == null ? 0 : fiEntity.ID;
		}
    }
}