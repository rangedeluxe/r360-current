﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class CheckDigitRoutineModel
	{
		public long Key;
		public int Offset { get; set; }
		public string Method { get; set; }
		public int Modulus { get; set; }
		public int Compliment { get; set; }
		public string WeightsDirection { get; set; }
		public string Weights { get; set; }
		public bool IgnoreSpaces { get; set; }
		public int Rem10Replacement { get; set; }
		public int Rem11Replacement { get; set; }
		public string FailureMessage { get; set; }
		public ReplacementValueModel[] ReplacementValues { get; set; }

		#region Check Digit Method Helpers
		private static object _oLock = new object();

		private static Dictionary<CheckDigitMethod, string> _checkDigitMethodMapping = null;
		private static Dictionary<CheckDigitMethod, string> CheckDigitMethodMapping
		{
			get
			{
				if (_checkDigitMethodMapping == null)
				{
					lock (_oLock)
					{
						if (_checkDigitMethodMapping == null)
						{
							_checkDigitMethodMapping = new Dictionary<CheckDigitMethod, string>();
							_checkDigitMethodMapping.Add(CheckDigitMethod.SumOfDigits, "Sum Of Digits");
							_checkDigitMethodMapping.Add(CheckDigitMethod.OnesDigit, "Ones Digit");
							_checkDigitMethodMapping.Add(CheckDigitMethod.ProductAddition, "Product Addition");
						}
					}
				}
				return _checkDigitMethodMapping;
			}
		}

		public static string[] CheckDigitMethods
		{
			get
			{
				return CheckDigitMethodMapping.Values.ToArray();
			}
		}

		private static string GetCheckDigitMethod(CheckDigitMethod checkDigitMethod)
		{
			string desc;
			if (CheckDigitMethodMapping.TryGetValue(checkDigitMethod, out desc))
				return desc;
			return checkDigitMethod.ToString();
		}

		private static CheckDigitMethod GetCheckDigitMethod(string checkDigitMethodDesc)
		{
			return CheckDigitMethodMapping.FirstOrDefault(o => o.Value == checkDigitMethodDesc).Key;
		}

		#endregion

		#region Weight Pattern Direction Helpers

		private static Dictionary<WeightPatternDirection, string> _weightPatternDirectionMapping = null;
		private static Dictionary<WeightPatternDirection, string> WeightPatternDirectionMapping
		{
			get
			{
				if (_weightPatternDirectionMapping == null)
				{
					lock (_oLock)
					{
						if (_weightPatternDirectionMapping == null)
						{
							_weightPatternDirectionMapping = new Dictionary<WeightPatternDirection, string>();
							_weightPatternDirectionMapping.Add(WeightPatternDirection.LeftToRight, "Left to Right");
							_weightPatternDirectionMapping.Add(WeightPatternDirection.RightToLeft, "Right to Left");
						}
					}
				}
				return _weightPatternDirectionMapping;
			}
		}

		public static string[] WeightPatternDirections
		{
			get
			{
				return WeightPatternDirectionMapping.Values.ToArray();
			}
		}

		private static string GetWeightPatternDirection(WeightPatternDirection weightPatternDirection)
		{
			string desc;
			if (WeightPatternDirectionMapping.TryGetValue(weightPatternDirection, out desc))
				return desc;
			return weightPatternDirection.ToString();
		}

		private static WeightPatternDirection GetWeightPatternDirection(string weightPatternDirectionDesc)
		{
			return WeightPatternDirectionMapping.FirstOrDefault(o => o.Value == weightPatternDirectionDesc).Key;
		}

		#endregion

		public static CheckDigitRoutineModel LoadFromDto(CheckDigitRoutineDto cdrDto)
		{
			return new CheckDigitRoutineModel()
			{
				Key = cdrDto.Key,
				Offset = cdrDto.Offset,
				Method = GetCheckDigitMethod(cdrDto.Method),
				Modulus = cdrDto.Modulus,
				Compliment = cdrDto.Compliment,
				WeightsDirection = GetWeightPatternDirection(cdrDto.WeightsDirection),
				Weights = cdrDto.Weights != null ? string.Join(",", cdrDto.Weights.Select(o => o.ToString())) : null,
				IgnoreSpaces = cdrDto.IgnoreSpaces,
				Rem10Replacement = cdrDto.Rem10Replacement,
				Rem11Replacement = cdrDto.Rem11Replacement,
				FailureMessage = cdrDto.FailureMessage,
				ReplacementValues = cdrDto.ReplacementValues == null ? null : cdrDto.ReplacementValues.Select(o => ReplacementValueModel.LoadFromDto(o)).ToArray()
			};
		}

		public static CheckDigitRoutineDto LoadFromModel(CheckDigitRoutineModel cdrModel)
		{
			return new CheckDigitRoutineDto()
			{
				Key = cdrModel.Key,
				Offset = cdrModel.Offset,
				Method = GetCheckDigitMethod(cdrModel.Method),
				Modulus = cdrModel.Modulus,
				Compliment = cdrModel.Compliment,
				WeightsDirection = GetWeightPatternDirection(cdrModel.WeightsDirection),
				Weights = cdrModel.Weights == null ? null : cdrModel.Weights.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(o => int.Parse(o)).ToArray(),
				IgnoreSpaces = cdrModel.IgnoreSpaces,
				Rem10Replacement = cdrModel.Rem10Replacement,
				Rem11Replacement = cdrModel.Rem11Replacement,
				FailureMessage = cdrModel.FailureMessage,
				ReplacementValues = cdrModel.ReplacementValues == null ? null : cdrModel.ReplacementValues.ToDictionary(k => (k.Original == '\0' ? ' ' : k.Original), v => v.Replacement)
			};
		}
	}
}