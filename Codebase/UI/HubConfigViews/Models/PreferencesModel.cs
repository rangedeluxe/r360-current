﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Models
{
    [Serializable]
    public class PreferencesModel
    {
        #region Member Variables

        private static Wfs.Logging.ILogSource _logger = null;
        public static Wfs.Logging.ILogSource Logger
        {
            get
            {
                if (_logger == null)
                    _logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(PreferencesModel));
                return _logger;
            }
        }

        #endregion

        #region Properties

       
        public Guid ID { get; set; }
       
        public string Name { get; set; }
        
        public string Description { get; set; }
        
        public bool IsOnline { get; set; }
        
        public bool IsSystem { get; set; }
        
        public string DefaultSetting { get; set; }
        
        public byte AppType { get; set; }
        
        public string Group { get; set; }
        
        public int FieldDataTypeEnum { get; set; }
        
        public int FieldSize { get; set; }
        
        public int FieldMaxLength { get; set; }
        
        public string FieldRegEx { get; set; }
        
        public int FieldMinValue { get; set; }
        
        public int FieldMaxValue { get; set; }
        
        public bool EncodeOnSerialization { get; set; }

        #endregion

        #region Methods

        #region Public

        public static bool GetPreferences(out List<PreferencesModel> preferences, out List<string> errors)
        {
            errors = null;
            preferences = null;
            try
            {
                var manager = new HubConfigServicesManager();
                var response = manager.GetPreferences();

                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                preferences = response.Data.Select(o => LoadFromDto(o)).ToList<PreferencesModel>();

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetPreferences\r\n" + ex.ToString());
                return false;
            }
        }

        #region Helper Methods

        public static PreferencesModel LoadFromDto(PreferenceDto preferenceDto)
        {
            return new PreferencesModel()
            {
                AppType = preferenceDto.AppType,
                DefaultSetting = preferenceDto.DefaultSetting,
                Description = preferenceDto.Description,
                EncodeOnSerialization = preferenceDto.EncodeOnSerialization,
                FieldDataTypeEnum = preferenceDto.FieldDataTypeEnum,
                FieldMaxLength = preferenceDto.FieldMaxLength,
                FieldMaxValue = preferenceDto.FieldMaxValue,
                FieldMinValue = preferenceDto.FieldMinValue,
                FieldRegEx = preferenceDto.FieldRegEx,
                FieldSize = preferenceDto.FieldSize,
                Group = preferenceDto.Group,
                ID = preferenceDto.ID,
                IsOnline = preferenceDto.IsOnline,
                IsSystem = preferenceDto.IsSystem,
                Name = preferenceDto.Name
            };
        }

        public static PreferenceDto LoadFromModel(PreferencesModel preferencesModel)
        {
            return new PreferenceDto()
            {
                AppType = preferencesModel.AppType,
                DefaultSetting = preferencesModel.DefaultSetting,
                Description = preferencesModel.Description,
                EncodeOnSerialization = preferencesModel.EncodeOnSerialization,
                FieldDataTypeEnum = preferencesModel.FieldDataTypeEnum,
                FieldMaxLength = preferencesModel.FieldMaxLength,
                FieldMaxValue = preferencesModel.FieldMaxValue,
                FieldMinValue = preferencesModel.FieldMinValue,
                FieldRegEx = preferencesModel.FieldRegEx,
                FieldSize = preferencesModel.FieldSize,
                Group = preferencesModel.Group,
                ID = preferencesModel.ID,
                IsOnline = preferencesModel.IsOnline,
                IsSystem = preferencesModel.IsSystem,
                Name = preferencesModel.Name
            };
        }

        #endregion

        #endregion

        #endregion

    }
}