﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Models
{
	public class DataEntryTemplateModel
	{
		public string ShortName { get; set; }
		public string Description { get; set; }
		public List<DataEntryColumnModel> DataEntryColumns { get; set; }

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(DataEntryTemplateModel));
				return _logger;
			}
		}

		public static bool GetDataEntryTemplates(out DataEntryTemplateModel[] templates, out List<string> errors)
		{
			errors = null;
			templates = null;
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetDataEntryTemplates();
				if (response.Status != StatusCode.SUCCESS)
				{
					errors = response.Errors;
					return false;
				}

				templates = response.Data.Select(o => LoadFromDto(o)).ToArray();

				return true;
			}
			catch (Exception ex)
			{
				Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetDataEntryTemplates\r\n" + ex.ToString());
				return false;
			}
		}


		#region Helper Methods

		public static DataEntryTemplateModel LoadFromDto(DataEntryTemplateDto deTemplateDto)
		{
			return new DataEntryTemplateModel()
			{
				ShortName = deTemplateDto.ShortName,
				Description = deTemplateDto.Description,
				DataEntryColumns = deTemplateDto.DEColumns == null ? null : deTemplateDto.DEColumns.Select(o => DataEntryColumnModel.LoadFromDto(o)).ToList()
			};
		}

		#endregion
	}
}
