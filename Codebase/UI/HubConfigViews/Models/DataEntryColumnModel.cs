﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
	public class DataEntryColumnModel
	{
		public int DataEntryColumnKey { get; set; }
		public string TableType { get; set; }
		public string DataType { get; set; }
		public short FieldLength { get; set; }
		public short ScreenOrder { get; set; }
		public string FieldName { get; set; }
		public string DisplayName { get; set; }
		public bool HubCreated { get; set; }
		public bool MarkSense { get; set; }
        public bool IsActive { get; set; }
        public bool IsRequired { get; set; }
        public string UILabel { get; set; }
        public string PaymentSource { get; set; }
        public bool IsCheck { get; set; }
        public int BatchSourceKey { get; set; }

        public string BatchSourceShortName { get; set; }

        #region Table Type/Name Helpers

        private static string[] _tableTypes = new string[] { "Payment", "Invoice" };
		public static string[] TableTypes { get { return _tableTypes; } }

		internal static string GetTableTypeFromEnum(ETableType type)
		{
			switch (type) 
            {
				case ETableType.Payments:
					return "Payment";
				default:
					return "Invoice";
			}
		}

		internal static ETableType GetTableTypeFromSetup(string tableType, string fieldName)
		{
			//Special cases:
			// For payments/checks, everything will be a payment data entry
			// For stubs, fields named amount or account number are Stubs, all else is StubsDataEntry
			if (tableType == "Payment")
	            return ETableType.Payments;			
			else 
                return ETableType.Invoice;

			
		}

		private static string GetTableName(ETableType type)
		{
			switch (type)
			{
				case ETableType.Payments:
					return "Checks";
				case ETableType.Invoice:
					return "Stubs";
				default:
					return type.ToString();
			}
		}

		#endregion

		#region Data Type Helpers

		private static object _oLock = new object();

		private static Dictionary<EDataType, string> _dataTypeMapping = null;
		private static Dictionary<EDataType, string> DataTypeMapping
		{
			get
			{
				if (_dataTypeMapping == null)
				{
					lock (_oLock)
					{
						if (_dataTypeMapping == null)
						{
							_dataTypeMapping = new Dictionary<EDataType, string>();
							EDataType[] dataTypes = (EDataType[])Enum.GetValues(typeof(EDataType));
							foreach (EDataType type in dataTypes)
							{
								//add additional translations here
								string description = type == EDataType.FloatingPoint ? "Floating Point" : type.ToString();
								_dataTypeMapping.Add(type, description);
							}
						}
					}
				}
				return _dataTypeMapping;
			}
		}

		public static string[] DataTypes
		{
			get
			{
				return DataTypeMapping.Values.ToArray();
			}
		}

		private static string GetDataTypeFromEnum(EDataType dataType)
		{
			string desc;
			if (DataTypeMapping.TryGetValue(dataType, out desc))
				return desc;
			return dataType.ToString();
		}

		private static EDataType GetDataType(string dataTypeDesc)
		{
			return DataTypeMapping.FirstOrDefault(o => o.Value == dataTypeDesc).Key;
		}

		#endregion

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(DataEntryColumnModel));
				return _logger;
			}
		}

		#region Helper Methods

		public static DataEntryColumnModel LoadFromDto(DataEntryColumnDto deColumnDto)
		{
			return new DataEntryColumnModel()
			{
				DataEntryColumnKey = deColumnDto.DataEntryColumnKey,
				TableType = DataEntryColumnModel.GetTableTypeFromEnum(deColumnDto.TableType),
				DataType = DataEntryColumnModel.GetDataTypeFromEnum(deColumnDto.DataType),
				FieldLength = deColumnDto.FieldLength,
				ScreenOrder = deColumnDto.ScreenOrder,
				FieldName = deColumnDto.FieldName,
				DisplayName = deColumnDto.DisplayName,
				HubCreated = deColumnDto.HubCreated,
				MarkSense = deColumnDto.MarkSense,
                IsActive = deColumnDto.IsActive,
                IsRequired = deColumnDto.IsRequired,
                UILabel = deColumnDto.UILabel,
                PaymentSource = deColumnDto.PaymentSource,
                IsCheck = deColumnDto.isCheck,
                BatchSourceKey = deColumnDto.BatchSourceKey,
                BatchSourceShortName = deColumnDto.BatchSourceShortName
			};
		}

		public static DataEntryColumnDto LoadFromModel(DataEntryColumnModel deColumnModel)
		{
			ETableType tableType = DataEntryColumnModel.GetTableTypeFromSetup(deColumnModel.TableType, deColumnModel.FieldName);
			string tableName = DataEntryColumnModel.GetTableName(tableType);
			EDataType dataType = DataEntryColumnModel.GetDataType(deColumnModel.DataType);

			return new DataEntryColumnDto()
			{
				TableType = tableType,
				DataType = dataType,
				FieldLength = deColumnModel.FieldLength,
				ScreenOrder = deColumnModel.ScreenOrder,
				TableName = tableName,
				FieldName = deColumnModel.FieldName,
				DisplayName = deColumnModel.DisplayName,
				MarkSense = deColumnModel.MarkSense,
                IsActive = deColumnModel.IsActive,
                IsRequired = deColumnModel.IsRequired,
                UILabel = deColumnModel.UILabel,
                PaymentSource = deColumnModel.PaymentSource,
                isCheck = (int)tableType == 1,   //tableType has been limited to 0 or 1
                BatchSourceKey = deColumnModel.BatchSourceKey,
                DataEntryColumnKey = deColumnModel.DataEntryColumnKey,
                BatchSourceShortName = deColumnModel.BatchSourceShortName
			};
		}
		#endregion
	}
}
