﻿using CommonUtils.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubConfigViews.Models
{
	public class ResponseModel<T>
	{
		public T Data = default(T);
		public bool Success = true;
		public List<WebErrors> Errors = null;

		public ResponseModel(T data, bool success, List<string> errors)
		{
			Data = data;
			Success = success;
			Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null;
		}
	}
}