﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Models
{
	public class StaticDescriptionModel
	{
		public int ID { get; set; }
		public string Description { get; set; }

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(StaticDescriptionModel));
				return _logger;
			}
		}

		#region Static Data Helpers
		private static object _oLock = new object();

		private static StaticDescriptionModel[] _validationTableTypes = null;
		public static bool GetValidationTableTypes(out StaticDescriptionModel[] types, out List<string> errors)
		{
			types = null;
			errors = null;
			if (_validationTableTypes == null)
			{
				lock (_oLock)
				{
					if (_validationTableTypes == null)
					{
						var manager = new HubConfigServicesManager();
						var response = manager.GetValidationTableTypes();
						if (response.Status != StatusCode.SUCCESS)
						{
							errors = response.Errors;
							return false;
						}

						_validationTableTypes = response.Data.Select(o => LoadFromDto(o)).ToArray();
					}
				}
			}
			types = _validationTableTypes;
			return true;
		}

		public static bool GetBatchSources(int fiEntityID, out PaymentSourceDto[] sources, out List<string> errors)
		{
			sources = null;
			errors = null;

			var manager = new HubConfigServicesManager();
            var response = manager.GetPaymentSources();
			if (response.Status != StatusCode.SUCCESS)
			{
				errors = response.Errors;
				return false;
			}

            sources = response.Data
                .Where(x => x.FIEntityID == fiEntityID)
                .ToArray();
			return true;
		}

		private static StaticDescriptionModel[] _batchPaymentTypes = null;
		public static bool GetBatchPaymentTypes(out StaticDescriptionModel[] batchPaymentTypes, out List<string> errors)
		{
			batchPaymentTypes = null;
			errors = null;
			if (_batchPaymentTypes == null)
			{
				lock (_oLock)
				{
					if (_batchPaymentTypes == null)
					{
						var manager = new HubConfigServicesManager();
						var response = manager.GetBatchPaymentTypes();
						if (response.Status != StatusCode.SUCCESS)
						{
							errors = response.Errors;
							return false;
						}

						_batchPaymentTypes = response.Data.Select(o => LoadFromDto(o)).ToArray();
					}
				}
			}
			batchPaymentTypes = _batchPaymentTypes;
			return true;
		}

		#endregion

		#region Helper Methods

		public static StaticDescriptionModel LoadFromDto(Tuple<int, string> idDescDto)
		{
			return new StaticDescriptionModel()
			{
				ID = idDescDto.Item1,
				Description = idDescDto.Item2
			};
		}
		#endregion
	}
}
