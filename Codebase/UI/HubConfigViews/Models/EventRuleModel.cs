﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Wfs.Logging;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;
using  WFS.RecHub.HubConfig.DTO;

namespace HubConfigViews.Models
{
    public class EventRuleModel
    {
        #region Member Variables

        private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(EventRuleModel));

        #endregion

        #region Properties

        public long ID { get; set; }
        public long EventRuleID { get; set; }
        public string Description { get; set; }
        public string EventName { get; set; }
        public string EventLongName { get; set; }
        public int SiteCodeID { get; set; }
        public int SiteBankID { get; set; }
        public int SiteClientAccountID { get; set; }
        public bool IsActive { get; set; }
        public string Table { get; set; }
        public string Column { get; set; }
        public int EventID { get; set; }
        public string Operator { get; set; }
        public string EventValue { get; set; }
        public List<string> AssignedSIDs { get; set; }
        public int? EntityID { get; set; }
        public string ParentHierarchy { get; set; }
        public string WorkgroupName { get; set; }
        public IEnumerable<EventRuleUser> AssociatedUsers { get; set; }
        #endregion 

        #region Methods

        public static bool GetEventRules(out List<EventRuleModel> eventRules, out List<string> errors)
        {
            errors = null;
            eventRules = null;
            try
            {
                var manager = new HubConfigServicesManager();
                var response = manager.GetEventRules();

                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                eventRules = response.Data.Select(o => LoadFromDto(o)).ToList<EventRuleModel>();
                GetHierarchyListings(eventRules);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetEventRules\r\n" + ex.ToString());
                return false;
            }
        }

        public static bool GetEventRule(int eventRuleID, out EventRuleModel eventRule, out List<string> errors)
        {
            errors = null;
            eventRule = null;

            try
            {
                var manager = new HubConfigServicesManager();
                var response = manager.GetEventRule(eventRuleID);

                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                eventRule = LoadFromDto(response.Data);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetEventRules\r\n" + ex.ToString());
                return false;
            }
        }

        #region Helper Methods

        public static EventRuleModel LoadFromDto(EventRuleDto eventRuleDto)
        {
            
            return new EventRuleModel()
            {
                ID = eventRuleDto.ID,
                EventRuleID = eventRuleDto.EventRuleID,
                Description = eventRuleDto.Description,
                EventName = eventRuleDto.EventName,
                EventLongName = eventRuleDto.EventLongName,
                SiteCodeID = eventRuleDto.SiteCodeID,
                SiteBankID = eventRuleDto.SiteBankID,
                SiteClientAccountID = eventRuleDto.SiteClientAccountID,
                IsActive = eventRuleDto.IsActive,
                Table = eventRuleDto.Table,
                Column = eventRuleDto.Column,
                EventValue = eventRuleDto.EventValue,
                EventID = eventRuleDto.EventID,
                Operator = eventRuleDto.Operator,
                AssignedSIDs = eventRuleDto.UserRA3MSIDs,
                EntityID = eventRuleDto.EntityID,
                WorkgroupName = eventRuleDto.WorkgroupName,
                AssociatedUsers = eventRuleDto.AssociatedUsers
            };
        }

      

        public static EventRuleDto LoadFromModel(EventRuleModel eventRuleModel)
        {
            return new EventRuleDto()
            {
                ID = eventRuleModel.ID,
                EventRuleID = eventRuleModel.EventRuleID,
                Description = eventRuleModel.Description,
                EventName = eventRuleModel.EventName,
                SiteCodeID = eventRuleModel.SiteCodeID,
                SiteBankID = eventRuleModel.SiteBankID,
                SiteClientAccountID = eventRuleModel.SiteClientAccountID,
                IsActive = eventRuleModel.IsActive,
                EventID = eventRuleModel.EventID,
                Table = eventRuleModel.Table,
                Column = eventRuleModel.Column,
                Operator = eventRuleModel.Operator,
                EventValue = eventRuleModel.EventValue,
                UserRA3MSIDs = eventRuleModel.AssignedSIDs
            };
        }

        #endregion

        #region Private

        private static void GetHierarchyListings(List<EventRuleModel> eventRules)
        {
            Dictionary<int, string> hierarchyList = new Dictionary<int, string>();

            foreach (EventRuleModel eventRule in eventRules)
            {
                if (!eventRule.EntityID.HasValue)
                {
                    eventRule.ParentHierarchy = String.Empty;
                }
                else if (hierarchyList.ContainsKey(eventRule.EntityID.Value))
                {
					eventRule.ParentHierarchy = hierarchyList[eventRule.EntityID.Value];
                }
                else
                {
					eventRule.ParentHierarchy = EventRuleModel.GetParentHierarchyForEntity(eventRule.EntityID.Value);
                    hierarchyList.Add(eventRule.EntityID.Value, eventRule.ParentHierarchy);
                }
            }
        }


        private static string GetParentHierarchyForEntity(int entityID)
        {
            var client = new RaamClient(new ConfigHelpers.Logger(
                                                o => Logger.Log(LogLevel.Informational, o),
                                                o => Logger.Log(LogLevel.Warning, o)));

            return client.GetEntityBreadcrumb(entityID);

        }

        private static WorkgroupModel GetWorkgroupForKey(int clientAccountKey)
        {
            WorkgroupModel workgroup;
            List<string> errors;

            WorkgroupModel.GetWorkgroup(clientAccountKey, out workgroup, out errors);

            return workgroup;
        }

        #endregion

        #endregion
    }
}