﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubConfig;

namespace HubConfigViews.Models
{
    public class NotificationType
    {
        public string FileType { get; set; }
        public string FileTypeDescription { get; set; }
        public int NotificationFileTypeKey { get; set; }
        public bool IsEdit { get; set; }
        public NotificationType()
        {
        }

        public NotificationTypeDto ToNotificationTypeDto()
        {
            return new NotificationTypeDto
            {
                FileType = this.FileType,
                FileTypeDescription = this.FileTypeDescription,
                NotificationFileTypeKey = this.NotificationFileTypeKey
            };
        }
    }
}