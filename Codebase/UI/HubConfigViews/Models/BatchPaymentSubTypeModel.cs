﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Shared;

namespace HubConfigViews.Models
{
	public class BatchPaymentSubTypeModel
	{
		public int BatchPaymentSubTypeKey { get; set; }
		public int BatchPaymentTypeKey { get; set; }
		public string SubTypeDescription { get; set; }

		private static Wfs.Logging.ILogSource _logger = null;
		public static Wfs.Logging.ILogSource Logger
		{
			get
			{
				if (_logger == null)
					_logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(BatchPaymentSubTypeModel));
				return _logger;
			}
		}

		#region Static Data Helpers
		private static object _oLock = new object();

		private static BatchPaymentSubTypeModel[] _batchPaymentSubTypes = null;
		public static bool GetBatchPaymentSubTypes(out BatchPaymentSubTypeModel[] subTypes, out List<string> errors)
		{
			subTypes = null;
			errors = null;
			if (_batchPaymentSubTypes == null)
			{
				lock (_oLock)
				{
					if (_batchPaymentSubTypes == null)
					{
						var manager = new HubConfigServicesManager();
						var response = manager.GetBatchPaymentSubTypes();
						if (response.Status != StatusCode.SUCCESS)
						{
							errors = response.Errors;
							return false;
						}

						_batchPaymentSubTypes = response.Data.Select(o => LoadFromDto(o)).ToArray();
					}
				}
			}
			subTypes = _batchPaymentSubTypes;
			return true;
		}


		#endregion

		#region Helper Methods

		public static BatchPaymentSubTypeModel LoadFromDto(BatchPaymentSubTypeDto subTypeDto)
		{
			return new BatchPaymentSubTypeModel()
			{
				BatchPaymentSubTypeKey = subTypeDto.BatchPaymentSubTypeKey,
				BatchPaymentTypeKey = subTypeDto.BatchPaymentTypeKey,
				SubTypeDescription = subTypeDto.SubTypeDescription
			};
		}
		#endregion
	}
}
