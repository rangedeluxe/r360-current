﻿define(["jquery", "ko", "frameworkUtils", "wfs.select", "wfs.treeselector"], function ($, ko, framework) {

    var _me = this;
    var _container = {};
    var _vm = {};
    var _namePattern = /^[A-Za-z0-9 &,\.:'()_-]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash
    var $getE = function (b) { alert("Not initialized!"); };

    var setSelectedEvent = function (value) {
        if (value) {
            _vm.selectedEvent(value);
            _vm.message(value.Message);
            _vm.table(value.Table);
            _vm.column(value.Column);

            _vm.operatorSelect.wfsSelectbox('setItems', value.Operators);
            if (value.Operators && value.Operators.length === 1) {
                _vm.operatorSelect.wfsSelectbox('setValue', value.Operators[0].ID);
            }

            setMainGroupVisible(true);

            setWorkgroupVisible(value.Name);

            setAppliesToGroupVisible(value.Name, value.Table, value.Column, value.Operators.length);
        }
        else
            setMainGroupVisible(false);
    };

    var setSelectedWorkgroup = function (value) {
        _vm.selectedWorkgroup(value);
    };

    var setSelectedOperator = function (value) {
        if (value)
            _vm.selectedOperator(value);
    };

    var userDataCallback = function (data) {
        _vm.userSelector.wfsTreeSelector('initialize');
        var title = null;
        if (_vm.defaultUsers.length > 1) {
            title = 'Selected: Multiple Users';
        } else if (_vm.defaultUsers.length == 1) {
            var item = _vm.userSelector.wfsTreeSelector('getItemById', _vm.defaultUsers[0]);
            if (item != null) {
                title = 'Selected: ' + item.label;
            }
        }
        _vm.userSelector.wfsTreeSelector('setTitle', title);
    };

    var selectedUser = function (selected, initial) {
        var title = null;
        // Set the title of our UserSelector
        if (selected.length > 1)
            title = 'Selected: Multiple Users';
        else if (selected.length == 1)
            title = 'Selected: ' + selected[0].label;
        _vm.userSelector.wfsTreeSelector('setTitle', title);        
    }

    //*****************************************************
    //*****************************************************
    //  View Model
    //*****************************************************
    var ViewModel = function (model) {
        var self = this;
        self.model = model.Data;

        self.eventRuleID = self.model.EventRule.EventRuleID;
        self.bankID = self.model.EventRule.SiteBankID;
        self.clientAccountID = self.model.EventRule.SiteClientAccountID;
        self.eventName = self.model.EventRule.EventName;

        self.events = self.model.Events;
        self.labels = self.model.Labels;
        self.workgroups = self.model.Workgroups;

        self.message = ko.observable(null);

        self.isChecked = ko.observable(self.model.EventRule.IsActive);
        self.description = ko.observable(self.model.EventRule.Description);
        self.table = ko.observable(null);
        self.column = ko.observable(null);
        self.eventValue = ko.observable(self.model.EventRule.EventValue);

        self.selectedEvent = ko.observable(null);
        self.selectedOperator = ko.observable(null);
        self.selectedUsers = ko.observableArray(self.model.AssignedUsers);
        self.selectedWorkgroup = null;
        self.defaultUsers = self.model.AssignedUsers;

        self.isOperatorGroupVisible = false;
        self.isLoading = ko.observable(false);
    }
    //*****************************************************

    //*****************************************************
    //*****************************************************
    //  Bindings
    //*****************************************************

    var initWorkgroupSelector = function() {
        if (_vm.workGroupSelector) {
            $getE('#treeBase').wfsTreeSelector('destroy');
        }
        _vm.workGroupSelector = $getE('#treeBase').wfsTreeSelector({
            useExpander: true,
            entityURL: '/RecHubRaamProxy/api/entity',
            callback: selectedTreeItem,
            dataCallback: dataCallback,
            expanderTitle: "Select Workgroup",
            entitiesOnly: false,
            closeOnSelectNonNode: false
        });
    };

    var setupElementBindings = function () {

        $getE('#cancelButton').bind('click', function () {
            framework.closeModal();
            return false;
        });

        $getE('#saveButton').bind('click', saveOnClickValidate);
        _vm.eventSelect = $('#eventSelect').wfsSelectbox({
            'items': _vm.events,
            'callback': setSelectedEvent,
            'displayField': 'EventLongName',
            'idField': 'ID',
            'placeHolder': 'Select an Event...',
            'width': 330
        });

        _vm.operatorSelect = $getE('#operatorsSelect').wfsSelectbox({
            'items': [],
            'callback': setSelectedOperator,
            'displayField': 'Name',
            'idField': 'ID',
            'placeHolder': 'Select an Operator...',
            'width': 330
        });

        _vm.userSelector = $getE('#userSelect').wfsTreeSelector({
            useExpander: true,
            entityURL: '/RecHubRaamProxy/api/entityUser/',
            checkedStatusChanged: selectedUser,
            dataCallback: userDataCallback,
            expanderTitle: "Select User",
            entitiesOnly: false,
            icon: 'fa fa-user',
            multiSelect: true,
            checkedItems: _vm.defaultUsers
        });

        setWorkgroupVisible(_vm.eventName);

    }
    //*****************************************************

    //*****************************************************
    //*****************************************************
    //  Validation
    //*****************************************************

    var setFormValidation = function (validateNumber) {

        $getE("#eventRuleForm").removeData("validator");

        var validator = $getE("#eventRuleForm").validate({
            ignore: ".ignore, .select2-input",
            rules: {
                hdnEvent: { required: true },
                description: { required: true, maxlength: 30 },
                hdnOperator: { required: function (element) { return _vm.isOperatorGroupVisible; } },
                eventValue: {
                    required: function (element) { return _vm.isOperatorGroupVisible; },
                    number: validateNumber
                }
            },
            messages: {
                hdnEvent: { required: _vm.labels.ErrorEventRequired },
                description: { required: _vm.labels.ErrorDescriptionRequired, maxlength: _vm.labels.ErrorDescriptionMaxLength },
                hdnOperator: { required: _vm.labels.ErrorEventOperatorRequired },
                eventValue: { required: _vm.labels.ErrorEventValueRequired }
            },
            submitHandler: function (form) {
                // Validation successful - submit the values
                if (_vm.isWorkgroupGroupVisible && (!_vm.selectedWorkgroup || _vm['selectedWorkgroup'].isNode == false)) {
                    $getE('#workgroupSelectError').css('display', 'inline-block');
                    _vm.isLoading(false);
                    return;
                }
                else
                    saveOnClick();
            },
            invalidHandler: function (event, validator) {
                // Submit was cancelled - errors should already be showing
                _vm.isLoading(false);
            }
        });
    }
    //*****************************************************

    //*****************************************************
    //*****************************************************
    //  Set Up Form
    //*****************************************************
    var setForm = function () {
        _vm.eventSelect.wfsSelectbox('setValue', _vm.model.EventRule.EventID);
        _vm.operatorSelect.wfsSelectbox('setValue', _vm.model.EventRule.Operator);
    }
    //*****************************************************

    var closeModal = function () {
        framework.closeModal();
    }

    var selectedSelectItem = function (selected) {
        _vm['selectedWorkgroup'] = selected;
    };

    var selectedTreeItem = function (selected, initial) {
        _vm['selectedWorkgroup'] = null;


        if (!_vm.isWorkgroupGroupVisible || !selected || initial)
            return;
        
        var title = null;
        if (selected.isNode) {
            title = 'Selected: ' + selected['label'];
            _vm['selectedWorkgroup'] = selected;
        }
        else {
            framework.errorToast(_container, "Please select a single workgroup.");
            title = "Select a Workgroup";
            return false;
        }
        _vm.workGroupSelector.wfsTreeSelector('setTitle', title);

    };

    var dataCallback = function (data) {
        var entities = _vm.workGroupSelector.wfsTreeSelector('getEntities');
        var workgroups = _vm.workGroupSelector.wfsTreeSelector('getWorkgroups');
        var ecount = entities.length;
        var wcount = workgroups.length;

        _vm.workGroupSelector.wfsTreeSelector('initialize');
        _vm.workGroupSelector.wfsTreeSelector('setSelection', _vm.bankID + '|' + _vm.clientAccountID);
    };

    var setMainGroupVisible = function (visible) {
        if (visible)
            $getE('#mainGroup').css('display', 'block');
        else
            $getE('#mainGroup').css('display', 'none');
    }

    var setWorkgroupVisible = function (eventName) {
        if (eventName.toLowerCase() == 'processingexception') {
            _vm.isWorkgroupGroupVisible = false;
            $getE('#workgroupSelect').wfsSelectbox('setValue', null);
            $getE('#workgroupGroup').css('display', 'none');
        }
        else if (!_vm.isWorkgroupGroupVisible) {
            _vm.isWorkgroupGroupVisible = true;
            $getE('#workgroupGroup').css('display', 'block');

            // Reset the WGS, as it can't initialize properly when hidden.
            initWorkgroupSelector();
        }
    }

    var setAppliesToGroupVisible = function (eventName, table, column, operators) {
        if (eventName.toLowerCase() != 'decisionpending' && table != '') {

            $getE('#appliesToGroup').css('display', 'block');

            if (column != '')
                setColumnGroupVisible(true);
            else
                setColumnGroupVisible(false);

            // set number validation or text validation.
            if (eventName.toLowerCase() == 'payerpaymentreceived') {
                setFormValidation(false);   //validate text
                $getE("#dollarIcon").text("");
            }
            else {
                setFormValidation(true);    //validate number
                $getE("#dollarIcon").text("$");
            }
        }
        else {
            setColumnGroupVisible(false);
            $getE('#appliesToGroup').css('display', 'none');
        }

        if (operators > 0)
            setOperatorsGroupVisible(true);
        else
            setOperatorsGroupVisible(false);
    }

    var setColumnGroupVisible = function (visible) {
        if (visible)
            $getE('#columnGroup').css('display', 'block');
        else
            $getE('#columnGroup').css('display', 'none');
    }

    var setOperatorsGroupVisible = function (visible) {
        if (visible) {
            $getE('#operatorsGroup').css('display', 'block');
            _vm.isOperatorGroupVisible = true;
        }
        else {
            $getE('#operatorsGroup').css('display', 'none');
            _vm.isOperatorGroupVisible = false;
        }
    }

    var saveOnClickValidate = function () {
        _vm.isLoading(true);
        $getE("#eventRuleForm").submit();
    };

    var saveOnClickCallback = function (serverResponse) {
        if (serverResponse.HasErrors) {
            _vm.isLoading(false);
            framework.errorToast(_container, serverResponse.Errors[0].Message);
        }
        else {
            closeModal();
        }
    };
    var saveOnClick = function () {
        var users = [];
        $(_vm.userSelector.wfsTreeSelector('getCheckedItems')).each(function (i, element) {
            users.push(element.id);
        });
        var url = "/RecHubConfigViews/Alerts/UpdateEventRule/";
        var data = {
            eventRuleID: _vm.eventRuleID,
            eventID: _vm.selectedEvent().ID,
            description: _vm.description(),
            isActive: _vm.isChecked() == null ? false : _vm.isChecked(),
            eventOperator: _vm.selectedOperator() == null ? '' : _vm.selectedOperator().ID,
            eventValue: _vm.eventValue(),
            siteBankID: _vm.isWorkgroupGroupVisible ? _vm.selectedWorkgroup.id.split('|')[0] : -1,
            siteClientAcctID: _vm.isWorkgroupGroupVisible ? _vm.selectedWorkgroup.id.split('|')[1] : -1,
            users: users
        };
        

        framework.doJSON(url, $.toDictionary(data), saveOnClickCallback);
    }

    //*****************************************************
    //*****************************************************
    //  Initialization
    //*****************************************************
    var init = function (myContainerId, model) {
        $(function () {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b) };
            _vm = new ViewModel(model);

            ko.applyBindings(_vm, $getE('#editEventRuleModal').get(0));

            setupElementBindings();

            setFormValidation(true);

            setForm();

        });
    };

    return {
        "init": init
    }
    //*****************************************************
});
