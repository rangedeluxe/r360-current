﻿define(["jquery", "ko", "frameworkUtils", "wfs.select", "wfs.treeselector"], function ($, ko, framework) {

    var _me = this;
    var _container = {};
    var _vm = {};
    var _namePattern = /^[A-Za-z0-9 &,\.:'()_-]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash
    var $getE = function (b) { alert("Not initialized!"); };

    var setSelectedEvent = function (value) {
        if (value) {
            _vm.selectedEvent(value);
            _vm.message(value.Message);
            _vm.table(value.Table);
            _vm.column(value.Column);

            _vm.operatorSelect.wfsSelectbox('setItems', value.Operators);
            if (value.Operators  && value.Operators.length === 1) {
                _vm.operatorSelect.wfsSelectbox('setValue', value.Operators[0].ID);
            }

            setMainGroupVisible(true);

            setWorkgroupVisible(value.Name);

            setAppliesToGroupVisible(value.Name, value.Table, value.Column, value.Operators.length);
        }
        else
            setMainGroupVisible(false);
    };

    var setSelectedOperator = function (value) {
        if (value)
            _vm.selectedOperator(value);
    };

    var userDataCallback = function (data) {
        _vm.userSelector.wfsTreeSelector('initialize');
    };

    var selectedUser = function (selected, initial) {
        var title = null;
        _vm.selectedUsers(null);
        var newusers = [];

        // Set the title of our UserSelector
        if (selected.length > 1)
            title = 'Selected: Multiple Users';
        else if (selected.length == 1)
            title = 'Selected: ' + selected[0].label;
        _vm.userSelector.wfsTreeSelector('setTitle', title);

        // Now we need to grab all selected users for our VM.
        $(selected).each(function (i, e) {
            newusers.push(e.id);
        });
        _vm.selectedUsers(newusers);

    }

    //*****************************************************
    //*****************************************************
    //  View Model
    //*****************************************************
    var ViewModel = function (model) {
        var self = this;
        self.model = model;

        self.events = model.Data.Events;
        self.labels = model.Data.Labels;

        self.message = ko.observable(null);

        self.isChecked = null;
        self.description = null;
        self.table = ko.observable(null);
        self.column = ko.observable(null);
        self.eventValue = null;

        self.selectedEvent = ko.observable(null);
        self.selectedOperator = ko.observable(null);
        self.selectedUsers = ko.observableArray([]);

        self.isWorkgroupGroupVisible = true;
        self.isOperatorGroupVisible = false;

        self.selectedWorkgroup = null;
        self.isLoading = ko.observable(false);
    }
    //*****************************************************

    //*****************************************************
    //*****************************************************
    //  Bindings
    //*****************************************************

    var initWorkgroupSelector = function() {
        if (_vm.workGroupSelector) {
            $getE('#treeBase').wfsTreeSelector('destroy');
        }
        _vm.workGroupSelector = $getE('#treeBase').wfsTreeSelector({
            useExpander: true,
            entityURL: '/RecHubRaamProxy/api/entity',
            callback: selectedTreeItem,
            dataCallback: dataCallback,
            expanderTitle: "Select Workgroup",
            entitiesOnly: false,
            closeOnSelectNonNode: false
        });
    };

    var setupElementBindings = function () {

        $getE('#cancelButton').bind('click', function () {
            framework.closeModal();
            return false;
        });

        $getE('#saveButton').bind('click', saveOnClickValidate);

        _vm.eventSelect = $('#eventSelect').wfsSelectbox({
            'items': _vm.events,
            'callback': setSelectedEvent,
            'displayField': 'EventLongName',
            'idField': 'ID',
            'placeHolder': 'Select an Event...',
            'width': 330
        });

        initWorkgroupSelector();

        _vm.operatorSelect = $getE('#operatorsSelect').wfsSelectbox({
            'items': [],
            'callback': setSelectedOperator,
            'displayField': 'Name',
            'idField': 'ID',
            'placeHolder': 'Select an Operator...',
            'width': 330
        });

        _vm.userSelector = $getE('#userSelect').wfsTreeSelector({
            useExpander: true,
            entityURL: '/RecHubRaamProxy/api/entityUser/',
            checkedStatusChanged: selectedUser,
            dataCallback: userDataCallback,
            expanderTitle: "Select User",
            entitiesOnly: false,
            icon: 'fa fa-user',
            multiSelect: true
        });


    }
    //*****************************************************

    //*****************************************************
    //*****************************************************
    //  Validation
    //*****************************************************

    var setFormValidationOptions = function (options) {
        var setFieldRules = function (fieldSelector, rules) {
            var element = $getE(fieldSelector);
            element.rules("remove");
            element.rules("add", rules);
        };

        setFieldRules("#hdnEvent", { required: true });
        setFieldRules("#description", { required: true, maxlength: 30 });
        setFieldRules("#hdnOperator", { required: function(element) { return _vm.isOperatorGroupVisible; } });
        setFieldRules("#eventValue", {
            required: function(element) { return _vm.isOperatorGroupVisible; },
            number: options.number
        });
    };

    var initFormValidation = function() {
        $getE("#eventRuleForm").validate({
            ignore: ".ignore, .select2-input",
            messages: {
                hdnEvent: { required: _vm.labels.ErrorEventRequired },
                description: { required: _vm.labels.ErrorDescriptionRequired, maxlength: _vm.labels.ErrorDescriptionMaxLength },
                hdnOperator: { required: _vm.labels.ErrorEventOperatorRequired },
                eventValue: { required: _vm.labels.ErrorEventValueRequired }
            },
            submitHandler: function(form) {
                // Validation successful - submit the values
                if (_vm.isWorkgroupGroupVisible && (!_vm.selectedWorkgroup || _vm['selectedWorkgroup'].isNode == false)) {
                    $getE('#workgroupSelectError').css('display', 'inline-block');
                    _vm.isLoading(false);
                    return;
                } else
                    saveOnClick();
            },
            invalidHandler: function(event, validator) {
                // Submit was cancelled - errors should already be showing
                _vm.isLoading(false);
            }
        });

        setFormValidationOptions({ number: true });
    };

    //*****************************************************

    var closeModal = function () {
        framework.closeModal();
    }

    var selectedSelectItem = function (selected) {
        _vm['selectedWorkgroup'] = selected;
    };

    var selectedTreeItem = function (selected, initial) {
        _vm['selectedWorkgroup'] = null;

        if (!_vm.isWorkgroupGroupVisible || !selected || initial)
            return;

        var title = null;
        if (selected.isNode) {
            title = 'Selected: ' + selected['label'];
            _vm['selectedWorkgroup'] = selected;
        }
        else {
            framework.errorToast(_container, "Please select a single workgroup.");
            title = "Select a Workgroup";
            return false;
        }
        _vm.workGroupSelector.wfsTreeSelector('setTitle', title);
    };

    var dataCallback = function (data) {
        var entities = _vm.workGroupSelector.wfsTreeSelector('getEntities');
        var workgroups = _vm.workGroupSelector.wfsTreeSelector('getWorkgroups');
        var ecount = entities.length;
        var wcount = workgroups.length;

        _vm.workGroupSelector.wfsTreeSelector('initialize');
    };

    var setMainGroupVisible = function (visible) {
        if (visible)
            $getE('#mainGroup').css('display', 'block');
        else
            $getE('#mainGroup').css('display', 'none');
    }

    var setAppliesToGroupVisible = function (eventName, table, column, operators) {
        if (eventName.toLowerCase() != 'decisionpending' && table != '') {

            $getE('#appliesToGroup').css('display', 'block');

            if (column != '')
                setColumnGroupVisible(true);
            else
                setColumnGroupVisible(false);

            if (operators > 0)                
                setOperatorsGroupVisible(true);           
            else
                setOperatorsGroupVisible(false);

            // set number validation or text validation.
            if (eventName.toLowerCase() == 'payerpaymentreceived') {
                setFormValidationOptions({ number: false });
                $getE("#dollarIcon").text("");
            }
            else {
                setFormValidationOptions({ number: true });
                $getE("#dollarIcon").text("$");
            }
        }
        else {
            setColumnGroupVisible(false);
            setFormValidationOptions({ number: true });
            $getE('#appliesToGroup').css('display', 'none');
        }
    }

    var setWorkgroupVisible = function (eventName) {
        if (eventName == '' || eventName.toLowerCase() == 'undefined' || eventName.toLowerCase() == 'processingexception') {
            _vm.isWorkgroupGroupVisible = false;
            $getE('#workgroupGroup').css('display', 'none');
        }
        else if (!_vm.isWorkgroupGroupVisible) {
            _vm.isWorkgroupGroupVisible = true;
            $getE('#workgroupGroup').css('display', 'block');

            // Reset the WGS, as it can't initialize properly when hidden.
            initWorkgroupSelector();
        }
    }

    var setColumnGroupVisible = function (visible) {
        if (visible)
            $getE('#columnGroup').css('display', 'block');
        else
            $getE('#columnGroup').css('display', 'none');
    }

    var setOperatorsGroupVisible = function (visible) {
        if (visible) {
            $getE('#operatorsGroup').css('display', 'block');
            _vm.isOperatorGroupVisible = true;
        }
        else {
            $getE('#operatorsGroup').css('display', 'none');
            _vm.isOperatorGroupVisible = false;
        }
    }

    var saveOnClickValidate = function () {
        _vm.isLoading(true);
        $getE("#eventRuleForm").submit();
    };

    var saveOnClick = function () {
        var url = "/RecHubConfigViews/Alerts/InsertEventRule/";
        var data = {
            eventID: _vm.selectedEvent().ID,
            description: _vm.description,
            isActive: _vm.isChecked == null ? false : _vm.isChecked,
            eventOperator: _vm.selectedOperator() == null ? '' : _vm.selectedOperator().ID,
            eventValue: _vm.eventValue,
            siteBankID: _vm.isWorkgroupGroupVisible ? _vm.selectedWorkgroup.id.split('|')[0] : -1,
            siteClientAcctID: _vm.isWorkgroupGroupVisible ? _vm.selectedWorkgroup.id.split('|')[1] : -1,
            users: _vm.selectedUsers() == null ? [] : _vm.selectedUsers()
        };

        var saveOnClickCallback = function (serverResponse) {
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, serverResponse.Errors[0].Message);
                _vm.isLoading(false);
            }
            else {
                closeModal();
            }
        };

        framework.doJSON(url, $.toDictionary(data), saveOnClickCallback);
    }

    //*****************************************************
    //*****************************************************
    //  Initialization
    //*****************************************************
    var init = function (myContainerId, model) {
        $(function () {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b) };
            _vm = new ViewModel(model);

            ko.applyBindings(_vm, $getE('#addEventRuleModal').get(0));

            setupElementBindings();

            initFormValidation();
        });
    };

    return {
        "init": init
    }
    //*****************************************************
});
