﻿define(["jquery", "ko", "frameworkUtils", "accounting"], function ($, ko, framework, accounting) {

    function Alerts() {

        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var setupElementBindings = function () {

            $getE('.refresh').bind('click', function () {
                doRefresh();
                return false;
            });

            $getE('#addEventRule').bind('click', function () {
                openAddModal();
                return false;
            });
            var callbacks = $(window).data("hubCallbacks");
            if (!callbacks) {
                callbacks = $.Callbacks("unique");
            }
            callbacks.add(resizeGrid);
            $(window).data("hubCallbacks", callbacks);

            doRefresh();

        }

        var getColumns = function () {

            var baseCols = [
              { id: "EventLongName", name: "Event Name", field: "EventLongName", sortable: true },
              { id: "Description", name: "Alert Description", field: "Description", sortable: true },
              { id: "Hierarchy", name: "Entity", field: "ParentHierarchy", sortable: true },
              { id: "SiteClientAccountID", name: "Workgroup", field: "SiteClientAccountID", sortable: true, formatter: workgroupFormatter },
              { id: "IsActive", name: "Active", field: "IsActive", sortable: true, headerCssClass: "text-center", formatter: activeFormatter, width: 30 },
            ];

            if (_vm.canEditAlerts()) {
                var colModify = { id: "EditEventRule", name: "", field: "", sortable: false, headerCssClass: "text-center", formatter: editFormatter, width: 30, minWidth: 30, maxWidth: 30 };
                baseCols.unshift(colModify);
            }

            return baseCols;
        };

        var workgroupFormatter = function (row, cell, value, columnDef, item) {
            if (value == -1) {
                return "All";
            }
            else {
                return item.WorkgroupName;
            }
        }

        var activeFormatter = function (row, cell, value, columnDef, item) {
            if (value == true) {
                return "<span style='text-align:center; margin: 0 auto; display: block;'><i class='fa fa-check wfs'></i></span>";
            }
            else {
                return "";
            }
        }

        var editFormatter = function (row, cell, value, columnDef, item) {
            if (1 == 1) { //if (_vm['canViewDetail']) {
                return "<a id='" + value + "' title='Edit Event Rule' href='#_'><span style='text-align:center; margin: 0 auto; display: block;'><i class='fa fa-edit'></i></span></a>";
            }
            else {
                return "";
            }
        };

        var getGrid = function () {
            doRefresh();

            var dataView;
            var grid;
            var options = {
                enableColumnReorder: false,
                enableCellNavigation: false,
                fullWidthRows: true,
                forceFitColumns: true,
                autoHeight: true,
                asyncEditorLoading: false,
                autoEdit: false,
                editable: false,
                showHeaderRow: false,
                explicitInitialization: true
            };
            var columns = getColumns();
            var columnFilters = {};

            var dataView = new Slick.Data.DataView();
            dataView.setPagingOptions({ pageSize: 15 });

            var grid = new Slick.Grid('#alertGrid', dataView, columns, options);

            $($getE('#alertGrid')).bind('destroyed', function () {
                $(window).unbind('resize', resizeGridNoParameters);
                grid.destroy();
            });

            var pager = new Slick.Controls.Pager(dataView, grid, $getE("#pager"));

            _vm['grid'] = grid;
            _vm['dataView'] = dataView;
            _vm['columnFilters'] = columnFilters;

            dataView.onRowCountChanged.subscribe(function (e, args) {
                grid.updateRowCount();
                grid.render();
            });

            dataView.onRowsChanged.subscribe(function (e, args) {
                grid.invalidateRows(args.rows);
                grid.render();
            });

            grid.onSort.subscribe(function (e, args) {
                gridSorter(args.sortCol.id, args.sortCol.field, args.sortAsc);
            });

            dataView.onPagingInfoChanged.subscribe(function (e, pagingInfo) {
                var isLastPage = pagingInfo.pageNum == pagingInfo.totalPages - 1;
                var options = grid.getOptions();
            });

            grid.onClick.subscribe(function (e, args) {
                var item = grid.getDataItem(args.row);
                var colDef = grid.getColumns()[args.cell];
                if (colDef.id === "EditEventRule") {
                    if (item && !item.rows) {
                        openEditModal(item);
                    }
                }
            });

            grid.init();

            $(window).bind('resize', function () {
                resizeGrid();
            });

        }

        var resizeGridNoParameters = function () { resizeGrid(); }; // Call resizeGrid, ignoring any parameters passed to this function

        var resizeGrid = function () {
            var grid = _vm["grid"];
            grid.invalidate();
            grid.render();
            grid.resizeCanvas();
        };

        var doRefresh = function () {
            _vm['columnFilters'] = {};
            var eventRulesURL = "/RecHubConfigViews/Alerts/GetEventRules";
            var data = { inactive: _vm.showInactive() };
            framework.doJSON(eventRulesURL, $.toDictionary(data), refreshCallback);
            framework.showSpinner('Loading...');
        };

        var refreshCallback = function (serverResponse) {
            $getE('#alertNoItemsMessage').empty();
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
                return
            }
            if (!serverResponse.Data || serverResponse.Data.length < 1) {
                var messageDiv = $('<div style="font-weight: bold;" >' + 'No Event Rule Data' + '</div>');
                $getE('#alertNoItemsMessage').append(messageDiv);
                _vm['items'] = [];
            }
            else {
                _vm['items'] = serverResponse.Data;
            }
            
            var dataView = _vm['dataView'];
            var grid = _vm['grid'];

            function filter(item) {
                var columnFilters = _vm['columnFilters'];
                for (var columnId in columnFilters) {
                    if (columnId !== undefined && columnFilters[columnId] !== "") {
                        var c = grid.getColumns()[grid.getColumnIndex(columnId)];
                        if (item[c.field].toString().indexOf(columnFilters[columnId]) == -1) {
                            return false;
                        }
                    }
                }
                return true;
            }

            dataView.beginUpdate();
            dataView.setItems(_vm['items'], 'EventRuleID');
            dataView.setFilter(filter);
            dataView.endUpdate();

            gridSorter(_vm.sortcol, _vm.sortField, _vm.sortdir);

            resizeGrid();

            framework.hideSpinner();

            var callbacks = $(window).data("hubCallbacks");
            if (callbacks)
                callbacks.fire();
        };

        var gridSorter = function (colId, colField, sortAsc) {
            var grid = _vm["alertGrid"];
            var dataView = _vm['dataView'];

            _vm.sortdir = sortAsc;
            _vm.sortcol = colId;
            _vm.sortField = colField;

            // using native sort with comparer
            // preferred method but can be very slow in IE with huge datasets
            dataView.sort(function (a, b) {
                var colField = _vm.sortField;
                var x = a[colField], y = b[colField];

                if ('siteclientaccountid' === _vm.sortcol.toLowerCase()) {
                    //dual sort, first on ID, if ID matches let the full display name go through
                    // otherwise use the ID to sort appropriately
                    if (x === y) {
                        x = a.WorkgroupName;
                        y = b.WorkgroupName;
                    }
                }

                if (typeof x !== "string" || x.toLowerCase() === y.toLowerCase()) {
                    return (x == y ? 0 : (x > y ? 1 : -1));
                }
                else {
                    return (x.toLowerCase() === y.toLowerCase() ? 0 : (x.toLowerCase() > y.toLowerCase() ? 1 : -1));
                }
            }, sortAsc);
            resizeGrid();
        }

        //*****************************************************
        //*****************************************************
        //  Add Modal
        //*****************************************************
        var openAddModal = function () {
            var url = '/RecHubConfigViews/Alerts/AddEventRule/';
            var ww = $(window).width() / 2 - 350;
            var modalOptions = {
                title: 'Add Alert', width: '700', height: '700', position: [ww, 180],
                closeCallback: doRefresh
            }

            framework.loadByContainer(url, framework.getModalContent(), null, framework.openModal(modalOptions));
        }
        //*****************************************************

        //*****************************************************
        //*****************************************************
        //  Edit Modal
        //*****************************************************
        var openEditModal = function (eventRule) {
            var url = '/RecHubConfigViews/Alerts/EditEventRule/'

            var ww = $(window).width() / 2 - 350;
            var modalOptions = {
                title: 'Edit Event Rule', width: '700', height: '700', position: [ww, 180],
                closeCallback: doRefresh
            }
            framework.loadByContainer(url, framework.getModalContent(), eventRule, framework.openModal(modalOptions));
        }
        //*****************************************************

        //*****************************************************
        //*****************************************************
        //  View Model
        //*****************************************************
        var viewModel = function (model) {
            var self = this;
            self.model = model;
            self.showInactive = ko.observable(true);

            // Assign Page level permissions from RAAM
            self.canModifyEventRules = true;
            self.canViewAlerts = ko.observable(model.CanViewAlerts);
            self.showInactive.subscribe(function (val) {
                doRefresh();
            });            
            self.canEditAlerts = ko.observable(model.CanEditAlerts);
            self.canViewNotifications = ko.observable(model.CanViewNotifications);
            self.canDownload = ko.observable(model.CanDownload);

            self.sortcol = "EventLongName";
            self.sortField = "EventLongName";
            self.sortdir = true;

        }
        //*****************************************************


        //*****************************************************
        //*****************************************************
        //  Initialization
        //*****************************************************
        function init(myContainerId, model) {
            $(function () {
                _container = $('#' + myContainerId);
                $getE = function (b) { return _container.find(b); };
                var conElement = $getE('.alertPortlet').get(0);
                if(conElement)
                {
                    _vm = new viewModel(model);
                    try {
                        ko.applyBindings(_vm, conElement);
                    }
                    catch (err) {
                        alert(err);
                    }
                    setupElementBindings();

                    getGrid();
                }
            });
        }
        return {
            init: init
        }
    }

    function init(myContainerId, model) {
        $(function () {
            var alerts = new Alerts();
            alerts.init(myContainerId, model);
        });
    }

    return {
        init: init
    }
    //*****************************************************
});