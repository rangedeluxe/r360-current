﻿define(["frameworkUtils"], function (framework) {

    function init(myContainerId) {
        $(function () {
            var myContainer = $('#' + myContainerId);
            var $getE = function (b) { return myContainer.find(b) };

            var links = $getE('.hvUl').find('a');
            $.each(links, function () {
                var link = $(this);
                var id = link.attr('id');
                var container = $getE('#hvHomeContent')
                var data = { fromMenu: "anyValueWillDo" };
                link.click(function () {
                    $(this).parent().addClass('active').siblings().removeClass('active');
                    framework.showSpinner('', {}, myContainer);
                    framework.loadByContainer("/RecHubConfigViews/" + id, container, data, framework.hideSpinner, myContainer);
                });
            });
            links[0].click();

        });
    }

    return {
        init: init
    }
});

