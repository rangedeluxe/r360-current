﻿define(["jquery", "ko", "frameworkUtils", "accounting", "wfs.gridbase", "wfs.treeselector"], function ($, ko, framework, accounting, WFSGridBase) {

    function ErrorHandlingBindingProvider() {
        var original = new ko.bindingProvider();

        //determine if an element has any bindings
        this.nodeHasBindings = original.nodeHasBindings;

        //return the bindings given a node and the bindingContext
        this.getBindings = function (node, bindingContext) {
            var result;
            try {
                result = original.getBindings(node, bindingContext);
            }
            catch (e) {
                alert("Error in binding: " + e.message);
            }

            return result;
        };
    };

    function Workgroups() {
        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var refreshCallback = function () {
        	framework.loadByContainer('/RecHubConfigViews/Workgroups', _container);
        };

        var entityDataLoadedCallback = function (resp, widget) {
            //Only hide the spinner if there is no entity that can be selected, otherwise the selection of the root entity will trigger workgroups to load and
            // eventually hide the spinner
            if (resp == undefined || resp == null || resp.id === undefined || resp.id === null)
                framework.hideSpinner();

            _vm['entitySelector'].wfsTreeSelector('initialize');
        };

        var entityLoadedCallback = function (serverResponse) {
            framework.hideSpinner();
            if (serverResponse.Errors && serverResponse.Errors.length > 0) {
                _vm.notAssignable(true);
                _vm.selectedEntity({});

                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else if (serverResponse.Entity === null) {
            	_vm.notAssignable(serverResponse.NotAssignable);
            	_vm.noDefaults(true);
            	_vm.selectedEntity({});
            }
            else {
            	_vm.notAssignable(false);
            	_vm.noDefaults(false);
            	_vm.selectedEntity(serverResponse.Entity);
            	refreshWorkgroups(serverResponse.Entity.EntityID);
            }
        };

        var refreshSelectedEntity = function (entity) {
            if (entity === undefined) {
            	entity = _vm['entitySelector'].wfsTreeSelector('getSelection');
            	_vm.selectedEntity({});//TODO: Remove this line?
            }

            if (entity === null || (entity.entityTypeCode !== "Corp" && entity.entityTypeCode !== "HC" && entity.entityTypeCode !== "LOB") ) {
            	_vm.notAssignable(true);
            	_vm.selectedEntity({});
            }
            else {
                var purl = "/RecHubConfigViews/RecHubEntityMaintenance/GetOrCreateEntityWorkgroupDefaults";
                framework.showSpinner(_vm.labels.Loading);
                framework.doJSON(purl, { entityId: entity.id }, entityLoadedCallback);
            }
        };

        var entitySelectionChanged = function (entity) {
            refreshSelectedEntity(entity);
        };

        var reloadWorkgroups = function (clientAccountKey) {
            var entity = _vm['entitySelector'].wfsTreeSelector("getSelection");
            refreshWorkgroups(entity.id, clientAccountKey);
        }

        var setWorkgroups = function (banks, workgroups, selClientAccountKey) {
            _vm.banks = banks;
            _vm.workgroups = [];
            if (workgroups !== null && banks !== null) {
                var newId = 0;
                $.each(workgroups, function (index, item) {
                    var bank = ko.utils.arrayFirst(banks, function (bank) {
                        return item.SiteBankID === bank.SiteBankID;
                    });

                    _vm.workgroups.push($.extend({ BankName: (bank !== null ? bank.BankName + " - " : "") + item.SiteBankID }, item));
                });
            }
            _vm['workgroupGrid'].setItems(_vm.workgroups);
            _vm['workgroupGrid'].selectItem(selClientAccountKey);
        };

        var workgroupsLoadedCallback = function (serverResponse, selClientAccountKey) {
            framework.hideSpinner();
            var tabLabel = _vm.labels.EntityAssignment;
            if (serverResponse.HasErrors) {
                setWorkgroups(null, null);
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else {
                tabLabel += " (" + serverResponse.Data.UnassignedCount + " " + _vm.labels.Unassigned + ")";
                setWorkgroups(serverResponse.Data.Banks, serverResponse.Data.Workgroups, selClientAccountKey);
            }
            _vm.assignmentTabText(tabLabel);
            redrawGrid();
            setTimeout(function () {
                var el = $('#workgroupGrid').find('.slick-pane-top');
                el.height(el.height() + 25);
            }, 250);
        };

        var refreshWorkgroups = function (entityID, selClientAccountKey) {
            if (entityID !== null && entityID !== undefined) {
            	var purl = "/RecHubConfigViews/Workgroups/GetWorkgroups";
                framework.showSpinner(_vm.labels.Loading);
                framework.doJSON(purl, { entityID: entityID }, workgroupsLoadedCallback, selClientAccountKey);
            }
            else {
                setWorkgroups(null, null);
            }
        };

        var initTree = function () {
            if (_vm['entitySelector'] === undefined) {
                framework.showSpinner(_vm.labels.Loading);
                _vm['entitySelector'] = $getE("#entitySelector").wfsTreeSelector({ callback: entitySelectionChanged, entityURL: '/RecHubRaamProxy/api/entity', height: "487px", entitiesOnly: true, dataCallback: entityDataLoadedCallback });
            }
        }

        var maintainWorkgroup = function (clientAccountKey) {
            var entity = _vm['entitySelector'].wfsTreeSelector("getSelection");
            var data = { adding: clientAccountKey === undefined, entityID: entity.id, clientAccountKey: clientAccountKey, entityName: entity.label };

            var desiredWidth = 1100;
            var modalOptions = { title: data.adding ? "Add Workgroup" : "Edit Workgroup", width: desiredWidth, position: [($(window).width() - desiredWidth) / 2, 75], closeCallback: reloadWorkgroups };
            framework.loadByContainer('/RecHubConfigViews/Workgroups/WorkgroupDetails', framework.getModalContent(), data, framework.openModal(modalOptions));
            return false;
        };

        var editSelectedWorkgroup = function (gridRow, col, dataIndex, item) {
            if (item === undefined || item.ClientAccountKey === undefined) {
                framework.warningToast("A workgroup must be selected for editing.");
                return false;
            }
            
            maintainWorkgroup(item.ClientAccountKey);
            return false;
        };

        var unassignWorkgroupCallback = function (serverResponse) {
            framework.hideSpinner();
            if (serverResponse.Errors && serverResponse.Errors.length > 0) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else {
                refreshSelectedEntity();
            }
        };

        var unassignWorkgroupConfirmed = function (workgroup) {
            if (workgroup !== null && workgroup !== undefined) {
            	var purl = "/RecHubConfigViews/Workgroups/RemoveWorkgroupFromEntity";
                framework.showSpinner(_vm.labels.Removing);
                framework.doJSON(purl, $.toDictionary(workgroup), unassignWorkgroupCallback);
            }
        };

        var unassignWorkgroup = function (gridRow, col, dataIndex, item) {
            var selectedEntity = _vm['entitySelector'].wfsTreeSelector('getSelection');
            var removeConfirmationMessage = "You are about to remove workgroup \"" + item.LongName + "\" from entity \"" + selectedEntity.label + "\".  Would you like to continue?";

            framework.openModal({ closable: false, closeCallback: unassignWorkgroupConfirmed, width: '700px' });
            framework.getModalContent()
                .append($('<legend>Please Confirm</legend>'))
                .append($('<div class="row"></div>')
                    .append($('<div class="col-md-12"></div>')
                        .append($('<label>' + removeConfirmationMessage + '</label>'))
                        )
                    )
                .append($('<br />'))
                .append($('<div class="row"></div>')
                    .append($('<div class="col-md-12"></div>')
                        .append($('<a id="removeButton" class="btn wausauBtn btn-primary" href="#">Yes</a>')
                            .on('click', function() { framework.closeModal(item); return false; })
                            )
                        .append('&nbsp;')
                        .append($('<a id="cancelButton" class="btn wausauBtn btn-inverse" href="#">No</a>')
                            .on('click', function () { framework.closeModal(null); return false; })
                            )
                    )
                );
        };

        var unassignButtonRenderer = function (row, cell, value, columnDef, dataContext) {
            return "<a class='tableIcon' title='" + columnDef.toolTip + "' href='#'><i class='fa fa-unlink'></i></a>";
        };

        var initWorkgroupGrid = function () {
            if (_vm['workgroupGrid'] === undefined) {
                _vm.colUnassign = { Id: 'colUnassign', ColumnTitle: ' ', ColumnFieldName: '', Tooltip: _vm.labels.UnassignTooltip, resizable: false, width: 21, formatter: unassignButtonRenderer, onCellClick: unassignWorkgroup, visible: false };
                _vm.gridColumns = [
                    _vm.colUnassign,
                    { Id: "colLongName", ColumnTitle: _vm.labels.LongName, ColumnFieldName: "LongName", Tooltip: _vm.labels.LongNameTooltip, DataType: 1/*, width: 60*/, sortable: true },
                    { Id: "colBank", ColumnTitle: _vm.labels.Bank, ColumnFieldName: "BankName", Tooltip: _vm.labels.BankTooltip, DataType: 1/*, width: 60*/, sortable: true },
                    { Id: "colWorkgroupID", ColumnTitle: _vm.labels.WorkgroupID, ColumnFieldName: "SiteClientAccountID", Tooltip: _vm.labels.WorkgroupIDTooltip, DataType: 6/*, width: 60*/, sortable: true },
                    { Id: "colActive", ColumnTitle: _vm.labels.Active, ColumnFieldName: "IsActive", Tooltip: _vm.labels.ActiveTooltip, DataType: 20, resizable: false, width: 60, sortable: true }
                ];

                var gridModel = {
                    gridOptions: {
                        //forceFitColumns: false
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#workgroupGrid',
                        pagerSelector: '#workgroupPager',
                        pageSize: 15,
                        displayPagerSettings: true,
                        data: [],
                        idFieldName: 'ClientAccountKey',
                        allowRowDelete: false,
                        allowShowDetail: _vm.selectedTab() === "tabMaintenance",
                        showDetailTooltip: _vm.labels.EditWorkgroupTooltip,
                        showDetailCallback: editSelectedWorkgroup,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        sortField: "LongName",
                        columns: _vm.gridColumns,
                    },
                };

                _vm['workgroupGrid'] = new WFSGridBase.GridBase();
                _vm['workgroupGrid'].init(_container.attr('id'), gridModel);
            }
        };

        var setupElementBindings = function () {

            $getE('#workgroupMaintenanceTabs a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
                setTimeout(function () {
                    redrawGrid();
                    var el = $('#workgroupGrid').find('.slick-pane-top');
                    el.height(el.height() + 25);
                }, 100);
            });

        	// bind click event to edit button
            $getE('#editEntityButton').bind('click', function () {
            	var entity = _vm['entitySelector'].wfsTreeSelector("getSelection");
            	var data = { entityId: entity.id };
            	var desiredWidth = 400;
            	var modalOptions = { title: "Edit Entity", width: desiredWidth, position: [($(window).width() - desiredWidth) / 2, 75], closeCallback: refreshSelectedEntity };
            	framework.loadByContainer('/RecHubConfigViews/RecHubEntityMaintenance/EditEntity', framework.getModalContent(), data, framework.openModal(modalOptions));
            	return false;
            });

            // bind click event to add tab button
            $getE('#addWorkgroupButton').bind('click', function () {
                maintainWorkgroup();
                return false;
            });

            $getE('#assignWorkgroupsButton').bind('click', function () {
                var entity = _vm['entitySelector'].wfsTreeSelector("getSelection");
                var desiredWidth = 900;
                var modalOptions = { width: desiredWidth, position: [($(window).width() - desiredWidth) / 2, 75], closeCallback: reloadWorkgroups };
                framework.loadByContainer('/RecHubConfigViews/Workgroups/AssignWorkgroups', framework.getModalContent(), { entityID: entity.id, entityName: entity.label }, framework.openModal(modalOptions));
                return false;
            });

            initTree();
            //initWorkgroupGrid();
            $getE('#workgroupMaintenanceTabs > li > a').on('shown.bs.tab', function (e) {
                _vm.selectedTab(e.target.id);

                //hack to get around the grid hiding last column, this mess needs to be replaced asap
                //
                var el = $('#workgroupGrid').find('.slick-pane-top');
                el.height(el.height() + 25);
                
                $('.slick-pager-settings-expanded').find('a').each(function () {               
                    $(this).on('click', function () {
                            redrawGrid();
                            var el = $('#workgroupGrid').find('.slick-pane-top');
                            el.height(el.height() + 25);
                    });
                });

                $('#workgroupGrid').find('.slick-header-sortable').each(function () {
                    $(this).on('click', function () {
                        setTimeout(function () {
                            redrawGrid();
                            var el = $('#workgroupGrid').find('.slick-pane-top');
                            el.height(el.height() + 25);
                        }, 100);
                    })
                });
            
                $('.slick-pager-nav').find('.ui-icon').each(function () {
                    $(this).on('click', function () {
                        setTimeout(function () {
                        redrawGrid();
                        var el = $('#workgroupGrid').find('.slick-pane-top');
                        el.height(el.height() + 25);
                        }, 50);
                    });
                });
            });
        };

        var redrawGrid = function () { _vm['workgroupGrid'].resizeGrid(); };

        var sViewModel = function (model) {
            var self = this;
            self.model = model;
            self.CanManage = model.CanManage;
            self.CanViewAssociations = model.CanViewAssociations;
            self.CanManageAssociations = model.CanManageAssociations;

            self.labels = {
                PageRefresh: "Refresh the workgroup list.",
                EditWorkgroupTooltip: "View/Maintain the workgroup information.",
                LongName: "Long Name",
                Bank: "Bank",
                WorkgroupID: "Workgroup ID",
                Active: "Active",

                LongNameTooltip: "The name displayed when showing the workgroup throughout the application.",
                BankTooltip: "The bank the workgroup belongs to.",
                WorkgroupIDTooltip: "The workgroup's identifier from the source system.",
                ActiveTooltip: "Indicates whether the workgroup is currently active.",
                UnassignTooltip: "Remove the workgroup from the entity.",

                Loading: "Loading...",
                Removing: "Removing...",

                PleaseSelectAssignableEntity: "Please select an entity that may have workgroups assigned to it.",
                EntityIsNotConfigured: "The Workgroup Defaults must be configured before this entity can be assigned any workgroups.",

                EntityAssignment: "Entity Assignment",
                DefaultsTab: "Entity Workgroup Defaults",
                Unassigned: "unassigned",

                Edit: "Edit",

                PaymentImageDisplayModeLabel: "Payment Image Display Mode:",
                DocumentImageDisplayModeLabel: "Document Image Display Mode:",
                DisplayBatchIDLabel: "Display Batch ID:",
                ViewingDaysLabel: "Viewing Days:",
                MaximumSearchDaysLabel: "Max Search Days:",
                BillingAccountLabel: "Billing Account:",
                BillingField1Label: "Billing Field 1:",
				BillingField2Label: "Billing Field 2:"
            };

            self.notAssignable = ko.observable(true);
            self.assignable = ko.computed(function () { return self.notAssignable() === false; });
            self.noDefaults = ko.observable(true);
            self.assignableEntitySelected = ko.computed(function () { return self.notAssignable() === false && self.noDefaults() === false; });
            self.assignableEntitySelected.subscribe(function (newValue) {
                setTimeout(initWorkgroupGrid, 50);
            });

            self.workgroups = null;

            self.refreshScreen = refreshCallback;
            self.selectedTab = ko.observable("tabDefaults");//default tab
            self.selectedTab.subscribe(function (newTab) {
            	if (self.colUnassign)
            	{
            	    self.colUnassign.visible = (newTab === "tabAssignment") && _vm.CanManageAssociations;
            	    _vm['workgroupGrid'].updateGridColumns(_vm.gridColumns, newTab === "tabMaintenance");

            		// If the grid was not previously visible, it won't size the columns correctly...
            		setTimeout(redrawGrid, 50);
            	}
            });
            self.allowAdd = ko.computed(function () { return self.assignableEntitySelected() && self.selectedTab() == "tabMaintenance" });
            self.allowAssignment = ko.computed(function () { return self.assignableEntitySelected() && self.selectedTab() == "tabAssignment" });
            self.showWorkgroupGrid = ko.computed(function () { return self.assignableEntitySelected() && (self.selectedTab() == "tabMaintenance" || self.selectedTab() == "tabAssignment") });
            self.showNoDefaultsError = ko.computed(function () { return self.noDefaults() && (self.selectedTab() == "tabMaintenance" || self.selectedTab() == "tabAssignment") });
            self.showDefaults = ko.computed(function () { return self.selectedTab() == "tabDefaults" });

            self.assignmentTabText = ko.observable(self.labels.EntityAssignment);

            self.imageDisplayModeText = function (modeId) {
            	if (modeId === 1) return "Front and Back";
            	else if (modeId === 2) return "Front only";
            	else return "Use System Setting";
            };
            self.selectedEntity = ko.observable({});

            self.assignWorkgroupsButtonText = self.CanManageAssociations ? "Assign Workgroups" : "View Unassigned Workgroups";
        };

        function init(myContainerId, model) {
            $(function () {
                _container = $('#' + myContainerId);
                $getE = function (b) { return _container.find(b); };

                if (model.Success !== true) {
                    //TODO: come up with a better way of handling the error without ever showing the modal
                    framework.hideSpinner();
                    framework.errorToast(_container, framework.formatErrors(model.Errors));
                    return;
                }

                ko.bindingProvider.instance = new ErrorHandlingBindingProvider();

                _vm = new sViewModel(model);
                ko.applyBindings(_vm, $getE('#workgroupsBase').get(0));

                setupElementBindings();
            });
        };

        return {
            init: init
        };
    }

    function init(myContainerId, model) {
        $(function () {
            var workgroups = new Workgroups();
            workgroups.init(myContainerId, model);
        });
    }

    return {
        init: init
    };
});

