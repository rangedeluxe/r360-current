﻿define(["jquery", "ko", "frameworkUtils", "accounting", "wfs.gridbase"], function ($, ko, framework, accounting, WFSGridBase) {

    function ErrorHandlingBindingProvider() {
        var original = new ko.bindingProvider();

        //determine if an element has any bindings
        this.nodeHasBindings = original.nodeHasBindings;

        //return the bindings given a node and the bindingContext
        this.getBindings = function (node, bindingContext) {
            var result;
            try {
                result = original.getBindings(node, bindingContext);
            }
            catch (e) {
                alert("Error in binding: " + e.message);
            }

            return result;
        };
    };

    function AssignWorkgroups() {
        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var moveSelected = function (selectedItems, source, destination) {
            if (selectedItems && selectedItems.length > 0) {
                $.each(selectedItems, function (index, item) {
                    var index = source.indexOf(item);
                    if (index >= 0)
                        source.splice(index, 1);
                    destination.push(item);
                });
            }
        };

        var refreshGridItems = function (availableWorkgroups, selectedWorkgroups) {
            _vm['availableGrid'].setItems(availableWorkgroups);
            _vm['selectedGrid'].setItems(selectedWorkgroups);
        };

        var assignAllClick = function () {
            _vm.selectedWorkgroups = _vm.selectedWorkgroups.concat(_vm.availableWorkgroups);
            _vm.availableWorkgroups = [];
            refreshGridItems(_vm.availableWorkgroups, _vm.selectedWorkgroups);
        }

        var assignSelectedClick = function () {
            moveSelected(_vm['availableGrid'].getSelectedItems(), _vm.availableWorkgroups, _vm.selectedWorkgroups);
            refreshGridItems(_vm.availableWorkgroups, _vm.selectedWorkgroups);
        };

        var removeAllClick = function () {
            _vm.availableWorkgroups = _vm.availableWorkgroups.concat(_vm.selectedWorkgroups);
            _vm.selectedWorkgroups = [];
            refreshGridItems(_vm.availableWorkgroups, _vm.selectedWorkgroups);
        };

        var removeSelectedClick = function () {
            moveSelected(_vm['selectedGrid'].getSelectedItems(), _vm.selectedWorkgroups, _vm.availableWorkgroups);
            refreshGridItems(_vm.availableWorkgroups, _vm.selectedWorkgroups);
        };

        this.saveComplete = function (jsonResult) {
            framework.hideSpinner();

            if (!jsonResult.HasErrors) {
                if (jsonResult.PartialSuccess === true)
                    framework.errorToast(_container, _vm.labels.PartialSuccess);
                if (jsonResult.InfoMessage !== undefined && jsonResult.InfoMessage !== null && jsonResult.InfoMessage.length > 0)
                    framework.noticeToast(_container, jsonResult.InfoMessage, { sticky: true });
                framework.closeModal();
            }
            else {
                //display errors
                framework.errorToast(_container, framework.formatErrors(jsonResult.Errors));
            }
        };

        this.saveAssignments = function () {
            framework.showSpinner(_vm.labels.Saving);

            var entityID = _vm.entityID;
            var workgroups = _vm.selectedWorkgroups;

            if (workgroups === null || workgroups.length === 0) {
                framework.closeModal();
                framework.hideSpinner();
            }
            else {
                var data = {
                    entityID: entityID,
                    workgroups: workgroups
                };
                framework.doJSON("/RecHubConfigViews/Workgroups/SaveAssignments", $.toDictionary(data), _me.saveComplete);
            }
            return false;
        };

        var initAvailableGrid = function () {
            if (_vm['availableGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        multiSelect: true
                    },
                    implOptions: {
                        gridSelector: '#availableGrid',
                        pagerSelector: '#availablePager',
                        pageSize: 10,
                        displayPagerSettings: true,
                        data: _vm.availableWorkgroups,
                        idFieldName: 'ClientAccountKey',
                        allowRowDelete: false,
                        allowShowDetail: false,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        sortField: "LongName",
                        fullRowSelect: true,
                        columns: [
                            { Id: "colLongName", ColumnTitle: _vm.labels.LongName, ColumnFieldName: "LongName", Tooltip: _vm.labels.LongNameTooltip, DataType: 1, width: 65, sortable: true },
                            { Id: "colWorkgroupID", ColumnTitle: _vm.labels.WorkgroupID, ColumnFieldName: "SiteClientAccountID", Tooltip: _vm.labels.WorkgroupIDTooltip, DataType: 6, width: 35, sortable: true },
                        ],
                    },
                };

                _vm['availableGrid'] = new WFSGridBase.GridBase();
                _vm['availableGrid'].init(_container.attr('id'), gridModel);
            }
        };

        var initSelectedGrid = function () {
            if (_vm['selectedGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        multiSelect: true
                    },
                    implOptions: {
                        gridSelector: '#selectedGrid',
                        pagerSelector: '#selectedPager',
                        pageSize: 10,
                        displayPagerSettings: true,
                        data: [],
                        idFieldName: 'ClientAccountKey',
                        allowRowDelete: false,
                        allowShowDetail: false,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        sortField: "LongName",
                        fullRowSelect: true,
                        columns: [
                            { Id: "colLongName", ColumnTitle: _vm.labels.LongName, ColumnFieldName: "LongName", Tooltip: _vm.labels.LongNameTooltip, DataType: 1, width: 65, sortable: true },
                            { Id: "colWorkgroupID", ColumnTitle: _vm.labels.WorkgroupID, ColumnFieldName: "SiteClientAccountID", Tooltip: _vm.labels.WorkgroupIDTooltip, DataType: 6, width: 35, sortable: true },
                        ],
                    },
                };

                _vm['selectedGrid'] = new WFSGridBase.GridBase();
                _vm['selectedGrid'].init(_container.attr('id'), gridModel);
            }
        };

        var setupElementBindings = function () {

            $getE('#cancelButton').bind('click', function () {
                framework.closeModal();
                return false;
            });

            initAvailableGrid();

            if (_vm.CanAssociate) {
                initSelectedGrid();

                $getE('#addAll').bind('click', assignAllClick);
                $getE('#addSelected').bind('click', assignSelectedClick);
                $getE('#removeSelected').bind('click', removeSelectedClick);
                $getE('#removeAll').bind('click', removeAllClick);

                // bind click event to add tab button
                $getE('#saveButton').bind('click', function () {
                    _me.saveAssignments();
                });
            }
        };

        var sViewModel = function (model) {
            var self = this;
            self.CanAssociate = model.CanAssociate;

            self.labels = {
                LongName: "Long Name",
                WorkgroupID: "Workgroup ID",

                LongNameTooltip: "The name displayed when showing the workgroup throughout the application.",
                WorkgroupIDTooltip: "The workgroup's identifier from the source system.",

                Saving: "Saving...",

                PartialSuccess: "The assignments attempted partially succeeded.  Please review the workgroups and retry any assignments that failed.",
            };

            self.entityID = model.EntityID;

            self.availableWorkgroups = model.UnassignedWorkgroups === null ? [] : model.UnassignedWorkgroups;
            self.selectedWorkgroups = [];

            self.headerText = self.CanAssociate ? "Assign Workgroups to " + model.EntityName : "Unassigned Workgroups";
            self.cancelButtonText = self.CanAssociate ? "Cancel" : "Close";
        };

        function init(myContainerId, model) {
            $(function () {
                _container = $('#' + myContainerId);
                $getE = function (b) { return _container.find(b); };

                ko.bindingProvider.instance = new ErrorHandlingBindingProvider();

                _vm = new sViewModel(model);
                ko.applyBindings(_vm, $getE('#assignWorkgroups').get(0));

                setupElementBindings();
            });
        };

        return {
            init: init
        };
    }

    function init(myContainerId, model) {
        $(function () {
            var assignWorkgroups = new AssignWorkgroups();
            assignWorkgroups.init(myContainerId, model);
        });
    }

    return {
        init: init
    };
});

