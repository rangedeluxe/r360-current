﻿define(["jquery", "ko", "frameworkUtils", "wfs.gridbase", "mapping", "wfs.select"], function ($, ko, framework, WFSGridBase, mapping) {

    function ErrorHandlingBindingProvider() {
        var original = new ko.bindingProvider();

        //determine if an element has any bindings
        this.nodeHasBindings = original.nodeHasBindings;

        //return the bindings given a node and the bindingContext
        this.getBindings = function (node, bindingContext) {
            var result;
            try {
                result = original.getBindings(node, bindingContext);
            }
            catch (e) {
                alert("Error in binding: " + e.message);
            }

            return result;
        };
    };

    function WorkgroupDetails() {
        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var _namePattern = /^[A-Za-z0-9 &,\.:'()_-]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash
        var _fieldNamePattern = /^[-A-Za-z0-9_]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash
        var _intPattern = /^\d{0,10}$/;//this gets us close, we still have to verify it's within the integer range
        var _shortPattern = /^\d{0,5}$/;
        var _shortNotZeroPattern = /^[1-9]\d{0,4}$/
        var _tinyIntPattern = /^\d{0,3}$/;
        var _twoDigitPattern = /^\d{0,2}$/;
        var _twoDigitNoZeroPattern = /^(0*[1-9][0-9]{0,1})$/;
        var _threeDigitNegNoZeroPattern = /^(-?0*[1-9][0-9]{0,2})$/;
        var _rtPattern = /^\d{9}$/;
        var _ddaPattern = /^\d{0,35}$/;
        var _fileGroupPattern = /^(([a-zA-Z]\:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>""|]*))+)$/;
        var _commaSeparatedNumbersPattern = /^\d+(,\d+)*,?$/;
        var _singleDigitPattern = /^\d$/;
        var _validateDateSymbols = "~`@#$^&*()_-+={}[]\\|:;\"\'<>./?"; //do not allow % and comma
        var _timePattern = /^((0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])$/;

        var _fixedMaxSearchDays = 365;
        var _fixedMaxRetentionDays = 2557;

        this.validateInt = function (value, validPattern, observableError, allowNull) {
            var returnVal = null;

            if (value !== undefined && value !== null) {
                //if we currently have a number, turn it into a string so we can validate the format
                if (typeof value === "number") {
                    value = value.toString();
                }
                if (value.length === 0)
                    value = null;
                else if (!value.match(validPattern)) {
                    observableError(true);
                }
                else {
                    returnVal = parseInt(value, 10);
                }
            }

            if (value === undefined || (!allowNull && returnVal === null))
                observableError(true);
            return returnVal;
        };

        this.verifyDDANotAssociatedComplete = function (jsonResult, dda) {
            if (!jsonResult.HasErrors) {
                if (jsonResult.Data === null || jsonResult.Data === undefined) {
                    framework.errorToast(_container, "An error occurred while attempting to verify the DDA is not already assigned to a workgroup.");
                }
                else if (jsonResult.Data.DDAIsAssociated !== false) {
                    _vm.associateDDAErrorText(_vm.labels.DDAAssociatedToDifferentWorkgroup);
                    _vm.associateDDAError(true);
                }
                else {
                    _vm['ddaGrid'].addItem(dda);
                    //clear the DDA but not the RT as the RT may be used again
                    _vm.associateDDA('');
                }
            }
            else {
                //display errors
                framework.errorToast(_container, framework.formatErrors(jsonResult.Errors));
            }

            framework.hideSpinner();
            $getE('#DDA').focus();
        };

        this.associateDDAOnClick = function () {
            framework.showSpinner(_vm.labels.Verifying);

            var associateRT = _vm.associateRT();
            var associateDDA = _vm.associateDDA();

            _vm.associateRTError(associateRT === undefined || associateRT.length == 0 || !associateRT.match(_rtPattern));

            _vm.associateDDAError(associateDDA === undefined || associateDDA.length == 0 || !associateDDA.match(_ddaPattern));
            if (_vm.associateDDAError()) {
                _vm.associateDDAErrorText(_vm.labels.RequiredDDAFormat);
                framework.hideSpinner();
                return false;
            }

            if (_vm.associateRTError() || _vm.associateDDAError()) {
                framework.hideSpinner();
                return false;
            }

            var maxId = -1;
            var match = ko.utils.arrayFirst(_vm.selectedDDAs, function (item) {
                maxId = item.Id > maxId ? item.Id : maxId;
                return associateRT === item.RT && parseInt(associateDDA, 10) == parseInt(item.DDA, 10);
            });

            if (!match) {
                var dda = {
                    Id: maxId + 1,
                    RT: associateRT,
                    DDA: associateDDA,
                };

                var parms = {
                    allowedSiteBankID: _vm.updatingExisting ? _vm.selectedWorkgroup().SiteBankID : 0,
                    allowedSiteClientAccountID: _vm.updatingExisting ? _vm.selectedWorkgroup().SiteClientAccountID : 0,
                    dda: dda
                };

                framework.doJSON("/RecHubConfigViews/Workgroups/VerifyDDANotAssociated", $.toDictionary(parms), _me.verifyDDANotAssociatedComplete, dda);
            }
            else {
                _vm.associateDDAErrorText(_vm.labels.DDAAlreadyAssociated);
                _vm.associateDDAError(true);
                framework.hideSpinner();
            }
            return false;
        };

        var clearFieldErrors = function () {
            _vm.deColumnFieldNameError(false);
            _vm.deColumnScreenOrderError(false);
            _vm.deColumnDocTypeError(false);
            _vm.deColumnDataTypeError(false);
            _vm.deACHSourceError(false);
            _vm.deColumnUILabelError(false);
        };

        var endEditingField = function () {
            _vm['docTypeSelect'].wfsSelectbox('setValue', undefined);
            _vm['dataTypeSelect'].wfsSelectbox('setValue', undefined);
            _vm['paymentSourceSelect'].wfsSelectbox('setValue', undefined);
            _vm['paymentSourceACHSelect'].wfsSelectbox('setValue', undefined);
            _vm.newField({});
            _vm.DEActive(true);
            _vm.DERequired(false);
            _vm.markSense(false);
            _vm.editingField(false);
            _vm.DEFieldKeyEdit(0);
            _vm.creatingACHFields(false);
            _vm.AddingField(false);
            clearFieldErrors();
        };

        //Save for DE Fields, only saves locally
        this.addFieldOnClick = function () {
            var template = _vm["templateSelect"].wfsSelectbox("getData");
            if (template === null) {
                //shouldn't be possible
                return false;
            }

            var succeeded = true;
            if (template.Default === true) {
                succeeded = _vm.DEFieldKeyEdit() == 0 ? addNewField() : editDEField();
            }
            else {
                succeeded = addTemplateFields(template);
            }

            if (succeeded) {
                endEditingField();
                setTimeout(function () {
                    var el = $('#fieldGrid').find('.slick-pane-top');
                    el.height(el.height() + 25);
                }, 500);
            }
        };

        //Add function only adds to the local list of DE fields, fields are only saved to the server when all the changes in a WG have been saved
        var addNewField = function () {
            var validateField = _vm.newField();
            var screenOrder = 0;

            validateField.MarkSense = _vm.markSense();
            validateField.DEActive = _vm.DEActive();
            validateField.DERequired = _vm.DERequired();

            _vm.deColumnFieldNameError(false);
            if (validateField.FieldName === undefined || validateField.FieldName.length == 0 || !validateField.FieldName.match(_fieldNamePattern)) {
                _vm.deColumnFieldNameErrorText(_vm.labels.RequiredFieldNameFormat);
                _vm.deColumnFieldNameError(true);
            }

            _vm.deColumnUILabelError(false);
            if (validateField.UILabel === undefined || validateField.UILabel.length == 0) {
                _vm.deColumnUILabelErrorText(_vm.labels.RequiredUILabelFormat);
                _vm.deColumnUILabelError(true);
            }

            _vm.deColumnScreenOrderError(false);
            screenOrder = _me.validateInt(validateField.ScreenOrder, _tinyIntPattern, _vm.deColumnScreenOrderError, false);
            if (_vm.deColumnScreenOrderError() !== true && (screenOrder < 0 || screenOrder > 255)) {
                _vm.deColumnScreenOrderError(true);
            }

            _vm.deColumnDocTypeError(validateField.DocumentType === undefined || validateField.DocumentType.length == 0);
            _vm.deColumnDataTypeError(validateField.DataType === undefined || validateField.DataType.length == 0);
            _vm.deColumnPaymentSourceError(validateField.DEBatchSourceKey === undefined || validateField.DEBatchSourceKey == 0);

            if (_vm.deColumnFieldNameError() || _vm.deColumnDocTypeError() || _vm.deColumnDataTypeError() ||
                _vm.deColumnScreenOrderError() || _vm.deColumnPaymentSourceError() ||
                _vm.deColumnUILabelError())
                return false;

            var maxId = -1;
            var match = undefined;

            //find the max id and verify the column does not already exist
            //a field is a repeat if bankID, workgroupID, paymentSource
            //fieldName and isCheck fields match
            var match = ko.utils.arrayFirst(_vm.selectedDEColumns, function (item) {
                maxId = item.DataEntryColumnKey > maxId ? item.DataEntryColumnKey : maxId;

                return (validateField.DEBatchSourceKey === item.BatchSourceKey
                    && RegExp('^' + item.FieldName + '$', "i").test(validateField.FieldName)
                    && validateField.DocumentType === item.TableType) ||
                    (validateField.DEBatchSourceKey == item.BatchSourceKey && validateField.UILabel === item.UILabel && item.DataType != validateField.DataType);
            });

            if (!match) {
                var batchSourceShortNames = $.grep(_vm.sources, function (e) { return e.BatchSourceKey === validateField.DEBatchSourceKey; });
                var addField = {
                    Id: maxId + 1,
                    DataEntryColumnKey: 0,
                    FieldName: validateField.FieldName,
                    DisplayName: validateField.DisplayName,
                    TableType: validateField.DocumentType,
                    DataType: validateField.DataType,
                    ScreenOrder: screenOrder,
                    HubCreated: true,
                    MarkSense: validateField.MarkSense,
                    IsActive: validateField.DEActive,
                    IsRequired: validateField.DERequired,
                    UILabel: validateField.UILabel,
                    BatchSourceKey: validateField.DEBatchSourceKey,
                    BatchSourceShortName: batchSourceShortNames.length > 0 ? batchSourceShortNames[0].ShortName : "",
                    PaymentSource: validateField.DEPaymentType,
                };
                _vm['fieldGrid'].addItem(addField);

                updateRulesFieldSelect(true);

                return true;
            }
            if (validateField.DEBatchSourceKey === match.BatchSourceKey
                && RegExp('^' + match.FieldName + '$', "i").test(validateField.FieldName)
                && validateField.DocumentType === match.TableType) {
                _vm.deColumnFieldNameErrorText(_vm.labels.DuplicateFieldName);
                _vm.deColumnFieldNameError(true);
            }
            else if (validateField.DEBatchSourceKey == match.BatchSourceKey && validateField.UILabel === match.UILabel && match.DataType != validateField.DataType) {
                _vm.deColumnUILabelErrorText(_vm.labels.DuplicateUILabel.replace('@param1', match.DataType));
                _vm.deColumnUILabelError(true);
            }

            return false;
        };

        var editDEField = function () {
            var validateField = _vm.newField();
            validateField.MarkSense = _vm.markSense();
            validateField.DEActive = _vm.DEActive();
            validateField.DERequired = _vm.DERequired();
            //validation
            screenOrder = _me.validateInt(validateField.ScreenOrder, _tinyIntPattern, _vm.deColumnScreenOrderError, false);
            _vm.deColumnFieldNameError(false);
            if (validateField.FieldName === undefined || validateField.FieldName.length == 0 || !validateField.FieldName.match(_fieldNamePattern)) {
                _vm.deColumnFieldNameErrorText(_vm.labels.RequiredFieldNameFormat);
                _vm.deColumnFieldNameError(true);
            }
            _vm.deColumnScreenOrderError(false);
            screenOrder = _me.validateInt(validateField.ScreenOrder, _tinyIntPattern, _vm.deColumnScreenOrderError, false);
            if (_vm.deColumnScreenOrderError() !== true && (screenOrder < 0 || screenOrder > 255)) {
                _vm.deColumnScreenOrderError(true);
            }

            _vm.deColumnDocTypeError(validateField.DocumentType === undefined || validateField.DocumentType.length == 0);
            _vm.deColumnDataTypeError(validateField.DataType === undefined || validateField.DataType.length == 0);
            _vm.deColumnPaymentSourceError(validateField.DEBatchSourceKey === undefined || validateField.DEBatchSourceKey == 0);

            if (_vm.deColumnFieldNameError() || _vm.deColumnDocTypeError() || _vm.deColumnDataTypeError() ||
                _vm.deColumnScreenOrderError() || _vm.deColumnPaymentSourceError())
                return false;
            //makes sure no repeated UI Labels with different data type are allowed
            var validation = true;
            $.each(_vm.selectedDEColumns, function (index, item) {
                if (validateField.DEBatchSourceKey == item.BatchSourceKey && validateField.UILabel === item.UILabel
                    && item.DataType != validateField.DataType) {
                    _vm.deColumnUILabelErrorText(_vm.labels.DuplicateUILabel.replace('@param1', item.DataType));
                    _vm.deColumnUILabelError(true);
                    validation = false;
                    return validation;
                }
            });
            if (!validation) return false;
            //end validation

            var updateField = {
                DataEntryColumnKey: validateField.DataEntryColumnKey,
                FieldName: validateField.FieldName,
                DisplayName: validateField.DisplayName,
                TableType: validateField.DocumentType,
                DataType: validateField.DataType,
                ScreenOrder: screenOrder,
                HubCreated: true,
                MarkSense: validateField.MarkSense,
                IsActive: validateField.DEActive,
                IsRequired: validateField.DERequired,
                UILabel: validateField.UILabel,
                BatchSourceKey: validateField.DEBatchSourceKey,
                BatchSourceShortName: validateField.BatchSourceShortName,
                PaymentSource: validateField.DEPaymentType,
                Id: _vm.DEFieldKeyEdit()
            };
            _vm['fieldGrid'].updateItems([updateField], _vm.DEFieldKeyEdit());
            if (_vm.hideInactive()) {
                $.each(_vm.selectedDEColumns, function (index, item) {
                    if (item.Id === _vm.DEFieldKeyEdit()) {
                        _vm.selectedDEColumns[index] = updateField;
                    }
                });
            }
            updateRulesFieldSelect(true);
            return true;

        };

        //Adds DE Fields from template list to the local DE fields
        var addTemplateFields = function (template) {
            var validateField = _vm.newField();
            //can't add a null or default template (shouldn't happen)
            if (template === null || template.Default === true) {
                return false;
            }
            //validate the new batch source
            _vm.deACHSourceError(validateField.DEBatchSourceKey === undefined || validateField.DEBatchSourceKey == 0);
            if (_vm.deACHSourceError()) {
                return false;
            }

            //now merge the template's fields onto the workgroups....
            var addList = [];
            var maxId = -1;
            $.each(template.DataEntryColumns, function (index, column) {
                var match = ko.utils.arrayFirst(_vm.selectedDEColumns, function (item) {
                    maxId = item.Id > maxId ? item.Id : maxId;

                    return validateField.DEBatchSourceKey === item.BatchSourceKey
                        && RegExp('^' + item.FieldName + '$', "i").test(column.FieldName)
                        && column.TableType === item.TableType;
                });

                if (match === undefined || match === null) {
                    addList.push(column);
                }

            });

            if (addList.length > 0) {
                $.each(addList, function (index, column) {
                    maxId = maxId + 1;
                    var addField = {
                        Id: maxId,  //id for grid
                        DataEntryColumnKey: 0,  //id needs to be 0 for new DE field
                        FieldName: column.FieldName,
                        DisplayName: column.DisplayName,
                        TableType: column.TableType,
                        DataType: column.DataType,
                        ScreenOrder: column.ScreenOrder,
                        HubCreated: true,
                        MarkSense: false,
                        IsActive: true,
                        UILabel: column.UILabel,
                        BatchSourceKey: validateField.DEBatchSourceKey,
                        PaymentSource: validateField.DEPaymentType,
                    };
                    _vm.selectedDEColumns.push(addField);
                });

                _vm['fieldGrid'].setItems(_vm.selectedDEColumns);

                updateRulesFieldSelect(true);
                return true;
            }
            else {
                framework.errorToast(_container, _vm.labels.DEFieldsDataSourceExists.replace("@templateName", template.Description).replace("@dataSource", validateField.DEPaymentType));
                return false;
            }

        };

        this.validateGeneralTab = function (selectedWorkgroup, updatedWorkgroup) {
            _vm.longNameError(_vm.longName() === null || _vm.longName().length == 0 || !_vm.longName().match(_namePattern));
            _vm.shortNameError(selectedWorkgroup.ShortName === null || selectedWorkgroup.ShortName.length == 0 || !selectedWorkgroup.ShortName.match(_namePattern));

            //handle validation of the file group
            if (selectedWorkgroup.FileGroup !== null && selectedWorkgroup.FileGroup.length > 0) {
                _vm.fileGroupError(!selectedWorkgroup.FileGroup.match(_fileGroupPattern));

                //TODO: Must also verify that it does not include invalid file name characters (or is this good enough?)
            }
            else {
                _vm.fileGroupError(false);
            }

            var bank = null;
            _vm.workgroupIDError(false);
            if (!_vm.updatingExisting) {
                bank = _vm['bankSelect'].wfsSelectbox('getData');
                _vm.bankError(bank === null);

                selectedWorkgroup.SiteClientAccountID = _me.validateInt(_vm.workgroupID(), _intPattern, _vm.workgroupIDError, false);
                if (!_vm.workgroupIDError() && selectedWorkgroup.SiteClientAccountID > 2147483647) {
                    _vm.workgroupIDError(true);
                };

                if (_vm.workgroupIDError())
                    _vm.workgroupIDErrorText(_vm.labels.RequiredWorkgroupIDFormat);
            }

            //handle validation of the retention days

            _vm.dataRetentionDaysError(false);
            _vm.imageRetentionDaysError(false);
            _vm.viewingDaysError(false);
            _vm.maximumSearchDaysError(false);

            var dataRetentionDays = _me.validateInt(selectedWorkgroup.DataRetentionDays, _shortNotZeroPattern, _vm.dataRetentionDaysError, true);
            var imageRetentionDays = _me.validateInt(selectedWorkgroup.ImageRetentionDays, _shortNotZeroPattern, _vm.imageRetentionDaysError, true);
            var viewingDays = _me.validateInt(selectedWorkgroup.ViewingDays, _shortPattern, _vm.viewingDaysError, true);
            var maximumSearchDays = _me.validateInt(selectedWorkgroup.MaximumSearchDays, _shortPattern, _vm.maximumSearchDaysError, true);

            if (_vm.dataRetentionDaysError())
                _vm.dataRetentionDaysErrorText(_vm.labels.DaysFormatNonZero);
            else if (dataRetentionDays > 0) {
                //validate against system maximum
                if (dataRetentionDays > _vm.maxRetentionDays) {
                    _vm.dataRetentionDaysError(true);
                    _vm.dataRetentionDaysErrorText(_vm.labels.DaysLessThanSysMax);
                }
            }

            if (_vm.imageRetentionDaysError())
                _vm.imageRetentionDaysErrorText(_vm.labels.DaysFormatNonZero);
            else if (imageRetentionDays > 0) {
                if (dataRetentionDays > 0 && imageRetentionDays > dataRetentionDays) {
                    _vm.imageRetentionDaysErrorText(_vm.labels.DaysLessThanDataDays);
                    _vm.imageRetentionDaysError(true);
                }
                //validate against system maximum
                else if (imageRetentionDays > _vm.maxRetentionDays) {
                    _vm.imageRetentionDaysError(true);
                    _vm.imageRetentionDaysErrorText(_vm.labels.DaysLessThanSysMax);
                }
            }

            if (_vm.viewingDaysError())
                _vm.viewingDaysErrorText(_vm.labels.DaysFormat);
            else if (viewingDays > 0) {
                if (dataRetentionDays > 0 && viewingDays > dataRetentionDays) {
                    _vm.viewingDaysErrorText(_vm.labels.DaysLessThanDataDays);
                    _vm.viewingDaysError(true);
                }
                //validate against system maximum
                else if (viewingDays > _vm.maxRetentionDays) {
                    _vm.viewingDaysError(true);
                    _vm.viewingDaysErrorText(_vm.labels.DaysLessThanSysMax);
                }
            }

            if (_vm.maximumSearchDaysError())
                _vm.maximumSearchDaysErrorText(_vm.labels.DaysFormat);
            else if (maximumSearchDays > 0) {
                if (dataRetentionDays > 0 && maximumSearchDays > dataRetentionDays) {
                    _vm.maximumSearchDaysErrorText(_vm.labels.DaysLessThanDataDays);
                    _vm.maximumSearchDaysError(true);
                }
                //validate against system maximum
                else if (maximumSearchDays > _vm.maxSearchDays) {
                    _vm.maximumSearchDaysError(true);
                    _vm.maximumSearchDaysErrorText(_vm.labels.DaysLessThanSearchMax);
                }
            }

            if (_vm.shortNameError() || _vm.longNameError() || _vm.bankError() || _vm.workgroupIDError() || _vm.dataRetentionDaysError() || _vm.imageRetentionDaysError() || _vm.fileGroupError() ||
                _vm.viewingDaysError() || _vm.maximumSearchDaysError()) {
                return false;
            }

            var displayBatchIDSelValue = _vm['displayBatchIDSelect'].wfsSelectbox('getData');
            displayBatchIDSelValue = displayBatchIDSelValue.id === 1 ? true : displayBatchIDSelValue.id === 0 ? false : null;

            updatedWorkgroup.ClientAccountKey = _vm.updatingExisting ? selectedWorkgroup.ClientAccountKey : 0;
            updatedWorkgroup.SiteBankID = _vm.updatingExisting ? selectedWorkgroup.SiteBankID : bank.SiteBankID;
            updatedWorkgroup.SiteClientAccountID = selectedWorkgroup.SiteClientAccountID;
            updatedWorkgroup.ShortName = selectedWorkgroup.ShortName;
            updatedWorkgroup.LongName = _vm.longName();
            updatedWorkgroup.IsActive = selectedWorkgroup.IsActive;
            updatedWorkgroup.IsRequired = selectedWorkgroup.IsRequired;
            updatedWorkgroup.DataRetentionDays = dataRetentionDays;
            updatedWorkgroup.ImageRetentionDays = imageRetentionDays;
            updatedWorkgroup.FileGroup = selectedWorkgroup.FileGroup;
            updatedWorkgroup.ViewingDays = viewingDays;
            updatedWorkgroup.MaximumSearchDays = maximumSearchDays;
            updatedWorkgroup.CheckImageDisplayMode = _vm['checkImageDisplayModeSelect'].wfsSelectbox('getValue');
            updatedWorkgroup.DocumentImageDisplayMode = _vm['documentImageDisplayModeSelect'].wfsSelectbox('getValue');
            updatedWorkgroup.HOA = selectedWorkgroup.HOA;
            updatedWorkgroup.BusinessRules = {
                InvoiceRequired: selectedWorkgroup.BusinessRules.InvoiceRequired,
                PreDepositBalancingRequired: selectedWorkgroup.BusinessRules.PreDepositBalancingRequired,
                PostDepositBalancingRequired: selectedWorkgroup.BusinessRules.PostDepositBalancingRequired,
                PostDepositPayerRequired: selectedWorkgroup.BusinessRules.PostDepositPayerRequired
            };
            updatedWorkgroup.DisplayBatchID = displayBatchIDSelValue;

            updatedWorkgroup.EntityID = selectedWorkgroup.EntityID;

            //all is well for the general tab
            return true;
        };

        this.validateDDATab = function (selectedWorkgroup, updatedWorkgroup) {
            var ddas = _vm.selectedDDAs;

            updatedWorkgroup.DDAs = [];
            $.each(ddas, function (index, item) {
                updatedWorkgroup.DDAs.push({ RT: item.RT, DDA: item.DDA });
            });

            //all is well for the DDA tab
            return true;
        };

        this.validateRulesTab = function (selectedWOrkgroup, updatedWorkgroup) {
            //var exceptionDeadline = null;
            //_vm.exceptionDeadlineError(false);
            //if (_vm.exceptionDeadline() !== null && _vm.exceptionDeadline().length > 0) {
            //    if (!_vm.exceptionDeadline().match(_timePattern)) {
            //        _vm.exceptionDeadlineError(true);
            //    }
            //    exceptionDeadline = parseInt(_vm.exceptionDeadline().replace(":", ""), 10);
            //}

            //if (_vm.exceptionDeadlineError()) {
            //    return false;
            //}

            //updatedWorkgroup.DataEntrySetup = { DeadLineDay: exceptionDeadline, RequiresInvoice: _vm.requiresInvoice() };
            //updatedWorkgroup.InvoiceBalancing = _vm['postProcessBalancingSelect'].wfsSelectbox('getValue');
            return true;
        };

        this.validateBillingTab = function (selectedWorkgroup, updatedWorkgroup) {
            _vm.billingAccountError(_vm.billingAccount() && _vm.billingAccount().length > 0 && !_vm.billingAccount().match(_namePattern));
            _vm.billingField1Error(_vm.billingField1() && _vm.billingField1().length > 0 && !_vm.billingField1().match(_namePattern));
            _vm.billingField2Error(_vm.billingField2() && _vm.billingField2().length > 0 && !_vm.billingField2().match(_namePattern));

            if (_vm.billingAccountError() || _vm.billingField1Error() || _vm.billingField2Error())
                return false;

            updatedWorkgroup.BillingAccount = _vm.billingAccount();
            updatedWorkgroup.BillingField1 = _vm.billingField1();
            updatedWorkgroup.BillingField2 = _vm.billingField2();
            return true;
        };

        this.saveComplete = function (jsonResult) {
            framework.hideSpinner();

            if (!jsonResult.HasErrors) {
                if (jsonResult.InfoMessage !== undefined && jsonResult.InfoMessage !== null && jsonResult.InfoMessage.length > 0)
                    framework.noticeToast(_container, jsonResult.InfoMessage, { sticky: true });

                framework.closeModal(jsonResult.Data.ClientAccountKey);
            }

            else {
                //display errors
                framework.errorToast(_container, framework.formatErrors(jsonResult.Errors));
            }
        };

        this.saveOnClick = function () {
            framework.showSpinner(_vm.labels.Verifying);

            var selectedWorkgroup = _vm.selectedWorkgroup();
            var workgroup = {};

            workgroup.EntityID = selectedWorkgroup.EntityID;
            workgroup.EntityName = selectedWorkgroup.EntityName;
            if (!_me.validateGeneralTab(selectedWorkgroup, workgroup)) {
                $getE('#tabGeneral').tab('show');
                framework.hideSpinner();
                return false;
            }

            if (!_me.validateDDATab(selectedWorkgroup, workgroup)) {
                $getE('#tabDDA').tab('show');
                framework.hideSpinner();
                return false;
            }

            if (!_me.validateRulesTab(selectedWorkgroup, workgroup)) {
                $getE('#tabRules').tab('show');
                framework.hideSpinner();
                return false;
            }

            if (!_me.validateBillingTab(selectedWorkgroup, workgroup)) {
                $getE('#tabBilling').tab('show');
                framework.hideSpinner();
                return false;
            }

            //nothing to validate on the field setup tab
            workgroup.DataEntryColumns = _vm.selectedDEColumns;
            //workgroup.DataEntrySetup.BusinessRules = _vm.fieldRules;

            framework.setSpinnerText(_vm.labels.Saving);
            framework.doJSON("/RecHubConfigViews/Workgroups/Save", $.toDictionary(workgroup), _me.saveComplete);
            return false;
        };

        var ddaSorter = function (dataView, sortFieldName, ascending) {
            //we have two levels to sort by
            dataView.sort(function (a, b) {
                var sortBy = sortFieldName;
                var result = comparePropertyValues(sortBy, a, b);
                if (result === 0) {
                    if (sortBy == "RT") {
                        sortBy = "DDA";
                    }
                    else {
                        sortBy = "RT";
                    }
                    result = comparePropertyValues(sortBy, a, b);
                }
                return result;
            }, ascending);
        };

        var initDDAGrid = function () {
            if (_vm['ddaGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        //forceFitColumns: false
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#ddaGrid',
                        pagerSelector: '#ddaPager',
                        pageSize: 15,
                        displayPagerSettings: false,
                        data: _vm.selectedDDAs,
                        idFieldName: 'Id',
                        allowRowDelete: _vm.CanManage,
                        rowDeleteToolsip: _vm.labels.DeleteDDATooltip,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        customSorter: ddaSorter,
                        sortField: 'RT',
                        columns: [
                            { Id: 'colRT', ColumnTitle: _vm.labels.RT, ColumnFieldName: 'RT', DataType: 6, width: 60, sortable: true },
                            { Id: 'colDDA', ColumnTitle: _vm.labels.DDA, ColumnFieldName: 'DDA', DataType: 6, width: 150, sortable: true },
                        ],
                    },
                };

                _vm['ddaGrid'] = new WFSGridBase.GridBase();
                _vm['ddaGrid'].init(_container.attr('id'), gridModel);
            }
        }

        var comparePropertyValues = function (sortBy, a, b) {
            return compareValues(a[sortBy], b[sortBy]);
        };

        var compareValues = function (x, y) {
            if (typeof x !== "string" && typeof x === typeof y) {
                return (x == y ? 0 : (x > y ? 1 : -1));
            }
            else {
                //convert them both to strings just in case one was a number and the other was not
                var lowX = !x ? "" : x.toString().toLowerCase();
                var lowY = !y ? "" : y.toString().toLowerCase();
                return (lowX === lowY ? 0 : (lowX > lowY ? 1 : -1));
            }
        };

        var fieldSorter = function (dataView, sortFieldName, ascending) {
            //we have two levels to sort by when sorting by table type
            dataView.sort(function (a, b) {
                var sortBy = sortFieldName;
                var result = comparePropertyValues(sortBy, a, b);
                if (result === 0) {
                    if (sortBy == "TableType") {
                        result = comparePropertyValues("ScreenOrder", a, b);
                    }
                }
                return result;
            }, ascending);
        };

        var initFieldGrid = function () {
            if (_vm['fieldGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#fieldGrid',
                        pagerSelector: '#fieldPager',
                        pageSize: 15,
                        displayPagerSettings: false,
                        data: _vm.selectedDEColumns,
                        idFieldName: 'Id',
                        allowRowDelete: false,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        customSorter: fieldSorter,
                        sortField: 'UILabel',
                        allowShowDetail: true,
                        showDetailTooltip: _vm.labels.EditDEFieldToolTip,
                        showDetailCallback: editSelectedDEField,
                        columns: [
                            { Id: 'colUILabel', ColumnTitle: _vm.labels.UILabel, ColumnFieldName: 'UILabel', Tooltip: _vm.labels.FieldNameTooltip, DataType: 1, width: 100, sortable: true },
                            { Id: 'colPaymentSource', ColumnTitle: _vm.labels.PaymentSource, ColumnFieldName: 'PaymentSource', Tooltip: _vm.labels.FieldNameTooltip, DataType: 1, width: 120, sortable: true },
                            { Id: 'colTableType', ColumnTitle: _vm.labels.DocumentType, ColumnFieldName: 'TableType', Tooltip: _vm.labels.DocumentTypeTooltip, DataType: 1, width: 70, sortable: true },
                            { Id: 'colScreenOrder', ColumnTitle: _vm.labels.ScreenOrder, ColumnFieldName: 'ScreenOrder', Tooltip: _vm.labels.ScreenOrderTooltip, DataType: 6, width: 100, sortable: true },
                            { Id: 'colFieldName', ColumnTitle: _vm.labels.FieldName, ColumnFieldName: 'FieldName', Tooltip: _vm.labels.FieldNameTooltip, DataType: 1, width: 130, sortable: true },
                            { Id: 'colDisplayName', ColumnTitle: _vm.labels.DisplayName, ColumnFieldName: 'DisplayName', Tooltip: _vm.labels.DisplayNameTooltip, DataType: 1, width: 130, sortable: true },
                            { Id: 'colDataType', ColumnTitle: _vm.labels.DataType, ColumnFieldName: 'DataType', Tooltip: _vm.labels.DataTypeTooltip, DataType: 1, width: 70, sortable: true },
                            { Id: 'colMarkSense', ColumnTitle: _vm.labels.MarkSense, ColumnFieldName: 'MarkSense', Tooltip: _vm.labels.MarkSenseTooltip, DataType: 20, width: 80, sortable: true },
                            { Id: 'colKey', ColumnTitle: _vm.labels.Key, ColumnFieldName: 'DataEntryColumnKey', Tooltip: _vm.labels.KeyTooltip, DataType: 6, width: 50, sortable: true },
                            { Id: 'colActive', ColumnTitle: _vm.labels.Active, ColumnFieldName: 'IsActive', Tooltip: _vm.labels.FieldNameTooltip, DataType: 20, width: 50, sortable: true },
                        ],
                    },
                };

                _vm['fieldGrid'] = new WFSGridBase.GridBase();
                _vm['fieldGrid'].init(_container.attr('id'), gridModel);
            };
        }

        var editSelectedDEField = function (gridRow, col, dataIndex, item) {
            _vm.editingField(true); //displays the editing pane
            _vm.DEFieldKeyEdit(item.Id); //
            _vm.newField({
                DisplayName: item.DisplayName,
                FieldName: item.FieldName,
                UILabel: item.UILabel,
                DocumentType: item.TableType,
                ScreenOrder: item.ScreenOrder,
                DataEntryColumnKey: item.DataEntryColumnKey,
                BatchSourceShortName: item.BatchSourceShortName,
                IsIntegraPAY: item.BatchSourceShortName.toLowerCase().indexOf('integrapay') > -1
            });

            _vm['docTypeSelect'].wfsSelectbox('setValue', item.TableType)
            _vm['dataTypeSelect'].wfsSelectbox('setValue', item.DataType);
            _vm['paymentSourceSelect'].wfsSelectbox('setValue', item.BatchSourceKey);
            _vm.markSense(item.MarkSense);
            _vm.DEActive(item.IsActive);
            _vm.DERequired(item.IsRequired);
        }

        var updateRulesFieldSelect = function (refreshFields, select) {
            if (select === undefined) {
                select = _vm['fieldSelect'].wfsSelectbox('getData');
            }
            if (refreshFields === true) {
                _vm.distinctDEFieldNames = getDistinctFieldNames(_vm.selectedDEColumns);
                _vm['fieldSelect'].wfsSelectbox('setItems', _vm.distinctDEFieldNames);

            }
            if (select !== null) {
                select = ko.utils.arrayFirst(_vm.distinctDEFieldNames, function (e) {
                    return e.TableType === select.TableType && e.FieldName === select.FieldName;
                });
                _vm['fieldSelect'].wfsSelectbox('setValueByData', select);
            }
        };


        var getDistinctFieldNames = function (deColumns) {
            var distinctFields = [];
            var curId = 0;
            $.each(deColumns, function (index, deColumn) {
                var match = ko.utils.arrayFirst(distinctFields, function (field) {
                    return deColumn.TableType === field.TableType && deColumn.FieldName === field.FieldName;
                });
                if (match === null) {
                    distinctFields.push({ TableType: deColumn.TableType, FieldName: deColumn.FieldName, id: curId });
                    curId++;
                }
            });
            distinctFields.sort(function (a, b) {
                var x = a.TableType + " - " + a.FieldName;
                var y = b.TableType + " - " + b.FieldName;
                return (x.toLowerCase() === y.toLowerCase() ? 0 : (x.toLowerCase() > y.toLowerCase() ? 1 : -1));
            });
            return distinctFields;
        };

        var deleteSelectedRule = function (gridRow, col, dataIndex, item) {
            if (_vm.currentRule.Id === item.Id)
                endEditingBusinessRule();

            _vm['ruleGrid'].deleteItem(item, gridRow);
        };

        var editSelectedRule = function (gridRow, col, dataIndex, item) {
            item['AddingNew'] = false;
            editBusinessRule(item);
            _vm['ruleGrid'].selectItem(item.Id);
        };

        var clearAppliesToErrors = function () {
            _vm.ruleFieldError(false);
        };

        var clearGeneralErrors = function () {
            _vm.ruleRequiredMessageError(false);
            _vm.exceptionDisplayOrderError(false);
            _vm.ruleLengthError(false);
            _vm.dateMaskError(false);
            _vm.maskError(false);
        };

        var clearCheckDigitErrors = function () {
            _vm.cdModulusError(false);
            _vm.cdComplimentError(false);
            _vm.cdRem10Error(false);
            _vm.cdRem11Error(false);
            _vm.cdOffsetError(false);
            _vm.cdWeightDirectionError(false);
            _vm.cdWeightPatternError(false);
            _vm.cdMessageError(false);
            _vm.cdReplacementError(false);
        };

        var clearValidValuesErrors = function () {
            _vm.vtMessageError(false);
            _vm.validationValueError(false);
        };

        var clearBusinessRuleErrors = function () {
            clearAppliesToErrors();
            clearGeneralErrors();
            clearCheckDigitErrors();
            clearValidValuesErrors();
        };

        var editBusinessRule = function (editRule) {
            //make a copy of the rule for us to work on...
            var resetCDRoutine = false;
            var resetFV = false;
            if (editRule.CheckDigitRoutine === null) {
                editRule.CheckDigitRoutine = undefined;
                resetCDRoutine = true;
            }
            if (editRule.FieldValidations === null) {
                editRule.FieldValidations = undefined;
                resetFV = true;
            }

            var rule = $.extend(true, {}, _vm.defaultRule, editRule);

            ko.mapping.fromJS(rule, _vm.currentRule);

            if (resetCDRoutine)
                editRule.CheckDigitRoutine = null;
            if (resetFV)
                editRule.FieldValidations = null;

            _vm.originalValue(null);
            _vm.replaceValue(null);

            if (_vm.currentRule.MaxLength() == 0) {
                if (_vm.currentRule.MinLength() == 0)
                    _vm.currentRule.MinLength("");
                _vm.currentRule.MaxLength("");
            }

            clearBusinessRuleErrors();

            updateRulesFieldSelect(false, rule);
            _vm["fieldSelect"].wfsSelectbox("setEnabled", _vm.CanManage && rule.AddingNew === true);


            var sourceMatch = ko.utils.arrayFirst(_vm.sources, function (item) {
                return rule.BatchSourceKey === item.BatchSourceKey;
            });
            if (!sourceMatch)
                sourceMatch = _vm.defaultSource;
            _vm['sourceSelect'].wfsSelectbox('setValue', sourceMatch.ID);

            var typeMatch = ko.utils.arrayFirst(_vm.paymentTypes, function (item) {
                return rule.BatchPaymentTypeKey === item.ID;
            });
            if (!typeMatch)
                typeMatch = _vm.defaultPaymentType;
            _vm['typeSelect'].wfsSelectbox('setValue', typeMatch.ID);

            var subTypeMatch = null;
            if (_vm.paymentSubTypes !== null) {
                subTypeMatch = ko.utils.arrayFirst(_vm.paymentSubTypes, function (item) {
                    return rule.BatchPaymentSubTypeKey === item.BatchPaymentSubTypeKey;
                });
            }
            if (subTypeMatch === null)
                subTypeMatch = _vm.defaultPaymentSubType;
            _vm['subTypeSelect'].wfsSelectbox('setValue', subTypeMatch.BatchPaymentSubTypeKey);

            var formatTypeMatch = ko.utils.arrayFirst(_vm.formatTypes, function (item) {
                return rule.FormatType === item.text;
            });
            _vm['formatTypeSelect'].wfsSelectbox('setValue', formatTypeMatch === null || formatTypeMatch === undefined ? "Any" : formatTypeMatch.id);

            _vm['cdMethodSelect'].wfsSelectbox('setValue', rule.CheckDigitRoutine ? rule.CheckDigitRoutine.Method : null);
            _vm['cdWeightDirectionSelect'].wfsSelectbox('setValue', rule.CheckDigitRoutine ? rule.CheckDigitRoutine.WeightsDirection : null);

            //create a copy of the check digit replacement value array
            _vm.currentReplacementValues = rule.CheckDigitRoutine.ReplacementValues ? rule.CheckDigitRoutine.ReplacementValues.slice(0) : [];
            if (_vm['replacementGrid']) {
                _vm['replacementGrid'].setItems(_vm.currentReplacementValues);
            }
            if (_vm.checkDigitPanelVisible()) {
                if (_vm.checkDigitMenuVisible()) {
                    resizeGrid('replacementGrid');
                }
                else {
                    _vm.selectedRulePanel('appliesToPanel');
                }
            }

            _vm['vtTypeSelect'].wfsSelectbox('setValue', rule.FieldValidations.ValidationTypeKey);

            //create a copy of the field validations values
            _vm.currentValidationValues = [];
            if (rule.FieldValidations.FieldValues) {
                for (var i = 0; i < rule.FieldValidations.FieldValues.length; i++) {
                    _vm.currentValidationValues.push({ Value: rule.FieldValidations.FieldValues[i] });
                }
            }
            if (_vm['validValuesGrid']) {
                _vm['validValuesGrid'].setItems(_vm.currentValidationValues);
            }
            if (_vm.validValuesPanelVisible()) {
                if (_vm.validationValuesMenuVisible()) {
                    resizeGrid('validValuesGrid');
                }
                else {
                    _vm.selectedRulePanel('appliesToPanel');
                }
            }

            _vm.editingRule(true);
        };

        var endEditingBusinessRule = function () {
            ko.mapping.fromJS(_vm.defaultRule, _vm.currentRule);
            _vm.originalValue(null);
            _vm.replaceValue(null);
            _vm.currentReplacementValues = [];
            _vm.currentValidationValues = [];
            _vm.editingRule(false);
        };

        var removeAll = function (original, remove) {
            return original.split(remove).join("");
        };

        var validateCustomDateFormat = function (mask) {
            if (mask === undefined || mask === null || mask.length === 0) {
                _vm.maskErrorText(_vm.labels.CustomDateMaskRequired);
                _vm.maskError(true);
                return;
            }

            var checkMask = mask.toLowerCase();
            //we do not care what case they enter the date in at this point
            // we will validate there is...
            // one or two Ms
            // one or two Ds
            // one, two or four Ys
            // zero or one distinct symbol
            // no leading or trailing symbol
            // no sequential symbols 

            var replacedMask = removeAll(checkMask, "m");
            var lenDiff = checkMask.length - replacedMask.length;
            if (lenDiff !== 1 && lenDiff !== 2) {
                _vm.maskErrorText(_vm.labels.CustomDateContainsInvalidMonth);
                _vm.maskError(true);
                return;
            }

            checkMask = replacedMask;
            replacedMask = removeAll(checkMask, "d");
            lenDiff = checkMask.length - replacedMask.length;
            if (lenDiff !== 1 && lenDiff !== 2) {
                _vm.maskErrorText(_vm.labels.CustomDateContainsInvalidDay);
                _vm.maskError(true);
                return;
            }

            checkMask = replacedMask;
            replacedMask = removeAll(checkMask, "y");
            lenDiff = checkMask.length - replacedMask.length;
            if (lenDiff !== 1 && lenDiff !== 2 && lenDiff !== 4) {
                _vm.maskErrorText(_vm.labels.CustomDateContainsInvalidYear);
                _vm.maskError(true);
                return;
            }

            //we should only have the symbols left now
            var separator = undefined;
            checkMask = mask.toLowerCase();
            for (var i = 0; i < checkMask.length; i++) {
                if (checkMask[i] !== "m" && checkMask[i] !== "d" && checkMask[i] !== "y") {
                    if (_validateDateSymbols.indexOf(checkMask[i]) < 0) {
                        _vm.maskErrorText(_vm.labels.CustomDateContainsInvalidSeparator);
                        _vm.maskError(true);
                        return;
                    }

                    if (separator === undefined) {
                        separator = checkMask[i];
                    }
                    else if (checkMask[i] !== separator) {
                        _vm.maskErrorText(_vm.labels.CustomDateContainsMultipleSeparator);
                        _vm.maskError(true);
                        return;
                    }
                    else if (i > 0 && checkMask[i] === checkMask[i - 1]) {
                        _vm.maskErrorText(_vm.labels.CustomDateContainsSequentialSeparators);
                        _vm.maskError(true);
                        return;
                    }
                }
            }

            //if we have a separator, make sure it's not the first or last
            if (separator !== undefined) {
                if (checkMask[0] === separator || checkMask[checkMask.length - 1] === separator) {
                    _vm.maskErrorText(_vm.labels.CustomDateContainsLeadingOrTrailingSeparator);
                    _vm.maskError(true);
                    return;
                }
            }
        };


        this.addRuleOnClick = function (e) {
            e.preventDefault();

            var rule = ko.mapping.toJS(_vm.currentRule);

            var selectedField = _vm["fieldSelect"].wfsSelectbox("getData");

            //validate another rule does not exist for the field, source, type and subtype
            _vm.ruleFieldError(false);
            if (selectedField === null) {
                _vm.selectedRulePanel('appliesToPanel');
                _vm.ruleFieldErrorText(_vm.labels.SelectionRequired);
                _vm.ruleFieldError(true);
                return false;
            }
            rule.TableType = selectedField.TableType;
            rule.FieldName = selectedField.FieldName;

            var selectedVal = _vm['sourceSelect'].wfsSelectbox('getData');
            rule.BatchSourceKey = selectedVal.Default ? null : selectedVal.ID;

            selectedVal = _vm['typeSelect'].wfsSelectbox('getData');
            rule.BatchPaymentTypeKey = selectedVal.Default ? null : selectedVal.ID;

            var subTypeKey = null;
            if (selectedVal.HasSubTypes) {
                selectedVal = _vm['subTypeSelect'].wfsSelectbox('getData');
                subTypeKey = selectedVal.Default ? null : selectedVal.BatchPaymentSubTypeKey;
            }
            rule.BatchPaymentSubTypeKey = subTypeKey;

            var maxId = -1;
            var match = ko.utils.arrayFirst(_vm.fieldRules, function (item) {
                maxId = item.Id > maxId ? item.Id : maxId;
                return rule.TableType === item.TableType && rule.FieldName === item.FieldName && rule.BatchSourceKey === item.BatchSourceKey &&
                    rule.BatchPaymentTypeKey == item.BatchPaymentTypeKey && rule.BatchPaymentSubTypeKey == item.BatchPaymentSubTypeKey && rule.Id != item.Id;
            });

            if (match) {
                _vm.selectedRulePanel('appliesToPanel');
                _vm.ruleFieldErrorText(_vm.labels.DuplicateRule);
                _vm.ruleFieldError(true);
                return false;
            }

            if (rule.AddingNew)
                rule['Id'] = maxId + 1;

            //validate the required message
            if (!rule.Required) {
                rule.RequiredMessage = null;
            }
            _vm.ruleRequiredMessageError(rule.RequiredMessage !== null && rule.RequiredMessage.length > 0 && !rule.RequiredMessage.match(_namePattern));

            //validate the exception display order
            //rule.ExceptionDisplayOrder = _me.validateInt(rule.ExceptionDisplayOrder, _tinyIntPattern, _vm.exceptionDisplayOrderError, true);
            //if (_vm.exceptionDisplayOrderError() !== true && (rule.ExceptionDisplayOrder < 0 || rule.ExceptionDisplayOrder > 255)) {
            //    _vm.exceptionDisplayOrderError(true);
            //}

            //validate the min and max length
            _vm.ruleLengthError(false);

            //get the length of the data entry columns this corresponds to
            var fieldLengths = [];
            $.each(_vm.selectedDEColumns, function (index, item) {
                if (item.TableType === rule.TableType && item.FieldName === rule.FieldName) {
                    if (ko.utils.arrayFirst(fieldLengths, function (e) { return e === item.FieldLength; }) === null) {
                        fieldLengths.push(item.FieldLength);
                    }
                }
            });
            var selectedFieldLength = fieldLengths.length === 1 ? fieldLengths[0] : null;

            rule.MinLength = _me.validateInt(rule.MinLength, _tinyIntPattern, _vm.ruleLengthError, true);
            if (_vm.ruleLengthError()) {
                _vm.ruleLengthErrorText(_vm.labels.OptionalLengthFormat);
            }
            else if (rule.MinLength !== null) {
                if (rule.MinLength < 0 || rule.MinLength > 255) {
                    _vm.ruleLengthErrorText(_vm.labels.OptionalLengthFormat);
                    _vm.ruleLengthError(true);
                }
                else if (selectedFieldLength !== null && rule.MinLength > selectedFieldLength) {
                    _vm.ruleLengthErrorText(_vm.labels.LengthTooLong);
                    _vm.ruleLengthError(true);
                }
            }

            if (!_vm.ruleLengthError()) {
                rule.MaxLength = _me.validateInt(rule.MaxLength, _tinyIntPattern, _vm.ruleLengthError, true);
                if (_vm.ruleLengthError()) {
                    _vm.ruleLengthErrorText(_vm.labels.OptionalLengthFormat);
                }
                else if (rule.MaxLength !== null) {
                    if (rule.MaxLength < 0 || rule.MaxLength > 255) {
                        _vm.ruleLengthErrorText(_vm.labels.OptionalLengthFormat);
                        _vm.ruleLengthError(true);
                    }
                    else if (selectedFieldLength !== null && rule.MaxLength > selectedFieldLength) {
                        _vm.ruleLengthErrorText(_vm.labels.LengthTooLong);
                        _vm.ruleLengthError(true);
                    }
                    else if (rule.MinLength !== null && rule.MinLength > rule.MaxLength) {
                        _vm.ruleLengthErrorText(_vm.labels.MinLengthLessThanMax);
                        _vm.ruleLengthError(true);
                    }
                }
            }

            _vm.formatTypeError(rule.FormatType === null);
            _vm.dateMaskError(false);
            _vm.maskError(false);
            if (rule.FormatType === "Custom") {
                if (rule.Mask === null || rule.Mask.length === 0) {
                    _vm.maskErrorText(_vm.labels.MaskRequired);
                    _vm.maskError(true);
                }
            }
            else if (rule.FormatType === "Date") {
                selectedVal = _vm["dateMaskSelect"].wfsSelectbox("getData");
                if (selectedVal === null)
                    _vm.dateMaskError(true);
                else {
                    rule.Mask = selectedVal.text;
                }
            }
            else if (rule.FormatType === "Custom Date") {
                validateCustomDateFormat(rule.Mask);
            }

            if (_vm.ruleRequiredMessageError() || _vm.exceptionDisplayOrderError() || _vm.ruleLengthError() || _vm.formatTypeError() || _vm.maskError() || _vm.dateMaskError()) {
                _vm.selectedRulePanel('generalPanel');
                return false;
            }

            //Check Digit Validation
            clearCheckDigitErrors();
            if (!rule.CheckDigitRoutine.Method)
                rule.CheckDigitRoutine = null;//wipe it out
            else {
                //validate it
                rule.CheckDigitRoutine.Modulus = _me.validateInt(rule.CheckDigitRoutine.Modulus, _twoDigitNoZeroPattern, _vm.cdModulusError);
                rule.CheckDigitRoutine.Compliment = _me.validateInt(rule.CheckDigitRoutine.Compliment, _twoDigitPattern, _vm.cdComplimentError);
                rule.CheckDigitRoutine.Rem10Replacement = _me.validateInt(rule.CheckDigitRoutine.Rem10Replacement, _twoDigitPattern, _vm.cdRem10Error);
                rule.CheckDigitRoutine.Rem11Replacement = _me.validateInt(rule.CheckDigitRoutine.Rem11Replacement, _twoDigitPattern, _vm.cdRem11Error);

                rule.CheckDigitRoutine.Offset = _me.validateInt(rule.CheckDigitRoutine.Offset, _threeDigitNegNoZeroPattern, _vm.cdOffsetError);
                if (_vm.cdOffsetError()) {
                    _vm.cdOffsetErrorText(_vm.labels.RequiredFieldLengthFormat);
                }
                else {
                    var absOffset = rule.CheckDigitRoutine.Offset < 0 ? 0 - rule.CheckDigitRoutine.Offset : rule.CheckDigitRoutine.Offset;

                    if (selectedFieldLength !== null && selectedField.FieldLength > 0 && absOffset > selectedField.FieldLength) {
                        _vm.cdOffsetErrorText(_vm.labels.OffsetTooLong);
                        _vm.cdOffsetError(true);
                    }
                    else if (rule.MaxLength > 0 && absOffset > rule.MaxLength) {
                        _vm.cdOffsetErrorText(_vm.labels.OffsetOutsideValidLength);
                        _vm.cdOffsetError(true);
                    }
                }

                selectedVal = _vm['cdWeightDirectionSelect'].wfsSelectbox('getData');
                rule.CheckDigitRoutine.WeightsDirection = !selectedVal ? null : selectedVal.text;
                _vm.cdWeightDirectionError(rule.CheckDigitRoutine.WeightsDirection === null);

                _vm.cdWeightPatternError(rule.CheckDigitRoutine.Weights === null || !rule.CheckDigitRoutine.Weights.match(_commaSeparatedNumbersPattern));

                _vm.cdMessageError(rule.CheckDigitRoutine.FailureMessage !== null && rule.CheckDigitRoutine.FailureMessage.length > 0 && !rule.CheckDigitRoutine.FailureMessage.match(_namePattern));

                if (_vm.cdModulusError() || _vm.cdComplimentError() || _vm.cdRem10Error() || _vm.cdRem11Error()
                    || _vm.cdOffsetError() || _vm.cdWeightDirectionError() || _vm.cdWeightPatternError() || _vm.cdMessageError()) {
                    _vm.selectedRulePanel('checkDigitPanel');
                    resizeGrid('ruleGrid', true);
                    return false;
                }

                //copy the replacement values to the rule we'll be saving...
                rule.CheckDigitRoutine.ReplacementValues = _vm.currentReplacementValues.splice(0);
            }

            if (rule.FieldValidations.ValidationTypeKey === null)
                rule.FieldValidations = null;
            else {
                //validate it
                _vm.vtMessageError(rule.FieldValidations.FailureMessage !== null && rule.FieldValidations.FailureMessage.length > 0 && !rule.FieldValidations.FailureMessage.match(_namePattern));

                if (_vm.vtMessageError()) {
                    _vm.selectedRulePanel('validValuesPanel');
                    resizeGrid('ruleGrid', true);
                    return false;
                }

                //copy the validation values to the field validations we'll be saving...
                rule.FieldValidations.FieldValues = [];
                for (var i = 0; i < _vm.currentValidationValues.length; i++) {
                    rule.FieldValidations.FieldValues.push(_vm.currentValidationValues[i].Value);
                }
            }

            var adding = rule.AddingNew;
            delete rule.AddNew;//TODO: Should I be deleting this or allowing it to remain
            if (adding) {
                _vm['ruleGrid'].addItem(rule);
            }
            else {
                _vm['ruleGrid'].updateItems([rule], rule.Id);
            }

            endEditingBusinessRule();
            return false;
        };

        var paymentSourceRenderer = function (row, cell, value, columnDef, dataContext) {
            return getPaymentSourceDesc(dataContext.BatchSourceKey);
        };

        var getPaymentSourceDesc = function (sourceKey) {
            if (sourceKey === undefined || sourceKey === null)
                return "All";
            else {
                var source = $.grep(_vm.sources, function (e) { return e.ID === sourceKey });
                if (source.length > 0) {
                    return source[0].LongName;
                }
            }
            return sourceKey;
        };

        var paymentTypeRenderer = function (row, cell, value, columnDef, dataContext) {
            return getPaymentTypeDesc(dataContext.BatchPaymentTypeKey);
        };

        var getPaymentTypeDesc = function (paymentTypeKey) {
            if (paymentTypeKey === undefined || paymentTypeKey === null)
                return "All";
            else {
                var type = $.grep(_vm.paymentTypes, function (e) { return e.ID === paymentTypeKey });
                if (type.length > 0) {
                    return type[0].Description;
                }
            }
            return paymentTypeKey;
        };

        var paymentSubTypeRenderer = function (row, cell, value, columnDef, dataContext) {
            return getPaymentSubTypeDesc(dataContext.BatchPaymentTypeKey, dataContext.BatchPaymentSubTypeKey);
        };

        var getPaymentSubTypeDesc = function (paymentTypeKey, paymentSubTypeKey) {
            if (paymentSubTypeKey === null || paymentSubTypeKey === undefined) {
                var type = ko.utils.arrayFirst(_vm.paymentTypes, function (e) { return e.ID === paymentTypeKey });
                return (type !== null && type.HasSubTypes) ? "All" : "";
            }
            else {
                var subType = $.grep(_vm.paymentSubTypes, function (e) { return e.BatchPaymentSubTypeKey === paymentSubTypeKey });
                if (subType.length > 0) {
                    return subType[0].SubTypeDescription;
                }
            }
            return paymentSubTypeKey;
        };

        var setRulesSelectedField = function (field) {
            var selectedField = _vm["fieldSelect"].wfsSelectbox("getData");
            _vm.selectedRulesField(selectedField === null ? "Not Selected" : selectedField.TableType + " - " + selectedField.FieldName);
        };

        var setSelectedPaymentType = function (paymentType) {
            //update the available sub types
            var subTypes = [];

            if (paymentType !== null && paymentType.HasSubTypes) {
                subTypes = $.grep(_vm.paymentSubTypes, function (e) { return e.BatchPaymentTypeKey === paymentType.ID });
            }

            _vm.defaultPaymentSubType.SubTypeDescription = subTypes.length === 0 ? "N/A" : "All";
            _vm.availableSubTypes = [_vm.defaultPaymentSubType].concat(subTypes);

            _vm.subTypeSelectVisible(paymentType.HasSubTypes);
            _vm['subTypeSelect'].wfsSelectbox('setItems', _vm.availableSubTypes);
            _vm['subTypeSelect'].wfsSelectbox('setValueByData', _vm.defaultPaymentSubType);//reset the selection to the default
        };

        var setSelectedFormatType = function (formatType) {
            //if they changed something, wipe out the mask
            if (formatType === null || formatType.text !== _vm.currentRule.FormatType()) {
                _vm.currentRule.Mask(null);
            }

            if (formatType)
                _vm.currentRule.FormatType(formatType.text);

            _vm["dateMaskSelect"].wfsSelectbox("setValue", _vm.currentRule.Mask());
        };

        var setSelectedCDMethod = function (method) {
            _vm.currentRule.CheckDigitRoutine.Method(method ? method.text : null);
            if (method === null) {
                clearCheckDigitErrors();
            }
        };

        var setSelectedVTType = function (vtType) {
            _vm.currentRule.FieldValidations.ValidationTypeKey(vtType ? vtType.ID : null);
            if (vtType === null) {
                clearValidValuesErrors();
            }
        };

        var fieldSorter = function (dataView, sortFieldName, ascending) {
            //we have two levels to sort by when sorting by table type
            dataView.sort(function (a, b) {
                var sortBy = sortFieldName;
                var result = comparePropertyValues(sortBy, a, b);
                if (result === 0) {
                    if (sortBy == "TableType") {
                        result = comparePropertyValues("ScreenOrder", a, b);
                    }
                }
                return result;
            }, ascending);
        };


        var ruleSorter = function (dataView, sortFieldName, ascending) {
            // using native sort with comparer
            // preferred method but can be very slow in IE with huge datasets
            dataView.sort(function (a, b) {
                var sortBy = sortFieldName;
                if (sortBy === "TableType") {
                    var result = comparePropertyValues(sortBy, a, b);
                    if (result === 0) {
                        result = comparePropertyValues("ExceptionDisplayOrder", a, b);
                    }
                    return result;
                }
                else {
                    var x = undefined, y = undefined;
                    if (sortBy === "BatchSource") {
                        x = getPaymentSourceDesc(a.BatchSourceKey);
                        y = getPaymentSourceDesc(b.BatchSourceKey);
                    }
                    else if (sortBy === "PaymentType") {
                        x = getPaymentTypeDesc(a.BatchPaymentTypeKey);
                        y = getPaymentTypeDesc(b.BatchPaymentTypeKey);
                    }
                    else if (sortBy === "PaymentSubType") {
                        x = getPaymentSubTypeDesc(a.BatchPaymentTypeKey, a.BatchPaymentSubTypeKey);
                        y = getPaymentSubTypeDesc(b.BatchPaymentTypeKey, b.BatchPaymentSubTypeKey);
                    }
                    else if (sortBy === "ExceptionDisplayOrder") {
                        x = a.ExceptionDisplayOrder;
                        y = b.ExceptionDisplayOrder;
                    }
                    return compareValues(x, y);
                }
            }, ascending);
        };


        var initRuleGrid = function () {
            if (_vm['ruleGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        //forceFitColumns: false
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#ruleGrid',
                        pagerSelector: '#rulePager',
                        pageSize: 10,
                        displayPagerSettings: false,
                        data: _vm.fieldRules,
                        idFieldName: 'Id',
                        allowRowDelete: _vm.CanManage,
                        rowDeleteTooltip: _vm.labels.DeleteRuleTooltip,
                        rowDeleteCallback: deleteSelectedRule,
                        allowShowDetail: true,
                        showDetailTooltip: _vm.labels.EditRuleTooltip,
                        showDetailCallback: editSelectedRule,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        customSorter: ruleSorter,
                        sortField: "TableType",
                        columns: [
                            { Id: 'colTableType', ColumnTitle: _vm.labels.DocumentType, ColumnFieldName: 'TableType', Tooltip: _vm.labels.DocumentTypeTooltip, DataType: 1/*, width: 90*/, sortable: true },
                            { Id: "colOrder", ColumnTitle: _vm.labels.DisplayOrder, ColumnFieldName: "ExceptionDisplayOrder", Tooltip: _vm.labels.DisplayOrderTooltip, DataType: 6/*, width: 150*/, sortable: true },
                            { Id: 'colFieldName', ColumnTitle: _vm.labels.FieldName, ColumnFieldName: 'FieldName', Tooltip: _vm.labels.FieldNameTooltip, DataType: 1/*, width: 130*/, sortable: true },
                            { Id: "colSource", ColumnTitle: _vm.labels.PaymentSource, ColumnFieldName: "BatchSource", Tooltip: _vm.labels.BatchSourceTooltip, DataType: 1/*, width: 60*/, formatter: paymentSourceRenderer, sortable: true },
                            { Id: "colType", ColumnTitle: _vm.labels.PaymentType, ColumnFieldName: "PaymentType", Tooltip: _vm.labels.PaymentTypeTooltip, DataType: 1/*, width: 150*/, formatter: paymentTypeRenderer, sortable: true },
                            { Id: "colSubType", ColumnTitle: _vm.labels.SubType, ColumnFieldName: "PaymentSubType", Tooltip: _vm.labels.PaymentSubTypeTooltip, DataType: 1/*, width: 150*/, formatter: paymentSubTypeRenderer, sortable: true }
                        ],
                    },
                };

                _vm['ruleGrid'] = new WFSGridBase.GridBase();
                _vm['ruleGrid'].init(_container.attr('id'), gridModel);
            }
        };


        this.addReplacementValueClick = function (e) {
            e.preventDefault();

            _vm.cdReplacementError(false);

            var original = _vm.originalValue();
            var replace = _vm.replaceValue();

            if (!original || original.length !== 1) {
                _vm.cdReplacementErrorText(_vm.labels.OriginalValueRequired);
                _vm.cdReplacementError(true);
                return false;
            }

            var replace = _me.validateInt(replace, _singleDigitPattern, _vm.cdReplacementError, true);
            if (_vm.cdReplacementError() || replace > 9) {
                _vm.cdReplacementErrorText(_vm.labels.ReplaceValueRequired);
                _vm.cdReplacementError(true);
                return false;
            }

            //verify the original value does not exist
            var match = ko.utils.arrayFirst(_vm.currentReplacementValues, function (item) {
                return original == item.Original;
            });
            if (!match) {
                _vm['replacementGrid'].addItem({ Original: original, Replacement: replace });
                _vm.originalValue(null);
                _vm.replaceValue(null);
            }
            else {
                _vm.cdReplacementErrorText(_vm.labels.ReplacementValueAlreadyExists);
                _vm.cdReplacementError(true);
                return false;
            }

            return false;
        };


        var initReplacementGrid = function () {
            if (_vm['replacementGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        //forceFitColumns: false
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#cdReplacementGrid',
                        pagerSelector: '#cdReplacementPager',
                        pageSize: 4,
                        displayPagerSettings: false,
                        data: _vm.currentReplacementValues,
                        idFieldName: 'Original',
                        allowRowDelete: _vm.CanManage,
                        rowDeleteToolsip: _vm.labels.DeleteReplacementTooltip,
                        allowShowDetail: false,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        sortField: 'Original',
                        columns: [
                            { Id: 'colOriginal', ColumnTitle: _vm.labels.Original, ColumnFieldName: 'Original', DataType: 1/*, width: 60*/, sortable: true },
                            { Id: 'colReplacement', ColumnTitle: _vm.labels.Replacement, ColumnFieldName: 'Replacement', DataType: 6/*, width: 60*/, sortable: true },
                        ],
                    },
                };

                _vm['replacementGrid'] = new WFSGridBase.GridBase();
                _vm['replacementGrid'].init(_container.attr('id'), gridModel);
            }
        };

        this.addValidationValueClick = function (e) {
            e.preventDefault();

            var value = _vm.validationValue();

            _vm.validationValueError(!value || value.length === 0);
            if (!_vm.validationValueError()) {
                //verify the original value does not exist
                var match = ko.utils.arrayFirst(_vm.currentValidationValues, function (item) {
                    return value == item.Value;
                });
                //if it's not a match add it, if it is a match, ignore it
                if (!match) {
                    _vm['validValuesGrid'].addItem({ Value: value });
                }

                _vm.validationValue(null);
            }

            $getE('#ValidationValue').focus();
            return false;
        };

        var initValidValuesGrid = function () {
            if (_vm['validValuesGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        //forceFitColumns: false
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#validValuesGrid',
                        pagerSelector: '#validValuesPager',
                        pageSize: 5,
                        displayPagerSettings: false,
                        data: _vm.currentValidationValues,
                        idFieldName: 'Value',
                        allowRowDelete: _vm.CanManage,
                        rowDeleteToolsip: _vm.labels.DeleteValidationValueTooltip,
                        allowShowDetail: false,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        sortField: 'Value',
                        columns: [
                            { Id: 'colValue', ColumnTitle: _vm.labels.Value, ColumnFieldName: 'Value', DataType: 1/*, width: 60*/, sortable: true },
                        ],
                    },
                };

                _vm['validValuesGrid'] = new WFSGridBase.GridBase();
                _vm['validValuesGrid'].init(_container.attr('id'), gridModel);
            }
        };

        var updateFieldGridHeight = function () {
            setTimeout(function () {
            var el = $('#fieldGrid').find('.slick-pane-top');
            el.height(el.height() + 25);
           }, 300);
        }
        var resizeGrid = function (gridPropertyId, immediate, newPageSize) {
            if (_vm[gridPropertyId]) {
                if (immediate) {
                    _vm[gridPropertyId].resizeGrid(newPageSize);
                }
                else {
                    setTimeout(function () {
                        _vm[gridPropertyId].resizeGrid(newPageSize);
                    }, 50);
                }
            }
        };



        //reloads the DE fields with an ajax call if reloadFields is set, otherwise
        //it uses the list on load.
        var getDEFields = function () {
            if (!_vm.reloadDEFields) {
                if (_vm.hideInactive()) {
                    _vm['fieldGrid'].setItems(_vm.ActiveDECoumns);
                }
                else {
                    _vm['fieldGrid'].setItems(_vm.selectedDEColumns);
                }
            }
            else {
                var eventRulesURL = "/RecHubConfigViews/Workgroups/GetWorkGroupDEFields";
                var data = { clientAccountKey: _vm.ClientAccountKey(), inactive: _vm.hideInactive() };
                framework.doJSON(eventRulesURL, $.toDictionary(data), getDEFieldsCallback);
                framework.showSpinner('Loading...');
            }
        }

        var getDEFieldsCallback = function (serverResponse) {
            framework.hideSpinner();
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
                return
            }
            _vm['fieldGrid'].setItems(serverResponse.Data, 'fieldGrid');
        }


        var setupElementBindings = function () {
            // bind click event to cancel first so they user should always be able to exit
            $getE('#cancelButton').bind('click', function () {
                framework.closeModal();
                return false;
            });

            if (!_vm.CanManage) {
                $getE("input").prop("disabled", true);
            }

            $getE('#workgroupDetailsTabs > li > a').click(function (e) {
                _vm.selectedTab(e.target.id);
            });

            if (!_vm.updatingExisting) {
                _vm['bankSelect'] = $getE('#bankSelect').wfsSelectbox({
                    displayField: 'BankText',
                    idField: 'SiteBankID',
                    width: '100%',
                    minimumResultsForSearch: 100,
                    allowClear: false,
                    items: _vm.banks
                });
            }

            _vm["displayBatchIDSelect"] = $getE("#displayBatchIDSelect").wfsSelectbox({
                displayField: 'text',
                idField: 'id',
                width: '100%',
                minimumResultsForSearch: 100,
                minimumResultsForSearch: 100,
                allowClear: false,
                items: _vm.displayBatchIDOptions
            });
            _vm['displayBatchIDSelect'].wfsSelectbox('setValue', _vm.selectedWorkgroup().DisplayBatchID === null ? -1 : _vm.selectedWorkgroup().DisplayBatchID ? 1 : 0);

            _vm['checkImageDisplayModeSelect'] = $getE('#checkImageDisplayModeSelect').wfsSelectbox({
                displayField: 'text',
                idField: 'text',
                width: '100%',
                minimumResultsForSearch: 100,
                allowClear: false,
                items: _vm.imageDisplayModes
            });
            _vm['checkImageDisplayModeSelect'].wfsSelectbox('setValue', _vm.selectedWorkgroup().CheckImageDisplayMode === null ? _vm.imageDisplayModes[0] : _vm.selectedWorkgroup().CheckImageDisplayMode);

            _vm['documentImageDisplayModeSelect'] = $getE('#documentImageDisplayModeSelect').wfsSelectbox({
                displayField: 'text',
                idField: 'text',
                width: '100%',
                minimumResultsForSearch: 100,
                allowClear: false,
                items: _vm.imageDisplayModes
            });
            _vm['documentImageDisplayModeSelect'].wfsSelectbox('setValue', _vm.selectedWorkgroup().DocumentImageDisplayMode === null ? _vm.imageDisplayModes[0] : _vm.selectedWorkgroup().DocumentImageDisplayMode);

            _vm['postProcessBalancingSelect'] = $getE('#postProcessBalancingSelect').wfsSelectbox({
                displayField: 'text',
                idField: 'text',
                width: '100%',
                minimumResultsForSearch: 100,
                allowClear: false,
                items: _vm.balancingOptions
            });
            _vm['postProcessBalancingSelect'].wfsSelectbox('setValue', _vm.selectedWorkgroup().InvoiceBalancing === null ? _vm.balancingOptions[0] : _vm.selectedWorkgroup().InvoiceBalancing);

            if ($getE('#storageSettingsPanel').outerHeight() > $getE('#displaySettingsPanel').outerHeight())
                $getE('#displaySettingsPanel').css("min-height", $getE('#storageSettingsPanel').outerHeight());
            else if ($getE('#displaySettingsPanel').outerHeight() > $getE('#storageSettingsPanel').outerHeight())
                $getE('#storageSettingsPanel').css("min-height", $getE('#displaySettingsPanel').outerHeight());

            //DDA Association Tab
            $getE('#achAssociateButton').bind('click', _me.associateDDAOnClick);

            $getE('#tabDDA').on('shown.bs.tab', function (e) {
                initDDAGrid();

                resizeGrid('ddaGrid', true);
            });

            //Field Setup Tab
            $getE('#addFieldLink').bind('click', function (e) {
                _vm.editingField(true);
                _vm.AddingField(true);
                e.preventDefault();
                return false;
            });

            _vm['docTypeSelect'] = $getE('#docTypeSelect').wfsSelectbox({
                displayField: 'text',
                idField: 'text',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.tableTypes,
                callback: _vm.setDocType
            });

            _vm['dataTypeSelect'] = $getE('#dataTypeSelect').wfsSelectbox({
                displayField: 'text',
                idField: 'text',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.dataTypes,
                callback: _vm.setDataType
            });
            _vm['paymentSourceSelect'] = $getE('#paymentSourceSelect').wfsSelectbox({
                displayField: ['LongName'],
                idField: 'BatchSourceKey',
                width: '100%',
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.DESources,
                callback: _vm.setDEPaymentSource
            });

            _vm['paymentSourceACHSelect'] = $getE('#paymentSourceACHSelect').wfsSelectbox({
                displayField: ['LongName'],
                idField: 'BatchSourceKey',
                width: '100%',
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.DESources,
                callback: _vm.setDEPaymentSource
            });

            $getE('#fieldAddButton').bind('click', _me.addFieldOnClick);
            $getE('#fieldCancelButton').bind('click', function (e) {
                e.preventDefault();
                endEditingField();
                return false;
            });

            _vm["templateSelect"] = $getE("#templateSelect").wfsSelectbox({
                displayField: "Description",
                idField: "ShortName",
                width: "100%",
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: true,
                items: _vm.deTemplates,
                callback: function (template) {
                    _vm.creatingACHFields(template.id == "ACH" || (template.id != null && template.id.indexOf('WIRE') > -1));
                    _vm.creatingNewField(template === null || template.Default === true);
                }
            });


            $getE('#tabFieldSetup').on('shown.bs.tab', function (e) {
                initFieldGrid();

                resizeGrid('fieldGrid', true);
                setTimeout(function () {
                    var el = $('#fieldGrid').find('.slick-pane-top');
                    el.height(el.height() + 25);
                }, 300);
       
                var el = $('#fieldPager').find('.ui-icon-seek-first');
                el.off('click');
                el.on('click', function () {
                    if (!$(this).hasClass("ui-state-disabled")) {
                        updateFieldGridHeight();
                    }
                })

                var el = $('#fieldPager').find('.ui-icon-seek-prev');
                el.off('click');
                el.on('click', function () {
                    if (!$(this).hasClass("ui-state-disabled")) {
                        updateFieldGridHeight();
                    }
                })

                el = $('#fieldPager').find('.ui-icon-seek-next');
                el.off('click');
                el.on('click', function () {
                    if (!$(this).hasClass("ui-state-disabled")) {
                        updateFieldGridHeight();
                    }
                })

                el = $('#fieldPager').find('.ui-icon-seek-end');
                el.off('click');
                el.on('click', function () {
                    if (!$(this).hasClass("ui-state-disabled")) {
                        updateFieldGridHeight();
                    }
                })

                $('#fieldGrid').find('.slick-header-sortable').each(function () {
                    $(this).on('click', function () {
                        resizeGrid('fieldGrid', true);
                        setTimeout(function () {
                            var el = $('#fieldGrid').find('.slick-pane-top');
                            el.height(el.height() + 25);
                        }, 100);
                    })
                });
            });

            //Rules Tab
            _vm['fieldSelect'] = $getE('#fieldSelect').wfsSelectbox({
                displayField: ['TableType', 'FieldName'],
                idField: 'id',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.distinctDEFieldNames,
                callback: setRulesSelectedField
            });

            _vm['sourceSelect'] = $getE('#sourceSelect').wfsSelectbox({
                displayField: ['LongName'],
                idField: 'BatchSourceKey',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.sources
            });

            _vm['typeSelect'] = $getE('#typeSelect').wfsSelectbox({
                displayField: ['Description'],
                idField: 'ID',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.paymentTypes,
                callback: setSelectedPaymentType
            });

            _vm['subTypeSelect'] = $getE('#subTypeSelect').wfsSelectbox({
                displayField: ['SubTypeDescription'],
                idField: 'BatchPaymentSubTypeKey',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.availableSubTypes
            });
            
            _vm['formatTypeSelect'] = $getE('#formatTypeSelect').wfsSelectbox({
                displayField: ['text'],
                idField: 'text',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.formatTypes,
                callback: setSelectedFormatType
            });
           
            _vm['dateMaskSelect'] = $getE("#dateMaskSelect").wfsSelectbox({
                displayField: ["text"],
                idField: "text",
                width: "100%",//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: [
                    { text: "mm/dd/yyyy" },
                    { text: "mm/dd/yy" },
                    { text: "mmddyyyy" },
                    { text: "mmddyy" },
                    { text: "yyyymmdd" }
                ]
            });
        
            _vm['cdMethodSelect'] = $getE('#cdMethodSelect').wfsSelectbox({
                displayField: ['text'],
                idField: 'text',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: true,
                items: _vm.cdMethods,
                callback: setSelectedCDMethod
            });

            _vm['cdWeightDirectionSelect'] = $getE('#cdWeightDirectionSelect').wfsSelectbox({
                displayField: ['text'],
                idField: 'text',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: false,
                items: _vm.weightDirections
            });
            
            _vm['vtTypeSelect'] = $getE('#vtTypeSelect').wfsSelectbox({
                displayField: ['Description'],
                idField: 'ID',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should never have that many
                allowClear: true,
                items: _vm.validationValueTypes,
                callback: setSelectedVTType
            });
            
            $getE('#addRuleLink').bind('click', function (e) {
                e.preventDefault();
                _vm.selectedRulePanel('appliesToPanel');
                editBusinessRule({});
                return false;
            });
            $getE('#ruleCancelButton').bind('click', function (e) {
                e.preventDefault();
                endEditingBusinessRule();
                return false;
            });
            
            $getE('#ruleAddButton').bind('click', _me.addRuleOnClick);

            $getE('#tabRules').on('shown.bs.tab', function (e) {
                initRuleGrid();

                resizeGrid('ruleGrid', true);
                resizeGrid('replacementGrid', true);
                resizeGrid('validValuesGrid', true);
            });

            $('#rulesPanelBodies > div').hide();
            $getE('.panel-link').bind('click', function (e) {
                e.preventDefault();
                _vm.selectedRulePanel(e.target.hash.substring(1));
                return false;
            });

            $getE('#addReplacement').bind('click', _me.addReplacementValueClick);

            $getE("#addValidationValue").bind("click", _me.addValidationValueClick);

            //bind the save last so that the user can only save if things rendered properly
            $getE('#saveButton').bind('click', _me.saveOnClick);
        };

        var sViewModel = function (model) {
            var self = this;

            self.maxRetentionDays = Math.min(model.MaxRetentionDays, _fixedMaxRetentionDays);
            self.maxSearchDays = Math.min(self.maxRetentionDays, _fixedMaxSearchDays);

            self.labels = {
                RequiredUILabelFormat: "UI Label is required.",
                RequiredNameFormat: "Required. (Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.)",
                OptionalNameFormat: "Optional. (Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.)",
                RequiredWorkgroupIDFormat: "The Workgroup ID must be a numeric value.",
                WorkgroupIDAlreadyExists: "The Workgroup ID entered already exists.",
                FileGroupFormat: "Must be a valid file path.",
                DaysFormat: "Must be a numeric value or blank.",
                DaysFormatNonZero: "Must be a numeric value greater than zero or blank.",
                DaysLessThanDataDays: "Must be less than or equal to the Data Retention Days.",
                DaysLessThanSysMax: "Must be less than or equal to the System's Max Retention Days (" + self.maxRetentionDays + ").",
                DaysLessThanSearchMax: "Must be less than or equal to the System's Max Search Days (" + self.maxSearchDays + ").",
                ExceptionDeadlineFormat: "Optional. (Allows HH:MM)",
                RT: "RT",
                DDA: "DDA",
                RequiredRTFormat: "The RT must be a 9-digit value.",
                RequiredDDAFormat: "The DDA must be a numeric value.",
                DDAAlreadyAssociated: "The DDA specified is already associated with the workgroup.",
                DDAAssociatedToDifferentWorkgroup: "The DDA specified is already associated with a different workgroup and must be removed before it can be associated to this workgroup.",
                DeleteDDATooltip: "Remove the DDA from the workgroup.",
                Saving: "Saving...",
                Verifying: "Verifying...",

                //Data Entry Columns Grid Header Labels
                UILabel: "UI Label",
                PaymentSource: "Payment Source",
                ScreenOrder: "Screen Order",
                DocumentType: "Item Type",
                DataType: "Data Type",
                FieldLength: "Length",
                FieldName: "Name",
                DisplayName: "Source Display Name",
                MarkSense: "Mark Sense",
                Key: "Key",
                Active: "Active",
                EditDEFieldToolTip: "Edit Data Entry Field",
                DEFieldsDataSourceExists: "@templateName data entry fields for source @dataSource already exist.",
                //Data Entry Column Grid Tooltips
                ScreenOrderTooltip: "The order the data field will be displayed to the user.",
                DocumentTypeTooltip: "The type of document the data field belongs to.",
                DataTypeTooltip: "The type of data that is stored in the data field.",
                FieldLengthTooltip: "The maximum length of the data field's contents.",
                FieldNameTooltip: "The name used for the field when importing work.",
                DisplayNameTooltip: "The name used for displaying the field to the end user.",
                MarkSenseTooltip: "Indicated whether the field is recognized as mark sense.",
                KeyTooltip: "The identifier of the data entry column.",

                RequiredFieldNameFormat: "Required. (Allows alphanumeric, hyphens and underscores.)",
                DuplicateFieldName: "The field name entered already exists.",
                DuplicateUILabel: "This UI Label already exists and has data type @param1, 2 fields can not have the same UI Label and different data types",
                UnableToDeleteImportedDEColumn: "The field was assigned by the import process and may not be deleted.",
                RequiredFieldLengthFormat: "Required (Allows numbers 1-255)",
                RequiredScreenOrderFormat: "Required (Allows numbers 0-255)",
                OptionalLengthFormat: "Optional (Allows numbers 0-255)",

                SelectionRequired: "Selection Required.",

                //Rule Tab
                NotApplicable: "N/A",

                //Rule Entry Columns Grid Header Labels
                DisplayOrder: "Display Order",
                PaymentSource: "Payment Source",
                PaymentType: "Payment Type",
                SubType: "Sub Type",

                //Rule Grid Tooltips
                DeleteRuleTooltip: "Remove the rule from the field.",
                EditRuleTooltip: "Edit the rule.",
                FieldTooltip: "The field the rule applies to.",
                BatchSourceTooltip: "The payment source the rule will be applied for.",
                PaymentTypeTooltip: "The payment type the rule will be applied for.",
                PaymentSubTypeTooltip: "The payment sub-type the rule will be applied for.",
                DisplayOrderTooltip: "The order the field will be displayed in the exceptions keying screen.",


                //Rule validation text
                DuplicateRule: "A rule already exists that applies to the selected options.",
                LengthTooLong: "The min and max length must be less than or equal to the field's length.",
                MinLengthLessThanMax: "Min length must be less than or equal to max.",
                MaskRequired: "The validation mask must be supplied.",
                CustomDateMaskRequired: "The custom date maks must be supplied.",
                CustomDateContainsInvalidMonth: "The month component must be defined as 'm' or 'mm'.",
                CustomDateContainsInvalidDay: "The day component must be defined as 'd' or 'dd'.",
                CustomDateContainsInvalidYear: "The year component must be defined as 'y', 'yy' or 'yyyy'.",
                CustomDateContainsInvalidSeparator: "The custom date mask has an invalid separator (Allows symbols excluding percent sign and comma).",
                CustomDateContainsMultipleSeparator: "The custom date mask's separator must be unique.",
                CustomDateContainsSequentialSeparators: "The custom date mask may not contain sequential separators.",
                CustomDateContainsLeadingOrTrailingSeparator: "The custom date mask may not begin or end with a separator.",

                RequiredTwoDigit: "Required (Allows numbers 0-99)",
                RequiredTwoDigitNoZero: "Required (Allows numbers 1-99)",
                WeightPatternRequired: "Required (Allows numbers separated by commas)",
                OffsetTooLong: "The position must be less than or equal to the field's length.",
                OffsetOutsideValidLength: "The position must be less than or equal to the maximum length.",

                Original: "Original",
                Replacement: "Replacement",
                DeleteReplacementTooltip: "Remove the replacement value.",

                OriginalValueRequired: "Original must be a single character or digit.",
                ReplaceValueRequired: "Replace value must be blank or a single digit.",
                ReplacementValueAlreadyExists: "Original's replacement already exists.",

                DeleteValidationValueTooltip: "Remove the validation value from the set.",
                Value: "Value",
                ValidationValueRequired: "Value required."
            };

            self.model = model;
            self.updatingExisting = model !== null && model.Workgroup !== null;
            self.CanManage = model.CanManage;


            self.selectedTab = ko.observable("tabGeneral");//default tab

            //General Tab
            self.imageDisplayModes = [];
            $.each(model.StaticDescriptions.ImageDisplayModes, function (index, item) {
                self.imageDisplayModes.push({ text: item });
            });

            self.balancingOptions = [];
            $.each(model.StaticDescriptions.BalancingOptions, function (index, item) {
                self.balancingOptions.push({ text: item });
            });

            self.displayBatchIDOptions = [
                { id: -1, text: "Use Entity Setting" },
                { id: 1, text: "Yes" },
                { id: 0, text: "No" },
            ];

            //for the DDA list we need to add ids due to the DataView requiring them
            self.selectedDDAs = [];
            if (self.updatingExisting && model.Workgroup.DDAs !== null) {
                var newId = 0;
                $.each(model.Workgroup.DDAs, function (index, item) {
                    self.selectedDDAs.push($.extend({ Id: newId++ }, item));
                });
            }

            //Field tab
            self.DEActive = ko.observable(true);
            self.DERequired = ko.observable(false);
            self.selectedDEColumns = self.updatingExisting && model.Workgroup.DataEntryColumns !== null ? model.Workgroup.DataEntryColumns : [];
            //generate a unique identifier for the grid
            var tempId = 1;
            $.each(self.selectedDEColumns, function (index, item) {
                item.Id = tempId;
                tempId++;
            });

            self.ActiveDECoumns = [];
            $.each(self.selectedDEColumns, function (index, item) {
                if (item.IsActive == 1) {
                    self.ActiveDECoumns.push(item);
                }
            });
            self.distinctDEFieldNames = getDistinctFieldNames(self.selectedDEColumns);
            if (model.Workgroup != undefined && model.Workgroup.ClientAccountKey != undefined)
                self.ClientAccountKey = ko.observable(model.Workgroup.ClientAccountKey);
            self.CanManageDE = ko.observable(model.CanManage);
            self.ReloadDEFields = false;
            self.hideInactive = ko.observable(false);
            self.hideInactive.subscribe(function (val) {
                getDEFields();
            });


            self.tableTypes = [];
            $.each(model.StaticDescriptions.TableTypes, function (index, item) {
                self.tableTypes.push({ text: item });
            });

            self.dataTypes = [];
            $.each(model.StaticDescriptions.DataTypes, function (index, item) {
                self.dataTypes.push({ text: item });
            });

            self.banks = [];
            if (model.Banks !== null) {
                $.each(model.Banks, function (index, item) {
                    self.banks.push($.extend({ BankText: item.SiteBankID + " - " + item.BankName }, item));
                });
            }

            self.defaultTemplate = { ShortName: "BlankDataEntryField", Description: "Blank Data Entry Field", Default: true };
            self.deTemplates = model.DataEntryTemplates === null ? [self.defaultTemplate] : [self.defaultTemplate].concat(model.DataEntryTemplates);

            //setup our knockout objects for binding
            var blankWorkgroupModel = {
                ClientAccountKey: null,
                SiteBankID: null,
                SiteClientAccountID: null,
                MostRecent: null,
                IsActive: true,
                DataRetentionDays: null,
                ImageRetentionDays: null,
                ShortName: null,
                LongName: null,
                FileGroup: null,
                EntityID: model.EntityID,//only thing we'll set initially is the entity we are adding to
                EntityName: model.EntityName,
                ViewingDays: null,
                MaximumSearchDays: null,
                CheckImageDisplayMode: null,
                DocumentImageDisplayMode: null,
                HOA: null,
                BusinessRules: {
                    InvoiceRequired: null,
                    PreDepositBalancingRequired: null,
                    PostDepositBalancingRequired: null,
                    PostDepositPayerRequired: null
                },
                DisplayBatchID: null,
                InvoiceBalancing: null,
                DDAS: null,
                DataEntryColumns: null,
                DataEntrySetup: null,
                BillingAccount: null,
                BillingField1: null,
                BillingField2: null
            };
            // Set these 2 values to null just for displaying purposes.
            if (model.Workgroup && model.Workgroup.ImageRetentionDays == 0)
                model.Workgroup.ImageRetentionDays = null;
            if (model.Workgroup && model.Workgroup.DataRetentionDays == 0)
                model.Workgroup.DataRetentionDays = null;
            self.selectedWorkgroup = ko.observable(self.updatingExisting ? model.Workgroup : blankWorkgroupModel);
            self.longName = ko.observable(self.selectedWorkgroup().LongName);
            self.workgroupID = ko.observable(self.selectedWorkgroup().SiteClientAccountID);
            self.editTitle = ko.computed(function () {
                var title = "";
                if (self.CanManage)
                    title = self.updatingExisting ? "Editing " : "Adding ";

                title += "Workgroup";

                var addSeparator = false;
                if (self.workgroupID() !== null && self.workgroupID().toString().length > 0) {
                    title += ": " + self.workgroupID();
                    addSeparator = true;
                }
                if (self.longName() !== null && self.longName().length > 0) {
                    title += (addSeparator ? " - " : ": ") + self.longName();
                }
                return title;
            });

            var selectedBankText = null;
            if (self.updatingExisting) {
                var match = ko.utils.arrayFirst(self.banks, function (bank) {
                    return self.selectedWorkgroup().SiteBankID === bank.SiteBankID;
                });
                if (match !== null)
                    selectedBankText = match.SiteBankID + " - " + match.BankName;
            }
            self.BankText = ko.observable(selectedBankText);
            /*self.exceptionDeadline = ko.observable(self.updatingExisting && model.Workgroup.DataEntrySetup.DeadLineDay !== null
                                                    ? ("0" + Math.floor(model.Workgroup.DataEntrySetup.DeadLineDay / 100).toString()).slice(-2)
                                                        + ":"
                                                        + ("0" + (model.Workgroup.DataEntrySetup.DeadLineDay % 100).toString()).slice(-2)
                                                    : null); */
            //self.requiresInvoice = ko.observable(self.updatingExisting && model.Workgroup.DataEntrySetup.RequiresInvoice);

            //self.selectedWorkgroupDDAs = ko.observableArray(self.updatingExisting && model.Workgroup.DDAs !== null ? model.Workgroup.DDAs : []);
            self.associateRT = ko.observable('');
            self.associateDDA = ko.observable('');
            self.newField = ko.observable({});
            self.markSense = ko.observable(false);
            self.setDocType = function (docType) {
                _vm.newField()['DocumentType'] = !docType ? undefined : docType.text;
            };
            self.setDataType = function (dataType) {
                _vm.newField()['DataType'] = !dataType ? undefined : dataType.text;
            };
            self.setDEPaymentSource = function (paymentSource) {
                _vm.newField()['DEPaymentType'] = !paymentSource ? undefined : paymentSource.LongName;
                _vm.newField()['DEBatchSourceKey'] = !paymentSource ? undefined : paymentSource.BatchSourceKey;
                _vm.newField().IsIntegraPAY = !paymentSource ? false : paymentSource.SystemType.toLowerCase() === "integrapay";
                if (_vm.newField().IsIntegraPAY === false)
                    _vm.newField().DisplayName = '';
                _vm.newField.valueHasMutated();
            };

            //General Errors/Text
            self.shortNameError = ko.observable(false);
            self.longNameError = ko.observable(false);
            self.bankError = ko.observable(false);
            self.workgroupIDError = ko.observable(false);
            self.workgroupIDErrorText = ko.observable('');
            self.dataRetentionDaysError = ko.observable(false);
            self.dataRetentionDaysErrorText = ko.observable('');
            self.imageRetentionDaysError = ko.observable(false);
            self.imageRetentionDaysErrorText = ko.observable('');
            self.fileGroupError = ko.observable(false);
            self.viewingDaysError = ko.observable(false);
            self.viewingDaysErrorText = ko.observable('');
            self.maximumSearchDaysError = ko.observable(false);
            self.maximumSearchDaysErrorText = ko.observable('');
            self.exceptionDeadlineError = ko.observable(false);

            //DDA Association Errors/Text
            self.associateRTError = ko.observable(false);
            self.associateDDAError = ko.observable(false);
            self.associateDDAErrorText = ko.observable('');

            //Data Entry Columns Errors/Text
            self.deColumnDocTypeError = ko.observable(false);
            self.deColumnFieldNameError = ko.observable(false);
            self.deColumnFieldNameErrorText = ko.observable('');
            self.deColumnUILabelError = ko.observable(false);
            self.deColumnUILabelErrorText = ko.observable('');
            self.deColumnPaymentSourceError = ko.observable(false);
            self.deACHSourceError = ko.observable(false);
            self.deColumnDataTypeError = ko.observable(false);

            self.deColumnScreenOrderError = ko.observable(false);

            self.editingField = ko.observable(false);
            self.DEFieldKeyEdit = ko.observable(0);
            self.creatingNewField = ko.observable(false);
            self.AddingField = ko.observable(false);
            self.creatingACHFields = ko.observable(false);
            self.editingField.subscribe(function (newValue) {
                if (newValue === true) {
                    _vm["templateSelect"].wfsSelectbox("setValueByData", _vm.defaultTemplate);
                }
            });
            self.displayFieldGrid = ko.computed(function () { return !self.editingField() });
            self.displayFieldGrid.subscribe(function (newValue) {
                if (newValue === true)
                    resizeGrid('fieldGrid', false);
            });


            //Business Rule Tab
            //build an array with all the rules in it to feed to the grid
            self.fieldRules = [];
            var newId = 0;

            if (self.updatingExisting && model.Workgroup.DataEntrySetup && model.Workgroup.DataEntrySetup.BusinessRules) {
                $.each(model.Workgroup.DataEntrySetup.BusinessRules, function (index, item) {
                    self.fieldRules.push($.extend({ Id: newId++ }, item));
                });
            }

            self.defaultSource = { BatchSourceKey: "All", LongName: "All", Default: true };
            self.sources = model.StaticDescriptions.BatchSources === null ? [self.defaultSource] : [self.defaultSource].concat(model.StaticDescriptions.BatchSources);
            self.DESources = model.StaticDescriptions.BatchSources === null ? [] : model.StaticDescriptions.BatchSources;
            self.defaultPaymentType = { ID: "All", Description: "All", Default: true };
            self.paymentTypes = model.StaticDescriptions.BatchPaymentTypes === null ? [self.defaultPaymentType] : [self.defaultPaymentType].concat(model.StaticDescriptions.BatchPaymentTypes);
            self.defaultPaymentSubType = { BatchPaymentSubTypeKey: "All", SubTypeDescription: "All", Default: true };
            self.paymentSubTypes = model.StaticDescriptions.BatchPaymentSubTypes;
            self.availableSubTypes = [self.defaultPaymentSubType];
            $.each(self.paymentTypes, function (index, type) {
                var match = false;
                if (self.paymentSubTypes !== null) {
                    match = ko.utils.arrayFirst(self.paymentSubTypes, function (subType) {
                        return type.ID === subType.BatchPaymentTypeKey;
                    });
                }
                type['HasSubTypes'] = match ? true : false;
            });

            self.formatTypes = [];
            if (model.StaticDescriptions.FormatTypes !== null) {
                $.each(model.StaticDescriptions.FormatTypes, function (index, item) {
                    self.formatTypes.push({ text: item });
                });
            }

            self.cdMethods = [];
            if (model.StaticDescriptions.CDMethods !== null) {
                $.each(model.StaticDescriptions.CDMethods, function (index, item) {
                    self.cdMethods.push({ text: item });
                });
            }

            self.weightDirections = [];
            if (model.StaticDescriptions.WeightDirections !== null) {
                $.each(model.StaticDescriptions.WeightDirections, function (index, item) {
                    self.weightDirections.push({ text: item });
                });
            }

            self.validationValueTypes = model.StaticDescriptions.ValidationTableTypes;


            self.defaultRule = {
                AddingNew: true,
                TableType: null,
                FieldName: null,
                BatchSourceKey: null,
                BatchPaymentKey: null,
                BatchPaymentSubTypeKey: null,
                ExceptionDisplayOrder: null,
                UserCanEdit: false,
                Required: false,
                RequiredMessage: null,
                MinLength: null,
                MaxLength: null,
                FormatType: null,
                Mask: null,
                CheckDigitRoutine: {
                    Method: null,
                    Modulus: null,
                    Compliment: null,
                    WeightsDirection: null,
                    Weights: null,
                    IgnoreSpaces: false,
                    Rem10Replacement: 0,
                    Rem11Replacement: 1,
                    Offset: null,
                    FailureMessage: null
                },
                FieldValidations: {
                    ValidationTypeKey: null,
                    FailureMessage: null
                }
            };
            self.ruleMapping = {
                copy: ["TableType", "FieldName", "BatchSourceKey", "BatchPaymentTypeKey", "BatchPaymentSubTypeKey",
                       "CheckDigitRoutine.WeightsDirection"],
                ignore: ["CheckDigitRoutine.ReplacementValues", "FieldValidation.FieldValues"],
                observe: ["AddingNew", "UserCanEdit", "Required", "RequiredMessage", "ExceptionDisplayOrder", "MinLength", "MaxLength", "FormatType", "Mask",
                         "CheckDigitRoutine.Method", "CheckDigitRoutine.Modulus", "CheckDigitRoutine.Compliment", "CheckDigitRoutine.Weights",
                         "CheckDigitRoutine.IgnoreSpaces", "CheckDigitRoutine.Rem10Replacement", "CheckDigitRoutine.Rem11Replacement",
                         "CheckDigitRoutine.Offset", "CheckDigitRoutine.FailureMessage",
                         "FieldValidations.ValidationTypeKey", "FieldValidations.FailureMessage"]
            };
            self.currentRule = mapping.fromJS(self.defaultRule, self.ruleMapping);
            self.selectedRulesField = ko.observable(null);

            self.originalValue = ko.observable(null);
            self.replaceValue = ko.observable(null);
            self.currentReplacementValues = [];

            self.validationValue = ko.observable(null);
            self.currentValidationValues = [];

            self.editingRule = ko.observable(false);
            self.notEditingRule = ko.computed(function () { return self.editingRule() === false; });
            self.notEditingRule.subscribe(function (newValue) {
                if (newValue === true)
                    resizeGrid('ruleGrid', false);
            });

            self.selectedRulePanel = ko.observable('appliesToPanel');//default panel
            self.selectedRulePanel.subscribe(function (newValue) {
                if (newValue === 'checkDigitPanel') {
                    setTimeout(function () {
                        initReplacementGrid();
                        resizeGrid('replacementGrid');
                    }, 500);
                }
                else if (newValue === 'validValuesPanel') {
                    initValidValuesGrid();
                    resizeGrid('validValuesGrid');
                }
                resizeGrid('ruleGrid');
            });
            self.appliesToPanelVisible = ko.computed(function () {
                return self.selectedRulePanel() === 'appliesToPanel';
            });
            self.generalPanelVisible = ko.computed(function () {
                return self.selectedRulePanel() === 'generalPanel';
            });
            self.checkDigitPanelVisible = ko.computed(function () {
                return self.selectedRulePanel() === 'checkDigitPanel';
            });
            self.validValuesPanelVisible = ko.computed(function () {
                return self.selectedRulePanel() === 'validValuesPanel';
            });
            self.subTypeSelectVisible = ko.observable(false);
            self.requiredMessageEnabled = ko.computed(function () {
                return self.currentRule.Required() === true && self.CanManage;
            });
            self.dateMaskVisible = ko.computed(function () {
                return self.currentRule.FormatType() === "Date";
            });
            self.customMaskVisible = ko.computed(function () {
                var format = self.currentRule.FormatType()
                if (format === "Custom") {
                    _vm.customMaskLabel("Valid Characters");
                    return true;
                }
                else if (format === "Custom Date") {
                    _vm.customMaskLabel("Date Mask");
                    return true;
                }
                return false;
            });
            self.checkDigitEnabled = ko.computed(function () {
                return self.CanManage && self.currentRule.CheckDigitRoutine.Method() !== null;
            });
            self.validValuesEnabled = ko.computed(function () {
                return self.CanManage && self.currentRule.FieldValidations.ValidationTypeKey() !== null;
            });
            self.checkDigitMenuVisible = ko.computed(function () {
                var formatType = self.currentRule.FormatType();
                if (formatType !== "Amount" && formatType !== "Date" && formatType !== "Custom Date") {
                    return self.CanManage || self.currentRule.CheckDigitRoutine.Method() !== null;
                }
                return false;
            });
            self.validationValuesMenuVisible = ko.computed(function () {
                var formatType = self.currentRule.FormatType();
                if (formatType !== "Amount" && formatType !== "Date" && formatType !== "Custom Date") {
                    return self.CanManage || self.currentRule.FieldValidations.ValidationTypeKey() !== null;
                }
                return false;
            });

            self.customMaskLabel = ko.observable(null);

            self.ruleFieldError = ko.observable(false);
            self.ruleFieldErrorText = ko.observable(null);
            self.ruleRequiredMessageError = ko.observable(false);
            self.exceptionDisplayOrderError = ko.observable(false);
            self.ruleLengthError = ko.observable(false);
            self.ruleLengthErrorText = ko.observable(null);
            self.formatTypeError = ko.observable(null);
            self.dateMaskError = ko.observable(false);
            self.maskErrorText = ko.observable(null);
            self.maskError = ko.observable(false);

            self.cdModulusError = ko.observable(false);
            self.cdComplimentError = ko.observable(false);
            self.cdRem10Error = ko.observable(false);
            self.cdRem11Error = ko.observable(false);
            self.cdOffsetError = ko.observable(false);
            self.cdOffsetErrorText = ko.observable(null);
            self.cdWeightDirectionError = ko.observable(false);
            self.cdWeightPatternError = ko.observable(false);
            self.cdMessageError = ko.observable(false);

            self.cdReplacementErrorText = ko.observable(false);
            self.cdReplacementError = ko.observable(false);

            self.validationValueError = ko.observable(false);
            self.validValueErrorText = ko.observable(null);
            self.vtMessageError = ko.observable(false);

            // Billing Fields
            self.billingAccount = ko.observable(self.selectedWorkgroup().BillingAccount);
            self.billingField1 = ko.observable(self.selectedWorkgroup().BillingField1);
            self.billingField2 = ko.observable(self.selectedWorkgroup().BillingField2);
            self.billingAccountError = ko.observable(false);
            self.billingField1Error = ko.observable(false);
            self.billingField2Error = ko.observable(false);

            self.saveCancelEnabled = ko.computed(function () { return !self.editingField() && self.notEditingRule() });
            self.saveCancelDisabled = ko.computed(function () { return !self.saveCancelEnabled() });
            self.DisabledReasons = {
                CancelDEChangesManage: "Save or cancel your pending Data Entry Field changes.",
                CancelDEChanges: "Click Close, on the Data Entry Fields tab, to continue.",
                ReturnDEManage: "Return to Data Entry Fields to save or cancel your pending changes.",
                ReturnDE: "Return to Data Entry Fields to cancel your pending changes.",
                CancelBRManage: "Save or cancel your pending Business Rules changes.",
                CancelBR : "Cancel your pending Business Rules changes.",
                ReturnBRManage: "Return to Business Rules to save or cancel your pending changes.",
                ReturnBR: "Return to Business Rules to cancel your pending changes."
        };
            self.saveCancelDisabledReason = ko.computed(function () {
                if (self.saveCancelEnabled())
                    return "";
                else if (self.editingField()) {
                    if (self.selectedTab() === "tabFieldSetup") {
                        return self.CanManage ? self.DisabledReasons.CancelDEChangesManage : self.DisabledReasons.CancelDEChanges;
                    }
                    return self.CanManage ?  self.DisabledReasons.ReturnDEManage : self.DisabledReasons.ReturnDE;
                }
                else if (!self.notEditingRule()) {
                    if (self.selectedTab() === "tabRules") {
                        return self.CanManage ? self.DisabledReasons.CancelBRManage : self.DisabledReasons.CancelBR;
                    }
                    return self.CanManage ? self.DisabledReasons.ReturnBRManage : self.DisabledReasons.ReturnBR;
                }
                return;
            });

            self.cancelButtonText = self.CanManage ? "Cancel" : "Close";
        };

        function init(myContainerId, model) {
            $(function () {
                _container = $('#' + myContainerId);
                $getE = function (b) { return _container.find(b); };

                if (model.Success !== true) {
                    //TODO: come up with a better way of handling the error without ever showing the modal
                    framework.hideSpinner();
                    framework.errorToast(_container, framework.formatErrors(model.Errors));
                    framework.closeModal();
                    return;
                }

                ko.bindingProvider.instance = new ErrorHandlingBindingProvider();

                _vm = new sViewModel(model);
                ko.applyBindings(_vm, $getE('#workgroupDetails').get(0));

                setupElementBindings();
                framework.hideSpinner();
            });
        };

        return {
            init: init
        };
    }

    function init(myContainerId, model) {
        $(function () {
            var workgroupDetails = new WorkgroupDetails();
            workgroupDetails.init(myContainerId, model);
        });
    }

    return {
        init: init
    };
});

