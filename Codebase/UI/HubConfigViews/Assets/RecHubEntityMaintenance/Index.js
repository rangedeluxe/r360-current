﻿define(["jquery", "ko", "frameworkUtils", "accounting", "wfs.treeselector"], function ($, ko, framework, accounting) {

	function RecHubEntityFrame() {
        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var refreshCallback = function () {
        	framework.loadByContainer('/RecHubConfigViews/RecHubEntityMaintenance', _container);
        };

        var refreshSelectedEntity = function (entity) {
            if (entity === undefined) {
                entity = _vm['entitySelector'].wfsTreeSelector('getSelection');
            }

            if (entity !== null) {
                var purl = "/RecHubConfigViews/RecHubEntityMaintenance/GetOrCreateEntityWorkgroupDefaults";
                var data = { entityId: entity.id };
                framework.showSpinner(_vm.labels.Loading);
                framework.doJSON(purl, data, entityLoadedCallback, data);
            }
            else {
                _vm.selectedEntity({});
            }
        };

        var entityDataLoadedCallback = function (resp, widget) {
            framework.hideSpinner();
            _vm['entitySelector'].wfsTreeSelector('initialize');
        };

        var entityLoadedCallback = function (serverResponse, callbackData) {
            framework.hideSpinner();
            if (serverResponse.Errors && serverResponse.Errors.length > 0) {
                _vm.selectedEntity({});
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else if (serverResponse.Entity === null) {
                _vm.selectedEntity({ EntityID: callbackData.entityId, ExistsOnlyInRaam: true });
            }
            else {
                _vm.selectedEntity(serverResponse.Entity);
            }
        };

        var initTree = function () {
            if (_vm['entitySelector'] === undefined) {
                framework.showSpinner(_vm.labels.Loading);
                _vm['entitySelector'] = $getE("#entitySelector").wfsTreeSelector({ callback: entitySelectionChanged, entityURL: '/RecHubRaamProxy/api/entity', height: "487px", entitiesOnly: true, dataCallback: entityDataLoadedCallback });
            }
        }

        var entitySelectionChanged = function (entity) {
            refreshSelectedEntity(entity);
        }

        var setupElementBindings = function () {
            initTree();

            // bind click event to edit button
            $getE('#editEntityButton').bind('click', function () {
                var entityId = _vm.selectedEntity().EntityID;
                if (entityId === undefined)
                {
                    alert("Please select an entity to modify!");
                    return false;
                }

                var data = { entityId: entityId };

                var desiredWidth = 400;
                var modalOptions = { title: "Edit Entity", width: desiredWidth, position: [($(window).width() - desiredWidth) / 2, 75], closeCallback: refreshSelectedEntity };
                framework.loadByContainer('/RecHubConfigViews/RecHubEntityMaintenance/EditEntity', framework.getModalContent(), data, framework.openModal, modalOptions);
                return false;
            });
        };

        var sViewModel = function (model) {
            var self = this;
            self.model = model;

            self.labels = {
				Title: "Workgroup Defaults",
            	PageRefresh: "Refresh the current view.",
            	Loading: "Loading Workgroup Defaults...",
            	PleaseSelect: "(Please Select An Entity)",
            	EntityDoesNotExist: "Selected entity does not have Workgroup Defaults defined.",
                Add: "Add",
				Edit: "Edit",

            	PaymentImageDisplayModeLabel: "Payment Image Display Mode:",
            	DocumentImageDisplayModeLabel: "Document Image Display Mode:",
            	DisplayBatchIDLabel: "Display Batch ID:",
            	ViewingDaysLabel: "Viewing Days:",
            	MaximumSearchDaysLabel: "Max Search Days:",
            	BillingAccountLabel: "Billing Account:",
            	BillingField1Label: "Billing Field 1:",
            	BillingField2Label: "Billing Field 2:"
            };

            self.refreshScreen = refreshCallback;

            self.imageDisplayModeText = function (modeId) {
            	if (modeId === 1) return "Front and Back";
            	else if (modeId === 2) return "Front only";
            	else return "Use System Setting";
            };

            self.selectedEntity = ko.observable({});
            self.selectedEntityError = ko.computed(function () { return self.selectedEntity().EntityID === undefined || self.selectedEntity().ExistsOnlyInRaam === true });
            self.selectedEntityErrorText = ko.computed(function () {
                return self.selectedEntity().EntityID === undefined ? self.labels.PleaseSelect
                        : self.selectedEntity().ExistsOnlyInRaam === true ? self.labels.EntityDoesNotExist
                        : "";
            });
            self.validEntitySelected = ko.computed(function () { return self.selectedEntity().EntityID !== undefined; });
            self.entityExistsInHub = ko.computed(function () { return self.selectedEntity().ExistsOnlyInRaam !== true; });

            self.loadErrors = ko.observableArray(model.Errors);
        };

        function init(myContainerId, model) {
            $(function () {
                _container = $('#' + myContainerId);
                $getE = function (b) { return _container.find(b); };

                _vm = new sViewModel(model);
                ko.applyBindings(_vm, $getE('#recHubEntityFrameBase').get(0));

                setupElementBindings();
            });
        };

        return {
            init: init
        };
    }

    function init(myContainerId, model) {
        $(function () {
        	var entityFrame = new RecHubEntityFrame();
        	entityFrame.init(myContainerId, model);
        });
    }

    return {
        init: init
    };
});

