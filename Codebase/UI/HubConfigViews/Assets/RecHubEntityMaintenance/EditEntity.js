﻿define(["jquery", "ko", "frameworkUtils", "jquery.validate"], function ($, ko, framework, validation) {

    function ErrorHandlingBindingProvider() {
        var original = new ko.bindingProvider();

        //determine if an element has any bindings
        this.nodeHasBindings = original.nodeHasBindings;

        //return the bindings given a node and the bindingContext
        this.getBindings = function (node, bindingContext) {
            var result;
            try {
                result = original.getBindings(node, bindingContext);
            }
            catch (e) {
                alert("Error in binding: " + e.message);
            }

            return result;
        };
    };

    function EditEntity() {
        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var _fixedMaxSearchDays = 365;
        var _fixedMaxRetentionDays = 2557;

        var _namePattern = /^[A-Za-z0-9 &,\.:'()_-]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash

        this.saveComplete = function (jsonResult) {
            framework.hideSpinner();

            if (!jsonResult.HasErrors) {
                framework.closeModal();
            }
            else {
                //display errors
                framework.errorToast(_container, framework.formatErrors(jsonResult.Errors));
            }
        };

        this.saveOnClickValidate = function () {
            framework.showSpinner(_vm.labels.Verifying);
            $getE("#entityEditForm").submit();
        };

        this.saveOnClick = function () {

            var selectedEntity = _vm.selectedEntity();

            var entity = {
                EntityID: selectedEntity.EntityID,
                DisplayBatchID: selectedEntity.DisplayBatchID,
                DocumentImageDisplayMode: _vm.selectedDocumentImageDisplayMode(),
                MaximumSearchDays: selectedEntity.MaximumSearchDays,
                PaymentImageDisplayMode: _vm.selectedPaymentImageDisplayMode(),
                ViewingDays: selectedEntity.ViewingDays,
                BillingAccount: selectedEntity.BillingAccount,
                BillingField1: selectedEntity.BillingField1,
                BillingField2: selectedEntity.BillingField2,
                HashToken: $getE('input[name="hashtoken"]').val()
            };

            framework.setSpinnerText(_vm.labels.Saving);
            framework.doJSON("/RecHubConfigViews/RecHubEntityMaintenance/Save", $.toDictionary(entity), _me.saveComplete);

            return false;
        };

        var setupElementBindings = function () {

            // Check for failure to load
            if (_vm.errors) {
                framework.closeModal();
                framework.errorToast(_container, framework.formatErrors(_vm.errors));
                return;
            }

            // bind click event to cancel first so they user should always be able to exit
            $getE('#cancelButton').bind('click', function () {
                framework.closeModal();
                return false;
            });

            // Initialize the Payment ImageDisplayMode Selector
            _vm.paymentImageDisplayModeSelect = $getE('.PaymentImageDisplayModeSelect').wfsSelectbox({
                displayField: 'Name',
                idField: 'ID',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should probably never have that many
                allowClear: false,
                items: _vm.ImageDisplayModes,
                callback: _vm.setSelectedPaymentImageDisplayMode
            });

            // Initialize the Document ImageDisplayMode Selector
            _vm.documentImageDisplayModeSelect = $getE('.DocumentImageDisplayModeSelect').wfsSelectbox({
                displayField: 'Name',
                idField: 'ID',
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should probably never have that many
                allowClear: false,
                items: _vm.ImageDisplayModes,
                callback: _vm.setSelectedDocumentImageDisplayMode
            });

            // Setup validations
            $.validator.addMethod(
                "regExMatch",
                function (value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Value contains invalid characters"
            );

            // Setup validations
            $getE("#entityEditForm").validate({
                //ignore: "", // override the default ignore - which would ignore hidden IDs because they are of type "hidden"
                rules: {
                    ViewingDays: {
                        required: true,
                        min: 1,
                        max: _vm.maxRetentionDays,
                        digits: true,
                    },
                    MaximumSearchDays: {
                        required: true,
                        min: 1,
                        max: _vm.maxSearchDays,
                        digits: true,
                    },
                    EntityBillingAccount: {
                        maxlength: 20,
                        regExMatch: _namePattern
                    },
                    EntityBillingField1: {
                        maxlength: 20,
                        regExMatch: _namePattern
                    },
                    EntityBillingField2: {
                        maxlength: 20,
                        regExMatch: _namePattern
                    }
                },
                messages: {
                    ViewingDays: _vm.labels.DaysLessThanSysMax,
                    MaximumSearchDays: _vm.labels.DaysLessThanSearchMax,
                    EntityBillingAccount: {
                        maxlength: _vm.labels.LengthLessThan20,
                        regExMatch: _vm.labels.NameFormat
                    },
                    EntityBillingField1: {
                        maxlength: _vm.labels.LengthLessThan20,
                        regExMatch: _vm.labels.NameFormat
                    },
                    EntityBillingField2: {
                        maxlength: _vm.labels.LengthLessThan20,
                        regExMatch: _vm.labels.NameFormat
                    }
                },
                submitHandler: function (form) {
                    // Validation successful - submit the values
                    _me.saveOnClick();
                },
                invalidHandler: function (event, validator) {
                    // Submit was cancelled - errors should already be showing
                    framework.hideSpinner();
                },
            });

            // Bind the click event for save, to validate and submit
            $getE('#saveButton').bind('click', _me.saveOnClickValidate);

            // Set default value for the Image Display Selectors after everything is bound (it appears it may mess up the validate setup if it gets called before that...)
            _vm.paymentImageDisplayModeSelect.wfsSelectbox('setValue', _vm.selectedEntity().PaymentImageDisplayMode);
            _vm.documentImageDisplayModeSelect.wfsSelectbox('setValue', _vm.selectedEntity().DocumentImageDisplayMode);
        };

        var sViewModel = function (model) {
            var self = this;

            self.maxRetentionDays = Math.min(model.MaxRetentionDays, _fixedMaxRetentionDays);
            self.maxSearchDays = Math.min(self.maxRetentionDays, _fixedMaxSearchDays);

            self.labels = {
                LegendError: "Error",
                LegendEdit: "Edit Workgroup Defaults",

                Save: "Save",
                Cancel: "Cancel",

                Yes: "Yes",
                No: "No",

                PaymentImageDisplayModeLabel: "Payment Image Display Mode",
                DocumentImageDisplayModeLabel: "Document Image Display Mode",
                DisplayBatchIDLabel: "Display Batch ID",
                ViewingDaysLabel: "Viewing Days",
                MaximumSearchDaysLabel: "Max Search Days",
                BillingAccountLabel: "Billing Account:",
                BillingField1Label: "Billing Field 1:",
                BillingField2Label: "Billing Field 2:",
                Saving: "Saving...",
                Verifying: "Verifying...",

                DaysLessThanSysMax: "Must be less than or equal to the Max Retention Days (" + self.maxRetentionDays + ").",
                DaysLessThanSearchMax: "Must be less than or equal to the Max Search Days (" + self.maxSearchDays + ").",
                LengthLessThan20: "Must be less than or equal to 20 characters.",
                NameFormat: "Invalid Characters - Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.",

                DefaultError: "The operation could not be completed.  Please contact technical support.",

                UseInheritedSetting: "Use System Setting",
                BothSides: "Front and Back",
                FrontOnly: "Front only",
            };

            self.model = model;
            if (!model.Success) {
                self.errors = model.Errors;
                if (!self.errors || self.errors.length == 0)
                    self.errors = new Array(self.labels.DefaultError);
                self.formLegend = self.labels.LegendError;
            }
            else {
                self.formLegend = self.labels.LegendEdit;
            }

            self.ImageDisplayModes = new Array({ ID: 0, Name: self.labels.UseInheritedSetting }, { ID: 1, Name: self.labels.BothSides }, { ID: 2, Name: self.labels.FrontOnly });

            //setup our knockout objects for binding
            self.selectedEntity = ko.observable(model.Entity);

            // Bind ImageDisplayMode selection for knockout
            self.selectedPaymentImageDisplayMode = ko.observable(self.selectedEntity().PaymentImageDisplayMode);
            self.setSelectedPaymentImageDisplayMode = function (mode) {
                _vm.selectedPaymentImageDisplayMode(mode.ID);
            };

            self.selectedDocumentImageDisplayMode = ko.observable(self.selectedEntity().DocumentImageDisplayMode);
            self.setSelectedDocumentImageDisplayMode = function (mode) {
                _vm.selectedDocumentImageDisplayMode(mode.ID);
            };
        };

        function init(myContainerId, model) {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b); };

            ko.bindingProvider.instance = new ErrorHandlingBindingProvider();

            _vm = new sViewModel(model);
            ko.applyBindings(_vm, $getE('#entityEdit').get(0));

            setupElementBindings();
        };

        return {
            init: init
        };
    }

    function init(myContainerId, model) {
        var editEntity = new EditEntity();
        editEntity.init(myContainerId, model);
    }

    return {
        init: init
    };
});

