﻿define(["jquery", "ko", "frameworkUtils", "jquery.validate", "wfs.select"], function ($, ko, framework, validation) {

    function ErrorHandlingBindingProvider() {
        var original = new ko.bindingProvider();

        //determine if an element has any bindings
        this.nodeHasBindings = original.nodeHasBindings;

        //return the bindings given a node and the bindingContext
        this.getBindings = function (node, bindingContext) {
            var result;
            try {
                result = original.getBindings(node, bindingContext);
            }
            catch (e) {
                alert("Error in binding: " + e.message);
            }

            return result;
        };
    };

    function EditBank() {
        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var _namePattern = /^[A-Za-z0-9 &,\.:'()_-]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash

        this.saveComplete = function (jsonResult) {
            framework.hideSpinner();

            if (!jsonResult.HasErrors) {
                framework.closeModal();
            }
            else {
                //display errors
                framework.errorToast(_container, framework.formatErrors(jsonResult.Errors));
            }
        };

        this.saveOnClickValidate = function () {
            framework.showSpinner(_vm.labels.Verifying);
            $getE("#bankEditForm").submit();
        };

        this.saveOnClick = function () {

            var selectedBank = _vm.selectedBank();

            var bank = {
                BankKey: _vm.updatingExisting ? selectedBank.BankKey : 0,
                SiteBankID: selectedBank.SiteBankID,
                BankName: selectedBank.BankName,
                FIEntityID: _vm.selectedFI().ID,
            };

            framework.setSpinnerText(_vm.labels.Saving);
            framework.doJSON("/RecHubConfigViews/BankMaintenance/Save", $.toDictionary(bank), _me.saveComplete);

            return false;
        };

        var setupElementBindings = function () {
            // bind click event to cancel first so they user should always be able to exit
            $getE('#cancelButton').bind('click', function () {
                framework.closeModal();
                return false;
            });

            // Initialize the FI Dropdown Selector
            _vm["entitySelect"] = $getE('.fiEntitySelect').wfsSelectbox({
                displayField: 'Name',
                idField: 'ID',
                minimumSelectionSize: 0,
                width: '100%',//TODO
                minimumResultsForSearch: 100,//Should probably never have that many
                allowClear: false,
                items: _vm.FIEntities,
                callback: _vm.setSelectedFI
            });

            // Setup validations
            $.validator.addMethod(
                "regExMatch",
                function (value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Value contains invalid characters"
            );

            $getE("#bankEditForm").validate({
                ignore: "", // override the default ignore - which would ignore fiEntityId because it is of type "hidden"
                rules: {
                    fiEntityId: {
                        required: true,
                        min: 1
                    },
                    siteBankID: {
                        required: true,
                        min: 1,
                        max: 999999999,
                        digits: true,
                    },
                    bankName: {
                        required: true,
                        minlength: 1,
                        maxlength: 128,
                        regExMatch: _namePattern,
                    }
                },
                messages: {
                    fiEntityId: _vm.labels.FIRequired,
                    siteBankID: _vm.labels.IDRequired,
                    bankName: {
                        required: _vm.labels.NameRequired,
                        minLength: _vm.labels.NameRequired,
                        regExMatch: _vm.labels.NameFormat,
                    },
                },
                submitHandler: function (form) {
                    // Validation successful - submit the values
                    _me.saveOnClick();
                },
                invalidHandler: function (event, validator) {
                    // Submit was cancelled - errors should already be showing
                    framework.hideSpinner();
                },
            });

            // Bind the click event for save, to validate and submit
            $getE('#saveButton').bind('click', _me.saveOnClickValidate);

            // Set default value for the FI Selector after everything is bound (it appears it may mess up the validate setup if it gets called before that...)
            if (_vm.selectedBank().FI.ID > 0)
                _vm["entitySelect"].wfsSelectbox('setValue', _vm.selectedBank().FI.ID);

            // Setup hover help text binding
            //$getE("#helpIcon").popover({ trigger: 'hover', html: true, placement: 'bottom', content: function () { return $getE('#popOverHelp').html(); } });

            // Check for failure to load
            if (_vm.errors) {
                framework.closeModal();
                framework.errorToast(_container, framework.formatErrors(_vm.errors));
            }
        };

        var sViewModel = function (model) {
            var self = this;

            self.labels = {
                LegendError: "Error",
                LegendEdit: "Edit Bank",
                LegendAdd: "Add Bank",

                Save: "Save",
                Cancel: "Cancel",

                ID: "ID",
                Name: "Name",
                FI: "Entity/FI",
                Saving: "Saving...",
                Verifying: "Verifying...",

                FIRequired: "Please select the Entity/FI owner.",
                IDRequired: "ID must be 1-9 digits in length.",
                NameRequired: "Name is required.",
                NameFormat: "Invalid Characters - Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.",

                DefaultError: "The operation could not be completed.  Please contact technical support.",

                //FIHelp: "This is the top-level entity of type FI that owns / controls the set of Workgroups.",
                //SiteBankIDHelp: "This is the Bank ID - a unique numeric identifier for the set of Workgroups.",
                //BankNameHelp: "This is the Bank Name that represents the set of Workgroups.",

                //OverviewHTML: "<p>Data must be assigned to Workgroups for viewing and processing.&nbsp; The Workgroups themselves are grouped into sets based on the source / owner of the data.&nbsp; "
                //	+ "This is referred to as the \"Bank\" that the work comes from.</p>"
                //	+ "<p>Data comes into the system for a specific Workgroup, but the FI Entity is not passed in with the source data."
                //	+ "<br/>A \"Bank\" is used to secure Workgroup data and properly associate it with the correct FI Entity.</p>"
                //	+ "<p>In many cases the \"Bank\" is the same as the FI Entity.&nbsp; However, even a single financial institution may group work via multiple \"Banks\" representing different regions, acquisitions / mergers, or correspondent banking relationships.</p>"
                //	+ "<p>In other cases, work must be grouped by the physical capture \"site\" in order to ensure trackability - and the ID is included in all files from that site.&nbsp; "
                //	+ "Please configure \"Banks\" and the Bank IDs as appropriate for defining sets of Workgroups and/or matching IDs from capture sources.</p>",
            };

            self.model = model;
            if (!model.Success) {
                self.updatingExisting = false;
                self.errors = model.Errors;
                if (!self.errors || self.errors.length == 0)
                    self.errors = new Array(self.labels.DefaultError);
                self.formLegend = self.labels.LegendError;
            }
            else {
                self.updatingExisting = model !== null && model.Bank !== null;
                self.formLegend = (self.updatingExisting ? self.labels.LegendEdit : self.labels.LegendAdd);
            }

            self.FIEntities = model.FIs;

            //setup our knockout objects for binding
            self.selectedBank = ko.observable(self.updatingExisting ? model.Bank : {});

            // Attach an FI object to the bank object
            self.selectedBank().FI = { ID: 0, Name: null };
            if (self.selectedBank().FIEntityID > 0) {
                var match = $.grep(model.FIs, function (fi) { return fi.ID == self.selectedBank().FIEntityID; });
                if (match.length > 0)
                    self.selectedBank().FI = match[0];
            }

            // Default the FI if there is only one value, and/or set initial selection
            self.hasSingleFI = (self.FIEntities.length === 1);
            if (self.hasSingleFI)
                self.selectedBank().FI = self.FIEntities[0];

            // For updates - act like there is only a single FI (assuming the FI has already been set...)
            if (self.updatingExisting && self.selectedBank().FI.ID > 0)
                self.hasSingleFI = true;

            // Bind FI selection for knockout
            self.selectedFI = ko.observable(self.selectedBank().FI).extend({ required: true, minLength: 1 });
            self.setSelectedFI = function (fi) {
                _vm.selectedFI(fi);
                $getE("#fiEntityId").valid();
            };
        };

        function init(myContainerId, model) {

            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b); };
            ko.bindingProvider.instance = new ErrorHandlingBindingProvider();

            _vm = new sViewModel(model);
            ko.applyBindings(_vm, $getE('#bankEdit').get(0));
            setupElementBindings();

        };

        return {
            "init": init
        };
    }

    function init(myContainerId, model) {
        var editBank = new EditBank();
        editBank.init(myContainerId, model);
    };

    return {
        init: init
    };
});
