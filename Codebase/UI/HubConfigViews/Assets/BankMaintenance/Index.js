﻿define(["jquery", "ko", "frameworkUtils", "accounting", "wfs.gridbase"], function ($, ko, framework, accounting, WFSGridBase) {

    function Banks() {
        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var refreshCallback = function () {
            //framework.loadByContainer('/RecHubConfigViews/BankMaintenance', _container);

            var bankMaintenanceSourcesUrl = "/RecHubConfigViews/BankMaintenance/GetBankList";
            framework.doJSON(bankMaintenanceSourcesUrl, {}, doRefresh);
            framework.showSpinner('Loading...');
        };

        var doRefresh = function (serverResponse) {

            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            } else if (!serverResponse.Data || serverResponse.Data.length < 1) {
                _vm['bankGrid'].setItems([]);

            } else {
                _vm.banks = serverResponse.Data;
                mapBanksToFIs();
                _vm['bankGrid'].setItems(serverResponse.Data);
            }
            framework.hideSpinner();
        }

        var updateBank = function (gridRow, col, dataIndex, item) {
            var data = { adding: false, bankKey: item.BankKey };

            var desiredWidth = 700;
            var modalOptions = { title: _vm.labels.LegendEdit, width: desiredWidth, position: [($(window).width() - desiredWidth) / 2, 75], closeCallback: refreshCallback };
            framework.loadByContainer('/RecHubConfigViews/BankMaintenance/EditBank', framework.getModalContent(), data, framework.openModal(modalOptions));
            return false;

        };

        var initBankGrid = function () {
            if (_vm['bankGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        //forceFitColumns: false
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#bankGrid',
                        pagerSelector: '#bankPager',
                        pageSize: 10,
                        displayPagerSettings: true,
                        data: _vm.banks,
                        idFieldName: 'SiteBankID',
                        allowRowDelete: false,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        allowShowDetail: _vm.canEdit,
                        showDetailTooltip: _vm.labels.EditTooltip,
                        showDetailCallback: updateBank,

                        sortField: 'BankName',
                        columns: [
                            { Id: 'colSiteBankId', ColumnTitle: _vm.labels.SiteBankId, Tooltip: _vm.labels.SiteBankIdTooltip, ColumnFieldName: 'SiteBankID', DataType: 6, width: 30, sortable: true },
                            { Id: 'colBankName', ColumnTitle: _vm.labels.BankName, Tooltip: _vm.labels.BankNameTooltip, ColumnFieldName: 'BankName', DataType: 1, width: 200, sortable: true },
                            { Id: 'colFIEntity', ColumnTitle: _vm.labels.FIEntity, Tooltip: _vm.labels.FIEntityTooltip, ColumnFieldName: 'FIEntityName', DataType: 1, width: 150, sortable: true },
                        ],
                    },
                };

                _vm['bankGrid'] = new WFSGridBase.GridBase();
                _vm['bankGrid'].init(_container.attr('id'), gridModel);
            }
        };

        var setupElementBindings = function () {

            // bind click event to add button
            $getE('#addBankButton').bind('click', function () {
                var data = { adding: true };

                var desiredWidth = 700;
                var modalOptions = { title: _vm.labels.LegendAdd, width: desiredWidth, position: [($(window).width() - desiredWidth) / 2, 75], closeCallback: refreshCallback };
                framework.loadByContainer('/RecHubConfigViews/BankMaintenance/EditBank', framework.getModalContent(), data, framework.openModal(modalOptions));
                return false;
            });

            mapBanksToFIs();

            initBankGrid();
        };

        var mapBanksToFIs = function () {
            // Populate FI Names for easier binding
            $.each(_vm.banks, function (index, item) {
                var match = $.grep(_vm.model.FIs, function (fi) { return fi.ID == item.FIEntityID; });
                if (match.length > 0)
                    item.FIEntityName = match[0].Name;
                else
                    item.FIEntityName = "";
            });

        }

        var sViewModel = function (model) {
            var self = this;
            self.model = model;

            self.labels = {
                Title: "Bank Maintenance",
                LegendEdit: "Edit Bank",
                LegendAdd: "Add Bank",

                Add: "Add",

                PageRefresh: "Refresh the Bank list.",
                SiteBankId: "Id",
                BankName: "Name",
                FIEntity: "Entity/FI",

                SiteBankIdTooltip: "Numeric identifier",
                BankNameTooltip: "Name",
                FIEntityTooltip: "Entity/FI",
                EditTooltip: "Update",
            };

            self.banks = model.Banks;
            if (!self.banks) self.banks = new Array();

            self.canAdd = model.CanAdd;
            self.canEdit = model.CanEdit;

            self.refreshScreen = refreshCallback;

            self.loadErrors = ko.observableArray(model.Errors);
        };

        function init(myContainerId, model) {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b); };
            var contElement = $getE('#banksBase').get(0);
            if (contElement) {       
                _vm = new sViewModel(model);
                ko.applyBindings(_vm, contElement);
                setupElementBindings();
            }
        };

        return {
            init: init
        };
    }

    function init(myContainerId, model) {
        var banks = new Banks();
        banks.init(myContainerId, model);
    }

    return {
        init: init
    };
});

