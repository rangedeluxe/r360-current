﻿function initPreferences(model) {
    require.config({
        paths: {
            // Dependencies.
            'PreferencesController': '/RecHubConfigViews/Assets/UserPreferences/PreferencesController',
            'PreferencesViewModel': '/RecHubConfigViews/Assets/UserPreferences/PreferencesViewModel',
        }
    });

    require(['PreferencesController'], function (controller) {
        controller.init(model);
    });
}