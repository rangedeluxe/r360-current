﻿// Controls the  UI, uses these dependencies.
define(['jquery', 'ko', 'frameworkUtils', 'PreferencesViewModel'],
function ($, ko, framework, PreferencesViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            // Set up the initialize 
            var container = $('#preferences-container');
            _vm = new PreferencesViewModel();
            _vm.init(container, model);

            // Wire up the disposal 
            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }

    // Need this so we can get inside this 'define' method every time we hit the page.
    // See the app js on how it's called.
    return {
        init: init
    };
});