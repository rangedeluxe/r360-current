﻿define(['jquery', 'frameworkUtils', 'ko'],

function ($, framework, ko) {
    var self = this;
    var intPattern = /^\d{0,10}$/;
    var applyUI = function (model) {
        self.displayRemitterAccesible(model.Data.displayRemitterAccesible);
        self.recordsPageAccesible(model.Data.recordsPageAccesible);
        self.displayPayerName(model.Data.displayRemitterNameInPDF);
        self.rowsPerPage(model.Data.recordsPerPage);
        $getE("#SubmitButton").click(validateAndSubmit);
        $getE("#RestoreDefaults").click(restoreDefaults);
        if (!self.displayRemitterAccesible() && !self.recordsPageAccesible()) {
            framework.warningToast(_container, self.labels().noPreferencesAvailable, {sticky: true});
        }
    }


    var restoreDefaults = function () {
        var restPrefsURL = "/RecHubConfigViews/UserPreferences/RestoreDefaults";
        framework.doJSON(restPrefsURL, {}, restorePreferencesCallBack);
        framework.showSpinner(self.labels().loading);
    };

    var restorePreferencesCallBack = function (serverResponse) {
        var data = serverResponse.Data;
        if (serverResponse.HasErrors) {
            framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
        }
        else {

            self.displayPayerName(data.displayRemitterNameinPdf);
            self.rowsPerPage(data.recordsPerPage);
            framework.successToast(_container, self.labels().restoreSuccess);
        }
        framework.hideSpinner();
    }
    var validateAndSubmit = function () {
        // Check if rows per page makes sense.
        self.rowsDisplayedError(self.rowsPerPage() === '' || !intPattern.test(self.rowsPerPage()));
        if (self.rowsDisplayedError())
            return;

        var num = parseInt(self.rowsPerPage());
        // Check if rows per page is greater than 4 and less than 256. (same as system settings)
        self.rowsDisplayedRangeError(num <= 4 || num >= 256);
        if (self.rowsDisplayedRangeError())
            return;

        var savePrefURL = "/RecHubConfigViews/UserPreferences/SavePreferences";
        var data = { recordsPerPage: self.rowsPerPage(), displayRemitterNameInPDF: self.displayPayerName() };
        framework.doJSON(savePrefURL, $.toDictionary(data), savePreferencesCallBack);
        framework.showSpinner(self.labels().loading);
    };

    var savePreferencesCallBack = function(serverResponse){
        var data = serverResponse.Data;
        if (serverResponse.HasErrors) {
            framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
        }
        else{
            framework.successToast(_container, self.labels().updateSucess );
        }
        framework.hideSpinner();
    }

    //ctor
    function PreferencesViewModel() {

    }

    //public props
    this.labels = ko.observable({
        loading: "Loading",
        recordsPerPageRequired: "Records Displayed per Page is required and must be numeric.",
        updateSucess: "Your user preferences have been updated.",
        restoreSuccess: "All user preferences have been restored to the default settings.",
        noPreferencesAvailable: "There are no user preferences available at this time.",
        recordsPerPageRange: "Value must be greater than 4 and less then 256."
    });
   
    this.displayPayerName = ko.observable(false);
    this.rowsPerPage = ko.observable(0).extend({ required: this.labels().recordsPerPageRequired, numeric: 0  });
    this.rowsDisplayedError = ko.observable(false);
    this.rowsDisplayedRangeError = ko.observable(false);
    this.recordsPageAccesible = ko.observable(false);
    this.displayRemitterAccesible = ko.observable(false);
    PreferencesViewModel.prototype = {
        init: function(container, model){
            _container = container;
            $getE = function (b) { return _container.find(b); };
            applyUI(model);
            ko.applyBindings(self, container.get(0));
            
        },
        dispose: function () {
            //destroy things
            displayPayerName(null);
            rowsPerPage(null);
            rowsDisplayedError(null);
        }
    }

    return PreferencesViewModel;

});