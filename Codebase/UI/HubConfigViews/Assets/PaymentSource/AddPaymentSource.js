﻿define(["jquery", "ko", "frameworkUtils", "wfs.select"], function ($, ko, framework) {

	var _me = this;
	var _container = {};
	var _vm = {};
	var _namePattern = /^[A-Za-z0-9 &,\.'()_-]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash
	var _integaPAYShortNamePattern = /^.+-I\d+$/; // {Short Name}-I{Bank ID}
	var $getE = function (b) { alert("Not initialized!"); };

	var setupElementBindings = function () {

		$getE("#formCancel").click(function () { cancel(); return false; });

		_vm["systemTypeSelect"] = $getE('#selectSystemType').wfsSelectbox({ 'items': _vm.model.SystemTypes, 'callback': _vm.setSelectedSystemType, 'displayField': 'Name', 'idField': 'ID', 'width': 330 });
		_vm["fiSelect"] = $getE('#selectFI').wfsSelectbox({ 'items': _vm.model.FIEntities, 'callback': _vm.setSelectedFI, 'displayField': 'Name', 'idField': 'ID', 'width': 330 });

		setFormValidation();

	}

	var setFormValidation = function () {
		$.validator.addMethod(
			"regExMatch",
			function (value, element, regexp) {
				var re = new RegExp(regexp);
				return this.optional(element) || re.test(value);
			},
			"Value contains invalid characters"
		);

		$.validator.addMethod(
			"regExMatchBySystemType",
			function (value, element, props) {
				var re = new RegExp(props.regexp);
				return this.optional(element) || re.test(value) || !(_vm.selectedSystemType()) || _vm.selectedSystemType().ID != props.systemType;
			},
			"Value contains invalid characters"
		);

		var validator = $getE("#paymentSourceForm").validate({
			ignore: ".ignore, .select2-input",
			rules: {
				inputShortName: { required: true, maxlength: 30, regExMatch: _namePattern, regExMatchBySystemType: { regexp: _integaPAYShortNamePattern, systemType: "integraPAY" } },
				inputLongName: { required: true, maxlength: 128, regExMatch: _namePattern }
			},
			messages: {
				inputShortName: { required: _vm.labels.ShortNameRequired, regExMatch: _vm.labels.NameFormat, regExMatchBySystemType: _vm.labels.integraPAYShortNameFormat },
				inputLongName: { required: _vm.labels.DescriptionRequired, regExMatch: _vm.labels.NameFormat }
			},
			submitHandler: function (form) {

                if (_vm["systemTypeSelect"].wfsSelectbox('getValue') == '') {
                    $getE('#errorSelectSystemType').css('display', 'inline-block');
                    $getE('#errorSelectSystemType').text("A System must be selected.");
                    return;
                }
                else if (_vm["fiSelect"].wfsSelectbox('getValue') == '') {
                    $getE('#errorSelectFI').css('display', 'inline-block');
                    $getE('#errorSelectFI').text("An Entity/FI must be selected.");
                    return;
                }
                else
                    savePaymentSource();
			}
		});
	}

	var cancel = function () {
		framework.closeModal();
	}

	var closeModal = function () {
		framework.closeModal();
	}

	var savePaymentSource = function () {
		var url = "/RecHubConfigViews/PaymentSource/InsertPaymentSource/";
		var data = {
			shortName: _vm.model.ShortName,
			longName: _vm.model.LongName,
			isActive: _vm.model.IsActive,
			systemType: _vm.selectedSystemType().ID,
			fiEntityID: _vm.selectedFI().ID
		};

		var savePaymentSourceCallback = function (serverResponse) {
			if (serverResponse.HasErrors) {
				framework.errorToast(_container, serverResponse.Errors[0].Message);
			}
			else {
				closeModal();
			}
		};

		framework.doJSON(url, $.toDictionary(data), savePaymentSourceCallback);
	}

	//*****************************************************
	//*****************************************************
	//  View Model
	//*****************************************************
	var ViewModel = function (model) {
		var self = this;
		self.model = model;
		self.model.IsActive = true;

		self.selectedSystemType = ko.observable(null);
		self.setSelectedSystemType = function (value) {
			self.selectedSystemType(value);
			$getE("#errorSelectSystemType").css('display', 'none');
		};

		self.selectedFI = ko.observable(null);
		self.setSelectedFI = function (value) {
			self.selectedFI(value);
			$getE("#errorSelectFI").css('display', 'none');
		};

		self.labels = {
			NameFormat: "Invalid Characters - Allows alphanumeric with symbols & , . ' ( ) _ - and spaces.",
			DescriptionRequired: "Description is a required field.",
			ShortNameRequired: "Short Name is a required field.",
			integraPAYShortNameFormat: "integraPAY Short Names must append the Bank ID.  Format is '{Short Name}-I{Bank ID}'.  e.g.\xA0'integraPAY-I999'",
		}
	}
	//*****************************************************



	//*****************************************************
	//*****************************************************
	//  Initialization
	//*****************************************************
	var init = function (myContainerId, model) {
		$(function () {
			_container = $('#' + myContainerId);
			$getE = function (b) { return _container.find(b) };
			_vm = new ViewModel(model);

			ko.applyBindings(_vm, $getE('.addPaymentSourceModal').get(0));

			setupElementBindings();

		});
	};

	return {
		"init": init
	}
	//*****************************************************
});
