﻿define(["jquery", "ko", "frameworkUtils", "wfs.select","jquery.validate"], function ($, ko, framework) {

	var _me = this;
	var _container = {};
	var _vm = {};
	var _namePattern = /^[A-Za-z0-9 &,\.:'()_-]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash
	var $getE = function (b) { alert("Not initialized!"); };

    var setupElementBindings = function () {

        $getE("#formCancel").click(function () { cancel(); return false; });

        _vm["systemTypeSelect"] = $getE('#selectSystemType').wfsSelectbox({ 'items': _vm.model.SystemTypes, 'callback': _vm.setSelectedSystemType, 'displayField': 'Name', 'idField': 'ID', 'width': 330 });
        _vm["fiSelect"] = $getE('#selectFI').wfsSelectbox({ 'items': _vm.model.FIEntities, 'callback': _vm.setSelectedFI, 'displayField': 'Name', 'idField': 'ID', 'width': 330 });

        setFormValidation();

        _vm["systemTypeSelect"].wfsSelectbox('setValue', _vm.model.PaymentSource.SystemType);
        _vm["fiSelect"].wfsSelectbox('setValue', _vm.model.PaymentSource.FIEntityID);

    };

    var setFormValidation = function () {
        $.validator.addMethod(
            "regExMatch",
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Value contains invalid characters"
        );

        var validator = $getE("#paymentSourceForm").validate({
            ignore: ".ignore, .select2-input",
            rules: {
                inputLongName: { required: true, maxlength: 128, regExMatch: _namePattern }
            },
            messages: {
                inputLongName: { required: _vm.labels.DescriptionRequired, regExMatch: _vm.labels.NameFormat }
            },
            submitHandler: function (form) {

                if (_vm["systemTypeSelect"].wfsSelectbox('getValue') == '') {
                    $getE('#errorSelectSystemType').css('display', 'inline-block');
                    $getE('#errorSelectSystemType').text("A System must be selected.");
                    return;
                }
                else if (_vm["fiSelect"].wfsSelectbox('getValue') == '') {
                    $getE('#errorSelectFI').css('display', 'inline-block');
                    $getE('#errorSelectFI').text("An Entity/FI must be selected.");
                    return;
                }
                else
                    savePaymentSource();
            }
        });
    };

    var cancel = function () {
        framework.closeModal();
    };

    var closeModal = function () {
        framework.closeModal();
    };

    var savePaymentSource = function () {
        var url = "/RecHubConfigViews/PaymentSource/UpdatePaymentSource/";
        var data = {
            batchSourceKey: _vm.model.PaymentSource.BatchSourceKey,
            shortName: _vm.model.PaymentSource.ShortName,
            longName: _vm.model.PaymentSource.LongName,
            isActive: _vm.model.PaymentSource.IsActive,
            systemType: _vm.selectedSystemType.ID,
            fiEntityID: _vm.selectedFI.ID
        };

        var savePaymentSourceCallback = function (serverResponse) {
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, serverResponse.Errors[0].Message);
            }
            else {
                closeModal();
            }
        };

        framework.doJSON(url, $.toDictionary(data), savePaymentSourceCallback);
    };

	//*****************************************************
	//*****************************************************
	//  View Model
	//*****************************************************
    var ViewModel = function (model) {
        var self = this;
        self.model = model;

        self.selectedSystemType = null;
        self.setSelectedSystemType = function (value) {
            _vm.selectedSystemType = value;
            $getE("#errorSelectSystemType").css('display', 'none');
        };

        self.selectedFI = null;
        self.setSelectedFI = function (value) {
            _vm.selectedFI = value;
            $getE("#errorSelectFI").css('display', 'none');
        };

        self.labels = {
            NameFormat: "Invalid Characters - Allows alphanumeric with symbols & , . : ' ( ) _ - and spaces.",
            DescriptionRequired: "Description is a required field."
        };

    };
	//*****************************************************



	//*****************************************************
	//*****************************************************
	//  Initialization
	//*****************************************************
	var init = function (myContainerId, model) {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b) };
            _vm = new ViewModel(model);

            setupElementBindings();

            ko.applyBindings(_vm, $getE('.editPaymentSourceModal').get(0));
	};

    return {
        "init": init
    };
	//*****************************************************
});
