﻿define(["jquery", "ko", "frameworkUtils", "accounting", "wfs.gridbase","jquery.validate"], function ($, ko, framework, accounting, WFSGridBase) {

    function PaymentSource() {

        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        var doRefresh = function () {
            var paymentSourcesUrl = "/RecHubConfigViews/PaymentSource/GetPaymentSources";
            framework.doJSON(paymentSourcesUrl, {}, refreshCallback);
            framework.showSpinner('Loading...');
        };

        var refreshCallback = function (serverResponse) {
            $getE('#paymentSourceNoItemsMessage').empty();
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            } else if (!serverResponse.Data || serverResponse.Data.length < 1) {
                _vm['psGrid'].setItems([]);
                var messageDiv = $('<div style="font-weight: bold;" >' + 'No Payment Source Data' + '</div>');
                $getE('#paymentSourceNoItemsMessage').append(messageDiv);
            } else {
                //extend the items to have a CanDelete field
                $.each(serverResponse.Data, function (index, item) {
                    item.CanDelete = item.HasBatchAssociation === false;
                });
                _vm['psGrid'].setItems(serverResponse.Data);
            }
            framework.hideSpinner();
        };

        var openAddModal = function () {
            var url = '/RecHubConfigViews/PaymentSource/AddPaymentSource/';
            var ww = $(window).width() / 2 - 350;
            var modalOptions = {
                title: 'Add Payment Source', width: '600', height: '300', position: [ww, 180],
                closeCallback: doRefresh
            };

            framework.loadByContainer(url, framework.getModalContent(), null, framework.openModal(modalOptions));
        };

        var openEditModal = function (gridRow, col, dataIndex, item) {
            var url = '/RecHubConfigViews/PaymentSource/EditPaymentSource/'

            var ww = $(window).width() / 2 - 350;
            var modalOptions = {
                title: 'Edit Payment Source', width: '600', height: '300', position: [ww, 180],
                closeCallback: doRefresh
            };
            framework.loadByContainer(url, framework.getModalContent(), item, framework.openModal(modalOptions));
        };

        var openDeleteModal = function (gridRow, col, dataIndex, item) {
            var url = '/RecHubConfigViews/PaymentSource/DeletePaymentSourceModal/'

            var ww = $(window).width() / 2 - 350;
            var modalOptions = {
                title: 'Delete Payment Source', width: '600', height: '300', position: [ww, 180],
                closeCallback: doRefresh
            };
            framework.loadByContainer(url, framework.getModalContent(), item, framework.openModal(modalOptions));
        };

        var initPaymentSourceGrid = function () {
            if (_vm['psGrid'] === undefined) {
                var gridModel = {
                    gridOptions: {
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#psGrid',
                        pagerSelector: '#psPager',
                        pageSize: 15,
                        displayPagerSettings: true,
                        data: [],
                        idFieldName: 'BatchSourceKey',
                        allowRowDelete: _vm.canModifyPaymentSources(),
                        rowDeleteCallback: openDeleteModal,
                        allowDeleteFieldName: "CanDelete",
                        allowShowDetail: _vm.canModifyPaymentSources(),
                        showDetailCallback: openEditModal,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        sortField: "ShortName",
                        columns: [{ Id: "ShortName", ColumnTitle: "Short Name", ColumnFieldName: "ShortName", DataType: 1, resizable: true, sortable: true },
                                    { Id: "LongName", ColumnTitle: "Long Name", ColumnFieldName: "LongName", DataType: 1, resizable: true, sortable: true },
                                    { Id: "SystemType", ColumnTitle: "System", ColumnFieldName: "SystemType", DataType: 1, resizable: true, sortable: true },
                                    { Id: "FIEntityName", ColumnTitle: "Entity/FI", ColumnFieldName: "FIEntityName", DataType: 1, resizable: true, sortable: true },
                                    { Id: "IsActive", ColumnTitle: "Active", ColumnFieldName: "IsActive", DataType: 20, resizable: false, sortable: true }]
                    }
                };

                _vm['psGrid'] = new WFSGridBase.GridBase();
                _vm['psGrid'].init(_container.attr('id'), gridModel);
            }
        };


        var setupElementBindings = function () {

            $getE('.refresh').bind('click', function () {
                doRefresh();
                return false;
            });

            $getE('#addPaymentSource').bind('click', function () {
                openAddModal();
                return false;
            });

            initPaymentSourceGrid();

            doRefresh();
        };

        //*****************************************************
        //*****************************************************
        //  View Model
        //*****************************************************
        var ViewModel = function (model) {
            var self = this;
            self.model = model;
            self.canModifyPaymentSources = ko.observable(model.CanModifyPaymentSources);
        };
        //*****************************************************



        //*****************************************************
        //*****************************************************
        //  Initialization
        //*****************************************************
        function init(myContainerId, model) {
                _container = $('#' + myContainerId);
                $getE = function (b) { return _container.find(b); };
                var contElement = $getE('.psPortlet').get(0);
                if (contElement) {
                    _vm = new ViewModel(model);
                    try {
                        ko.applyBindings(_vm, contElement);
                    }
                    catch (err) {
                        alert(err);
                    }
                    setupElementBindings();
                }
        }
        return {
            init: init
        };
    }

    function init(myContainerId, model) {
        var paymentSource = new PaymentSource();
        paymentSource.init(myContainerId, model);
    }

    return {
        init: init
    };
    //*****************************************************
});