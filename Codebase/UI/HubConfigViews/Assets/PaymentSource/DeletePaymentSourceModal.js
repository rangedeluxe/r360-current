﻿define(["jquery", "ko", "frameworkUtils"], function ($, ko, framework) {

    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };

    var setupElementBindings = function () {

        $getE("#formCancel").click(function () { cancel(); return false; });
        $getE("#formSubmit").click(function () { deletePaymentType(); return false; });

    }

    var cancel = function () {
        framework.closeModal();
    }

    var closeModal = function () {
        framework.closeModal();
    }

    var deletePaymentType = function () {
    	var url = "/RecHubConfigViews/PaymentSource/DeletePaymentSource/";
        var data = {
            batchSourceKey: _vm.model.PaymentSource.BatchSourceKey
        };

        var toastParms = {
            sticky: true
        }

        var deletePaymentSourceCallback = function (serverResponse) {
            if (serverResponse.HasErrors) {
                framework.errorToast(_container,  serverResponse.Errors[0].Message, toastParms);
            }
            else {
                closeModal();
            }
        };

        framework.doJSON(url, $.toDictionary(data), deletePaymentSourceCallback);
    }

    //*****************************************************
    //*****************************************************
    //  View Model
    //*****************************************************
    var ViewModel = function (model) {
        var self = this;
        self.model = model;
        
        model.deleteLabel = 'Do you want to delete Payment Source "' + model.PaymentSource.ShortName + '"?';
    }
    //*****************************************************



    //*****************************************************
    //*****************************************************
    //  Initialization
    //*****************************************************
    var init = function (myContainerId, model) {
        $(function () {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b) };
            _vm = new ViewModel(model);

            setupElementBindings();

            ko.applyBindings(_vm, $getE('.deletePaymentSourceModal').get(0));

        });
    };

    return {
        "init": init
    }
    //*****************************************************
});
