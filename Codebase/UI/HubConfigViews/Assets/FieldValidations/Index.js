﻿define(["jquery", "ko", "frameworkUtils", "accounting", "wfs.gridbase", "wfs.treeselector"], function ($, ko, framework, accounting, WFSGridBase) {

    function ErrorHandlingBindingProvider() {
        var original = new ko.bindingProvider();

        //determine if an element has any bindings
        this.nodeHasBindings = original.nodeHasBindings;

        //return the bindings given a node and the bindingContext
        this.getBindings = function (node, bindingContext) {
            var result;
            try {
                result = original.getBindings(node, bindingContext);
            }
            catch (e) {
                alert("Error in binding: " + e.message);
            }

            return result;
        };
    };

    function FieldValidations() {
        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };


        var initTree = function () {
            if (_vm['treeSelector'] === undefined) {
                framework.showSpinner(_vm.labels.Loading);
                _vm['treeSelector'] = $getE("#treeSelector").wfsTreeSelector({ callback: treeSelectionChanged, entityURL: '/RecHubRaamProxy/api/entity', height: "487px", entitiesOnly: false, dataCallback: treeDataLoadedCallback });
            }
        }

        var treeDataLoadedCallback = function (resp, widget) {
            //Only hide the spinner if there is no entity that can be selected, otherwise the selection of the root entity will trigger workgroups to load and
            // eventually hide the spinner
            if (resp == undefined || resp == null || resp.id === undefined || resp.id === null)
                framework.hideSpinner();

            _vm['treeSelector'].wfsTreeSelector('initialize');
        };

        var treeSelectionChanged = function (entity) {
            refreshFieldValidations(entity);
        };

        var refreshFieldValidations = function (entity) {
            if (entity === undefined) {
                entity = _vm['entitySelector'].wfsTreeSelector('getSelection');
            }

            if (entity === null || entity.isNode !== true) {
                displayFieldValidations([]);
            }
            else {
            	var purl = "/RecHubConfigViews/FieldValidations/GetFieldValidations";
                var data = {
                    siteBankID: entity.id.split('|')[0],
                    workgroupID: entity.id.split('|')[1]
                };
                framework.showSpinner(_vm.labels.Loading);
                framework.doJSON(purl, data, validationsLoadedCallback);
            }
        };

        var displayFieldValidations = function (validations) {
            if (validations === undefined || validations === null)
                validations = [];

            var tmpId = 0;
            var displayValidations = $.map(validations, function (e) {
                return {
                    Id: ++tmpId,
                    TableType: e.TableType,
                    FieldName: e.FieldName,
                    BatchSourceKey: e.BatchSourceKey,
                    BatchPaymentTypeKey: e.BatchPaymentTypeKey,
                    BatchPaymentSubTypeKey: e.BatchPaymentSubTypeKey,
                    ValidationTypeKey: e.FieldValidations.ValidationTypeKey,
                    ModificationDateTime: WFSDateUtil.formatWFSDateTime(e.FieldValidations.ModificationDateTime),
                    ValueCount: e.FieldValidations.ValueCount
                };
            });

            _vm['validationsGrid'].setItems(displayValidations);
        };

        var validationsLoadedCallback = function (serverResponse) {
            framework.hideSpinner();
            if (serverResponse.Errors && serverResponse.Errors.length > 0) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else {
                displayFieldValidations(serverResponse.Data);
            }
        };

        var initGrid = function () {
            if (_vm['validationsGrid'] === undefined) {
                _vm.gridColumns = [
                    { Id: "colField", ColumnTitle: _vm.labels.Field, ColumnFieldName: "Field", Tooltip: _vm.labels.FieldTooltip, DataType: 1, width: 120, formatter: fieldRenderer, sortable: true },
                    { Id: "colSource", ColumnTitle: _vm.labels.PaymentSource, ColumnFieldName: "BatchSource", Tooltip: _vm.labels.BatchSourceTooltip, DataType: 1, width: 60, formatter: paymentSourceRenderer, sortable: true },
                    { Id: "colType", ColumnTitle: _vm.labels.PaymentType, ColumnFieldName: "PaymentType", Tooltip: _vm.labels.PaymentTypeTooltip, DataType: 1, width: 60, formatter: paymentTypeRenderer, sortable: true },
                    { Id: "colSubType", ColumnTitle: _vm.labels.SubType, ColumnFieldName: "PaymentSubType", Tooltip: _vm.labels.PaymentSubTypeTooltip, DataType: 1, width: 60, formatter: paymentSubTypeRenderer, sortable: true },
                    { Id: "colValidationType", ColumnTitle: _vm.labels.ValidationType, ColumnFieldName: "ValidationType", Tooltip: _vm.labels.ValidationTypeTooltip, DataType: 1, width: 60, formatter: validationTypeRenderer, sortable: true },
                    { Id: "colValueCount", ColumnTitle: _vm.labels.ValueCount, ColumnFieldName: "ValueCount", Tooltip: _vm.labels.ValueCountTooltip, DataType: 6, width: 60, sortable: true },
                    { Id: "colUpdatedTime", ColumnTitle: _vm.labels.UpdateTime, ColumnFieldName: "ModificationDateTime", Tooltip: _vm.labels.UpdatedTimeTooltip, DataType: 11, width: 120, sortable: true },
                ];

                var gridModel = {
                    gridOptions: {
                        //forceFitColumns: false
                        //nothing changed from default
                    },
                    implOptions: {
                        gridSelector: '#validationsGrid',
                        pagerSelector: '#validationsPager',
                        pageSize: 15,
                        displayPagerSettings: true,
                        data: [],
                        idFieldName: 'Id',
                        allowRowDelete: false,
                        allowShowDetail: true,
                        showDetailTooltip: _vm.labels.EditValidationsTooltip,
                        showDetailCallback: editSelectedFieldValidations,
                        allowColumnSorting: true,
                        allowDynamicHeight: false,
                        sortField: "Field",
                        columns: _vm.gridColumns,
                    },
                };

                _vm['validationsGrid'] = new WFSGridBase.GridBase();
                _vm['validationsGrid'].init(_container.attr('id'), gridModel);
            }
        };

        var fieldRenderer = function (row, cell, value, columnDef, dataContext) {
            return dataContext.TableType + ' - ' + dataContext.FieldName;
        };

        var paymentSourceRenderer = function (row, cell, value, columnDef, dataContext) {
            return getPaymentSourceDesc(dataContext.BatchSourceKey);
        };

        var getPaymentSourceDesc = function (sourceKey) {
            if (sourceKey === undefined || sourceKey === null)
                return "All";
            else if (_vm.sources !== null) {
                var source = ko.utils.arrayFirst(_vm.sources, function (e) { return e.ID === sourceKey; });
                if (source !== null) {
                    return source.Description;
                }
            }
            return sourceKey;
        };

        var paymentTypeRenderer = function (row, cell, value, columnDef, dataContext) {
            return getPaymentTypeDesc(dataContext.BatchPaymentTypeKey);
        };

        var getPaymentTypeDesc = function (paymentTypeKey) {
            if (paymentTypeKey === undefined || paymentTypeKey === null)
                return "All";
            else if (_vm.paymentTypes !== null) {
                var type = ko.utils.arrayFirst(_vm.paymentTypes, function (e) { return e.ID === paymentTypeKey; });
                if (type !== null) {
                    return type.Description;
                }
            }
            return paymentTypeKey;
        };

        var paymentSubTypeRenderer = function (row, cell, value, columnDef, dataContext) {
            return getPaymentSubTypeDesc(dataContext.BatchPaymentTypeKey, dataContext.BatchPaymentSubTypeKey);
        };

        var getPaymentSubTypeDesc = function (paymentTypeKey, paymentSubTypeKey) {
            if (paymentSubTypeKey === null || paymentSubTypeKey === undefined) {
                var type = null;
                if (_vm.paymentTypes !== null) {
                    type = ko.utils.arrayFirst(_vm.paymentTypes, function (e) { return e.ID === paymentTypeKey; });
                }
                return (type !== null && type.HasSubTypes) ? "All" : "";
            }
            else if (_vm.paymentSubTypes !== null) {
                var subType = ko.utils.arrayFirst(_vm.paymentSubTypes, function (e) { return e.BatchPaymentSubTypeKey === paymentSubTypeKey; });
                if (subType !== null) {
                    return subType.SubTypeDescription;
                }
            }
            return paymentSubTypeKey;
        };

        var validationTypeRenderer = function (row, cell, value, columnDef, dataContext) {
            return getValidationTypeDesc(dataContext.ValidationTypeKey);
        };

        var getValidationTypeDesc = function (validationTypeKey) {
            if (_vm.validationValueTypes !== null) {
                var type = ko.utils.arrayFirst(_vm.validationValueTypes, function (e) { return e.ID === validationTypeKey; });
                if (type !== null) {
                    return type.Description;
                }
            }
            return validationTypeKey;
        };

        var editSelectedFieldValidations = function (gridRow, col, dataIndex, item) {
            //if (item === undefined || item.ClientAccountKey === undefined) {
            //    framework.warningToast("A workgroup must be selected for editing.");
            //    return false;
            //}

            //maintainWorkgroup(item.ClientAccountKey);
            return false;
        };

        var refreshCallback = function () {
        	framework.loadByContainer('/RecHubConfigViews/FieldValidations', _container);
        };

        var sViewModel = function (model) {
            var self = this;

            self.labels = {
                PageRefresh: "Refresh the field validations list.",
                Loading: "Loading...",

                EditValidationsTooltip: "View/maintain the field validation information.",
                FieldTooltip: "The field the rule applies to.",
                BatchSourceTooltip: "The payment source the rule will be applied for.",
                PaymentTypeTooltip: "The payment type the rule will be applied for.",
                PaymentSubTypeTooltip: "The payment sub-type the rule will be applied for.",
                ValidationTypeTooltip: "The type of validation being performed for the list of values.",
                ValueCountTooltip: "The number of values assigned to the set.",
                UpdatedTimeTooltip: "The last time the field validations were updated.",

                Field: "Field",
                PaymentSource: "Payment Source",
                PaymentType: "Payment Type",
                SubType: "Sub Type",
                ValidationType: "Validation Type",
                ValueCount: "# of Values",
                UpdateTime: "Last Updated",
            };

            if (model.StaticDescriptions === undefined || model.StaticDescriptions === null) {
                self.sources = [];
                self.paymentTypes = [];
                self.paymentSubTypes = [];
                self.validationValueTypes = [];
            }
            else {
                self.sources = model.StaticDescriptions.BatchSources;
                self.paymentTypes = model.StaticDescriptions.BatchPaymentTypes;
                self.paymentSubTypes = model.StaticDescriptions.BatchPaymentSubTypes;
                self.validationValueTypes = model.StaticDescriptions.ValidationTableTypes;
            }
        };

        var setupElementBindings = function () {
            initTree();
            initGrid();
        };

        function init(myContainerId, model) {
            $(function () {
                _container = $('#' + myContainerId);
                $getE = function (b) { return _container.find(b); };

                ko.bindingProvider.instance = new ErrorHandlingBindingProvider();

                _vm = new sViewModel(model);
                ko.applyBindings(_vm, $getE('#fieldValidationsBase').get(0));

                setupElementBindings();
            });
        };


        return {
            init: init
        };
    }

    function init(myContainerId, model) {
        $(function () {
            var fieldValidations = new FieldValidations();
            fieldValidations.init(myContainerId, model);
        });
    }

    return {
        init: init
    };
});

