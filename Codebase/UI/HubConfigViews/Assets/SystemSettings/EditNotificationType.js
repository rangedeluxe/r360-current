﻿define(["jquery", "ko", "frameworkUtils"], function ($, ko, framework) {

    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };

    var setupElementBindings = function () {
        $getE("#formCancel").click(function () { cancel(); return false; });
        setFormValidation();
    }

    var setFormValidation = function () {
        $getE("#notificationTypeForm").validate({
            rules: {
                inputDescription: { required: true }
            },
            messages: {
                inputDescription: { required: "Description must be entered." }
            },
            submitHandler: function (form) {
                saveNotificationType();
            }
        });
    }

    var cancel = function () {
        framework.closeModal();
    }

    var closeModal = function () {
        framework.closeModal();
    }

    var saveNotificationType = function () {
        
    	var url = "/RecHubConfigViews/SystemSettings/AddOrUpdateNotificationType/";
    	var  notificationtype =  
    	{
    	    NotificationFileTypeKey: _vm.model.Data.NotificationFileTypeKey,
    	    FileType: _vm.model.Data.FileType,
    	    FileTypeDescription: _vm.model.Data.FileTypeDescription,
    	    IsEdit: _vm.model.IsEdit
    	};

        var saveNotificationTypeCallback = function (serverResponse) {
            
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, serverResponse.Errors[0].Message);
                framework.hideSpinner();
            }
            else {
                closeModal();
            }
        };

        framework.doJSON(url, notificationtype, saveNotificationTypeCallback);
    }

    var ViewModel = function (model) {
        var self = this;
        self.model = model;
        self.operation = model.IsEdit == false ? "Add Notification Type" : "Edit Notification Type";
        // Means we are adding a new notification, not updating.
        if (!model.Data)
            model.Data = {};
        
    }

    var init = function (myContainerId, model) {
        $(function () {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b) };
            _vm = new ViewModel(model);

            ko.applyBindings(_vm, $getE('.editNotificationTypeModal').get(0));
            
            setupElementBindings();
        });
    };

    return {
        "init": init
    }
});
