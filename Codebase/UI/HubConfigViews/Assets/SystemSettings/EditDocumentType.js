﻿define(["jquery", "ko", "frameworkUtils"], function ($, ko, framework) {

    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };

    var setupElementBindings = function () {
        $getE("#formCancel").click(function () { cancel(); return false; });
        setFormValidation();
    }

    var setFormValidation = function () {
        $getE("#documentTypeForm").validate({
            rules: {
                inputDescription: { required: true }
            },
            messages: {
                inputDescription: { required: "Description must be entered." }
            },
            submitHandler: function (form) {
                saveDocumentType();
            }
        });
    }

    var cancel = function () {
        framework.closeModal();
    }

    var closeModal = function () {
        framework.closeModal();
    }

    var saveDocumentType = function () {
        
    	var url = "/RecHubConfigViews/SystemSettings/UpdateDocumentType/";
        var data = {
            DocumentTypeKey: _vm.model.DocumentTypeKey,
            Description: _vm.model.Description,
            FileDescriptor: _vm.model.FileDescriptor
        };

        var saveDocumentTypeCallback = function (serverResponse) {
            
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, serverResponse.Errors[0].Message);
                framework.hideSpinner();
            }
            else {
                closeModal();
            }
        };

        framework.doJSON(url, $.toDictionary(data), saveDocumentTypeCallback);
    }

    var ViewModel = function (model) {
        var self = this;
        self.model = model;
    }

    var init = function (myContainerId, model) {
        $(function () {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b) };
            _vm = new ViewModel(model);

            ko.applyBindings(_vm, $getE('.editDocumentTypeModal').get(0));

            setupElementBindings();
        });
    };

    return {
        "init": init
    }
});
