﻿define(["jquery", "ko", "frameworkUtils", "wfs.select"], function ($, ko, framework) {

    var _me = this;
    var _container = {};
    var _vm = {};
    var _namePattern = /^[A-Za-z0-9 &,\.:'()_-]+$/;//allow names to be alphanumeric with spaces, ampersand, comma, period, colon, apostrophe, parenthesis, underscore and dash
    var $getE = function (b) { alert("Not initialized!"); };
    var _maxprintablerows = 5000;

    var findMatch = function (preferences, matchString) {
        return $.grep(preferences, function (pref) { return pref.Name == matchString; })[0];
    }

    var getDisplayMode = function (settingValue) {
        if (settingValue < 2)
            return 'Front and back';
        else
            return 'Front only';
    }

    var getBoolFromYN = function (settingValue) {
        if (settingValue.toLowerCase() == 'y')
            return true;
        else
            return false;
    }

    var setSelectedCheckImageDisplayMode = function (value) {
        _vm.selectedCheckImageDisplayMode = value;
    };

    var setSelectedDocumentImageDisplayMode = function (value) {
        _vm.selectedDocumentImageDisplayMode = value;
    };

    //*****************************************************
    //*****************************************************
    //  View Model
    //*****************************************************
    var ViewModel = function (model) {
        var self = this;
        self.model = model;

        self.displayOptions = [{ ID: 1, Name: 'Front and back' }, { ID: 2, Name: 'Front only' }];

        self.selectedCheckImageDisplayMode = null;
        self.selectedDocumentImageDisplayMode = null;

        self.prefRecordsPerPage = findMatch(model.Preferences.Data.Data, 'RecordsPerPage');
        self.recordsPerPage = ko.observable(self.prefRecordsPerPage.DefaultSetting);
        self.isRecordsOverride = ko.observable(!self.prefRecordsPerPage.IsSystem && self.prefRecordsPerPage.IsOnline);

        //self.prefMinutesToApply = findMatch(model.Preferences.Data.Data, 'ExceptionDeadlineBufferMinutes');
        //self.minutesToApply = ko.observable(self.prefMinutesToApply.DefaultSetting);

        self.prefDisplayBatchCueID = findMatch(model.Preferences.Data.Data, 'DisplayBatchCueIDOnline');
        self.isDisplayBatchQue = ko.observable(getBoolFromYN(self.prefDisplayBatchCueID.DefaultSetting));

        self.prefShowAcctSite = findMatch(model.Preferences.Data.Data, 'ShowLockboxSiteCodeOnline');
        self.isShowAcctSite = ko.observable(getBoolFromYN(self.prefShowAcctSite.DefaultSetting));

        self.prefShowBankId = findMatch(model.Preferences.Data.Data, 'ShowBankIDOnline');
        self.isShowBankId = ko.observable(getBoolFromYN(self.prefShowBankId.DefaultSetting));

        self.prefShowBatchCode = findMatch(model.Preferences.Data.Data, 'ShowBatchSiteCodeOnline');
        self.isShowBatchCode = ko.observable(getBoolFromYN(self.prefShowBatchCode.DefaultSetting));

        self.prefShowCheckSeq = findMatch(model.Preferences.Data.Data, 'ShowCheckSequenceOnline');
        self.isShowCheckSeq = ko.observable(getBoolFromYN(self.prefShowCheckSeq.DefaultSetting));

        self.prefShowTransSeq = findMatch(model.Preferences.Data.Data, 'ShowTransactionStubSeqOnline');
        self.isShowTransSeq = ko.observable(getBoolFromYN(self.prefShowTransSeq.DefaultSetting));

        self.prefCheckImageDisplayMode = findMatch(model.Preferences.Data.Data, 'CheckImageDisplayMode');
        self.prefDocumentImageDisplayMode = findMatch(model.Preferences.Data.Data, 'DocumentImageDisplayMode');
        self.prefDisplayRemitterName = findMatch(model.Preferences.Data.Data, 'DisplayRemitterNameInPDF');

        self.checkPDFText = ko.observable(getDisplayMode(self.prefCheckImageDisplayMode.DefaultSetting));
        self.displayPDFText = ko.observable(getDisplayMode(self.prefDocumentImageDisplayMode.DefaultSetting));
        self.isDisplayRemitterName = ko.observable(getBoolFromYN(self.prefDisplayRemitterName.DefaultSetting));
        self.isDisplayRemitterNameOverride = ko.observable(!self.prefDisplayRemitterName.IsSystem && self.prefDisplayRemitterName.IsOnline);

        self.prefMaxQueriesSaved = findMatch(model.Preferences.Data.Data, 'MaximumQueriesSaved');
        self.maxQueries = ko.observable(self.prefMaxQueriesSaved.DefaultSetting);

        self.prefMaxWorkgroups = findMatch(model.Preferences.Data.Data, 'MaximumWorkgroups');
        self.maxWorkgroups = ko.observable(self.prefMaxWorkgroups.DefaultSetting);

        self.prefMaxPrintableRows = findMatch(model.Preferences.Data.Data, 'MaxPrintableRows');
        self.maxPrintableRows = ko.observable(self.prefMaxPrintableRows.DefaultSetting);

        self.prefDefaultViewingDays = findMatch(model.Preferences.Data.Data, 'ViewingDays');
        self.defaultViewingDays = ko.observable(self.prefDefaultViewingDays.DefaultSetting);

        _maxprintablerows = model.MetaData.MaxPrintableRowsGovernor;
    }
    //*****************************************************



    //*****************************************************
    //*****************************************************
    //  Bindings
    //*****************************************************
    var setupElementBindings = function () {

        $getE('#cancelButton').on('click', function () {
            framework.closeModal();
            return false;
        });

        $getE('#saveButton').on('click', saveOnClickValidate);

        $('#checkImageDisplayModeSelect').wfsSelectbox({
            'items': _vm.displayOptions,
            'callback': setSelectedCheckImageDisplayMode,
            'displayField': 'Name',
            'idField': 'ID',
            'placeHolder': 'Select a Display Mode...',
            'width': 330
        });

        $('#documentImageDisplayModeSelect').wfsSelectbox({
            'items': _vm.displayOptions,
            'callback': setSelectedDocumentImageDisplayMode,
            'displayField': 'Name',
            'idField': 'ID',
            'placeHolder': 'Select a Display Mode...',
            'width': 330
        });
    }
    //*****************************************************



    //*****************************************************
    //*****************************************************
    //  Validation
    //*****************************************************
    var setFormValidation = function () {
        var validator = $getE("#editSystemSettingForm").validate({
            rules: {
                //inputMinutesToApply: { digits: true, min: 0, max: 120 },
                inputRecordsPerPage: { required: true, digits: true, min: 5, max: 255 },
                inputMaxQueries: { digits: true, min: 1, max: 1000 },
                inputMaxWorkgroups: { digits: true, min: 1, max: 10000 },
                inputMaxPrintableRows: { digits: true, min: 1, max: _maxprintablerows },
                inputDefaultViewingDays: { required: true, digits: true, min: 1, max: 9999 }
            },
            messages: {
                //inputMinutesToApply: { digits: 'Use only numbers.', min: 'Value must be greater than 0.', max: 'Value must be less than 120.' },
                inputRecordsPerPage: { required: 'Records Per Page is required.', digits: 'Use only numbers.', min: 'Value must be greater than 4.', max: 'Value must be less than 256.' },
                inputMaxQueries: { digits: 'Use only numbers.', min: 'Value must be greater than 0.', max: 'Value must be less than 1,000.' },
                inputMaxWorkgroups: { digits: 'Use only numbers.', min: 'Value must be greater than 0.', max: 'Value must be less than 10,000' },
                inputMaxPrintableRows: { digits: 'Use only numbers.', min: 'Value must be greater than 0.', max: 'Value must be less than ' + _maxprintablerows },
                inputDefaultViewingDays: { required:'Default Viewing days is required', digits: 'Use only numbers.', min: 'Value must be greater than 0.', max: 'Value must be less than 9,999' }
            },
            submitHandler: function (form) {
                if (_vm.selectedDocumentImageDisplayMode == null) { 
                    $getE('#displayModeSelectError').css('display', 'inline-block');
                    $getE('#displayModeSelectError').text('A display status must be selected.');
                    framework.hideSpinner();
                    return;
                }
                else if (_vm.selectedCheckImageDisplayMode == null) {
                    $getE('#checkModeSelectError').css('display', 'inline-block');
                    $getE('#checkModeSelectError').text('A display status must be selected.');
                    framework.hideSpinner();
                    return;
                }
                else
                    saveOnClick();
            },
            invalidHandler: function (event, validator) {
                // Submit was cancelled - errors should already be showing
                framework.hideSpinner();
            }
        });
    }
    //*****************************************************



    //*****************************************************
    //  Set Up Form
    //*****************************************************
    var setForm = function () {
        $getE('#checkImageDisplayModeSelect').wfsSelectbox('setValue', _vm.prefCheckImageDisplayMode.DefaultSetting == '0' ? 1 : _vm.prefCheckImageDisplayMode.DefaultSetting);
        $getE('#documentImageDisplayModeSelect').wfsSelectbox('setValue', _vm.prefDocumentImageDisplayMode.DefaultSetting == '0' ? 1 : _vm.prefDocumentImageDisplayMode.DefaultSetting);
    }
    //*****************************************************



    var saveOnClickValidate = function () {
        framework.showSpinner('Verifying...');
        $getE("#editSystemSettingForm").submit();
    };

    var saveOnClick = function () {
    	var url = "/RecHubConfigViews/SystemSettings/UpdatePreferences/";

        _vm.prefMaxQueriesSaved.DefaultSetting = _vm.maxQueries() == '' ? 0 : _vm.maxQueries();
        //_vm.prefMinutesToApply.DefaultSetting = _vm.minutesToApply() == '' ? 0 : _vm.minutesToApply();

        _vm.prefRecordsPerPage.DefaultSetting = _vm.recordsPerPage();
        _vm.prefRecordsPerPage.IsOnline = _vm.isRecordsOverride();

        _vm.prefDisplayBatchCueID.DefaultSetting = _vm.isDisplayBatchQue() ? 'Y' : 'N';
        _vm.prefShowAcctSite.DefaultSetting = _vm.isShowAcctSite() ? 'Y' : 'N';
        _vm.prefShowBankId.DefaultSetting = _vm.isShowBankId() ? 'Y' : 'N';
        _vm.prefShowBatchCode.DefaultSetting = _vm.isShowBatchCode() ? 'Y' : 'N';
        _vm.prefShowCheckSeq.DefaultSetting = _vm.isShowCheckSeq() ? 'Y' : 'N';
        _vm.prefShowTransSeq.DefaultSetting = _vm.isShowTransSeq() ? 'Y' : 'N';

        _vm.prefDisplayRemitterName.DefaultSetting = _vm.isDisplayRemitterName() ? 'Y' : 'N';
        _vm.prefDisplayRemitterName.IsOnline = _vm.isDisplayRemitterNameOverride();

        _vm.prefCheckImageDisplayMode.DefaultSetting = _vm.selectedCheckImageDisplayMode.ID;
        _vm.prefDocumentImageDisplayMode.DefaultSetting = _vm.selectedDocumentImageDisplayMode.ID;

        _vm.prefMaxQueriesSaved.DefaultSetting = _vm.maxQueries() == '' ? 10 : _vm.maxQueries();

        _vm.prefMaxWorkgroups.DefaultSetting = _vm.maxWorkgroups() == '' ? 500 : _vm.maxWorkgroups();

        _vm.prefMaxPrintableRows.DefaultSetting = _vm.maxPrintableRows() == '' ? _maxprintablerows : _vm.maxPrintableRows();

        _vm.prefDefaultViewingDays.DefaultSetting = _vm.defaultViewingDays();

        var prefs = [];
        prefs.push(_vm.prefMaxQueriesSaved);
        //prefs.push(_vm.prefMinutesToApply);
        prefs.push(_vm.prefRecordsPerPage);
        prefs.push(_vm.prefDisplayBatchCueID);
        prefs.push(_vm.prefShowAcctSite);
        prefs.push(_vm.prefShowBankId);
        prefs.push(_vm.prefShowBatchCode);
        prefs.push(_vm.prefShowCheckSeq);
        prefs.push(_vm.prefShowTransSeq);
        prefs.push(_vm.prefDisplayRemitterName);
        prefs.push(_vm.prefCheckImageDisplayMode);
        prefs.push(_vm.prefDocumentImageDisplayMode);
        prefs.push(_vm.prefMaxQueriesSaved);
        prefs.push(_vm.prefMaxWorkgroups);
        prefs.push(_vm.prefMaxPrintableRows);
        prefs.push(_vm.prefDefaultViewingDays);

        var data = { preferences: prefs };

        var saveOnClickCallback = function (serverResponse) {
            if (serverResponse.HasErrors) {
                framework.hideSpinner();
                framework.errorToast(_container, serverResponse.Errors[0].Message);
            }
            else {
                framework.closeModal();
            }
        };

        framework.restPOST(url, JSON.stringify(data), saveOnClickCallback);
    }



    //*****************************************************
    //*****************************************************
    //  Initialization
    //*****************************************************
    var init = function (myContainerId, model) {
        $(function () {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b) };
            _vm = new ViewModel(model);

            ko.applyBindings(_vm, $getE('#editSystemSettingsModal').get(0));

            setupElementBindings();

            setFormValidation();

            setForm();

        });
    };

    return {
        "init": init
    }
    //*****************************************************
});
