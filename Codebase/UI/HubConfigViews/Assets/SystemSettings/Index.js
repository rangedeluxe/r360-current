﻿define(["jquery", "ko", "frameworkUtils", "accounting",
    "slickgrid/lib/jquery.event.drag-2.3.0",
    "slickgrid/lib/jquery.event.drop-2.3.0",
    "slickgrid/slick.core",
    "slickgrid/plugins/slick.cellrangeselector",
    "slickgrid/plugins/slick.cellselectionmodel",
    "slickgrid/plugins/slick.rowselectionmodel",
    "slickgrid/plugins/slick.autotooltips",
    "slickgrid/controls/slick.pager",
    "slickgrid/slick.dataview",
    "slickgrid/slick.formatters",
    "slickgrid/slick.editors",
    "slickgrid/slick.grid","jquery.validate"], function ($, ko, framework, accounting) {

        function SystemSettings() {

            var _me = this;
            var _container = {};
            var _vm = {};
            var _docgrid;
            var _docdataview;
            var _notgrid;
            var _notdataview;
            var $getE = function (b) { alert("Not initialized!"); };

            var findMatch = function (preferences, matchString) {
                return $.grep(preferences, function (pref) { return pref.Name == matchString; })[0];
            }

            var getDisplayMode = function (settingValue) {

                if (settingValue < 2)
                    return 'Front and back';
                else
                    return 'Front only';
            }

            var getBoolFromYN = function (settingValue) {
                if (settingValue.toLowerCase() == 'y')
                    return 'Yes';
                else
                    return 'No';
            }

            //*****************************************************
            //*****************************************************
            //  Setup Bindings
            //*****************************************************
            var setupElementBindings = function () {

                $getE('#editSystemSettings').on('click', function () {
                    openEditSystemSettingsModal();
                });

                $getE('#system-settings-tab').on('click', function () {
                    doRefresh();
                });

                $getE('#document-type-tab').on('click', function () {
                    doRefreshDocumentTab();
                });

                $getE('#notification-type-tab').on('click', function () {
                    doRefreshNotificationTab();
                });

                $getE('#add-notification-button').on('click', function () {
                    openNotificationEditModal(null, false);
                });

                doRefresh();
            }
            //*****************************************************



            //*****************************************************
            //*****************************************************
            //  Open Modal
            //*****************************************************
            var openEditSystemSettingsModal = function () {
                var url = '/RecHubConfigViews/SystemSettings/EditSystemSettings/';

                var desiredWidth = 900;
                var modalOptions = {
                    title: 'Edit Display Options', width: desiredWidth, position: [($(window).width() - desiredWidth) / 2, 25],
                    closeCallback: doRefresh
                }
              
                framework.loadByContainer(url, framework.getModalContent(), null, framework.openModal(modalOptions));
            }

            //*****************************************************
            //*****************************************************
            //  Modal Callback
            //*****************************************************
            var doRefresh = function () {
                var preferencesURL = "/RecHubConfigViews/SystemSettings/GetPreferences/";
                framework.doJSON(preferencesURL, {}, refreshCallback);
                framework.showSpinner('Loading...');
            };
            //*****************************************************

            //*****************************************************
            //*****************************************************
            //  NotificationType Callback
            //*****************************************************
            var doRefreshNotificationTab = function () {
                var documentsURL = "/RecHubConfigViews/SystemSettings/GetNotificationTypes/";
                framework.doJSON(documentsURL, {}, notificationTypesCallback);
                framework.showSpinner('Loading...');
            };
            //*****************************************************

            //*****************************************************
            //*****************************************************
            //  DocumentType Callback
            //*****************************************************
            var doRefreshDocumentTab = function () {
                var documentsURL = "/RecHubConfigViews/SystemSettings/GetDocumentTypes/";
                framework.doJSON(documentsURL, {}, documentTypesCallback);
                framework.showSpinner('Loading...');
            };
            //*****************************************************

            //*****************************************************
            //  Default options for the SlikGrid
            //*****************************************************
            var getOptions = function () {
                return {
                    enableColumnReorder: false,
                    enableCellNavigation: false,
                    fullWidthRows: true,
                    forceFitColumns: true,
                    autoHeight: true,
                    asyncEditorLoading: false,
                    autoEdit: false,
                    editable: false
                };
            };
            //*****************************************************

            //*****************************************************
            //*****************************************************
            //  Page Refresh Callback for Notification's Tab
            //*****************************************************
            var notificationTypesCallback = function (serverResponse) {
                if (serverResponse.HasErrors || !serverResponse.Data || serverResponse.Data.length < 1) {
                    framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
                }
                else {
                    _vm.notificationTypes = serverResponse.Data.Data;
                    _vm.canEditNotifications(serverResponse.Data.CanEditNotifications);

                    if (!_notgrid) {
                        // New notification grid
                        _notdataview = new Slick.Data.DataView({
                            inlineFilters: true
                        });

                        _notdataview.setPagingOptions({
                            pageSize: 15,
                        });

                        var editFormatter = function (row, cell, value, columnDef, dataContext) {
                            if (_vm.canEditNotifications())
                                return "<a class='tableIcon' id='" + value + "' title='Edit Notification Type' href='#_'><span style='margin: 0 auto; display: block;'><i class='fa fa-edit'></i></span></a>";
                            else
                                return "";
                        };

                        var _notcolumns = [
                            { name: "File Type", field: "FileType", id: "FileType", sortable: true },
                            { name: "Description", field: "FileTypeDescription", id: "FileTypeDescription", sortable: true }
                        ];
                        var editColNotifications = { name: "", field: "NotificationFileTypeKey", id: "NotificationFileTypeKey", sortable: false, headerCssClass: "text-center", formatter: editFormatter, width: 30, minWidth: 30, maxWidth: 30 };
                        if (_vm.canEditNotifications())
                            _notcolumns.unshift(editColNotifications);

                        _notdataview.beginUpdate();
                        _notdataview.setItems(_vm.notificationTypes, "FileType");
                        _notdataview.endUpdate();
                        _notgrid = new Slick.Grid($getE("#notification-types-grid"), _notdataview, _notcolumns, getOptions());
                        var pager = new Slick.Controls.Pager(_notdataview, _notgrid, $getE("#notification-pager"));
                        if (_vm.canEditNotifications()) {
                            _notgrid.onClick.subscribe(function (e, args) {
                                var item = _notgrid.getDataItem(args.row);
                                var colDef = _notgrid.getColumns()[args.cell];
                                if (colDef.id === "NotificationFileTypeKey") {
                                    if (item && !item.rows) {
                                        openNotificationEditModal(item, true);
                                    }
                                }
                            });
                        }
                        _notdataview.onPagingInfoChanged.subscribe(function (e, pagingInfo) {
                            var isLastPage = pagingInfo.pageNum == pagingInfo.totalPages - 1;
                            var enableAddRow = isLastPage || pagingInfo.pageSize == 0;
                            var options = _notgrid.getOptions();
                        });

                        $($getE('#notification-grid')).bind('destroyed', function () {
                            $(window).unbind('resize', resizeGridNoParameters);
                            _notgrid.destroy();
                        });

                        _notdataview.onRowCountChanged.subscribe(function (e, args) {
                            _notgrid.updateRowCount();
                            resizeGrid(_notgrid);
                        });
                        _notdataview.onRowsChanged.subscribe(function (e, args) {
                            resizeGrid(_notgrid);
                        });

                        _notgrid.onSort.subscribe(function (e, args) {
                            gridSorter(_notgrid, _notdataview, args.sortCol.id, args.sortCol.field, args.sortAsc);
                        });

                        // Default sorting column.
                        _notgrid.setSortColumn("FileType");
                    }
                    else {
                        // Update notification grid
                        _notdataview.beginUpdate();
                        _notdataview.setItems(_vm.notificationTypes, "FileType");
                        _notdataview.endUpdate();
                        _notdataview.reSort();
                    }

                    var sortcol = _notgrid.getSortColumns()[0];
                    gridSorter(_notgrid, _notdataview, sortcol.columnId, sortcol.columnId, sortcol.sortAsc);
                    _notgrid.invalidate();
                }
                framework.hideSpinner();
            };
            //*****************************************************

            var resizeGrid = function (grid) {
                grid.invalidate();
                grid.render();
                grid.resizeCanvas();
            };

            var gridSorter = function (grid, dataView, colId, colField, sortAsc) {
                _vm.sortdir = sortAsc;
                _vm.sortcol = colId;
                _vm.sortField = colField;

                // using native sort with comparer
                // preferred method but can be very slow in IE with huge datasets
                dataView.sort(function (a, b) {
                    var colField = _vm.sortField;
                    var x = a[colField], y = b[colField];

                    if (typeof x !== "string" || x.toLowerCase() === y.toLowerCase()) {
                        return (x == y ? 0 : (x > y ? 1 : -1));
                    }
                    else {
                        return (x.toLowerCase() === y.toLowerCase() ? 0 : (x.toLowerCase() > y.toLowerCase() ? 1 : -1));
                    }
                }, sortAsc);
                resizeGrid(grid);
            }

            //*****************************************************
            //*****************************************************
            //  Page Refresh Callback for Document's Tab
            //*****************************************************
            var documentTypesCallback = function (serverResponse) {
                if (serverResponse.HasErrors || !serverResponse.Data || serverResponse.Data.length < 1) {
                    framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
                }
                else {
                    _vm.documentTypes = serverResponse.Data.Data;
                    _vm.canEditDocuments = serverResponse.Data.CanEditDocuments;

                    if (!_docgrid) {
                        // New document grid

                        _docdataview = new Slick.Data.DataView({
                            inlineFilters: true
                        });

                        _docdataview.setPagingOptions({
                            pageSize: 15,
                        });

                        var editFormatter = function (row, cell, value, columnDef, dataContext) {
                            if (_vm.canEditDocuments && dataContext.IsReadOnly === false)
                                return "<a class='tableIcon' id='" + value + "' title='Edit Document Type' href='#_'><span style='margin: 0 auto; display: block;'><i class='fa fa-edit'></i></span></a>";
                            else
                                return "";
                        };

                        var _doccolumns = [
                            { name: "File Descriptor", field: "FileDescriptor", id: "FileDescriptor", sortable: true },
                            { name: "Description", field: "Description", id: "Description", sortable: true }
                        ];

                        if (_vm.canEditDocuments) {
                            var modCol = { name: "", field: "DocumentTypeKey", id: "DocumentTypeKey", sortable: false, headerCssClass: "text-center", formatter: editFormatter, width: 30, minWidth: 30, maxWidth: 30 };
                            _doccolumns.unshift(modCol);
                        }

                        _docdataview.beginUpdate();
                        _docdataview.setItems(_vm.documentTypes, "FileDescriptor");
                        _docdataview.endUpdate();
                        _docgrid = new Slick.Grid($getE("#document-types-grid"), _docdataview, _doccolumns, getOptions());
                        _docgrid.onClick.subscribe(function (e, args) {
                            var item = _docgrid.getDataItem(args.row);
                            var colDef = _docgrid.getColumns()[args.cell];
                            if (colDef.id === "DocumentTypeKey") {
                                if (item && !item.rows) {
                                    if (_vm.canEditDocuments && item.IsReadOnly === false) {
                                        openDocumentEditModal(item);
                                    }
                                }
                            }
                        });

                        var pager = new Slick.Controls.Pager(_docdataview, _docgrid, $getE("#document-pager"));

                        _docdataview.onPagingInfoChanged.subscribe(function (e, pagingInfo) {
                            var isLastPage = pagingInfo.pageNum == pagingInfo.totalPages - 1;
                            var enableAddRow = isLastPage || pagingInfo.pageSize == 0;
                            var options = _docgrid.getOptions();
                        });

                        $($getE('#document-grid')).bind('destroyed', function () {
                            $(window).unbind('resize', resizeGridNoParameters);
                            _docgrid.destroy();
                        });

                        _docdataview.onRowCountChanged.subscribe(function (e, args) {
                            _docgrid.updateRowCount();
                            resizeGrid(_docgrid);
                        });
                        _docdataview.onRowsChanged.subscribe(function (e, args) {
                            resizeGrid(_docgrid);
                        });

                        _docgrid.onSort.subscribe(function (e, args) {
                            gridSorter(_docgrid, _docdataview, args.sortCol.id, args.sortCol.field, args.sortAsc);
                        });

                        // Default sorting column.
                        _docgrid.setSortColumn("FileDescriptor");
                    }
                    else {
                        // Update document grid
                        _docdataview.beginUpdate();
                        _docdataview.setItems(_vm.documentTypes, "FileDescriptor");
                        _docdataview.endUpdate();
                        _docdataview.reSort();

                    }

                    var sortcol = _docgrid.getSortColumns()[0];
                    gridSorter(_docgrid, _docdataview, sortcol.columnId, sortcol.columnId, sortcol.sortAsc);
                    _docgrid.invalidate();
                }
                framework.hideSpinner();
            };
            //*****************************************************

            //*****************************************************
            //  Open Notification Modal Dialog
            //*****************************************************
            var openNotificationEditModal = function (notificationtype, isedit) {
                var url = '/RecHubConfigViews/SystemSettings/EditNotificationType/';
                var ww = $(window).width() / 2 - 350;
                var modalOptions = {
                    title: 'Edit Notification Type', width: '600', height: '300', position: [ww, 180],
                    closeCallback: doRefreshNotificationTab
                }
                var data = notificationtype;
                framework.loadByContainer(url, framework.getModalContent(), data, framework.openModal(modalOptions));
            }
            //*****************************************************

            //*****************************************************
            //  Open Document Modal Dialog
            //*****************************************************
            var openDocumentEditModal = function (documenttype) {
                var url = '/RecHubConfigViews/SystemSettings/EditDocumentType/';
                var ww = $(window).width() / 2 - 350;
                var modalOptions = {
                    title: 'Edit Document Type', width: '600', height: '300', position: [ww, 180],
                    closeCallback: doRefreshDocumentTab
                }
                var data = documenttype;
                framework.loadByContainer(url, framework.getModalContent(), data, framework.openModal(modalOptions));
            }
            //*****************************************************

            //*****************************************************
            //*****************************************************
            //  Page Refresh Callback
            //*****************************************************
            var refreshCallback = function (serverResponse) {
                if (serverResponse.HasErrors || !serverResponse.Data || serverResponse.Data.length < 1) {
                    framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
                }
                else {
                    try {
                        _vm.preferences = serverResponse.Data;

                        _vm.prefRecordsPerPage = findMatch(_vm.preferences, 'RecordsPerPage');

                        _vm.recordsPerPage(_vm.prefRecordsPerPage.DefaultSetting);
                        _vm.isRecordsOverride(!_vm.prefRecordsPerPage.IsSystem && _vm.prefRecordsPerPage.IsOnline ? 'Yes' : 'No');

                        _vm.prefCheckImageDisplayMode = findMatch(_vm.preferences, 'CheckImageDisplayMode');
                        _vm.prefDocumentImageDisplayMode = findMatch(_vm.preferences, 'DocumentImageDisplayMode');
                        _vm.prefDisplayRemitterName = findMatch(_vm.preferences, 'DisplayRemitterNameInPDF');

                        _vm.checkPDFText(getDisplayMode(_vm.prefCheckImageDisplayMode.DefaultSetting));
                        _vm.displayPDFText(getDisplayMode(_vm.prefDocumentImageDisplayMode.DefaultSetting));
                        _vm.isDisplayRemitterName(getBoolFromYN(_vm.prefDisplayRemitterName.DefaultSetting));
                        _vm.isDisplayRemitterNameOverride(!_vm.prefDisplayRemitterName.IsSystem && _vm.prefDisplayRemitterName.IsOnline ? 'Yes' : 'No');

                        //_vm.prefMinutesToApply = findMatch(_vm.preferences, 'ExceptionDeadlineBufferMinutes');
                        //_vm.minutesToApply(_vm.prefMinutesToApply.DefaultSetting);

                        _vm.prefDisplayBatchCueID = findMatch(_vm.preferences, 'DisplayBatchCueIDOnline');
                        _vm.isDisplayBatchQue(getBoolFromYN(_vm.prefDisplayBatchCueID.DefaultSetting));

                        _vm.prefShowAcctSite = findMatch(_vm.preferences, 'ShowLockboxSiteCodeOnline');
                        _vm.isShowAcctSite(getBoolFromYN(_vm.prefShowAcctSite.DefaultSetting));

                        _vm.prefShowBankId = findMatch(_vm.preferences, 'ShowBankIDOnline');
                        _vm.isShowBankId(getBoolFromYN(_vm.prefShowBankId.DefaultSetting));

                        _vm.prefShowBatchCode = findMatch(_vm.preferences, 'ShowBatchSiteCodeOnline');
                        _vm.isShowBatchCode(getBoolFromYN(_vm.prefShowBatchCode.DefaultSetting));

                        _vm.prefShowCheckSeq = findMatch(_vm.preferences, 'ShowCheckSequenceOnline');
                        _vm.isShowCheckSeq(getBoolFromYN(_vm.prefShowCheckSeq.DefaultSetting));

                        _vm.prefShowTransSeq = findMatch(_vm.preferences, 'ShowTransactionStubSeqOnline');
                        _vm.isShowTransSeq(getBoolFromYN(_vm.prefShowTransSeq.DefaultSetting));

                        _vm.prefMaxQueriesSaved = findMatch(_vm.preferences, 'MaximumQueriesSaved');
                        _vm.maxQueries(_vm.prefMaxQueriesSaved.DefaultSetting);

                        _vm.prefMaxWorkgroups = findMatch(_vm.preferences, 'MaximumWorkgroups');
                        _vm.maxWorkgroups(_vm.prefMaxWorkgroups.DefaultSetting);

                        _vm.prefMaxPrintableRows = findMatch(_vm.preferences, 'MaxPrintableRows');
                        _vm.maxPrintableRows(_vm.prefMaxPrintableRows.DefaultSetting);

                        _vm.prefDefaultViewingDays = findMatch(_vm.preferences, 'ViewingDays');
                        _vm.defaultViewingDays(_vm.prefDefaultViewingDays.DefaultSetting);
                    }
                    catch (err) {
                        framework.errorToast(_container, 'Error loading System Settings.  Some settings did not load properly.');
                    }
                }
                framework.hideSpinner();
            };
            //*****************************************************


            //*****************************************************
            //*****************************************************
            //  View Model
            //*****************************************************
            var viewModel = function (model) {
                var self = this;
                self.model = model;

                self.preferences = ko.observableArray([]);
                self.CanEditSystemSettings = ko.observable(model.CanEditSystemSettings);

                self.prefRecordsPerPage = null;
                self.recordsPerPage = ko.observable(null);
                self.isRecordsOverride = ko.observable(null);

                self.prefCheckImageDisplayMode = null;
                self.prefDisplayRemitterName = null;
                self.prefDocumentImageDisplayMode = null;
                self.checkPDFText = ko.observable(null);
                self.isDisplayRemitterName = ko.observable(null);
                self.isDisplayRemitterNameOverride = ko.observable(null);
                self.displayPDFText = ko.observable(null);

                self.prefMinutesToApply = null;
                //self.minutesToApply = ko.observable(null);

                self.prefDisplayBatchCueID = null;
                self.isDisplayBatchQue = ko.observable(null);

                self.prefShowAcctSite = null;
                self.isShowAcctSite = ko.observable(null);

                self.prefShowBankId = null;
                self.isShowBankId = ko.observable(null);

                self.prefShowBatchCode = null;
                self.isShowBatchCode = ko.observable(null);

                self.prefShowCheckSeq = null;
                self.isShowCheckSeq = ko.observable(null);

                self.prefShowTransSeq = null;
                self.isShowTransSeq = ko.observable(null);

                self.prefMaxQueriesSaved = null;
                self.maxQueries = ko.observable(null);

                self.prefMaxWorkgroups = null;
                self.maxWorkgroups = ko.observable(null);

                self.prefMaxPrintableRows = null;
                self.maxPrintableRows = ko.observable(null);

                self.prefDefaultViewingDays = null;
                self.defaultViewingDays = ko.observable(null);

                self.canEditNotifications = ko.observable(false);

            }
            //*****************************************************


            //*****************************************************
            //*****************************************************
            //  Initialization
            //*****************************************************
            function init(myContainerId, model) {
                $(function () {
                    _container = $('#' + myContainerId);
                    $getE = function (b) { return _container.find(b); };
                    var contElement = $getE('.systemSettingsPortlet').get(0)
                    if (contElement) {
                        _vm = new viewModel(model);

                        ko.applyBindings(_vm, contElement);

                        //when someone is ready to standardize the tabs to bootstrap instead of jqueryui, add a header above the tabs,
                        // and get the page to be left justified, etc, all they need to do to take this to bootstrap tabs it remove
                        // this line here (and you can remove the tabbase id from the cshtml)
                        // that just leaves creating the header and left justifying everything, turning the standard html tables
                        // into *probably* bootstrap panels with rows/columns instead of td/tr
                        $getE("#tabbase").tabs();

                        setupElementBindings();
                    }
                });
            }
            return {
                init: init
            }
        }

        function init(myContainerId, model) {
            $(function () {
                var systemSettings = new SystemSettings();
                systemSettings.init(myContainerId, model);
            });
        }

        return {
            init: init
        }
        //*****************************************************
    });