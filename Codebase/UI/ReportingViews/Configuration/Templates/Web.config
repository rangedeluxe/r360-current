<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="system.identityModel" type="System.IdentityModel.Configuration.SystemIdentityModelSection, System.IdentityModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
    <section name="system.identityModel.services" type="System.IdentityModel.Services.Configuration.SystemIdentityModelServicesSection, System.IdentityModel.Services, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
    <section name="StructureMap" type="StructureMap.Configuration.StructureMapConfigurationSection,StructureMap" />
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" requirePermission="false" />
    <section name="wfs.raam.trustedsubsystem.credential" type="Wfs.Raam.Core.Configuration.TrustedSubsystemCredentialSection, Wfs.Raam.Core"/>
    <section name="wfs.raam.core" type="Wfs.Raam.Core.Configuration.CoreSettingsSection, Wfs.Raam.Core"/>
  </configSections>
  <appSettings>
    <add key="webpages:Version" value="3.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="PreserveLoginUrl" value="true" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <add key="siteKey" value="IPOnline" />
    <add key="localIni" value="C:\WFSApps\RecHub\bin2\RecHub.ini" />
    <add key="WCFConfigLocation" value="C:\WFSApps\RecHub\bin2\WCFClients.web.config" />
    <add key="LogFilePath" value="C:\WFSApps\logs\{0:yyyyMMdd}_RecHubReportingViews_log.txt"/>
    <add key="LogFileMaxSize" value="2048"/>
    <add key="LoggingDepth" value="2"/>
  </appSettings>
  <system.web>
    <authentication mode="None" />
    <httpRuntime targetFramework="4.6" />
    <compilation debug="true" targetFramework="4.6" />

    <!-- Should not be present for this Relying Party-->
    <!--<authorization>
      <deny users="?"/>
    </authorization>-->
    <pages controlRenderingCompatibilityVersion="4.0">
      <namespaces>
        <add namespace="System.Web.Helpers" />
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Web.WebPages" />
      </namespaces>
    </pages>

    <!-- Required for RAAM Token Cache -->
    <machineKey configSource="webMachineKey.config" />
  </system.web>
  <system.webServer>
    <httpProtocol>
      <customHeaders>
        <clear/>
        <add name="X-UA-Compatible" value="IE=edge"/>
        <add name="Cache-Control" value="no-cache, no-store" />
        <add name="Pragma" value="no-cache" />
        <add name="Expires" value="-1" />
      </customHeaders>
    </httpProtocol>
    <handlers>
      <remove name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" />
      <remove name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" />
      <remove name="ExtensionlessUrlHandler-Integrated-4.0" />
      <add name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness32" responseBufferLimit="0" />
      <add name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness64" responseBufferLimit="0" />
      <remove name="OPTIONSVerbHandler" />
      <remove name="TRACEVerbHandler" />
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
    </handlers>
    <modules>
      <remove name="FormsAuthentication" />
      <!-- 
        DEFAULT WIF MODULES
        <add name="WSFederationAuthenticationModule" type="System.IdentityModel.Services.WSFederationAuthenticationModule, System.IdentityModel.Services, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" preCondition="managedHandler" />
		    <add name="SessionAuthenticationModule" type="System.IdentityModel.Services.SessionAuthenticationModule, System.IdentityModel.Services, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" preCondition="managedHandler" />
		  -->
      <add name="RaamFederationAuthenticationModule" type="Wfs.Raam.Core.Modules.RaamFederationAuthenticationModule, Wfs.Raam.Core" preCondition="managedHandler" />
      <add name="SlidingSessionAuthenticationModule" type="Wfs.Raam.Core.Modules.SlidingSessionAuthenticationModule, Wfs.Raam.Core" preCondition="managedHandler" />
    </modules>
  </system.webServer>
  <system.identityModel configSource="identity.web.config" />
  <system.identityModel.services configSource="identityServices.web.config" />

  <!-- Server-Side Token Caching attribute must be coordinated with all Relying Party Web Applications to be either on or off 
       If this setting is omitted then Server-Side Token Caching is enabled(true) by default.
  -->
  <wfs.raam.core serversidetokencachingenabled="true" applicationcontextswitchenabled="true"/>

  <!-- Put Logging configuration here -->
  <log4net>
    <root>
      <level value="Debug" />
      <appender-ref ref="RollingFileAppender" />
    </root>
    <appender name="RollingFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="C:\WFSApps\Logs\RecHubReportingViews.log" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="10240KB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] - %message%newline" />
      </layout>
    </appender>
  </log4net>
  <!-- Put StructureMap configuration here -->
  <StructureMap>
    <Assembly Name="Wfs.Logging" />
    <DefaultInstance PluginType="Wfs.Logging.ILogSource, Wfs.Logging" PluggedType="Wfs.Logging.Log4NetLogSource, Wfs.Logging" Scope="Singleton" />
  </StructureMap>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31BF3856AD364E35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31BF3856AD364E35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="ltaLog" publicKeyToken="5dd238c0e3af4d57" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-1.0.0.0" newVersion="1.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-11.0.0.0" newVersion="11.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.diagnostics>
    <sources>
      <!--  switchValue indicates what level of logging 
        Critical        - 	  Logs the following exceptions: OutOfMemoryException,ThreadAbortException,StackOverflowException,
                              ConfigurationErrorsException, SEHException,Application start errors,Failfast events,System hangs,
                              Poison messages: message traces that cause the application to fail
        Error           -     All exceptions are logged.
        Warning         -     Events logged: The application is receiving more requests than its throttling settings allow, 
                              the receiving queue is near its maximum configured capacity, timeout has exceeded.Credentials are rejected
        Information     -     Messages helpful for monitoring and diagnosing system status, measuring performance or profiling are generated.
        Verbose         -     Positive events are logged. Events that mark successful milestones.Low level events for both user code and servicing are emitted.
        ActivityTracing -     Flow events between processing activities and components.
        All             -     All events are logged.
        Off             -     No logs.
      -->
      <source name="System.ServiceModel" switchValue="Error,ActivityTracing" propagateActivity="true">
        <listeners>
          <add name="CircularTraceListener" />
        </listeners>
      </source>
      <source name="System.ServiceModel.MessageLogging">
        <listeners>
          <add name="messages"
               type="System.Diagnostics.XmlWriterTraceListener"
               initializeData="C:\Wfsapps\logs\RecHubReportingViews_Messages.svclog" />
        </listeners>
      </source>
      <source name="Wfs.Raam.Core" switchName="Wfs.Raam.Core.Switch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <clear/>
          <add name="WfsRaamCoreTraceListener" initializeData="C:\WFSApps\Logs\RecHubReportingViews_wfs.raam.core.svclog" type="System.Diagnostics.XmlWriterTraceListener" traceOutputOptions="Timestamp"/>
        </listeners>
      </source>
    </sources>
    <switches>
      <add name="Wfs.Raam.Core.Switch" value="Verbose"/>
    </switches>
    <sharedListeners>
      <add name="CircularTraceListener" type="WFS.System.Tracing.CircularTraceListener, CircularTraceListener, Version=1.0, Culture=neutral"
           initializeData="C:\Wfsapps\logs\RecHubReportingViews_Traces.svclog" maxFileSizeKB="1000" />
    </sharedListeners>
    <trace autoflush="true" />
  </system.diagnostics>
</configuration>
