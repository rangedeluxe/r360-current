﻿define(["jquery", "ko", "frameworkUtils", "moment","wfs.treeselector"], function ($, ko, framework, moment, wfsDateUtil) {

    var WFSDateUtil = (function () {

        // format date, validating input is in the correct date format (so, pretty much just returns what was already there?)
        function formatWFSDate(dateIn, format) {
            var defaultFormat = 'MM/DD/YYYY';
            if (format) {
                defaultFormat = format;
            }

            var aMoment = moment(dateIn, defaultFormat);
            var rvalue = 'Invalid date.';
            if (aMoment && aMoment.isValid()) {
                var inYear = aMoment.year();
                var todayYear = moment(new Date()).year() - 2000;
                // if two digit... then add a century
                if (inYear < (todayYear + 20))
                    aMoment.year(2000 + inYear);
                if (inYear >= (todayYear + 20) && inYear < 100)
                    aMoment.year(1900 + inYear);
                // century added so check for date earlier than 1900
                if (aMoment.year() < 1900)
                    aMoment.year(todayYear);
                rvalue = aMoment.format(defaultFormat);
            }
            return rvalue;
        };

        function isValid(dateIn, format, strictParsing) {
            var defaultFormat = 'MM/DD/YYYY';
            if (format) {
                defaultFormat = format;
            }
            var aMoment = moment(dateIn, defaultFormat, strictParsing);
            return (aMoment && aMoment.isValid());
        }

        function getWFSDate(dateIn) {
            return moment(dateIn);
        }

        function getYear(dateIn, format) {
            return moment(dateIn, format).year();
        }

        function getMonth(dateIn, format) {
            return moment(dateIn, format).month();
        }

        function getDay(dateIn, format) {
            return moment(dateIn, format).date();
        }

        function formatWFSTimeOfDay(dateIn, optionsIn) {
            var defaults = {
                includeDatePart: false,
                includeTimePart: true,
                includeMeridiem: true,
                includeSeconds: false
            };
            var options = $.extend({}, defaults, optionsIn);

            var aMoment = moment(dateIn);
            var rvalue = 'Invalid time.';
            if (aMoment) {
                rvalue = aMoment.format(getDateTimeFormat(options));
            }
            return rvalue;
        }

        function formatWFSDateTime(dateIn, optionsIn) {
            var aMoment = moment(dateIn);
            var rvalue = 'Invalid time.';
            if (aMoment) {
                rvalue = aMoment.format(getDateTimeFormat(optionsIn));
            }
            return rvalue;
        }

        function formatWFSDateForServer(dateIn) {
            var aMoment = moment(dateIn);
            var rvalue = null;
            if (aMoment) {
                rvalue = aMoment.toISOString();
            }
            return rvalue;
        }

        function getDateTimeFormat(optionsIn) {
            var format = "";
            var defaults = {
                includeDatePart: true,
                includeTimePart: true,
                includeMeridiem: true,
                includeSeconds: false
            };
            var options = $.extend({}, defaults, optionsIn);

            format = options.includeDatePart ? "MM/DD/YYYY" : "";
            if (options.includeTimePart) {
                format = format + (options.includeDatePart ? " " : "");
                format = format + "h:mm"
                format = format + (options.includeSeconds ? ":ss" : "");
                format = format + (options.includeMeridiem ? " a" : "");
            }
            return format;
        };

        return {
            formatWFSDate: formatWFSDate,
            getWFSDate: getWFSDate,
            getYear: getYear,
            getMonth: getMonth,
            getDay: getDay,
            isValid: isValid,
            formatWFSTimeOfDay: formatWFSTimeOfDay,
            formatWFSDateForServer: formatWFSDateForServer,
            formatWFSDateTime: formatWFSDateTime
        }

    }());

    function ParameterSelectionView() {

        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };
        var framework = require("frameworkUtils");
        var moment = require("moment");
        var executing = false;

        function formatWFSDate(dateIn, format) {
            var defaultFormat = 'MM/DD/YYYY';
            if (format) {
                defaultFormat = format;
            }

            var aMoment = moment(dateIn, defaultFormat);
            var rvalue = 'Invalid date.';
            if (aMoment && aMoment.isValid()) {
                var inYear = aMoment.year();
                var todayYear = moment(new Date()).year() - 2000;
                // if two digit... then add a century
                if (inYear < (todayYear + 20))
                    aMoment.year(2000 + inYear);
                if (inYear >= (todayYear + 20) && inYear < 100)
                    aMoment.year(1900 + inYear);
                // century added so check for date earlier than 1900
                if (aMoment.year() < 1900)
                    aMoment.year(todayYear);
                rvalue = aMoment.format(defaultFormat);
            }
            return rvalue;
        };

        var userDataCallback = function (data) {
            _vm.userSelector.wfsTreeSelector('initialize');
        };

        var raamUserDataCallback = function (data) {
            _vm.raamUserSelector.wfsTreeSelector('initialize');
        };

        var groupDataCallback = function (data) {
            _vm.groupSelector.wfsTreeSelector('initialize');
        };

        paymentSourcesLoadedCallback = function (serverResponse) {
            framework.hideSpinner();

            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else {
                var resp = [new PaymentSourceModel({ ShortName: '-- All --', ID: 0 })];
                $.each(serverResponse.Data, function (index, item) {
                    resp.push(new PaymentSourceModel(item));
                });
             
                _vm.paymentSources(resp);
                $.each(_vm.reportConfig.Parameters, function (index, item) {
                    if (item.ParameterType == 'PaymentSource') {
                       
                        _vm[item.ParameterName + '_selectedPaymentSource'](resp[0]);
                        var paymentSourceSelect = $getE('.paymentSourceSelect');
                        var callback = _vm[paymentSourceSelect.attr('id') + '_selectedPaymentSource'];

                        _vm.PaymentSourceSelectbox = paymentSourceSelect.wfsSelectbox({
                            items: _vm.paymentSources(),
                            callback: callback,
                            displayField: 'ShortName',
                            idField: 'ID'
                        });
                    }
                });

               
            }
        };

        eventTypesLoadedCallback = function (serverResponse) {
            framework.hideSpinner();

            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else {
                var resp = [new EventTypeModel({ ShortName: '-- All --', ID: 'All' })];
                $.each(serverResponse.Data, function (index, item) {
                    resp.push(new EventTypeModel({ ShortName: item, ID: item }));
                });
                _vm.eventTypes(resp);

                $.each(_vm.reportConfig.Parameters, function (index, item) {
                    if (item.ParameterType == 'EventType') {
                        _vm[item.ParameterName + '_selectedEventType'](resp[0]);
                        var eventTypesSelect = $getE('.eventTypesSelect');
                        var callback = _vm[eventTypesSelect.attr('id') + '_selectedEventType'];

                        _vm.EventTypesSelectbox = eventTypesSelect.wfsSelectbox({
                            items: _vm.eventTypes(),
                            callback: callback,
                            displayField: 'ShortName',
                            idField: 'ID'
                        });
                    }
                });
            }
        };

        raamEventTypesLoadedCallback = function (serverResponse) {
            framework.hideSpinner();

            if (!serverResponse && serverResponse.length == 0) {
                framework.errorToast(_container, framework.formatErrors('Unable to retrieve Event Types'));
            }
            else {
                var resp = [new EventTypeModel({ ShortName: '-- All --', ID: 'All' })];
                $.each(serverResponse, function (index, item) {
                    resp.push(new EventTypeModel({ ShortName: item, ID: item }));
                });
                _vm.raamEventTypes(resp);

                $.each(_vm.reportConfig.Parameters, function (index, item) {
                    if (item.ParameterType == 'RAAMEventType') {
                        _vm[item.ParameterName + '_selectedRaamEventType'](resp[0]);
                        var raamEventTypesSelect = $getE('.raamEventTypesSelect');
                        var callback = _vm[raamEventTypesSelect.attr('id') + '_selectedRaamEventType'];

                        _vm.RaamEventTypesSelectbox = raamEventTypesSelect.wfsSelectbox({
                            items: _vm.raamEventTypes(),
                            callback: callback,
                            displayField: 'ShortName',
                            idField: 'ID'
                        });
                    }
                });
            }
        };

        function PaymentSourceModel(model) {
            var self = this;

            self.ID = model.ID;
            self.ShortName = model.ShortName;
        };

        function EventTypeModel(model) {
            var self = this;

            self.ID = model.ID;
            self.ShortName = model.ShortName;
        };

        function ViewModel(model) {
            var self = this;
            self.reportConfig = model.Data;
            self.success = model.Success;
            self.errors = model.Errors;


            self.reportGenerationOptions = [{ ID: 1, Name: 'PDF' }, { ID: 2, Name: 'CSV' }];

            var loadingPaymentSource = new PaymentSourceModel({ ShortName: 'Loading Payment Sources...' });
            var loadingEventTypes = { ShortName: 'Loading Event Types...' };

            self.paymentSources = ko.observableArray([loadingPaymentSource]);
            self.eventTypes = ko.observableArray([loadingEventTypes]);
            self.raamEventTypes = ko.observableArray([loadingEventTypes]);

            $.each(self.reportConfig.Parameters, function (index, item) {
                if (item.ParameterType == 'User') {
                    self[item.ParameterName + '_selectedUser'] = ko.observable(null);
                    self[item.ParameterName + '_selectedEntity'] = ko.observable(null);
                }
                else if (item.ParameterType === "RAAMUser") {
                    self[item.ParameterName + '_selectedRAAMUser'] = ko.observable(null);
                    self[item.ParameterName + '_selectedRAAMEntity'] = ko.observable(null);
                }
                else if (item.ParameterType == 'PaymentSource') {
                    self[item.ParameterName + '_selectedPaymentSource'] = ko.observable(loadingPaymentSource);
                }
                else if (item.ParameterType == 'EventType') {
                    self[item.ParameterName + '_selectedEventType'] = ko.observable(loadingEventTypes);
                }
                else if (item.ParameterType == 'RAAMEventType') {
                    self[item.ParameterName + '_selectedRaamEventType'] = ko.observable(loadingEventTypes);
                }
                else if (item.ParameterType == 'ReportGenerationType') {
                    self[item.ParameterName + '_selectedReportGenerationType'] = ko.observable(null);
                } else if (item.ParameterType == 'Group') {
                    self[item.ParameterName + '_selectedGroup'] = ko.observable(null);
                }
            });
        };

        printCallback = function (serverResponse) {
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else {
                //gets rid of spaces to avoid problems with browser
                var reportName = _vm.reportConfig.ReportName.replace(/ /g, '-') + "." + _vm['GT_selectedReportGenerationType']().Name;
                window.open("/RecHubReportingViews/LoadReport?instanceId=" + serverResponse.Data + "&reportName=" + reportName, "_blank");
            }
            framework.hideSpinner();
            executing = false;
        };

        executeReport = function () {
            if (!executing) {
                executing = true;
                var reportExInfo = {
                    reportID: _vm.reportConfig.ReportID,
                    parameterValues: []
                };

                if (validateParameters(reportExInfo)) {
                    var url = '/RecHubReportingViews/Reporting/InvokeReportByIDAsync/';
                    framework.doJSON(url, $.toDictionary(reportExInfo), printCallback);
                    framework.showSpinner("Loading...");
                }
            }
            return false;
        };

        var newMomentDate = function(theDateIn) {
            return moment.utc(theDateIn, 'MM/DD/YYYY');
        }

        var swapDatesIfNeeded = function (reportExInfo) {
            var parameterType;
            var index;
            for (index = 0; index < reportExInfo.parameterValues.length; index++) {
                if ('SD' == reportExInfo.parameterValues[index].Name) {
                    parameterType = reportExInfo.parameterValues[index].Name;
                    break;
                }
            }
            if (parameterType == undefined)  //no dates found
                return;
            
            var beginDate = reportExInfo.parameterValues[index].Value;
            var endDate = reportExInfo.parameterValues[index + 1].Value;

            if (newMomentDate(beginDate).isAfter(newMomentDate(endDate))) {
                // we need to swap the dates
                var numberOfElementsToRemove = 2;
                var removedElements = reportExInfo.parameterValues.splice(index, numberOfElementsToRemove);
                //now add the values back in, but switch SD and ED
                reportExInfo.parameterValues.push({ Name: "SD", Value: endDate });
                reportExInfo.parameterValues.push({ Name: "ED", Value: beginDate });
            } 
        };

        validateParameters = function (reportExInfo) {
            //based on the report, get the necessary parameters
            $.each(_vm.reportConfig.Parameters, function (index, item) {
                var value;
                var name = item.ParameterName;

                if (item.ParameterType == 'BeginDate' || item.ParameterType == 'EndDate') {
                    value = formatWFSDate($getE('#' + item.ParameterName).wfsDatePicker('getDate'));
                }
                else if (item.ParameterType == 'User' && _vm[item.ParameterName + '_selectedUser']() != undefined) {
                    value = _vm[item.ParameterName + '_selectedUser']().value;
                }
                else if (item.ParameterType == 'User' && _vm[item.ParameterName + '_selectedEntity']() != undefined) {
                    value = _vm[item.ParameterName + '_selectedEntity']().id;
                    name = "ENT"; // swap out the name because the User Selector can be either a User or Entity
                }
                else if (item.ParameterType == 'RAAMUser') {
                    if (_vm[item.ParameterName + '_selectedRAAMUser']() != undefined)
                        value = _vm[item.ParameterName + '_selectedRAAMUser']().value;
                    else if (_vm[item.ParameterName + '_selectedRAAMEntity']() != undefined) {
                        value = _vm[item.ParameterName + '_selectedRAAMEntity']().id;
                        name = "ENT"; // swap out the name because the User Selector can be either a User or Entity
                    }
                }
                else if (item.ParameterType == 'Group' && _vm[item.ParameterName + '_selectedGroup']() != undefined) {
                    value = _vm[item.ParameterName + '_selectedGroup']().id;
                }
                else if (item.ParameterType == 'PaymentSource' && _vm[item.ParameterName + '_selectedPaymentSource']() != undefined) {
                    value = _vm[item.ParameterName + '_selectedPaymentSource']().ShortName;
                }
                else if (item.ParameterType == 'EventType' && _vm[item.ParameterName + '_selectedEventType']() != undefined) {
                    value = _vm[item.ParameterName + '_selectedEventType']().ShortName;
                    if (value == '-- All --') {
                        value == undefined;
                    }
                }
                else if (item.ParameterType == 'RAAMEventType' && _vm[item.ParameterName + '_selectedRaamEventType']() != undefined) {
                    value = _vm[item.ParameterName + '_selectedRaamEventType']().ShortName;
                    if (value == '-- All --') {
                        value == undefined;
                    }
                }
                else if (item.ParameterType == 'ReportGenerationType' && _vm[item.ParameterName + '_selectedReportGenerationType']() != undefined) {
                    value = _vm[item.ParameterName + '_selectedReportGenerationType']().Name;
                }

                if (value)
                    reportExInfo.parameterValues.push({ Name: name, Value: value });
            });
            swapDatesIfNeeded(reportExInfo);
            return true;
        };

        dateSelectionChanged = function (picker) {
        };

        setupElementBindings = function () {

            var datePickers = $getE('.parmDate');
            $.each(datePickers, function () {
                var picker = $(this);
                picker.wfsDatePicker({
                    callback: function () {
                        dateSelectionChanged(picker);
                    }
                });
            });
            var selectedTreeItem = function (selected, initial) {
                var title = null;
                _vm.USER_selectedUser(null);
                _vm.USER_selectedEntity(null);
                if (selected !== null && selected !== undefined) {
                    title = 'Selected: ' + selected['label'];
                    if (selected.isNode)
                        _vm.USER_selectedUser(selected);
                    else
                        _vm.USER_selectedEntity(selected);
                }
                _vm.userSelector.wfsTreeSelector('setTitle', title);
            };

            var selectedRaamUser = function (selected, initial) {
                var title = null;
                _vm.USER_selectedRAAMUser(null);
                _vm.USER_selectedRAAMEntity(null);
                if (selected !== null && selected !== undefined) {
                    title = 'Selected: ' + selected['label'];
                    if (selected.isNode)
                        _vm.USER_selectedRAAMUser(selected);
                    else
                        _vm.USER_selectedRAAMEntity(selected);
                }
                _vm.raamUserSelector.wfsTreeSelector('setTitle', title);
            };

            var selectedGroup = function (selected, initial) {
                var title = null;
                _vm.GRP_selectedGroup(null);
                if (selected && selected.isNode) {
                    title = 'Selected: ' + selected['label'];
                    _vm.GRP_selectedGroup(selected);
                }
                _vm.groupSelector.wfsTreeSelector('setTitle', title);
            };

            _vm.userSelector = $getE('#rptUser').wfsTreeSelector({
                useExpander: true,
                entityURL: '/RecHubRaamProxy/api/entityUser/',
                callback: selectedTreeItem,
                dataCallback: userDataCallback,
                expanderTitle: "Select User",
                entitiesOnly: false,
                icon: 'fa fa-user'
            });

            _vm.raamUserSelector = $getE('#rptRaamUser').wfsTreeSelector({
                useExpander: true,
                entityURL: '/RecHubRaamProxy/api/entityRaamUser/',
                callback: selectedRaamUser,
                dataCallback: raamUserDataCallback,
                expanderTitle: "Select User",
                entitiesOnly: false,
                icon: 'fa fa-user'
            });

            _vm.groupSelector = $getE('#rptGroup').wfsTreeSelector({
                useExpander: true,
                entityURL: '/RecHubRaamProxy/api/entityGroup/',
                callback: selectedGroup,
                dataCallback: groupDataCallback,
                expanderTitle: "Select Group",
                entitiesOnly: false,
                icon: 'fa fa-users',
                closeOnSelectNonNode: false,
                displayMessageOnSelectNonNode: true,
                messageOnselectNonNode: "Please select a single group."
            });

            var paymentSourceSelect = $getE('.paymentSourceSelect');
            if (paymentSourceSelect.length > 0) {
               /* this no longer works, values must now be bound in  paymentSourcesLoadedCallback **/
               /* var callback = _vm[paymentSourceSelect.attr('id') + '_selectedPaymentSource'];
                _vm.PaymentSourceSelectbox = paymentSourceSelect.wfsSelectbox({
                    items: _vm.paymentSources(),
                    callback: callback,
                    displayField: 'ShortName',
                    idField: 'ID'
                });*/

                var purl = "/RecHubReportingViews/Reports/GetPaymentSourceList";
                framework.doJSON(purl, {}, paymentSourcesLoadedCallback);
                framework.showSpinner("Loading...");
            };

            var eventTypesSelect = $getE('.eventTypesSelect');
            if (eventTypesSelect.length > 0) {
                var purl = "/RecHubReportingViews/Reports/GetEventTypeList";
                framework.doJSON(purl, {}, eventTypesLoadedCallback);
                framework.showSpinner("Loading...");
            };

            var raamEventTypesSelect = $getE('.raamEventTypesSelect');
            if (raamEventTypesSelect.length > 0) {
                var purl = "/RecHubRaamProxy/api/eventType";
                framework.restGET(purl, null, raamEventTypesLoadedCallback);
                framework.showSpinner("Loading...");
            };

            var reportGenerationTypeSelect = $getE('.generateAsSelect');
            if (reportGenerationTypeSelect) {
                var callback = _vm[reportGenerationTypeSelect.attr('id') + '_selectedReportGenerationType'];

                _vm.ReportGenerationTypeSelect = reportGenerationTypeSelect.wfsSelectbox({
                    items: _vm.reportGenerationOptions,
                    callback: callback,
                    displayField: 'Name',
                    idField: 'ID'
                });

                _vm.ReportGenerationTypeSelect.wfsSelectbox('setValue', _vm.reportGenerationOptions[0].id); // Preset to PDF
            };

        };

        this.init = function (myContainerId, model) {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b) };

            _vm = new ViewModel(model);

            var parameterSelection = $getE('#parameterSelection');
            ko.applyBindings(_vm, parameterSelection.get(0));

            setupElementBindings();

            $getE('#btnPrintReport').bind('click', executeReport);
        };
    }

    function init(myContainerId, model) {
        $(function () {
            var view = new ParameterSelectionView();
            view.init(myContainerId, model);
        });
    }

    return {
        init: init
    };
});