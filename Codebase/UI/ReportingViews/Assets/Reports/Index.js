﻿define(["jquery", "ko", "frameworkUtils", "accounting"], function ($, ko, framework, accounting) {

  function ReportingView() {

        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };

        function init(myContainerId, model) {
            // you need to do this... 
            _container = $('#' + myContainerId)
            $getE = function (b) { return _container.find(b) };

            _vm = {
                reportMenu: model.Data,
                success: model.Success,
                errors: model.Errors
            };

            ko.applyBindings(_vm, _container.get(0));

            if (_vm.success == false) {
                framework.errorToast(_container, framework.formatErrors(model.Errors));
            }
            else {
                var menuDiv = $getE('#reportMenu');
                menuDiv.accordion();

                var menuGroups = menuDiv.find('ul');
                $.each(menuGroups, function () {
                    var group = $(this);
                    var links = group.find('a');
                    $.each(links, function () {
                        var link = $(this);
                        var id = link.attr('id');
                        var container = $getE('#utilityContent')
                        var data = { reportID: id };
                        link.click(function () {
                        	framework.loadByContainer("/RecHubReportingViews/Reports/ParameterSelection", container, data);
                            return false;
                        });
                    });
                });
            }
        };

        return {
          init: init
        }
    }

    function get(myContainerId, model) {
      return new ReportingView();
    }

    return {
      get: get
    }
});