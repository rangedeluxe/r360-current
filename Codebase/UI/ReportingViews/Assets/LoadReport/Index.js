﻿var loadReportViewManager = (function () {

    function LoadReportView() {

        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };


        checkStatusCallback = function (serverResponse, callbackArgs) {
            if (serverResponse.HasErrors) {
                framework.hideSpinner();
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else if (serverResponse.Data == 1/*In Progress*/) {
                checkStatus(callbackArgs.instanceId);
            }
            else if (serverResponse.Data != 2/*Completed*/) {
                framework.hideSpinner();
                framework.errorToast(_container, "An error occurred while running the report.");
            }
            else {
                framework.hideSpinner();
                window.location.href = "/RecHubReportingViews/LoadReport/GetReportInstance?instanceId="
                                     + callbackArgs.instanceId + "&reportName=" + _vm.reportName;
            }
        };

        checkStatus = function (instanceId) {
            var purl = "/RecHubReportingViews/LoadReport/CheckReportCompletionStatus";
            var data = { instanceId: instanceId };
            var callback = checkStatusCallback;
            var callbackArgs = { instanceId: instanceId };

            framework.doJSON(purl, data, callback, callbackArgs);
        };

        this.init = function init(myContainerId, mod) {
            // you need to do this... 
           // _container = $('#' + myContainerId);
            $getE = function (b) { return myContainerId.find(b); };
            _vm.reportName = mod.reportName;
            framework.showSpinner('Loading...');
            checkStatus(mod.instanceId);
        };
    };

    return {
        get: function () { return new LoadReportView(); }
    };
}());
