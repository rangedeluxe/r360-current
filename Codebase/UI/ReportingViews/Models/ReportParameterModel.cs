﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ReportServicesClient;

namespace ReportingViews.Models
{
    public class ReportParameterModel
    {
        public int ReportID;
        public int OrderValue;
        public string DisplayName;
        public string ParameterName;
        public string ParameterType;
        public bool IsRequired;
        public bool AllowMultiple;

        private static Wfs.Logging.ILogSource _logger = null;
        public static Wfs.Logging.ILogSource Logger
        {
            get
            {
                if (_logger == null)
                    _logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(ReportParameterModel));
                return _logger;
            }
        }

        public static bool GetParametersForReport(ReportModel reportModel, out List<string> errors)
        {
            errors = null;
            reportModel.Parameters = null;
            try
            {
                ReportServiceManager manager = new ReportServiceManager();
                var response = manager.GetParametersForReport(reportModel.ReportID);
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }
                    
                reportModel.Parameters = response.ReportParameters.OrderBy(o => o.OrderValue).Select(o => ReportParameterModel.LoadFromDto(o)).ToList();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetParametersForReport\r\n" + ex.ToString());
                return false;
            }
        }

        #region Helper Methods

        public static ReportParameterModel LoadFromDto(ReportParameterDto parameterDto)
        {
            return new ReportParameterModel()
            {
                ReportID = parameterDto.ReportID,
                OrderValue = parameterDto.OrderValue,
                DisplayName = parameterDto.DisplayName,
                ParameterName = parameterDto.ParameterName,
                ParameterType = parameterDto.ParameterType.ToString(),
                IsRequired = parameterDto.IsRequired,
                AllowMultiple = parameterDto.AllowMultiple
            };
        }
        #endregion
    }
}