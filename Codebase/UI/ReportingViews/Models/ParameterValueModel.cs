﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingViews.Models
{
    public class ParameterValueModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
		public List<string> ValueList { get; set; }
    }
}