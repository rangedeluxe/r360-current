﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ReportServicesClient;

namespace ReportingViews.Models
{
    public class PaymentSourceModel
    {
        public int ID;
        public string ShortName;

        private static Wfs.Logging.ILogSource _logger = null;
        public static Wfs.Logging.ILogSource Logger
        {
            get
            {
                if (_logger == null)
                    _logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(AuditUserModel));
                return _logger;
            }
        }

        public static bool GetPaymentSourceList(out List<PaymentSourceModel> paymentSources, out List<string> errors)
        {
            errors = null;
            paymentSources = null;
            try
            {
                ReportServiceManager manager = new ReportServiceManager();
                var response = manager.GetPaymentSourceList();
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                paymentSources = response.AuditPaymentSources.OrderBy(o => o.ShortName).Select(o => PaymentSourceModel.LoadFromDto(o)).ToList();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetPaymentSourceList\r\n" + ex.ToString());
                paymentSources = null;
                return false;
            }
        }

        #region Helper Methods

        public static PaymentSourceModel LoadFromDto(AuditPaymentSourceDto paymentSourceDto)
        {
            return new PaymentSourceModel()
            {
                ID = paymentSourceDto.ID,
                ShortName = paymentSourceDto.ShortName
            };
        }
        #endregion
    }
}