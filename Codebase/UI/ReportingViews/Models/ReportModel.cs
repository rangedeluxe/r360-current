﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.ReportServicesClient;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360Shared;


namespace ReportingViews.Models
{
    public class ReportModel
    {
        public int ReportID { get; set; }
        public int GroupID { get; set; }
        public int OrderValue { get; set; }
        public string ReportName { get; set; }
        public List<ReportParameterModel> Parameters { get; set; }

        private static Wfs.Logging.ILogSource _logger = null;
        public static Wfs.Logging.ILogSource Logger
        {
            get
            {
                if (_logger == null)
                    _logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(ReportModel));
                return _logger;
            }
        }


        public static bool GetReportsForGroups(out ReportModel[] reports, out List<string> errors)
        {
            errors = null;
            reports = null;

            try
            {
                ReportServiceManager manager = new ReportServiceManager();

                var response = manager.GetReportsForGroups();
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                reports = response.ReportConfigurations == null
                    ? null
                    : response.ReportConfigurations.Select(o => ReportModel.LoadFromDto(o)).ToArray();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetReportsForGroups\r\n" + ex.ToString());
                return false;
            }
        }

        public static bool GetReportConfigurationById(int reportId, bool includeParameters, out ReportModel model, out List<string> errors)
        {
            errors = null;
            model = null;
            try
            {
                ReportServiceManager manager = new ReportServiceManager();

                var response = manager.GetReportConfigurationById(reportId);
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                model = ReportModel.LoadFromDto(response.ReportConfiguration);

                if (includeParameters && !ReportParameterModel.GetParametersForReport(model, out errors))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetReportConfigurationById\r\n" + ex.ToString());
                model = null;
                return false;
            }
        }

        public static bool GetReportConfigurationByName(string reportName, bool includeParameters, out ReportModel model, out List<string> errors)
        {
            errors = null;
            model = null;
            try
            {
                ReportServiceManager manager = new ReportServiceManager();

                var response = manager.GetReportConfigurationByName(reportName);
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                model = ReportModel.LoadFromDto(response.ReportConfiguration);

                if (includeParameters && !ReportParameterModel.GetParametersForReport(model, out errors))
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetReportConfigurationByName\r\n" + ex.ToString());
                model = null;
                return false;
            }
        }

        public static bool InvokeReportAsync(int? reportId, string reportName, List<ParameterValueModel> parameters, out Guid reportInstanceId, out List<string> errors)
        {
            errors = null;
            reportInstanceId = Guid.Empty;
            try
            {
                //convert the parameter models to the dto
				List<ParameterValueDto> parameterDtos = parameters.ConvertAll(o => new ParameterValueDto() { Name = o.Name, Value = o.Value, ValueList = o.ValueList });

                ReportServiceManager manager = new ReportServiceManager();

				InvokeReportResponse response = null;
				if (reportId.HasValue)
					response = manager.InvokeReportByIdAsync(reportId.Value, parameterDtos);
				else
					response = manager.InvokeReportAsync(reportName, parameterDtos);

                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }
                reportInstanceId = response.ReportInstanceId;
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in InvokeReportAsync\r\n" + ex.ToString());
                reportInstanceId = Guid.Empty;
                return false;
            }
        }

        public static bool CheckReportCompletionStatus(Guid instanceId, out ReportStatus status, out List<string> errors)
        {
            errors = null;
            status = ReportStatus.None;
            try
            {
                ReportServiceManager manager = new ReportServiceManager();

                DateTime endTime = DateTime.Now.AddSeconds(20/*TODO - Config It*/);
                while (DateTime.Now < endTime)
                {
                    var response = manager.GetReportInstance(instanceId);
                    if (response.Status != StatusCode.SUCCESS)
                    {
                        errors = response.Errors;
                        return false;
                    }
                    status = response.ReportInstance.Status;
                    if (status != ReportStatus.InProgress)
                        break;

                    //wait a second before checking again
                    System.Threading.Thread.Sleep(1000/*TODO - Config It*/);
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in CheckReportCompletionStatus\r\n" + ex.ToString());
                return false;
            }
        }

        public static bool GetReportInstance(Guid instanceId, out byte[] reportData, out string mimeType, out List<string> errors)
        {
            errors = null;
            reportData = null;
            mimeType = null;
            try
            {
                ReportServiceManager manager = new ReportServiceManager();

                var response = manager.GetReportPDF(instanceId);
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                reportData = response.Bytes;
                mimeType = response.MimeType;
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetReportInstance\r\n" + ex.ToString());
                reportData = null;
                return false;
            }
        }

        #region Helper Methods

        public static ReportModel LoadFromDto(ReportConfigurationDto reportDto)
        {
            return new ReportModel()
                {
                    ReportID = reportDto.ReportID,
                    GroupID = reportDto.GroupID,
                    OrderValue = reportDto.OrderValue,
                    ReportName = reportDto.ReportName,
                    Parameters = null
                };
        }
        #endregion
    }
}