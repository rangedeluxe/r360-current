﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ReportServicesClient;

namespace ReportingViews.Models
{
    public class ReportGroupModel
    {
        public int GroupID { get; set; }
        public int OrderValue { get; set; }
        public string GroupName { get; set; }
        public List<ReportModel> Reports { get; set; }

        private static Wfs.Logging.ILogSource _logger = null;
        public static Wfs.Logging.ILogSource Logger
        {
            get
            {
                if (_logger == null)
                    _logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(ReportGroupModel));
                return _logger;
            }
        }

        public static bool GetOrderedReportGroups(out ReportGroupModel[] reportGroups, out List<string> errors)
        {
            errors = null;
            reportGroups = null;
            try
            {
                ReportServiceManager manager = new ReportServiceManager();
                var response = manager.GetReportGroups();
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                var groups = response.ReportGroups.OrderBy(o => o.OrderValue).Select(o => ReportGroupModel.LoadFromDto(o)).ToArray();

                ReportModel[] reports;
                if (!ReportModel.GetReportsForGroups(out reports, out errors))
                    return false;

                if (reports == null)
                {
                    // No access to the report groups.
                    errors = new List<string>();
                    errors.Add("You do not have access to any reports.");
                    return false;
                }

                var orderedReports = reports.OrderBy(o => o.GroupID).ThenBy(o => o.OrderValue);

                //now go through the ordered reports and place them on the appropriate group
                ReportGroupModel group = null;
                int groupID = 0;
                foreach (ReportModel report in orderedReports)
                {
                    if (groupID == 0 || groupID != report.GroupID)
                    {
                        //get the parent group id
                        groupID = report.GroupID;
                        group = groups.FirstOrDefault(o => o.GroupID == groupID);
                    }

                    //if we found the group, add the report to it
                    if (group != null)
                    {
                        if (group.Reports == null)
                            group.Reports = new List<ReportModel>();
                        group.Reports.Add(report);
                    }
                }

                reportGroups = groups.Where(o => o.Reports != null && o.Reports.Count > 0).ToArray();

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetOrderedReportGroups\r\n" + ex.ToString());
                return false;
            }
        }

        #region Helper Methods

        public static ReportGroupModel LoadFromDto(ReportGroupDto groupDto)
        {
            return new ReportGroupModel()
            {
                GroupID = groupDto.GroupID,
                OrderValue = groupDto.OrderValue,
                GroupName = groupDto.GroupName,
                Reports = null
            };
        }
        #endregion
    }
}