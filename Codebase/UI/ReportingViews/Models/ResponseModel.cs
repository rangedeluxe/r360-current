﻿using CommonUtils.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportingViews.Models
{
    public class ResponseModel<T>
    {
        public T Data = default(T);
        public bool Success = true;
        public List<WebErrors> Errors = null;
    }
}