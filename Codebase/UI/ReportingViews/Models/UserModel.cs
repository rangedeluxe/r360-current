﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ReportServicesClient;

namespace ReportingViews.Models
{
    public class AuditUserModel
    {
        public int UserID;
        public string LogonName;
		public string FirstName;
        public string LastName;
        public Guid SID;

        private static Wfs.Logging.ILogSource _logger = null;
        public static Wfs.Logging.ILogSource Logger
        {
            get
            {
                if (_logger == null)
                    _logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(AuditUserModel));
                return _logger;
            }
        }

        //public static bool GetUserList(out List<AuditUserModel> users, out List<string> errors)
        //{
        //    //errors = null;
        //    //users = null;
        //    //try
        //    //{
        //    //    ReportServiceManager manager = new ReportServiceManager();
        //    //    var response = manager.GetUserList();
        //    //    if (response.Status != StatusCode.SUCCESS)
        //    //    {
        //    //        errors = response.Errors;
        //    //        return false;
        //    //    }
                    
        //    //    users = response.AuditUsers.OrderBy(o => o.LogonName).Select(o => AuditUserModel.LoadFromDto(o)).ToList();
        //    //    return true;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    Logger.Log(Wfs.Logging.LogLevel.Error, "Exception occurred in GetUserList\r\n" + ex.ToString());
        //    //    users = null;
        //    //    return false;
        //    //}
        //    return true;
        //}

        #region Helper Methods

        public static AuditUserModel LoadFromDto(AuditUserDto userDto)
        {
            return new AuditUserModel()
            {
                UserID = userDto.UserID,
                SID = userDto.SID
            };
        }
        #endregion
    }
}