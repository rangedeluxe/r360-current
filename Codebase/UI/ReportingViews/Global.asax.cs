﻿using log4net.Config;
using StructureMap;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Wfs.Raam.Core;
using Wfs.Raam.TokenCache;

namespace ReportingViews
{
  // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
  // visit http://go.microsoft.com/?LinkId=9394801
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();

      WebApiConfig.Register(GlobalConfiguration.Configuration);
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);

      XmlConfigurator.Configure();
      ObjectFactory.Initialize(x =>
      {
        x.PullConfigurationFromAppConfig = true;
      });

	  WSPassiveSessionConfiguration.Start();
	}

	public override void Init()
	{
		WSPassiveSessionConfiguration.Init();
		base.Init();
	}

    protected void Application_PreSendRequestHeaders()
    {
        var allowFrom = string.Empty;
        var principal = Thread.CurrentPrincipal as ClaimsPrincipal;
        if (principal != null)
        {
            var identity = principal.Identity as ClaimsIdentity;
            if (identity != null)
            {
                var allowFromClaim = identity.Claims.FirstOrDefault(c => c.Type == WfsClaimTypes.AllowFromUrlInFrame);
                allowFrom = allowFromClaim == null ? "" : allowFromClaim.Value;
            }
        }

        if (!string.IsNullOrEmpty(allowFrom))
        {
            Response.Headers["X-Frame-Options"] = "ALLOW-FROM " + allowFrom;
            Response.Headers["Content-Security-Policy"] = "frame-ancestors " + allowFrom;
        }
        else
        {
            Response.Headers["X-Frame-Options"] = "SAMEORIGIN";
            Response.Headers["Content-Security-Policy"] = "frame-ancestors 'self'";
        }
    }
    }
}