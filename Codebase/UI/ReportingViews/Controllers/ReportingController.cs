﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using CommonUtils.Web;
using ReportingViews.Models;
using WFS.RecHub.R360Shared;

namespace ReportingViews.Controllers
{
	public class ReportingController : BaseController
	{
		[System.Web.Mvc.Authorize]
		public JsonResult GetReportConfigurationById(int reportId)
		{
			ReportModel model = null;
			List<string> errors = null;
			bool success = true;

			if (reportId <= 0)
			{
				success = false;
				errors = new List<string>() { "Error occurred getting the report configuration." };
			}
			else if (!ReportModel.GetReportConfigurationById(reportId, false, out model, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occured retreiving the report configuration." };
			}

			var resp = new JSONResponse()
			{
				Data = model,
				HasErrors = !success,
				Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
			};
			return this.Json(resp);
		}

		[System.Web.Mvc.Authorize]
		public JsonResult GetReportConfigurationByName(string reportName)
		{
			ReportModel model = null;
			List<string> errors = null;
			bool success = true;

			if (string.IsNullOrEmpty(reportName))
			{
				success = false;
				errors = new List<string>() { "Error occurred getting the report configuration." };
			}
			else if (!ReportModel.GetReportConfigurationByName(reportName, false, out model, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occured retreiving the report configuration." };
			}

			var resp = new JSONResponse()
			{
				Data = model,
				HasErrors = !success,
				Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
			};
			return this.Json(resp);
		}

		[System.Web.Mvc.Authorize]
		public JsonResult InvokeReportAsync(string reportName, List<ParameterValueModel> parameterValues)
		{
			Guid instanceId;
			List<string> errors = null;
			bool success = true;

			if (!ReportModel.InvokeReportAsync(null, reportName, parameterValues, out instanceId, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occurred running the report." };
			}
			else if (instanceId == Guid.Empty)
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Unable to run the report." };
			}

			var resp = new JSONResponse()
			{
				Data = instanceId,
				HasErrors = !success,
				Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
			};
			return this.Json(resp);
		}

		[System.Web.Mvc.Authorize]
		public JsonResult InvokeReportByIDAsync(int reportID, List<ParameterValueModel> parameterValues)
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_ReportConfigurationAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExtractAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportDITAuditAcknowlegement, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportUserActivity, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExceptionsAuditReport, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImportReconciliation, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityGroupProfile, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityUserActivity, R360Permissions.ActionType.View)
			)
			{
				Guid instanceId;
				List<string> errors = null;
				bool success = true;

				if (!ReportModel.InvokeReportAsync(reportID, null, parameterValues, out instanceId, out errors))
				{
					success = false;
					if (errors == null || errors.Count == 0)
						errors = new List<string>() {"Error occurred running the report."};
				}
				else if (instanceId == Guid.Empty)
				{
					success = false;
					if (errors == null || errors.Count == 0)
						errors = new List<string>() {"Unable to run the report."};
				}

				var resp = new JSONResponse()
				{
					Data = instanceId,
					HasErrors = !success,
					Errors = errors != null ? errors.Select(o => new WebErrors() {Message = o}).ToList() : null
				};
				return this.Json(resp);
			}
			else
			{
				throw new HttpResponseException(HttpStatusCode.Unauthorized);
			}
		}
	}
}
