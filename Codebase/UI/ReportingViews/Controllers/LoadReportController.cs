﻿using CommonUtils.Web;
using ReportingViews.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Mvc;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360Shared;

namespace ReportingViews.Controllers
{
	public class LoadReportController : BaseController
	{
		private List<string> AuthorizedReportNames = new List<string>
		{
			"Receivables",
			"DDASummary",
			"TransactionReport",
			"ItemReport",
			"Report",
			"Receivables Summary",
			"Receivables Detail",
			"Receivables Exceptions",
			"Receivables And Exceptions",
			"Configuration Audit Report",
			"Extract Audit Report",
			"User Activity Report",
			"Import Reconciliation Report",
			"Group Profile Report",
			"User Activity Report",
			"DDA Summary"
		};

		[System.Web.Mvc.Authorize]
		public ActionResult Index(Guid instanceId, string reportName = "Report")
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_ReportConfigurationAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExtractAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportDITAuditAcknowlegement, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportUserActivity, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExceptionsAuditReport, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImportReconciliation, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityGroupProfile, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityUserActivity, R360Permissions.ActionType.View)
			)
			{
				var model = new { instanceId = instanceId, reportName = reportName };
				return View(model);
			}
			else
			{
				return new HttpUnauthorizedResult();
			}
		}

		[System.Web.Mvc.Authorize]
		public JsonResult CheckReportCompletionStatus(Guid instanceId)
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_ReportConfigurationAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExtractAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportDITAuditAcknowlegement, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportUserActivity, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExceptionsAuditReport, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImportReconciliation, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityGroupProfile, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityUserActivity, R360Permissions.ActionType.View)
			)
			{

				ReportStatus status = ReportStatus.None;
				List<string> errors = null;
				bool success = true;

				if (instanceId == Guid.Empty)
				{
					success = false;
					errors = new List<string>() { "Error occurred checking report status." };
				}

				if (!ReportModel.CheckReportCompletionStatus(instanceId, out status, out errors))
				{
					success = false;
					if (errors == null || errors.Count == 0)
						errors = new List<string>() { "Error occurred retreiving report status." };
				}


				var resp = new JSONResponse()
				{
					Data = (int)status,
					HasErrors = !success,
					Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
				};

				return this.Json(resp);
			}
			else
			{
				throw new HttpResponseException(HttpStatusCode.Unauthorized);
			}
		}

		[System.Web.Mvc.Authorize]
		public ActionResult GetReportInstance(Guid instanceId, string reportName = "Report")
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_ReportConfigurationAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExtractAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportDITAuditAcknowlegement,R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportUserActivity, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExceptionsAuditReport, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImportReconciliation, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityGroupProfile, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityUserActivity, R360Permissions.ActionType.View)
			)
			{

				byte[] reportData;
				List<string> errors = null;
				bool success = true;
				string mimeType = null;

				// Validation
				if (instanceId == Guid.Empty)
				{
					success = false;
					errors = new List<string>() {"Error occurred getting the report data."};
				}

				// Validate our report name.  We're using this in an HTTP header, so this is just for security.
				var name = Regex.Replace(reportName, @"\.pdf", string.Empty, RegexOptions.IgnoreCase)
					.Replace('-', ' ');
				name = Regex.Replace(name, @"\.csv", string.Empty, RegexOptions.IgnoreCase);

				if (!AuthorizedReportNames.Contains(name))
				{
					success = false;
					errors = new List<string>() {"Invalid Report Name."};
				}

				// Get report data
				if (!ReportModel.GetReportInstance(instanceId, out reportData, out mimeType, out errors))
				{
					success = false;
					errors = new List<string>() {"Error occured retrieving the report."};
				}

				if (reportData == null)
				{
					success = false;
					errors = new List<string>() {"Error occured retrieving the report data."};
				}

				// Kick out early if failure.
				if (!success)
				{
					var response = new ResponseModel<byte[]>()
					{
						Data = reportData,
						Success = success,
						Errors = errors != null ? errors.Select(o => new WebErrors() {Message = o}).ToList() : null
					};

					return View(response);
				}

				// Return the data.

				MemoryStream ms = new MemoryStream(reportData);
				//makes the report open in-browser
				Response.AppendHeader("Content-Disposition", "inline;filename=" + reportName);
				if (reportName.ToLower().EndsWith(".pdf"))
				{
					mimeType = "application/pdf";
				}

				return File(ms, mimeType);
			}
			else
			{
				return new HttpUnauthorizedResult();
			}
		}
	}
}
