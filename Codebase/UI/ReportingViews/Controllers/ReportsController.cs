﻿using CommonUtils.Web;
using ReportingViews.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ReportServicesClient;

namespace ReportingViews.Controllers
{
	public class ReportsController : BaseController
	{
		[System.Web.Mvc.Authorize]
		public ActionResult Index()
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_ReportConfigurationAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExtractAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportDITAuditAcknowlegement, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportUserActivity, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExceptionsAuditReport, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImportReconciliation, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityGroupProfile, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityUserActivity, R360Permissions.ActionType.View)
			)
			{

				ReportGroupModel[] orderedGroups = null;
				List<string> errors = null;
				bool success = true;

				if (!ReportGroupModel.GetOrderedReportGroups(out orderedGroups, out errors))
				{
					success = false;
					if (errors == null || errors.Count == 0)
						errors = new List<string>() { "Error occured retreiving the report groups." };
				}

				var response = new ResponseModel<ReportGroupModel[]>()
				{
					Data = orderedGroups,
					Success = success,
					Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
				};

				return View(response);
			}
			else
			{
				return new HttpUnauthorizedResult();
			}
		}

		[System.Web.Mvc.Authorize]
		public ActionResult ParameterSelection(int reportID)
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_ReportConfigurationAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExtractAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportDITAuditAcknowlegement, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportUserActivity, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExceptionsAuditReport, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImportReconciliation, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityGroupProfile, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityUserActivity, R360Permissions.ActionType.View)
			)
			{

				ReportModel report;
				List<string> errors = null;
				bool success = true;

				if (!ReportModel.GetReportConfigurationById(reportID, true, out report, out errors))
				{
					success = false;
					if (errors == null || errors.Count == 0)
						errors = new List<string>() {"Error occured retreiving the report parameters."};
				}

				var response = new ResponseModel<ReportModel>()
				{
					Data = report,
					Success = success,
					Errors = errors != null ? errors.Select(o => new WebErrors() {Message = o}).ToList() : null
				};

				return View(response);
			}
			else
			{
				return new HttpUnauthorizedResult();
			}
		}

		//[Authorize]
		//public JsonResult GetUserList()
		//{
		//    List<AuditUserModel> users;
		//    List<string> errors = null;
		//    bool success = true;

		//    if (!AuditUserModel.GetUserList(out users, out errors))
		//    {
		//        success = false;
		//        if (errors == null || errors.Count == 0)
		//            errors = new List<string>() { "Error occured retreiving the user list." };

		//    }

		//    var resp = new JSONResponse()
		//    {
		//        Data = users,
		//        HasErrors = !success,
		//        Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
		//    };
		//    return this.Json(resp);
		//}

		[System.Web.Mvc.Authorize]
		public JsonResult GetPaymentSourceList()
		{
			List<PaymentSourceModel> paymentSources;
			List<string> errors = null;
			bool success = true;

			if (!PaymentSourceModel.GetPaymentSourceList(out paymentSources, out errors))
			{
				success = false;
				if (errors == null || errors.Count == 0)
					errors = new List<string>() { "Error occured retreiving the payment source list." };

			}

			var resp = new JSONResponse()
			{
				Data = paymentSources,
				HasErrors = !success,
				Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
			};
			return this.Json(resp);
		}

		[System.Web.Mvc.Authorize]
		public JsonResult GetEventTypeList()
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_ReportConfigurationAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExtractAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportDITAuditAcknowlegement, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportUserActivity, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportExceptionsAuditReport, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImportReconciliation, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityGroupProfile, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_ReportSecurityUserActivity, R360Permissions.ActionType.View)
			)
			{
				List<string> errors = null;
				bool success = true;

				ReportServiceManager manager = new ReportServiceManager();
				var response = manager.GetEventTypeList();
				if (response.Errors.Count > 0)
				{
					success = false;
					if (errors == null || errors.Count == 0)
						errors = new List<string>() {"Error occured retreiving the event type list."};

				}

				var resp = new JSONResponse()
				{
					Data = response.EventTypes,
					HasErrors = !success,
					Errors = errors != null ? errors.Select(o => new WebErrors() {Message = o}).ToList() : null
				};
				return this.Json(resp);
			}
			else
			{
				throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
			}
		}
	}
}
