﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Error</title>
</head>
<body>
    <h1>Sorry, an error occurred while processing your request. Please contact your system administrator.</h1>
    <a href="/Framework/" style="text-decoration: underline">Click here to reload this page</a>
</body>
</html>