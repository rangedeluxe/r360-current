﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace ReportingViews.Raam
{
  public static class DevSidClaim
  {
    public static void AddSid(string sid)
    {
      ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.Sid, sid) }));
    }
  }
}