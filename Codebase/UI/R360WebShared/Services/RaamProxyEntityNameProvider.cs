﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360RaamClient;

namespace WFS.RecHub.R360WebShared.Services
{
    public class RaamProxyEntityNameProvider : IEntityNameProvider
    {
        private readonly RaamClient _raamclient;

        public RaamProxyEntityNameProvider()
        {
            _raamclient = new RaamClient(null);
        }

        public string GetEntityName(int entityid)
        {
            var ent = _raamclient.GetEntityHierarchy(entityid);
            return ent != null
                ? ent.Name
                : string.Empty;
        }
    }
}
