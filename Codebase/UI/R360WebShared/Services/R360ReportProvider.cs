﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ReportServicesClient;

namespace WFS.RecHub.R360WebShared.Services
{
    public class R360ReportProvider : IReportProvider
    {
        private readonly ReportServiceManager _manager;

        public R360ReportProvider()
        {
            _manager = new ReportServiceManager();
        }

        public string GetReportName(int reportid)
        {
            var response = _manager.GetReportConfigurationById(reportid);
            if (response.Status != R360Shared.StatusCode.SUCCESS || response.ReportConfiguration == null)
                return string.Empty;
            return response.ReportConfiguration.ReportName;
        }
    }
}
