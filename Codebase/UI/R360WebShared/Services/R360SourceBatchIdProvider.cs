﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.R360ServicesServicesClient;

namespace WFS.RecHub.R360WebShared.Services
{
    public class R360SourceBatchIdProvider : ISourceBatchIdProvider
    {
        private readonly R360ServiceManager _manager;

        public R360SourceBatchIdProvider()
        {
            _manager = new R360ServiceManager();
        }

        public long GetSourceBatchId(long batchid)
        {
            var result = _manager.GetSourceBatchID(batchid);
            return result != null && result.Status == R360Shared.StatusCode.SUCCESS
                ? result.Data
                : default(long);
        }
    }
}
