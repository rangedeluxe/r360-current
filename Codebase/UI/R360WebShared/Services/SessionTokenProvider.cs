﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360WebShared.Services
{
    public class SessionTokenProvider : ISessionTokenProvider
    {
        /// <summary>
        /// This implementation uses SHA512 of the session ID to generate a token for use against CSRF attacks.
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public string GenerateSessionToken(string session)
        {
            var hash = string.Empty;
            var data = Encoding.UTF8.GetBytes(session);
            //512CNG is a FIPS compliant hash generator
            using (var sha = new SHA512Cng())
            {
                var hashbytes = sha.ComputeHash(data);
                var sb = new StringBuilder();
                foreach (var b in hashbytes)
                    sb.AppendFormat("{0:x2}", b);
                hash = sb.ToString();
            }
            return hash;
        }
    }
}
