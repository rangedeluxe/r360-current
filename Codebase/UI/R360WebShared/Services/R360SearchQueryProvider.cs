﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.R360Services.R360ServicesServicesClient;

namespace WFS.RecHub.R360WebShared.Services
{
    public class R360SearchQueryProvider : ISearchQueryProvider
    {
        private readonly R360ServiceManager _manager;

        public R360SearchQueryProvider()
        {
            _manager = new R360ServiceManager();
        }

        public string GetSearchQueryName(string queryid)
        {
            Guid tempguid;
            if (!Guid.TryParse(queryid, out tempguid))
                return string.Empty;

            var response = _manager.GetStoredQuery(tempguid);
            if (response.Status != R360Shared.StatusCode.SUCCESS || response.Data == null)
                return string.Empty;

            var result = response.Data;
            return result.Name;
        }
    }
}
