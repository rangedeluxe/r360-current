﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.HubConfig;

namespace WFS.RecHub.R360WebShared.Services
{
    public class R360PaymentSourceProvider : IPaymentSourceProvider
    {
        private readonly HubConfigServicesManager _manager;

        public R360PaymentSourceProvider()
        {
            _manager = new HubConfigServicesManager();
        }

        public string GetPaymentSourceName(int id)
        {
            var payment = _manager.GetPaymentSource(id);
            if (payment.Status == R360Shared.StatusCode.SUCCESS && payment.Data != null)
                return payment.Data.LongName;
            return string.Empty;
        }

        public string GetPaymentTypeName(int id)
        {
            var types = _manager.GetBatchPaymentTypes();
            if (types.Status != R360Shared.StatusCode.SUCCESS || types.Data == null)
                return string.Empty;

            var type = types.Data.FirstOrDefault(x => x.Item1 == id);
            if (type == null)
                return string.Empty;

            return type.Item2;
        }
    }
}
