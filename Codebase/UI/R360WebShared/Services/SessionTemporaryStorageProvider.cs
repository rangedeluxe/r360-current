﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WFS.RecHub.R360WebShared.Services
{
    public class SessionTemporaryStorageProvider : ITemporaryStorageProvider
    {

        public object GetValue(string key)
        {
            return HttpContext.Current.Session[key];
        }

        public void SetValue(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }
    }
}
