﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WFS.RecHub.R360WebShared.Services
{
    /// <summary>
    /// Standard AuditFilter for WFS.  Removes a bunch of fields we don't want to display, and
    /// gather's data based on fields in which the value needs to be replaced.
    /// </summary>
    public class WfsAuditFilter : IAuditFilter
    {
        private readonly IEntityNameProvider _entitynameprovider;
        private readonly ISourceBatchIdProvider _sourcebatchidprovider;
        private readonly IPaymentSourceProvider _paymentsourceprovider;
        private readonly ISearchQueryProvider _searchqueryprovider;
        private readonly IReportProvider _reportprovider;

        // Protected values if we want to override them later on.
        protected string[] ValuesToRemove { get; set; }
        protected KeyValuePair<string, string>[] ValuesToReplace { get; set; }
        // Looks a little complex, but basically just
        // "perform this action(fullList, index) when we hit 'x' key"
        protected List<KeyValuePair<string, Action<List<KeyValuePair<string, string>>, int>>> ActionsToPerform { get; set; }

        /// <summary>
        /// Default constructor for production.
        /// </summary>
        public WfsAuditFilter() 
            : this(new RaamProxyEntityNameProvider(), new R360SourceBatchIdProvider(), 
                   new R360PaymentSourceProvider(), new R360SearchQueryProvider(),
                   new R360ReportProvider()) { }

        /// <summary>
        /// Constructor for dependency injection.
        /// </summary>
        /// <param name="entityNameProvider"></param>
        /// <param name="sourceBatchIdProvider"></param>
        public WfsAuditFilter(IEntityNameProvider entityNameProvider, 
            ISourceBatchIdProvider sourceBatchIdProvider,
            IPaymentSourceProvider paymentsourceprovider,
            ISearchQueryProvider searchqueryprovider,
            IReportProvider reportprovider)
        {
            _entitynameprovider = entityNameProvider;
            _sourcebatchidprovider = sourceBatchIdProvider;
            _paymentsourceprovider = paymentsourceprovider;
            _searchqueryprovider = searchqueryprovider;
            _reportprovider = reportprovider;

            ActionsToPerform = new List<KeyValuePair<string, Action<List<KeyValuePair<string, string>>, int>>>();
            ActionsToPerform.AddRange(new List<KeyValuePair<string, Action<List<KeyValuePair<string, string>>, int>>>()
            {
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>("^Batch$", (list, i) => ReplaceBatchID(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>(@"^(FI)?Entity\.?ID$", (list, i) => ReplaceEntityID(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>("^(cbo)?PaymentType(Id)?$", (list, i) => ReplacePaymentTypeID(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>("^(cbo)?PaymentSource(Id)?$", (list, i) => ReplacePaymentSourceID(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>("^(hdn|hid)", (list, i) => RemoveField(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>("^cboSortBy$", (list, i) => FilterSortByColumn(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>("^(txt|fld|cbo|sel|chk)", (list, i) => RemovePreTag(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>("^(txt)?PreDefSearchID(_Uncheck)?$", (list, i) => ReplaceSearchID(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>("^ReportId$", (list, i) => ReplaceReportID(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>(@"^Entity\.(?!id$).*$", (list, i) => RemoveField(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>(@"^parameterValues\[.*$", (list, i) => RemoveField(list,i)),
                new KeyValuePair<string, Action<List<KeyValuePair<string,string>>, 
                    int>>(@"^users\[.*$", (list, i) => RemoveField(list,i)),
            });

            ValuesToRemove = new string[]
            {
                "ID", "EventRuleID", "EventID", "InstanceID",
                "DocumentTypeKey", "IsReadOnly", "NotificationTypeKey", "BankKey",
                "BatchSourceKey", "ClientAccountKey", "NotificationFileTypeKey", 
                "lckbx", "printview", "hdtSecurity_Token", "lockboxPayerId", 
                "selSaveQuery"
            };

            ValuesToReplace = new KeyValuePair<string, string>[]
            {
                new KeyValuePair<string, string>("SiteBankID", "BankID"),
                new KeyValuePair<string, string>("SiteClientAccountID", "WorkgroupID"),
                new KeyValuePair<string, string>("SiteClientAcctID", "WorkgroupID"),
                new KeyValuePair<string, string>("ClientAccountID", "WorkgroupID"),
                new KeyValuePair<string, string>("Bank", "BankID"),
                new KeyValuePair<string, string>("Lbx", "WorkgroupID"),
                new KeyValuePair<string, string>("fldDisplayRemitterNameInPDF", "DisplayPayerNameInPDF"),
            };
        }

        /// <summary>
        /// Applies the filter to the given list. Returns a new list and does not 
        /// alter the original. 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> ApplyFilter(List<KeyValuePair<string, string>> list)
        {
            // Copy values over.
            var output = new List<KeyValuePair<string, string>>(list);

            // Remove all unwanted data.
            output.RemoveAll(x => ValuesToRemove.Contains(x.Key, StringComparer.CurrentCultureIgnoreCase));
            
            // Replace all data with alternates.
            for (var i = 0; i < output.Count; i++)
            {
                var pair = output[i];
                var replace = ValuesToReplace.FirstOrDefault(x => x.Key.Equals(pair.Key, StringComparison.CurrentCultureIgnoreCase));
                // If we actually found something.  (can't null a struct)
                if (!replace.Equals(default(KeyValuePair<string, string>)))
                {
                    // Replace without altering the ordering. (Replace.Value is the new key)
                    output[i] = new KeyValuePair<string, string>(replace.Value, pair.Value);
                }
            }

            // Perform any particular action over the fields.
            for (var i = 0; i < output.Count; i++)
            {
                var pair = output[i];
                if (pair.Key == null)
                    continue;
                var actions = ActionsToPerform
                    .Where(x => new Regex(x.Key, RegexOptions.IgnoreCase).IsMatch(pair.Key));
                actions
                    .ToList()
                    .ForEach(a => a.Value(output, i));
            }

            // Clean up
            output.RemoveAll(x => x.Key == default(string));

            return output;
        }

        /// <summary>
        /// Replaces the BatchID value with the SourceBatchID.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void ReplaceBatchID(List<KeyValuePair<string, string>> list, int index)
        {
            var templong = 0L;
            if (!long.TryParse(list[index].Value, out templong))
                return;

            var sourcebatchid = _sourcebatchidprovider.GetSourceBatchId(templong);
            list[index] = new KeyValuePair<string, string>(list[index].Key, sourcebatchid.ToString());
        }

        /// <summary>
        /// Replaces the EntityID with the EntityName.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void ReplaceEntityID(List<KeyValuePair<string, string>> list, int index)
        {
            // Input format could be an int (EntityID), or a piped string with BankID|WorkgroupID.

            // If we have a piped string, we just want to replace the keyname.
            if (list[index].Value.Contains("|"))
            {
                list[index] = new KeyValuePair<string, string>("WorkgroupSelection", list[index].Value);
                return;
            }

            // Else, we want to lookup the entity name.
            var tempint = 0;
            if (!int.TryParse(list[index].Value, out tempint))
                return;

            var entityname = _entitynameprovider.GetEntityName(tempint);
            list[index] = new KeyValuePair<string, string>(list[index].Key, entityname);
        }

        /// <summary>
        /// Replaces the PaymentTypeID with the name of the Payment type.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void ReplacePaymentTypeID(List<KeyValuePair<string, string>> list, int index)
        {
            var tempint = 0;
            if (!int.TryParse(list[index].Value, out tempint))
                return;

            var paymentname = _paymentsourceprovider.GetPaymentTypeName(tempint);
            list[index] = new KeyValuePair<string, string>(list[index].Key, paymentname);
        }

        /// <summary>
        /// Replaces the PaymentSourceID with the name of the Payment source.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void ReplacePaymentSourceID(List<KeyValuePair<string, string>> list, int index)
        {
            var tempint = 0;
            if (!int.TryParse(list[index].Value, out tempint))
                return;

            var paymentname = _paymentsourceprovider.GetPaymentSourceName(tempint);
            list[index] = new KeyValuePair<string, string>(list[index].Key, paymentname);
        }

        /// <summary>
        /// Replaces the Advanced Search Query ID.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void ReplaceSearchID(List<KeyValuePair<string, string>> list, int index)
        {
            var queryname = _searchqueryprovider.GetSearchQueryName(list[index].Value);
            list[index] = new KeyValuePair<string, string>(list[index].Key, queryname);
        }

        /// <summary>
        /// Replaces the ReportID with the ReportName.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void ReplaceReportID(List<KeyValuePair<string, string>> list, int index)
        {
            var tempint = 0;
            if (!int.TryParse(list[index].Value, out tempint))
                return;

            var reportname = _reportprovider.GetReportName(tempint);
            list[index] = new KeyValuePair<string, string>(list[index].Key, reportname);
        }

        /// <summary>
        /// Replaces values in the SortBy Column.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void FilterSortByColumn(List<KeyValuePair<string, string>> list, int index)
        {
            list[index] = new KeyValuePair<string, string>(list[index].Key, list[index].Value.Replace("ClientAccountID", "WorkgroupID"));
        }

        /// <summary>
        /// Removes the field from the list (rather marks it for removal)
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void RemoveField(List<KeyValuePair<string, string>> list, int index)
        {
            list[index] = default(KeyValuePair<string, string>);
        }

        /// <summary>
        /// Removes the pre-tag from the beginning of the key name.
        /// Examples: "txtFieldName" becomes "FieldName". 
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        private void RemovePreTag(List<KeyValuePair<string, string>> list, int index)
        {
            list[index] = new KeyValuePair<string, string>(
                Regex.Replace(list[index].Key, "^(txt|fld|cbo|sel|chk)(.*)", "$2"), list[index].Value);
        }
    }
}
