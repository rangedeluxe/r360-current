﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360WebShared.Services
{
    public interface ITemporaryStorageProvider
    {
        void SetValue(string key, object value);
        object GetValue(string key);
    }
}
