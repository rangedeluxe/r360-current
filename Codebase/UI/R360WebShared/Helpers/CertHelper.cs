﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360WebShared.DTOs;

namespace WFS.RecHub.R360WebShared.Helpers
{
    public static class CertHelper
    {
        public static X509Certificate2 FindByThumbprint(this string thumbprint, StoreName storeName, StoreLocation storeLocation)
        {
            if (string.IsNullOrEmpty(thumbprint)) return null;

            var certificateStore = new X509Store(storeName, storeLocation);
            certificateStore.Open(OpenFlags.ReadOnly);

            foreach (var certificate in certificateStore.Certificates)
            {
                if (string.Equals(certificate?.Thumbprint, thumbprint, StringComparison.CurrentCultureIgnoreCase))
                {
                    certificateStore.Close();
                    return certificate;
                }
            }
            return null;
        }

        public static string GetJwt(this JwtInfo jwtInfo, IEnumerable<Claim> claims)
        {
            try
            {
                var cert = jwtInfo.Thumbprint.FindByThumbprint(StoreName.My, StoreLocation.LocalMachine);
                if (cert == null) return string.Empty;
                var x509signingkey = new X509SecurityKey(cert);
                var signingCredentials = new SigningCredentials(x509signingkey, SecurityAlgorithms.RsaSha256);
                var token = new JwtSecurityToken(
                    issuer: jwtInfo.Issuer,
                    audience: jwtInfo.Audience,
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(jwtInfo.Lifetime),
                    signingCredentials: signingCredentials
                    );
                var signedAndEncodedToken = new JwtSecurityTokenHandler().WriteToken(token);
                return signedAndEncodedToken;
            }
            catch (Exception e)
            {
                e.Data.Add("GetJwt", "An error occurred while creating the JWT token.");
                throw;
            }
        }
    }
}
