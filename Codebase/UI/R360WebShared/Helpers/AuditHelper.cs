﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WFS.RecHub.R360WebShared.Helpers
{
    /// <summary>
    /// Contains some methods to help with audit messages.
    /// </summary>
    public static class AuditHelper
    {
        public const int AUDIT_MAX_LENGTH = 100;

        /// <summary>
        /// Parses the query string into a list of pairs.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static List<KeyValuePair<string, string>> GetFieldsFromQueryString(string query)
        {
            var collection = HttpUtility.ParseQueryString(query);
            return collection
                .Cast<string>()
                .Select(x => new KeyValuePair<string, string>(x, collection[x]))
                .ToList();
        }

        /// <summary>
        /// Just concatenates the fields into a message.
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public static string BuildAuditMessage(List<KeyValuePair<string, string>> fields)
        {
            var output = string.Empty;
            foreach (var pair in fields)
            {
                string value = pair.Value;
                if (!string.IsNullOrWhiteSpace(value))
                {
                    string fieldVal = " " + pair.Key + "=" + value + ";";
                    if (fieldVal.Length > AUDIT_MAX_LENGTH)
                        fieldVal = fieldVal.Substring(0, AUDIT_MAX_LENGTH) + "...";
                    output += fieldVal;
                }
            }
            return output;
        }
    }
}
