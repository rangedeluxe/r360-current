﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360WebShared.Helpers
{
    public static class StringHelper
    {
        public static string FormatUrl(this string url)
        {
            var furl = url.Trim();
            furl = url.EndsWith("/") ? furl : $"{furl}/";
            return furl;
        }
    }
}
