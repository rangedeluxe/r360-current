﻿using System.Net.Http;
using System.Threading.Tasks;

namespace WFS.RecHub.R360WebShared.Helpers
{
    public interface IHttpClient
    {
        Task<HttpResponseMessage> Get(string url);
        Task<HttpResponseMessage> Post(string url, object data);
        Task<HttpResponseMessage> Delete(string id);
    }
}
