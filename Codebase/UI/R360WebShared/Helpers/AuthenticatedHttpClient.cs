﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using WFS.RecHub.R360WebShared.DTOs;

namespace WFS.RecHub.R360WebShared.Helpers
{
    public class AuthenticatedHttpClient : IHttpClient
    {
        private const string MEDIATYPE = "application/json";
        private string baseUrl;
        private string token;
        public AuthenticatedHttpClient(IConfiguration configuration, IEnumerable<Claim> claims)
        {
            this.baseUrl = configuration.GetPayerApiUrl().FormatUrl();
            this.token = configuration.GetJwtInfo().GetJwt(claims);
        }
        
        public async Task<HttpResponseMessage> Get(string url)
        {
            var finalurl = $"{baseUrl}{url}";
            HttpResponseMessage response = null;
            using(var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(MEDIATYPE));
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
                response = await client.GetAsync(finalurl);        
            }
            return response;
        }

        public async Task<HttpResponseMessage> Post(string url, object data)
        {
            var finalurl = $"{baseUrl}{url}";
            HttpResponseMessage response = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(MEDIATYPE));
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
                response = await client.PostAsJsonAsync(finalurl, data);
            }
            return response;
        }
        public async Task<HttpResponseMessage> Delete(string url)
        {
            var finalurl = $"{baseUrl}{url}";
            HttpResponseMessage response = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(MEDIATYPE));
                client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
                response = await client.DeleteAsync(finalurl);
            }
            return response;
        }
    }
}
