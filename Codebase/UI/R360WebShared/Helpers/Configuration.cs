﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360WebShared.DTOs;

namespace WFS.RecHub.R360WebShared.Helpers
{
    //TODO: All of these keys could change, these are just temporary names
    public class Configuration : IConfiguration
    {
        public JwtInfo GetJwtInfo()
        {

            try
            {
                int lifetime = int.TryParse(ConfigurationManager.AppSettings.Get("R360ApiLifetime"), out lifetime)
            ? lifetime
            : 0;
                return new JwtInfo
                {
                    Audience = ConfigurationManager.AppSettings.Get("R360ApiAudience").ToLower(),
                    Issuer = ConfigurationManager.AppSettings.Get("R360ApiIssuer").ToLower(),
                    Lifetime = lifetime,
                    Thumbprint = ConfigurationManager.AppSettings.Get("R360ApiThumbprint")
                };
            }
            catch (Exception e)
            {
                e.Data.Add("Error GetJwtInfo", "Error gathering JWT info");
                throw;
            }
        }

        public string GetPayerApiUrl()
        {
            try
            {
                return ConfigurationManager.AppSettings.Get("R360Api");
            }
            catch (Exception e)
            {
                e.Data.Add("Error GetPayerApiUrl", "Error gathering r360 api url");
                throw;
            }
        }
    }
}
