﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360WebShared.DTOs;

namespace WFS.RecHub.R360WebShared.Helpers
{
    public interface IConfiguration
    {
        string GetPayerApiUrl();
        JwtInfo GetJwtInfo();
    }
}
