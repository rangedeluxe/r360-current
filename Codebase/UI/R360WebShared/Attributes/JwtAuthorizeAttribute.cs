﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wfs.Raam.Core.Services;

namespace WFS.RecHub.R360WebShared.Attributes
{
    public class JwtAuthorizeAttribute : AuthorizeAttribute
    {
        public const string APPLICATION_NAME = "Receivables360Online";
        public const string RAAM_AUDIENCE = "urn:Wfs.AppServices";

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            try
            {
                // Grab le token from the Authorization header.
                var token = httpContext.Request.Headers["Authorization"];
                if (string.IsNullOrWhiteSpace(token))
                    return false;

                // Grab our signing cert - we have some code in place to find it already.
                var cert = CustomSecurityTokenProvider.GetCertificate(
                    System.Security.Cryptography.X509Certificates.StoreName.My,
                    System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine,
                    CertificateIdentificationType.Thumbprint,
                    WSFederatedAuthentication.GetThumbPrintFromConfigFile(string.Empty));

                // Validate the token.
                var validation = new TokenValidationParameters()
                {
                    ValidIssuer = cert.Thumbprint,
                    ValidAudience = RAAM_AUDIENCE,
                    IssuerSigningKey = new X509SecurityKey(cert)
                };

                SecurityToken validatedtoken;
                var principal = new JwtSecurityTokenHandler().ValidateToken(token, validation, out validatedtoken);
                if (principal == null || validatedtoken == null)
                    return false;

                // We're good, so just set who we are.
                Thread.CurrentPrincipal = principal;
                return true;
            }
            catch (Exception ex)
            {
                // TODO: do some logging and stuff.
                return false;
            }
        }
    }
}
