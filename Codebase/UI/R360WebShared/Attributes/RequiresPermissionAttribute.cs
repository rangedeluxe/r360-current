﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 08/15/2014
*
* Purpose: Just a simple attribute for authorization on permissions for 
*          ActionResults.
*
* Modification History
*   WI 153981 BLR 04/02/2014
*     - Initial Version
******************************************************************************/

namespace WFS.RecHub.R360WebShared.Attributes
{
    public class RequiresPermissionAttribute : AuthorizeAttribute
    {
        private readonly string _permission;
        private readonly WFS.RecHub.R360Shared.R360Permissions.ActionType _actiontype;

        public RequiresPermissionAttribute(string permission, WFS.RecHub.R360Shared.R360Permissions.ActionType type)
        {
            _permission = permission;
            _actiontype = type;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return R360Permissions.Current.Allowed(_permission, _actiontype);
        }
    }
}