﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Services;

namespace WFS.RecHub.R360WebShared.Attributes
{
    /// <summary>
    /// Class can attach to an action and validates the current session's token against
    /// what was passed by the client.
    /// </summary>
    public class ValidateSessionTokenAttribute : AuthorizeAttribute
    {
        private const string DEFAULT_TOKEN_KEY = "hashtoken";

        private readonly ISessionTokenProvider tokenProvider;
        private readonly string tokenKey;

        /// <summary>
        /// The tokenkey is which POST value we need to validate against.
        /// If you POST values look like this: "&hashkey=ABF212", then pass "hashkey".
        /// </summary>
        /// <param name="tokenkey"></param>
        public ValidateSessionTokenAttribute(string tokenkey = DEFAULT_TOKEN_KEY)
        {
            tokenProvider = new SessionTokenProvider();
            tokenKey = tokenkey;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // Check to see if anything was passed at all.
            if (!httpContext.Request.Form.AllKeys.Contains(tokenKey))
                return false;

            // Now grab both values and check for equality.
            var clientvalue = httpContext.Request.Form[tokenKey];
            var servervalue = tokenProvider.GenerateSessionToken(R360ServiceContext.Current.GetSessionID().ToString());
            return clientvalue == servervalue;
        }
    }
}