﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Wfs.Logging;
using Wfs.Logging.NLog;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Helpers;
using WFS.RecHub.R360WebShared.Services;

namespace WFS.RecHub.R360WebShared
{
    public abstract class WfsSharedBaseController : Controller
    {
        private readonly WfsLog Logger = new WfsLog();
        private readonly IAuditFilter _auditfilter;

		public WfsSharedBaseController()
		{
			LogManager.Logger = new ConfigHelpers.Logger(o => Logger.Log(WfsLogLevel.Info, o), o => Logger.Log(WfsLogLevel.Warn, o));
			R360ServiceContext.PerformImpersonation();
            _auditfilter = new WfsAuditFilter();
		}

		/* This will only catch certain errors - not all.  So, not doing anything here and instead requiring each application create an Application_Error method in Global.asax
		protected override void OnException(ExceptionContext filterContext)
		{
			// Log the error
			var ctx = filterContext.HttpContext;
			if (ctx != null)
			{
				Exception exception = filterContext.Exception;
				if (exception != null)
				{
					Logger.Log(Wfs.Logging.LogLevel.Fatal, "Error: Unhandled Action Exception:\r\n  Offending URL: {0}\r\n  Exception: {1}", 
						ctx.Request.Url.PathAndQuery, exception.ToString());

					// TODO: filterContext.SwitchToErrorView() ...?
				}
			}

			base.OnException(filterContext);
		}
		*/

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			base.OnActionExecuting(filterContext);

			// Initialize state
			_auditFormFields.Clear();
			IsAuditEventSuppressed = false;
		}

		//protected override void OnActionExecuted(ActionExecutedContext filterContext)
		//{
		//	base.OnActionExecuted(filterContext);
		//}

		protected override void OnResultExecuting(ResultExecutingContext filterContext)
		{
			base.OnResultExecuting(filterContext);

			if (!IsAuditEventSuppressed)
				LogPageView(filterContext.RouteData, filterContext.Result.GetType());
		}

		//protected override void OnResultExecuted(ResultExecutedContext filterContext)
		//{
		//	base.OnResultExecuted(filterContext);
		//}

		private readonly List<string> _auditFormFields = new List<string>();

		/// <summary>
		/// Pass in a list of the parameters to your controller method that should be audited
		/// </summary>
		/// <param name="fields">The names of the parameters in the Form collection</param>
		protected void AuditFormFields(params string[] fields)
		{
			_auditFormFields.AddRange(fields);
		}

		/// <summary>
		/// Set this to false in your controller method to prevent an audit event getting created, if the method would create too much noise in the audit log
		/// </summary>
		protected bool IsAuditEventSuppressed { get; set; }

        /// <summary>
        /// Recursive function to flatten JToken objects.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="list"></param>
        protected void AppendJTokenToDictionary(JToken token, List<KeyValuePair<string, string>> list)
        {
            if (token.Type == JTokenType.Object || token.Type == JTokenType.Array || token.Type == JTokenType.Property)
            {
                foreach (JToken prop in token.Children())
                    AppendJTokenToDictionary(prop, list);
            }
            else
            {
                list.Add(new KeyValuePair<string, string>(token.Path, token.Value<string>()));
            }
        }


        private const int AUDIT_MAX_LEN = 100;
		protected void LogPageView(RouteData routeData, Type resultType)
		{
			try
			{
				HttpRequestBase req = this.Request;
				string qs = req.QueryString.ToString();
				var formCollection = req.Form;

				string applicationName = req.ApplicationPath.Trim('/');
				string controllerName = routeData.Values["controller"].ToString();
				string actionName = routeData.Values["action"].ToString();
				string pageDescription = controllerName + "/" + actionName;

				if (qs.Length > 0)
				{
                    var fields = AuditHelper.GetFieldsFromQueryString(qs);
                    fields = _auditfilter.ApplyFilter(fields);
                    var qsmessage = AuditHelper.BuildAuditMessage(fields);
                    pageDescription += " QueryString: " + qsmessage;
				}

				// Determine the type of audit event, based on the input values and result type
				string auditEvent = "Web Request";	// Default value
				string auditType = "Page View";	// Default value
                if (resultType == typeof(ViewResult))
                {
                    auditEvent = "Viewed Page";
                }
                else if (resultType == typeof(JsonResult))
                {
                    if (formCollection.Count > 0 || (req.InputStream.Length > 0 && req.ContentType.ToLower().Contains("application/json")))
                        auditEvent = "Submitted / Retrieved Data";
                    else
                        auditEvent = "Viewed Data";
                }

				// Deal with Form Fields
				if (formCollection.Count > 0)
				{
					_auditFormFields.AddRange(formCollection.AllKeys.Where(o => o != "parentId"));


                    var pairs = _auditFormFields
                            .Select(key => new KeyValuePair<string, string>(key, formCollection[key]))
                            .ToList();

                    pairs = _auditfilter.ApplyFilter(pairs);
                    if (pairs.Count > 0)
					{
						pageDescription += " Fields: ";
                        var formmessage = AuditHelper.BuildAuditMessage(pairs);
                        pageDescription += formmessage;
					}
				}
                // Attempt to deal with JSON based data.
                else if (req.InputStream.Length > 0 && req.ContentType.ToLower().Contains("application/json"))
                {
                    try
                    {
                        req.InputStream.Position = 0;
                        var stream = new StreamReader(req.InputStream);
                        var data = stream.ReadToEnd();
                        var token = JObject.Parse(data);
                        var fields = new List<KeyValuePair<string, string>>();
                        AppendJTokenToDictionary(token, fields);
                        _auditFormFields.AddRange(fields.Select(x => x.Key));
                        var pairs = _auditFormFields
                            .Select(key => new KeyValuePair<string, string>(key, fields.First(x => x.Key == key).Value?.ToString()))
                            .ToList();
                        pairs = _auditfilter.ApplyFilter(pairs);
                        pageDescription += " Fields: ";
                        var formmessage = AuditHelper.BuildAuditMessage(pairs);
                        pageDescription += formmessage;
                    }
                    catch
                    {
                        // turns out it wasn't json, so we'll just do nothing here.
                    }
                    
                }

				// Call the Hub Service to write the event to the database
				var manager = new R360ServiceManager();
				var response = manager.WriteAuditEvent(auditEvent, auditType, applicationName, pageDescription);
				if (response.Status != StatusCode.SUCCESS)
					throw new Exception("Error writing Audit Event: " +
										response.Errors.Aggregate((s1, s2) => s1 + ", " + s2));
			}
			catch (Exception ex)
			{
				Logger.Log(WfsLogLevel.Error, "Exception occurred in LogPageView\r\n" + ex.ToString());
			}
		}

	}
}
