﻿
define(['jquery', 'ko', 'frameworkUtils', 'uploadExtractDefinitionViewModel'],
function ($, ko, framework, uploadExtractDefinitionViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#uploadExtractDefinition');
            _vm = new uploadExtractDefinitionViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    return {
        init: init
    };
});