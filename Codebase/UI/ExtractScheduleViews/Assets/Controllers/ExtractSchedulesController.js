﻿define(['jquery', 'ko', 'frameworkUtils', 'scheduleListViewModel'],
function ($, ko, framework, scheduleListViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#extractSchedulesPortlet');
            _vm = new scheduleListViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    return {
        init: init
    };
});