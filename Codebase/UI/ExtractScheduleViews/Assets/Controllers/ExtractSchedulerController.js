﻿
define(['jquery', 'ko', 'frameworkUtils', 'schedulerViewModel'],
function ($, ko, framework, schedulerViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#schedulerbase');
            _vm = new schedulerViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    return {
        init: init
    };
});