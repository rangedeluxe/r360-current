﻿define(['jquery', 'ko', 'frameworkUtils', 'definitionListViewModel'],
function ($, ko, framework, definitionListViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#extractSchedulesPortlet');
            _vm = new definitionListViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    return {
        init: init
    };
});