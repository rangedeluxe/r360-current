﻿
define(['jquery', 'ko', 'frameworkUtils', 'uploadExtractBillingViewModel'],
function ($, ko, framework, uploadExtractBillingViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#uploadExtractBilling');
            _vm = new uploadExtractBillingViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    return {
        init: init
    };
});