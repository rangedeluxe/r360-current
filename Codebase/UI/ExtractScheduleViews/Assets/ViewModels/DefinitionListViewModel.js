﻿define(['jquery', 'ko', 'frameworkUtils', 'datatables', 'datatables-bootstrap'],

function ($, ko, framework) {
    var $getE;
    var _definitiontable;
    var _container;    
    let self = this;
    var applyUI = function (model) {
        
        // Initialize the Grid.

        var gridoptions = {
            "data": this.VisibleDefinitions(),
            "columns": [
                { "data": "ExtractDefinitionId", "orderable": false},
                { "data": "ExtractDefinitionId" },
                { "data": "ExtractName" },
                { "data": "ExtractFilename" },
                { "data": "ExtractDefinitionTypeName" },
                { "data": "ExtractDefinitionSize" },
            ],
            "order": [[2, 'asc']],
            "createdRow": function (row, data, index) {
                var html =  "<span class='options'>";
                html +=    self.CanManage() ? "<i title= 'Edit Definition' class='fa fa-edit fa-lg update' data-id='" + data.ExtractDefinitionId + "'></i>" : "";
                html +=         "<i title= 'View Schedule' class='fa fa-calendar fa-lg scheduler' data-id='" + data.ExtractDefinitionId + "'></i>";
                html +=         "<i title= 'Download Definition' class='fa fa-cloud-download fa-lg download' data-id='" + data.ExtractDefinitionId + "'></i>";
                html +=    self.CanManage() ? "<i title= 'Delete Definition' class='fa fa-times-circle fa-lg delete' data-id='" + data.ExtractDefinitionId + "'></i>" : "";
                html +=     "</span>";
                $('td', row).eq(0).html(html);
            }
        };

        _definitiontable = $getE('#definitionsTable').DataTable(gridoptions);

        // Wire up events for row clicks.
        $getE('#definitionsTable').on('click', '.update', function () {
            var id = $(this).data('id');
            var data = "?mode=update&id=" + id;
            var item = findItemById(id);
            var defType = item.ExtractDefinitionTypeName;
            var url = defType == 'Extract Billing'
                ? "/RecHubExtractScheduleViews/ExtractSchedules/UploadEB"
                : "/RecHubExtractScheduleViews/ExtractSchedules/Upload";

            var modalOptions = { closeCallback: updateData };
            framework.loadByContainer(url + data, framework.getModalContent(), {}, framework.openModal(modalOptions));
        });
        $getE('#definitionsTable').on('click', '.scheduler', function () {
            var id = $(this).data('id');
            framework.loadByContainer("/RecHubExtractScheduleViews/ExtractSchedules/ScheduleList?id=" + id, _container.parent());
        });
        $getE('#definitionsTable').on('click', '.download', function () {
            var id = $(this).data('id');
            window.location = "/RecHubExtractScheduleViews/ExtractSchedules/Download/" + id;
        });
        $getE('#definitionsTable').on('click', '.delete', function () {
            var id = $(this).data('id');
            framework.doJSON("/RecHubExtractScheduleViews/ExtractSchedules/Delete", { 'id': id }, function (result) {
                if (result.HasErrors && result.HasErrors === true
                    && result.Errors && result.Errors.length > 0)
                {
                    framework.errorToast(_container, result.Errors[0].Message);
                    return;
                }
                updateData();
            });
        });

        var modalOptions = { closeCallback: updateData };

        // Wire up our 'new' buttons.
        $getE('#addextractbutton').click(function() {
            framework.loadByContainer("/RecHubExtractScheduleViews/ExtractSchedules/Upload", framework.getModalContent(), {}, framework.openModal(modalOptions));
        });
        $getE('#addbillingbutton').click(function () {
            framework.loadByContainer("/RecHubExtractScheduleViews/ExtractSchedules/UploadEB", framework.getModalContent(), {}, framework.openModal(modalOptions));
        });

        // Finally gather our data.        

        updateData();
    };

    var findItemById = function(id) {
        var output = null;
        $(VisibleDefinitions()).each(function (i, e) {
            if (e.ExtractDefinitionId === id)
                output = e;
        });
        return output;
    }

    var updateData = function () {
        framework.showSpinner('Loading...');
        framework.doJSON('/RecHubExtractScheduleViews/ExtractSchedules/GetDefinitions', {}, function (data) {
            this.CanManage(data.CanManage);
            this.VisibleDefinitions(data.Definitions);
            updateGrid();
            framework.hideSpinner();
        });
    };

    var updateGrid = function () {
        _definitiontable.clear();
        _definitiontable.data().rows.add(this.VisibleDefinitions());
        _definitiontable.order();
        _definitiontable.draw();
    };

    // Constructor
    function definitionListViewModel() {

    }

    // Public properties
    this.VisibleDefinitions = ko.observable([]);
    this.CanManage = ko.observable(false);
    
    // Public methods
    definitionListViewModel.prototype = {
        constructor: definitionListViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) { return _container.find(b); };

            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            $getE('#definitionsTable').off('click');
            VisibleDefinitions([]);
            CanManage(false);
            if (_definitiontable)
                _definitiontable.destroy();
        }
    };

    // Return a pointer to the function so the caller can instanciate.
    return definitionListViewModel;
});

