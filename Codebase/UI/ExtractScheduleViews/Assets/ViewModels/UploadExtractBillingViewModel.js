﻿define(["jquery", "ko", "frameworkUtils", '/Assets/jquery/js/jquery.form.js'],
function ($, ko, framework) {
    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };

    var setupElementBindings = function (model) {

        securityToken(model.SecurityToken);
        heading(model.Heading);
        extractName(model.ExtractName);
        selectedExtractBillingName(model.SelectedExtractBillingName);
        selectedExtractBillingValue(model.SelectedExtractBillingValue);
        id(model.Id);
        isNewDefinition = true;
        mode(model.Mode);
        labels = model.Labels;
        isInvalidXml(false);
        extractBillings = model.ExtractBillings;

        if (model.Mode != 'new') {
            isNewDefinition = false;
        }

        $getE(".extractRequiredEB").hide();
        $getE(".extractNameError").hide();

        $getE("#btnCancel").bind("click", function (event) {
            event.preventDefault();
            framework.closeModal();
        });

        $getE("#selExtractBilling").bind("change", function () {
            var extractValue = $(this).val();
            var extractName = $getE("#selExtractBilling option:selected").text();
            $getE("#SelectedExtractBillingName").val(extractName);
            $getE("#SelectedExtractBillingValue").val(extractValue);

            if ($getE("#extractName").val() == '')
                $getE("#extractName").val(extractName);
        });


        $getE("#btnUploadExtractBilling").bind("click", function (event) {
            event.preventDefault();

            var result = true;
            $getE(".extractRequiredEB").hide();
            $getE(".extractNameError").hide();

            $getE("#extractName").each(function () {
                if (!$(this).val() || $(this).val() == '') {
                    $getE(".extractNameError").show();
                    result = false;
                }
            });

            if (_vm.isNewDefinition) {
                var selBilling = $getE("#selExtractBilling").val();
                if (!selBilling) {
                    $getE(".extractRequiredEB").show();
                    result = false;
                }
            }

            if (!result)
                return;

            var $form = $($(this).parents('form'));
            $.ajax({
                type: "POST",
                url: $form.attr("action"),
                data: $form.serialize(), // serializes the form's elements.
                success: function (data) {
                }
            });

            framework.closeModal();
        });

    };


    // Constructor
    function uploadExtractBillingViewModel() {

    }

    // Public properties
    this.securityToken = ko.observable('');
    this.heading = ko.observable('');
    this.extractName = ko.observable('');
    this.selectedExtractBillingName = ko.observable('');
    this.selectedExtractBillingValue = ko.observable('');
    this.id = ko.observable(0);
    this.isNewDefinition = true;
    this.mode = ko.observable('');
    this.labels = {};
    this.isInvalidXml = ko.observable(false);
    this.extractBillings = [];

    // Public methods
    uploadExtractBillingViewModel.prototype = {
        constructor: uploadExtractBillingViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) { return _container.find(b); };
            setupElementBindings(model);
            ko.applyBindings(self, _container.get(0));
        },
        dispose: function () {

        }
    };

    // Return a pointer to the function so the caller can instanciate.
    return uploadExtractBillingViewModel;
});

