﻿define(["jquery", "ko", "frameworkUtils", '/Assets/jquery/js/jquery.form.js'],
function ($, ko, framework) {
    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };

    function init(myContainerId, model) {
        $(function () {
            _container = $('#' + myContainerId);
            $getE = function (b) { return _container.find(b); };

            _vm = new sViewModel(model);
            ko.applyBindings(_vm, $getE('#uploadBase').get(0));

            setupElementBindings();
        });
    }

    var setupElementBindings = function (model) {

        self.currentData(model);
        self.securityToken(model.SecurityToken);
        self.heading(model.Heading);
        self.extractName(model.ExtractName);
        self.id(model.Id);
        self.isNewDefinition = true;
        self.mode(model.Mode);
        self.labels = model.Labels;
        self.isInvalidXml(false);
        if (model.Mode != 'new') {
            self.isNewDefinition = false;
        }

        $getE(".extractNameError").hide();
        $getE(".extractRequiredFile").hide();

        $getE('.btn-file :file').on('fileselect', function (event, numFiles, label) {
            var input = $getE('#uploadFilePlaceholder'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
            if (input.length) {
                input.val(log);
            }
        });

        $getE("#btnCancel").bind("click", function (event) {
            event.preventDefault();
            framework.closeModal();
        });

        $getE('.btn-file :file').on('change', function () {
            var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

            var nameelement = $getE("#ExtractName");
            if (nameelement.val() === '')
                nameelement.val(label.replace(/\.[^/.]+$/, ""));
            input.trigger('fileselect', [numFiles, label]);
        });

        $getE("#btnFinalUpload").on("click", function (event) {
            event.preventDefault();
            var $form = $($(this).parents('form'));

            if (!validateForm())
                return;

            // Create the iframe, due to IE9 bug for downloading json
            $('<iframe />', {
                id: 'upload_iframe',
                name: 'upload_iframe',
                width: '0"',
                height: '0"',
                border: '0"',
                style: 'width: 0; height: 0; border: none;"'
            }).appendTo('body');

            window.frames['upload_iframe'].name = "upload_iframe";

            var iframeId = document.getElementById("upload_iframe");

            // Add event...
            var eventHandler = function () {

                if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                else iframeId.removeEventListener("load", eventHandler, false);

                // Message from server...
                if (iframeId.contentDocument) {
                    content = iframeId.contentDocument.body.innerHTML;
                } else if (iframeId.contentWindow) {
                    content = iframeId.contentWindow.document.body.innerHTML;
                } else if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }

                var data = $.parseJSON(content);
                framework.hideSpinner();

                // Check for an error.
                if (data.Error) {
                    $getE("#spnInvalidXml").text(data.Error);
                    $getE("#spnInvalidXml").show();
                    return;
                }


                framework.closeModal();

                // Del the iframe...
                setTimeout(function () { $("#upload_iframe").remove(); }, 250);
            }

            if (iframeId.addEventListener)
                iframeId.addEventListener("load", eventHandler, true);
            if (iframeId.attachEvent)
                iframeId.attachEvent("onload", eventHandler);


            // Set properties of form...
            $form.attr("target", "upload_iframe");
            $form.attr("method", "post");
            $form.attr("enctype", "multipart/form-data");
            $form.attr("encoding", "multipart/form-data");

            $form.submit();

        });

        $getE("#btnSaveName").on("click", function (event) {
            event.preventDefault();

            var result = true;
            $getE(".extractNameError").hide();
            if (extractName().length == 0) {
                $getE(".extractNameError").show();
                result = false;
            }

            if (!result)
                return;

            var $form = $($(this).parents('form'));
            $.ajax({
                type: "POST",
                url: "/RecHubExtractScheduleViews/ExtractSchedules/SaveExtractWizardName",
                data: $form.serialize(), // serializes the form's elements.
                success: function (data) {
                }
            });

            framework.closeModal();

        });

    };

    function validateForm() {
        var result = true;
        // kick out early if we're updating.
        if ($getE('input[name="Mode"]').val() == 'update')
            return true;

        $getE(".extractNameError").hide();

        $getE("input[name='ExtractName']").each(function () {
            if (!$(this).val() || $(this).val() == '') {
                $getE(".extractNameError").show();
                result = false;
            }
        });
        return result;
    }

    // Constructor
    function uploadExtractDefinitionViewModel() {

    }

    // Public properties
    this.currentData = ko.observable({});
    this.securityToken = ko.observable('');
    this.heading = ko.observable('');
    this.extractName = ko.observable('');
    this.id = ko.observable(0);
    this.isNewDefinition = true;
    this.mode = ko.observable('');
    this.isInvalidXml = ko.observable(false);

    // Public methods
    uploadExtractDefinitionViewModel.prototype = {
        constructor: uploadExtractDefinitionViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) { return _container.find(b); };

            setupElementBindings(model);
            ko.applyBindings(self, _container.get(0));
        },
        dispose: function () {

        }
    };

    // Return a pointer to the function so the caller can instanciate.
    return uploadExtractDefinitionViewModel;
});

