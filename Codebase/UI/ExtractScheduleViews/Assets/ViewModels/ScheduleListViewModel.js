﻿define(["jquery", "ko", "frameworkUtils", 'wfs.select', 'datatables', 'datatables-bootstrap'],
function ($, ko, framework) {
    var $getE;
    var _scheduletable;
    let self = this;
    var formatScheduleType = function (value) {
        if (value && value.toString() == "2")
            return "Monthly";
        else
            return "Weekly";
    };

    var applyUI = function (model) {

        // Data coming from the server on page load.
        CurrentDefinition(model.SelectedDefinition);
        AllDefinitions = model.AllDefinitions;

        // Setup the drop-down definition list.
        $getE('#definitionList').wfsSelectbox({
            items: self.AllDefinitions,
            callback: function (data) {
                if (data)
                    setDefinition(data);
            },
            displayField: 'ExtractName',
            idField: 'ExtractDefinitionId'
        });

        // Setup the Grid.
        var gridoptions = {
            "data": this.Schedules(),
            "columns": [
                { "data": "ExtractScheduleId", "orderable": false },
                { "data": "ExtractScheduleId" },
                { "data": "Description" },
                { "data": "ScheduleTimeString" },
                { "data": "ScheduleType" },
                { "data": "Sunday" },
                { "data": "Monday" },
                { "data": "Tuesday" },
                { "data": "Wednesday" },
                { "data": "Thursday" },
                { "data": "Friday" },
                { "data": "Saturday" },
                { "data": "DayInMonth" },
            ],
            "order": [[2, 'asc']],
            "createdRow": function (row, data, index) {
                // Create the option column.
                if (self.CanManage()) {
                    var html = "<span class='options'>";
                    html += "<i title='Edit Schedule' class='fa fa-edit fa-lg update' data-id='" +
                        data.ExtractScheduleId + "'></i>";
                    html += "<i title='Delete Schedule' class='fa fa-times-circle fa-lg delete' data-id='" +
                        data.ExtractScheduleId + "'></i>";
                    html += "</span>";
                    $('td', row).eq(0).html(html);
                }

                // Format the 'Type' column.
                var str = formatScheduleType(data.ScheduleType);
                $('td', row).eq(4).html(str);

                // Change the 'true/false' on the days columns to a check.
                var html =  "<span>";
                html +=         "<i class='fa fa-{class} fa-lg'></i>"
                html +=     "</span>";

                // Clear the column out for a 'monthly' type.
                if (data.ScheduleType === 2)
                    html = "";

                var sun = data.Sunday ? "check" : "times";
                var mon = data.Monday ? "check" : "times";
                var tues = data.Tuesday ? "check" : "times";
                var wed = data.Wednesday ? "check" : "times";
                var thurs = data.Thursday ? "check" : "times";
                var fri = data.Friday ? "check" : "times";
                var sat = data.Saturday ? "check" : "times";
                $('td', row).eq(5).html(html.replace("{class}", sun));
                $('td', row).eq(6).html(html.replace("{class}", mon));
                $('td', row).eq(7).html(html.replace("{class}", tues));
                $('td', row).eq(8).html(html.replace("{class}", wed));
                $('td', row).eq(9).html(html.replace("{class}", thurs));
                $('td', row).eq(10).html(html.replace("{class}", fri));
                $('td', row).eq(11).html(html.replace("{class}", sat));

                // Set the visibility for the last column, which is only monthly schedules.
                if (data.ScheduleType !== 2)
                    $('td', row).eq(12).html('');
            }
        };
        _scheduletable = $getE('#schedulesTable').DataTable(gridoptions);

        // Hook up the optionset with edit/deletes.
        $getE('#schedulesTable').on('click', '.update', function () {
            var id = $(this).data('id');
            var data = { 'mode': 'edit', 'id': id, 'defid': CurrentDefinition().ExtractDefinitionId };
            var modalOptions = { closeCallback: updateData };
            framework.loadByContainer("/RecHubExtractScheduleViews/ExtractSchedules/Scheduler", framework.getModalContent(), data, framework.openModal(modalOptions));
        });
        $getE('#schedulesTable').on('click', '.delete', function () {
            var id = $(this).data('id');
            var sched = findItemById(id);
            framework.doJSON("/RecHubExtractScheduleViews/ExtractSchedules/DeleteSchedule", { 'id': id, 'defid': sched.ExtractDefinitionId }, function () {
                updateData();
            });
        });

        // Hook up the link for the back button.
        $getE('#definitionslink').click(function () {
            framework.loadByContainer("/RecHubExtractScheduleViews/ExtractSchedules", _container.parent());
        });

        // Hook up the add schedule button.
        $getE('#addschedulebutton').click(function () {
            var data = { 'mode': 'new', 'defid': CurrentDefinition().ExtractDefinitionId };
            var modalOptions = { closeCallback: updateData };
            framework.loadByContainer("/RecHubExtractScheduleViews/ExtractSchedules/Scheduler", framework.getModalContent(), data, framework.openModal(modalOptions));
        });

        // Set our definition, and which hits the callback reloads the schedule data.
        $getE('#definitionList').wfsSelectbox('setValue', CurrentDefinition().ExtractDefinitionId);
    };

    var findItemById = function (id) {
        var output = null;
        $(Schedules()).each(function (i, e) {
            if (e.ExtractScheduleId === id)
                output = e;
        });
        return output;
    }

    var setDefinition = function (definition) {
        self.CurrentDefinition(definition);
        updateData();
    };

    var updateData = function () {
        framework.showSpinner('Loading...');
        framework.doJSON('/RecHubExtractScheduleViews/ExtractSchedules/GetSchedules', { id: CurrentDefinition().ExtractDefinitionId }, function (data) {
            this.Schedules(data.Schedules);
            this.CanManage(data.CanManage);
            updateGrid();
            framework.hideSpinner();
        });
    };

    var updateGrid = function () {
        if (!self.CanManage()){
            _scheduletable.column(0).visible(false);
        }
        
        _scheduletable.clear();
        _scheduletable.data().rows.add(this.Schedules());
        _scheduletable.order();
        _scheduletable.draw();
    };

    // Constructor
    function scheduleListViewModel() {

    }

    // Public properties
    this.Schedules = ko.observable([]);
    this.CurrentDefinition = ko.observable({});
    this.AllDefinitions = [];
    this.CanManage = ko.observable(false);
    // Public methods
    scheduleListViewModel.prototype = {
        constructor: scheduleListViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) { return _container.find(b); };

            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            $getE('#schedulesTable').off('click');
            AllDefinitions = [];
            CurrentDefinition({});
            Schedules([]);
            CanManage(false);
            if (_scheduletable)
                _scheduletable.destroy();
        }
    };

    // Return a pointer to the function so the caller can instanciate.
    return scheduleListViewModel;
});

