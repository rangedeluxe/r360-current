﻿define(["jquery", "ko", "frameworkUtils", "wfs.select", "select2"],
    function ($, ko, framework) {

        function formatTime(n) {
            return n > 9
                ? "" + n
                : "0" + n;
        }

        var _me = this;
        var _container = {};
        var _controls = {};
        let _outputDrop;
        var $getE = function (b) { alert("Not initialized!"); };

        var setupElementBindings = function (model) {

            // Update the model
            this.schedule = ko.observable(model.Schedule);
            this.ExtractRunArgs = ko.observable(this.schedule().ExtractRunArguments);
            this.mode = ko.observable(model.Mode);
            this.labels = model.Labels;
            this.allDefinitions = model.AllDefinitions;
            this.isEditMode = model.Mode == 'edit' || model.Mode == 'new';
            this.isNewMode = model.Mode == 'new';
            this.timeFormat = ko.computed({
                read: function (key) {
                    return formatTime(this.schedule().ScheduleTime.Hours) + ':' + formatTime(this.schedule().ScheduleTime.Minutes);
                },
                write: function (value) {
                    this.schedule().ScheduleTime.Hours = value.split(":")[0];
                    this.schedule().ScheduleTime.Minutes = value.split(":")[1];
                }
            });
            this.scheduleType = ko.computed({
                read: function (key) {
                    return this.schedule().ScheduleType.toString();
                },
                write: function (value) {
                    return this.schedule().ScheduleType = value;
                }
            });

            // Update the layout for modes.
            var st = this.schedule().ScheduleType.toString();
            if (st == "2") {
                $getE("#tblWeeklySchedule").hide();
                $getE("#tblMonthlySchedule").show();
            }
            else {
                $getE("#tblWeeklySchedule").show();
                $getE("#tblMonthlySchedule").hide();
            }

            var isEW = false;
            if (this.allDefinitions) {
                for (var i = 0; i < this.allDefinitions.length; i++) {
                    var def = this.allDefinitions[i];
                    if (def.ExtractDefinitionId == this.schedule().ExtractDefinitionId) {
                        this.schedule().ExtractName = def.ExtractName;
                        if (def.ExtractDefinitionType == 1) {
                            isEW = true;
                        }
                        else {
                            isEW = false;
                        }
                    }
                }

            }
            this.isExtractWizard = isEW;




            _outputDrop = $getE('#outputMode').wfsSelectbox({
                items: [
                    { type: 0, name: "Write to disk" }, { type: 1, name: "Send to notifications" },
                    { type: 2, name: "Write to disk and send to notifications" }
                ],
                displayField: 'name',
                idField: 'type',
                width: '500px',
                callback: function(selected) {
                    if (selected && _me.schedule() && _me.schedule().ExtractRunArguments && _me.schedule().ExtractRunArguments.trim() !== '' &&
                        (_me.schedule().ExtractRunArguments.indexOf('/o:') >= 0 || _me.schedule().ExtractRunArguments.indexOf('/outpudmode:') >= 0)) {
                        _me.schedule().ExtractRunArguments = _me.schedule().ExtractRunArguments.replace(/\/o(utputmode)?:([^\s]+)/, '/o:' + selected.id);
                        _me.ExtractRunArgs(_me.schedule().ExtractRunArguments);
                    }
                    else if (selected && _me.schedule()) {
                        _me.schedule().ExtractRunArguments = '/p:{today} /o:' + selected.id;
                        _me.ExtractRunArgs(schedule().ExtractRunArguments);
                    }
                }
            });


            $getE('#outputMode').wfsSelectbox('setValue', getOutputMode());


            //hide any error messages
            $getE(".requiredDescriptionError").hide();
            $getE(".requiredTimeError").hide();
            $getE(".argumentFormatError").hide();

            //set the month days
            var days = [];
            for (var i = 0; i < 32; i++) {
                days.push({
                    dayId: (i + 1).toString(),
                    dayLabel: (i + 1).toString()
                });
            }
            days[31] = {
                dayId: "32",
                dayLabel: "Last"
            };
            _controls['wfsbox'] = $getE('.monthDays').wfsSelectbox({ 'items': days, 'displayField': 'dayLabel', 'idField': 'dayId', 'placeHolder': '1', minimumResultsForSearch: -1, allowClear: false });
            _controls['wfsbox'].wfsSelectbox('setValue', schedule().DayInMonth);
            $getE('.monthDays').on("change", function (e) {
                schedule().DayInMonth = e.val;
            })

            if (!isEditMode) {
                //disable the select box
                _controls['wfsbox'].wfsSelectbox("setEnabled", false);
            }


            var actionTitle = labels.UpdateSchedule;
            if (isNewMode) {
                actionTitle = labels.NewSchedule;
            }

            $getE("#actionTitle").text(actionTitle);

            $getE(".scheduleType").bind("click", function (event) {
                var val = $(this).val();
                if (val == "2") {
                    $getE("#tblWeeklySchedule").hide();
                    $getE("#tblMonthlySchedule").show();
                }
                else {
                    $getE("#tblWeeklySchedule").show();
                    $getE("#tblMonthlySchedule").hide();
                }
            });

            $getE("#btnUpdate").bind("click", function (event) {
                event.preventDefault();
                _me.schedule().ExtractRunArguments = _me.ExtractRunArgs();
                if (!validateSchedule())
                    return;
                
                var data = ko.toJSON(schedule);

                framework.showSpinner();

                var url = schedule().ExtractScheduleId == 0
                    ? "/RecHubExtractScheduleViews/ExtractSchedules/AddSchedule"
                    : "/RecHubExtractScheduleViews/ExtractSchedules/UpdateSchedule";

                $.ajax({
                    url: url,
                    type: "post",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    success: function () {
                        framework.hideSpinner();
                        var defid = schedule().ExtractDefinitionId;
                        framework.closeModal();
                    },
                    error: function () {
                        // TODO: show error.
                        console.log("Error on Update");
                        framework.hideSpinner();
                    }
                });

            });

            $getE("#btnReset").bind("click", function (event) {
                event.preventDefault();
                //reload without saving
                var id = $(this).data('id');
                framework.loadByContainer("/RecHubExtractScheduleViews/ExtractSchedules/Scheduler?mode=edit&id=" + id, _container);
            });

            $getE("#btnDelete").bind("click", function (event) {
                event.preventDefault();
                if (!window.confirm("Are you sure?"))
                    return;
                framework.showSpinner();
                var id = $(this).data('id');
                var defid = $(this).data('defid');
                $.post("/RecHubExtractScheduleViews/ExtractSchedules/DeleteSchedule?id=" + id + "&defid=" + defid, function (data) {
                    framework.hideSpinner();
                    framework.closeModal();
                });


            });

            $getE(".viewSchedules").bind("click", function (event) {
                event.preventDefault();
                //go back to the schedule list page
                var id = $(this).data('defid');
                framework.closeModal();
            });

            $getE("#btnEdit").bind("click", function (event) {
                event.preventDefault();
                var id = $(this).data('id');

                framework.loadByContainer("/RecHubExtractScheduleViews/ExtractSchedules/Scheduler?mode=edit&id=" + id, _container);
            });

            $getE("#btnRunNow").bind("click", function (event) {
                event.preventDefault();
                var id = $(this).data('id');

                framework.doJSON("/RecHubExtractScheduleViews/ExtractSchedules/GenerateFileNow", { "id": id });
                framework.noticeToast(_container, "File generation process has been started.");
            });

            function validateSchedule() {
                var result = true;
                $getE(".requiredDescriptionError").hide();
                $getE(".requiredTimeError").hide();
                $getE(".argumentFormatError").hide();

                if (!schedule().Description || schedule().Description.length == 0) {
                    $getE(".requiredDescriptionError").show();
                    result = false;
                }

                if (!validateTime($getE(".time").val())) {
                    $getE(".requiredTimeError").show();
                    result = false;
                }

                if (!isExtractWizard) {
                    var xml = "<a " + schedule().ExtractRunArguments + "/>";
                    var oParser = new DOMParser();
                    try {
                        var oDOM = oParser.parseFromString(xml, "text/xml");
                        if (oDOM.documentElement == "parsererror") {
                            $getE(".argumentFormatError").show();
                            result = false;
                        }
                    }
                    catch (e) {
                        $getE(".argumentFormatError").show();
                        result = false;
                    }
                }

                return result;
            }

            function getOutputMode () {
                let result = /\/o(utputmode)?:([^\s]+)/.exec(schedule().ExtractRunArguments);
                return result == null ? 0 : result[2];
            }


            function validateTime(s) {
                var t = s.split(':');

                return /^\d?\d:\d\d?$/.test(s) &&
                    t[0] >= 0 && t[0] < 25 &&
                    t[1] >= 0 && t[1] < 60;
            }


        };


        // Constructor
        function schedulerViewModel() {

        }

        // Public properties
        this.schedule = ko.observable({});
        this.mode = ko.observable('');
        this.labels = [];
        this.allDefinitions = [];
        this.timeFormat = null;
        this.scheduleType = null;
        this.isEditMode = false;
        this.isNewMode = false;
        this.isExtractWizard = false;
        this.ExtractRunArgs = ko.observable('');
        // Public methods
        schedulerViewModel.prototype = {
            constructor: schedulerViewModel,
            init: function (container, model) {
                _container = container;
                $getE = function (b) { return _container.find(b); };
                setupElementBindings(model);
                ko.applyBindings(self, _container.get(0));
            },
            dispose: function () {

            }
        };

        // Return a pointer to the function so the caller can instanciate.
        return schedulerViewModel;

    });

