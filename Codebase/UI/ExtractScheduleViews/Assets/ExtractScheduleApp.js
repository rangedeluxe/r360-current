﻿function initExtractSchedules(model, controllername) {
    require.config({
        paths: {
            'extractDefinitionsController': '/RecHubExtractScheduleViews/Assets/Controllers/ExtractDefinitionsController',
            'extractSchedulesController': '/RecHubExtractScheduleViews/Assets/Controllers/ExtractSchedulesController',
            'extractUploadDefinitionController': '/RecHubExtractScheduleViews/Assets/Controllers/ExtractUploadDefinitionController',
            'extractUploadBillingController': '/RecHubExtractScheduleViews/Assets/Controllers/ExtractUploadBillingController',
            'extractSchedulerController': '/RecHubExtractScheduleViews/Assets/Controllers/ExtractSchedulerController',

            'definitionListViewModel': '/RecHubExtractScheduleViews/Assets/ViewModels/DefinitionListViewModel',
            'scheduleListViewModel': '/RecHubExtractScheduleViews/Assets/ViewModels/ScheduleListViewModel',
            'uploadExtractDefinitionViewModel': '/RecHubExtractScheduleViews/Assets/ViewModels/UploadExtractDefinitionViewModel',
            'uploadExtractBillingViewModel': '/RecHubExtractScheduleViews/Assets/ViewModels/UploadExtractBillingViewModel',
            'schedulerViewModel': '/RecHubExtractScheduleViews/Assets/ViewModels/SchedulerViewModel',
        }
    });

    require([controllername], function (controller) {
        controller.init(model);
    });
}