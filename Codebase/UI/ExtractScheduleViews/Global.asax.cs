﻿using System.Linq;
using System.Security.Claims;
using System.Threading;
using log4net.Config;
using StructureMap;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske, Dave Wanta
* Date: 04/01/2014
*
* Purpose: 
*
* Modification History
*   WI 132768 BLR 04/02/2014
*     - Initial Version
******************************************************************************/
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Wfs.Raam.Core;
using Wfs.Raam.TokenCache;

namespace ExtractScheduleViews
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            //RouteDebug.RouteDebugger.RewriteRoutesForTesting(RouteTable.Routes);

			XmlConfigurator.Configure();
			ObjectFactory.Initialize(x =>
            {
                x.PullConfigurationFromAppConfig = true;
            });

			WSPassiveSessionConfiguration.Start();
		}

		public override void Init()
		{
			WSPassiveSessionConfiguration.Init();
			base.Init();
		}

        protected void Application_PreSendRequestHeaders()
        {
            var allowFrom = string.Empty;
            var principal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (principal != null)
            {
                var identity = principal.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    var allowFromClaim = identity.Claims.FirstOrDefault(c => c.Type == WfsClaimTypes.AllowFromUrlInFrame);
                    allowFrom = allowFromClaim == null ? "" : allowFromClaim.Value;
                }
            }

            if (!string.IsNullOrEmpty(allowFrom))
            {
                Response.Headers["X-Frame-Options"] = "ALLOW-FROM " + allowFrom;
                Response.Headers["Content-Security-Policy"] = "frame-ancestors " + allowFrom;
            }
            else
            {
                Response.Headers["X-Frame-Options"] = "SAMEORIGIN";
                Response.Headers["Content-Security-Policy"] = "frame-ancestors 'self'";
            }
        }
    }
}