﻿using CommonUtils;
using CommonUtils.Web;
using FrameworkClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Web.Mvc;
using Wfs.Raam.Core;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske, Dave Wanta
* Date: 04/01/2014
*
* Purpose: 
*
* Modification History
*   WI 132768 BLR 04/02/2014
*     - Initial Version
* WI 146351 DJW 6/23/2014
*  Added Logging/Auditing support 
******************************************************************************/

namespace ExtractScheduleViews.Controllers
{
    public class BaseController : WfsSharedBaseController
    {
        public LanguageManager _LanguageManager { get; set; }
        //public R360ServiceManager _R360ServiceManager { get; set; }
        //public IPermission _HubPermissions { get; set; }
        //public ClientPayerServiceClient _PayerManager { get; set; }

        protected static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(BaseController));

        public BaseController()
        {
            // initialize multi lingual
            //_R360ServiceManager = new R360ServiceManager();
            _LanguageManager = new LanguageManager();
           // _HubPermissions = new HubPermissions(_R360ServiceManager);
            //_PayerManager = new ClientPayerServiceClient();

            string sid = ConfigurationManager.AppSettings["MockSid"];
            string rechubSession = ConfigurationManager.AppSettings["MockRecHubSession"];
            var doMock = !String.IsNullOrEmpty(sid) ? true : false;
            if (doMock)
            {
                ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.Sid, sid) }));
                ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { new Claim(WfsClaimTypes.SessionId, rechubSession) }));
            }
        }

        public string Get(string group, string property)
        {
            return _LanguageManager.Get("ExtractScheduleViews", group, property);
        }

        public Dictionary<string, string> GetLabelsForPage(string page, bool includeGeneral)
        {
            var pageLabels = new Dictionary<string, string>();

            var doMock = Convert.ToBoolean(ConfigurationManager.AppSettings["MockData"]);
            if (doMock)
            {
                pageLabels = addMock(page);
            }
            else
            {
				pageLabels = _LanguageManager.GetGroup("ExtractScheduleViews", page);
                if (includeGeneral)
                {
					var general = _LanguageManager.GetGroup("ExtractScheduleViews", "General");
                    var generalBase = _LanguageManager.GetGroup("Base", "General");
                    pageLabels = _LanguageManager.Merge(_LanguageManager.Merge(pageLabels, general), generalBase);
                }
            }

            return pageLabels;
        }

        private Dictionary<string, string> addMock(string page)
        {
            var overall = new Dictionary<string, Dictionary<string, string>>();

            var recsum = new Dictionary<string, string>();
            recsum.Add("Title", "Receivables Summary");
            overall.Add("Summary", recsum);

            var sum = new Dictionary<string, string>();
            sum.Add("Title", "Batch Summary");
            overall.Add("BatchSummary", sum);

            var det = new Dictionary<string, string>();
            det.Add("Title", "Batch Details");
            overall.Add("BatchDetails", det);

            var tran = new Dictionary<string, string>();
            tran.Add("Title", "Transaction Details");
            overall.Add("TransactionDetails", tran);

            var dash = new Dictionary<string, string>();
            dash.Add("Title", "Dashboard");
            overall.Add("Dashboard", dash);

            var rvalue = overall[page];

            rvalue.Add("Denom", "$");
            rvalue.Add("NoData", "No Data Available.");

            return rvalue;
        }

        // convenience method to get a dictionary from an array for ui... 
        protected Dictionary<string, string> getEnumKeyValue<T>()
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("T is not an Enum type");
            if (Enum.GetUnderlyingType(typeof(T)) != typeof(int))
                throw new ArgumentException("The underlying type of the enum T is not Int32");
            return Enum.GetValues(typeof(T)).Cast<T>()
                .ToDictionary(t => ((int)(object)t).ToString(), t => t.ToString());
        }

        protected void SetResponse(BaseResponse response, JSONResponse webResponse, object data, [CallerMemberName] string caller = "")
        {
            webResponse.HasErrors = (response.Status == StatusCode.FAIL) ? true : false;
            webResponse.Data = data;
            if (webResponse.HasErrors)
            {
                if (response.Errors != null && response.Errors.Count > 0)
                {
                    webResponse.AddErrors(response.Errors);
                }
                else
                {
                    Logger.Log(Wfs.Logging.LogLevel.Debug, "Server response status set to FAIL but provided no message.  " + caller);
                    webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = Get("General", "InternalServerError") } };
                }
            }
        }

        protected void SetErrorResponse(JSONResponse webResponse, string debugMessage, string message, [CallerMemberName] string caller = "")
        {
            Logger.Log(Wfs.Logging.LogLevel.Debug, debugMessage + " - " + caller);
            webResponse.HasErrors = true;
            webResponse.Errors = new List<WebErrors>() { new WebErrors() { Message = message } };
        }

        protected int GetNewRN(IList<int> taken, Random random)
        {
            int rn = random.Next(1, 75);
            if (taken.Contains(rn))
            {
                rn = GetNewRN(taken, random);
            }
            return rn;
        }

    }
}
