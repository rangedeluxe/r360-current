﻿
using ExtractScheduleClient;
using ExtractScheduleViews.Models;
using ExtractScheduleViews.Models.DAL;
using WFS.RecHub.BillingExtracts.ServicesClient;
using WFS.RecHub.BillingExtracts.Common;
using CommonUtils.Web;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;

namespace ExtractScheduleViews.Controllers
{
    public class ExtractSchedulesController : BaseController
    {
        public const string DEFAULT_RUN_ARGUMENTS = "/p:{today} /o:0";

        private readonly IExtractScheduleClient extractConfigurationClient;

        public ExtractSchedulesController()
        {
            extractConfigurationClient = new ExtractScheduleClient.ExtractScheduleClient();
        }

        /// <summary>
        /// Default view, shows a list of Extract Defs.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.View)]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.View)]
        [HttpPost]
        public ActionResult GetDefinitions()
        {
            var definitions = extractConfigurationClient.GetAllDefinitions()
                .Select(ExtractDefinitionDTO.FromServiceReference);

            return Json(new
            {
                Definitions = definitions,
                CanManage = R360Permissions.Current.Allowed(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage) 
            });
        }

        /// <summary>
        /// Front page editing of the Extract Definition Names.
        /// </summary>
        /// <param name="definitions"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult Save(IEnumerable<ExtractDefinitionDTO> definitions)
        {
            // Populate our definitions ahead of time.
            var dbdefs = extractConfigurationClient.GetAllDefinitions()
                .Select(x => ExtractDefinitionDTO.FromServiceReference(x));

            foreach (var d in definitions)
            {
                // Grab the old version first, so we don't update every field.
                var dbdef = dbdefs.FirstOrDefault(x => x.ExtractDefinitionId == d.ExtractDefinitionId);

                // Kick-out early if the database failed to find the id.
                if (dbdef == null)
                    continue;

                // Update our values only if non-whitespace. (Server-Side Validation)
                if (!string.IsNullOrWhiteSpace(d.ExtractName))
                    dbdef.ExtractName = d.ExtractName;

                // Send the values to the DB.
                extractConfigurationClient.UpdateDefinition(dbdef.ToServiceReference());
            }            
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Simply deletes an extract Def.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public JsonResult Delete(int id)
        {
            // Simply attempt to delete the database entry.
            var item = new ExtractDefinitionDTO()
            {
                ExtractDefinitionId = id
            }.ToServiceReference();
            bool result = extractConfigurationClient.RemoveDefinition(item);
            List<string> errors = null;
            if (!result)
            {
                errors = new List<string>();
                Dictionary<string,string> lables = GetLabels("Delete");
                errors.Add(lables["DeleteError"]);
            }

            var response = new
            {
                HasErrors = !result,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };
            return this.Json(response);
        }

        /// <summary>
        /// For displaying the Upload form.
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="extractName"></param>
        /// <returns></returns>
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult Upload(string mode, int? id)
        {
            UploadModel model = new UploadModel();
            // Server-side validation.  An ID must be present if 'update' is passed.
            // So, force 'new' if ID is not passed.
            model.Mode = id.HasValue ? mode : "new";
            model.Id = id.HasValue ? id.Value : 0;
            if (string.Equals(model.Mode, "update", StringComparison.OrdinalIgnoreCase))
            {
                var def = ExtractDefinitionDTO
                    .FromServiceReference(extractConfigurationClient.GetAllDefinitions()
                        .FirstOrDefault(x => x.ExtractDefinitionId == id));
                model.Heading = "Replace Extract Configuration File for \"" + def.ExtractName + "\"";
                model.ExtractName = def.ExtractName;
              
            }
            else
            {
                model.Mode = "new";
                model.Heading = "Add Extract Configuration File";
            }

            model.Labels = GetLabels("Upload");

            return View(model);
        }

        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult SaveExtractWizardName(int id, string extractName)
        {
            if ((extractName != null) && (extractName.Trim().Length > 0))
            {
                extractConfigurationClient.UpdateExtractName(id, extractName);
            }


            return new EmptyResult();
        }

        /// <summary>
        /// Used for displaying the upload Extract Billing form.
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult UploadEB(string mode, int? id)
        {
            try
            {

                UploadEBModel model = new UploadEBModel();
                // Server-side validation.  An ID must be present if 'update' is passed.
                // So, force 'new' if ID is not passed.
                model.Mode = id.HasValue ? mode : "new";
                model.Id = id.HasValue ? id.Value : 0;
                if (string.Equals(model.Mode, "update", StringComparison.OrdinalIgnoreCase))
                {
                    var def = ExtractDefinitionDTO
                        .FromServiceReference(extractConfigurationClient.GetAllDefinitions()
                            .FirstOrDefault(x => x.ExtractDefinitionId == id));
                    model.Heading = "Update Extract Billing \"" + def.ExtractName + "\"";
                    model.ExtractName = def.ExtractName;
                    model.SelectedExtractBillingName = def.ExtractFilename;
                    model.SelectedExtractBillingValue = def.ExtractDefinition;
                }
                else
                {
                    model.Mode = "new";
                    model.Heading = "Add Extract Billing Definition";
                }

                model.Labels = GetLabels("UploadEB");

                BillingExtractsManager mgr = new BillingExtractsManager();
                var resp = mgr.GetBillingFormats();
                foreach (BillingFormat format in resp.formatList)
                {
                    model.ExtractBillings.Add(new KeyValuePair<string, string>(format.DisplayName, format.BaseParameters));
                }

                return View(model);
            }
            catch(Exception ex)
            {
                Logger.Log(Wfs.Logging.LogLevel.Error, ex.Message);
                if (ex.InnerException != null)
                    Logger.Log(Wfs.Logging.LogLevel.Error, ex.InnerException.Message);
                return null;
            }
        }

        /// <summary>
        /// Used for accepting the Upload ExtractBilling form POST.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult UpdateExtractBilling([Bind(Include = "SelectedExtractBillingValue, ExtractName, Id, SelectedExtractBillingName")] UploadEBModel model)
        {
            // So we can grab our upload later one.
            var timestamp = DateTime.Now;

            var newitem = new ExtractDefinitionDTO()
            {
                ExtractDefinition = model.SelectedExtractBillingValue,
                ExtractName = model.ExtractName,
                ExtractDefinitionId = model.Id,
                ExtractFilename = model.SelectedExtractBillingName,
                ExtractDefinitionType = 2
            }.ToServiceReference();

            if (model.Id <= 0)
                // Calling 'Add' with edit the ID for us.
                extractConfigurationClient.AddDefinition(newitem);
            else
                extractConfigurationClient.UpdateDefinition(newitem);

            return Json(ExtractDefinitionDTO.FromServiceReference(newitem));
        }

        /// <summary>
        /// For posting the uploaded file.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult UploadFile([Bind (Include="Mode, ExtractName, File, ExtractDefinitionFilename, Id")] UploadModel model)
        {
            // So we can grab our file later one.
            var timestamp = DateTime.Now;
            var labels = GetLabels( "UploadFile");
            // Just some validation first.
            if (model.Mode != "update" && string.IsNullOrWhiteSpace(model.ExtractName))
                return Json(new { Error = labels["EmptyName"] }, "text/html");

            if (Request.Files.Count == 0
                || model.File == null
                || model.File.ContentLength == 0)
            {
                return Json(new { Error = labels["MissingFile"] }, "text/html");
            }

            if( model.File.FileName != null )
                model.ExtractDefinitionFilename = Path.GetFileName(model.File.FileName);

            // Grab all the xml text and stuff everything into the database.
            var xml = new StreamReader(model.File.InputStream)
                .ReadToEnd();
            // Validate the XML
            try
            {
                var doc = new XmlDocument();
                doc.XmlResolver = null;
                doc.LoadXml(xml);
            }
            catch(Exception)
            {
                // Not valid XML.
                return Json(new { Error = labels["InvalidXml"] }, "text/html");
            }

            var newitem = new ExtractDefinitionDTO()
            {
                ExtractDefinition = xml,
                ExtractName = model.ExtractName,
                ExtractDefinitionId = model.Id,
                ExtractFilename = model.ExtractDefinitionFilename,
                ExtractDefinitionType = 1
            }.ToServiceReference();

            if (model.Id <= 0)
                // Calling 'Add' with edit the ID for us.
                extractConfigurationClient.AddDefinition(newitem);
            else
                extractConfigurationClient.UpdateDefinition(newitem);

            return Json(ExtractDefinitionDTO.FromServiceReference(newitem), "text/html");
        }

        /// <summary>
        /// Downloads the XML file.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.View)]
        public FileResult Download(int id)
        {
            // Server-side validation
            if (id <= 0)
                return null;

            var def = extractConfigurationClient.GetDefinitionXML(id);

            if (def == null || string.IsNullOrEmpty(def.ExtractDefinition))
                return null;

            string filename = def.ExtractFilename;
            if (!filename.EndsWith(".xml", StringComparison.OrdinalIgnoreCase))
                filename += ".xml";

            return File(System.Text.Encoding.UTF8.GetBytes(def.ExtractDefinition), "application/xml", filename );
        }

        /// <summary>
        /// Adds a schedule to the Database.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.View)]
        public ActionResult AddSchedule([Bind(Include = "ExtractDefinitionId, ExtractRunArguments, Description, ExtractScheduleId, LastTimeRun, ModificationDate, ScheduleTime, IsActive, DayInMonth, ScheduleType, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, DayInMonth")]
                                         UpdateScheduleModel model)
        {
            // Load Definition Type
            var def = extractConfigurationClient
                .GetAllDefinitions()
                .First(x => x.ExtractDefinitionId == model.ExtractDefinitionId);

            // Just a little default arg.
            if (string.IsNullOrWhiteSpace(model.ExtractRunArguments) && def.ExtractDefinitionType == ExtractScheduleCommon.BusinessObjects.ExtractDefinitionType.Wizard)
                model.ExtractRunArguments = DEFAULT_RUN_ARGUMENTS;

            var serviceobj = model.ToServiceReference();
            extractConfigurationClient.AddSchedule(serviceobj);
            return Json(ExtractScheduleDTO.FromServiceReference(serviceobj));
        }

        /// <summary>
        /// Shows a schedule list for a particular definition.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.View)]
        public ActionResult ScheduleList(int id)
        {
            // id is the 'definition id'.
            var defs = extractConfigurationClient
                .GetAllDefinitions()
                .Select(x => ExtractDefinitionDTO.FromServiceReference(x));

            var selecteddef = defs
                .FirstOrDefault(x => x.ExtractDefinitionId == id);

            var model = new
            {
                AllDefinitions = defs,
                SelectedDefinition = selecteddef
            };
            return View(model);
        }


        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.View)]
        [HttpPost]
        public ActionResult GetSchedules(int id)
        {
            // id is the 'definition id'.
            var schedules = extractConfigurationClient
                .GetAllSchedules()
                .Where(x => x.ExtractDefinitionId == id)
                .Select(x => ExtractScheduleDTO.FromServiceReference(x));

            return Json(new
            {
                Schedules = schedules,
                CanManage = R360Permissions.Current.Allowed(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)
            });
        }

        /// <summary>
        /// Deletes a schedule from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="defid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult DeleteSchedule(int id, int defid)
        {
            var sched = new ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO()
            {
                ExtractScheduleId = id,
                ExtractDefinitionId = defid
            };
            extractConfigurationClient.RemoveSchedule(sched);

            return RedirectToAction("ScheduleList", new { id = defid });
        }

        /// <summary>
        /// Updates a schedule.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult UpdateSchedule([Bind(Include = "ExtractDefinitionId, ExtractRunArguments, Description, ExtractScheduleId, LastTimeRun, ModificationDate, ScheduleTime, IsActive, DayInMonth, ScheduleType, Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, DayInMonth")]
                                           UpdateScheduleModel model)
        {
            // Load Definition Type
            var def = extractConfigurationClient
                .GetAllDefinitions()
                .Where(x => x.ExtractDefinitionId == model.ExtractDefinitionId)
                .First();

            // Just a little default arg.
            if (string.IsNullOrWhiteSpace(model.ExtractRunArguments) && def.ExtractDefinitionType == ExtractScheduleCommon.BusinessObjects.ExtractDefinitionType.Wizard)
                model.ExtractRunArguments = DEFAULT_RUN_ARGUMENTS;

            var sched = model.ToServiceReference();
            extractConfigurationClient.UpdateSchedule(sched);
            return RedirectToAction("ScheduleList", new { id = model.ExtractDefinitionId });
        }

        /// <summary>
        /// Shows the 'update' or 'new' schedule form.
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public ActionResult Scheduler(string mode, int? id, int? defid)
        {
            SchedulerModel schedulerModel = new SchedulerModel();
            if (mode != null)
                schedulerModel.Mode = mode.Trim().ToLower();

            // Update vs New.
            schedulerModel.Schedule = id.HasValue
                ? ExtractScheduleDTO
                    .FromServiceReference(extractConfigurationClient
                        .GetAllSchedules()
                        .FirstOrDefault(x => x.ExtractScheduleId == id))
                : new ExtractScheduleDTO();

            schedulerModel.AllDefinitions = extractConfigurationClient
                .GetAllDefinitions()
                .Select(x => ExtractDefinitionDTO.FromServiceReference(x));

            schedulerModel.Labels = GetLabels("Scheduler");

            if (defid != null)
                schedulerModel.Schedule.ExtractDefinitionId = defid.Value;
            return View(schedulerModel);
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_ExtractConfigurationManager, R360Permissions.ActionType.Manage)]
        public JsonResult GenerateFileNow(int id)
        {
            var webResponse = new JSONResponse();
            webResponse.Data = null;
            webResponse.Errors = null;
            webResponse.HasErrors = false;

            // id is the 'schedule id'.
            var schedule = extractConfigurationClient
                .GetAllSchedules()
                .Where(x => x.ExtractScheduleId == id)
                .Select(x => ExtractScheduleDTO.FromServiceReference(x)).Single();

            var definition = ExtractDefinitionDTO.FromServiceReference(extractConfigurationClient
                .GetDefinitionXML((int)schedule.ExtractDefinitionId));

            BillingExtractsManager mgr = new BillingExtractsManager();
            mgr.GenerateFile(definition.ExtractDefinition, schedule.ExtractRunArguments);

            // TODO: Should actually check for errors and report them... But the UI doesn't wait for errors right now so that would have to be fixed, too...

            return this.Json(webResponse);
        }

        private int SetFileSize(int contentLength)
        {
            int kbSize = contentLength / 1024;

            if (kbSize < 1)
                return 1;
            else
                return kbSize;
        }

        #region Labels

        /// <summary>
        /// Function to pull the label for a page.
        /// TODO: To be replaced in a future TB.
        /// </summary>
        /// <param name="sPageName"></param>
        /// <returns></returns>
        private static Dictionary<string, string> GetLabels(string sPageName)
        {
            Dictionary<string, string> Labels = new Dictionary<string, string>();
            switch (sPageName)
            {
                case "Delete":
                    {
                        Labels["DeleteError"] = "There was an error deleting the Extract Definition. <br />Please verify all schedules have been removed from this Extract Definition.";
                        break;
                    }
                case "Index":
                    {
                        Labels["Title"] = "Extract Configuration Manager";
                        Labels["ExtractDefinitionId"] = "Id";
                        Labels["ExtractDefinitionName"] = "Extract Definition Name";
                        Labels["Size"] = "Size (kb)";
                        Labels["Download"] = "Download";
                        Labels["Update"] = "Update";
                        Labels["Delete"] = "Delete";
                        Labels["SaveChanges"] = "Save Changes";
                        Labels["UploadNewEWDefinition"] = "Add Extract Design Definition";
                        Labels["CreateNewEBDefinition"] = "Add Extract Billing Definition";
                        Labels["Schedule"] = "Schedule";
                        Labels["ExtractDefinitionType"] = "Type";
                        Labels["ExtractOriginalName"] = "Original Name";
                        Labels["ExtractFilename"] = "Extract Filename";
                        Labels["NoData"] = "No data available.";
                        Labels["ConfirmDeleteDefinition"] = "Are you sure you want to delete this extract definition?";
                        break;
                    }
                case "ScheduleList":
                    {
                        Labels["Title"] = "Extract Definition Scheduler";
                        Labels["Extract"] = "Extract";
                        Labels["Time"] = "Time";
                        Labels["Days"] = "Days";
                        Labels["Id"] = "Id";
                        Labels["Description"] = "Description";
                        Labels["TimeValue"] = "Time (00:00-23:59)";
                        Labels["Sun"] = "S";
                        Labels["Mon"] = "M";
                        Labels["Tue"] = "T";
                        Labels["Wed"] = "W";
                        Labels["Thu"] = "T";
                        Labels["Fri"] = "F";
                        Labels["Sat"] = "S";
                        Labels["Active"] = "Active";
                        Labels["Action"] = "Action";
                        Labels["NewSchedule"] = "Add Schedule";
                        Labels["Navigate"] = "Navigate";
                        Labels["Name"] = "Definition Name";
                        Labels["Cancel"] = "Cancel";
                        Labels["ScheduleType"] = "Schedule Type";
                        Labels["DayInMonth"] = "Day In Month";
                        Labels["NoData"] = "No data available.";
                        Labels["Details"] = "Details";
                        Labels["Delete"] = "Delete";
                        Labels["Back"] = "Back";

                        break;
                    }
                case "Scheduler":
                    {                  
                        Labels["Title"] = "Extract Definition Scheduler";
                        Labels["Definition"] = "Definition";
                        Labels["Description"] = "Description";
                        Labels["Time"] = "Time (24hr)";
                        Labels["IsActive"] = "Is Active";
                        Labels["DaysofWeek"] = "Days of Week";
                        Labels["ScheduleType"] = "Schedule Type";
                        Labels["Weekly"] = "Weekly";
                        Labels["Monthly"] = "Monthly";
                        Labels["WeeklySchedule"] = "Weekly Schedule";
                        Labels["MonthlySchedule"] = "Monthly Schedule";
                        Labels["SelectDayToRun"] = "Select a day to run:";
                        Labels["Sun"] = "S";
                        Labels["Mon"] = "M";
                        Labels["Tue"] = "T";
                        Labels["Wed"] = "W";
                        Labels["Thu"] = "T";
                        Labels["Fri"] = "F";
                        Labels["Sat"] = "S";
                        Labels["RunTimeArguments"] = "Run Time Arguments";
                        Labels["Save"] = "Save";
                        Labels["Undo"] = "Undo";
                        Labels["Delete"] = "Delete";
                        Labels["Cancel"] = "Cancel";
                        Labels["Update"] = "Update";
                        Labels["Back"] = "Back";
                        Labels["DefaultRunTimeArguments"] = "Default: /p:{today}";
                        Labels["ExampleRunTimeArgumentsBilling"] = "Example: StartDate=\"20140101\" EndDate=\"20140131\"";
                        Labels["NewSchedule"] = "Add New Schedule";
                        Labels["UpdateSchedule"] = "Update Schedule";
                        Labels["RequiredDesriptionError"] = "Required.";
                        Labels["RequiredTimeError"] = "Required. (Allows time in the format of 00:00).";
                        Labels["ArgumentFormatError"] = "Billing arguments must be in <Name>=\"<Value>\" format, separated by spaces.";
                        Labels["RunNow"] = "Create Extract";
                        Labels["OutputMode"] = "Output Mode";
                        break;
                    }
                case "Upload":
                    {
                        Labels["Name"] = "Name";
                        Labels["File"] = "File";
                        Labels["Upload"] = "Upload";
                        Labels["Cancel"] = "Cancel";
                        Labels["Browse"] = "Browse";
                        Labels["InvalidXml"] = "Invalid Xml Extract Definition Document";
                        Labels["SaveName"] = "Save Name";
                        Labels["RequiredNameError"] = "Required. (Allows alphanumeric with symbols &,.:'()_- and spaces.)";
                        Labels["RequiredFileError"] = "Required. Please select a valid XML definition file to upload.";

                        break; 
                    }
                case "UploadFile":
                    {
                        Labels["EmptyName"] = "Please enter an Extract Name.";
                        Labels["MissingFile"] = "Please select an Extract Definition file to upload.";
                        Labels["InvalidXml"] = "The Extract Definition is not a valid Xml document.";
                        break;
                    }
                case "UploadEB":
                    {
                        Labels["Name"] = "Name";
                        Labels["Upload"] = "OK";
                        Labels["Cancel"] = "Cancel";
                        Labels["SelectExtractBilling"] = "Extract Billing Format";
                        Labels["SaveName"] = "Save Name";
                        Labels["RequiredNameError"] = "Required. (Allows alphanumeric with symbols &,.:'()_- and spaces.)";
                        Labels["RequiredEBError"] = "Required. Please select an Extract Billing Format.";

                        break;
                    }
            }
            return Labels;
        }

        #endregion

    }
}