﻿using System;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske, Dave Wanta
* Date: 04/01/2014
*
* Purpose: 
*
* Modification History
*   WI 132768 BLR 04/02/2014
*     - Initial Version
*   WI 138899 DJW 05/08/2014
*   WI 138887 
*   WI 138889 
*   WI 138886 
*   WI 140642 
*     - Added support for ExtractBillings
*     - Added support for storing the Extract filename
* WI 159239 BLR 08/18/2014
*   - Added Definition Size.  
******************************************************************************/
namespace ExtractScheduleViews.Models.DAL
{
    public class ExtractDefinitionDTO
    {
        public string ExtractDefinition { get; set; }
        public string ExtractName { get; set; }
        public Int64 ExtractDefinitionId { get; set; }
        public string ExtractFilename { get; set; }
        public int ExtractDefinitionType { get; set; }
        public string ExtractDefinitionTypeName { get; set; }
        public string Mode { get; set; }
        public int ExtractDefinitionSize { get; set; }

        /// <summary>
        /// Just a quick mapping between the DTO objects.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static ExtractDefinitionDTO FromServiceReference(ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO dto)
        {
            return new ExtractDefinitionDTO()
            {
                ExtractDefinition = dto.ExtractDefinition,
                ExtractDefinitionId = dto.ExtractDefinitionId,
                ExtractName = dto.ExtractName,
                ExtractFilename = dto.ExtractFilename,
                ExtractDefinitionType = (int)dto.ExtractDefinitionType,
                ExtractDefinitionTypeName = (dto.ExtractDefinitionType == ExtractScheduleCommon.BusinessObjects.ExtractDefinitionType.Billing ? "Extract Billing" : "Extract Design Manager"),
                ExtractDefinitionSize = dto.ExtractDefinitionSize
            };
        }

        public ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO ToServiceReference()
        {
            return new ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO()
            {
                ExtractDefinition = this.ExtractDefinition,
                ExtractDefinitionId = this.ExtractDefinitionId,
                ExtractName = this.ExtractName,
                ExtractDefinitionType = (ExtractScheduleCommon.BusinessObjects.ExtractDefinitionType)this.ExtractDefinitionType,
                ExtractFilename = this.ExtractFilename,
                ExtractDefinitionSize = this.ExtractDefinitionSize
            };
        }

       
    }

}
