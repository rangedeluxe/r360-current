﻿using System;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske, Dave Wanta
* Date: 04/01/2014
*
* Purpose: 
*
* Modification History
*   WI 132768 BLR 04/02/2014
*     - Initial Version
*   WI 138899 DJW 05/08/2014
*     - Added support for a Monthly schedule type
* WI 141430 DJW 5/13/2014
*   - Moved the enum outside the object.
******************************************************************************/
namespace ExtractScheduleViews.Models.DAL
{
    public class ExtractScheduleDTO
    {
        // these map to the DB table
        public Int64 ExtractScheduleId { get; set; }
        public Int64 ExtractDefinitionId { get; set; }
        public DateTime ModificationDate { get; set; }
        public bool IsActive { get; set; }
        private string _daysOfWeek;
        public string DaysOfWeek 
        { 
            get
            {
                return _daysOfWeek;
            }
            set
            {
                _daysOfWeek = value;
                SetDaysOfWeek();
            }
        }
        public TimeSpan ScheduleTime { get; set; }
        public string Description { get; set; }
        public string ExtractRunArguments { get; set; }
        public string ScheduleTimeString 
        {
            get { return string.Format("{0:00}:{1:00}", ScheduleTime.Hours, ScheduleTime.Minutes); }
        }

        // for easy view bindings (From Days of Week)
        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public int ScheduleType { get; set; }
        public int DayInMonth { get; set; }
        // processing value
        public DateTime LastTimeRun;

        private void SetDaysOfWeek()
        {
            try
            {
                Sunday = DaysOfWeek[0] == '1';
                Monday = DaysOfWeek[1] == '1';
                Tuesday = DaysOfWeek[2] == '1';
                Wednesday = DaysOfWeek[3] == '1';
                Thursday = DaysOfWeek[4] == '1';
                Friday = DaysOfWeek[5] == '1';
                Saturday = DaysOfWeek[6] == '1';
            }
            catch(Exception e)
            {
                // TODO: Log invalid DaysOfWeek value.
            }
        }

        public ExtractScheduleDTO()
        {
            this.LastTimeRun = DateTime.Parse("1/1/1900");
            this.ScheduleType = 1;
            this.DayInMonth = 1;
        }

        /// <summary>
        /// Just a quick mapping between the DTO objects.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static ExtractScheduleDTO FromServiceReference(ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO dto)
        {
            return new ExtractScheduleDTO()
            {
                ExtractScheduleId = dto.ExtractScheduleId,
                ExtractDefinitionId = dto.ExtractDefinitionId,
                ModificationDate = dto.ModificationDate,
                DaysOfWeek = dto.DaysOfWeek,
                ScheduleTime = dto.ScheduleTime,
                Description = dto.Description,
                ExtractRunArguments = dto.ExtractRunArguments,
                LastTimeRun = dto.LastTimeRun,
                IsActive = dto.IsActive,
                DayInMonth = dto.DayInMonth,
                ScheduleType = (int)dto.ScheduleType
            };
        }

        public ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO ToServiceReference()
        {
            return new ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO()
            {
                ExtractScheduleId = this.ExtractScheduleId,
                ExtractDefinitionId = this.ExtractDefinitionId,
                ModificationDate = this.ModificationDate,
                DaysOfWeek = this.DaysOfWeek,
                ScheduleTime = this.ScheduleTime,
                Description = this.Description,
                ExtractRunArguments = this.ExtractRunArguments,
                LastTimeRun = this.LastTimeRun,
                IsActive = this.IsActive,
                DayInMonth = this.DayInMonth,
                ScheduleType = (ExtractScheduleCommon.BusinessObjects.ExtractScheduleType)this.ScheduleType
            };
        }

    }
}
