﻿using ExtractScheduleViews.Models.DAL;
using System.Collections.Generic;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske, Dave Wanta
* Date: 04/01/2014
*
* Purpose: 
*
* Modification History
*   WI 132768 BLR 04/02/2014
*     - Initial Version
******************************************************************************/

namespace ExtractScheduleViews.Models
{
    public class ScheduleListModel
    {
        public IEnumerable<ExtractScheduleDTO> Schedules { get; set; }
        public IEnumerable<ExtractDefinitionDTO> AllDefinitions { get; set; }
        public ExtractDefinitionDTO SelectedDefinition { get; set; }
        public Dictionary<string, string> Labels { get; set; }
    }
}