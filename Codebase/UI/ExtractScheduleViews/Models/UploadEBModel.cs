﻿using System.Collections.Generic;
using System.Web;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Wanta
* Date: 05/08/2014
*
* Purpose: 
*
* Modification History
*   WI 138899 DJW 05/08/2014
*   WI 138887 
*   WI 138889 
*   WI 138886 
*   WI 140642 
*     - Initial Version
******************************************************************************/

namespace ExtractScheduleViews.Models
{
    public class UploadEBModel
    {

        public string Heading { get; set; }
        public int Id { get; set; }
        public string Mode { get; set; }
        public string ExtractName { get; set; }
        public string SecurityToken { get; set; }
        public bool UploadComplete { get; set; }
        public string FileUploadError { get; set; }
        public string SelectedExtractBillingName { get; set; }
        public string SelectedExtractBillingValue { get; set; }
        public List<KeyValuePair<string, string>> ExtractBillings { get; set; }
        public Dictionary<string, string> Labels { get; set; }


        public UploadEBModel()
        {
            this.Heading = "Add Extract Billing";
            this.UploadComplete = false;
            this.Mode = "new";
            this.SelectedExtractBillingName = string.Empty;
            this.ExtractBillings = new List<KeyValuePair<string, string>>();
        }
        
    }
}