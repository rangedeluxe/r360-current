﻿using System;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske, Dave Wanta
* Date: 04/01/2014
*
* Purpose: 
*
* Modification History
*   WI 132768 BLR 04/02/2014
*     - Initial Version
*   WI 138899 DJW 05/08/2014
*     - Added support for a Monthly schedule type
* WI 141430 DJW 5/13/2014
*   - Moved the enum outside the object.
******************************************************************************/
namespace ExtractScheduleViews.Models
{
    public class UpdateScheduleModel
    {
        public Int64 ExtractScheduleId { get; set; }
        public Int64 ExtractDefinitionId { get; set; }
        public DateTime ModificationDate { get; set; }
        public string Description { get; set; }
        public string ExtractRunArguments { get; set; }
        public ShortTimeSpanType ScheduleTime { get; set; }
        public bool IsActive { get; set; }

        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }
        public int ScheduleType { get; set; }
        public int DayInMonth { get; set; }

        public DateTime LastTimeRun;

        public ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO ToServiceReference()
        {
            return new ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO()
            {
                DaysOfWeek = string.Format("{0}{1}{2}{3}{4}{5}{6}", 
                    Sunday ? "1": "0",
                    Monday ? "1": "0",
                    Tuesday ? "1": "0",
                    Wednesday ? "1": "0",
                    Thursday ? "1": "0",
                    Friday ? "1": "0",
                    Saturday ? "1": "0"
                ),
                Description = Description,
                ExtractDefinitionId = ExtractDefinitionId,
                ExtractRunArguments = ExtractRunArguments,
                ExtractScheduleId = ExtractScheduleId,
                LastTimeRun = LastTimeRun,
                ModificationDate = ModificationDate, 
                ScheduleTime = new TimeSpan(ScheduleTime.Hours, ScheduleTime.Minutes, 0),
                IsActive = IsActive,
                DayInMonth = DayInMonth,
                ScheduleType = (ExtractScheduleCommon.BusinessObjects.ExtractScheduleType)ScheduleType
            };
        }
    }
}
