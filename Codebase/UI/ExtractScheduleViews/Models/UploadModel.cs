﻿using System.Collections.Generic;
using System.Web;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske, Dave Wanta
* Date: 04/01/2014
*
* Purpose: 
*
* Modification History
*   WI 132768 BLR 04/02/2014
*     - Initial Version
*   WI 140642 DJW 05/08/2014
*     - Added support for storing the original filename
******************************************************************************/

namespace ExtractScheduleViews.Models
{
    public class UploadModel
    {

        public string Heading { get; set; }
        public int Id { get; set; }
        public string Mode { get; set; }
        public string ExtractName { get; set; }
        public string SecurityToken { get; set; }
        public bool UploadComplete { get; set; }
        public string FileUploadError { get; set; }
        public string ExtractDefinitionFilename { get; set; }
        public HttpPostedFileBase File { get; set; }
        public Dictionary<string, string> Labels { get; set; }


        public UploadModel()
        {
            this.Heading = "Add Extract Configuration File";
            this.UploadComplete = false;
            this.Mode = "new";
            this.ExtractDefinitionFilename = string.Empty;
        }
        
    }
}