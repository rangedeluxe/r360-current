﻿using HubViews.App_Start;
using HubViews.DependencyResolution;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Wfs.Logging;
using Wfs.Logging.NLog;
using Wfs.Raam.Core;
using Wfs.Raam.TokenCache;

namespace HubViews
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            WSPassiveSessionConfiguration.Start();
        }

        public override void Init()
        {
            WSPassiveSessionConfiguration.Init();
            base.Init();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var logger = new WfsLog();
            // Dependency injection logging.
            var scan = StructuremapMvc.StructureMapDependencyScope.Container.WhatDidIScan();
            var have = StructuremapMvc.StructureMapDependencyScope.Container.WhatDoIHave();
            logger.Log(WfsLogLevel.Fatal, "Dependency Injection SCAN:");
            logger.Log(WfsLogLevel.Fatal, scan);
            logger.Log(WfsLogLevel.Fatal, "Dependency Injection HAVE:");
            logger.Log(WfsLogLevel.Fatal, have);

            var ctx = HttpContext.Current;
            if (ctx == null)
            {
                logger.Log(WfsLogLevel.Fatal, "Application Failure, HTTPContext is Null.");
                return;
            }

            var exception = ctx.Server.GetLastError();
            if (exception != null)
            {
                logger.Log(WfsLogLevel.Fatal, "Error: Unhandled Application Exception:\r\n  Offending URL: {0}\r\n  Exception: {1}",
                    ctx.Request.Url.PathAndQuery, exception.ToString());
            }

            if (ctx.AllErrors != null && ctx.AllErrors.Length > 0)
            {
                //logger.Log(WfsLogLevel.Fatal,
                //    "Error: Unexpected exception during page lifecycle - displaying last Error only\r\n  Offending URL: {0}\r\n  Exception: {1}",
                //    ctx.Request.Url.PathAndQuery, ctx.AllErrors[ctx.AllErrors.Length - 1].ToString());
                foreach (var errorInAllError in ctx.AllErrors)
                {
                    logger.Log(WfsLogLevel.Fatal,
                        "Error:  {0}", errorInAllError.ToString());
                }
            }
            else
            {
                logger.Log(WfsLogLevel.Fatal,
                    "Error: Error event triggered with no Error context - please look for previous exceptions...?\r\n  Offending URL: {0}",
                    ctx.Request.Url.PathAndQuery);
            }
        }

        protected void Application_PreSendRequestHeaders()
        {
            var allowFrom = string.Empty;
            var principal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (principal != null)
            {
                var identity = principal.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    var allowFromClaim = identity.Claims.FirstOrDefault(c => c.Type == WfsClaimTypes.AllowFromUrlInFrame);
                    allowFrom = allowFromClaim == null ? "" : allowFromClaim.Value;
                }
            }

            if (!string.IsNullOrEmpty(allowFrom))
            {
                Response.Headers["X-Frame-Options"] = "ALLOW-FROM " + allowFrom;
                Response.Headers["Content-Security-Policy"] = "frame-ancestors " + allowFrom;
            }
            else
            {
                Response.Headers["X-Frame-Options"] = "SAMEORIGIN";
                Response.Headers["Content-Security-Policy"] = "frame-ancestors 'self'";
            }
        }
    }
}