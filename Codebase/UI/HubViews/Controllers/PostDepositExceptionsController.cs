﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using System.Web.Mvc;
using AutoMapper;
using HubViews.Models;
using HubViews.Models.PostDepositDetail;
using Wfs.Logging;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;

namespace HubViews.Controllers
{
    public class PostDepositExceptionsController : BaseController
    {
        private const string PostDepositEventType = "Post-Deposit Exceptions";

        private readonly IR360Services _r360Services;
        private readonly IRaamClient _raamClient;
        private readonly IContextContainer _contextContainer;
        private readonly IPostDepositExceptionServices _postDepositExceptionServices;
        private readonly IMapper _mapper;

        public PostDepositExceptionsController(
            IR360Services r360Services,
            IRaamClient raamClient,
            IContextContainer contextContainer,
            IPostDepositExceptionServices postDepositExceptionServices)
        {
            _r360Services = r360Services;
            _raamClient = raamClient;
            _contextContainer = contextContainer;
            _postDepositExceptionServices = postDepositExceptionServices;
            _mapper = CreateMapperConfiguration().CreateMapper();
        }

        public static MapperConfiguration CreateMapperConfiguration()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PostDepositTransactionRequestModel, PostDepositPendingTransactionsRequestDto>();
                cfg.CreateMap<PostDepositTransactionResponseDto, PostDepositTransactionResponseModel>()
                    .ForMember(x => x.DepositDate, x => x.MapFrom(source => source.DepositDate.ToShortDateString()))
                    .ForMember(x => x.UserCanEdit, x => x.Ignore());
                cfg.CreateMap<PostDepositTransactionDetailResponseDto, TransactionWebModel>()
                    .ForMember(x => x.DepositDate, x => x.MapFrom(source => source.DepositDate.ToShortDateString()))
                    .ForMember(x => x.UserCanEdit, x => x.Ignore());
                cfg.CreateMap<PostDepositStubDetailDto, StubWebModel>();
                cfg.CreateMap<DataEntryValueDto, StubDataEntryWebModel>()
                    .ForMember(x => x.BusinessRuleExceptions, x => x.Ignore());
                cfg.CreateMap<PostDepositTransactionDetailRequestModel, PostDepositTransactionRequestDto>();
            });
        }

        [RequiresPermission(R360Permissions.Perm_PostDepositExceptions, R360Permissions.ActionType.View)]
        public ActionResult Index()
        {
            IsAuditEventSuppressed = true;
            _r360Services.WriteAuditEvent("Viewed Page", "Page View", "PostDepositExceptions/Index",
                "Post-Deposit Exceptions Summary page was viewed by " +
                _raamClient.GetNameFromSid(_contextContainer.Current.GetSID()));
            return View();
        }

        [RequiresPermission(R360Permissions.Perm_PostDepositExceptions, R360Permissions.ActionType.View)]
        public JsonResult GetTransactions(
            [ModelBinder(typeof(AllowEmptyStringsModelBinder))] PostDepositTransactionRequestModel request)
        {
            try
            {
                IsAuditEventSuppressed = true;
                _r360Services.WriteAuditEvent(PostDepositEventType, PostDepositEventType, "PostDepositExceptions/Index",
                    $"{_raamClient.GetNameFromSid(_contextContainer.Current.GetSID())} " +
                    "viewed pending exception transactions on the Post-Deposit Exceptions Summary page.");

                var dto = _mapper.Map<PostDepositPendingTransactionsRequestDto>(request);
                var resp = _postDepositExceptionServices.GetPendingTransactions(dto);
                var model = resp.Data.Transactions.Select(x => _mapper.Map<PostDepositTransactionResponseModel>(x));
                return Json(
                    new
                    {
                        success = resp.Errors.Count == 0,
                        data = model,
                        error = resp.Errors.FirstOrDefault(),
                        draw = request.Draw,
                        recordsTotal = resp.Data.TotalRecords,
                        recordsFiltered = resp.Data.TotalFilteredRecords
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while retrieving Pending Transactions." },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_PostDepositExceptions, R360Permissions.ActionType.View)]
        public JsonResult SaveTransaction(
            [ModelBinder(typeof(AllowEmptyStringsModelBinder))] PostDepositTransactionSaveDto request)
        {
            try
            {
                var response = _postDepositExceptionServices.SaveTransaction(request);
                return Json(new
                {
                    success = response.Status == StatusCode.SUCCESS,
                    data = response.Data,
                    error = response.Errors != null && response.Errors.Any()
                        ? string.Join("\r\n", response.Errors)
                        : null,
                });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new
                {
                    success = false,
                    error = "An error occurred while saving the transaction.",
                });
            }
        }




        [RequiresPermission(R360Permissions.Perm_PostDepositExceptions, R360Permissions.ActionType.View)]
        [Authorize]
        [HttpPost]
        public JsonResult CompleteTransaction(PostDepositTransactionRequestDto request)
        {
            try
            {
                var response = _postDepositExceptionServices.CompleteTransaction(request);
                return Json(new
                {
                    success = response.Status == StatusCode.SUCCESS,
                    data = response.Data,
                    error = response.Errors != null && response.Errors.Any()
                        ? string.Join("\r\n", response.Errors)
                        : null,
                });
            }
            catch (Exception exe)
            {
                Logger.Log(WfsLogLevel.Error, exe.Message);
                return Json(new
                {
                    success = false,
                    error = "An error occurred while completing the transaction.",
                });
            }
        }

        [RequiresPermission(R360Permissions.Perm_PostDepositExceptions, R360Permissions.ActionType.View)]
        public ActionResult Detail(
            [ModelBinder(typeof(AllowEmptyStringsModelBinder))] PostDepositTransactionDetailRequestModel request)
        {
            IsAuditEventSuppressed = true;
            return View(request);
        }

        [RequiresPermission(R360Permissions.Perm_PostDepositExceptions, R360Permissions.ActionType.View)]
        public JsonResult UnlockTransaction(
            [ModelBinder(typeof(AllowEmptyStringsModelBinder))] LockTransactionRequestModel request)
        {
            try
            {
                var req = new LockTransactionRequestDto()
                {
                    BatchId = request.BatchId,
                    DepositDate = DateTime.Parse(request.DepositDate),
                    TransactionId = request.TransactionId
                };
                var resp = _postDepositExceptionServices.UnlockTransaction(req);
                return Json(new { success = resp.Status == StatusCode.SUCCESS });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while retrieving Transactions Detail." },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [RequiresPermission(R360Permissions.Perm_PostDepositExceptions, R360Permissions.ActionType.View)]
        public JsonResult GetTransactionDetail(
            [ModelBinder(typeof(AllowEmptyStringsModelBinder))] PostDepositTransactionDetailRequestModel request)
        {
            try
            {
                // Unlock the previous transaction if we need to.
                if (request.PreviousBatchId.HasValue
                    && request.PreviousTransactionId.HasValue
                    && request.PreviousDepositDate != null)
                {
                    DateTime prevdate;
                    DateTime.TryParse(request.PreviousDepositDate, out prevdate);
                    _postDepositExceptionServices.UnlockTransactionWithoutOverride(new LockTransactionRequestDto()
                    {
                        BatchId = request.PreviousBatchId.Value,
                        DepositDate = prevdate,
                        TransactionId = request.PreviousTransactionId.Value
                    });
                }

                DateTime date;
                DateTime.TryParse(request.DepositDate, out date);

                // Attempt to lock it first, but we don't really care about the response at this point.
                _postDepositExceptionServices.LockTransaction(new LockTransactionRequestDto()
                {
                    BatchId = request.BatchId,
                    DepositDate = date,
                    TransactionId = request.TransactionId
                });

                // Audit that the user is viewing the transaction detail
                _postDepositExceptionServices.AuditTransactionDetailView(new PostDepositExceptionTransactionDto
                {
                    BatchId = request.BatchId,
                    DepositDate = date,
                    TransactionId = request.TransactionId
                });
                // Grab the transaction & lock status.
                var dto = _mapper.Map<PostDepositTransactionRequestDto>(request);
                var response = _postDepositExceptionServices.GetTransaction(dto);
                AddNewRelatedItemIfRequired(response.Data);
                var validationDetails = _postDepositExceptionServices.GetTransactionValidationResults(response.Data);

                // Get the payers for each payment in the list

                if (response.Data.Payments != null && response.Data.Payments.Any())
                {
                    
                    foreach (var payment in response.Data.Payments)
                    {
                        var payerLookup = new PayerLookupRequestDto
                        {
                            Account = payment.AccountNumber,
                            RoutingNumber = payment.RT,
                            BankId = response.Data.BankId,
                            WorkgroupId = response.Data.WorkgroupId
                        };

                        payment.PayerList = ToPayerResponseDto(payerLookup);

                    }

                }
                var model = _mapper.Map<TransactionWebModel>(response.Data);

                var exceptionList = validationDetails.Data.StubDataEntryExceptions;
                ApplyBusinessRuleExceptionsToStubs(exceptionList, model.Stubs);

                model.UserCanEdit = model.Lock != null && model.Lock.UserSID == _contextContainer.Current.GetSID();

               
                var result = Json(new
                {
                    success = response.Errors.Count == 0,
                    data = model,
                    error = response.Errors
                }, JsonRequestBehavior.AllowGet);
                return result;
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while retrieving Transactions Detail." },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [RequiresPermission(R360Permissions.Perm_PostDepositExceptions, R360Permissions.ActionType.View)]
        public void AddNewRelatedItemIfRequired(PostDepositTransactionDetailResponseDto pDETransactionResponseDto)
        {
            if (pDETransactionResponseDto.Stubs != null && pDETransactionResponseDto.Stubs.Any())
                //We have at least one stub, so return
                return;

            //We have an exception and there is no related-item(stub) lets add one so that the user can address the exception.
            if (pDETransactionResponseDto.DataEntrySetupList != null &&
                pDETransactionResponseDto.DataEntrySetupList.Any())
            {
                CreateNewRelatedItem(pDETransactionResponseDto);
            }
        }

        private void CreateNewRelatedItem(PostDepositTransactionDetailResponseDto pDETransactionResponseDto)
        {
            var stub = CreateStubDetailDto();
            AddDataEntryFields(pDETransactionResponseDto.DataEntrySetupList, stub);
            var stubs = new List<PostDepositStubDetailDto> { stub };
            pDETransactionResponseDto.Stubs = stubs;
        }

        private IEnumerable<PostDepositRequestPayerResponseDto> ToPayerResponseDto(PayerLookupRequestDto payerLookup)
        {
            var response = new List<PostDepositRequestPayerResponseDto>();
            _postDepositExceptionServices.GetPayerList(payerLookup).Data.PayerList.ToList()
                .ForEach(payer =>
                {
                    response.Add(new PostDepositRequestPayerResponseDto
                    {
                        PayerName = payer.PayerName,
                        UserName = payer.CreatedBy
                    });
                });

            return response;
        }

        private static PostDepositStubDetailDto CreateStubDetailDto()
        {
            var stub = new PostDepositStubDetailDto
            {
                BatchSequence = -1,
                DataEntryFields = new List<DataEntryValueDto>(),
                StubSequence = -1,
            };
            return stub;
        }

        private void AddDataEntryFields(IEnumerable<DataEntrySetupDto> dataDataEntrySetupList,
            PostDepositStubDetailDto stub)
        {
            var dataEntryFields = new List<DataEntryValueDto>();
            foreach (var dataEntrySetupDto in dataDataEntrySetupList)
            {
                dataEntryFields.Add(new DataEntryValueDto
                {
                    FieldName = dataEntrySetupDto.FieldName,
                    IsEditable = true,
                    IsUIAddedField = true,
                    Title = dataEntrySetupDto.Title,
                    Type = dataEntrySetupDto.DataType,
                    Value = null
                });
            }
            stub.DataEntryFields = dataEntryFields;
        }

        private static void ApplyBusinessRuleExceptionsToStubs(
            IEnumerable<PostDepositStubDataEntryExceptionDto> exceptionList, IEnumerable<StubWebModel> stubs)
        {
            var exceptionsWithStubs =
                from exception in exceptionList
                join stub in stubs on
                new { BatchSequence = exception.BatchSequence, StubSequence = exception.StubSequence } equals
                new { BatchSequence = stub.BatchSequence, StubSequence = stub.StubSequence }
                select new BusinessRuleExceptionWithStub(stub, exception.FieldName, exception.Error);

            foreach (var stubGroup in exceptionsWithStubs.GroupBy(e => e.Stub))
            {
                ApplyBusinessRuleExceptionsToStub(stubGroup, stubGroup.Key);
            }
        }
        private static void ApplyBusinessRuleExceptionsToStub(
            IEnumerable<BusinessRuleExceptionWithStub> stubExceptions, StubWebModel stub)
        {
            var errorsByFieldModel =
                from stubError in stubExceptions
                join field in stub.DataEntryFields on stubError.FieldName equals field.FieldName
                group stubError.Message by field;
            foreach (var group in errorsByFieldModel)
            {
                var stubModel = group.Key;
                stubModel.BusinessRuleExceptions = group;
                // Make the field editable if it isn't already
                stubModel.IsEditable = true;
            }
        }

        private sealed class BusinessRuleExceptionWithStub
        {
            public BusinessRuleExceptionWithStub(StubWebModel stub, string fieldName, string message)
            {
                Stub = stub;
                FieldName = fieldName;
                Message = message;
            }

            public StubWebModel Stub { get; }
            public string FieldName { get; }
            public string Message { get; }
        }
    }
}
