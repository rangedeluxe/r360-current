﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CommonUtils.Web;
using HubViews.Models;
using WFS.RecHub.R360Shared;

namespace HubViews.Controllers
{
    public class DDASummaryController : BaseController
    {
		[Authorize]
        public ActionResult Index()
        {
            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_DDASummary, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return new EmptyResult();
            }

            var model = new
            {
            };

            return View(model);
        }

		[Authorize]
        [HttpPost]
        public JsonResult GetSummaryData(DateTime depositDate)
        {
            AuditFormFields("DepositDate");

            var hasPerm = R360Permissions.Current.Allowed(R360Permissions.Perm_DDASummary, R360Permissions.ActionType.View);
            if (!hasPerm)
            {
                return this.Json(null);
            }

            bool success = true;
            List<string> errors = null;
            List<DDASummaryModel> summaryData = null;
            var model = new DDASummaryModel();

            if (!model.GetDDASummary(depositDate, out summaryData, out errors))
            {
                success = false;
                if (errors == null || errors.Count == 0)
                    errors = new List<string>() { "Error occurred retrieving DDA Summary data." };
            }

            var response = new
            {
                Data = summaryData,
                HasErrors = !success,
                Errors = errors != null ? errors.Select(o => new WebErrors() { Message = o }).ToList() : null
            };

            return this.Json(response);
        }

    }
}
