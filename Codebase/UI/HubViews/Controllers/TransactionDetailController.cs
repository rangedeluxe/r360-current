﻿using HubViews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using AutoMapper;
using Wfs.Logging;
using System.Net;

namespace HubViews.Controllers
{
	public class TransactionDetailController : BaseController
	{
		private readonly IConfigurationProvider _mapperconfig;
		private readonly IMapper _mapper;

		public TransactionDetailController()
		{
			_mapperconfig = new MapperConfiguration(cfg =>
			{
				cfg.CreateMap<TransactionDetailRequestDto, TransactionDetailRequestModel>();
				cfg.CreateMap<TransactionDetailResponseDto, TransactionDetailResponseModel>().ForMember(
						x => x.DepositDateString,
						x => x.MapFrom(src => src.DepositDate.ToString("MM/dd/yyyy")));
			});
			_mapper = _mapperconfig.CreateMapper();
		}
		public ActionResult Index(TransactionDetailRequestModel model)
		{
			return View(model);
		}

		[HttpPost]
		public JsonResult GetTransactionDetail(TransactionDetailRequestModel model)
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSearch, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_BatchSummary, R360Permissions.ActionType.View))
			{
				try
				{
					DateTime depDate = DateTime.TryParse(model.DepositDateString, out depDate) ? depDate : DateTime.MinValue;
					var request = new TransactionDetailRequestDto
					{
						BankId = model.BankId,
						BatchID = model.BatchId,
						DepositDate = depDate,
						TransactionId = model.TransactionID,
						TransactionSequence = model.TransactionSequence,
						WorkgroupID = model.WorkgroupId
					};
					var result = _R360ServiceManager.GetTransactionDetail(request);
					var message = string.Empty;
					if (result.Status != StatusCode.SUCCESS)
					{
						message = "Error communicating with the server.";
						if (result.Errors != null && result.Errors.Any())
						{
							message = result.Errors.First();
							return Json(new { success = false, error = message }, JsonRequestBehavior.AllowGet);
						}
					}

					var mappedResults = _mapper.Map<TransactionDetailResponseModel>(result);

					var hasImageRPSPerm =
						R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View)
						&& mappedResults.ImportTypeShortName == "ImageRPS";
					mappedResults.HasImageRPSReportPermissions = hasImageRPSPerm;

					return Json(new
					{
						success = result.Status == StatusCode.SUCCESS,
						data = mappedResults,
						error = message
					}, JsonRequestBehavior.AllowGet);

				}
				catch (Exception e)
				{
					Logger.Log(WfsLogLevel.Error, e.Message);
					return Json(new { success = false, error = "There was a problem communicating with the server."}, JsonRequestBehavior.AllowGet);
				}
			}
			else
			{
				throw new System.Web.Http.HttpResponseException(HttpStatusCode.Unauthorized);
			}
		}

		public ActionResult Print(TransactionDetailRequestModel model)
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSearch, R360Permissions.ActionType.View) ||
				R360Permissions.Current.Allowed(R360Permissions.Perm_BatchSummary, R360Permissions.ActionType.View))
			{

				var data = new TransactionDetailResponseModel();
				try
				{
					DateTime depDate = DateTime.TryParse(model.DepositDateString, out depDate) ? depDate : DateTime.MinValue;
					var request = new TransactionDetailRequestDto
					{
						BankId = model.BankId,
						BatchID = model.BatchId,
						DepositDate = depDate,
						TransactionId = model.TransactionID,
						TransactionSequence = model.TransactionSequence,
						WorkgroupID = model.WorkgroupId
					};
					var result = _R360ServiceManager.GetTransactionDetail(request);
					data = _mapper.Map<TransactionDetailResponseModel>(result);
				}
				catch (Exception e)
				{
					Logger.Log(WfsLogLevel.Error, "Exception occurred in Print\r\n" + e);
					data.Error = "An Error occurred communicating with the server.";
				}
				return View(data);
			}
			else
			{
				return new HttpUnauthorizedResult();
			}
		}
	}
}