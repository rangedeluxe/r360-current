﻿using AutoMapper;
using HubViews.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;
using Wfs.Logging;
using Wfs.Logging.NLog;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;
using WFS.RecHub.R360WebShared.Services;

namespace HubViews.Controllers
{
    public class AdvancedSearchController : BaseController
    {
        private const string LastQuerySessionKey = "session_last_advanced_search_query";

        private readonly IR360Services _api;
        private readonly ISystemServices _systemserv;
        private readonly ITemporaryStorageProvider _storage;
        private readonly IMapper _mapper;
        private readonly IWfsLog _logger;
        private readonly string CSRFToken;

        public AdvancedSearchController()
        {
            var sessionTokenProvider = new SessionTokenProvider();
            var context = new R360ContextContainer();
            _api = new R360ServiceManager();
            _systemserv = new SystemServiceManager();
            _storage = new SessionTemporaryStorageProvider();
            _logger = new WfsLog();
            _mapper = CreateMapperConfiguration().CreateMapper();
            // Get the sessiontoken for CSRF attacks.
            CSRFToken = sessionTokenProvider.GenerateSessionToken(context.Current.GetSessionID().ToString());
        }

        public AdvancedSearchController(
            IR360Services api,
            ISystemServices systemserv,
            ITemporaryStorageProvider storage,
            IWfsLog logger,
            ISessionTokenProvider sessionTokenProvider,
            IContextContainer context)
        {
            _api = api;
            _systemserv = systemserv;
            _storage = storage;
            _logger = logger;
            _mapper = CreateMapperConfiguration().CreateMapper();
            // Get the sessiontoken for CSRF attacks.
            CSRFToken = sessionTokenProvider.GenerateSessionToken(context.Current.GetSessionID().ToString());
        }

        public static MapperConfiguration CreateMapperConfiguration()
        {
            return new MapperConfiguration(x =>
            {
                x.CreateMap<PaymentTypeDto, PaymentTypeModel>();
                x.CreateMap<PaymentSourceDto, PaymentSourceModel>();
            });
        }

        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        public ActionResult Index(AdvancedSearchRequestModel model)
        {
            model = model ?? new AdvancedSearchRequestModel();
            model.CSRFToken = CSRFToken;
            return View(model);
        }

        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        public ActionResult Results(AdvancedSearchRequestModel model)
        {
            model = model ?? new AdvancedSearchRequestModel();
            model.CSRFToken = CSRFToken;
            return View(model);
        }

        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        public ActionResult QueryDialog(AdvancedSearchEditQueryRequestModel model)
        {
            model = model ?? new AdvancedSearchEditQueryRequestModel();
            model.CSRFToken = CSRFToken;
            return View(model);
        }

        [ValidateSessionToken("CSRFToken")]
        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        [System.Web.Mvc.HttpPost]
        public JsonResult SaveQuery(AdvancedSearchEditQueryRequestModel model)
        {
            if (string.IsNullOrWhiteSpace(model.QueryName))
                return Json(new { Error = "Please enter a query name." });

            try
            {
                var dto = PopulateServiceRequestObject(model);
                var storedquery = new AdvancedSearchStoredQueryDto()
                {
                    Description = model.QueryDescription,
                    Name = model.QueryName,
                    Query = dto,
                    Id = model.QueryId ?? Guid.Empty,
                    IsDefault = model.QueryIsDefault
                };
                var result = _api.SaveStoredQuery(storedquery);
                if (result.Status != StatusCode.SUCCESS)
                {
                    var error = result.Errors.Any()
                        ? result.Errors.First()
                        : "Error occurred while saving query.";
                    return Json(new { Error = error });
                }
                return Json(new { Success = true, Id = result.Data.Id });
            }
            catch(Exception ex)
            {
                _logger.Log(WfsLogLevel.Error, ex.Message);
                return Json(new { Error = "Error occurred while saving query." });
            }
        }

        [ValidateSessionToken("CSRFToken")]
        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        [System.Web.Mvc.HttpPost]
        public JsonResult GetAdvancedSearch(AdvancedSearchRequestModel model)
        {
            // Validation and data gathering.
            if (string.IsNullOrWhiteSpace(model.Workgroup) || model.DateFrom == null || model.DateTo == null)
                return Json(new { Error = "Please fill in all required information." });
            if (!model.Workgroup.Contains("|"))
                return Json(new { Error = "Please select a single workgroup." });

            try
            {

                var req = PopulateServiceRequestObject(model);

                // Gather api data.
                var results = _api.GetAdvancedSearch(req);
                if (results.Status == StatusCode.FAIL)
                {
                    var message = "Error occurred while performing search.";
                    if (results.Errors != null && results.Errors.Any())
                        message = results.Errors.First();
                    return Json(new { Error = message });
                }

                // Store the query for future use.
                _storage.SetValue(LastQuerySessionKey, model);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = results.Results,
                        success = results.Status == StatusCode.SUCCESS,
                        draw = model.draw,
                        recordsTotal = results.Totals.TotalRecords,
                        recordsFiltered = results.Totals.TotalRecords,
                        metadata = results.Metadata,
                        totals = results.Totals
                    },
                    MaxJsonLength = Int32.MaxValue
                };
            }
            catch (Exception ex)
            {
                _logger.Log(WfsLogLevel.Error, ex.Message);
                return Json(new { Error = "Error occurred while performing search." });
            }
        }

        private AdvancedSearchDto PopulateServiceRequestObject(AdvancedSearchRequestModel request)
        {
            var req = new AdvancedSearchDto();
            var model = request.Clone();

            req.BankId = int.Parse(model.Workgroup.Split('|')[0]);
            req.WorkgroupId = int.Parse(model.Workgroup.Split('|')[1]);
            req.DepositDateFrom = DateTime.Parse(model.DateFrom);
            req.DepositDateTo = DateTime.Parse(model.DateTo);
            req.Start = model.start;
            req.Length = model.length;
            req.OrderBy = model.OrderBy;
            req.OrderDirection = model.OrderDirection;
            req.OrderByDisplayName = model.OrderByDisplayName;
            req.SelectFields = model.SelectFields?.Where(x => x != null).ToList();

            req.PaymentSourceKey = model.PaymentSourceKey;
            req.PaymentTypeKey = model.PaymentTypeKey;

            // A quick flip if they happen to be out of order.
            if (req.DepositDateFrom > req.DepositDateTo)
            {
                var datetemp = req.DepositDateFrom;
                req.DepositDateFrom = req.DepositDateTo;
                req.DepositDateTo = datetemp;
            }

            // Put together the whereclauses.
            var whereclauses = new List<AdvancedSearchWhereClauseDto>();
            if (model.Criteria == null)
                model.Criteria = new List<AdvancedSearchCriteria>();
            foreach (var w in model.Criteria.Where(x => x != null))
            {
                w.Column.Value = w.Value;
                w.Column.Operator = w.Operator;
                whereclauses.Add(w.Column);
            }
            req.WhereClauses = whereclauses;

            // Handle the standard fields.
            var whereaddfields = new List<AdvancedSearchWhereClauseDto>();
            var whereremovefields = new List<AdvancedSearchWhereClauseDto>();
            foreach (var w in req.WhereClauses.Where(x => x.IsStandard))
            {
                if (w.Operator == AdvancedSearchWhereClauseOperator.IsGreaterThan)
                    w.StandardXmlColumnName += "From";
                else if (w.Operator == AdvancedSearchWhereClauseOperator.IsLessThan)
                    w.StandardXmlColumnName += "To";
                else if (w.Operator == AdvancedSearchWhereClauseOperator.Equals)
                {
                    // If we have an equals, we need to set both the 'From' and the 'To';
                    whereaddfields.Add(new AdvancedSearchWhereClauseDto()
                    {
                        StandardXmlColumnName = w.StandardXmlColumnName + "From",
                        IsStandard = w.IsStandard,
                        ReportTitle = w.ReportTitle,
                        DataType = w.DataType,
                        Value = w.Value,
                        Operator = w.Operator
                    });
                    whereaddfields.Add(new AdvancedSearchWhereClauseDto()
                    {
                        StandardXmlColumnName = w.StandardXmlColumnName + "To",
                        IsStandard = w.IsStandard,
                        ReportTitle = w.ReportTitle,
                        DataType = w.DataType,
                        Value = w.Value,
                        Operator = w.Operator
                    });
                    whereremovefields.Add(w);
                }
            }
            req.WhereClauses.RemoveAll(x => whereremovefields.Any(y =>
                y.Table == x.Table
                && y.DataType == x.DataType
                && y.ReportTitle == x.ReportTitle));
            req.WhereClauses.AddRange(whereaddfields);

            return req;
        }

        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        public ActionResult DownloadAsText([FromUri] AdvancedSearchDownloadAsTextOptions downloadAsTextOptions)
        {
            try
            {
                // Get the AS results
                var m = (AdvancedSearchRequestModel)_storage.GetValue(LastQuerySessionKey);
                if (m == null)
                {
                    return File(new UTF8Encoding().GetBytes("An error occurred while processing the request."),
                        "text/csv", "search_results.csv");
                }

                var model = m.Clone();
                var req = PopulateServiceRequestObject(model);
                req.Start = 0;
                req.Length = 0;
                req.PaginateResults = false;
                req.ActivityCode = ActivityCodes.acSearchResultsDownloadCsv;
                var results = _api.GetAdvancedSearch(req);

                // Pull together the CSV File
                var sb = new StringBuilder();
                var columns = new List<AdvancedSearchWhereClauseDto>();
                columns.AddRange(model.SelectFields.Where(x => x != null));

                foreach (var c in columns)
                    sb.Append(c.ReportTitle + ",");
                sb.AppendLine();

                foreach (var r in results.Results)
                {
                    foreach(var c in columns)
                    {
                        var de = r.DataEntry.FirstOrDefault(x => x.FieldName == c.FieldName && x.Title == c.ReportTitle);
                        var val = string.Empty;
                        if (de != null)
                        {
                            val = de.Value;
                            var type = (AdvancedSearchWhereClauseDataType)int.Parse(de.Type);
                            if (type == AdvancedSearchWhereClauseDataType.Decimal)
                            {
                                double d;
                                var format = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
                                format.CurrencyNegativePattern = 1;
                                if (double.TryParse(de.Value, out d))
                                    val = $"{d.ToString("C2", format)}";
                                if (downloadAsTextOptions.ExcludeCommasFromNumeric)
                                {
                                    val = val.Replace(",", "");
                                }
                            }
                        }

                        sb.Append(downloadAsTextOptions.DoNotQuoteFields ? $"{val}," : $"\"{val}\",");
                    }
                    sb.AppendLine();
                }

                // Return the file
                return File(new UTF8Encoding().GetBytes(sb.ToString()), "text/csv", "search_results.csv");
            }
            catch (Exception ex)
            {
                _logger.Log(WfsLogLevel.Error, ex.Message);
                return File(new UTF8Encoding().GetBytes("An error occurred while processing the request."), "text/csv", "search_results.csv");
            }
        }

        [ValidateSessionToken("CSRFToken")]
        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        [System.Web.Mvc.HttpPost]
        public JsonResult GetAdvancedFindData(AdvancedFindRequestModel model)
        {
            // Validation
            if (string.IsNullOrWhiteSpace(model.Workgroup))
                return Json(new { Error = "Please select a single workgroup." });

            int bankid;
            int workgroupid;
            try
            {
                bankid = int.Parse(model.Workgroup.Split('|')[0]);
                workgroupid = int.Parse(model.Workgroup.Split('|')[1]);
            }
            catch
            {
                return Json(new { Error = "Please select a single workgroup." });
            }

            // Grab our data
            var datafields = _api.GetDataEntryFieldsForWorkgroup(new DataEntrySearchDTO()
            {
                SiteBankID = bankid,
                SiteClientAccountID = workgroupid
            });

            // Grouping to prevent "duplicates".  It doesn't matter which 
            // batch source key the user selects, all will be searched in 
            // the database anyway.
            datafields.DataEntryFields = datafields.DataEntryFields
                .GroupBy(x => new { x.DisplayName, x.DataType, x.TableName })
                .Select(x => x.First())
                .ToList();

            var data = new List<AdvancedSearchWhereClauseDto>();
            foreach (var de in datafields.DataEntryFields)
            {
                data.Add(new AdvancedSearchWhereClauseDto()
                {
                    BatchSourceKey = de.BatchSourceKey,
                    DataType = (AdvancedSearchWhereClauseDataType)de.DataType,
                    FieldName = de.FldName,
                    IsDataEntry = true,
                    ReportTitle = de.DisplayName,
                    Table = de.TableName == "Checks"
                        ? AdvancedSearchWhereClauseTable.Checks
                        : de.TableName == "Stubs"
                        ? AdvancedSearchWhereClauseTable.Stubs
                        : AdvancedSearchWhereClauseTable.None,
                    OrderByName = $"{de.TableName}.{de.FldName}"
                });
            }

            var workgroup = _systemserv.GetLockBoxByAccountIDBankID(bankid, workgroupid);
            var showbatchid = workgroup.Data.DisplayBatchID;

            AppendDefaultWhereClauses(data, showbatchid);

            return Json(data);
        }

        private void AppendDefaultWhereClauses(List<AdvancedSearchWhereClauseDto> list, bool includebatchid)
        {
            var defaults = new List<AdvancedSearchWhereClauseDto>()
            {
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "Batch.Deposit_Date",
                    DataType = AdvancedSearchWhereClauseDataType.DateTime,
                    ReportTitle = "Deposit Date",
                    BatchSourceKey = 255,
                    IsDataEntry = false,
                    IsDisplayFieldOnly = true,
                    OrderByName = "Batch.DepositDate",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    StandardXmlColumnName = "BatchNumber",
                    ReportTitle = "Batch Number",
                    FieldName = "Batch.BatchNumber",
                    IsStandard = true,
                    DataType = AdvancedSearchWhereClauseDataType.Float,
                    OrderByName = "BatchNumber",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "Batch.PaymentType",
                    DataType = AdvancedSearchWhereClauseDataType.String,
                    ReportTitle = "Payment Type",
                    BatchSourceKey = 255,
                    IsDataEntry = false,
                    IsDisplayFieldOnly = true,
                    OrderByName = "Batch.PaymentType",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "Batch.PaymentSource",
                    DataType = AdvancedSearchWhereClauseDataType.String,
                    ReportTitle = "Payment Source",
                    BatchSourceKey = 255,
                    IsDataEntry = false,
                    IsDisplayFieldOnly = true,
                    OrderByName = "Batch.PaymentSource",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "Transactions.TxnSequence",
                    DataType = AdvancedSearchWhereClauseDataType.Float,
                    ReportTitle = "Transaction",
                    BatchSourceKey = 255,
                    IsDataEntry = false,
                    IsDisplayFieldOnly = true,
                    OrderByName = "Transactions.TxnSequence",
                    IsDefault = true
                },

                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "Amount",
                    DataType = AdvancedSearchWhereClauseDataType.Decimal,
                    ReportTitle = "Payment Amount",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "Checks.Amount",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "RT",
                    DataType = AdvancedSearchWhereClauseDataType.String,
                    ReportTitle = "R/T",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "Checks.RT",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "Account",
                    DataType = AdvancedSearchWhereClauseDataType.String,
                    ReportTitle = "Account Number",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "Checks.Account",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "Serial",
                    DataType = AdvancedSearchWhereClauseDataType.String,
                    ReportTitle = "Check/Trace/Ref Number",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "Checks.Serial",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.None,
                    FieldName = "DDA",
                    DataType = AdvancedSearchWhereClauseDataType.String,
                    ReportTitle = "DDA",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "DDA",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "Payer",
                    DataType = AdvancedSearchWhereClauseDataType.String,
                    ReportTitle = "Payer",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "Checks.Payer",
                    IsDefault = true
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "CheckSequence",
                    DataType = AdvancedSearchWhereClauseDataType.Float,
                    ReportTitle = "Payment Sequence",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "Checks.CheckSequence"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Checks,
                    FieldName = "BatchSequence",
                    DataType = AdvancedSearchWhereClauseDataType.Float,
                    ReportTitle = "Payment Batch Sequence",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "Checks.BatchSequence"
                },
                new AdvancedSearchWhereClauseDto()
                {
                    Table = AdvancedSearchWhereClauseTable.Stubs,
                    FieldName = "BatchSequence",
                    DataType = AdvancedSearchWhereClauseDataType.Float,
                    ReportTitle = "Invoice Batch Sequence",
                    BatchSourceKey = 255,
                    IsDataEntry = true,
                    OrderByName = "Stubs.BatchSequence"
                }
            };

            if (includebatchid)
            {
                defaults.Insert(1, new AdvancedSearchWhereClauseDto()
                {
                    StandardXmlColumnName = "BatchID",
                    ReportTitle = "Batch ID",
                    FieldName = "Batch.SourceBatchID",
                    IsStandard = true,
                    DataType = AdvancedSearchWhereClauseDataType.Float,
                    OrderByName = "Batch.BatchID",
                    IsDefault = true
                });
            }

            list.InsertRange(0, defaults);
        }

        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        public JsonResult GetStoredQueries()
        {
            try
            {
                var queries = _api.GetStoredQueries();
                return Json(new { success = true, data = queries.Data });
            }
            catch( Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An Error occurred while retrieving stored queries." });
            }
        }

        [System.Web.Mvc.Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        public JsonResult GetStoredQuery(Guid id)
        {
            try
            {
                var q = _api.GetStoredQuery(id);
                return Json(new { success = true, data = q.Data });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An Error occurred while retrieving stored query." });
            }
        }

    }

}