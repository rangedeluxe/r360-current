﻿using HubViews.Models;
using System;
using System.Web.Mvc;
using Wfs.Logging;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;

namespace HubViews.Controllers
{
    public class ManageQueriesController : BaseController
    {
        private readonly IWfsLog _logger;
        private readonly IR360Services _api;

        public ManageQueriesController(IWfsLog logger)
        {
            _api = new R360ServiceManager();
            _logger = logger;
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        [HttpPost]
        public JsonResult GetStoredQueries()
        {
            try
            {
                var queries = _api.GetStoredQueries();
                return Json(new { Success = true, data = queries.Data });
            }
            catch( Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { Success = false, Error = "An Error occurred while retrieving stored queries." });
            }
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        [HttpPost]
        public JsonResult ToggleDefaultQuery([Bind(Include = "Id")] AdvancedSearchStoredQueryDto entity)
        {
            try
            {
                var query = _api.GetStoredQuery(entity.Id)
                    .Data;
                query.IsDefault = !query.IsDefault;
                var result = _api.SaveStoredQuery(query);
                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { Success = false, Error = "An Error occurred while toggling the default query." });
            }
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        [HttpPost]
        public JsonResult DeleteQuery([Bind(Include = "Id")] AdvancedSearchStoredQueryDto entity)
        {
            try
            {
                var result = _api.DeleteStoredQuery(entity.Id);
                return Json(new { Success = true });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { Success = false, Error = "An Error occurred while deleting a query." });
            }
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View)]
        [HttpPost]
        public JsonResult UpdateQuery([Bind(Include = "QueryName,QueryDescription," +
            "QueryId")] AdvancedSearchEditQueryRequestModel entity)
        {
            if (string.IsNullOrWhiteSpace(entity.QueryName))
                return Json(new {Error = "Please enter a query name."});

            try
            {
                var e = _api.GetStoredQuery(entity.QueryId.Value).Data;
                e.Name = entity.QueryName;
                e.Description = entity.QueryDescription;
                var result = _api.SaveStoredQuery(e);
                if (result.Status == StatusCode.FAIL)
                    return Json(new {Error = result.Errors[0]});
                return Json(new {Success = true});
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new {Success = false, Error = "An Error occurred while updating a query."});
            }
        }
    }
}