﻿using CommonUtils.Web;
using HubViews.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Wfs.Logging;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;

namespace HubViews.Controllers
{
  public class ReceivablesSummaryController : BaseController
  {
    private readonly IWfsLog Logger;

    public ReceivablesSummaryController(IWfsLog logger)
    {
      Logger = logger;
    }

    [Authorize]
    [HttpPost]
    [RequiresPermission(R360Permissions.Perm_Dashboard, R360Permissions.ActionType.View)]
    public JsonResult GetSummaryData(ReceivablesSummaryRequestModel model)
    {
	    // a little validation first.
		DateTime date;
        if (model == null
          || (model.BankId.HasValue && !model.WorkgroupId.HasValue)
          || (!model.BankId.HasValue && model.WorkgroupId.HasValue)
          || !DateTime.TryParse(model.DepositDateString, out date))
			return Json(new { success = false, error = "Please provide a deposit date and either a workgroup or entity." });


		try
		{
			// The old rec summary page doesn't select an entity or workgroup, so we'll default it here.
			// TODO - remove when the FW RecSummary page is gone.
			if (!model.BankId.HasValue && !model.WorkgroupId.HasValue && !model.EntityId.HasValue)
			{
				var client = new RaamClient(new ConfigHelpers.Logger(
					o => Logger.Log(WfsLogLevel.Info, o),
					o => Logger.Log(WfsLogLevel.Warn, o)));
				var root = client.GetAuthorizedEntities();
				model.EntityId = root.ID;
			}

			// build the request obj and fire it off.
			var request = new ReceivablesSummaryRequestDto()
			{
				BankId = model.BankId,
				WorkgroupId = model.WorkgroupId,
				DepositDate = date,
				EntityId = model.EntityId
			};

			var response = _R360ServiceManager.GetClientAccountSummary(request);
			// Set the Workgroup text to the EntityBreadCrumb.
			foreach (var r in response.Summaries)
				r.Organization = GetEntityBreadcrumb(r.EntityID);

			return Json(new
			{
				success = true,
				data = response.Summaries
			});
		}
		catch (Exception e)
		{
			Logger.Log(WfsLogLevel.Error, e.Message);
			return Json(new
			{
				success = false,
				error = "Error retrieving summary data."
			});
		}
    }

    private RaamClient _raamClient = null;
    private string GetEntityBreadcrumb(int entityId)
    {
      try
      {
        if (_raamClient == null)
          _raamClient = new RaamClient(new ConfigHelpers.Logger(
                                      o => Logger.Log(WfsLogLevel.Info, o),
                                      o => Logger.Log(WfsLogLevel.Warn, o)));

        return _raamClient.GetEntityBreadcrumb(entityId);
      }
      catch (Exception e)
      {
        Logger.Log(WfsLogLevel.Debug, "Error in ReceivableSummaryController.GetEntityBreadcrumb: " + e.Message);
      }
      return string.Empty;
    }

  }
}
