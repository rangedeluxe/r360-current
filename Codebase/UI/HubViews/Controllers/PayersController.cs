﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Newtonsoft.Json;
using System.Web.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

using ApiModels;
using HubViews.Helpers;
using HubViews.Models;
using Wfs.Logging;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;
using System.Security.Cryptography.X509Certificates;

namespace HubViews.Controllers
{
    public class PayersController : BaseController
    {
        private readonly HttpClient _httpClient;

        public PayersController()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings.Get("PayerAPIBaseURL"));
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + GetJWTToken());
        }

        private string GetFormmatedBaseUrl()
        {
            return ConfigurationManager.AppSettings.Get("PayerAPIBaseURL").EndsWith("/")
               ? ConfigurationManager.AppSettings.Get("PayerAPIBaseURL")
               : ConfigurationManager.AppSettings.Get("PayerAPIBaseURL") + "/";
        }

        private X509Certificate2 FindByThumbprint( string thumbprint, StoreName storeName,StoreLocation storeLocation)
        {
            try
            {
                var certificateStore = new X509Store(storeName, storeLocation);
                certificateStore.Open(OpenFlags.ReadOnly);

                foreach (var certificate in certificateStore.Certificates)
                {
                    if (string.Equals(certificate?.Thumbprint, thumbprint, StringComparison.CurrentCultureIgnoreCase))
                    {
                        certificateStore.Close();
                        return certificate;
                    }
                }
                return null;
            }
            catch(Exception)
            {
                Logger.Log(WfsLogLevel.Error, $"Cannot find certificate with thumbprint {thumbprint} in certificate store");
                return null;
            }
        }

        private string GetJWTToken()
        {
            try
            {
                var certificate = FindByThumbprint(ConfigurationManager.AppSettings.Get("PayerAPIThumbprint").ToLower(), StoreName.My, StoreLocation.LocalMachine);
                var x509SigningKey = new X509SecurityKey(certificate);
                var signingCredentials = new SigningCredentials(x509SigningKey, SecurityAlgorithms.RsaSha256);

                int lifetime = int.TryParse(ConfigurationManager.AppSettings.Get("PayerAPILifetime"), out lifetime) ? lifetime : 0;

                //generate tokens
                //
                var claims = new List<Claim>()
            {
                new Claim("sid",ClaimsPrincipal.Current.Claims.FirstOrDefault(x=>x.Type.Contains("claims/sessionid")).Value),
                new Claim("sub",ClaimsPrincipal.Current.Claims.FirstOrDefault(x=>x.Type.Contains("claims/username")).Value),
                new Claim("name",ClaimsPrincipal.Current.Claims.FirstOrDefault(x=>x.Type.Contains("claims/name")).Value),
                new Claim("eid",ClaimsPrincipal.Current.Claims.FirstOrDefault(x=>x.Type.Contains("claims/entityid")).Value),
                new Claim("ename",ClaimsPrincipal.Current.Claims.FirstOrDefault(x=>x.Type.Contains("claims/entityname")).Value),
                new Claim("uid",ClaimsPrincipal.Current.Claims.FirstOrDefault(x=>x.Type.Contains("claims/userid")).Value),
            };


                var token = new JwtSecurityToken(
                   ConfigurationManager.AppSettings.Get("PayerAPIIssuer").ToLower(),
                   ConfigurationManager.AppSettings.Get("PayerAPIAudience").ToLower(),
                   claims,
                   expires: DateTime.Now.AddMinutes(lifetime),
                   signingCredentials: signingCredentials
                );
                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return "";
            }
        }

        public ActionResult Index()
        {
            return View();
        }


        [Authorize]
        [HttpGet]
        public ActionResult GetPayers(int bankId, int workgroupId, bool isPageView = false)
        {
            var errormessage = "An error occurred while processing the request!";
            try
            {
                string url = $@"{GetFormmatedBaseUrl()}api/banks/{bankId}/workgroups/{workgroupId}/payers?isPageView={ isPageView}";
                var response = _httpClient.GetAsync(url).Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Log(WfsLogLevel.Error, $"Get PayerAPIError StatusCode:{response.StatusCode}");
                    return Json(new { Success = false, error = errormessage }, JsonRequestBehavior.AllowGet);
                }

                var result = JsonConvert.DeserializeObject<ServiceResponseModel>(response.Content.ReadAsStringAsync().Result);
                return new JsonCamelCaseResult(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { Success = false, error = errormessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_PayerMaintenance, R360Permissions.ActionType.Manage)]
        public ActionResult UpdatePayer(PayerModel payer)
        {
            var errormessage = "An error occurred while processing the request!";
            try
            {
                string url = $@"{GetFormmatedBaseUrl()}api/payers";
                var response = _httpClient.PostAsJsonAsync(url, payer).Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Log(WfsLogLevel.Error, $"Update PayerAPIError StatusCode:{response.StatusCode}");
                    return Json(new ServiceResponseModel { Success = false });
                }

                var result = JsonConvert.DeserializeObject<ServiceResponseModel>(response.Content.ReadAsStringAsync().Result);
                return new JsonCamelCaseResult(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { Success = false, error = errormessage }, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        [HttpDelete]
        [RequiresPermission(R360Permissions.Perm_PayerMaintenance, R360Permissions.ActionType.Manage)]
        public ActionResult DeletePayer(int bankId, int workgroupId, string routingNumber, string account)
        {
            var errormessage = "An error occurred while processing the request!";
            try
            {
                string url = $@"{GetFormmatedBaseUrl()}api/banks/{bankId}/workgroups/{workgroupId}/routing/{routingNumber}/account/{account}";
                var response = _httpClient.DeleteAsync(url).Result;

                if (!response.IsSuccessStatusCode)
                {
                    Logger.Log(WfsLogLevel.Error, $"Delete PayerAPIError StatusCode:{response.StatusCode}");
                    return Json(new ServiceResponseModel { Success = false });
                }


                var result = JsonConvert.DeserializeObject<ServiceResponseModel>(response.Content.ReadAsStringAsync().Result);
                return new JsonCamelCaseResult(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { Success = false, error = errormessage },JsonRequestBehavior.AllowGet);
            }
        }
    }
}