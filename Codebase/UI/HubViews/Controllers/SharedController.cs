﻿using AutoMapper;
using framework.services.common;
using HubViews.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wfs.Logging;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Controllers
{
    /// <summary>
    /// This contains some basic calls which multiple pages can call.
    /// </summary>
    public class SharedController : BaseController
    {
        private readonly IR360Services _hub;
        private readonly IHubConfig _hubconfig;
        private readonly IConfigurationProvider _mapperconfig;
        private readonly IMapper _mapper;

        public SharedController(
            IR360Services hub,
            IHubConfig hubconfig)
        {
            _hub = hub;
            _hubconfig = hubconfig;
            _mapperconfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PaymentTypeDto, PaymentTypeModel>();
                cfg.CreateMap<WFS.RecHub.R360Services.Common.DTO.PaymentSourceDto, PaymentSourceModel>();
            });
            _mapper = _mapperconfig.CreateMapper();
        }

        [Authorize]
        public JsonResult GetPaymentTypes()
        {
            IsAuditEventSuppressed = true;
            try
            {
                var paymentypes = _hub.GetPaymentTypes()
                    .PaymentTypes
                    .Select(x => _mapper.Map<PaymentTypeModel>(x))
                    .ToList();
                paymentypes.Insert(0, new PaymentTypeModel { LongName = "-- All --", PaymentTypeKey = -1 });
                return Json(new { success = true, data = paymentypes }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An Error occurred while retrieving payment types." });
            }
        }

        [Authorize]
        public JsonResult GetPaymentSources()
        {
            IsAuditEventSuppressed = true;
            try
            {
                var paymentsources = _hub.GetPaymentSources()
                    .PaymentSources
                    .Select(x => _mapper.Map<PaymentSourceModel>(x))
                    .ToList();
                paymentsources.Insert(0, new PaymentSourceModel { LongName = "-- All --", PaymentSourceKey = -1 });
                return Json(new { success = true, data = paymentsources }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An Error occurred while retrieving payment sources." });
            }
        }

        [Authorize]
        public new JsonResult GetSystemPreferences()
        {
            try
            {
                var response = _configService.GetPreferences();
                var preferences = response.Data;
                return Json(new { data = preferences, success = true });
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, "Exception occurred in GetPreferences (SharedController)\r\n" + ex);
                return Json(new { success = false, error = "An error occurred while retreiving the system preferences." });
            }
        }

        [Authorize]
        public JsonResult GetSystemPreference(string name)
        {
            IsAuditEventSuppressed = true;
            try
            {
                var response = _configService.GetPreferences();
                var preferences = response.Data;
                var pref = preferences.FirstOrDefault(x => x.Name == name);
                return Json(new { data = pref, success = pref != null });
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, "Exception occurred in GetPreference (SharedController)\r\n" + ex);
                return Json(new { success = false, error = "An error occurred while retreiving a system preference." });
            }
        }
    }
}