﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using HubViews.Models;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using Propel.Framework.Logging;
using Wfs.Logging;
using System.Net;
using WFS.RecHub.R360WebShared.Attributes;

namespace HubViews.Controllers
{
	public class BatchSummaryController : BaseController
	{
		private readonly IWfsLog Logger;
		private readonly R360ServiceManager _r360service;
		private readonly string _sitekey;
		private readonly SystemServiceManager _systemservice;
		private readonly IConfigurationProvider _mapperconfig;
		private readonly IMapper _mapper;

		public BatchSummaryController(IWfsLog logger)
		{
			_r360service = new R360ServiceManager();
			_systemservice = new SystemServiceManager();
			_sitekey = R360ServiceContext.Current.SiteKey;
			Logger = logger;

			// Just some server object -> view model object mappings.
			_mapperconfig = new MapperConfiguration(cfg =>
			{
				cfg.CreateMap<BatchSummaryDto, BatchResultModel>()
					.ForMember(
						x => x.DepositDateString,
						x => x.MapFrom(src => src.DepositDate.ToString("MM/dd/yyyy")))
					.ForMember(
						x => x.WorkgroupID,
						x => x.MapFrom(src => src.ClientAccountId));
				cfg.CreateMap<PaymentTypeDto, PaymentTypeModel>();
				cfg.CreateMap<PaymentSourceDto, PaymentSourceModel>();
			});
			_mapper = _mapperconfig.CreateMapper();
		}

		[RequiresPermission(R360Permissions.Perm_BatchSummary, R360Permissions.ActionType.View)]
		public ActionResult Index(BatchSearchModel model)
		{
			// First things first.  We need to grab the referrer URI and check to see 
			// if we need to redirect to batch detail.  (TODO: remove this after everything is a portlet).
			if (Request.Cookies.AllKeys.Contains("r360-redir-url"))
			{
				// Get the cookie, clear it.
				var cookie = Request.Cookies["r360-redir-url"];
				Response.Cookies["r360-redir-url"].Expires = DateTime.Now.AddDays(-1);

				// Redirect to the correct page.
				if (cookie.Value != null && cookie.Value.Contains("redir=batchdetail"))
					return Redirect("/RecHubViews/BatchDetail");
				else if (cookie.Value != null && cookie.Value.Contains("redir=transactiondetail"))
					return Redirect("/RecHubViews/TransactionDetail");
			}


			// Now finally onto batch summary.
			var query = new BatchSearchModel();

			// Params From rec summary or breadcrumbs.
			if (model != null && !string.IsNullOrWhiteSpace(model.DateFrom))
			{
				query = model;
			}

			// Register the page activity with our SessionAPI.  This handle's the breadcrumbs
			// since we're no longer on a legacy page.
			_systemservice.InitPageActivity("batchsummary.aspx", string.Empty);

			return View(query);
		}

		[RequiresPermission(R360Permissions.Perm_BatchSummary, R360Permissions.ActionType.View)]
		public JsonResult GetBatches(BatchSearchModel query)
		{
			try
			{
				// First grab the additional form data posted by the datatables grid.
				var index = Request.Form["order[0][column]"];
				var key = "columns[" + index + "][data]";
				query.OrderBy = Request.Form[key];
				query.OrderDirection = Request.Form["order[0][dir]"];
				query.OrderBy = query.OrderBy ?? string.Empty;
				query.OrderDirection = query.OrderDirection ?? string.Empty;

				// Parsing and validation
				int bankid;
				int workgroupid;
				if (query.Workgroup == null)
					return Json(new { success = false, error = "Please select a workgroup." },
						JsonRequestBehavior.AllowGet);

				// Expected format: "bankid|workgroupid"
				var workgroupstrings = query.Workgroup.Split('|');
				if (workgroupstrings.Length < 2
					|| !int.TryParse(workgroupstrings[0], out bankid)
					|| !int.TryParse(workgroupstrings[1], out workgroupid))
					return Json(new { success = false, error = "Please select a workgroup." },
						JsonRequestBehavior.AllowGet);

				// Validation for the date ranges.
				DateTime datefrom;
				DateTime dateto;
				if (!DateTime.TryParse(query.DateFrom, out datefrom)
					|| !DateTime.TryParse(query.DateTo, out dateto))
					return Json(new { success = false, error = "Please enter a valid date range." },
						JsonRequestBehavior.AllowGet);

				// A quick flip if they happen to be out of order, no biggie.
				if (datefrom > dateto)
				{
					var datetemp = datefrom;
					datefrom = dateto;
					dateto = datetemp;
				}

				// Perform the search!
				var request = new BatchSummaryRequestDto
				{
					BankId = bankid,
					ClientAccountId = workgroupid,
					FromDate = datefrom,
					ToDate = dateto,
					PaymentTypeId = query.PaymentType,
					PaymentSourceId = query.PaymentSource,
					Start = query.start,
					Length = query.length,
					OrderBy = query.OrderBy,
					OrderDirection = query.OrderDirection,
					Search = query.search
				};
				var results = _r360service.GetBatchSummary(request);
				var message = string.Empty;

				if (results.Status != StatusCode.SUCCESS)
				{
					// We failed, so let's get an Error message together.
					message = "Error communicating with the server.";
					if (results.Errors != null && results.Errors.Any())
						message = results.Errors.First();
					return Json(new { success = false, error = message }, JsonRequestBehavior.AllowGet);
				}
				var mappedresults = results.BatchSummaryList
					.Select(x => _mapper.Map<BatchResultModel>(x));

				// Set the property to show or hide the BatchId in the Grid. A bit of a hack, but should work
				var showBatchId = ShowBatchId(workgroupid, bankid);
				var showBathSiteCode =
					results.Preferences.Any(x => x.Name == "ShowBatchSiteCodeOnline" && x.Value == "Y");
				var batchResultModels = mappedresults as BatchResultModel[] ?? mappedresults.ToArray();
				foreach (var bs in batchResultModels)
				{
					bs.ShowBatchId = showBatchId;
					bs.ShowBathSiteCode = showBathSiteCode;
				}

				return Json(new
				{
					success = results.Status == StatusCode.SUCCESS ? true : false,
					data = batchResultModels,
					query.draw,
					recordsTotal = results.TotalRecords,
					recordsFiltered = results.FilteredRecords,
                    transactionCount = results.TransactionCount,
                    checkCount = results.CheckCount,
                    documentCount = results.DocumentCount,
                    checkTotal = results.CheckTotal,
					error = message
				}, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				Logger.Log(WfsLogLevel.Error, e.Message);
				return Json(new { success = false, error = "An Error occurred communicating with the server." });
			}
		}

		[RequiresPermission(R360Permissions.Perm_BatchSummary, R360Permissions.ActionType.View)]
		public JsonResult GetPaymentTypes()
		{
			try
			{
				var paymentypes = _r360service.GetPaymentTypes()
					.PaymentTypes
					.Select(x => _mapper.Map<PaymentTypeModel>(x))
					.ToList();
				paymentypes.Insert(0, new PaymentTypeModel { LongName = "-- All --", PaymentTypeKey = null });
				return Json(new { success = true, data = paymentypes });
			}
			catch (Exception e)
			{
				Logger.Log(WfsLogLevel.Error, e.Message);
				return Json(new { success = false, error = "An Error occurred while retrieving payment types." });
			}
		}

		[RequiresPermission(R360Permissions.Perm_BatchSummary, R360Permissions.ActionType.View)]
		public JsonResult GetPaymentSources()
		{
			try
			{
				var paymentsources = _r360service.GetPaymentSources()
					.PaymentSources
					.Select(x => _mapper.Map<PaymentSourceModel>(x))
					.ToList();
				paymentsources.Insert(0, new PaymentSourceModel { LongName = "-- All --", PaymentSourceKey = null });
				return Json(new { success = true, data = paymentsources });
			}
			catch (Exception e)
			{
				Logger.Log(WfsLogLevel.Error, e.Message);
				return Json(new { success = false, error = "An Error occurred while retrieving payment sources." });
			}
		}

		public ActionResult Print(BatchSearchModel query)
		{
			try
			{
				query.OrderBy = query.OrderBy ?? string.Empty;
				query.OrderDirection = query.OrderDirection ?? string.Empty;
				// Parsing and validation
				int bankid;
				int workgroupid;
				if (query.Workgroup == null)
					return View(new BatchSummaryPrintModel { Success = false, Error = "Please select a workgroup." });


				// Expected format: "bankid|workgroupid"
				var workgroupstrings = query.Workgroup.Split('|');
				if (workgroupstrings.Length < 2
					|| !int.TryParse(workgroupstrings[0], out bankid)
					|| !int.TryParse(workgroupstrings[1], out workgroupid))
					return View(new BatchSummaryPrintModel { Success = false, Error = "Please select a workgroup." });

				// Validation for the date ranges.
				DateTime datefrom;
				DateTime dateto;
				if (!DateTime.TryParse(query.DateFrom, out datefrom)
					|| !DateTime.TryParse(query.DateTo, out dateto))
					return View(new BatchSummaryPrintModel { Success = false, Error = "Please enter a valid date range." });

				// A quick flip if they happen to be out of order, no biggie.
				if (datefrom > dateto)
				{
					var datetemp = datefrom;
					datefrom = dateto;
					dateto = datetemp;
				}

				// Perform the search!
				var request = new BatchSummaryRequestDto
				{
					BankId = bankid,
					ClientAccountId = workgroupid,
					FromDate = datefrom,
					ToDate = dateto,
					PaymentTypeId = query.PaymentType,
					PaymentSourceId = query.PaymentSource,
					Start = query.start,
					Length = query.length,
					OrderBy = query.OrderBy,
					OrderDirection = query.OrderDirection,
					Search = query.search
				};
				var results = _r360service.GetBatchSummary(request);
				var message = string.Empty;

				if (results.Status != StatusCode.SUCCESS)
				{
					// We failed, so let's get an Error message together.
					message = "Error communicating with the server.";
					if (results.Errors != null && results.Errors.Any())
						message = results.Errors.First();
					return View(new BatchSummaryPrintModel { Success = false, Error = message });
				}
				var mappedresults = results.BatchSummaryList
					.Select(x => _mapper.Map<BatchResultModel>(x));

				// Set the property to show or hide the BatchId in the Grid. A bit of a hack, but should work
				var showBatchId = ShowBatchId(workgroupid, bankid);
				var batchResultModels = mappedresults as BatchResultModel[] ?? mappedresults.ToArray();
				foreach (var bs in batchResultModels)
				{
					bs.ShowBatchId = showBatchId;
				}
				return View(new BatchSummaryPrintModel
				{
					Success = results.Status == StatusCode.SUCCESS ? true : false,
					Data = batchResultModels,
					Preferences = results.Preferences,
					SiteCode = results.SiteCode,
					RecordsTotal = results.TotalRecords,
					RecordsFiltered = results.FilteredRecords,
					Error = message
				});
			}
			catch (Exception e)
			{
				Logger.Log(WfsLogLevel.Error, e.Message);
				return View(new BatchSummaryPrintModel { Success = false, Error = "An Error occurred communicating with the server." });
			}
		}
	}
}