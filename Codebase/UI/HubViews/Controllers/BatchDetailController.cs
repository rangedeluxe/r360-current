﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using AutoMapper;
using HubViews.Models;
using Wfs.Logging;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;

namespace HubViews.Controllers
{
	public class BatchDetailController : BaseController
	{
		private readonly IWfsLog Logger;

		private readonly R360ServiceManager _r360Service;
		private readonly string _sitekey;
		private readonly SystemServiceManager _systemservice;
		private readonly IConfigurationProvider _mapperconfig;
		private readonly IMapper _mapper;

		public BatchDetailController(IWfsLog logger)
		{
			_r360Service = new R360ServiceManager();
			_sitekey = R360ServiceContext.Current.SiteKey;
			_systemservice = new SystemServiceManager();
			logger = Logger;

			_mapperconfig = new MapperConfiguration(x =>
			{
                x.CreateMap<BatchDetailDto, BatchDetailModel>()
                    .ForMember(
                        y => y.DepositDateString,
                        y => y.MapFrom(src => src.DepositDate.ToString("MM/dd/yyyy")))
                    .ForMember(
                        y => y.ProcessingDateString,
                        y => y.MapFrom(src => src.ProcessingDate.ToString("MM/dd/yyyy")));

            });
			_mapper = _mapperconfig.CreateMapper();
		}

		// GET: Intial Batch Detail Page for Legecy calls
		public ActionResult Index()
		{
			//Anonymous Type to keep the interface in tact
			var data = Json(new
			{
				success = true,
				data = "",
				error = "Request Records Pending",
				totalRecords = 0,
				filteredRecords = 0
			}, JsonRequestBehavior.AllowGet);

			return View(data);
		}

		public ActionResult IndexResults(BatchDetailModel model)
		{
			if (R360Permissions.Current.Allowed(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSearch, R360Permissions.ActionType.View) ||
			    R360Permissions.Current.Allowed(R360Permissions.Perm_BatchSummary, R360Permissions.ActionType.View))
			{
				JsonResult data = null;
				try
				{
					data = GetBatchDetailList(model);
				}
				catch (Exception e)
				{
					Logger.Log(WfsLogLevel.Error, e.Message);
					data = Json(new {success = false, error = "An Error occurred communicating with the server."});
				}

				return View("Index", data);
			}

			return new HttpUnauthorizedResult();
		}

		public ActionResult Print(BatchDetailModel model)
		{
			BatchDetailResultsModel data = new BatchDetailResultsModel();
			try
			{
				data = GetBatchDetailAsObject(model);

			}
			catch (Exception e)
			{
				Logger.Log(WfsLogLevel.Error, e.Message);
				data.Error = "An Error occurred communicating with the server.";
			}
			return View(data);
		}

		// GET: BatchDetail Records
		public JsonResult GetBatchDetails(BatchDetailModel model)
		{
			try
			{
				// First grab the additional form data posted by the datatables grid.
				var index = Request.Form["order[0][column]"];
				var key = "columns[" + index + "][data]";
				model.OrderBy = Request.Form[key];
				model.OrderDirection = Request.Form["order[0][dir]"];
				model.OrderBy = model.OrderBy ?? string.Empty;
				model.OrderDirection = model.OrderDirection ?? string.Empty;
				return GetBatchDetailList(model);
			}
			catch (Exception e)
			{
				Logger.Log(WfsLogLevel.Error, e.Message);
				return Json(new { success = false, error = "An Error occurred communicating with the server." });
			}
		}

		private JsonResult GetBatchDetailList(BatchDetailModel model)
		{
			// Get the System Preference so we can decide Headers to show
			var preferences = GetSystemPreferences();

			// Perform the search!
			var request = new BatchDetailRequestDto
			{
				BankId = Convert.ToInt32(model.BankId),
				WorkgroupId = Convert.ToInt32(model.WorkgroupId),
				DepositDate = Convert.ToDateTime(model.DepositDateString, CultureInfo.CurrentCulture),
				BatchId = Convert.ToInt32(model.BatchId),
				Start = model.start,
				Length = model.length,
				OrderBy = model.OrderBy,
				OrderDirection = model.OrderDirection,
				Search = model.Search
			};
		var results = _r360Service.GetBatchDetail(request);

			var message = string.Empty;

			if (results.Status != StatusCode.SUCCESS)
			{
				// We failed, so let's get an Error message together.
				message = "Error communicating with the server.";
				if (results.Errors != null && results.Errors.Any())
					message = results.Errors.First();
			}

			if (results.TotalRecords > 0 && results.BatchDetailList.Any())
			{
				var mappedresults = results.BatchDetailList.Select(x => _mapper.Map<BatchDetailModel>(x)).ToList();
				var firstRec = mappedresults[0];
				var showImageRps =
						   R360Permissions.Current.Allowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View)
						   && firstRec.ImportTypeShortName == "ImageRPS";
				return Json(new
				{
					success = results.Status == StatusCode.SUCCESS ? true : false,
					data = mappedresults,
					workgroup = model.WorkgroupId + " - " + firstRec.Workgroup,
					workgroupId = model.WorkgroupId,
					bank = model.BankId,
					accountSiteCode = firstRec.AccountSiteCode,
					batchSiteCode = firstRec.BatchSiteCode,
					depositDate = model.DepositDateString,
					batchCue = firstRec.BatchCueID,
					sourceBatchId = firstRec.SourceBatchId,
					batchId = model.BatchId,
					batch = firstRec.BatchNumber,
					showBatchId = model.BankId != null && (model.WorkgroupId != null && ShowBatchId(model.WorkgroupId.Value, model.BankId.Value)),
					showBank = GetDisplayPreference(preferences, "ShowBankIDOnline"),
					showAccountSiteCode = GetDisplayPreference(preferences, "ShowLockboxSiteCodeOnline"),
					showBatchSiteCode = GetDisplayPreference(preferences, "ShowBatchSiteCodeOnline"),
					showBatchCue = GetDisplayPreference(preferences, "DisplayBatchCueIDOnline"),
					showImageRpsAudit = showImageRps,
					recordsTotal = results.TotalRecords,
					recordsFiltered = results.FilteredRecords,
					totalAmount = firstRec.TotalAmount,
					error = message
				}, JsonRequestBehavior.AllowGet);
			}
			return Json(new
			{
				success = results.Status == StatusCode.SUCCESS ? true : false,
				recordsTotal = results.TotalRecords,
				recordsFiltered = results.FilteredRecords,
				data = ""

			} , JsonRequestBehavior.AllowGet);
		}

		private BatchDetailResultsModel GetBatchDetailAsObject(BatchDetailModel model)
		{
			// Get the System Preference so we can decide Headers to show
			var preferences = GetSystemPreferences();

			// Perform the search!
			var request = new BatchDetailRequestDto
			{
				BankId = Convert.ToInt32(model.BankId),
				WorkgroupId = Convert.ToInt32(model.WorkgroupId),
				DepositDate = Convert.ToDateTime(model.DepositDateString, CultureInfo.CurrentCulture),
				BatchId = Convert.ToInt32(model.BatchId),
				Start = model.start,
				Length = model.length,
				OrderBy = model.OrderBy,
				OrderDirection = model.OrderDirection,
				Search = model.Search
			};
			var results = _r360Service.GetBatchDetail(request);

			var message = string.Empty;

			if (results.Status != StatusCode.SUCCESS)
			{
				// We failed, so let's get an Error message together.
				message = "Error communicating with the server.";
				if (results.Errors != null && results.Errors.Any())
					message = results.Errors.First();
			}

			var mappedresults = results.BatchDetailList
				.Select(x => _mapper.Map<BatchDetailModel>(x)).ToList();
			var firstRec = mappedresults[0];
			return new BatchDetailResultsModel
			{
				Success = results.Status == StatusCode.SUCCESS ? true : false,
				Data = mappedresults,
				Workgroup = model.WorkgroupId + " - " + firstRec.Workgroup,
				WorkgroupId = model.WorkgroupId,
				Bank = model.BankId,
				AccountSiteCode = firstRec.AccountSiteCode,
				BatchSiteCode = firstRec.BatchSiteCode,
				DepositDate = model.DepositDateString,
				BatchCue = firstRec.BatchCueID,
				SourceBatchId = firstRec.SourceBatchId,
				BatchId = model.BatchId,
				Batch = firstRec.BatchNumber,
				ShowBatchId = model.BankId != null && (model.WorkgroupId != null && ShowBatchId(model.WorkgroupId.Value, model.BankId.Value)),
				ShowBank = GetDisplayPreference(preferences, "ShowBankIDOnline"),
				ShowAccountSiteCode = GetDisplayPreference(preferences, "ShowLockboxSiteCodeOnline"),
				ShowBatchSiteCode = GetDisplayPreference(preferences, "ShowBatchSiteCodeOnline"),
				ShowBatchCue = GetDisplayPreference(preferences, "DisplayBatchCueIDOnline"),
				RecordsTotal = results.TotalRecords,
				RecordsFiltered = results.FilteredRecords,
				Error = message
			};
		}

	}
}