﻿using CommonUtils.Web;
using HubViews.Authorization;
using HubViews.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFS.RecHub.R360Services.Common;

namespace HubViews.Controllers
{
  public class SearchController : BaseController
  {
    private static readonly Wfs.Logging.ILogSource Logger = Wfs.Logging.LogSourceFactory.GetInstance(typeof(SearchController));

    public ActionResult Search()
    {
      var model = new ActionResponse
      {
        Data = new
        {
          Labels = GetLabelsForPage("BatchSummary", true)
        }
      };
      return View(model);
    }

    public ActionResult Filter()
    {
      var model = new ActionResponse
      {
        Data = new
        {
          Labels = GetLabelsForPage("BatchSummary", true),
          entities = GetMockOrgData()
        }
      };
      return View(model);
    }

    public JsonResult GetFilter()
    {
      var webResponse = new JSONResponse();
      try
      {
        var data = new
        {
          Labels = GetLabelsForPage("BatchSummary", true),
          entities = GetMockOrgData()
        };
        webResponse.HasErrors = false;
        webResponse.Data = data;
      }
      catch (Exception e)
      {
        // this is a communication error... error is logged in the service client, 
        // rethrown and caught here to send clean response to user
        string message = "Error in SearchController: " + e.Message;
        SetErrorResponse(webResponse, message, Get("General", "UnableToRetrieve"));
      }

      var jsonResult = this.Json(webResponse);
      jsonResult.MaxJsonLength = int.MaxValue;
      return jsonResult;
    }

    private IList<OrganizationModelMock> GetMockOrgData()
    {
      IList<OrganizationModelMock> rvalue = new List<OrganizationModelMock>();

      string[] orgs = new string[6] { "WFS", "ABC", "XYZ", "QRS", "TUV", "DEF" };
      string[] orgs2 = new string[8] { "Insurance", "Manufacturing", "Electronics", "Restaurant", "Hair Stylists", "Clothing", "Medical", "Realty" };

      var root = new OrganizationModelMock { id = "1", parentId = "-1", label = "Big FI" };
      var holding = new OrganizationModelMock { id = "2", parentId = "1", label = "ABC Holding" };
      var holding2 = new OrganizationModelMock { id = "3", parentId = "1", label = "XYZ Holding" };
      root.items.Add(holding);
      root.items.Add(holding2);
      rvalue.Add(root);

      int counter = 0;

      // ABC holding
      for (int i = 4; i < 10; i++)
      {
        counter++;
        var orgId = System.Guid.NewGuid().ToString();
        var org = new OrganizationModelMock { id = orgId, parentId = "2", label = orgs[i % 6] + " " + orgs2[i % 8] };
        for (int j = 100; j < 103; j++)
        {
          counter++;
          var divId = System.Guid.NewGuid().ToString();
          var div = new OrganizationModelMock { id = divId, parentId = orgId, label = "Division-" + j };
          for (int k = 300; k < 303; k++)
          {
            counter++;
            div.items.Add(new OrganizationModelMock { id = System.Guid.NewGuid().ToString(), parentId = divId, label = "Workgroup-" + k });
          }
          org.items.Add(div);
        }
        for (int m = 700; m < 703; m++)
        {
          counter++;
          org.items.Add(new OrganizationModelMock { id = System.Guid.NewGuid().ToString(), parentId = orgId, label = "Workgroup-" + m });
        }
        holding.items.Add(org);
      }

      // ABC holding
      for (int i = 11; i < 15; i++)
      {
        counter++;
        var orgId = System.Guid.NewGuid().ToString();
        var org = new OrganizationModelMock { id = orgId, parentId = "2", label = orgs[i % 6] + " " + orgs2[i % 8] };
        for (int j = 100; j < 103; j++)
        {
          counter++;
          var divId = System.Guid.NewGuid().ToString();
          var div = new OrganizationModelMock { id = divId, parentId = orgId, label = "Division-" + j };
          for (int k = 300; k < 303; k++)
          {
            counter++;
            div.items.Add(new OrganizationModelMock { id = System.Guid.NewGuid().ToString(), parentId = divId, label = "Workgroup-" + k });
          }
          org.items.Add(div);
        }
        for (int m = 700; m < 703; m++)
        {
          counter++;
          org.items.Add(new OrganizationModelMock { id = System.Guid.NewGuid().ToString(), parentId = orgId, label = "Workgroup-" + m });
        }
        holding2.items.Add(org);
      }

      return rvalue;
    }

    public List<OrganizationModelMock> FlatToHierarchy(IList<OrganizationModelMock> list)
    {
      // hashtable lookup that allows us to grab references to containers based on id
      var lookup = new Dictionary<string, OrganizationModelMock>();
      // actual nested collection to return
      List<OrganizationModelMock> nested = new List<OrganizationModelMock>();

      foreach (OrganizationModelMock item in list)
      {
        if (lookup.ContainsKey(item.parentId))
        {
          // add to the parent's child list 
          lookup[item.parentId].items.Add(item);
        }
        else
        {
          // no parent added yet (or this is the first time)
          nested.Add(item);
        }
        lookup.Add(item.id, item);
      }

      return nested;
    }
  }
}
