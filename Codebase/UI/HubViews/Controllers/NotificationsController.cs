﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using HubViews.Models;
using Wfs.Logging;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared.Attributes;

namespace HubViews.Controllers
{
    public class NotificationsController : BaseController
    {
        private readonly IConfigurationProvider _mapperconfig;
        private readonly IMapper _mapper;

        public NotificationsController()
        {
            _mapperconfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<NotificationsRequestModel, NotificationsRequestDto>()
                    .ForMember(x => x.StartRecord, x => x.MapFrom(source => source.start))
                    .ForMember(x => x.RecordsPerPage, x => x.MapFrom(source => source.length))
                    .ForMember(x => x.StartDate, x => x.MapFrom(source => DateTime.Parse(source.StartDate)))
                    .ForMember(x => x.EndDate, x => x.MapFrom(source => DateTime.Parse(source.EndDate)));
                cfg.CreateMap<NotificationWorkgroupModel, WorkgroupDto>();
                cfg.CreateMap<NotificationFileTypeDto, NotificationFileTypeModel>();
                cfg.CreateMap<NotificationDto, NotificationModel>();
            });
            _mapper = _mapperconfig.CreateMapper();
        }
        [RequiresPermission(R360Permissions.Perm_Notifications, R360Permissions.ActionType.View)]
        public ActionResult Index(NotificationsRequestModel model)
        {
            return View(model);
        }

        [RequiresPermission(R360Permissions.Perm_Notifications, R360Permissions.ActionType.View)]
        public ActionResult Detail(NotificationDto model)
        {
            return View(model);
        }

        [RequiresPermission(R360Permissions.Perm_Notifications, R360Permissions.ActionType.View)]
        public JsonResult GetNotifications(NotificationsRequestModel model)
        {
            try
            {
                var request = _mapper.Map<NotificationsRequestDto>(model);
                var results = _R360ServiceManager.GetNotifications(request);
                var message = string.Empty;
                if (results.Status != StatusCode.SUCCESS)
                {
                    message = "There was an error fetching Notifications";
                    if (results.Errors != null && results.Errors.Any())
                        message = results.Errors.First();
                    return Json(new { success = false, error = message });
                }
                var mappedResults = results.Notifications.Select(_mapper.Map<NotificationModel>).ToArray();
               
                return Json(new
                {
                    success = results.Status == StatusCode.SUCCESS,
                    data = mappedResults,
                    model.draw,
                    recordsTotal = results.TotalRecords,
                    recordsFiltered = results.FilteredTotal,
                    error = message,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while retrieving Notifications." });
            }
        }

        [RequiresPermission(R360Permissions.Perm_Notifications, R360Permissions.ActionType.View)]
        public JsonResult GetNotificationDetail(NotificationRequestDto model)
        {
            var errormessage = "An error occurred while retrieving Notification Detail.";
            var errorresult = Json(new { success = false, error = errormessage });

            try
            {
                if (model == null)
                    return errorresult;

                var notification = _R360ServiceManager.GetNotification(model);
                if (notification == null || notification.Status == StatusCode.FAIL)
                {
                    var error = errormessage;
                    if (notification.Errors != null && notification.Errors.Count > 0)
                        error = notification.Errors[0];
                    return Json(new { success = false, error = error });
                }
                return Json(new
                {
                    success = true,
                    data = notification.Notification,
                    metadata = notification.MetaData
                });
            }
            catch(Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return errorresult;
            }
        }


        //private and embarrasing :(
        private string GetTimeZoneLabel()
        {
            //Time zone label
            // Retrieve null separated list of key/site value pairs and separate into array
            var arrSites = ipoINILib.GetINISection("Sites");
            string siteKey = string.Empty;
            foreach (string strSite in arrSites)
            {
                if (strSite.Length > 0)
                {
                    var arrEntry = strSite.Split('=');  // Split site key/value pair into array
                    if (Request.ServerVariables["SERVER_NAME"] == arrEntry[arrEntry.GetUpperBound(0)])
                    {
                        siteKey = arrEntry[arrEntry.GetLowerBound(0)];
                        break;
                    }
                }
            }

            var timezoneLabel = ipoINILib.IniReadValue(siteKey, "TimeZoneLabel");
            if (timezoneLabel.Length == 0)
            {
                timezoneLabel = "Eastern";
            }
            bool adjustDls = bool.TryParse(ipoINILib.IniReadValue(siteKey, "AdjustDaylightSavings"), out adjustDls) && adjustDls;
            timezoneLabel += adjustDls ? "-dst" : "";

            return timezoneLabel;
        }

        [RequiresPermission(R360Permissions.Perm_Notifications, R360Permissions.ActionType.View)]
        public JsonResult GetNotificationsFileTypes()
        {
            try
            {
                var fileTypes = _R360ServiceManager.GetNotificationsFileTypesResponse()
                    .FileTypes.Select(_mapper.Map<NotificationFileTypeModel>)
                    .ToList();
                fileTypes.Insert(0, new NotificationFileTypeModel { FileTypeDescription = "All", NotificationFileTypeKey = 0 });
                return Json(new { success = true, data = fileTypes });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, data = "An error occurred while retrieving Notifications File Types" });
            }
        }
    }
}