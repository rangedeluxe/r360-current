﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using CommonUtils.Web;
using FrameworkClient;
using Wfs.Logging;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360WebShared;
using Wfs.Logging.NLog;

namespace HubViews.Controllers
{
    public class BaseController : WfsSharedBaseController
    {
        protected readonly IWfsLog Logger;

        protected readonly HubConfigServicesManager _configService;
        protected readonly UserServiceManager _userService;

        public BaseController()
        {
            // initialize multi lingual
            _R360ServiceManager = new R360ServiceManager();
            _LanguageManager = new LanguageManager();
            _configService = new HubConfigServicesManager();
            _userService = new UserServiceManager();
            Logger = new WfsLog();
        }

        public LanguageManager _LanguageManager { get; set; }
        public R360ServiceManager _R360ServiceManager { get; set; }

        public string Get(string group, string property)
        {
            return _LanguageManager.Get("HubViews", group, property);
        }

        public Dictionary<string, string> GetLabelsForPage(string page, bool includeGeneral)
        {
            var pageLabels = new Dictionary<string, string>();

            var doMock = Convert.ToBoolean(ConfigurationManager.AppSettings["MockData"]);
            if (doMock)
            {
                pageLabels = addMock(page);
            }
            else
            {
                pageLabels = _LanguageManager.GetGroup("HubViews", page);
                if (includeGeneral)
                {
                    var general = _LanguageManager.GetGroup("HubViews", "General");
                    var generalBase = _LanguageManager.GetGroup("Base", "General");
                    pageLabels = _LanguageManager.Merge(_LanguageManager.Merge(pageLabels, general), generalBase);
                }
            }

            return pageLabels;
        }

        private Dictionary<string, string> addMock(string page)
        {
            var overall = new Dictionary<string, Dictionary<string, string>>();

            var recsum = new Dictionary<string, string>();
            recsum.Add("Title", "Receivables Summary");
            overall.Add("Summary", recsum);

            var sum = new Dictionary<string, string>();
            sum.Add("Title", "Batch Summary");
            overall.Add("BatchSummary", sum);

            var det = new Dictionary<string, string>();
            det.Add("Title", "Batch Details");
            overall.Add("BatchDetails", det);

            var tran = new Dictionary<string, string>();
            tran.Add("Title", "Transaction Details");
            overall.Add("TransactionDetails", tran);

            var dash = new Dictionary<string, string>();
            dash.Add("Title", "Dashboard");
            overall.Add("Dashboard", dash);

            var rvalue = overall[page];

            rvalue.Add("Denom", "$");
            rvalue.Add("NoData", "No Data Available.");

            return rvalue;
        }

        // convenience method to get a dictionary from an array for ui... 
        protected Dictionary<string, string> getEnumKeyValue<T>()
        {
            if (!typeof (T).IsEnum)
                throw new ArgumentException("T is not an Enum type");
            if (Enum.GetUnderlyingType(typeof (T)) != typeof (int))
                throw new ArgumentException("The underlying type of the enum T is not Int32");
            return Enum.GetValues(typeof (T)).Cast<T>()
                .ToDictionary(t => ((int) (object) t).ToString(), t => t.ToString());
        }

        protected void SetResponse(BaseResponse response, JSONResponse webResponse, object data,
            [CallerMemberName] string caller = "")
        {
            webResponse.HasErrors = response.Status == StatusCode.FAIL ? true : false;
            webResponse.Data = data;
            if (webResponse.HasErrors)
            {
                if (response.Errors != null && response.Errors.Count > 0)
                {
                    webResponse.AddErrors(response.Errors);
                }
                else
                {
                    Logger.Log(WfsLogLevel.Error, "Server response status set to FAIL but provided no message.  " + caller);
                    webResponse.Errors = new List<WebErrors>
                    {
                        new WebErrors {Message = Get("General", "InternalServerError")}
                    };
                }
            }
        }

        protected void SetErrorResponse(JSONResponse webResponse, string debugMessage, string message,
            [CallerMemberName] string caller = "")
        {
            Logger.Log(WfsLogLevel.Debug, debugMessage + " - " + caller);
            webResponse.HasErrors = true;
            webResponse.Errors = new List<WebErrors> {new WebErrors {Message = message}};
        }

        protected int GetNewRN(IList<int> taken, Random random)
        {
            var rn = random.Next(1, 75);
            if (taken.Contains(rn))
            {
                rn = GetNewRN(taken, random);
            }
            return rn;
        }


        protected List<PreferenceDto> GetSystemPreferences()
        {
            try
            {
                var response = _configService.GetPreferences();

                if (response.Status != StatusCode.SUCCESS)
                {
                    var sb = new StringBuilder();
                    sb.Append("Exception occurred in GetPreferences").Append(Environment.NewLine);
                    foreach (var error in response.Errors)
                    {
                        sb.Append(error).Append(Environment.NewLine);
                    }
                    Logger.Log(WfsLogLevel.Error, sb.ToString());
                    return null;
                }

                var preferences = response.Data;
                return preferences;
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, "Exception occurred in GetPreferences\r\n" + ex);
                return null;
            }
        }

        protected bool ShowBatchId(int siteClientAccountId, int siteBankId)
        {
            try
            {
                var response = _configService.GetWorkgroupByWorkgroupAndBank(siteClientAccountId, siteBankId,
                    GetWorkgroupAdditionalData.None);
                if (response.Status != StatusCode.SUCCESS)
                {
                    var sb = new StringBuilder();
                    sb.Append("Exception occurred in ShowBatchId").Append(Environment.NewLine);
                    foreach (var error in response.Errors)
                    {
                        sb.Append(error).Append(Environment.NewLine);
                    }
                    Logger.Log(WfsLogLevel.Error, sb.ToString());
                    return false;
                }

                var wg = response.Data;
                if (wg.DisplayBatchID.HasValue)
                {
                    return wg.DisplayBatchID.Value;
                }
                else
                {
                    return wg.EntityID != null && ShowBatchIdEntity(wg.EntityID.Value);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, "Exception occurred in ShowBatchId\r\n" + ex);
            }
            return false;
        }

        protected bool ShowBatchIdEntity(int entityId)
        {
            try
            {
                var response = _configService.GetEntity(entityId);
                if (response.Status != StatusCode.SUCCESS)
                {
                    var sb = new StringBuilder();
                    sb.Append("Exception occurred in ShowBatchIdEntity").Append(Environment.NewLine);
                    foreach (var error in response.Errors)
                    {
                        sb.Append(error).Append(Environment.NewLine);
                    }
                    Logger.Log(WfsLogLevel.Error, sb.ToString());
                    return false;
                }

                var entity = response.Data;
                return entity.DisplayBatchID;
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, "Exception occurred in ShowBatchIdEntity\r\n" + ex);
            }
            return false;
        }

        protected bool GetDisplayPreference(List<PreferenceDto> preferences, string name)
        {
            var pref = preferences.Find(s => s.Name == name);
            if (pref != null)
            {
                return pref.DefaultSetting == "Y";
            }

            return false;
        }

        protected UserPreferencesDTO GetUserPreferences()
        {
            try
            {
                var response = _userService.GetUserPreferences();

                if (response.Status != StatusCode.SUCCESS)
                {
                    var sb = new StringBuilder();
                    sb.Append("Exception occurred in GetUserPreferences").Append(Environment.NewLine);
                    foreach (var error in response.Errors)
                    {
                        sb.Append(error).Append(Environment.NewLine);
                    }
                    Logger.Log(WfsLogLevel.Error, sb.ToString());
                    return null;
                }

                var preferences = response.Data;
                return preferences;
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, "Exception occurred in GetUserPreferences\r\n" + ex);
                return null;
            }
        }
    }
}