﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Mvc;
using AutoMapper;
using HubViews.Models;
using OLDecisioningShared.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;
using Wfs.Logging;
using Wfs.Raam.Core;
using WFS.RecHub.HubConfig;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.R360WebShared.Attributes;
using WFS.RecHub.R360Services.Common;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.R360RaamClient;

namespace HubViews.Controllers
{
    public class DecisioningController : BaseController
    {
        private readonly IExceptionServices _exceptionsService;
        private readonly IRaamClient _raamclient;
        private readonly IConfigurationProvider _mapperconfig;
        private readonly IMapper _mapper;
        private readonly IR360Services _r360services;
        private readonly IContextContainer _contextcontainer;

        private const string AccessedPageUseractivitymessage = "User has accessed the Exceptions page.";
        private const string AuditEvent = "Web Request";
        private const string AuditType = "Page View";
        private const string AppPath = "Decisioning/Index";

        // These are to sanitize the timezone name input, so we don't have HTML injection.
        private const string UtcName = "Coordinated Universal Time";
        private string[] ValidTimeZones = new string[]
        {
            "Atlantic Standard Time",
            "Atlantic Daylight Time",
            "Eastern Standard Time",
            "Eastern Daylight Time",
            "Central Standard Time",
            "Central Daylight Time",
            "Mountain Standard Time",
            "Mountain Daylight Time",
            "Pacific Standard Time",
            "Pacific Daylight Time",
            "Alaska Time",
            "Alaska Daylight Time",
            "Hawaii Standard Time",
            "Hawaii-Aleutian Standard Time",
            "Hawaii-Aleutian Daylight Time",
            "Samoa Standard Time",
            "Samoa Daylight Time",
            "Chamorro Standard Time"
        };

        public DecisioningController(
            IExceptionServices exceptionservices,
            IRaamClient raamclient,
            IR360Services r360services,
            IContextContainer contextcontainer
            )
        {
            _mapperconfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<DecisioningBatchDto, DecisioningBatchModel>()
                    .ForMember(x => x.DepositDate, x => x.MapFrom(source => source.DepositDate.ToShortDateString()))
                    .ForMember(x => x.ProcessDate, x => x.MapFrom(source => source.DepositDate.ToShortDateString()))
                    .ForMember(x => x.DeadLine, x => x.MapFrom(source => source.DeadLine.HasValue ?
                    source.DeadLine.Value.ToString(CultureInfo.InvariantCulture) : string.Empty));
                cfg.CreateMap<DecisioningBatchModel, DecisioningBatchDto>();

                cfg.CreateMap<DecisioningTransactionDto, DecisioningTransactionModel>();
                cfg.CreateMap<DecisioningTransactionModel, DecisioningTransactionDto>();

                cfg.CreateMap<DecisioningPaymentDto, DecisioningPaymentModel>();
                cfg.CreateMap<DecisioningPaymentModel, DecisioningPaymentDto>();

                cfg.CreateMap<DecisioningDataEntryItemFieldDto, DecisioningDataEntryItemModel>();
                cfg.CreateMap<DecisioningDataEntryItemModel, DecisioningDataEntryItemFieldDto>();

                cfg.CreateMap<DecisioningDataEntryDto, DecisioningDataEntryModel>();
                cfg.CreateMap<DecisioningDataEntryModel, DecisioningDataEntryDto>();

                cfg.CreateMap<DecisioningDataEntrySetupDto, DecisioningDataEntrySetupModel>();
                cfg.CreateMap<DecisioningDataEntrySetupModel, DecisioningDataEntrySetupDto>();

                cfg.CreateMap<DecisioningStubDto, DecisioningStubModel>();
                cfg.CreateMap<DecisioningStubModel, DecisioningStubDto>();
                cfg.CreateMap<DecisioningCompleteBatchModel, DecisioningCompleteBatchDto>();
            });
            _mapper = _mapperconfig.CreateMapper();

            _exceptionsService = exceptionservices;
            _r360services = r360services;
            _raamclient = raamclient;
            _contextcontainer = contextcontainer;
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public ActionResult Index()
        {
            IsAuditEventSuppressed = true;
            _R360ServiceManager.WriteAuditEvent("Viewed Page", AuditType, AppPath,
                $"Exceptions Summary page was viewed by {SidToName(CurrentSid)}.");
            _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions", AppPath,
                    $"{SidToName(CurrentSid)} viewed pending exceptions on the Exceptions Summary page.");
            return View();
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public JsonResult GetPendingBatches()
        {
            try
            {
                IsAuditEventSuppressed = true;
                var results = _exceptionsService.GetPendingBatches();
                var message = string.Empty;

                if (results.Status != StatusCode.SUCCESS)
                {
                    message = "There was an error fetching ";
                    if (results.Errors != null && results.Errors.Any())
                        message = results.Errors.First();
                    return Json(new { Success = false, error = message }, JsonRequestBehavior.AllowGet);
                }
                var mappedResults = results.Batches.Select(_mapper.Map<DecisioningBatchModel>).ToArray();
                return Json(new
                {
                    succes = results.Status == StatusCode.SUCCESS,
                    data = mappedResults,
                    error = message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while retrieving Pending Batches." },
                    JsonRequestBehavior.AllowGet);
            }

        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public static TransactionTotals CalculateTransactionTotals(DecisioningTransactionDto transaction)
        {
            return new TransactionTotals()
            {
                PaymentTotal = transaction.Payments.Sum(payment => payment.Amount),
                StubsTotal = transaction.Stubs.Sum(stub => stub.Amount)
            };
        }

        public static bool IsBalanced(DecisioningTransactionDto transaction)
        {
            var totals = CalculateTransactionTotals(transaction);
            return totals.PaymentTotal == totals.StubsTotal;
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public ActionResult Detail(int globalBatchId)
        {
            IsAuditEventSuppressed = true;
            return View(globalBatchId);
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public ActionResult AllTransactionsPrintableView(int globalBatchId, int timezoneoffset, string timezonename)
        {
            IsAuditEventSuppressed = true;
            var result = _exceptionsService.GetBatchDetails(globalBatchId);
            if (result.Errors != null && result.Errors.Any())
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            var mappedResult = _mapper.Map<DecisioningBatchModel>(result.Data);

            // Convert the deadline UTC to the timezone from the browser. Default to UTC if it's not valid.
            if (result?.Data?.DeadLine != null)
            {
                var tzname = ValidTimeZones.Contains(timezonename) ? timezonename : UtcName;
                var offset = ValidTimeZones.Contains(timezonename) ? timezoneoffset : 0;
                var usertime = result.Data.DeadLine.Value.AddMinutes(offset);
                mappedResult.DeadLine = $"{usertime.ToString("hh:mm")} {usertime.ToString("tt").ToLower()} {tzname}";
            }

            _r360services.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions", "Decisioning/Detail",
                $"{SidToName(CurrentSid)} generated the Exception Detail report for Batch: {mappedResult.BatchId}, " +
                $" for Deposit Date: {mappedResult.DepositDate}, Process Date: {mappedResult.ProcessDate} for " +
                $"Bank: {mappedResult.BankId}-{GetBankName(mappedResult.BankId)}, Workgroup: {mappedResult.WorkgroupName} and Payment Source: {mappedResult.PaymentSource}");

            _r360services.WriteAuditEvent("Report Executed", "Reports", "Decisioning/Detail",
                $"The Exception Detail Report was viewed by {SidToName(CurrentSid)} for Batch: {mappedResult.BatchId}, " +
                $" Deposit Date: {mappedResult.DepositDate}, Process Date: {mappedResult.ProcessDate} for " +
                $"Bank: {mappedResult.BankId}-{GetBankName(mappedResult.BankId)}, Workgroup: {mappedResult.WorkgroupName} and Payment Source: {mappedResult.PaymentSource}");

            return View(mappedResult);
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage)]
        public JsonResult LockBatch(int globalBatchId)
        {
            try
            {
                IsAuditEventSuppressed = false;

                var sid = CurrentSid;
                var batchData = _exceptionsService.GetBatch(globalBatchId);
                var mapped = _mapper.Map<DecisioningBatchModel>(batchData.Data);
                if (!CheckIfUserCanLock(mapped.SID))
                {
                    return Json(new { success = false, error = "Cannot lock that batch." });
                }
                var user = SidToUser(sid);
                var person = user.Person;

                var result = _exceptionsService.CheckOutBatch(globalBatchId, sid, person.FirstName, person.LastName, user != null ? user.LoginId : "");
                if (result.Status == StatusCode.SUCCESS)
                {
                    _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions", "Decisioning/Detail",
                        $"{SidToName(sid)} locked Batch: {batchData.Data.BatchId} " +
                        $"for Deposit Date: {batchData.Data.DepositDate.ToShortDateString()}, Process Date: {batchData.Data.ProcessDate.ToShortDateString()} " +
                        $"for Bank: {batchData.Data.BankId}-{GetBankName(batchData.Data.BankId)}, Workgroup: {batchData.Data.WorkgroupName}, " +
                        $"Payment Source: {batchData.Data.PaymentSource} for exception processing");
                }


                return GetExceptionDetail(globalBatchId);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while locking Batch." },
                    JsonRequestBehavior.AllowGet);
            }
        }

        private Guid CurrentSid
        {
            get
            {
                var sid = Guid.Parse(_contextcontainer.Current.RAAMClaims
                    .First(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/sid")
                    .Value);
                return sid;
            }
        }

        public JsonResult UnlockBatch(int globalBatchId)
        {
            try
            {
                IsAuditEventSuppressed = false;
                var batchData = _exceptionsService.GetBatch(globalBatchId);
                var mapped = _mapper.Map<DecisioningBatchModel>(batchData.Data);
                if (!CheckIfUserCanUnlock(mapped.SID))
                {
                    return Json(new { success = false, error = "Cannot unlock that batch." });
                }

                var result = _exceptionsService.ResetBatch(globalBatchId);
                if (result == null || result.Status != StatusCode.SUCCESS)
                {
                    var error = result?.Errors?.FirstOrDefault() ?? "Error unlocking batch.";
                    return Json(new { success = false, error = error });
                }
                else
                {
                    _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions", "Decisioning/Detail",
                        $"{SidToName(CurrentSid)} unlocked Batch: {batchData.Data.BatchId} " +
                        $"for Deposit Date: {batchData.Data.DepositDate.ToShortDateString()}, Process Date: {batchData.Data.ProcessDate.ToShortDateString()} " +
                        $"for Bank: {batchData.Data.BankId}-{GetBankName(batchData.Data.BankId)}, Workgroup: {batchData.Data.WorkgroupName}, " +
                        $"Payment Source: {batchData.Data.PaymentSource} for exception processing");
                }

                return GetExceptionDetail(globalBatchId);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while locking Batch." });
            }
        }

        private string GetBankName(int siteBankId)
        {
            var result = "";
            var bankNameData = _r360services.GetBankName(siteBankId);

            if (bankNameData.Status == StatusCode.SUCCESS)
            {
                result = bankNameData.Data;
            }
            return result;
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public JsonResult GetExceptionDetail(int globalBatchId)
        {
            try
            {
                IsAuditEventSuppressed = true;
                var result = _exceptionsService.GetBatchDetails(globalBatchId);
                //the original transaction collection contains a transaction per row in db
                //since 1.04 we display a transaction for each check in every transaction
                //and if the transaction doesn't have any checks we just display one transaction
                //this is what we'll do here
                var origTransactions = result.Data.Transactions;

                var checkTransactions = new List<DecisioningTransactionDto>();
                foreach (var transaction in origTransactions)
                {
                    if (transaction.Payments.Any())
                    {
                        checkTransactions.AddRange(transaction.Payments.Select(payment => new DecisioningTransactionDto
                        {
                            Payments = transaction.Payments,
                            Stubs = transaction.Stubs,
                            TransactionSequence = transaction.TransactionSequence,
                            TransactionId = transaction.TransactionId,
                            Amount = payment.Amount,
                            RT = payment.RT,
                            Account = payment.Account,
                            CheckTraceRef = payment.Serial,
                            Payer = payment.RemitterName,
                            TransactionStatus = transaction.TransactionStatus
                        }));
                    }
                    else
                    {
                        checkTransactions.Add(transaction);
                    }
                }

                result.Data.Transactions = checkTransactions;
                var mappedResult = _mapper.Map<DecisioningBatchModel>(result.Data);
                var message = string.Empty;
                if (result.Errors != null && result.Errors.Any())
                {
                    message = result.Errors.First();
                    return Json(new { Success = false, error = message }, JsonRequestBehavior.AllowGet);

                }

                // Set the user keying status based on our sid.
                mappedResult.UserCanEdit = R360Permissions.Current.Allowed(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage)
                    && mappedResult.SID == R360ServiceContext.Current.GetSID().ToString();
                mappedResult.UserCanUnlock = CheckIfUserCanUnlock(mappedResult.SID);
                mappedResult.UserCanLock = CheckIfUserCanLock(mappedResult.SID);

                //custom user activity message
                _R360ServiceManager.WriteAuditEvent("Viewed Page", AuditType, "Exceptions Summary/Detail",
                                   $"Exceptions Detail page for Batch {result.Data.BatchId} was viewed by {SidToName(CurrentSid)}");
                _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions", "Exceptions Summary/Detail",
                    $"{SidToName(CurrentSid)} viewed the Exceptions Detail page for Batch: {result.Data.BatchId}" +
                    $" for Deposit Date: {mappedResult.DepositDate}, Process Date: {mappedResult.ProcessDate} for " +
                    $"Bank: {mappedResult.BankId}-{GetBankName(mappedResult.BankId)}, Workgroup: {mappedResult.WorkgroupName} and Payment Source: {mappedResult.PaymentSource}");

                return Json(new
                {
                    success = result.Status == StatusCode.SUCCESS,
                    data = mappedResult,
                    error = message
                });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while retrieving Batch Details." },
                    JsonRequestBehavior.AllowGet);
            }
        }

        private bool CheckIfUserCanLock(string sid)
        {
            // If it's not already locked, and we have manage permissions.
            return string.IsNullOrWhiteSpace(sid) && R360Permissions.Current.Allowed(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage);
        }

        private bool CheckIfUserCanUnlock(string sid)
        {
            // the user owns the batch OR the user can override the batch
            if(sid == R360ServiceContext.Current.GetSID().ToString() && 
                R360Permissions.Current.Allowed(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage))
                return true;
             return R360Permissions.Current.Allowed(R360Permissions.Perm_ExceptionsUnlockOverride,
                 R360Permissions.ActionType.Manage);
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public JsonResult GetSingleTransaction(int globalBatchId, int transactionid)
        {
            try
            {
                IsAuditEventSuppressed = true;
                var result = _exceptionsService.GetTransactionDetails(globalBatchId, transactionid);
                var mappedResult = _mapper.Map<DecisioningTransactionModel>(result.Data);
                var transaction = mappedResult;
                var message = string.Empty;
                if (result.Errors != null && result.Errors.Any())
                {
                    message = result.Errors.First();
                    return Json(new { success = false, error = message }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    success = result.Status == StatusCode.SUCCESS,
                    data = transaction
                });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while retrieving Transaction Exception Details." },
                    JsonRequestBehavior.AllowGet);
            }
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public ActionResult TransactionDetail(DecisioningTransactionRequest request)
        {
            IsAuditEventSuppressed = true;
            return View(request);
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public static bool ValidTransactionBalance(WorkgroupBusinessRulesDto workgroupBusinessRules, DecisioningTransactionDto transaction)
        {
            // if transaction is rejected just do the operation
            if (transaction.TransactionStatus == 4)
            {
                return true;
            }

            return IsBalanced(transaction) || !workgroupBusinessRules.PreDepositBalancingRequired;
        }

        private WorkgroupBusinessRulesDto GetWorkgroupBusinessRules(int workgroupId, int bankId)
        {
            var workgroupResponse = _configService.GetWorkgroupByWorkgroupAndBank(workgroupId, bankId,
                GetWorkgroupAdditionalData.BusinessRules);
            if (workgroupResponse.Status == StatusCode.FAIL || workgroupResponse.Data == null)
            {
                throw new Exception($"Unable to find workgroup: {workgroupId} for bank: {bankId}");
            }
            return workgroupResponse.Data.BusinessRules;
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage)]
        public JsonResult UpdateTransaction(DecisioningTransactionModel request)
        {
            try
            {
                IsAuditEventSuppressed = true;

                var batch = _exceptionsService.GetBatchDetails(request.GlobalBatchId).Data;
                if (batch.SID.ToString() != R360ServiceContext.Current.GetSID().ToString())
                {
                    return Json(new { success = false, error = "Can't Update Transaction." },
                        JsonRequestBehavior.AllowGet);
                }
                var req = _mapper.Map<DecisioningTransactionDto>(request);
                var businessRules = GetWorkgroupBusinessRules(batch.WorkgroupId, batch.BankId);
                if (!ValidTransactionBalance(businessRules, req))
                {
                    return Json(new
                    {
                        success = false,
                        errors = new string[] { },
                        message = "This transaction is out of balance. Click OK to " +
                                          "return to the Exceptions Transaction Detail page " +
                                          "and correct the out of balance condition."
                    });
                }
                var res = _exceptionsService.UpdateTransaction(req);
                if (res.Status == StatusCode.SUCCESS)
                {
                    LogUpdatetransaction(request, batch);
                }

                return Json(new
                {
                    success = res.Data,
                    validations = res.ValidationResults,
                    errors = res.Errors
                });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while updating Transaction Exception Details." },
                    JsonRequestBehavior.AllowGet);
            }
        }

        private void LogUpdatetransaction(DecisioningTransactionModel request, DecisioningBatchDto oldbatch)
        {
            IsAuditEventSuppressed = true;
            // Only auditing here if Accepted or Rejected button is chosen
            if (request.PreviousTransactionStatus != request.TransactionStatus && (request.TransactionStatus == 3 || request.TransactionStatus == 4))
            {
                var action = request.TransactionStatus == 3 ? "accepted" : "rejected";
                _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions",
                    "Exceptions Summary / Detail / Transaction Detail",
                    $"{SidToName(CurrentSid)} {action} Transaction: {request.TransactionId} " +
                    $"for Bank: {oldbatch.BankId}-{GetBankName(oldbatch.BankId)}, Workgroup: {oldbatch.WorkgroupName}, " +
                    $"Batch: {oldbatch.BatchId}.");
            }

            var oldTransaction = oldbatch.Transactions.FirstOrDefault(x => x.TransactionId == request.TransactionId);
            if (oldTransaction == null) return;
            var currentStubs = request.Stubs?.ToList();
            var oldStubs = oldTransaction.Stubs?.ToList();
            if (currentStubs == null || oldStubs == null) return;

            foreach (var oldstub in oldStubs)
            {
                var newStub = currentStubs.FirstOrDefault(x => x.DEItemRowDataID == oldstub.DEItemRowDataID);

                if (newStub == null)
                {

                    _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions",
                        "Exceptions Summary/Detail/Transaction Detail",
                        $"{SidToName(CurrentSid)} deleted a related item to Batch: {oldbatch.BatchId} , Transaction: {oldTransaction.TransactionSequence} " +
                        $"for Deposit Date: {oldbatch.DepositDate.ToString("MM/dd/yyyy")}, Process Date: {oldbatch.ProcessDate.ToString("MM/dd/yyyy")} for Bank: {oldbatch.BankId}-{GetBankName(oldbatch.BankId)}, " +
                        $"Workgroup: {oldbatch.WorkgroupName}, and Payment Source: {oldbatch.PaymentSource} " +
                        $"for an Amount of ${string.Format("{0:0.00}", oldstub.Amount)}");

                }
            }

            foreach (var stub in currentStubs)
            {
                var oldStub = oldStubs.FirstOrDefault(x => x.DEItemRowDataID == stub.DEItemRowDataID);

                if (oldStub == null)
                {

                    _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions",
                        "Exceptions Summary/Detail/Transaction Detail",
                        $"{SidToName(CurrentSid)} added a related item to Batch: {oldbatch.BatchId} , Transaction: {oldTransaction.TransactionSequence} " +
                        $"for Deposit Date: {oldbatch.DepositDate.ToString("MM/dd/yyyy")}, Process Date: {oldbatch.ProcessDate.ToString("MM/dd/yyyy")} for Bank: {oldbatch.BankId}-{GetBankName(oldbatch.BankId)}, " +
                        $"Workgroup: {oldbatch.WorkgroupName}, and Payment Source: {oldbatch.PaymentSource} " +
                        $"for an Amount of ${string.Format("{0:0.00}", stub.Amount)}");

                }
                else if (stub.DataEntry != null)
                {
                    foreach (var deField in stub.DataEntry)
                    {
                        var oldDe =
                            oldStub.DataEntry.FirstOrDefault(x => x.DEItemFieldDataID == deField.DEItemFieldDataID);

                        if (oldDe != null && oldDe.Value != deField.Value)
                        {
                            _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions",
                                "Exceptions Summary/Detail/Transaction Detail",
                            $"{SidToName(CurrentSid)} keyed an exception for Bank: {oldbatch.BankId}-{GetBankName(oldbatch.BankId)}, " +
                                $"Workgroup: {oldbatch.WorkgroupName}, Batch: {oldbatch.BatchId}, Transaction: {oldTransaction.TransactionSequence}. " +
                                $"{deField.FieldName} was changed from {oldDe.Value} to { deField.Value}.");
                        }
                    }
                }
            }
        }

        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View)]
        public JsonResult GetTransactionDetail(DecisioningTransactionRequest request)
        {
            try
            {
                IsAuditEventSuppressed = true;
                var transaction = _exceptionsService.GetTransactionDetails(request.GlobalBatchId, request.TransactionId);
                var mappedResult = _mapper.Map<DecisioningTransactionModel>(transaction.Data);
                if (transaction.Errors != null && transaction.Errors.Any())
                {
                    var message = transaction.Errors.First();
                    return Json(new { Success = false, error = message }, JsonRequestBehavior.AllowGet);
                }
                mappedResult.ParentBatch.UserCanEdit = R360Permissions.Current.Allowed(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage)
                    && mappedResult.ParentBatch.SID == R360ServiceContext.Current.GetSID().ToString();

                _R360ServiceManager.WriteAuditEvent("Viewed Page", AuditType, "Exceptions Summary/Detail/Transaction Detail",
                    $"Exceptions Transaction Detail page for Batch: {mappedResult.ParentBatch.BatchId}, Transaction: {mappedResult.TransactionSequence}" +
                    $" was viewed by {SidToName(CurrentSid)}");
                _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions", "Exceptions Summary/Detail/Transaction Detail",
                    $"{SidToName(CurrentSid)} viewed the Exceptions Transaction Detail page for Batch: {mappedResult.ParentBatch.BatchId}, Transaction: {mappedResult.TransactionSequence}" +
                    $" for Deposit Date: {mappedResult.ParentBatch.DepositDate}, Process Date: {mappedResult.ParentBatch.ProcessDate} for " +
                    $"Bank: {mappedResult.ParentBatch.BankId}-{GetBankName(mappedResult.ParentBatch.BankId)}, Workgroup: {mappedResult.ParentBatch.WorkgroupName} and Payment Source: {mappedResult.ParentBatch.PaymentSource}");


                // Map the Title from the FieldName if Title is missing, so it show up on te 
                // Stubs Grid as a header and in the error messagews
                foreach (var parentDE in mappedResult.ParentBatch.DESetupList)
                {
                    parentDE.Title = parentDE.Title.Trim() == "" ? parentDE.FieldName : parentDE.Title;
                }

                // Map the Title from the FieldName if Title is missing
                foreach (var stub in mappedResult.Stubs)
                {
                    foreach (var de in stub.DataEntry)
                    {
                        
                        de.Title = de.Title.Trim() == "" ? de.FieldName : de.Title;
                    }
                }

                return Json(new
                {
                    success = transaction.Status == StatusCode.SUCCESS,
                    data = mappedResult,
                });

            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while retrieving Transaction Details." },
                    JsonRequestBehavior.AllowGet);
            }
        }
        
        [RequiresPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage)]
        public JsonResult CompleteBatch(DecisioningCompleteBatchModel model)
        {
            try
            {
                IsAuditEventSuppressed = true;
                var dto = _mapper.Map<DecisioningCompleteBatchDto>(model);
                var complete = _exceptionsService.CompleteBatch(dto);
                var batchDetails = _exceptionsService.GetBatch(dto.globalBatchId).Data;
                var errors = string.Empty;
                if (complete.Errors != null && complete.Errors.Any()) { errors = complete.Errors.First(); }
                if (complete.Status == StatusCode.SUCCESS)
                {
                    _R360ServiceManager.WriteAuditEvent("Pre-Deposit Exceptions", "Pre-Deposit Exceptions", "Exceptions Summary/Detail",
                    $"{SidToName(CurrentSid)} submitted Batch: {batchDetails.BatchId} from the Exceptions Detail page " +
                    $" for Deposit Date: {batchDetails.DepositDate.ToShortDateString()}, Process Date: {batchDetails.ProcessDate.ToShortDateString()} for " +
                    $"Bank: {batchDetails.BankId}-{GetBankName(batchDetails.BankId)}, Workgroup: {batchDetails.WorkgroupName} and Payment Source: {batchDetails.PaymentSource}");
                }
                return Json(new
                {
                    success = complete.Status == StatusCode.SUCCESS,
                    data = complete.Data,
                    error = errors
                });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { success = false, error = "An error occurred while Completing a batch." },
                    JsonRequestBehavior.AllowGet);
            }

        }

        private string SidToName(Guid? sid)
        {
            var user = SidToUser(sid);

            return $"{user.Person.FirstName} {user.Person.LastName}".Trim();
        }

        private UserDto SidToUser(Guid? sid)
        {
            var result = new UserDto()
            {
                LoginId = "",
                UserSID = Guid.Empty,
                Person = new PersonDto()
                {
                    FirstName = "",
                    LastName = ""
                }
            };

            if (sid != null)
            {
                result = _raamclient.GetUserByUserSID(sid.Value);
            }
            return result;
        }
    }
}