using System.Web.Mvc;

namespace HubViews.Controllers
{
    /// <summary>
    /// When parsing JSON in controller classes, parse empty strings as actual empty strings.
    /// I.e., suppress MVC's default behavior of converting empty strings to nulls (!!).
    /// </summary>
    /// <remarks>
    /// Source: https://stackoverflow.com/a/12734370/87399
    /// </remarks>
    public class AllowEmptyStringsModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            bindingContext.ModelMetadata.ConvertEmptyStringToNull = false;
            Binders = new ModelBinderDictionary {DefaultBinder = this};
            return base.BindModel(controllerContext, bindingContext);
        }
    }
}
