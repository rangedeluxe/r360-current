﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Mvc;
using System.Xml;
using CommonUtils.Web;
using HubViews.Models;
using Wfs.Logging;
using WFS.RecHub.Common;
using WFS.RecHub.OLFServices;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360WebShared.Attributes;
using System.Net;
using System.Web;
using WFS.RecHub.HubConfig;
using WFS.RecHub.OlfRequestCommon;
using System.Threading.Tasks;

namespace HubViews.Controllers
{
    public class ImageController : BaseController
    {

        private readonly IWfsLog Logger;
        private readonly IImageServices _imageservice;
        private readonly IReportServices _reportservice;
        private readonly IOLFOnlineClient _olfclient;
        private readonly IHubConfig _hubconfigservice;
        private readonly IContextContainer _contextcontainer;
        private readonly IUserServices _userservice;

        private readonly IXmlTransformer<AdvancedSearchRequestDto> _xmlTransformer;
        private readonly string _sitekey;

        public ImageController(IImageServices imageservice,
            IReportServices reportservice,
            IOLFOnlineClient olfclient,
            IHubConfig hubconfigclient,
            IContextContainer contextcontainer,
            IUserServices userservice,
            IWfsLog logger)
        {
            _sitekey = "IPOnline";
            _imageservice = imageservice;
            _reportservice = reportservice;
            _olfclient = olfclient;
            _hubconfigservice = hubconfigclient;
            _contextcontainer = contextcontainer;
            _userservice = userservice;
            Logger = logger;
            _xmlTransformer = new AdvancedSearchRequestXmlTransformer();
        }

        // GET: Image
        public ActionResult Index([Bind(Include = "WorkgroupId, BankId, DepositDateString," +
                                                   "ProcessingDateString, BatchId, BatchNumber," +
                                                   "PICSDate, TxnSequence,TransactionId, " +
                                                   "BatchSequence, ImportTypeKey, Page," +
                                                   "ImageFilterOption, ReportName, ImportTypeShortName")] ImageViewModel model)
        {

            // Get the System Preference so we can decide Headers to show
            var preferences = _hubconfigservice.GetPreferences().Data;

            var metaData = new FileMetaData
            {
                SiteKey = _sitekey,
                SessionID = _contextcontainer.Current.GetSessionID().ToString(),
                EmulationID = Guid.NewGuid().ToString()
            };

            var picDate = 0;
            if (model.PICSDate != null)
            {
                picDate = int.Parse(model.PICSDate);
            }

            var displayPayer = GetDisplayPreference(preferences, "DisplayRemitterNameInPDF");

            var userPrefs = _userservice.GetUserPreferences().Data;

            if (userPrefs != null && userPrefs.displayRemitterNameinPdf.HasValue)
            {
                displayPayer = userPrefs.displayRemitterNameinPdf.Value;
            }

            var wg = _hubconfigservice.GetWorkgroupByWorkgroupAndBank(model.WorkgroupId.Value, model.BankId.Value,
                GetWorkgroupAdditionalData.None).Data;
            var ent = _hubconfigservice.GetEntity(wg.EntityID.Value).Data;
            var showbatchid = wg.DisplayBatchID.HasValue
                ? wg.DisplayBatchID.Value
                : ent.DisplayBatchID;

            var request = new ImageRequestDto()
            {
                IsCheck = model.ImportTypeKey == 3,
                BankId = model.BankId ?? 0,
                BatchId = model.BatchId ?? 0,
                BatchSequence = model.BatchSequence ?? 0,
                DepositDate = Convert.ToDateTime(model.DepositDateString, CultureInfo.CurrentCulture),
                TransactionId = model.TxnSequence ?? 0,
                WorkgroupId = model.WorkgroupId ?? 0,
                PicsDate = picDate,
                SiteKey = _sitekey,
                DisplayRemitterName = displayPayer,
                DisplayBatchId = showbatchid,
                SessionId = _contextcontainer.Current.GetSessionID(),
                WorkgroupName = wg.LongName,
                ImportTypeShortName =  model.ImportTypeShortName
                
            };
            var response = _olfclient.DownloadImage(metaData, request);

            if (response.FrontImageData == null)
            {
                var errorResp = new JSONResponse
                {
                    Data = response.IsSuccessful,
                    HasErrors = true,
                    Errors = new List<WebErrors> { new WebErrors { Message = "Image could not be retrieved. " } }
                };

                return View(errorResp);
            }

            // Set some headers.
            Response.AppendHeader("Content-Disposition", "inline;test.pdf");

            return File(response.FrontImageData, "application/pdf");
        }

        /// <summary>
        /// This instructs the JS to load the request from localstorage. Specifically for things that require a large amount of data.
        /// </summary>
        /// <returns></returns>
        public ActionResult LoadFromLocal()
        {
            return View();
        }

        [Authorize]
        public JsonResult GetSingleImage(SingleImageModel model)
        {
            var imageDto = new SingleImageRequestDto
            {
                BatchId = model.BatchId,
                BatchSequence = model.BatchSequence,
                DepositDate = DateTime.Parse(model.DepositDateString),
                IsCheck = model.IsCheck,
                DisplayMode = 0,
                SessionId = _contextcontainer.Current.GetSessionID(),
                SiteKey = _sitekey
            };
            var response = _olfclient.DownloadSingleImage(imageDto);
            if (response.FrontImageData == null || !response.IsSuccessful)
            {
                return Json(new
                {
                    success = response.IsSuccessful,
                    message = response.Messages.FirstOrDefault()
                });
            }
            return Json(new
            {
                success = response.IsSuccessful,
                frontData = Convert.ToBase64String(response.FrontImageData),
                backData = response.BackImageData != null
                    ? Convert.ToBase64String(response.BackImageData)
                    : null,
                message = response.Messages
            });
        }

        [Authorize]
        public ActionResult LoadInstance(Guid instanceId)
        {
            return View("LoadFromLocal", new { InstanceId = instanceId });
        }

        [Authorize]
        public JsonResult CheckRequestStatus([Bind(Include = "InstanceId,DeliveryMethod,NumberOfTries,JobType")] ImageRequestModel model)
        {
            try
            {
                DataTable dataTable = null;
                var errorMessage = "";
                var status = 0;

                var datSet = _imageservice.CheckImageJobStatus(model.InstanceId);
                dataTable = datSet.Data.Tables[0];
                var dataRow = dataTable.Rows[0];
                status = (int)dataRow["ProcessStatusID"];

                if (datSet.Status != StatusCode.SUCCESS)
                {
                    errorMessage = dataRow["ProcessStatusText"].ToString();
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    var errorResp = new JSONResponse
                    {
                        Data = status,
                        HasErrors = true,
                        Errors = new List<WebErrors> { new WebErrors { Message = errorMessage } }
                    };
                    return Json(errorResp);
                }

                var resp = new JSONResponse
                {
                    Data = status,
                    HasErrors = false,
                    Errors = null
                };
                return Json(resp);
            }
            catch(Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, ex.Message);
                return Json(new { Errors = new List<WebErrors> { new WebErrors { Message = ex.Message } } });
            }
        }


        private string CheckErrorStatus(DataRow row)
        {
            var status = (int)row["ProcessStatusID"];

            if (status == 1) // still processing 
            {
                return "";
            }
            if (status != 2) // some Error encountered
            {
                return row["ProcessStatusText"].ToString();
            }
            return "";
        }

        [Authorize]
        public ActionResult GetRequestInstance(Guid instanceId, string reportName = "Images")
        {
            var instance = _imageservice.GetImageJobFileAsBuffered(instanceId);

            // use a whitelist of acceptable characters to prevent header manipulation attacks
            Regex regEx = new Regex("[^a-zA-Z0-9- _.{}]+");
            reportName = regEx.Replace(reportName, "");

            // Kick out early if failure.
            if (instance.Errors.Count > 0)
            {
                var errorResp = new JSONResponse
                {
                    Data = instance.Status,
                    HasErrors = true,
                    Errors = new List<WebErrors> { new WebErrors { Message = instance.Errors.FirstOrDefault() } }
                };

                return View("Index", errorResp);
            }

            // sometimes the image isnt totally ready on the first call
            // keep trying until the image is returned or until 30 seconds expires
            var attempts = 0;
            while (instance.Data == null)
            {
                attempts++;
                Thread.Sleep(TimeSpan.FromSeconds(1));
                instance = _imageservice.GetImageJobFileAsBuffered(instanceId);
                if (attempts > 30) break;
            }

            if (instance.Data == null)
            {
                var errorResp = new JSONResponse
                {
                    Data = instance.Status,
                    HasErrors = true,
                    Errors = new List<WebErrors> { new WebErrors { Message = "Image could not be retrieved. " } }
                };

                return View("Index", errorResp);
            }

            // Return the data.
            Response.AppendHeader("Content-Disposition", "inline;filename=" + reportName);
            var mimeType = "application/pdf";
            return File(instance.Data, mimeType);
        }

        public ActionResult DownloadInstance(Guid instanceId)
        {
            try
            {
                var size = _olfclient.GetImageJobSize(new FileMetaData { SiteKey = this._sitekey }, instanceId);
                var instance = _olfclient.GetImageJobFileAsAttachment(instanceId);

                if (instance == null)
                {
                    var errorResp = new JSONResponse
                    {
                        Data = null,
                        HasErrors = true,
                        Errors = new List<WebErrors> { new WebErrors { Message = "Download could not be retrieved. " } }
                    };

                    return View("Index", errorResp);
                }

                TransmitFile(instance, "application/zip, application/octet-stream", "search_results.zip", size);

                // Response already handled.
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch(Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }

        [RequiresPermission(R360Permissions.Perm_AlertDownload, R360Permissions.ActionType.View)]
        public ActionResult DownloadCENDSFile(string path)
        {
            try
            {
                var size = _olfclient.GetCENDSFileSize(new FileMetaData() { SiteKey = _sitekey }, path);
                if (size <= 0)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                var stream = _olfclient.GetCENDSFileAsAttachment(path);
                if (stream == null)
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);

                var contentType = MimeMapping.GetMimeMapping(path);
                var filename = Path.GetFileName(path);

                TransmitFile(stream, contentType, filename, size);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch(Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        [Authorize]
        public JsonResult ImagesExist(ImageViewModel model)
        {
            try
            {
                var picsDate = 0;
                if (model.PICSDate != null)
                {
                    picsDate = int.Parse(model.PICSDate);
                }

                var imageDto = new ImageRequestDto()
                {
                    BatchId = model.BatchId ?? 0L,
                    SourceBatchId = model.SourceBatchId ?? 0L,
                    BankId = model.BankId ?? 0,
                    WorkgroupId = model.WorkgroupId ?? 0,
                    TransactionId = model.TransactionId ?? -1,
                    PicsDate = picsDate,
                    BatchPaymentSource = model.BatchPaymentSource,
                    DepositDate = DateTime.Parse(model.DepositDateString),
                    ImportTypeShortName = model.ImportTypeShortName ?? string.Empty,
                    SiteKey = _sitekey//,
                    //FileGroup = model.FileGroup
                };
                //R360-9330 - Image Icons Aren't Appearing when ImageStorageTyp is set to 3 on rechub.ini file FileGroup in dto
                var response = _olfclient.ImagesExist(imageDto);

                var jsonReponse = new JSONResponse
                {
                    Data = response,
                    HasErrors = false,
                    Errors = null
                };
                return Json(jsonReponse);
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new JSONResponse
                {
                    HasErrors = true,
                    Errors = new List<WebErrors> { new WebErrors { Message = e.Message } }
                });
            }
        }

        private void TransmitFile(Stream data, string mimetype, string filename, long size)
        {
            // Buffer to read 5K bytes in chunk:
            var bufferlength = 5120;
            var totalread = 0L;
            byte[] buffer = new Byte[bufferlength];

            try
            {
                // Total bytes to read:
                int bytesread;

                Response.Clear();
                Response.ContentType = mimetype;
                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
                Response.AddHeader("Content-Length", size.ToString());

                while (totalread < size)
                {
                    // Read the data in buffer.
                    bytesread = data.Read(buffer, 0, bufferlength);
                    totalread += bytesread;

                    // Write the data to the current output stream.
                    Response.OutputStream.Write(buffer, 0, bytesread);

                    // Flush the data to the output.
                    Response.Flush();
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (data != null)
                {
                    data.Close();
                    data.Dispose();
                }
                Response.Close();
            }
        }

        public ActionResult AllImages([Bind(Include = "WorkgroupId, BankId, DepositDateString," +
                                                   "ProcessingDateString, BatchId, BatchNumber," +
                                                   "PICSDate, TxnSequence,TransactionId, " +
                                                   "BatchSequence, ImportTypeKey, Page," +
                                                   "ImageFilterOption, ReportName")]ImageViewModel model)
        {
            BaseGenericResponse<DataSet> respdoc = null;
            ImageResponseModel response = null;

            try
            {
                // Build Request Xml
                var objXML = GetRequestDoc();

                ipoXmlLib.addElement(objXML.DocumentElement, "BankID", model.BankId.ToString());
                ipoXmlLib.addElement(objXML.DocumentElement, "LockboxID", model.WorkgroupId.ToString());
                ipoXmlLib.addElement(objXML.DocumentElement, "BatchID", model.BatchId.ToString());
                ipoXmlLib.addElement(objXML.DocumentElement, "DepositDate", model.DepositDateString);
                ipoXmlLib.addElement(objXML.DocumentElement, "BatchNumber", model.BatchNumber);
                if (model.Page.HasValue)
                {
                    ipoXmlLib.addElement(objXML.DocumentElement, "BatchSequence", model.BatchSequence.ToString());
                    ipoXmlLib.addElement(objXML.DocumentElement, "Page", model.Page.ToString());
                    ipoXmlLib.addElement(objXML.DocumentElement, "TransactionID", model.TransactionId.ToString());
                    ipoXmlLib.addElement(objXML.DocumentElement, "TxnSequence", model.TxnSequence.ToString());
                    ipoXmlLib.addElement(objXML.DocumentElement, "ImageFilterOption", model.ImageFilterOption);
                }

                var nodeJobRequestOptions = ipoXmlLib.addElement(objXML.DocumentElement, "jobRequestOptions", "");
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "returnType", "IPO_IMAGE_JOB_TYPE_VIEW_SELECTED");

                var nodeSelectedItems = ipoXmlLib.addElement(objXML.DocumentElement, "SelectedItems", "");
                ipoXmlLib.addAttribute(nodeSelectedItems, "ImageFilterOption", model.ImageFilterOption);

                respdoc = _imageservice.CreateViewAllJobRequest(objXML);

                var row = respdoc.Data.Tables[0].Rows[0];
                response = new ImageResponseModel();
                response.InstanceId = (Guid)row["OnlineImageQueueID"];
                respdoc.Data.Dispose();
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, ex.Message);
                if (response != null)
                {
                    response.HasErrors = true;
                    response.ErrorMessage = ex.Message;
                }
            }

            return View("Index", response);
        }

        public ActionResult Report([Bind(Include = "WorkgroupId, BankId, DepositDateString," +
                                                   "ProcessingDateString, BatchId, BatchNumber," +
                                                   "PICSDate, TxnSequence,TransactionId, " +
                                                   "BatchSequence, ImportTypeKey, Page," +
                                                   "ImageFilterOption, ReportName")] ImageViewModel model)
        {
            BaseGenericResponse<DataSet> respdoc = null;
            ImageResponseModel response = null;

            try
            {
                // Build Request Xml
                XmlDocument objXml;
                objXml = new XmlDocument();

                XmlNode nodeRoot = objXml.CreateElement("Root");
                XmlNode nodeReports = objXml.CreateElement("Reports");
                XmlNode nodeReport = objXml.CreateElement("Report");

                ipoXmlLib.addElement(nodeReport, "ReportName", model.ReportName);
                ipoXmlLib.addAttribute(nodeReport, "BankID", model.BankId.ToString());
                ipoXmlLib.addAttribute(nodeReport, "LockboxID", model.WorkgroupId.ToString());
                ipoXmlLib.addAttribute(nodeReport, "BatchID", model.BatchId.ToString());
                ipoXmlLib.addAttribute(nodeReport, "DepositDate", model.DepositDateString);
                ipoXmlLib.addAttribute(nodeReport, "BatchNumber", model.BatchNumber);
                ipoXmlLib.addAttribute(nodeReport, "BatchSequence", model.BatchSequence.ToString());
                ipoXmlLib.addAttribute(nodeReport, "TransactionID", model.TransactionId.ToString());
                ipoXmlLib.addAttribute(nodeReport, "TxnSequence", model.TxnSequence.ToString());
                ipoXmlLib.addAttribute(nodeReport, "ProcessingDate", model.ProcessingDateString);
                ipoXmlLib.addAttribute(nodeReport, "PICSDate", model.PICSDate);

                nodeReports.AppendChild(nodeReport);
                nodeRoot.AppendChild(nodeReports);
                objXml.AppendChild(nodeRoot);

                respdoc = _reportservice.CreateReportJobRequest(objXml);

                var row = respdoc.Data.Tables[0].Rows[0];
                response = new ImageResponseModel();
                response.InstanceId = (Guid)row["OnlineImageQueueID"];
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, ex.Message);
                if (response != null)
                {
                    response.HasErrors = true;
                    response.ErrorMessage = ex.Message;
                }
            }

            return View("Index", response);
        }

        [HttpPost]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearchDownload, R360Permissions.ActionType.View)]
        public ActionResult AdvancedSearchDownloadRequest(AdvancedSearchRequestModel model)
        {
            BaseGenericResponse<DataSet> respdoc = null;
            ImageResponseModel response = null;
            try
            {
                // The download XML request.
                var docReqXml = GetRequestDoc();
                var nodeJobRequestOptions = ipoXmlLib.addElement(docReqXml.DocumentElement, "jobRequestOptions");
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "returnType", "ZIP");
                //ipoXmlLib.addAttribute(nodeJobRequestOptions, "checkImageDisplayMode", "ZIP");
                //ipoXmlLib.addAttribute(nodeJobRequestOptions, "documentImageDisplayMode", "ZIP");
                var nodeoutputs = ipoXmlLib.addElement(nodeJobRequestOptions, "outputs");
                var nodeoutput = ipoXmlLib.addElement(nodeoutputs, "output");
                nodeoutput.SetAttribute("outputFormat", "TEXT");
                nodeoutput = ipoXmlLib.addElement(nodeoutput, "textOptions");
                nodeoutput.SetAttribute("fileExtension", "csv");
                nodeoutput = ipoXmlLib.addElement(nodeoutputs, "output");
                nodeoutput.SetAttribute("outputFormat", "HTML");
                nodeoutput = ipoXmlLib.addElement(nodeoutputs, "output");
                nodeoutput.SetAttribute("outputFormat", "XML");

                // The AS search XML request.
                var dto = PopulateServiceRequestObject(model);
                dto.SessionId = _contextcontainer.Current.GetSessionID();
                dto.Paginate = false;
                var authorizedworkgroups = _userservice.GetUserLockboxes().Data;
                var requestedworkgroup = authorizedworkgroups.FirstOrDefault(
                    x => x.Value.BankID == dto.BankId && x.Value.LockboxID == dto.WorkgroupId);
                dto.DisplayBatchID = requestedworkgroup.Value.DisplayBatchID;
                var xml = _xmlTransformer.ToXML(dto);

                respdoc = _imageservice.CreateSearchResultsJobRequest(xml, docReqXml);
                var row = respdoc.Data.Tables[0].Rows[0];
                response = new ImageResponseModel();
                return Json(new { InstanceId = (Guid)row["OnlineImageQueueID"], Delivery = "Download" });
            }
            catch (Exception e)
            {
                return Json(new { Error = e.Message });
            }
        }

        [Authorize]
        public ActionResult AdvancedSearchPDFSelectedRequest(AdvancedSearchSelectedRequest model)
        {
            try
            {
                // Converting the model into the XML request object.
                var nodeservicerequest = GetRequestDoc();

                var nodeselecteditems = ipoXmlLib.addElement(nodeservicerequest.DocumentElement, "SelectedItems");
                nodeselecteditems.SetAttribute("ImageFilterOption", model.SelectMode);
                foreach (var t in model.Transactions)
                {
                    var nodet = ipoXmlLib.addElement(nodeselecteditems, "SelectedItem");
                    nodet.SetAttribute("BankID", t.BankId.ToString());
                    nodet.SetAttribute("ClientAccountID", t.WorkgroupId.ToString());
                    nodet.SetAttribute("BatchID", t.BatchId.ToString());
                    nodet.SetAttribute("DepositDate", t.DepositDate);
                    nodet.SetAttribute("TransactionID", t.TransactionId.ToString());
                    nodet.SetAttribute("BatchNumber", t.BatchNumber.ToString());
                }

                var noderequestoptions = ipoXmlLib.addElement(nodeservicerequest.DocumentElement, "jobRequestOptions");
                noderequestoptions.SetAttribute("returnType", "IPO_IMAGE_JOB_TYPE_VIEW_SELECTED");

                // Send the request off to the Image Service.
                var result = _imageservice.CreateViewSelectedJobRequest(nodeservicerequest);
                var row = result.Data.Tables[0].Rows[0];
                var response = new ImageResponseModel();
                response.InstanceId = (Guid)row["OnlineImageQueueID"];
                return Json(response);

            }
            catch(Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        [Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearchDownload, R360Permissions.ActionType.View)]
        public ActionResult AdvancedSearchPDF(AdvancedSearchRequestModel model)
        {
            BaseGenericResponse<DataSet> respdoc = null;
            ImageResponseModel response = null;
            try
            {
                // Build Request Xml
                var docReqXml = GetRequestDoc();
                var nodeJobRequestOptions = ipoXmlLib.addElement(docReqXml.DocumentElement, "jobRequestOptions", "");
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "returnType", "PDF");
                
                
                // Turn the model into a dto so we can transform it into xml for olf
                var dto = PopulateServiceRequestObject(model);
                dto.DisplayBatchID = false;
                dto.SessionId = _contextcontainer.Current.GetSessionID();
                dto.Paginate = false;
                var xml = _xmlTransformer.ToXML(dto);
                respdoc = _imageservice.CreateSearchResultsJobRequest(xml, docReqXml);
                
                var row = respdoc.Data.Tables[0].Rows[0];
                response = new ImageResponseModel();
                response.InstanceId = (Guid)row["OnlineImageQueueID"];
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                if (response != null)
                {
                    response.HasErrors = true;
                    response.ErrorMessage = e.Message;
                }
            }
            return View("Index", response);
        }

        /// <summary>
        /// Same thing as the previous, but just returns the data and not a view.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [RequiresPermission(R360Permissions.Perm_AdvanceSearchDownload, R360Permissions.ActionType.View)]
        [HttpPost]
        public JsonResult AdvancedSearchPDFRequest(AdvancedSearchRequestModel model)
        {
            BaseGenericResponse<DataSet> respdoc = null;
            ImageResponseModel response = null;
            try
            {
                // Build Request Xml
                var docReqXml = GetRequestDoc();
                var nodeJobRequestOptions = ipoXmlLib.addElement(docReqXml.DocumentElement, "jobRequestOptions", "");
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "returnType", "PDF");
                // Turn the model into a dto
                var dto = PopulateServiceRequestObject(model);
                dto.SessionId = _contextcontainer.Current.GetSessionID();
                dto.Paginate = false;
                var authorizedworkgroups = _userservice.GetUserLockboxes().Data;
                var requestedworkgroup = authorizedworkgroups.FirstOrDefault(
                    x => x.Value.BankID == dto.BankId && x.Value.LockboxID == dto.WorkgroupId);
                dto.DisplayBatchID = requestedworkgroup.Value.DisplayBatchID;
                var xml = _xmlTransformer.ToXML(dto);
                respdoc = _imageservice.CreateSearchResultsJobRequest(xml, docReqXml);

                var row = respdoc.Data.Tables[0].Rows[0];
                response = new ImageResponseModel();
                return Json(new { InstanceId = (Guid)row["OnlineImageQueueID"] });
            }
            catch (Exception e)
            {
                return Json(new { Error = e.Message });
            }
        }

        public JsonResult InProcessExceptionImages(InProcessExceptionImageRequestDto request)
            // public JsonResult InProcessExceptionImages(
            //[Bind(Include = "Text")] InProcessExceptionImageRequestDto request)
        {
            var respdoc = _imageservice.CreateInProcessExceptionJobRequest(request);

            var row = respdoc.Data.Tables[0].Rows[0];
            return Json(new {InstanceId = (Guid) row["OnlineImageQueueID"]});
        }

        private AdvancedSearchRequestDto PopulateServiceRequestObject(AdvancedSearchRequestModel model)
        {
            var req = new AdvancedSearchRequestDto();

            req.BankId = int.Parse(model.Workgroup.Split('|')[0]);
            req.WorkgroupId = int.Parse(model.Workgroup.Split('|')[1]);
            req.DepositDateFrom = DateTime.Parse(model.DateFrom);
            req.DepositDateTo = DateTime.Parse(model.DateTo);
            req.StartRecord = model.start;
            req.RecordsPerPage = model.length;
            req.SortBy = model.OrderBy;
            req.SortByDir = model.OrderDirection;
            req.SortByDisplayName = model.OrderByDisplayName;
            req.SelectFields = model.SelectFields != null
                ? model.SelectFields.Where(x => x != null).ToList()
                : null;

            req.PaymentSourceKey = model.PaymentSourceKey;
            req.PaymentTypeKey = model.PaymentTypeKey;
            req.Paginate = false;

            // A quick flip if they happen to be out of order.
            if (req.DepositDateFrom > req.DepositDateTo)
            {
                var datetemp = req.DepositDateFrom;
                req.DepositDateFrom = req.DepositDateTo;
                req.DepositDateTo = datetemp;
            }

            // Put together the whereclauses.
            var whereclauses = new List<AdvancedSearchWhereClauseDto>();
            if (model.Criteria == null)
                model.Criteria = new List<AdvancedSearchCriteria>();
            foreach (var w in model.Criteria.Where(x => x != null))
            {
                w.Column.Value = w.Value;
                w.Column.Operator = w.Operator;
                whereclauses.Add(w.Column);
            }
            req.WhereClauses = whereclauses;

            // Handle the standard fields.
            var whereaddfields = new List<AdvancedSearchWhereClauseDto>();
            var whereremovefields = new List<AdvancedSearchWhereClauseDto>();
            foreach (var w in req.WhereClauses.Where(x => x.IsStandard))
            {
                if (w.Operator == AdvancedSearchWhereClauseOperator.IsGreaterThan)
                    w.StandardXmlColumnName += "From";
                else if (w.Operator == AdvancedSearchWhereClauseOperator.IsLessThan)
                    w.StandardXmlColumnName += "To";
                else if (w.Operator == AdvancedSearchWhereClauseOperator.Equals)
                {
                    // If we have an equals, we need to set both the 'From' and the 'To';
                    whereaddfields.Add(new AdvancedSearchWhereClauseDto()
                    {
                        StandardXmlColumnName = w.StandardXmlColumnName + "From",
                        IsStandard = w.IsStandard,
                        ReportTitle = w.ReportTitle,
                        DataType = w.DataType,
                        Value = w.Value,
                        Operator = w.Operator
                    });
                    whereaddfields.Add(new AdvancedSearchWhereClauseDto()
                    {
                        StandardXmlColumnName = w.StandardXmlColumnName + "To",
                        IsStandard = w.IsStandard,
                        ReportTitle = w.ReportTitle,
                        DataType = w.DataType,
                        Value = w.Value,
                        Operator = w.Operator
                    });
                    whereremovefields.Add(w);
                }
            }
            req.WhereClauses.RemoveAll(x => whereremovefields.Any(
                y => y.Table == x.Table
                && y.DataType == x.DataType
                && y.ReportTitle == x.ReportTitle));
            req.WhereClauses.AddRange(whereaddfields);

            return req;
        }

        protected static XmlDocument GetRequestDoc()
        {
            return GetRequestDoc("Root");
        }

        protected static XmlDocument GetRequestDoc(string rootNodeName)
        {
            var docReqXml = new XmlDocument();
            var nodeRoot = docReqXml.CreateNode(XmlNodeType.Element, rootNodeName, docReqXml.NamespaceURI);
            docReqXml.AppendChild(nodeRoot);

            ipoXmlLib.addAttribute(docReqXml.DocumentElement, "ReqID", Guid.NewGuid().ToString());
            ipoXmlLib.addAttribute(docReqXml.DocumentElement, "ReqDateTime",
                DateTime.Now.ToString(CultureInfo.CurrentCulture));

            return docReqXml;
        }
    }
}