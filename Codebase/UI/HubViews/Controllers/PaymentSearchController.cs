﻿using AutoMapper;
using HubViews.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wfs.Logging;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Controllers
{
    public class PaymentSearchController : BaseController
    {
        private readonly IConfigurationProvider _mapperconfig;
        private readonly IMapper _mapper;

        public PaymentSearchController()
        {
            _mapperconfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PaymentSearchRequestModel, PaymentSearchRequestDto>()
                    .ForMember(x => x.DateFrom, x => x.MapFrom(model => DateTime.Parse(model.DateFrom)))
                    .ForMember(x => x.DateTo, x => x.MapFrom(model => DateTime.Parse(model.DateTo)))
                    .ForMember(x => x.Start, x => x.MapFrom(model => model.start))
                    .ForMember(x => x.Length, x => x.MapFrom(model => model.length))
                    .ForMember(x => x.PaymentSource, x => x.MapFrom(model => model.PaymentSource != null && model.PaymentSource.HasValue  ? model.PaymentSource.Value : -1))
                    .ForMember(x => x.PaymentType, x => x.MapFrom(model => model.PaymentType != null && model.PaymentType.HasValue ? model.PaymentType.Value : -1));
            });
            _mapper = _mapperconfig.CreateMapper();
        }

        public ActionResult Index(PaymentSearchRequestModel query)
        {
            return View(query);
        }

        public ActionResult Results(PaymentSearchRequestModel query)
        {
            return View(query);
        }

        public JsonResult GetPaymentSearch(PaymentSearchRequestModel query)
        {
            var errormessage = "An error occurred while requesting the search.";

            try
            {
                var req = _mapper.Map<PaymentSearchRequestDto>(query);

                // A quick flip if they happen to be out of order.
                if (req.DateFrom > req.DateTo)
                {
                    var datetemp = req.DateFrom;
                    req.DateFrom = req.DateTo;
                    req.DateTo = datetemp;
                }

                var results = _R360ServiceManager.GetPaymentSearch(req);
                if (results.Status == WFS.RecHub.R360Shared.StatusCode.FAIL)
                {
                    var m = errormessage;
                    if (results.Errors.Count > 0)
                        m = results.Errors[0];
                    return Json(new { error = m });
                }

                return Json(new
                {
                    data = results.Results,
                    metadata = results.Metadata,
                    query.draw,
                    recordsTotal = results.TotalRecords,
                    recordsFiltered = results.TotalRecords
                });
            }
            catch (Exception e)
            {
                Logger.Log(WfsLogLevel.Error, e.Message);
                return Json(new { error = errormessage });
            }
        }
    }
}