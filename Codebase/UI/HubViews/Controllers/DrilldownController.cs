﻿using HubViews.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HubViews.Controllers
{
  public class DrilldownController : BaseController
    {
        //
        // GET: /Drilldown/

        public ActionResult Drilldown()
        {
          var model = new ActionResponse
          {
            Data = new
            {
              Labels = GetLabelsForPage("BatchSummary", true)
            }
          };
          return View(model);
        }

    }
}
