﻿// Small object to open a confirmation dialog with some text and 2 buttons.

define(['jquery', 'frameworkUtils'],
function ($, framework) {

    // private
    var self = this;
    var _okcallback;
    var _cancelcallback;

    var buildUI = function (title, message, okbuttontext, cancelbuttontext) {
        if (!okbuttontext)
            okbuttontext = "OK";
        
        framework.getModalContent()
            .empty()
            .append($('<h4>' + title + '</h4>'))
            .append($('<div class="row"></div>')
                .append($('<div class="col-md-12"></div>')
                    .append($('<p>' + message + '</p>'))
                    )
                )
            .append($('<br />'))
            .append($('<div class="row"></div>')
                .append($('<div class="col-md-12"></div>')
                    .append($('<a class="btn btn-primary" href="#">' + okbuttontext + '</a>')
                        .on('click', onOKClick)
                        )
                    .append('&nbsp;')
                    .append($('<a class="btn btn-inverse" href="#"' + (cancelbuttontext?'':'style="display:none"') + '>' + (cancelbuttontext?cancelbuttontext:'') + '</a>')
                        .on('click', onCancelClick)
                        )
                    )
                );
    };

    var onOKClick = function () {
        framework.closeModal(true);
        if (_okcallback)
            _okcallback();
        return false;
    };
    var onCancelClick = function () {
        framework.closeModal(true);
        if (_cancelcallback)
            _cancelcallback();
        return false;
    };

    // Constructor
    function confirmationDialog() {

    }

    // Public methods
    confirmationDialog.prototype = {
        constructor: confirmationDialog,
        show: function (title, message, okcallback, cancelcallback, okbuttontext, cancelbuttontext) {
            buildUI(title, message, okbuttontext, cancelbuttontext);
            _okcallback = okcallback;
            _cancelcallback = _cancelcallback;
            framework.openModal({
                closable: false,
                width: '500px'
            });
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return confirmationDialog;
});