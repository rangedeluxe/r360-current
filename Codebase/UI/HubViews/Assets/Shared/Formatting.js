﻿// Service contains functions for formatting in views.  

define(['jquery', 'moment.timezone', 'jsTimezoneDetect'],
function ($, moment) {

    // private
    var abbrs = {
        AST:    "Atlantic Standard Time",
        ADT:    "Atlantic Daylight Time",
        EST:	"Eastern Standard Time",
        EDT:	"Eastern Daylight Time",
        CST:	"Central Standard Time",
        CDT:	"Central Daylight Time",
        MST:	"Mountain Standard Time",
        MDT:	"Mountain Daylight Time",
        PST:	"Pacific Standard Time",
        PDT:	"Pacific Daylight Time",
        AKST:	"Alaska Time",
        AKDT:	"Alaska Daylight Time",
        HST:	"Hawaii Standard Time",
        HAST:	"Hawaii-Aleutian Standard Time",
        HADT:	"Hawaii-Aleutian Daylight Time",
        SST:	"Samoa Standard Time",
        SDT:	"Samoa Daylight Time",
        CHST:	"Chamorro Standard Time"
    };
    // private methods
    var formatCurrency = function (value, persistVal) {
        persistVal = (typeof persistVal !== 'undefined') 
            ? persistVal 
            : false;

        if (value === null || value === undefined)
            return persistVal ? value : '';

        if (typeof value === 'number' && isNaN(parseFloat(value)))
            return persistVal ? value : '';

        if(isNaN(parseFloat(value)))
            return persistVal ? value : '';

        var val = typeof value === 'string'
            ? parseFloat(value).toFixed(2)
            : value.toFixed(2);

        val = '$' + val.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,");

        // If we have an "infinity" result, clear it.
        if (/[Ii]nfinity/.test(val))
            return persistVal ? value : '';

        return val;
    };

    var convertDateToJson = function (value) {
        var re = /-?\d+/;
        var m = re.exec(value);
        var d = new Date(parseInt(m[0]));
        return d;
    };

    var getLocalTimeZone = function() {
        let tz = jstz.determine();
        let tzabbr = moment.tz(tz.name()).zoneAbbr();
        return abbrs[tzabbr];
    };

    var getLocalTimeZoneOffset = function() {
        let tz = jstz.determine();
        let localoffset = moment.tz(tz.name())._offset;
        return localoffset;
    };

    //this only converts from utc to the clients/browser time zone
    //It uses moment to figure out local time zone and then we use 
    //a hash for the long name :(
    var toLocalTimeZone = function (time, longName, format) {
        if (!time) return "";
        let defaultFormat = 'hh:mm a ';
        if (!format) format = defaultFormat;
        let tz = jstz.determine();
        let finalTz = moment.tz(tz.name()).zoneAbbr();
        if (longName) {
            finalTz = abbrs[moment.tz(tz.name()).zoneAbbr()] != undefined ?
                abbrs[moment.tz(tz.name()).zoneAbbr()] : finalTz;
        }
        let localoffset = moment.tz(tz.name())._offset;
        return moment.utc(time).utcOffset(localoffset).format(format) + finalTz;
    }

    // Constructor
    function formatting() { }

    // Public methods
    formatting.prototype = {
        constructor: formatting,
        formatCurrency: formatCurrency,
        convertDateToJson: convertDateToJson,
        toLocalTimeZone: toLocalTimeZone,
        getLocalTimeZoneOffset: getLocalTimeZoneOffset,
        getLocalTimeZone: getLocalTimeZone
    };

    // Return a pointer to the function so the caller can instanciate.
    return formatting;
});