﻿// This service keeps track of and controls the breadcrumming logic for all of the pages.
// Local Storage will be used to persist the data.

// Storage Format:
//   [{Name: 'Batch Summary', URL: '/RecHubViews/BatchSummary', Data:{params},...]

define(['jquery', 'frameworkUtils'],
function ($, framework) {

    // private
    var self = this;
    var LOCAL_STORAGE_BREADCRUMB_KEY = "breadcrumb_local_storage";

    // private methods
    var getBreadcrumbs = function () {
        var bcs = JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_BREADCRUMB_KEY));
        if (bcs === null)
            bcs = [];
        return bcs;
    };

    var setBreadcrumbs = function (data) {
        window.localStorage.setItem(LOCAL_STORAGE_BREADCRUMB_KEY, JSON.stringify(data));
    };

    var registerBreadcrumb = function (text, url, data, externalUrl) {
        // Logic: push the current breadcrumb. However, if the breadcrumb already exists
        //        in the stack, then we need to pop that breadcrumb.
        //        For example, RecSummary->BatchSummary->BatchDetail, then we click
        //        on RecSummary.

        popBreadcrumb(text);
        var breaddata = getBreadcrumbs();
        breaddata.unshift({ Name: text, url: url, Data: data, label: text, params: data, externalUrl: externalUrl });

        setBreadcrumbs(breaddata);
    };

    var popBreadcrumb = function (text) {
        // Logic: pop a breadcrumb, regardless if it's the first in the queue or not.
        //        We will continue popping until we have popped the breadcrumb we pass.
        
        var data = getBreadcrumbs();

        // First, we find if the text exists.  We do nothing if not.
        var index = -1;
        $.each(data, function (i, e) {
            if (e.Name === text)
                index = i;
        });

        if (index < 0)
            return null;

        // Shift until the breadcrumb is found.
        var breadcrumb;
        for(var shifts = 0; shifts <= index; shifts++) {
            breadcrumb = data.shift();
        }

        setBreadcrumbs(data);

        return breadcrumb;
    };

    var renderBreadcrumbs = function () {
        var bcs = getBreadcrumbs().reverse();
        var span = "";

        span += "<span class='breadcrumbs'>";
        $.each(bcs, function (i, e) {
            span += "<a href='#' data-name='" + e.Name + "'>" + e.Name + "</a> > ";
        });
        span += "</span>";
        return span;
    };

    var loadBreadcrumb = function (container, name) {
        var bcs = getBreadcrumbs();
        var bc = null;
        $.each(bcs, function (i, e) {
            if (e.Name === name)
                bc = e;
        });

        if (bc === null)
            return;

        // Needed for switching between applications.
        if (bc.Reload === true)
            window.location = bc.url;
        else
            framework.loadByContainer(bc.url, container, bc.Data);
    };

    // Hook up a breadcrumb clearer for the Tabs.
    $('#recTabs a[role="menu"]').click(function () {
        setBreadcrumbs([]);
    });

    // Hook up a window unload event for auto redirects like session timing out.
    window.addEventListener("beforeunload", function (event) {
        // clear the breadcrumbs every time we exit the page unless it's one of these elements:
        // transaction detail link, batch detail link and download results as text as it triggers an 
        // exit event even though it doesn't really exit
        if (document.activeElement.classList.contains('transaction-link') || document.activeElement.classList.contains('batch-link')
             || document.activeElement.id === 'text-view-button') {
            return;
        }
        else{
            setBreadcrumbs([]);
        }
    });


    // Constructor
    function breadcrumbs() {

    }

    // Public methods
    breadcrumbs.prototype = {
        constructor: breadcrumbs,
        register: function (text, url, data, externalUrl) {
            registerBreadcrumb(text, url, data, externalUrl);
        },
        pop: function (text) {
            return popBreadcrumb(text);
        },
        get: function () {
            return getBreadcrumbs();
        },
        render: function () {
            return renderBreadcrumbs();
        },
        load: function (container, name) {
            loadBreadcrumb(container, name);
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return breadcrumbs;
});