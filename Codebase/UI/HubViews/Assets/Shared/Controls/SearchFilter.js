﻿// Object controls the filters for searching.

define(['jquery', 'frameworkUtils', 'ko', 'moment',
    '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap',
    'bootstrap', 'wfs.select', 'select2'],
function ($, framework, ko, moment) {
    var _container;
    var $getE;

    var _dataentrycolumns;
    var _selected;
    var _isinitialized;
    var _onerrorcallback;
    var _controlid;
    var _operatorsnormal;
    var _operatorsstring;
    var _operatorsbatch;
    var _$controlbase;
    

    // private
    var self = this;

    var initialize = function() {
        // Load up the base html;
        _$controlbase = $('<div id="' + _controlid + '"></div>');
        _$controlbase
        $(_container).append(_$controlbase);
        var table = $('<table class="table table-striped">')
            .append('<thead><tr><th>Field</th><th>Operator</th><th>Value</th><th class="options-column"></th></tr></thead>')
            .append('<tbody></tbody>');
        _$controlbase.append(table);
        _$controlbase.append('<button class="btn-add-criteria btn btn-secondary"><span class="fa fa-plus fa-lg text-success"></span> Add search criteria...</button>');

        // Events
        $getE('.btn-add-criteria').click(function () {
            newRow();
        });
    };

    var newRow = function (criteria) {

        // Quick validation first - kick out if we can't find the column in the DE list.
        if (criteria && $.grep(_dataentrycolumns, function (e) { return e.ReportTitle == criteria.Column.ReportTitle }).length == 0)
            return;

        var $row = $('<tr>')
            .append('<td><span class="select-column"></span></td>')
            .append('<td><span class="select-operator"></span></td>')
            .append('<td><span><input type="text" class="form-control input-value" /></span></td>')
            .append('<td><span class="btn-remove"><a href="#" class="fa fa-times fa-2x text-danger"></a></span></td>');
                
        _$controlbase.find('table tbody').append($row);

        var $selectcolumn = $row.find('.select-column');
        var $selectoperator = $row.find('.select-operator');
        var $inputvalue = $row.find('.input-value');
        var selectedobj = {};
        _selected.push(selectedobj);

        // Row events and setup.
        $row.find('.btn-remove').click(function () {
            var $r = $(this).parents('tr');
            _selected.splice($r.index(), 1);
            $r.remove();

            // returning false prevents the screen from 
            // auto-scrolling to the top on anchor tags.
            return false;
        });

        $selectcolumn.wfsSelectbox({
            items: _dataentrycolumns,
            callback: function (selected) {
                selectedobj.Column = selected;

                // If we selected nothing, clear out our stuff.
                if (selected == null) {
                    $selectoperator.wfsSelectbox('setItems', []);
                    return;
                }

                // When we select a column, fill in the operators.
                var items;

                // If we passed in overwriting operators from the caller, use those.
                if (selected.Operators && selected.Operators.length > 0) {
                    items = selected.Operators;
                }
                else {
                    // Otherwise use the defaults.
                    if (selected.IsStandard === true)
                        items = _operatorsbatch;
                    else if (selected.DataType === 1)
                        items = _operatorsstring;
                    else
                        items = _operatorsnormal;
                }
                selectedobj.Operator = null;
                $selectoperator.wfsSelectbox('setItems', items);
            },
            placeHolder: 'Select a Column',
            displayField: 'ReportTitle',
            idField: 'ReportTitle'
        });

        $selectoperator.wfsSelectbox({
            items: [],
            callback: function (selected) {
                selectedobj.Operator = (selected) ? selected.Id : null;
            },
            placeHolder: 'Select an Operator',
            displayField: 'Text',
            idField: 'Id'
        });

        $inputvalue.change(function () {
            selectedobj.Value = $inputvalue.val();
        });

        // Finally set the rows values, if we have values.
        if (criteria && criteria.Column) {
            $selectcolumn.wfsSelectbox('setValue', criteria.Column.ReportTitle);
        }
        if (criteria && criteria.Operator) {
            $selectoperator.wfsSelectbox('setValue', criteria.Operator);
        }
        if (criteria && criteria.Value) {
            $inputvalue.val(criteria.Value);
            $inputvalue.change();
        }

        return $row;
    };

    var setCriteria = function (criteria) {
        $.each(criteria, function (i, e) {
            newRow(e);
        });
    };

    var clear = function () {
        _selected = [];
        _$controlbase.find('tbody tr').remove();
    };

    var validateSelected = function () {
        var errors = [];

        
        $.each(_selected, function (i, e) {

            // Rule: Rows must be filled out completely.
            if (!e.Column || !e.Operator || !e.Value) {
                errors.push("Please fill in all required criteria.");
            }

            // Rule: No duplicate rows with similar Column+Operator. 'Contains' is an exception,
            // as it makes sense to have multiple contains on a single field.
            let dups = $.grep(_selected, function(a) {
                return a != e && a.Column == e.Column && a.Operator == e.Operator && a.Operator != 6;
            });
            if (dups && dups.length > 0) {
                errors.push("Please remove duplicate (column and operator) rows from the optional critera.");
            }

            // Rule: For date columns, the format mm/dd/yyyy is required.
            if (e.Column && e.Column.DataType == 11 && !e.Value.match(/^\d{1,2}\/\d{1,2}\/\d{4}$/)) {
                errors.push("Date fields must be in the format MM/DD/YYYY");
            }

            // Rule: The date must be a valid date range mm/dd/yyyy
            if (e.Column && e.Column.DataType == 11 && !moment(e.Value, 'MM/DD/YYYY').isValid()) {
                errors.push("Invalid Date Range, must be in the format MM/DD/YYYY");
            }

            // Rule: Floats and Currencies need to be in a numeric format.
            if (e.Column && (e.Column.DataType == 6 || e.Column.DataType == 7) && !$.isNumeric(e.Value)) {
                errors.push("Numeric Columns must have Numeric Values");
            } 
        });


        return errors;
    };

    // Constructor
    function searchFilter(container, id) {
        _container = container;
        $getE = function (b) { return _container.find(b); };
        _isinitialized = false;
        _controlid = id;
        _selected = [];
        _dataentrycolumns = [];
        // These need to match the DTO.
        _operatorsnormal = [
            {
                Text: 'Is Greater Than',
                Id: 1
            },
            {
                Text: 'Is Less Than',
                Id: 2
            },
            {
                Text: 'Equals',
                Id: 3
            },
        ];
        _operatorsstring = [
            {
                Text: 'Begins With',
                Id: 4
            },
            {
                Text: 'Ends With',
                Id: 5
            },
            {
                Text: 'Contains',
                Id: 6
            },
            {
                Text: 'Equals',
                Id: 3
            }
        ];
        _operatorsbatch = [
            {
                Text: 'Is Greater Than or Equal To',
                Id: 1
            },
            {
                Text: 'Is Less Than or Equal To',
                Id: 2
            },
            {
                Text: 'Equals',
                Id: 3
            }
        ];
    }

    // Public properties


    // Public methods
    searchFilter.prototype = {
        constructor: searchFilter,
        init: function () {
            initialize();
        },
        getSelected: function() {
            return _selected;
        },
        setSelected: function(criteria){
            clear();
            setCriteria(criteria);
        },
        setItems: function (items) {
            clear();
            _dataentrycolumns = items;
        },
        validate: function() {
            return validateSelected();
        },
        clear : function() {
            return clear();
        },
        dispose: function () {
            _dataentrycolumns = [];
            _selected = [];
            _isinitialized = false;
            _onerrorcallback = null;
            _controlid = '';
            _operatorsnormal = [];
            _operatorsstring = [];

            _$controlbase.remove();
        }
    }

    return searchFilter;
});