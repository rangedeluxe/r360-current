﻿
define(['jquery', 'frameworkUtils'],
function ($, framework) {

    // private
    let self = this;
    let $fakeEl;

    // private methods
    let measureTextWidth = function (text, offset, minimum, htmltag, font) {
        if (!offset)
            offset = 25;
        if (!minimum)
            minimum = 100;
        if (!htmltag)
            htmltag = '<strong>';

        if (!$fakeEl) 
            $fakeEl = $(htmltag).hide().appendTo(document.body);
        $fakeEl.text(text).css('font', font || $fakeEl.css('font'));
        let w = $fakeEl.width() + offset;
        return w > minimum ? w : minimum;
    };

    // Constructor
    function dataTablesUtilities() {

    }

    // Public methods
    dataTablesUtilities.prototype = {
        constructor: dataTablesUtilities,
        measureTextWidth: function (text, offset, minimum, htmltag, font) {
            return measureTextWidth(text, offset, minimum, htmltag, font);
        }
    };

    // Return a pointer to the function so the caller can instanciate.
    return dataTablesUtilities;
});