﻿
define(['jquery', 'ko', 'frameworkUtils'],
function ($, ko, framework) {
    var _vm;

    var init = function (model, viewmodel, containername) {
        $(function () {
            var container = $('#' + containername);
            _vm = new viewmodel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }

    return {
        init: init
    };
});