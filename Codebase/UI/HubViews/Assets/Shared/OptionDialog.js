﻿// Small object to open a confirmation dialog with some text and 2 buttons.

define(['jquery', 'frameworkUtils'],
function ($, framework) {

    var buildUI = function (title, message, buttons) {
        
        let dialog = framework.getModalContent()
            .empty()
            .append($('<h4>'+ title + '</h4>'))
            .append($('<div class="row"></div>')
                .append($('<div class="col-md-12"></div>')
                    .append($('<p>' + message + '</p>'))
                    )
                )
            .append($('<br />'))
            .append($('<div class="row"></div>')
                .append($('<div class="col-md-12 button-row"></div>')));
        
        let row = dialog.find('.button-row');
        buttons.forEach(function(b) {
            let enabled = '';
            if (b.enabled === false)
                enabled = 'disabled="disabled"';
            let $btn = $('<a class="btn '+ b.class + '" ' + enabled + ' href="#" style="margin-right: 5px;">' + b.text + '</a>')
                .on('click', function(){ onClick(this, b.action); return false; });
            if (b.enabled === false)
                $btn.prop('disabled', true);
            if (b.title) 
                $btn.attr('title', b.title);
            row.append($btn);
        });
        
    };

    var onClick = function (btn, callback) {
        let disabled = $(btn).prop('disabled');
        if (disabled === true) 
            return;
        $(btn).parent().find('.btn').off('click');
        framework.closeModal(true);
        callback();
    };

    // Constructor
    function confirmationDialog() {

    }

    // Public methods
    confirmationDialog.prototype = {
        constructor: confirmationDialog,
        show: function (title, message, buttons, width) {
            let w = 500;
            if (width)
                w = width;
            buildUI(title, message, buttons);
            framework.openModal({
                closable: false,
                width: w + 'px'
            });
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return confirmationDialog;
});