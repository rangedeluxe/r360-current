﻿// This service contains some methods to show different types of images around R360.

define(['jquery','frameworkUtils'],
function ($,framework) {

    // private
    var _baseurl = '/RecHubViews/';
    var _singleimagepath = 'Image?';
    var _multipleimagepath = 'Image/AllImages?';
    var _reportpath = 'Image/Report?';
    var _pdfpath = "Image/AdvancedSearchPDF?";
    var _localstorageimagerequest = "imagerequestobject";
    var _imagesExist = "Image/ImagesExist";

    // private methods
    var displaySinglePayment = function (bankid, workgroupid, batchid, depositdate, transactionid, batchsequence) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate,
            TxnSequence: transactionid, BatchSequence: batchsequence, ImportTypeKey: 3
        });
        window.open(_baseurl + _singleimagepath + querystring, '_blank');
    };

    var displaySingleDocument = function (bankid, workgroupid, batchid, depositdate, transactionid, batchsequence) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate,
            TxnSequence: transactionid, BatchSequence: batchsequence, ImportTypeKey: 1
        });
        window.open(_baseurl + _singleimagepath + querystring, '_blank');
    };

    var displayTransactionDocuments = function (bankid, workgroupid, batchid, depositdate, transactionsequence) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate,
            TransactionId: transactionsequence, ImportTypeKey: 2, Page: -1, ImageFilterOption: 'DocumentsOnly'
        });
        window.open(_baseurl + _multipleimagepath + querystring, '_blank');
    };

    var displayTransactionImages = function (bankid, workgroupid, batchid, depositdate, transactionid) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate,
            TransactionId: transactionid, ImportTypeKey: 1, Page: -1
        });
        window.open(_baseurl + _multipleimagepath + querystring, '_blank');
    };

    var displayTransactionReport = function (bankid, workgroupid, batchid, depositdate, transactionsequence, transactionid, batchsequence, batchnumber, processingdate) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate, TxnSequence: transactionsequence,
            TransactionId: transactionid, BatchNumber: batchnumber, ProcessingDateString: processingdate, ReportName: 'TransactionReport'
        });
        window.open(_baseurl + _reportpath + querystring, '_blank');
    };

    var displayItemReport = function (bankid, workgroupid, batchid, depositdate, transactionsequence, transactionid, batchsequence, batchnumber, processingdate) {
        var querystring = $.param({
            BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate, TxnSequence: transactionsequence, TransactionId: transactionid,
            BatchSequence: batchsequence, BatchNumber: batchnumber, ProcessingDateString: processingdate, ReportName: 'ItemReport'
        });
        window.open(_baseurl + _reportpath + querystring, '_blank');
    };

    var displayBatchImages = function (bankid, workgroupid, batchid, batchnumber, depositdate) {
        var querystring = $.param({ BankId: bankid, WorkgroupId: workgroupid, BatchId: batchid, DepositDateString: depositdate, BatchNumber: batchnumber });
        window.open(_baseurl + _multipleimagepath + querystring, '_blank');
    };

    var displayASPdf = function (model) {
        window.localStorage.setItem(_localstorageimagerequest, JSON.stringify(
            { Data: model, URL: '/RecHubViews/Image/AdvancedSearchPDFRequest' }
        ));
        window.open('/RecHubViews/Image/LoadFromLocal', '_blank');
    };

    var displayASSelected = function (model) {
        window.localStorage.setItem(_localstorageimagerequest, JSON.stringify(
            { Data: model, URL: '/RecHubViews/Image/AdvancedSearchPDFSelectedRequest' }
        ));
        window.open('/RecHubViews/Image/LoadFromLocal', '_blank');
    };
    
    var displayDownloadAS = function (model) {
        window.localStorage.setItem(_localstorageimagerequest, JSON.stringify(
            { Data: model, URL: '/RecHubViews/Image/AdvancedSearchDownloadRequest' }
        ));
        window.open('/RecHubViews/Image/LoadFromLocal', '_blank');
    };

    var displayInProcessExceptionImages = function(model) {
        window.localStorage.setItem(_localstorageimagerequest, JSON.stringify(
            { Data: model, URL: _baseurl + 'Image/InProcessExceptionImages' }
        ));
        window.open('/RecHubViews/Image/LoadFromLocal', '_blank');
    };

    var downloadCENDSFile = function (path) {
        window.location = '/RecHubViews/Image/DownloadCENDSFile?path=' + path;
    };

    var imagesExist = function (container, query, callback) {
        framework.doJSON(_baseurl + _imagesExist, query,
            function (data) {
                if (!data || !data.Data.IsSuccessful) {
                    framework.errorToast(container, 'Error finding image exists.');
                } else {
                    callback(data.Data.ImagesAvailable);
                }
            });
    }

    // Constructor
    function imageService() { }

    // Public methods
    imageService.prototype = {
        constructor: imageService,
        displaySinglePayment: displaySinglePayment,
        displaySingleDocument: displaySingleDocument,
        displayTransactionDocuments: displayTransactionDocuments,
        displayTransactionImages: displayTransactionImages,
        displayTransactionReport: displayTransactionReport,
        displayItemReport: displayItemReport,
        displayBatchImages: displayBatchImages,
        displayASPdf: displayASPdf,
        displayASSelected: displayASSelected,
        displayDownloadAS: displayDownloadAS,
        displayInProcessExceptionImages: displayInProcessExceptionImages,
        downloadCENDSFile: downloadCENDSFile,
        imagesExist: imagesExist
    };

    // Return a pointer to the function so the caller can instanciate.
    return imageService;
});