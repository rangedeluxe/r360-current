﻿// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.

define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js', '/RecHubViews/Assets/Shared/Formatting.js',
    '/RecHubViews/Assets/Shared/Controls/searchFilter.js',
    '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap',
    'bootstrap', 'wfs.select', 'wfs.treeselector', 'select2'],
function ($, framework, ko, breadcrumbs, formatting, searchFilter) {

    let _container;
    let self = this;
    let $getE;
    let _workgroupSelector;
    let _treeloaded = false;
    let _treeInitialized = false;
    let _breadcrumbs = new breadcrumbs();
    let _searchFilter;
    let _model;
    let _preference;

    let _orderby = [
                { OrderBy: "ClientAccountID", OrderDirection: "asc", Text: "Workgroup / Ascending" },
                { OrderBy: "ClientAccountID", OrderDirection: "desc", Text: "Workgroup / Descending" },
                { OrderBy: "DepositDate", OrderDirection: "asc", Text: "Deposit Date / Ascending" },
                { OrderBy: "DepositDate", OrderDirection: "desc", Text: "Deposit Date / Descending" },
                { OrderBy: "SourceBatchID", OrderDirection: "asc", Text: "Batch ID / Ascending" },
                { OrderBy: "SourceBatchID", OrderDirection: "desc", Text: "Batch ID / Descending" },
                { OrderBy: "BatchNumber", OrderDirection: "asc", Text: "Batch / Ascending" },
                { OrderBy: "BatchNumber", OrderDirection: "desc", Text: "Batch / Descending" },
                { OrderBy: "TxnSequence", OrderDirection: "asc", Text: "Transaction / Ascending" },
                { OrderBy: "TxnSequence", OrderDirection: "desc", Text: "Transaction / Descending" },
                { OrderBy: "Amount", OrderDirection: "asc", Text: "Amount / Ascending" },
                { OrderBy: "Amount", OrderDirection: "desc", Text: "Amount / Descending" },
                { OrderBy: "RT", OrderDirection: "asc", Text: "RT / Ascending" },
                { OrderBy: "RT", OrderDirection: "desc", Text: "RT / Descending" },
                { OrderBy: "DDA", OrderDirection: "asc", Text: "DDA / Ascending" },
                { OrderBy: "DDA", OrderDirection: "desc", Text: "DDA / Descending" },
                { OrderBy: "Account", OrderDirection: "asc", Text: "Account / Ascending" },
                { OrderBy: "Account", OrderDirection: "desc", Text: "Account / Descending" },
                { OrderBy: "Serial", OrderDirection: "asc", Text: "Check/Trace/Ref Number / Ascending" },
                { OrderBy: "Serial", OrderDirection: "desc", Text: "Check/Trace/Ref Number / Descending" },
                { OrderBy: "Remitter", OrderDirection: "asc", Text: "Payer / Ascending" },
                { OrderBy: "Remitter", OrderDirection: "desc", Text: "Payer / Descending" },
                { OrderBy: "PaymentSource", OrderDirection: "asc", Text: "PaymentSource / Ascending" },
                { OrderBy: "PaymentSource", OrderDirection: "desc", Text: "PaymentSource / Descending" },
                { OrderBy: "PaymentType", OrderDirection: "asc", Text: "PaymentType / Ascending" },
                { OrderBy: "PaymentType", OrderDirection: "desc", Text: "PaymentType / Descending" }
    ];
    // Operators for the query builder.
    let containsonlyoperators = [
        { Text: 'Contains', Id: 1 }
    ];
    let inclusiveoperators = [
        { Text: 'Is Greater Than Or Equal To', Id: 2 },
        { Text: 'Is Less Than Or Equal To', Id: 3 },
        { Text: 'Equals', Id: 4 },
    ];
    // Where clauses for payment filters. These are more or less static for now; no use calling an API to get them.
    let clauses = [
        { XmlColumnName: "RemitterName", ReportTitle: "Payer", DataType: 1, Operators: containsonlyoperators },
        { XmlColumnName: "Amount", ReportTitle: "Payment Amount", DataType: 7, Operators: inclusiveoperators },
        { XmlColumnName: "Account", ReportTitle: "Account Number", DataType: 1, Operators: containsonlyoperators },
        { XmlColumnName: "DDA", ReportTitle: "DDA", DataType: 1, Operators: containsonlyoperators },
        { XmlColumnName: "Serial", ReportTitle: "Check/Trace/Ref Number", DataType: 1, Operators: containsonlyoperators },
        { XmlColumnName: "RT", ReportTitle: "R/T", DataType: 1, Operators: containsonlyoperators },
    ];

    let applyUI = function (model) {
        window.localStorage.removeItem('redirectorasesults');
        let bcdata = _breadcrumbs.pop("Payment Search");
        var bcs = _breadcrumbs.render();
        $getE('.breadcrumbheader').html(bcs);
        if (!model.Workgroup && !model.DateTo && !model.DateFrom && bcdata && bcdata.Data) {
            for (let attrname in bcdata.Data) {
                if (bcdata.Data.hasOwnProperty(attrname)) {
                    model[attrname] = bcdata.Data[attrname];
                }
            }
        }

        _model = model;
        $getE('#dateFrom').wfsDatePicker({ callback: function (val) { self.DateFrom(val); } });
        $getE('#dateTo').wfsDatePicker({ callback: function (val) { self.DateTo(val); } });
        if (model.Workgroup) {
            $getE('#dateFrom').wfsDatePicker('setDate', model.DateFrom);
            $getE('#dateTo').wfsDatePicker('setDate', model.DateTo);
        }
        self.DateFrom($getE('#dateFrom').wfsDatePicker('getDate'));
        self.DateTo($getE('#dateTo').wfsDatePicker('getDate'));

        _searchFilter = new searchFilter($getE('#query-filter'), 'ps-filter');
        _searchFilter.init();
        _searchFilter.setItems(clauses);

        if (model.Criteria) {
            let c = model.Criteria.map(function (e) {
                return {
                    Column: {
                        ReportTitle: e.ReportTitle,
                        DataType: e.DataType
                    },
                    Operator: e.Operator,
                    Value: e.Value
                };
            });
            _searchFilter.setSelected(c);
        }

        // Load up the WGS after everything else has finished.
        framework.showSpinner("Loading...");
        $.when(
            framework.doJSON('/RecHubViews/Shared/GetPaymentTypes', null, paymentTypesCallBack),
            framework.doJSON('/RecHubViews/Shared/GetPaymentSources', null, paymentSourcesCallBack),
            framework.doJSON('/RecHubViews/Shared/GetSystemPreference', { name: 'MaximumWorkgroups' }, preferenceCallback)
        ).then(function() {
            framework.hideSpinner();
            _workgroupSelector = $getE('#ps-workgroup').wfsTreeSelector({
                useExpander: true,
                entityURL: '/RecHubRaamProxy/api/entity',
                callback: selectedTreeItem,
                dataCallback: treeDataCallback,
                expanderTitle: "Select an Entity or Workgroup",
                entitiesOnly: false
            });
        }, function() {
            // Error in the request(s).
            framework.hideSpinner();
        });

        //sort sort-by elements
        _orderby = _orderby.sort(function (a, b) { return a.Text.localeCompare(b.Text); });
        $getE('#payment-sort').wfsSelectbox({
            items: _orderby,
            displayField: "Text",
            idField: "Text",
            callback: function () {
                let match = $.grep(_orderby, function (e) {
                    return $getE('#payment-sort').wfsSelectbox('getValue') == e.Text;
                });
                let ob = match.length > 0 ? match[0] : null;
                self.OrderBy(ob);
            },
            width: "300px"
        });

        if (model.OrderBy) {
            self.OrderBy({ OrderBy: model.OrderBy, OrderDirection: model.OrderDirection });
            let match = $.grep(_orderby, function (e) {
                return e.OrderBy === model.OrderBy && e.OrderDirection === model.OrderDirection;
            });
            self.OrderBy(match[0]);
            $getE('#payment-sort').wfsSelectbox('setValue', self.OrderBy().Text);
        }

        $getE('#search-button').click(function () {
            if (validate())
                search();
        });

        $getE('#clear-search-button').click(function () {
            clear();
        });

    };

    let preferenceCallback = function(data) {
        if (!data || !data.success) {
            framework.errorToast(_container, 'Error Loading System Preferences.');
            return;
        }
        _preference = data.data;
    };

    let paymentTypesCallBack = function (data) {

        if (!data || !data.success) {
            framework.errorToast(_container, 'Error Loading Payment Types.');
            return;
        }
        self.PaymentTypes(data.data);
        $getE('#paymentType').wfsSelectbox({
            items: self.PaymentTypes(),
            callback: function (e) { self.PaymentType(e ? e.id : -1); },
            placeHolder: '-- All --',
            displayField: 'LongName',
            idField: 'PaymentTypeKey',
            width: "300px"
        });
       $getE('#paymentType').wfsSelectbox('setValue', _model.PaymentType);
    };

    let paymentSourcesCallBack = function (data) {
        if (!data || !data.success) {
            framework.errorToast(_container, 'Error Loading Payment Sources.');
            return;
        }

        
        self.PaymentSources(data.data);
        $getE('#paymentSource').wfsSelectbox({
            items: self.PaymentSources(),
            callback: function (e) { self.PaymentSource(e ? e.id : -1); },
            placeHolder: '-- All --',
            displayField: 'LongName',
            idField: 'PaymentSourceKey',
            width: "300px"
        });
        $getE('#paymentSource').wfsSelectbox('setValue', _model.PaymentSource);
    };

    let getWorkgroupCount = function(entity)
    {
        let count = 0;
        if (entity.isNode)
            return 1;
        $.each(entity.items, function(ind,el)
        {
            if (el.isNode)
                count++;
            else
                count += getWorkgroupCount(el);
        });
        return count;
    }

    let selectedTreeItem = function (selected, initial) {
        let title = null;

        let workgroupcount = getWorkgroupCount(selected);
        if ((selected && !initial)
            || (selected && initial && !self.Workgroup())) {

            if (workgroupcount > parseInt(_preference.DefaultSetting)) {
                if (!initial) {
                    let message = "The number of workgroups for the selected entity hierarchy is " + workgroupcount + "."
                        + "<br/>The system will only allow " + _preference.DefaultSetting + " workgroups in a search."
                        + "<br/>Please select a lower level entity or contact your administrator.";
                    framework.errorToast(_container, message);
                }
            }
            else {
                title = 'Selected: ' + selected['label'];
                _workgroupSelector.wfsTreeSelector('setTitle', title);
                self.Workgroup(selected.id);
            }
        }
    };

    let treeDataCallback = function (data) {
        let entities = _workgroupSelector.wfsTreeSelector('getEntities');
        let workgroups = _workgroupSelector.wfsTreeSelector('getWorkgroups');
        let ecount = entities.length;
        let wcount = workgroups.length;

        if (ecount === 1 && wcount === 1) {
            let element = $('<div><input value="'
              + workgroups[0].label
              + '" class="form-control input-md" type="text" readonly="true" style="width: 300px;display:inline;"></div>');
            $getE('#ps-workgroup').html(element);
            self.Workgroup(workgroups[0].id);
        }
        else {
            _workgroupSelector.wfsTreeSelector('initialize');
            if (_model.Workgroup != undefined && _model.Workgroup != '')
                _workgroupSelector.wfsTreeSelector('setSelection', _model.Workgroup);
            _treeInitialized = true;
        }
        _treeloaded = true;
    };

    let getCurrentSearch = function () {
        var ordBy = this.OrderBy() ? this.OrderBy() : _orderby[0];
        let data = {
            DateFrom: self.DateFrom(),
            DateTo: self.DateTo(),
            Workgroup: self.Workgroup(),
            PaymentType: self.PaymentType(),
            PaymentSource: self.PaymentSource(),
            Criteria: _searchFilter.getSelected()
                .map(function (e) {
                    return {
                        XmlColumnName: e.Column.XmlColumnName,
                        ReportTitle: e.Column.ReportTitle,
                        DataType: e.Column.DataType,
                        Operator: e.Operator,
                        Value: e.Value
                    };
                }),
            OrderBy: ordBy.OrderBy,
            OrderDirection: ordBy.OrderDirection
        };

        return data;
    };

    let validate = function () {

        // Rule - User needs to select either a workgroup or an entity.
        if (self.Workgroup() == '') {
            framework.errorToast(_container, 'Please select an entity or workgroup first.');
            return false;
        }

        // Rule - The criteria filter needs to be validated too.
        let errors = _searchFilter.validate();
        if (errors && errors.length > 0) {
            framework.errorToast(_container, errors[0]);
            return false;
        }

        return true;
    };

    let search = function () {
        let data = getCurrentSearch();
        _breadcrumbs.register("Payment Search", "/RecHubViews/PaymentSearch", data, "/Framework/?tab=8");
        framework.loadByContainer('/RecHubViews/PaymentSearch/Results', _container.parent(), data);
    };

    let clear = function () {
        self.DateFrom(new Date());
        self.DateTo(new Date());
        self.PaymentType(-1);
        self.PaymentSource(-1);
        self.OrderBy(null);
        $getE('#dateFrom').wfsDatePicker('setDate', self.DateFrom());
        $getE('#dateTo').wfsDatePicker('setDate', self.DateTo());
        $getE('#paymentType').wfsSelectbox('setValue', null);
        $getE('#paymentSource').wfsSelectbox('setValue', null);
        $getE('#payment-sort').wfsSelectbox('setValue', null);

        if (_treeInitialized === true) {
            self.Workgroup('');
            _workgroupSelector.wfsTreeSelector('reset');
        }
        _searchFilter.clear();
    };

    // Constructor
    function paymentSearchViewModel() {

    }

    // Public properties
    self.DateFrom = ko.observable('');
    self.DateTo = ko.observable('');
    self.Workgroup = ko.observable('');
    self.PaymentSource = ko.observable(-1);
    self.PaymentType = ko.observable(-1);
    self.PaymentSources = ko.observable([]);
    self.PaymentTypes = ko.observable([]);
    self.OrderBy = ko.observable(null);
    // Public methods
    paymentSearchViewModel.prototype = {
        constructor: paymentSearchViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) {
                return _container.find(b);
            }
            //this if is only here because with the old UI we cant redirect directly to a page unless it's in the menu.
            //We are just seding the user directly to results before loading the whole ui since we have results in local storage
            const data = JSON.parse(window.localStorage.getItem('redirectorasesults'));
            if (data) {
                framework.loadByContainer('/RecHubViews/PaymentSearch/Results', _container.parent(), data);
            } else {
                applyUI(model);
                ko.applyBindings(self, container.get(0));
            }
        },
        dispose: function () {
            _treeloaded = false;
            _treeInitialized = false;
            self.DateFrom('');
            self.DateTo('');
            self.Workgroup('');
            self.PaymentSource(-1);
            self.PaymentType(-1);
            self.PaymentSources([]);
            self.PaymentTypes([]);
            self.OrderBy(null);
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return paymentSearchViewModel;
});