﻿// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.
// Added moment to the define

define(['jquery', 'frameworkUtils', 'ko', 'moment', '/RecHubViews/Assets/Shared/Breadcrumbs.js', '/RecHubViews/Assets/Shared/Formatting.js',
    '/RecHubViews/Assets/Shared/DataTablesUtilities.js', '/RecHubViews/Assets/Shared/ImageService.js', '/Assets/wfs/wfs-dateutil.js', 
    '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap', 'bootstrap', 'wfs.select', 'wfs.treeselector', 'select2'],
function ($, framework, ko, moment, breadcrumbs, formatting, utilities, imageService) {

    let _container;
    let $getE;
    let _resultTable;
    let _breadcrumbs = new breadcrumbs();
    let _formatting = new formatting();
    let _utilities = new utilities();
    let _model;
    let _imageservice = new imageService();
    let applyUI = function (model) {
        _model = model;
        window.localStorage.removeItem('redirectorasesults');
        // Standard Breadcrumb logic.
        _breadcrumbs.pop("Search Results");
        let bcs = _breadcrumbs.render();
        $getE('.breadcrumbheader').html(bcs);
        $getE('.breadcrumbheader a').click(function () {
            _breadcrumbs.load(_container.parent(), $(this).data('name'));
        });

        let columns = [];
        columns.push({ data: 'WorkgroupId', name: 'Images', sortable: false, width:1 });
        columns.push({ data: 'WorkgroupId', orderbyname: 'ClientAccountID', class: 'alignRight' });
        columns.push({ data: 'DepositDateString', orderbyname: 'DepositDate' });
        columns.push({ data: 'SourceBatchId', orderbyname: 'SourceBatchID', class: 'alignRight', name: 'SourceBatchId' });
        columns.push({ data: 'BatchNumber', orderbyname: 'BatchNumber', class: 'alignRight', name: 'BatchNumber' });
        columns.push({ data: 'PaymentSource', orderbyname: 'PaymentSource' });
        columns.push({ data: 'PaymentType', orderbyname: 'PaymentType' });
        columns.push({ data: 'TransactionSequence', orderbyname: 'TxnSequence', class: 'alignRight', name: 'TransactionSequence' });
        columns.push({ data: 'Amount', name: 'Amount', orderbyname: 'Amount', class: 'alignRight' });
        columns.push({ data: 'RT', orderbyname: 'RT' });
        columns.push({ data: 'Account', orderbyname: 'Account' });
        columns.push({ data: 'Serial', orderbyname: 'Serial' });
        columns.push({ data: 'RemitterName', orderbyname: 'Remitter' });
        columns.push({ data: 'DDA', orderbyname: 'DDA', width:175 });

        // Set the widths for each.
        $.each(columns, function(i, e) {
            e.width = e.width ? e.width : _utilities.measureTextWidth($getE('#results-table thead th').eq(i).text());
        });
        let initOrder = [];
        let indSort = columns.map(function (e) { return e.orderbyname; }).indexOf(model.OrderBy);
        indSort = indSort >= 0 ? indSort : 0;
        initOrder.push([indSort, model.OrderDirection]);

        framework.showSpinner('Loading...');
        _resultTable = $getE('#results-table').DataTable({
            order: initOrder,
            autoWidth: false,
            processing: false,
            serverSide: true,
            displayStart: (_model.length && _model.length > 0) ? _model.start : 0,
            pageLength: (_model.length && _model.length > 0) ? _model.length : 10,
            language: {
                emptyTable: "No records were found matching the criteria specified."
            },
            ajax: {
                url: '/RecHubViews/PaymentSearch/GetPaymentSearch',
                type: 'POST',
                data: function (d) {
                    d.Workgroup = _model.Workgroup;
                    d.DateFrom = _model.DateFrom;
                    d.DateTo = _model.DateTo;
                    d.Criteria = _model.Criteria;
                    d.PaymentType = _model.PaymentType;
                    d.PaymentSource = _model.PaymentSource;

                    // Add the order by clause to the sent data.
                    let column = columns[d.order[0].column];
                    d.OrderBy = column.orderbyname;
                    d.OrderDirection = d.order[0].dir;
                    _model.OrderBy = d.OrderBy;
                    _model.OrderDirection = d.OrderDirection;
                }
            },
            columns: columns,
            createdRow: function (row, data, index) {
                let api = this.api();

                // Appending images.
                colindex = api.column('Images:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                $(td).empty();

                let $images = $('<span class="as-image-column">');
                if (data.ShowPaymentIcon === true)
                    $images.append('<i class="fa fa-money fa-lg paymentImage" title="View Payment Image"></i>');
                $(td).append($images);

                // Format the currency row.
                colindex = api.column('Amount:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                let currency = _formatting.formatCurrency(data.Amount);
                $(td).html(currency);

                // Adding links for drill-down.
                colindex = api.column('TransactionSequence:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                let link = $('<a href="#" title="View Transaction" class="transaction-link"></a>');
                link.text(data.TransactionSequence);
                $(td).html(link);

                colindex = api.column('BatchNumber:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                link = $('<a href="#" title="View Batch" class="batch-link"></a>');
                link.text(data.BatchNumber);
                $(td).html(link);

                colindex = api.column('SourceBatchId:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                link = $('<a href="#" title="View Batch" class="batch-link"></a>');
                link.text(data.SourceBatchId);
                $(td).html(link);
            },
            scrollX: true
        });

        // Hook up image clicks.
        $getE('#results-table tbody').on('click', '.paymentImage', function () {
            let rowdata = _resultTable.row($(this).parents('tr').first()).data();
            _imageservice.displaySinglePayment(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'), rowdata.TransactionId, rowdata.BatchSequence);
        });

        // Hook up Batch and Transaction drill down links.
        $getE('#results-table tbody').on('click', '.batch-link', function () {
            const rowdata = _resultTable.row($(this).parents('tr').first()).data();
            const url = '/UI/BatchDetail';
            const data = {
                "BankId": rowdata.BankId,
                "WorkgroupId": rowdata.WorkgroupId,
                "BatchId": rowdata.BatchId,
                "DepositDateString": rowdata.DepositDateString
            };
            const pageinfo = _resultTable.page.info();
            _model.length = pageinfo.length;
            _model.start = pageinfo.start;
            _breadcrumbs.register("Search Results", "/RecHubViews/PaymentSearch/Results", _model, "/Framework/?tab=8");
            
            const bdrequest = {
                depositDate : rowdata.DepositDateString,
                workGroup : rowdata.WorkgroupId,
                batchId : rowdata.BatchId,
                bankId: rowdata.BankId
            };
            window.localStorage.setItem('batchdetaildata', JSON.stringify(bdrequest));    
            window.location.href = url;
                
        });

        $getE('#results-table tbody').on('click', '.transaction-link', function () {
            const rowdata = _resultTable.row($(this).parents('tr').first()).data();
            const url = '/UI/TransactionDetail';
            const pageinfo = _resultTable.page.info();
            _model.length = pageinfo.length;
            _model.start = pageinfo.start;
            _breadcrumbs.register("Search Results", "/RecHubViews/PaymentSearch/Results", _model, "/Framework/?tab=8");

            const tdRequest = {
                bankId: rowdata.BankId,
                workgroupId: rowdata.WorkgroupId,
                batchId: rowdata.BatchId,
                depositDate: rowdata.DepositDateString,
                transactionId: rowdata.TransactionId,
                transactionSequence: rowdata.TxnSequence
            };
            window.localStorage.setItem('transactiondetaildata', JSON.stringify(tdRequest));
            window.location.href = url;

        });

        // Cancel the default Error mode.  This prevents a standard 'alert'.
        $.fn.dataTable.ext.errMode = 'none';

        // Wire up pre and post load events.
        $getE('#results-table').on('preXhr.dt', function (e, settings, data) {
            framework.showSpinner('Loading...');
        });
        $getE('#results-table').on('xhr.dt', function (e, settings, json, xhr) {
            framework.hideSpinner();

            if (json.error) {
                // Gracefully handle missing data.
                json.data = [];
                json.recordsTotal = 0;
                framework.errorToast(_container, json.error);
                return;
            }

            _resultTable.column("SourceBatchId:name").visible(json.metadata.DisplayBatchId);
        });
    };

    // Constructor
    function paymentSearchResultsViewModel() {

    }

    // Public properties


    // Public methods
    paymentSearchResultsViewModel.prototype = {
        constructor: paymentSearchResultsViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) {
                return _container.find(b);
            };

            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            _model = {};
            if (_resultTable)
                _resultTable.destroy();
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return paymentSearchResultsViewModel;
});