﻿function initPaymentSearch(model, controllername, viewmodelname, container) {
    require.config({
        paths: {
            'paymentSearchViewModel': '/RecHubViews/Assets/PaymentSearch/ViewModels/PaymentSearchViewModel',
            'paymentSearchResultsViewModel': '/RecHubViews/Assets/PaymentSearch/ViewModels/PaymentSearchResultsViewModel',
            'searchFilter': '/RecHubViews/Assets/Shared/Controls/SearchFilter',
        }
    });

    require([controllername, viewmodelname], function (controller, viewmodel) {
        controller.init(model, viewmodel, container);
    });
}