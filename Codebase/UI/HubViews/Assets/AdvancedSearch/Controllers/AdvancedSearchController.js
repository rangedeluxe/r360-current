﻿
define(['jquery', 'ko', 'frameworkUtils', 'advancedSearchViewModel'],
function ($, ko, framework, advancedSearchViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#advancedSearchPortlet');
            _vm = new advancedSearchViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    return {
        init: init
    };
});