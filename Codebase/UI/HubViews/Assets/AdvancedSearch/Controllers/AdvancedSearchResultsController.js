﻿
define(['jquery', 'ko', 'frameworkUtils', 'advancedSearchResultsViewModel'],
function ($, ko, framework, advancedSearchResultsViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#advancedSearchResultsPortlet');
            _vm = new advancedSearchResultsViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    return {
        init: init
    };
});