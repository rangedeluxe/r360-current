﻿
define(['jquery', 'ko', 'frameworkUtils', 'advancedSearchQueryDialogViewModel'],
function ($, ko, framework, advancedSearchQueryDialogViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#advancedSearchQueryDialog');
            _vm = new advancedSearchQueryDialogViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    return {
        init: init
    };
});