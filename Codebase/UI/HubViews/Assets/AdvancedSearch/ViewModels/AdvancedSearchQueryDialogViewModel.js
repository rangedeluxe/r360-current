﻿// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.

define(['jquery', 'frameworkUtils', 'ko',
    '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap',
    'bootstrap', 'wfs.select', 'wfs.treeselector', 'select2'],
function ($, framework, ko) {

    var $getE;
    var self = this;
    var _container;
    var _model;

    var applyUI = function (model) {
        _model = model;
        if (model.QueryName) {
            self.QueryId(model.QueryId);
            self.QueryName(model.QueryName);
            self.QueryDescription(model.QueryDescription);
            self.HeaderText('Edit Stored Query');
        }
        else {
            self.HeaderText('Add Stored Query');
        }

        // Button event handlers.
        $getE('.btn-cancel-query').click(function () {
            framework.closeModal({cancel: true});
        });

        $getE('.btn-submit-query').click(function () {
            model.QueryId = self.QueryId();
            model.QueryName = self.QueryName();
            model.QueryDescription = self.QueryDescription();

            framework.showSpinner('Loading...');
            var url = _model.DataUrl ? _model.DataUrl : '/RecHubViews/AdvancedSearch/SaveQuery';
            framework.doJSON(url, model, function (result) {
                if (result.Error) {
                    framework.errorToast(_container, result.Error);
                    framework.hideSpinner();
                    return;
                }

                framework.closeModal({ QueryId: result.Id });
            });
        });

    };

    // Constructor
    function advancedSearchQueryDialogViewModel() {

    }

    // Public properties
    self.QueryName = ko.observable('');
    self.QueryDescription = ko.observable('');
    self.QueryId = ko.observable('');
    self.HeaderText = ko.observable('');

    // Public methods
    advancedSearchQueryDialogViewModel.prototype = {
        constructor: advancedSearchQueryDialogViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) {
                return _container.find(b);
            };

            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            // Function to clean up our data.
            self.QueryDescription('');
            self.QueryName('');
            self.QueryId('');
            self.HeaderText('');
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return advancedSearchQueryDialogViewModel;
});