﻿// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.
// Added moment to the define

define(['jquery', 'frameworkUtils', 'ko', 'moment', '/RecHubViews/Assets/Shared/Breadcrumbs.js',
    '/RecHubViews/Assets/Shared/ImageService.js', '/RecHubViews/Assets/Shared/Formatting.js', '/Assets/wfs/wfs-dateutil.js',
    '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap', 'bootstrap', 'wfs.select', 'wfs.treeselector', 'select2'],
function ($, framework, ko, moment, breadcrumbs, imageService, formatting) {

    let _container = $('#advancedSearchResultsPortlet');
    let $getE;
    let _resultTable;
    let _breadcrumbs = new breadcrumbs();
    let _imageservice = new imageService();
    let _formatter = new formatting();
    let _model;
    let _orderby;
    let _selectiveprintsetup = false;

    var downloadAsTextOptions = { "ExcludeCommasFromNumeric": false, "DoNotQuoteFields" : false};

    // private
    let self = this;

    // Sets up all of the UI elements and handles callbacks.
    let applyUI = function (model) {
        window.localStorage.removeItem('redirectorasesults');
        self.Token(model.CSRFToken);
        _breadcrumbs.pop("Search Results");
        let bcs = _breadcrumbs.render();
        $getE('.breadcrumbheader').html(bcs);
        $getE('.breadcrumbheader a').click(function () {
            _breadcrumbs.load(_container.parent(), $(this).data('name'));
        });

        framework.showSpinner("Loading...");

        // Lots of variables here. 
        // columnsdata: Contains data about all of the columns we view. Specifically the 'FieldName', which
        //              is needed for the order-by clauses.
        // columns: Column data to be used when initializing the DataTables result table.
        // htmlcolumns: Columns to be written out as TH's. This is actual html.
        // selectedcolumns: Columns for the selected values.  Just to be appended to the columnsdata array.
        let columnsdata = [];
        let columns = [];
        let initOrder = [];
        let htmlcolumns = $("<tr></tr>");
        let selectedcolumns = [];

        // Add just the fields the user selected.
        if (!model.SelectFields) {
            model.SelectFields = [];
        }
        $.each(model.SelectFields, function (i, e) {
            e.Title = cleanDataTableTitle(e.ReportTitle);
            e.Type = e.DataType;
            e.Name = e.ReportTitle;
            selectedcolumns.push(e);
        });

        // Append the standard columns.
        columns.push({ data: 'DepositDateString', name:'SelectedPrint', sortable: false, orderable: false, width: 10 });
        columnsdata.push({ });
        htmlcolumns.append('<th><input type="checkbox" id="set-all-printmode" /></th>');

        columns.push({ data: 'DepositDateString', name: 'Images', sortable: false, orderable: false, width: 90 });
        columnsdata.push({ });
        htmlcolumns.append('<th></th>');

        // Append the DE columns.
        let defields = [];
        $.each(selectedcolumns, function (i, de) {
            let gp = $.grep(columns, function (value) { return value.data === de.Title; });
            if (gp.length === 0) {
                let title = de.Title;
                if (de.Name && de.Name.length > 0)
                    title = de.Name;
                let col = {
                    data: cleanDataTableTitle(de.Title),
                    name: title.replace(/\s/g, '') //remove spaces
                };
                htmlcolumns.append("<th>" + title + "</th>");
                // If type is either number or currency, right-justify.
                // Weak comparison (not ===) because sometimes this is a string.
                if (de.Type == "7" || de.Type == "6")
                    col.class = "alignRight";
                columns.push(col);
                columnsdata.push(de);
            }
        });

        $getE('#resultsTable thead').append(htmlcolumns);

        // Set all of the widths for our columns.
        let columndefs = [];
        for (let i = 0; i < columns.length; i++) {
            let def = {};
            if (columns[i].width) {
                // Just use the width defined above (hard coded).
                let w = columns[i].width;
                delete columns[i].width;
                def.width = w;
            }
            else {
                // We're measuring the text here so we know how long the header for each column is.
                let iconwidth = 25;
                let text = $getE('#resultsTable thead th').eq(i).text();
                let measuredw = $.fn.textWidth(text) + iconwidth;
                def.width = measuredw;

                // Minimum width.  (for columns like 'DDA')
                if (def.width < 80)
                    def.width = 80;
            }

            def.targets = i;
            columndefs.push(def);
        }

        let indSort = columnsdata.map(function (e) { return e.FieldName; }).indexOf(model.OrderBy);
        indSort = indSort >= 0 ? indSort : 0;
        initOrder.push([indSort, model.OrderDirection]);

        // Initialize our table.
        _resultTable = $getE('#resultsTable').DataTable({
            autoWidth: false,
            processing: false,
            serverSide: true,
            displayStart: (_model.length && _model.length > 0) ? _model.start : 0,
            pageLength: (_model.length && _model.length > 0) ? _model.length : 10,
            ajax: {
                url: '/RecHubViews/AdvancedSearch/GetAdvancedSearch',
                type: 'POST',
                data: function (d) {
                    d.Workgroup = _model.Workgroup;
                    d.DateFrom = _model.DateFrom;
                    d.DateTo = _model.DateTo;
                    d.Criteria = _model.Criteria;
                    d.SelectFields = _model.SelectFields;
                    d.OrderBy = _model.OrderBy;
                    d.OrderByDisplayName = _model.OrderByDisplayName;
                    d.OrderDirection = _model.OrderDirection;
                    d.PaymentSourceKey = _model.PaymentSourceKey;
                    d.PaymentTypeKey = _model.PaymentTypeKey;
                    d.CSRFToken = self.Token();
                }
            },
            columns: columns,
            columnDefs: columndefs,
            createdRow: function (row, data, index) {
                let api = this.api();

                // Add the selective print mode checkboxes.
                let colindex = api.column('SelectedPrint:name')[0][0];
                let td = api.cells(row, colindex).nodes()[0];
                $(td).empty()
                    .append('<input type="checkbox" class="selective-print-mode-checkbox" value="">');

                // Appending images.
                colindex = api.column('Images:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                $(td).empty();

                let $images = $('<span class="as-image-column">');
                if (data.ShowPaymentIcon === true)
                    $images.append('<i class="fa fa-money fa-lg paymentImage" title="View Payment Image"></i>');
                if (data.ShowDocumentIcon === true)
                    $images.append('<i class="fa fa-file-o fa-lg docImage" title="View Transaction Image"></i>');
                if (data.ShowAllIcon === true)
                    $images.append('<i class="fa fa-picture-o fa-lg allImage" title="View All Images for Transaction"></i>');
                if (data.IsMarkSense === true)
                    $images.append('<i class="fa fa-check-square-o fa-lg marksense-icon" title="Transaction contains marksense data"></i>')
                $(td).append($images);

                // Adding the drilldown links.
                colindex = api.column('Transaction:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                let link = $('<a href="#" title="View Transaction" class="transaction-link"></a>');
                link.text(data.TransactionSequence);
                $(td).html(link);

                colindex = api.column('BatchNumber:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                link = $('<a href="#" title="View Batch" class="batch-link"></a>');
                link.text(data.BatchNumber);
                $(td).html(link);

                colindex = api.column('BatchID:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                link = $('<a href="#" title="View Batch" class="batch-link"></a>');
                link.text(data.SourceBatchId);
                $(td).html(link);
            },
            order: initOrder,
            scrollX: true
        });

        // Hide the SelectedPrint column by default.
        _resultTable.column(0).visible(false);

        $getE('#view-selected-button').click(function() {
            displaySelectedTransactions();
        });

        $('#advancedSearchResultsPortlet').on('click', '#set-all-printmode', function () {
            setAllViewSelected($("#set-all-printmode").is(':checked'));
        });

        // Hook up Batch and Transaction drill down links.
        $getE('#resultsTable tbody').on('click', '.batch-link', function () {
            let rowdata = _resultTable.row($(this).parents('tr').first()).data();
            let url = '/UI/BatchDetail';
            let pageinfo = _resultTable.page.info();
            _model.length = pageinfo.length;
            _model.start = pageinfo.start;
            _breadcrumbs.register("Search Results", "/RecHubViews/AdvancedSearch/Results", _model, "/Framework/?tab=9");
            //framework.loadByContainer("/RecHubViews/BatchDetail/IndexResults", _container.parent(), data);
            window.location.href = url;
            let bdrequest = {
                depositDate : rowdata.DepositDateString,
                workGroup : rowdata.WorkgroupId,
                batchId : rowdata.BatchId,
                bankId: rowdata.BankId
            };
            window.localStorage.setItem('batchdetaildata', JSON.stringify(bdrequest));
        });

        $getE('#resultsTable tbody').on('click', '.transaction-link', function () {
            let rowdata = _resultTable.row($(this).parents('tr').first()).data();
            let url = '/UI/TransactionDetail';
            let pageinfo = _resultTable.page.info();
            _model.length = pageinfo.length;
            _model.start = pageinfo.start;
            _breadcrumbs.register("Search Results", "/RecHubViews/AdvancedSearch/Results", _model, "/Framework/?tab=9");
            let tdRequest = {
                bankId: rowdata.BankId,
                workgroupId: rowdata.WorkgroupId,
                batchId: rowdata.BatchId,
                depositDate: rowdata.DepositDateString,
                transactionId: rowdata.TransactionId,
                transactionSequence: rowdata.TxnSequence
            };
            window.location.href = url;
            window.localStorage.setItem('transactiondetaildata', JSON.stringify(tdRequest));
        });

        // Hook up image clicks.
        $getE('#resultsTable tbody').on('click', '.paymentImage', function () {
            let rowdata = _resultTable.row($(this).parents('tr').first()).data();
            _imageservice.displaySinglePayment(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'), rowdata.TransactionId, rowdata.BatchSequence);
        });

        $getE('#resultsTable tbody').on('click', '.docImage', function () {
            let rowdata = _resultTable.row($(this).parents('tr').first()).data();
            _imageservice.displayTransactionDocuments(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'), rowdata.TransactionSequence);
        });

        $getE('#resultsTable tbody').on('click', '.allImage', function () {
            let rowdata = _resultTable.row($(this).parents('tr').first()).data();
            _imageservice.displayTransactionImages(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'), rowdata.TransactionId);
        });
        $getE('#pdf-view-button').click(function () {
            _imageservice.displayASPdf(model);
        });

        $getE('#text-view-button').click(function () {
            window.location = "/RecHubViews/AdvancedSearch/DownloadAsText?ExcludeCommasFromNumeric=" + downloadAsTextOptions.ExcludeCommasFromNumeric + "&DoNotQuoteFields=" + downloadAsTextOptions.DoNotQuoteFields;
        });

        $getE('#ExcludeCommasFromNumeric').click(function () {
            downloadAsTextOptions.ExcludeCommasFromNumeric = $getE('#ExcludeCommasFromNumeric').is(":checked");
            window.localStorage.setItem('downloadAsTextOptions', JSON.stringify(downloadAsTextOptions));
        });
        $getE('#DoNotQuoteFields').click(function () {
            downloadAsTextOptions.DoNotQuoteFields = $getE('#DoNotQuoteFields').is(":checked");
            window.localStorage.setItem('downloadAsTextOptions', JSON.stringify(downloadAsTextOptions));
        });

        $getE('#download-results-button').click(function () {
            _imageservice.displayDownloadAS(model);
        });

        // Get rid of the search functionality.
        $getE('#resultsTable input').unbind();

        // Cancel the default Error mode.  This prevents a standard 'alert'.
        $.fn.dataTable.ext.errMode = 'none';

        // Wire up pre and post load events.
        $getE('#resultsTable').on('preXhr.dt', function (e, settings, data) {
            framework.showSpinner('Loading...');

            // Add the order by clause to the sent data.
            let column = columnsdata[data.order[0].column];
            data.OrderBy = column.OrderByName;
            data.OrderDirection = data.order[0].dir;
            _model.OrderBy = data.OrderBy;
            _model.OrderDirection = data.OrderDirection;
            if (column.ReportTitle) {
                data.OrderByDisplayName = column.ReportTitle;
                _model.OrderByDisplayName = data.OrderByDisplayName;
            }
        });
        $getE('#resultsTable').on('xhr.dt', function (e, settings, json, xhr) {
            framework.hideSpinner();

            if (json.Error) {
                // Gracefully handle missing data.
                json.data = [];
                json.recordsTotal = 0;
                framework.errorToast(_container, json.Error);
                return;
            }

            // Set the DE Values in the rows' objects.
            self.WorkgroupText(json.metadata.WorkgroupName);
            self.MaxPrintableRows(json.metadata.MaxRowsForDownload);
            self.TotalRecords(json.recordsTotal);
            self.HasDownloadPermission(json.metadata.HasDownloadPermission);

            // Set the totals for the bottom text.
            self.TotalPaymentCount(json.totals.CheckCount);
            self.TotalPaymentSum(_formatter.formatCurrency(json.totals.CheckTotal));
            self.SearchCriteriaText(json.metadata.CriteraString);

            $.each(json.data, function (i, row) {
                $.each(row.DataEntry, function (id, de) {
                    let value = de.Value;
                    if ((de.Type === "7" || de.FieldName.toLowerCase() === "amount") && de.Value && de.Value.length > 0)
                        value = _formatter.formatCurrency(de.Value);
                    row[cleanDataTableTitle(de.Title)] = value;
                });
            });

            $getE('#set-all-printmode')
                .prop('checked', false);
            setAllViewSelected(false);
            setupSelectOptions();
        });

        // Selective print mode toggleing.
        $getE('#selective-print-mode-button').click(function () {
            togglePrintModeUI();
        });

        // Handler for changing the view-selected checkboxes.
        $getE('#resultsTable').on('change', '.selective-print-mode-checkbox', function() {
            setupSelectOptions();
        });
    };

    // Datatables (component, not C# data tables) doesn't seem to support these special characters.
    let cleanDataTableTitle = function (title) {
        return title.replaceAll('.', '')
            .replaceAll('[', '')
            .replaceAll(']', '')
            .replaceAll('/', '');
    }

    let setupSelectOptions = function () {
        // Init the dropdown box.
        let items = [
            { Text: 'Print All', Value: 'All' },
        ];
        let inputs = $getE('.selective-print-mode-checkbox:checked');
        let selecteddata = $.map(inputs, function(e) {
            return _resultTable.row($(e).parents('tr').first()).data();
        });
        let displaychecks = $.grep(selecteddata, function(e) {
            return e.ShowPaymentIcon === true;
        }).length > 0;
        let displaydocuments = $.grep(selecteddata, function(e) {
            return e.ShowDocumentIcon === true;
        }).length > 0;

        if (displaychecks === true)
            items.push({ Text: 'Print Payments', Value: 'ChecksOnly' });
        if (displaydocuments === true)
            items.push({ Text: 'Print Documents', Value: 'DocumentsOnly' });

        if (_selectiveprintsetup === false) {
            $getE('#selective-type-picker').wfsSelectbox({
                items: items,
                callback: function (v) { if (v) { self.SelectedPrintMode(v); } },
                displayField: 'Text',
                idField: 'Value',
                allowClear: false
            });
            $getE('#selective-type-picker').wfsSelectbox('setValue', 'All');
            _selectiveprintsetup = true;
        }
        else {
            $getE('#selective-type-picker').wfsSelectbox('setItems', items);
            $getE('#selective-type-picker').wfsSelectbox('setValue', 'All');
        }
    };

    let setAllViewSelected = function (val) {
        $getE('.selective-print-mode-checkbox')
            .prop('checked', val)
            .change();
    };

    let displaySelectedTransactions = function () {
        // gather data first.
        let inputs = $getE('.selective-print-mode-checkbox:checked');
        if (inputs.length == 0) {
            framework.errorToast(_container, "Please select at least one record.");
            return;
        }

        let data = [];
        $.each(inputs, function () {
            let rowdata = _resultTable.row($(this).parents('tr').first()).data();
            data.push({
                BankId: rowdata.BankId,
                WorkgroupId: rowdata.WorkgroupId,
                BatchId: rowdata.BatchId,
                DepositDate: rowdata.DepositDateString,
                TransactionId: rowdata.TransactionId,
                BatchNumber: rowdata.BatchNumber
            });
        });

        let dto = { Transactions: data, SelectMode: self.SelectedPrintMode().Value };
        _imageservice.displayASSelected(dto);
    };

    let togglePrintModeUI = function() {
        self.IsInSelectivePrintMode(!self.IsInSelectivePrintMode());
        let buttontext = IsInSelectivePrintMode()
            ? 'Exit Selective Print Mode'
            : 'Selective Print Mode';
        self.PrintModeText(buttontext);
        _resultTable.column(0).visible(IsInSelectivePrintMode());
        if (IsInSelectivePrintMode())
            $getE('#s2id_selective-type-picker').show();
        else
            $getE('#s2id_selective-type-picker').hide();
        if (self.IsInSelectivePrintMode() === true)
            setupSelectOptions();
    };

    let search = function (model) {
        _resultTable.ajax.reload();
    };

    // Public Bindables.
    self.WorkgroupText = ko.observable('');
    self.PrintModeText = ko.observable('Selective Print Mode');
    self.IsInSelectivePrintMode = ko.observable(false);
    self.SelectedPrintMode = ko.observable({});
    self.MaxPrintableRows = ko.observable(0);
    self.TotalRecords = ko.observable(0);
    self.HasDownloadPermission = ko.observable(false);
    self.TotalPaymentCount = ko.observable(0);
    self.TotalPaymentSum = ko.observable(0);
    self.SearchCriteriaText = ko.observable('');
    self.Token = ko.observable('');
    // Constructor
    function advancedSearchResultsViewModel() {

    }

    // Public methods
    advancedSearchResultsViewModel.prototype = {
        constructor: advancedSearchResultsViewModel,
        init: function (container, model) {
            _container = container;
            _model = model;
            $getE = function (b) { return _container.find(b); };

            // Adding a method to the string object.
            String.prototype.replaceAll = function (search, replace) {
                if (replace === undefined)
                    return this.toString();
                return this.replace(new RegExp('[' + search + ']', 'g'), replace);
            };

            // Function to measure text width.
            $.fn.textWidth = function (text, font) {
                if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<strong>').hide().appendTo(document.body);
                $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
                return $.fn.textWidth.fakeEl.width();
            };

            var savedDownloadAsTextOptions = JSON.parse(window.localStorage.getItem("downloadAsTextOptions"));
            if (savedDownloadAsTextOptions !== null) {
                downloadAsTextOptions = savedDownloadAsTextOptions;
            }
            if (downloadAsTextOptions.ExcludeCommasFromNumeric) {
                $getE('#ExcludeCommasFromNumeric').prop('checked', true);
            }
            if (downloadAsTextOptions.DoNotQuoteFields) {
                $getE('#DoNotQuoteFields').prop('checked', true);
            }

            applyUI(model);
            ko.applyBindings(self, container.get(0));

        },
        dispose: function () {
            // Function to clean up our data.
            $getE('#resultsTable tbody').off('click');
            $getE('#resultsTable thead').off('click');
            $getE('#resultsTable').off('change');
            $('#advancedSearchResultsPortlet').off('click');
            self.WorkgroupText('');
            self.PrintModeText('Selective Print Mode');
            self.IsInSelectivePrintMode(false);
            self.MaxPrintableRows(0);
            self.TotalRecords(0);
            self.HasDownloadPermission(false);
            self.TotalPaymentCount(0);
            self.TotalPaymentSum(0);
            self.SearchCriteriaText('');
            _selectiveprintsetup = false;

            if (_resultTable) {
                _resultTable.destroy();
            }
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return advancedSearchResultsViewModel;
});