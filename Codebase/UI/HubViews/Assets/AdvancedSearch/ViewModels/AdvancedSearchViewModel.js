﻿// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.

define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js', '/RecHubViews/Assets/Shared/Controls/searchFilter.js', 'advancedSearchSelectFieldPicker',
    '/RecHubViews/Assets/Shared/Formatting.js', '/RecHubViews/Assets/Shared/ConfirmationDialog.js',
    '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap',
    'bootstrap', 'wfs.select', 'wfs.treeselector', 'select2'],
function ($, framework, ko, breadcrumbs, advancedSearchFilter, advancedSearchSelectFieldPicker, formatting, confirmationdialog) {

    var _container = $('#advancedSearchPortlet');
    var $getE;
    var _workgroupSelector;
    var _breadcrumbs = new breadcrumbs();
    var _formatting = new formatting();
    var _dialog = new confirmationdialog();
    var _advancedSearchFilter;
    var _advancedSearchSelectFieldPicker;
    var _treeInitialized = false;
    var _model;
    var _loadingquery = false;
    var _dataentryitems;

    var _workgroupsloaded = false;
    var _paymenttypesloaded = false;
    var _paymentsourcesloaded = false;
    var _queriesloaded = false;

    var _orderby = [{ OrderBy: "Checks.Account", OrderDirection: "asc", Text: "Account Number" },
                    { OrderBy: "Batch.BatchID", OrderDirection: "asc", Text: "Batch ID" },
                    { OrderBy: "Batch.BatchNumber", OrderDirection: "asc", Text: "Batch" },
                    { OrderBy: "Checks.Amount", OrderDirection: "asc", Text: "Payment Amount/Ascending" },
                    { OrderBy: "Checks.Amount", OrderDirection: "desc", Text: "Payment Amount/Descending" },
                    { OrderBy: "Batch.DepositDate", OrderDirection: "desc", Text: "Deposit Date" },
                    { OrderBy: "Checks.RT", OrderDirection: "asc", Text: "RT" },
                    { OrderBy: "Checks.Serial", OrderDirection: "asc", Text: "Check/Trace/Ref Number" },
                    { OrderBy: "Transactions.TxnSequence", OrderDirection: "asc", Text: "Transaction Sequence" },
                    { OrderBy: "Batch.PaymentSource", OrderDirection: "asc", Text: "Payment Source/Ascending" },
                    { OrderBy: "Batch.PaymentSource", OrderDirection: "desc", Text: "Payment Source/Descending" },
                    { OrderBy: "Batch.PaymentType", OrderDirection: "asc", Text: "Payment Type/Ascending" },
                    { OrderBy: "Batch.PaymentType", OrderDirection: "desc", Text: "Payment Type/Descending" },
                    { OrderBy: "DDA", OrderDirection: "asc", Text: "DDA/Ascending" },
                    { OrderBy: "DDA", OrderDirection: "desc", Text: "DDA/Descending" }
    ];
    // private
    var self = this;

    var doIfLoaded = function (callback) {
        if (_workgroupsloaded == true
            && _paymenttypesloaded == true
            && _paymentsourcesloaded == true
            && _queriesloaded == true)
            callback();
    }

    var loadDefaultQueryIfLoaded = function () {
        doIfLoaded(function () {

            // Use the query from the model if it was passed (from manage queries).
            var defquery = _model.QueryId
                ? $.grep(self.StoredQueries(), function (e) { return e.Id == _model.QueryId; })
                : $.grep(self.StoredQueries(), function (e) { return e.IsDefault == true; });

            defquery = defquery.length > 0
                ? defquery[0]
                : null;

            var populateFormSelections = defquery && !_model.Workgroup;
            self.StoredQuery(defquery);
            if (defquery) {
                $getE('#query-selector').wfsSelectbox('setValue', defquery.Id, populateFormSelections);
            }
        });
    };

    // Sets up all of the UI elements and handles callbacks.
    var applyUI = function (model) {

        _model = model;
        self.Token(model.CSRFToken);
        // Standard Breadcrumb logic.
        let bcdata = _breadcrumbs.pop("Advanced Search");

        var bcs = _breadcrumbs.render();
        $getE('.breadcrumbheader').html(bcs);
        if (!model.Workgroup && !model.DateTo && !model.DateFrom && bcdata && bcdata.Data) {
            for (let attrname in bcdata.Data) {
                if (bcdata.Data.hasOwnProperty(attrname)) {
                    model[attrname] = bcdata.Data[attrname];
                }
            }
        }

        if (model.Workgroup && model.Workgroup.length > 0) {
            self.Workgroup(model.Workgroup);
            self.DateTo(model.DateTo);
            self.DateFrom(model.DateFrom);
            var ordBy = $.grep(_orderby, function (e) { return model.OrderBy == e.OrderBy && model.OrderDirection == e.OrderDirection; });
            if (ordBy.length != 0) {
                self.OrderBy(ordBy[0]);
            }
            if (_model.PaymentTypeKey >= 0) {
                self.PaymentType(_model.PaymentTypeKey);
            }
            else {
                self.PaymentType(-1);
            }
            if (_model.PaymentSourceKey >= 0) {
                self.PaymentSource(_model.PaymentSourceKey);
            }
            else {
                self.PaymentSource(-1);
            }
        }

        $getE('#dateFrom').wfsDatePicker({ callback: function () { self.DateFrom($getE('#dateFrom').wfsDatePicker('getDate')); } });
        $getE('#dateFrom').wfsDatePicker('setDate', self.DateFrom());

        $getE('#dateTo').wfsDatePicker({ callback: function () { self.DateTo($getE('#dateTo').wfsDatePicker('getDate')); } });
        $getE('#dateTo').wfsDatePicker('setDate', self.DateTo());

        _workgroupSelector = $getE('#as-workgroup').wfsTreeSelector({
            useExpander: true,
            entityURL: '/RecHubRaamProxy/api/entity',
            callback: selectedTreeItem,
            dataCallback: treeDataCallback,
            expanderTitle: "Select Workgroup",
            entitiesOnly: false,
            closeOnSelectNonNode: false
        });
        //sort fields to ensure they are always sorted alphabetically
        _orderby = _orderby.sort(function (a, b) { return a.Text.localeCompare(b.Text); });
        $getE('#query-sort').wfsSelectbox({
            items: _orderby,
            displayField: "Text",
            idField: "Text",
            callback: function () {
                var match = $.grep(_orderby, function (e) {
                    return $getE('#query-sort').wfsSelectbox('getValue') == e.Text;
                });
                self.OrderBy(match[0]);
            },
            width: "300px"
        });

        if (self.OrderBy())
            $getE('#query-sort').wfsSelectbox('setValue', self.OrderBy().Text);


        //get payment types
        framework.doJSON('/RecHubViews/Shared/GetPaymentTypes', null, paymentTypesCallBack);

        //get payment sources
        framework.doJSON('/RecHubViews/Shared/GetPaymentSources', null, paymentSourcesCallBack);

        // Get the user's stored queries.
        loadQueries();

        _advancedSearchFilter = new advancedSearchFilter($getE('#query-filter'), 'as-filter');
        _advancedSearchFilter.init();

        _advancedSearchSelectFieldPicker = new advancedSearchSelectFieldPicker($getE('#select-picker'), 'ReportTitle');
        _advancedSearchSelectFieldPicker.init();

        $getE('#search-button').click(function () {
            validateAndSearch();
        });

        $getE('#clear-search-button')
            .click(function () {
                self.DateFrom(new Date());
                self.DateTo(new Date());
                $getE('#dateFrom').wfsDatePicker('setDate', self.DateFrom());
                $getE('#dateTo').wfsDatePicker('setDate', self.DateTo());
                self.PaymentType(-1);
                $getE('#paymentType').wfsSelectbox('setValue', self.PaymentType());
                self.PaymentSource(-1);
                $getE('#paymentSource').wfsSelectbox('setValue', self.PaymentSource);
                self.OrderBy(null);
                $getE('#query-sort').wfsSelectbox('setValue', null);
                _advancedSearchFilter.clear();
                if (_treeInitialized) {
                    _workgroupSelector.wfsTreeSelector('reset');
                    self.Workgroup(null);
                }
                _advancedSearchSelectFieldPicker.clearSelected();
                self.StoredQuery({});
                $getE('#query-selector').wfsSelectbox('setValue', self.StoredQuery());
            });

        // Wire up events for the query dialog.
        $getE('.btn-save-query').click(function () {
            var data = getCurrentQuery();

            // Validation first.
            if (!data.Workgroup || data.Workgroup == '') {
                framework.errorToast(_container, 'Please select a workgroup first.');
                return;
            }
            var criteriaerrors = _advancedSearchFilter.validate();
            if (criteriaerrors && criteriaerrors.length > 0){
                framework.errorToast(_container, criteriaerrors[0]);
                return;
            }

            if (!self.StoredQuery() || !self.StoredQuery().Id) {
                // we need to create a new query.
                framework.loadByContainer('/RecHubViews/AdvancedSearch/QueryDialog', framework.getModalContent(), data, framework.openModal({
                    closeCallback: dialogCallback
                }), {});
            }
            else {
                // we need to save the current query without the dialog.
                data.QueryId = self.StoredQuery().Id;
                data.QueryName = self.StoredQuery().Name;
                data.QueryDescription = self.StoredQuery().Description;
                data.QueryIsDefault = self.StoredQuery().IsDefault;
                data.CSRFToken = $getE('input[name="hashtoken"]').val();
                framework.showSpinner('Loading...');
                framework.doJSON('/RecHubViews/AdvancedSearch/SaveQuery', data, function (result) {
                    if (result.Error) {
                        framework.errorToast(_container, result.Error);
                        framework.hideSpinner();
                        return;
                    }
                    dialogCallback({ QueryId: result.Id });
                });
            }
        });
    };

    var dialogCallback = function (callbackdata) {
        if (callbackdata && callbackdata.cancel == true)
            return;
        var id;
        if (callbackdata && callbackdata.QueryId)
            id = callbackdata.QueryId;
        loadQueries(id);
    };

    var loadQueries = function (id) {
        framework.doJSON('/RecHubViews/AdvancedSearch/GetStoredQueries', null, function (data) {
            data.LoadQueryId = id;
            storedQueriesCallback(data);
        });
    };

    var paymentTypesCallBack = function (data) {

        if (!data || !data.success) {
            framework.errorToast(_container, 'Error Loading Payment Types.');
            return;
        }
        self.PaymentTypes(data.data);
        $getE('#paymentType').wfsSelectbox({
            items: self.PaymentTypes(),
            callback: function () { self.PaymentType($getE('#paymentType').wfsSelectbox('getValue') ? $getE('#paymentType').wfsSelectbox('getValue') : -1); },
            placeHolder: '-- All --',
            displayField: 'LongName',
            idField: 'PaymentTypeKey',
            width: "300px"
        });
        $getE('#paymentType').wfsSelectbox('setValue', self.PaymentType());
        _paymenttypesloaded = true;
        loadDefaultQueryIfLoaded();
    };

    var paymentSourcesCallBack = function (data) {
        if (!data || !data.success) {
            framework.errorToast(_container, 'Error Loading Payment Sources.');
            return;
        }
        self.PaymentSources(data.data);
        $getE('#paymentSource').wfsSelectbox({
            items: self.PaymentSources(),
            callback: function () { self.PaymentSource($getE('#paymentSource').wfsSelectbox('getValue') ? $getE('#paymentSource').wfsSelectbox('getValue') : -1); },
            placeHolder: '-- All --',
            displayField: 'LongName',
            idField: 'PaymentSourceKey',
            width: "300px"
        });
        $getE('#paymentSource').wfsSelectbox('setValue', self.PaymentSource());
        _paymentsourcesloaded = true;
        loadDefaultQueryIfLoaded();
    };

    var storedQueriesCallback = function (data) {
        if (!data || !data.success) {
            framework.errorToast(_container, 'Error Loading Stored Queries.');
            return;
        }
        self.StoredQueries(data.data);
        if (_queriesloaded == false) {
            $getE('#query-selector').wfsSelectbox({
                items: self.StoredQueries(),
                callback: selectedStoredQuery,
                placeHolder: 'Select Query...',
                displayField: 'Name',
                idField: 'Id',
                width: "300px"
            });
            _queriesloaded = true;
        }
        else {
            $getE('#query-selector').wfsSelectbox('setItems', self.StoredQueries());
        }

        // This loads the query that the dialog returns.
        if (data.LoadQueryId) {
            var q = $.grep(self.StoredQueries(), function (e) {
                return e.Id == data.LoadQueryId
            });
            q = q.length > 0 ? q[0] : null;
            if (q) {
                self.StoredQuery(q);
                $getE('#query-selector').wfsSelectbox('setValue', self.StoredQuery().Id);
            }
        }
        else {
            loadDefaultQueryIfLoaded();
        }
    };

    var selectedStoredQuery = function (selected) {
        if (!selected) {
            self.StoredQuery(selected);
            return;
        }
        framework.showSpinner('Loading...');
        framework.doJSON('/RecHubViews/AdvancedSearch/GetStoredQuery', {
            id: selected.Id
        }, function (result) {
            if (!result || result.success == false || !result.data || !result.data.Query) {
                framework.errorToast(_container, 'Error loading query.');
                return;
            }
            self.StoredQuery(result.data);
            loadQuery(result.data);
        });

    };

    var loadQuery = function (storedquery) {
        // Setting this tells the workgroup selector to load in the select fields 
        // and where clauses when it's done loading in the workgroups DE fields.
        _loadingquery = true;
        var query = storedquery.Query;

        // Transform the data into what the filter expects.
        $.each(query.WhereClauses, function (i, e) {
            query.WhereClauses[i] = {
                Column: e,
                Operator: e.Operator,
                Value: e.Value
            };
        });

        var workgroup = query.BankId + '|' + query.WorkgroupId;
        setWorkgroup(workgroup);

        var order = $.grep(_orderby, function (element) {
            return element.OrderBy == query.OrderBy && element.OrderDirection == query.OrderDirection
        })[0];
        var orderval = order ? order.Text : null;
        $getE('#query-sort').wfsSelectbox('setValue', orderval);
        self.OrderBy(order);

        $getE('#paymentType').wfsSelectbox('setValue', query.PaymentTypeKey);
        self.PaymentType(query.PaymentTypeKey);

        $getE('#paymentSource').wfsSelectbox('setValue', query.PaymentSourceKey);
        self.PaymentSource(query.PaymentSourceKey);
    };

    var setWorkgroup = function(id) {
        self.Workgroup(id);
        if (_treeInitialized) {
            _workgroupSelector.wfsTreeSelector('setSelection', self.Workgroup());
        }
        else {
            // only have access to 1 workgroup
            var workgroups = _workgroupSelector.wfsTreeSelector('getWorkgroups');
            var element = $('<div><input value="'
              + workgroups[0].label
              + '" class="form-control input-md" type="text" readonly style="width: 300px;display:inline;"></div>');
            $getE('#as-workgroup').html(element);
            selectedTreeItem(workgroups[0], false);
        }
    };

    var formatCurrency = function (n) {
        return '$' + n.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,");
    };

    var selectedTreeItem = function (selected, initial) {
        var title = null;

        if (selected && !initial) {
            if (selected.isNode) {
                title = 'Selected: ' + selected['label'];
                _workgroupSelector.wfsTreeSelector('setTitle', title);
                self.Workgroup(selected.id);

                // Load up the DE columns.
                framework.showSpinner("Loading...");
                framework.doJSON('/RecHubViews/AdvancedSearch/GetAdvancedFindData', {
                    Workgroup: selected.id,
                    CSRFToken: $getE('input[name="hashtoken"]').val()
                }, function (data) {
                    if (data.Error) {
                        framework.errorToast(_container, data.Error);
                        framework.hideSpinner();
                        return;
                    }

                    framework.hideSpinner();
                    var filteritems = $.grep(data, function(e){
                        return e.IsDisplayFieldOnly === false;
                    });
                    _advancedSearchFilter.setItems(filteritems);
                    _advancedSearchSelectFieldPicker.setItems(data);
                    _dataentryitems = data;
                    if (_model && !_model.Loaded) {
                        if (_model.Criteria)
                            _advancedSearchFilter.setSelected(_model.Criteria);
                        if (_model.SelectFields)
                            _advancedSearchSelectFieldPicker.setSelected(_model.SelectFields);
                        _model.Loaded = true;
                    }
                    if (_loadingquery == true) {
                        _advancedSearchFilter.setSelected(self.StoredQuery().Query.WhereClauses);
                        _advancedSearchSelectFieldPicker.setSelected(self.StoredQuery().Query.SelectFields);
                        _loadingquery = false;
                    }
                });


            }
            else {
                framework.errorToast(_container, 'Please select a single workgroup.');
                framework.hideSpinner();
                return false;
            }
        }
    };

    var treeDataCallback = function (data) {
        var entities = _workgroupSelector.wfsTreeSelector('getEntities');
        var workgroups = _workgroupSelector.wfsTreeSelector('getWorkgroups');
        var ecount = entities.length;
        var wcount = workgroups.length;

        if (ecount === 1 && wcount === 1) {
            var element = $('<div><input value="'
              + workgroups[0].label
              + '" class="form-control input-md" type="text" readonly style="width: 300px;display:inline;"></div>');
            $getE('#as-workgroup').html(element);
            self.Workgroup(workgroups[0].id);
            selectedTreeItem(workgroups[0], false);
        }
        else {
            _workgroupSelector.wfsTreeSelector('initialize');
            _treeInitialized = true;
            if (self.Workgroup() != undefined && self.Workgroup() != '')
                _workgroupSelector.wfsTreeSelector('setSelection', self.Workgroup());
        }
        _workgroupsloaded = true;
        loadDefaultQueryIfLoaded();
    };

    var validateAndSearch = function () {
        var error;

        if (!self.Workgroup() || self.Workgroup() == '')
            error = 'Please select a single workgroup';
        else {
            var criteriaerrors = _advancedSearchFilter.validate();
            error = criteriaerrors[0];
        }

        // Kick out early if we have a validation error.
        if (error) {
            framework.errorToast(_container, error);
            return;
        }

        var data = getCurrentQuery();
        search();
    };

    var getCurrentQuery = function () {
        self.DateFrom($getE('#dateFrom').wfsDatePicker('getDate'));
        self.DateTo($getE('#dateTo').wfsDatePicker('getDate'));
        //Deposit Date is default if no orderby was chosen
        var ordBy = this.OrderBy() ? this.OrderBy() : _orderby[6];
        var pSource = this.PaymentSource();
        var pType = this.PaymentType();
        var queryId = this.StoredQuery() ? this.StoredQuery().ID : null;
        var data = {
            Workgroup: this.Workgroup(),
            DateTo: this.DateTo(),
            DateFrom: this.DateFrom(),
            Criteria: _advancedSearchFilter.getSelected(),
            SelectFields: _advancedSearchSelectFieldPicker.getSelected(),
            OrderBy: ordBy.OrderBy,
            PaymentSourceKey: pSource,
            PaymentTypeKey: pType,
            OrderDirection: ordBy.OrderDirection,
            QueryId: queryId
        };
        return data;
    };

    var search = function () {
        var data = getCurrentQuery();

        // Register our breadcrumb.
        _breadcrumbs.register("Advanced Search", "/RecHubViews/AdvancedSearch/", data,  "/Framework/?tab=9");

        // Override the select fields if the user didn't select anything by adding in the defaults.
        if (data.SelectFields.length === 0) {
            let defaults = $.grep(_dataentryitems, function(e) {
                return e.IsDefault && e.IsDefault === true;
            });
            data.SelectFields = defaults;
        }

        framework.loadByContainer('/RecHubViews/AdvancedSearch/Results', _container.parent(), data);
    };


    // Constructor
    function advancedSearchViewModel() {

    }

    // Public properties
    this.DateFrom = ko.observable(Date.now());
    this.DateTo = ko.observable(Date.now());
    this.Workgroup = ko.observable('');
    this.OrderBy = ko.observable(null);
    this.PaymentType = ko.observable(-1);
    this.PaymentTypes = ko.observable([]);
    this.PaymentSource = ko.observable(-1);
    this.PaymentSources = ko.observable([]);
    this.StoredQuery = ko.observable({});
    this.StoredQueries = ko.observable([]);
    this.Token = ko.observable('');
    // Public methods
    advancedSearchViewModel.prototype = {
        constructor: advancedSearchViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) {
                return _container.find(b);
            };
            let data = JSON.parse(window.localStorage.getItem('redirectorasesults'));
            if (data) {
                framework.loadByContainer('/RecHubViews/AdvancedSearch/Results', _container.parent(), data);
            } else {
                applyUI(model);
                ko.applyBindings(self, container.get(0));
            }
        },
        dispose: function () {
            // Function to clean up our data.
            PaymentType(-1);
            PaymentSource(-1);
            StoredQuery({});
            StoredQueries([]);
            PaymentTypes([]);
            PaymentSources([]);
            DateFrom(null);
            DateTo(null);
            Workgroup(null);
            OrderBy(null);
            Results = [];

            _treeInitialized = false;
            _queriesloaded = false;
            _loadingquery = false;
            _model = {};
            _workgroupsloaded = false;
            _paymenttypesloaded = false;
            _paymentsourcesloaded = false;
            _queriesloaded = false;
            _dataentryitems = [];

            _advancedSearchFilter.dispose();
            _advancedSearchSelectFieldPicker.dispose();
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return advancedSearchViewModel;
});