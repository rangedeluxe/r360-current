﻿function initAdvancedSearch(model, controllername) {
    require.config({
        paths: {
            'advancedSearchController': '/RecHubViews/Assets/AdvancedSearch/Controllers/AdvancedSearchController',
            'advancedSearchResultsController': '/RecHubViews/Assets/AdvancedSearch/Controllers/AdvancedSearchResultsController',
            'advancedSearchQueryDialogController': '/RecHubViews/Assets/AdvancedSearch/Controllers/AdvancedSearchQueryDialogController',

            'advancedSearchViewModel': '/RecHubViews/Assets/AdvancedSearch/ViewModels/AdvancedSearchViewModel',
            'advancedSearchResultsViewModel': '/RecHubViews/Assets/AdvancedSearch/ViewModels/AdvancedSearchResultsViewModel',
            'advancedSearchQueryDialogViewModel': '/RecHubViews/Assets/AdvancedSearch/ViewModels/AdvancedSearchQueryDialogViewModel',

            'advancedSearchFilter': '/RecHubViews/Assets/Shared/Controls/SearchFilter',
            'advancedSearchSelectFieldPicker': '/RecHubViews/Assets/AdvancedSearch/Objects/AdvancedSearchSelectFieldPicker',
        }
    });

    require([controllername], function (controller) {
        controller.init(model);
    });
}