﻿// Object controlls the filter portion of advanced search.

define(['jquery', 'frameworkUtils', 'ko',
    '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap',
    'bootstrap', 'wfs.select', 'select2'],
function ($, framework, ko) {
    var _container;
    var $getE;

    var _$controlbase;
    var _$selectboxavailable;
    var _$selectboxselected;
    
    var _displayfield;
    var _selected;
    var _available;
    var _items;

    // private
    var self = this;

    var initialize = function () {
        _$controlbase = _container;

        // Establish the UI.
        var $availfields = $('<div>').addClass('col-sm-5');
        $availfields.append('<strong>Available Fields</strong>');
        $availfields.append('<br/>');
        $availfields.append('<select class="available-list" multiple></select>');
        _$controlbase.append($availfields);

        var $movebuttons = $('<div>').addClass('col-sm-1 d-flex align-items-center justify-content-center').addClass('button-array');
        var $btngroup = $('<div class="btn-group-vertical" role="group"></div>');
        $btngroup.append('<button class="btn btn-secondary add-all-button" title="Add All Available Items"><span class="fa fa-chevron-right"></span><span class="fa fa-chevron-right"></span></button>');
        $btngroup.append('<button class="btn btn-secondary add-sel-button" title="Add Selected Items"><span class="fa fa-chevron-right"></span></button>');
        $btngroup.append('<button class="btn btn-secondary rem-sel-button" title="Remove Selected Items"><span class="fa fa-chevron-left"></span></button>');
        $btngroup.append('<button class="btn btn-secondary rem-all-button" title="Remove All Selected Items"><span class="fa fa-chevron-left"></span><span class="fa fa-chevron-left"></span></button>');
        $movebuttons.append($btngroup);
        _$controlbase.append($movebuttons);


        var $selectedfields = $('<div>').addClass('col-sm-5');
        $selectedfields.append('<strong>Selected Fields</strong>');
        $selectedfields.append('<br/>');
        $selectedfields.append('<select class="selected-list" multiple></select>');
        _$controlbase.append($selectedfields);

        var $orderbuttons = $('<div>').addClass('col-sm-1 d-flex align-items-center justify-content-center').addClass('button-array');
        var $btngroupupdown = $('<div class="btn-group-vertical" role="group"></div>');
        $btngroupupdown.append('<button class="btn btn-secondary move-up-button" title="Move Selected Up"><span class="fa fa-chevron-up"></span></button>');
        $btngroupupdown.append('<button class="btn btn-secondary move-down-button" title="Move Selected Down"><span class="fa fa-chevron-down"></span></button>');
        $orderbuttons.append($btngroupupdown);
        _$controlbase.append($orderbuttons);

        // For later use.
        _$selectboxavailable = $getE(".available-list");
        _$selectboxselected = $getE(".selected-list");

        // Hook up the buttons.
        $getE('.add-all-button').click(function () {
            moveAllToSelected();
        });
        $getE('.add-sel-button').click(function () {
            moveSelectedToSelected();
        });
        $getE('.rem-sel-button').click(function () {
            moveSelectedToAvailable();
        });
        $getE('.rem-all-button').click(function () {
            moveAllToAvailable();
        });
        $getE('.move-up-button').click(function () {
            moveSelectedUp();
        });
        $getE('.move-down-button').click(function () {
            moveSelectedDown();
        });

        
    };

    var clearItems = function () {
        // Clears out everything.
        _items = [];
        _selected = [];
        _available = [];
        _$selectboxavailable.empty();
        _$selectboxselected.empty();
    };

    var setItems = function (items) {
        clearItems();
        _items = items;
        $.each(_items, function (i, e) {
            var text = e[_displayfield];
            _$selectboxavailable.append("<option>" + text + "</option>");
            _available.push(e);
        });
    };

    var setSelectedItems = function (items) {
        $.each(items, function (i, e) {
            moveToSelectedInOrder(e);
        });
    };

    var moveSelectedUp = function () {
        var options = _$selectboxselected.find('option:selected');
        $.each(options, function (i, e) {
            var itemindex = $(e).index();
            if (itemindex <= 0)
                return;

            // Remove at current index, add at new index.
            var item = _selected.splice(itemindex, 1);
            _selected.splice(itemindex - 1, 0, item[0]);

            // Set the HTML to correspond with the data.
            var $item = $(_$selectboxselected.find('option').get(itemindex))
                .detach();
            if (itemindex === 1)
                _$selectboxselected.prepend($item);
            else
                _$selectboxselected.find('option:nth-child(' + (itemindex-1) + ')').after($item);

        });
    };

    var moveSelectedDown = function () {
        var options = _$selectboxselected.find('option:selected');
        var total = _$selectboxselected.find('option').length;
        $.each(options, function (i, e) {
            var itemindex = $(e).index();
            if (itemindex >= total - 1)
                return;

            // Remove at current index, add at new index.
            var item = _selected.splice(itemindex, 1);
            _selected.splice(itemindex + 1, 0, item[0]);

            // Set the HTML to correspond with the data.
            var $item = $(_$selectboxselected.find('option').get(itemindex))
                .detach();
            if (itemindex === total - 2)
                _$selectboxselected.append($item);
            else
                _$selectboxselected.find('option:nth-child(' + (itemindex + 1) + ')').after($item);

        });
    };

    var moveAllToAvailable = function () {
        // Moves all selected back into available.
        var options = _$selectboxselected.find('option');
        $.each(options, function (i, e) {
            moveToAvailable(_selected[$(e).index()]);
        });
    };

    var moveAllToSelected = function () {
        // Moves all available to selected.
        var options = _$selectboxavailable.find('option');
        $.each(options, function (i, e) {
            moveToSelected(_available[$(e).index()]);
        });
    };

    var moveSelectedToSelected = function () {
        // Moves the selected available to selected.
        var options = _$selectboxavailable.find('option:selected');
        $.each(options, function (i, e) {
            moveToSelected(_available[$(e).index()]);
        });
    };

    var moveSelectedToAvailable = function () {
        // Moves the selected available to selected.
        var options = _$selectboxselected.find('option:selected');
        $.each(options, function (i, e) {
            moveToAvailable(_selected[$(e).index()]);
        });
    };

    var findIndex = function (e, array) {
        var indexes = $.map(array, function (obj, index) {
            if (obj.BatchSourceKey === e.BatchSourceKey
                && obj.DateType === e.DateType
                && obj.FieldName === e.FieldName
                && obj.ReportTitle === e.ReportTitle) {
                return index;
            }
        });
        if (indexes.length > 0)
            return indexes[0];
        return -1;
    };

    var moveToAvailable = function (e) {
        var index = findIndex(e, _selected);
        var itemindex = findIndex(e, _items);
        // Add to our array.
        var item = _selected[index];
        _selected.splice(index, 1);

        // Find the indexes of all of the current available items. This is the weight used to 
        // determine where the new item should go.
        var availableindexes = $.map(_available, function(val){
            return findIndex(val, _items);
        });
        var newindex = 0;
        $.each(availableindexes, function(i, val){
            if (itemindex > val)
                newindex = i + 1;
        });

        _available.splice(newindex, 0, item);

        // Add to the selectbox.
        var $item = $(_$selectboxselected.find('option').get(index))
            .detach();
        if (newindex == 0)
            _$selectboxavailable.prepend($item);
        else
            _$selectboxavailable.children('option').eq(newindex-1).after($item)
    };

    var moveToSelectedInOrder = function (e) {
        // This function doesn't persist the proper order and just appends the
        // selected to the list.  This is used when coming back from breadcrumbs.
        var index = findIndex(e, _available);
        // Add to our array.
        var item = _available[index];
        _available.splice(index, 1);
        _selected.push(item);

        // Add to the selectbox.
        var $item = $(_$selectboxavailable.find('option').get(index))
            .detach();
        _$selectboxselected.append($item);
    };

    var moveToSelected = function (e) {
        var index = findIndex(e, _available);
        var itemindex = findIndex(e, _items);
        // Add to our array.
        var item = _available[index];
        _available.splice(index, 1);

        // Find the indexes of all of the current selected items. This is the weight used to 
        // determine where the new item should go.
        var selectedindexes = $.map(_selected, function(val){
            return findIndex(val, _items);
        });
        var newindex = 0;
        $.each(selectedindexes, function(i, val){
            if (itemindex > val)
                newindex = i + 1;
        });

        // Add to the selected array.
        _selected.splice(newindex, 0, item);

        // Add to the selectbox.
        var $item = $(_$selectboxavailable.find('option').get(index))
            .detach();
        if (newindex == 0)
            _$selectboxselected.prepend($item);
        else
            _$selectboxselected.children('option').eq(newindex-1).after($item)
    };

    // Constructor
    function advancedSearchSelectFieldPicker(container, displayfield) {
        _container = container;
        $getE = function (b) { return _container.find(b); };

        _displayfield = displayfield;
    }

    // Public properties


    // Public methods
    advancedSearchSelectFieldPicker.prototype = {
        constructor: advancedSearchSelectFieldPicker,
        init: function () {
            initialize();
        },
        getSelected: function () {
            return _selected;
        },
        setItems: function (items) {
            setItems(items);
        },
        setSelected: function(items){
            setSelectedItems(items);
        },
        clearSelected: function() {
            return moveAllToAvailable();
        },
        dispose: function () {
            _$controlbase.empty();
            _available = null;
            _selected = null;
            _items = null;
            _displayfield = null;
        }
    }

    return advancedSearchSelectFieldPicker;
});