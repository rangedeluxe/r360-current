﻿define(["jquery", "ko", "frameworkUtils", "accounting", '/RecHubViews/Assets/Shared/Breadcrumbs.js', 'datatables', 'datatables-bootstrap'],
function ($, ko, framework, accounting, breadcrumbs) {

    function RecSummary() {
        var _container = {};
        var _vm = {};
        var _breadcrumbs = new breadcrumbs();
        var $getE = function (b) { alert("Not initialized!"); };
        
        var _rectable = null;

        var refreshData = function () {
            $getE('#summaryDepositError').css('display', 'none');
            var summaryUrl = "/RecHubViews/ReceivablesSummary/GetSummaryData";
            var data = {
                DepositDateString: $getE('.summaryDepositDate').wfsDatePicker('getDate')
            };
            framework.doJSON(summaryUrl, $.toDictionary(data), refreshDataCallback);
            framework.showSpinner(_vm.labels['Loading']);
        };

        var refreshDataCallback = function (serverResponse) {
            if (!serverResponse.success) {
                framework.errorToast(_container, serverResponse.error);
            } else {
                _vm.items(serverResponse.data);
                updateGrid();
            }
            sortGrid();
            framework.hideSpinner();
        };

        var printCallback = function (serverResponse) {
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            } else {
                window.open("/RecHubReportingViews/LoadReport?instanceId=" + serverResponse.Data + "&reportName=" + _vm.currentReport + ".pdf", "_blank");
            }
            framework.hideSpinner();
        };

        var doPrint = function (reportName) {
            //Persist the name of the report  for the callback
            _vm.currentReport = reportName;
            // mapping report parameters to object/grid values
            var reportGBMapping = { 'PaymentSource': 'BatchSource', 'PaymentType': 'PaymentType', 'Account': 'Workgroup' };
            var reportSBMapping = [  'Workgroup', 'BatchSource',  'PaymentType', 'TranCount' ];
            // get the map values from selected grouping/sort and sort direction
            var grouping = _vm.selectedGrouping() ? _vm.selectedGrouping().Id : null;
            var sb = reportSBMapping[_vm.selectedColumn()[0]] ? reportSBMapping[_vm.selectedColumn()[0]] : 'TotalAmount';
            var gb = reportGBMapping[grouping] ? reportGBMapping[grouping] : '';
            var sd = _vm.selectedColumn()[1] == "desc" ? 'Descending' : 'Ascending';
            // build the report
            var report = {
                reportName: reportName,
                parameterValues: [
                { Name: "FD", Value: WFSDateUtil.formatWFSDate($getE('.summaryDepositDate').wfsDatePicker('getDate')) },
                  { Name: "GB", Value: gb },
                  { Name: "SB", Value: sb },
                  { Name: "SO", Value: sd }
                ]
            };
            // call report engine
            var url = '/RecHubReportingViews/Reporting/InvokeReportAsync/';
            framework.doJSON(url, $.toDictionary(report), printCallback);
            framework.showSpinner();
        };

        var setupElementBindings = function () {

            // Standard Breadcrumb logic.
            _breadcrumbs.pop("Receivables Summary");
            var bcs = _breadcrumbs.render();
            $getE('.breadcrumbheader').html(bcs);

            $getE('.resizeFull').click(showAllRows);
            $getE('.resizeSmall').click(hideAllRows);

            // create datepicker
            $getE('.summaryDepositDate').wfsDatePicker({ callback: refreshData });

            // bind refresh button
            $getE('.summaryRefresh').bind('click', function () {
                refreshData();
            });

            $getE('.btnPrintSummary').bind('click', function () {
                doPrint("Receivables Summary");
            });

            $getE('.btnPrintDetail').bind('click', function () {
                doPrint("Receivables Detail");
            });

            // initialize tooltips
            $getE('.tabbase').tooltip();

            // Datagrid Setup. We're overwritting the default sorting.
            // If you need multi-column sorting, you need to disable it and handle it yourself.  It's not that bad. :)
            var columns = [
                { "data": "Account", "sortable": false },
                { "data": "PaymentSource", "sortable": false },
                { "data": "PaymentType", "sortable": false },
                { "data": "TransactionCount", "sortable": false, class: "alignRight", name: "TransactionCount"},
                { "data": "Total", "sortable": false, class: "alignRight", name: "Total" },
                // We need this data populated for sorting, but we don't need to display it.
                { "data": "Organization", "visible": false, "sortable": false }
            ];
            _rectable = $getE('#batchResultsTable').DataTable({
                "pageLength": 50,
                "data": _vm.items(),
                "columns": columns,
                "footerCallback": function (row, data, start, end, display) {
                    // This is to just count up some totals and show them at the bottom.
                    var api = this.api();
                    var total = api.column('Total:name').data().reduce(function (a, b) {
                        return a + b;
                    }, 0);
                    var totalcurrency = formatCurrency(total.toFixed(2).toString());
                    _vm.total(totalcurrency);
                },
                "createdRow": function (row, data, index) {
                    // This is to format the 'amount' column to look like currency.
                    var amt = formatCurrency(data.Total.toFixed(2).toString());
                    var colindex = this.api().column('Total:name')[0][0];
                    $(row).children('td').eq(colindex).html(amt);

                    // And this is to add a small tool-tip.
                    $(row).attr('title', 'Click here for Batch Summary');
                },
                "drawCallback": function (settings) {
                    // This is to draw out the grouping headers after the sorting is completed.
                    // Really all this is, is just looping through the data and looking for differences.
                    // So, we're really assuming things are sorted correctly ahead of time. Which is fine.
                    var api = this.api();
                    var rows = api.rows({ page: 'current' }).nodes();
                    var currentrows = api.rows({ page: 'current' }).data();
                    var alldata = api.rows().data();
                    var last = null;

                    // Show all rows, we could have hid them due to grouping.
                    $getE('tr[role="row"]').show();

                    $.each(currentrows, function (ind, data) {
                        var value = data[_vm.selectedGrouping().DataSource];
                        var groupingname = _vm.selectedGrouping().Name;
                        if (value !== last)
                        {
                            // Grabs the total for this group (filtering out other groups).
                            var total = alldata.reduce(function (count, curr) {
                                return (curr[_vm.selectedGrouping().DataSource] === value)
                                    ? count + curr.Total
                                    : count;
                            }, 0);
                            var transactiontotal = alldata.reduce(function (count, curr) {
                                return (curr[_vm.selectedGrouping().DataSource] === value)
                                    ? count + curr.TransactionCount
                                    : count;
                            }, 0);

                            var totalcurrency = formatCurrency(total.toFixed(2).toString());
                            $getE(rows).eq(ind).before('<tr class="group"><td colspan="3"><strong><span class="fa fa-2 fa-minus" /> '
                                + groupingname + ': ' + value + '</strong></td><td colspan="1" class="alignRight"><strong>' + transactiontotal + '</strong></td><td colspan="1" class="alignRight"><strong>' + totalcurrency + '</strong></td></tr>');
                        }
                        last = value;
                    });
                }
            });

            // Handle Header clicks.
            $getE('#batchResultsTable tbody').on('click', 'tr.group', function () {
                toggleRows(this);
            });


            // Handle row clicks. Simple jquery-style.
            $getE('#batchResultsTable tbody').on('click', 'tr[role="row"]', function () {
                var data = _rectable.row(this).data();
                var url = '/RecHubViews/BatchSummary/Index?DateFrom={datefrom}&DateTo={dateto}&Workgroup={workgroup}&PaymentType={paymenttype}&PaymentSource={paymentsource}';
                var date = $getE('.summaryDepositDate').wfsDatePicker('getDate');
                url = url.replace("{datefrom}", date);
                url = url.replace("{dateto}", date);
                url = url.replace("{paymenttype}", data.PaymentTypeId);
                url = url.replace("{workgroup}", data.RecHubBankId + '|' + data.RecHubWorkgroupId);
                url = url.replace("{paymentsource}", data.PaymentSourceId);

                // Register our breadcrumb.
                _breadcrumbs.register("Receivables Summary", "/RecHubViews/ReceivablesSummary/", { DepositDateString: date });

                // Clear out both portlets on this page.
                framework.loadByContainer(url, _container.parent());
            });

            // Handle sorting, since we disabled the default. The default conflicts with multi-column sorting.
            $getE('#batchResultsTable th').click(function () {
                var index = $getE(this).index();
              
                var order = _vm.selectedColumn()[1] == 'desc'
                    ? 'asc'
                    : 'desc';
                _vm.selectedColumn([index, order]);
                sortGrid();
                //gets rid of dim sort icon on currently sorted as
                $getE(this).removeClass('sorting');
            });

            // initialize the grouping text boxes
            $getE('.groupingSelectSummary').wfsSelectbox({
                items: _vm.groupings,
                callback: function (value) {
                    _vm.selectedGrouping(value);
                    sortGrid();
                },
                placeHolder: _vm.labels.SelectGrouping
            });

            if (_vm.DepositDate) {
                $getE('.summaryDepositDate').wfsDatePicker('setDate', _vm.DepositDate);
            }

            // enable a way to reach the additional pages that are not in production quite yet.
            $getE('.summaryDepositDate input').on('keydown', function (event) {
                if (event.which == 220 && event.shiftKey === true) {
                    framework.loadByContainer('/RecHubViews/PostDepositExceptions', _container.parent());
                }
                if (event.which == 221 && event.shiftKey === true) {
                    framework.loadByContainer('/RecHubViews/InvoiceSearch', _container.parent());
                }
            });

            refreshData();
        };

        var toggleRows = function (header) {
            var head = $getE(header);
            // Toggle the icon.
            var icon = $(head).find('.fa');
            var newicon = icon.hasClass('fa-plus')
                ? 'fa-minus'
                : 'fa-plus';
            icon.removeClass('fa-plus')
                .removeClass('fa-minus')
                .addClass(newicon);

            var rows = $(head).nextUntil('.group', 'tr[role="row"]');
            if (newicon === 'fa-plus')
                rows.hide();
            else
                rows.show();
        };

        var hideAllRows = function() {
            $getE('tr.group')
                .find('.fa')
                .removeClass('fa-minus')
                .addClass('fa-plus');
            $getE('tbody tr[role="row"]')
                .hide();
        };

        var showAllRows = function() {
            $getE('tr.group')
                .find('.fa')
                .removeClass('fa-plus')
                .addClass('fa-minus');
            $getE('tbody tr[role="row"]')
                .show();
        }

        var formatCurrency = function (n) {
            return '$' + n.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,");
        };

        var sortGrid = function () {
            // Grab the grouping from the dropdown.
            var groupsorting = [_vm.selectedGrouping().Column, 'asc'];

            // Grab the current sorting on the grid. This will be in format [0, "asc"];
            var gridsort = _vm.selectedColumn();

            // Do the sort!
            _rectable.order(groupsorting, gridsort);
            _rectable.draw();
            //adds the dimmed sort icon to the headers unless the column is the currently the one sorted as
            $getE('#batchResultsTable > thead > tr > th').not('.sorting_asc').not('sorting_desc').addClass('sorting');
            
        };

        var updateGrid = function () {
            _rectable.clear();
            _rectable.data().rows.add(_vm.items());
            _rectable.draw();
        };

        var sViewModel = function (model) {
            var self = this;
            self.model = model;
            self.labels = model.Labels;

            self.items = ko.observableArray([]);
            self.groupings = [
                { Id: 'Entity', Name: self.labels.Entity, DataSource: 'Organization', Column: 5 },
                { Id: 'PaymentSource', Name: self.labels.PaymentSource, DataSource: 'PaymentSource', Column: 1 },
                { Id: 'PaymentType', Name: self.labels.PaymentType, DataSource: 'PaymentType', Column: 2 },
                { Id: 'Account', Name: self.labels.AccountId, DataSource: 'Account', Column: 0 }
            ],
            self.selectedGrouping = ko.observable(self.groupings[0]);
            self.currentReport = "";
            self.sortcol = "total";
            self.sortField = "Total";
            self.sortdir = true;
            self.total = ko.observable('');
            self.pageTotal = ko.observable('');
            self.selectedColumn = ko.observable([0, 'asc']);
            self.DepositDate = model.DepositDateString;
        }

        function init(myContainerId, model) {
            $(function () {
                _container = $('#summaryPortletContainer');
                $getE = function (b) { return _container.find(b); };
                _vm = new sViewModel(model);
                ko.applyBindings(_vm, _container.get(0));
                setupElementBindings();
            });
        }

        return {
            init: init
        }
    }

    function init(myContainerId, model) {
        $(function () {
            var summary = new RecSummary();
            summary.init(myContainerId, model);
        });
    }

    return {
        init: init
    }
});

