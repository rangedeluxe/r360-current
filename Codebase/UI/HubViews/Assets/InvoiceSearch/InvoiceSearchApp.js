﻿function initInvoiceSearch(model, controllername, viewmodelname, container) {
    require.config({
        paths: {
            'invoiceSearchViewModel': '/RecHubViews/Assets/InvoiceSearch/ViewModels/InvoiceSearchViewModel',
            'invoiceSearchResultsViewModel': '/RecHubViews/Assets/InvoiceSearch/ViewModels/InvoiceSearchResultsViewModel.js?v=20170201',
            'searchFilter': '/RecHubViews/Assets/Shared/Controls/SearchFilter',
        }
    });

    require([controllername, viewmodelname], function (controller, viewmodel) {
        controller.init(model, viewmodel, container);
    });
}