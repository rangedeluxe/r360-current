﻿define(["jquery", "ko", "frameworkUtils", "accounting", "wfs.gridbase", "wfs.select"], function ($, ko, framework, accounting, WFSGridBase) {

    function DDASummary() {

        var _me = this;
        var _container = {};
        var _vm = {};
        var $getE = function (b) { alert("Not initialized!"); };
        var ddaSummaryColors = ["#373D42", "#F47920", "#4B7229", "#005860", "#001228", "#FFDC5D", "#C6D486", "#A7B6BF", "#7BC0D2", "#227BA6"];
        var maxDDAsForCharts = 10;


        //*****************************************************
        //*****************************************************
        //  Setup Bindings
        //*****************************************************
        var setupElementBindings = function () {

            $getE('#summaryDepositDate').wfsDatePicker({ callback: getSummaryData });

            $getE('.refresh').on('click', function () {
                doRefresh();
                return false;
            });

            $getE('.btnPrint').on('click', function () {
                // mapping report parameters to object/grid values
                var reportSBMapping = { 'ParentHierarchy': 'Entity', 'WorkgroupName': 'Workgroup', 'PaymentSource': 'BatchSource', 'PaymentType': 'PaymentType', 'PaymentCount': 'PaymentCount', 'PaymentTotal': 'TotalAmount' };
                // get the map values from selected grouping/sort and sort direction
                var sortInfo = _vm.ddaSummaryGrid.getSortInfo();
                var sb = sortInfo !== null && reportSBMapping[sortInfo.ColumnFieldName] ? reportSBMapping[sortInfo.ColumnFieldName] : 'Entity';
                var sd = sortInfo.Ascending ? 'Ascending' : 'Descending';
                // build the report
                var report = {
                    reportName: "DDA Summary",
                    parameterValues: [
                        { Name: "FD", Value: WFSDateUtil.formatWFSDate($getE('#summaryDepositDate').wfsDatePicker('getDate')) },
                        { Name: "SB", Value: sb },
                        { Name: "SO", Value: sd }
                    ]
                };

                var url = '/RecHubReportingViews/Reporting/InvokeReportAsync/';
                framework.doJSON(url, $.toDictionary(report), printCallback);
                framework.showSpinner();

                return false;
            });

            initDDASummaryGrid();
        };
        //*****************************************************
        //*****************************************************


        //*****************************************************
        //*****************************************************
        //  Get Summary Data
        //*****************************************************
        var getSummaryData = function () {
            var data = {
                depositDate: $getE('#summaryDepositDate').wfsDatePicker('getDate')
            };
            var preferencesURL = "/RecHubViews/DDASummary/GetSummaryData/";
            framework.doJSON(preferencesURL, $.toDictionary(data), getSummaryDataCallback);
            framework.showSpinner('Loading...');
        };
        //*****************************************************
        //*****************************************************


        //*****************************************************
        //*****************************************************
        //  Get Summary Data Callback
        //*****************************************************
        var getSummaryDataCallback = function (serverResponse) {

            if (serverResponse.HasErrors) {
                clearPageData();
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else if (!serverResponse.Data || serverResponse.Data.length < 1) {
                clearPageData();
            }
            else {
                _vm.hasData(true);
                $getE('#ddaSummaryNoDataMessage').empty();
                _vm.items = createIDColumn(serverResponse.Data);
                _vm['ddaSummaryGrid'].setItems(_vm.items);
                //this actually just invalidates the grid in the wfs wrapper of slickgrid.
                _vm['ddaSummaryGrid'].resizeGrid();
                
                buildPieCharts();
            }
            
            framework.hideSpinner();
        };
        //*****************************************************
        //*****************************************************

        //*****************************************************
        //*****************************************************
        //  Print Report Callback
        //*****************************************************
        var printCallback = function (serverResponse) {
            if (serverResponse.HasErrors) {
                framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
            }
            else {
                window.open("/RecHubReportingViews/LoadReport?instanceId=" + serverResponse.Data+"&reportName=DDASummary.pdf", "_blank");
            }
            framework.hideSpinner();
        };
        //*****************************************************
        //*****************************************************


        //*****************************************************
        //*****************************************************
        //  View Model
        //*****************************************************
        var viewModel = function (model) {
            var self = this;
            self.model = model;

            self.items = [];

            self.hasData = ko.observable(self.items.length > 0);
            self.displayCharts = ko.observable(true);
            self.tooManyDDAs = ko.observable(false);
        };
        //*****************************************************
        //*****************************************************

        var gridSorter = function (dataView, sortFieldName, ascending) {
            // using native sort with comparer
            // preferred method but can be very slow in IE with huge datasets
            dataView.sort(function (a, b) {
                var sortBy = sortFieldName;
                var x = a[sortFieldName];
                var y = b[sortFieldName];
                var Adda = a.DDA;
                var Bdda = b.DDA;
                if (sortBy === "WorkgroupName") {
                    //dual sort, first on ID, if ID matches let the full display name go through
                    // otherwise use the ID to sort appropriately
                    if (a.SiteClientAccountID !== b.SiteClientAccountID) {
                        x = a.SiteClientAccountID;
                        y = b.SiteClientAccountID;
                    }
                }
                // Make sure we Sort Each DDA versus and entire Sort on the grid. 
                // If DDA is not equal then bail out, treat DDA as Grouping versus sorting every record.
                if (Adda != Bdda) {
                    return (Adda < Bdda ? 1 : -1) * ascending;
                }
                else {
                    if (typeof x !== "string" && typeof x === typeof y) {
                        return (x == y ? 0 : (x > y ? 1 : -1));
                    }
                    else {
                        //convert them both to strings just in case one was a number and the other was not
                        var lowX = x === null ? "" : x.toString().toLowerCase();
                        var lowY = y === null ? "" : y.toString().toLowerCase();
                        return(lowX === lowY ? 0 : (lowX > lowY ? 1 : -1));
                    }
                }
            }, ascending);
        };

        //*****************************************************
        //*****************************************************
        //  Initialize Grid
        //*****************************************************
        var initDDASummaryGrid = function () {
            if (_vm['ddaSummaryGrid'] === undefined) {

                var aggregators = [new Slick.Data.Aggregators.Sum("PaymentCount"),
                                   new Slick.Data.Aggregators.Sum("PaymentTotal")];

                var gridModel = {
                    gridOptions: {
                        //forceFitColumns: false
                        //nothing changed from default
                    },
                    dataViewOptions: {
                        groupItemMetadataProvider: new Custom.DDASummary.GroupItemMetadataProvider(),
                    },
                    implOptions: {
                        gridSelector: '#ddaSummaryGrid',
                        pageSize: 200,
                        displayPagerSettings: false,
                        data: _vm.items,
                        idFieldName: 'ID',
                        allowRowDelete: false,
                        allowColumnSorting: true,
                        allowDynamicHeight: true,
                        allowShowDetail: false,
                        enableDefaultGrouping: true,
                        customSorter: gridSorter,

                        sortField: 'ParentHierarchy',
                        columns: [
                             { Id: "ID", ColumnTitle: '', ColumnFieldName: "", Tooltip: '', width: 20, maxWidth: 20 },
                             { Id: "Entity", ColumnTitle: 'Entity', ColumnFieldName: "ParentHierarchy", Tooltip: '', DataType: 1, resizable: true, sortable: true },
                             { Id: "WorkgroupName", ColumnTitle: 'Workgroup', ColumnFieldName: "WorkgroupName", Tooltip: '', DataType: 1, resizable: true, sortable: true },
                             { Id: "PaymentSource", ColumnTitle: 'Payment Source', ColumnFieldName: "PaymentSource", Tooltip: '', DataType: 1, resizable: true, sortable: true },
                             { Id: "PaymentType", ColumnTitle: 'Payment Type', ColumnFieldName: "PaymentType", Tooltip: '', DataType: 1, resizable: true, sortable: true },
                             //{ Id: "DDA", ColumnTitle: 'DDA', ColumnFieldName: "DDA", Tooltip: '', DataType: 1, resizable: true, sortable: true, groupTotalsFormatter: textTotalFormatter },
                             { Id: "PaymentCount", ColumnTitle: 'Payment Count', ColumnFieldName: "PaymentCount", Tooltip: '', DataType: 6, resizable: true, sortable: true, formatter: paymentCountFormatter },
                             { Id: "PaymentTotal", ColumnTitle: 'Total', ColumnFieldName: "PaymentTotal", Tooltip: '', DataType: 7, resizable: true, sortable: true, formatter: paymentTotalFormatter }
                        ],
                    },
                };

                _vm['ddaSummaryGrid'] = new WFSGridBase.GridBase();
                _vm['ddaSummaryGrid'].init(_container.attr('id'), gridModel);
                _vm['ddaSummaryGrid'].setGrouping('DDA', groupFormatter, false, null, null, aggregators, true, false);
            }
        };
        //*****************************************************
        //*****************************************************


        //*****************************************************
        //*****************************************************
        //  Formatters
        //*****************************************************
        var groupFormatter = function (groupItem) {
            return "<span style='font-size: 1.1em;'>DDA:  " + groupItem.value + "</span>";
        };

        var paymentTotalFormatter = function (row, cell, value, columnDef, dataContext) {
            if (dataContext.__group) {
                value = accounting.formatMoney(dataContext.totals.sum.PaymentTotal, ' ');
            }
            if (value != null) {
                return accounting.formatMoney(value, ' ');
            }
            return "";
        }

        var paymentCountFormatter = function (row, cell, value, columnDef, dataContext) {
            if (dataContext.__group) {
                value = dataContext.totals.sum.PaymentCount;
            }
            if (value != null) {
                return (Math.round(parseFloat(value) * 100) / 100);
            }
            return "";
        }
        //*****************************************************
        //*****************************************************


        //*****************************************************
        //*****************************************************
        //  Private Methods
        //*****************************************************
        function createIDColumn(arr) {
            var count = 0;
            var idItems = $.map(arr, function (e) {
                e.DDA = e.DDA.trim();//trim spaces right away
                return $.extend({}, e, {
                    ID: ++count
                });
            });

            return idItems;
        }

        function doRefresh() {
            getSummaryData();
        };

        function buildPieCharts() {

            var chartData = getChartData();

            var paymentAmountChartTitle = getPaymentAmountChartTitle(chartData);
            var paymentCountChartTitle = getPaymentCountChartTitle(chartData);
            var tooManyDDAs = false;

            if (chartData.localdata.length > maxDDAsForCharts) {
                chartData = clearChartData(chartData);
                tooManyDDAs = true;
            }

            //if we have chart data to display do so
            // or if we were displaying data and we now have too many DDAs and need to reset the chart data *before hiding the chart*
            //update the charts to have the new data / no data(must be done before hiding them)
            if (tooManyDDAs === false || _vm.displayCharts() === true) {
                //make sure it's visible before updating data
                _vm.displayCharts(true);

                var dataAdapter = new $.jqx.dataAdapter(chartData, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });

                var paymentAmountSettings = {
                    title: paymentAmountChartTitle,
                    description: "",
                    enableAnimations: true,
                    showLegend: true,
                    showBorderLine: true,
                    legendLayout: { left: 290, top: 23, width: 200, height: 200, flow: 'vertical' },
                    padding: { left: 5, top: 5, right: 200, bottom: 5 },
                    titlePadding: { left: 0, top: 0, right: 0, bottom: 5 },
                    source: dataAdapter,
                    colorScheme: 'ddaSummaryPies',
                    seriesGroups: [{
                        type: 'pie',
                        showLabels: false,
                        categoryAxis: {//I think that categoryAxis has been replace in newer versions of 
                            formatFunction: function (value, itemIndex, serie, group) {
                                var displayValue = value.toString();
                                if (displayValue.length > 15) {
                                    displayValue = displayValue.slice(0, 6) + "..." + displayValue.slice(-6);
                                }
                                return displayValue;
                            }
                        },
                        series: [{
                            dataField: 'PaymentTotal',
                            displayText: 'DDA',
                            initialAngle: 15,
                            centerOffset: 0,
                            toolTipFormatFunction: paymentAmountChartTooltip }]
                    }]
                };

                $getE('#paymentTotals').jqxChart(paymentAmountSettings);

                var paymentCountSettings = {
                    title: paymentCountChartTitle,
                    description: "",
                    enableAnimations: true,
                    showLegend: true,
                    showBorderLine: true,
                    legendLayout: { left: 290, top: 23, width: 200, height: 200, flow: 'vertical' },
                    padding: { left: 5, top: 5, right: 200, bottom: 5 },
                    titlePadding: { left: 0, top: 0, right: 0, bottom: 5 },
                    source: dataAdapter,
                    colorScheme: 'ddaSummaryPies',
                    seriesGroups: [{
                        type: 'pie',
                        showLabels: false,
                        categoryAxis: {//I think that categoryAxis has been replace in newer versions of 
                            formatFunction: function (value, itemIndex, serie, group) {
                                var displayValue = value.toString();
                                if (displayValue.length > 15) {
                                    displayValue = displayValue.slice(0, 6) + "..." + displayValue.slice(-6);
                                }
                                return displayValue;
                            }
                        },
                        series: [{
                            dataField: 'PaymentCount',
                            displayText: 'DDA',
                            initialAngle: 15,
                            centerOffset: 0,
                            toolTipFormatFunction: paymentCountChartTooltip }]
                    }]
                };

                $getE('#paymentCounts').jqxChart(paymentCountSettings);
            }

            _vm.displayCharts(!tooManyDDAs);
            _vm.tooManyDDAs(tooManyDDAs);
        }

        var getChartData = function () {
            var chartData =
            {
                datatype: "localdata",
                datafields: [
                  { name: "DDA" },
                  { name: "PaymentTotal" },
                  { name: "PaymentCount" },
                  { name: "PaymentTotalPercent" },
                  { name: "PaymentCountPercent" }
                ],
                id: 'ID',
                localdata: []
            };

            $.each(_vm.items, function (index, item) {

                var result = $.grep(chartData.localdata, function (e) {
                    return e.DDA.trim().toLowerCase() == item.DDA.trim().toLowerCase();
                });

                if (result.length == 0) {
                    var tempItem = { ID: item.ID, DDA: item.DDA, PaymentTotal: item.PaymentTotal, PaymentCount: item.PaymentCount, PaymentTotalPercent: 0, PaymentCountPercent: 0 };
                    chartData.localdata.push(tempItem);
                }
                else {
                    result[0].PaymentTotal += item.PaymentTotal;
                    result[0].PaymentCount += item.PaymentCount;
                }

            });

            chartData.localdata.sort(function (a, b) {
                var x = a !== undefined && a !== null ? a.DDA.toString().toLowerCase() : "";
                var y = b !== undefined && b !== null ? b.DDA.toString().toLowerCase() : "";

                //textual sorting of numeric values
                if (x.length > y.length)
                    return 1;
                if (x.length === y.length) {
                    return (x === y ? 0 : (x > y ? 1 : -1));
                }
                return -1;
            });

            // get percentages
            var itemTotalPercent = 0;
            var itemCountPercent = 0;
            for (var i = 0; i < chartData.localdata.length; i++) {
                var item = chartData.localdata[i];
                item.PaymentTotalPercent = Math.round((item.PaymentTotal / getAmountGrandTotal() * 100).toFixed(2));
                item.PaymentCountPercent = Math.round((item.PaymentCount / getCountGrandTotal() * 100).toFixed(2));
                itemTotalPercent += item.PaymentTotalPercent;
                itemCountPercent += item.PaymentCountPercent;
            }
            //adjust percentages if there is data
            if (chartData.localdata.length > 0) {
                //if the item total percentage is not 100, add the diff to the highest percentage slice
                if (itemTotalPercent !== 100) {
                    var item = getAmountHighestPercentage(chartData.localdata);
                    item.PaymentTotalPercent += (100 - itemTotalPercent);
                }
                //if the item count percentage is not 100, add the diff to the highest percentage slice
                if (itemCountPercent !== 100) {
                    var item = getCountHighestPercentage(chartData.localdata);
                    item.PaymentCountPercent += (100 - itemCountPercent);
                }
            }

           _vm.items.chartData = chartData;

            return chartData;
        }

        var paymentAmountChartTooltip = function (value, itemIndex, serie, group, categoryValue, categoryAxis) {
            var dataItem = _vm.items.chartData.localdata[itemIndex];
            return '<DIV style="text-align:left"><b>DDA: ' + dataItem.DDA + '<br />' + accounting.formatMoney(value, '$') + ' (' + dataItem.PaymentTotalPercent + '%)</DIV>';
        };

        var paymentCountChartTooltip = function (value, itemIndex, serie, group, categoryValue, categoryAxis) {
            var dataItem = _vm.items.chartData.localdata[itemIndex];
            return '<DIV style="text-align:left"><b>DDA: ' + dataItem.DDA + '<br />' + value + ' (' + dataItem.PaymentCountPercent + '%)</DIV>';
        };

        function getAmountHighestPercentage(localdata) {
            var amountHighestPercentage = 0;
            var index = 0;
            for (var i = 0; i < localdata.length; i++) {
                if (localdata[i].PaymentTotalPercent > amountHighestPercentage) {
                    amountHighestPercentage = localdata[i].PaymentTotalPercent;
                    index = i;
                }
            }
            return localdata[index];
        }

        function getCountHighestPercentage(localdata) {
            var countHighestPercentage = 0;
            var index = 0;
            for (var i = 0; i < localdata.length; i++) {
                if (localdata[i].PaymentCountPercent > countHighestPercentage) {
                    countHighestPercentage = localdata[i].PaymentCountPercent;
                    index = i;
                }
            }
            return localdata[index];
        }

        function getAmountGrandTotal() {
            var amountGrandTotal = 0;

            for (var i = 0; i < _vm.items.length; i++) {
                amountGrandTotal = amountGrandTotal + _vm.items[i].PaymentTotal;
            }

            return amountGrandTotal;
        }

        function getCountGrandTotal() {
            var countGrandTotal = 0;

            for (var i = 0; i < _vm.items.length; i++) {
                countGrandTotal = countGrandTotal + _vm.items[i].PaymentCount;
            }

            return countGrandTotal;
        }

        function clearItems() {

            if (_vm.items != null && typeof (_vm.items) != 'undefined') {
                while (_vm.items.length > 0) {
                    _vm.items.pop();
                };
            }

            if (_vm.items.chartData != null && typeof (_vm.items.chartData) != 'undefined') {
                if (_vm.items.chartData.localdata != null && typeof (_vm.items.chartData.localdata) != 'undefined') {
                    _vm.items.chartData = {};
                };
            }
        }

        function clearPageData() {
            _vm.hasData(false);
            var messageDiv = $('<div style="font-weight: bold;" >' + 'No Data Available' + '</div>');
            $getE('#ddaSummaryNoDataMessage').empty().append(messageDiv);
            clearItems();
            _vm['ddaSummaryGrid'].setItems(_vm.items);
            buildPieCharts();
        }

        function clearChartData(data) {

            if (data.localdata != null && typeof (data.localdata) != 'undefined') {
                while (data.localdata.length > 0) {
                    data.localdata.pop();
                };
            }

            return data;
        }

        function getPaymentAmountChartTitle(data) {
            if (data.localdata.length <= 0)
                return "No Data Available";
            else
                return "Total Amount  " + accounting.formatMoney(getAmountGrandTotal(), '$');
        }

        function getPaymentCountChartTitle(data) {
            if (data.localdata.length <= 0)
                return "No Data Available";
            else
                return "Total Payments  " + getCountGrandTotal();
        }
        //*****************************************************
        //*****************************************************


        //*****************************************************
        //*****************************************************
        //  Initialization
        //*****************************************************
        function init(myContainerId, model) {
            $(function () {
                _container = $('#' + myContainerId);
                $getE = function (b) { return _container.find(b); };
                var contElement = $getE('.ddaSumPortlet').get(0);
                if(contElement){                  
                    _vm = new viewModel(model);
                    ko.applyBindings(_vm, contElement);
                    $.jqx._jqxChart.prototype.colorSchemes.push({ name: 'ddaSummaryPies', colors: ddaSummaryColors });
                    setupElementBindings();
                    getSummaryData();
                }
            });
        };

        return {
            init: init
        }
    }

    function init(myContainerId, model) {
        $(function () {
            var ddaSummary = new DDASummary();
            ddaSummary.init(myContainerId, model);
        });
    }

    return {
        init: init
    }
    //*****************************************************
    //*****************************************************

});
