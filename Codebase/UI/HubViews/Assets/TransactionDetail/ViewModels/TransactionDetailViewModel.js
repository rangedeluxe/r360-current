// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.

define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js',
    '/RecHubViews/Assets/Shared/ImageService.js', '/RecHubViews/Assets/Shared/DataTablesUtilities.js', 
    'datatables', 'datatables-bootstrap', 'bootstrap', 'moment'],
function ($, framework, ko, breadcrumbs, imageService, datatableutilities) {

    var _container;
    var $getE;
    var query;
    var paymentdata = [];
    var paymentgrid;
    var stubdata = [];
    var stubcols = [];
    var stubgrid;
    var documentdata = [];
    var documentgrid;
    var bread = new breadcrumbs();
    var imageservice = new imageService();
    var utilities = new datatableutilities();

    var appendDataEntry = function (row, data) {

        // Kick out if we dont have any DE data.
        if (!data.DataEntryFields)
            return;

        var td = $(row).children('td:nth-child(2)');
        var html = "</br><br><span class ='defields'>Data Entry Fields</span>";
        html += "<ul style='min-width:200px;'>";
        $.each(data.DataEntryFields, function (i, e) {
            var deVal = e.Type === "7" ? formatCurrency(e.Value) : e.Value;
            html += "<li><small>" + e.Title + ": " + deVal + "</small></li>";
        });
        html += "</ul>";
        td.append(html);
    };

    //Payment Items grid
    var paymentGridOptions = {
        data: paymentdata,
        order: [[1,"asc"]],
        "columns": [
            { "data": "PaymentSequence", sortable: false, "width" : "6%" },
            { "data": "PaymentSequence" },
            { "data": "RT" },
            { "data": "AccountNumber" },
            { "data": "CheckTraceRefNumber" },
            { "data": "Payer" },
            { "data": "DDA" },
            { "data": "PaymentAmount", class:"alignRight"}            
        ],
        "footerCallback": function (row, data, start, end, display) {
            // Create the totals.
            var api = this.api();
            var total = api.column(7).data().reduce(function (a, b) {
                return a + b;
            }, 0);
            var totalcurrency = formatCurrency(total);
            self.PaymentTotal(totalcurrency);
        },
        "createdRow": function (row, data, index) {

            // Set the html for the icons.
            var td = $(row).children('td:first-child');
            var html = "";
            var html = "<span class='iconbox' style='white-space: nowrap' > ";
            if (data.CheckIconVisible === true)
                html += "<i class='fa fa-lg fa-money checkicon' title='View Image(s)' />";

            if (self.HasImageRPSReportPermissions() === true)
                html += "<i class='fa fa-lg fa-print reporticon' title='View Item Report' />";

            html += "</span>";
            td.html(html);
            // Append data entry to column 1.
            appendDataEntry(row, data);

            // Format the Payment Amount.
            var amt = formatCurrency(data.PaymentAmount);
            $(row).children('td:nth-last-child(1)').html(amt);
        }
    };

    var formatCurrency = function (n) {
        var floatValue = parseFloat(n);
        var stringWithFixedDecimals = isFinite(floatValue) ? floatValue.toFixed(2) : n;
        return '$' + stringWithFixedDecimals.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,");
    };

    //Documents grid
    var documentGridOptions = {
        data: documentdata,
        order: [[1, "asc"]],
        "columns": [
            { "data": "DocumentSequence", sortable: false },
            { "data": "DocumentSequence" },
            { "data": "Description" },            
        ],
        "createdRow": function (row, data, index) {
            // Set the html for the icons.
            var td = $(row).children('td').eq(0);
            var html = "<span class='iconbox' >";
            if (data.DocumentIconVisible === true)
                html += "<i class='fa fa-lg fa-file-o documenticon' title='View Image(s)' />";
            else
                td.html("");
             html += "</span>";
             td.html(html);
        }
    };


    // private
    var self = this;

    var applyUI = function (model) {
        query = model;

        // Standard Breadcrumb logic.
        bread.pop("Transaction Detail");
        var bcs = bread.render();
        $getE('.breadcrumbheader').html(bcs);
        $getE('.breadcrumbheader a').click(function () {
            bread.load(_container.parent(), $(this).data('name'));
        });

        // Wire up the grids.
        paymentgrid = $getE('#paymentsTable').DataTable(paymentGridOptions);
        documentgrid = $getE('#documentsTable').DataTable(documentGridOptions);

        // The switch transaction button.
        $getE('#transactionswitch').click(function () {
            var trans = $.grep(transactiondata.BatchTransactions,
                            function(value) { return value.Item2 == self.SelectedTransactionSequence();
                });
            if (trans.length === 0) {
                if (transactiondata.BatchTransactions.length > 0) {
                    var first = transactiondata.BatchTransactions[0].Item2;
                    var last = transactiondata.BatchTransactions[transactiondata.BatchTransactions.length - 1].Item2;
                    var message = "Please enter a transaction number between @firstTrans and @lastTrans".
                        replace("@firstTrans", first).
                        replace("@lastTrans", last);
                    framework.errorToast(_container, message);
                }
            }
            else {
                query.TransactionSequence = self.SelectedTransactionSequence();
                query.TransactionID = -1;
                $getE('#stubsTable tbody').off('click');
                refresh();
            }
        });

        // Hook up images.
        $getE('#paymentsTable tbody').on('click', '.checkicon', function () {
            var rowdata = paymentgrid.row($(this).parents('tr').first()).data();
            imageservice.displaySinglePayment(transactiondata.BankID, transactiondata.WorkgroupID, transactiondata.BatchID,
                moment.utc(transactiondata.DepositDateString).format('YYYY-MM-DD'), transactiondata.TransactionId, rowdata.BatchSequence);
            return false;
        });

        $getE('#documentsTable tbody').on('click', '.documenticon', function () {
            var rowdata = documentgrid.row($(this).parents('tr').first()).data();
            imageservice.displaySingleDocument(transactiondata.BankID, transactiondata.WorkgroupID, transactiondata.BatchID,
                moment.utc(transactiondata.DepositDateString).format('YYYY-MM-DD'), transactiondata.TransactionId, rowdata.BatchSequence);
            return false;
        });

        // Hook up the report icons.
        $getE('#paymentsTable tbody').on('click', '.reporticon', function () {
            var rowdata = paymentgrid.row($(this).parents('tr').first()).data();
            imageservice.displayItemReport(transactiondata.BankID, transactiondata.WorkgroupID, transactiondata.BatchID, moment.utc(transactiondata.DepositDateString).format('YYYY-MM-DD'), self.TransactionSequence(), transactiondata.TransactionId, rowdata.BatchSequence, transactiondata.BatchNumber, moment.utc(rowdata.SourceProcessingDate).format('YYYY-MM-DD'));
            return false;
        });


        // Hook up the View All Images buttons.
        $getE('.viewallimagesicon').click(function () {
            imageservice.displayTransactionImages(transactiondata.BankID, transactiondata.WorkgroupID, transactiondata.BatchID, moment.utc(transactiondata.DepositDateString).format('YYYY-MM-DD'), transactiondata.TransactionId);
            return false;
        });

        // If redirected here from Legacy, grab the params from local storage.
        if (model.BankId === 0) {
            query = JSON.parse(window.localStorage.getItem("transactiondetailparams"));
            window.localStorage.removeItem("transactiondetailparams", null);
        }

        // Printer friendly version 
        $getE('#printerlink').click(function () {

            var parms = "BankId=" + transactiondata.BankID + "&BatchId=" + transactiondata.BatchID + "&DepositDateString=" + transactiondata.DepositDateString
                        + "&TransactionID=" + transactiondata.TransactionId + "&TransactionSequence=" + transactiondata.TransactionNumber
                        + "&WorkgroupId=" + transactiondata.WorkgroupID;
            var url = "/RecHubViews/TransactionDetail/Print?" + parms;
            window.open(url, "_blank");
        });

        // Get the transaction data.
        refresh();
    };

    var refresh = function () {
        framework.showSpinner("Loading...");
        var data = query;
        framework.doJSON('/RecHubViews/TransactionDetail/GetTransactionDetail', data, function (result) {
            if (result.error) {
                framework.errorToast(_container, result.error);
            }
            else {
                transactiondata = result.data;

                // Refresh base transaction data.
                self.BankID(transactiondata.BankID);
                self.Workgroup(transactiondata.WorkgroupID + " - " + transactiondata.WorkgroupDisplayName);
                self.DepositDateString(transactiondata.DepositDateString);
                self.AccountSiteCode(transactiondata.AccountSiteCode);
                self.BatchNumber(transactiondata.BatchNumber);
                self.BatchSiteCode(transactiondata.BatchSiteCode);
                self.BatchCueID(transactiondata.BatchCueID);
                self.TransactionSequence(transactiondata.TransactionNumber);
                self.SelectedTransactionSequence(transactiondata.TransactionNumber);
                self.BatchID(transactiondata.SourceBatchID);
                self.HasImageRPSReportPermissions(transactiondata.HasImageRPSReportPermissions);
                self.DisplayBatchID(transactiondata.DisplayBatchID);
                // Set the preferences
                var ShowBankIDOnline = $.grep(transactiondata.Preferences, function (e) { return e.Name == 'ShowBankIDOnline'; })[0];
                self.PreferenceShowBankIDOnline(ShowBankIDOnline.Value === 'Y');
                var ShowLockboxSiteCodeOnline = $.grep(transactiondata.Preferences, function (e) { return e.Name == 'ShowLockboxSiteCodeOnline'; })[0];
                self.PreferenceShowLockboxSiteCodeOnline(ShowLockboxSiteCodeOnline.Value === 'Y');
                var ShowBatchSiteCodeOnline = $.grep(transactiondata.Preferences, function (e) { return e.Name == 'ShowBatchSiteCodeOnline'; })[0];
                self.PreferenceShowBatchSiteCodeOnline(ShowBatchSiteCodeOnline.Value === 'Y');
                var DisplayBatchCueIDOnline = $.grep(transactiondata.Preferences, function (e) { return e.Name == 'DisplayBatchCueIDOnline'; })[0];
                self.PreferenceDisplayBatchCueIDOnline(DisplayBatchCueIDOnline.Value === 'Y');
                var ShowCheckSequenceOnline = $.grep(transactiondata.Preferences, function (e) { return e.Name == 'ShowCheckSequenceOnline'; })[0];
                self.PreferenceShowCheckSequenceOnline(ShowCheckSequenceOnline.Value === 'Y');
                var ShowTransactionStubSeqOnline = $.grep(transactiondata.Preferences, function (e) { return e.Name == 'ShowTransactionStubSeqOnline'; })[0];
                self.PreferenceShowTransactionStubSeqOnline(ShowTransactionStubSeqOnline.Value === 'Y');

                if (transactiondata.ViewAllImagesIconVisible)
                    $getE(".viewallimagesicon").show();

                // Refresh Grids.
                paymentgrid.column(0).visible(self.PreferenceShowCheckSequenceOnline());
                paymentgrid.clear();
                
                (transactiondata.Payments || []).forEach(function (payment) {
                    payment.Payer = htmlEncode(payment.Payer);
                    (payment.DataEntryFields || []).forEach(function (de) { de.Value = htmlEncode(de.Value);});
                });
                paymentdata = transactiondata.Payments;
                paymentgrid.data().rows.add(paymentdata);
                paymentgrid.draw();

                //Stubs
                if (stubgrid) {
                    stubgrid.clear();
                    stubgrid.destroy();
                }
                //rebuild stubs grid and flatten data entry fields
                $getE('#stubsTable thead tr').remove();
                

                var $deColumns = self.PreferenceShowTransactionStubSeqOnline() ?
                                    $("<tr><th>Stub Sequence</th></tr>") : $("<tr></tr>");
                $deColumns.append("");
                var dtColumns = self.PreferenceShowTransactionStubSeqOnline() ?
                                    [{ data: "StubSequence" }] : [];
                //icons
                $deColumns.prepend("<th></th>");
                dtColumns.unshift({ data: "StubSequence", sortable: false, width: 35 });
                var deFields = [];
                $.each(transactiondata.Stubs, function (key, stub) {
                    var deObj = { "StubSequence": stub.StubSequence };
                    deObj["StubIconVisible"] = stub.StubIconVisible;
                    deObj["ItemReportIconVisible"] = stub.ItemReportIconVisible;
                    deObj["BatchSequence"] = stub.BatchSequence;
                    deObj["DocumentBatchSequence"] = stub.DocumentBatchSequence;
                    if (stub.DataEntryFields != null) {
                        $.each(stub.DataEntryFields, function(i, de) {
                        //builds column headers for the dom and  data array for DataTable
                            var gp = $.grep(dtColumns, function(value) { return value.data === de.Title; }); 
                            if (gp.length === 0) {
                                $deColumns.append("<th>" + de.Title + "</th>");
                                var col = {data: de.Title};
                                // If type is either number or currency, right-justify.
                                if (de.Type === "7" || de.Type === "6")
                                    col.class = "alignRight";
                                dtColumns.push(col);
                            }
                            //Type = 7 currency
                            deObj[de.Title] = (de.Type === "7" && de.Value && de.Value.length > 0)
                                ? formatCurrency(de.Value)
                                : htmlEncode(de.Value);
                        });
                    }
                    deFields.push(deObj);
                });
                                
                

                // Measure the widths for each header.
                $getE('#stubsTable thead').append($deColumns);
                $.each(dtColumns, function(i, e) {
                    e.width = e.width ? e.width : utilities.measureTextWidth($getE('#stubsTable thead th').eq(i).text());
                });

                // disable sorting when we only have 1 column for the icons.
                let sort = dtColumns.length > 1;
                let order = sort ? [[1, "asc"]] : [];

                //Create new DataTables
                stubgrid = $getE('#stubsTable').DataTable({
                    autoWidth: false,
                    scrollX: true,
                    data: deFields,
                    sort: sort,
                    order: order,
                    columns: dtColumns,
                    createdRow: function (row, data, index) {
                        // Set the html for the icons.
                        var td = $(row).children('td:first-child');
                        var html = "<span class='iconbox' >";
                        if (data.StubIconVisible === true)
                            html += "<i class='fa fa-lg fa-file-o documenticon' title='View Image(s)'/>";

                        if (self.HasImageRPSReportPermissions() === true)
                            html += "<i class='fa fa-lg fa-print reporticon' title='View Item Report'/>";

                        html += "</span>";
                        td.html(html);
                    }
                });


                // Stub images
                $getE('#stubsTable tbody').on('click', '.documenticon', function() {
                    var rowdata = stubgrid.row($(this).parents('tr').first()).data();
                    imageservice.displaySingleDocument(transactiondata.BankID, transactiondata.WorkgroupID, transactiondata.BatchID, moment.utc(transactiondata.DepositDateString).format('YYYY-MM-DD'), transactiondata.TransactionId, rowdata.DocumentBatchSequence);
                    return false;
                });

                // Stub report
                $getE('#stubsTable tbody').on('click', '.reporticon', function() {
                    var rowdata = stubgrid.row($(this).parents('tr').first()).data();
                    imageservice.displayItemReport(transactiondata.BankID, transactiondata.WorkgroupID, transactiondata.BatchID,
                         moment.utc(transactiondata.DepositDateString).format('YYYY-MM-DD'), self.TransactionSequence(), transactiondata.TransactionId,
                         rowdata.BatchSequence, transactiondata.BatchNumber, moment.utc(rowdata.SourceProcessingDate).format('YYYY-MM-DD'));
                    return false;
                });

                //docs
                documentgrid.clear();
                documentdata = transactiondata.Documents;
                documentgrid.data().rows.add(documentdata);
                documentgrid.draw();
            }
            framework.hideSpinner();
        });
    };

    let htmlEncode = function(value) {
        //create a in-memory div, set it's inner text(which jQuery automatically encodes)
        //then grab the encoded contents back out.  The div never exists on the page.
        return $('<div/>').text(value).html();
    };
    // Constructor
    function transactionDetailViewModel() {

    }

    // Public properties
    self.BankID = ko.observable(0);
    self.Workgroup = ko.observable('');
    self.DepositDateString = ko.observable('');
    self.AccountSiteCode = ko.observable(0);
    self.BatchNumber = ko.observable(0);
    self.BatchSiteCode = ko.observable(0);
    self.BatchCueID = ko.observable(0);
    self.TransactionSequence = ko.observable(0);
    self.SelectedTransactionSequence = ko.observable(0);
    self.PaymentTotal = ko.observable(0);
    self.StubTotalAmount = ko.observable(0);
    self.BatchID = ko.observable(0);
    self.HasImageRPSReportPermissions = ko.observable(false);
    self.PreferenceShowBankIDOnline = ko.observable(false);
    self.PreferenceShowLockboxSiteCodeOnline = ko.observable(false);
    self.PreferenceShowBatchSiteCodeOnline = ko.observable(false);
    self.PreferenceDisplayBatchCueIDOnline = ko.observable(false);
    self.PreferenceShowCheckSequenceOnline = ko.observable(false);
    self.PreferenceShowTransactionStubSeqOnline = ko.observable(false);
    self.DisplayBatchID = ko.observable(false);
    // Public methods
    transactionDetailViewModel.prototype = {
        constructor: transactionDetailViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) { return _container.find(b); };

            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            // Function to clean up our data.
            $getE('#paymentsTable tbody').off('click');
            $getE('#stubsTable tbody').off('click');
            $getE('#documentsTable tbody').off('click');
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return transactionDetailViewModel;
});