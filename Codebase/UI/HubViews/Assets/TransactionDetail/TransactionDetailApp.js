﻿function initTransactionDetail(model) {
    // File contains initialize code.
    require.config({
        paths: {
            // Dependencies for Batch summary.
            // Notice we're using absolute paths so we don't screw up the baseURL for framework.
            'transactionDetailController': '/RecHubViews/Assets/TransactionDetail/Controllers/TransactionDetailController',
            'transactionDetailViewModel': '/RecHubViews/Assets/TransactionDetail/ViewModels/TransactionDetailViewModel'
        }
    });

    require(['transactionDetailController'], function (controller) {
        controller.init(model);
    });
}