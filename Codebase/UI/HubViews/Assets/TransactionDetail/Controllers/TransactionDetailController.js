﻿define(['jquery', 'ko', 'frameworkUtils', 'transactionDetailViewModel'],
function ($, ko, framework, transactionDetailViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            var container = $('#transactionDetailPortlet');
            _vm = new transactionDetailViewModel();
            _vm.init(container, model);

            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }

    return {
        init: init
    };
});