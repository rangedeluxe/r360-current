﻿function initBatchDetail(model) {
    // File contains initialize code.
    require.config({
        paths: {
            // Dependencies for Batch summary.
            // Notice we're using absolute paths so we don't screw up the baseURL for framework.
            'batchDetailController': '/RecHubViews/Assets/BatchDetail/Controllers/BatchDetailController',
            'batchDetailViewModel': '/RecHubViews/Assets/BatchDetail/ViewModels/batchDetailViewModel'
        }
    });

    require(['batchDetailController'], function (controller) {
        controller.init(model);
    });
}