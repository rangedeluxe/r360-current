﻿// Controls the batch detail UI, uses these dependencies.

define(['jquery', 'ko', 'frameworkUtils', 'batchDetailViewModel'],
function ($, ko, framework, batchDetailViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            // Set up the initialize for Batch summary.
            var container = $('#batchDetailPortlet');
            _vm = new batchDetailViewModel();
            _vm.init(container, model);

            // Wire up the disposal for Batch summary.
            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    // Need this so we can get inside this 'define' method every time we hit the page.
    // See BatchSummaryApp.js on how it's called.
    return {
        init: init
    };
});