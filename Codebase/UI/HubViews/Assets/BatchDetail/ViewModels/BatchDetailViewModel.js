﻿// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.

define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js',
    '/RecHubViews/Assets/Shared/ImageService.js', '/Assets/wfs/wfs-dateutil.js', 'datatables',
    'datatables-bootstrap', 'bootstrap', 'moment'],
function ($, framework, ko, breadcrumbs, imageService) {

    var _container = $('#batchDetailPortlet');
    var _parameters;
    var $getE;
    var searchOnLoad = false;
    var _breadcrumbs = new breadcrumbs();
    var _imageService = new imageService();

    // private
    var self = this;
    var applyUI;
    var getBatchDetailsCallback = function (serverResponse) {
        if (serverResponse.status === false) {
            framework.errorToast(_container, serverResponse.error);
        } else {
            applyUI({ Data: serverResponse });
            ko.applyBindings(self, _container.get(0));
        }
    };

    var getBatchImagesAvailableCallback = function (batchImagesAvailable) {
        if (batchImagesAvailable) {
            $getE(".allBatchImage").show();
        }
    };

    var sum = function (items, prop) {
        return items.reduce(function (a, b) {
            return a + b[prop];
        }, 0);
    };

    // Sets up all of the UI elements and handles callbacks.
    applyUI = function(model) {
        framework.showSpinner("Loading...");

        var data = model.Data;

        this.Workgroup(data.workgroup);
        this.WorkgroupId(data.workgroupId);
        this.DepositDate(data.depositDate);
        this.AccountSiteCode(data.accountSiteCode);
        this.BatchSiteCode(data.batchSiteCode);
        this.BatchId(data.batchId);
        this.SourceBatchId(data.sourceBatchId);
        this.Batch(data.batch);
        this.Bank(data.bank);
        this.BatchCue(data.batchCue);
        this.ShowAccountSiteCode(data.showAccountSiteCode);
        this.ShowBatchSiteCode(data.showBatchSiteCode);
        this.ShowBank(data.showBank);
        this.ShowBatchCue(data.showBatchCue);
        this.showImageRpsAudit(data.showImageRpsAudit);
        this.ShowBatchId(data.showBatchId);
        
        // Standard Breadcrumb logic.
        _breadcrumbs.pop("Batch Detail");
        var bcs = _breadcrumbs.render();
        $getE('.breadcrumbheader').html(bcs);
        $getE('.breadcrumbheader a').click(function () {
            _breadcrumbs.load(_container.parent(), $(this).data('name'));
        });

        // Overwrite the stored query so we can come back after transaction detail.
        var query = JSON.stringify({
            BankId: data.bank,
            WorkgroupId: data.workgroupId,
            BatchId: data.batchId,
            DepositDateString: data.depositDate,
            Source: BreadCrumbText()
        });
        window.localStorage.setItem("batchdetailparams", query);

        if (data.data[0].PaymentType.toUpperCase() === "ACH" || data.data[0].PaymentType.toUpperCase() === "WIRE") {
            getBatchImagesAvailableCallback(true);
        } 
        else {
            _imageService.imagesExist(_container, {
                    BankId: data.bank,
                    WorkgroupId: data.workgroupId,
                    BatchId: data.batchId,
                    DepositDateString: data.depositDate,
                    SourceBatchId: data.sourceBatchId,
                    PICSDate: data.data[0].PICSDate,
                    BatchPaymentSource: data.data[0].BatchSourceShortName
                },
                getBatchImagesAvailableCallback
            );
        }
        // www.datatables.net
        // Note: big difference between 'dataTable' and 'DataTable'.
        // One is jquery object, other is DataTable object.
        _batchtable = $getE('#batchDetailsTable').DataTable({
            data: data.data,
            columns: [
                { "data": "TxnSequence", class:"alignRight", name:"TxnSequence"},
                { "data": "Amount" , "className": "align-right alignRight", name:"Amount"},
                { "orderable": false, "defaultContent": '<a class="showTip paymentImage" title="View Payment Image" href="#"><i class="fa fa-money fa-lg"></i></a>&nbsp;&nbsp;<a class="showTip docImage" title="View Transaction Image" href="#"><i class="fa fa-file-o fa-lg"></i></a>&nbsp;&nbsp;<a class="showTip allImage" title="View All Images for Transaction" href="#"><i class="fa fa-picture-o fa-lg"></i></a>' },
                { "data": "RT" },
                { "data": "Account" },
                { "data": "Serial" },
                { "data": "RemitterName" },
                { "data": "DDA" },
                { "orderable": false, "visible": showImageRpsAudit(), "defaultContent": '<a class="showTip imageRPSAudit" title="View ImageRPS Audit" href="#"><i class="fa fa-print fa-lg"></i></a>' }
            ],
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api();

                var total = api.column(1).data().reduce(function (a, b) {
                    return a + b;
                }, 0);
                var totalcurrency = formatCurrency(total.toFixed(2).toString());
                self.Total(totalcurrency);
            },
            "createdRow": function (row, data, index) {
                // This is to format the 'amount' column to look like currency.
                if (data.Amount < 0) data.amount = 0;
                var amt = formatCurrency(data.Amount.toFixed(2).toString());
                $(row).children('td').eq(1).html(amt);

                // Assemble the Images for the PDF views
                var imageString = "";
                if (data.ShowPaymentIcon === true) {
                    imageString += '<a class="showTip paymentImage" title="View Payment Image(s)" href="#"><i class="fa fa-money fa-lg"></i></a>&nbsp;&nbsp;';
                }
                if (data.ShowDocumentIcon === true) {
                    imageString += '<a class="showTip docImage" title="View Transaction Image(s)" href="#"><i class="fa fa-file-o fa-lg"></i></a>&nbsp;&nbsp;';
                }
                if (data.ShowAllIcon === true) {
                    imageString += '<a class="showTip allImage" title="View All Images for Transaction" href="#"><i class="fa fa-picture-o fa-lg"></i></a>';
                }
                $(row).children('td').eq(2).html(imageString);

                // And this is to add a small tool-tip.
                $(row).attr('title', 'Click here for Transaction Detail');
            }

        });
     
        // Recommended method of handling row clicks.
        $getE('#batchDetailsTable tbody').on('click', 'tr', function () {
            var rowdata = _batchtable.row(this).data();
            
            // Load up the transaction detail portlet.
            var url = '/RecHubViews/TransactionDetail';
            _breadcrumbs.register("Batch Detail", "/RecHubViews/BatchDetail/IndexResults", { BankId: self.Bank(), WorkgroupId: self.WorkgroupId(), BatchId: self.BatchId(), DepositDateString: self.DepositDate() });
            var tdRequest = {
                BankId: rowdata.BankId,
                WorkgroupId: rowdata.WorkgroupId,
                BatchId: rowdata.BatchId,
                DepositDateString: rowdata.DepositDateString,
                TransactionId: rowdata.TransactionID,
                TransactionSequence: rowdata.TxnSequence
            };
            framework.loadByContainer(url, _container.parent(), tdRequest);
        });

        $getE('#batchDetailsTable tbody').on('click', '.paymentImage', function () {
            var rowdata = _batchtable.row($(this).parents('tr').first()).data();
            _imageService.displaySinglePayment(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'), rowdata.TransactionID, rowdata.BatchSequence);
            return false;
        });

        $getE('#batchDetailsTable tbody').on('click', '.docImage', function () {
            var rowdata = _batchtable.row($(this).parents('tr').first()).data();
            _imageService.displayTransactionDocuments(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'), rowdata.TxnSequence);
            return false;
        });

        $getE('#batchDetailsTable tbody').on('click', '.allImage', function () {
            var rowdata = _batchtable.row($(this).parents('tr').first()).data();
            _imageService.displayTransactionImages(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'), rowdata.TransactionID);
            return false;
        });

        $getE('.allBatchImage').on('click', function () {
            var rowdata = _batchtable.row(0).data();
            _imageService.displayBatchImages(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, rowdata.BatchNumber, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'));
            return false;
        });

        $getE('#batchDetailsTable tbody').on('click', '.imageRPSAudit', function () {
            var rowdata = _batchtable.row($(this).parents('tr').first()).data();
            _imageService.displayTransactionReport(rowdata.BankId, rowdata.WorkgroupId, rowdata.BatchId, moment.utc(rowdata.DepositDate).format('YYYY-MM-DD'), rowdata.TxnSequence, rowdata.TransactionID, rowdata.BatchSequence, rowdata.BatchNumber, moment.utc(rowdata.ProcessingDate).format('YYYY-MM-DD'));
            return false;
        });

        $getE('#lnkBatchSummary').on('click', function () {
         
            var url = BreadCrumbLink();
            if (BreadCrumbText() === "Batch Summary") {
                framework.loadByContainer(url, _container.parent(), null);
            }
            else {
                window.location = url;
            }
            return false;
        });

        // Printer friendly version (Still going to legacy page for this one).
        $getE('#printerlink').click(function () {
            var parms = "BankId=" + self.Bank() + "&WorkgroupId=" + self.WorkgroupId() + "&BatchId=" + self.BatchId() + "&DepositDateString=" + self.DepositDate();
            var url = "/RecHubViews/BatchDetail/Print?" + parms;
            window.open(url, "_blank");
        });

        framework.hideSpinner();

    };
    var formatCurrency = function (n) {
        return '$' + n.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,");
    };

   
    // Constructor
    function batchDetailViewModel() {

    }

    // Public properties
  
    this.Workgroup = ko.observable('');
    this.WorkgroupId = ko.observable('');
    this.DepositDate = ko.observable('');
    this.AccountSiteCode = ko.observable('');
    this.BatchId = ko.observable('');
    this.SourceBatchId = ko.observable('');
    this.Batch = ko.observable('');
    this.BatchSiteCode = ko.observable('');
    this.Bank = ko.observable('');
    this.BatchCue = ko.observable('');
    this.ShowAccountSiteCode = ko.observable(false);
    this.ShowBatchSiteCode = ko.observable(false);
    this.ShowBatchId = ko.observable(false);
    this.ShowBank = ko.observable(false);
    this.ShowBatchCue = ko.observable(false);
    this.showImageRpsAudit = ko.observable(false);
    this.Total = ko.observable(0);
    this.BreadCrumbLink = ko.observable('');
    this.BreadCrumbText = ko.observable('');

    this.Results = [];

    // Public methods
    batchDetailViewModel.prototype = {
        constructor: batchDetailViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) { return _container.find(b); };

            if (model.Data.error === "Request Records Pending") {
                _parameters = JSON.parse(window.localStorage.getItem("batchdetailparams"));
                framework.doJSON("/RecHubViews/BatchDetail/GetBatchDetails", $.toDictionary(_parameters), getBatchDetailsCallback);
            } else {
                applyUI(model);
                ko.applyBindings(self, container.get(0));
            }
            
        },
        dispose: function () {
            // Function to clean up our data.
            $getE('#batchDetailsTable tbody').off('click');
            Results = [];
            searchOnLoad = false;
            Workgroup(null);
            if (_batchtable)
                _batchtable.destroy();
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return batchDetailViewModel;
});