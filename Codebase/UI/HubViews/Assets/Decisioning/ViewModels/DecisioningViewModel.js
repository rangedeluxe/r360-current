﻿define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/DataTablesUtilities.js',
    '/RecHubViews/Assets/Shared/Breadcrumbs.js', '/RecHubViews/Assets/Shared/Formatting.js', 'bootstrap', 'datatables-bootstrap'],
function ($, framework, ko, utilities, breadcrumbs, formatting) {
    let $getE;
    let self = this;
    let _container;
    let _utilities = new utilities();
    let _exceptionsTable;
    let _breadcrumbs = new breadcrumbs();
    let _formatting = new formatting();
    let _gridOptions = {
        processing: false,
        ajax: {
            url: '/RecHubViews/Decisioning/GetPendingBatches',
            type: 'POST'
        },
        autoWidth: false,
        columns: [
            { "data": "Entity", width: 150 },
            { "data": "WorkgroupName" },
            { "data": "BatchId", class: 'alignRight' },
            { "data": "NumberOfTransactions", class: 'alignRight' },
            { "data": "DeadLine", "name" : "Deadline" },
            { "data": "DepositStatusDisplayName" },
            { "data": "User" },
            { "data": "DepositDate" },
            { "data": "ProcessDate" },
            { "data": "PaymentSource"}
        ],
        scrollX: true,
        "createdRow": function (row, data, index) {
            //adds tooltip
            $(row).prop('title', 'Click here to view Exceptions Detail');

            let api = this.api();
            let colindex = api.column('Deadline:name')[0][0];
            let td = api.cells(row, colindex).nodes()[0];
            $(td)
                .empty()
                .append(_formatting.toLocalTimeZone(data.DeadLine, false));
        }
    };

    let applyUI = function (model) {
        // Set the widths for each.
        $.each(_gridOptions.columns, function(i, e) {
            e.width = e.width ? e.width : _utilities.measureTextWidth($getE('#exceptionsTable thead th').eq(i).text());
        });
        $getE('#exceptionsTable').on('preXhr.dt', function (e, settings, data) {
            framework.showSpinner('Loading...');
        });
        $getE('#exceptionsTable').on('xhr.dt', function (e, settings, json, xhr) {
            framework.hideSpinner();
            if (json.error) {
                json.data = [];
                framework.errorToast(_container, json.error);
            }
        });
        _exceptionsTable = $getE('#exceptionsTable').DataTable(_gridOptions);

        $getE('#exceptionsTable tbody').on('click', 'tr', function () {
            
            var rowdata = _exceptionsTable.row(this).data();
            let data = { globalBatchId: rowdata.GlobalBatchId };
            _breadcrumbs.register("Pre-Deposit Exceptions Summary", "/RecHubViews/Decisioning", {});
            framework.loadByContainer("/RecHubViews/Decisioning/Detail", _container.parent(), data);
        });

    };

    function decisioningViewModel() {

    }

    this.Response = ko.observable({});

    decisioningViewModel.prototype = {
        constructor: decisioningViewModel(),
        init: function(container, model) {
            _container = container;
            $getE = function(a) {
                return _container.find(a);
            };
            // Function to measure text width.
            $.fn.textWidth = function(text, font) {
                if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<strong>').hide().appendTo(document.body);
                $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
                return $.fn.textWidth.fakeEl.width();
            };

            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },

        dispose: function () {
            if (_exceptionsTable) {
                _exceptionsTable.destroy();
            }
            $getE('#exceptionsTable tbody').off('click');
        }
    };
    return decisioningViewModel;
});