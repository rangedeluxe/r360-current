define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js', 
    '/RecHubViews/Assets/Shared/Formatting.js', '/RecHubViews/Assets/Shared/DataTablesUtilities.js', 'bootstrap'],
function ($, framework, ko, breadcrumbs, formatting, utilities) {
    let $getE;
    let self = this;
    let _container;
    let _model;
    let _data;
    let applyUI = function (model) {
       _model = model;

       $getE('.button-cancel').click(function() {
           close(null);
       });

       $getE('.button-save').click(function() {
           close(_data);
       });

       refreshData();
    };

    let refreshData = function() {
        framework.showSpinner('Loading...');
        framework.doJSON('/RecHubViews/Decisioning/GetSingleTransaction', { globalBatchId: _model.GlobalBatchId, transactionid: _model.TransactionId }, function(result) {
            if (!result || result.error) {
                framework.errorToast(_container, result.error);
                return;
            }
            let row = $.grep(result.data.Stubs, function(e) {
                return e.DEItemRowDataID == _model.DEItemRowDataID;
            });
            if (row.length == 0) {
                framework.errorToast(_container, "Could not find row.");
                return;
            }
            _data = row[0];
            this.EditableFields(_data.DataEntry);
            framework.hideSpinner();
        });
    };

    let close = function(data) {
        framework.closeModal(data);
    };

    //public properties
    this.EditableFields = ko.observable([]);

    function decisioningRowEditorViewModel() { };

    decisioningRowEditorViewModel.prototype = {
        constructor: decisioningRowEditorViewModel(),
        init: function (container, model) {
            _container = container;
            $getE = function (a) {
                return _container.find(a);
            };
            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function(){
            _model = null;
            _data = null;
            this.EditableFields([]);
        }
    };
    return decisioningRowEditorViewModel;
});