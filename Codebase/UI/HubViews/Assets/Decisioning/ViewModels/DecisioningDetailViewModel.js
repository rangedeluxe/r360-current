﻿define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js',
    '/RecHubViews/Assets/Shared/DataTablesUtilities.js', '/RecHubViews/Assets/Shared/Formatting.js',
    '/RecHubViews/Assets/Shared/ConfirmationDialog.js', '/RecHubViews/Assets/Shared/OptionDialog.js',
    'bootstrap', 'datatables-bootstrap', 'bootstrap-toggle', 'datatables-bootstrap'],
    function ($, framework, ko, breadcrumbs, utilities, formatting, confirmdialog, optiondialog) {
        let FEATURE_COUNT_THRESHOLD = 10;

        let $getE;
        let self = this;
        let _container;
        let _breadcrumbs = new breadcrumbs();
        let _utilities = new utilities();
        let _formatting = new formatting();
        let _detailsTable;
        let _batchResetID;
        let _model;
        let _dialog = new confirmdialog();
        let _optiondialog = new optiondialog();
        let LOCKED = 241;
        let UNLOCKED = 240;
        let labels = {
            'submitToComplete': "Submit the batch to complete processing",
            'submitDisabled': "Submit permitted only if batch is locked by current user and all " +
            "transactions have been accepted or rejected.",
            'sliderLock': "Click here to unlock the batch",
            'sliderUnlock': "Click here to lock the batch.",
            'unlockPermissions': "You do not have permissions to unlock this batch.",
            'usercantlock': "You do not have permissions to lock this batch.",
            'gridHover': "Click here to view Exceptions Transaction Detail"
        };

        let showPrintView = function () {
            let tz = _formatting.getLocalTimeZoneOffset();
            let tzname = _formatting.getLocalTimeZone();
            window.open('/RecHubViews/Decisioning/AllTransactionsPrintableView?globalBatchId=' + _model + '&timezoneoffset=' + tz + '&timezonename=' + tzname);
        };

        let completeBatch = function (callback) {
            framework.showSpinner('Loading...');
            var data = { "batchResetId": _batchResetID, "globalBatchId": _model };
            framework.doJSON('/RecHubViews/Decisioning/CompleteBatch', data, function (result) {
                if (result.error) {
                    framework.hideSpinner();
                    framework.errorToast(_container, result.error);
                    return;
                }
                if (callback) {
                    callback();
                }
                framework.hideSpinner();
                framework.successToast(_container, "Your work has been successfully submitted");
                framework.loadByContainer("/RecHubViews/Decisioning/", _container.parent(), {});
            });
        };

        let applyUI = function (model) {
            _model = model;
            _breadcrumbs.pop('Exceptions Detail');
            let bcs = _breadcrumbs.render();
            $getE('.breadcrumbheader').html(bcs);
            $getE('.breadcrumbheader a')
                .click(function () {
                    _breadcrumbs.load(_container.parent(), $(this).data('name'));
                });

            $getE('#printableView').click(function () {
                showPrintView();
            });

            loadBatchDetails(model);
            $getE('#batch-lock').change(function () {
                let data = { "globalBatchId": _model };
                if ($(this).is(':checked')) {
                    framework.showSpinner('Loading...');
                    framework.doJSON('/RechubViews/Decisioning/LockBatch', data, function (res) {
                        if (!res || !res.success) {
                            framework.errorToast(_container, res.error);
                            framework.hideSpinner();
                            return;
                        }
                        exceptionHeaderUpdate(res);
                    });
                } else {
                    framework.showSpinner('Loading...');
                    framework.doJSON('/RechubViews/Decisioning/UnlockBatch', data, function (res) {
                        if (!res || !res.success) {
                            framework.errorToast(_container, res.error);
                            framework.hideSpinner();
                            return;
                        }
                        exceptionHeaderUpdate(res);
                    });
                }
            });
            $getE('.submit-button').click(function () {
                _optiondialog.show("Confirm Submit", "You have selected to submit batch " + self.Batch() +
                    ". After submitting a batch, you cannot go back to view, print, or change details.<br/><br/>" +
                    "To print the Exception Detail Report and submit, click <strong>Print &amp; Submit</strong>.<br/>" +
                    "To submit without printing, click <strong>Submit</strong>.<br/>",
                    [
                        {
                            text: 'Print &amp; Submit',
                            action: function() {
                                completeBatch(function(){
                                    showPrintView();
                                });
                            },
                            class: 'btn-primary'
                        },
                        {
                            text: 'Submit',
                            action: function() {
                                completeBatch();
                            },
                            class: 'btn-primary'
                        },
                        {
                            text: 'Cancel',
                            action: function() { },
                            class: 'btn-inverse'
                        }
                    ], 575);
            });
        }

        let loadBatchDetails = function (model) {
            //fetch batch details
            let data = { "globalBatchId": model };
            framework.doJSON('/RechubViews/Decisioning/GetExceptionDetail', data, exceptionDetailsCallBack);
            framework.showSpinner('Loading...');
        }
        let exceptionHeaderUpdate = function (data) {

            if (!data || !data.success) {
                framework.errorToast(_container, 'Error Loading Batch details.');
                return;
            }
            let batch = data.data;
            _batchResetID = batch.BatchResetID;
            self.LockBox(batch.WorkgroupName);
            self.DepositDate(batch.DepositDate);
            self.Batch(batch.BatchId);
            self.DepositStatus(batch.DepositStatusDisplayName);
            self.DecisioningDeadLine(_formatting.toLocalTimeZone(batch.DeadLine, true));
            let user = batch.DepositStatus === LOCKED ?
                batch.User : "";
            self.LockSwitch(batch.DepositStatus === LOCKED);
            $getE('.LockToggle').bootstrapToggle();
            self.User(user);
            self.Entity(batch.Entity);
            self.PaymentSource(batch.PaymentSource);
            self.ProcessDate(batch.ProcessDate);
            //find the transactions the user has accepted or rejected
            var acceptedOrRejectedTxns = batch.Transactions.filter(function (txn) {
                return txn.TransactionStatus === 3 || txn.TransactionStatus === 4;
            });
            //make sure the user has the batch locked and all the transactions have either been accepted or rejected
            self.SubmitEnabled(batch.UserCanEdit && acceptedOrRejectedTxns.length === batch.Transactions.length);
            self.LockEnabled((batch.UserCanUnlock === true && batch.DepositStatus === LOCKED) ||
                             (batch.DepositStatus === UNLOCKED && batch.UserCanLock));
            //UserCanUnlock = unlock override
            //UserCanEdit  = r360 permission to manage exceptions
            if (self.LockEnabled()) {
                self.SliderMessage(self.LockSwitch() ? labels['sliderLock'] : labels['sliderUnlock']);
                $getE('.LockToggle').parent().attr('title', self.SliderMessage());
            }
            else {
                //switchbutton already disabled we now check the state for proper message
                let message = batch.DepositStatus === LOCKED ? labels['unlockPermissions'] : labels['usercantlock'];
                self.LockDisabledTitle(message);
                $getE('.LockToggle').parent().attr('title', self.LockDisabledTitle());
            }
            if (!self.LockEnabled())
                $getE('.LockToggle').bootstrapToggle('disable');
            else
                $getE('.LockToggle').bootstrapToggle('enable');

            self.SubmitMessage(batch.UserCanEdit ? labels['submitToComplete'] : labels['submitDisabled']);
            framework.hideSpinner();
        };

        let exceptionDetailsCallBack = function (data) {
            exceptionHeaderUpdate(data);

            let batch = data.data;
            self.LockSwitch(batch.DepositStatus === LOCKED);
            $getE('.LockToggle').bootstrapToggle();
            let _gridOptions = {
                data: batch.Transactions,
                columns: [
                    { "data": "TransactionSequence", class: 'alignRight' },
                    { "data": "Amount", class: 'alignRight', name: 'Amount' },
                    { "data": "RT" },
                    { "data": "Account" },
                    { "data": "CheckTraceRef" },
                    { "data": "Payer" },
                    { "data": "TransactionStatusDisplay" },
                ],
                drawCallback: function(settings) {
                    let enablefeatures = batch.Transactions && batch.Transactions.length > FEATURE_COUNT_THRESHOLD;
                    if (enablefeatures) {
                        $getE('#transactionsTable_length').parent().parent().show();
                        $getE('#transactionsTable_info').parent().parent().show();
                    }
                    else {
                        $getE('#transactionsTable_length').parent().parent().hide();
                        $getE('#transactionsTable_info').parent().parent().hide();
                    }
                },
                "createdRow": function (row, data, index) {
                    let api = this.api();

                    $(row).prop('title', labels['gridHover']);
                    let colindex = api.column('Amount:name')[0][0];
                    let td = api.cells(row, colindex).nodes()[0];
                    let currency = _formatting.formatCurrency(data.Amount);
                    $(td).html(currency);
                }
            };

            $.each(_gridOptions.columns, function (i, e) {
                e.width = e.width ? e.width : _utilities.measureTextWidth($getE('#transactionsTable thead th').eq(i).text());
            });

            _detailsTable = $getE('#transactionsTable').DataTable(_gridOptions);
            _detailsTable.on('preXhr.dt', function (e, settings, data) {
                framework.showSpinner('Loading...');
            });
            _detailsTable.on('xhr.dt', function (e, settings, json, xhr) {
                framework.hideSpinner();
                if (json.error) {
                    json.data = [];
                    framework.errorToast(_container, json.error);
                }
            });

            $getE('#transactionsTable tbody').on('click', 'tr', function () {
                let rowData = _detailsTable.row(this).data();
                let data = { GlobalBatchId: _model, TransactionId: rowData.TransactionId };
                _breadcrumbs.register("Exceptions Detail", "/RecHubViews/Decisioning/Detail",
                    { globalBatchId: _model });
                framework.loadByContainer("/RecHubViews/Decisioning/TransactionDetail", _container.parent(), data);
            });

        };
        //public bound props
        this.LockBox = ko.observable('');
        this.DepositDate = ko.observable('');
        this.Batch = ko.observable(0);
        this.DepositStatus = ko.observable('');
        this.LockSwitch = ko.observable(false);
        this.DecisioningDeadLine = ko.observable('');
        this.User = ko.observable('');
        this.Entity = ko.observable('');
        this.PaymentSource = ko.observable('');
        this.ProcessDate = ko.observable('');
        this.SubmitEnabled = ko.observable(false);
        this.LockEnabled = ko.observable(false);
        this.LockDisabledTitle = ko.observable('');
        this.SubmitMessage = ko.observable('');
        this.SliderMessage = ko.observable('');
        //ctor
        function decisioningDetailViewModel() {

        }

        decisioningDetailViewModel.prototype = {
            constructor: decisioningDetailViewModel(),
            init: function (container, model) {
                _container = container;
                $getE = function (a) {
                    return _container.find(a);
                };
                applyUI(model);
                ko.applyBindings(self, container.get(0));
            },
            dispose: function () {
                self.LockBox('');
                self.DepositDate('');
                self.Batch(0);
                self.DepositStatus('');
                self.LockSwitch(false);
                self.DecisioningDeadLine('');
                self.CheckedOutMessage('');
                self.Entity('');
                self.User('');
                self.PaymentSource('');
                self.ProcessDate('');
                self.LockEnabled(false);
                self.LockDisabledTitle('');
                if (_detailsTable) {
                    _detailsTable.destroy();
                }
            }
        };
        return decisioningDetailViewModel;
    });