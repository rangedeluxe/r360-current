﻿define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js',
    '/RecHubViews/Assets/Shared/Formatting.js', '/RecHubViews/Assets/Shared/DataTablesUtilities.js',
    '/RecHubViews/Assets/Shared/ImageService.js', '/RecHubViews/Assets/Shared/ConfirmationDialog.js', 'bootstrap'],
    function ($, framework, ko, breadcrumbs, formatting, utilities, imageService, confirmationDialog) {
        let FEATURE_COUNT_THRESHOLD = 10;

        let $getE;
        let self = this;
        let _container;
        let _breadcrumbs = new breadcrumbs();
        let _formatting = new formatting();
        let _utilities = new utilities();
        let _imageService = new imageService();
        let _confirmationDialog = new confirmationDialog();
        let _paymentsTable;
        let _stubsTable;
        let _model;
        let _data;
        let _currenttransaction;
        let _generatedID = 0;
        let _errorControls;
        let labels = {
            'saveEditsAllowed': "Click here to save changes to the current transaction",
            'acceptsEditsAllowed': "Click here to accept the current transaction",
            'rejectEditsAllowed': "Click here to reject the current transaction",
            'noEditsAllowed': "Edits permitted only if locked by current user"
        };
        let applyUI = function (model) {
            _model = model;
            _breadcrumbs.pop("Exceptions Transaction Detail");
            let bcs = _breadcrumbs.render();
            $getE('.breadcrumbheader').html(bcs);

            $getE('.breadcrumbheader a').click(function () {
                _breadcrumbs.load(_container.parent(), $(this).data('name'));
            });

            $getE('.view-all-images-link').click(function () {
                _imageService.displayInProcessExceptionImages({
                    Workgroup: _data.ParentBatch.WorkgroupName,
                    WorkgroupId: _data.ParentBatch.WorkgroupId,
                    PaymentSource: _data.ParentBatch.PaymentSource,
                    ProcessingDate: _data.ParentBatch.ProcessDate,
                    DepositDate: _data.ParentBatch.DepositDate,
                    BankId: _data.ParentBatch.BankId,
                    BatchId: _data.ParentBatch.BatchId,
                    TransactionId: _currenttransaction.TransactionId,
                    GlobalBatchId: _data.ParentBatch.GlobalBatchId,
                    TransactionSequence: _currenttransaction.TransactionSequence,
                    Amount: _currenttransaction.Amount
                });
                return false;
            });

            $getE('#button-first-transaction').click(function () {
                if (self.UserCanEdit()) {
                    saveChanges(false, function (success) {
                        if (success === true) {
                            applyFirstTransaction();
                        }
                    });
                }
                else {
                    applyFirstTransaction();
                }
            });

            $getE('#button-previous-transaction').click(function () {
                if (self.UserCanEdit()) {
                    saveChanges(false, function (success) {
                        if (success === true) {
                            applyPreviousTransaction();
                        }
                    });
                }
                else {
                    applyPreviousTransaction();
                }
            });

            $getE('#button-next-transaction').click(function () {
                if (self.UserCanEdit()) {
                    saveChanges(false, function (success) {
                        if (success === true) {
                            applyNextTransaction();
                        }
                    });
                }
                else {
                    applyNextTransaction();
                }
            });

            $getE('#button-last-transaction').click(function () {
                if (self.UserCanEdit()) {
                    saveChanges(false, function (success) {
                        if (success === true) {
                            applyLastTransaction();
                        }
                    });
                }
                else {
                    applyLastTransaction();
                }
            });

            $getE('.save-button').click(function () {
                saveChanges();
            });

            $getE('#button-approve').click(function () {
                approveTransaction(true);
            });

            $getE('#button-reject').click(function () {
                rejectTransaction(true);
            });

            // A rather crude way to 'bind' the value to a particular DE field.
            $getE('#stubs-table-container').on("change", ".stub-editor", function () {
                let data = {
                    DEItemFieldDataID: $(this).data('itemid'),
                    DEItemRowDataID: $(this).data('stubid'),
                    Value: $(this).val()
                };
                updateRow(data, this);
            });

            $getE('#stubs-table-container').on('click', '.btn-add-related-item', function () {
                addRelatedItem();
            });

            $getE('#stubs-table-container').on('click', '.btn-remove', function () {
                let rowdata = _stubsTable.row($(this).parents('tr').first()).data();
                _confirmationDialog.show('Confirm Delete', 'Are you sure you want to delete this related item?',
                    function () {
                        deleteRow(rowdata);
                    },
                    function () {
                        // Do nothing.
                    }, 'Delete', 'Cancel');

                return false;
            });

            reload(model.GlobalBatchId, model.TransactionId);
        };


        let calculateTotals = function () {
            let defaultamount = 0;
            let paymentamount = 0;
            let stubamount = 0;
            $.each(_currenttransaction.Payments, function (i, e) {
                paymentamount += isNaN(e.Amount) ? 0 : e.Amount;
            });
            paymentamount = paymentamount;
            $.each(_currenttransaction.Stubs, function (i, e) {
                stubamount += isNaN(e.Amount) ? 0 : e.Amount;
            });
            stubamount = stubamount;
            defaultamount = paymentamount - stubamount;

            return {
                PaymentsTotal: paymentamount,
                StubsTotal: stubamount,
                Difference: defaultamount
            };
        };

        let updateBalancing = function () {
            let totals = calculateTotals();
            let payments = _formatting.formatCurrency(totals.PaymentsTotal);
            let stubs = _formatting.formatCurrency(totals.StubsTotal);
            let diff = _formatting.formatCurrency(totals.Difference);
            self.PaymentTotal(payments);
            self.StubTotal(stubs);
            self.Difference(diff);

            $getE('.balancing-box')
                .removeClass('text-danger')
                .removeClass('text-success')
                .addClass(totals.Difference === 0 ? 'text-success' : 'text-danger');
        };

        let addRelatedItem = function () {
            let de = [];
            let totals = calculateTotals();

            let newrow = {
                DataEntry: [],
                DEItemRowDataID: generateId(),
                IsNewRow: true,
                Account: 0,
                Amount: totals.Difference.toFixed(2),
                StubsAmount: totals.Difference.toFixed(2)
            };

            // Add some default DE fields.
            let stubsdelist = _data.ParentBatch.DESetupList.filter(function (e) {
                return e.TableName.toLowerCase().indexOf("stubs") >= 0;
            });
            $.each(stubsdelist, function (i, e) {
                // Set the stub property for the grid.
                let key = e.TableName + e.FieldName;
                newrow[key] = newrow[key] ? newrow[key] : '';
                // Add the DE column for the data.
                newrow.DataEntry.push({
                    DESetupFieldID: e.DESetupFieldID,
                    TableName: e.TableName,
                    FieldName: e.FieldName,
                    DataType: e.DataType,
                    Value: newrow[key],
                    DEItemFieldDataID: generateId(),
                    DEItemRowDataID: newrow.DEItemRowDataID,
                    FieldTitle: _stubsTable.column(i + 1).header().textContent,
                    Title: _stubsTable.column(i + 1).header().textContent
                });
            });

            _currenttransaction.Stubs.push(newrow);

            _stubsTable.clear();
            _stubsTable.rows.add(_currenttransaction.Stubs);
            _stubsTable.columns.adjust().draw();
            _stubsTable.page('last').draw('page');

            updateBalancing();
        };

        let generateId = function () {
            // This a function to add a primary key to the data we're adding in this web page.
            // It won't get pushed up to the server.
            return _generatedID++;
        };

        let approveTransaction = function (updateTransaction) {
            //sets reject button dimmer and gets rid of icon
            let oldStatus = _currenttransaction.TransactionStatus;
            _currenttransaction.TransactionStatus = 3;
            if (updateTransaction) {
                saveChanges(showapproved);
                _currenttransaction.TransactionStatus = oldStatus;
            }
            else {
                showapproved();
            }
        };

        let showapproved = function () {
            let reject = $getE('#button-reject');
            reject.addClass('gray-out');
            $getE('.reject').removeClass('fa fa-thumbs-down');
            reject.html(reject.html().replace(/\bRejected\b/, "Reject"));
            $getE('.approve').addClass('fa fa-thumbs-up');
            $getE('#button-approve').removeClass('gray-out');
            $getE('#button-approve').html($getE('#button-approve').html().replace(/\bAccept\b/, "Accepted"));
            _currenttransaction.PreviousTransactionStatus = _currenttransaction.TransactionStatus;
            _currenttransaction.TransactionStatus = 3;
        };

        let rejectTransaction = function (updateTransaction) {
            let oldStatus = _currenttransaction.TransactionStatus;
            _currenttransaction.TransactionStatus = 4;
            if (updateTransaction) {
                saveChanges(showrejected);
                _currenttransaction.TransactionStatus = oldStatus;
            }
            else {
                showrejected();
            }
        };

        let showrejected = function () {
            let approved = $getE('#button-approve');
            approved.addClass('gray-out');
            $getE('.approve').removeClass('fa fa-thumbs-up');
            approved.html(approved.html().replace(/\bAccepted\b/, "Accept"));
            $getE('.reject').addClass('fa fa-thumbs-down');
            $getE('#button-reject').removeClass('gray-out');
            $getE('#button-reject').html($getE('#button-reject').html().replace(/\bReject\b/, "Rejected"));
            _currenttransaction.PreviousTransactionStatus = _currenttransaction.TransactionStatus;
            _currenttransaction.TransactionStatus = 4;
        };

        let resetApproveReject = function () {

            let reject = $getE('#button-reject');
            $getE('.reject').removeClass('fa fa-thumbs-down');
            reject.html(reject.html().replace(/\bRejected\b/, "Reject"));
            $getE('#button-approve').removeClass('gray-out');
            let approved = $getE('#button-approve');
            $getE('.approve').removeClass('fa fa-thumbs-up');
            approved.html(approved.html().replace(/\bAccepted\b/, "Accept"));
            $getE('#button-reject').removeClass('gray-out');
        };

        let applyFirstTransaction = function () {
            let transactionid = _data.ParentBatch.TransactionIds[0];
            _model.TransactionId = transactionid;
            reload(_model.GlobalBatchId, transactionid);
        };

        let applyPreviousTransaction = function () {
            let currentitem = _model.TransactionId;
            let currentindex = _data.ParentBatch.TransactionIds.indexOf(currentitem);
            let transactionid = _data.ParentBatch.TransactionIds[currentindex - 1];
            _model.TransactionId = transactionid;
            reload(_model.GlobalBatchId, transactionid);
        };

        let applyNextTransaction = function () {
            let currentitem = _model.TransactionId;
            let currentindex = _data.ParentBatch.TransactionIds.indexOf(currentitem);
            let transactionid = _data.ParentBatch.TransactionIds[currentindex + 1];
            _model.TransactionId = transactionid;
            reload(_model.GlobalBatchId, transactionid);
        };

        let applyLastTransaction = function () {
            let transactionid = _data.ParentBatch.TransactionIds[_data.ParentBatch.TransactionIds.length - 1];
            _model.TransactionId = transactionid;
            reload(_model.GlobalBatchId, transactionid);
        };

        let updateStubsErrors = function (message, fieldTitle, row) {
            var thisStub = _currenttransaction.Stubs[row];
            var result = thisStub.DataEntry.filter(function (o) { return o.Title === fieldTitle; });
            if (result.length > 0 && (!result[0].DecisioningReasonDesc || result[0].DecisioningReasonDesc.length === 0)) {
                result[0].Invalid = true;
                result[0].DecisioningReasonDesc = message;
            }
        };

        let populateErrors = function (validations) {
            let errors = [];

            validations.Errors.forEach(function (e) {
                var row = ' ( Related Items - Row ' + (e.RowIndex + 1) + ' )';
                var message = '';
                var fieldName = (e.FieldTitle && e.FieldTitle.length > 0) ? e.FieldTitle : e.FieldName;

                if (e.Type === 0) {
                    message = 'No setup field found';
                    errors.push({
                        html: 'No setup field found <strong>' + fieldName + '</strong> for data entry row ' + e.RowIndex + '.',
                        rowindex: e.RowIndex
                    });
                }
                else if (e.Type === 1) {
                    message = 'Required field';
                    e.Invalid = true;
                    errors.push({
                        html: '<strong>' + fieldName + '</strong> is a required field.' + row,
                        rowindex: e.RowIndex
                    });
                }
                else if (e.Type === 2) {
                    message = 'Field can\'t be longer than ' + e.MaxLength + ' characters';
                    errors.push({
                        html: '<strong>' + fieldName + '</strong> field can\'t be longer than ' + e.MaxLength + ' characters.' + row,
                        rowindex: e.RowIndex
                    });
                }
                else if (e.Type === 3) {
                    message = 'Invalid float value';
                    errors.push({
                        html: '<strong>' + e.Value + '</strong> is an invalid float value for <strong>' + fieldName + '</strong> field.' + row,
                        rowindex: e.RowIndex
                    });
                }
                else if (e.Type === 4) {
                    message = 'Invalid currency value';
                    errors.push({
                        html: '<strong>' + e.Value + '</strong> is an invalid currency value for <strong>' + fieldName + '</strong> field.' + row,
                        rowindex: e.RowIndex
                    });
                }
                else if (e.Type === 5) {
                    message = 'Invalid date value';
                    errors.push({
                        html: '<strong>' + e.Value + '</strong> is an invalid date value for <strong>' + fieldName + '</strong> field.' + row,
                        rowindex: e.RowIndex
                    });
                }
                else if (e.Type === 6) {
                    message = 'Mask validation failed';
                    errors.push({
                        html: '<strong>' + e.Value + '</strong> is an invalid value for <strong>' + fieldName + '</strong> because mask validation failed.' + row,
                        rowindex: e.RowIndex
                    });
                }
                else if (e.Type === 7) {
                    message = 'Cannot be greater than $900,000,000,000,000.00';
                    errors.push({
                        html: '<strong>' + fieldName + '</strong> field cannot be greater than $900,000,000,000,000.00.' + row,
                        rowindex: e.RowIndex
                    });
                }
                else if (e.Type === 8) {
                    message = 'Cannot be less than $-900,000,000,000,000.00';
                    errors.push({
                        html: '<strong>' + fieldName + '</strong> field cannot be less than $-900,000,000,000,000.00.' + row,
                        rowindex: e.RowIndex
                    });
                }

                // Update Field Error Message
                if (message.length > 0) {
                    updateStubsErrors(message, e.FieldTitle, e.RowIndex);
                }
            });

            return errors;
        };

        let saveChanges = function (refresh, callback) {
            framework.showSpinner('Loading...');

            $.each(_currenttransaction.Stubs, function (i, e) {
                $.each(e.DataEntry, function (ii, d) {
                    if (!d.FieldDecisionStatus) {
                        d.Invalid = false;
                        d.DecisioningReasonDesc = "";
                    }
                });
            });

            self.Errors([]);
            framework.doJSON('/RecHubViews/Decisioning/UpdateTransaction', _currenttransaction, function (result) {
                if (result && result.message && result.message !== "") {
                    framework.hideSpinner();
                    _confirmationDialog.show('Out of Balance Condition', result.message, function () { }, function () { }, "OK");
                    self.Errors([{
                        html: result.message
                    }]);
                }
                else if (result && result.errors && result.errors.length > 0) {
                    framework.errorToast(_container, result.errors[0], {
                        sticky: false,
                        stayTime: 8000
                    });
                    framework.hideSpinner();
                }
                else if (result && result.success == false && result.validations && result.validations.Errors.length > 0) {
                    $getE('#error-display').show();

                    // Validation errors
                    let errors = populateErrors(result.validations);
                    self.Errors(errors);
                    refreshStubGrid();
                    framework.hideSpinner();
                    framework.errorToast(_container, 'Unable to save the transaction. See validation errors on the top of the page for more detail.', {
                        sticky: false,
                        stayTime: 5000
                    });
                }
                else if (!result || !result.success) {
                    framework.errorToast(_container, 'Error updating transaction.');
                    framework.hideSpinner();
                }
                else if (refresh !== false) {
                    reload(_model.GlobalBatchId, _currenttransaction.TransactionId);
                }

                if (callback)
                    callback(self.Errors().length === 0);
            });
        };

        let reload = function (batchid, transactionid) {
            let data = { GlobalBatchId: batchid, TransactionId: transactionid };
            framework.doJSON('/RechubViews/Decisioning/GetTransactionDetail', data, function (result) {
                if (!result || !result.success) {
                    framework.errorToast(_container, 'Error Loading Transaction details.');
                    return;
                }
                let transaction = result.data;
                if (transaction.ParentBatch.MinutesUntilDeadLine && transaction.ParentBatch.MinutesUntilDeadLine < 15) {
                    let mins = transaction.ParentBatch.MinutesUntilDeadLine >= 0 ? transaction.ParentBatch.MinutesUntilDeadLine : 0;
                    mins = Math.floor(mins);
                    let message = transaction.ParentBatch.MinutesUntilDeadLine >= 0
                        ? 'Exception Deadline is approaching. You have ' + mins + ' minutes until this batch is no longer available for processing.'
                        : "Exception Deadline has passed. You can no longer process this batch.";
                    if (transaction.ParentBatch.MinutesUntilDeadLine >= 0)
                        framework.warningToast(_container, message, {
                            sticky: false,
                            stayTime: 8000
                        });
                    else
                        framework.errorToast(_container, message, {
                            sticky: false,
                            stayTime: 8000
                        });
                }

                renderPage(result.data);
            });
            framework.showSpinner('Loading...');
        };

        let updateNavigation = function (data) {
            _transactions = data.ParentBatch.TransactionIds;
            let currentindex = _transactions.indexOf(_model.TransactionId);
            let total = _transactions.length;
            self.EnableFirstTransaction(total > 1 && currentindex > 0);
            self.EnablePreviousTransaction(total > 1 && currentindex > 0);
            self.EnableNextTransaction(total > 1 && currentindex < total - 1);
            self.EnableLastTransaction(total > 1 && currentindex < total - 1);
            self.CurrentTransactionText(currentindex + 1 + "/" + total);
        };

        let renderPage = function (data) {
            framework.hideSpinner();
            $getE('#error-display').hide();

            _data = data;
            let batch = _data.ParentBatch;
            self.DepositDate(batch.DepositDate);
            self.ProcessDate(batch.ProcessDate);
            self.Entity(batch.Entity);
            self.Workgroup(batch.WorkgroupName);
            self.PaymentSource(batch.PaymentSource);
            self.BatchId(batch.BatchId);
            self.DeadLine(_formatting.toLocalTimeZone(batch.DeadLine, true));
            self.TransactionStatus(batch.DepositStatusDisplayName);
            self.LockedBy(batch.User);
            self.UserCanEdit(batch.UserCanEdit);
            self.UserDisabledMessage(self.UserCanEdit() ? labels["saveEditsAllowed"] : labels["noEditsAllowed"]);
            self.UserDisabledAcceptMessage(self.UserCanEdit() ? labels["acceptsEditsAllowed"] : labels["noEditsAllowed"]);
            self.UserDisabledRejectMessage(self.UserCanEdit() ? labels["rejectEditsAllowed"] : labels["noEditsAllowed"]);
            updateNavigation(data);

            _currenttransaction = data;

            //sets the thumbs up/down buttons
            if (_currenttransaction.TransactionStatus === 3)
                approveTransaction(false);
            else if (_currenttransaction.TransactionStatus === 4)
                rejectTransaction(false);
            else
                resetApproveReject();
            self.TransactionSequence(_currenttransaction.TransactionSequence);

            // Payment Table.
            let $paymentcolumns = $("<tr></tr>");
            let paymentcolumns = [];

            $paymentcolumns.append("<th>Exception Reason</th>");
            paymentcolumns.push({ data: 'DecisioningReasonDesc' });
            $paymentcolumns.append("<th>Payment Amount</th>");
            paymentcolumns.push({ data: 'Amount', class: "alignRight", name: "Amount" });
            $paymentcolumns.append("<th>Payee</th>");
            paymentcolumns.push({ data: 'PayeeName' });
            $paymentcolumns.append("<th>RT</th>");
            paymentcolumns.push({ data: 'RT' });
            $paymentcolumns.append("<th>Account Number</th>");
            paymentcolumns.push({ data: 'Account' });
            $paymentcolumns.append("<th>Check/Trace/Ref Number</th>");
            paymentcolumns.push({ data: 'Serial' });
            $paymentcolumns.append("<th>Payer</th>");
            paymentcolumns.push({ data: 'RemitterName' });

            if (_currenttransaction.Payments.length > 0 && _currenttransaction.Payments[0].DataEntry.length > 0) {
                $.each(_currenttransaction.Payments[0].DataEntry, function (i, e) {
                    // Add the data to the columns.
                    $paymentcolumns.append("<th>" + e.FieldName + "</th>");
                    let key = e.TableName + e.FieldName;
                    let col = { data: key };
                    if (e.DataType == "7" || e.DataType == "6")
                        col.class = "alignRight";
                    paymentcolumns.push(col);
                    // Add the data to the payment records.
                    $.each(_currenttransaction.Payments, function (j, p) {
                        let de = $.grep(p.DataEntry, function (decol) {
                            return decol.FieldName == e.FieldName && decol.TableName == e.TableName;
                        })[0];
                        let value = de.Value;
                        if (de.DataType == "7" && de.Value && de.Value.length > 0)
                            value = _formatting.formatCurrency(de.Value);
                        p[key] = value;
                    });
                });
            }

            if (_paymentsTable) {
                _paymentsTable.clear();
                _paymentsTable.destroy();
            }
            let $paymentdiv = $getE('#payments-table-container');
            $paymentdiv.empty();
            $paymentdiv.append($("<table>")
                .attr("id", "payments-table")
                .attr("class", "table dataTable table-striped table-hover")
                .append("<thead></thead>"));

            $getE('#payments-table thead').append($paymentcolumns);
            $.each(paymentcolumns, function (i, e) {
                e.width = e.width ? e.width : _utilities.measureTextWidth($getE('#payments-table thead th').eq(i).text());
            });

            _paymentsTable = $getE('#payments-table').DataTable({
                autoWidth: false,
                columns: paymentcolumns,
                data: _currenttransaction.Payments,
                drawCallback: function (settings) {
                    let enablefeatures = _currenttransaction.Payments && _currenttransaction.Payments.length > FEATURE_COUNT_THRESHOLD;
                    if (enablefeatures) {
                        $getE('#payments-table_length').parent().parent().show();
                        $getE('#payments-table_info').parent().parent().show();
                    }
                    else {
                        $getE('#payments-table_length').parent().parent().hide();
                        $getE('#payments-table_info').parent().parent().hide();
                    }
                },
                createdRow: function (row, data, index) {
                    let api = this.api();
                    let colindex = api.column('Amount:name')[0][0];
                    let td = api.cells(row, colindex).nodes()[0];
                    $(td)
                        .empty()
                        .append(_formatting.formatCurrency(data.Amount));
                },
                scrollX: true
            });

            // Stubs table
            let $stubcolumns = $("<tr></tr>");
            let stubcolumns = [];

            $stubcolumns.append("<th></th>");
            stubcolumns.push({ data: "Account", name: "icon", sortable: false, width: "12px" });

            let stubsdelist = _data.ParentBatch.DESetupList.filter(function (e) {
                return e.TableName.toLowerCase().indexOf("stubs") >= 0;
            });
            $.each(stubsdelist, function (i, e) {
                // Add the data to the columns.
                $stubcolumns.append("<th>" + e.Title + "</th>");
                let key = e.TableName + e.FieldName;
                let col = { data: key };
                if (e.DataType == "7" || e.DataType == "6")
                    col.class = "alignRight";
                stubcolumns.push(col);
                // Add the data to the stub records.
                $.each(_currenttransaction.Stubs, function (j, p) {
                    let de = $.grep(p.DataEntry, function (decol) {
                        return decol.FieldName == e.FieldName && decol.TableName == e.TableName;
                    })[0];
                    let value = de ? de.Value : '';
                    if (value && value.length > 0 && de.DataType == "7")
                        value = _formatting.formatCurrency(value);
                    p[key] = value;
                });
            });

            $stubcolumns.append("<th></th>");
            stubcolumns.push({ data: "Account", name: "options", sortable: false, class: "alignRight" });

            if (_stubsTable) {
                _stubsTable.clear();
                _stubsTable.destroy();
            }
            let $stubdiv = $getE('#stubs-table-container');
            $stubdiv.empty();
            let tooltip = self.UserCanEdit() ? "Add a related item to the current transaction" : self.UserDisabledMessage();
            $stubdiv.append($("<table>")
                .attr("id", "stubs-table")
                .attr("class", "table dataTable table-hover table-striped")
                .append("<thead></thead>")
                .append("<tfoot><tr><td colspan='" + stubcolumns.length + "'>"
                    + "<button" + (self.UserCanEdit() ? "" : " disabled") + " title='" + tooltip + "' class='btn-add-related-item btn btn-secondary'><span class='fa fa-plus fa-lg text-success'></span> Add related item</button></td></tr></tfoot>"));

            $getE('#stubs-table thead').append($stubcolumns);
            $.each(stubcolumns, function (i, e) {
                e.width = e.width ? e.width : _utilities.measureTextWidth($getE('#stubs-table thead th').eq(i).text());
            });

            let order = [1, 'asc'];
            if (stubcolumns.length == 1) {
                order = [0, 'asc'];
            }

            _stubsTable = $getE('#stubs-table').DataTable({
                order: order,
                autoWidth: false,
                columns: stubcolumns,
                data: _currenttransaction.Stubs,
                ordering: false,
                drawCallback: function (settings) {
                    let enablefeatures = _currenttransaction.Stubs && _currenttransaction.Stubs.length > FEATURE_COUNT_THRESHOLD;
                    if (enablefeatures) {
                        $getE('#stubs-table_length').parent().parent().show();
                        $getE('#stubs-table_info').parent().parent().show();
                    }
                    else {
                        $getE('#stubs-table_length').parent().parent().hide();
                        $getE('#stubs-table_info').parent().parent().hide();
                    }
                },
                createdRow: function (row, data, index) {
                    let api = this.api();

                    // Edit the html for the options column.
                    let colindex = api.column('options:name')[0][0];
                    let td = api.cells(row, colindex).nodes()[0];
                    let tooltip = self.UserCanEdit() ? "Delete this related item from the current transaction" : self.UserDisabledMessage();
                    $(td).empty()
                        .append('<button class="btn btn-secondary btn-remove"' + (self.UserCanEdit() ? "" : " disabled") + ' title="' + tooltip + '"><i href="#" class="fa fa-times fa-2x text-danger"></i></button>');

                    // Loop through the columns and show an edit-field.
                    //we start at 1 to skip the icon col
                    let anyExcep = false;
                    for (let i = 1; i < stubcolumns.length - 1; i++) {
                        let defield = $.grep(data.DataEntry, function (e) {
                            return e.TableName + e.FieldName == stubcolumns[i].data;
                        })[0];

                        if (!defield) {
                            // If we didnt find the field in the row's DE list.
                            let col = $.grep(stubsdelist, function (e) {
                                return e.TableName + e.FieldName == stubcolumns[i].data;
                            })[0];
                            defield = $.extend({}, col);
                            defield.Value = '';
                            defield.FieldDecisionStatus = 0;
                            defield.DEItemFieldDataID = generateId();
                            data.DataEntry.push(defield);
                        }

                        // Applying the styles and html for row editing.
                        let coltd = api.cells(row, i).nodes()[0];
                        let val = defield.Value;
                        if (!val)
                            val = "";
                        // Escape the value to not inject HTML incorrectly.
                        val = val.replace(new RegExp("'", 'g'), "&apos;")
                            .replace(new RegExp("\"", 'g'), "&quot;");

                        let disabled = self.UserCanEdit() ? "" : "disabled";
                        let title = self.UserCanEdit() ? "" : self.UserDisabledMessage();
                        let hasexception = !data.IsNewRow && defield.FieldDecisionStatus !== 0;
                        anyExcep = anyExcep | hasexception;
                        $(coltd).empty()
                            .append("<input type='text' title='" + title + "' " + disabled + " class='form-control stub-editor" + (hasexception ? " has-exception" : "") + "' value='" + val + "' data-itemid='" + defield.DEItemFieldDataID + "' data-stubid='" + data.DEItemRowDataID + "' />")
                            .append("<p class='text-danger'>" + (defield.DecisioningReasonDesc ? defield.DecisioningReasonDesc : '') + "</p>");
                        // Trigger a change to update the data.
                        updateRow({ DEItemFieldDataID: defield.DEItemFieldDataID, DEItemRowDataID: defield.DEItemRowDataID, Value: val }, $(coltd).children('.stub-editor')[0]);
                    }

                    let res = $.grep(self.Errors(), function (e) {
                        return e.rowindex == index;
                    });

                    anyExcep = anyExcep | res.length > 0;
                    // Edit the html for the icon for exception-ed stubs
                    let iconColIndex = api.column('icon:name')[0][0];
                    icontd = api.cells(row, iconColIndex).nodes()[0];
                    $(icontd).empty();
                    if (anyExcep) {
                        $(icontd).append('<i class="fa fa-exclamation-triangle excep-icon-row" aria-hidden="true"></i>');
                    }
                },
                scrollX: true
            });

            updateBalancing();
        };

        let refreshStubGrid = function () {

            // Redraw the grid.
            _stubsTable.clear();
            _stubsTable.data().rows.add(_currenttransaction.Stubs);
            _stubsTable.draw(false);

            // set the error border on validation errors
            var errorControls = document.querySelectorAll("p.text-danger");
            $.each(errorControls, function (i, e) {
                if ($(e).text().length > 0) {
                    var txtbox = $(e).siblings('input');
                    if (!txtbox.hasClass("has-exception")) {
                        txtbox.addClass("error-border");
                    }
                }
            });

        };

        let updateRow = function (data, input) {
            let stubsearch = $.grep(_currenttransaction.Stubs, function (e) {
                return e.DEItemRowDataID == data.DEItemRowDataID;
            });
            if (stubsearch.length === 0) {
                return;
            }
            let stub = stubsearch[0];
            let desearch = $.grep(stub.DataEntry, function (e) {
                return e.DEItemFieldDataID == data.DEItemFieldDataID;
            });
            if (desearch.length === 0) {
                return;
            }
            let de = desearch[0];
            // Unescape the data.
            de.Value = data.Value
                .replace(new RegExp("&apos;", 'g'), "'")
                .replace(new RegExp("&quot;", 'g'), "\"");
            // Specialcase - if it's StubsAmount, update the amount too.
            if (de.TableName + de.FieldName == 'StubsAmount') {
                stub.Amount = parseFloat(de.Value);

                // Update balancing in the UI.
                updateBalancing();

                // If it happens to be negative, we'll need to show it in red.
                if (stub.Amount < 0) {
                    $(input).addClass('negative-amount');
                }
                else {
                    $(input).removeClass('negative-amount');
                }
            }
            // Finally, update the grid's data for the next redraw.
            let value = de ? de.Value : '';
            let key = de.TableName + de.FieldName;
            if (value && value.length > 0 && de.DataType == "7")
                value = _formatting.formatCurrency(value);
            stub[key] = value;
        };

        let deleteRow = function (data) {
            let itemindex = -1;
            $.each(_currenttransaction.Stubs, function (i, e) {
                if (e.DEItemRowDataID == data.DEItemRowDataID)
                    itemindex = i;
            });
            if (itemindex === -1) {
                return;
            }
            _currenttransaction.Stubs.splice(itemindex, 1);
            let page = _stubsTable.page.info()["page"];
            if (_stubsTable.page.info()["end"] == _stubsTable.page.info()["start"] + 1)
                page--;
            _stubsTable.clear();
            _stubsTable.rows.add(_currenttransaction.Stubs);
            _stubsTable.columns.adjust().draw();
            _stubsTable.page(page).draw('page');

            updateBalancing();
        };

        let updateData = function (data) {
            if (!data) {
                return;
            }

            let stubsearch = $.grep(_currenttransaction.Stubs, function (e) {
                return e.DEItemRowDataID == data.DEItemRowDataID;
            });

            if (stubsearch.length === 0) {
                return;
            }
            let stub = stubsearch[0];
            stub.DataEntry = data.DataEntry;

            // Update the stub grid.
            $.each(stub.DataEntry, function (i, e) {
                let key = e.TableName + e.FieldName;
                let value = e.Value;
                if (e.DataType == "7" && e.Value && e.Value.length > 0)
                    value = _formatting.formatCurrency(e.Value);
                stub[key] = value;
            });

            _stubsTable.clear();
            _stubsTable.rows.add(_currenttransaction.Stubs);
            _stubsTable.columns.adjust().draw();
        };

        //public properties
        this.DepositDate = ko.observable('');
        this.ProcessDate = ko.observable('');
        this.Entity = ko.observable('');
        this.Workgroup = ko.observable('');
        this.PaymentSource = ko.observable('');
        this.BatchId = ko.observable(0);
        this.DeadLine = ko.observable('');
        this.TransactionStatus = ko.observable('');
        this.LockedBy = ko.observable('');
        this.EnableFirstTransaction = ko.observable(false);
        this.EnablePreviousTransaction = ko.observable(false);
        this.EnableNextTransaction = ko.observable(false);
        this.EnableLastTransaction = ko.observable(false);
        this.CurrentTransactionText = ko.observable('');
        this.TransactionSequence = ko.observable(0);
        this.UserCanEdit = ko.observable(false);
        this.UserDisabledMessage = ko.observable('');
        this.PaymentTotal = ko.observable('');
        this.StubTotal = ko.observable('');
        this.Difference = ko.observable('');
        this.Errors = ko.observable([]);
        this.UserDisabledAcceptMessage = ko.observable('');
        this.UserDisabledRejectMessage = ko.observable('');

        //ctor ctor ctor
        function decisioningTransactionDetailViewModel() { };


        decisioningTransactionDetailViewModel.prototype = {
            constructor: decisioningTransactionDetailViewModel(),
            init: function (container, model) {
                _container = container;
                $getE = function (a) {
                    return _container.find(a);
                };
                applyUI(model);
                ko.applyBindings(self, container.get(0));
            },
            dispose: function () {
                _data = null;
                _currenttransaction = null;
                $('#stubs-table-container').off('change');
                $('#stubs-table-container').off('click');

                self.Errors([]);

                if (_stubsTable) {
                    _stubsTable.destroy();
                }
                if (_paymentsTable) {
                    _paymentsTable.destroy();
                }

            }
        };
        return decisioningTransactionDetailViewModel;
    });