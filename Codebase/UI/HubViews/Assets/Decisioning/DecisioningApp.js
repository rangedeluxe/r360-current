﻿function initDecisioning(model, controllername, viewmodelname, container) {
    require.config({
        paths: {
            'decisioningViewModel': '/RecHubViews/Assets/Decisioning/ViewModels/DecisioningViewModel',
            'decisioningDetailViewModel': '/RecHubViews/Assets/Decisioning/ViewModels/DecisioningDetailViewModel.js?v=20170126',
            'decisioningTransactionDetailViewModel': '/RecHubViews/Assets/Decisioning/ViewModels/DecisioningTransactionDetailViewModel',
            'decisioningRowEditorViewModel': '/RecHubViews/Assets/Decisioning/ViewModels/DecisioningRowEditorViewModel',
        }
    });

    require([controllername, viewmodelname], function (controller, viewmodel) {
        controller.init(model, viewmodel, container);
    });
}