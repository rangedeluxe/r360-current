﻿function initBatchSummary(model) {
    // File contains initialize code.
    require.config({
        paths: {
            // Dependencies for Batch summary.
            // Notice we're using absolute paths so we don't screw up the baseURL for framework.
            'batchSummaryController': '/RecHubViews/Assets/BatchSummary/Controllers/BatchSummaryController',
            'batchSummaryService': '/RecHubViews/Assets/BatchSummary/Services/BatchSummaryService',
            'batchSearchViewModel': '/RecHubViews/Assets/BatchSummary/ViewModels/batchSearchViewModel'
        }
    });

    require(['batchSummaryController'], function (controller) {
        controller.init(model);
    });
}