﻿// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.

define(['jquery', 'frameworkUtils', 'ko', 'batchSummaryService', '/RecHubViews/Assets/Shared/Breadcrumbs.js',
    '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap',
    'bootstrap', 'wfs.select', 'wfs.treeselector', 'moment', 'select2'],
function ($, framework, ko, batchservice, breadcrumbs) {

    var _container = $('#batchSummaryPortlet');
    var $getE;
    var _workgroupSelector;
    var _workgroupLongName;
    var _batchtable;
    var _suppressload = true;
    var _workgrouploaded = false;
    var _paymentsloaded = false;
    var _sourcesloaded = false;
    var _breadcrumbs = new breadcrumbs();
    var _treeInitialized = false;
    var _totalRecords = 0;
    var _model;
    // private
    var self = this;

    // Sets up all of the UI elements and handles callbacks.
    var applyUI = function (model) {
        _model = model;

        // Standard Breadcrumb logic.
        _breadcrumbs.pop("Batch Summary");
        var bcs = _breadcrumbs.render();
        $getE('.breadcrumbheader').html(bcs);
        $getE('.breadcrumbheader a').click(function () {
            _breadcrumbs.load(_container.parent(), $(this).data('name'));
        });

        // If we don't have any input data, but we do find it from the 
        // local storage (from dashboard), we use that.
        let query = window.localStorage.getItem('batch_summary_query');
        if (!model.DateFrom && query) {
            let obj = JSON.parse(query);
            model.DateFrom = obj.dateFrom;
            model.DateTo = obj.dateTo;
            model.PaymentType = obj.paymentType;
            model.PaymentSource = obj.paymentSource;
            model.Workgroup = obj.workgroup;
            window.localStorage.removeItem('batch_summary_query');
        }

        self.DateFrom(model.DateFrom);
        self.DateTo(model.DateTo);
        self.Workgroup(model.Workgroup);
        self.PaymentType(model.PaymentType);
        self.PaymentSource(model.PaymentSource);
        self.ShowBatchId(model.showBatchId);

        if (self.Workgroup() != null && self.Workgroup() != undefined && self.Workgroup().length > 0)
            _suppressload = false;

        $getE('#searchButton').click(validateAndSearch);

        $getE('#dateFrom').wfsDatePicker({ callback: function () { self.DateFrom($getE('#dateFrom').wfsDatePicker('getDate')); } });
        $getE('#dateFrom').wfsDatePicker('setDate', self.DateFrom());

        $getE('#dateTo').wfsDatePicker({ callback: function () { self.DateTo($getE('#dateTo').wfsDatePicker('getDate')); } });
        $getE('#dateTo').wfsDatePicker('setDate', self.DateTo());

        _workgroupSelector = $getE('#batch-summary-treebase').wfsTreeSelector({
            useExpander: true,
            entityURL: '/RecHubRaamProxy/api/entity',
            callback: selectedTreeItem,
            dataCallback: treeDataCallback,
            expanderTitle: "Select Workgroup",
            entitiesOnly: false,
            closeOnSelectNonNode: false
        });

        // Need to fetch our payment types first.
        batchservice.getPaymentTypes(function (data) {
            if (!data || !data.success) {
                framework.errorToast(_container, 'Error Loading Payment Types.');
                return;
            }
            _paymentsloaded = true;
            self.PaymentTypes(data.data);
            $getE('#paymentType').wfsSelectbox({
                items: self.PaymentTypes(),
                callback: function () { self.PaymentType($getE('#paymentType').wfsSelectbox('getValue')); },
                placeHolder: '-- All --',
                displayField: 'LongName',
                idField: 'PaymentTypeKey'
            });
            $getE('#paymentType').wfsSelectbox('setValue', self.PaymentType());
            performInitialSearch();
        });

        // And also our payment sources.
        batchservice.getPaymentSources(function (data) {
            if (!data || !data.success) {
                framework.errorToast(_container, 'Error Loading Payment Sources.');
                return;
            }
            _sourcesloaded = true;
            self.PaymentSources(data.data);
            $getE('#paymentSource').wfsSelectbox({
                items: self.PaymentSources(),
                callback: function () { self.PaymentSource($getE('#paymentSource').wfsSelectbox('getValue')); },
                placeHolder: '-- All --',
                displayField: 'LongName',
                idField: 'PaymentSourceKey'
            });
            $getE('#paymentSource').wfsSelectbox('setValue', self.PaymentSource());
            performInitialSearch();
        });

        // Printer friendly version 
        $getE('#printerlink').click(function () {          
            var order = _batchtable.order();
            var colIndex = 0;
            var colOrdered = "";
            var direction = "";
            if (order.length > 0) {
                colIndex = order[0][0];
                colOrdered = _batchtable.settings().init().columns[colIndex].data;
                direction = order[0][1];
            }


            var url = "/RechubViews/BatchSummary/Print?DateFrom={datefrom}&DateTo={dateto}&PaymentType={paymenttype}&Workgroup={workgroup}&" +
                       "OrderBy={orderby}&OrderDirection={orderdirection}&Length={length}&WorkgroupLongName={workgrouplongname}" +
                "&PaymentSource={paymentsource}";
            url = url.replace("{datefrom}", self.DateFrom());
            url = url.replace("{dateto}", self.DateTo());
            url = url.replace("{paymenttype}", self.PaymentType());
            url = url.replace("{workgroup}", self.Workgroup());
            url = url.replace("{orderby}", colOrdered);
            url = url.replace("{orderdirection}", direction);
            url = url.replace("{length}", _totalRecords);
            if (_treeInitialized) {
                _workgroupLongName = _workgroupSelector.wfsTreeSelector('getSelection').label;
            }
            url = url.replace("{workgrouplongname}", _workgroupLongName);
            url = url.replace("{paymentsource}", self.PaymentSource());
            url = encodeURI(url);
            window.open(url);
        });
    };

    var initTable = function () {
        // To be called AFTER all loading is completed.
        var gridoptions = {
            processing: false,
            serverSide: true,
            order: (_model.order) ? [[_model.order[0].column, _model.order[0].dir]] : [],
            displayStart: (_model.length && _model.length > 0) ? _model.start : 0,
            pageLength: (_model.length && _model.length > 0) ? _model.length : 10,
            search: {
                search: (_model.search) ? _model.search : ''
            },
            ajax: {
                url: '/RecHubViews/BatchSummary/GetBatches',
                type: 'POST',
                data: function (d) {
                    updateModel();
                    d.DateFrom = self.DateFrom();
                    d.DateTo = self.DateTo();
                    d.Workgroup = self.Workgroup();
                    d.PaymentType = self.PaymentType();
                    d.PaymentSource = self.PaymentSource();
                    d.search = $getE('#batchResultsTable_filter input').val();
                }
            },
            columns: [
                { "data": "SourceBatchID", "visible": false, class: "alignRight", name: "BatchID" },
                { "data": "BatchNumber", class: "alignRight" },
                { "data": "DepositDateString", "type": "date" },
                { "data": "PaymentSource" },
                { "data": "PaymentType" },
                { "data": "BatchSiteCode", name: "BatchSiteCode", "visible" : false, "class" : "alignRight" },
                { "data": "TransactionCount", class: "alignRight" },
                { "data": "PaymentCount", class: "alignRight" },
                { "data": "DocumentCount", class: "alignRight" },
                { "data": "TotalAmount", class: "alignRight", name: "Total" }
            ],
            headerCallback: function (row, data, start, end, display) {
                var api = this.api();

                // Show or Hide BatchId 
                if (data.length > 0) {
                    api.column('BatchID:name').visible(data[0].ShowBatchId);
                    api.column('BatchSiteCode:name').visible(data[0].ShowBathSiteCode);
                }
            },
            footerCallback: function (row, data, start, end, display) {
                var api = this.api();

                var total = api.column(8).data().reduce(function (a, b) {
                    return a + b;
                }, 0);
                var totalcurrency = formatCurrency(total.toFixed(2).toString());
                self.Total(totalcurrency);
            },
            createdRow: function (row, data, index) {
                var api = this.api();
                var colindex = api.column('Total:name')[0][0];
                var td = api.cells(row, colindex).nodes()[0];
                // This is to format the 'amount' column to look like currency.
                var amt = formatCurrency(data.TotalAmount.toFixed(2).toString());
                $(td).html(amt);

                // And this is to add a small tool-tip.
                $(row).attr('title', 'Click here for Batch Detail');
            }
        };

        // This overwrites our pagination settings, but if we don't have any input, then we can suppress the first load.
        if (_suppressload)
            gridoptions.deferLoading = 0;

        // Wire up pre and post load events.
        $getE('#batchResultsTable').on('preXhr.dt', function (e, settings, data) {
            framework.showSpinner('Loading...');
        });
        $getE('#batchResultsTable').on('xhr.dt', function (e, settings, json, xhr) {
            framework.hideSpinner();
            if (json.error) {
                // Gracefully handle missing data.
                json.data = [];
                json.recordsTotal = 0;
                json.recordsFiltered = 0;
                framework.errorToast(_container, json.error);
            }
            _totalRecords = json.recordsTotal;
        });

        // Initialize the table, this will call the first AJAX request.
        _batchtable = $getE('#batchResultsTable').DataTable(gridoptions);

        // Wire up search.  We need to remove the default because it's not debounced and calls too much.
        $getE('#batchResultsTable_filter input').unbind();
        $getE('#batchResultsTable_filter input').keyup(function (e) {
            if (e.which === 13) {
                // 13 is the 'enter' key.
                search();
            }
        });

        // Cancel the default Error mode.  This prevents a standard 'alert'.
        $.fn.dataTable.ext.errMode = 'none';

        // Alter the HTML slightly for the search control to be bootstrap-friendly.
        $getE('#batchResultsTable_filter input')
            .wrap("<div></div>")
            .parent()
            .addClass("input-group")
            .prepend("<span><i class='fa fa-1 fa-search' title='Press enter to start search.' /></span>")
            .children().eq(0)
            .addClass("input-group-addon");

        // Recommended method of handling row clicks.
        $getE('#batchResultsTable tbody').on('click', 'tr', function () {
            var rowdata = _batchtable.row(this).data();
            // create data input
            var data = {
                "BankId": rowdata.BankID,
                "WorkgroupId": rowdata.WorkgroupID,
                "BatchId": rowdata.BatchID,
                "DepositDateString": rowdata.DepositDateString
            };
            var breaddata = {
                DateFrom: self.DateFrom(),
                DateTo: self.DateTo(),
                Workgroup: self.Workgroup(),
                PaymentType: self.PaymentType(),
                PaymentSource: self.PaymentSource(),
            };
            var pageinfo = _batchtable.page.info();
            breaddata.length = pageinfo.length;
            breaddata.start = pageinfo.start;
            breaddata.search = $getE('#batchResultsTable_filter input').val();
            if (_batchtable.order().length > 0) {
                breaddata.order = [
                    {
                        column: _batchtable.order()[0][0],
                        dir: _batchtable.order()[0][1]
                    }
                ];
            }
            
            _breadcrumbs.register("Batch Summary", "/RecHubViews/BatchSummary", breaddata);
            framework.loadByContainer("/RecHubViews/BatchDetail/IndexResults", _container.parent(), data);
            return true;
        });


    };

    var formatCurrency = function (n) {
        return '$' + n.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1,");
    };

    var selectedTreeItem = function (selected, initial) {
        var title = null;

        if (selected && !initial) {
            if (selected.isNode)
            {
                title = 'Selected: ' + selected['label'];
                _workgroupSelector.wfsTreeSelector('setTitle', title);
            }
            else 
            {
                framework.errorToast(_container, 'Please select a single workgroup.');
                return false;
            }
        }
    };

    var treeDataCallback = function (data) {
        var entities = _workgroupSelector.wfsTreeSelector('getEntities');
        var workgroups = _workgroupSelector.wfsTreeSelector('getWorkgroups');
        var ecount = entities.length;
        var wcount = workgroups.length;

        if (ecount === 1 && wcount === 1) {
            var element = $('<div><input value="'
              + workgroups[0].label
              + '" class="form-control input-md" type="text" readonly style="width: 200px;display:inline;margin-left: 5px;"></div>');
            $getE('#batch-summary-treebase').html(element);
            self.Workgroup(workgroups[0].id);
            _workgroupLongName = workgroups[0].label;
        }
        else {
            _workgroupSelector.wfsTreeSelector('initialize');
            _treeInitialized = true;
            if (self.Workgroup() != undefined)
                _workgroupSelector.wfsTreeSelector('setSelection', self.Workgroup());
        }
        _workgrouploaded = true;
        performInitialSearch();
    };

    var performInitialSearch = function () {
        if (_workgrouploaded === true && _paymentsloaded === true && _sourcesloaded === true) {
            initTable();
        }
    };

    var validateAndSearch = function () {
        updateModel();
        var query = getQuery();
        
        search(query);
    };

    var search = function (query) {
        _batchtable.ajax.reload();
        _batchtable.column(0).visible(self.ShowBatchId());
    };

    var updateModel = function () {
        // All of our UI is custom controls, which is currently not auto-bindable.
        // So we need to set the VM properties a bit manually.
        self.DateFrom($getE('#dateFrom').wfsDatePicker('getDate'));
        self.DateTo($getE('#dateTo').wfsDatePicker('getDate'));
        if (_treeInitialized) {
            self.Workgroup(_workgroupSelector.wfsTreeSelector('getSelection').id);
        }
        self.PaymentType($getE('#paymentType').wfsSelectbox('getValue'));
    };

    var getQuery = function () {
        var query = {
            DateFrom: self.DateFrom(),
            DateTo: self.DateTo(),
            Workgroup: self.Workgroup(),
            PaymentType: self.PaymentType()
        };

        return query;
    };

    // Constructor
    function batchSearchViewModel() {

    }

    // Public properties
    this.DateFrom = ko.observable(Date.now());
    this.DateTo = ko.observable(Date.now());
    this.Workgroup = ko.observable('');
    this.PaymentType = ko.observable(-1);
    this.PaymentTypes = ko.observable([]);
    this.Results =[];
    this.TreeTitle = ko.observable('Select Workgroup');
    this.Total = ko.observable('');
    this.PaymentSource = ko.observable(-1);
    this.PaymentSources = ko.observable([]);
    this.ShowBatchId = ko.observable(false);

    // Public methods
    batchSearchViewModel.prototype = {
        constructor: batchSearchViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) { return _container.find(b); };

            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            // Function to clean up our data.
            $getE('#batchResultsTable').off('xhr.dt');
            $getE('#batchResultsTable').off('preXhr.dt');
            $getE('#batchResultsTable tbody').off('click');
            DateFrom(null);
            DateTo(null);
            Results = [];
            PaymentType(-1);
            PaymentTypes([]);
            PaymentSource(-1);
            PaymentSources([]);
            ShowBatchId(false);
            Workgroup('');
            _suppressload = true;
            _workgrouploaded = false;
            _paymentsloaded = false;
            _sourcesloaded = false;
            if (_batchtable) {
                _batchtable.destroy();
            }
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return batchSearchViewModel;
});