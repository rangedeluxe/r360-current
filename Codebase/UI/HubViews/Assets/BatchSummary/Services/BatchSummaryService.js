﻿// Service allows for searching for batches with the BatchSummarySearchQuery.

define(['jquery', 'frameworkUtils'], function ($, framework) {

    // Private Vars

    // Constructor for the Service Object
    function batchSummaryService() {

    }

    // Private Methods

    // Public Methods
    batchSummaryService.prototype = {

        // Map the Constructor
        constructor: batchSummaryService,

        // Define our Search Method
        getBatches: function (query, callback) {
            framework.doJSON('/RecHubViews/BatchSummary/GetBatches', query, callback);
        },

        getPaymentTypes: function (callback) {
            framework.doJSON('/RecHubViews/BatchSummary/GetPaymentTypes', null, callback);
        },

        getPaymentSources: function (callback) {
            framework.doJSON('/RecHubViews/BatchSummary/GetPaymentSources', null, callback);
        }

    }

    // Returning new is optional, depends on what you're trying to do.
    return new batchSummaryService();
});