﻿// Service allows for searching for batches with the BatchSummarySearchQuery.

define(['jquery', 'frameworkUtils'], function ($, framework) {

    function batchSummaryService() {

    }

    batchSummaryService.prototype = {
        constructor: batchSummaryService,
        getBatches: function (query, callback) {

            var anon = {
                BatchID: 1010,
                SourceBatchID: 2020,
                DepositDate: Date.now(),
                PaymentSource: 'Image RPS',
                PaymentSourceID: 2,
                PaymentType: 'Check',
                PaymentTypeID: 2,
                PaymentCount: 10,
                TransactionCount: 5,
                DocumentCount: 5,
                TotalAmount: 500.25
            };

            var anon2 = {
                BatchID: 202020,
                SourceBatchID: 3054020,
                DepositDate: Date.now(),
                PaymentSource: 'Image RPS',
                PaymentSourceID: 2,
                PaymentType: 'Check',
                PaymentTypeID: 2,
                PaymentCount: 26,
                TransactionCount: 5,
                DocumentCount: 5,
                TotalAmount: 5260.25
            };

            if (callback)
                callback([anon, anon, anon2, anon2, anon2, anon2, anon2, anon2, anon2, anon2, anon2, anon2, anon2]);
        },
        getPaymentTypes: function (callback) {
            var data = [{ Id: 0, Name: '-- All --' },{ Id: 1, Name: 'ACH' }, { Id: 2, Name: 'Check' }];
            if (callback)
                callback(data);
        }

    }

    return new batchSummaryService();
});