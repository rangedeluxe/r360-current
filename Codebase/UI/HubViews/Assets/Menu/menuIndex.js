﻿define(["jquery", "ko", "frameworkUtils"], function ($, ko, framework) {

  function Menu() {

    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };

    setupElementBindings = function () {

    };

    function ViewModel(model) {
      var self = this;
    }

    this.init = function (myContainerId, model) {

      _container = $('#' + myContainerId);
      $getE = function (b) { return _container.find(b); };
      _vm = new ViewModel(model);
      
      setupElementBindings();

      var url = '/IPOnline/RecHubPage/Menu/';
      var data = {};
      var divContainer = $('#' + 'oltaMenu');

      framework.loadByContainer(url, divContainer, data);

    };
  }

  function init(myContainerId, model) {
    $(function () {
      var menu = new Menu();
      menu.init(myContainerId, model);
    });
  }

  return {
    init: init
  }
});
