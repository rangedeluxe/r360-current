﻿define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/DataTablesUtilities.js',
    '/RecHubViews/Assets/Shared/Breadcrumbs.js', '/RecHubViews/Assets/Shared/Formatting.js', 
    'datatables-bootstrap'],
    function ($, framework, ko, utilities, breadcrumbs, formatting) {
        let $getE;
        let self = this;
        let _container;
        let _model;
        let _breadcrumbs = new breadcrumbs();
        let _formatting = new formatting();
        let _utils = new utilities();
        let _exceptionsGrid;
        let _recordsTotal;
        let _request = {};
        let _search = '';
        let columns = [
            { "data": "TransactionSequence", name: "LockIcon", width: 1, orderable: false },
            { "data": "EntityBreadcrumb", OrderBy: "Entity", name: "EntityBreadcrumb", width: 200 },
            { "data": "Workgroup", OrderBy: "WorkgroupName", name: "Workgroup" },
            { "data": "PaymentSource", OrderBy: "PaymentSource" },
            { "data": "PaymentType", OrderBy: "PaymentType" },
            { "data": "SourceBatchId", OrderBy: "BatchID", class: 'alignRight' },
            { "data": "TransactionSequence", OrderBy: "TransactionID", class: 'alignRight' },
            { "data": "DepositDate", OrderBy: "DepositDate" },
            { "data": "PaymentTotal", OrderBy: "Amount", name: "PaymentTotal", class: 'alignRight'}
        ];
        let _gridOptions = {
            processing: false,
            ajax: {
                url: '/RecHubViews/PostDepositExceptions/GetTransactions',
                type: 'POST',
                data: function(d) {
                    // Add the order by clause to the sent data.
                    let column = columns[d.order[0].column];
                    d.OrderBy = column.OrderBy;
                    d.OrderByDirection = d.order[0].dir;
                    d.Start = d.start;
                    d.Length = d.length;
                    d.Draw = d.draw;
                    d.Search = _search;
                    _request.OrderBy = column.OrderBy;
                    _request.OrderByDirection = d.OrderByDirection;
                    _request.Start = d.start;
                    _request.Length = d.length;
                    _request.Draw = d.draw;
                    _request.Search = d.Search;
                }
            },
            pageLength: (_model && _model.length && _model.length > 0) ? _model.length : 10,
            serverSide: true,
            autoWidth: false,
            scrollX: true,
            columns: columns,
            order: [[1, 'asc']],
            search: {
                search: (_model && _model.search) ? _model.search : ''
            },
            createdRow: function (row, data, index) {
                var api = this.api();
                var colindex = api.column('PaymentTotal:name')[0][0];
                var td = api.cells(row, colindex).nodes()[0];
                var tdEntity = api.cells(row, api.column('EntityBreadcrumb:name')[0][0]).nodes()[0];
                var tdWorkgroup = api.cells(row, api.column('Workgroup:name')[0][0]).nodes()[0];

                // This is to format the 'amount' column to look like currency.
                var amt = _formatting.formatCurrency(data.PaymentTotal.toFixed(2).toString());
                $(td).html(amt);
                $(tdEntity).prop('title', data.EntityBreadcrumb);
                $(tdWorkgroup).prop('title', data.Workgroup);
                $(row).prop('title', 'Click here to view Exceptions Transaction Detail');

                api = this.api();
                colindex = api.column('LockIcon:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];
                $(td).empty();
                if (data.Lock) {
                    let message = data.UserCanUnlock 
                        ? 'Click here to unlock the transaction'
                        : 'Transaction is locked by ' + data.Lock.User + '. Click here to view the locked transaction details';
                    let color = data.UserCanUnlock
                        ? ' text-primary'
                        : '';
                    let obj = data.UserCanUnlock
                        // leaving this commented for a future sprint.
                        //? '<img src="/RecHubViews/Assets/PostDepositExceptions/Css/LockedByCurrentUser.svg" />'
                        ? '<i class="fa fa-lock fa-2" />'
                        : '<i class="fa fa-lock fa-x" />';
                    let $obj = $('<a href="#" class="unlock-button' + color + '" title="' + message + '"></a>')
                        .append(obj);
                    $(td).html($obj);
                }
            }
        };

        let applyUI = function (model) {

            _search = (_model && _model.search) 
                ? _model.search 
                : '';

            // Wire up pre and post load events.
            $getE('#postDepositTable').on('preXhr.dt', function (e, settings, data) {
                $getE('#postDepositTable_filter input').val(_search);
                framework.showSpinner('Loading...');
            });
            $getE('#postDepositTable').on('xhr.dt', function (e, settings, json, xhr) {
                framework.hideSpinner();

                if (json.Error) {
                    // Gracefully handle missing data.
                    json.data = [];
                    json.recordsTotal = 0;
                    framework.errorToast(_container, json.Error);
                    return;
                }
                _recordsTotal = json.recordsTotal;
            });
            $.each(columns, function(i, e) {
                e.width = e.width ? e.width : _utils.measureTextWidth($getE('#postDepositTable thead th').eq(i).text());
            });
            _exceptionsGrid = $getE("#postDepositTable").DataTable(_gridOptions);

            $getE('#postDepositTable tbody').on('click', 'tr', function () {
                var rowdata = _exceptionsGrid.row(this).data();
                let data = {
                    BatchId: rowdata.BatchId,
                    TransactionId: rowdata.TransactionId,
                    DepositDate: rowdata.DepositDate,
                    RecordsTotal: _recordsTotal,
                    OrderBy: columns[_exceptionsGrid.order()[0][0]].OrderBy,
                    OrderDirection: _exceptionsGrid.order()[0][1]
                };
                _breadcrumbs.register("Post-Deposit Exceptions Summary", "/RecHubViews/PostDepositExceptions", _request);
                framework.loadByContainer("/RecHubViews/PostDepositExceptions/Detail", _container.parent(), data);
            });
            $getE('#postDepositTable tbody').on('click', '.unlock-button', function () {
               
                var rowdata = _exceptionsGrid.row($(this).parents('tr').first()).data();
                let data = {
                    BatchId: rowdata.BatchId,
                    TransactionId: rowdata.TransactionId,
                    DepositDate: rowdata.DepositDate,
                };
            
                let dialoghtml = '' +
                    '<dl class="row">' +
                    '<dt class="col-sm-4">Locked By:</dt>' +
                    '<dd class="col-sm-8">' + rowdata.Lock.User + '</dd>'  +
                    '<dt class="col-sm-4">User Login Entity:</dt>'  +
                    '<dd class="col-sm-8">' + rowdata.Lock.UserEntity + '</dd>'  +
                    '<dt class="col-sm-4"> At:</dt>'  +
                    '<dd class="col-sm-8">' + _formatting.toLocalTimeZone(rowdata.Lock.LockedDateString) + '</dd>'  +
                    '<dt class="col-sm-4">Entity:</dt>'  +
                    '<dd class="col-sm-8" data-toggle="tooltip" title="' + rowdata.EntityBreadcrumb + '">' + rowdata.EntityBreadcrumb + '</dd>'  +
                    '<dt class="col-sm-4">Workgroup:</dt>'  +
                    '<dd class="col-sm-8" data-toggle="tooltip" title="' + rowdata.Workgroup + '">' + rowdata.Workgroup + '</dd>'  +
                    '<dt class="col-sm-4">Payment Source:</dt>'  +
                    '<dd class="col-sm-8">' + rowdata.PaymentSource + '</dd>'  +
                    '<dt class="col-sm-4">Payment Type:</dt>'  +
                    '<dd class="col-sm-8">' + rowdata.PaymentType + '</dd>'  +
                    '<dt class="col-sm-4">Batch Id:</dt>'  +
                    '<dd class="col-sm-8">' + rowdata.SourceBatchId + '</dd>'  +
                    '<dt class="col-sm-4">Transaction Id:</dt>'  +
                    '<dd class="col-sm-8">' + rowdata.TransactionId + '</dd>'  +
                    '<dt class="col-sm-4">Deposit Date:</dt>'  +
                    '<dd class="col-sm-8">' + rowdata.DepositDate + '</dd>'  +
                    '<dt class="col-sm-4">Amount:</dt>'  +
                    '<dd class="col-sm-8">' + _formatting.formatCurrency(rowdata.PaymentTotal) + '</dd>'  +
                    '</dl>';

                $('#lockModal').find(".modal-body .container").html(dialoghtml);


                $("#btnUnlock").remove();
                if (rowdata.UserCanUnlock || rowdata.UserCanOverrideUnlock) {
                    var $footer = $('#lockModal').find(".modal-footer");
                    $footer.prepend($("<a />").addClass("btn btn-primary")
                        .attr("id", "btnUnlock")
                        .attr("href", "#")
                        .attr("title", "Click here to unlock the transaction")
                        .text("Unlock")
                        .click(function() {
                            unlockTransaction(rowdata);
                        }));
                }

                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });

                $('#lockModal').modal('show');
                return false;
            });
            // Wire up search.  We need to remove the default because it's not debounced and calls too much.
            $getE('#postDepositTable_filter input').unbind();
            $getE('#postDepositTable_filter input').keyup(function (e) {
                _search = $(this).val();
                if (e.which === 13) {
                    // 13 is the 'enter' key.
                    _exceptionsGrid.ajax.reload();
                }
            });

         
        };

        var unlockTransaction = function(rowdata) {
            let data = {
                TransactionId: rowdata.TransactionId,
                BatchId: rowdata.BatchId,
                DepositDate: rowdata.DepositDate
            };
            framework.showSpinner('Loading...');
            framework.doJSON('/RecHubViews/PostDepositExceptions/UnlockTransaction', data, function(result) {
                if (!result || !result.success || result.success === false) {
                    framework.errorToast(_container, 'An error occurred while unlocking the transaction.');
                    framework.hideSpinner('Loading...');
                }
                else {
					$('#lockModal').modal('hide');
                    _exceptionsGrid.ajax.reload(null, false);
                }
            });
        };

        function postDepositViewModel() {
         
        }

        postDepositViewModel.prototype = {
            constructor: postDepositViewModel(),
            init: function (container, model) {
                _container = container;
                _model = model || {};
                $getE = function (a) {
                    return _container.find(a);
                };
                applyUI(model);
                ko.applyBindings(self, container.get(0));
            },
            dispose: function () {
                $getE('#postDepositTable').off('xhr.dt');
                $getE('#postDepositTable').off('preXhr.dt');
                $getE('#postDepositTable tbody').off('click');
                if (_exceptionsGrid) _exceptionsGrid.destroy();
            }
        };

        return postDepositViewModel;
    }
);