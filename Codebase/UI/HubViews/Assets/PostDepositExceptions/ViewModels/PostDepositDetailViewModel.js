define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js',
    '/RecHubViews/Assets/Shared/DataTablesUtilities.js',
    '/RecHubViews/Assets/Shared/Formatting.js',
    '/RecHubViews/Assets/Shared/ConfirmationDialog.js',
    'bootstrap'],
    function ($, framework, ko, breadcrumbs, utilities, formatting, confirmationdialog) {

        let FEATURE_COUNT_THRESHOLD = 10;
        let $getE;
        let _container;
        let _breadcrumbs = new breadcrumbs();
        let _utilities = new utilities();
        let _formatting = new formatting();
        let _confirmationDialog = new confirmationdialog();
        let _transactions;
        let self = this;
        let _model;
        let _currentTransaction;
        let _paymentsTable;
        let _stubstable;
        let _imageCollection = [];
        let _payerList = [];
        let _currentImageIndex = 0;

        let _clicking = false;
        let _previousX;
        let _previousY;
        let labels = {
            'canAddRelatedItem': "Add a related item to the current transaction",
            'cantAddRelatedItem': "Edits permitted only if locked by current user",
            'canDeleteRelatedItem': "Delete this related item from the current transaction",
            'cantDeleteRelatedItem': "Edits permitted only if locked by current user",
            'canSaveTransaction': "Click here to save current changes",
            'cannotSaveTransaction': "Edits permitted only if locked by current user",
            'canAcceptTransaction': "Click here to accept the current transaction",
            'cannotAcceptTransaction': "Edits permitted only if locked by current user",
            'acceptUnresolved': "Are you sure you want to accept this transaction with unresolved exceptions?"
        };

        let applyUI = function (model) {
            _model = model;
            _currentImageIndex = 0;
            _breadcrumbs.pop('Exceptions Transaction Details');
            let bcs = _breadcrumbs.render();
            $getE('.breadcrumbheader').html(bcs);
            $getE('.breadcrumbheader a').click(function () {
                _breadcrumbs.load(_container.parent(), $(this).data('name'));
            });
            loadTransactionDetail(_model);

            $getE('#button-first-transaction').click(function () {
                applyFirstTransaction();
            });

            $getE('#button-previous-transaction').click(function () {
                applyPreviousTransaction();
            });

            $getE('#button-next-transaction').click(function () {
                applyNextTransaction();
            });

            $getE('#button-last-transaction').click(function () {
                applyLastTransaction();
            });

            $getE('.flip-image').click(function () {
                self.ShowFrontImage(!self.ShowFrontImage());
            });

            $getE('#stubs-table-container').on('click', '.btn-add-related-item', function () {
                addRelatedItem();
            });

            $getE('#button-save').click(function () {
                save();
            });




            $getE('#button-accept').click(function () {
                completeAndNavigate();
            });

            $getE('#stubs-table-container').on('click', '.btn-remove', function () {
                let rowdata = _stubstable.row($(this).parents('tr').first()).data();
                _confirmationDialog.show('Confirm Delete', 'Are you sure you want to delete this related item?',
                    function () {
                        deleteRow(rowdata);
                    },
                    function () {
                        // Do nothing.
                    }, 'Delete', 'Cancel');

                return false;
            });

            $getE('#previous-image').click(function () {
                _currentImageIndex -= 1;
                _currentImageIndex = _currentImageIndex < 0 ? _imageCollection.length - 1 : _currentImageIndex;
                setImage(_currentImageIndex);

            });
            $getE('#next-image').click(function () {
                _currentImageIndex += 1;
                _currentImageIndex = _currentImageIndex >= _imageCollection.length ? 0 : _currentImageIndex;
                setImage(_currentImageIndex);
            });
            $getE('.drawer').click(function () {
                toggleImage();
            });

            $getE('#imageDrawer').click(function () {
                $getE('#imageDrawer').toggleClass('imageDrawer');
            });

            //From here on  is for image panning. This probably should go into its own control
            $getE("#scroll").mousedown(function (e) {
                e.preventDefault();
                _previousX = e.clientX;
                _previousY = e.clientY;
                _clicking = true;
            });
            //this neeeds to be in the whole document so we are not stuck
            //dragging the picture in case we unclick outside the container
            $(document).mouseup(function () {
                _clicking = false;
            });

            $getE("#scroll").mousemove(function (e) {
                if (_clicking) {
                    e.preventDefault();
                    $getE("#scroll").scrollLeft($getE("#scroll").scrollLeft() + (_previousX - e.clientX));
                    $getE("#scroll").scrollTop($getE("#scroll").scrollTop() + (_previousY - e.clientY));
                    _previousX = e.clientX;
                    _previousY = e.clientY;
                }
            });
            $getE("#scroll").mouseleave(function (e) {
                _clicking = false;
            });
            //this keeps the locked warning always on top
            $(window).on('scroll.PostDepositDetailViewModel', function () {
                if ($getE('.header-locked-warning')) {
                    $getE('.locked-warning').toggleClass('fixed',
                        $(window).scrollTop() > $getE('.header-locked-warning').offset().top);
                }
            });

            // A rather crude way to 'bind' the value to a particular DE field.
            $getE('#stubs-table-container').on("change", ".stub-editor", function () {                
                let data = {
                    DataEntryFieldId: $(this).data('itemid'),
                    StubId: $(this).data('stubid'),
                    Value: $(this).val()
                };
                updateRow(data);
            });

            var findPayment = function (rt, account, payerName) {
                for (var i = 0, len = _payerList.length; i < len; i++) {

                    if (!payerName) {
                        if (_payerList[i].RoutingNumber === rt &&
	                        _payerList[i].Account === account)
			                return _payerList[i]; // Return as soon as the object is found
	                } else {
		                if (_payerList[i].RoutingNumber === rt &&
			                _payerList[i].Account === account &&
			                _payerList[i].PayerName === payerName)
			                return _payerList[i]; // Return as soon as the object is found
	                }
                }
	            return null; // The object was not found
            }

            var loadPayerList = function(savePayerModel, payerName) {
	            _payerList.push({
		            BankId: savePayerModel.BankId,
		            ClientAccountId: savePayerModel.ClientAccountId,
                    PayerName: payerName.trim(),
		            RoutingNumber: savePayerModel.RoutingNumber,
		            Account: savePayerModel.Account
	            });
            }

            let handlePayerLoad = function (savePayerModel, val) {
	            var found = findPayment(savePayerModel.RoutingNumber, savePayerModel.Account);
	            if (found != null) {
		            found.PayerName = val.trim();
	            } else {
		            loadPayerList(savePayerModel, val);
	            }
            }

            $getE('#payments-table-container').on("change", ".check-editor", function () {
	            let val = $(this).val();
	            let currentPaymentSeq = $(this).attr('paymentsequence');
	            let data = {
		            PaymentSequence: $(this).attr('paymentsequence'),
		            Value: $(this).val()
	            };
	            
	            let currpayment = _currentTransaction.Payments.filter(function (payment) {
		            return payment.PaymentSequence == currentPaymentSeq;
                });

	            let trowdata = _paymentsTable.row($(this).closest('tr')).data();
	            let savePayerModel = getSavePayerModel(trowdata);

	            if (this.defaultValue !== this.value && !$(this).hasClass('has-payer-suggestion')) {
		            handlePayerLoad(savePayerModel, val);
	            }

                if ($(this).hasClass('has-payer-suggestion') && currpayment[0].PayerList[0].PayerName !== val) {
	                currpayment[0].HasClassSuggestion = "";
                    $(this).removeClass('has-payer-suggestion');

                    handlePayerLoad(savePayerModel, val);
                }
                
	            updatePaymentRow(data);
            });
        };

        


        let getBlankSavePayerModel = function () {
	        return {
		        BankId: 0,
		        ClientAccountId: 0,
		        PayerName: '',
		        RoutingNumber: '',
		        Account: ''
	        };
        };

        let getSavePayerModel = function (data) {
            return {
                BankId: _currentTransaction.BankId,
                ClientAccountId: _currentTransaction.WorkgroupId,
                PayerName: data.Payer.trim(),
                RoutingNumber: data.RT,
                Account: data.AccountNumber
            };
        };

        let callSavePayer = function(data) {
            if (data.currentPayerName === null || data.currentPayerName !== data.Payer.trim() || data.Payer.trim() !== "") {
                let savePayerModel = getSavePayerModel(data);
                if (savePayerModel !== null) {
                    data.currentPayerName = data.Payer.trim();
                    framework.doJSON('/RechubViews/PostDepositExceptions/SavePayer', savePayerModel);
                }
            }
        };

        let deleteRow = function (data) {
            let itemindex = -1;
            $.each(_currentTransaction.Stubs, function (i, e) {
                if (e.BatchSequence == data.BatchSequence)
                    itemindex = i;
            });
            if (itemindex === -1) {
                return;
            }
            _currentTransaction.Stubs.splice(itemindex, 1);
            let page = _stubstable.page.info()["page"];
            if (_stubstable.page.info()["end"] == _stubstable.page.info()["start"] + 1)
                page--;
            _stubstable.clear();
            _stubstable.rows.add(_currentTransaction.Stubs);
            _stubstable.columns.adjust().draw();
            _stubstable.page(page).draw('page');

            //updateBalancing();
        };

        let toggleImage = function () {
            $getE('#colinfo').toggleClass('col-md-5 col-md-12');
            $getE('#colimage').toggleClass('col-md-7 col-md-0');
            $getE('.viewer-wrapper').toggleClass('hide');
        };

        let calculateTotals = function () {
            let defaultamount = 0;
            let paymentamount = 0;
            let stubamount = 0;
            $.each(_currentTransaction.Payments, function (i, e) {
                paymentamount += isNaN(e.Amount) ? 0 : e.Amount;
            });
            $.each(_currentTransaction.Stubs, function (i, stub) {
                $.each(stub.DataEntryFields, function(j, field) {
                    if (field.FieldName === "Amount") {
                        stubamount += isNaN(field.Value) ? 0 : field.Value;
                    }
                });
            });
            defaultamount = paymentamount - stubamount;

            return {
                PaymentsTotal: paymentamount,
                StubsTotal: stubamount,
                Difference: defaultamount
            };
        };

        let loadTransactionDetail = function (model) {
            framework.showSpinner('Loading...');
            framework.doJSON('/RechubViews/PostDepositExceptions/GetTransactionDetail', model, function (result) {
                if (!result.success) {
                    framework.hideSpinner();
                    framework.errorToast(_container, result.error);
                    return;
                }
                _currentImageIndex = 0;
                renderPage(result.data);
                setImage(_currentImageIndex);
            });
        };

        let stubDataEntryFieldErrors = function (stubField) {
            let allErrors = (stubField.BusinessRuleExceptions || [])
                .concat(stubField.DataTypeValidationErrors || []);
            return allErrors;
        };

        let stubDataEntryFieldHasErrors = function (stubField) {
            return stubDataEntryFieldErrors(stubField).length > 0;
        };

        let stubHasDataEntryErrors = function (stub) {
            return stub.DataEntryFields.some(function (stubField) {
                if (stubField.IsEditable) return stubDataEntryFieldHasErrors(stubField);
            });
        };

        let paymentHasErrors = function(checkdata) {
            return !checkdata || checkdata.Payer === "";
        };

        let paymentHasRequiredPayerError = function (payment) {
            return _currentTransaction.PostDepositPayerRequired && (!payment.Payer || payment.Payer.trim() === '');
        };
        //looks in the editable defields to match for the required decolumn
        //and returns true if any are empty
        let stubHasRequiredDataEntryErrors = function (stub) {
            let decollection = stub.DataEntryFields || [];
            return decollection.filter(function (defield) { return defield.IsEditable; })
                .some(function (defield) {
                    let decols = _currentTransaction.DataEntrySetupList || [];
                    return decols.filter(function (decol) { return decol.IsRequired; })
                        .some(function (field) {
                            return defield.FieldName == field.FieldName &&
                                (!defield.Value || defield.Value === '');
                        });
                });
        };       

        let updatePaymentRow = function(data) {
            let payment = _currentTransaction.Payments.filter(function(payment) {
                return payment.PaymentSequence == data.PaymentSequence;
            });
            if (payment) {
                payment[0].Payer = data.Value;
            }
        };

        let updateRow = function(data) {
            let fieldName = data.DataEntryFieldId;
            let stubSequence = data.StubId;
            let stubs = _currentTransaction.Stubs.filter(function(stub) {
                return stub.BatchSequence == stubSequence;
            });
            if (stubs != null && stubs.length > 0) {
                let fields = stubs[0].DataEntryFields.filter(function(field) {
                    return field.FieldName === fieldName;
                });

                // Update the data entry value.
                if (fields != null && fields.length > 0) {
                    fields[0].Value = data.Value;
                }

                // Update the value for data tables to render.
                let fieldname = "Stubs" + fields[0].FieldName;
                stubs[0][fieldname] = data.Value;
            }
        };

        let renderPage = function (transaction) {
            if (!transaction)
                return;

            _currentTransaction = transaction;

            $.each(_currentTransaction.Payments, function (index, payment) {
                if (!payment.Payer) {
	                payment.Payer = "";
                };
            });

            _newsequence = 0;
            self.DepositDate(_currentTransaction.DepositDate);
            self.Entity(_currentTransaction.EntityBreadcrumb);
            self.Workgroup(_currentTransaction.Workgroup);
            self.PaymentSource(_currentTransaction.PaymentSource);
            self.PaymentSource(_currentTransaction.PaymentSource);
            self.PaymentType(_currentTransaction.PaymentType);
            self.BatchId(_currentTransaction.SourceBatchId);
            self.TransactionId(_currentTransaction.TransactionSequence);
            self.LockOwnerWarning(!_currentTransaction.UserCanEdit);
            self.UserCanEdit(_currentTransaction.UserCanEdit);
            let user = _currentTransaction.Lock
                ? _currentTransaction.Lock.User
                : '';
            self.LockedByUser(user);
            updateNavigation(_currentTransaction.Transactions);
            self.SaveTooltip(_currentTransaction.UserCanEdit
                ? labels['canSaveTransaction']
                : labels['cannotSaveTransaction']);
            self.AcceptTooltip(_currentTransaction.UserCanEdit ?
                labels['canAcceptTransaction'] : labels['cannotAcceptTransaction']);
            let totals = calculateTotals();
            self.PaymentTotal(_formatting.formatCurrency(totals.PaymentsTotal));
            self.StubsTotal(_formatting.formatCurrency(totals.StubsTotal));

            // Payments table.
            let $paymentcolumns = $("<tr></tr>");
            let paymentcolumns = [];
            $paymentcolumns.append('<th class="sorting_disabled"></th>');
            paymentcolumns.push({ data: 'PaymentSequence', name: "icon", class: "alignRight", sortable: false, width: "12px", defaultCol: true });

            $paymentcolumns.append("<th>Payment Sequence</th>");
            paymentcolumns.push({ data: 'PaymentSequence', class: "alignRight" });
            $paymentcolumns.append("<th>R/T</th>");
            paymentcolumns.push({ data: 'RT' });
            $paymentcolumns.append("<th>Account Number</th>");
            paymentcolumns.push({ data: 'AccountNumber' });
            $paymentcolumns.append("<th>Check/Trace/Ref Number</th>");
            paymentcolumns.push({ data: 'CheckTraceRefNumber' });
            $paymentcolumns.append("<th>Payer</th>");
            paymentcolumns.push({ data: 'Payer', name: "Payer", width:"160px"});
            $paymentcolumns.append("<th>DDA</th>");
            paymentcolumns.push({ data: 'DDA' });
            $paymentcolumns.append("<th>Payment Amount</th>");
            paymentcolumns.push({ data: 'Amount', class: "alignRight", name: "Amount" });

            if (_currentTransaction.Payments.length > 0 && _currentTransaction.DataEntrySetupList.length > 0) {
                let paymentdesetupfields = _currentTransaction.DataEntrySetupList.filter(function (e) {
                    return e.TableName.toLowerCase().indexOf("checks") >= 0;
                });
                $.each(paymentdesetupfields, function (i, e) {
                    // Add the data to the columns.
                    $paymentcolumns.append("<th>" + e.FieldName + "</th>");
                    let key = e.TableName + e.FieldName;
                    let col = { data: key };
                    if (e.DataType == "7" || e.DataType == "6")
                        col.class = "alignRight";
                    paymentcolumns.push(col);
                    // Add the data to the payment records.
                    $.each(_currentTransaction.Payments, function (j, p) {
                        let de = $.grep(p.DataEntryFields, function (decol) {
                            return decol.FieldName === e.FieldName && e.TableName === "Checks" && decol.Type === e.DataType;
                        })[0];
                        if (de) {
                            let value = de.Value;
                            if (de.Type == "7" && de.Value && de.Value.length > 0)
                                value = _formatting.formatCurrency(de.Value);
                            p[key] = value;
                        }
                        else {
                            p[key] = '';
                        }
                    });
                });
            }

            if (_paymentsTable) {
                _paymentsTable.clear();
                _paymentsTable.destroy();
            }

            let $paymentdiv = $getE('#payments-table-container');
            $paymentdiv.empty();
            $paymentdiv.append($("<table>")
                .attr("id", "payments-table")
                .attr("class", "table dataTable table-striped table-hover")
                .append("<thead></thead>"));

            $getE('#payments-table thead').append($paymentcolumns);
            $.each(paymentcolumns, function (i, e) {
                e.width = e.width ? e.width : _utilities.measureTextWidth($getE('#payments-table thead th').eq(i).text());
            });

            _paymentsTable = $getE('#payments-table').DataTable({
                autoWidth: false,
                columns: paymentcolumns,
                data: _currentTransaction.Payments,
                drawCallback: function (settings) {
                    let enablefeatures = _currentTransaction.Payments && _currentTransaction.Payments.length > FEATURE_COUNT_THRESHOLD;
                    if (enablefeatures) {
                        $getE('#payments-table_length').parent().parent().show();
                        $getE('#payments-table_info').parent().parent().show();
                    }
                    else {
                        $getE('#payments-table_length').parent().parent().hide();
                        $getE('#payments-table_info').parent().parent().hide();
                    }
                },
                createdRow: function (row, data, index) {
                    let api = this.api();
                    let colindex = api.column('Amount:name')[0][0];
                    let td = api.cells(row, colindex).nodes()[0];
                    $(td)
                        .empty()
                        .append(_formatting.formatCurrency(data.Amount));

                    let hoverMessage = _currentTransaction.UserCanEdit
                        ? 'Payer'
                        : 'Edits permitted only if locked by current user';


                    let iconcell = api.cells(row, 0).nodes()[0];
                    $(iconcell)
                        .empty();
                    if (_currentTransaction.PostDepositPayerRequired && paymentHasErrors(data)) {
                        $(iconcell)
                            .append('<i class="fa fa-exclamation-triangle excep-icon-row" aria-hidden="true"></i>');
                    }


                    if (_currentTransaction.PostDepositPayerRequired) {
                        let payerIndex = api.column('Payer:name')[0][0];
                        let payerTd = api.cells(row, payerIndex).nodes()[0];
                        //the original content will be our original payer
                        let originalContent = _currentTransaction.Payments[index].Payer;
                        let $input = $('<input type="text" class="form-control check-editor">')
                            .attr('value', originalContent ? originalContent : '')
                            .attr('paymentsequence', data.PaymentSequence)
                            .attr('title', hoverMessage)
                            .attr('maxLength', 60)
                            .addClass('payerField');

                        _currentTransaction.Payments[index].currentPayerName = $(payerTd).text();
                        
                        let curPayerList = _currentTransaction.Payments[index].PayerList;
                        //we check if we have suggested payer names and set the value of the field and of the current payment
                        if (curPayerList && curPayerList.length > 0 && (!originalContent)) {
	                        _currentTransaction.Payments[index].HasClassSuggestion = "yes";
                            $input.addClass('has-payer-suggestion');
                            $input.val(curPayerList[0].PayerName);
                            _currentTransaction.Payments[index].currentPayerName = curPayerList[0].PayerName;                            
                        }
                        else if (!originalContent || originalContent.trim() === "" || paymentHasErrors(data)) {
                            $input.addClass("has-exception");
                        }
                        
                        if (!_currentTransaction.UserCanEdit)
                            $input.prop('disabled', true);

                        $(payerTd).empty().append($input);
                    }

                },
                scrollX: true
            });



            // Stubs Table.
            let $stubcolumns = $("<tr></tr>");
            let stubcolumns = [];
            $stubcolumns.append("<th></th>");
            stubcolumns.push({ data: 'StubSequence', name: "icon", class: "alignRight", sortable: false, width: "12px", defaultCol: true });
            $stubcolumns.append("<th>Stub Sequence</th>");
            stubcolumns.push({
                data: 'StubSequence',
                class: "alignRight",
                defaultCol: true,
                "render": function(data, type, full, meta) { return full.StubSequence < 0 ? "" : full.StubSequence; }
            });

            if (_currentTransaction.DataEntrySetupList.length > 0) {
                let stubdesetupfields = _currentTransaction.DataEntrySetupList.filter(function (e) {
                    return e.TableName.toLowerCase().indexOf("stubs") >= 0;
                });
                $.each(stubdesetupfields, function (i, e) {
                    $stubcolumns.append("<th>" + e.FieldName + "</th>");
                    let key = e.TableName + e.FieldName;
                    let col = { data: key };
                    if (e.DataType == "7" || e.DataType == "6")
                        col.class = "alignRight";
                    stubcolumns.push(col);
                    $.each(_currentTransaction.Stubs, function (j, p) {
                        let de = $.grep(p.DataEntryFields, function (decol) { 
                            return decol.FieldName == e.FieldName && e.TableName == "Stubs" && (decol.Type == e.DataType || decol.DataType == e.DataType);
                        })[0];
                        if (de) {
                            let value = de.Value;
                            if (de.Type == "7" && de.Value && de.Value.length > 0)
                                value = _formatting.formatCurrency(value, true);
                            p[key] = value;
                        }
                        else {
                            p[key] = '';
                        }
                    });
                });
            }

            $stubcolumns.append("<th></th>");
            stubcolumns.push({ data: "StubSequence", name: "options", sortable: false, class: "alignRight" });
            if (_stubstable) {
                _stubstable.clear();
                _stubstable.destroy();
            }

            let $stubdiv = $getE('#stubs-table-container');
            let tooltip = _currentTransaction.UserCanEdit ? labels["canAddRelatedItem"] :
                labels["cantAddRelatedItem"];
            $stubdiv.empty();
            $stubdiv.append($("<table>")
                .attr("id", "stubs-table")
                .attr("class", "table dataTable table-striped table-hover")
                .append("<thead></thead>")
                .append("<tfoot><tr><td colspan='" + stubcolumns.length + "'>"
                + "<button" + (_currentTransaction.UserCanEdit ? "" : " disabled")
                + " title='" + tooltip + "' class='btn-add-related-item btn btn-secondary'>"
                + "<span class='fa fa-plus fa-lg text-success'></span> Add related item</button>"
                + "</td></tr></tfoot>"));


            $getE('#stubs-table thead').append($stubcolumns);
            $.each(stubcolumns, function (i, e) {
                e.width = e.width ? e.width : _utilities.measureTextWidth($getE('#stubs-table thead th').eq(i).text());
            });

            let order = [1, 'asc'];
            if (stubcolumns.length == 1) {
                order = [0, 'asc'];
            }
            _stubstable = $getE('#stubs-table').DataTable({
                order: order,
                autoWidth: false,
                columns: stubcolumns,
                data: _currentTransaction.Stubs,
                ordering: false,
                drawCallback: function (settings) {
                    let enablefeatures = _currentTransaction.Stubs && _currentTransaction.Stubs.length > FEATURE_COUNT_THRESHOLD;
                    if (enablefeatures) {
                        $getE('#stubs-table_length').parent().parent().show();
                        $getE('#stubs-table_info').parent().parent().show();
                    }
                    else {
                        $getE('#stubs-table_length').parent().parent().hide();
                        $getE('#stubs-table_info').parent().parent().hide();
                    }
                },
                createdRow: function (row, data, index) {
                    let api = this.api();
                    let amountColumnIndex = api.column('Amount:name')[0][0];
                    let amountTd = api.cells(row, amountColumnIndex).nodes()[0];
                    let iconcell = api.cells(row, 0).nodes()[0];
                    $(amountTd)
                        .empty()
                        .append(data.Amount);

                    $(iconcell)
                        .empty();
                    if (stubHasDataEntryErrors(data)) {
                        $(iconcell)
                            .append('<i class="fa fa-exclamation-triangle excep-icon-row" aria-hidden="true"></i>');
                    }

                    $.each(data.DataEntryFields,
                        function (stubFieldIndex, stubField) {
                            if (stubField.IsEditable) {
                                let stubFieldName = 'Stubs' + stubField.FieldName;
                                let editableColumnIndex = stubcolumns.indexOf($.grep(stubcolumns, function (column) {
                                    return (column.data === stubFieldName || column.data === stubField.FieldName) && !column.defaultCol;
                                })[0]);
                                let editableTd = api.cells(row, editableColumnIndex).nodes()[0];
                                let $editableTd = $(editableTd);
                                let allErrors = stubDataEntryFieldErrors(stubField);
                                let hoverMessage = _currentTransaction.UserCanEdit
                                    ? allErrors.join("\n")
                                    : 'Edits permitted only if locked by current user';
                                let originalContent = $editableTd.text();
                                if (editableColumnIndex > -1) {
                                    // Field is editable. Let's build the input element.
                                    let $input = $('<input type="text" class="form-control stub-editor">')
                                        .attr('title', hoverMessage)
                                        .attr('value', originalContent)
                                        .data('itemid', stubField.FieldName)
                                        .data('stubid', data.BatchSequence);

                                    // Add CSS classes to indicate the data type. Not used by JS or CSS;
                                    // these are here for use by Selenium tests.
                                    if (stubField.Type === 6) {
                                        $input.attr('data-type', 'numeric');
                                    } else if (stubField.Type === 7) {
                                        $input.attr('data-type', 'currency');
                                    } else if (stubField.Type === 11) {
                                        $input.attr('data-type', 'date');
                                    } else {
                                        $input.attr('data-type', 'text');
                                    }

                                    if (allErrors.length > 0)
                                        $input.addClass("has-exception");

                                    if (!_currentTransaction.UserCanEdit)
                                        $input.prop('disabled', true);

                                    $editableTd
                                        .empty()
                                        .append($input);

                                    let dataTypeErrors = stubField.DataTypeValidationErrors || [];
                                    $.each(dataTypeErrors,
                                        function (errorIndex, error) {
                                            let $errorDiv = $('<div></div>')
                                                .addClass('data-type-error text-danger')
                                                .text(error);
                                            $editableTd.append($errorDiv);
                                        });
                                }
                            }
                        }
                    );

                    // Edit the html for the options column.
                    let showicon = data.DataEntryFields.filter(function (e) { return !e.IsUIAddedField; }).length == 0;
                    let canedit = _currentTransaction.UserCanEdit;
                    let colindex = api.column('options:name')[0][0];
                    let td = api.cells(row, colindex).nodes()[0];
                    $(td).empty();
                    if (showicon) {
                        let tooltip = canedit
                            ? labels.canDeleteRelatedItem
                            : labels.cantDeleteRelatedItem;
                        $(td).append('<button class="btn btn-secondary btn-remove"' + (canedit ? "" : " disabled") + ' title="' + tooltip + '"><i href="#" class="fa fa-times fa-2x text-danger"></i></button>');
                    }
                },
                scrollX: true
            });

            //Image stuff
            //Build payments image list
            let paymentsImgs = _currentTransaction.Payments
                .filter(function (payment) {
                    return payment.ImageIsAvailable === true;
                })
                .map(function (payment) {
                    return {
                        BatchId: _currentTransaction.BatchId,
                        BatchSequence: payment.BatchSequence,
                        DepositDateString: _currentTransaction.DepositDate,
                        IsCheck: true
                    };
                });

            let docsImgs = _currentTransaction.Documents.filter(function (doc) {
                return doc.ImageIsAvailable
            }).map(function (doc) {
                return {
                    BatchId: _currentTransaction.BatchId,
                    BatchSequence: doc.BatchSequence,
                    DepositDateString: _currentTransaction.DepositDate,
                    IsCheck: false
                };
            });
            _imageCollection = paymentsImgs.concat(docsImgs);
        };

        let setImage = function (imageIndex) {
            if (imageIndex < 0 || imageIndex >= _imageCollection.length) {
                self.ImageNavigationText('0/0');
                self.CurrentBackImage('');
                self.ImageAvailable(false);
                framework.hideSpinner();
                return;
            };
            self.ImageAvailable(true);
            let queryString = $.param({
                BatchId: _imageCollection[imageIndex].BatchId,
                BatchSequence: _imageCollection[imageIndex].BatchSequence,
                DepositDateString: _imageCollection[imageIndex].DepositDateString,
                IsCheck: _imageCollection[imageIndex].IsCheck
            });
            framework.showSpinner("Loading...");
            framework.doJSON('/RechubViews/Image/GetSingleImage', queryString, function (result) {
                if (!result.success || !result.frontData) {
                    framework.errorToast(_container, "An error occurred while retrieving the image.");
                }
                self.CurrentImage('data:image/png;base64, ' + result.frontData);
                self.CurrentBackImage(
                    result.backData ? 'data:image/png;base64, ' + result.backData : ''
                );
                self.ImageNavigationText(imageIndex + 1 + '/' + _imageCollection.length);
                framework.hideSpinner();
            });
        };

        let updateNavigation = function (data) {
            _transactions = data;
            let currentIndex = findTransactionIndex(_currentTransaction);
            let total = _transactions.length;
            self.EnableFirstTransaction(total > 1 && currentIndex > 0);
            self.EnablePreviousTransaction(total > 1 && currentIndex > 0);
            self.EnableNextTransaction(total > 1 && currentIndex < total - 1);
            self.EnableLastTransaction(total > 1 && currentIndex < total - 1);
            self.CurrentTransactionText(currentIndex + 1 + "/" + total);
        };

        let mapErrorsAndReloadGrid = function (data) {
            $.each(_currentTransaction.Stubs, function (stubIndex, stub) {
                let businessRuleExceptionsForThisStub = data.BusinessRuleExceptions.filter(function (e) {
                    return e.BatchSequence === stub.BatchSequence;
                });
                let dataTypeErrorsForThisStub = data.DataTypeErrors.filter(function (e) {
                    return e.BatchSequence === stub.BatchSequence;
                });

                $.each(stub.DataEntryFields, function (dataEntryFieldIndex, dataEntryField) {
                    let businessRuleExceptionsForThisField = businessRuleExceptionsForThisStub.filter(function (e) {
                        return e.FieldName === dataEntryField.FieldName;
                    }).map(function (e) { return e.Error; });
                    let dataTypeErrorsForThisField = dataTypeErrorsForThisStub.filter(function (e) {
                        return e.FieldName === dataEntryField.FieldName;
                    }).map(function(e) { return e.Error; });

                    dataEntryField.BusinessRuleExceptions = businessRuleExceptionsForThisField;
                    dataEntryField.DataTypeValidationErrors = dataTypeErrorsForThisField;
                });
            });

            renderPage(_currentTransaction);
        };

        let applyFirstTransaction = function () {
            saveAndNavigate(0);
        };

        let applyPreviousTransaction = function () {
            let currentindex = findTransactionIndex(_currentTransaction);
            saveAndNavigate(currentindex - 1);
        };

        let applyNextTransaction = function () {
            let currentindex = findTransactionIndex(_currentTransaction);
            saveAndNavigate(currentindex + 1);
        };

        let applyLastTransaction = function () {
            saveAndNavigate(_transactions.length - 1);
        };

        let getEditableStubs = function () {

            let editableStubs = [];

            $.each(_currentTransaction.Stubs, function (stubIndex, stub) {
                let editableFields = stub.DataEntryFields
                    .filter(function (field) { return field.IsEditable; })
                    .map(function (field) {
                        // Grab the fields we need in order to save:
                        //onDataEntryFieldChanged should have update the ViewModel _currentTransaction.
                        return {
                            FieldName: field.FieldName,
                            Value: field.Value && field.Type == 7
                                ? field.Value
                                    .replace(new RegExp('\\$', 'g'), '')
                                    .replace(new RegExp(',', 'g'), '')
                                : field.Value
                        };
                    });

                // ignore rows that don't have any data entered in.
                let isnewblankrow = stub.BatchSequence < 0 && !editableFields.some(function(e) { return e.Value && e.Value.length > 0 });
                if (editableFields.length > 0 && !isnewblankrow) {
                    editableStubs.push({
                        BatchSequence: stub.BatchSequence,
                        DataEntryFields: editableFields
                    });
                }
            });

            return editableStubs;
        };

        let getEditableChecks = function () {
            //we grab the suggested payer name if the current payer is empty. 
            //Because thats what the field is displaying before a save
            let chkIndex = 0;
            let editableChecks = _currentTransaction.Payments.map(function (chk) {

                if (_currentTransaction.Payments 
                && _currentTransaction.Payments[chkIndex].PayerList 
                && _currentTransaction.Payments[chkIndex].PayerList.length > 0 
                && !chk.Payer 
                && _currentTransaction.Payments[chkIndex].HasClassSuggestion 
                && _currentTransaction.Payments[chkIndex].HasClassSuggestion === "yes") {
                    chk.Payer = _currentTransaction.Payments[chkIndex].PayerList[0].PayerName;
                    _currentTransaction.Payments[chkIndex].HasClassSuggestion = "";
                }

                chkIndex++;

	            let chmodel = {
                    CheckSequence: chk.PaymentSequence,
                    Payer: chk.Payer
                };
                return chmodel;
            });
            return editableChecks;
        };

        let getSaveModel = function () {
            let editableChecks = getEditableChecks();
            let editableStubs = getEditableStubs();
            let payers = _payerList;
            return {
                DepositDate: _currentTransaction.DepositDate,
                BatchId: _currentTransaction.BatchId,
                SourceBatchId: _currentTransaction.SourceBatchId,
                TransactionId: _currentTransaction.TransactionId,
                Stubs: editableStubs,
                Checks: editableChecks,
                Payers: payers
            };
        };

        let save = function () {
            callSave(function (result) {
                // The screen is going to stay on this transaction, so we need to update the grid with
                // any new exception and data-type error information, whether the save succeeded or not.
                if (result.data != null) {
                    mapErrorsAndReloadGrid(result.data);
                }
                framework.hideSpinner();
                if (!result.success) {
                    framework.errorToast(_container, result.error);
                }
            });
        };

        let completeAndNavigate = function () {                
            callSave(function (result) {
                if (!result.success) {
                    framework.hideSpinner();
                    framework.errorToast(_container, result.error);
                    if (result.data != null &&
                        result.data.DataTypeErrors != null &&
                        result.data.DataTypeErrors.length > 0) {

                        mapErrorsAndReloadGrid(result.data);
                    }
                }
                else {

                    //Validation here occurs on a client basis, as of right now server validation is being kept in the servers
                    //and the results are not truly displayed in the client, this is why im redoing payment's payer validation here.
                    let anyExceptions = false;                  
                    let paymentErrors = false;
                    let stubs = _currentTransaction.Stubs || [];
                    let payments = _currentTransaction.Payments || [];                    
                    payments.forEach(function (payment) {
                        paymentErrors |=  paymentHasRequiredPayerError(payment);
                    });
                    anyExceptions = paymentErrors | anyExceptions;
                    stubs.forEach(function (stub) { 
                        anyExceptions = anyExceptions || stubHasRequiredDataEntryErrors(stub);
                    });
                    

                    if (anyExceptions) {
                        if (shouldMapErrors(result.data)) {
                            mapErrorsAndReloadGrid(result.data);
                        }
                        
                        if (paymentErrors) {
                            //here is where we'd add more error displaying
                            renderPage(_currentTransaction);
                        }
                        framework.hideSpinner();
                        _confirmationDialog.show('Confirm Accept', labels['acceptUnresolved'],
                            function () { complete(completeCallbackAndNavigate); },
                            function () {/*do nothing because you asked to do nothing*/ },
                            'Accept', 'Cancel');
                    }
                    else {
                        complete(completeCallbackAndNavigate);
                    }
                }
            });
        };

        let shouldMapErrors = function (data) {
            if (!data) return false;
            if (data.DataTypeErrors && data.DataTypeErrors.length > 0) return true;
            if (data.BusinessRuleExceptions && data.BusinessRuleExceptions.length > 0) return true;
            return false;
        };

        let completeCallbackAndNavigate = function (result) {
            if (!result.success) {
                framework.errorToast(_container, "There was an error Accepting the transaction.");
                framework.hideSpinner();
            }
            else {
                let currentIndex = findTransactionIndex(_currentTransaction) + 1;
                if(currentIndex < _transactions.length){
                    navigateToTransaction(_transactions[currentIndex]);
                }
                else {
                    framework.loadByContainer("/RecHubViews/PostDepositExceptions/", _container.parent(), {});
                    framework.hideSpinner();
                }
            }
        };

        let callSave = function (saveCallback) {
            framework.showSpinner('Saving...');
            let saveModel = getSaveModel();
            framework.doJSON('/RechubViews/PostDepositExceptions/SaveTransaction', saveModel, saveCallback);
        };

        let complete = function (completeCallback) {
            framework.showSpinner('Saving...');
            let saveModel = getSaveModel();
            framework.doJSON('/RechubViews/PostDepositExceptions/CompleteTransaction',
                saveModel, completeCallback);
        };

        let saveAndNavigate = function (transactionIndex) {

            framework.showSpinner('Saving...');

            if (!_currentTransaction.UserCanEdit) {
                //If cannot edit
                //navigateToTransaction will hide the Spinner.
                navigateToTransaction(_transactions[transactionIndex]);
                return;
            }

            let saveModel = getSaveModel();

            framework.doJSON('/RechubViews/PostDepositExceptions/SaveTransaction',
                saveModel,
                function (result) {
                    if (!result.success) {
                        framework.hideSpinner();
                        framework.errorToast(_container, result.error);
                        // We're staying on this transaction, because navigation failed.
                        // Update the grid with new exceptions and data-type errors.
                        if (result.data != null &&
                            result.data.DataTypeErrors != null &&
                            result.data.DataTypeErrors.length > 0) {

                            mapErrorsAndReloadGrid(result.data);
                        }
                    } else {
                        //result.success move to next record
                        //navigateToTransaction will hide the Spinner.
                        navigateToTransaction(_transactions[transactionIndex]);
                    }
                });
        };

        let addRelatedItem = function () {
            let totals = calculateTotals();

            // sets the first batch sequence to -1, followed by lower numbers going forward.
            let batchSequence = -1;
            if (_currentTransaction.Stubs && _currentTransaction.Stubs.length > 0) {
                let min = Math.min.apply(null, _currentTransaction.Stubs.map(function(s) { return s.BatchSequence }));
                if (min < 0)
                    batchSequence = min - 1;
            }

            let newrow = {
                BatchSequence: batchSequence,
                StubSequence: "",
                DataEntryFields: [],
                IsNewRow: true,
                NewRow: true
            };
            //Add the current DE fields
            let stubsdelist = _currentTransaction.DataEntrySetupList
                .filter(function (val) {
                    return val.TableName.toLowerCase().indexOf('stubs') >= 0;
                });

            $.each(stubsdelist, function (i, e) {
                // Set the stub property for the grid.
                let key = e.TableName + e.FieldName;
                newrow[key] = newrow[key] ? newrow[key] : '';
                // Add the DE column for the data.
                newrow.DataEntryFields.push({
                    DESetupFieldID: e.DESetupFieldID,
                    TableName: e.TableName,
                    FieldName: e.FieldName,
                    DataType: e.DataType,
                    Value: newrow[key],
                    IsEditable: true,
                    IsUIAddedField: true
                });
            });

            _currentTransaction.Stubs.push(newrow);
            _stubstable.clear();
            _stubstable.rows.add(_currentTransaction.Stubs);
            _stubstable.columns.adjust().draw();
            _stubstable.page('last').draw('page');
        };

        let findTransactionIndex = function (transaction) {
            for (let i = 0; i < _transactions.length; ++i) {
                if (_transactions[i].TransactionId === transaction.TransactionId
                    && _transactions[i].BatchId === transaction.BatchId && _transactions[i].ShortDepositDate == transaction.DepositDate) {
                    return i;
                }
            }
        };

        let navigateToTransaction = function (transaction) {
            let model = transaction;
            model.OrderBy = _model.OrderBy;
            model.OrderDirection = _model.OrderDirection;
            model.DepositDate = transaction.ShortDepositDate;
            model.RecordsTotal = _model.RecordsTotal;
            model.PreviousBatchId = _currentTransaction.BatchId;
            model.PreviousTransactionId = _currentTransaction.TransactionId;
            model.PreviousDepositDate = _currentTransaction.DepositDate;
            loadTransactionDetail(model);
        };
        //public bound props
        self.DepositDate = ko.observable('');
        self.Entity = ko.observable('');
        self.Workgroup = ko.observable('');
        self.PaymentSource = ko.observable('');
        self.PaymentType = ko.observable('');
        self.BatchId = ko.observable('');
        self.EnableFirstTransaction = ko.observable(false);
        self.EnablePreviousTransaction = ko.observable(false);
        self.EnableNextTransaction = ko.observable(false);
        self.EnableLastTransaction = ko.observable(false);
        self.CurrentTransactionText = ko.observable('');
        self.TransactionId = ko.observable('');
        self.PaymentTotal = ko.observable(0);
        self.StubsTotal = ko.observable(0);
        self.CurrentImage = ko.observable('');
        self.CurrentBackImage = ko.observable('');
        self.LockedByUser = ko.observable('');
        self.ImageNavigationText = ko.observable('');
        self.ShowFrontImage = ko.observable(true);
        self.LockOwnerWarning = ko.observable(false);
        self.UserCanEdit = ko.observable(false);
        self.SaveTooltip = ko.observable('');
        self.AcceptTooltip = ko.observable('');
        self.ImageAvailable = ko.observable(false);

        function postDepositDetailViewModel() {
        }

        postDepositDetailViewModel.prototype = {
            constructor: postDepositDetailViewModel(),
            init: function (container, model) {
                _container = container;
                $getE = function (a) {
                    return _container.find(a);
                };
                applyUI(model);
                ko.applyBindings(self, container.get(0));
            },
            dispose: function () {
                $(window).off('scroll.PostDepositDetailViewModel');
                self.DepositDate('');
                self.Entity('');
                self.Workgroup('');
                self.PaymentSource('');
                self.PaymentType('');
                self.BatchId('');
                self.EnableFirstTransaction(false);
                self.EnablePreviousTransaction(false);
                self.EnableNextTransaction(false);
                self.EnableLastTransaction(false);
                self.CurrentTransactionText('');
                self.TransactionId('');
                self.PaymentTotal(0);
                self.StubsTotal(0);
                self.CurrentImage('');
                self.CurrentBackImage('');
                self.ShowFrontImage(true);
                _currentImageIndex = 0;
                self.LockOwnerWarning(false);
                self.SaveTooltip('');
                self.AcceptTooltip('');
            }
        };
        return postDepositDetailViewModel;
    });