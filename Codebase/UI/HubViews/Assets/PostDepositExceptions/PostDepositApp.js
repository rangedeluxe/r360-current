﻿function initPostDeposit(model, controllername, viewmodelname, container) {
    require.config({
        paths: {
            'postDepositViewModel': '/RecHubViews/Assets/PostDepositExceptions/ViewModels/PostDepositViewModel',
            'postDepositDetailViewModel': '/RecHubViews/Assets/PostDepositExceptions/ViewModels/PostDepositDetailViewModel.js?v=20171219'
        }
    });

    require([controllername, viewmodelname], function (controller, viewmodel) {
        controller.init(model, viewmodel, container);
    });
}