﻿function initNotifications(model, controllername, viewmodelname, container) {
    require.config({
        paths: {
            'notificationsViewModel': '/RecHubViews/Assets/Notifications/ViewModels/NotificationsViewModel.js?v=20170425',
            'notificationDetailViewModel': '/RecHubViews/Assets/Notifications/ViewModels/NotificationDetailViewModel',
        }
    });

    require([controllername, viewmodelname], function (controller, viewmodel) {
        controller.init(model, viewmodel, container);
    });
}