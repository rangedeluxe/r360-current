define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js', 
        '/RecHubViews/Assets/Shared/ImageService.js', '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js',
        'datatables', 'datatables-bootstrap', 'bootstrap', 'wfs.select', 'wfs.treeselector', 'select2'],
function ($, framework, ko, breadcrumbs, imageService) {
    let $getE;
    let _container;
    let _attachmenttable;
    let _imageService = new imageService();
    let _breadcrumbs = new breadcrumbs();

    let applyUI = function(model) {

        // Standard Breadcrumb logic.
        _breadcrumbs.pop("Notification Detail");
        let bcs = _breadcrumbs.render();
        $getE('.breadcrumbheader').html(bcs);
        $getE('.breadcrumbheader a').click(function () {
            _breadcrumbs.load(_container.parent(), $(this).data('name'));
        });
        
        // Get the table defined.
        let columnsdata = [
            { "data": "UserFileName" },
            { "data": "FileTypeDescription" },
            { "data": "UserFileName", "name": "download", width:100 }
        ];
        let gridOptions = {
            columns: columnsdata,
            createdRow: function (row, data, index) {
                let api = this.api();
                let colindex = api.column('download:name')[0][0];
                let td = api.cells(row, colindex).nodes()[0];

                let showDownloadLink = data.FileSize > 0 && self.MetaData().HasDownloadPermission === true;
                if (showDownloadLink)
                    $(td).html('<i class="fa fa-cloud-download fa-lg download-icon"></i>');
                else
                    $(td).html('File not Available');
            }
        }; 

        _attachmenttable = $getE('.files-table')
            .DataTable(gridOptions);

        // Hook up download links.
        $getE('.files-table tbody').on('click', '.download-icon', function () {
            let rowdata = _attachmenttable.row($(this).parents('tr').first()).data();
            _imageService.downloadCENDSFile(rowdata.FilePath);
        });
        

        loadNotification(model.MessageGroup);
    };

    let refreshGrid = function(data) {
        _attachmenttable.clear();
        _attachmenttable.data().rows.add(data);
        _attachmenttable.draw();
    };

    let loadNotification = function(id) {
        let data = {
            MessageGroup: id
        };

        framework.showSpinner('Loading...');
        framework.doJSON('/RechubViews/Notifications/GetNotificationDetail', data, function(results) { 
            if (!results || results.error) {
                framework.errorToast(_container, results.error);
                framework.hideSpinner();
                return;
            }

            framework.hideSpinner();
            self.Notification(results.data);
            self.MetaData(results.metadata);
            refreshGrid(self.Notification().Files);
        });
    };

    self.Notification = ko.observable({});
    self.MetaData = ko.observable({});

    function notificationDetailViewModel() {
        
    }

    notificationDetailViewModel.prototype = {
        constructor: notificationDetailViewModel,
        init: function(container, model) {
            _container = container;
            $getE = function(a) {
                return _container.find(a);
            };
            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            self.Notification({});

            if (_attachmenttable) {
                _attachmenttable.destroy();
            }
        }
    }
    return notificationDetailViewModel;
});