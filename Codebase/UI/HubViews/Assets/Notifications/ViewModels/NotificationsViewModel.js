﻿// Disable ReSharper inspections for now:
// ReSharper disable InconsistentNaming
// ReSharper disable VariableUsedInInnerScopeBeforeDeclared
define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/Breadcrumbs.js', 'moment', '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js',
        'datatables', 'datatables-bootstrap', 'bootstrap', 'wfs.select', 'wfs.treeselector', 'select2'],
function ($, framework, ko, breadcrumbs, moment) {
    //private members
    var $getE;
    var _container;
    var _notificationsTable;
    var _notifications;
    var _workgroupSelector;
    var _workgroupSelectorEnabledAndInitialized = false;
    var _notificationTypesLoaded = false;
    var _workgrouploaded = false;
    var _breadcrumbs = new breadcrumbs();
    var _model;
    var _performInitialSearch = false;
    var _search;
    var _uiDateFrom;
    var _uiDateTo;
    var self = this;

    var WfsDatePickerViewModel = (function() {
        function WfsDatePickerViewModel($e) {
            this.$e = $e;
        }

        WfsDatePickerViewModel.prototype = {
            getString: function() {
                return this.$e.wfsDatePicker('getDate');
            },
            getMoment: function() {
                return moment.utc(this.getString(), 'MM/DD/YYYY');
            },
            setString: function(value) {
                this.$e.wfsDatePicker('setDate', value);
            }
        };
        return WfsDatePickerViewModel;
    }());

    var applyUI = function(model) {

        _model = model;
  
        self.DateFrom(model.StartDate);
        self.DateTo(model.EndDate);
        self.SelectedWorkgroup(model.Workgroup);
        self.NotificationName(model.FileName);
        self.NotificationType(model.FileTypeKey);
        if (model.StartDate)
            _performInitialSearch = true;

        $getE('#notifications-search').click(searchNotification);
        $getE('#notifications-reset').click(resetNotification);

        _workgroupSelector = $getE('#workgroup-selector')
            .wfsTreeSelector({
                useExpander: true,
                entityURL: '/RecHubRaamProxy/api/entity',
                callback: selectedTreeItem,
                dataCallback: dataCallback,
                expanderTitle: "Select an Entity or Workgroup",
                entitiesOnly: false
            });
        //fetch file types
        framework.doJSON('/RechubViews/Notifications/GetNotificationsFileTypes', null, notificationTypesCallBack);

        $getE('#dateFrom')
            .wfsDatePicker({ callback: function() { self.DateFrom(_uiDateFrom.getString()); } });
        _uiDateFrom.setString(self.DateFrom());

        $getE('#dateTo')
            .wfsDatePicker({ callback: function() { self.DateTo(_uiDateTo.getString()); } });
        _uiDateTo.setString(self.DateTo());
    };

    var initTable = function() {
        var columnsdata = [
            { "data": "DateString", name: "Date", OrderByName: "DATE", width:"15%" },
            { "data": "Id", name: "ID", OrderByName: "NOTIFICATIONSOURCE", width: "10%" },
            { "data": "Message", OrderByName: "MESSAGE", width: "65%" },
            { "data": "AttachmentCount", name: "Attachments", width: "10%", OrderByName: "FILE" }
        ];

        var ordercol = 1;
        var orderdir = _model.SortByDir ? _model.SortByDir : 'asc';
        var g = $.grep(columnsdata, function(e, i) { 
            e.index = i;
            return e.OrderByName == _model.SortBy; 
        });
        if (g && g.length > 0)
            ordercol = g[0].index;

        var gridOptions = {
            processing: false,
            serverSide: true,
            searching: false,
            displayStart: (_model.length && _model.length > 0) ? _model.start : 0,
            pageLength: (_model.length && _model.length > 0) ? _model.length : 10,
            ajax : {
                url: '/RechubViews/Notifications/GetNotifications',
                type : 'POST',
                data : function(d) {
                    d.StartDate = self.DateFrom();
                    d.EndDate = self.DateTo();
                    d.SortBy = _model.SortBy;
                    d.SortByDir = _model.SortByDir;
                    d.FileName = self.NotificationName();
                    d.FileTypeKey = self.NotificationType();
                    d.Workgroup = self.SelectedWorkgroup();
                    d.search = _search;
                }
            },
            order: [[ordercol, orderdir]],
            columns: columnsdata
        };

        framework.showSpinner('Loading...');

        _notificationsTable = $getE('#notification-table')
            .DataTable(gridOptions);

        $getE('#notification-table').on('preXhr.dt', function(e, settings, data) {
            framework.showSpinner('Loading...');
            var column = columnsdata[data.order[0].column];
            data.SortBy = column.OrderByName;
            data.SortByDir = data.order[0].dir;
            _model.SortBy = data.SortBy;
            _model.SortByDir = data.SortByDir;
        });
        $getE('#notification-table').on('xhr.dt', function (e, settings, json, xhr) { framework.hideSpinner(); });

        $getE('#notification-table_filter input').unbind();
        $getE('#notification-table_filter input').keyup(function (e) {
            if(e.which === 13) {
                // 13 is the 'enter' key.
                searchNotification();
            }
        });

        $getE('#notification-table tbody').on('click', 'tr', function () {
            var rowdata = _notificationsTable.row(this).data();
            var pageinfo = _notificationsTable.page.info();
            var breaddata = {
                StartDate: self.DateFrom(),
                EndDate: self.DateTo(),
                Workgroup: self.SelectedWorkgroup(),
                NotificationType: self.NotificationType(),
                NotificationName: self.NotificationName(),
                FileName: self.NotificationName(),
                FileTypeKey: self.NotificationType(),
                start: pageinfo.start,
                length: pageinfo.length,
                SortBy: _model.SortBy,
                SortByDir: _model.SortByDir
            };
            var data = {
                MessageGroup: rowdata.MessageGroup
            };

            _breadcrumbs.register("Notifications", "/RecHubViews/Notifications", breaddata);
            framework.loadByContainer("/RecHubViews/Notifications/Detail", _container.parent(), data);
            return true;
        });

        // Cancel the default Error mode.  This prevents a standard 'alert'.
        $.fn.dataTable.ext.errMode = 'none';

            // Alter the HTML slightly for the search control to be bootstrap-friendly.
            $getE('#notification-table input')
            .wrap("<div></div>")
            .parent()
            .addClass("input-group")
            .prepend("<span><i class='fa fa-1 fa-search' title='Press enter to start search.' /></span>")
            .children().eq(0)
            .addClass("input-group-addon");

            $getE('#notification-table').on('xhr.dt', function (e, settings, json, xhr) {
            framework.hideSpinner();
            if (json.error) {
                // Gracefully handle missing data.
                json.data = [];
                json.recordsTotal = 0;
                json.recordsFiltered = 0;
                framework.errorToast(_container, json.error);
            }
        });

    };

    var searchNotification = function () {
        // Validation
        if (!self.SelectedWorkgroup() || self.SelectedWorkgroup() == '') {
            framework.errorToast(_container, 'Please select an entity or workgroup first.');
            return;
        }

        updateModel();
        _notificationsTable.ajax.reload();
    };

    var swapDatesIfNeeded = function() {
        if (_uiDateFrom.getMoment().isAfter(_uiDateTo.getMoment())) {
            var temp = _uiDateFrom.getString();
            _uiDateFrom.setString(_uiDateTo.getString());
            _uiDateTo.setString(temp);
        }
    };

    var updateModel = function () {
        swapDatesIfNeeded();
        self.DateFrom(_uiDateFrom.getString());
        self.DateTo(_uiDateTo.getString());
        self.NotificationType($getE('#notifications-type').wfsSelectbox('getValue'));
        _search = $getE('#notification-table_filter input').val();
    }
    var initialSearch = function() {
        if (_notificationTypesLoaded && _workgrouploaded) {
            updateModel();
            initTable();
        }
    };

    var resetNotification = function() {

        self.DateFrom(Date.now());
        self.DateTo(Date.now());
        _uiDateTo.setString(self.DateTo());
        _uiDateFrom.setString(self.DateFrom());
        self.NotificationName('');
        self.NotificationType(0);
        if (_notificationTypesLoaded) {
            $getE('#notifications-type').wfsSelectbox('setValue', self.NotificationType());
        }
        _workgroupSelector.wfsTreeSelector('setSelection');
        searchNotification();
    };

    var notificationTypesCallBack = function (data) {
        if (!data || !data.success) {
            framework.errorToast(_container, 'Error Loading Notification Types.');
            return;
        }
        _notificationTypesLoaded = true;
        self.NotificationTypes(data.data);
        $getE('#notifications-type').wfsSelectbox({
            items: self.NotificationTypes(),
            displayField: "FileTypeDescription",
            idField: "NotificationFileTypeKey",
            width: "300px",
            callback: function () {
                self.NotificationType($getE('#notifications-type').wfsSelectbox('getValue'));
            }
        });
        $getE('#notifications-type').wfsSelectbox('setValue', self.NotificationType());
        initialSearch();
    }

    var selectedTreeItem = function (selected, initial) {
        var title = null;
    
        if ((selected && !initial)
            || (selected && initial && !self.SelectedWorkgroup())) {
            title = 'Selected: ' + selected['label'];
            self.SelectedWorkgroup(selected.id);
        }
        _workgroupSelector.wfsTreeSelector('setTitle', title);
    };

    var dataCallback = function (data) {
        var entities = _workgroupSelector.wfsTreeSelector('getEntities');
        var workgroups = _workgroupSelector.wfsTreeSelector('getWorkgroups');
        var ecount = entities.length;
        var wcount = workgroups.length;

        if (ecount === 1 && wcount === 1) {
            var element = $('<div><input value="'
              + workgroups[0].label
              + '" class="form-control input-md" type="text" readonly style="width: 200px;display:inline;"></div>');
            $getE('#workgroup-selector').html(element);
            self.SelectedWorkgroup(workgroups[0].id);
        }
        else {
           _workgroupSelector.wfsTreeSelector('initialize');
           _workgroupSelectorEnabledAndInitialized = true;
           if (self.SelectedWorkgroup() && self.SelectedWorkgroup() != '')
                _workgroupSelector.wfsTreeSelector('setSelection', self.SelectedWorkgroup());
        }
        _workgrouploaded = true;
        initialSearch();
    };

    var loadNotification = function(callback) {
        framework.showSpinner("Loading...");
        framework.doJSON('/RechubViews/Notifications/GetNotifications',
            {},
            function(results) {
                if (result.Errors) {
                    framework.errorToast(_container, result.Error);
                    framework.hideSpinner();
                    _notifications = null;
                    return;
                }
                framework.HideSpinner();
                _notifications = result.data;
                callback(_notifications);
            });
    };

    var refreshResults = function (data) {
        _notificationsTable.clear();
        _notificationsTable.data.rows.add(data);
    };

    //public props
    this.DateFrom = ko.observable('');
    this.DateTo = ko.observable('');
    this.NotificationTypes = ko.observable([]);
    this.NotificationType= ko.observable(0);
    this.NotificationName = ko.observable('');
    this.SelectedWorkgroup = ko.observable('');

    //ctor
    function notificationsViewModel() {
        
    }
    //exposes ctor
    notificationsViewModel.prototype = {
        constructor: notificationsViewModel,
        init: function(container, model) {
            _container = container;
            $getE = function(a) {
                return _container.find(a);
            };
            _uiDateFrom = new WfsDatePickerViewModel($getE('#dateFrom'));
            _uiDateTo = new WfsDatePickerViewModel($getE('#dateTo'));
            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            _notificationTypesLoaded = false;
            _workgrouploaded = false;
            _performInitialSearch = false;
            _uiDateFrom = undefined;
            _uiDateTo = undefined;

            if (_notificationTable) {
                _notificationTable.destroy();
            }
        }
    }
    return notificationsViewModel;
});