﻿function Batch() {
  var _vm = {};
  var accounting = require("accounting");

  var getOptions = function (defaults) {
    var mine = {};
    var options = $.extend({}, defaults, mine);
    return options;
  };

  var getColumns = function (columMD) {
    var cols = [];
    var colIndex = 0;

    var colBatch = { id: 'colBatchDetail', name: ' ', field: '', resizable: false, width: 30, formatter: detailRenderer };
    cols[colIndex++] = colBatch;

    for (var i = 0; i < columMD.length; i++) {
      // if datatype = currency add the denom to column header
      var denom = columMD[i].DataType === 7 ? " (" + _vm.labels.Denom + ")" : "";
      // right align date(11), currency(7) and floating point(6)... this couples data values with presentation :(
      var align = columMD[i].DataType === 7 || columMD[i].DataType === 6 || columMD[i].DataType === 11 ? "alignRight" : "";
      var col = {
        id: columMD[i].Id,
        name: columMD[i].ColumnTitle + denom,
        field: columMD[i].ColumnFieldName,
        toolTip: columMD[i].Tooltip,
        formatter: valueRenderer,
        dataType: columMD[i].DataType,
        sortable: true,
        headerCssClass: align,
        cssClass: align
      };
      cols[colIndex++] = col;
    }
    return cols;
  };

  var valueRenderer = function (row, cell, value, columnDef, dataContext) {
    if (columnDef.dataType === 7 && ($.trim(value).length >= 0)) {
      var denom = _vm.labels.Denom;
      value = accounting.formatMoney(value, denom + " ");
    }
    if (value && columnDef.dataType === 11 && ($.trim(value).length >= 0)) {
      var d = WFSDateUtil.getWFSDate(new Date(parseInt(value.substr(6))));
      value = WFSDateUtil.formatWFSDate(d);
    }
    return value ? value : "";
  };

  var detailRenderer = function (row, cell, value, columnDef, dataContext) {
    return '<a class="tableIcon" title="' + _vm.labels.ViewDetails + '"><span><i class="fa fa-edit"></i></span></a>';
  };

  var processClick = function (item, colDef) {
    if (colDef.id === "colBatchDetail") {
      return new Detail();
    }
    return null;
  };

  var getPrintOptions = function () {
    // mapping report parameters to object/grid values
    var reportGBMapping = { 'PaymentSource': 'BatchSource', 'PaymentType': 'PaymentType', 'Account': 'ClientAccount' };
    var reportSBMapping = { 'Account': 'ClientAccount', 'PaymentSource': 'BatchSource', 'PaymentType': 'PaymentType', 'TransactionCount': 'TranCount' };
    // get the map values from selected grouping/sort and sort direction
    var grouping = "";
    var sb = reportSBMapping[_vm.sortField] ? reportSBMapping[_vm.sortField] : 'TotalAmount';
    var gb = reportGBMapping[grouping] ? reportGBMapping[grouping] : '';
    var sd = _vm.sortdir ? 'Ascending' : 'Descending';
    // build the report
    var report = {
      reportName: "Receivables Detail",
      parameterValues: [
      { Name: "FD", Value: WFSDateUtil.formatWFSDate($getE('.summaryDepositDate').wfsDatePicker('getDate')) },
        { Name: "GB", Value: gb },
        { Name: "SB", Value: sb },
        { Name: "SO", Value: sd }
      ]
    };
    return report;
  };

  var getSortField = function () { return _vm['sortField']; }
  var getLabels = function () { return _vm['labels']; }

  var sViewModel = function (model) {
    var self = this;
    // labels for this view
    self.labels = model.Labels;
    // initial sort column
    self.sortField = "Total";
  }

  function init(model) {
    $(function () {
      _vm = new sViewModel(model);
    });
  }

  return {
    init: init,
    getPrintOptions: getPrintOptions,
    getColumns: getColumns,
    getOptions: getOptions,
    getSortField: getSortField,
    processClick: processClick,
    getLabels: getLabels,
    dataLocation: "/RecHubViews/BatchSummary/GetData"
  }
}