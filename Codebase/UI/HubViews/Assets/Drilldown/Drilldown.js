﻿define(["jquery", "ko", "frameworkUtils", "accounting"], function ($, ko, framework, accounting) {

  function Drilldown() {
    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };

    var defaultOptions = {
      enableCellNavigation: false,
      enableColumnReorder: false,
      forceFitColumns: true,
      fullWidthRows: true,
      editable: false,
      asyncEditorLoading: false,
      autoEdit: false,
      autoHeight: true
    };


    var resizeGrid = function () {
      var grid = _vm["grid"];
      if (grid) {
        grid.invalidate();
        grid.render();
        grid.resizeCanvas();
      }
    };

    var gridSorter = function (colField, sortAsc) {
      var grid = _vm["grid"];
      var dataView = _vm['dataView'];

      _vm.sortdir = sortAsc;
      _vm.sortField = colField;

      // using native sort with comparer
      // preferred method but can be very slow in IE with huge datasets
      dataView.sort(function (a, b) {
        var colField = _vm.sortField;
        var x = a[colField], y = b[colField];

        if (typeof x !== "string" || x.toLowerCase() === y.toLowerCase()) {
          return (x == y ? 0 : (x > y ? 1 : -1));
        }
        else {
          return (x.toLowerCase() === y.toLowerCase() ? 0 : (x.toLowerCase() > y.toLowerCase() ? 1 : -1));
        }
      }, sortAsc);
    }

    ko.bindingHandlers.grid = {
      init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var groupItemMetadataProvider = new Slick.Data.GroupItemMetadataProvider();

        var dataView = new Slick.Data.DataView({
          groupItemMetadataProvider: groupItemMetadataProvider
        });

        viewModel['dataView'] = dataView;
        viewModel['groupItemMetadataProvider'] = groupItemMetadataProvider;

        dataView.setPagingOptions({
          pageSize: 15,
        });

        $(window).bind('resize', function () {
          resizeGrid();
        });
      },
      update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var items = _vm.currentData().Items;
        var dataView = _vm['dataView'];
        if (_vm.current && _vm.currentData().FieldDefinitions) {
          var columns = _vm.current.getColumns(_vm.currentData().FieldDefinitions);
          var options = _vm.current.getOptions(defaultOptions);
          var grid = new Slick.Grid(element, dataView, columns, options);
          grid.registerPlugin(viewModel['groupItemMetadataProvider']);
          var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"));

          dataView.beginUpdate();
          dataView.setItems(items, "Id");
          dataView.endUpdate();

          _vm["grid"] = grid;

          // sort
          grid.onSort.subscribe(function (e, args) {
            gridSorter(args.sortCol.field, args.sortAsc);
          });

          grid.onClick.subscribe(function (e, args) {
            var item = grid.getDataItem(args.row);
            var colDef = grid.getColumns()[args.cell];

            var obj = _vm.current.processClick(item, colDef);
            if (obj && !item.__group) {
              _vm.current = obj;
              getData();
            }
          });

          // wire up model events to drive the grid
          dataView.onRowCountChanged.subscribe(function (e, args) {
            grid.updateRowCount();
            resizeGrid();
          });

          dataView.onRowsChanged.subscribe(function (e, args) {
            resizeGrid();
          });

          var columnItem = $.grep(_vm.currentData().FieldDefinitions, function (e) { return e.ColumnFieldName == _vm.current.getSortField(); });
          if (columnItem && columnItem.length > 0) {
          grid.setSortColumn(columnItem[0].Id, _vm.sortdir);
          gridSorter(_vm.sortField, _vm.sortdir);
          }
          resizeGrid();
        }
      }
    };

    var groupBy = function () {
      var dataView = _vm.dataView;
      var groupings = new Array();
      //  group by secondary
      if (_vm.selectedGrouping()) {
        var groupId = _vm.selectedGrouping().Id;
        var groupName = _vm.selectedGrouping().Name;
        groupings[0] =
        {
          getter: groupId,
          formatter: function (g) {
            return groupName + ': ' + g.value;
          },
          comparer: function (a, b) {
            var x = a['value'], y = b['value'];
            if (typeof x !== "string" || x.toLowerCase() === y.toLowerCase()) {
              return (x == y ? 0 : (x > y ? 1 : -1));
            }
            else {
              return (x.toLowerCase() === y.toLowerCase() ? 0 : (x.toLowerCase() > y.toLowerCase() ? 1 : -1));
            }
          },
          aggregators: [
            new Slick.Data.Aggregators.Sum("TransactionCount"),
            new Slick.Data.Aggregators.Sum("Total")
          ],
          collapsed: false,
          displayTotalsRow: true
        };
      }
      dataView.setGrouping(groupings);
    };


    var groupSelection = function (value) {
      _vm.selectedGrouping(value);
      groupBy();
      var item = _vm["grid"].getDataItem(0);
      if (item) {
        _vm["grid"].getData().expandGroup(item.groupingKey);
      }
    };

    var getData = function (obj) {
      if (obj && typeof obj === 'object') {
        _vm.current = obj.data;
      }

      var url = _vm.current.dataLocation;
      var data = {
        //DepositDate: $getE('.depositDate').wfsDatePicker('getDate')
      };
      framework.doJSON(url, $.toDictionary(data), getDataCallback);
      framework.showSpinner(_vm.labels['Loading']);
    };

    var getDataCallback = function (serverResponse) {
      $getE('#noItemsMessage').empty();
      if (serverResponse.HasErrors) {
        framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
      } else if (!serverResponse.Data) {
        framework.errorToast(_container, "Error");
      } else {
        if (!serverResponse.Data.Items || serverResponse.Data.Items.length < 1) {
          _vm.current.init(serverResponse.Data);
          var messageDiv = $('<div style="font-weight: bold;" >' + _vm.labels.NoData + '</div>');
          $getE('#noItemsMessage').append(messageDiv);
        } else {
          _vm.current.init(serverResponse.Data);
          _vm.sortField = _vm.current.getSortField();
        }
        addCrumb(_vm.current.getLabels().Title, getData, _vm.current);
        _vm.currentData(serverResponse.Data);
      }

      framework.hideSpinner();
    };

    var printCallback = function (serverResponse) {
      framework.hideSpinner();
      if (serverResponse.HasErrors) {
        framework.errorToast(_container, framework.formatErrors(serverResponse.Errors));
      } else {
      	window.open("/RecHubReportingViews/LoadReport?instanceId=" + serverResponse.Data, "_blank");
      }
    };

    var doPrint = function () {
      // call report engine
    	var url = '/RecHubReportingViews/Reporting/InvokeReportAsync/';
      framework.doJSON(url, $.toDictionary(_vm.current.getPrintOptions()), printCallback);
      framework.showSpinner();
    };

    var addCrumb = function (title, callback, obj) {
      var crumb = framework.getViewContainer(_container).find('.hubCrumb');
      if (crumb) {
        HubCrumbs.addCrumb(crumb, title, callback, obj);
      }
    };

    var searchExecuteFilter = function (value, v2) {
      getData();
    };

    var setupElementBindings = function () {
      
      // set filter execute callback
      var execute = framework.getViewContainer(_container).data('executeFilter');
      execute.add(searchExecuteFilter);

      // initialize the grouping select box
      //$getE('.groupingSelectSummary').wfsSelectbox({ items: _vm.groupings, callback: groupSelection, placeHolder: _vm.labels.SelectGrouping });

      $getE('.btnPrintSummary').bind('click', function () {
        doPrint();
      });

      // initialize tooltips
      $getE('.tabbase').tooltip();

    };

    var sViewModel = function (model) {
      var self = this;
      self.labels = model.Labels;
      self.currentData = ko.observable({});

      self.groupings = [
       { Id: 'DDA', Name: 'DDA' },
       { Id: 'PaymentType', Name: self.labels.PaymentType },
       { Id: 'PaymentSource', Name: self.labels.PaymentSource }];
      self.selectedGrouping = ko.observable(null);

      // default sort column
      self.sortField = "";
      self.sortdir = true;
      self.current = new Summary();
    }

    function init(myContainerId, model) {
      $(function () {
        _container = $('#' + myContainerId);
        $getE = function (b) { return _container.find(b); };
        _vm = new sViewModel(model);

        ko.applyBindings(_vm, $getE('.drilldownData').get(0));

        setupElementBindings();

      });
    }

    return {
      init: init
    }
  }

  function init(myContainerId, model) {
    var drilldown = new Drilldown();
    drilldown.init(myContainerId, model);
  }

  return {
    init: init
  }
});

