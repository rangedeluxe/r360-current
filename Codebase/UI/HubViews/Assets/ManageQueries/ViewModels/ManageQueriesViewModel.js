﻿// This is the Knockout-enabled view model. Handles just about everything
// on the back of the view.

define(['jquery', 'frameworkUtils', 'ko', '/RecHubViews/Assets/Shared/ConfirmationDialog.js',
    '/Assets/wfs/wfs-dateutil.js', '/Assets/wfs/wfs-datepicker.js', 'datatables', 'datatables-bootstrap',
    'bootstrap', 'wfs.select', 'wfs.treeselector', 'select2'],
function ($, framework, ko, confirmdialog) {

    var _container;
    var $getE;

    var _queries;
    var _resultTable;
    var _dialog = new confirmdialog();

    var applyUI = function (model) {

        // Setup Grid.
        _resultTable = $getE('#query-table').DataTable({
            data: _queries,
            order: [[1, 'asc']],
            columns: [
                { "data": "Name", name: "OptionsLeft", orderable: false },
                { "data": "Name", name: "Name" },
                { "data": "Description", name: "Description" },
                { "data": "IsDefault", name: "Default" },
                { "data": "Name", name: "OptionsRight", "orderable": false, class: "alignRight" }
            ],
            createdRow: function (row, data, index) {
                // Appending the left and right options columns.
                var api = this.api();

                // Right options
                var colindex = api.column('OptionsRight:name')[0][0];
                var td = api.cells(row, colindex).nodes()[0];

                $(td).empty();
                $(td).append('<span><i class="fa fa-lg fa-times delete-query" title="Delete query"></i></span>');


                // Left options
                colindex = api.column('OptionsLeft:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];

                $(td).empty();
                $(td).append('<span><i class="fa fa-lg fa-edit edit-query" title="Edit query"></i><i class="fa fa-lg fa-search load-query" title="Load query"></i></span>');

                // Default buttons
                var deficon = data.IsDefault == true ? 'fa-check-square-o' : 'fa-square-o';
                var deftext = data.IsDefault == true ? 'Unset default query' : 'Set default query';
                colindex = api.column('Default:name')[0][0];
                td = api.cells(row, colindex).nodes()[0];

                $(td).empty();
                $(td).append('<span><i class="fa fa-lg toggle-default ' + deficon + '" title="' + deftext + '"></i></span>');
            }
        });

        // Button click events
        $getE('#query-table').on('click', '.toggle-default', function () {
            var rowdata = _resultTable.row($(this).parents('tr')).data();

            framework.showSpinner('Loading...');
            framework.doJSON('/RecHubViews/ManageQueries/ToggleDefaultQuery', rowdata, function (result) {
                if (result.Error) {
                    framework.hideSpinner();
                    framework.errorToast(_container, result.Error);
                    return;
                }

                loadQueries(refreshGrid);
            });

        });

        $getE('#query-table').on('click', '.delete-query', function () {
            var rowdata = _resultTable.row($(this).parents('tr')).data();

            _dialog.show("Confirm Delete", "Are you sure you want to delete '" + rowdata.Name + "' query?", function () {

                framework.showSpinner('Loading...');
                framework.doJSON('/RecHubViews/ManageQueries/DeleteQuery', rowdata, function (result) {
                    if (result.Error) {
                        framework.hideSpinner();
                        framework.errorToast(_container, result.Error);
                        return;
                    }

                    loadQueries(refreshGrid);
                });

            }, null, "Delete", "Cancel");

        });

        $getE('#query-table').on('click', '.edit-query', function () {
            var rowdata = _resultTable.row($(this).parents('tr')).data();
            var data = {
                QueryId: rowdata.Id,
                QueryName: rowdata.Name,
                QueryDescription: rowdata.Description,
                DataUrl: '/RecHubViews/ManageQueries/UpdateQuery'
            };

            framework.loadByContainer('/RecHubViews/AdvancedSearch/QueryDialog', framework.getModalContent(), data, framework.openModal({
                closeCallback: function (result) { if (!result.cancel) { loadQueries(refreshGrid); } }
            }), {});
        });

        $getE('#query-table').on('click', '.load-query', function () {
            var rowdata = _resultTable.row($(this).parents('tr')).data();
            var data = {
                QueryId: rowdata.Id,
            };

            framework.loadByContainer('/RecHubViews/AdvancedSearch', _container.parent(), data);
        });

        // Load in the data.
        loadQueries(refreshGrid);
    };

    // draw with 'false' means don't reset our paging data. good for row edits.
    var refreshGrid = function (data) {
        var page = _resultTable.page();
        _resultTable.clear();
        _resultTable.data().rows.add(data);
        _resultTable.draw(false);
    };

    var loadQueries = function (callback) {
        framework.showSpinner("Loading...");
        framework.doJSON('/RecHubViews/ManageQueries/GetStoredQueries', {}, function (result) {
            if (result.Error) {
                framework.errorToast(_container, result.Error);
                framework.hideSpinner();
                _queries = null;
                return;
            }

            framework.hideSpinner();
            _queries = result.data;
            callback(_queries);
        });
    };

    // Constructor
    function advancedSearchViewModel() {

    }

    // Public methods
    advancedSearchViewModel.prototype = {
        constructor: advancedSearchViewModel,
        init: function (container, model) {
            _container = container;
            $getE = function (b) {
                return _container.find(b);
            };

            applyUI(model);
            ko.applyBindings(self, container.get(0));
        },
        dispose: function () {
            // Function to clean up our data.
            Results = [];

            $getE('#query-table').off('click');

            if (_querytable) {
                _querytable.destroy();
            }
        }
    }

    // Return a pointer to the function so the caller can instanciate.
    return advancedSearchViewModel;
});