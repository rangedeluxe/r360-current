﻿function initManageQueries(model, controllername, viewmodelname, container) {
    require.config({
        paths: {
            'manageQueriesViewModel': '/RecHubViews/Assets/ManageQueries/ViewModels/ManageQueriesViewModel',
        }
    });

    require([controllername, viewmodelname], function (controller, viewmodel) {
        controller.init(model, viewmodel, container);
    });
}