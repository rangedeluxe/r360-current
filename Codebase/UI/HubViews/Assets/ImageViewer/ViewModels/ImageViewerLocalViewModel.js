﻿var ImageViewManager = (function () {

    function imageViewerViewModel() {

        var LOCAL_STORAGE_REQUEST_KEY = "imagerequestobject";

        var _container;
        var $getE;

        // private
        var self = this;
        self.Download = false;
        self.RestoreAttempt = false;
        self.Timer = null;
        self.Finished = false;

        var errorMessage = function (message) {
            $().toastmessage('showToast', {
                text: message,
                sticky: false,
                position: 'middle-center',
                type: 'Error',
                closeText: '',
                close: function () {
                    console.log("toast is closed ...");
                }
            });
        };

        var checkStatus = function (id) {
            if (self.Finished === true)
                return;

            var purl = "/RecHubViews/Image/CheckRequestStatus";
            var data = { instanceId: id };
            var callback = checkStatusCallback;
            var callbackArgs = { instanceId: id };

            $.ajax({
                type: 'POST',
                url: purl,
                data: data,
                dataType: "json",
                success: function (resp, textStatus, jqXHR) {
                    checkStatusCallback(resp, callbackArgs);
                },
                error: function (resp, textStatus, jqXHR) {
                    if (resp.status === 401) {
                        // Session Expired
                        sessionExpired();
                    }
                    else {
                        // Different kind of error.
                        checkStatusCallback(resp, callbackArgs);
                    }
                }
            });
            // Required after not using the doJSON method.
            framework.setWebRequestTimestamp();
        };

        var sessionExpired = function() {
            var err = "Received a 401 unauthorized response.";
            self.error(err);
            self.showRestorePanel(true);
            self.RestoreAttempt = true;
            framework.hideSpinner();
        };

        var checkStatusCallback = function (serverResponse, callbackArgs) {
            if (self.Finished === true)
                return;
            if (!serverResponse || !serverResponse.Data) {
                console.log("Invalid Server Response");
            }
            else if (serverResponse.Data < 3 ) {
                // Pending or In Progress
                if (self.RestoreAttempt === true) {
                    self.RestoreAttempt = false;
                    self.showRestorePanel(false);
                    framework.showSpinner('Loading...');
                }
                return;
            } 
            else if (serverResponse.Data === 4) {
                // Error
                var err = "An Error occurred while retrieving the file.";
                if (serverResponse.Errors && serverResponse.Errors.length > 0)
                    err = serverResponse.Errors.length[0];
                self.error(err);
                self.showRestorePanel(true);
                self.RestoreAttempt = true;
                framework.hideSpinner();
            }
            else if (serverResponse.Data === 3) { 
                // Completed
                window.clearInterval(self.Timer);
                framework.hideSpinner();
                if (self.Download == true) {
                    window.location = "/RecHubViews/Image/DownloadInstance?instanceId="
                        + callbackArgs.instanceId;
                }
                else {
                    window.location.href = "/RecHubViews/Image/GetRequestInstance?instanceId="
                        + callbackArgs.instanceId + "&reportName=Images";
                }
                self.Finished = true;
            }
        };

        var createRequest = function (instance) {
            if (instance) {
                self.Download = true;
                self.Timer = window.setInterval(function () {
                    checkStatus(instance);
                }, 5000);
            }
            else {
                var requestobj = JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_REQUEST_KEY));
                if (!requestobj)
                {
                    errorMessage("No request.");
                    framework.hideSpinner();
                    return;
                }
                window.localStorage.removeItem(LOCAL_STORAGE_REQUEST_KEY);
                framework.doJSON(requestobj.URL, requestobj.Data, function (result) {
                    if (!result || result.Error || !result.InstanceId) {
                        var message = 'An error occurred requesting the report.';
                        if (result.Error)
                            message = result.Error;
                        framework.hideSpinner();
                        return;
                    }
                    if (result.Delivery && result.Delivery == 'Download') {
                        self.Download = true;
                    }
                    self.Timer = window.setInterval(function () {
                        checkStatus(result.InstanceId);
                    }, 5000);
                });
            }
        };

        // Public "bindables".  Keeping KO out of this context.
        self.showRestorePanel = function(val) {
            if (val === true)
                $getE('#restore-panel').show();
            else
                $getE('#restore-panel').hide();
        };
        self.error = function(val) {
            $getE('#alert-error').html(val);
        };

        // Public methods
        this.init = function init(container, model) {
            $getE = function (b) { return _container.find(b); };
            _container = $('#' + container);
            framework.showSpinner("Loading...");
            var instance = model && model.InstanceId ? model.InstanceId : null;
            createRequest(instance);

            // Dispose event
            _container.on("remove", function () {
                window.clearInterval(self.Timer);
                self.Download = false;
                self.RestoreAttempt = false;
                self.Finished = false;

                _container.off("remove");
            });
        }
    }

    return {
        get: function () { return new imageViewerViewModel(); }
    };
}());