﻿var ImageViewManager = (function () {

    function imageViewerViewModel() {
        var _container = $("#imageViewerPortlet");

        // private
        var self = this;


        var errorMessage = function(message) {
            $().toastmessage('showToast', {
                text     : message,
                sticky   : false,
                position : 'middle-center',
                type     : 'Error',
                closeText: '',
                close    : function () {
                    console.log("toast is closed ...");
                }
            });
        };

      checkStatus = function(instanceId) {
            var purl = "/RecHubViews/Image/CheckRequestStatus";
            var data = { instanceId: instanceId };
            var callback = checkStatusCallback;
            var callbackArgs = { instanceId: instanceId };

            framework.doJSON(purl, data, callback, callbackArgs);
        };

        checkStatusCallback = function(serverResponse, callbackArgs) {
            if (serverResponse.Data < 3 /*Pending , In Progress */) {
                checkStatus(callbackArgs.instanceId);
            } else if (serverResponse.Data === 4 /*Error*/) {
               framework.hideSpinner();
               errorMessage("An Error occurred while retrieving Image.");
            } else { /*Completed*/
                framework.hideSpinner();
                window.location.href = "/RecHubViews/Image/GetRequestInstance?instanceId="
                    + callbackArgs.instanceId + "&reportName=Images";
            }
        };


        // Constructor


        instanceId = "";

        // Public methods
        this.init = function init(container, model) {
            _container = container;
            $getE = function(b) { return _container.find(b); };
            framework.showSpinner("Loading...");
            instanceId = model.InstanceId;
            // ko.applyBindings(self, container.get(0));
            checkStatus(model.InstanceId);
        }
   

        // Return a pointer to the function so the caller can instanciate.
        //return imageViewerViewModel;
    }

    return {
            get: function () { return new imageViewerViewModel(); }
        };
}());