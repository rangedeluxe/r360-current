﻿function initImageViewer(model) {
    // File contains initialize code.
    require.config({
        paths: {
            "jquery": "/Assets/jquery/js/jquery.min",
            "bootstrap": "/Assets/bootstrap/js/bootstrap",
            'infuser': '/Assets/infuser/infuser.min',
            "bootstrap-dialog": "/Assets/bootstrap-modal/js/bs-modal",
            "knockout": "/Assets/knockout/knockout",
            "jquery.toDictionary": "/Assets/jquery/js/jquery-toDictionary",
            "jquery.toast": "/Assets/jquery-toastmessage/js/jquery.toastmessage",
            "spinner": "/Assets/etc/spin.min",
            "frameworkUtils": "/Assets/framework/JS/frameworkUtils",

            // Dependencies for image viewer.
            // Notice we're using absolute paths so we don't screw up the baseURL for framework.
            'imageViewerController': '/RecHubViews/Assets/ImageViewer/Controllers/ImageViewerController',
            'imageViewerViewModel': '/RecHubViews/Assets/ImageViewer/ViewModels/ImageViewerViewModel',

            //map: {
            //    // Remap 'ko' to 'knockout'.  R360 uses "ko", but knockout.validation requires "knockout"
            //    '*': { 'ko': 'knockout' }
            //},
            shim: {
                "bootstrap": {
                    deps: ["jquery", "jqueryui", "respond"]
                },
                "bootstrap-dialog": {
                    deps: ["jquery", "bootstrap"]
                },
                "knockout": {
                    deps: ["jquery", "infuser"],
                    exports: "ko"

                },
                "jquery.toDictionary": {
                    deps: ["jquery"]
                },
                "jquery.toast": {
                    deps: ["jquery"]
                },
                "frameworkUtils": {
                    deps: ["jquery", "jquery.toast", "jquery.toDictionary", "spinner", "bootstrap-dialog", "wfs.select"]
                }
            }
        }
    });

    
    require(['imageViewerController'], function(controller) {
        controller.init(model);
    });

}