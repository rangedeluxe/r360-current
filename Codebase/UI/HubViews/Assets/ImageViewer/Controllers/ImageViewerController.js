﻿// Controls the Image View UI, uses these dependencies.

define(['jquery', 'knockout', 'frameworkUtils', 'imageViewerViewModel'],
function ($, ko, framework, imageViewerViewModel) {
    var _vm;

    var init = function (model) {
        $(function () {
            // Set up the initialize for batch summary.
            var container = $('#imageViewerPortlet');
            _vm = new imageViewerViewModel();
            _vm.init(container, model);

            // Wire up the disposal for batch summary.
            container.on("remove", function () {
                _vm.dispose();
                container.off("remove");
            });
        });
    }
    
    // Need this so we can get inside this 'define' method every time we hit the page.
    // See ImageViewerApp.js on how it's called.
    return {
        init: init
    };
});