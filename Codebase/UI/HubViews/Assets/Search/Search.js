﻿define(["jquery", "ko", "frameworkUtils", "accounting"], function ($, ko, framework, accounting) {

  function Search() {
    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };

    var searchExecuteFilter = function () {
      var s = $getE(".accordion");
      $getE(".accordion").accordion("option", "active", 1);
    }

    var setupElementBindings = function () {
      // event callbacks for 
      var execute = $.Callbacks();
      execute.add(searchExecuteFilter);
      // set filter execute callbacks to the container
      framework.getViewContainer(_container).data('executeFilter', execute);

      framework.loadByContainer('/RecHubViews/Search/Filter', $getE('#theFilter'));

      framework.loadByContainer('/RecHubViews/Drilldown/Drilldown', $getE('#theDrilldown'));

      var icons = {
        header: "ui-icon-circle-arrow-e",
        activeHeader: "ui-icon-circle-arrow-s"
      };
      $getE(".accordion").accordion({
        heightStyle: "content",
        icons: icons
      });
      
      _vm.showAccordian(true);
    };
    
    var sViewModel = function (model) {
      var self = this;
      self.labels = model.Labels;

      self.showAccordian = ko.observable(false);;
    }

    function init(myContainerId, model) {
      $(function () {
        _container = $('#' + myContainerId);
        $getE = function (b) { return _container.find(b); };
        _vm = new sViewModel(model);

        ko.applyBindings(_vm, $getE('#searchBase').get(0));

        setupElementBindings();
      });
    }

    return {
      init: init
    }
  }

  function init(myContainerId, model) {
    $(function () {
      var search = new Search();
      search.init(myContainerId, model);
    });
  }

  return {
    init: init
  }
});

