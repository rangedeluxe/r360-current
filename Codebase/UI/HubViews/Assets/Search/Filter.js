﻿define(["jquery", "ko", "frameworkUtils", "accounting"], function ($, ko, framework, accounting) {

  function Filter() {
    var _me = this;
    var _container = {};
    var _vm = {};
    var $getE = function (b) { alert("Not initialized!"); };
    var _conditionId = 0;

    var setupElementBindings = function () {

      ////////////
      _vm.paymentSources([
         { "Id": "1", "Name": "ACH" },
         { "Id": "2", "Name": "Credit Card" },
         { "Id": "3", "Name": "Lockbox" },
         { "Id": "4", "Name": "Wire" }
      ]);
      var s = $getE('.paymentSourceSelect').bootstrapDualListbox({ showfilterinputs: false, moveonselect: false });

      //////////////////
      _vm.paymentTypes([
         { "Id": "1", "Name": "ACH" },
         { "Id": "2", "Name": "Check" },
         { "Id": "3", "Name": "Credit Card" },
         { "Id": "4", "Name": "Wire" }
      ]);
      $getE('.paymentTypeSelect').bootstrapDualListbox({ showfilterinputs: false, moveonselect: false });
      /////////////////
      _vm.ddas([
         { "Id": "1", "Name": "DDA 1" },
         { "Id": "2", "Name": "DDA 2" },
         { "Id": "3", "Name": "DDA 3" },
         { "Id": "4", "Name": "DDA 4" },
         { "Id": "5", "Name": "DDA 5" },
         { "Id": "6", "Name": "DDA 6" },
         { "Id": "7", "Name": "DDA 7" }
      ]);
      $getE('.ddaSelect').bootstrapDualListbox({ showfilterinputs: true, moveonselect: false });

      // create datepicker
      $getE('.depositDate').wfsDatePicker({});
      // create datepicker
      $getE('.depositDateTo').wfsDatePicker({});

      $getE(".entitySelect").wfsSelectbox({
        minimumInputLength: 1,
        width: '275px',
        placeHolder: 'Search entities...',
        callback: entitySelection,
        query: function (query) {
          var data = { results: [] };
          findLabels(_vm.entities(), query.term, data.results);
          query.callback(data);
        }
      });


      $getE(".btnFilter").button().click(function () {
        var execute = framework.getViewContainer(_container).data('executeFilter');
        execute.fire({ 'adam': 'awesome' }, 'test');
      });

      buildTree();
    };

    var buildTree = function () {
      $getE('#jqxTree').jqxTree({
        source: _vm.entities(),
        checkboxes: true,
        hasThreeStates: true,
        allowDrag: false,
        height: '375px', width: '275px'
      });

      $getE('#jqxTree').jqxTree('selectItem', $getE("#jqxTree").find('li:first')[0]);
      $getE('#jqxTree').jqxTree('expandItem', $getE("#jqxTree").find('li:first')[0]);
    }

    var entitySelection = function (value) {
      $getE('#jqxTree').jqxTree('clear');
      //if value is an object... else load all entities
      $getE('#jqxTree').jqxTree('addTo', value ? value['entity'] : _vm.entities());
      $getE('#jqxTree').jqxTree('selectItem', $getE("#jqxTree").find('li:first')[0]);
      $getE('#jqxTree').jqxTree('expandItem', $getE("#jqxTree").find('li:first')[0]);
    };

    function findLabels(arr, term, rvalue) {
      for (var i = 0; i < arr.length; i++) {
        var obj = arr[i];
        var result = obj['label'].search(new RegExp(term, "i"));
        if (result > -1) {
          rvalue.push({ id: obj['id'], text: obj['label'], entity: obj });
        }
        if (obj['items']) findLabels(obj['items'], term, rvalue);
      }
      return rvalue;
    }

    var sViewModel = function (model) {
      var self = this;
      self.labels = model.Labels;

      self.entities = ko.observableArray(model.entities ? model.entities : []);
      self.selectedEntity = ko.observable({ label: 'Select one...' });

      self.paymentSources = ko.observableArray([]);
      self.selectedSources = ko.observableArray([]);

      self.paymentTypes = ko.observableArray([]);
      self.selectedTypes = ko.observableArray([]);

      self.ddas = ko.observableArray([]);
      self.selectedDDAs = ko.observableArray([]);

      self.conditions = ko.observableArray([new Condition()]);


    }
    var Condition = function () {
      var self = this;
      self.id = _conditionId++;
      self.selectedField = ko.observable(null);
      self.fieldValues = [{ Id: 0, Name: 'Batch Id', Type: 0 }, { Id: 1, Name: 'Payment Amount', Type: 1 }, { Id: 2, Name: 'Check/Trace/Ref #', Type: 0 }];
      self.selectedComparison = ko.observable('=');
      self.comparisonNumeric = [{ Id: 0, Name: '=' }, { Id: 1, Name: '<>' }, { Id: 2, Name: '<' }, { Id: 3, Name: '<=' }, { Id: 4, Name: '>' }, { Id: 5, Name: '>=' }];
      self.comparisonString = [{ Id: 0, Name: '=' }, { Id: 1, Name: 'Starts with' }, { Id: 2, Name: 'Contains' }, { Id: 3, Name: 'Ends with' }];
      self.value = ko.observable('');
    }
    ko.bindingHandlers.fieldSelect = {
      init: function (element, valueAccessor, allBindings, cModel, bindingContext) {
        cModel['fieldElement'] = $(element);
        $(element).wfsSelectbox({
          items: cModel.fieldValues,
          width: '175px',
          placeHolder: 'Select...'
        });
        $(element).on("change", function (obj) {
          if (obj.val) {
            if (cModel.selectedField() === null) {
              _vm.conditions.push(new Condition());
            }
            cModel.selectedField(obj.val);
            if (obj.added.Type === 0) {
              cModel['comparisonElement'].wfsSelectbox('setItems', cModel.comparisonString)
            }
            else {
              cModel['comparisonElement'].wfsSelectbox('setItems', cModel.comparisonNumeric)
            };
          } else {
            _vm.conditions.remove(cModel)
          }
        });
      }
    };
    ko.bindingHandlers.conditionSelect = {
      init: function (element, valueAccessor, allBindings, cModel, bindingContext) {
        cModel['comparisonElement'] = $(element);
        $(element).wfsSelectbox({
          items: cModel.comparisonNumeric,
          width: '125px',
          placeHolder: null,
          minimumResultsForSearch: 99,
          allowClear: false
        });
        $(element).on("change", function (obj) {
          cModel.selectedComparison(obj.val);
        });
        $(element).wfsSelectbox('setValue', 0);
      }
    };

    function init(myContainerId, model) {
      $(function () {
        _container = $('#' + myContainerId);
        $getE = function (b) { return _container.find(b); };
        _vm = new sViewModel(model);

        ko.applyBindings(_vm, $getE('#filterBase').get(0));

        setupElementBindings();

        // set 'this' to the container for access 
        framework.getViewContainer(_container).data('filter', this);
      });
    }

    return {
      init: init
    }
  }

  function init(myContainerId, model) {
    var filter = new Filter();
    filter.init(myContainerId, model);
  }

  return {
    init: init
  }
});

