namespace HubViews.DependencyResolution
{
    using Controllers;
    using StructureMap;
    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;
    using System;
    using Wfs.Logging;
    using Wfs.Logging.NLog;
    using WFS.RecHub.HubConfig;
    using WFS.RecHub.OLFServicesClient;
    using WFS.RecHub.R360RaamClient;
    using WFS.RecHub.R360Services.Common;
    using WFS.RecHub.R360Services.R360ServicesServicesClient;
    using WFS.RecHub.R360Shared;
    using WFS.RecHub.R360WebShared.Helpers;
    using WFS.RecHub.R360WebShared.Services;
    using static WFS.RecHub.R360Shared.ConfigHelpers;

    public class DefaultRegistry : Registry
    {

        public DefaultRegistry()
        {
            // Controllers
            For<AdvancedSearchController>().AlwaysUnique();
            For<BatchDetailController>().AlwaysUnique();
            For<BatchSummaryController>().AlwaysUnique();
            For<DDASummaryController>().AlwaysUnique();
            For<DecisioningController>().AlwaysUnique();
            For<HomeController>().AlwaysUnique();
            For<ImageController>().AlwaysUnique();
            For<InvoiceSearchController>().AlwaysUnique();
            For<ManageQueriesController>().AlwaysUnique();
            For<MenuController>().AlwaysUnique();
            For<NotificationsController>().AlwaysUnique();
            For<PaymentSearchController>().AlwaysUnique();
            For<ReceivablesSummaryController>().AlwaysUnique();
            For<SharedController>().AlwaysUnique();
            For<TransactionDetailController>().AlwaysUnique();
            For<PostDepositExceptionsController>().AlwaysUnique();
            For<PayersController>().AlwaysUnique();

            // Dependencies
            For<IWfsLog>()
                .Use<WfsLog>();
            For<IImageServices>()
                .Use<ImageServiceManager>();
            For<IReportServices>()
                .Use<ReportServiceManager>();
            For<IOLFOnlineClient>()
                .Use<OLFOnlineClient>()
                .Ctor<string>("vSiteKey").Is("IPOnline");
            For<IHubConfig>()
                .Use<HubConfigServicesManager>();
            For<IServiceContext>()
                .Use(R360ServiceContext.Current);
            For<IContextContainer>()
                .Use<R360ContextContainer>();
            For<IUserServices>()
                .Use<UserServiceManager>();
            For<IR360Services>()
                .Use<R360ServiceManager>();
            For<ISystemServices>()
                .Use<SystemServiceManager>();
            For<ITemporaryStorageProvider>()
                .Use<SessionTemporaryStorageProvider>();
            For<IExceptionServices>()
                .Use<ExceptionsServiceManager>()
                .SelectConstructor(() => new ExceptionsServiceManager());
            For<IRaamClient>()
                .Use<RaamClient>()
                .Ctor<ConfigHelpers.ILogger>().Is("Logger to Nlog Builder", map => new Logger(
                    x => map.GetInstance<IWfsLog>().Log(WfsLogLevel.Info, x),
                    x => map.GetInstance<IWfsLog>().Warn(x)
                ));
            For<IPostDepositExceptionServices>()
                .Use<PostDepositExceptionsServiceManager>().SelectConstructor(() => new PostDepositExceptionsServiceManager());
            For<ISessionTokenProvider>().Use<SessionTokenProvider>();
            For<IConfiguration>().Use<Configuration>();
            For<IHttpClient>().Use<AuthenticatedHttpClient>();
        }
    }
}