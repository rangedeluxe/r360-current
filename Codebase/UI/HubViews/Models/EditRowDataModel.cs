﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class EditRowDataModel
    {
        public int GlobalBatchId { get; set; }
        public int TransactionId { get; set; }
        public Guid DEItemRowDataID { get; set; }
    }
}