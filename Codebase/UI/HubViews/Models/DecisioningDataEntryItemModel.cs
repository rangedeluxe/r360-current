﻿using System;

namespace HubViews.Models
{
    public class DecisioningDataEntryItemModel : IDataEntryFieldWithValue
    {
        public Guid DEItemRowDataID { get; set; }
        public int GlobalCheckID { get; set; }
        public Guid DEItemFieldDataID { get; set; }
        public Guid DESetupFieldID { get; set; }
        public string Value { get; set; }
        public int FieldDecisionStatus { get; set; }
        public Guid LockboxDecisioningReasonId { get; set; }
        public string DecisioningReasonDesc { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int FieldLength { get; set; }
        public int DataType { get; set; }
        public string Title { get; set; }
    }
}