﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class PostDepositTransactionRequestModel
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public string OrderBy { get; set; }
        public string OrderByDirection { get; set; }
        public string Search { get; set; }
        // From datatables
        public int Draw { get; set; }
    }
}