﻿namespace HubViews.Models
{
    public class AdvancedSearchDownloadAsTextOptions
    {
        public bool ExcludeCommasFromNumeric { get; set; }
        public bool DoNotQuoteFields { get; set; }
    }
}