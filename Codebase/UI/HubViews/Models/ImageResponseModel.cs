﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubViews.Models
{
    public class ImageResponseModel
    {
        public Guid InstanceId { get; set; }

        public bool HasErrors { get; set; }

        public string ErrorMessage { get; set; }

    }
}
