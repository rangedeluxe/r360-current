﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Models
{
    public class PostDepositTransactionResponseModel
    {
        public string EntityBreadcrumb { get; set; }
        public string Workgroup { get; set; }
        public string PaymentSource { get; set; }
        public string PaymentType { get; set; }
        public int BatchId { get; set; }
        public long SourceBatchId { get; set; }
        public int TransactionId { get; set; }
        public int TransactionSequence { get; set; }
        public string DepositDate { get; set; }
        public decimal PaymentTotal { get; set; }
        public IEnumerable<PostDepositExceptionTransactionDto> Transactions { get; set; }
        public IEnumerable<PaymentDto> Payments { get; set; }
        public IEnumerable<StubDto> Stubs { get; set; }
        public IEnumerable<DataEntrySetupDto> DataEntrySetupList { get; set; }
        public TransactionLockDto Lock { get; set; }
        public bool UserCanUnlock { get; set; }
        public bool UserCanEdit { get; set; }
        public bool UserCanOverrideUnlock { get; set; }
    }
}