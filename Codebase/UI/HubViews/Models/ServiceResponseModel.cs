﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class ServiceResponseModel
    {
        public bool Success { get; set; }

        public IEnumerable<string> Errors { get; set; }

        public  object Data { get; set; }
    }
}