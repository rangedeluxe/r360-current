﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class LockTransactionRequestModel
    {
        public int TransactionId { get; set; }
        public long BatchId { get; set; }
        public string DepositDate { get; set; }
    }
}