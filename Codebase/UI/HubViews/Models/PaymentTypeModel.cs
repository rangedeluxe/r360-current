﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class PaymentTypeModel
    {
        public int? PaymentTypeKey { get; set; }
        public string LongName { get; set; }
    }
}