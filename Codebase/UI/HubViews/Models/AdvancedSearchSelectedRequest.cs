﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class AdvancedSearchSelectedRequest
    {
        public List<AdvancedSearchTransactionRequest> Transactions { get; set; }
        public string SelectMode { get; set; }
    }
}