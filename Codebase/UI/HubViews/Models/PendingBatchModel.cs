﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class DecisioningBatchModel
    {
        public int BankId { get; set; }
        public int CustomerId { get; set; }
        public string Entity { get; set; }
        public int WorkgroupId { get; set; }
        public string WorkgroupName { get; set; }
        public string WorkgroupDisplay => WorkgroupId.ToString() + WorkgroupName;
        public string PaymentSource { get; set; }
        public string DepositDate { get; set; }
        public string ProcessDate { get; set; }
        public string DeadLine { get; set; }
        public double? MinutesUntilDeadLine { get; set; }
        public int LocalTimeZoneBias { get; set; }
        public int BatchId { get; set; }
        public int DepositStatus { get; set; }
        public string DepositStatusDisplayName { get; set; }
        public int NumberOfTransactions { get; set; }
        public string SID { get; set; }
        public string User { get; set; }
        public int GlobalBatchId { get; set; }
        public bool UserCanEdit { get; set; }
        public bool UserCanUnlock { get; set; }
        public Guid? BatchResetID { get; set; }
        public bool UserCanLock { get; set; }
        public IEnumerable<DecisioningTransactionModel> Transactions { get; set; }
        public IEnumerable<DecisioningDataEntrySetupModel> DESetupList { get; set; }
        public IEnumerable<int> TransactionIds { get; set; }
    }
}