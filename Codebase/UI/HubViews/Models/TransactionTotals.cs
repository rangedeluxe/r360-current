﻿namespace HubViews.Models {
    public class TransactionTotals {
        public decimal PaymentTotal;
        public decimal StubsTotal;
    };
}