﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class AdvancedSearchStoredQueryModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public AdvancedSearchRequestModel Query { get; set; }
    }
}