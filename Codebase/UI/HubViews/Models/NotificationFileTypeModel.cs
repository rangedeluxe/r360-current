﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class NotificationFileTypeModel
    {
        public int NotificationFileTypeKey { get; set; }
        public string FileTypeDescription { get; set; }
    }
}