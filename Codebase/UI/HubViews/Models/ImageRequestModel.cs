﻿using System;

namespace HubViews.Models
{
    public enum OLFDeliveryMethod
    {
        DetermineBySize,
        Download,
        Stream
    }

    public class ImageRequestModel
    {
        public Guid InstanceId { get; set; }

        public OLFDeliveryMethod DeliveryMethod { get; set; }

        public int NumberOfTries { get; set; }

        public string JobType { get; set; }
    }
}