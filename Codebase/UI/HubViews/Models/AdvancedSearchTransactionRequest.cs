﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class AdvancedSearchTransactionRequest
    {
        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public long BatchId { get; set; }
        public string DepositDate { get; set; }
        public int TransactionId { get; set; }
        public int BatchNumber { get; set; }

    }
}