﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Models
{
    public class AdvancedSearchRequestModel : WFSModel
    {
        public string Workgroup { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public List<AdvancedSearchCriteria> Criteria { get; set; }
        public List<AdvancedSearchWhereClauseDto> SelectFields { get; set; }
        public Guid? QueryId { get; set; }

        // Client-side for datatables pagination.
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }

        // Server-side for datatables Ordering.
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string OrderByDisplayName { get; set; }
        public int PaymentSourceKey { get; set; }
        public  int PaymentTypeKey { get; set; }

        public AdvancedSearchRequestModel Clone()
        {
            var c = new AdvancedSearchRequestModel()
            {
                Workgroup = this.Workgroup,
                DateFrom = this.DateFrom,
                DateTo = this.DateTo,
                Criteria = new List<AdvancedSearchCriteria>(),
                SelectFields = new List<AdvancedSearchWhereClauseDto>(),
                QueryId = this.QueryId,
                draw = this.draw,
                start = this.start,
                length = this.length,
                OrderBy = this.OrderBy,
                OrderDirection = this.OrderDirection,
                OrderByDisplayName = this.OrderByDisplayName,
                PaymentSourceKey = this.PaymentSourceKey,
                PaymentTypeKey = this.PaymentTypeKey
            };
            if (this.Criteria != null)
                foreach (var cr in this.Criteria.Where(x => x != null))
                {
                    c.Criteria.Add(new AdvancedSearchCriteria()
                    {
                        Column = new AdvancedSearchWhereClauseDto()
                        {
                            BatchSourceKey = cr.Column.BatchSourceKey,
                            DataType = cr.Column.DataType,
                            FieldName = cr.Column.FieldName,
                            IsDataEntry = cr.Column.IsDataEntry,
                            IsStandard = cr.Column.IsStandard,
                            Operator = cr.Column.Operator,
                            OrderByName = cr.Column.OrderByName,
                            ReportTitle = cr.Column.ReportTitle,
                            StandardXmlColumnName = cr.Column.StandardXmlColumnName,
                            Table = cr.Column.Table,
                            Value = cr.Column.Value
                        },
                        Operator = cr.Operator,
                        Value = cr.Value
                    });
                }
            if (this.SelectFields != null)
                foreach (var s in this.SelectFields.Where(x => x != null))
                {
                    c.SelectFields.Add(new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = s.BatchSourceKey,
                        DataType = s.DataType,
                        FieldName = s.FieldName,
                        IsDataEntry = s.IsDataEntry,
                        IsStandard = s.IsStandard,
                        Operator = s.Operator,
                        OrderByName = s.OrderByName,
                        ReportTitle = s.ReportTitle,
                        StandardXmlColumnName = s.StandardXmlColumnName,
                        Table = s.Table,
                        Value = s.Value
                    });
                }
            return c;
        }
    }
}