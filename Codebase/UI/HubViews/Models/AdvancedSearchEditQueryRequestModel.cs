﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class AdvancedSearchEditQueryRequestModel : AdvancedSearchRequestModel
    {
        public string QueryName { get; set; }
        public string QueryDescription { get; set; }
        public string DataUrl { get; set; }
        public bool QueryIsDefault { get; set; }
    }
}