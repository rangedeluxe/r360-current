﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class PaymentSourceModel
    {
        public int? PaymentSourceKey { get; set; }
        public string LongName { get; set; }
    }
}