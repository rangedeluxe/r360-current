﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class PostDepositTransactionDetailRequestModel
    {
        public long BatchId { get; set; }
        public int TransactionId { get; set; }
        public long? PreviousBatchId { get; set; }
        public string PreviousDepositDate { get; set; }
        public int? PreviousTransactionId { get; set; }
        public string DepositDate { get; set; }
        public int RecordsTotal { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
    }
}