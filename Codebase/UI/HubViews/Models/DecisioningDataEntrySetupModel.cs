﻿using System;

namespace HubViews.Models
{
    public class DecisioningDataEntrySetupModel : IDataEntryField
    {
        public Guid DESetupFieldID { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int DataType { get; set; }
        public string Title { get; set; }
    }
}