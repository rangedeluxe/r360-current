﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wfs.Logging;
using Wfs.Logging.NLog;
using WFS.RecHub.Common;

namespace HubViews.Models
{
	public class EntityModel
	{
        private readonly IWfsLog Logger;

		public string id { get; set; }

		public string label { get; set; }

		public string icon { get; set; }

		public long value { get; set; }

		public bool isNode { get; set; }

		public IList<EntityModel> items { get; set; }

		public EntityModel()
		{
			items = new List<EntityModel>();
            Logger = new WfsLog();
		}

		internal EntityDTO ToEntityDto()
		{
			if (isNode)
				return null;

			EntityDTO dto = new EntityDTO();
			dto.EntityId = Convert.ToInt32(id);
			foreach (var item in items)
			{
				//we are only including entities (non-nodes)
				if (!item.isNode)
				{
					dto.ChildEntities.Add(item.ToEntityDto());
				}
			}

			return dto;
		}

		private static readonly char[] _separator = new char[] { '|' };
		public WorkgroupIDsDTO ToWorkgroupIDsDto()
		{
			if (!isNode)
				return null;

			try
			{
				WorkgroupIDsDTO dto = new WorkgroupIDsDTO();
				var pieces = id.Split(_separator, 2);
				dto.SiteBankID = int.Parse(pieces[0]);
				dto.SiteWorkgroupID = int.Parse(pieces[1]);
				return dto;
			}
			catch (Exception ex)
			{
				Logger.Log(WfsLogLevel.Error, "Could not parse Workgroup ID: '{0}' - Error: {1}", id, ex.ToString());
				throw;
			}
		}
	}
}