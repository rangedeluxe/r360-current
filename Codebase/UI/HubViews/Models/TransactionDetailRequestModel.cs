﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class TransactionDetailRequestModel
    {
        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public long BatchId { get; set; }
        public DateTime DepositDate { get; set; }
        public string DepositDateString { get; set; }
        public int TransactionID { get; set; }
        public int TransactionSequence { get; set; }
    }
}