﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class DataTablesOrderModel
    {
        public int column { get; set; }
        public string dir { get; set; }
    }
}