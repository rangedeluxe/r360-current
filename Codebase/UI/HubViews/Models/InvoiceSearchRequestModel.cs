﻿
using System.Collections.Generic;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Models
{
    public class InvoiceSearchRequestModel
    {
        public InvoiceSearchRequestModel()
        {
            Criteria = new List<InvoiceSearchCriteriaDto>();
        }

        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Workgroup { get; set; }
        public int? PaymentType { get; set; }
        public int? PaymentSource { get; set; }

        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }

        // From datatables
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }

        public List<InvoiceSearchCriteriaDto> Criteria { get; set; }
    }
}