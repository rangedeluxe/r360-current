﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class NotificationModel
    {
        public DateTime Date { get; set; }
        public string DateString { get; set; }
        public string Id { get; set; }
        public string Message { get; set; }
        public int AttachmentCount { get; set; }
        public bool ViewNotificationIcon { get; set; }
        public int MessageGroup { get; set; }
    }
}