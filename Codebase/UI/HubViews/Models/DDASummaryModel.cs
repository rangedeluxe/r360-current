﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wfs.Logging;
using Wfs.Logging.NLog;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;

namespace HubViews.Models
{
    public class DDASummaryModel
    {

        #region Member Variables

        private readonly IWfsLog Logger;

        public DDASummaryModel()
        {
            Logger = new WfsLog();
        }

        #endregion

        #region Properties

        public int EntityID { get; set; }

		public int SiteBankID { get; set; }

		public int SiteClientAccountID { get; set; }

        public string WorkgroupName { get; set; }

        public string PaymentSource { get; set; }

        public string PaymentType { get; set; }

        public string DDA { get; set; }

        public int PaymentCount { get; set; }

        public decimal PaymentTotal { get; set; }

        public string ParentHierarchy { get; set; }

        #endregion

        public bool GetDDASummary(DateTime depositDate, out List<DDASummaryModel> ddaSummaryData, out List<string> errors)
        {
            errors = null;
            ddaSummaryData = null;
            try
            {
                var manager = new R360ServiceManager();
                var response = manager.GetDDASummary(depositDate);
                if (response.Status != StatusCode.SUCCESS)
                {
                    errors = response.Errors;
                    return false;
                }

                ddaSummaryData = response.Data.Select(o => LoadFromDto(o)).ToList();
                GetHierarchyListings(ddaSummaryData);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(WfsLogLevel.Error, "Exception occurred in GetDDASummary\r\n" + ex.ToString());
                return false;
            }
        }

        private DDASummaryModel LoadFromDto(DDASummaryDataDto summaryData)
        {
            return new DDASummaryModel()
            {
                EntityID = summaryData.EntityID,
                DDA = summaryData.DDA,
				SiteBankID = summaryData.SiteBankID,
				SiteClientAccountID = summaryData.SiteClientAccountID,
                PaymentCount = summaryData.PaymentCount,
                PaymentSource = summaryData.PaymentSource,
                PaymentTotal = summaryData.PaymentTotal,
                PaymentType = summaryData.PaymentType,
                WorkgroupName = summaryData.WorkgroupName
            };
        }

        private void GetHierarchyListings(List<DDASummaryModel> ddaSummaryData)
        {
            Dictionary<int, string> hierarchyList = new Dictionary<int,string>();

            foreach (DDASummaryModel ddaModel in ddaSummaryData)
            {
                if (hierarchyList.Keys.Any(key => key == ddaModel.EntityID))
                    ddaModel.ParentHierarchy = hierarchyList.First(x => x.Key == ddaModel.EntityID).Value;
                else
                {
                    ddaModel.ParentHierarchy = GetParentHierarchyForEntity(ddaModel.EntityID);
                    hierarchyList.Add(ddaModel.EntityID, ddaModel.ParentHierarchy);
                }

            }
        }

        private string GetParentHierarchyForEntity(int entityID)
        {
            var client = new RaamClient(new ConfigHelpers.Logger(
                                                o => Logger.Log(WfsLogLevel.Info, o),
                                                o => Logger.Log(WfsLogLevel.Warn, o)));

            return client.GetEntityBreadcrumb(entityID);
        }
    }
}