﻿using System.Collections.Generic;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Models.PostDepositDetail
{
    public class TransactionWebModel
    {
        public string EntityBreadcrumb { get; set; }
        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public string Workgroup { get; set; }
        public string PaymentSource { get; set; }
        public string PaymentType { get; set; }
        public int BatchId { get; set; }
        public long SourceBatchId { get; set; }
        public int TransactionId { get; set; }
        public int TransactionSequence { get; set; }
        public string DepositDate { get; set; }
        public IEnumerable<PostDepositExceptionTransactionDto> Transactions { get; set; }
        public IEnumerable<PaymentDto> Payments { get; set; }
        public IEnumerable<StubWebModel> Stubs { get; set; }
        public IEnumerable<DocumentDto> Documents { get; set; }
        public IEnumerable<DataEntrySetupDto> DataEntrySetupList { get; set; }
        public TransactionLockDto Lock { get; set; }
        public bool UserCanEdit { get; set; }
        public bool PostDepositPayerRequired { get; set; }
    }
}