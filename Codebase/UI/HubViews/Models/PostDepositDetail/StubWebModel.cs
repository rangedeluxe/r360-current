﻿using System.Collections.Generic;

namespace HubViews.Models.PostDepositDetail
{
    public class StubWebModel
    {
        public int StubSequence { get; set; }
        public int BatchSequence { get; set; }
        public IEnumerable<StubDataEntryWebModel> DataEntryFields { get; set; }
    }
}