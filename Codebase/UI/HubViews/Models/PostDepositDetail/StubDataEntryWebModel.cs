﻿using System.Collections.Generic;

namespace HubViews.Models.PostDepositDetail
{
    public class StubDataEntryWebModel
    {
        public string FieldName { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
        public bool IsEditable { get; set; }
        public bool IsUIAddedField { get; set; }
        public IEnumerable<string> BusinessRuleExceptions { get; set; }
    }
}