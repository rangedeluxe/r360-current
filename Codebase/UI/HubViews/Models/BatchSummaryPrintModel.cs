﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Models
{
    public class BatchSummaryPrintModel
    {
        public bool Success { get; set; }
        public BatchResultModel[] Data { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public IEnumerable<OLPreference> Preferences { get; set; }
        public int SiteCode { get; set; }
        public string Error { get; set; }
    }
}