﻿using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.R360Shared;

namespace HubViews.Models
{
    public class BatchDetailResultsModel
    {
        public bool Success { get; set; }
        public List<BatchDetailModel> Data { get; set; }
        public string Workgroup { get; set; }
        public int? WorkgroupId { get; set; }
        public int? Bank { get; set; }
        public int? AccountSiteCode { get; set; }
        public int? BatchSiteCode { get; set; }
        public string DepositDate { get; set; }
        public long? BatchId { get; set; }
        public long? SourceBatchId { get; set; }
        public int? BatchCue { get; set; }
        public string Batch { get; set; }
        public bool ShowBatchId { get; set; }
        public bool ShowBank { get; set; }
        public bool ShowAccountSiteCode { get; set; }
        public bool ShowBatchSiteCode { get; set; }
        public bool ShowBatchCue { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
        public string Error { get; set; }

        public decimal? BatchTotal
        {
            get { return Data.Sum(item => item.Amount); }
        }
    }
}