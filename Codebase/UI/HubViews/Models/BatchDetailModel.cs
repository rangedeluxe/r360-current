﻿using System;

namespace HubViews.Models
{
    public class BatchDetailModel
    {
        public string Workgroup { get; set; }

        public int? WorkgroupId { get; set; }

        public int? BankId { get; set; }

        public string SiteCode { get; set; }

        public string DepositDateString { get; set; }

        public long? SourceBatchId { get; set; }

        public long? BatchId { get; set; }

        public string BatchNumber { get; set; }

        public string PaymentSource { get; set; }

        public string PaymentType { get; set; }

        public DateTime? DepositDate { get; set; }

        public int? TransactionCount { get; set; }

        public int? PaymentCount { get; set; }

        public int? DocumentCount { get; set; }

        public decimal? TotalAmount { get; set; }

        public string PICSDate { get; set; }

        public DateTime? ProcessingDate { get; set; }
        public string ProcessingDateString { get; set; }

        public int? TransactionID { get; set; }

        public int? TxnSequence { get; set; }

        public decimal? BatchTotal { get; set; }

        public int? BatchSequence { get; set; }

        public string Serial { get; set; }

        public decimal? Amount { get; set; }

        public string RT { get; set; }

        public string Account { get; set; }

        public string RemitterName { get; set; }

        public int? CheckCount { get; set; }

        public int? StubCount { get; set; }

        public int? BatchSiteCode { get; set; }

        public int? BatchCueID { get; set; }

        public string DDA { get; set; }

        public int? ImportTypeKey { get; set; }

        public string BatchSourceShortName { get; set; }

        public string ImportTypeShortName { get; set; }

        public int? AccountSiteCode { get; set; }

        public bool ShowPaymentIcon { get; set; }

        public bool ShowDocumentIcon { get; set; }

        public bool ShowAllIcon { get; set; }

        // The below matches datatables search data model.
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }

        // Set in the server-side controller
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string Search { get; set; }

		public string FileGroup { get; set; }
    }
}