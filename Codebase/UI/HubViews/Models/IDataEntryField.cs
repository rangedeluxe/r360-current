﻿namespace HubViews.Models
{
    public interface IDataEntryField
    {
        int DataType { get; }
        string FieldName { get; }
        string TableName { get; }
    }
}