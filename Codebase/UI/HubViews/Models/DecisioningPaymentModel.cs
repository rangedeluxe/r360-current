﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class DecisioningPaymentModel
    {
        public int GlobalCheckID { get; set; }
        public int CheckSequence { get; set; }
        public int BatchSequence { get; set; }
        public string RT { get; set; }
        public string Account { get; set; }
        public string Serial { get; set; }
        public string TransactionCode { get; set; }
        public decimal Amount { get; set; }
        public int PayeeID { get; set; }
        public string PayeeName { get; set; }
        public string TraceNumber { get; set; }
        public string RemitterName { get; set; }
        public Guid? DecisioningReasonID { get; set; }
        public string DecisioningReasonDesc { get; set; }
        public IEnumerable<DecisioningDataEntryModel> DataEntry { get; set; }
    }
}