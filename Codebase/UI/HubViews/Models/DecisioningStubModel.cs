﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class DecisioningStubModel
    {
        public Guid DEItemRowDataID { get; set; }
        public decimal Amount { get; set; }
        public string Account { get; set; }
        public IEnumerable<DecisioningDataEntryItemModel> DataEntry { get; set; }
    }
}