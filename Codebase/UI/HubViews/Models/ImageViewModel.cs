﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HubViews.Models
{
    public class ImageViewModel
    {
        public int? WorkgroupId { get; set; }

        public int? BankId { get; set; }

        public string DepositDateString { get; set; }

        public string ProcessingDateString { get; set; }

        public long? BatchId { get; set; }

        public string BatchNumber { get; set; }

        public string PICSDate { get; set; }

        public int? TxnSequence { get; set; }

        public int? TransactionId { get; set; }

        public int? BatchSequence { get; set; }

        public int? ImportTypeKey { get; set; }

        public int? Page { get; set; }

        public string ImageFilterOption { get; set; }

        public string ReportName { get; set; }

        public long? SourceBatchId { get; set; }
        public string BatchPaymentSource { get; set; }
        public string FileGroup { get; set; }
        public string ImportTypeShortName { get; set; }
    }
}
