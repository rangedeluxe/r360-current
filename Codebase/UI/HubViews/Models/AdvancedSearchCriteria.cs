﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Models
{
    public class AdvancedSearchCriteria
    {
        public AdvancedSearchWhereClauseDto Column { get; set; }
        public AdvancedSearchWhereClauseOperator Operator { get; set; }
        public string Value { get; set; }
    }
}