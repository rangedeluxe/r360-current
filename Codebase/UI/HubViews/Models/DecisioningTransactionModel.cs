﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace HubViews.Models
{
    public class DecisioningTransactionModel
    {
        public int TransactionId { get; set; }
        public int GlobalBatchId { get; set; }
        public int TransactionSequence { get; set; }
        public decimal Amount { get; set; }
        public string RT { get; set; }
        public string Account { get; set; }
        public string CheckTraceRef { get; set; }
        public string Payer { get; set; } //RemitterName
        public int TransactionStatus { get; set; }
        public int PreviousTransactionStatus { get; set; }

        public DecisioningBatchModel ParentBatch { get; set; }
        public IEnumerable<DecisioningPaymentModel> Payments { get; set; }
        public IEnumerable<DecisioningStubModel> Stubs { get; set; }

        public string TransactionStatusDisplay =>
            TransactionStatus == 3 ? "Accepted"
            : TransactionStatus == 4 ? "Rejected"
            : "Pending Decisioning";
    }
}