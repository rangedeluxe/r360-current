﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class ReceivablesSummaryRequestModel
    {
        public string DepositDateString { get; set; }
        public int? BankId { get; set; }
        public int? WorkgroupId { get; set; }
        public int? EntityId { get; set; }
    }
}