﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using HubViews.Helpers;
using WFS.RecHub.R360Services.Common.DTO;

namespace HubViews.Models
{
    public class TransactionDetailResponseModel
    {
        public int BankID { get; set; }
        public int WorkgroupID { get; set; }
        public string WorkgroupDisplayName { get; set; }
        public string DepositDateString { get; set; }
        public int DepositDateKey { get; set; }
        public int AccountSiteCode { get; set; }
        public int BatchNumber { get; set; }
        public int SourceBatchID { get; set; }
        public int BatchID { get; set; }
        public int BatchSiteCode { get; set; }
        public int TransactionId { get; set; }
        public int BatchCueID { get; set; }
        public int TransactionNumber { get; set; }
        public bool DisplayBatchID { get; set; }
        public bool ViewAllImagesIconVisible { get; set; }

        public int PaymentType { get; set; }
        public int PaymentSource { get; set; }
        public double BatchTotal { get; set; }
        public int PICSDate { get; set; }
        public string BatchSourceShortName { get; set; }
        public string ImportTypeShortName { get; set; }
        public string PaymentTypeShortName { get; set; }
        public string Error { get; set; }
        public IEnumerable<TransactionDetailPayment> Payments { get; set; }
        public IEnumerable<TransactionDetailStubs> Stubs { get; set; }
        public IEnumerable<TransactionDetailDocument> Documents { get; set; }
        public IEnumerable<OLPreference> Preferences { get; set; }
        public IEnumerable<Tuple<int, int>> BatchTransactions { get; set; }
        public bool HasImageRPSReportPermissions { get; set; }
        public string FileGroup { get; set; }

        public string PaymentTotal
        {
            get
            {
                return AmountHelper.FormatAmountUsingNumberPattern(Payments.Sum(p => p.PaymentAmount));
            }
        }
    }
}