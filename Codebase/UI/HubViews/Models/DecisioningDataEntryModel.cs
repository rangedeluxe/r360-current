﻿namespace HubViews.Models
{
    public class DecisioningDataEntryModel : IDataEntryFieldWithValue
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public string Value { get; set; }
        public int DataType { get; set; }
    }
}