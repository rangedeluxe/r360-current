﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class BatchResultModel
    {
        public int BankID { get; set; }
        public int WorkgroupID { get; set; }
        public long BatchID { get; set; }
        public long SourceBatchID { get; set; }
        public string BatchNumber { get; set; }
        public DateTime DepositDate { get; set; }
        public string PaymentSource { get; set; }
        public string PaymentType { get; set; }
        public int TransactionCount { get; set; }
        public int PaymentCount { get; set; }
        public int DocumentCount { get; set; }
        public decimal TotalAmount { get; set; }
        public string DepositDateString { get; set; }
        public int BatchSiteCode { get; set; }
        public bool ShowBatchId { get; set; }
        public bool ShowBathSiteCode { get; set; }
        public string DDA { get; set; }
    }
}