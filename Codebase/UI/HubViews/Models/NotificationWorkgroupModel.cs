﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class NotificationWorkgroupModel
    {
        public int BankId { get; set; }
        public int ClientAccountId { get; set; }
    }
}