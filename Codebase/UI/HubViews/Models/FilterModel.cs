﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFS.RecHub.Common;

namespace HubViews.Models
{
    public class FilterModel
    {
        public DateTime? DepositDate { get; set; }
        public EntityModel Entity { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

    }
}