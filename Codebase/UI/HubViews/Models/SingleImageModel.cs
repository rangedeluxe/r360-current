﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class SingleImageModel
    {
        public long BatchId { get; set; }
        public int BatchSequence { get; set; }
        public string DepositDateString { get; set; }
        public bool IsCheck { get; set; }
    }
}