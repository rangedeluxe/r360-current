﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class BatchSearchModel
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Workgroup { get; set; }
        public int? PaymentType { get; set; }
        public int? PaymentSource { get; set; }

        // The below matches datatables search data model.
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public string search { get; set; }
        public List<DataTablesOrderModel> order { get; set; }

        // Set in the server-side controller
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }

    }
}