﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class NotificationsRequestModel
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string SortBy { get; set; } = "DATE";
        public string SortByDir { get; set; } = "asc";
        public string FileName { get; set; }
        public int FileTypeKey { get; set; }
        public int StartRecord { get; set; } = 1;
        public int RecordsPerPage { get; set; }
        public string Workgroup { get; set; }
        public string NotificationType { get; set; }
        public string NotificationName { get; set; }
        public int UtcOffset { get; set; }
        // The below matches datatables search data model.
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public string search { get; set; }
    }
}