﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class DecisioningCompleteBatchModel
    {
        public Guid batchResetId { get; set; }
        public int globalBatchId { get; set; }
    }
}