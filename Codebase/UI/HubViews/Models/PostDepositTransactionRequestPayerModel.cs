﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace HubViews.Models
{
    public class PostDepositTransactionRequestPayerModel
    {
        [DataMember]
        public long BatchId { get; set; }

        [DataMember]
        public DateTime DepositDate { get; set; }

        [DataMember]
        public string RoutingNumber { get; set; }

        [DataMember]
        public string Account { get; set; }
    }
}