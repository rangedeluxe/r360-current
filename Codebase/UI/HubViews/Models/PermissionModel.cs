﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
  public class PermissionModel
  {
    public string Name { get; set; }

    public string Mode { get; set; }
  }
}