﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HubViews.Models
{
    public class DashboardModel
    {
        public bool CanViewPaymentsChart { get; set; }
        public bool CanViewExceptionPostProcessChart { get; set; }
        public bool CanViewExceptionInProcessChart { get; set; }
        public Dictionary<string, string> Labels { get; set; }
    }
}