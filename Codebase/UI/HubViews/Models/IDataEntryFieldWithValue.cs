﻿namespace HubViews.Models
{
    public interface IDataEntryFieldWithValue : IDataEntryField
    {
        string Value { get; }
    }
}