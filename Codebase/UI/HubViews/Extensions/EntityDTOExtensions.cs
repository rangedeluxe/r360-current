﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.Common;

namespace HubViews.Extensions
{
    public static class EntityDTOExtensions
    {
        public static EntityDTO ToEntityDTO (this EntityDto raamentity)
        {
            return new EntityDTO
            {
                EntityId = raamentity.ID,
                ChildEntities = raamentity.ChildEntities
                    .Select(x => x.ToEntityDTO())
                    .ToList()
            };
        }
    }
}