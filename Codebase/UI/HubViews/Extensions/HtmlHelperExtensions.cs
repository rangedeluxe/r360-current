﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HubViews.Extensions
{
    public static class HtmlHelperExtensions
    {

        public static string SetVisibilityForPermission(this HtmlHelper helper, bool prop)
        {
            return prop ? "" : "hidden";
        }
    }
}