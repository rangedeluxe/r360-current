﻿
using System.Globalization;

namespace HubViews.Helpers
{
    public static class AmountHelper
    {
        public const int NumberPattern = 2;

        public static string FormatAmountUsingNumberPattern(decimal amountIn)
        {
            var numberFormat = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            numberFormat.CurrencyNegativePattern = NumberPattern;
            var amountstring = amountIn.ToString("C2", numberFormat);
            return amountstring;
        }

        public static string FormatAmountUsingNumberPattern(double amountIn)
        {
            var numberFormat = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            numberFormat.CurrencyNegativePattern = NumberPattern;
            var amountstring = amountIn.ToString("C2", numberFormat);
            return amountstring;
        }
    }
}