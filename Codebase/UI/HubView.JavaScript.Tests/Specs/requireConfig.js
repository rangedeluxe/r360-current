﻿require.config({
    baseUrl: '../../../../../../WFS/Framework/Dev/Assets/',
    paths: {
        //Framework scripts
        "jquery": "jquery/js/jquery.min",
        "jqueryui": "jquery/js/jquery-ui.min",
        "respond": "etc/respond",
        "datatables": "datatables/js/jquery.dataTables",
        "datatables-bootstrap": "datatables/js/dataTables.bootstrap",
        'underscore': "underscore/underscore",
        "moment": "moment/moment",
        "jquery.toDictionary": "jquery/js/jquery-toDictionary",
        "jquery.toast": "jquery-toastmessage/js/jquery.toastmessage",
        "jquery.mousewheel": "jquery/js/jquery.mousewheel.min",
        "spinner": "etc/spin.min",
        "select2": "select2/select2",
        "bootstrap": "bootstrap/js/bootstrap",
        "bootstrap-datetimepicker": "bootstrap-datetimepicker/bootstrap-datetimepicker",
        "bootstrap-dialog": "bootstrap-modal/js/bs-modal",
        "frameworkUtils": "framework/JS/frameworkUtils",
        "knockout": "knockout/knockout",
        'accounting': 'etc/accounting',
        "wfs-datatypes": "wfs/wfs-datatypes",
        "wfs-datepicker": "wfs/wfs-datepicker",
        "wfs-dateutil": "wfs/wfs-dateutil",
        "wfs-select": "wfs/wfs-select",

        // Squire
        "Squire": "../../../../Receivables360 Online/Dev/Codebase/UI/HubView.JavaScript.Tests/Specs/Squire",
        "sinon": "../../../../Receivables360 Online/Dev/Codebase/UI/HubView.JavaScript.Tests/Specs/sinon",
        "jasmine-jquery": "../../../../Receivables360 Online/Dev/Codebase/UI/HubView.JavaScript.Tests/Specs/jasmine-jquery",
        "testData": "../../../../Receivables360 Online/Dev/Codebase/UI/HubView.JavaScript.Tests/Specs/testData",
        "batchDetail": "../../../../Receivables360 Online/Dev/Codebase/UI/HubViews/Assets/BatchDetail/ViewModels/BatchDetailViewModel"
    },
    map: {
        // Remap 'ko' to 'knockout'.  R360 uses "ko", but knockout.validation requires "knockout"
        '*': { 'ko': 'knockout' }
    },
    shim: {
        "bootstrap": {
            deps: ["jquery", "jqueryui", "respond"]
        },
        "bootstrap-dialog": {
            deps: ["jquery", "bootstrap"]
        },
        "jqueryui": {
            deps: ["jquery"]
        },
        "jquery.validate": {
            deps: ["jquery"]
        },
        "jquery.toast": {
            deps: ["jquery"]
        },
        "frameworkUtils": {
            deps: ["jquery", "jquery.toast", "jquery.toDictionary", "spinner", "bootstrap-dialog"]
        },
        "moment": {
            deps: ["jquery"]
        },
        "moment.timezone": {
            deps: ["jquery", "moment"]
        },
        "wfs-datatypes": {
            deps: ["accounting"]
        },
        "wfs-dateutil": {
            deps: ["jquery", "moment"]
        },
       "underscore": {
            exports: '_'
        }
    }
});
