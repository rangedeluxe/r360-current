define(["jquery", "frameworkUtils", "ko",  "sinon", "jasmine-jquery", "Squire"], function ($,framework, ko, sinon, jasminejquery, Squire) {
    "use strict";

    describe("BatchTests", function () {

        var injector;

        beforeEach(function (done) {
            require(["Squire"], function (Squire) {
                injector = new Squire();
                done();
            });
            //var path = jasmine.getFixtures().fixturesPath;
            //loadFixtures('batchDetailView.html');
            setFixtures("<div id='batchDetailPortlet'><table id='batchDetailsTable'></table></div>");
        });
        
        afterEach(function () {
            injector.remove();
        });
        
        describe("batchDetail ", function () {

            it("sums the amout total of the batch", function (done) {

                injector.require(["batchDetail", "frameworkUtils", "ko", 'datatables', 'datatables-bootstrap', 'bootstrap', 'moment', 'wfs-dateutil'],
                    function (batchDetail) {
                        var container;
                        var json = data;
                        var bd = new batchDetail();
                        bd.init(container, json);
                        expect(bd.sum(json.data, "Amount")).toEqual(600);
                        done();
                    },
                    function (error) {
                        done.error(error);
                    });
            });
        });

        var data = {
            Data: {
                success: true,
                data: [
                    {
                        SourceBatchId: 1111,
                        BatchSiteCode: 1,
                        BatchId: 929393,
                        BatchNumber: 9898,
                        DepositDate: "10/01/2015",
                        PICSDate: 20151015,
                        ProcessingDate: "10/01/2015",
                        TransactionID: 1,
                        TxnSequence: 1,
                        BatchSequence: 1,
                        DocumentCount: 1,
                        CheckCount: 1,
                        StubCount: 1,
                        PaymentSource: "Wire",
                        PaymentType: "Check",
                        TotalAmount: 100,
                        Amount: 100,
                        TransactionCount: 2,
                        Workgroup: "2000 - 20",
                        BankId: 999,
                        WorkgroupId: 2000,
                        Serial: 92929292,
                        RT: 10,
                        Account: 2872083028,
                        RemitterName: "Test",
                        BatchCueID: 100,
                        ImportTypeKey: 1,
                        DDA: 10101010,
                        BatchSourceShortName: "Test Batch SOurce",
                        ImportTypeShortName: "Wire"
                    },
                    {
                        SourceBatchId: 1111,
                        BatchSiteCode: 1,
                        BatchId: 929393,
                        BatchNumber: 9898,
                        DepositDate: "10/01/2015",
                        PICSDate: 20151015,
                        ProcessingDate: "10/01/2015",
                        TransactionID: 2,
                        TxnSequence: 2,
                        BatchSequence: 1,
                        DocumentCount: 1,
                        CheckCount: 1,
                        StubCount: 1,
                        PaymentSource: "Wire",
                        PaymentType: "Check",
                        TotalAmount: 500,
                        Amount: 500,
                        TransactionCount: 2,
                        Workgroup: "2000 - 20",
                        BankId: 999,
                        WorkgroupId: 2000,
                        Serial: 92929292,
                        RT: 10,
                        Account: 2872083028,
                        RemitterName: "Test",
                        BatchCueID: 100,
                        ImportTypeKey: 1,
                        DDA: 10101010,
                        BatchSourceShortName: "Test Batch SOurce",
                        ImportTypeShortName: "Wire"
                    }
                ],
                workgroup: "20000 -20",
                workgroupId: 100,
                bank: 999,
                accountSiteCode: 1111,
                batchSiteCode: 2222,
                depositDate: "10/01/2015",
                batchCue: 100,
                batchId: 98988,
                batch: 098098,
                showBank: true,
                showAccountSiteCode: true,
                showBatchSiteCode: true,
                showBatchCue: true,
                recordsTotal: 2,
                recordsFiltered: 2,
                error: ""
            }
        }
    });


});
