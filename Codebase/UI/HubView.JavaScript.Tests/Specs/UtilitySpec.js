﻿define(["underscore", "Squire"], function (_, Squire) {
    "use strict";

    // Test Utility functions from underscore.js
    describe("Utility", function () {

        var injector;
        
        // Initialize context for "Utility" class
        beforeEach(function () {
            injector = new Squire();
        });

        afterEach(function () {
            injector.remove();

        });
        
        // Just to demo some of the expectations
        it("should return true.", function () {
            expect(true).toBe(true);
            expect(true).not.toBe(false);

            var message = 'foo bar';
            expect(message).toEqual('foo bar');

            expect(message).toMatch(/foo bar/);
            expect(message).toMatch('foo bar');

            var obj = {foo: 'foo', bar: null };
            expect(obj.foo).toBeDefined();
            expect(obj.foobar).toBeUndefined();
            expect(obj.bar).toBeNull();

            expect(obj.foo).toBeTruthy();
            expect(obj.bar).toBeFalsy();

            expect(message).toContain('foo');

            var pi = 3.14159265358979311599796;
            expect(pi).toBeGreaterThan(3);
            expect(pi).toBeLessThan(4);
            expect(pi).toBeCloseTo(3.1415, 0.1);

            //expect(func).toThrow()

        });

        it("min should be less than max.", function () {
            var array = _.range(1000);
            var min = Math.pow(2, 31);
            var max = Math.pow(2, 62);
            expect(min).toBeLessThan(_.random(min, max));
        });

        it('should return the data and time.', function () {
            var diff = _.now() - new Date().getTime();
            expect(diff <= 0 && diff > -5).toBeTruthy();
        });
        
        it('should have all ids be unique.', function () {
            var ids = [], i = 0;
            while (i++ < 100) ids.push(_.uniqueId());
            expect(_.uniq(ids).length).toEqual(ids.length);
        });

        it('should escape a value', function () {
            var string = 'Curly & Moe';
            expect(_.escape(null)).toEqual('');
            expect(_.escape(string)).toEqual('Curly &amp; Moe');
        });

        it('should unescape a value', function () {
            var string = 'Curly & Moe'; 
            expect(_.unescape(null)).toEqual('');
            expect(_.unescape(_.escape(string))).toEqual(string);
            expect(_.unescape(string)).toEqual(string);
        });

        it('should return primitive values', function () {
            var obj = { w: '', x: 'x', y: function () { return this.x; } };
            expect(_.result(obj, 'w')).toEqual('');
            expect(_.result(obj, 'x')).toEqual('x');
            expect(_.result(obj, 'y')).toEqual('x');
            expect(_.result(obj, 'z')).toEqual(void 0);
            expect(_.result(null, 'x')).toEqual(void 0);
        });

        it('should retun correct item for _.first', function () {
            expect(_.first([1, 2, 3])).toEqual(1);
            expect(_([1, 2, 3]).first()).toEqual(1);
            var result = (function () { return _.first(arguments); }(4, 3, 2, 1));
            expect(result).toEqual(4);
            expect(_.first(null)).toEqual(void 0);
        });

        it('should return union arrays.', function () {
            var result = _.union([1, 2, 3], [2, 30, 1], [1, 40]);
            expect(result).toEqual([1, 2, 3, 30, 40]);
        });

        it('should return difference of arrays.', function () {
            var result = _.difference([1, 2, 3], [2, 30, 40]);
            expect(result).toEqual([1, 3]);
        });
    });
});
