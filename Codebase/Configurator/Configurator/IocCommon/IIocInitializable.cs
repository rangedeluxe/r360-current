﻿namespace Configurator.IocCommon
{
    public interface IIocInitializable
    {
        void Initialize();
    }
}
