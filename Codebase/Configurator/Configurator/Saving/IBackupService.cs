﻿using System.IO;

namespace Configurator.Saving
{
    public interface IBackupService
    {
        void BackUp(FileStream sourceStream);
    }
}