﻿using System;
using System.IO;
using Configurator.Io;

namespace Configurator.Saving
{
    public class BackupService : IBackupService
    {
        public BackupService(DateTime backupDateTime, string baseDirectory)
        {
            BaseDirectory = baseDirectory;
            BackupBaseDirectory = @"Backups\" + backupDateTime.ToString("yyyy-MM-dd_HH.mm.ss");
        }

        private string BackupBaseDirectory { get; }
        private string BaseDirectory { get; }

        public void BackUp(FileStream sourceStream)
        {
            var backupFilePath = GetBackupFilePath(sourceStream);

            EnsureFileDirectoryExists(backupFilePath);
            CopyFileContents(sourceStream, backupFilePath);
            CopyFileTimestamp(sourceStream.Name, backupFilePath);
        }
        private string GetBackupFilePath(FileStream sourceStream)
        {
            var sourceFilePath = sourceStream.Name;
            var relativePath = PathUtils.GetRelativePath(BaseDirectory, sourceFilePath);
            if (relativePath.StartsWith(".."))
                throw new InvalidOperationException($"Cannot back up files outside '{BaseDirectory}'");
            return Path.Combine(BackupBaseDirectory, relativePath);
        }
        private static void EnsureFileDirectoryExists(string filePath)
        {
            var backupDirectory = Path.GetDirectoryName(filePath);
            if (backupDirectory != null)
                Directory.CreateDirectory(backupDirectory);
        }
        private static void CopyFileContents(Stream sourceStream, string targetFilePath)
        {
            var originalPosition = sourceStream.Position;
            try
            {
                sourceStream.Position = 0;
                using (var targetStream = new FileStream(targetFilePath, FileMode.Create))
                {
                    sourceStream.CopyTo(targetStream);
                }
            }
            finally
            {
                sourceStream.Position = originalPosition;
            }
        }
        private void CopyFileTimestamp(string sourceFilePath, string targetFilePath)
        {
            var targetFile = new FileInfo(targetFilePath);
            var sourceFile = new FileInfo(sourceFilePath);
            targetFile.LastWriteTime = sourceFile.LastWriteTime;
        }
    }
}
