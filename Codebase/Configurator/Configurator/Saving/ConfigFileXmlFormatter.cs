﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Configurator.Saving
{
    public class ConfigFileXmlFormatter
    {
        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding => Encoding.UTF8;
        }

        private class FormattedXmlWriterDecorator : XmlWriter
        {
            private readonly List<string> _currentNestedElements = new List<string>();
            private readonly XmlWriter _inner;
            private readonly Action<string> _writeRawText;

            public FormattedXmlWriterDecorator(XmlWriter inner, Action<string> writeRawText)
            {
                _inner = inner;
                _writeRawText = writeRawText;
            }

            public override WriteState WriteState => _inner.WriteState;

            public override void WriteStartDocument() => _inner.WriteStartDocument();
            public override void WriteStartDocument(bool standalone) => _inner.WriteStartDocument(standalone);
            public override void WriteEndDocument() => _inner.WriteEndDocument();
            public override void WriteDocType(string name, string pubid, string sysid, string subset) =>
                _inner.WriteDocType(name, pubid, sysid, subset);
            public override void WriteStartElement(string prefix, string localName, string ns)
            {
                _inner.WriteStartElement(prefix, localName, ns);
                _currentNestedElements.Add(localName);
            }
            public override void WriteEndElement()
            {
                _inner.WriteEndElement();
                if (_currentNestedElements.Any())
                    _currentNestedElements.RemoveAt(_currentNestedElements.Count - 1);
            }
            public override void WriteFullEndElement()
            {
                _inner.WriteFullEndElement();
                if (_currentNestedElements.Any())
                    _currentNestedElements.RemoveAt(_currentNestedElements.Count - 1);
            }
            public override void WriteStartAttribute(string prefix, string localName, string ns)
            {
                if (_currentNestedElements.LastOrDefault() == "add" &&
                    _currentNestedElements.Contains("DataImportConfigSections"))
                {
                    _inner.Flush();
                    // _inner.WriteStartAttribute will add one more space before the attribute
                    _writeRawText("\r\n" + new string(' ', _currentNestedElements.Count * 2 - 1));
                }
                _inner.WriteStartAttribute(prefix, localName, ns);
            }
            public override void WriteEndAttribute() => _inner.WriteEndAttribute();
            public override void WriteCData(string text) => _inner.WriteCData(text);
            public override void WriteComment(string text) => _inner.WriteComment(text);
            public override void WriteProcessingInstruction(string name, string text) =>
                _inner.WriteProcessingInstruction(name, text);
            public override void WriteEntityRef(string name) => _inner.WriteEntityRef(name);
            public override void WriteCharEntity(char ch) => _inner.WriteCharEntity(ch);
            public override void WriteWhitespace(string ws) => _inner.WriteWhitespace(ws);
            public override void WriteString(string text) => _inner.WriteString(text);
            public override void WriteSurrogateCharEntity(char lowChar, char highChar) =>
                _inner.WriteSurrogateCharEntity(lowChar, highChar);
            public override void WriteChars(char[] buffer, int index, int count) =>
                _inner.WriteChars(buffer, index, count);
            public override void WriteRaw(char[] buffer, int index, int count) => _inner.WriteRaw(buffer, index, count);
            public override void WriteRaw(string data) => _inner.WriteRaw(data);
            public override void WriteBase64(byte[] buffer, int index, int count) =>
                _inner.WriteBase64(buffer, index, count);
            public override void Flush() => _inner.Flush();
            public override string LookupPrefix(string ns) => _inner.LookupPrefix(ns);
        }

        public string Format(XElement element)
        {
            var textWriter = new Utf8StringWriter();
            var settings = new XmlWriterSettings { Indent = true };
            using (var baseWriter = XmlWriter.Create(textWriter, settings))
            {
                var formattedWriter = new FormattedXmlWriterDecorator(baseWriter, text => textWriter.Write(text));
                element.Save(formattedWriter);
            }
            var preliminaryResult = textWriter.ToString();
            var withCommentsDetabbed = preliminaryResult.Replace("\t", "  ");
            return withCommentsDetabbed;
        }
    }
}
