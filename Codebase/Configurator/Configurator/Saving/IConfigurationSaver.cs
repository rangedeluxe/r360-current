﻿using Configurator.ConfigurationModels;

namespace Configurator.Saving
{
    public interface IConfigurationSaver
    {
        void Save(ConfigurationModel configurationModel);
    }
}
