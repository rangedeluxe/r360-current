﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Configurator.ConfigurationModels;
using Configurator.Loading;
using WFS.RecHub.Common.Crypto;

namespace Configurator.Saving
{
    public class DitClientConfigurationSaver : IConfigurationSaver
    {
        public DitClientConfigurationSaver(IBackupService backupService)
        {
            BackupService = backupService;
        }

        private IBackupService BackupService { get; }

        public void Save(ConfigurationModel configurationModel)
        {
            var formatter = new ConfigFileXmlFormatter();

            using (var stream = new FileStream(ConfigFilePaths.DitClient, FileMode.OpenOrCreate, FileAccess.ReadWrite,
                FileShare.None))
            {
                // We've successfully opened the file for writing.
                // But before actually saving anything, let's make a backup.
                BackupService.BackUp(stream);

                var root = XElement.Load(stream);
                SetAppSettingsValue(root, DitClientAppSettings.LoggingDepth,
                    configurationModel.DitClientLoggingDepth.ToString(CultureInfo.InvariantCulture));
                SetAppSettingsValue(root, DitClientAppSettings.LogonName, Encrypt(configurationModel.LogonName));
                SetAppSettingsValue(root, DitClientAppSettings.Password, Encrypt(configurationModel.Password));

                stream.Position = 0;
                using (var writer = new StreamWriter(stream, Encoding.UTF8, 4096, leaveOpen: true))
                {
                    writer.Write(formatter.Format(root));
                }
                stream.SetLength(stream.Position);
            }
        }

        private string Encrypt(string value)
        {
            try
            {
                cCrypto3DES cryp3DES = new cCrypto3DES();
                string strEncryptedValue;
                if (!cryp3DES.Encrypt(value, out strEncryptedValue))
                {
                    throw new Exception(cryp3DES.LastException.Message);
                }
                return strEncryptedValue;
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to encrypt string " + ex.Message);
            }
        }

        private static void SetAppSettingsValue(XElement root, string name, string value)
        {
            var attribute = root.Element("appSettings")
                ?.Elements("add")
                .FirstOrDefault(e => e.Attribute("key")?.Value == name)
                ?.Attribute("value");
            if (attribute == null)
                throw new InvalidOperationException("Cannot find appSetting {name}");
            attribute.Value = value;
        }
    }
}
