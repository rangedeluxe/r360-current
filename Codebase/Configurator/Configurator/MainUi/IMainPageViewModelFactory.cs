﻿using System;
using Configurator.ConfigurationModels;
using Configurator.ViewModelCommon;

namespace Configurator.MainUi
{
    public interface IMainPageViewModelFactory
    {
        IMainPage CreateEditPageViewModel(ConfigurationModel configurationModel);
        IMainPage CreateStartupErrorPageViewModel(Exception ex);
    }
}