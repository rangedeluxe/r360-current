﻿using System;
using Configurator.IocCommon;
using Configurator.Loading;
using Configurator.ViewModelCommon;

namespace Configurator.MainUi
{
    public class MainWindowViewModel : IIocInitializable
    {
        public MainWindowViewModel(IConfigurationLoader loader, IMainPageViewModelFactory viewModelFactory)
        {
            Loader = loader;
            ViewModelFactory = viewModelFactory;
        }

        public IMainPage CurrentPage { get; private set; }
        private IConfigurationLoader Loader { get; }
        private IMainPageViewModelFactory ViewModelFactory { get; }

        public void Initialize()
        {
            try
            {
                var configurationModel = Loader.Load();
                CurrentPage = ViewModelFactory.CreateEditPageViewModel(configurationModel);
            }
            catch (Exception ex)
            {
                CurrentPage = ViewModelFactory.CreateStartupErrorPageViewModel(ex);
            }
        }
    }
}
