﻿using System;
using Configurator.ConfigurationModels;
using Configurator.EditPage;
using Configurator.MainUi;
using Configurator.StartupErrorPage;
using Configurator.ViewModelCommon;
using StructureMap;

namespace Configurator.Startup
{
    public class StructureMapMainPageViewModelFactory : IMainPageViewModelFactory
    {
        public StructureMapMainPageViewModelFactory(IContainer container)
        {
            Container = container;
        }

        private IContainer Container { get; }

        public IMainPage CreateEditPageViewModel(ConfigurationModel configurationModel)
        {
            return Container.With(configurationModel).GetInstance<EditPageViewModel>();
        }
        public IMainPage CreateStartupErrorPageViewModel(Exception ex)
        {
            return Container.With(ex).GetInstance<StartupErrorPageViewModel>();
        }
    }
}
