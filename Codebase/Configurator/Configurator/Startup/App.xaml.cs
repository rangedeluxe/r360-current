﻿using System;
using System.Windows;
using Configurator.MainUi;
using StructureMap;

namespace Configurator.Startup
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // The configurator will be installed under WFSApps. Set the current directory to the
            // EXE directory, so that relative paths like "\WFSApps" will work as desired.
            Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;

            var container = new Container(new ConfiguratorIocRegistry());
            var mainWindow = container.GetInstance<MainWindow>();
            mainWindow.Show();
        }
    }
}
