﻿using System;
using System.Collections.Generic;
using Configurator.IocCommon;
using StructureMap.Building.Interception;
using StructureMap.Pipeline;

namespace Configurator.Startup
{
    /// <summary>
    /// StructureMap policy: on activation, call IIocInitializable.Initialize() on any type that implements it.
    /// </summary>
    public class IocInitializableInterceptionPolicy : IInterceptorPolicy
    {
        public string Description => "Configurator interception policy";

        public IEnumerable<IInterceptor> DetermineInterceptors(Type pluginType, Instance instance)
        {
            if (typeof(IIocInitializable).IsAssignableFrom(pluginType))
                yield return new ActivatorInterceptor<IIocInitializable>(obj => obj.Initialize());
        }
    }
}