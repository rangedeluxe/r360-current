﻿using System;
using Configurator.Loading;
using Configurator.MainUi;
using Configurator.Saving;
using Configurator.ViewModelCommon;
using StructureMap;

namespace Configurator.Startup
{
    public class ConfiguratorIocRegistry : Registry
    {
        public ConfiguratorIocRegistry()
        {
            Policies.Interceptors(new IocInitializableInterceptionPolicy());

            For<MainWindow>().Singleton();
            For<IBackupService>().Use(context => new BackupService(DateTime.Now, ConfigFilePaths.BaseDirectory));
            For<IConfigurationLoader>().Use<ClientConfigurationLoader>();
            For<IConfigurationSaver>().Use<DitClientConfigurationSaver>();
            For<IMessageBoxService>().Use(context => new MessageBoxService(context.GetInstance<MainWindow>));
            For<IMainPageViewModelFactory>().Use<StructureMapMainPageViewModelFactory>();
        }
    }
}
