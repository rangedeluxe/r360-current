﻿using System;
using System.Windows;

namespace Configurator.ViewModelCommon
{
    public class MessageBoxService : IMessageBoxService
    {
        public MessageBoxService(Func<Window> getOwner)
        {
            GetOwner = getOwner;
        }

        private Func<Window> GetOwner { get; }

        public void ShowMessage(string message)
        {
            var owner = GetOwner();
            MessageBox.Show(owner, message, owner.Title);
        }
    }
}