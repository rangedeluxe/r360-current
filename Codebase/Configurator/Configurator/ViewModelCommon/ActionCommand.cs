﻿using System;
using System.Windows.Input;
#pragma warning disable CS0067 // The event '...' is never used

namespace Configurator.ViewModelCommon
{
    public class ActionCommand : ICommand
    {
        private readonly Action _action;
        private readonly Func<bool> _canExecute = () => true;

        public ActionCommand(Action action)
        {
            _action = action;
        }
        public ActionCommand(Action action, Func<bool> canExecute)
        {
            _action = action;
            _canExecute = canExecute;
        }
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return _canExecute();
        }
        public void Execute(object parameter)
        {
            _action();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

    }
}
