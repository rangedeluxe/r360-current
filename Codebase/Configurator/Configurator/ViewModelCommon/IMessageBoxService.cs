﻿namespace Configurator.ViewModelCommon
{
    public interface IMessageBoxService
    {
        void ShowMessage(string message);
    }
}