﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Configurator.Io
{
    public static class PathUtils
    {
        /// <summary>
        /// Converts an absolute path to a relative path.
        /// </summary>
        /// <param name="fromPath">The base path, to make <see cref="toPath"/> relative to.</param>
        /// <param name="toPath">The path to make relative.</param>
        /// <returns>The relative path.</returns>
        /// <remarks>
        /// Source: https://stackoverflow.com/a/485516/87399
        /// </remarks>
        public static string GetRelativePath(string fromPath, string toPath)
        {
            // Make sure both paths are fully qualified.
            fromPath = Path.GetFullPath(fromPath);
            toPath = Path.GetFullPath(toPath);

            var fromAttr = GetPathAttribute(fromPath);
            var toAttr = GetPathAttribute(toPath);

            var path = new StringBuilder(260); // MAX_PATH
            if (PathRelativePathTo(path, fromPath, fromAttr, toPath, toAttr) == 0)
            {
                throw new ArgumentException("Paths must have a common prefix");
            }
            return path.ToString();
        }

        private static int GetPathAttribute(string path)
        {
            var di = new DirectoryInfo(path);
            if (di.Exists)
            {
                return FILE_ATTRIBUTE_DIRECTORY;
            }

            var fi = new FileInfo(path);
            if (fi.Exists)
            {
                return FILE_ATTRIBUTE_NORMAL;
            }

            throw new FileNotFoundException();
        }

        // ReSharper disable InconsistentNaming
        private const int FILE_ATTRIBUTE_DIRECTORY = 0x10;
        private const int FILE_ATTRIBUTE_NORMAL = 0x80;
        // ReSharper restore InconsistentNaming

        [DllImport("shlwapi.dll", SetLastError = true)]
        private static extern int PathRelativePathTo(StringBuilder pszPath,
            string pszFrom, int dwAttrFrom, string pszTo, int dwAttrTo);
    }
}
