﻿using System;
using System.Windows.Input;
using Configurator.ConfigurationModels;
using Configurator.Saving;
using Configurator.ViewModelCommon;
using JetBrains.Annotations;

namespace Configurator.EditPage
{
    public class EditPageViewModel : IMainPage
    {
        public EditPageViewModel([NotNull] ConfigurationModel configurationModel,
            [NotNull] IConfigurationSaver configurationSaver, [NotNull] IMessageBoxService messageBoxService)
        {
            if (configurationModel == null)
                throw new ArgumentNullException(nameof(configurationModel));
            if (configurationSaver == null)
                throw new ArgumentNullException(nameof(configurationSaver));
            if (messageBoxService == null)
                throw new ArgumentNullException(nameof(messageBoxService));

            Fields = new EditFieldsViewModel(configurationModel);
            Fields.ConfigurationModel.PropertyChanged += ConfigurationModel_PropertyChanged;
            SaveCommand = new ActionCommand(action: () =>
                {
                    configurationSaver.Save(configurationModel);
                    messageBoxService.ShowMessage("Saved.");
                },
                canExecute: () => (!string.IsNullOrWhiteSpace(configurationModel.Password) &&
                                   !string.IsNullOrWhiteSpace(configurationModel.LogonName)));
        }

        private void ConfigurationModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var actCmd = (ActionCommand)SaveCommand;
            actCmd.RaiseCanExecuteChanged();
        }

        [NotNull]
        public EditFieldsViewModel Fields { get; }
        [NotNull]
        public ICommand SaveCommand { get; }
    }
}