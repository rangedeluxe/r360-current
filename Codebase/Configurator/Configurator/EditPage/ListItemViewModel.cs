﻿using System.Windows;
using JetBrains.Annotations;

namespace Configurator.EditPage
{
    public class ListItemViewModel<T>
    {
        public ListItemViewModel(T value, string title, string subtitle = "")
        {
            Title = title;
            Subtitle = subtitle;
            Value = value;
        }

        public string Subtitle { get; }
        public Visibility SubtitleVisibility => string.IsNullOrEmpty(Subtitle)
            ? Visibility.Collapsed
            : Visibility.Visible;
        public string Title { get; }
        [UsedImplicitly] // ReSharper doesn't seem to recognize SelectedValuePath
        public T Value { get; }
    }
}
