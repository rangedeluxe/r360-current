﻿using System;
using System.Collections.Generic;
using Configurator.ConfigurationModels;
using JetBrains.Annotations;

namespace Configurator.EditPage
{
    public class EditFieldsViewModel
    {
        public EditFieldsViewModel([NotNull] ConfigurationModel configurationModel)
        {
            ConfigurationModel = configurationModel ?? throw new ArgumentNullException(nameof(configurationModel));

            LoggingDepthListItems = new[]
            {
                new ListItemViewModel<int>(0, "Essential", "recommended for production"),
                new ListItemViewModel<int>(1, "Verbose", "additional detail"),
                new ListItemViewModel<int>(2, "Debug", "highest level of detail"),
            };
        }

        public ConfigurationModel ConfigurationModel { get; }
        public IReadOnlyList<ListItemViewModel<int>> LoggingDepthListItems { get; }
    }
}