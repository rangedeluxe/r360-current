namespace Configurator.ConfigurationModels
{
    public class DitClientAppSettings
    {
        public static string LoggingDepth = "LoggingDepth";
        public static string LogonName = "LogonName";
        public static string Password = "Password";
    }
}