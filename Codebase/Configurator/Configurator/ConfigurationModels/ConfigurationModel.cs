﻿using System.ComponentModel;
using Configurator.ViewModelCommon;

namespace Configurator.ConfigurationModels
{
    public class ConfigurationModel : PropertyChangedBase, IDataErrorInfo
    {
        private int _ditClientLoggingDepth;
        private string _logonName;
        private string _password;

        public int DitClientLoggingDepth
        {
            get => _ditClientLoggingDepth;
            set => SetProperty(ref _ditClientLoggingDepth, value);
        }

        public string LogonName
        {
            get => _logonName;
            set => SetProperty(ref _logonName, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        #region IDataErrorInfo

        public string this[string columnName]
        {
            get
            {
                switch (columnName)
                {
                    case "Password":
                        return string.IsNullOrWhiteSpace(Password)
                            ? "Password cannot be enpty"
                            : null;
                    case "LogonName":
                        return string.IsNullOrWhiteSpace(LogonName)
                            ? "Password cannot be enpty"
                            : null;
                    default:
                        return null;
                }
            }
        }

        public string Error => null;

        #endregion //IDataErrorInfo

    }
}