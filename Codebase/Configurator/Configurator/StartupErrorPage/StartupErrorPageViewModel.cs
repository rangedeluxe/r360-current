﻿using System;
using Configurator.ViewModelCommon;

namespace Configurator.StartupErrorPage
{
    public class StartupErrorPageViewModel : IMainPage
    {
        public StartupErrorPageViewModel(Exception exception)
        {
            ErrorMessage = exception.Message;
        }

        public string ErrorMessage { get; }
    }
}