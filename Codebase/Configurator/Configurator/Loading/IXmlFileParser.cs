﻿using System.Xml.Linq;
using Configurator.ConfigurationModels;

namespace Configurator.Loading
{
    public interface IXmlFileParser
    {
        void Parse(XElement root, ConfigurationModel configurationModel);
    }
}