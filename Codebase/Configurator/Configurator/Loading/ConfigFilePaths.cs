﻿namespace Configurator.Loading
{
    public static class ConfigFilePaths
    {
        public const string BaseDirectory = @"\WFSApps";
        public const string DitClient = @"\WFSApps\RecHub\bin\DataImportClientSvc.exe.config";
    }
}