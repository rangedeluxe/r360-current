﻿using System.IO;
using System.Xml.Linq;
using Configurator.ConfigurationModels;

namespace Configurator.Loading
{
    public class ClientConfigurationLoader : IConfigurationLoader
    {
        public ConfigurationModel Load()
        {
            var configurationModel = new ConfigurationModel();

            ParseFile(ConfigFilePaths.DitClient, new DitClientConfigFileParser(), configurationModel);

            return configurationModel;
        }
        private void ParseFile(string filePath, IXmlFileParser parser, ConfigurationModel configurationModel)
        {
            // Make sure to open the file as read/write, so we fail early if we won't be able to save later.
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite))
            {
                var root = XElement.Load(stream);
                parser.Parse(root, configurationModel);
            }
        }
    }
}
