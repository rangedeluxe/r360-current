﻿using Configurator.ConfigurationModels;

namespace Configurator.Loading
{
    public interface IConfigurationLoader
    {
        ConfigurationModel Load();
    }
}