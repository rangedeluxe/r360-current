﻿using System;
using System.Linq;
using System.Xml.Linq;
using Configurator.ConfigurationModels;
using WFS.RecHub.Common.Crypto;

namespace Configurator.Loading
{
    public class DitClientConfigFileParser : IXmlFileParser
    {
        public void Parse(XElement root, ConfigurationModel configurationModel)
        {
            var intValue = GetAppSettingInt(root, DitClientAppSettings.LoggingDepth);
            if (intValue < 0 || intValue > 2)
                throw new InvalidOperationException($"'{intValue}' is not a valid value for LoggingDepth");
            configurationModel.DitClientLoggingDepth = intValue;

            configurationModel.LogonName = Decrypt(GetAppSetting(root, DitClientAppSettings.LogonName));
            configurationModel.Password = Decrypt(GetAppSetting(root, DitClientAppSettings.Password));
        }

        private static int GetAppSettingInt(XElement root, string name)
        {
            var value = GetAppSetting(root, name);
            if (!int.TryParse(value, out var intValue))
                throw new InvalidOperationException($"Invalid value for the {name} app setting");
            return intValue;
        }

        private static string GetAppSetting(XElement root, string name)
        {
            var appSettings = root.Element("appSettings");
            if (appSettings == null)
                throw new InvalidOperationException("Could not find appSettings element");

            var element = appSettings.Elements().First(e => e.Attribute("key")?.Value == name);
            if (element == null)
                throw new InvalidOperationException($"Could not find  {name} app-setting element");

            var value = element.Attribute("value")?.Value;
            if (value == null)
                throw new InvalidOperationException($"Missing 'value' attribute for the {name} app setting");
            return value;
        }
        private string Decrypt(string strEncryptedValue)
        {
            string strDecryptedValue;
            try
            {
                cCrypto3DES cryp3DES = new cCrypto3DES();

                if (!cryp3DES.Decrypt(strEncryptedValue, out strDecryptedValue))
                {
                    strDecryptedValue = string.Empty;
                }
                return strDecryptedValue;
            }
            catch (Exception)
            {
                strDecryptedValue = string.Empty;
            }
            return strDecryptedValue;
        }
    }
}
