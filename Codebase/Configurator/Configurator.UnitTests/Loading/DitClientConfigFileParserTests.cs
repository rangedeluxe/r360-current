﻿using System.Xml.Linq;
using Configurator.ConfigurationModels;
using NUnit.Framework;

namespace Configurator.Loading
{
    public class DitClientConfigFileParserTests
    {
        [TestCase(0)]
        [TestCase(2)]
        public void ReadsLoggingDepth(int loggingDepth)
        {
            var configurationModel = new ConfigurationModel();
            var parser = new DitClientConfigFileParser();
            var xml = new XElement("configuration",
                new XElement("appSettings",
                    new XElement("add",
                        new XAttribute("key", "LoggingDepth"),
                        new XAttribute("value", loggingDepth)),
                    new XElement("add",
                        new XAttribute("key", "LogonName"),
                        new XAttribute("value", "sa")),
                    new XElement("add",
                        new XAttribute("key", "Password"),
                        new XAttribute("value", "qwert"))));

            parser.Parse(xml, configurationModel);

            Assert.That(configurationModel.DitClientLoggingDepth, Is.EqualTo(loggingDepth));
        }
    }
}
