﻿using System;
using Configurator.ConfigurationModels;
using Configurator.Loading;
using Configurator.MainUi;
using Configurator.ViewModelCommon;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;

namespace Configurator.Startup
{
    public class MainWindowViewModelTests
    {
        [Test]
        public void CurrentPage_IsInitiallyNull()
        {
            var viewModel = new MainWindowViewModel(Substitute.For<IConfigurationLoader>(),
                Substitute.For<IMainPageViewModelFactory>());
            Assert.That(viewModel.CurrentPage, Is.Null);
        }
        [Test]
        public void Load_IfLoaderReturnsNoErrors_SetsCurrentPageToEditPage()
        {
            var configurationModel = new ConfigurationModel();
            var loader = Substitute.For<IConfigurationLoader>();
            loader.Load().Returns(configurationModel);

            var editPageViewModel = Substitute.For<IMainPage>();
            var viewModelFactory = Substitute.For<IMainPageViewModelFactory>();
            viewModelFactory.CreateEditPageViewModel(configurationModel).Returns(editPageViewModel);

            var viewModel = new MainWindowViewModel(loader, viewModelFactory);
            viewModel.Initialize();

            Assert.That(viewModel.CurrentPage, Is.SameAs(editPageViewModel));
        }
        [Test]
        public void Load_IfLoaderReturnsErrors_SetsCurrentPageToErrorPage()
        {
            var exception = new InvalidOperationException();
            var loader = Substitute.For<IConfigurationLoader>();
            loader.Load().Throws(exception);

            var errorPageViewModel = Substitute.For<IMainPage>();
            var viewModelFactory = Substitute.For<IMainPageViewModelFactory>();
            viewModelFactory.CreateStartupErrorPageViewModel(exception).Returns(errorPageViewModel);

            var viewModel = new MainWindowViewModel(loader, viewModelFactory);
            viewModel.Initialize();

            Assert.That(viewModel.CurrentPage, Is.SameAs(errorPageViewModel));
        }
    }
}
