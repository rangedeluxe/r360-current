﻿using Configurator.ConfigurationModels;
using Configurator.Saving;
using Configurator.ViewModelCommon;
using NSubstitute;
using NUnit.Framework;

namespace Configurator.EditPage
{
    public class EditPageViewModelTests
    {
        [Test]
        public void SaveClick_CallsSaveOnConfigurationSaver()
        {
            var configurationModel = new ConfigurationModel();
            var configurationSaver = Substitute.For<IConfigurationSaver>();
            var viewModel = new EditPageViewModel(configurationModel, configurationSaver,
                Substitute.For<IMessageBoxService>());

            viewModel.SaveCommand.Execute(null);

            configurationSaver.Received().Save(configurationModel);
        }
    }
}