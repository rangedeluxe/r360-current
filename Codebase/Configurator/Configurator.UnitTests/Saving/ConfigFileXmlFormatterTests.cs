﻿using System.Xml.Linq;
using NUnit.Framework;

namespace Configurator.Saving
{
    public class ConfigFileXmlFormatterTests
    {
        private const string XmlDeclarationLine = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";

        [Test]
        public void Element_NoAttributes()
        {
            var element = new XElement("root");
            var result = new ConfigFileXmlFormatter().Format(element);
            Assert.That(result, Is.EqualTo(XmlDeclarationLine + "<root />"));
        }
        [Test]
        public void Element_WithAttributes_WritesElementOnSingleLine()
        {
            var element = new XElement("root",
                new XAttribute("Key", "Key1"),
                new XAttribute("Value", "Value1"),
                new XAttribute("Path", "Path1"));
            var result = new ConfigFileXmlFormatter().Format(element);
            Assert.That(result,
                Is.EqualTo(XmlDeclarationLine + @"<root Key=""Key1"" Value=""Value1"" Path=""Path1"" />"));
        }
        [Test]
        public void AppSettingsAddElement_WithAttributes_WritesElementOnSingleLine()
        {
            var element = new XElement("configuration",
                new XElement("appSettings",
                    new XElement("add",
                        new XAttribute("Key", "Key1"),
                        new XAttribute("Value", "Value1"),
                        new XAttribute("Path", "Path1"))));
            var result = new ConfigFileXmlFormatter().Format(element);
            Assert.That(result, Is.EqualTo(XmlDeclarationLine + @"<configuration>
  <appSettings>
    <add Key=""Key1"" Value=""Value1"" Path=""Path1"" />
  </appSettings>
</configuration>"));
        }
        [Test]
        public void DataImportConfigSectionsAddElement_WithAttributes_WritesOneAttributePerLine()
        {
            var element = new XElement("configuration",
                new XElement("DataImportSettings",
                    new XElement("DataImportConfigSections",
                        new XElement("add",
                            new XAttribute("Key", "Key1"),
                            new XAttribute("Value", "Value1"),
                            new XAttribute("Path", "Path1")))));
            var result = new ConfigFileXmlFormatter().Format(element);
            Assert.That(result, Is.EqualTo(XmlDeclarationLine + @"<configuration>
  <DataImportSettings>
    <DataImportConfigSections>
      <add
        Key=""Key1""
        Value=""Value1""
        Path=""Path1"" />
    </DataImportConfigSections>
  </DataImportSettings>
</configuration>"));
        }
        [Test]
        public void ReformatsTabs_EvenInComments()
        {
            var inputLines = new[]
            {
                "<root>",
                "\t<sub/>",
                "\t<!--",
                "\tMultiline comment, indented with tabs",
                "\t-->",
                "</root>"
            };
            var input = string.Join("\r\n", inputLines);
            var element = XElement.Parse(input);
            var result = new ConfigFileXmlFormatter().Format(element);
            Assert.That(result, Is.EqualTo(
                XmlDeclarationLine +
                "<root>\r\n  <sub />\r\n  <!--\r\n  Multiline comment, indented with tabs\r\n  -->\r\n</root>"));
        }
    }
}
