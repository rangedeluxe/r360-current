﻿using System;
using System.IO;
using NUnit.Framework;

namespace Configurator.Saving
{
    public class BackupServiceTests
    {
        private static readonly DateTime BackupDateTime = new DateTime(2017, 2, 28, 23, 9, 8);

        [SetUp]
        public void SetUp()
        {
            WfsAppsDirectory = Path.Combine(Path.GetTempPath(), "WFSApps");

            ConfiguratorDirectory = Path.Combine(WfsAppsDirectory, "Configurator");
            ConfiguratorBackupsDirectory = Path.Combine(ConfiguratorDirectory, "Backups");
            CurrentBackupDirectory = Path.Combine(ConfiguratorBackupsDirectory, "2017-02-28_23.09.08");
            MyAppConfigBackupPath = Path.Combine(CurrentBackupDirectory, @"MyApp\MyApp.exe.config");

            MyAppDirectory = Path.Combine(WfsAppsDirectory, "MyApp");
            MyAppConfigPath = Path.Combine(MyAppDirectory, "MyApp.exe.config");

            Directory.CreateDirectory(ConfiguratorDirectory);
            if (Directory.Exists(ConfiguratorBackupsDirectory))
                Directory.Delete(ConfiguratorBackupsDirectory, recursive: true);
            Directory.CreateDirectory(MyAppDirectory);
            File.WriteAllText(MyAppConfigPath, Guid.NewGuid().ToString());

            Environment.CurrentDirectory = ConfiguratorDirectory;
        }

        private string WfsAppsDirectory { get; set; }
        private string ConfiguratorDirectory { get; set; }
        private string ConfiguratorBackupsDirectory { get; set; }
        private string CurrentBackupDirectory { get; set; }
        private string MyAppDirectory { get; set; }
        private string MyAppConfigPath { get; set; }
        private string MyAppConfigBackupPath { get; set; }

        [Test]
        public void BackUp_CopiesFile()
        {
            var backupService = new BackupService(BackupDateTime, WfsAppsDirectory);
            using (var stream = File.OpenRead(MyAppConfigPath))
                backupService.BackUp(stream);

            Assert.That(File.Exists(MyAppConfigBackupPath), Is.True, $"File.Exists({MyAppConfigBackupPath})");
        }
        [Test]
        public void BackUp_IfBackupFileAlreadyExists_OverwritesIt()
        {
            // Should be super unlikely, but if we already have a backup for this exact timestamp, just overwrite it.

            File.WriteAllText(MyAppConfigPath, @"1");
            var firstBackupService = new BackupService(BackupDateTime, WfsAppsDirectory);
            using (var stream = File.OpenRead(MyAppConfigPath))
                firstBackupService.BackUp(stream);

            File.WriteAllText(MyAppConfigPath, @"2");
            var secondBackupService = new BackupService(BackupDateTime, WfsAppsDirectory);
            using (var stream = File.OpenRead(MyAppConfigPath))
                secondBackupService.BackUp(stream);

            var content = File.ReadAllText(MyAppConfigBackupPath);
            Assert.That(content, Is.EqualTo("2"));
        }
        [Test]
        public void BackUp_IfFileIsOutsideWfsApps_Throws()
        {
            var outsideFilePath = Path.Combine(Path.GetTempPath(), "temp.txt");
            File.WriteAllText(outsideFilePath, "");

            var backupService = new BackupService(BackupDateTime, WfsAppsDirectory);
            using (var stream = File.OpenRead(outsideFilePath))
                Assert.Throws<InvalidOperationException>(() => backupService.BackUp(stream));
        }
        [Test]
        public void BackUp_PreservesOriginalModificationTime()
        {
            new FileInfo(MyAppConfigPath).LastWriteTime = new DateTime(2000, 1, 1);

            var backupService = new BackupService(BackupDateTime, WfsAppsDirectory);
            using (var stream = File.OpenRead(MyAppConfigPath))
                backupService.BackUp(stream);

            var backupFile = new FileInfo(MyAppConfigBackupPath);
            Assert.That(backupFile.LastWriteTime, Is.EqualTo(new DateTime(2000, 1, 1)));
        }
    }
}
