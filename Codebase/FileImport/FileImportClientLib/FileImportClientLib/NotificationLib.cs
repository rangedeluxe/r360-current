﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53793 JMC 07/17/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientLib {

    public static class NotificationLib {

        public const string NOTIFICATION_SCHEMAVERSION = "1.00.00";

        public static bool NotificationsToXml(
            Guid sourceTrackingID, 
            string clientProcessCode, 
            List<cNotification> notifications, 
            out XmlDocument notificationsXml) {

            bool bolRetVal;
            XmlDocument docNotificationsXml;
            XmlNode nodeRoot;
            XmlNode nodeNotification;

            try {

                docNotificationsXml = new XmlDocument();

                nodeRoot = docNotificationsXml.CreateElement("Notifications");
                XmlLib.AddAttribute(nodeRoot, "SourceTrackingID", sourceTrackingID.ToString());
                XmlLib.AddAttribute(nodeRoot, "ClientProcessCode", clientProcessCode);
                XmlLib.AddAttribute(nodeRoot, "XSDVersion", NOTIFICATION_SCHEMAVERSION);

                foreach(cNotification notification in notifications) {
                    nodeNotification = docNotificationsXml.ImportNode(notification.ToXml(), true);
                    nodeRoot.AppendChild(nodeNotification);
                }

                docNotificationsXml.AppendChild(nodeRoot);

                bolRetVal = true;
            } catch(Exception) {
                throw;
            }

            notificationsXml = docNotificationsXml;

            return(bolRetVal);
        }

        public static bool NotificationsFromXml(XmlDocument notificationsXml, out List<cNotification> notifications) {

            bool bolRetVal;
            List<cNotification> arNotifications;

            try {

                arNotifications = new List<cNotification>();

                XmlNodeList nlNotifications;
                nlNotifications = notificationsXml.DocumentElement.SelectNodes("./Notification");

                foreach(XmlNode node in nlNotifications) {
                    arNotifications.Add(new cNotification(node));
                }

                bolRetVal = true;
            } catch(Exception) {
                throw;
            }

            notifications = arNotifications;

            return(bolRetVal);
        }
    }
}
