﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WFS.LTA.FileImportClientLib
{
    public class ConfigurationContext
    {
        public int BankId { get; set; }
        public int OrganizationId { get; set; }
    }
}
