﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53793 JMC 08/23/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientLib {

    public delegate void FITClientOutputLogEventHandler(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance);
    public delegate void FITClientOutputTraceEventHandler(string msg, FITClientConsoleLTAMessageType messageType);
    public delegate void FITClientOutputLogAndTraceEventHandler(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance);
    public delegate void FITClientOutputLogAndTraceExceptionEventHandler(string msg, string src, Exception ex);

    public enum FITClientMessageType
    {
        Error = 0,
        Information = 1,
        Warning = 2,
    }
	
	
    public enum FITClientMessageImportance
    {
        Essential = 0,
        Verbose = 1,
        Debug = 2,
    }	
	
    public enum FITClientConsoleLTAMessageType {
        Error = 0,
        Information = 1,
        Warning = 2,
        TraceInfo = 3
    }	
}
