﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53793 JMC 07/17/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientLib {
 
    public class cNotification {

        private List<cNotificationFile> _NotificationFiles = new List<cNotificationFile>();
        private List<string> _MessageParts = new List<string>();

        public cNotification() {
            this.NotificationTrackingID = Guid.NewGuid();
        }

        public cNotification(XmlNode notificationNode) {

            cNotificationFile objNotificationFile;

            if(notificationNode == null) {
                throw(new Exception("Cannot create Notification.  Xml document object was not defined."));
            } else {
                this.BankID = XmlLib.GetAttribute(notificationNode, "BankID", -1);
                this.ClientGroupID = XmlLib.GetAttribute(notificationNode, "ClientGroupID", -1);
                this.ClientID = XmlLib.GetAttribute(notificationNode, "ClientID", -1);
                this.NotificationDate = XmlLib.GetAttribute(notificationNode, "NotificationDate", DateTime.MinValue);
                this.SourceNotificationID = XmlLib.GetAttribute(notificationNode, "SourceNotificationID", Guid.Empty);
                this.NotificationSourceKey = XmlLib.GetAttribute(notificationNode, "NotificationSourceKey", -1);
                this.NotificationTrackingID = XmlLib.GetAttribute(notificationNode, "NotificationTrackingID", Guid.Empty);

                _MessageParts = new List<string>();
                foreach(XmlNode node in notificationNode.SelectNodes("MessageParts/MessagePart")) {
                    _MessageParts.Add(node.InnerText);
                }

                _NotificationFiles = new List<cNotificationFile>();
                foreach(XmlNode node in notificationNode.SelectNodes("Files/File")) {
                    
                    objNotificationFile = new cNotificationFile();

                    objNotificationFile.FileIdentifier = XmlLib.GetAttribute(node, "FileIdentifier", Guid.Empty);
                    objNotificationFile.FileType = XmlLib.GetAttribute(node, "FileType", string.Empty);
                    objNotificationFile.UserFileName = XmlLib.GetAttribute(node, "UserFileName", string.Empty);
                    objNotificationFile.FileExtension = XmlLib.GetAttribute(node, "FileExtension", string.Empty);
                    objNotificationFile.ClientFilePath = XmlLib.GetAttribute(node, "ClientFilePath", string.Empty);
                    objNotificationFile.FileSize = XmlLib.GetAttribute(node, "FileSize", string.Empty);
                    this.AddNotificationFile(objNotificationFile);
                }

            }
        }

        public int BankID {
            get;
            set;
        }

        public int ClientGroupID {
            get;
            set;
        }

        public int ClientID {
            get;
            set;
        }

        public DateTime NotificationDate {
            get;
            set;
        }

        public int NotificationDateKey {
            get {
                return(int.Parse(this.NotificationDate.ToString("yyyyMMdd")));
            }
        }

        public Guid SourceNotificationID {
            get;
            set;
        }

        public int NotificationSourceKey {
            get;
            set;
        }

        public Guid NotificationTrackingID {
            get;
            set;
        }

        public List<string> MessageParts {
            get {
                return(_MessageParts);
            }
        }

        public List<cNotificationFile> NotificationFiles {
            get {
                return(_NotificationFiles);
            }
        }

        public void SetMessage(string message) {

            int chunkSize = 128;
            int stringLength = message.Length;
            
            _MessageParts = new List<string>();

            for(int i=0; i<stringLength;i+=chunkSize) {
                if(i + chunkSize > stringLength) {
                    chunkSize = stringLength - i;
                }
                _MessageParts.Add(message.Substring(i, chunkSize));
            }
        }

        public void AddNotificationFile(cNotificationFile notificationNode) {
            _NotificationFiles.Add(notificationNode);
        }

        public XmlNode ToXml() {
            
            XmlDocument docXml;
            XmlNode nodeNotification;
            XmlNode nodeNotificationFiles;
            XmlNode nodeNotificationFile;
            XmlNode nodeMessage;
            XmlNode nodeMessagePart;

            docXml = new XmlDocument();
            nodeNotification = docXml.CreateNode(XmlNodeType.Element, "Notification", docXml.NamespaceURI);

            XmlLib.AddAttribute(nodeNotification, "BankID", this.BankID.ToString());
            XmlLib.AddAttribute(nodeNotification, "ClientGroupID", this.ClientGroupID.ToString());
            XmlLib.AddAttribute(nodeNotification, "ClientID", this.ClientID.ToString());
            XmlLib.AddAttribute(nodeNotification, "NotificationDate", this.NotificationDate.ToUniversalTime().ToString("u"));
            XmlLib.AddAttribute(nodeNotification, "SourceNotificationID", this.SourceNotificationID.ToString());
            XmlLib.AddAttribute(nodeNotification, "NotificationSourceKey", this.NotificationSourceKey.ToString());
            XmlLib.AddAttribute(nodeNotification, "NotificationTrackingID", this.NotificationTrackingID.ToString());
            
            // Append Message.
            nodeMessage = docXml.CreateNode(XmlNodeType.Element, "MessageParts", docXml.NamespaceURI);
            if(_MessageParts.Count > 0) {
                foreach(string msgPart in _MessageParts) {
                    nodeMessagePart = docXml.CreateNode(XmlNodeType.Element, "MessagePart", docXml.NamespaceURI);
                    nodeMessagePart.InnerText = msgPart;
                    nodeMessage.AppendChild(nodeMessagePart);
                }
            } else {
                if(_NotificationFiles.Count > 0) {
                    foreach(cNotificationFile notificationFile in _NotificationFiles) {
                        nodeMessagePart = docXml.CreateNode(XmlNodeType.Element, "MessagePart", docXml.NamespaceURI);
                        nodeMessagePart.InnerText = notificationFile.UserFileName + "\n";
                        nodeMessage.AppendChild(nodeMessagePart);
                    }
                } else {
                    nodeMessagePart = docXml.CreateNode(XmlNodeType.Element, "MessagePart", docXml.NamespaceURI);
                    nodeMessagePart.InnerText = "Notification";
                    nodeMessage.AppendChild(nodeMessagePart);
                }
            }
            nodeNotification.AppendChild(nodeMessage);

            // Append Files.
            nodeNotificationFiles = docXml.CreateNode(XmlNodeType.Element, "Files", docXml.NamespaceURI);

            foreach(cNotificationFile notificationfile in _NotificationFiles) {

                nodeNotificationFile = docXml.CreateNode(XmlNodeType.Element, "File", docXml.NamespaceURI);

                XmlLib.AddAttribute(nodeNotificationFile, "FileIdentifier", notificationfile.FileIdentifier.ToString());
                XmlLib.AddAttribute(nodeNotificationFile, "FileType", notificationfile.FileType);
                XmlLib.AddAttribute(nodeNotificationFile, "UserFileName", notificationfile.UserFileName);
                XmlLib.AddAttribute(nodeNotificationFile, "FileExtension", notificationfile.FileExtension);
                XmlLib.AddAttribute(nodeNotificationFile, "ClientFilePath", notificationfile.ClientFilePath);
                if (!string.IsNullOrEmpty(notificationfile.FileSize))
                {
                    XmlLib.AddAttribute(nodeNotificationFile, "FileSize", notificationfile.FileSize);
                }
                nodeNotificationFiles.AppendChild(nodeNotificationFile);
            }



            nodeNotification.AppendChild(nodeNotificationFiles);

            return(nodeNotification);
        }
    }
}
