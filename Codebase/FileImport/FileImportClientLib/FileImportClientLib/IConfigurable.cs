﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.FileImportClientLib
{
    public interface IConfigurable
    {
        void Configure(ConfigurationContext configurationContext);
    }
}
