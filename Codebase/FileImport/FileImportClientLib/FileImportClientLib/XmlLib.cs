﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53793 JMC 07/17/2012 
*   -Initial version.
* WI 88713 TWE 02/20/2013
*   - modify the datetime to always return utc.
*******************************************************************************/
namespace WFS.LTA.FileImportClientLib {

    public static class XmlLib {

        public static void AddAttribute(XmlNode node, string Key, string Value) {

            XmlAttribute att;

            att = node.OwnerDocument.CreateAttribute(Key);
            att.InnerText = Value;
            node.Attributes.Append(att);
        }

        public static int GetAttribute(XmlNode notificationNode, string keyName, int defaultValue) {

            XmlAttribute objAtt;
            int intTemp;
            int intRetVal;

            if(notificationNode.Attributes[keyName] != null) {
                objAtt = notificationNode.Attributes[keyName];
                if(objAtt != null && int.TryParse(objAtt.Value, out intTemp)) {
                    intRetVal = intTemp;
                } else {
                    intRetVal = defaultValue;
                }
            } else {
                intRetVal = defaultValue;
            }

            return(intRetVal);
        }

        public static DateTime GetAttribute(XmlNode notificationNode, string keyName, DateTime defaultValue) {

            XmlAttribute objAtt;
            DateTime dteTemp;
            DateTime dteRetVal;

            if(notificationNode.Attributes[keyName] != null) {
                objAtt = notificationNode.Attributes[keyName];
                if(objAtt != null && DateTime.TryParse(objAtt.Value, out dteTemp)) {
                    //tryparse automatically converts the date
                    // if the date comes in with z then dteTemp contains timezone local adjusted
                    //    by the local server timezone offset
                    // if the date comes in without z then dteTemp contains entered time as local time.
                    // so to output utc we just need to convert the date to utc.
                    dteRetVal = dteTemp.ToUniversalTime();
                } else {
                    dteRetVal = defaultValue;
                }
            } else {
                dteRetVal = defaultValue;
            }

            return(dteRetVal);
        }

        public static Guid GetAttribute(XmlNode notificationNode, string keyName, Guid defaultValue) {

            XmlAttribute objAtt;
            Guid gidTemp;
            Guid gidRetVal;

            if(notificationNode.Attributes[keyName] != null) {
                objAtt = notificationNode.Attributes[keyName];
                if(objAtt != null && TryParse(objAtt.Value, out gidTemp)) {
                    gidRetVal = gidTemp;
                } else {
                    gidRetVal = defaultValue;
                }
            } else {
                gidRetVal = defaultValue;
            }

            return(gidRetVal);
        }

        public static string GetAttribute(XmlNode notificationNode, string keyName, string defaultValue) {

            XmlAttribute objAtt;
            string strRetVal;

            if(notificationNode.Attributes[keyName] != null) {
                objAtt = notificationNode.Attributes[keyName];
                if(objAtt != null) {
                    strRetVal = objAtt.Value.ToString();
                } else {
                    strRetVal = defaultValue;
                }
            } else {
                strRetVal = defaultValue;
            }

            return(strRetVal);
        }

        private static bool TryParse(string value, out Guid result) {

            Guid gidTemp;
            Guid gidResult;
            bool bolRetVal;

            try {
                gidTemp = new Guid(value);
                gidResult = gidTemp;
                bolRetVal = true;
            } catch {
                gidResult = Guid.Empty;
                bolRetVal = false;
            }

            result = gidResult;

            return(bolRetVal);
        }
    }
}
