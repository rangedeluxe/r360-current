﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53793 JMC 07/17/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientLib {

    public interface IFileDropXClient : IConfigurable
    {

        event FITClientOutputLogEventHandler OutputLog;
        event FITClientOutputTraceEventHandler OutputTrace;
        event FITClientOutputLogAndTraceEventHandler OutputLogAndTrace;
        event FITClientOutputLogAndTraceExceptionEventHandler OutputLogAndTraceException;

        bool GetNotificationXml(
            string inputFilePath, 
            string clientProcessCode,
            out XmlDocument fileData, 
            out int notificationCount);
    }
}
