﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 06/17/2013
*
* Purpose: 
*
* Modification History
* WI 105767 JMC 06/17/2013
*   Initial Version.  Ported from ipoDAL.dll
******************************************************************************/
namespace WFS.RecHub.DAL.FileImportServicesDAL {

    public class cFileImportServicesDAL : _DALBase {

        public cFileImportServicesDAL(string siteKey) : base(siteKey) {
        }

        /// <summary>
        /// Gets the notification data XSD.
        /// </summary>
        /// <param name="xsdName">Name of the XSD.</param>
        /// <param name="xsdVersion">The XSD version.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetNotificationDataXsd(string xsdName, string xsdVersion, out DataTable dt) {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubSystem.usp_ImportSchemas_Get_BySchemaType";
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSchemaType", SqlDbType.VarChar, xsdName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmXSDVersion", SqlDbType.VarChar, xsdVersion, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms, out dt);
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        /// <summary>
        /// Inserts the notification.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns></returns>
        public bool InsertNotification(string xml) {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubSystem.usp_DataImportIntegrationServices_InsertNotification";
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmNotification", SqlDbType.Xml, xml, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms);
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        /// <summary>
        /// Gets the notification responses.
        /// </summary>
        /// <param name="clientProcessCode">The client process code.</param>
        /// <param name="dtNotificationResponses">The dt notification responses.</param>
        /// <returns></returns>
        public bool GetNotificationResponses(string clientProcessCode, out DataTable dtNotificationResponses) {
            SqlParameter[] parms;
            DataTable dt;
            bool bolRetVal;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubSystem.usp_DataImportIntegrationServices_GetResponseNotification";
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmClientProcessCode", SqlDbType.VarChar, clientProcessCode, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms, out dt);
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                bolRetVal = false;
            }

            dtNotificationResponses = dt;
            return (bolRetVal);
        }

        /// <summary>
        /// Sets the response complete.
        /// </summary>
        /// <param name="responseTrackingID">The response tracking ID.</param>
        /// <param name="entityTrackingID">The entity tracking ID.</param>
        /// <returns></returns>
        public bool SetResponseComplete(Guid responseTrackingID, Guid entityTrackingID) {
            SqlParameter[] parms;
            bool bolRetVal;
            List<SqlParameter> arParms;
            const string PROCNAME = "RecHubSystem.usp_DataImportQueue_Upd_QueueStatus";
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmResponseTrackingID", SqlDbType.UniqueIdentifier, responseTrackingID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEntityTrackingID", SqlDbType.UniqueIdentifier, entityTrackingID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms);
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bolRetVal = false;
            }

            return (bolRetVal);
        }
    }
}
