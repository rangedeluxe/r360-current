﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     07/25/2012
*
* Purpose:  
*
* Modification History
* CR 54515 JMC 07/25/2012
*   -New File used for File Import Toolkit
* WI 118334 JMC 10/22/2013
*   -Modified NotificationDate to include time portion.  This is needed so it 
*    can be down-converted to local time at the server when saving attachments.
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportWCFLib {

    [DataContract]
    public class NotificationFileInfoContract {

        [DataMember]
        public DateTime NotificationDateUTC {
            get;
            set;
        }

        [DataMember]
        public int BankID {
            get;
            set;
        }

        [DataMember]
        public int CustomerID {
            get;
            set;
        }

        [DataMember]
        public int LockboxID {
            get;
            set;
        }

        [DataMember]
        public Guid FileIdentifier {
            get;
            set;
        }

        [DataMember]
        public string FileType {
            get;
            set;
        }

        [DataMember]
        public string UserFileName {
            get;
            set;
        }

        [DataMember]
        public string FileExtension {
            get;
            set;
        }
    }
}
