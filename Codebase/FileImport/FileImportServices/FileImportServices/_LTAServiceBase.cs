﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Web;
using System.Xml;
using WFS.LTA.Common;
using WFS.LTA.FileImport.FileImportServicesAPI;
using WFS.LTA.FileImport.FileImportWCFLib;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ExternalLogonService;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.ImportToolkit.API;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* CR 54224 JMC 07/17/2012
*   -Initial Version.
* WI 105795 JMC 06/17/2013
*   -Updated to use 2.00 libraries.
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServices {

    public abstract class _LTAServiceBase {

        private cSiteOptions _SiteOptions = null;
        private cEventLog _EventLog = null;
        private string _SiteKey = null;
        private IExternalLogonService logonService;
        private IFileImportRepository fileImportRepository;
        private IAlertable alert;
        private IAuditable audit;
        private Guid sessionId;
        private const string HTTPHEADER_SITEKEY = "SiteKey";
        private const string HTTPHEADER_SESSIONID = "SessionID";
        private const string HTTPHEADER_ENTITY = "Entity";

        protected abstract void SetUp(Guid sessionId);
        protected abstract void SetUp();

        protected const string NOTIFICATION_SCHEMATYPE = "Notification Import Integration Services";

        protected IExternalLogonService LogonService
        {
            get
            {
                if (logonService == null)
                    return new ExternalLogonClient();

                return logonService;
            }
            set
            {
                logonService = value;
            }
        }

        protected IAlertable Alert
        {
            get
            {
                if (alert == null)
                    return new ImportToolkitRepository(SiteKey);

                return alert;
            }
            set
            {
                alert = value;
            }
        }

        protected IAuditable Audit
        {
            get
            {
                if (audit == null)
                    return new ImportToolkitRepository(SiteKey);

                return audit;
            }
            set
            {
                audit = value;
            }
        }

        protected IFileImportRepository FileImportRepository
        {
            get
            {
                return (fileImportRepository);
            }
            set
            {
                fileImportRepository = value;
            }
        }

        // Ping
        public string Ping() {

            FaultException error;

            try {

               return "Pong";

            } catch(Exception ex) {
                error = BuildGeneralException(ex, "Ping", "A general exception occurred while executing Ping() method");
                throw error;
            }
        }

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected cSiteOptions SiteOptions {
            get {
                if (_SiteOptions == null){
                    _SiteOptions = new cSiteOptions(_SiteKey);
                }
                return _SiteOptions;
            }
            set
            {
                _SiteOptions = value;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog EventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (MessageImportance)SiteOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        protected string SiteKey {
            get {
                return(_SiteKey);
            }
            set {
                _SiteKey = value;
            }
        }

        protected Guid SessionId
        {
            get
            {
                return sessionId;
            }
            set
            {
                sessionId = value;
            }
        }

        protected string Entity
        {
            get;
            set;
        }

        protected bool GetSiteKeyHeader(out string value) {

            if(FileImportServiceContext.Current.SiteKey != null && 
               FileImportServiceContext.Current.SiteKey.Length > 0) {
                value = FileImportServiceContext.Current.SiteKey;
                return(true);
            } else {
                value = string.Empty;
                return(false);
            }
        }

        protected bool GetSessionIDHeader(out Guid value) {

            Guid gidTemp;

            if(FileImportServiceContext.Current.SessionID != null && 
               ipoLib.TryParse(FileImportServiceContext.Current.SessionID, out gidTemp)) {
                value = gidTemp;
                return(true);
            } else {
                value = Guid.Empty;
                return(false);
            }
        }
        
        protected bool GetNotificationFileHeader(out NotificationFileInfoContract value) {

            if(FileImportServiceContext.Current.NotificationFileContract != null) {
                value = FileImportServiceContext.Current.NotificationFileContract;
                return(true);
            } else {
                value = null;
                return(false);
            }
        }

        protected XmlDocument BuildOperationSuccessDoc() {
            return(BuildMsgDoc("Information", new string[]{"Operation Successful"}));
        }

        protected T Execute<T>(Func<FileImportServicesAPI.FileImportServices, T> operation, [CallerMemberName] string procName = null) where T : new()
        {
            try
            {
                GetHeaderItems();

                if (sessionId == null)
                    throw new InvalidSessionIDException();

                SetUp(sessionId);

                FileImportServicesAPI.FileImportServices api =
                    new FileImportServicesAPI.FileImportServices
                        (new ServiceSetupContext()
                        {
                            FileImportRepository = this.FileImportRepository,
                            LogonService = this.LogonService,
                            SiteOptions = this.SiteOptions,
                            Alert = this.Alert,
                            Audit = this.Audit
                        }
                        );
                return operation(api);
            }
            catch (InvalidSiteKeyException ex)
            {
                var fault = new FileImportServiceFault(0, (int)FileImportServiceErrorCodes.InvalidSiteKey, this.GetType().Name, ex.Message);
                throw new FaultException<FileImportServiceFault>(fault, fault.Message);
            }
            catch (InvalidSessionIDException ex)
            {
                var fault = new FileImportServiceFault(0, (int)FileImportServiceErrorCodes.InvalidSessionID, this.GetType().Name, ex.Message);
                throw new FaultException<FileImportServiceFault>(fault, fault.Message);
            }
            catch (SessionExpiredException ex)
            {
                var fault = new FileImportServiceFault(0, (int)FileImportServiceErrorCodes.SessionExpired, this.GetType().Name, ex.Message);
                throw new FaultException<FileImportServiceFault>(fault, fault.Message);
            }
            catch (FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                var fault = new FileImportServiceFault(0, (int)FileImportServiceErrorCodes.UndefinedError, this.GetType().Name, ex.Message);
                throw new FaultException<FileImportServiceFault>(fault, fault.Message);
            }
           
        }

        protected T ManageLogon<T>(Func<IExternalLogonService, T> operation, [CallerMemberName] string procName = null) where T : new()
        {
            try
            {
                GetHeaderItems();

                SetUp();

                return operation(LogonService);

            }
            catch (InvalidSiteKeyException ex)
            {
                var fault = new FileImportServiceFault(0, (int)FileImportServiceErrorCodes.InvalidSiteKey, this.GetType().Name, ex.Message);
                throw new FaultException<FileImportServiceFault>(fault, fault.Message);
            }
            catch (FaultException)
            {
                throw ;
            }
            catch (Exception ex)
            {
                throw BuildGeneralException(ex, procName, string.Format("A general exception occurred while executing {0}() method", procName));
            }
        }

        //==================================================================================================    
        

        protected void GetHeaderItems()
        {
            _SiteKey = Utility.GetPropertyValue<FileImportServiceContext, string>(FileImportServiceContext.Current, HTTPHEADER_SITEKEY) ?? string.Empty;
            Entity = Utility.GetPropertyValue<FileImportServiceContext, string>(FileImportServiceContext.Current, HTTPHEADER_ENTITY) ?? string.Empty;
            sessionId = Utility.GetPropertyValue<FileImportServiceContext, Guid>(FileImportServiceContext.Current, HTTPHEADER_SESSIONID);

            if (string.IsNullOrEmpty(_SiteKey))
                throw new InvalidSiteKeyException();
        }

        protected XmlDocument BuildOperationFailedDoc() {
            return(BuildMsgDoc("Error", new string[]{"Operation Failed"}));
        }
 
        protected XmlDocument BuildErrorDoc(string[] ErrorMessages) {
            return(BuildMsgDoc("Error", ErrorMessages));
        }

        private XmlDocument BuildMsgDoc(string msgType, string[] errorMessages) {

            XmlDocument docXml;
            XmlNode nodeRoot;
            XmlNode nodeMessages;
            XmlAttribute attType;
            XmlNode nodeMsg;


            docXml = new XmlDocument();
            nodeRoot = docXml.CreateElement("Root", docXml.NamespaceURI);
            nodeMessages = docXml.CreateElement("Messages", docXml.NamespaceURI);
            foreach(string msg in errorMessages) {
                nodeMsg = docXml.CreateElement("Message");
                attType = docXml.CreateAttribute("Type", docXml.NamespaceURI);
                attType.Value = msgType;
                nodeMsg.Attributes.Append(attType);
                nodeMsg.InnerText = msg;
                nodeMessages.AppendChild(nodeMsg);
            }
            nodeRoot.AppendChild(nodeMessages);
            docXml.AppendChild(nodeRoot);

            return(docXml);
        }

        protected FaultException BuildGeneralException(Exception ex, string procName, string msg) {
            EventLog.logEvent("A general exception occurred", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(FileImportServiceErrorCodes.UndefinedError, msg, msg));
        }

        private FaultException BuildIniReadException(Exception ex, string procName, string section, string key) {
            EventLog.logEvent("Unable to read ini setting: [" + section + "]." + key, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(FileImportServiceErrorCodes.ConfigurationError, 
                                             "A configuration error occurred at the server",
                                             "A configuration error occurred at the server"));
        }

        private FaultException BuildCommunicationsChannelException(Exception ex, string procName, string msg) {
            EventLog.logEvent("Unable to establish communications channel - " + msg, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            EventLog.logError(ex, this.GetType().Name, procName);
            return(BuildServerFaultException(FileImportServiceErrorCodes.CommunicationError, 
                                             "A communications channel occurred at the server",
                                             "A communications channel occurred at the server"));
        }

        private static FaultException BuildServerFaultException(FileImportServiceErrorCodes errorCode, string msg, string details) {

            FaultException objError;

            var errorCodeString = ((int)errorCode).ToString();
            var message = string.Format("{0} - {1}", errorCode, msg);
            objError = new FaultException(string.Format("Error Code {0} Encountered.  Message: {1} Details: {2}", errorCode, message, details));
            
            return(objError);
        }
    }
}