﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* CR 54224 JMC 07/17/2012
*   -Initial Version.
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServices {

    /// <summary></summary>
    public enum FileImportServiceErrorCodes {
        /// <summary></summary>
        Success = 200,
        /// <summary></summary>
        UndefinedError = 700,
        /// <summary></summary>
        InvalidSiteKey = 701,
        /// <summary></summary>
        InvalidSessionID = 702,
        /// <summary></summary>
        SessionExpired = 703,
        /// <summary></summary>
        InvalidSession = 704,
        /// <summary></summary>
        CommunicationError = 731,
        /// <summary></summary>
        ConfigurationError = 732,
        /// <summary></summary>
        XsdValidationFailedFault = 751,
        /// <summary></summary>
        DataValidationFailedFault = 752
    }
}