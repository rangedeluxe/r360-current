﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* CR 54224 JMC 07/17/2012
*   -Initial Version.
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServices {

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IFileUploadService
    {
        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "SendNotificationFile")]
        [FaultContract(typeof(ServerFaultException))]
        [FaultContract(typeof(DataValidationFailedFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        ReturnItemUpload SendNotificationFile(UploadItem item);
    }
}
