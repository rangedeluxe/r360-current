﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Xml;
using System.Data;
using System.IO;

using WFS.LTA.FileImport.FileImportServicesAPI;
using WFS.LTA.FileImport.FileImportWCFLib;
using WFS.RecHub.Common;
using System.Configuration;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using ImportToolkit = WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* CR 54224 JMC 07/17/2012
*   -Initial Version
* WI 96178 WJS 4/11/2013
*   - Allow FIT to be hosted service.
*   -Initial Version.
* WI 105795 JMC 06/17/2013
*   -Updated to use 2.00 libraries.
* WI 118360 JMC 10/22/2013
*   -Updated File Import Services to expect UTC Notification Date when 
*    receiving files
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServices {

    public class FileUploadService : _LTAServiceBase, IFileUploadService, IAlertable, IAuditable {

        private const string HTTPHEADER_NOTIFICATIONFILECONTRACT = "NotificationFileContract";
        private FileImportServicesAPI.FileImportServices service;

        public FileUploadService() {
        }

        public StreamResponse SendNotificationFile(StreamRequest item)
        {
            bool bolRetVal = false; 
            NotificationFileInfoContract notificationFileContract;
            XmlDocument docRespXml;
            var itemupload = new StreamResponse();
            INotificationFile ltaNotificationFileContract = null;

            if (!GetNotificationFileHeader(out notificationFileContract))
            {
                throw (new InvalidNotificationFileContract());
            }

            ltaNotificationFileContract = CopyNotificationFileInfoContract(notificationFileContract);

            bolRetVal = Execute<bool>(service => {return service.SendNotificationFile(ltaNotificationFileContract, item.DataStream);});

            docRespXml = bolRetVal ? BuildOperationSuccessDoc() : BuildOperationFailedDoc();

            itemupload.Status = bolRetVal ? ImportToolkit.StreamResponse.StatusCode.SUCCESS : ImportToolkit.StreamResponse.StatusCode.FAIL;
            itemupload.ResponseXml = docRespXml.OuterXml;
            return itemupload;
        }

        private INotificationFile CopyNotificationFileInfoContract(NotificationFileInfoContract notificationFileContract) {

            var ltaNotificationFileContract = new WFS.RecHub.ImportToolkit.Common.DataTransferObjects.NotificationFileInfoContract();

            ltaNotificationFileContract.NotificationDateKey = int.Parse(notificationFileContract.NotificationDateUTC.ToLocalTime().ToString("yyyyMMdd"));
            ltaNotificationFileContract.BankID = notificationFileContract.BankID;
            ltaNotificationFileContract.CustomerID = notificationFileContract.CustomerID;
            ltaNotificationFileContract.LockboxID = notificationFileContract.LockboxID;
            ltaNotificationFileContract.FileIdentifier = notificationFileContract.FileIdentifier;
            ltaNotificationFileContract.FileType = notificationFileContract.FileType;
            ltaNotificationFileContract.UserFileName = notificationFileContract.UserFileName;
            ltaNotificationFileContract.FileExtension = notificationFileContract.FileExtension;

            return ltaNotificationFileContract;
        }


        protected override void SetUp(Guid sessionId)
        {
            SetUp();
            service = new FileImportServicesAPI.FileImportServices
                (new ServiceSetupContext { FileImportRepository = FileImportRepository, SiteOptions = SiteOptions, LogonService = LogonService, Alert = Alert, Audit = Audit });
            if (!service.IsSessionValid(new SessionRequest { Session = sessionId }))
                throw new InvalidSessionException();
        }

        protected override void SetUp()
        {
            FileImportRepository = new FileImportRepository(SiteKey);
            SiteOptions = new cSiteOptions(SiteKey);

            //grab the assembly and the service name (the object name to instantiate) from the config file
            var assembly = ConfigurationManager.AppSettings.Get("LogonModuleAssembly");
            var moduleName = ConfigurationManager.AppSettings.Get("LogonModuleName");

            try
            {
                LogonService = Activator.CreateInstance(assembly, moduleName).Unwrap() as IExternalLogonService;
            }
            catch { }//if we can't dynamically load the login client, it will be defaulted
        }

        public AlertResponse Log(AlertRequest requestContext)
        {
            return Execute<AlertResponse>(service => { return service.LogAlert(requestContext); });
        }

        public AuditResponse LogAuditEvent(AuditRequest requestContext)
        {
            return Execute<AuditResponse>(service => { return service.LogAuditEvent(requestContext); });
        }
    }
}
