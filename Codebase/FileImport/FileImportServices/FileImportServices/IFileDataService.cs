﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

using WFS.LTA.FileImport.FileImportWCFLib;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* CR 54224 JMC 07/17/2012
*   -Initial Version.
* WI 105795 JMC 06/17/2013
*   -Updated to use 2.00 libraries.
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServices {

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IFileDataService
    {
        [OperationContract(Action = "Ping")]
        string Ping();

        [OperationContract(Action = "GetSession")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool GetSession(string LogonName, string Password, out Guid SessionID);

        [OperationContract(Action = "EndSession")]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool EndSession();

        [OperationContract(Action = "GetSchemaDefinition")]
        [FaultContract(typeof(ServerFaultException))]
        bool GetSchemaDefinition(string sType, string XSDVersion, out string SetupDataXsd, out string RespXml);

        [OperationContract(Action = "SendNotificationData")]
        [FaultContract(typeof(DataValidationFailedFault))]
        [FaultContract(typeof(ServerFaultException))]
        bool SendNotificationData(string NotificationDataXml, out string RespXml);

        [OperationContract(Action = "GetNotificationResponses")]
        [FaultContract(typeof(ServerFaultException))]
        bool GetNotificationResponses(string clientProcessCode, out string notificationRespXml, out string respXml);

        [OperationContract(Action = "SendResponseComplete")]
        [FaultContract(typeof(ServerFaultException))]
        bool SendResponseComplete(Guid responseTrackingID, Guid notificationTrackingID);
    }

    //**************************************
    // Common Data Contracts
    //**************************************
    [DataContract]
    public class ServerFaultException {

        [DataMember]
        public string errorcode;

        [DataMember]
        public string message;

        [DataMember]
        public string details;
    }

    [MessageContract]
    public class UploadItem : IDisposable
    {
        [MessageBodyMember(Order = 1)]
        public System.IO.Stream data;

        public void Dispose()
        {
            if (data != null)
            {
                data.Close();
                data = null;
            }
        }

    }

    [MessageContract]
    public class ReturnItemUpload {
         [MessageHeader(MustUnderstand = true)]
         public bool bRetVal;

         [MessageBodyMember]
         public string strRespXml;
    }

    [DataContract]
    public class LTANotificationFileInfoContract : INotificationFile {

        [DataMember]
        public int NotificationDateKey {
            get;
            set;
        }

        [DataMember]
        public int BankID {
            get;
            set;
        }

        [DataMember]
        public int CustomerID {
            get;
            set;
        }

        [DataMember]
        public int LockboxID {
            get;
            set;
        }

        [DataMember]
        public Guid FileIdentifier {
            get;
            set;
        }

        [DataMember]
        public string FileType {
            get;
            set;
        }

        [DataMember]
        public string UserFileName {
            get;
            set;
        }

        [DataMember]
        public string FileExtension {
            get;
            set;
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSiteKeyFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSiteKeyFault : FileImportServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSiteKeyFault(int category, string source, string message) : base(category, (int)FileImportServiceErrorCodes.InvalidSiteKey, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSessionIDFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSessionIDFault : FileImportServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSessionIDFault(int category, string source, string message) : base(category, (int)FileImportServiceErrorCodes.InvalidSessionID, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SessionExpiredFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class SessionExpiredFault : FileImportServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public SessionExpiredFault(int category, string source, string message) : base(category, (int)FileImportServiceErrorCodes.SessionExpired, source, message) {
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSessionFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class InvalidSessionFault : FileImportServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public InvalidSessionFault(int category, string source, string message) : base(category, (int)FileImportServiceErrorCodes.InvalidSession, source, message) {
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }

  

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DataValidationFailedFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    public partial class DataValidationFailedFault : FileImportServiceFault, System.ComponentModel.INotifyPropertyChanged {
        
        public DataValidationFailedFault(int category, string source, string message) : base(category, (int)FileImportServiceErrorCodes.DataValidationFailedFault, source, message) {
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
