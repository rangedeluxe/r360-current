﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Xml;

using WFS.LTA.FileImport.FileImportServicesAPI;
using WFS.RecHub.Common;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* CR 54224 JMC 07/17/2012
*   -Initial Version.
* WI 105795 JMC 06/17/2013
*   -Updated to use 2.00 libraries.
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServices {

    public abstract class _LTASessionServiceBase : _LTAServiceBase {

        public bool GetSession(string LogonName,
                               string Password,
                               out int UserID,
                               out Guid SessionID) {

            bool bolRetVal;

            var session = ManageLogon<ExternalLogonResponse>(operation => 
                { 
                    return operation.ExternalLogon(new ExternalLogonRequest{ Username = LogonName, Password = Password, Entity = string.Empty, IPAddress = "1.1.1.1"});
                });

            UserID = session.UserId;
            SessionID = session.Session;
            bolRetVal = session.Status == StatusCode.SUCCESS ? true : false;

            if (!bolRetVal)
                session.Errors
                    .ToList()
                    .ForEach(message => { EventLog.logEvent(message, "GetSession", MessageType.Error, MessageImportance.Essential); });

            return(bolRetVal);
        }

        public bool EndSession() {

            bool bolRetVal;

            GetHeaderItems();

            if (SessionId == null)
                throw new InvalidSessionIDException();

            var response = ManageLogon<SessionResponse>(operation => { return operation.EndSession(new SessionRequest { Session = SessionId }); });

            return bolRetVal = response.Status == StatusCode.SUCCESS ? true : false;
        }

        
   }
}