﻿using System.IO;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

namespace WFS.LTA.FileImportServicesAPI {
    public class BasicFileInformationProvider: IFileInformationProvider {
        private cEventLog EventLog { get; set; }

        public BasicFileInformationProvider(cEventLog eventLog) {
            EventLog = eventLog;
        }

        public long GetFileSize(string filename) {
            if (File.Exists(filename)) {
                var info = new FileInfo(filename);
                return info.Length;
            }
            else {
                EventLog.logEvent($"Unable to find attachment {filename}", this.GetType().Name, MessageType.Warning,
                    MessageImportance.Essential);
                return 0;
            }
        }
    }
}
