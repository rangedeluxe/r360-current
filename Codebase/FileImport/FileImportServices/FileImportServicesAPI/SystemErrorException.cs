﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.LTA.FileImport.FileImportServices
{
    public class SystemErrorException : OnlineFormattedException
    {
        public SystemErrorException()
            : base("A System Error Occurred")
        {
        }
    }
}
