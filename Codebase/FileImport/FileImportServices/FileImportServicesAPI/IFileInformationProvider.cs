﻿namespace WFS.LTA.FileImportServicesAPI {
    public interface IFileInformationProvider {
        long GetFileSize(string filename);
    }
}