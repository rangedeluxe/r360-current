﻿using System;
using System.IO;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:
*
* Modification History
* CR 54224 JMC 07/17/2012
*   -Initial Version.
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServicesAPI {

    public static class Support {

        internal const string NOTIFICATION_SCHEMA_TYPE = "Notification Import Integration Services";
        internal const string NOTIFICATION_STYLESHEET_TYPE = "Notification Import Integration Services Xsl";

        [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod () {

            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace ();
            System.Diagnostics.StackFrame sf = st.GetFrame(1);

            return sf.GetMethod().ToString();
        }

        public static string GetFileName(INotificationFile notificationFile, string cendsPath, cEventLog eventLog) {
            string strOutputFileName;
            eventLog.logEvent(
                "Receiving Notification File [" + notificationFile.FileIdentifier.ToString() + "]: " +
                notificationFile.UserFileName + "." + notificationFile.FileExtension, "FileImportHelper",
                MessageType.Information, MessageImportance.Verbose);

            if (notificationFile.BankID < 0) {
                throw (new Exception("Cannot save Notification File.  Invalid BankID defined: [" +
                                     notificationFile.BankID.ToString() + "]."));
            }

            if (notificationFile.CustomerID < -1) {
                throw (new Exception("Cannot save Notification File.  Invalid CustomerID defined: [" +
                                     notificationFile.CustomerID.ToString() + "]."));
            }
            eventLog.logEvent("CENDS PATH: " + cendsPath, "FileImportHelper", MessageType.Information,
                MessageImportance.Verbose);

            strOutputFileName = Path.Combine(cendsPath, notificationFile.NotificationDateKey.ToString());
            strOutputFileName = Path.Combine(strOutputFileName, notificationFile.BankID.ToString());

            if (notificationFile.LockboxID >= 0) {
                strOutputFileName = Path.Combine(strOutputFileName, "Lbx");
                strOutputFileName = Path.Combine(strOutputFileName, notificationFile.LockboxID.ToString());
            }
            else {
                strOutputFileName = Path.Combine(strOutputFileName, notificationFile.CustomerID.ToString());
            }

            strOutputFileName = Path.Combine(strOutputFileName,
                notificationFile.FileIdentifier + (notificationFile.FileExtension.StartsWith(".") ? "" : ".") +
                notificationFile.FileExtension);
            return strOutputFileName;
        }

        public static NotificationFileInfoContract NodeToNotificationFileInfoContracts(int bankId, int customerID, int notificationDateKey, DateTime notificationDate, int lockboxId, XmlNode fileNode) {
            return new NotificationFileInfoContract() {
                BankID=bankId,
                CustomerID = customerID,
                FileExtension = fileNode.Attributes["FileExtension"].Value,
                FileIdentifier = Guid.Parse(fileNode.Attributes["FileIdentifier"].Value),
                FileType = fileNode.Attributes["FileType"].Value,
                LockboxID = lockboxId,
                NotificationDateKey = notificationDateKey,
                NotificationDateUTC = notificationDate,
                UserFileName = fileNode.Attributes["UserFileName"].Value
            };
        }
    }
}
