﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.LTA.FileImport.FileImportServicesAPI
{
    public interface IFileImportRepository
    {
        /// <summary>
        /// Gets the notification data XSD.
        /// </summary>
        /// <param name="xsdName">Name of the XSD.</param>
        /// <param name="xsdVersion">The XSD version.</param>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        bool GetNotificationDataXsd(string xsdName, string xsdVersion, out DataTable dt);

        /// <summary>
        /// Inserts the notification.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns></returns>
        bool InsertNotification(string xml);

        /// <summary>
        /// Gets the notification responses.
        /// </summary>
        /// <param name="clientProcessCode">The client process code.</param>
        /// <param name="dtNotificationResponses">The dt notification responses.</param>
        /// <returns></returns>
        bool GetNotificationResponses(string clientProcessCode, out DataTable dtNotificationResponses);

        /// <summary>
        /// Sets the response complete.
        /// </summary>
        /// <param name="responseTrackingID">The response tracking ID.</param>
        /// <param name="entityTrackingID">The entity tracking ID.</param>
        /// <returns></returns>
        bool SetResponseComplete(Guid responseTrackingID, Guid entityTrackingID);
    }
}
