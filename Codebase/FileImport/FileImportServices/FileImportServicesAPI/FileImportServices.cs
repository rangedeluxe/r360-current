﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using WFS.LTA.FileImport.FileImportServices;
using WFS.LTA.FileImportServicesAPI;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;
using WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.R360Shared;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Chris Colombo
* Date:     08/04/2014
*
* Purpose:  
*
* Modification History
* WI 155242 CMC 08/04/2014
*    - New class to replace obsolete cFileImportServices class.
*    - Key refactors include:
*       1) abstracted data access layer (IFileImportRepository)
*       2) abstracted logon context (IExternalLogonClient)
*       3) abstracted site options
*       4) refactoring to remove logon dependencies      
*
******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServicesAPI
{
    public class FileImportServices : IDisposable
    {
        private const string ServiceKey = "FileImportServices";
        private cSiteOptions siteOptions = null;
        private cEventLog eventLog = null;

        private readonly IFileImportRepository repository;
        private readonly IExternalLogonService logonService;
        private readonly IAlertable alert; 
        private readonly IAuditable audit;

        private string siteKey = string.Empty;

        // Track whether Dispose has been called.
        private bool disposed = false;

        public FileImportServices(ServiceSetupContext paramServiceSetupContext)
            : this(paramServiceSetupContext, null) {
        }
        // Constructor
        public FileImportServices(ServiceSetupContext paramServiceSetupContext,
            IFileInformationProvider fileInformationProvider) {

            if (paramServiceSetupContext == null)
                throw new ArgumentNullException("paramServiceSetupContext", "Service setup context must be injected.");

            if (paramServiceSetupContext.LogonService == null)
                throw new ArgumentNullException("paramServiceSetupContext.LogonService",
                    "Logon service must be injected.");

            if (paramServiceSetupContext.SiteOptions == null)
                throw new ArgumentNullException("paramServiceSetupContext.SiteOptions", "Site options must be injected.");

            if (paramServiceSetupContext.FileImportRepository == null)
                throw new ArgumentNullException("paramServiceSetupContext.SiteOptions",
                    "Data Import repository must be injected.");

            if (paramServiceSetupContext.Alert == null)
                throw new ArgumentNullException("paramServiceSetupContext.Alert", "Alert module must be injected.");

            if (paramServiceSetupContext.Audit == null)
                throw new ArgumentNullException("paramServiceSetupContext.Audit", "Audit module must be injected.");

            logonService = paramServiceSetupContext.LogonService;
            siteOptions = paramServiceSetupContext.SiteOptions;
            repository = paramServiceSetupContext.FileImportRepository;
            alert = paramServiceSetupContext.Alert;
            audit = paramServiceSetupContext.Audit;
            if (fileInformationProvider == null) {
                FileInformationProvider = new BasicFileInformationProvider(EventLog);
            }
            else {
                FileInformationProvider = fileInformationProvider;
            }
        }

        public IFileInformationProvider FileInformationProvider { get; set; }

        public bool IsSessionValid(SessionRequest session)
        {
            return logonService.ValidateSession(session).Status == StatusCode.SUCCESS ? true : false;
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        private cEventLog EventLog
        {
            get
            {
                if (eventLog == null)
                {
                    eventLog = new cEventLog(siteOptions.logFilePath,
                                              siteOptions.logFileMaxSize,
                                              (MessageImportance)siteOptions.loggingDepth);
                }

                return eventLog;
            }
        }

        public bool GetSchemaDefinition(string schemaType, string schemaVersion,out XmlDocument schemaDoc) {

            bool bolRetVal;
            DataTable dt;
            XmlDocument docNotificationDataXsd;

            try {


                if (repository.GetNotificationDataXsd(schemaType, schemaVersion, out dt)) {
                    if (dt != null &&  dt.Rows.Count > 0 && !dt.Rows[0].IsNull("ImportSchema")) {
                        docNotificationDataXsd = new XmlDocument();
                        docNotificationDataXsd.LoadXml(dt.Rows[0]["ImportSchema"].ToString());
                        bolRetVal = true;
                    } else {
                        EventLog.logEvent("Notification Data Xsd was not found in the database.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        docNotificationDataXsd = null;
                        bolRetVal = false;
                    }
                } else {
                    EventLog.logEvent("Unable to retreive Notification Data Xsd from the database.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    docNotificationDataXsd = null;
                    bolRetVal = false;
                }

            } catch (Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "GetSchemaDefinition");
                throw (new SystemErrorException());
            }

            schemaDoc = docNotificationDataXsd;
            return (bolRetVal);
        }

        /// <summary>
        /// </summary>
        /// <param name="notificationDataXml"></param>
        /// <returns></returns>
        public bool SendNotificationData(XmlDocument notificationData) {

            string strNotificationXSDVersion;
            XmlDocument docXsd;
            XmlDocument docXsl;

            string strOptimizedNotificationData;

            bool bolRetVal;

            try {

                if(notificationData != null && notificationData.DocumentElement != null) {

                    if(notificationData.DocumentElement.Attributes["XSDVersion"] != null && 
                       (!string.IsNullOrEmpty(notificationData.DocumentElement.Attributes["XSDVersion"].Value))) {

                        strNotificationXSDVersion = notificationData.DocumentElement.Attributes["XSDVersion"].Value;
                    } else {
                        EventLog.logEvent("Could not read XSDVersion from source Xml.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        strNotificationXSDVersion = string.Empty;
                    }

                } else {
                    EventLog.logEvent("Could not read Document Element from source Xml.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    strNotificationXSDVersion = string.Empty;
                }

                if(!string.IsNullOrEmpty(strNotificationXSDVersion)) {

                    if(GetSchemaDefinition(Support.NOTIFICATION_SCHEMA_TYPE, strNotificationXSDVersion, out docXsd)) {

                        EventLog.logEvent("Validating Xml.", this.GetType().Name, MessageType.Information, MessageImportance.Verbose);

                        if(XML_XSD_Validator.Validate(notificationData.OuterXml, docXsd.OuterXml)) {
                            EventLog.logEvent("Xml Document was validated successfully.", this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
                            notificationData=PopulateFileSizes(notificationData, FileInformationProvider.GetFileSize);
                            // Get the Xsl document from the database so we can transform to database optimized format.
                            if(GetSchemaDefinition(Support.NOTIFICATION_STYLESHEET_TYPE, strNotificationXSDVersion, out docXsl)) {

                                EventLog.logEvent("Transforming Xml for database optimization.", this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
    
                                if(TransformXml(notificationData, docXsl, out strOptimizedNotificationData)) {
                                    bolRetVal = repository.InsertNotification(strOptimizedNotificationData);
                                } else {
                                    EventLog.logEvent("Xsl transformation failed.  Job will be rejected.", this.GetType().Name, MessageType.Warning, MessageImportance.Essential);
                                    bolRetVal = false;
                                }

                            } else {
                                EventLog.logEvent("Could not retrieve Xsl from the database.  Job will be rejected.", this.GetType().Name, MessageType.Warning, MessageImportance.Essential);
                                bolRetVal = false;
                            }
                            
                            
                            
                        } else {
                            EventLog.logEvent(XML_XSD_Validator.GetError(), this.GetType().Name, MessageType.Warning, MessageImportance.Essential);
                            EventLog.logEvent("Xml Document was not valid.  Job will be rejected.", this.GetType().Name, MessageType.Warning, MessageImportance.Essential);
                            bolRetVal = false;
                        }
                    } else {
                        EventLog.logEvent("Could not retrieve Xsd from the database.  Job will be rejected.", this.GetType().Name, MessageType.Warning, MessageImportance.Essential);
                        bolRetVal = false;
                    }
                } else {
                    bolRetVal = false;
                }

            } catch(OnlineFormattedException) {
                throw;
            } catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "SendNotificationData");
                throw(new SystemErrorException());
            }

            return(bolRetVal);
        }

        protected virtual XmlDocument PopulateFileSizes(XmlDocument docNotificationData, Func<string, long> getFileSize) {
            XmlDocument result = docNotificationData;

            if (result != null) {
                var xmlNotificationNodeList = result.SelectNodes("/Notifications/Notification");
                if (xmlNotificationNodeList != null) {
                    foreach (XmlElement notificationNode in xmlNotificationNodeList) {
                        int bankId = int.Parse(notificationNode.Attributes["BankID"].Value);
                        int customerId = int.Parse(notificationNode.Attributes["ClientGroupID"].Value);
                        int lockboxId = int.Parse(notificationNode.Attributes["ClientID"].Value);
                        DateTime notificationDate = DateTime.Parse(notificationNode.Attributes["NotificationDate"].Value);
                        int notificationDateKey = int.Parse(notificationDate.ToString("yyyyMMdd"));
                        var xmlFileNodeList = notificationNode.SelectNodes("./Files/File");
                        
                        if (xmlFileNodeList != null) {
                            foreach (XmlElement fileNode in xmlFileNodeList) {
                                var fileInfo = Support.NodeToNotificationFileInfoContracts(bankId, customerId,
                                    notificationDateKey, notificationDate, lockboxId, fileNode);
                                var filesize = getFileSize(Support.GetFileName(fileInfo, siteOptions.CENDSPath, EventLog));
                                fileNode.SetAttribute("FileSize", filesize.ToString());
                            }
                        }
                    }
                }


            }
            return result;
        }

        /// <summary>
        /// Save to PICS folder.
        /// </summary>
        /// <param name="ImageInfo"></param>
        /// <param name="BatchImage">/param>
        /// <returns></returns>
        public bool SendNotificationFile(INotificationFile notificationFile,
                                         Stream batchImageStream)
        {

            bool bolRetVal;
            string strOutputFileName;

            try
            {

                strOutputFileName = Support.GetFileName(notificationFile, siteOptions.CENDSPath, EventLog);

                if (!Directory.Exists(Path.GetDirectoryName(strOutputFileName)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(strOutputFileName));
                }

                // write the stream to a file.
                using (Stream file = File.OpenWrite(strOutputFileName))
                {
                    CopyStream(batchImageStream, file);
                }

                EventLog.logEvent("Notification File saved to disk: " + strOutputFileName, this.GetType().Name, MessageType.Information, MessageImportance.Verbose);

                bolRetVal = true;

            }
            catch (OnlineFormattedException ex)
            {

                EventLog.logError(ex, this.GetType().Name, "SendNotificationFile");
                throw;

            }
            catch (Exception ex)
            {

                EventLog.logError(ex, this.GetType().Name, "SendNotificationFile");
                throw (new SystemErrorException());


            }

            return (bolRetVal);
        }
        
        public AlertResponse LogAlert(AlertRequest requestContext)
        {
            return alert.Log(requestContext);
        }

        public AuditResponse LogAuditEvent(AuditRequest requestContext)
        {
            return audit.LogAuditEvent(requestContext);
        }


        private bool TransformXml(XmlDocument docXml, XmlDocument docXsl, out string transformedDoc) {

            XPathNavigator navXsl;
            XslCompiledTransform transXsl;
            XPathNavigator navXml;
            XPathDocument pathDocXml;

            bool bolRetVal;

            try {
                
                navXsl = docXsl.CreateNavigator();
                transXsl = new XslCompiledTransform();
                transXsl.Load(navXsl, null, null);
                navXml = docXml.CreateNavigator();

                //CR 51697 Created PathDocument from Document for speedier transformations.
                pathDocXml = new XPathDocument(new XmlNodeReader(docXml));
                navXml = pathDocXml.CreateNavigator();
                
                using (StringWriter sw = new StringWriter()) {
                    transXsl.Transform(pathDocXml,null, sw);
                    transformedDoc = sw.ToString();
                    bolRetVal = true;
                }

            } catch(Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "TransformXml");
                transformedDoc = string.Empty;
                bolRetVal = false;
            }

            return bolRetVal;
        }

        /// <summary>
        /// Copies the contents of input to output. Doesn't close either stream.
        /// </summary>
        private static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ( (len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }    
        }

        public bool GetNotificationResponses(string clientProcessCode, out XmlDocument respXmlDoc) {

            XmlDocument docRespXml;
            DataTable dtNotificationResponses;

            bool bolRetVal;

            if(repository.GetNotificationResponses(clientProcessCode, out dtNotificationResponses)) {
                if(dtNotificationResponses.Rows.Count > 0) {
                    docRespXml = new XmlDocument();
                    docRespXml.LoadXml(dtNotificationResponses.Rows[0]["XMLResponseDocument"].ToString());
                    bolRetVal = true;
                } else {
                    EventLog.logEvent("Notification responses were not returned from the database.", this.GetType().Name, MessageType.Warning, MessageImportance.Essential);
                    docRespXml = null;
                    bolRetVal = false;
                }
            } else {
                EventLog.logEvent("Unable to retrieve Notification responses from the database.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                docRespXml = null;
                bolRetVal = false;
            }

            respXmlDoc = docRespXml;

            return(bolRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uniqueIDResponseComplete"></param>
        /// <returns></returns>
        public bool SendResponseComplete(Guid responseTrackingID, Guid notificationTrackingID) {

            bool bolRetVal = false;

            try {

                if(repository.SetResponseComplete(responseTrackingID, notificationTrackingID)) {
                    bolRetVal = true;
                }

            } 
            catch (OnlineFormattedException) 
            {
                throw;
            } 
            catch (Exception ex) 
            {
                EventLog.logError(ex, this.GetType().Name, "SendResponseComplete");
                throw (new SystemErrorException());
            }

            return (bolRetVal);
        }


        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose() {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing) {
            // Check to see if Dispose has already been called.
            if(!this.disposed) {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if(disposing) {
                    
                }

                // Note disposing has been done.
                disposed = true;

            }
        }    
    }
}
