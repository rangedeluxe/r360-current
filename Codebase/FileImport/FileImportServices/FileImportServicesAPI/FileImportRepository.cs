﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DAL.FileImportServicesDAL;

namespace WFS.LTA.FileImport.FileImportServicesAPI
{
    public class FileImportRepository : IFileImportRepository
    {
        private readonly string siteKey;

        public FileImportRepository(string paramSiteKey)
        {
            siteKey = paramSiteKey;
        }

        public bool GetNotificationDataXsd(string xsdName, string xsdVersion, out System.Data.DataTable dt)
        {
            using (cFileImportServicesDAL dataAccess = new cFileImportServicesDAL(siteKey))
            {
                return dataAccess.GetNotificationDataXsd(xsdName, xsdVersion, out dt);
            }
        }

        public bool InsertNotification(string xml)
        {
            using (cFileImportServicesDAL dataAccess = new cFileImportServicesDAL(siteKey))
            {
                return dataAccess.InsertNotification(xml);
            }
        }

        public bool GetNotificationResponses(string clientProcessCode, out System.Data.DataTable dtNotificationResponses)
        {
            using (cFileImportServicesDAL dataAccess = new cFileImportServicesDAL(siteKey))
            {
                return dataAccess.GetNotificationResponses(clientProcessCode, out dtNotificationResponses);
            }
        }

        public bool SetResponseComplete(Guid responseTrackingID, Guid entityTrackingID)
        {
            using (cFileImportServicesDAL dataAccess = new cFileImportServicesDAL(siteKey))
            {
                return dataAccess.SetResponseComplete(responseTrackingID, entityTrackingID);
            }
        }
    }
}
