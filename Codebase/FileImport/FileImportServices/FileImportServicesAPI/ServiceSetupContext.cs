﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ImportToolkit.Common.Interfaces;

namespace WFS.LTA.FileImport.FileImportServicesAPI
{
    public class ServiceSetupContext
    {
        public IExternalLogonService LogonService { get; set; }
        public IFileImportRepository FileImportRepository { get; set; }
        public cSiteOptions SiteOptions { get; set; }
        public IAlertable Alert { get; set; }
        public IAuditable Audit { get; set; }
    }
}
