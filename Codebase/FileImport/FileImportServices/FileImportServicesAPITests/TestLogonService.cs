﻿using WFS.RecHub.ExternalLogon.Common;

namespace FileImportServicesAPITests {
    internal class TestLogonService:IExternalLogonService {
        public TestLogonService() {
        }
        public ExternalLogonResponse ExternalLogon(ExternalLogonRequest request) {
            throw new System.NotImplementedException();
        }
        public ExternalLogonResponse PermissionExternalLogon(PermissionLogonRequest request) {
            throw new System.NotImplementedException();
        }
        public SessionResponse ValidateSession(SessionRequest request) {
            throw new System.NotImplementedException();
        }
        public SessionResponse EndSession(SessionRequest request) {
            throw new System.NotImplementedException();
        }
        public PingResponse Ping() {
            throw new System.NotImplementedException();
        }
    }
}