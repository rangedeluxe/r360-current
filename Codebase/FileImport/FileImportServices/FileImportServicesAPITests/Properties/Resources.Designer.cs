﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FileImportServicesAPITests.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("FileImportServicesAPITests.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Notifications
        ///		SourceTrackingID=&quot;10BF0D22-5A3D-4741-84AA-01E485645777&quot;
        ///		ClientProcessCode=&quot;SimpleFileDrop_00&quot;
        ///		XSDVersion=&quot;1.00.00&quot;&gt;
        ///	&lt;Notification
        ///			BankID=&quot;9999&quot;
        ///			ClientGroupID=&quot;-1&quot;
        ///			ClientID=&quot;97&quot;
        ///			NotificationDate=&quot;2017-05-19 09:33:17Z&quot;
        ///			SourceNotificationID=&quot;C02FAE59-69D8-41C0-8967-C614E90FEA43&quot;
        ///			NotificationSourceKey=&quot;100&quot;
        ///			NotificationTrackingID=&quot;E2421726-98A5-4554-9203-2BAD881104DB&quot;&gt;
        ///		&lt;MessageParts&gt;
        ///			&lt;MessagePart&gt;Trish is testing notification XML with Attachment&lt;/Mess [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string EXAMPLE_XML_MULTIPLE_FILES {
            get {
                return ResourceManager.GetString("EXAMPLE_XML_MULTIPLE_FILES", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Notifications
        ///		SourceTrackingID=&quot;10BF0D22-5A3D-4741-84AA-01E485645777&quot;
        ///		ClientProcessCode=&quot;SimpleFileDrop_00&quot;
        ///		XSDVersion=&quot;1.00.00&quot;&gt;
        ///	&lt;Notification
        ///			BankID=&quot;9999&quot;
        ///			ClientGroupID=&quot;-1&quot;
        ///			ClientID=&quot;97&quot;
        ///			NotificationDate=&quot;2017-05-19 09:33:17Z&quot;
        ///			SourceNotificationID=&quot;C02FAE59-69D8-41C0-8967-C614E90FEA43&quot;
        ///			NotificationSourceKey=&quot;100&quot;
        ///			NotificationTrackingID=&quot;E2421726-98A5-4554-9203-2BAD881104DB&quot;&gt;
        ///		&lt;MessageParts&gt;
        ///			&lt;MessagePart&gt;Trish is testing notification XML with Attachment&lt;/Mess [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string EXAMPLE_XML_WITHOUT_FILES {
            get {
                return ResourceManager.GetString("EXAMPLE_XML_WITHOUT_FILES", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;Notifications
        ///		SourceTrackingID=&quot;10BF0D22-5A3D-4741-84AA-01E485645777&quot;
        ///		ClientProcessCode=&quot;SimpleFileDrop_00&quot;
        ///		XSDVersion=&quot;1.00.00&quot;&gt;
        ///	&lt;Notification
        ///			BankID=&quot;9999&quot;
        ///			ClientGroupID=&quot;-1&quot;
        ///			ClientID=&quot;97&quot;
        ///			NotificationDate=&quot;2017-05-19 09:33:17Z&quot;
        ///			SourceNotificationID=&quot;C02FAE59-69D8-41C0-8967-C614E90FEA43&quot;
        ///			NotificationSourceKey=&quot;100&quot;
        ///			NotificationTrackingID=&quot;E2421726-98A5-4554-9203-2BAD881104DB&quot;&gt;
        ///		&lt;MessageParts&gt;
        ///			&lt;MessagePart&gt;Trish is testing notification XML with Attachment&lt;/Mess [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ExampleXml {
            get {
                return ResourceManager.GetString("ExampleXml", resourceCulture);
            }
        }
    }
}
