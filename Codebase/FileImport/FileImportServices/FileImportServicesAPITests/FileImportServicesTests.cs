﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.LTA.FileImport.FileImportServicesAPI;
using WFS.LTA.FileImportServicesAPI;
using WFS.LTA.FileImportServicesAPI.Fakes;
using WFS.RecHub.Common;

namespace FileImportServicesAPITests {
    [TestClass]
    public class FileImportServicesTests {
        
        [TestMethod]
        public void SendNotificationData_ShouldPass_AddsFileSizeToASingleFileNode() {
            var notificationXml = "";
            var fileImportServices = CreateFileImportServices(
                xmlData => notificationXml = xmlData,
                filename => 21474836470
                );
            var doc = new XmlDocument();

            doc.LoadXml(Properties.Resources.ExampleXml);

            Assert.IsTrue(fileImportServices.SendNotificationData(doc));
            var fileSizeAttribute = XElement.Parse(notificationXml)
                .Descendants("File")
                .Single()
                .Attribute("FileSize");
            Assert.IsNotNull(fileSizeAttribute, "FileSize attribute was not found");
        }
        [TestMethod]
        public void SendNotificationData_ShouldPass_NoFileNodes() {
            var notificationXml = "";
            var fileImportServices = CreateFileImportServices(
                xmlData => notificationXml = xmlData,
                filename => 21474836470
                );
            var doc = new XmlDocument();

            doc.LoadXml(Properties.Resources.EXAMPLE_XML_WITHOUT_FILES);

            Assert.IsTrue(fileImportServices.SendNotificationData(doc));
        }
        [TestMethod]
        public void SendNotificationData_ShouldPass_MultipleFileNodes() {
            var notificationXml = "";
            var fileSizeQueue = new Queue<long>(new long[]{10, 20, 30});
            var fileImportServices = CreateFileImportServices(
                xmlData => notificationXml = xmlData,
                filename => fileSizeQueue.Dequeue()
                );
            var doc = new XmlDocument();

            doc.LoadXml(Properties.Resources.EXAMPLE_XML_MULTIPLE_FILES);

            Assert.IsTrue(fileImportServices.SendNotificationData(doc));
            var fileSizes = XElement.Parse(notificationXml)
                .Descendants("File")
                .Select(element => element.Attribute("FileSize")?.Value);
            Assert.AreEqual("10, 20, 30", string.Join(", ", fileSizes));
        }

        private static FileImportServices CreateFileImportServices(Action<string> onWriteNotification, Func<string, long> getFileSize) {
            var mockFileInformationProvider = new StubIFileInformationProvider() {
                GetFileSizeString = filename => getFileSize(filename)
            };
            
            return new FileImportServices(new ServiceSetupContext() {
                    LogonService = new TestLogonService(),
                    Alert = new TestAlert(),
                    Audit = new TestAudit(),
                    SiteOptions = new cSiteOptions(""),
                    FileImportRepository = new TestFileImportRepository() {
                        OnWriteNotification = onWriteNotification
                    }
                },
                mockFileInformationProvider
            );
        }
    }
}
