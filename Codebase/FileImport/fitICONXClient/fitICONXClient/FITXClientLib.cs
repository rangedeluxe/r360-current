﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WFS.LTA.Common;
using WFS.LTA.FileImportClientLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53790 JMC 08/29/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportServices.ICONXClient {

    internal static class FITXClientLib {

        public static LTAMessageType ConvertFITtoLTA(FITClientMessageType messageType) {

            LTAMessageType enmType;

            switch(messageType) {
                case FITClientMessageType.Information:
                    enmType = LTAMessageType.Information;
                    break;
                case FITClientMessageType.Warning:
                    enmType = LTAMessageType.Warning;
                    break;
                case FITClientMessageType.Error:
                    enmType = LTAMessageType.Error;
                    break;
                default:
                    enmType = LTAMessageType.Information;
                    break;
            }

            return(enmType);
        }

        public static LTAMessageImportance ConvertFITtoLTA(FITClientMessageImportance messageImportance) {

            LTAMessageImportance enmImportance;

            switch(messageImportance) {
                case FITClientMessageImportance.Essential:
                    enmImportance = LTAMessageImportance.Essential;
                    break;
                case FITClientMessageImportance.Verbose:
                    enmImportance = LTAMessageImportance.Verbose;
                    break;
                case FITClientMessageImportance.Debug:
                    enmImportance = LTAMessageImportance.Debug;
                    break;
                default:
                    enmImportance = LTAMessageImportance.Essential;
                    break;
            }

            return(enmImportance);
        }

        public static FITClientConsoleLTAMessageType ConvertFITtoFITClientConsoleLTA(FITClientMessageType messageType) {

            FITClientConsoleLTAMessageType enmMessageType;

            switch(messageType) {
                case FITClientMessageType.Information:
                    enmMessageType = FITClientConsoleLTAMessageType.Information;
                    break;
                case FITClientMessageType.Warning:
                    enmMessageType = FITClientConsoleLTAMessageType.Warning;
                    break;
                case FITClientMessageType.Error:
                    enmMessageType = FITClientConsoleLTAMessageType.Error;
                    break;
                default:
                    enmMessageType = FITClientConsoleLTAMessageType.TraceInfo;
                    break;
            }

            return(enmMessageType);
        }

        public static LTAConsoleMessageType ConvertFITtoConsoleLTA(FITClientConsoleLTAMessageType messageType) {

            LTAConsoleMessageType enmMessageType;

            switch(messageType) {
                case FITClientConsoleLTAMessageType.Information:
                    enmMessageType = LTAConsoleMessageType.Information;
                    break;
                case FITClientConsoleLTAMessageType.Warning:
                    enmMessageType = LTAConsoleMessageType.Warning;
                    break;
                case FITClientConsoleLTAMessageType.Error:
                    enmMessageType = LTAConsoleMessageType.Error;
                    break;
                case FITClientConsoleLTAMessageType.TraceInfo:
                    enmMessageType = LTAConsoleMessageType.TraceInfo;
                    break;
                default:
                    enmMessageType = LTAConsoleMessageType.Information;
                    break;
            }

            return(enmMessageType);
        }
    }
}
