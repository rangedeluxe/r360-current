﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53790 JMC 06/22/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportServices.ICONXClient {

    internal static class IniLib {
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="def"></param>
        /// <param name="retVal"></param>
        /// <param name="size"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(
            string section,
            string key,
            string def,
            StringBuilder retVal,
            int size,
            string filePath);
 
        /// <summary>
        /// Returns the path of the INI file that the executing assembly will use.
        /// </summary>
        private static string path {

            get { 
                string strFileName;

                strFileName=System.Configuration.ConfigurationManager.AppSettings["localIni"];

                return(strFileName);
            }
        }
        
        /// <summary>
        /// Reads Data Value From an Ini File.
        /// </summary>
        /// <param name="Section">Section where key is located.</param>
        /// <param name="Key">Key to be found.</param>
        /// <returns>Value underneath specified key and section.</returns>
        public static string IniReadValue(
            string section,
            string key) {

            StringBuilder sbValue = new StringBuilder(255);

            int i = GetPrivateProfileString(
                section,
                key,
                "",
                sbValue,
                255,
                path);

            return(sbValue.ToString());
        }
    }
}
