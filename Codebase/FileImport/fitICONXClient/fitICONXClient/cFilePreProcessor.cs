﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

using WFS.LTA.FileImportClientLib;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53790 JMC 06/22/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportServices.ICONXClient {

    public class cFilePreProcessor : IFileDropXClient {

        public event FITClientOutputLogEventHandler OutputLog;
        public event FITClientOutputTraceEventHandler OutputTrace;
        public event FITClientOutputLogAndTraceEventHandler OutputLogAndTrace;
        public event FITClientOutputLogAndTraceExceptionEventHandler OutputLogAndTraceException;

        private ConfigurationContext configurationContext;

        public cFilePreProcessor() {
        }
        
        private bool GetDefaultIDs(out int defaultBankId, out int defaultOrganizationId) {


            defaultBankId = -1;
            defaultOrganizationId = -100;

            if (this.configurationContext != null)
            {
                defaultBankId = configurationContext.BankId;
                defaultOrganizationId = configurationContext.OrganizationId;
            }

            if (defaultBankId > -1)
            {
                OnOutputLogAndTrace("Found ICON XCLient Default BankID: " + defaultBankId.ToString(), this.GetType().Name, FITClientMessageType.Information, FITClientMessageImportance.Debug);
            }
            else
            {
                OnOutputLogAndTrace("Did not find ICON XCLient Default BankID: ", this.GetType().Name, FITClientMessageType.Error, FITClientMessageImportance.Essential);
            }

            if (defaultOrganizationId > -2)
            {
                OnOutputLogAndTrace("Found ICON XCLient Default Organization: " + defaultOrganizationId.ToString(), this.GetType().Name, FITClientMessageType.Information, FITClientMessageImportance.Debug);
            }
            else
            {
                OnOutputLogAndTrace("Did not find ICON XCLient Default Organization: ", this.GetType().Name, FITClientMessageType.Error, FITClientMessageImportance.Essential);
            }

            if (defaultBankId > -1 && defaultOrganizationId > -2)
            {
                return (true);
            }
            else
            {
                OnOutputLogAndTrace("Failed to get bank and organization defaults: ", this.GetType().Name, FITClientMessageType.Error, FITClientMessageImportance.Essential);
                return (false);
            }
        }

        public bool GetNotificationXml(
            string inputFilePath, 
            string clientProcessCode, 
            out XmlDocument fileData, 
            out int notificationCount) {

            XmlDocument docFileData;
            bool bolRetVal;
            List<cNotification> arNotifications;
            List<cNotificationFile> arNotificationFiles;
            int intNotificationCount;

            try {

                if(BuildData(inputFilePath, out arNotifications)) {
                
                    if(NotificationLib.NotificationsToXml(Guid.NewGuid(), clientProcessCode, arNotifications, out docFileData)) {

                        arNotificationFiles = new List<cNotificationFile>();
                        foreach(cNotification notification in arNotifications) {
                            foreach(cNotificationFile notificationfile in notification.NotificationFiles) {
                                arNotificationFiles.Add(notificationfile);
                            }
                        }
                    
                        intNotificationCount = arNotifications.Count;

                        bolRetVal = true;
                    } else {
                        docFileData = null;
                        arNotificationFiles = null;
                        intNotificationCount = 0;
                        bolRetVal = false;
                    }

                } else {
                    docFileData = null;
                    arNotificationFiles = null;
                    intNotificationCount = 0;
                    bolRetVal = false;
                }

            } catch(Exception ex) {
                OnOutputLogAndTraceException("An unexpected exception occurred in BuildCanonicalXml()", this.GetType().Name, ex);
                docFileData = null;
                intNotificationCount = 0;
                bolRetVal = false;
            }

            fileData = docFileData;

            notificationCount = intNotificationCount;

            return(bolRetVal);
        }

        private bool BuildData(string inputFilePath, out List<cNotification> notifications) {

            bool bolRetVal;
            List<cNotification> arNotifications;
            int intDefaultBankID;
            int intDefaultCustomerID;

            try {

                OnOutputTrace("Reading default Bank and CustomerID from .ini file: " + inputFilePath, FITClientConsoleLTAMessageType.TraceInfo);
                if(!GetDefaultIDs(out intDefaultBankID, out intDefaultCustomerID)) {
                    notifications = null;
                    return(false);
                }
            
                OnOutputTrace("Reading input file: " + inputFilePath, FITClientConsoleLTAMessageType.TraceInfo);

                if(File.Exists(inputFilePath)) {

                    if(inputFilePath.EndsWith(".xml")) {
                        bolRetVal = ParseXml(inputFilePath, intDefaultBankID, intDefaultCustomerID, out arNotifications);
                    } else {
                        bolRetVal = ParseDat(inputFilePath, intDefaultBankID, intDefaultCustomerID, out arNotifications);
                    }

                } else {
                    throw new FileNotFoundException("File not found by XClient", inputFilePath);
                }

            } catch(Exception ex) {
                OnOutputLogAndTraceException("An unexpected exception occurred in BuildData()", this.GetType().Name, ex);
                arNotifications = null;
                bolRetVal = false;
            }

            notifications = arNotifications;

            return(bolRetVal);
        }

        private bool ParseXml(string inputFilePath, int defaultBankID, int defaultCustomerID, out List<cNotification> notifications) {

            List<cNotification> arNotifications;

            XmlDocument docXml;
            XmlNodeList nlFiles;
            cNotification objNotification;
            cNotificationFile objNotificationFile;
            XmlAttribute att;
            XmlNode nodeMsg;
            DateTime dteTemp;
            int intTemp;
            bool bolRetVal;

            try {

                OnOutputLogAndTrace("Parsing Xml file: " + inputFilePath, this.GetType().Name, FITClientMessageType.Information, FITClientMessageImportance.Verbose);

                docXml = new XmlDocument();
                docXml.PreserveWhitespace = true;
                docXml.Load(inputFilePath);

                arNotifications = new List<cNotification>();
            
                if(docXml.DocumentElement != null) {

                    nlFiles = docXml.DocumentElement.SelectNodes("file");
                    if(nlFiles.Count > 0) {
                    
                        foreach(XmlNode nodeFile in nlFiles) {

                            objNotification = new cNotification();
                            objNotificationFile = new cNotificationFile();

                            objNotification.BankID = defaultBankID;
                            objNotification.ClientGroupID = defaultCustomerID;

                            att = nodeFile.Attributes["doctype"];
                            if(att != null) {
                                objNotificationFile.FileType = att.Value;
                            }

                            att = nodeFile.Attributes["docdate"];
                            if(att != null && DateTime.TryParse(att.Value, out dteTemp)) {
                                objNotification.NotificationDate = dteTemp;
                            }

                            att = nodeFile.Attributes["clientid"];
                            if(att != null && int.TryParse(att.Value, out intTemp)) {
                                objNotification.ClientID = intTemp;
                            }

                            nodeMsg = docXml.DocumentElement.SelectSingleNode("message");
                            if(nodeMsg != null && nodeMsg.InnerText.Length > 0) {
                                objNotification.SetMessage(nodeMsg.InnerText);
                            }

                            att = nodeFile.Attributes["fullpath"];
                            if(att != null) {
                                objNotificationFile.FileIdentifier = Guid.NewGuid();
                                objNotificationFile.ClientFilePath = att.Value;
                                objNotificationFile.UserFileName = Path.GetFileNameWithoutExtension(att.Value);
                                objNotificationFile.FileExtension = Path.GetExtension(att.Value);
                            }

                            //att = nodeFile.Attributes["siteid"];

                            OnOutputLogAndTrace("Adding Notification to output Xml...", this.GetType().Name, FITClientMessageType.Information, FITClientMessageImportance.Verbose);

                            objNotification.AddNotificationFile(objNotificationFile);

                            arNotifications.Add(objNotification);
                        }

                    } else {
                        OnOutputLogAndTrace("No notification records were found in file: " + inputFilePath, this.GetType().Name, FITClientMessageType.Warning, FITClientMessageImportance.Essential);
                    }
                } else {
                    OnOutputLogAndTrace("Document element was NULL in file: " + inputFilePath, this.GetType().Name, FITClientMessageType.Warning, FITClientMessageImportance.Essential);
                }

                bolRetVal = true;

            } catch(Exception ex) {
                bolRetVal = false;
                arNotifications = null;
                OnOutputLogAndTraceException("An unexpected exception occurred in ParseXml()", this.GetType().Name, ex);
            }

            notifications = arNotifications;
            return(bolRetVal);
        }

        private bool ParseDat(string inputFilePath, int defaultBankID, int defaultCustomerID, out List<cNotification> notifications) {

            string strLine;
            int intIndex;
            string strKey;
            string strValue;

            DateTime dteTemp;
            int intTemp;

            List<cNotification> arNotifications;
            cNotification objNotification;
            cNotificationFile objNotificationFile;

            bool bolRetVal;

            try {

                using(StreamReader sr = new StreamReader(inputFilePath)) {

                    arNotifications = new List<cNotification>();

                    objNotification = null;
                    objNotificationFile = null;
                    
                    while((strLine = sr.ReadLine()) != null) {

                        if(strLine.Trim().ToLower().StartsWith(">>>")) {
                            //skip
                        } else if(strLine.Trim().ToLower().StartsWith("begin")) {
                            if(objNotification == null) {
                                objNotificationFile = null;
                            } else {
                                if(objNotificationFile != null) {
                                    objNotification.AddNotificationFile(objNotificationFile);
                                    objNotificationFile = null;
                                }
                                arNotifications.Add(objNotification);
                                objNotification = null;
                            }
                        } else {

                            intIndex = strLine.Trim().IndexOf(':');
                            if(intIndex > 0) {
                                strKey = strLine.Trim().ToLower().Substring(0, intIndex);
                                if(intIndex < strLine.Trim().Length) {
                                    strValue = strLine.Trim().ToLower().Substring(intIndex+1);
                                } else {
                                    strValue = string.Empty;
                                }

                                switch(strKey) {
                                    case ">>doctypename":

                                        if(objNotificationFile == null) {
                                            objNotificationFile = new cNotificationFile();
                                        }

                                        objNotificationFile.FileType = strValue;
                                        break;
                                    case ">>docdate":

                                        if(objNotification == null) {
                                            objNotification = new cNotification();
                                            objNotification.BankID = defaultBankID;
                                            objNotification.ClientGroupID = defaultCustomerID;
                                        }

                                        if(DateTime.TryParse(strValue, out dteTemp)) {
                                            objNotification.NotificationDate = dteTemp;
                                        } else {
                                            OnOutputLogAndTrace("Invalid value specified for DocDate: " + strValue + "]", this.GetType().Name, FITClientMessageType.Error, FITClientMessageImportance.Essential);
                                            throw(new InvalidDataException());
                                        }
                                        break;
                                    case "client id":

                                        if(objNotification == null) {
                                            objNotification = new cNotification();
                                            objNotification.BankID = defaultBankID;
                                            objNotification.ClientGroupID = defaultCustomerID;
                                        }

                                        if(int.TryParse(strValue, out intTemp)) {
                                            objNotification.ClientID = intTemp;
                                        } else {
                                            OnOutputLogAndTrace("Invalid value specified for client id: " + strValue + "]", this.GetType().Name, FITClientMessageType.Error, FITClientMessageImportance.Essential);
                                            throw(new InvalidDataException());
                                        }
                                        break;
                                    case ">>fullpath":

                                        if(objNotificationFile == null) {
                                            objNotificationFile = new cNotificationFile();
                                        }

                                        objNotificationFile.FileIdentifier = Guid.NewGuid();
                                        objNotificationFile.ClientFilePath = strValue;
                                        objNotificationFile.UserFileName = Path.GetFileNameWithoutExtension(strValue);
                                        objNotificationFile.FileExtension = Path.GetExtension(strValue);

                                        break;
                                    ////case "site id":
                                    ////    att = docFileData.CreateAttribute("siteid");
                                    ////    att.InnerText = strValue;
                                    ////    nodeNotification.Attributes.Append(att);
                                    ////    break;
                                    case "end":
                                        if(objNotification == null) {
                                            objNotificationFile = null;
                                        } else {
                                            if(objNotificationFile != null) {
                                                objNotification.AddNotificationFile(objNotificationFile);
                                                objNotificationFile = null;
                                            }
                                            arNotifications.Add(objNotification);
                                            objNotification = null;
                                        }
                                        break;
                                }
                            }
                        }
                    }

                    bolRetVal = true;
                }

            } catch(InvalidDataException) {
                bolRetVal = false;
                arNotifications = null;
            } catch(Exception ex) {
                bolRetVal = false;
                arNotifications = null;
                OnOutputLogAndTraceException("An unexpected exception occurred in ParseDat()", this.GetType().Name, ex);
            }

            notifications = arNotifications;
            
            return(bolRetVal);
        }

        public void OnOutputLog(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance) {
            if(OutputLog != null) {
                OutputLog(msg, src, messageType, messageImportance);
            } else {
                LTA.Common.LTAConsole.ConsoleWriteLine(msg, LTAConvert.ConvertLTALogToConsoleLTA(FITXClientLib.ConvertFITtoLTA(messageType)));
            }
        }

        public void OnOutputTrace(string msg, FITClientConsoleLTAMessageType messageType) {
            if(OutputTrace != null) {
                OutputTrace(msg, messageType);
            } else {
                LTA.Common.LTAConsole.ConsoleWriteLine(msg, FITXClientLib.ConvertFITtoConsoleLTA(messageType));
            }
        }

        public void OnOutputLogAndTrace(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance) {
            if(OutputLogAndTrace != null) {
                OutputLogAndTrace(msg, src, messageType, messageImportance);
            } else {
                OnOutputLog(msg, src, messageType, messageImportance);
                OnOutputTrace(msg, FITXClientLib.ConvertFITtoFITClientConsoleLTA(messageType));
            }
        }

        public void OnOutputLogAndTraceException(string msg, string src, Exception e) {
            if(OutputLogAndTraceException != null) {
                OutputLogAndTraceException(msg, src, e);
            } else {
                OnOutputLog(msg, src, FITClientMessageType.Error, FITClientMessageImportance.Essential);
                OnOutputLog(e.Message, src, FITClientMessageType.Error, FITClientMessageImportance.Essential);

                OnOutputTrace(msg, FITClientConsoleLTAMessageType.Error);
                OnOutputTrace(e.Message, FITClientConsoleLTAMessageType.Error);
            }
        }

        private class InvalidDataException : Exception {
        }

        public void Configure(ConfigurationContext configurationContext)
        {
            this.configurationContext = configurationContext;
        }
    }
}
