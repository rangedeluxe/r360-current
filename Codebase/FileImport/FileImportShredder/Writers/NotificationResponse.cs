﻿using System;

namespace FileImportShredder {
    public class NotificationResponse {
        public long NotificationId { get; set; }
        public Guid NotificationTrackingId { get; set; }
        public short ResponseStatus { get; set; }
        public string ResultsMessage { get; set; }
    }
}