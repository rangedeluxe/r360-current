﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileImportShredder {
    public interface INotificationWriter {
        void WriteNotification(Notification notificationData);
        void WriteNotificationFile(NotificationFile fileData);
        void WriteNotificationMessage(NotificationMessage messageData);
        void WriteNotificationResponse(NotificationResponse response);
    }
}
