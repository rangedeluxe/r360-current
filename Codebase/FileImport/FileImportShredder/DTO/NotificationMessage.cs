﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileImportShredder {
    public class NotificationMessage {
        public long NotificationId { get; set; }
        public int NotificationMessagePart { get; set; }
        public string MessageText { get; set; }
    }
}
