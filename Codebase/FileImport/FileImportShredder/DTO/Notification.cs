﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FileImportShredder {
    public class Notification {
        public long NotificationId { get; set; }
        public Guid NotificationTrackingId { get; set; }
        public int BankId { get; set; }
        public int ClientGroupId { get; set; }
        public int ClientId { get; set; }
        public DateTime NotificationDate { get; set; }
        public short NotificationSourceKey { get; set; }
        public Guid SourceNotificationId { get; set; }
        public Guid? MessageWorkgroups { get; set; }
    }
}
