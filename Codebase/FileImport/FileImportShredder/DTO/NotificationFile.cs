﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileImportShredder {
    public class NotificationFile {
        public long NotificationId { get; set; }
        public long NotificationFileId { get; set; }
        public string FileType { get; set; }
        public Guid FileIdentifier { get; set; }
        public string UserFileName { get; set; }
        public string FileExtension { get; set; }
        public long FileSize { get; set; }
    }
}
