﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FileImportShredder {
    public static class XmlLinqExtensions {
        public static string GetAttribute(this XElement element, string attribName, string defaultValue) {
            if (element.Attributes(attribName).Any()) {
                return element.Attribute(attribName)?.Value;
            }
            else {
                return defaultValue;
            }
        }

        public static int GetAttribute(this XElement element, string attribName, int defaultValue) {
            if (element.Attributes(attribName).Any()) {
                return int.Parse(element.Attribute(attribName).Value);
            }
            else {
                return defaultValue;
            }
        }

        public static long GetAttribute(this XElement element, string attribName, long defaultValue) {
            if (element.Attributes(attribName).Any()) {
                return long.Parse(element.Attribute(attribName).Value);
            }
            else {
                return defaultValue;
            }
        }

        public static short GetAttribute(this XElement element, string attribName, short defaultValue) {
            if (element.Attributes(attribName).Any()) {
                return short.Parse(element.Attribute(attribName).Value);
            }
            else {
                return defaultValue;
            }
        }

        public static DateTime GetAttribute(this XElement element, string attribName, DateTime defaultValue) {
            if (element.Attributes(attribName).Any()) {
                return DateTime.Parse(element.Attribute(attribName).Value).ToUniversalTime();
            }
            else {
                return defaultValue;
            }
        }

        public static Guid GetAttribute(this XElement element, string attribName, Guid defaultValue) {
            if (element.Attributes(attribName).Any()) {
                return Guid.Parse(element.Attribute(attribName).Value);
            }
            else {
                return defaultValue;
            }
        }
    }
}
