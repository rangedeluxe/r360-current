﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using WFS.LTA.DataImport;

namespace FileImportShredder
{
    public class FITShredder:BaseShredder
    {
        public FITShredder(XmlReader xml, long dataImportQueueId, INotificationWriter writer) : base(xml) {
            Writer = writer;
            DataImportQueueId = dataImportQueueId;
        }

        private INotificationWriter Writer { get; }

        private long DataImportQueueId { get; }

        public void ProcessAll() {
            var doc = XDocument.Load(Xml);
            Writer.WriteNotification(ParseNotificationNode(doc.Root));
            foreach (var file in ParseNotificationFileNodes(doc.Root)) {
                Writer.WriteNotificationFile(file);
            }
            foreach (var messageNode in ParseNotificationMessageNodes(doc.Root)) {
                Writer.WriteNotificationMessage(messageNode);
            }
        }
        private IEnumerable<NotificationMessage> ParseNotificationMessageNodes(XElement node) {
            var messagePartNodes = node.Elements("MessageParts").SelectMany(parts=>
                parts.Elements("MessagePart"));
            foreach (var badMessagePartNode in messagePartNodes.Where(messageNode => messageNode.Value.Length > 128)) {
                Writer.WriteNotificationResponse(new NotificationResponse() {
                    NotificationId = DataImportQueueId,
                    NotificationTrackingId = node.GetAttribute("NotificationTrackingID", Guid.Empty),
                    ResponseStatus = 1,
                    ResultsMessage = $"Message Part {badMessagePartNode.GetAttribute("NotificationMessagePart", -1)} is greater then 128 bytes. (Actual Length is {badMessagePartNode.Value.Length}.) Correct and resubmit."
                });
            }
            return messagePartNodes.Where(messageNode=>messageNode.Value.Length < 129).Select(
                goodMessageNode=>new NotificationMessage() {
                    NotificationId = DataImportQueueId,
                    NotificationMessagePart = goodMessageNode.GetAttribute("NotificationMessagePart", -1),
                    MessageText = goodMessageNode.Value
                }
            );
        }
        private IEnumerable<NotificationFile> ParseNotificationFileNodes(XElement node) {
            return node.Elements("Files").SelectMany(
                files=>files.Elements("File").Select(
                    fileNode => new NotificationFile() {
                        NotificationId = DataImportQueueId,
                        FileExtension = fileNode.GetAttribute("FileExtension", ""),
                        FileSize=fileNode.GetAttribute("FileSize", -1),
                        FileIdentifier = fileNode.GetAttribute("FileIdentifier", Guid.Empty),
                        FileType = fileNode.GetAttribute("FileType", ""),
                        NotificationFileId = fileNode.GetAttribute("File_Id", (long)-1),
                        UserFileName = fileNode.GetAttribute("UserFileName", "")
                    }
                )
            );
        }
        private Notification ParseNotificationNode(XElement node) {
            var sourceNotificationID = node.GetAttribute("SourceNotificationID", Guid.Empty);

            if (sourceNotificationID == Guid.Empty) {
                sourceNotificationID = Guid.NewGuid();
            }

            Guid? MessageWorkgroups = node.GetAttribute("MessageWorkgroups", Guid.Empty);
            if (MessageWorkgroups == Guid.Empty)
                MessageWorkgroups = null;

            return new Notification() {
                NotificationId = DataImportQueueId,
                ClientGroupId = node.GetAttribute("ClientGroupID", -1),
                NotificationDate = node.GetAttribute("NotificationDate", DateTime.MinValue),
                SourceNotificationId = sourceNotificationID,
                NotificationTrackingId = node.GetAttribute("NotificationTrackingID", Guid.Empty),
                ClientId = node.GetAttribute("ClientID", -1),
                NotificationSourceKey = node.GetAttribute("NotificationSourceKey", (short)-1),
                BankId = node.GetAttribute("BankID", -1),
                MessageWorkgroups = MessageWorkgroups
            };
        }
        private NotificationMessage[] GetMessageParts(XmlReader xml) {
            var results = new List<NotificationMessage>();
            while (xml.Read()) {
                if (xml.Name == "MessagePart" && xml.IsStartElement()) {
                    var partIndex = GetInt32("NotificationMessagePart");

                    xml.Read();
                    results.Add(new NotificationMessage() {
                        NotificationId = DataImportQueueId,
                        MessageText = xml.Value,
                        NotificationMessagePart = partIndex
                    });
                }
            }
            return results.ToArray();
        }
    }
}
