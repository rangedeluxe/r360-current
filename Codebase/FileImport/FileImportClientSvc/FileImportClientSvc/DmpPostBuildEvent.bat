echo Started DmpPostBuildEvent processing.

REM =================================================================
REM Developer Customization Required!
REM Change SUBASSEMBLYLIST to match the list of non-Windows references 
REM for your assembly. You must use the complete path, and use a space 
REM between each assembly listed. Use the value NONE when there are no 
REM sub-assemblies to be merged.
REM =================================================================

REM Get parameters: $(TargetFileName) $(TargetName) $(TargetExt) $(ConfigurationName)
set ASSEMBLYFILE=%1
set ASSEMBLYNAME=%2
set ASSEBMLYTYPE=%3
set BUILDMODE=%4
echo Parameters: %ASSEMBLYFILE% %ASSEMBLYNAME% %ASSEBMLYTYPE% %BUILDMODE% 
					
set SUBASSEMBLYLIST=.\R360Shared.dll ^
				    .\ipoLib.dll ^
				    .\SessionDAL.dll ^
					.\ipoLog.dll ^
					.\DALBase.dll ^
					.\ltaLog.dll ^
					.\FileImportWCFLib.dll ^
					.\ltaCommon.dll ^
					.\ipoCrypto.dll ^
					.\WFS.RecHub.ApplicationBlocks.Common.dll ^
					.\FileImportClientAPI.dll ^
					.\ImportToolkitCommon.dll ^
					.\FileImportServicesClient.dll
					
REM =================================================================
echo Sub-Assemblies: %SUBASSEMBLYLIST%

REM do not run in debug mode
if "%BUILDMODE%"=="Debug" goto SnDone

if "%ASSEBMLYTYPE%"==".exe" goto UsingEXE
if "%ASSEBMLYTYPE%"==".EXE" goto UsingEXE
if "%ASSEBMLYTYPE%"==".dll" goto UsingDLL
if "%ASSEBMLYTYPE%"==".DLL" goto UsingDLL

:UsingDLL
set ASSEMBLYEXT=dll
goto UsingExtDone

:UsingEXE
set ASSEMBLYEXT=exe
goto UsingExtDone

:UsingExtDone

echo Deleting previous build ilmerge result files.
if exist ILMergeResults.txt del ILMergeResults.txt

echo Deleting previous build non-merged assembly file.
if exist %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% del %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT%
if exist %ASSEMBLYNAME%_nonmerged.pdb del %ASSEMBLYNAME%_nonmerged.pdb

if "%SUBASSEMBLYLIST%"=="NONE" goto SkipILMerge
if "%SUBASSEMBLYLIST%"=="none" goto SkipILMerge

echo Renaming the current assembly to a temp name for merging.
ren %ASSEMBLYFILE% %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT%
if exist %ASSEMBLYNAME%.pdb ren %ASSEMBLYNAME%.pdb %ASSEMBLYNAME%_nonmerged.pdb

echo Running ILMERGE to combine assemblies.
if "%BUILDMODE%"=="Debug" goto ILMergeDebug
if "%BUILDMODE%"=="Release" goto ILMergeRelease

:ILMergeRelease
REM--------------------------------------------------------------------------------------------------------------
REM  The following two lines are here because Microsoft re-wrote system.servicemodel between framework 4.0 and 4.5
REM   this is causing a problem if you ilmerge on a machine with framework 4.5 and execute on a
REM    machine with only framework 4.0
REM  At some time in the future (when 4.0 is not allowed) then the commented out lines should be used.
REM--------------------------------------------------------------------------------------------------------------
start /wait c:\tools\SR_2005\ilmerge /targetplatform:"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0" /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt
::start /wait c:\tools\SR_2005\ilmerge /targetplatform:"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0" /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt /delaysign /keyfile:..\..\..\..\..\..\SharedDependencies\keys\publickey.snk /allowDup
::start /wait C:\Tools\ILMerge_2.12.0803\ilmerge /targetplatform:"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0" /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt /delaysign /keyfile:..\..\..\..\..\..\SharedDependencies\keys\publickey.snk /allowDup
REM ::start /wait c:\tools\SR_2005\ilmerge /targetplatform:v4,C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319 /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt /delaysign /keyfile:..\..\..\..\..\..\SharedDependencies\keys\publickey.snk /allowDup
REM start /wait C:\Tools\ILMerge_2.12.0803\ilmerge /targetplatform:v4,C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319 /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt /delaysign /keyfile:..\..\..\..\..\..\SharedDependencies\keys\publickey.snk /allowDup
goto ILMergeDone

:ILMergeDebug
REM--------------------------------------------------------------------------------------------------------------
REM  The following two lines are here because Microsoft re-wrote system.servicemodel between framework 4.0 and 4.5
REM   this is causing a problem if you ilmerge on a machine with framework 4.5 and execute on a
REM    machine with only framework 4.0
REM  At some time in the future (when 4.0 is not allowed) then the commented out lines should be used.
REM--------------------------------------------------------------------------------------------------------------
::start /wait c:\tools\SR_2005\ilmerge /targetplatform:"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0" /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt /delaysign /keyfile:..\..\..\..\..\..\SharedDependencies\keys\publickey.snk /allowDup
start /wait C:\Tools\ILMerge_2.12.0803\ilmerge /targetplatform:"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0" /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt /delaysign /keyfile:..\..\..\..\..\..\SharedDependencies\keys\publickey.snk /allowDup
REM ::start /wait c:\tools\SR_2005\ilmerge /targetplatform:v4,C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319 /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt /delaysign /keyfile:..\..\..\..\..\..\SharedDependencies\keys\publickey.snk /allowDup
REM start /wait C:\Tools\ILMerge_2.12.0803\ilmerge /targetplatform:v4,C:\WINDOWS\Microsoft.NET\Framework\v4.0.30319 /ndebug /internalize %ASSEMBLYNAME%_nonmerged.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:%ASSEMBLYFILE% /log:ILMergeResults.txt /delaysign /keyfile:..\..\..\..\..\..\SharedDependencies\keys\publickey.snk /allowDup
goto ILMergeDone

:ILMergeDone

:SkipILMerge

echo Running SN to sign the combined assembly.
if "%BUILDMODE%"=="Debug" goto SnDebug
if "%BUILDMODE%"=="Release" goto SnRelease

:::SnDebug
::start /wait C:\Tools\sn.exe -Vr %ASSEMBLYFILE%
::goto SnDone

:SnRelease
::start /wait C:\Tools\sn.exe -R %ASSEMBLYFILE% ..\..\..\..\..\..\SharedDependencies\keys\publicprivatekeypair.snk
goto SnDone

:SnDone

echo Completed DmpPostBuildEvent processing.
