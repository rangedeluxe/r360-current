﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using WFS.LTA.Common;
using WFS.LTA.FileImportClient;
using WFS.LTA.FileImportClientAPI;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 54178 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClient
{

	public partial class FileImportClientSvc : ServiceBase
	{

		private cClientService _ClientService;
		private cClientOptions _ClientOptions = null;
		private ltaLog _EventLog = null;

		/// <summary>
		/// 
		/// </summary>
		private ltaLog LTAEventLog
		{
			get
			{
				if (_EventLog == null)
				{
					// Read the app.settings directly rather than using the Client Options as the cClientOptions.GetSettings could
					// be what errored out, if it the user/password did not decode.
					int depth = 0;
					int fileMaxSize = 0;
					int.TryParse(ConfigurationManager.AppSettings.Get(cClientOptions.LOGGING_DEPTH), out depth);
					int.TryParse(ConfigurationManager.AppSettings.Get(cClientOptions.LOG_FILE_MAX_SIZE), out fileMaxSize);


					_EventLog = new ltaLog(ConfigurationManager.AppSettings.Get(cClientOptions.LOG_FILE),
												fileMaxSize,
												(LTAMessageImportance)depth);
				}

				return _EventLog;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		private cClientOptions ClientOptions
		{
			get
			{
				if (_ClientOptions == null)
				{
					_ClientOptions = new cClientOptions();
				}
				return _ClientOptions;
			}
		}

		public FileImportClientSvc()
		{

			try
			{
				InitializeComponent();

				_ClientService = new cClientService(ClientOptions);

				_ClientService.OutputLog += XProc_LogEvent;
				_ClientService.OutputTrace += XProc_TraceEvent;
			}
			catch (Exception ex)
			{
				LTAEventLog.logEvent(ex.Message, "FileImportClientSvc", LTAMessageType.Error, LTAMessageImportance.Essential);
			}
		}


		private void XProc_LogEvent(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance)
		{
			LTAEventLog.logEvent(msg, src, messageType, messageImportance);
		}

		private static void XProc_TraceEvent(string msg, LTAConsoleMessageType consoleMessageType)
		{
			LTAConsole.ConsoleWriteLine(msg, consoleMessageType);
		}

		private void InitializeProcessor(string[] args)
		{
			_ClientService.StartService(args);
		}
		

		protected override void OnStart(string[] args)
		{
			// NOTE: Need to move this to anthor thread as we allow continuous retries due to 
			// see: https://stackoverflow.com/questions/31529508/windows-service-execute-logic-after-onstart
			new Thread(() =>
			{
				Thread.CurrentThread.IsBackground = true;
				InitializeProcessor(args);

			}).Start(); 
		}

		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{
			try
			{

				_ClientService.StopService();

				LTAEventLog.logEvent("File Import Client Service stopped."
								, this.GetType().Name
								, LTAMessageImportance.Essential);

			}
			catch (Exception e)
			{
				LTAEventLog.logEvent("File Import Client Service stopped with errors : " + e.Message
								, e.Source
								, LTAMessageType.Error
								, LTAMessageImportance.Essential);
			}
			finally
			{
				_ClientService.Dispose();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		protected override void OnPause()
		{
			try
			{

				_ClientService.StopService();

				LTAEventLog.logEvent("File Import Client Service paused."
								, this.GetType().Name
								, LTAMessageImportance.Essential);

			}
			catch (Exception e)
			{
				LTAEventLog.logEvent("File Import Client Service paused with errors : " + e.Message
								, e.Source
								, LTAMessageType.Error
								, LTAMessageImportance.Essential);
			}
			finally
			{
				_ClientService.Dispose();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		protected override void OnContinue()
		{
			try
			{

				_ClientService.StartService(new string[] { });

				LTAEventLog.logEvent("File Import Client Service resumed."
								, this.GetType().Name
								, LTAMessageImportance.Essential);

			}
			catch (Exception e)
			{
				LTAEventLog.logEvent("File Import Client Service continued with errors : " + e.Message
								, e.Source
								, LTAMessageType.Error
								, LTAMessageImportance.Essential);
			}
		}
	}
}