﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

using WFS.LTA.FileImportClientLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    internal abstract class _ClientProcessOptionsBase : IClientProcessOptions {

        private string _ClientProcessCode;
        private string _PendingResponseFolder = string.Empty;
        private string _InProcessFolder = string.Empty;
        private string _ResponseFolder = string.Empty;
        private string _InputErrorFolder = string.Empty;
        private string _ResponseErrorFolder = string.Empty;
        private string _ArchiveFolder = string.Empty;
        private string _XClientDllFilePath = string.Empty;
        private string _IniSectionKey = string.Empty;

        public _ClientProcessOptionsBase(ProcessElement processConfigSettings) {

            _ClientProcessCode = processConfigSettings.ClientProcessCode;
            _PendingResponseFolder = processConfigSettings.PendingResponseFolder;
            _InProcessFolder = processConfigSettings.InProcessFolder;
            _ResponseFolder = processConfigSettings.ResponseFolder;
            _InputErrorFolder = processConfigSettings.InputErrorFolder;
            _ResponseErrorFolder = processConfigSettings.ResponseErrorFolder;
            _ArchiveFolder = processConfigSettings.ArchiveFolder;
            _XClientDllFilePath = processConfigSettings.XClientDllFilePath;
            _IniSectionKey = processConfigSettings.IniSectionKey;
            var defaultBankId = 0;
            var defaultOrganizationId = 0;
            int.TryParse(processConfigSettings.DefaultBankId, out defaultBankId);
            int.TryParse(processConfigSettings.DefaultOrganizationId, out defaultOrganizationId);
            DefaultBankId = defaultBankId;
            DefaultOrganizationId = defaultOrganizationId;
        }

        public abstract int ClientType {
            get;
        }

        public string ClientProcessCode {
            get {
                return(_ClientProcessCode);
            }
            set {
                _ClientProcessCode = value;
            }
        }

        public string PendingResponseFolder {
            get {
                return(_PendingResponseFolder);
            }
            set {
                _PendingResponseFolder = value;
            }
        }

        public string InProcessFolder {
            get {
                return(_InProcessFolder);
            }
            set {
                _InProcessFolder = value;
            }
        }

        public string ResponseFolder {
            get {
                return(_ResponseFolder);
            }
            set {
                _ResponseFolder = value;
            }
        }

        public string InputErrorFolder {
            get {
                return(_InputErrorFolder);
            }
            set {
                _InputErrorFolder = value;
            }
        }

        public string ResponseErrorFolder {
            get {
                return(_ResponseErrorFolder);
            }
            set {
                _ResponseErrorFolder = value;
            }
        }

        public string ArchiveFolder {
            get {
                return(_ArchiveFolder);
            }
            set {
                _ArchiveFolder = value;
            }
        }

        public string XClientDllFilePath {
            get {
                return(_XClientDllFilePath);
            }
            set {
                _XClientDllFilePath = value;
            }
        }

        public string IniSectionKey {
            get {
                return(_IniSectionKey);
            }
            set {
                _IniSectionKey = value;
            }
        }

        public int DefaultBankId
        {
            get;
            set;
        }

        public int DefaultOrganizationId
        {
            get;
            set;
        }

    }
}
