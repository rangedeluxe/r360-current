﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    internal class cFileImportSettingsConfigSection : ConfigurationSection {
        /// <summary>
        /// The value of the property here "Process" needs to match that of the config file section
        /// </summary>
        [ConfigurationProperty("FileImportConfigSections")]
        public ProcessCollection ProcessItems {
            get { 
                return ((ProcessCollection)(base["FileImportConfigSections"])); 
            }
        }
    }

    /// <summary>
    /// The collection class that will store the list of each element/item that
    ///        is returned back from the configuration manager.
    /// </summary>
    [ConfigurationCollection( typeof( ProcessElement ) )]
    internal class ProcessCollection : ConfigurationElementCollection {

        protected override ConfigurationElement CreateNewElement() {
            return new ProcessElement();
        }
 
        protected override object GetElementKey( ConfigurationElement element ) {
            return ( (ProcessElement)( element ) ).ClientProcessCode;
        }
 
        public ProcessElement this[int idx ] {
            get {
                return (ProcessElement) BaseGet(idx);
            }
        }
    }

    /// <summary>
    /// The class that holds onto each element returned by the configuration manager.
    /// </summary>
    internal class ProcessElement : ConfigurationElement {
 
        private const string CONFIG_PROCESS_CLIENT_PROCESS_CODE_KEY = "ClientProcessCode";
        private const string CONFIG_PROCESS_XCLIENT_TYPE_KEY = "XClientType";
        private const string CONFIG_PROCESS_INPUT_PATH_KEY = "InputFolder";
        private const string CONFIG_PROCESS_INPROCESS_PATH_KEY = "InProcessFolder";
        private const string CONFIG_PROCESS_PENDING_RESPONSE_FOLDER_KEY = "PendingResponseFolder";
        private const string CONFIG_PROCESS_RESPONSE_FOLDER_KEY = "ResponseFolder";
        private const string CONFIG_PROCESS_INPUT_ERROR_FOLDER_KEY = "InputErrorFolder";
        private const string CONFIG_PROCESS_RESPONSE_ERROR_FOLDER_KEY = "ErrorResponseFolder";
        private const string CONFIG_PROCESS_ARCHIVE_FOLDER_KEY = "ArchiveFolder";
        private const string CONFIG_PROCESS_FILE_PATTERN_KEY = "FilePattern";
        private const string CONFIG_PROCESS_XCLIENT_DLL_KEY = "XClientDllFilePath";
        private const string CONFIG_PROCESS_INI_SECTION_KEY = "IniSectionKey";
        private const string CONFIG_PROCESS_DEFAULT_BANK_ID = "DefaultBankID";
        private const string CONFIG_PROCESS_DEFAULT_ORGANIZATION_ID = "DefaultOrganizationID";

        [ConfigurationProperty(CONFIG_PROCESS_CLIENT_PROCESS_CODE_KEY, DefaultValue="", IsKey=true, IsRequired=true)]
        public string ClientProcessCode {
            get {
                return ((string) (base[CONFIG_PROCESS_CLIENT_PROCESS_CODE_KEY]));
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_XCLIENT_TYPE_KEY, DefaultValue = "", IsKey = false, IsRequired = true)]
        public string XClientType {
            get {
                return ( (string)( base[ CONFIG_PROCESS_XCLIENT_TYPE_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_INPUT_PATH_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string InputFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_INPUT_PATH_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_INPROCESS_PATH_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string InProcessFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_INPROCESS_PATH_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_PENDING_RESPONSE_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string PendingResponseFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_PENDING_RESPONSE_FOLDER_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_RESPONSE_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ResponseFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_RESPONSE_FOLDER_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_INPUT_ERROR_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string InputErrorFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_INPUT_ERROR_FOLDER_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_RESPONSE_ERROR_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string ResponseErrorFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_RESPONSE_ERROR_FOLDER_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_ARCHIVE_FOLDER_KEY, DefaultValue = "", IsKey = false, IsRequired = true)]
        public string ArchiveFolder {
            get {
                return ( (string)( base[ CONFIG_PROCESS_ARCHIVE_FOLDER_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_FILE_PATTERN_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string FilePattern {
            get {
                return ( (string)( base[ CONFIG_PROCESS_FILE_PATTERN_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_XCLIENT_DLL_KEY, DefaultValue = "", IsKey = false, IsRequired = true)]
        public string XClientDllFilePath {
            get {
                return ( (string)( base[ CONFIG_PROCESS_XCLIENT_DLL_KEY ] ) );
            }
        }
 
        [ConfigurationProperty( CONFIG_PROCESS_INI_SECTION_KEY, DefaultValue = "", IsKey = false, IsRequired = false)]
        public string IniSectionKey {
            get {
                return ( (string)( base[ CONFIG_PROCESS_INI_SECTION_KEY ] ) );
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_DEFAULT_BANK_ID, DefaultValue = "-1", IsKey = false, IsRequired = false)]
        public string DefaultBankId
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_DEFAULT_BANK_ID]));
            }
        }

        [ConfigurationProperty(CONFIG_PROCESS_DEFAULT_ORGANIZATION_ID, DefaultValue = "-1", IsKey = false, IsRequired = false)]
        public string DefaultOrganizationId
        {
            get
            {
                return ((string)(base[CONFIG_PROCESS_DEFAULT_ORGANIZATION_ID]));
            }
        }
    }
}
