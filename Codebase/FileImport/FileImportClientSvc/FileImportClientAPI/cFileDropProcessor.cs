﻿//#define use_local

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

using WFS.LTA.FileImport.FileImportServicesClient;
using WFS.LTA.FileImportClientLib;
using WFS.LTA.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
* WI 70351 CRG 02/01/2013
*	changed Assembly load file to load from.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    internal class cFileDropProcessor : _FileImportProcessorBase {

#if !use_local
        private IFileDropXClient _XClient = null;
#endif

        private cFileDropClientProcessOptions _FileDropClientProcessOptions = null;
        
        private System.Collections.Generic.Dictionary<string, int> _FileRetryQueue = null;

        public cFileDropProcessor(cClientOptions clientOptions, IClientProcessOptions xProc) : base(clientOptions, xProc) 
        {
        }

        public cFileDropProcessor(cClientOptions clientOptions, IClientProcessOptions xProc, FileDataServiceClient dataServiceClient)
            : base(clientOptions, xProc, dataServiceClient)
        {
        }

        private System.Collections.Generic.Dictionary<string, int> FileRetryQueue {
            get {
                if(_FileRetryQueue == null) {
                    _FileRetryQueue = new System.Collections.Generic.Dictionary<string, int>();
                }
                return(_FileRetryQueue);
            }
        }

        public bool ProcessPendingFiles(out int FilesProcessed) {
            
            DirectoryInfo directoryInfo;
            List<FileInfo> arFiles;
            int intFilesProcessed;
            bool bolRetVal;
            bool bolContinue;
            string[] arFilePatterns;

            intFilesProcessed = 0;

            try {

                _FileDropClientProcessOptions = (cFileDropClientProcessOptions)this.ClientProcessOptions;

                arFilePatterns = _FileDropClientProcessOptions.FilePattern.Split(',');

                foreach(string filepattern in arFilePatterns) {

                    bolContinue = true;

                    while(bolContinue) {

                        lock(this) {

                            directoryInfo = new DirectoryInfo(_FileDropClientProcessOptions.InputFolder);
                            arFiles = directoryInfo.GetFiles(filepattern.Trim()).OrderBy(t => t.LastWriteTime).ToList();

                            if(arFiles.Count > 0) {

                                System.Threading.Thread.Sleep(2000);

                                foreach(FileInfo fileinfo in arFiles) {
                                    ProcessFile(fileinfo);
                                    intFilesProcessed++;
                                }
                            } else {
                                bolContinue = false;
                            }
                        }
                    }
                }

                bolRetVal = true;

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in ProcessPendingFiles()", this.GetType().Name, ex);
                bolRetVal = false;
            }

            FilesProcessed = intFilesProcessed;
            return(bolRetVal);
        }

        private bool ProcessFile(FileInfo pendingFileInfo) {

            XmlDocument docNotificationData;
            FileUploadServiceClient objFileUploadClient;
            Guid gidSessionID;
            int userid;

            int intNotificationCount;

            bool bolRetVal;
            Guid gidSourceTrackingID;

            string strPendingResponseInputFileName;
            string strInputErrorFileName;

            FileInfo fiPendingFile;
            FileInfo fiInProcess;
            FileInfo fiInputErrorFile;
            FileInfo fiArchiveFile;
            FileInfo fiPendingResponse;

            bool bolAttemptRetry;

            try {

                lock(this) {

                    // Verify that the pending input file exists, and move it to the inprocess folder.
                    if(File.Exists(pendingFileInfo.FullName) && MoveFileToInProcess(pendingFileInfo, out fiInProcess)) 
                    {

                        // Parse xml from the input file.  The calling function will pre-process the file and 
                        // transform it to canonical Xml if necessary.
                        if(GetNotificationXmlFromFile(fiInProcess, out docNotificationData, out intNotificationCount)) {

                            if(intNotificationCount > 0) {
                                // Get a session from the service.
                                if(GetSession(DataServiceClient, out userid, out gidSessionID)) {

                                    // Validate input file against the service Xsd.
                                    if(ValidateNotificationDoc(DataServiceClient, docNotificationData, out gidSourceTrackingID)) {

                                        // Instantiate the connection to the service.
                                        objFileUploadClient = 
                                            new FileUploadServiceClient
                                                (new ConnectionContext
                                                    {
                                                        SiteKey = ClientOptions.siteKey,
                                                        SessionId = gidSessionID,
                                                        FileUploadServiceLocation = ClientOptions.FileUploadServiceLocation,
                                                        Entity = string.Empty
                                                    }
                                                ); 

                                        // Send the canonical xml document to the service.
                                        if(UploadNotification(DataServiceClient, objFileUploadClient, gidSessionID, docNotificationData, out bolAttemptRetry)) {

                                            // Append the source trackingid, and move the input file from the 
                                            // inprocess folder to the pending response folder.
                                            strPendingResponseInputFileName = Path.Combine(_FileDropClientProcessOptions.PendingResponseFolder, Path.GetFileNameWithoutExtension(pendingFileInfo.Name) + "." + gidSourceTrackingID.ToString() + pendingFileInfo.Extension);

                                            // If a file exists that is already pending for this SourceTrackingID, 
                                            // then we abort the mission and move the input file to the InputErrors folder.
                                            if(File.Exists(strPendingResponseInputFileName)) {
                                                OnOutputLogAndTrace("Could not save pending response document.  Pending response document already exists: [" + strPendingResponseInputFileName + "].", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                                strInputErrorFileName = Path.Combine(_FileDropClientProcessOptions.InputErrorFolder, Path.GetFileNameWithoutExtension(pendingFileInfo.Name) + "." + gidSourceTrackingID.ToString() + pendingFileInfo.Extension);
                                                MoveFile(Path.Combine(_FileDropClientProcessOptions.InProcessFolder, fiInProcess.Name), strInputErrorFileName, out fiInputErrorFile);
                                                LogAlert(DataServiceClient, fiInProcess, fiInputErrorFile);
                                                LogAuditEvent(DataServiceClient, fiInProcess, fiInputErrorFile);
                                            } else {
                                            // Otherwise the file is moved to the PendingResponses folder.
                                                MoveFile(Path.Combine(_FileDropClientProcessOptions.InProcessFolder, fiInProcess.Name), strPendingResponseInputFileName, out fiPendingResponse);
                                                LogAuditEvent
                                                    (
                                                        DataServiceClient, 
                                                        new AuditRequest 
                                                            {  
                                                                ApplicationName = AuditApplicationName,
                                                                EventName = AuditFileSubmissionSuccessfulEventName,
                                                                EventType = AuditFileSubmissionEventType,
                                                                Message = string.Format("Submitted file '{0}' and awaiting response.", strPendingResponseInputFileName),
                                                                UserId = userid
                                                            }
                                                    );
                                            }

                                            // Save each individual notification to a seperate 'Pending response' xml document.
                                            SavePendingResponseDocuments(docNotificationData, pendingFileInfo.Name, _FileDropClientProcessOptions);
                                            bolRetVal = true;
                                        } else {

                                            if(bolAttemptRetry) {
                                                if(FileRetryQueue.Keys.Contains(pendingFileInfo.Name)) {
                                                    if(FileRetryQueue[pendingFileInfo.Name] >= 3) {
                                                        // Move the file to 'InputError'
                                                        OnOutputLogAndTrace("Could not upload Notification.  Max retry attempts (3) exceeded.  Moving file to the InputError folder.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                                        strInputErrorFileName = Path.Combine(_FileDropClientProcessOptions.InputErrorFolder, Path.GetFileNameWithoutExtension(pendingFileInfo.Name) + "." + gidSourceTrackingID.ToString() + pendingFileInfo.Extension);
                                                        MoveFile(Path.Combine(_FileDropClientProcessOptions.InProcessFolder, fiInProcess.Name), strInputErrorFileName, out fiInputErrorFile);
                                                        LogAlert(DataServiceClient, fiInProcess, fiInputErrorFile);
                                                        LogAuditEvent(DataServiceClient, fiInProcess, fiInputErrorFile);
                                                        FileRetryQueue.Remove(pendingFileInfo.Name);
                                                    } else {
                                                        // Move the file back to 'Pending'
                                                        FileRetryQueue[pendingFileInfo.Name] += 1;
                                                        OnOutputLogAndTrace("Could not upload Notification.  Retry attempt " + FileRetryQueue[pendingFileInfo.Name].ToString() + " of 3.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                                        MoveFile(Path.Combine(_FileDropClientProcessOptions.InProcessFolder, fiInProcess.Name), Path.Combine(_FileDropClientProcessOptions.InputFolder, pendingFileInfo.Name), out fiPendingFile);
                                                    }
                                                } else {
                                                    // Move the file back to 'Pending'
                                                    FileRetryQueue[pendingFileInfo.Name] = 1;
                                                    OnOutputLogAndTrace("Could not upload Notification.  Retry attempt 1 of 3.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                                    MoveFile(Path.Combine(_FileDropClientProcessOptions.InProcessFolder, fiInProcess.Name), Path.Combine(_FileDropClientProcessOptions.InputFolder, pendingFileInfo.Name), out fiPendingFile);
                                                }
                                            } else {
                                                // Move the file to 'InputError'
                                                OnOutputLogAndTrace("Could not upload Notification.  Moving file to the InputError folder.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                                strInputErrorFileName = Path.Combine(_FileDropClientProcessOptions.InputErrorFolder, Path.GetFileNameWithoutExtension(pendingFileInfo.Name) + "." + gidSourceTrackingID.ToString() + pendingFileInfo.Extension);
                                                MoveFile(Path.Combine(_FileDropClientProcessOptions.InProcessFolder, fiInProcess.Name), strInputErrorFileName, out fiInputErrorFile);
                                                LogAlert(DataServiceClient, fiInProcess, fiInputErrorFile);
                                                LogAuditEvent(DataServiceClient, fiInProcess, fiInputErrorFile);
                                            }
                                            bolRetVal = false;
                                        }

                                    } else {
                                        // We failed Xsd validation, so we abort the mission and move the input file to the InputErrors folder.
                                        strInputErrorFileName = Path.Combine(_FileDropClientProcessOptions.InputErrorFolder, Path.GetFileNameWithoutExtension(pendingFileInfo.Name) + "." + gidSourceTrackingID.ToString() + pendingFileInfo.Extension);
                                        MoveFile(Path.Combine(_FileDropClientProcessOptions.InProcessFolder, fiInProcess.Name), strInputErrorFileName, out fiInputErrorFile);
                                        LogAlert(DataServiceClient, fiInProcess, fiInputErrorFile);
                                        LogAuditEvent(DataServiceClient, fiInProcess, fiInputErrorFile);
                                        bolRetVal = false;
                                    }

                                } else {
                                    bolRetVal = false;
                                }
                            } else {
                                OnOutputLogAndTrace("File parsed successfully.  No Notifications found to process.", this.GetType().Name, LTAMessageType.Warning, LTAMessageImportance.Essential);
                                MoveFile(fiInProcess.FullName, Path.Combine(_FileDropClientProcessOptions.ArchiveFolder, pendingFileInfo.Name), out fiArchiveFile);
                                bolRetVal = true;
                            }
                        } else {
                            OnOutputLogAndTrace("Unable to parse file.  Moving file to InputErrorFolder.", this.GetType().Name, LTAMessageType.Warning, LTAMessageImportance.Essential);
                            MoveFile(fiInProcess.FullName, Path.Combine(_FileDropClientProcessOptions.InputErrorFolder, pendingFileInfo.Name), out fiArchiveFile);
                            LogAlert(DataServiceClient, pendingFileInfo, fiArchiveFile);
                            LogAuditEvent(DataServiceClient, pendingFileInfo, fiArchiveFile);
                            bolRetVal = false;
                        }
                    } else {
                        bolRetVal = false;
                    }
                }
            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in ProcessFile()", this.GetType().Name, ex);
                bolRetVal = false;
            }

            OnOutputLogAndTrace(string.Empty, string.Empty, LTAMessageType.Information, LTAMessageImportance.Essential);

            return(bolRetVal);
        }


        

        private bool MoveFileToInProcess(FileInfo pendingFileInfo, out FileInfo inProcessFile) {

            bool bolRetVal;
            Guid gidTemporarySourceTrackingID;
            string strInProcessFileName;
            FileInfo fiInProcessFile;

            OnOutputLogAndTrace("Moving Job File to InProcessFolder..." + pendingFileInfo.Name, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);

            try {
                gidTemporarySourceTrackingID = Guid.NewGuid();
                strInProcessFileName = Path.Combine(_FileDropClientProcessOptions.InProcessFolder, pendingFileInfo.Name + "." + gidTemporarySourceTrackingID.ToString() + "_inproc" + pendingFileInfo.Extension);
                MoveFile(pendingFileInfo.FullName, strInProcessFileName, out fiInProcessFile);
                bolRetVal = true;
            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in MoveFileToInProcess()", this.GetType().Name, ex);
                fiInProcessFile = null;
                bolRetVal = false;
            }

            inProcessFile = fiInProcessFile;

            return(bolRetVal);
        }

        private bool GetNotificationXmlFromFile(FileInfo inProcessFileInfo, out XmlDocument notificationData, out int notificationCount) {

            bool bolRetVal;
            XmlDocument docNotificationData;
            int intNotificationCount;

            OnOutputLogAndTrace("Reading Job File..." + inProcessFileInfo.Name, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);

            try {

                if(_FileDropClientProcessOptions.XClientDllFilePath.Length > 0) {

                    if(PreProcessNotificationData(inProcessFileInfo, out docNotificationData, out intNotificationCount)) {
                        OnOutputLogAndTrace("Job File converted to Xml [" + docNotificationData.OuterXml.ToString().Length + " bytes]", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                        bolRetVal = true;
                    } else {
                        docNotificationData = null;
                        intNotificationCount = 0;
                        bolRetVal = false;
                    }

                } else {

                    try { 
                        OnOutputLogAndTrace("Loading Xml from file: " + inProcessFileInfo.Name, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                        docNotificationData = new XmlDocument();
                        docNotificationData.Load(inProcessFileInfo.FullName);
                        intNotificationCount = docNotificationData.SelectNodes("/Notifications/Notification").Count;
                        bolRetVal = true;
                    } catch(Exception ex) {
                        OnOutputLogAndTrace("An unexpected exception occurred in GetNotificationXmlFromFile().  Could not load Xml from file: " + inProcessFileInfo.Name, this.GetType().Name, ex);
                        docNotificationData = null;
                        intNotificationCount = 0;
                        bolRetVal = false;
                    }
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in GetNotificationXmlFromFile()", this.GetType().Name, ex);
                docNotificationData = null;
                intNotificationCount = 0;
                bolRetVal = false;
            }

            OnOutputLogAndTrace(string.Empty, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Debug);

            notificationData = docNotificationData;

            notificationCount = intNotificationCount;

            return(bolRetVal);
        }

        private bool PreProcessNotificationData(FileInfo fileinfo, out XmlDocument notificationData, out int notificationCount) {

            XmlDocument docNotificationData;
            int intNotificationCount;
            bool bolRetVal;

            try {

                OnOutputLogAndTrace(
                    "Reformatting file data to Canonical Xml. [" + fileinfo.Name + "]", 
                    this.GetType().Name, 
                    LTAMessageType.Information, 
                    LTAMessageImportance.Verbose);

                

                if(XClient.GetNotificationXml(
                    Path.Combine(fileinfo.DirectoryName, fileinfo.Name), 
                    ClientProcessOptions.ClientProcessCode, 
                    out docNotificationData, 
                    out intNotificationCount)) {

                    OnOutputLogAndTrace(
                        "Canonical Xml was generated.", 
                        this.GetType().Name, 
                        LTAMessageType.Information, 
                        LTAMessageImportance.Verbose);

                    bolRetVal = true;

                } else {

                    OnOutputLogAndTrace(
                        "Canonical Xml failed to generate.", 
                        this.GetType().Name, 
                        LTAMessageType.Information, 
                        LTAMessageImportance.Verbose);

                    bolRetVal = false;
                    intNotificationCount = 0;
                    docNotificationData = null;
                }

            } catch (Exception ex) {
                docNotificationData = null;
                bolRetVal = false;
                intNotificationCount = 0;
                OnOutputLogAndTrace("An unexpected exception occurred in PreProcessNotificationData()", this.GetType().Name, ex);
            }

            notificationCount = intNotificationCount;
            notificationData = docNotificationData;
            return(bolRetVal);
        }

        protected override void OnResponseSuccess(Guid sourceTrackingID) {
            // Do Nothing on purpose.
        }

        protected override void OnResponseError(Guid sourceTrackingID) {
            // Do Nothing on purpose.
        }

#if use_local

        private bool GetXClient(out IFileDropXClient xClient) {
            xClient = new WFS.LTA.FileImportServices.ICONXClient.cFilePreProcessor();
            xClient.OutputLog += new FileImportClientLib.OutputLogEventHandler(xClient_OutputLog);
            xClient.OutputLogAndTrace += new FileImportClientLib.OutputLogAndTraceEventHandler(xClient_OutputLogAndTrace);
            xClient.OutputTrace += new FileImportClientLib.OutputTraceEventHandler(xClient_OutputTrace);
            xClient.OutputLogAndTraceException += new FileImportClientLib.OutputLogAndTraceExceptionEventHandler(xClient_OutputLogAndTraceException);
            return(true);
        }

        private IFileDropXClient XClient {

            get {
                IFileDropXClient objXClient;
                if(GetXClient(out objXClient)) {
                    return(objXClient);
                } else {
                    return(null);
                }
            }
        }

#else

        private IFileDropXClient XClient {

            get {

                Assembly asmDLL;

                try {

                    if(_XClient == null) {

                        asmDLL = Assembly.LoadFrom(_FileDropClientProcessOptions.XClientDllFilePath);

                        foreach (Type typCurType in asmDLL.GetTypes()) {
                            if(typCurType.FindInterfaces(MyInterfaceFilter, typeof(IFileDropXClient).FullName).Length > 0) {
                                _XClient = (IFileDropXClient)typCurType.GetConstructor(new Type[] { }).Invoke(null);
                                _XClient.OutputLog += new FileImportClientLib.FITClientOutputLogEventHandler(xClient_OutputLog);
                                _XClient.OutputTrace += new FileImportClientLib.FITClientOutputTraceEventHandler(xClient_OutputTrace);
                                _XClient.OutputLogAndTrace += new FileImportClientLib.FITClientOutputLogAndTraceEventHandler(xClient_OutputLogAndTrace);
                                _XClient.OutputLogAndTraceException += new FileImportClientLib.FITClientOutputLogAndTraceExceptionEventHandler(xClient_OutputLogAndTraceException);
                                break;
                            }
                        }

                        XClient.Configure
                            (new ConfigurationContext
                                {
                                    BankId = this._FileDropClientProcessOptions.DefaultBankId,
                                    OrganizationId = this._FileDropClientProcessOptions.DefaultOrganizationId
                                }
                            );
                    }

                } catch(Exception ex) {
                    OnOutputLogAndTrace("Could not load File-Drop XClient [" + _FileDropClientProcessOptions.XClientDllFilePath + "]", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                    throw(ex);
                }

                return(_XClient);
            }
        }


        private static bool MyInterfaceFilter(Type typeObj,Object criteriaObj) {
            if(typeObj.ToString() == criteriaObj.ToString()) {
                return true;
            } else {
                return false;
            }
        }

#endif
    }
}
