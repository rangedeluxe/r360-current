﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WFS.LTA.FileImportClientLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    internal class cPollingClientProcessOptions : _ClientProcessOptionsBase {

        protected ProcessElement _ProcessConfigSettings;

        public cPollingClientProcessOptions(ProcessElement processConfigSettings) : base(processConfigSettings) {
            _ProcessConfigSettings = processConfigSettings;
        }

        public override int ClientType {
            get {
                return(Support.CLIENT_TYPE_POLL);
            }
        }
    }
}
