﻿//#define use_local


using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;

using WFS.LTA.FileImport.FileImportServicesClient;
using WFS.LTA.FileImportClientLib;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    internal class cPollingProcessor : _FileImportProcessorBase {

#if !use_local
        private IPollingProcessorXClient _XClient = null;
#endif

        public cPollingProcessor(cClientOptions clientOptions, IClientProcessOptions xProc) : base(clientOptions, xProc) 
        {
        }

        public cPollingProcessor(cClientOptions clientOptions, IClientProcessOptions xProc, FileDataServiceClient dataServiceClient)
            : base(clientOptions, xProc, dataServiceClient)
        {
        }

        private cPollingClientProcessOptions PollingProcessOptions {
            get {
                return((cPollingClientProcessOptions)base.ClientProcessOptions);
            }
        }

        public bool ProcessPendingNotifications() {

            XmlDocument docNotificationData;
            bool bolContinue;
            bool bolRetVal;

            bolContinue = true;
            bolRetVal = true;

            while(bolContinue) {

                OnOutputLogAndTrace(string.Empty, string.Empty, LTAMessageType.Information, LTAMessageImportance.Verbose);
                OnOutputLogAndTrace("Retrieving pending Notifications", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);

                if(XClient.GetNotificationXml(ClientProcessOptions.ClientProcessCode, out docNotificationData)) {

                    if(docNotificationData.DocumentElement.ChildNodes.Count > 0) {

                        if(docNotificationData == null) {

                            //Exception.
                            OnOutputLogAndTrace("Attempt to retrieve Xml using XClient [" + PollingProcessOptions.ClientProcessCode + "] dll failed..", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                            bolRetVal = false;
                            bolContinue = false;

                        } else {

                            OnOutputLogAndTrace("Job Xml successfully collected using XClient dll. [" + docNotificationData.OuterXml.ToString().Length + " bytes]", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);

                            bolRetVal = SendNotification(docNotificationData);
                            if(!bolRetVal) {
                                bolContinue = false;
                            }
                        }

                    } else {
                        OnOutputLogAndTrace("No pending Notifications were found by Polling Processor XClient [" + PollingProcessOptions.ClientProcessCode + "]", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                        bolRetVal = true;
                        bolContinue = false;
                    }

                } else {
                    OnOutputLogAndTrace("Unable to retrieve Notifications from Polling Processor XClient [" + PollingProcessOptions.ClientProcessCode + "]", this.GetType().Name, LTAMessageType.Warning, LTAMessageImportance.Essential);
                    bolRetVal = false;
                    bolContinue = false;
                }

                OnOutputLogAndTrace("Done processing pending Notifications", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
            }

            return(bolRetVal);
        }

        protected bool SendNotification(XmlDocument docNotificationData) {

            FileDataServiceClient objDataClient;
            FileUploadServiceClient objFileUploadClient;
            Guid gidSessionID;
            int userid;
            bool bolRetVal;
            Guid gidSourceTrackingID;
            List<cNotification> arNotifications;

            objDataClient = new FileDataServiceClient
                (new ConnectionContext 
                    {
                        SiteKey = ClientOptions.siteKey,
                        DataServiceLocation = ClientOptions.DataServiceLocation,
                        Entity = string.Empty
                    }
                );
            
            if(GetSession(objDataClient, out userid, out gidSessionID)) {

                if(ValidateNotificationDoc(objDataClient, docNotificationData, out gidSourceTrackingID)) {

                    // Instantiate the connection to the service.
                    objFileUploadClient = 
                        new FileUploadServiceClient
                            (new ConnectionContext
                                {
                                    SiteKey = ClientOptions.siteKey,
                                    SessionId = gidSessionID, 
                                    FileUploadServiceLocation = ClientOptions.FileUploadServiceLocation,
                                    Entity = string.Empty
                                }); 

                    if(UploadNotification(objDataClient, objFileUploadClient, gidSessionID, docNotificationData)) {

                        SavePendingResponseDocuments(docNotificationData, string.Empty, PollingProcessOptions);

                        if(NotificationLib.NotificationsFromXml(docNotificationData, out arNotifications)) {
                            foreach(cNotification notification in arNotifications) {
                                XClient.UpdateNotificationStatusSubmitted(gidSourceTrackingID, notification);
                            }
                        }

                        bolRetVal = true;
                    } else {
                        bolRetVal = false;
                    }

                } else {
                    bolRetVal = false;
                }

            } else {
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected override void OnResponseSuccess(Guid sourceTrackingID) {
            try {
                XClient.UpdateNotificationStatusSuccess(sourceTrackingID);
            } catch(Exception ex) {
                OnOutputLogAndTrace("Unable to notify Polling-Process Client on Response Success.", this.GetType().Name, ex);
            }
        }

        protected override void OnResponseError(Guid sourceTrackingID) {
            try {
                XClient.UpdateNotificationStatusFailed(sourceTrackingID);
            } catch(Exception ex) {
                OnOutputLogAndTrace("Unable to notify Polling-Process Client on Response Error.", this.GetType().Name, ex);
            }
        }

#if use_local

        private bool GetXClient(out IPollingProcessorXClient xClient) {

            xClient = new WFS.LTA.FileImportServices.ItemProcessingXClient.cProcessor();
            xClient.IniSectionKey = _PollingClientProcessOptions.IniSectionKey;
            xClient.OutputLog += new FileImportClientLib.OutputLogEventHandler(xClient_OutputLog);
            xClient.OutputLogAndTrace += new FileImportClientLib.OutputLogAndTraceEventHandler(xClient_OutputLogAndTrace);
            xClient.OutputTrace += new FileImportClientLib.OutputTraceEventHandler(xClient_OutputTrace);
            xClient.OutputLogAndTraceException += new FileImportClientLib.OutputLogAndTraceExceptionEventHandler(xClient_OutputLogAndTraceException);

            return(true);
        }

        private IPollingProcessorXClient XClient {

            get {
                IPollingProcessorXClient objXClient;
                if(GetXClient(out objXClient)) {
                    return(objXClient);
                } else {
                    return(null);
                }
            }
        }

#else

        private IPollingProcessorXClient XClient {

            get {

                Assembly asmDLL;

                try {

                    if(_XClient == null) {

                        asmDLL = Assembly.LoadFile(PollingProcessOptions.XClientDllFilePath);

                        foreach (Type typCurType in asmDLL.GetTypes()) {
                            if(typCurType.FindInterfaces(MyInterfaceFilter, typeof(IPollingProcessorXClient).FullName).Length > 0) {
                                _XClient = (IPollingProcessorXClient)typCurType.GetConstructor(new Type[] { }).Invoke(null);
                                _XClient.IniSectionKey = PollingProcessOptions.IniSectionKey;
                                _XClient.OutputLog += new FileImportClientLib.FITClientOutputLogEventHandler(xClient_OutputLog);
                                _XClient.OutputTrace += new FileImportClientLib.FITClientOutputTraceEventHandler(xClient_OutputTrace);
                                _XClient.OutputLogAndTrace += new FileImportClientLib.FITClientOutputLogAndTraceEventHandler(xClient_OutputLogAndTrace);
                                _XClient.OutputLogAndTraceException += new FileImportClientLib.FITClientOutputLogAndTraceExceptionEventHandler(xClient_OutputLogAndTraceException);
                                break;
                            }
                        }

                        XClient.Configure
                            (new ConfigurationContext
                                {
                                    BankId = this.PollingProcessOptions.DefaultBankId,
                                    OrganizationId = this.PollingProcessOptions.DefaultOrganizationId
                                }
                            );
                    }

                } catch(Exception ex) {
                    OnOutputLogAndTrace("Could not load Polling Processor XClient [" + PollingProcessOptions.XClientDllFilePath + "]", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                    throw(ex);
                }

                return(_XClient);
            }
        }

        private static bool MyInterfaceFilter(Type typeObj,Object criteriaObj) {

            if(typeObj.ToString() == criteriaObj.ToString()) {
                return true;
            } else {
                return false;
            }
        }

#endif

    }
}
