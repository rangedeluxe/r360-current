﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;

using WFS.RecHub.Common;
using WFS.RecHub.Common.Crypto;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    public class cClientOptions : cSiteOptions {

        private int _SessionExpiration = 30;
        private int _MaxLogonAttempts = 5;
        private string _DataServiceLocation = string.Empty;
        private string _FileUploadServiceLocation = string.Empty;
        private string _LogonName = string.Empty;
        private string _Password = string.Empty;
        private bool _SkipClientXmlValidation = false;
        private const string INIKEY_SESSION_EXPIRATION = "SessionExpiration";
        private const string INIKEY_MAX_LOGON_ATTEMPTS = "MaxLogonAttempts";
        private const string INIKEY_DATA_SERVICE_LOCATION = "DataServiceLocation";
        private const string INIKEY_FILE_UPLOAD_SERVICE_LOCATION = "FileUploadServiceLocation";
        private const string INIKEY_LOGON_NAME = "LogonName";
        private const string INIKEY_PASSWORD = "Password";
        private const string INIKEY_SKIP_CLIENT_XML_VALIDATION = "SkipClientXmlValidation";
        public const string LOG_FILE = "LogFile";
        public const string LOGGING_DEPTH = "LoggingDepth";
        public const string LOG_FILE_MAX_SIZE = "LogFileMaxSize";

		public cClientOptions() : base(Support.FILE_IMPORT_CLIENT_SITE_KEY) {

            StringCollection colSiteOptions = ipoINILib.GetINISection(Support.FILE_IMPORT_CLIENT_SITE_KEY);
            string strKey;
            string strValue;
            int intValue;

            int intTemp;

            System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {
                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if(strKey.ToLower() == INIKEY_SESSION_EXPIRATION.ToLower()) {
                    try {
                        intValue = System.Convert.ToInt32(strValue); 
                        if(intValue > 0) {
                            _SessionExpiration = intValue;
                        } else {
                            _SessionExpiration = 30;
                        }
                    }
                    catch { _SessionExpiration = 30; }
                } else if(strKey.ToLower() == INIKEY_MAX_LOGON_ATTEMPTS.ToLower()) {
                    try {
                        intValue = System.Convert.ToInt32(strValue); 
                        if(intValue > 0) {
                            _MaxLogonAttempts = intValue;
                        } else {
                            _MaxLogonAttempts = 30;
                        }
                    }
                    catch { _MaxLogonAttempts = 30; }
                }
                else if(strKey.ToLower() == INIKEY_DATA_SERVICE_LOCATION.ToLower()) {
                    _DataServiceLocation = strValue.Trim(); 
                }
                else if(strKey.ToLower() == INIKEY_FILE_UPLOAD_SERVICE_LOCATION.ToLower()) {
                    _FileUploadServiceLocation = strValue.Trim(); 
                }
                else if(strKey.ToLower() == INIKEY_LOGON_NAME.ToLower()) {
                    _LogonName = strValue.Trim(); 
                }
                else if(strKey.ToLower() == INIKEY_PASSWORD.ToLower()) {
                    _Password = strValue.Trim(); 
                }
                else if(strKey.ToLower() == INIKEY_SKIP_CLIENT_XML_VALIDATION.ToLower()) {
                    if(strValue.Length > 0 && int.TryParse(strValue, out intTemp) && intTemp > 0) {
                        _SkipClientXmlValidation = true;
                    }
                }
            }
			try
			{
				GetSettings();
			}
			catch (ArgumentNullException)
			{
				throw new Exception(
					"Error loading client options, invalid or missing username or password. File Import Client Service is stopping.");
			}
			catch (FormatException)
			{
				throw new Exception(
					"Error loading client options, invalid username or password encrypted length. File Import Client Service is stopping.");
			}
		}

        public int SessionExpiration {
            get {
                return(_SessionExpiration);
            }
        }

        public int MaxLogonAttempts {
            get {
                return(_MaxLogonAttempts);
            }
        }

        public string DataServiceLocation {
            get {
                return(_DataServiceLocation);
            }
        }

        public string FileUploadServiceLocation {
            get {
                return(_FileUploadServiceLocation);
            }
        }

        public string LogonName {
            get {
                return(_LogonName);
            }
        }

        public string Password {
            get {
                return(_Password);
            }
        }

        public LTAMessageImportance LTALoggingDepth {
            get {
                switch((int)base.loggingDepth) {
                    case (int)LTAMessageImportance.Debug:
                        return(LTAMessageImportance.Debug);
                    case (int)LTAMessageImportance.Essential:
                        return(LTAMessageImportance.Essential);
                    case (int)LTAMessageImportance.Verbose:
                        return(LTAMessageImportance.Verbose);
                    default:
                        return(LTAMessageImportance.Essential);
                }
            }
        }

        public bool SkipClientXmlValidation {
            get {
                return(_SkipClientXmlValidation);
            }
        }

        public new string siteKey {
            get {
                return(ConfigurationManager.AppSettings["siteKey"]);
            }
        }

        public new string logFilePath
        {
            get;

            private set;
        }

        public new int logFileMaxSize
        {
            get;
            private set;
        }

        public new int loggingDepth
        {
            get;
            private set;
        }

        private void GetSettings()
        {
            cCrypto3DES cryp3DES = new cCrypto3DES();

            _FileUploadServiceLocation = ConfigurationManager.AppSettings.Get(INIKEY_FILE_UPLOAD_SERVICE_LOCATION);
            _DataServiceLocation = ConfigurationManager.AppSettings.Get(INIKEY_DATA_SERVICE_LOCATION);
            cryp3DES.Decrypt(ConfigurationManager.AppSettings.Get(INIKEY_LOGON_NAME), out _LogonName);
            cryp3DES.Decrypt(ConfigurationManager.AppSettings.Get(INIKEY_PASSWORD), out _Password);
            this.logFilePath = ConfigurationManager.AppSettings.Get(LOG_FILE);
            int depth = 0;
            int fileMaxSize = 0;
            int.TryParse(ConfigurationManager.AppSettings.Get(LOGGING_DEPTH), out depth);
            int.TryParse(ConfigurationManager.AppSettings.Get(LOG_FILE_MAX_SIZE), out fileMaxSize);
            this.loggingDepth = depth;
            this.logFileMaxSize = fileMaxSize;

        }
    }
}
