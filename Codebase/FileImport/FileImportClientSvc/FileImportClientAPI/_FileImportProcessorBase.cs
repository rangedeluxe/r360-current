﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using WFS.LTA.Common;
using WFS.LTA.FileImport.FileImportServicesClient;
using WFS.LTA.FileImport.FileImportWCFLib;
using WFS.LTA.FileImportClientLib;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
* WI 118356 JMC 10/22/2013
*   -Updated File Import Client Service to send UTC Notification Date when
*    sending file attachments.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    internal abstract class _FileImportProcessorBase : _ClientAPIBase {

        private IClientProcessOptions _ClientProcessOptions;

        private const string NOTIFICATION_SCHEMATYPE = "Notification Import Integration Services";
        private const string ProcessingExceptionEventName = "ProcessingException";
        protected const string AuditApplicationName = "FIT";
        protected const string AuditFileSubmissionEventType = "File Submission";
        protected const string AuditFileSubmissionSuccessfulEventName = "Moved To Pending Response Folder";
        protected const string AuditFileSubmissionFailureEventName = "Moved To Input Error Folder";

        protected _FileImportProcessorBase(cClientOptions clientOptions, IClientProcessOptions clientProcessOptions) : base(clientOptions)
        {
            var connectionContext = new ConnectionContext
                {
                    DataServiceLocation = clientOptions.DataServiceLocation,
                    Entity = string.Empty,
                    SiteKey = clientOptions.siteKey
                };
            var dataServiceClient = new FileDataServiceClient(connectionContext);
            Initialize(clientProcessOptions, dataServiceClient); 
        }

        protected _FileImportProcessorBase(cClientOptions clientOptions, IClientProcessOptions clientProcessOptions, FileDataServiceClient dataServiceClient)
            : base(clientOptions)
        {
            Initialize(clientProcessOptions, dataServiceClient);
        }

        protected IClientProcessOptions ClientProcessOptions {
            get {
                return(_ClientProcessOptions);
            }
        }

        protected FileDataServiceClient DataServiceClient
        {
            get;
            private set;
        }

        protected bool GetSession(FileDataServiceClient svcClient, out int userID, out Guid sessionID) {

            bool bolRetVal;
            Guid gidSessionID;
            int userid;

            if (svcClient.GetSession(ClientOptions.LogonName, ClientOptions.Password, out userid, out gidSessionID))
            {
                sessionID = gidSessionID;
                userID = userid;
                bolRetVal = true;
            } else {
                OnOutputLogAndTrace("Unable to establish session.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                sessionID = Guid.Empty;
                userID = 0;
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected bool GetSchemaDefinition(FileDataServiceClient svcClient, string schemaVersion, out XmlDocument xsd) {

            string strDataXsd;
            string strRespXml;
            bool bolRetVal;
            XmlDocument docXsd;

            try {

                OnOutputLogAndTrace("Getting Notification schema", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                if(svcClient.GetSchemaDefinition(NOTIFICATION_SCHEMATYPE, schemaVersion, out strDataXsd, out strRespXml)) {

                    OnOutputLogAndTrace("Schema Definition request was successful.", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                    LogAndTraceResponseXml(strRespXml, this.GetType().Name, true, LTAMessageImportance.Debug);

                    OnOutputLogAndTrace("Schema Definition was [" + strDataXsd.Length.ToString() + " bytes]", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                    OnOutputLog("Schema Definition: " + strDataXsd, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Debug);
                

                    try {
                        docXsd = new XmlDocument();
                        docXsd.LoadXml(strDataXsd);
                        bolRetVal = true;
                    } catch(Exception ex) {
                        OnOutputLogAndTrace("An exception occured transforming Xsd document to Xml.", this.GetType().Name, ex);
                        docXsd = null;
                        bolRetVal = false;
                    }

                } else {
                    OnOutputLogAndTrace("Schema Definition request failed.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                    docXsd = null;
                    bolRetVal = false;
                }
            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in GetSchemaDefinition()", this.GetType().Name, ex);
                docXsd = null;
                bolRetVal = false;
            }

            xsd = docXsd;
            return(bolRetVal);
        }

        protected bool ValidateNotificationDoc(FileDataServiceClient svcClient, XmlDocument notificationData, out Guid sourceTrackingID) {

            XmlDocument docXsd;
            bool bolRetVal;
            Guid gidTemp;
            Guid gidSourceTrackingID;
            string strNotificationXSDVersion;

            try {

                if(notificationData != null && notificationData.DocumentElement != null) {

                    if(notificationData.DocumentElement.Attributes["SourceTrackingID"] != null && 
                       ipoLib.TryParse(notificationData.DocumentElement.Attributes["SourceTrackingID"].InnerText, out gidTemp) && 
                       gidTemp != Guid.Empty) {

                        gidSourceTrackingID = gidTemp;
                    } else {
                        OnOutputLogAndTrace("Could not read SourceTrackingID from source Xml.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                        gidSourceTrackingID = Guid.Empty;
                    }

                    if(notificationData.DocumentElement.Attributes["XSDVersion"] != null && 
                       (!string.IsNullOrEmpty(notificationData.DocumentElement.Attributes["XSDVersion"].Value))) {

                        strNotificationXSDVersion = notificationData.DocumentElement.Attributes["XSDVersion"].Value;
                    } else {
                        OnOutputLogAndTrace("Could not read XSDVersion from source Xml.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                        strNotificationXSDVersion = string.Empty;
                    }

                } else {
                    OnOutputLogAndTrace("Could not read Document Element from source Xml.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                    gidSourceTrackingID = Guid.Empty;
                    strNotificationXSDVersion = string.Empty;
                }

                if(gidSourceTrackingID != Guid.Empty && (!string.IsNullOrEmpty(strNotificationXSDVersion))) {

                    if(GetSchemaDefinition(svcClient, strNotificationXSDVersion, out docXsd)) {

                        OnOutputLogAndTrace("Validating Xml.", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);

                        if(ClientOptions.SkipClientXmlValidation) {
                            OnOutputLogAndTrace("Skipping Client Xml validation.", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                            bolRetVal = true;
                        } else if(XML_XSD_Validator.Validate(notificationData.OuterXml, docXsd.OuterXml)) {
                            OnOutputLogAndTrace("Xml Document was validated successfully.", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                            bolRetVal = true;
                        } else {
                            OnOutputLogAndTrace(XML_XSD_Validator.GetError(), this.GetType().Name, LTAMessageType.Warning, LTAMessageImportance.Essential);
                            OnOutputLogAndTrace("Xml Document was not valid.  Files were not sent.", this.GetType().Name, LTAMessageType.Warning, LTAMessageImportance.Essential);
                            bolRetVal = false;
                        }
                    } else {
                        OnOutputLogAndTrace("Could not retrieve Xsd from the server.  Files were not sent.", this.GetType().Name, LTAMessageType.Warning, LTAMessageImportance.Essential);
                        bolRetVal = false;
                    }
                } else {
                    bolRetVal = false;
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in ValidateNotificationDoc()", this.GetType().Name, ex);
                gidSourceTrackingID = Guid.Empty;
                bolRetVal = false;
            }

            sourceTrackingID = gidSourceTrackingID;
            return(bolRetVal);
        }

        protected bool UploadNotification(
            FileDataServiceClient svcDataClient, 
            FileUploadServiceClient svcFileUploadClient, 
            Guid sessionID, 
            XmlDocument notificationData) {

            bool bolAttemptRetry;

            return(UploadNotification(
                svcDataClient,
                svcFileUploadClient,
                sessionID,
                notificationData,
                out bolAttemptRetry));
        }

        protected bool UploadNotification(
            FileDataServiceClient svcDataClient, 
            FileUploadServiceClient svcFileUploadClient, 
            Guid sessionID, 
            XmlDocument notificationData,
            out bool attemptRetry) {

            bool bolRetVal;

            try {

                notificationData = ApplyDefaults(notificationData);

                if(SendNotificationFiles(svcFileUploadClient, notificationData, out attemptRetry)) {
                    if(SendNotificationData(svcDataClient, notificationData)) {
                        bolRetVal = true;
                    } else {
                        bolRetVal = false;
                    }
                } else {
                    bolRetVal = false;
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in UploadNotification()", this.GetType().Name, ex);
                attemptRetry = true;
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected bool SendNotificationFiles(FileUploadServiceClient svcClient, XmlDocument docNotificationData, out bool attemptRetry) {

            bool bolRetVal;
            List<cNotification> arNotifications;
            StreamRequest objUploadItem;
            NotificationFileInfoContract objContract;
            StreamResponse objReturnItem;
            bool bolAttemptRetry = false;


            try {

                OnOutputLogAndTrace("Sending Notification files...", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);

                if(NotificationLib.NotificationsFromXml(docNotificationData, out arNotifications)) {

                    bolRetVal = true;

                    foreach(cNotification notification in arNotifications) {

                        foreach(cNotificationFile notificationfile in notification.NotificationFiles) {

                            objContract = new NotificationFileInfoContract();
                            objContract.BankID = notification.BankID;
                            objContract.CustomerID = notification.ClientGroupID;
                            objContract.LockboxID = notification.ClientID;
                            objContract.NotificationDateUTC = notification.NotificationDate;
                            objContract.FileIdentifier = notificationfile.FileIdentifier;
                            objContract.FileExtension = notificationfile.FileExtension;

                            if(string.IsNullOrEmpty(notificationfile.ClientFilePath)) {
                                OnOutputLogAndTrace("Attachment file name cannot be empty.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                bolRetVal = false;
                                break;
                            } else if(Path.GetDirectoryName(notificationfile.ClientFilePath).IndexOfAny(Path.GetInvalidPathChars()) > -1) {
                                OnOutputLogAndTrace("Attachment file path contains invalid characters." + notificationfile.ClientFilePath, this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                bolRetVal = false;
                                break;
                            } else if(Path.GetFileName(notificationfile.ClientFilePath).IndexOfAny(Path.GetInvalidFileNameChars()) > -1) {
                                OnOutputLogAndTrace("Attachment file name contains invalid characters." + notificationfile.ClientFilePath, this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                bolRetVal = false;
                                break;
                            } else if(File.Exists(notificationfile.ClientFilePath)) {
                                using (FileStream fs = File.OpenRead(notificationfile.ClientFilePath)) {

                                    objUploadItem = 
                                        new StreamRequest 
                                            {
                                                
                                                NotificationFileInfo = objContract, 
                                                DataStream = fs
                                            };

                                    OnOutputLogAndTrace("Sending Notification file: " + notificationfile.ClientFilePath, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);

                                    objReturnItem = svcClient.SendNotificationFile(objUploadItem);
                                }
                            } else {
                                OnOutputLogAndTrace("Could not locate client file: " + notificationfile.ClientFilePath, this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                bolRetVal = false;
                                bolAttemptRetry = true;
                                break;
                            }

                        }

                        if(!bolRetVal) {
                            break;
                        }
                    }

                } else {
                    OnOutputLogAndTrace("Could not retrieve Notifications from Xml.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                    bolRetVal = false;
                }


            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in SendNotificationFiles()", this.GetType().Name, ex); 
                bolRetVal = false;
                bolAttemptRetry = true;
            }        

            attemptRetry = bolAttemptRetry;
            return(bolRetVal);
        }

        protected bool SendNotificationData(FileDataServiceClient svcClient, XmlDocument docNotificationData) {

            string strRespXml;
            bool bolRetVal;
            
            OnOutputLogAndTrace("Sending Notification data...", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
            if(svcClient.SendNotificationData(docNotificationData.OuterXml.ToString(), out strRespXml)) {
                OnOutputLogAndTrace("Notification data sent.", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                LogAndTraceResponseXml(strRespXml, this.GetType().Name, true, LTAMessageImportance.Verbose);
                bolRetVal = true;
            } else {
                OnOutputLogAndTrace("Sending Notification data failed.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                LogAndTraceResponseXml(strRespXml, this.GetType().Name, false, LTAMessageImportance.Essential);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        protected void LogAlert(_ServicesClientBase service, string eventName, string message)
        {
            var response = service.LogAlert(new AlertRequest {EventName = eventName, Message = message });
            if (response.Errors.FirstOrDefault() != null)
                response.Errors
                    .ToList()
                    .ForEach(error =>
                    {
                        OnOutputLogAndTrace(error, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                    });
        }

        protected void LogAlert(_ServicesClientBase service, FileInfo inputFile, FileInfo errorFile)
        {
            LogAlert(service, ProcessingExceptionEventName,
                string.Format("There was an issue processing file {0}.  The file has been moved to {1}, and is named {2}",
                    inputFile.Name, errorFile.DirectoryName, errorFile.Name));
        }

        protected void LogAuditEvent(_ServicesClientBase service, AuditRequest requestContext)
        {
            var response = service.LogAuditEvent(requestContext);
            if (response.Errors.FirstOrDefault() != null)
                response.Errors
                    .ToList()
                    .ForEach(error =>
                    {
                        OnOutputLogAndTrace(error, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                    });
        }

        protected void LogAuditEvent(_ServicesClientBase service, string applicationName, string eventType, string eventName, string message)
        {
            LogAuditEvent(service, new AuditRequest {ApplicationName = applicationName, EventName = eventName, EventType = eventType, Message = message });
           
        }

        protected void LogAuditEvent(_ServicesClientBase service, FileInfo inputFile, FileInfo errorFile)
        {
            LogAuditEvent
            (
                service,
                AuditApplicationName,
                AuditFileSubmissionEventType,
                AuditFileSubmissionFailureEventName,
                string.Format("There was an issue processing file {0}.  The file has been moved to {1} and is named {2}.", inputFile.Name, errorFile.DirectoryName, errorFile.Name)

            );
        }

        private void Initialize(IClientProcessOptions clientProcessOptions, FileDataServiceClient dataServiceClient)
        {
            this._ClientProcessOptions = clientProcessOptions;
            this.DataServiceClient = dataServiceClient;
        }

        private bool GetNotificationResponses(FileDataServiceClient svcClient, out XmlDocument notificationRespXml) {

            string strNotificationRespXml;
            string strRespXml;
            bool bolRetVal;
            XmlDocument docNotificationRespXml;

            try {

                OnOutputLogAndTrace("Getting Notification Responses", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);

                if(svcClient.GetNotificationResponses(
                    ClientProcessOptions.ClientProcessCode, 
                    out strNotificationRespXml, 
                    out strRespXml)) {

                    OnOutputLogAndTrace("Notification Responses request was successful.", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                    LogAndTraceResponseXml(strRespXml, this.GetType().Name, true, LTAMessageImportance.Debug);

                    OnOutputLogAndTrace("Notification Response document was [" + strNotificationRespXml.Length.ToString() + " bytes]", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                    OnOutputLog("Notification Responses: " + strNotificationRespXml, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Debug);
                
                    try {
                        docNotificationRespXml = new XmlDocument();
                        docNotificationRespXml.LoadXml(strNotificationRespXml);
                        bolRetVal = true;
                    } catch(Exception ex) {
                        OnOutputLogAndTrace("An unexpected exception occured transforming Notification Responses document to Xml.", this.GetType().Name, ex);
                        docNotificationRespXml = null;
                        bolRetVal = false;
                    }

                } else {
                    OnOutputLogAndTrace("Notification Responses request failed.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                    LogAndTraceResponseXml(strRespXml, this.GetType().Name, false, LTAMessageImportance.Essential);
                    docNotificationRespXml = null;
                    bolRetVal = false;
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in GetNotificationResponses()", this.GetType().Name, ex);
                docNotificationRespXml = null;
                bolRetVal = false;
            }

            notificationRespXml = docNotificationRespXml;
            return(bolRetVal);
        }

        public bool ProcessResponses() {
            XmlDocument docNotificationRespXml;
            Guid gidSessionID;
            int userid;

            bool bolRetVal;

            try {
                
                // Get a session from the service.
                if(GetSession(DataServiceClient, out userid, out gidSessionID)) {
            
                    if(GetNotificationResponses(DataServiceClient, out docNotificationRespXml)) {
                    
                        if(ProcessNotificationResponses(DataServiceClient, docNotificationRespXml)) {
                            bolRetVal = true;
                        } else {
                            bolRetVal = false;
                        }

                    } else {
                        bolRetVal = false;
                    }
                } else {
                    bolRetVal = false;
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in ProcessResponses()", this.GetType().Name, ex);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        private XmlDocument ApplyDefaults(XmlDocument xml)
        {
            if (xml == null)
                throw new ArgumentNullException("XML Documment cannot be null when attempting to set the defaults.");

            if (string.IsNullOrEmpty(xml.InnerXml))
                throw new ArgumentNullException("XML cannot be null or empty when attempting to set the defaults.");

            var notifications = XElement.Parse(xml.InnerXml);
            var xmlDocument = new XmlDocument();

            notifications
                .Elements()
                .Where(element => string.Equals(element.Name.ToString(), "notification", StringComparison.InvariantCultureIgnoreCase))
                .ToList()
                .ForEach //process each notification element
                (
                    notification => //apply default configurations
                    {
                        ApplyNotificationSourceDefault(notification);
                    }
                );

            xmlDocument.LoadXml(notifications.ToString());
            return xmlDocument;
        }

        private void ApplyNotificationSourceDefault(XElement notification)
        {
            if (ApplyConfigurationDefault(notification, "NotificationSourceKey") || !string.Equals(notification.Attribute("NotificationSourceKey").Value, "100"))
                notification.SetAttributeValue("NotificationSourceKey", "100");
        }

        private bool ApplyConfigurationDefault(XElement notification, string setting)
        {
            if (notification == null)
                throw new ArgumentNullException();

            if (notification.Attribute(setting) == null || string.IsNullOrEmpty(notification.Attribute(setting).Value))
                return true;

            return false;
        }

        private bool ProcessNotificationResponses(FileDataServiceClient svcClient, XmlDocument docNotificationsResponses) {

            XmlNodeList nlResponsesNotificationGroups;
            XmlNodeList nlResponseNotifications;

            XmlAttribute attResponseTrackingID;
            Guid gidResponseTrackingID;

            XmlAttribute attResponseSourceTrackingID;
            string strSourceTrackingID;
            string[] arPendingResponseInputFiles;

            XmlAttribute attNotificationTrackingID;
            Guid gidNotificationTrackingID;
            string[] arPendingResponseCanonicalFiles;

            FileInfo fiCompletedFile;

            Guid gidTemp;

            Guid gidSourceTrackingID;

            XmlNode nodeResult;
            bool bolSuccess;

            bool bolRetVal;

            try {

                // The response document can contain multiple Notification Groups that in turn contains 
                // multiple Notifications.  A Notification Group is a list of Notifications that were 
                // uploaded in a single request and as a group are assigned a unique SourceTrackingID.
                nlResponsesNotificationGroups = docNotificationsResponses.DocumentElement.SelectNodes("Notifications");

                // For each Notifications Group with the document...
                foreach(XmlNode nodeResponseNotifications in nlResponsesNotificationGroups)  {

                    // Each Notification has it's own SourceTrackingID.
                    attResponseSourceTrackingID = nodeResponseNotifications.Attributes["SourceTrackingID"];
                    if(attResponseSourceTrackingID != null && attResponseSourceTrackingID.Value.Length > 0) {

                        strSourceTrackingID = attResponseSourceTrackingID.Value;

                        if(ipoLib.TryParse(strSourceTrackingID, out gidTemp)) {
                            gidSourceTrackingID = gidTemp;
                        } else {
                            OnOutputLogAndTrace("Invalid SourceTrackingID on response document: [" + strSourceTrackingID + "]", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                            gidSourceTrackingID = Guid.Empty;
                        }

                        // Here, we're looking for the original input file that is associated to the input Notification, and 
                        // was originally dropped into the 'pending' folder.  This only applies to File-Drops, and not 
                        // Polling Processes.
                        arPendingResponseInputFiles = Directory.GetFiles(this.ClientProcessOptions.PendingResponseFolder, "*" + strSourceTrackingID + "*");

                        foreach(string strPendingResponseFile in arPendingResponseInputFiles) {
                            if(string.IsNullOrEmpty(this.ClientProcessOptions.ArchiveFolder)) {
                                File.Delete(strPendingResponseFile);
                            } else {
                                // We found the original file.  Since we're now processing this
                                // response, we can move this file to the archive folder.
                                MoveFile(strPendingResponseFile, Path.Combine(this.ClientProcessOptions.ArchiveFolder, Path.GetFileName(strPendingResponseFile)), out fiCompletedFile);
                            }
                        }

                        // Each Notification has it's own ResponseTrackingID.
                        attResponseTrackingID = nodeResponseNotifications.Attributes["ResponseTrackingID"];
                        if(attResponseTrackingID != null && 
                            attResponseTrackingID.Value.Length > 0 && 
                            ipoLib.TryParse(attResponseTrackingID.Value, out gidTemp)) {

                            gidResponseTrackingID = gidTemp;
                
                            // Since a Notifications Group can contain multiple notifications, we need to 
                            // parse out each Notification from the list and process individually.
                            nlResponseNotifications = nodeResponseNotifications.SelectNodes("Notification"); 

                            foreach(XmlNode nodeResponseNotification in nlResponseNotifications) {

                                // Each Notification contains a unique NotificationTrackingID.
                                attNotificationTrackingID = nodeResponseNotification.Attributes["NotificationTrackingID"];
                                if(attNotificationTrackingID != null && 
                                    attNotificationTrackingID.Value.Length > 0 && 
                                    ipoLib.TryParse(attNotificationTrackingID.Value, out gidTemp)) {

                                    gidNotificationTrackingID = gidTemp;

                                    nodeResult = nodeResponseNotification.SelectSingleNode("Results");
                                    bolSuccess = (nodeResult != null && nodeResult.InnerText == "Success");

                                    if(bolSuccess) {
                                        if(gidSourceTrackingID != Guid.Empty) {
                                            OnResponseSuccess(gidSourceTrackingID);
                                        } else {
                                            OnOutputLogAndTrace("Unable to notify client OnResponseSuccess due to Invalid SourceTrackingID on response document: [" + strSourceTrackingID + "]", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                        }
                                    } else {
                                        if(gidSourceTrackingID != Guid.Empty) {
                                            OnResponseError(gidSourceTrackingID);
                                        } else {
                                            OnOutputLogAndTrace("Unable to notify client OnResponseError due to Invalid SourceTrackingID on response document: [" + strSourceTrackingID + "]", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                        }
                                    }

                                    if(bolSuccess) {
                                        // Save responses IF the Response folder has been defined.
                                        if(!string.IsNullOrEmpty(this.ClientProcessOptions.ResponseFolder)) {
                                            SaveResponseDoc(gidNotificationTrackingID, bolSuccess, nodeResponseNotification);
                                        }
                                    } else {
                                        // Also save response if the server reported a failure.
                                        if(string.IsNullOrEmpty(this.ClientProcessOptions.ResponseErrorFolder)) {
                                            OnOutputLogAndTrace("Response reported failure.  ResponseErrorFolder was not defined - no response file will be saved.  NotificationTrackingID = " + gidNotificationTrackingID.ToString(), this.GetType().Name, LTAMessageType.Warning, LTAMessageImportance.Essential);
                                        } else {
                                            SaveResponseDoc(gidNotificationTrackingID, bolSuccess, nodeResponseNotification);
                                        }
                                    }

                                    // Notify the service that the response has been recieved, and can be marked as complete at the server.
                                    if(svcClient.SendResponseComplete(gidResponseTrackingID, gidNotificationTrackingID)) {
                                        OnOutputLogAndTrace("Notified the service response complete for Notification Tracking ID: [" + gidNotificationTrackingID.ToString() + "].", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Verbose);
                                    } else {
                                        OnOutputLogAndTrace("Failed to notify the service response completion for Notification Tracking ID: [" + gidNotificationTrackingID.ToString() + "].", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                    }
                            
                                    // When files were uploaded, we saved each notification to an individual 
                                    // document and saved to the 'PendingResponse' folder.  Now we need to find 
                                    // these files and move them to a complete state.
                                    arPendingResponseCanonicalFiles = Directory.GetFiles(this.ClientProcessOptions.PendingResponseFolder, "*" + gidNotificationTrackingID.ToString() + "*");

                                    if(arPendingResponseCanonicalFiles.Length > 0) {

                                        foreach(string strPendingResponseFile in arPendingResponseCanonicalFiles) {
                                            if(bolSuccess) {
                                                if(string.IsNullOrEmpty(this.ClientProcessOptions.ArchiveFolder)) {
                                                    File.Delete(strPendingResponseFile);
                                                } else {
                                                    // Move the pending Notification file to the Complete folder.
                                                    MoveFile(strPendingResponseFile, Path.Combine(this.ClientProcessOptions.ArchiveFolder, Path.GetFileName(strPendingResponseFile)), out fiCompletedFile);
                                                }
                                            } else {
                                                if(string.IsNullOrEmpty(this.ClientProcessOptions.ResponseErrorFolder)) {
                                                    File.Delete(strPendingResponseFile);
                                                } else {
                                                    // Move the pending Notification file to the Error Response folder.
                                                    MoveFile(strPendingResponseFile, Path.Combine(this.ClientProcessOptions.ResponseErrorFolder, Path.GetFileName(strPendingResponseFile)), out fiCompletedFile);
                                                }
                                            }
                                        }
                                    }

                                } else {
                                    OnOutputLogAndTrace("Could not retrieve NotificationTrackingID from Response Xml.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                                }
                            }

                        } else {
                            OnOutputLogAndTrace("Could not retrieve ResponseTrackingID from Response Xml.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                        }
                    } else {
                        OnOutputLogAndTrace("Could not retrieve SourceTrackingID from Response Xml.", this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                    }
                }

                bolRetVal = true;

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in ProcessNotificationResponses()", this.GetType().Name, ex);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        abstract protected void OnResponseSuccess(Guid sourceTrackingID);

        abstract protected void OnResponseError(Guid sourceTrackingID);

        private bool SaveResponseDoc(Guid notificationTrackingID, bool isSuccess, XmlNode responseNotificationNode) {

            XmlDocument docSaveRespXml;
            XmlNode nodeSaveRespXmlDocumentElement;
            XmlNode nodeSaveRespNotifications;
            XmlNode nodeSaveRespNotification;

            bool bolRetVal;

            try {
            
                docSaveRespXml = new XmlDocument();
                nodeSaveRespXmlDocumentElement = docSaveRespXml.ImportNode(responseNotificationNode.OwnerDocument.DocumentElement, false);
                nodeSaveRespNotifications = docSaveRespXml.ImportNode(responseNotificationNode.ParentNode, false);
                nodeSaveRespNotification = docSaveRespXml.ImportNode(responseNotificationNode, true);

                nodeSaveRespNotifications.AppendChild(nodeSaveRespNotification);
                nodeSaveRespXmlDocumentElement.AppendChild(nodeSaveRespNotifications);
                docSaveRespXml.AppendChild(nodeSaveRespXmlDocumentElement);

                if(isSuccess) {
                    docSaveRespXml.Save(Path.Combine(ClientProcessOptions.ResponseFolder, notificationTrackingID.ToString() + "_resp.xml"));
                } else {
                    docSaveRespXml.Save(Path.Combine(ClientProcessOptions.ResponseErrorFolder, notificationTrackingID.ToString() + "_resp.xml"));
                }

                bolRetVal = true;

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected error occurred in SaveResponseDoc()", this.GetType().Name, ex);
                bolRetVal = false;
            } 

            return(bolRetVal);
        }

        protected void SavePendingResponseDocuments(XmlDocument docNotificationData, string originalFileName, _ClientProcessOptionsBase processOptions) {

            string strPendingResponseXmlFileName;

            XmlNodeList nlNotifications;
            Guid gidNotificationTrackingID;
            XmlDocument docPendingNotification;
            XmlNode nodeRoot;
            XmlNode nodeNotification;

            nlNotifications = docNotificationData.SelectNodes("Notifications/Notification");
            foreach(XmlNode node in nlNotifications) {
                gidNotificationTrackingID = new Guid(node.Attributes["NotificationTrackingID"].Value.ToString());
                docPendingNotification = new XmlDocument();
                nodeRoot = docPendingNotification.ImportNode(docNotificationData.DocumentElement, false);
                nodeNotification = docPendingNotification.ImportNode(node, true);
                nodeRoot.AppendChild(nodeNotification);
                docPendingNotification.AppendChild(nodeRoot);

                strPendingResponseXmlFileName = string.Empty;
                if(!string.IsNullOrEmpty(originalFileName)) {
                    strPendingResponseXmlFileName = Path.GetFileNameWithoutExtension(originalFileName) + ".";
                }
                strPendingResponseXmlFileName += gidNotificationTrackingID.ToString() + "_src.xml";
                strPendingResponseXmlFileName = Path.Combine(processOptions.PendingResponseFolder, strPendingResponseXmlFileName);
                docPendingNotification.Save(strPendingResponseXmlFileName);
            }
        }

        protected bool MoveFile(string fromFilePath, string toFilePath, out FileInfo movedFileInfo) {

            bool bolRetVal;
            int intFileIncrementer = 1;
            string strIncrementedToFilePath;
            FileInfo fiMovedFileInfo;

            try {
                if(File.Exists(toFilePath)) {
                    while(true) {
                        FileInfo fiToFile = new FileInfo(toFilePath);
                        strIncrementedToFilePath = Path.Combine(fiToFile.DirectoryName, Path.GetFileNameWithoutExtension(toFilePath) + " - Copy (" + intFileIncrementer.ToString() + ")" + Path.GetExtension(toFilePath));
                        if(File.Exists(strIncrementedToFilePath)) {
                            intFileIncrementer++;
                        } else {
                            bolRetVal = MoveFile(fromFilePath, strIncrementedToFilePath);
                            fiMovedFileInfo = new FileInfo(strIncrementedToFilePath);
                            break;
                        }
                    }
                } else {
                    if(MoveFile(fromFilePath, toFilePath)) {
                        fiMovedFileInfo = new FileInfo(toFilePath);
                        bolRetVal = true;
                    } else {
                        fiMovedFileInfo = null;
                        bolRetVal = false;
                    }
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected error occurred in MoveFile()", this.GetType().Name, ex);
                fiMovedFileInfo = null;
                bolRetVal = false;
            } 

            movedFileInfo = fiMovedFileInfo;

            return(bolRetVal);
        }

        private bool MoveFile(string fromFilePath, string toFilePath) {

            bool bolContinue = true;
            int intRetryCount = 0;
            bool bolRetVal = false;

            while(bolContinue) {

                try {
                    File.Move(fromFilePath, toFilePath);
                    bolRetVal = true;
                    bolContinue = false;
                } catch(Exception ex) {

                    if(intRetryCount < 3) {
                        intRetryCount++;
                        OnOutputLogAndTrace("Unable to move file.  Retrying (" + intRetryCount + " of 3)", this.GetType().Name, ex);
                    } else {
                        OnOutputLogAndTrace("Unable to move file.  Retry attempts exhausted.", this.GetType().Name, ex);
                        OnOutputLogAndTrace("    From: " + fromFilePath, this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                        OnOutputLogAndTrace("      To: " + toFilePath, this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                        bolContinue = false;
                    }
                }
            }

            return(bolRetVal);
        }

        private void LogAndTraceResponseXml(string respXml, string src, bool requestWasSuccessful, LTAMessageImportance msgImportance) {

            XmlDocument docRespXml;
            XmlNodeList nlMessage;
            LTAMessageType enmMsgType;

            if(!string.IsNullOrEmpty(respXml)) {
                try {
                    docRespXml = new XmlDocument();
                    docRespXml.LoadXml(respXml);
                } catch(Exception) {
                    docRespXml = null;
                    OnOutputLogAndTrace("Could not load response Xml from the server.", src, (requestWasSuccessful ? LTAMessageType.Warning : LTAMessageType.Error), msgImportance);
                    OnOutputLogAndTrace("Response message: " + respXml, src, (requestWasSuccessful ? LTAMessageType.Warning : LTAMessageType.Error), msgImportance);
                }

                if(docRespXml != null) {
                    OnOutputLogAndTrace("Received response Xml from the server.", src, (requestWasSuccessful ? LTAMessageType.Information : LTAMessageType.Error), msgImportance);

                    nlMessage = docRespXml.SelectNodes("/Root/Messages/Message");
                    if(nlMessage.Count > 0) {
                        foreach(XmlNode node in nlMessage) {
                            if(node.Attributes["Type"] != null) {
                                switch(node.Attributes["Type"].Value) {
                                    case "Information":
                                        enmMsgType = LTAMessageType.Information;
                                        break;
                                    case "Warning":
                                        enmMsgType = LTAMessageType.Warning;
                                        break;
                                    default:
                                        enmMsgType = (requestWasSuccessful ? LTAMessageType.Information : LTAMessageType.Error);
                                        break;
                                }
                            } else {
                                enmMsgType = (requestWasSuccessful ? LTAMessageType.Information : LTAMessageType.Error);
                            }
                        
                            OnOutputLogAndTrace(node.InnerText, src, enmMsgType, msgImportance);
                        }
                    } else {
                        OnOutputLogAndTrace("No messages were found in the response Xml.", src, (requestWasSuccessful ? LTAMessageType.Warning : LTAMessageType.Error), msgImportance);
                    }
                }
            } else {
                OnOutputLogAndTrace("No response message was received from the server.", src, (requestWasSuccessful ? LTAMessageType.Warning : LTAMessageType.Error), msgImportance);
            }
        }

        protected void xClient_OutputLog(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance) {
            OnOutputLog(msg, src, FileImportClientAPILib.ConvertFITtoLTA(messageType), FileImportClientAPILib.ConvertFITtoLTA(messageImportance));
        }

        protected void xClient_OutputTrace(string msg, FITClientConsoleLTAMessageType messageType) {
            OnOutputTrace(msg, FileImportClientAPILib.ConvertFITtoConsoleLTA(messageType));
        }

        protected void xClient_OutputLogAndTrace(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance) {
            OnOutputLogAndTrace(msg, src, FileImportClientAPILib.ConvertFITtoLTA(messageType), FileImportClientAPILib.ConvertFITtoLTA(messageImportance));
        }
        
        protected void xClient_OutputLogAndTraceException(string msg, string src, Exception ex) {
            OnOutputLogAndTrace(msg, src, ex);
        }
        
    }
}
