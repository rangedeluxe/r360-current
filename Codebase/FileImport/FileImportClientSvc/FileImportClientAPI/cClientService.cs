﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using WFS.LTA.Common;
using WFS.LTA.FileImport.FileImportServicesClient;
using WFS.LTA.FileImportClientLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    public class cClientService : _ClientAPIBase, IDisposable {

        private cProcessOptionsList _ProcessOptionsList = null;
		private const int _sleepInterval = 30 * 1000; // 30,000 miliseconds ==> 30 seconds

		private List<cFileDropClientProcessOptions> _FileDropClientProcessOptions;
        private List<FileSystemWatcher> _FileDropProcessorFileWatchers;
        private List<System.Timers.Timer> _FileWatcherResponseTimers;
        private System.Timers.Timer _FileWatcherBackupTimer;

        private List<cPollingClientProcessOptions> _PollingClientProcessOptions;
        private List<System.Timers.Timer> _PollingProcessorTimers;
        private List<System.Timers.Timer> _PollingProcessorResponseTimers;

        private bool _disposed;
        private FileDataServiceClient objDataClient;
        private Guid sessionId;
        private int userId;

		/// <summary>
		/// 
		/// </summary>
		private cProcessOptionsList ProcessOptionsList {
            get {
                try {
                    if (_ProcessOptionsList == null) { 
                        _ProcessOptionsList = new cProcessOptionsList();
                    }
                } catch(Exception ex) {
                    OnOutputLogAndTrace("An unexpected exception occurred in ProcessOptionsList()", this.GetType().Name, ex);
                }

                return _ProcessOptionsList;
            }
        }

	    private void Initialize()
	    {
		    var success = false;
		    int retries = 0;
		    do
		    {
			    try
			    {
				    //Connect to FIT Service
				    retries++;
				    OnOutputLogAndTrace(
						"Attempting to connect to FileDataServiceClient.",
					    this.GetType().Name,
					    LTAMessageType.Information,
					    LTAMessageImportance.Essential);
					objDataClient = new FileDataServiceClient
				    (new ConnectionContext
					    {
						    SiteKey = ClientOptions.siteKey,
						    DataServiceLocation = ClientOptions.DataServiceLocation,
						    Entity = string.Empty,
						    Logger = this.EventLog
					    }
				    );

				    objDataClient.GetSession(ClientOptions.LogonName, ClientOptions.Password, out userId, out sessionId);
					success = true;
			    }
			    catch (Exception e)
			    {
					OnOutputLogAndTrace(
						$"Failed to connect to FIT Service, try {retries}.  Retrying in {_sleepInterval / 1000} seconds.",
						this.GetType().Name,
						LTAMessageType.Information,
						LTAMessageImportance.Essential);
					Thread.Sleep(_sleepInterval);
			    }
		    } while (success == false);
		}

		public cClientService(cClientOptions clientOptions) : base(clientOptions) {
			_FileDropProcessorFileWatchers = new List<FileSystemWatcher>();
			_PollingProcessorTimers = new List<System.Timers.Timer>();

			_FileDropClientProcessOptions = new List<cFileDropClientProcessOptions>();
			_PollingClientProcessOptions = new List<cPollingClientProcessOptions>();

			_FileWatcherResponseTimers = new List<System.Timers.Timer>();
			_PollingProcessorResponseTimers = new List<System.Timers.Timer>();

			_disposed = false;
		}

        public void StartService(string[] args)
        {
	        Initialize();
			BuildImportProcesses();
        }

        public void StopService() {
            DestroyImportProcesses();
        }

        private void BuildImportProcesses() {

            cFileDropClientProcessOptions objFileDropClientProcess;
            System.Timers.Timer objTimer;
            System.IO.FileSystemWatcher objFileSystemWatcher;

            try {

                OnOutputLogAndTrace(
                    "File Import Client Service started normally.",
                    this.GetType().Name,
                    LTAMessageType.Information,
                    LTAMessageImportance.Essential);

                OnOutputLogAndTrace(
                    "    Set Deadlock Priority: " + ClientOptions.SetDeadlockPriority.ToString(), 
                    this.GetType().Name, 
                    LTAMessageType.Information,
                    LTAMessageImportance.Essential);

                OnOutputLogAndTrace(
                    "    Query Retry Attempts: " + ClientOptions.QueryRetryAttempts.ToString(), 
                    this.GetType().Name, 
                    LTAMessageType.Information,
                    LTAMessageImportance.Essential);

                if(ClientOptions.SkipClientXmlValidation) {
                    OnOutputLogAndTrace(
                        "    Skipping Client Xml validation.  This option should be disabled when running in production mode", 
                        this.GetType().Name, 
                        LTAMessageType.Warning,
                        LTAMessageImportance.Essential);
                }

                OnOutputClientProcessList();

                if(ProcessOptionsList.Count > 0) {

                    foreach(IClientProcessOptions xproc in ProcessOptionsList) {

                        if(xproc is cPollingClientProcessOptions) {

                            _PollingClientProcessOptions.Add((cPollingClientProcessOptions)xproc);

                            objTimer = new System.Timers.Timer(1000 * 60 * 1);  // One Minute
                            objTimer.Elapsed += new System.Timers.ElapsedEventHandler(Timer_Elapsed);
                            objTimer.Start();
                            _PollingProcessorTimers.Add(objTimer);

                            objTimer = new System.Timers.Timer(1000 * 60 * 1);  // One Minute
                            objTimer.Elapsed += new System.Timers.ElapsedEventHandler(PollingProcessResponseTimer_Elapsed);
                            objTimer.Start();
                            _PollingProcessorResponseTimers.Add(objTimer);

                        } else if(xproc is cFileDropClientProcessOptions) {

                            objFileDropClientProcess = (cFileDropClientProcessOptions)xproc;
                            _FileDropClientProcessOptions.Add(objFileDropClientProcess);

                            objFileSystemWatcher = new FileSystemWatcher(objFileDropClientProcess.InputFolder);
                            objFileSystemWatcher.Created += new FileSystemEventHandler(FileSystemWatcher_Created);
                            objFileSystemWatcher.Changed += new FileSystemEventHandler(FileSystemWatcher_Changed);
                            objFileSystemWatcher.Renamed += new RenamedEventHandler(FileSystemWatcher_Renamed);
                            objFileSystemWatcher.EnableRaisingEvents = true;
                            _FileDropProcessorFileWatchers.Add(objFileSystemWatcher);

                            objTimer = new System.Timers.Timer(1000 * 60 * 1);  // One Minute
                            objTimer.Elapsed += new System.Timers.ElapsedEventHandler(FileWatcherResponseTimer_Elapsed);
                            objTimer.Start();
                            _FileWatcherResponseTimers.Add(objTimer);
                        }

                    }

                    if(_FileDropProcessorFileWatchers.Count > 0) {
                        _FileWatcherBackupTimer = new System.Timers.Timer();
                        _FileWatcherBackupTimer.Interval = 1000 * 10;   // 10 Seconds
                        _FileWatcherBackupTimer.Elapsed += new System.Timers.ElapsedEventHandler(_FileWatcherBackupTimer_Elapsed);
                        _FileWatcherBackupTimer.Enabled = true;
                    }

                } else {
                    OnOutputLogAndTrace(
                        "No Client Processes defined.", 
                        this.GetType().Name, 
                        LTAMessageType.Information, 
                        LTAMessageImportance.Essential);
                }

            } catch(Exception e) {
                OnOutputLogAndTrace(
                    "File Import Client Service started with errors : " + e.Message, 
                    e.Source, 
                    LTAMessageType.Error, 
                    LTAMessageImportance.Essential);
            }
        }

        public void RunOnce() {

	        Initialize();

            int intFilesProcessed = 0;

            cPollingProcessor objPollingProcessor;
            cFileDropProcessor objFileDropProcessor;

            int i;

            try {

                OnOutputTrace("Here's a list of all of the processes defined by the", LTAConsoleMessageType.TraceInfo);
                OnOutputTrace("File Import Toolkit Console: ", LTAConsoleMessageType.TraceInfo);

                if(this.ProcessOptionsList.Count> 0) {
                    
                    OnOutputTrace(string.Empty, LTAConsoleMessageType.Information);
                    OnOutputClientProcessList();

                    OnOutputTrace("Let's run each of the process groups exactly once!", LTAConsoleMessageType.TraceInfo);
                    OnOutputTrace(string.Empty, LTAConsoleMessageType.Information);

                    intFilesProcessed = 0;

                    i = 0;

                    foreach(IClientProcessOptions xproc in this.ProcessOptionsList) {

                        i += 1;

                        OnOutputTrace("Processing Client Process " + i.ToString("##") + ": " + xproc.ClientProcessCode, LTAConsoleMessageType.TraceInfo);
                    
                        if(xproc is cPollingClientProcessOptions) {
                            objPollingProcessor = new cPollingProcessor(this.ClientOptions, (cPollingClientProcessOptions)xproc, objDataClient);
                            objPollingProcessor.OutputLog += OnOutputLog;
                            objPollingProcessor.OutputTrace += OnOutputTrace;   //XProc_OutputMessage;
                            objPollingProcessor.ProcessPendingNotifications();
                        } else if(xproc is cFileDropClientProcessOptions) {
                            objFileDropProcessor = new cFileDropProcessor(this.ClientOptions, (cFileDropClientProcessOptions)xproc, objDataClient);
                            objFileDropProcessor.OutputLog += OnOutputLog;
                            objFileDropProcessor.OutputTrace += OnOutputTrace;  //XProc_OutputMessage;
                            objFileDropProcessor.ProcessPendingFiles(out intFilesProcessed);
                        }
                    }

                } else {
                    OnOutputTrace("...", LTAConsoleMessageType.TraceInfo);
                    OnOutputTrace("I guess there weren't any.", LTAConsoleMessageType.TraceInfo);
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in RunOnce()", this.GetType().Name, ex);
            }
        }

        private void OnOutputClientProcessList() {

            int i = 1;

            try {

                foreach(IClientProcessOptions xproc in this.ProcessOptionsList) {

                    OnOutputLogAndTrace("Client Process " + i.ToString("##") + ": " + xproc.ClientProcessCode, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);

                    switch(xproc.ClientType) {
                        case (int)Support.CLIENT_TYPE_FILE_DROP:
                            OnOutputLogAndTrace("  Type: File-Drop Processor", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                            break;
                        case (int)Support.CLIENT_TYPE_POLL:
                            OnOutputLogAndTrace("  Type: Polling Processor", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                            break;
                        default:
                            OnOutputLogAndTrace("  Type: Unknown", this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                            break;
                    }

                    if(xproc is cFileDropClientProcessOptions) {
                        OnOutputLogAndTrace("  Input Folder: " + ((cFileDropClientProcessOptions)xproc).InputFolder, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                        OnOutputLogAndTrace("  InProcess Folder: " + ((cFileDropClientProcessOptions)xproc).InProcessFolder, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                        OnOutputLogAndTrace("  Pending Response Folder: " + ((cFileDropClientProcessOptions)xproc).PendingResponseFolder, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                        OnOutputLogAndTrace("  Response Folder: " + ((cFileDropClientProcessOptions)xproc).ResponseFolder, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                        OnOutputLogAndTrace("  ArchiveFolder: " + xproc.ArchiveFolder, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                        OnOutputLogAndTrace("  Input Error Folder: " + ((cFileDropClientProcessOptions)xproc).InputErrorFolder, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                        OnOutputLogAndTrace("  Response Error Folder: " + ((cFileDropClientProcessOptions)xproc).ResponseErrorFolder, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                        OnOutputLogAndTrace("  File Pattern: " + ((cFileDropClientProcessOptions)xproc).FilePattern, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                    } else if(xproc is cPollingClientProcessOptions) {
                        //OnOutputTrace("  Pending Response Folder: " + ((cPollingProcessor)xproc).PendingResponseFolder, LTAConsoleMessageType.Information);
                        //OnOutputTrace("  Response Folder: " + ((cPollingProcessor)xproc).ResponseFolder, LTAConsoleMessageType.Information);
                        OnOutputLogAndTrace("  ArchiveFolder: " + xproc.ArchiveFolder, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
                        //OnOutputTrace("  Input Error Folder: " + ((cPollingProcessor)xproc).InputErrorFolder, LTAConsoleMessageType.Information);
                        //OnOutputTrace("  Response Error Folder: " + ((cPollingProcessor)xproc).ResponseErrorFolder, LTAConsoleMessageType.Information);
                    }

                    OnOutputLogAndTrace("  XClientDllFilePath: " + xproc.XClientDllFilePath, this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);

                    ++i;
                    OnOutputTrace(string.Empty, LTAConsoleMessageType.Information);
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in OnOutputClientProcessList()", this.GetType().Name, ex);
            }
        }

        private void DestroyImportProcesses() {

            _ProcessOptionsList = null;
            _FileDropClientProcessOptions = null;
            _PollingClientProcessOptions = null;

            try {
                if(_FileDropProcessorFileWatchers != null) {
                    foreach(FileSystemWatcher fsw in _FileDropProcessorFileWatchers) {
                        if(fsw != null) {
                            fsw.Dispose();
                        }
                    }
                    _FileDropProcessorFileWatchers = null;
                }
            } catch(Exception) {
                // Do Nothing on purpose.
            }

            try {
                if(_PollingProcessorTimers != null) {
                    foreach(System.Timers.Timer timer in _PollingProcessorTimers) {
                        if(timer != null) {
                            timer.Dispose();
                        }
                    }
                    _PollingProcessorTimers = null;
                }
            } catch(Exception) {
                // Do Nothing on purpose.
            }

            if(_FileWatcherBackupTimer != null) {
                _FileWatcherBackupTimer.Dispose();
            }
        }

        private void FileSystemWatcher_Renamed(object sender, RenamedEventArgs e) {
            OnOutputTrace("FileSystemWatcher_Renamed", LTAConsoleMessageType.Information);
            ProcessFileDrop(sender);
        }

        private void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e) {
            OnOutputTrace("FileSystemWatcher_Changed", LTAConsoleMessageType.Information);
            ProcessFileDrop(sender);
        }

        private void FileSystemWatcher_Created(object sender, FileSystemEventArgs e) {
            OnOutputTrace("FileSystemWatcher_Created", LTAConsoleMessageType.Information);
            ProcessFileDrop(sender);
        }

        private void ProcessFileDrop(object sender) {

            cFileDropProcessor objFileDropProcessor;
            int intFilesProcessed;

            try {

                    ((FileSystemWatcher)sender).EnableRaisingEvents = false;

                    for(int i=0;i<_FileDropProcessorFileWatchers.Count;++i) {
                        if(_FileDropProcessorFileWatchers[i].Equals(sender)) {


                            objFileDropProcessor = new cFileDropProcessor(this.ClientOptions, (cFileDropClientProcessOptions)_FileDropClientProcessOptions[i], objDataClient);

                            objFileDropProcessor.OutputLog += XProc_LogEvent;
                            objFileDropProcessor.OutputTrace += XProc_TraceEvent;

                            objFileDropProcessor.ProcessPendingFiles(out intFilesProcessed);


                            break;
                        }
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in ProcessFileDrop()", this.GetType().Name, ex);
            } finally {
                ((FileSystemWatcher)sender).EnableRaisingEvents = true;
            }
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {

            cPollingProcessor objPollingProcessor;

            try {

                ((System.Timers.Timer)sender).Stop();

                for(int i=0;i<_PollingProcessorTimers.Count;++i) {
                    if(_PollingProcessorTimers[0].Equals(sender)) {
                        objPollingProcessor = new cPollingProcessor(this.ClientOptions, (cPollingClientProcessOptions)_PollingClientProcessOptions[i], objDataClient);

                        objPollingProcessor.OutputLog += XProc_LogEvent;
                        objPollingProcessor.OutputTrace += XProc_TraceEvent;

                        objPollingProcessor.ProcessPendingNotifications();
                        break;
                    }
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in Timer_Elapsed()", this.GetType().Name, ex);
            } finally {
                ((System.Timers.Timer)sender).Start();
            }

        }

        private void PollingProcessResponseTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {

            cPollingProcessor objPollingProcessor;

            try {

                ((System.Timers.Timer)sender).Stop();

                for(int i=0;i<_PollingProcessorResponseTimers.Count;++i) {
                    if(_PollingProcessorResponseTimers[i].Equals(sender)) {
                        objPollingProcessor = new cPollingProcessor(this.ClientOptions, (cPollingClientProcessOptions)_PollingClientProcessOptions[i], objDataClient);

                        objPollingProcessor.OutputLog += XProc_LogEvent;
                        objPollingProcessor.OutputTrace += XProc_TraceEvent;
                        objPollingProcessor.ProcessResponses();

                        break;
                    }
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in PollingProcessResponseTimer_Elapsed()", this.GetType().Name, ex);
            } finally {
                ((System.Timers.Timer)sender).Start();
            }
        }

        private void FileWatcherResponseTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {

            cFileDropProcessor objFileDropProcessor;

            try {

                ((System.Timers.Timer)sender).Stop();

                for(int i=0;i<_FileWatcherResponseTimers.Count;++i) {
                    if(_FileWatcherResponseTimers[i].Equals(sender)) {
                        objFileDropProcessor = new cFileDropProcessor(this.ClientOptions, (cFileDropClientProcessOptions)_FileDropClientProcessOptions[i], objDataClient);

                        objFileDropProcessor.OutputLog += XProc_LogEvent;
                        objFileDropProcessor.OutputTrace += XProc_TraceEvent;

                        objFileDropProcessor.ProcessResponses();
                        break;
                    }
                }

            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in FileWatcherResponseTimer_Elapsed()", this.GetType().Name, ex);
            } finally {
                ((System.Timers.Timer)sender).Start();
            }

            ((System.Timers.Timer)sender).Start();
        }

        private void _FileWatcherBackupTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e) {

            try {

                _FileWatcherBackupTimer.Enabled = false;

                if(_FileDropProcessorFileWatchers != null) {
                    foreach(System.IO.FileSystemWatcher fw in _FileDropProcessorFileWatchers) {
                        ProcessFileDrop(fw);
                    }
                }
            } catch(Exception ex) {
                OnOutputLogAndTrace("An unexpected exception occurred in _FileWatcherBackupTimer_Elapsed()", this.GetType().Name, ex);
            } finally {

                if(_FileWatcherBackupTimer.Interval != 1000 * 60 * 2) {
                    _FileWatcherBackupTimer.Interval = 1000 * 60 * 2;   // Two Minutes
                }

                _FileWatcherBackupTimer.Enabled = true;
            }
        }

        private void XProc_LogEvent(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance) {
            OnOutputLog(msg, src, messageType, messageImportance);
        }

        private void XProc_TraceEvent(string msg, LTAConsoleMessageType LTAConsoleMessageType) {
            OnOutputTrace(msg, LTAConsoleMessageType);
        }

        public void Dispose() {

            Dispose(true);

            // Use SupressFinalize in case a subclass
            // of this type implements a finalizer.
            GC.SuppressFinalize(this);      
        }

        protected virtual void Dispose(bool disposing) {

            // If you need thread safety, use a lock around these 
            // operations, as well as in your methods that use the resource.
            if (!_disposed) {
                if (disposing) {
                    DestroyImportProcesses();
                }
                if (objDataClient.EndSession())
                    OnOutputLog("Successfully Ended Session", string.Empty, LTAMessageType.Information, LTAMessageImportance.Essential);
                // Indicate that the instance has been disposed.
                _disposed = true;   
            }
        }
    }
}
