﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 50480 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientAPI {

    public abstract class _ClientAPIBase {

        private cClientOptions _ClientOptions;
        private ltaLog _EventLog = null;

        public event LTAOutputLogEventHandler OutputLog;
        public event LTAOutputTraceEventHandler OutputTrace;
        public event LTAOutputLogAndTraceEventHandler OutputLogAndTrace;
        public event LTAOutputLogAndTraceExceptionEventHandler OutputLogAndTraceException;

        protected _ClientAPIBase(cClientOptions clientOptions) {
            _ClientOptions = clientOptions;
        }

        protected cClientOptions ClientOptions {
            get {
                return(_ClientOptions);
            }
        }

        protected ltaLog EventLog {
            get {
                if(_EventLog == null) {
                    _EventLog = new ltaLog(ClientOptions.logFilePath, ClientOptions.logFileMaxSize, ClientOptions.LTALoggingDepth);
                }
                return(_EventLog);
            }
        }

        protected void OnOutputLog(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance) {
            if(OutputLog == null) {
                EventLog.logEvent(msg, src, messageType, messageImportance); 
            } else {
                OutputLog(msg, src, messageType, messageImportance);
            }
        }

        protected void OnOutputTrace(string msg, LTAConsoleMessageType messageType) {
            if(OutputTrace == null) {
                LTAConsole.ConsoleWriteLine(msg);
            } else {
                OutputTrace(msg, messageType);
            }
        }

        protected void OnOutputLogAndTrace(string msg, string src, Exception ex) {
            if(OutputLogAndTraceException == null) {
                OnOutputLog(msg, src, LTAMessageType.Error, LTAMessageImportance.Essential);
                OnOutputLog("Exception: " + ex.Message, src, LTAMessageType.Error, LTAMessageImportance.Essential);

                OnOutputTrace(msg, LTAConsoleMessageType.Error);
                OnOutputTrace("Exception: " + ex.Message, LTAConsoleMessageType.Error);
            } else {
                OutputLogAndTraceException(msg, src, ex);
            }
        }

        protected void OnOutputLogAndTrace(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance) {
            if(OutputLogAndTrace == null) {
                OnOutputLog(msg, src, messageType, messageImportance);
                OnOutputTrace(msg, LTAConvert.ConvertLTALogToConsoleLTA(messageType));
            } else {
                OutputLogAndTrace(msg, src, messageType, messageImportance);
            }
        }
    }
}
