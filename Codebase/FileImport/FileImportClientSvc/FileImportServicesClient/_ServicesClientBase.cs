﻿using System;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 54178 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServicesClient {

    public abstract class _ServicesClientBase { // : System.ServiceModel.ClientBase<IFileImportService> {

        private cSiteOptions _SiteOptions = null;
        private ltaLog _EventLog = null;
        private string _SiteKey;

        public _ServicesClientBase()
        {
        }

        public _ServicesClientBase(string vSiteKey) {
            _SiteKey = vSiteKey;
        }

        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        protected string SiteKey
        {
            get { return _SiteKey; }
            set { _SiteKey = value; }
        }

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected cSiteOptions SiteOptions{
            get{
                if (_SiteOptions == null){
                    _SiteOptions = new cSiteOptions(this.SiteKey);
                }
                return _SiteOptions;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected ltaLog EventLog {
            get{
                if (_EventLog == null){
                    _EventLog = new ltaLog(SiteOptions.logFilePath,
                                           SiteOptions.logFileMaxSize, 
                                           ConvertLoggingDepthToEnum(SiteOptions.loggingDepth));
                }

                return _EventLog;
            }

            set { _EventLog = value; }
        }

        public abstract AlertResponse LogAlert(AlertRequest requestContext);

        public abstract AuditResponse LogAuditEvent(AuditRequest requestContext);
       
        private static LTAMessageImportance ConvertLoggingDepthToEnum(int LoggingDepth) {

            switch(LoggingDepth) {
                case (int)LTAMessageImportance.Debug:
                    return(LTAMessageImportance.Debug);
                case (int)LTAMessageImportance.Essential:
                    return(LTAMessageImportance.Essential);
                case (int)LTAMessageImportance.Verbose:
                    return(LTAMessageImportance.Verbose);
                default:
                    return(LTAMessageImportance.Essential);
            }
        }

    }
}
