﻿using System;
using System.ServiceModel;
using WFS.LTA.FileImport.FileImportWCFLib;
using System.Runtime.CompilerServices;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.LTA.Common;
using WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 54178 JMC 07/16/2012 
*   -Initial version.
* WI 70443 TWE 12/11/2012
*   -Use SSL if endpoint is https
* WI 93065 WJS 3/19/2013
 *  - Change max receive message size to correct max size
 *    http://msdn.microsoft.com/en-us/library/ff599489%28v=office.14%29.aspx
 *    bottom comment:
 *      Limitation on MaxReceivedMessageSize being a Int32 property
 *      Being MaxReceivedMessageSize as Int32 property, maximum value that can be set is 2147483647 bytes.
 *       How to handle bigger values than this?
 *      Int32.MaxValue is 2gb, and is a hard limit (not just an administrative setting) that cannot be exceeded in any way. 
* WI 90209 WJS 3/4/2012
*   -FP:Use SSL if endpoint is https
*******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServicesClient {

    public class FileUploadServiceClient : _ServicesClientBase {

        private IFileUploadService _FileUploadService;
        private BasicHttpBinding _Binding = new BasicHttpBinding("BasicHttpBinding_MTOM");
        private EndpointAddress _Endpointaddress = null;
        private string _ServerLocation;

        private Guid _SessionID = Guid.Empty;

        [Obsolete]
        public FileUploadServiceClient(string vSiteKey, Guid sessionID, string serverLocation) : base(vSiteKey) 
        {
            Setup(new ConnectionContext {FileUploadServiceLocation = serverLocation, SiteKey = vSiteKey, SessionId = sessionID });
        }

        public FileUploadServiceClient(ConnectionContext connectionContext)
        {
            Setup(connectionContext);
        }

        private void Setup(ConnectionContext connectionContext)
        {
            if (connectionContext == null)
                throw new ArgumentNullException("Must provide connection context");

            base.SiteKey = connectionContext.SiteKey;
            base.EventLog = connectionContext.Logger;
            Entity = connectionContext.Entity;
            _SessionID = connectionContext.SessionId;
            _ServerLocation = connectionContext.FileUploadServiceLocation;
            SetEndPointAddress();
            CreateChannel();
        }

        private void SetEndPointAddress() {   
            _Endpointaddress = new EndpointAddress(_ServerLocation);
        }

        private void CreateChannel()
        {
            _FileUploadService = new ChannelFactory<IFileUploadService>(_Binding, _Endpointaddress).CreateChannel();
        }

        public string Ping() {

            try {
                return _FileUploadService.Ping();
            } catch(Exception ex) {
                EventLog.logError(ex, this.GetType().Name, "Ping");
                throw;
            }
        }
        
        public StreamResponse SendNotificationFile(StreamRequest item) 
        {
            return Execute<StreamResponse>(service => { return service.SendNotificationFile(item); }, item);
        }

        public string Entity
        {
            get;
            private set;
        }

        public override AlertResponse LogAlert(AlertRequest requestContext)
        {
            return Execute<AlertResponse>(service => { return service.Log(requestContext); }, null);
        }

        public override AuditResponse LogAuditEvent(AuditRequest requestContext)
        {
            return Execute<AuditResponse>(service => { return service.LogAuditEvent(requestContext); }, null);
        }

        private T Execute<T>(Func<IFileUploadService, T> operation, StreamRequest item, [CallerMemberName] string procName = null) where T : new()
        {
            try
            {
                return new OperationContextScope((IContextChannel)_FileUploadService).Use(service =>
                {
                    // The DataImportServiceContext is set and will exist for the duration of the operationscope
                    FileImportServiceContext.Current = new FileImportServiceContext(this.SiteKey, _SessionID.ToString(), item.NotificationFileInfo );
                    return operation(_FileUploadService);
                });
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, this.GetType().Name, procName);
                if (string.Equals(ex.Message, "session has expired.", StringComparison.InvariantCultureIgnoreCase))
                {
                    _SessionID = Guid.Empty;
                    EventLog.logEvent("Going to reestablish a session for the client", procName, LTAMessageType.Information, LTAMessageImportance.Essential);
                }

                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, procName);
                throw (new Exception(ex.Message, ex));
            }

        }
    }
}
