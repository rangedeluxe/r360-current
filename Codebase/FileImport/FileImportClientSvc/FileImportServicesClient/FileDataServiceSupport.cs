﻿using System.Runtime.Serialization;
using System;

using WFS.LTA.FileImport.FileImportWCFLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 54178 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServicesClient {
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ServiceFault", Namespace="http://Wfs.ServiceModel")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.InvalidSessionIDFault))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.DataValidationFailedFault))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.InvalidSiteKeyFault))]
    public partial class ServiceFault : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int CategoryField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ErrorCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.ServiceModel.ExceptionDetail ExceptionDetailField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MessageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SourceField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Category {
            get {
                return this.CategoryField;
            }
            set {
                if ((this.CategoryField.Equals(value) != true)) {
                    this.CategoryField = value;
                    this.RaisePropertyChanged("Category");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ErrorCode {
            get {
                return this.ErrorCodeField;
            }
            set {
                if ((this.ErrorCodeField.Equals(value) != true)) {
                    this.ErrorCodeField = value;
                    this.RaisePropertyChanged("ErrorCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.ServiceModel.ExceptionDetail ExceptionDetail {
            get {
                return this.ExceptionDetailField;
            }
            set {
                if ((object.ReferenceEquals(this.ExceptionDetailField, value) != true)) {
                    this.ExceptionDetailField = value;
                    this.RaisePropertyChanged("ExceptionDetail");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Message {
            get {
                return this.MessageField;
            }
            set {
                if ((object.ReferenceEquals(this.MessageField, value) != true)) {
                    this.MessageField = value;
                    this.RaisePropertyChanged("Message");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Source {
            get {
                return this.SourceField;
            }
            set {
                if ((object.ReferenceEquals(this.SourceField, value) != true)) {
                    this.SourceField = value;
                    this.RaisePropertyChanged("Source");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSessionIDFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    [System.SerializableAttribute()]
    public partial class InvalidSessionIDFault : WFS.LTA.FileImport.FileImportServicesClient.ServiceFault {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="DataValidationFailedFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    [System.SerializableAttribute()]
    public partial class DataValidationFailedFault : WFS.LTA.FileImport.FileImportServicesClient.ServiceFault {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="InvalidSiteKeyFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
    [System.SerializableAttribute()]
    public partial class InvalidSiteKeyFault : WFS.LTA.FileImport.FileImportServicesClient.ServiceFault {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.LTA.FileImport.FileImportServices")]
    [System.SerializableAttribute()]
    public partial class ServerFaultException : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string detailsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string errorcodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string messageField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string details {
            get {
                return this.detailsField;
            }
            set {
                if ((object.ReferenceEquals(this.detailsField, value) != true)) {
                    this.detailsField = value;
                    this.RaisePropertyChanged("details");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string errorcode {
            get {
                return this.errorcodeField;
            }
            set {
                if ((object.ReferenceEquals(this.errorcodeField, value) != true)) {
                    this.errorcodeField = value;
                    this.RaisePropertyChanged("errorcode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string message {
            get {
                return this.messageField;
            }
            set {
                if ((object.ReferenceEquals(this.messageField, value) != true)) {
                    this.messageField = value;
                    this.RaisePropertyChanged("message");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IFileDataService")]
    public interface IFileDataService {
        
        [System.ServiceModel.OperationContractAttribute(Action="Ping", ReplyAction="http://tempuri.org/IFileDataService/PingResponse")]
        string Ping();
        
        [System.ServiceModel.OperationContractAttribute(Action="GetSession", ReplyAction="http://tempuri.org/IFileDataService/GetSessionResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.InvalidSiteKeyFault), Action="http://tempuri.org/IFileDataService/GetSessionInvalidSiteKeyFaultFault", Name="InvalidSiteKeyFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.ServerFaultException), Action="http://tempuri.org/IFileDataService/GetSessionServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.LTA.FileImport.FileImportServices")]
        bool GetSession(string LogonName, string Password, out System.Guid SessionID);
        
        [System.ServiceModel.OperationContractAttribute(Action="EndSession", ReplyAction="http://tempuri.org/IFileDataService/EndSessionResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.InvalidSessionIDFault), Action="http://tempuri.org/IFileDataService/EndSessionInvalidSessionIDFaultFault", Name="InvalidSessionIDFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.ServerFaultException), Action="http://tempuri.org/IFileDataService/EndSessionServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.LTA.FileImport.FileImportServices")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.InvalidSiteKeyFault), Action="http://tempuri.org/IFileDataService/EndSessionInvalidSiteKeyFaultFault", Name="InvalidSiteKeyFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
        bool EndSession();
        
        [System.ServiceModel.OperationContractAttribute(Action="GetSchemaDefinition", ReplyAction="http://tempuri.org/IFileDataService/GetSchemaDefinitionResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.ServerFaultException), Action="http://tempuri.org/IFileDataService/GetSchemaDefinitionServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.LTA.FileImport.FileImportServices")]
        bool GetSchemaDefinition(string sType, string XSDVersion, out string SetupDataXsd, out string RespXml);
        
        [System.ServiceModel.OperationContractAttribute(Action="SendNotificationData", ReplyAction="http://tempuri.org/IFileDataService/SendNotificationDataResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.DataValidationFailedFault), Action="http://tempuri.org/IFileDataService/SendNotificationDataDataValidationFailedFaultFault", Name="DataValidationFailedFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.ServerFaultException), Action="http://tempuri.org/IFileDataService/SendNotificationDataServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.LTA.FileImport.FileImportServices")]
        bool SendNotificationData(string NotificationDataXml, out string RespXml);
        
        [System.ServiceModel.OperationContractAttribute(Action="GetNotificationResponses", ReplyAction="http://tempuri.org/IFileDataService/GetNotificationResponsesResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.ServerFaultException), Action="http://tempuri.org/IFileDataService/GetNotificationResponsesServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.LTA.FileImport.FileImportServices")]
        bool GetNotificationResponses(string clientProcessCode, out string notificationRespXml, out string respXml);
        
        [System.ServiceModel.OperationContractAttribute(Action="SendResponseComplete", ReplyAction="http://tempuri.org/IFileDataService/SendResponseCompleteResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.ServerFaultException), Action="http://tempuri.org/IFileDataService/SendResponseCompleteServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.LTA.FileImport.FileImportServices")]
        bool SendResponseComplete(Guid responseTrackingID, Guid notificationTrackingID);
    }
}
