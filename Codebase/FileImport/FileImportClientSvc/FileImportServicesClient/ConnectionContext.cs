﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common;

namespace WFS.LTA.FileImport.FileImportServicesClient
{
    public class ConnectionContext
    {
        public string SiteKey
        {
            get;
            set;
        }

        public string Entity
        {
            get;
            set;
        }

        public string DataServiceLocation
        {
            get;
            set;
        }

        public string FileUploadServiceLocation
        {
            get;
            set;
        }

        public ltaLog Logger
        {
            get;
            set;
        }

        public Guid SessionId
        {
            get;
            set;
        }
    }
}
