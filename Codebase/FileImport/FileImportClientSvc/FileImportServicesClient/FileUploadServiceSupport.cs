﻿using System.Runtime.Serialization;
using System;

using WFS.LTA.FileImport.FileImportWCFLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 54178 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServicesClient {

    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IFileUploadService")]
    public interface IFileUploadService {
        
        [System.ServiceModel.OperationContractAttribute(Action="Ping", ReplyAction="http://tempuri.org/IFileUploadService/PingResponse")]
        string Ping();
        
        // CODEGEN: Generating message contract since the wrapper name (UploadItem) of message UploadItem does not match the default value (SendNotificationFile)
        [System.ServiceModel.OperationContractAttribute(Action="SendNotificationFile", ReplyAction="http://tempuri.org/IFileUploadService/SendNotificationFileResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.ServerFaultException), Action="http://tempuri.org/IFileUploadService/SendNotificationFileServerFaultExceptionFault", Name="ServerFaultException", Namespace="http://schemas.datacontract.org/2004/07/WFS.LTA.FileImport.FileImportServicesBin")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.InvalidSessionIDFault), Action="http://tempuri.org/IFileUploadService/SendNotificationFileInvalidSessionIDFaultFault", Name="InvalidSessionIDFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
        [System.ServiceModel.FaultContractAttribute(typeof(WFS.LTA.FileImport.FileImportServicesClient.DataValidationFailedFault), Action="http://tempuri.org/IFileUploadService/SendNotificationFileDataValidationFailedFaultFault", Name="DataValidationFailedFault", Namespace="http://www.wfs.com/Security/v1.0.0")]
        WFS.LTA.FileImport.FileImportServicesClient.ReturnItemUpload SendNotificationFile(WFS.LTA.FileImport.FileImportServicesClient.UploadItem request);
    }

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UploadItem", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class UploadItem {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public System.IO.Stream data;

        [System.ServiceModel.MessageHeader(Namespace = "http://tempuri.org/")]
        public NotificationFileInfoContract contract;

       public UploadItem(NotificationFileInfoContract notificationFileContract, System.IO.Stream dataStream) {
           this.data = dataStream;
           this.contract = notificationFileContract;
       }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ReturnItemUpload", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class ReturnItemUpload {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="http://tempuri.org/")]
        public bool bRetVal;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string strRespXml;
        
        public ReturnItemUpload() {
        }
        
        public ReturnItemUpload(bool bRetVal, string strRespXml) {
            this.bRetVal = bRetVal;
            this.strRespXml = strRespXml;
        }
    }
}
