﻿using System;
using System.ServiceModel;
using WFS.LTA.FileImport.FileImportWCFLib;
using System.Runtime.CompilerServices;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.ImportToolkit.Common.Interfaces;
using WFS.RecHub.ImportToolkit.Common.DataTransferObjects;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 54178 JMC 07/16/2012 
*   -Initial version.
* WI 70443 TWE 12/11/2012
*   -Use SSL if endpoint is https
* WI 91062 TWE 03/07/2013
*    force binding to securemode=none
* WI 90209 WJS 03/04/2013
*   -FP:Use SSL if endpoint is https
*******************************************************************************/
namespace WFS.LTA.FileImport.FileImportServicesClient {

    public class FileDataServiceClient : _ServicesClientBase {

        private IFileDataService _FileDataService;
        private WS2007HttpBinding _Binding = new WS2007HttpBinding("WSHttpBinding_TEXT");
        private EndpointAddress _Endpointaddress = null;
        private string _ServerLocation;
         
        [Obsolete]
        public FileDataServiceClient(string siteKey, string serverLocation) 
            : base(siteKey) 
        {

            Setup(new ConnectionContext {DataServiceLocation = serverLocation, SiteKey = siteKey });
        }

        public FileDataServiceClient(ConnectionContext connectionContext)
        {
            Setup(connectionContext);
        }

        private void Setup(ConnectionContext connectionContext)
        {
            if (connectionContext == null)
                throw new ArgumentNullException("Must provide connection context");

            base.SiteKey = connectionContext.SiteKey;
            base.EventLog = connectionContext.Logger;
            Entity = connectionContext.Entity;

            _ServerLocation = connectionContext.DataServiceLocation;

            SetEndPointAddress();
            CreateChannel();
        }

        private void SetEndPointAddress() 
        {   

            _Endpointaddress = new EndpointAddress(_ServerLocation);
        }

        private void CreateChannel()
        {
            _FileDataService = new ChannelFactory<IFileDataService>(_Binding, _Endpointaddress).CreateChannel();
        }

        public string Ping() {

            string response = string.Empty;
            Execute<bool>(service => 
                { 
                    response = service.Ping(); 
                    return true; 
                });
            return response;
        }
        
        public bool GetSession(string logonName, string password, out int outuserid, out System.Guid sessionID) {

            bool bolRetVal;
            Guid gidSessionID = Guid.Empty;
            int userid = 0;
            if (SessionId == Guid.Empty)
            {
                bolRetVal = Execute<bool>(operation => { return operation.GetSession(logonName, password, out userid, out gidSessionID); });
                SessionId = bolRetVal ? gidSessionID : Guid.Empty;
                UserId = bolRetVal ? userid : default(int);
                if (bolRetVal)
                    EventLog.logEvent(string.Format("Session established: {0}", SessionId.ToString()), this.GetType().Name, LTAMessageType.Information, LTAMessageImportance.Essential);
            }
            else
                bolRetVal = true;

            outuserid = UserId;
            sessionID = SessionId;

            return(bolRetVal);
        }
        
        public bool EndSession() {

            if (SessionId != Guid.Empty)
                return Execute<bool>(operation => { return operation.EndSession(); });

            return false;
        }
        
        public bool GetSchemaDefinition(string schemaType, string xsdVersion,  out string dataXSD, out string respXml) {

            bool bolRetVal = false;
            var xmlResponse = string.Empty;
            var xsd = string.Empty;

            bolRetVal = Execute<bool>(operation => {return operation.GetSchemaDefinition(schemaType, xsdVersion, out xsd, out xmlResponse); });
            dataXSD = xsd;
            respXml = xmlResponse;

            return(bolRetVal);
        }
        
        public bool SendNotificationData(string notificationDataXml, out string respXml) {

            bool bolRetVal;
            var xmlResponse = string.Empty;

            bolRetVal = Execute<bool>(operation => { return operation.SendNotificationData(notificationDataXml, out xmlResponse); });
            respXml = xmlResponse;

            return(bolRetVal);
        }
        
        public bool GetNotificationResponses(
            string clientProcessCode,
            out string notificationRespXml,
            out string respXml) {

            bool bolRetVal;
            var notificationXmlResponse = string.Empty;
            var xmlResponse = string.Empty;

            bolRetVal = Execute<bool>(operation => { return operation.GetNotificationResponses(clientProcessCode, out notificationXmlResponse, out xmlResponse); });
            notificationRespXml = notificationXmlResponse;
            respXml = xmlResponse;

            return(bolRetVal);
        }
        
        public bool SendResponseComplete (Guid responseTrackingID, Guid notificationTrackingID) 
        {
            return Execute<bool>(operation => { return operation.SendResponseComplete(responseTrackingID, notificationTrackingID); });
        }

        public Guid SessionId
        {
            get;
            private set;
        }

        public int UserId
        {
            get;
            private set;
        }

        public string Entity
        {
            get;
            private set;
        }

        public override AlertResponse LogAlert(AlertRequest requestContext)
        {
            return Execute<AlertResponse>(service => { return service.Log(requestContext); });
        }

        public override AuditResponse LogAuditEvent(AuditRequest requestContext)
        {
            return Execute<AuditResponse>(service => { return service.LogAuditEvent(requestContext); });
        }

        private T Execute<T>(Func<IFileDataService, T> operation, [CallerMemberName] string procName = null) where T : new()
        {
            try
            {
                return new OperationContextScope((IContextChannel)_FileDataService).Use(service =>
                {
                    // The DataImportServiceContext is set and will exist for the duration of the operationscope
                    FileImportServiceContext.Current = new FileImportServiceContext(this.SiteKey, SessionId.ToString());
                    return operation(_FileDataService);
                });
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, this.GetType().Name, procName);
                if (string.Equals(ex.Message, "session has expired.", StringComparison.InvariantCultureIgnoreCase))
                {
                    SessionId = Guid.Empty;
                    EventLog.logEvent("Going to reestablish a session for the client", procName, LTAMessageType.Information, LTAMessageImportance.Essential);
                }

                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, procName);
                throw (new Exception(ex.Message, ex));
            }

        }

    }
}
