﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using WFS.LTA.Common;
using WFS.LTA.FileImportClientAPI;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 54178 JMC 07/16/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportClientConsole {

    internal class Program {

        private cClientOptions _ClientOptions = null;

	    private ltaLog _EventLog = null;

		private ltaLog LTAEventLog
	    {
		    get
		    {
			    if (_EventLog == null)
				{	// Read the app.settings directly rather than using the Client Options as the cClientOptions.GetSettings could
					// be what errored out, if it the user/password did not decode.
					int depth = 0;
					int fileMaxSize = 0;
					int.TryParse(ConfigurationManager.AppSettings.Get(cClientOptions.LOGGING_DEPTH), out depth);
					int.TryParse(ConfigurationManager.AppSettings.Get(cClientOptions.LOG_FILE_MAX_SIZE), out fileMaxSize);
					_EventLog = new ltaLog(ConfigurationManager.AppSettings.Get(cClientOptions.LOG_FILE),
						fileMaxSize,
						(LTAMessageImportance)depth);
				}

			    return _EventLog;
		    }
	    }


		static void Main(string[] args) {

            bool bolRunService = false;
            Program prog;

            try {
                if(args.Length > 0) {
                    foreach(string arg in args) {
                        if(arg.ToLower().Trim().StartsWith("/s")) {
                            bolRunService = true;
                        } else if(arg.ToLower().Trim().StartsWith("-s")) {
                            bolRunService = true;
                        }
                    }
                }

                prog = new Program();
                if(bolRunService) {
                    prog.RunService();
                } else {
                    prog.RunOnce();
                }

            } catch(Exception ex) {
                LTAConsole.ConsoleWriteLine("Exception occurred: " + ex.Message, (int)LTAMessageType.Error);
                PressAnyKey();
            }
        }

	   private void RunOnce() {

            try
            {
				cClientOptions objClientOptions = new cClientOptions();
                using(cClientService objClientService = new cClientService(objClientOptions)) {

                    objClientService.OutputLog += XProc_LogEvent;
                    objClientService.OutputTrace += XProc_TraceEvent;
                    objClientService.RunOnce();
                }
                PressAnyKey();

            } catch(Exception ex) {
				LTAEventLog.logEvent(ex.Message, "FileImportClientSvc", LTAMessageType.Error, LTAMessageImportance.Essential);
				PressAnyKey();
            }
        }

        private void RunService() {
	    

			try
			{
	            cClientOptions objClientOptions = new cClientOptions();
                using(cClientService objClientService = new cClientService(objClientOptions)) {

                    objClientService.OutputLog += XProc_LogEvent;
                    objClientService.OutputTrace += XProc_TraceEvent;

                    objClientService.StartService(new string[]{});

                    LTAConsole.ConsoleWriteLine(string.Empty);
                    LTAConsole.ConsoleWriteLine("Press Q to quit");

                    while (Console.ReadKey().KeyChar != 'Q') ;

                    objClientService.StopService();
                }

            } catch(Exception ex) {
				LTAEventLog.logEvent(ex.Message, "FileImportClientSvc", LTAMessageType.Error, LTAMessageImportance.Essential);
				PressAnyKey();
            }
        }

        private void XProc_LogEvent(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance) {
	        LTAEventLog.logEvent(msg, src, messageType, messageImportance);
        }

        private static void XProc_TraceEvent(string msg, LTAConsoleMessageType LTAConsoleMessageType) {
            LTAConsole.ConsoleWriteLine(msg, LTAConsoleMessageType);
        }

        private static void PressAnyKey() {
            LTAConsole.ConsoleWriteLine(string.Empty);
            LTAConsole.ConsoleWriteLine("Press any key to continue...");
            LTAConsole.ConsoleWriteLine(string.Empty);
            Console.ReadKey();
        }
    }
}
