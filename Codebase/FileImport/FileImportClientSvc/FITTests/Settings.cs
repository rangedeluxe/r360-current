﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppSettingsAbstract;

namespace FITTests
{
    public class Settings : cAppSettingAbstractClass
    {
        public string iniFile
            /// read-only property of the iniFile location.
            /// default: \WFSApps\RecHub\bin\FileImportClient.ini
            /// Assumes that the WFSApps directory is shared
        {
            get
            {
                return GetSetting("iniFile", @"\WFSApps\RecHub\bin\FileImportClient.ini");
            } // end get
        }

        public string configFile
        /// read-only property of the config file locations.
        /// default: \WFSApps\RecHub\bin\FileImportClientSvc.exe.config
        /// Assumes that the WFSApps directory is shared
        {
            get
            {
                return GetSetting("configFile", @"\WFSApps\RecHub\bin\FileImportClientSvc.exe.configs");
            }
        } // end configFile

        public string settingsNode
            /// read-only property of the FIT settings node path
            /// default: configuration/FileImportSettings/FileImportConfigSections/add
        {
            get
            {
                return GetSetting("settingsNode", "configuration/FileImportSettings/FileImportConfigSections/add");
            }
        }

        public string testMachine
            /// read-only property of the machine name where the FIT Client Service is running.
        {
            get
            {
                return GetSetting("testMachine", "");
            } // end get
        } // end machineName

        public string workDirectory
        {
            get
            {
                return GetSetting("workDirectory", "work");
            }
        } // end workDirectory
    }
}
