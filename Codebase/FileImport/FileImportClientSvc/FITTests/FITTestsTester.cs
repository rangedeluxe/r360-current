﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FITTests;
using System.Xml.Linq;
using TestingServicesLibrary;

namespace WFS.LTA.FileImportClientAPI
{
    class FITTestsTester
    {
        static Settings settings = new Settings();
        static FITTestFramework frame = new FITTestFramework(settings.testMachine);
        static ConfigFile fitConfig = new ConfigFile();
        
        public static void Main(string[] args)
        {
            DateTime testtime;
            Guid sourceTrackingID, sourceNotificationID;
            string workingFilePath, savedFilePath, logFilePath;
            Dictionary<string, string> notifications, notification;
            bool ErrorExpected = false
                , startupErrorExpected = false
                , cont = true;
            ChangeType[] changesToMake;

            if (args.Length < 2)
            {
                changesToMake = new ChangeType[2];
                changesToMake[1] = ChangeType.RunAsSetup;
            }
            else
            {
                changesToMake = new ChangeType[args.Length];
            }

            testtime = DateTime.Now;
            sourceTrackingID = new Guid("00000000-0000-0000-0000-000000000000");
            sourceNotificationID = sourceTrackingID;
            workingFilePath = null;
            savedFilePath = null;

            // 3a. Determine whether or not we expect an error response (InputError or ErrorResponse). Then, parse each
            //      subsequent parameter to see if it is a CONFIG file or an INI file change.
            if (args.Length >= 2)
            {
                int arg = 1;
                if (args[1].ToUpper() == "E")
                {
                    ErrorExpected = true;
                    changesToMake[1] = ChangeType.ExpectError;
                    arg++;
                }

                if (args[1].ToUpper() == "S")
                {
                    startupErrorExpected = true;
                    changesToMake[1] = ChangeType.ExpectShutdown;
                    arg++;
                }

                if (args[1].ToUpper() == "V")
                {
                    changesToMake[1] = ChangeType.ValidateOnly;
                }

                if (args[1].ToUpper() == "C")
                {
                    changesToMake[1] = ChangeType.CriticalProcessingError;
                    startupErrorExpected = true;
                    arg++;
                }

                for(;arg < args.Length & cont;arg++)
                { // Parse each parameter to see if it is an Config file change or an ini change.  Quit if an invalid parameter
                  // found.
                    if (ArgumentIsConfigFileChange(args[arg]))
                    {
                        changesToMake[arg] = ChangeType.Config;
                    }
                    else if (ArgumentIsINIFileChange(args[arg]))
                    {
                        changesToMake[arg] = ChangeType.INI;
                    }
                    else if (changesToMake[1] == ChangeType.ValidateOnly)
                    {
                        savedFilePath = args[0];
                        IEnumerable<XElement> snid = FITTestFramework.GetAllElementsOfType(savedFilePath, "Notification");
                        foreach (XElement x in snid)
                        {
                            sourceNotificationID = Guid.Parse(x.Attribute("SourceNotificationID").Value.ToString());
                        }
                    }
                    else
                    {
                        cont = false;
                    }
                } // end for
            }

            // Verify correct number of arguments
            if (cont == false)
            {
                Console.WriteLine(
                    "usage: FITTestsTester [<test file path>] [[E|S] | (one or more) CONFIG,<setting path>,<key>,<value> | " +
                    " (one or more) INI,<section>,<setting>,<value>] | (one or more additional test files)" +
                    "\nIf used, the E parameter must come immediately after the first test file path.");
            }
            else
            {
                // 1. Restore baseline settings or save settings and make config and ini file changes.
                frame.PreserveSettings(settings.testMachine);

                for (int change = 1; change < changesToMake.Length; change++)
                { // Make each config and INI file change for this run.
                    switch (changesToMake[change])
                    {
                        case ChangeType.Config:
                            string [] config = args[change].Split(',');
                            if (config.Length > 4)
                            { // Assume that the setting value is supposed to be a comma-separated list and build it
                                for (int i = 4; i < config.Length; i++)
                                    config[3] += ", " + config[i];
                            }
                            fitConfig.SetConfigFileValue(frame.fitConfigFile.FilePath, config[1], config[2], 
                                config[3]);
                            break;
                        case ChangeType.INI:
                            string [] ini = args[change].Split(',');
                            frame.iniFile.IniWriteValue(ini[1], ini[2], ini[3]);
                            break;
                        case ChangeType.ExpectError:
                        case ChangeType.ValidateOnly:
                        case ChangeType.RunAsSetup:
                        case ChangeType.ExpectShutdown:
                        case ChangeType.CriticalProcessingError:
                            break;
                    } // end switch
                } // end for

                if (changesToMake[1] != ChangeType.ValidateOnly)
                {
                    // 2. Clean out log files and all FIT directories and any "NEW" FileTypes
                    logFilePath = frame.SaveLogFile();
                    Console.WriteLine("Last logfile saved was " + logFilePath);

                    frame.CleanFITFolders();

                    frame.CleanOutNEWxFileTypes();

                    // 3. Prepare working copy of specified test file.  Note some key values.  If no test file specified, just
                    //      start the service

                    if (args.Length > 0)
                    {
                        if (changesToMake[1] == ChangeType.ExpectShutdown)
                        {
                            Console.WriteLine("Will not process any files.  Starting the service to generate logs. FIT should shut down.");
                        }
                        else
                        {
                            workingFilePath = frame.PrepareWorkingCopyOfTestFile(args[0], changesToMake[1]);
                            
                            try
                            {
                                notifications = fitConfig.GetConfigFileValues(workingFilePath, "Notifications");
                                notification = fitConfig.GetConfigFileValues(workingFilePath, "Notifications/Notification");
                                testtime = DateTime.Parse(notification["NotificationDate"]);
                                sourceTrackingID = Guid.Parse(notifications["SourceTrackingID"]);
                                sourceNotificationID = Guid.Parse(notification["SourceNotificationID"]);
                            } // end try
                            catch (Exception)
                            {
                                Console.WriteLine(workingFilePath + " is not valid XML.  Continuing, but will not be able to find key values.");
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Will not process any files.  Starting and stopping the service to generate logs.");
                    }

                    // 4. Start FIT.
                    frame.StartFITClientService();

                    // 5. Place working file into the InputFolder.
                    if (workingFilePath != null)
                        frame.CopyToInputFolder(workingFilePath);
                    else
                        frame.response = FITTestFramework.ResponseType.NoResponseExpected;

                    // 6. Wait for the file to appear in the various directories.
                    if (!startupErrorExpected)
                        while (frame.response == FITTestFramework.ResponseType.NotSet) ;
                    else
                        frame.response = FITTestFramework.ResponseType.NoResponseExpected;

                    switch (frame.response)
                    {
                        case FITTestFramework.ResponseType.Response:
                            Console.WriteLine(ErrorExpected ? "Expected error, received response file" : "Response File");
                            break;
                        case FITTestFramework.ResponseType.ErrorResponse:
                            Console.WriteLine(ErrorExpected ? "Error response as expected" :
                                "Expected response, received error response file.");
                            break;
                        case FITTestFramework.ResponseType.InputError:
                            Console.WriteLine(ErrorExpected ? "Input Error as expected" : "Expected response, received Input Error.");
                            break;
                        case FITTestFramework.ResponseType.NoResponseExpected:
                            if (!startupErrorExpected)
                            {
                                Console.WriteLine("Starting and stopping as requested.");
                            }
                            else
                            {
                                int t = 0;
                                Console.WriteLine("Waiting for FIT Client to shut down...");
                                while (frame.fitController.ServiceStatus != System.ServiceProcess.ServiceControllerStatus.Stopped)
                                {
                                    try
                                    {
                                        t++;
                                        if (t % 10000 == 0)
                                            Console.WriteLine("...still waiting t = {0}...", t);
                                        if (t > 1000000000) throw new Exception();
                                    }
                                    catch
                                    {
                                        Console.WriteLine("FIT did not shut down as expected. Test fails.");
                                        break;
                                    }
                                }
                            }
                            break;
                    } // end switch

                    // 7. Shut down FIT.
                    if (changesToMake[1] != ChangeType.ExpectShutdown)
                    { // Verify File Import Client Service is still running.
                        if (frame.fitController.ServiceStatus != System.ServiceProcess.ServiceControllerStatus.Running)
                        {
                            Console.WriteLine("File Import Client Service is in state {0}. Test expected FICS to still be running.  Test fails.", frame.fitController.ServiceStatus.ToString());
                        }
                        else
                        {
                            Console.WriteLine("FICS still running.");
                        }
                    }

                    frame.StopFITClientService();

                    // 8. Save log file and test file from run and let user know the file name.
                    logFilePath = frame.SaveLogFile();
                    Console.WriteLine("Last logfile saved was:\n" + logFilePath);

                    if (workingFilePath != null)
                    {
                        savedFilePath = frame.SaveWorkingCopyOfTestFile(workingFilePath, testtime, sourceTrackingID);
                        Console.WriteLine("Saved working file to:\n" + savedFilePath);
                    }
                } // End if not validate
               
            } // end else for command line format

            if (savedFilePath != null)
            {
                try
                {
                    // Verify message
                    Console.WriteLine("Message {0}created as expected", frame.VerifyMessage(savedFilePath, sourceNotificationID) ? "" :
                        "NOT ");

                    // Verify files
                    Console.WriteLine("Files {0}created as expected", frame.VerifyNotificationFiles(savedFilePath, sourceNotificationID) ?
                        "" : "NOT ");
                }
                catch
                {
                    Console.WriteLine("Cannot check messages and files creation.");
                }
            }

            EndRun();

            frame.RestoreSettings(settings.testMachine);
        }

        private static void EndRun()
        {
            Console.Write("Hit enter to continue.");
            Console.ReadLine();
        }

        private static void StartStopTests()
        {
            frame.StartFITClientService();

            Console.WriteLine("Starting in Debug:");
            frame.StartFITClientService(FITTestFramework.LoggingLevel.Debug);

            Console.WriteLine("Starting in Verbose:");
            frame.StartFITClientService(FITTestFramework.LoggingLevel.Verbose);

            Console.WriteLine("Starting in Essential:");
            frame.StartFITClientService(FITTestFramework.LoggingLevel.Essential);

            frame.StopFITClientService();
        }

        private static int FileCopyTest()
        {
            int copied = 0;
            copied = frame.CopyFilesToInputFolder(new string[] {"FITTests.exe.config"});
            Console.WriteLine("Copied {0} files.", copied);
            return copied;
        }

        private static void ConfigFileTests()
        {
            Console.WriteLine(frame.fitConfigFile.FilePath);

            frame.PreserveSettings("OMAQAOLTA01");
            fitConfig.SetConfigFileValue(frame.fitConfigFile.FilePath, 
                settings.settingsNode, "InputFolder", @"c:\Preserved");
            
            Console.WriteLine("InProcessFolder is " + fitConfig.GetConfigFileValue(frame.fitConfigFile.FilePath,
                settings.settingsNode, "InputFolder"));

            Console.WriteLine("The XClientType is (from GetConfigFileValues): " +
                fitConfig.GetConfigFileValues(frame.fitConfigFile.FilePath,
                    settings.settingsNode)["XClientType"]);

            frame.RestoreSettings("OMAQAOLTA01");
            Console.WriteLine("InProcessFolder is " + fitConfig.GetConfigFileValue(frame.fitConfigFile.FilePath,
                settings.settingsNode, "InputFolder"));

        } // end ConfigFileTests

        private static bool ArgumentIsConfigFileChange(string argument)
            /// Determines if the passed in parameter is a change to the config file.  Config File changes should be passed
            /// as CONFIG,<Setting path>,<key>,<value>
        {
            bool isConfigFileChange = false;

            if (argument.StartsWith("CONFIG,", true, null))
            { // Found CONFIG and first comma. Find the index of the next comma.  If there is not a second comma, the config file 
                // change isnot formatted properly
                argument = argument.Substring("CONFIG,".Length, argument.Length - 7);
                int i = argument.IndexOf(",");
                if (i > 0)
                {
                    argument = argument.Substring(i + 1, argument.Length - i - 1);

                    // Find the index of the third comma.  If there is not a third comma, the config file change is not formatted 
                    // properly.
                    if (argument.IndexOf(",") > 0)
                    {
                        isConfigFileChange = true;
                    } //Found third comma, properly formatted
                } // Found second comma
            } // Starts with CONFIG,

            return isConfigFileChange;
        } // ArgumentIsConfigFileChange

        private static bool ArgumentIsINIFileChange(string argument)
        /// Determines if the passed in parameter is a change to the INI file.  INI File changes should be passed
        /// as INI,<Section>,<Setting>,<value>
        {
            // Could probably rewrite this using a regular expression
            bool isINIFileChange = false;

            if (argument.StartsWith("INI,", true, null))
            { // Found INI and first comma. Find the index of the next comma.  If there is not a second comma, the INI file change is
              // not formatted properly
                argument = argument.Substring("INI,".Length, argument.Length - 4);
                if (argument.IndexOf(",") > 0)
                {
                    argument = argument.Substring(argument.IndexOf(",") + 1, argument.Length - argument.IndexOf(",") - 1);

                    // Find the index of the third comma.  If there is not a third comma, the INI file change is not formatted properly
                    if (argument.IndexOf(",") > 0)
                    {
                        isINIFileChange = true;
                    } //Found third comma, properly formatted
                } // Found second comma
            } // Starts with INI,

            return isINIFileChange;
        } // ArgumentIsConfigFileChange

        
    } // end class FITTestsTester

    public enum ChangeType { Config, INI, AdditionalTestFile, ExpectError, ValidateOnly, RunAsSetup, ExpectShutdown, CriticalProcessingError }

}
