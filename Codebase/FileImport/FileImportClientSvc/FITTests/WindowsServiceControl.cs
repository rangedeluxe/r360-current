﻿using System;
using System.ServiceProcess;
using System.Threading;

namespace WindowsService
{
    /// -------------------------------------------------------------------------------------------------
    /// <summary> Control for Controlling a Windows Service. </summary>
    /// -------------------------------------------------------------------------------------------------
    public class WindowsServiceControl
    {
        /// <summary> The restart timeout in milliseconds </summary>
        private const int RestartTimeout = 10000;

        /// <summary> The windows service </summary>
        private readonly ServiceController service;

        public ServiceControllerStatus ServiceStatus
        {
            get { return service.Status; }
        }
        
        /// -------------------------------------------------------------------------------------------------
        /// <summary> Constructs Control for Windows Service Control on Remote PC. </summary>
        /// <param name="serviceName">  Name of the windows service. </param>
        /// <param name="computerName"> Name of the computer running the service. </param>
        /// -------------------------------------------------------------------------------------------------
        public WindowsServiceControl(string serviceName, string computerName)
        {
            service = new ServiceController(serviceName, computerName);
        }

        /// -------------------------------------------------------------------------------------------------
        /// <summary> Constructs Control for Windows Service Control on Local PC. </summary>
        /// <param name="serviceName"> Name of the windows service. </param>
        /// -------------------------------------------------------------------------------------------------
        public WindowsServiceControl(string serviceName)
        {
            service = new ServiceController(serviceName);
        }

        /// -------------------------------------------------------------------------------------------------
        /// <summary> Starts the windows service. </summary>
        /// <returns> <see langword="true"/> if it succeeds, <see langword="false"/> if it fails. </returns>
        /// -------------------------------------------------------------------------------------------------
        public bool StartService()
        {
            try
            {
                switch (service.Status)
                {
                    case ServiceControllerStatus.ContinuePending:
                    case ServiceControllerStatus.PausePending:
                    case ServiceControllerStatus.StartPending:
                    case ServiceControllerStatus.StopPending:
                        throw new SystemException(service.DisplayName + " is in state " + service.Status + ". Cannot start.");
                        break;
                    
                    case ServiceControllerStatus.Running:
                        RestartService();
                        break;
                    case ServiceControllerStatus.Stopped:
                        service.Start();
                        break;
                    default:
                        throw new SystemException("Could not determine the " + service.DisplayName + "state.");
                } // end switch
                
                service.WaitForStatus(ServiceControllerStatus.Running);
                return service.Status == ServiceControllerStatus.Running;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Starting Service: " + ex.Message);
            }
            return false;
        }

        /// -------------------------------------------------------------------------------------------------
        /// <summary> Stops the windows service. </summary>
        /// <returns> <see langword="true"/> if it succeeds, <see langword="false"/> if it fails. </returns>
        /// -------------------------------------------------------------------------------------------------
        public bool StopService()
        {
            try
            {
                switch (service.Status)
                {
                    case ServiceControllerStatus.ContinuePending:
                    case ServiceControllerStatus.PausePending:
                    case ServiceControllerStatus.StartPending:
                    case ServiceControllerStatus.StopPending:
                        throw new SystemException(service.DisplayName + " is in state " + service.Status + ". Cannot stop.");
                        break;
                    
                    case ServiceControllerStatus.Running:
                        service.Stop();
                        break;
                    case ServiceControllerStatus.Stopped:
                        Console.WriteLine(service.DisplayName + " is already stopped.");
                        break;
                    default:
                        throw new SystemException("Could not determine the " + service.DisplayName + " state.");
                } // end switch
                
                service.WaitForStatus(ServiceControllerStatus.Stopped);
                return service.Status == ServiceControllerStatus.Stopped;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Starting Service: " + ex.Message);
            }
            return false;
        }

        /// -------------------------------------------------------------------------------------------------
        /// <summary> Restarts the windows service. </summary>
        /// <returns> <see langword="true"/> if it succeeds, <see langword="false"/> if it fails. </returns>
        /// -------------------------------------------------------------------------------------------------
        public bool RestartService()
        {
            try
            {
                switch (service.Status)
                {
                    case ServiceControllerStatus.ContinuePending:
                    case ServiceControllerStatus.PausePending:
                    case ServiceControllerStatus.StartPending:
                    case ServiceControllerStatus.StopPending:
                        throw new SystemException(service.DisplayName + " is in state " + service.Status + ". Cannot restart.");
                        break;

                    case ServiceControllerStatus.Running:
                        Console.WriteLine(service.DisplayName + " is already running.  Attempting to restart.");
                        service.Stop();
                        service.WaitForStatus(ServiceControllerStatus.Stopped);
                        service.Start();
                        break;
                    case ServiceControllerStatus.Stopped:
                        Console.WriteLine(service.DisplayName + " stopped. Starting.");
                        service.Start();
                        break;
                    default:
                        throw new SystemException("Could not determine the " + service.DisplayName + "state.");
                } // end switch

                service.WaitForStatus(ServiceControllerStatus.Running);
                return service.Status == ServiceControllerStatus.Running;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Restarting Service: " + ex.Message);
            }
            return false;
        }
    }
}
