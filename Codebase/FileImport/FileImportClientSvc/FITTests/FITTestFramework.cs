﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Configuration;
using System.Xml;
using System.Data;
using System.Xml.Linq;

using WFS.LTA.FileImportClient;
using WFS.LTA.FileImportClientAPI;

using WFS.integraPAY.Online.DAL;
using WFS.integraPAY.Online.Common;
using TestingServicesLibrary;
using Ini;
using FITTests;

namespace WFS.LTA.FileImportClientAPI
{
    public class FITTestFramework
    {
        public WindowsServiceControl fitController;
        public cSiteOptions fitOptions = new cSiteOptions("ipoServices");
        public IniFile iniFile;
        public Configuration fitConfigFile;
        public ConfigFile xmlToEdit = new ConfigFile();
        public Settings settings = new Settings();
        public FileSystemWatcher watcherResponseFolder, watcherErrorResponseFolder, watcherInputError, watcherArchiveFolder;
        public ResponseType response = ResponseType.NotSet;

        private QADAL qadal = new QADAL("R360H");
        private string _workingDirectory;
        private string _usedNotificationFiles;
        private string _inputFolder, _responseFolder, _errorResponseFolder, _inputError, _archiveFolder;

        public string WorkingDirectory
            /// Working directory used to store copies of the Notification Files to modify for testing.
            /// Creates a subdirectory to store the modified Notification Files
        {
            get { return _workingDirectory; }
            private set
            {
                _workingDirectory = @"\\" + settings.testMachine + settings.workDirectory;
                _usedNotificationFiles = _workingDirectory + @"\UsedNotificationFiles";
                Console.WriteLine("Checking working directory exists: " + _workingDirectory);
                // Verify working directory exists.  If not, create it.
                if (!Directory.Exists(_workingDirectory))
                {
                    try
                    {
                        DirectoryInfo di = Directory.CreateDirectory(_workingDirectory + @"\UsedNotificationFiles");
                    } // end try
                    catch (Exception e)
                    {
                        Console.WriteLine("Cannot create the working directory. " + e.Message);
                        _workingDirectory = null;
                    } // end catch
                } // end if
            }
        } // end workingDirectory

        public string InputFolder
        {
            get
            {
                return _inputFolder;
            }
            set
            {
                _inputFolder = xmlToEdit.GetConfigFileValue(this.fitConfigFile.FilePath,
                    settings.settingsNode, "InputFolder") + "\\";
            }
        } // end InputFolder

        public string ResponseFolder 
        {
            get
            {
                return _responseFolder;
            }
            set
            {
                _responseFolder = xmlToEdit.GetConfigFileValue(this.fitConfigFile.FilePath, settings.settingsNode,
                    "ResponseFolder") +"\\";
            }
        } // end ResponseFolder

        public string ArchiveFolder
        {
            get
            {
                return _archiveFolder;
            }
            set
            {
                _archiveFolder = xmlToEdit.GetConfigFileValue(this.fitConfigFile.FilePath,
                    settings.settingsNode, "ArchiveFolder") + "\\";
            }
        } // end ArchiveFolder
        
        public string ErrorResponseFolder
        {
            get
            {
                return _errorResponseFolder;
            }
            set
            {
                _errorResponseFolder = xmlToEdit.GetConfigFileValue(this.fitConfigFile.FilePath, settings.settingsNode,
                    "ErrorResponseFolder") + "\\";
            }
        } // end ErrorResponseFolder

        public string InputErrorFolder
        {
            get
            {
                return _inputError;
            }
            set
            {
                _inputError = xmlToEdit.GetConfigFileValue(this.fitConfigFile.FilePath,
                    settings.settingsNode, "InputErrorFolder") + "\\";
            }
        } // end InputFolder
        
        public string UsedNotificationFiles
        {
            get { return _usedNotificationFiles; }
            
            // For now, set by WorkingDirectory.
            // TODO: Could add a separate config file entry.
        } // end UsedNotificationFiles

        public FITTestFramework(string clientMachine)
        {
            fitController = new WindowsServiceControl("WFS File Import Client Service", clientMachine);
            iniFile = new IniFile(@"\\" + clientMachine + settings.iniFile);
            fitConfigFile = ConfigurationManager.OpenExeConfiguration(@"\\" + clientMachine + settings.configFile);

            WorkingDirectory = "value from config file";
            InputFolder = "value from config file";
            ResponseFolder = "value from config file";
            ErrorResponseFolder = "value from config file";
            InputErrorFolder = "value from config file";
            ArchiveFolder = "value from config file";
            Console.WriteLine("Archive folder is " + ArchiveFolder);

            try
            {
                this.watcherResponseFolder = new FileSystemWatcher();
                this.watcherResponseFolder.Created += new System.IO.FileSystemEventHandler(this.OnCreateResponse);
                SetupWatcher(this.watcherResponseFolder, ResponseFolder);
            }
            catch
            {
                Console.WriteLine("Will not be able to monitor ResponseFolder");
            }

            this.watcherErrorResponseFolder = new FileSystemWatcher();
            this.watcherErrorResponseFolder.Created += new System.IO.FileSystemEventHandler(this.OnCreateErrorResponse);
            SetupWatcher(this.watcherErrorResponseFolder, ErrorResponseFolder);

            this.watcherInputError = new FileSystemWatcher();
            this.watcherInputError.Created += new System.IO.FileSystemEventHandler(this.OnCreateInputError);
            SetupWatcher(this.watcherInputError, InputErrorFolder);

            this.watcherArchiveFolder = new FileSystemWatcher();
            this.watcherArchiveFolder.Created += new System.IO.FileSystemEventHandler(this.OnCreateArchiveFolder);
            SetupWatcher(this.watcherArchiveFolder, ArchiveFolder);
        }

        public void OnCreateResponse(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("New file in ResponseFolder: " + e.FullPath);
            response = ResponseType.Response;
            Console.WriteLine("Getting ready to check DataImportQueue.");
            if (qadal.DataImportQueueRecordsPending() > 0)
            { // Still waiting for our response?
                Console.WriteLine("Queue records pending...");
                response = ResponseType.NotSet;
            }
            Console.WriteLine("Checked DataImportQueue.  response is " + response);
        } // end OnCreateResponse

        public void OnCreateArchiveFolder(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("New file in ArchiveFolder: " + e.FullPath);
//            response = ResponseType.Response;
            /* I don't think I need this because we won't get an Archive without a Response. */
/*            Console.WriteLine("Getting ready to check DataImportQueue.");
            if (qadal.DataImportQueueRecordsPending() > 0)
            { // Still waiting for our response?
                response = ResponseType.NotSet;
            }
            /* */
        } // end OnCreateArchiveFolder
        
        public void OnCreateErrorResponse(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("New file in ErrorResponseFolder: " + e.FullPath);
            response = ResponseType.ErrorResponse;
        } // end OnCreateErrorResponse

        public void OnCreateInputError(object source, FileSystemEventArgs e)
        {
            Console.WriteLine("New file in InputErrorFolder: " + e.FullPath);
            response = ResponseType.InputError;
        } // end OnCreateInputError

        private void SetupWatcher(FileSystemWatcher w, string pathToWatch)
        {
            w.Path = pathToWatch;
            w.EnableRaisingEvents = true;
        }

        public int CopyFilesToInputFolder(string[] files)
        // Moves the files specified in the array to the InputFolder.
        // Returns the number of files copied.
        {
            int copied = 0;
            string inputFolder = ((cFileDropClientProcessOptions)ProcessOptionsList[0]).InputFolder;

            foreach (string file in files)
            {
                try
                {
                    File.Copy(file, inputFolder + "\\" + file, true);
                    copied++;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return copied;
        } // end MoveFilesToInputFolder
        
        private cProcessOptionsList _ProcessOptionsList = null;
        public cProcessOptionsList ProcessOptionsList
        {
            get
            {
                cProcessOptionsList _ProcessOptionsList = null;
                try
                {
                    cFileImportSettingsConfigSection r = new cFileImportSettingsConfigSection();

                    if (_ProcessOptionsList == null)
                    {
                        _ProcessOptionsList = new cProcessOptionsList();
                    }
                }

                catch (ConfigurationErrorsException ex)
                {
                    Console.WriteLine("Error reading " + ex.Filename);
                    Console.WriteLine("Error is " + ex.Message);
                    
                }

                return _ProcessOptionsList;
            }
        }
        
        public bool StartFITClientService()
            // Attempts to start the File Import Client Service.
            // Returns true if FIT starts and false otherwise.
        {
            try
            {
                if (fitController.StartService())
                {
                    Console.WriteLine("FIT Client is now running");
                    return true;
                } // end if
                else
                {
                    Console.WriteLine("Couldn't start FIT Client");
                    return false;
                } // end else
            } // ent try to start the FIT Client Service
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            } 
        } // end StartFITClientService

        public enum LoggingLevel { Essential, Verbose, Debug }

        public bool StartFITClientService(LoggingLevel loggingLevel)
        // Attempts to start the File Import Client Service with the LoggingLevel set to the value specified.
        // Returns true if FIT starts and false otherwise.
        {
            int logLevel;

            switch(loggingLevel)
            {
                case LoggingLevel.Debug:
                    logLevel = 2;
                    break;
                case LoggingLevel.Verbose:
                    logLevel = 1;
                    break;
                case LoggingLevel.Essential:
                    logLevel = 0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("LoggingLevel", loggingLevel, "Invalid LoggingLevel value");
            } // end switch(loggingLevel)
 
            try
            {
                iniFile.IniWriteValue("ipoServices", "LoggingDepth", logLevel.ToString());
                return StartFITClientService();
            } // ent try to start the FIT Client Service
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        } // end StartFITClientService

        public bool StopFITClientService()
        // Attempts to stop the File Import Client Service.
        // Returns true if FIT stops and false otherwise.
        {
            // Stop the FIT Service
            try
            {
                if (fitController.StopService())
                {
                    Console.WriteLine("FIT Client is now stopped.");
                    return true;
                }
                else
                {
                    Console.WriteLine("Could not stop FIT Client.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        } // end StopFITClientService()

        /*  Moved to TestingServicesLibrary 3/26/13
                public static void SetConfigFileValue(string configFileName, string settingPath, string key, string value)
                    // Sets a key in the specified .XML formatted configuration file.
                    // Parameters:
                    //  configFileName: The full path to the configuration file
                    //  settingPath:    The XML path to the setting in the config file
                    //  key:            The name of the configuration key
                    //  value:          The new value for the configuration key
                    //  Assumes that all keys are sent as strings.
                {
                    XmlDocument fitConfig = new XmlDocument();
                    XmlNode node;

                    fitConfig.Load(configFileName);

                    node = fitConfig.SelectSingleNode(settingPath);
                    node.Attributes[key].Value = value;

                    fitConfig.Save(configFileName);
                }

                public static string GetConfigFileValue(string configFileName, string settingPath, string key)
                    // Retrieves a single key value from the specified .XML configuration file.
                    // Parameters:
                    //  configFileName: The full path to the configuration file
                    //  settingPath:    The XML path to the setting in the config file
                    //  key:            The name of the configuration key
                {
                    XmlDocument config = new XmlDocument();
                    XmlNode node;

                    config.Load(configFileName);
                    node = config.SelectSingleNode(settingPath);
                    return node.Attributes[key].Value;
                }
        
                public static Dictionary<string, string> GetConfigFileValues(string configFileName, string settingPath)
                    // Retrieves a all of the keys and values from the specified .XML configuration file.
                    // Parameters:
                    //  configFileName: The full path to the configuration file
                    //  settingPath:    The XML path to the setting in the config file
                {
                    Dictionary<string, string> section = new Dictionary<string, string>();
                    XmlDocument config = new XmlDocument();
                    XmlNode node;

                    config.Load(configFileName);
                    node = config.SelectSingleNode(settingPath);
                    foreach (XmlAttribute attribute in node.Attributes)
                    {
                        section.Add(attribute.Name, attribute.Value);
                    }

                    return section;
                }
        */

        public void PreserveSettings(string machineName)
            /// If backups of the .config and .ini files already exist, restore them. Otherwise,
            /// makes backup copies of the original settings files before beginning a test run.
            /// NOTE: Be sure the current .config and .ini files are "clean" versions you want to preserve before
            ///     using PreserveSettings.
        {
            string iniFileName = @"\\" + machineName + settings.iniFile;
            BackupIfNecessary(iniFileName);

            string configFileName = @"\\" + machineName + settings.configFile + ".config";
            BackupIfNecessary(configFileName);
        } // end PreserveSettings

        private void BackupIfNecessary(string filename)
        {
            if (File.Exists(filename + ".bak"))
            {
                File.Copy(filename + ".bak", filename,true);
            }
            else
            {
                File.Copy(filename, filename + ".bak", true);
            }
        } // end BackupIfNecessary

        public void RestoreSettings(string machineName)
            /// Restores the backup copies of the settings files after completing a test run.
        {
            // Copy the ini file to a backup version we will restore at the end of each run.
            File.Copy(@"\\" + machineName + settings.iniFile + ".bak", @"\\" + machineName + settings.iniFile, true);

            // Copy the config file to a backup version we will restore at the end of each run.
            File.Copy(@"\\" + machineName + settings.configFile + ".config.bak",
                @"\\" + machineName + settings.configFile + ".config", true);
        } // end RestoreSettings

        public void SetFITNotificationAttribute(string notificationFile, string key, string value)
            /// Sets the key to the value specified in the <Notification> element of a FIT Notification file
        {
            try
            {
                xmlToEdit.SetConfigFileValue(notificationFile, "Notifications/Notification", key, value);
            }
            catch (Exception e)
            {
                Console.WriteLine("Couldn't set Notification Attribute: " + e.Message);
            }
        } // end SetFITNotificationAttribute

        public void SetFITNotificationsAttribute(string notificationFile, string key, string value)
        /// Sets the key to the value specified in the <Notification> element of a FIT Notification file
        {
            try
            {
                xmlToEdit.SetConfigFileValue(notificationFile, "Notifications", key, value);
            }
            catch(Exception e)
            {
                Console.WriteLine("Couldn't set Notifications Attribute: " + e.Message);
            }
        } // end SetFITNotificationAttribute

        public string PrepareWorkingCopyOfTestFile(string testFile, ChangeType change)
            /// Creates a working copy of the designated test file in the work directory configured in the app.config
            /// Sets the Notification/NotificationDate to the current date and time.
            /// Sets the Notifications/SourceTrackingID to a new guid
            /// Sets the Notification/NotificationTrackingID to a new guid
            /// Sets any File/FileIdentifier to a new guid
            /// Returns the path of the working copy
        {
            string filename = Path.GetFileName(testFile);
            string workingFilePath = WorkingDirectory + @"\" + filename;
            
            DateTime now = DateTime.Now;
            Guid sourceTrackingID = Guid.NewGuid();
            Guid notificationTrackingID = Guid.NewGuid();
            Guid fileIdentifier = Guid.NewGuid();
            Guid sourceNotificationID = Guid.NewGuid();

            // Create a working copy of the file. If the file already exists, overwrite it so we are working with
            // the correct copy.
            File.Copy(testFile, workingFilePath, true);
            
            // If the test file has invalid XML, just use the existing file
            if (change != ChangeType.ExpectError)
            {
                try
                {
                    // Set the Notifications/SourceTrackingID
                    xmlToEdit.SetConfigFileValue(workingFilePath, "Notifications", "SourceTrackingID", sourceTrackingID.ToString());

                    // Set the Notifications/Notification/NotificationDate
                    // , SourceNotificationTrackingID, and NotificationTrackingID
                    xmlToEdit.SetConfigFileValue(workingFilePath, "Notifications/Notification", "NotificationDate", now.ToString());
                    xmlToEdit.SetConfigFileValue(workingFilePath, "Notifications/Notification", "NotificationTrackingID"
                        , notificationTrackingID.ToString());
                    xmlToEdit.SetConfigFileValue(workingFilePath, "Notifications/Notification", "SourceNotificationID"
                        , sourceNotificationID.ToString());
                } // end try
                catch (Exception e)
                {
                    Console.WriteLine("Couldn't modify " + workingFilePath + ".  Continuing with the original test file. "
                        + e.Message);
                }

                try
                {
                    // Set the File/FileIdentifier
                    xmlToEdit.SetConfigFileValue(workingFilePath, "Notifications/Notification/Files/File", "FileIdentifier",
                        fileIdentifier.ToString());
                } // end try
                catch (Exception e)
                {
                    Console.WriteLine("Couldn't modify " + workingFilePath + " FileIdentifier.  Continuing with the modified test file. "
                        + e.Message);
                }
            } // end if

            return workingFilePath;
        }

        public string SaveWorkingCopyOfTestFile(string testFile, DateTime testtime, Guid sourceTrackingID)
            /// Moves the working copy of the designated test file to the UsedNotificationFiles directory under
            /// the working directory. Renames the file by appending the test time and SourceTrackingID used during
            /// the test
            /// Parameters:
            ///    testfile: Path to the test file to save
            ///    testtime: The NotificationDate from the test file
            ///    SourceTrackingID:    SourceTrackingID used during the test
            /// Returns the path of the last saved file.
        {
            string savedFileName = UsedNotificationFiles + "\\" + Path.GetFileNameWithoutExtension(testFile) + "_" + 
                testtime.ToString("yyyyMMdd_HHmmss") + "_" + sourceTrackingID.ToString() +
                Path.GetExtension(testFile);

            try
            {
                File.Move(testFile, savedFileName);
            }
            catch
            {
                File.Copy(testFile, savedFileName, true);
                File.Delete(testFile);
            }

            return savedFileName;
        } // end SaveWorkingCopyOfTestFile

        public string SaveLogFile()
        {
            string savedLogFileName="";
            string baseFileName;
            string[] logFilePattern, logs;
            string path, savePath, drive;

            baseFileName = iniFile.IniReadValue("ipoServices", "LogFile");
            path = Path.GetDirectoryName(baseFileName);
            drive = Path.GetPathRoot(baseFileName);

            logFilePattern = baseFileName.Split('\\');
            baseFileName = logFilePattern[logFilePattern.Length - 1];

            baseFileName = baseFileName.Split('{')[0];
            path = path.Replace(drive, @"\\" + settings.testMachine + @"\");
            
            savePath = path + @"\PreviousLogs\";
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            } // end if

            logs = Directory.GetFiles(path, baseFileName + "*");
            foreach (string log in logs)
            {
                savedLogFileName = savePath + Path.GetFileNameWithoutExtension(log) 
                    + DateTime.Now.ToString("_yyyyMMdd_HHmmss") + Path.GetExtension(log); 
                File.Move(log, savedLogFileName);
                
            } // end foreach

            return savedLogFileName;
        } // end SaveLogFile()

        public void CleanFITFolders()
            /// Moves all files out of the following directories to a subdirectory called Previous for the FIT Client.
            /// - InputFolder
            /// - InProcessFolder
            /// - PendingResponseFolder
            /// - ResponseFolder
            /// - ArchiveFolder
            /// - InputErrorFolder
            /// - ErrorResponseFolder
            /// Returns the path of the InputFolder.
        {
            // inputFolder
            PathCleaner(InputFolder);

            // InProcessFolder
            string inProcessFolder = xmlToEdit.GetConfigFileValue(this.fitConfigFile.FilePath, settings.settingsNode,
                "InProcessFolder");
            PathCleaner(inProcessFolder);

            /// - PendingResponseFolder
            string pendingResponseFolder = xmlToEdit.GetConfigFileValue(this.fitConfigFile.FilePath, settings.settingsNode,
                "PendingResponseFolder");
            PathCleaner(pendingResponseFolder);

            /// - ResponseFolder
            PathCleaner(ResponseFolder);
            
            /// - ArchiveFolder
            string archiveFolder = xmlToEdit.GetConfigFileValue(this.fitConfigFile.FilePath, settings.settingsNode,
                "ArchiveFolder");
            PathCleaner(archiveFolder);
            
            /// - InputErrorFolder
            PathCleaner(InputErrorFolder);
            
            /// - ErrorResponseFolder
            PathCleaner(ErrorResponseFolder);

        } // end CleanFITFolders

        private void PathCleaner(string directory)
            /// Moves all files in the specified directory into a subdirectory called Previous.
            /// Creates the Previous subdirectory if necessary.
        {
            string[] files;
            string targetDirectory = directory + "\\Previous\\";
            string targetFile;

            try
            {
                files = Directory.GetFiles(directory);

                if (!Directory.Exists(targetDirectory))
                {
                    Directory.CreateDirectory(targetDirectory);
                }

                foreach (string file in files)
                {
                    targetFile = Path.GetDirectoryName(file) + "\\Previous\\" + Path.GetFileName(file);
                    while (File.Exists(targetFile))
                    {
                        int renamer = 0;
                        targetFile += renamer++;
                    }

                    File.Move(file, targetFile);
                } // end foreach in InputFolder
            } // end try
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void CopyToInputFolder(string testfile)
            /// Copies the testfile to the InputFolder
        {
            try
            {
                string destination = InputFolder + Path.GetFileName(testfile);
                File.Copy(testfile, destination);
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not copy to InputFolder.\n" + e.Message);
            }
        } // end MoveToInputFolder

        public bool VerifyMessage(string savedFilePath, Guid sourceNotificationID)
            /// Verifies the total length of the MessageText in the target database matches the sent message length from
            /// all Message elements.
        {
            bool verified = true;

            // Data to compare
            IEnumerable<XElement> messageParts = GetAllElementsOfType(savedFilePath, "MessagePart");
            DataTable dt = qadal.RetrieveFactNotificationBySourceNoticationID(sourceNotificationID); 
            
            int notificationMessagePart = 1;
            foreach (XElement messagePart in messageParts)
            {
                try
                {
                    // for debugging
                    //string msg = (string)messagePart.Value;
                    //string ntf = (string)dt.Rows[notificationMessagePart - 1]["MessageText"];
                    //int d = string.Compare(msg, ntf);

                    verified &= (0 == string.Compare((string)messagePart.Value,
                        (string)dt.Rows[notificationMessagePart - 1]["MessageText"]));
                    notificationMessagePart++;
                }
                catch (Exception e)
                {
                    verified = false;
                    Console.WriteLine(e.Message);
                }
                if (dt.Rows.Count > messageParts.Count<XElement>())
                {
                    verified = false;
                    Console.WriteLine("There were only {0} MessageParts in the notification file, but there were {1} Notifications table entries created.",
                        messageParts.Count<XElement>(), dt.Rows.Count);
                }
                if (!verified)
                    break;  // Once we've failed, there's no need to continue.
            }

            return verified;
        }

        public bool VerifyNotificationFiles(string savedFilePath, Guid sourceNotificationID)
        /// Verifies that FIT imported all Files specified in the Notification file.  If the Notification file did not include
        /// any files, returns true and sends a message to the Console that there were no files to import.
        {
            bool verified = true;

            // Data to compare
            IEnumerable<XElement> attachments = GetAllElementsOfType(savedFilePath, "File");
            DataTable dt = qadal.RetrieveFactNotificationFilesBySourceNoticationID(sourceNotificationID);

            int file = 0;

            verified = attachments.Count<XElement>() == dt.Rows.Count;

            if (attachments.Count<XElement>() == 0)
            {
                Console.WriteLine("No files in the Notification file.");
            }
            foreach (XElement attachment in attachments)
            {
                if (!verified)
                    break; // Once we've failed, no need to continue.

                try
                {
                    string source = (string) attachment.Attribute("UserFileName");
                    string notif = (string) dt.Rows[file++]["UserFileName"];

                    verified &= (string.Compare(source, notif) == 0);
                }
                catch (Exception e)
                {
                    verified = false;
                    Console.WriteLine(e.Message);
                }
            }

            return verified;
        }

        public static IEnumerable<XElement> GetAllElementsOfType(string notificationFile, string elementPath)
        {
            XElement xml = XElement.Load(notificationFile);
            IEnumerable<XElement> elements = xml.Descendants(elementPath);
            return elements;
        }

        public void CleanOutNEWxFileTypes()
        {
            qadal.DeleteNEWxDimNotificationFileTypesEntries();
        }

        public enum ResponseType { NotSet, Response, InputError, ErrorResponse, NoResponseExpected }
    }
}
