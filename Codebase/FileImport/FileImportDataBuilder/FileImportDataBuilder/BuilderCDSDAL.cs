﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WFS.LTA.FileImportClientLib;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;

namespace FileImportDataBuilder {
    public class BuilderCDSDAL:_DALBase {
        private const string ContactID = "ba23f692-9120-4e0e-9c57-7115c66faaed";
        private const string EventName = "End of Day";
        private const string DeliveryMethodName = "Internet";
        private const string EventRuleID = "6f3fbe72-2312-4b4b-92d6-3708b90a8aaa";
        private const string AlertID = "CB646E28-CE7F-4705-926B-6282F6AC8DCE";

        public BuilderCDSDAL(string vSiteKey) : base(vSiteKey, ConnectionType.integraPay) {
        }

        private void ExecuteQuery(string query, string errorMessage) {
            if (Database.executeNonQuery(query) < 1) {
                throw new Exception(errorMessage);
            }
        }

        public void CreateNotification(cNotification notificationData) {
            var notificationID = Guid.NewGuid();
            var notificationFields = new Dictionary<string, string>() {
                {"NotificationID", notificationID.ToString()},
                {"AlertID", AlertID},
                {"EventLogID", "NULL"},
                {"NotificationDate", notificationData.NotificationDate.ToString("yyyy-MM-dd")},
                {"NotificationDeliveryDate", notificationData.NotificationDate.ToString("yyyy-MM-dd")},
                {"NotificationDeliveryStatus", "4"},
                {"MaxAttempts", "4"},
                {"AttemptsSoFar", "0"},
                {"DeliveryErrorCode", "NULL"},
                {"DeliveryErrorMessage", ""},
                {"MessageText", string.Join("", notificationData.MessageParts)},
                {"FaxID", "-1"},
                {"RedundantNotification", "0"},
                {"WorkviewUserReviewedStatus", "0"}
            };
            var notificationQuery = DynamicSQLUtils.BuildInsertQuery("dbo.Notifications", notificationFields);
            CreateContact(notificationData);
            CreateEventRule(notificationData);
            CreateAlert();
            ExecuteQuery(notificationQuery, "Unable to add new test Notification");
            foreach (var notificationFile in notificationData.NotificationFiles) {
                CreateAttachment(notificationID.ToString(), notificationFile);   
            }
        }

        public void CreateAttachment(string notificationID, cNotificationFile notificationFile) {
            var attachmentBasePath = Properties.Settings.Default.FileAttachmentsFolder.ToString() + @"\";
            var notificationFileFields = new Dictionary<string, string>() {
                {"FileID", "@fileID"},
                {"FileDescription", notificationFile.UserFileName},
                {"FileType", notificationFile.FileType},
                {"FilePathName", notificationFile.ClientFilePath.Replace(attachmentBasePath, "")}
            };
            var attachmentQuery = string.Format(
                "DECLARE @fileID AS UNIQUEIDENTIFIER\r\n" +
                "\r\n" +
                "SET @fileID=NEWID()\r\n" +
                "\r\n" +
                "{0}\r\n" +
                "\r\n" +
                "INSERT INTO dbo.NotificationFiles(\r\n" +
                "		NotificationID,\r\n" +
                "		FileID)\r\n" +
                "	VALUES (\r\n" +
                "		'{1}',\r\n" +
                "		@fileID)", DynamicSQLUtils.BuildInsertQuery("dbo.RenderedFiles", notificationFileFields), notificationID);
            ExecuteQuery(attachmentQuery, string.Format("Error attaching {0}", notificationFile.UserFileName));
        }

        public void CreateAlert() {
            var alertFields = new Dictionary<string, string>() {
                {"AlertID", AlertID},
                {"ContactID", ContactID},
                {"DeliveryMethodID", "@DeliveryMethodID"},
                {"EventRuleID", EventRuleID},
                {"Printer", "NULL"}
            };
            var alertsQuery = string.Format(
                "DECLARE @DeliveryMethodID AS UNIQUEIDENTIFIER\r\n\r\n" +
                "SELECT @DeliveryMethodID = DeliveryMethodID\r\n" +
                "  FROM dbo.DeliveryMethods\r\n" +
                "  WHERE DeliveryMethodName = '{0}'\r\n\r\n" +
                "{1}\r\n", DeliveryMethodName,
                DynamicSQLUtils.BuildUpsertQuery("dbo.Alerts", new[] { "AlertID" }, alertFields));
            ExecuteQuery(alertsQuery, "Failed to establish testing Alert");
        }

        public void CreateContact(cNotification notificationData) {
            var contactFields = new Dictionary<string, string>() {
                {"ContactID", ContactID},
                {"BankID", notificationData.BankID.ToString()},
                {"CustomerID", notificationData.ClientGroupID.ToString()},
                {"LockboxID", notificationData.ClientID.ToString()},
                {"ContactName", "Charlie"},
                {"ContactPin", "0"},
                {"ContactPassword", "NULL"},
                {"IsActive", "1"},
                {"IsPrimary", "0"},
                {"SiteName", "NULL"},
                {"Address", ""},
                {"City", ""},
                {"State", ""},
                {"Zip", ""},
                {"Voice", ""},
                {"Fax", ""},
                {"Modem", ""},
                {"Email", ""},
                {"TextPager", "NULL"},
                {"ExternalID", "NULL"},
                {"IsOnline", "0"}
            };
            var contactsQuery = DynamicSQLUtils.BuildUpsertQuery("dbo.Contacts", new[] {"ContactID"}, contactFields);
            ExecuteQuery(contactsQuery, "Failed to establish testing Contact");
        }

        public void CreateEventRule(cNotification notificationData) {
            var eventRulesFields = new Dictionary<string, string>() {
                {"EventRuleID", EventRuleID},
                {"EventID", "@EventID"},
                {"BankID", notificationData.BankID.ToString()},
                {"CustomerID", notificationData.ClientGroupID.ToString()},
                {"LockboxID", notificationData.ClientID.ToString()},
                {"RuleIsActive", "True"},
                {"EventOperator", "="},
                {"EventArgumentCode", "Done"},
                {"EventRuleDescription", "Just for testing"},
                {"EventRuleValue", ""},
                {"EventAction", "<Message>This is only a test."},
                {"InternetDeliverableID", "NULL"},
                {"OnlineViewable", "True"},
                {"Expression", ""},
                {"OnlineModifiable", "True"},
                {"Deadline", "NULL"},
                {"BatchesCompleted", "False"},
                {"TimesToDeliver", "NULL"},
                {"TimesDelivered", "0"}
            };
            var eventRuleQuery = string.Format(
                "DECLARE @EventID AS INT\r\n\r\n" +
                "SELECT @EventID = EventID\r\n" +
                "	FROM dbo.Events\r\n" +
                "	WHERE EventName = '{0}'\r\n\r\n" +
                "{1}", EventName,
                DynamicSQLUtils.BuildUpsertQuery("dbo.EventRules", new[] {"EventRuleID"}, eventRulesFields));
            ExecuteQuery(eventRuleQuery, "Failed to establish testing EventRule");
            }
        }
    }