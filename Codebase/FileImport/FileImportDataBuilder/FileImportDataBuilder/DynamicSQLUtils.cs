﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileImportDataBuilder {
    public static class DynamicSQLUtils {
        public static string BuildUpsertQuery(string tableName, string[] keyFieldNames, Dictionary<string, string> fieldValues, bool updateIfExists = true) {
            var whereClause = "Where " + string.Join("\r\n\t\t\t\tAnd ", fieldValues
                .Where(pair => keyFieldNames
                        .Any(item => item.Equals(pair.Key, StringComparison.CurrentCultureIgnoreCase)))
                .Select(pair => string.Format("{0} = {1}", pair.Key, pair.Value.ValueToSQLConst())));
            var nonkeyFieldValues = fieldValues
                .Where(pair => !keyFieldNames
                    .Any(item => item.Equals(pair.Key, StringComparison.CurrentCultureIgnoreCase)));
            var setClause = "Set " + string.Join(",\r\n\t\t\t", nonkeyFieldValues
                .Select(pair => string.Format("{0} = {1}", pair.Key, pair.Value.ValueToSQLConst())));
            var fieldList = string.Join(",\r\n\t\t", fieldValues
                .Select(pair => pair.Key));
            var valueList = string.Join(",\r\n\t\t", fieldValues
                .Select(pair => pair.Value.ValueToSQLConst()));
            var insertClause = BuildInsertQuery(tableName, fieldValues, "   ");
            string queryPattern;

            if (updateIfExists) {
                queryPattern = "IF EXISTS(\r\n" +
                               "		SELECT * FROM {0} \r\n" +
                               "			{1}\r\n" +
                               "	)\r\n" +
                               "   UPDATE {0} {2} \r\n" +
                               "		{1}\r\n" +
                               "ELSE\r\n" +
                               "{3};";
            }
            else {
                queryPattern = "IF NOT EXISTS(\r\n" +
                               "		SELECT * FROM {0} \r\n" +
                               "			{1}\r\n" +
                               "	)\r\n" +
                               "{3};";
            }
            return string.Format(queryPattern, tableName, whereClause, setClause, insertClause);
        }

        public static string BuildInsertQuery(string tableName, Dictionary<string, string> fieldValues, string indent = "") {
            var lineStart = string.Format("{0}\t\t", indent);
            var fieldList = string.Join(",\r\n" + lineStart, fieldValues
                .Select(pair => pair.Key));
            var valueList = string.Join(",\r\n" + lineStart, fieldValues
                .Select(pair => pair.Value.ValueToSQLConst()));
            string queryPattern;

            queryPattern = "{0}INSERT INTO {2}({1}{3}) {0}\tVALUES({1}{4});";
            return string.Format(queryPattern, indent, lineStart, tableName, fieldList, valueList);
        }

        private static string ValueToSQLConst(this string value) {
            return value == "NULL" ? value : 
                                value.StartsWith("@") ? value : value.Surround("'");
        }

        public static string Surround(this string value, string bounds) {
            return string.Format("{1}{0}{1}", value, bounds);
        }
    }
}
