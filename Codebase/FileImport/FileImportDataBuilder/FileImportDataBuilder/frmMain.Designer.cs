﻿namespace FileImportDataBuilder
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.cmdBuildIt = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pnlOutputFileData = new System.Windows.Forms.Panel();
            this.rdoPromptForFileName = new System.Windows.Forms.RadioButton();
            this.rdoUseGuidForFileName = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.txtNotificationXmlFolderPath = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtFileAttachmentsFolder = new System.Windows.Forms.TextBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.txtMessageText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboMessageText = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.checkedListBoxFiles = new System.Windows.Forms.CheckedListBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNotificationSourceKey = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtClientID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtClientGroupID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBankID = new System.Windows.Forms.TextBox();
            this.tabExtract = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkUseCurrentDateTime = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtClientProcessCode = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tabExtractContent = new System.Windows.Forms.TabPage();
            this.btnCreateExtract = new System.Windows.Forms.Button();
            this.txtExtractMessage = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtExtractFilePath = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtExtractWorkgroupName = new System.Windows.Forms.TextBox();
            this.txtExtractWorkgroupId = new System.Windows.Forms.TextBox();
            this.txtExtractBankId = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.chkUseCDSTables = new System.Windows.Forms.CheckBox();
            this.radioButtonxml = new System.Windows.Forms.RadioButton();
            this.radioButtonjson = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.pnlOutputFileData.SuspendLayout();
            this.tabExtract.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabExtractContent.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdBuildIt
            // 
            this.cmdBuildIt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdBuildIt.Location = new System.Drawing.Point(477, 451);
            this.cmdBuildIt.Name = "cmdBuildIt";
            this.cmdBuildIt.Size = new System.Drawing.Size(75, 23);
            this.cmdBuildIt.TabIndex = 0;
            this.cmdBuildIt.Text = "Build It!";
            this.cmdBuildIt.UseVisualStyleBackColor = true;
            this.cmdBuildIt.Click += new System.EventHandler(this.cmdBuildIt_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.pnlOutputFileData);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtFileAttachmentsFolder);
            this.groupBox1.Location = new System.Drawing.Point(4, 347);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(548, 99);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // pnlOutputFileData
            // 
            this.pnlOutputFileData.Controls.Add(this.rdoPromptForFileName);
            this.pnlOutputFileData.Controls.Add(this.rdoUseGuidForFileName);
            this.pnlOutputFileData.Controls.Add(this.label1);
            this.pnlOutputFileData.Controls.Add(this.btnSelectFolder);
            this.pnlOutputFileData.Controls.Add(this.txtNotificationXmlFolderPath);
            this.pnlOutputFileData.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlOutputFileData.Location = new System.Drawing.Point(3, 16);
            this.pnlOutputFileData.Name = "pnlOutputFileData";
            this.pnlOutputFileData.Size = new System.Drawing.Size(542, 42);
            this.pnlOutputFileData.TabIndex = 8;
            // 
            // rdoPromptForFileName
            // 
            this.rdoPromptForFileName.AutoSize = true;
            this.rdoPromptForFileName.Location = new System.Drawing.Point(379, 26);
            this.rdoPromptForFileName.Name = "rdoPromptForFileName";
            this.rdoPromptForFileName.Size = new System.Drawing.Size(126, 17);
            this.rdoPromptForFileName.TabIndex = 6;
            this.rdoPromptForFileName.TabStop = true;
            this.rdoPromptForFileName.Text = "Prompt For File Name";
            this.rdoPromptForFileName.UseVisualStyleBackColor = true;
            // 
            // rdoUseGuidForFileName
            // 
            this.rdoUseGuidForFileName.AutoSize = true;
            this.rdoUseGuidForFileName.Location = new System.Drawing.Point(235, 26);
            this.rdoUseGuidForFileName.Name = "rdoUseGuidForFileName";
            this.rdoUseGuidForFileName.Size = new System.Drawing.Size(131, 17);
            this.rdoUseGuidForFileName.TabIndex = 5;
            this.rdoUseGuidForFileName.TabStop = true;
            this.rdoUseGuidForFileName.Text = "Use Guid for FileName";
            this.rdoUseGuidForFileName.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Notification Folder Path:";
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectFolder.Location = new System.Drawing.Point(511, -1);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(27, 23);
            this.btnSelectFolder.TabIndex = 7;
            this.btnSelectFolder.Text = "...";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            // 
            // txtNotificationXmlFolderPath
            // 
            this.txtNotificationXmlFolderPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNotificationXmlFolderPath.Location = new System.Drawing.Point(150, 0);
            this.txtNotificationXmlFolderPath.Name = "txtNotificationXmlFolderPath";
            this.txtNotificationXmlFolderPath.Size = new System.Drawing.Size(355, 20);
            this.txtNotificationXmlFolderPath.TabIndex = 8;
            this.txtNotificationXmlFolderPath.Leave += new System.EventHandler(this.txtNotificationXmlFolderPath_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 13);
            this.label10.TabIndex = 7;
            this.label10.Text = "File Attatchment Folder Path:";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(514, 67);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // txtFileAttachmentsFolder
            // 
            this.txtFileAttachmentsFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileAttachmentsFolder.Location = new System.Drawing.Point(153, 68);
            this.txtFileAttachmentsFolder.Name = "txtFileAttachmentsFolder";
            this.txtFileAttachmentsFolder.Size = new System.Drawing.Size(355, 20);
            this.txtFileAttachmentsFolder.TabIndex = 6;
            this.txtFileAttachmentsFolder.Leave += new System.EventHandler(this.txtFileAttachmentsFolder_Leave);
            // 
            // txtMessageText
            // 
            this.txtMessageText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMessageText.Location = new System.Drawing.Point(134, 193);
            this.txtMessageText.Multiline = true;
            this.txtMessageText.Name = "txtMessageText";
            this.txtMessageText.Size = new System.Drawing.Size(391, 105);
            this.txtMessageText.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 166);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Message Text:";
            // 
            // cboMessageText
            // 
            this.cboMessageText.FormattingEnabled = true;
            this.cboMessageText.Items.AddRange(new object[] {
            "Custom",
            "Alice in Wonderland",
            "Beowulf",
            "Dracula",
            "Fairy Tales by the Brothers Grimm",
            "Frankenstein",
            "Huckleberry Finn",
            "The King James Bible",
            "War of the Worlds"});
            this.cboMessageText.Location = new System.Drawing.Point(134, 163);
            this.cboMessageText.Name = "cboMessageText";
            this.cboMessageText.Size = new System.Drawing.Size(121, 21);
            this.cboMessageText.TabIndex = 6;
            this.cboMessageText.SelectedIndexChanged += new System.EventHandler(this.cboMessageText_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(331, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Files:";
            // 
            // checkedListBoxFiles
            // 
            this.checkedListBoxFiles.FormattingEnabled = true;
            this.checkedListBoxFiles.Items.AddRange(new object[] {
            "Alice in Wonderland",
            "Beowulf",
            "Dracula",
            "Fairy Tales by the Brothers Grimm",
            "Frankenstein",
            "Huckleberry Finn",
            "The King James Bible",
            "War of the Worlds"});
            this.checkedListBoxFiles.Location = new System.Drawing.Point(368, 15);
            this.checkedListBoxFiles.Name = "checkedListBoxFiles";
            this.checkedListBoxFiles.Size = new System.Drawing.Size(157, 124);
            this.checkedListBoxFiles.TabIndex = 16;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "MM/dd/yyyy hh:mm:ss tt";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(134, 90);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Notification Source Key:";
            // 
            // txtNotificationSourceKey
            // 
            this.txtNotificationSourceKey.Location = new System.Drawing.Point(134, 137);
            this.txtNotificationSourceKey.Name = "txtNotificationSourceKey";
            this.txtNotificationSourceKey.Size = new System.Drawing.Size(67, 20);
            this.txtNotificationSourceKey.TabIndex = 13;
            this.txtNotificationSourceKey.Text = "100";
            this.txtNotificationSourceKey.Leave += new System.EventHandler(this.txtNotificationSourceKey_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Notification Date:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Client ID:";
            // 
            // txtClientID
            // 
            this.txtClientID.Location = new System.Drawing.Point(134, 67);
            this.txtClientID.Name = "txtClientID";
            this.txtClientID.Size = new System.Drawing.Size(67, 20);
            this.txtClientID.TabIndex = 9;
            this.txtClientID.Text = "4533";
            this.txtClientID.Leave += new System.EventHandler(this.txtClientID_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Client Group ID:";
            // 
            // txtClientGroupID
            // 
            this.txtClientGroupID.Location = new System.Drawing.Point(134, 41);
            this.txtClientGroupID.Name = "txtClientGroupID";
            this.txtClientGroupID.Size = new System.Drawing.Size(67, 20);
            this.txtClientGroupID.TabIndex = 7;
            this.txtClientGroupID.Text = "0";
            this.txtClientGroupID.Leave += new System.EventHandler(this.txtClientGroupID_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Bank ID:";
            // 
            // txtBankID
            // 
            this.txtBankID.Location = new System.Drawing.Point(134, 15);
            this.txtBankID.Name = "txtBankID";
            this.txtBankID.Size = new System.Drawing.Size(67, 20);
            this.txtBankID.TabIndex = 5;
            this.txtBankID.Text = "1";
            this.txtBankID.Leave += new System.EventHandler(this.txtBankID_Leave);
            // 
            // tabExtract
            // 
            this.tabExtract.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabExtract.Controls.Add(this.tabPage1);
            this.tabExtract.Controls.Add(this.tabPage2);
            this.tabExtract.Controls.Add(this.tabExtractContent);
            this.tabExtract.HotTrack = true;
            this.tabExtract.ItemSize = new System.Drawing.Size(200, 18);
            this.tabExtract.Location = new System.Drawing.Point(4, 0);
            this.tabExtract.Name = "tabExtract";
            this.tabExtract.SelectedIndex = 0;
            this.tabExtract.Size = new System.Drawing.Size(552, 348);
            this.tabExtract.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(544, 322);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Notification Data";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkUseCurrentDateTime);
            this.panel1.Controls.Add(this.txtMessageText);
            this.panel1.Controls.Add(this.txtBankID);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cboMessageText);
            this.panel1.Controls.Add(this.txtClientGroupID);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.checkedListBoxFiles);
            this.panel1.Controls.Add(this.txtClientID);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtNotificationSourceKey);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(538, 316);
            this.panel1.TabIndex = 20;
            // 
            // chkUseCurrentDateTime
            // 
            this.chkUseCurrentDateTime.AutoSize = true;
            this.chkUseCurrentDateTime.Location = new System.Drawing.Point(135, 114);
            this.chkUseCurrentDateTime.Name = "chkUseCurrentDateTime";
            this.chkUseCurrentDateTime.Size = new System.Drawing.Size(136, 17);
            this.chkUseCurrentDateTime.TabIndex = 21;
            this.chkUseCurrentDateTime.Text = "Use Current Date/Time";
            this.chkUseCurrentDateTime.UseVisualStyleBackColor = true;
            this.chkUseCurrentDateTime.CheckedChanged += new System.EventHandler(this.chkUseCurrentDateTime_CheckedChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(544, 322);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Client Configuration";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtClientProcessCode);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(538, 316);
            this.panel2.TabIndex = 0;
            // 
            // txtClientProcessCode
            // 
            this.txtClientProcessCode.Location = new System.Drawing.Point(134, 29);
            this.txtClientProcessCode.Name = "txtClientProcessCode";
            this.txtClientProcessCode.Size = new System.Drawing.Size(189, 20);
            this.txtClientProcessCode.TabIndex = 7;
            this.txtClientProcessCode.Text = "SimpleFileDrop_00";
            this.txtClientProcessCode.Leave += new System.EventHandler(this.txtClientProcessCode_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Client Process Code:";
            // 
            // tabExtractContent
            // 
            this.tabExtractContent.Controls.Add(this.btnCreateExtract);
            this.tabExtractContent.Controls.Add(this.txtExtractMessage);
            this.tabExtractContent.Controls.Add(this.label16);
            this.tabExtractContent.Controls.Add(this.txtExtractFilePath);
            this.tabExtractContent.Controls.Add(this.label15);
            this.tabExtractContent.Controls.Add(this.txtExtractWorkgroupName);
            this.tabExtractContent.Controls.Add(this.txtExtractWorkgroupId);
            this.tabExtractContent.Controls.Add(this.txtExtractBankId);
            this.tabExtractContent.Controls.Add(this.label14);
            this.tabExtractContent.Controls.Add(this.label13);
            this.tabExtractContent.Controls.Add(this.label12);
            this.tabExtractContent.Location = new System.Drawing.Point(4, 22);
            this.tabExtractContent.Name = "tabExtractContent";
            this.tabExtractContent.Padding = new System.Windows.Forms.Padding(3);
            this.tabExtractContent.Size = new System.Drawing.Size(544, 322);
            this.tabExtractContent.TabIndex = 2;
            this.tabExtractContent.Text = "Extract Data";
            this.tabExtractContent.UseVisualStyleBackColor = true;
            // 
            // btnCreateExtract
            // 
            this.btnCreateExtract.Location = new System.Drawing.Point(353, 247);
            this.btnCreateExtract.Name = "btnCreateExtract";
            this.btnCreateExtract.Size = new System.Drawing.Size(181, 23);
            this.btnCreateExtract.TabIndex = 10;
            this.btnCreateExtract.Text = "Create Extract FIT Notification";
            this.btnCreateExtract.UseVisualStyleBackColor = true;
            this.btnCreateExtract.Click += new System.EventHandler(this.btnCreateExtract_Click);
            // 
            // txtExtractMessage
            // 
            this.txtExtractMessage.Location = new System.Drawing.Point(115, 132);
            this.txtExtractMessage.Multiline = true;
            this.txtExtractMessage.Name = "txtExtractMessage";
            this.txtExtractMessage.Size = new System.Drawing.Size(419, 109);
            this.txtExtractMessage.TabIndex = 9;
            this.txtExtractMessage.Text = "An extract was completed successfully!";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 135);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Message:";
            // 
            // txtExtractFilePath
            // 
            this.txtExtractFilePath.Location = new System.Drawing.Point(115, 94);
            this.txtExtractFilePath.Name = "txtExtractFilePath";
            this.txtExtractFilePath.Size = new System.Drawing.Size(419, 20);
            this.txtExtractFilePath.TabIndex = 7;
            this.txtExtractFilePath.Text = "D:\\Extracts\\SampleExtract.txt";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 97);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(87, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Extract File Path:";
            // 
            // txtExtractWorkgroupName
            // 
            this.txtExtractWorkgroupName.Location = new System.Drawing.Point(115, 50);
            this.txtExtractWorkgroupName.Name = "txtExtractWorkgroupName";
            this.txtExtractWorkgroupName.Size = new System.Drawing.Size(419, 20);
            this.txtExtractWorkgroupName.TabIndex = 5;
            this.txtExtractWorkgroupName.Text = "Workgroup Name";
            // 
            // txtExtractWorkgroupId
            // 
            this.txtExtractWorkgroupId.Location = new System.Drawing.Point(115, 27);
            this.txtExtractWorkgroupId.Name = "txtExtractWorkgroupId";
            this.txtExtractWorkgroupId.Size = new System.Drawing.Size(419, 20);
            this.txtExtractWorkgroupId.TabIndex = 4;
            this.txtExtractWorkgroupId.Text = "4533";
            // 
            // txtExtractBankId
            // 
            this.txtExtractBankId.Location = new System.Drawing.Point(115, 4);
            this.txtExtractBankId.Name = "txtExtractBankId";
            this.txtExtractBankId.Size = new System.Drawing.Size(419, 20);
            this.txtExtractBankId.TabIndex = 3;
            this.txtExtractBankId.Text = "1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(94, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Workgroup Name:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 13);
            this.label13.TabIndex = 1;
            this.label13.Text = "Workgroup ID:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Bank ID:";
            // 
            // chkUseCDSTables
            // 
            this.chkUseCDSTables.AutoSize = true;
            this.chkUseCDSTables.Location = new System.Drawing.Point(370, 454);
            this.chkUseCDSTables.Name = "chkUseCDSTables";
            this.chkUseCDSTables.Size = new System.Drawing.Size(101, 17);
            this.chkUseCDSTables.TabIndex = 7;
            this.chkUseCDSTables.Text = "Use CDS tables";
            this.chkUseCDSTables.UseVisualStyleBackColor = true;
            this.chkUseCDSTables.CheckedChanged += new System.EventHandler(this.chkUseCDSTables_CheckedChanged);
            // 
            // radioButtonxml
            // 
            this.radioButtonxml.AutoSize = true;
            this.radioButtonxml.Checked = true;
            this.radioButtonxml.Location = new System.Drawing.Point(63, 6);
            this.radioButtonxml.Name = "radioButtonxml";
            this.radioButtonxml.Size = new System.Drawing.Size(47, 17);
            this.radioButtonxml.TabIndex = 8;
            this.radioButtonxml.TabStop = true;
            this.radioButtonxml.Text = "XML";
            this.radioButtonxml.UseVisualStyleBackColor = true;
            // 
            // radioButtonjson
            // 
            this.radioButtonjson.AutoSize = true;
            this.radioButtonjson.Location = new System.Drawing.Point(116, 6);
            this.radioButtonjson.Name = "radioButtonjson";
            this.radioButtonjson.Size = new System.Drawing.Size(53, 17);
            this.radioButtonjson.TabIndex = 9;
            this.radioButtonjson.Text = "JSON";
            this.radioButtonjson.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "File Format";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.radioButtonjson);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.radioButtonxml);
            this.panel3.Location = new System.Drawing.Point(7, 451);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(235, 27);
            this.panel3.TabIndex = 11;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 484);
            this.Controls.Add(this.chkUseCDSTables);
            this.Controls.Add(this.tabExtract);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmdBuildIt);
            this.Controls.Add(this.panel3);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(563, 391);
            this.Name = "frmMain";
            this.Text = "File Import Data Builder";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.pnlOutputFileData.ResumeLayout(false);
            this.pnlOutputFileData.PerformLayout();
            this.tabExtract.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabExtractContent.ResumeLayout(false);
            this.tabExtractContent.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmdBuildIt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBankID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNotificationSourceKey;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtClientID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtClientGroupID;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckedListBox checkedListBoxFiles;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cboMessageText;
        private System.Windows.Forms.TextBox txtMessageText;
        private System.Windows.Forms.TabControl tabExtract;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtClientProcessCode;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtFileAttachmentsFolder;
        private System.Windows.Forms.CheckBox chkUseCurrentDateTime;
        private System.Windows.Forms.CheckBox chkUseCDSTables;
        private System.Windows.Forms.Panel pnlOutputFileData;
        private System.Windows.Forms.RadioButton rdoPromptForFileName;
        private System.Windows.Forms.RadioButton rdoUseGuidForFileName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.TextBox txtNotificationXmlFolderPath;
        private System.Windows.Forms.RadioButton radioButtonxml;
        private System.Windows.Forms.RadioButton radioButtonjson;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabPage tabExtractContent;
        private System.Windows.Forms.TextBox txtExtractWorkgroupName;
        private System.Windows.Forms.TextBox txtExtractWorkgroupId;
        private System.Windows.Forms.TextBox txtExtractBankId;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtExtractFilePath;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtExtractMessage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnCreateExtract;
    }
}

