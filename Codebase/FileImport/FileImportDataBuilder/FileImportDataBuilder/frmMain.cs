﻿using fitExtractXClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using WFS.LTA.FileImportClientLib;

/******************************************************************************
** Wausau
** Copyright © 1997-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     09/29/2012
*
* Purpose:  
*
* Modification History
* CR 56120 JMC 09/29/2012
*   -Initial release version.
******************************************************************************/
namespace FileImportDataBuilder {

    public partial class frmMain : Form {

        private System.Collections.Generic.Dictionary<string, string> _AvailableFiles;

        public frmMain() {
            InitializeComponent();

            this.txtNotificationXmlFolderPath.Text = Properties.Settings.Default.OutputFolderPath;
            this.txtClientProcessCode.Text = Properties.Settings.Default.ClientProcessCode;
            this.txtFileAttachmentsFolder.Text = Properties.Settings.Default.FileAttachmentsFolder;

            this.txtBankID.Text = Properties.Settings.Default.BankID.ToString();
            this.txtClientGroupID.Text = Properties.Settings.Default.ClientGroupID.ToString();
            this.txtClientID.Text = Properties.Settings.Default.ClientID.ToString();
            this.txtNotificationSourceKey.Text = Properties.Settings.Default.NotificationSourceKey.ToString();

            this.dateTimePicker1.Value = DateTime.Now;
            this.cboMessageText.SelectedIndex = 0;
            this.chkUseCurrentDateTime.Checked = Properties.Settings.Default.UseCurrentDateTime;
            this.dateTimePicker1.Enabled = !Properties.Settings.Default.UseCurrentDateTime;

            this.rdoPromptForFileName.Checked = Properties.Settings.Default.PromptForFileName;
            this.rdoUseGuidForFileName.Checked = !Properties.Settings.Default.PromptForFileName;

            _AvailableFiles = new Dictionary<string,string>();
            _AvailableFiles.Add("Alice in Wonderland", "Alice");
            _AvailableFiles.Add("Beowulf", "Beowulf");
            _AvailableFiles.Add("Dracula", "Dracula");
            _AvailableFiles.Add("Fairy Tales by the Brothers Grimm", "FairyTalesByGrimm");
            _AvailableFiles.Add("Frankenstein", "Frankenstein");
            _AvailableFiles.Add("Huckleberry Finn", "HuckFinn");
            _AvailableFiles.Add("The King James Bible", "KJBible");
            _AvailableFiles.Add("War of the Worlds", "WarOfTheWorlds");  

        }

        private void btnSelectFolder_Click(object sender, EventArgs e) {
            DialogResult objDialogResult = this.folderBrowserDialog1.ShowDialog();
            if(objDialogResult == System.Windows.Forms.DialogResult.OK) {
                this.txtNotificationXmlFolderPath.Text = this.folderBrowserDialog1.SelectedPath;
                Properties.Settings.Default.OutputFolderPath = this.folderBrowserDialog1.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }

        private void cmdBuildIt_Click(object sender, EventArgs e) {

            try {

                cNotification objNotification = new cNotification();
                string strFileName;
                //bool bolCancel;

                objNotification.BankID = Properties.Settings.Default.BankID;
                objNotification.ClientGroupID = Properties.Settings.Default.ClientGroupID;
                objNotification.ClientID = Properties.Settings.Default.ClientID;

                if(Properties.Settings.Default.UseCurrentDateTime) {
                    objNotification.NotificationDate = DateTime.Now;
                } else {
                    objNotification.NotificationDate = this.dateTimePicker1.Value;
                }

                objNotification.NotificationSourceKey = Properties.Settings.Default.NotificationSourceKey;

                objNotification.NotificationTrackingID = Guid.NewGuid();
                objNotification.SourceNotificationID = Guid.Empty;

                if(this.cboMessageText.SelectedItem.ToString() == "Custom") {
                    objNotification.SetMessage(this.txtMessageText.Text);
                } else {
                    AddMessageText(objNotification, _AvailableFiles[this.cboMessageText.SelectedItem.ToString()]);
                }

                foreach(object objFileName in this.checkedListBoxFiles.CheckedItems) {
                    AddFile(objNotification, _AvailableFiles[objFileName.ToString()]);
                }

                List<cNotification> arNotifications = new List<cNotification>();
                arNotifications.Add(objNotification);

                System.Xml.XmlDocument docXml;
                Guid gidSourceTrackingID = Guid.NewGuid();
                if (chkUseCDSTables.Checked) {
                    var cdsDAL = new BuilderCDSDAL("ipoServices");
                    cdsDAL.CreateNotification(arNotifications.First());
                    cdsDAL.Dispose();
                    MessageBox.Show("The Notification has been loaded into the CDS Tables");
                }
                else {
                    WFS.LTA.FileImportClientLib.NotificationLib.NotificationsToXml(gidSourceTrackingID,
                        Properties.Settings.Default.ClientProcessCode, arNotifications, out docXml);

                    if (Properties.Settings.Default.PromptForFileName) {
                        strFileName = string.Empty;
                        //bolCancel = false;

                        while (string.IsNullOrEmpty(strFileName)) {
                            strFileName = Microsoft.VisualBasic.Interaction.InputBox("Enter File Name:", "File Name",
                                gidSourceTrackingID.ToString());
                            if (string.IsNullOrEmpty(strFileName)) {
                                DialogResult result = MessageBox.Show("Invalid File Name", "Invalid File Name",
                                    MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                                if (result == System.Windows.Forms.DialogResult.Cancel) {
                                    return;
                                }
                            }
                            else {
                                if(radioButtonxml.Checked)
                                {
                                    if(!strFileName.EndsWith(".xml"))strFileName += ".xml";
                                    docXml.Save(Path.Combine(Properties.Settings.Default.OutputFolderPath, strFileName));
                                }
                                else if (radioButtonjson.Checked)
                                {
                                    createJsonFile(objNotification, strFileName);
                                }
                                
                            }
                        }
                    }
                    else {
                        if (radioButtonxml.Checked)
                        {
                            docXml.Save(Path.Combine(Properties.Settings.Default.OutputFolderPath,
                                gidSourceTrackingID.ToString() + ".xml"));
                        }
                        else if (radioButtonjson.Checked)
                        {
                            createJsonFile(objNotification, gidSourceTrackingID + ".json");
                        }
                    }
                    MessageBox.Show("File(s) were created.");
                }
            } catch(Exception ex) {
                MessageBox.Show("An unexpected exception occurred.\n\n" + ex.Message);
            }
        }


        private void createJsonFile(cNotification notification, string fileName)
        {
            if (!fileName.EndsWith(".json")) fileName += ".json";
            var wgs = new[] { new { Bank = notification.BankID, Workgroup = notification.ClientID } };
            var file = notification.NotificationFiles.Count > 0
                ? notification.NotificationFiles[0]
                : new cNotificationFile()
                {
                    FileSize = "0",
                    FileExtension = ".txt",
                    UserFileName = "noFileSelected",
                    FileIdentifier = Guid.NewGuid(),
                    ClientFilePath = Properties.Settings.Default.FileAttachmentsFolder,
                    FileType = "txt"
                };

            var xfile = new { Workgroups = wgs, File = Path.Combine(file.ClientFilePath), Message = "Extract File" };
            var contents = xfile.ToJSON();
            File.WriteAllText(Path.Combine(Properties.Settings.Default.OutputFolderPath, fileName), contents);
        }
        /// <summary>
        /// Copies the contents of input to output. Doesn't close either stream.
        /// </summary>
        public static void CopyStream(Stream input, Stream output) {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ( (len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }    
        }

        private void cboMessageText_SelectedIndexChanged(object sender, EventArgs e) {
            this.txtMessageText.Visible = this.cboMessageText.Text == "Custom";
        }

        private static void AddMessageText(cNotification notification, string fileName) {
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("FileImportDataBuilder.Files." + fileName + ".txt")) {
                using (StreamReader reader = new StreamReader(stream)) {
                    notification.SetMessage(reader.ReadToEnd());
                }
            }
        }

        private static void AddFile(cNotification notification, string fileName) {
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("FileImportDataBuilder.Files." + fileName + ".txt")) {
                
                string strFileName = Path.Combine(Properties.Settings.Default.FileAttachmentsFolder, Guid.NewGuid() + ".txt");
                
                using (Stream file = File.OpenWrite(strFileName)) {
                    CopyStream(stream, file);
                }

                cNotificationFile objNotificationFile = new cNotificationFile();
                
                objNotificationFile.ClientFilePath = strFileName;
                objNotificationFile.FileExtension = ".txt";
                objNotificationFile.FileIdentifier = Guid.NewGuid();
                objNotificationFile.FileType = "text";
                objNotificationFile.UserFileName = fileName + ".txt";
                notification.AddNotificationFile(objNotificationFile);
            }
        }

        private void txtNotificationXmlFolderPath_Leave(object sender, EventArgs e) {
            if(this.txtNotificationXmlFolderPath.Text != Properties.Settings.Default.OutputFolderPath) {
                Properties.Settings.Default.OutputFolderPath = this.txtNotificationXmlFolderPath.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void txtClientProcessCode_Leave(object sender, EventArgs e) {
            if(this.txtClientProcessCode.Text != Properties.Settings.Default.ClientProcessCode) {
                Properties.Settings.Default.ClientProcessCode = this.txtClientProcessCode.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void txtFileAttachmentsFolder_Leave(object sender, EventArgs e) {
            if(this.txtFileAttachmentsFolder.Text != Properties.Settings.Default.FileAttachmentsFolder) {
                Properties.Settings.Default.FileAttachmentsFolder = this.txtFileAttachmentsFolder.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void txtBankID_Leave(object sender, EventArgs e) {

            int intTemp;

            if(int.TryParse(this.txtBankID.Text, out intTemp)) {
                if(intTemp != Properties.Settings.Default.BankID) {
                    Properties.Settings.Default.BankID = intTemp;
                    Properties.Settings.Default.Save();
                }
            } else {
                MessageBox.Show("Invalid BankID");

                if(this.txtBankID.Text.Length > 0) {
                    this.txtBankID.SelectionStart = 0;
                    this.txtBankID.SelectionLength = this.txtBankID.Text.Length;
                }

                this.txtBankID.Focus();
            }
        }

        private void txtClientGroupID_Leave(object sender, EventArgs e) {

            int intTemp;

            if(int.TryParse(this.txtClientGroupID.Text, out intTemp)) {
                if(intTemp != Properties.Settings.Default.ClientGroupID) {
                    Properties.Settings.Default.ClientGroupID = intTemp;
                    Properties.Settings.Default.Save();
                }
            } else {
                MessageBox.Show("Invalid ClientGroupID");

                if(this.txtClientGroupID.Text.Length > 0) {
                    this.txtClientGroupID.SelectionStart = 0;
                    this.txtClientGroupID.SelectionLength = this.txtClientGroupID.Text.Length;
                }

                this.txtClientGroupID.Focus();
            }
        }

        private void txtClientID_Leave(object sender, EventArgs e) {

            int intTemp;

            if(int.TryParse(this.txtClientID.Text, out intTemp)) {
                if(intTemp != Properties.Settings.Default.ClientID) {
                    Properties.Settings.Default.ClientID = intTemp;
                    Properties.Settings.Default.Save();
                }
            } else {
                MessageBox.Show("Invalid ClientID");

                if(this.txtClientID.Text.Length > 0) {
                    this.txtClientID.SelectionStart = 0;
                    this.txtClientID.SelectionLength = this.txtClientID.Text.Length;
                }

                this.txtClientID.Focus();
            }
        }

        private void txtNotificationSourceKey_Leave(object sender, EventArgs e) {

            int intTemp;

            if(int.TryParse(this.txtNotificationSourceKey.Text, out intTemp)) {
                if(intTemp != Properties.Settings.Default.NotificationSourceKey) {
                    Properties.Settings.Default.NotificationSourceKey = intTemp;
                    Properties.Settings.Default.Save();
                }
            } else {
                MessageBox.Show("Invalid NotificationSourceKey");

                if(this.txtNotificationSourceKey.Text.Length > 0) {
                    this.txtNotificationSourceKey.SelectionStart = 0;
                    this.txtNotificationSourceKey.SelectionLength = this.txtNotificationSourceKey.Text.Length;
                }

                this.txtNotificationSourceKey.Focus();
            }
        }

        private void chkUseCurrentDateTime_CheckedChanged(object sender, EventArgs e) {
            Properties.Settings.Default.UseCurrentDateTime = this.chkUseCurrentDateTime.Checked;
            Properties.Settings.Default.Save();
            this.dateTimePicker1.Enabled = !this.chkUseCurrentDateTime.Checked;
        }

        private void rdoPromptForFileName_CheckedChanged(object sender, EventArgs e) {
            Properties.Settings.Default.PromptForFileName = ((RadioButton)sender).Checked;
            Properties.Settings.Default.Save();
        }

        private void rdoUseGuidForFileName_CheckedChanged(object sender, EventArgs e) {
            Properties.Settings.Default.PromptForFileName = !((RadioButton)sender).Checked;
            Properties.Settings.Default.Save();
        }

        private void chkUseCDSTables_CheckedChanged(object sender, EventArgs e) {
            pnlOutputFileData.Enabled = !chkUseCDSTables.Checked;
        }

        private void btnCreateExtract_Click(object sender, EventArgs e)
        {
            var bankid = 0;
            var workgroupid = 0;
            try
            {
                bankid = int.Parse(txtExtractBankId.Text);
                workgroupid = int.Parse(txtExtractWorkgroupId.Text);
            }
            catch { }

            var model = new ExtractModel()
            {
                File = txtExtractFilePath.Text,
                Message = txtExtractMessage.Text,
                MessageWorkgroups = Guid.NewGuid(),
                Workgroups = new List<WorkgroupModel>()
                {
                    new WorkgroupModel()
                    {
                        Bank = bankid,
                        Workgroup = workgroupid,
                        WorkgroupName = txtExtractWorkgroupName.Text
                    }
                }
            };

            var json = model.ToJSON();
            if (!Directory.Exists(Properties.Settings.Default.OutputFolderPath))
                Directory.CreateDirectory(Properties.Settings.Default.OutputFolderPath);
            File.WriteAllText(Path.Combine(Properties.Settings.Default.OutputFolderPath, $"{Guid.NewGuid().ToString()}.json"), json);
        }
    }
}
