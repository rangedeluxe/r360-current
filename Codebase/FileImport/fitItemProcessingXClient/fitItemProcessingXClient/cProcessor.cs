﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

using WFS.itemProcessing.DAL;
using WFS.LTA.Common;
using WFS.LTA.FileImportClientLib;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53791 JMC 07/17/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportServices.ItemProcessingXClient 
{

    internal enum NotificationQueueStatusType {
        Pending = 10,
        InProcess = 20,
        Submitted = 99,
        Failed = 130,
        Success = 150
    }

    public class cProcessor : IPollingProcessorXClient {

        private cXClientSiteOptions _SiteOptions = null;

        public event FITClientOutputLogEventHandler OutputLog;
        public event FITClientOutputTraceEventHandler OutputTrace;
        public event FITClientOutputLogAndTraceEventHandler OutputLogAndTrace;
        public event FITClientOutputLogAndTraceExceptionEventHandler OutputLogAndTraceException;

        private string _IniSectionKey = string.Empty;
        private ConfigurationContext configurationContext;

        public cProcessor() {
        }

        public string IniSectionKey {
            set {
                _IniSectionKey = value;
            }
            get {
                return(_IniSectionKey);
            }
        }

        private cXClientSiteOptions XClientSiteOptions {
            get {
                
                if(_SiteOptions == null) {
                    _SiteOptions = new cXClientSiteOptions(_IniSectionKey);
                }

                return(_SiteOptions);
            }
        }

        public bool GetNotificationXml(string clientProcessCode, out XmlDocument notificationsXml) {

            DataTable dt = null;
            cNotification objNotification;
            cNotificationFile objNotificationFile;
            List<cNotification> arNotifications;

            XmlDocument docNotifications;

            Guid gidCurrentNotificationID;
            Guid gidCurrentFileID;

            bool bolRetVal;

            bool bolLockApplied = false;

            DateTime dteEndDate = DateTime.Now;

            Guid gidNotificationIDThatShouldBeSkipped = Guid.Empty;

            Guid gidSourceTrackingID;
            
            try {

                gidSourceTrackingID = Guid.NewGuid();

                using(cNotificationsDAL dal = new cNotificationsDAL(_IniSectionKey)) {

                    dal.OutputLog += this.OnOutputLog;
                    dal.OutputTrace += this.OnOutputTrace;
                    dal.OutputLogAndTrace += this.OnOutputLogAndTrace;
                    dal.OutputLogAndTraceException += this.OnOutputLogAndTraceException;

                    if(dal.GetNotifications(
                            XClientSiteOptions.RetentionDays, 
                            XClientSiteOptions.MaxNotificationsPerUpload, 
                            out dt)) {

                        arNotifications = new List<cNotification>();

                        gidCurrentNotificationID = Guid.Empty;
                        gidCurrentFileID = Guid.Empty;
                        objNotification = null;

                        foreach(DataRow dr in dt.Rows) {

                            if((Guid)dr["NotificationID"] != gidNotificationIDThatShouldBeSkipped) {

                                if((Guid)dr["NotificationID"] == gidCurrentNotificationID) {

                                    if(!dr.IsNull("FileID") && (Guid)dr["FileID"] != gidCurrentFileID) {

                                        objNotificationFile = new cNotificationFile();

                                        objNotificationFile.FileIdentifier = (Guid)dr["FileID"];
                                        objNotificationFile.FileType = dr["FileType"].ToString();
                                        objNotificationFile.UserFileName = dr["FileDescription"].ToString(); //Path.GetFileNameWithoutExtension(dr["FilePathName"].ToString());
                                        objNotificationFile.FileExtension = Path.GetExtension(Path.Combine(XClientSiteOptions.CENDSPath, dr["FilePathName"].ToString()));
                                        objNotificationFile.ClientFilePath = Path.Combine(XClientSiteOptions.CENDSPath, dr["FilePathName"].ToString());

                                        objNotification.AddNotificationFile(objNotificationFile);
                                    }

                                } else {

                                    if(objNotification != null) {
                                        arNotifications.Add(objNotification);
                                    }

                                    LockNotificationData((Guid)dr["NotificationID"], gidSourceTrackingID, out bolLockApplied);

                                    if(bolLockApplied) {
                                        
                                        gidNotificationIDThatShouldBeSkipped = Guid.Empty;

                                        objNotification = new cNotification();
                                        objNotification.SourceNotificationID = (Guid)dr["NotificationID"];
                                        objNotification.BankID = (int)dr["BankID"];
                                        objNotification.ClientGroupID = (int)dr["CustomerID"];
                                        objNotification.ClientID = (int)dr["LockboxID"];
                                        objNotification.NotificationDate = (DateTime)dr["NotificationDate"];
                                        objNotification.SetMessage(dr["MessageText"].ToString());

                                        if(!dr.IsNull("FileID")) {

                                            objNotificationFile = new cNotificationFile();

                                            objNotificationFile.FileIdentifier = (Guid)dr["FileID"];
                                            objNotificationFile.FileType = dr["FileType"].ToString();
                                            objNotificationFile.UserFileName = dr["FileDescription"].ToString(); //Path.GetFileNameWithoutExtension(dr["FilePathName"].ToString());
                                            objNotificationFile.FileExtension = Path.GetExtension(Path.Combine(XClientSiteOptions.CENDSPath, dr["FilePathName"].ToString()));
                                            objNotificationFile.ClientFilePath = Path.Combine(XClientSiteOptions.CENDSPath, dr["FilePathName"].ToString());

                                            objNotification.AddNotificationFile(objNotificationFile);
                                        }

                                    } else {
                                        gidNotificationIDThatShouldBeSkipped = (Guid)dr["NotificationID"];
                                    }
                                }
                            }

                            if((Guid)dr["NotificationID"] != gidNotificationIDThatShouldBeSkipped) {

                                gidCurrentNotificationID = (Guid)dr["NotificationID"];

                                if(dr.IsNull("FileID")) {
                                    gidCurrentFileID = Guid.Empty;
                                } else {
                                    gidCurrentFileID = (Guid)dr["FileID"];
                                }
                            } else {
                                gidCurrentFileID = Guid.Empty;
                                gidCurrentNotificationID = Guid.Empty;
                            }
                        }

                        if(objNotification != null) {
                            arNotifications.Add(objNotification);
                        }

                        if(NotificationLib.NotificationsToXml(gidSourceTrackingID, clientProcessCode, arNotifications, out docNotifications)) {
                            bolRetVal = true;
                        } else {
                            docNotifications = null;
                            bolRetVal = false;
                        }
            
                    } else {
                        docNotifications = null;
                        bolRetVal = false;
                    }
                }
            } catch(Exception ex) {
                OnOutputLogAndTraceException("An unexpected exception occurred in GetNotificationXml()", this.GetType().Name, ex);
                docNotifications = null;
                bolRetVal = false;
            } finally {
                if(dt != null) {
                    try {
                        dt.Dispose();
                    } catch(Exception) {
                        // Do Nothing on purpose.
                    }
                }
            }

            notificationsXml = docNotifications;
            return(bolRetVal);
        }

        private bool LockNotificationData(Guid sourceNotificationID, Guid sourceTrackingID, out bool lockApplied) {

            bool bolRetVal;
            int intRowsAffected;

            try {

                using(cNotificationsDAL dal = new cNotificationsDAL(_IniSectionKey)) {

                    dal.OutputLog += this.OnOutputLog;
                    dal.OutputTrace += this.OnOutputTrace;
                    dal.OutputLogAndTrace += this.OnOutputLogAndTrace;
                    dal.OutputLogAndTraceException += this.OnOutputLogAndTraceException;

                    if(!dal.UpsertNotificationQueueStatus(
                        sourceNotificationID,
                        sourceTrackingID,
                        (int)NotificationQueueStatusType.Pending, 
                        (int)NotificationQueueStatusType.InProcess, 
                        false,
                        out intRowsAffected)) {

                        OnOutputLogAndTrace("Unable to execute cNotificationsDAL.UpsertNotificationQueueStatus()", this.GetType().Name, FITClientMessageType.Error, FITClientMessageImportance.Essential);
                        intRowsAffected = 0;
                    }
                }

                lockApplied = intRowsAffected > 0;
                bolRetVal = true;

            } catch(Exception ex) {
                OnOutputLogAndTraceException("An unexpected exception occurred in LockNotificationData()", this.GetType().Name, ex);
                lockApplied = false;
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        public bool UpdateNotificationStatusSubmitted(Guid sourceTrackingID, cNotification notification) {
            bool bolRetVal;
            int intRowsAffected;

            try {

                using(cNotificationsDAL dal = new cNotificationsDAL(_IniSectionKey)) {

                    dal.OutputLog += this.OnOutputLog;
                    dal.OutputTrace += this.OnOutputTrace;
                    dal.OutputLogAndTrace += this.OnOutputLogAndTrace;
                    dal.OutputLogAndTraceException += this.OnOutputLogAndTraceException;

                    if(dal.UpsertNotificationQueueStatus(
                        notification.SourceNotificationID,
                        sourceTrackingID,
                        (int)NotificationQueueStatusType.InProcess, 
                        (int)NotificationQueueStatusType.Submitted, 
                        false,
                        out intRowsAffected)) {

                        bolRetVal = true;

                    } else {

                        OnOutputLogAndTrace(
                            "Unable to notify Notification submission to the itemProcessing database for Source NotificationID: " + notification.SourceNotificationID.ToString(), 
                            this.GetType().Name, 
                            FITClientMessageType.Error, 
                            FITClientMessageImportance.Essential);

                        bolRetVal = false;
                    }
                }

            } catch(Exception ex) {
                OnOutputLogAndTraceException("An unexpected exception occurred in NotificationDataSubmitted()", this.GetType().Name, ex);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        public bool UpdateNotificationStatusSuccess(Guid sourceTrackingID) {
            return(UpdateNotificationQueueStatusComplete(
                sourceTrackingID,
                NotificationQueueStatusType.Success));
        }

        public bool UpdateNotificationStatusFailed(Guid sourceTrackingID) {
            return(UpdateNotificationQueueStatusComplete(
                sourceTrackingID,
                NotificationQueueStatusType.Failed));
        }

        private bool UpdateNotificationQueueStatusComplete(
            Guid sourceTrackingID, 
            NotificationQueueStatusType newNotificationQueueStatus) {

            bool bolRetVal;
            int intRowsAffected;

            try {

                using(cNotificationsDAL dal = new cNotificationsDAL(_IniSectionKey)) {

                    dal.OutputLog += this.OnOutputLog;
                    dal.OutputTrace += this.OnOutputTrace;
                    dal.OutputLogAndTrace += this.OnOutputLogAndTrace;
                    dal.OutputLogAndTraceException += this.OnOutputLogAndTraceException;

                    if(dal.UpdateNotificationQueueStatusComplete(
                        sourceTrackingID, 
                        (int)NotificationQueueStatusType.Submitted,
                        (int)newNotificationQueueStatus, 
                        false,
                        out intRowsAffected)) {

                        bolRetVal = true;

                    } else {

                        OnOutputLogAndTrace(
                            "Unable to notify Notification complete status to the itemProcessing database for Source Tracking ID: " + sourceTrackingID.ToString(), 
                            this.GetType().Name, 
                            FITClientMessageType.Error, 
                            FITClientMessageImportance.Essential);

                        bolRetVal = false;
                    }
                }

            } catch(Exception ex) {
                OnOutputLogAndTraceException("An unexpected exception occurred in UpdateNotificationXFerStatusComplete()", this.GetType().Name, ex);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        //############################################################################
        //############################################################################
        public void OnOutputLog(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance) {
            if(OutputLog != null) {
                OutputLog(msg, src, FITItemProcessingXClientLib.ConvertLTAtoFIT(messageType), FITItemProcessingXClientLib.ConvertLTAtoFIT(messageImportance));
            }
        }

        public void OnOutputTrace(string msg, LTAConsoleMessageType msgType) {
            if(OutputTrace != null) {
                OutputTrace(msg, FITItemProcessingXClientLib.ConvertLTAtoFIT(msgType));
            }
        }

        public void OnOutputLogAndTrace(string msg, string src, LTAMessageType messageType, LTAMessageImportance messageImportance) {
            if(OutputLogAndTrace != null) {
                OutputLogAndTrace(msg, src, FITItemProcessingXClientLib.ConvertLTAtoFIT(messageType), FITItemProcessingXClientLib.ConvertLTAtoFIT(messageImportance));
            }
        }
        //############################################################################


        //############################################################################
        //############################################################################
        public void OnOutputLog(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance) {
            if(OutputLog != null) {
                OutputLog(msg, src, messageType, messageImportance);
            } else {
                LTA.Common.LTAConsole.ConsoleWriteLine(msg, LTAConvert.ConvertLTALogToConsoleLTA(FITItemProcessingXClientLib.ConvertFITtoLTA(messageType)));
            }
        }

        public void OnOutputTrace(string msg, FITClientConsoleLTAMessageType messageType) {
            if(OutputTrace != null) {
                OutputTrace(msg, messageType);
            } else {
                LTA.Common.LTAConsole.ConsoleWriteLine(msg, FITItemProcessingXClientLib.ConvertFITtoConsoleLTA(messageType));
            }
        }

        public void OnOutputLogAndTrace(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance) {
            if(OutputLogAndTrace != null) {
                OutputLogAndTrace(msg, src, messageType, messageImportance);
            } else {
                OnOutputLog(msg, src, messageType, messageImportance);
                OnOutputTrace(msg, FITItemProcessingXClientLib.ConvertFITtoFITClientConsoleLTA(messageType));
            }
        }

        public void OnOutputLogAndTraceException(string msg, string src, Exception e) {
            if(OutputLogAndTraceException != null) {
                OutputLogAndTraceException(msg, src, e);
            } else {
                OnOutputLog(msg, src, FITClientMessageType.Error, FITClientMessageImportance.Essential);
                OnOutputLog(e.Message, src, FITClientMessageType.Error, FITClientMessageImportance.Essential);

                OnOutputTrace(msg, FITClientConsoleLTAMessageType.Error);
                OnOutputTrace(e.Message, FITClientConsoleLTAMessageType.Error);
            }
        }
        //############################################################################


        
        //public bool NotificationDataSubmitted(Guid integraPAYNotificationID) {

        //    //List<cNotification> arNotifications;
        //    bool bolRetVal;
        //    int intRowsAffected;

        //    try {

        //        //if(NotificationLib.NotificationsFromXml(NotificationsXml, out arNotifications)) {
        //        //    foreach(cNotification notification in arNotifications) {
        //                if(!NotificationsDAL.UpsertNotificationQueueStatus(
        //                    integraPAYNotificationID, 
        //                    (int)NotificationQueueStatusTypes.Submitted, false,
        //                    out intRowsAffected)) {

        //                    //log error
        //                }
        //        //    }
        //        //}

        //        bolRetVal = true;

        //    } catch(Exception ex) {
        //        bolRetVal = false;
        //    }

        //    return(bolRetVal);
        //}


        //public bool LockNotificationData(Guid integraPAYNotificationID, out bool lockApplied) {

        //    bool bolRetVal;
        //    int intRowsAffected;

        //    try {

        //        if(!NotificationsDAL.UpsertNotificationQueueStatus(
        //            integraPAYNotificationID, 
        //            (int)NotificationQueueStatusTypes.InProcess, false,
        //            out intRowsAffected)) {

        //            //log error
        //        }

        //        lockApplied = intRowsAffected > 0;
        //        bolRetVal = true;

        //    } catch(Exception ex) {
        //        lockApplied = false;
        //        bolRetVal = false;
        //    }

        //    return(bolRetVal);
        //}

        /*

        /// <summary>
        /// Transfers notifications and files to CWDB.
        /// </summary>
        /// <returns></returns>
        private bool SendNotificationsData() {

            CBXCWDB.cTransferDoc xmlSource;
            CBXCWDB.cTransferDoc xmlReturn;
            bool blnErrors;
            bool blnHasErrors;
            DateTime datNotificationCheck;
            CBXCWDB.cSchema objTable;
            string strSQL;
            DataTable dt;
            bool blnError;

            bool bolRetVal;
    
            try {
    
                Console.WriteLine("CENDS data transfer process started.");  //, logVerbose, "SendNotificationData", EventTypeInformation
    
                datNotificationCheck = DateTime.Now;     // Save date/time of check
    
                objTable = new CBXCWDB.cSchema();
    
                xmlSource = new CBXCWDB.cTransferDoc();
                AddSiteCodeToDoc(xmlSource);
    
                // Add Notification records since last check
                if(objTable.LoadTable(Database.Connection, "Notifications")) {
                    m_objEncoder.AddSQL(xmlSource, objTable.TableName, SQL_GetNotifications(SystemOptions.LastNotificationCheck, datNotificationCheck), "x-schema:#" + objTable.TableName + "_Schema", objTable);
                } else {
                    blnErrors = true;
                }
    
                // Add RenderedFiles records since last check
                if(objTable.LoadTable(Database.Connection, "RenderedFiles")) {
                    m_objEncoder.AddSQL(xmlSource, objTable.TableName, SQL_GetNotifications2(SystemOptions.LastNotificationCheck, datNotificationCheck), "x-schema:#" + objTable.TableName + "_Schema", objTable);
                } else {
                    blnErrors = true;
                }
    
                // Add NotificationFiles records since last check
                if(objTable.LoadTable(Database.Connection, "NotificationFiles")) {
                    m_objEncoder.AddSQL(xmlSource, objTable.TableName, SQL_GetNotifications3(SystemOptions.LastNotificationCheck, datNotificationCheck), "x-schema:#" + objTable.TableName + "_Schema", objTable);
                } else {
                    blnErrors = true;
                }
    
                if(!blnErrors) {

                    // Send transfer doc to CWDB
                    xmlReturn = GetTransferClient.SendXMLDoc(xmlSource);
        
                    blnError = TransferErrors(xmlReturn);
                
                    if(blnError || SystemOptions.SaveXMLDocuments) {
                        // save transfer doc
                        Save(xmlSource, Path.Combine(SystemOptions.TransferLogPath, GetFilename("", "_cends_src", "xml")));
                        Save(xmlReturn, Path.Combine(SystemOptions.TransferLogPath, GetFilename("", "_cends_rtn", "xml")));
                        if(blnError) {
                            CENDSNotification ("An error occurred during SendNotificationsData. Please see the " + SystemOptions.TransferLogPath + "\\" + GetFilename("", @"_cends2_src/rtn", "xml") + " files for more information.");
                        }
                    }
        
                    if(!blnError) {
                        bolRetVal = true;
                        // Record date/time of check
                        Console.WriteLine("CENDS data transfer process completed.");    //, logVerbose, "SendNotificationData", EventTypeInformation
                    } else {
                        bolRetVal = false;
                        Console.WriteLine("CENDS data transfer process completed with errors.");    //, logNormal, "SendNotificationData", EventTypeError
                    }
        
                    // Record date/time of check
                    SystemOptions.LastNotificationCheck = datNotificationCheck;
                } else {
                    Console.WriteLine("CENDS data transfer process failed to complete.");   //, logNormal, "SendNotificationData", EventTypeError
                }
    
                xmlSource = null;
                xmlReturn = null;
    
            } catch(Exception ex) {
                bolRetVal = false;
                EventLog.LogError(Err, App.EXEName, "SendNotifications");
            }

            return(bolRetVal);
        }



        /// <summary>
        /// Transfers notifications files to CWDB.
        /// </summary>
        /// <returns></returns>
        private bool SendNotificationFiles() {

            CBXCWDB.cTransferDoc xmlSource;
            CBXCWDB.cTransferDoc xmlResult;
            IXMLDOMNode nodeFile;
            string strSQL;
            DataTable dt;
            string strPath;
            string strFileID;
            int lngTotalLen;
            string strCENDSDate;
            string strFilename;
            bool blnError;
            bool blnTransferError;
            int iCount;
            DateTime datLastCheckDate;
            DateTime datLastNotificationFile;
            DateTime datCheckDate;

            bool bolRetVal;
    
            try {
    
                bolRetVal = false;
    
                if(!SystemOptions.EnableENDSTransfer) {
                    Console.WriteLine("System option EnableCENDSTransfer has not been enabled.  Notification files will not be sent."); //, logVerbose, "SendNotificationFiles", EventTypeInformation
                    bolRetVal = true;
                    return(bolRetVal);
                }
    
                datCheckDate = DateTime.Now;
    
                Console.WriteLine("CENDS file transfer process started.");  //, logVerbose, "SendNotificationFiles", EventTypeInformation
    
                if(Database.CreateRecordset(SQL_GetNotificationsFiles(SystemOptions.LastNotificationFilesCheck), out dt)) {
        
                    foreach(DataRow dr in dt.Rows) {
        
                        if(xmlSource == null) {
                            xmlSource = new CBXCWDB.cTransferDoc();
                            AddSiteCodeToDoc(xmlSource);
                        }
            
                        strPath = Path.Combine(SystemOptions.CENDSPath, dr["FilePathName"].ToString());
                        datLastNotificationFile = (DateTime)dr["NotificationDeliveryDate"];
            
                        Console.WriteLine("Adding notification file '");    // + strPath + "'.", logVerbose, "SendNotificationFiles");
            
                        if(strPath.Length > 0) {

                            ////strCENDSDate = Left$(rs.Fields("FilePathName"), InStr(CStr(rs.Fields("FilePathName").Value), @"\") - 1);
                            ////strFilename = Right$(rs.Fields("FilePathName"), Len(rs.Fields("FilePathName")) - InStr(CStr(rs.Fields("FilePathName").Value), @"\"));
                            lngTotalLen += FileLen(strPath);
                
                            strFileID = GetAttachmentFileID();
                            nodeFile = xmlSource.AddFileAsAttachment(strPath, 1, strFileID);
                
                            xmlSource.addAttribute(nodeFile, "Date", nodeFile.namespaceURI, strCENDSDate);
                            xmlSource.addAttribute(nodeFile, "Filename", nodeFile.namespaceURI, strFilename);
                
                            // Since CENDS files are larger than images, only send multiple files in document
                            // if threshold is greather than 0.  Normally, send files individually.
                            if(SystemOptions.ImageDocSize == 0 || lngTotalLen > (SystemOptions.ImageDocSize * 1024)) {
                                Console.WriteLine("Sending notification files to Consolidation DB (size=" + lngTotalLen + ").");    //, logVerbose, "SendNotificationFiles"
                    
                                xmlResult = m_objClient.SendXMLFiles(xmlSource);
                    
                                if(!xmlResult == null) {
                                    blnError = TransferErrors(xmlResult);
                        
                                    if(blnError) {
                                        blnTransferError = true;
                                    }
                        
                                    if(blnError || SystemOptions.SaveXMLDocuments) {
                                        // Save xml transfer documents.
                                        Save(xmlSource, Path.Combine(SystemOptions.TransferLogPath, GetFilename("", "_CENDS_File" + iCount.ToString() + "_src", "xml")));
                                        Save(xmlResult, Path.Combine(SystemOptions.TransferLogPath, GetFilename("", "_CENDS_File" + iCount.ToString() + "_rtn", "xml")));
                                    }
                        
                                    // Save date/time as last file sent
                                    if(!blnTransferError) {
                                        datLastCheckDate = datLastNotificationFile;
                                    }

                                } else {
                                    Console.WriteLine("Sending notification files failed.  Response document not received.");   //, logVerbose, "SendNotificationFiles"
                                    blnTransferError = true;
                                }

                                lngTotalLen = 0;
                    
                                iCount++;
                                xmlResult = null;
                                xmlSource = null;
                            }
                        } else {
                            Console.WriteLine("Adding file '" + strPath + "' failed.  File not found.");    //, logVerbose, "SendNotificationFiles", EventTypeError
                        }
            
                    }
        
                    if((!xmlSource == null) && (!blnTransferError)) {
                        Console.WriteLine("Sending notification files to Consolidation DB (size=" + lngTotalLen + ").");    //, logVerbose, "SendNotificationFiles"
            
                        xmlResult = m_objClient.SendXMLFiles(xmlSource);
            
                        if(!xmlResult == null) {
                            blnError = TransferErrors(xmlResult);
                
                            if(blnError) {
                                blnTransferError = true;
                            }
                
                            if(blnError || SystemOptions.SaveXMLDocuments) {

                                // Save xml transfer documents.
                                Save(xmlSource, Path.Combine(SystemOptions.TransferLogPath, GetFilename("", "_CENDS_File" + iCount.ToString() + "_src", "xml")));
                                Save(xmlResult, Path.Combine(SystemOptions.TransferLogPath, GetFilename("", "_CENDS_File" + iCount.ToString() + "_rtn", "xml")));
                            }
            
                            // Save date/time as last file sent
                            if(!blnTransferError) {
                                datLastCheckDate = datLastNotificationFile;
                            }
                        } else {
                            blnTransferError = true;
                        }
            
                        xmlResult = null;
                        xmlSource = null;
                    }
        
                    if(blnTransferError) {
                        bolRetVal = false;
            
                        // update check date with last notification file sent
                        if(!CDate(0) == datLastCheckDate) {
                            SystemOptions.LastNotificationFilesCheck = datLastCheckDate;
                        }
            
                        Console.WriteLine("CENDS file transfer process completed with errors.");    //, logVerbose, "SendNotificationFiles", EventTypeInformation
                    } else {
                        bolRetVal = true;
            
                        // update check date with current time
                        if(!blnTransferError) {
                            SystemOptions.LastNotificationFilesCheck = datCheckDate;
                        }
            
                        Console.WriteLine("CENDS file transfer process completed.");    //, logVerbose, "SendNotificationFiles", EventTypeInformation
                    }
        
                } else {
                    Console.WriteLine("CENDS file transfer process failed to complete.");   //, logNormal, "SendNotificationFiles", EventTypeError
                }
    
                dt.Dispose();
    
            } catch(Exception ex) {
                bolRetVal = false;
                EventLog.LogError(Err, App.EXEName, "SendNotificationFiles");
            }

            return(bolRetVal);
        }
    
        private string SQL_GetNotifications(DateTime LastNotificationCheck, DateTime NotificationCheck) {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append("SELECT " + objTable.GetColumnNamesAsString(",") + " FROM Notifications");
            sbSQL.Append(" INNER JOIN Alerts ON Alerts.AlertID=Notifications.AlertID");
            sbSQL.Append(" INNER JOIN Contacts ON Contacts.ContactID=Alerts.ContactID");
            sbSQL.Append(" WHERE Contacts.CustomerID >= 0");
            sbSQL.Append(" AND NotificationDeliveryDate >= DATEADD(minute, -15, '" + LastNotificationCheck.ToString("MM/dd/yyyy") + "00.00.00.000')");
            sbSQL.Append(" AND NotificationDeliveryDate <= '" + NotificationCheck.ToString("MM/dd/yyyy") + "00.00.00.000'");

            return(sbSQL.ToString());
        }

        private string SQL_GetNotifications2(DateTime LastNotificationCheck, DateTime NotificationCheck) {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append("SELECT DISTINCT " + objTable.GetColumnNamesAsString(",") + " FROM RenderedFiles");
            sbSQL.Append(" INNER JOIN NotificationFiles ON RenderedFiles.FileID = NotificationFiles.FileID");
            sbSQL.Append(" INNER JOIN Notifications ON NotificationFiles.NotificationID = Notifications.NotificationID");
            sbSQL.Append(" INNER JOIN Alerts ON Alerts.AlertID=Notifications.AlertID");
            sbSQL.Append(" INNER JOIN Contacts ON Contacts.ContactID=Alerts.ContactID");
            sbSQL.Append(" WHERE Contacts.CustomerID >= 0");
            sbSQL.Append(" AND Notifications.NotificationDeliveryDate >= DATEADD(minute, -15, '" + LastNotificationCheck.ToString("MM/dd/yyyy") + "00.00.00.000')");
            sbSQL.Append(" AND Notifications.NotificationDeliveryDate <= '" + NotificationCheck.ToString("MM/dd/yyyy") + "00.00.00.000'");

            return(sbSQL.ToString());
        }

        private string SQL_GetNotifications3(DateTime LastNotificationCheck, DateTime NotificationCheck) {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append("SELECT " + objTable.GetColumnNamesAsString(",") + " FROM NotificationFiles");
            sbSQL.Append(" INNER JOIN Notifications ON NotificationFiles.NotificationID = Notifications.NotificationID");
            sbSQL.Append(" INNER JOIN Alerts ON Alerts.AlertID=Notifications.AlertID");
            sbSQL.Append(" INNER JOIN Contacts ON Contacts.ContactID=Alerts.ContactID");
            sbSQL.Append(" INNER JOIN RenderedFiles ON RenderedFiles.FileID = NotificationFiles.FileID");
            sbSQL.Append(" WHERE Contacts.CustomerID >= 0");
            sbSQL.Append(" AND Notifications.NotificationDeliveryDate >= DATEADD(minute, -15, '" + LastNotificationCheck.ToString("MM/dd/yyyy") + "00.00.00.000')");
            sbSQL.Append(" AND Notifications.NotificationDeliveryDate <= '" + NotificationCheck.ToString("MM/dd/yyyy") + "00.00.00.000'");

            return(sbSQL.ToString());
        }
    
        private string SQL_GetNotificationsFiles(DateTime LastNotificationCheck) {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append("SELECT Notifications.NotificationDeliveryDate, RenderedFiles.FilePathName");
            sbSQL.Append(" FROM Notifications");
            sbSQL.Append(" INNER JOIN Alerts ON Alerts.AlertID=Notifications.AlertID");
            sbSQL.Append(" INNER JOIN Contacts ON Contacts.ContactID=Alerts.ContactID");
            sbSQL.Append(" INNER JOIN NotificationFiles ON NotificationFiles.NotificationID = Notifications.NotificationID");
            sbSQL.Append(" INNER JOIN RenderedFiles ON RenderedFiles.FileID = NotificationFiles.FileID");
            sbSQL.Append(" WHERE Contacts.CustomerID >= 0");
            sbSQL.Append(" AND Notifications.NotificationDeliveryDate >= DATEADD(minute, -15, '" + LastNotificationCheck.ToString("MM/dd/yyyy") + "00.00.00.000')");
            sbSQL.Append(" GROUP BY Notifications.NotificationDeliveryDate, RenderedFiles.FilePathName");
            sbSQL.Append(" ORDER BY Notifications.NotificationDeliveryDate ASC");

            return(sbSQL.ToString());
        }
    */

        public void Configure(ConfigurationContext configurationContext)
        {
            this.configurationContext = configurationContext;
        }
    }

    //internal class cSystemOptions {
    //    public bool EnableENDSTransfer = false;
    //    public int ImageDocSize = 0;
    //    public bool SaveXMLDocuments =false;
    //    public DateTime LastNotificationCheck = DateTime.MinValue;
    //    public string CENDSPath = string.Empty;
    //    public DateTime LastNotificationFilesCheck = DateTime.MinValue;
    //    public string TransferLogPath = string.Empty;
    //    public int RetentionDays = 9999;
    //}

}
