﻿using System;
using System.Collections;
using System.Collections.Specialized;

using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 53791 JMC 07/17/2012 
*   -Initial version.
*******************************************************************************/
namespace WFS.LTA.FileImportServices.ItemProcessingXClient {

    internal class cXClientSiteOptions : cSiteOptions {
        
        private int _RetentionDays;
        private int _MaxNotificationsPerUpload;

        public cXClientSiteOptions(string siteKey) : base(siteKey) {

            StringCollection colSiteOptions = ipoINILib.GetINISection(siteKey);
            string strKey;
            string strValue;
            int intTemp;

            const string INIKEY_RETENTION_DAYS = "RetentionDays";
            const string INIKEY_MAX_NOTIFICATIONS_PER_UPLOAD = "MaxNotificationsPerUpload";

            System.Collections.IEnumerator myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {
                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if(strKey.ToLower() == INIKEY_RETENTION_DAYS.ToLower()) {
                    if(int.TryParse(strValue, out intTemp)) {
                        _RetentionDays = intTemp;
                    }
                } else if(strKey.ToLower() == INIKEY_MAX_NOTIFICATIONS_PER_UPLOAD.ToLower()) {
                    if(int.TryParse(strValue, out intTemp)) {
                        _MaxNotificationsPerUpload = intTemp;
                    }
                }
            }
        }

        public int RetentionDays {
            get {
                return(_RetentionDays);
            }
        }

        public int MaxNotificationsPerUpload {
            get {
                return(_MaxNotificationsPerUpload);
            }
        }
    }
}
