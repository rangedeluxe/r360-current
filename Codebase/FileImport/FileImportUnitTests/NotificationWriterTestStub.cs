﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileImportShredder;

namespace FileImportUnitTests {
    public class NotificationWriterTestStub : INotificationWriter {
        public NotificationWriterTestStub() {
            NotificationsTable = new List<Notification>();
            NotificationFilesTable = new List<NotificationFile>();
            NotificationMessagesTable = new List<NotificationMessage>();
            NotificationResponsesTable = new List<NotificationResponse>();
        }

    public void WriteNotification(Notification notificationData) {
            NotificationsTable.Add(notificationData);
        }
        public void WriteNotificationFile(NotificationFile fileData) {
            NotificationFilesTable.Add(fileData);
        }
        public void WriteNotificationMessage(NotificationMessage messageData) {
            NotificationMessagesTable.Add(messageData);
        }
        public void WriteNotificationResponse(NotificationResponse response) {
            NotificationResponsesTable.Add(response);
        }

        public List<Notification> NotificationsTable { get; }
        public List<NotificationFile> NotificationFilesTable { get; }
        public List<NotificationMessage> NotificationMessagesTable { get; }
        public List<NotificationResponse> NotificationResponsesTable { get; }
    }
}
