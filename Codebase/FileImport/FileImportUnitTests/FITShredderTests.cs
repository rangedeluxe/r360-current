﻿using System;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using FileImportShredder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FileImportUnitTests {
    [TestClass]
    public class FITShredderTests {
        const long DATA_IMPORT_QUEUE_ID = 150000;

        [TestMethod]
        public void ProcessAll_ShouldPass_ReadInOneMessagePartAndOneAttachment() {
            var writer = new NotificationWriterTestStub();
            ShredContent(CreateStandardQueryEntry(), writer);

            Assert.AreEqual(1, writer.NotificationFilesTable.Count, "Wrong number of attachments");
            Assert.AreEqual(1, writer.NotificationMessagesTable.Count, "Wrong number of message parts");
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_ReadAttachmentWithFileSizeOf50() {
            var writer = new NotificationWriterTestStub();
            ShredContent(CreateStandardQueryEntry(), writer);

            Assert.AreEqual(50, writer.NotificationFilesTable[0].FileSize, "File Size is parsed incorrectly");
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_NotificationDateAsZulu()
        {
            var writer = new NotificationWriterTestStub();
            ShredContent(CreateStandardQueryEntry(), writer);

            Assert.AreEqual(new DateTime(2017, 5, 10, 5, 00, 00), writer.NotificationsTable[0].NotificationDate);
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_NotificationDateAsNonZulu()
        {
            var writer = new NotificationWriterTestStub();
            ShredContent(CreateStandardQueryEntry(notificationDate: "2017-05-10 05:00:00"), writer);

            Assert.AreEqual(new DateTime(2017, 5, 10, 10, 00, 00), writer.NotificationsTable[0].NotificationDate);
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_ReadMessageThatSaysMessageOne() {
            var writer = new NotificationWriterTestStub();
            ShredContent(CreateStandardQueryEntry(), writer);

            Assert.AreEqual("Message One", writer.NotificationMessagesTable[0].MessageText, "Message Text is parsed incorrectly");
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_NotificationWithWorkgroupMessageId()
        {
            var messageWorkgroupsId = Guid.NewGuid();
            var notification = CreateNotificationElement(messageWorkgroups: messageWorkgroupsId);
            var writer = new NotificationWriterTestStub();
            ShredContent(notification.ToString(), writer);

            Assert.AreEqual(messageWorkgroupsId, writer.NotificationsTable[0].MessageWorkgroups);
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_NotificationWithoutWorkgroupMessageId()
        {
            var notification = CreateNotificationElement();
            var writer = new NotificationWriterTestStub();
            ShredContent(notification.ToString(), writer);

            Assert.AreEqual(null, writer.NotificationsTable[0].MessageWorkgroups);
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_ReadInNoMessagePartAndNoAttachment() {
            var writer = new NotificationWriterTestStub();
            ShredContent(FIT_Data_Import_Queue_Example_No_Messages_No_Attachments, writer);

            Assert.AreEqual(0, writer.NotificationFilesTable.Count, "Wrong number of attachments");
            Assert.AreEqual(0, writer.NotificationMessagesTable.Count, "Wrong number of message parts");
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_ReadInEmptySourceNotificationId_PopulateNewSourceNotificationId() {
            var writer = new NotificationWriterTestStub();
            ShredContent(FIT_Data_Import_Queue_Example_No_Messages_No_Attachments, writer);

            Assert.AreNotEqual(Guid.Empty, writer.NotificationsTable[0].SourceNotificationId, "Source Notification Id should not be empty");
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_ReadInNotificationWithoutSourceNotificationId_PopulateNewSourceNotificationId() {
            var writer = new NotificationWriterTestStub();
            ShredContent(FIT_Data_Import_Queue_Example_Without_SourceNotificationID, writer);

            Assert.AreNotEqual(Guid.Empty, writer.NotificationsTable[0].SourceNotificationId, "Source Notification Id should not be empty");
        }

        [TestMethod]
        public void ProcessAll_ShouldPass_NotificationsMissingAttributes() {
            var writer = new NotificationWriterTestStub();
            ShredContent(FIT_Data_Import_Queue_Example_Missing_Attributes, writer);

            Assert.AreEqual(0, writer.NotificationMessagesTable.Count, "Wrong number of message parts");
        }

        private XmlReader CreateXmlReaderOver(string content) {
            return XmlReader.Create(new StringReader(content));
        }

        private void ShredContent(string xmlContent, INotificationWriter writer) {
            var shredder = new FITShredder(CreateXmlReaderOver(xmlContent), 150000, writer);
            shredder.ProcessAll();
        }

        public XElement CreateNotificationElement(int bankId = 9999, int clientGroupId = -1,int clientId = 99, 
            DateTime? notificationDate = null, int notificationSourceKey = 100,Guid? notificationTrackingId = null,
            Guid? sourceNotificationId = null,Guid? messageWorkgroups = null)
        {
            var notification = new XElement(
                "Notification",
                    new XAttribute("BankID", bankId),
                    new XAttribute("ClientGroupID", clientGroupId),
                    new XAttribute("ClientID", clientId),
                    new XAttribute("NotificationDate", notificationDate?.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture) ?? DateTime.Now.ToString("MM-dd-yyyy", CultureInfo.InvariantCulture)),
                    new XAttribute("NotificationTrackingID", notificationTrackingId ?? Guid.NewGuid()),
                    new XAttribute("NotificationSourceKey", notificationSourceKey),
                    new XAttribute("SourceNotificationID", sourceNotificationId ?? Guid.NewGuid())
                );

            if (messageWorkgroups != null)
                notification.Add(new XAttribute("MessageWorkgroups", messageWorkgroups));

            return notification;
        }

        private string CreateStandardQueryEntry(string bankId = "9999", string clientGroupId = "-1",
            string clientId = "99", string notificationDate = "2017-05-10 05:00:00Z",
            string notificationTrackingId = "aeab8498-2391-435b-acc7-f324c94165a4",
            string notificationSourceKey = "100", string sourceNotificationId = "00000000-0000-0000-0000-000000000000",
            string notificationMessagePart = "1", string message = "Message One", string fileType = "tre",
            string userFileName = "product video", string fileExtension = ".mp4",
            string fileIdentifier = "e6ac4491-1d16-4f91-9a06-aff82ed4164f", string fileSize = "50")
        {
            return
                $"<Notification BankID=\"{bankId}\" ClientGroupID=\"{clientGroupId}\" ClientID=\"{clientId}\" NotificationDate=\"{notificationDate}\" NotificationTrackingID=\"{notificationTrackingId}\" NotificationSourceKey=\"{notificationSourceKey}\" SourceNotificationID=\"{sourceNotificationId}\">\r\n" +
                "	<MessageParts MessageParts_Id=\"1\">\r\n" +
                $"		<MessagePart NotificationMessagePart=\"{notificationMessagePart}\">{message}</MessagePart>\r\n" +
                "	</MessageParts>\r\n" +
                "	<Files Files_Id=\"1\">\r\n" +
                $"		<File File_Id=\"1\" FileType=\"{fileType}\" UserFileName=\"{userFileName}\" FileExtension=\"{fileExtension}\" FileIdentifier=\"{fileIdentifier}\" FileSize=\"{fileSize}\" />\r\n" +
                "	</Files>\r\n" +
                "</Notification>\r\n";
        }

        private string FIT_Data_Import_Queue_Example_Without_SourceNotificationID {
            get {
                return
                    "<Notification BankID=\"9999\" ClientGroupID=\"-1\" ClientID=\"99\" NotificationDate=\"2017-05-10 05:00:00Z\" NotificationTrackingID=\"aeab8498-2391-435b-acc7-f324c94165a4\" NotificationSourceKey=\"100\">\r\n" +
                    "	<MessageParts MessageParts_Id=\"1\">\r\n" +
                    "		<MessagePart NotificationMessagePart=\"1\">Message One</MessagePart>\r\n" +
                    "	</MessageParts>\r\n" +
                    "	<Files Files_Id=\"1\">\r\n" +
                    "		<File File_Id=\"1\" FileType=\"tre\" UserFileName=\"product video\" FileExtension=\".mp4\" FileIdentifier=\"e6ac4491-1d16-4f91-9a06-aff82ed4164f\" FileSize=\"50\" />\r\n" +
                    "	</Files>\r\n" +
                    "</Notification>\r\n";
            }
        }

        private string FIT_Data_Import_Queue_Example_No_Messages_No_Attachments {
            get {
                return
                    "<Notification BankID=\"9999\" ClientGroupID=\"-1\" ClientID=\"99\" NotificationDate=\"2017-05-10 05:00:00Z\" NotificationTrackingID=\"aeab8498-2391-435b-acc7-f324c94165a4\" NotificationSourceKey=\"100\" SourceNotificationID=\"00000000-0000-0000-0000-000000000000\"/>";
            }
        }

        private string FIT_Data_Import_Queue_Example_Missing_Attributes {
            get {
                return
                    "<Notification />";
            }
        }
    }
}
