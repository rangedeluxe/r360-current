﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WFS.LTA.FileImportClientLib;

namespace fitExtractXClient
{
    public class JsonParser
    {
        private readonly IFileReader _reader;
        public ExtractModel ExtractModel { get; }
        public JsonParser(IFileReader reader)
        {
            ExtractModel = JsonConvert.DeserializeObject<ExtractModel>(reader.FileContent());
            _reader = reader;
            ExtractModel.MessageWorkgroups = Guid.NewGuid();
        }

        public IEnumerable<cNotification> GetNotifications()
        {
            var list = new List<cNotification>();
            var sourceNotification = Guid.NewGuid();
            foreach (var wg in ExtractModel.Workgroups)
            {
                var file = new cNotificationFile
                {
                    FileIdentifier = Guid.NewGuid(),
                    ClientFilePath = ExtractModel.File,
                    UserFileName = _reader.GetFileNameWithoutExtension(ExtractModel.File),
                    FileExtension = _reader.FileExtension(ExtractModel.File),
                    FileSize = _reader.GetFileSize(ExtractModel.File).ToString(),
                    FileType = _reader.GetFileType(ExtractModel.File)
                };
                var notification = new cNotification
                {
                    NotificationFiles = {file},
                    BankID = wg.Bank,
                    ClientID = wg.Workgroup,
                    NotificationDate = DateTime.Now,
                    SourceNotificationID = sourceNotification
                };
                var message = !string.IsNullOrEmpty(ExtractModel.Message)
                    ? ExtractModel.Message.Replace("@FullWorkgroupName",
                        $"{wg.Workgroup} - {wg.WorkgroupName}")
                    : string.Empty;
                notification.SetMessage(message);
                list.Add(notification);
            }
            return list;
        }


    }
}
