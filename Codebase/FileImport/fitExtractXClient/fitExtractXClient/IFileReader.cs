﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitExtractXClient
{
    public interface IFileReader
    {
        string FileContent();
        string FileExtension(string file);
        string GetFileNameWithoutExtension(string file);
        long GetFileSize(string file);
        string GetFileType(string name);
    }
}
