﻿using System.IO;
using System.Web;

namespace fitExtractXClient
{
    public class SystemFileReader : IFileReader
    {
        private readonly string _filePath;
        public SystemFileReader(string filepath)
        {
            _filePath = filepath;
        }
        public string FileContent()
        {
            return File.ReadAllText(_filePath);
        }

        public string FileExtension(string file)
        {
            return Path.GetExtension(file ?? _filePath);
        }

        public string GetFileNameWithoutExtension(string file)
        {
            
            return Path.GetFileNameWithoutExtension(file ?? _filePath);
        }

        public long GetFileSize(string file)
        {
            if (!File.Exists(file)) throw new FileNotFoundException($"{file} not found."){Source = "SystemFileReader - GetFileSize" + file};
            return new FileInfo(file).Length;
        }

        public string GetFileType(string filename)
        {
            return MimeMapping.GetMimeMapping(filename);
        }
    }
}
