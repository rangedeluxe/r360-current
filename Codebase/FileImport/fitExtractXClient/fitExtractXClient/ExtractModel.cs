﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitExtractXClient
{
    public class ExtractModel
    {
        public IEnumerable<WorkgroupModel> Workgroups { get; set; }
        public string Message { get; set; }
        public string File { get; set; }
        public Guid MessageWorkgroups { get; set; }
    }
}