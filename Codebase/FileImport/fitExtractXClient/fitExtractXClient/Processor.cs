﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using WFS.LTA.Common;
using WFS.LTA.FileImportClientLib;
using WFS.LTA.FileImportServices.ICONXClient;

namespace fitExtractXClient
{
    public class Processor : IFileDropXClient
    {
        private ConfigurationContext configurationContext;
        private const string SCHEMAVERSION = "2.03.02.00";
        private const string MESSAGEWORKGROUPS = "MessageWorkgroups";
        public void Configure(ConfigurationContext configurationContext)
        {
            this.configurationContext = configurationContext;
        }

        public bool GetNotificationXml(string inputFilePath, string clientProcessCode, out XmlDocument fileData,
            out int notificationCount)
        {
            bool result;
            try
            {
                var jparser = new JsonParser(new SystemFileReader(inputFilePath));
                var notifications = jparser.GetNotifications().ToList();
                notificationCount = notifications.Count;
                XmlDocument docFileData;
                result = NotificationLib.NotificationsToXml(Guid.NewGuid(), clientProcessCode, notifications,
                    out docFileData);

                if (docFileData.DocumentElement != null)
                {
                    var messageGroupAtt = docFileData.CreateAttribute(MESSAGEWORKGROUPS);
                    messageGroupAtt.InnerText = jparser.ExtractModel.MessageWorkgroups.ToString();
                    docFileData.DocumentElement.Attributes.Append(messageGroupAtt);
                    docFileData.DocumentElement.Attributes["XSDVersion"].Value = SCHEMAVERSION;
                }

                fileData = docFileData;
            }
            catch (JsonReaderException ex)
            {
                OnOutputLogAndTrace("Data in file is not valid json format: " + inputFilePath, this.GetType().Name,
                    FITClientMessageType.Error, FITClientMessageImportance.Essential);
                fileData = null;
                notificationCount = 0;
                result = false;
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is ArgumentNullException || ex is DirectoryNotFoundException ||
                    ex is IOException || ex is FileNotFoundException)

                {
                    OnOutputLogAndTrace(
                        "There was a problem reading a specified file. Check for file integrity and existance : " +
                        ex.Message + ". Source: " + ex.Source,
                        this.GetType().Name, FITClientMessageType.Error, FITClientMessageImportance.Essential);

                }
                else if (ex is SecurityException || ex is UnauthorizedAccessException)
                {
                    OnOutputLogAndTrace("Access to the file was denied. Check for file file permissions: " + inputFilePath,
                        this.GetType().Name, FITClientMessageType.Error, FITClientMessageImportance.Essential);
                }


                fileData = null;
                notificationCount = 0;
                result = false;
            }
            return result;
        }



        public event FITClientOutputLogEventHandler OutputLog;
        public event FITClientOutputTraceEventHandler OutputTrace;
        public event FITClientOutputLogAndTraceEventHandler OutputLogAndTrace;
        public event FITClientOutputLogAndTraceExceptionEventHandler OutputLogAndTraceException;

        public void OnOutputLog(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance)
        {
            if (OutputLog != null)
            {
                OutputLog(msg, src, messageType, messageImportance);
            }
            else
            {
                LTAConsole.ConsoleWriteLine(msg, LTAConvert.ConvertLTALogToConsoleLTA(FITXClientLib.ConvertFITtoLTA(messageType)));
            }
        }

        public void OnOutputTrace(string msg, FITClientConsoleLTAMessageType messageType)
        {
            if (OutputTrace != null)
            {
                OutputTrace(msg, messageType);
            }
            else
            {
                LTAConsole.ConsoleWriteLine(msg, FITXClientLib.ConvertFITtoConsoleLTA(messageType));
            }
        }

        public void OnOutputLogAndTrace(string msg, string src, FITClientMessageType messageType, FITClientMessageImportance messageImportance)
        {
            if (OutputLogAndTrace != null)
            {
                OutputLogAndTrace(msg, src, messageType, messageImportance);
            }
            else
            {
                OnOutputLog(msg, src, messageType, messageImportance);
                OnOutputTrace(msg, FITXClientLib.ConvertFITtoFITClientConsoleLTA(messageType));
            }
        }

        public void OnOutputLogAndTraceException(string msg, string src, Exception e)
        {
            if (OutputLogAndTraceException != null)
            {
                OutputLogAndTraceException(msg, src, e);
            }
            else
            {
                OnOutputLog(msg, src, FITClientMessageType.Error, FITClientMessageImportance.Essential);
                OnOutputLog(e.Message, src, FITClientMessageType.Error, FITClientMessageImportance.Essential);

                OnOutputTrace(msg, FITClientConsoleLTAMessageType.Error);
                OnOutputTrace(e.Message, FITClientConsoleLTAMessageType.Error);
            }
        }
    }
}
