﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitExtractXClient
{
    public class WorkgroupModel
    {
        public int Bank { get; set; }
        public int Workgroup { get; set; }
        public string WorkgroupName { get; set; }
    }
}
