﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: Contains the implementation which thick-clients use to authenticate
*          with RAAM. 
*
* Modification History
* WI 155343 BLR 07/31/2014
*    Initial 
* ******************************************************************************/


namespace WFS.RecHub.ExternalLogonService
{
    public class ExternalLogonClient : IExternalLogonService
    {
        private readonly IExternalLogonService _client;

        public ExternalLogonClient()
        {
            //_client = new IExternalLogonService();
            _client = R360ServiceFactory.Create<IExternalLogonService>();
        }

        public ExternalLogonResponse ExternalLogon(ExternalLogonRequest request)
        {
            return _client.ExternalLogon(request);
        }

        public SessionResponse ValidateSession(SessionRequest request)
        {
            return _client.ValidateSession(request);
        }

        public SessionResponse EndSession(SessionRequest request)
        {
            return _client.EndSession(request);
        }

        public PingResponse Ping()
        {
            return _client.Ping();
        }

        public ExternalLogonResponse PermissionExternalLogon(PermissionLogonRequest request)
        {
            return _client.PermissionExternalLogon(request);
        }
    }
}
