﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.ApplicationBlocks.DataAccess;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ExternalLogon.Common.DTO;
using WFS.RecHub.ExternalLogon.Common.Interfaces;
using WFS.RecHub.R360Shared;
using static WFS.RecHub.R360Shared.ConfigHelpers;

namespace WFS.RecHub.ExternalLogon.API.Repositories
{
    public class SessionRepository : ISessionRepository
    {
        private readonly ConnectionStringSettings _connectionStringSettings;
        private readonly ILogger _logger;

        public SessionRepository(ILogger logger, R360DBConnectionSettings r360DBConnectionSettings)
        {
             _connectionStringSettings = r360DBConnectionSettings.ToConnectionStringSettings();
            _logger = logger;
        }

        public bool EndSession(Guid sessionId)
        {
            if (sessionId == Guid.Empty) return false;

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmSessionID", dbProviderFactory, DbType.Guid, ParameterDirection.Input, sessionId)
            };

            Database.ExecuteNonQuery(_connectionStringSettings, CommandType.StoredProcedure, "RecHubUser.usp_Session_Upd_EndSession", parms);

            return true;
        }

        public PossibleResult<IEnumerable<SessionDto>> GetSession(Guid sessionId)
        {
            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);
            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmSessionID", dbProviderFactory, DbType.Guid, ParameterDirection.Input, sessionId),
            };
            var result = Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubUser.usp_Session_Get_GetSession", parms);

            if (!result.HasData()) return Result.None<IEnumerable<SessionDto>>();

            return Result.Real(result.Tables[0].ToObjectList<SessionDto>() as IEnumerable<SessionDto>);
        }
    }
}
