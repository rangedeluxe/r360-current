﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: Contains configuration and validation for the service settings.
*
* Modification History
* WI 157573 BLR 08/11/2014
*  - Initial
* ******************************************************************************/

namespace WFS.RecHub.ExternalLogon.API
{
    public class ExternalLogonConfiguration : BaseConfigurationSettings
    {
         [ConfigurationSetting]
        public string ApplicationURI { get; set; }

        public ExternalLogonConfiguration(IConfigurationProvider configurationProvider)
            : base(configurationProvider)
        {

        }

        public override void ValidateSettings()
        {
            // Check to see if the URI is actually there.
            if (string.IsNullOrWhiteSpace(ApplicationURI))
                throw new ConfigurationException("ApplicationURI");

            // Check to see if we have a valid URI.
            try
            {
                new Uri(ApplicationURI);
            }
            catch
            {
                throw new ConfigurationException("ApplicationURI");
            }
        }
    }
}
