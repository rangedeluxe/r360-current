﻿using System;
using System.Data;
using System.Linq;
using Wfs.Raam.Core;
using Wfs.Raam.Core.Services;
using System.Security.Claims;
using System.IdentityModel.Services;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.SessionMaintenance.ServiceClient;
using WFS.RecHub.SessionMaintenance.Common;
using WFS.RecHub.ExternalLogon.Common.Interfaces;
using WFS.RecHub.ExternalLogon.API.Repositories;
using WFS.RecHub.ApplicationBlocks.Common;
using System.Collections.Generic;
using WFS.RecHub.ExternalLogon.Common.DTO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: The main implementation for the Authentication Service. All of the
*          logic is stored here. 
*
* Modification History
* WI 155343 BLR 07/31/2014
*   Initial 
* WI 157263 BLR 08/07/2014
*   Cleaned up the Claims access as per Monty's suggestions. 
* WI 157583 BLR 08/08/2014
*   Added the permission login.
* WI 157573 BLR 08/11/2014
*  - Removed Application from the request, this exists on the server side now.
*  - Added the ExternalLoginConfiguration for the AppSettings.  
* WI 165968 TWE 09/16/2014
*  - modified code to take advantage of latest drop (1.9) of RAAM
* WI 167653 TWE 09/24/2014
*    RAAM modifed signature, so adjust call to match
* WI 167783 BLR 09/24/2014
*  - Added PermissionTypes to the Permission Requests.
* ******************************************************************************/


namespace WFS.RecHub.ExternalLogon.API
{
    public class RAAMExternalLogon :APIBase, IExternalLogonService, IDisposable
    {
        // Track whether Dispose has been called.
        private bool disposed = false;

        private readonly WFS.RecHub.R360Shared.ConfigHelpers.ILogger _logger;
        private readonly ExternalLogonConfiguration _configuration;
        private SessionMaintenanceManager _SessionMaintenanceService = null;
        private readonly ISessionRepository _sessionRepository;
        

        public RAAMExternalLogon(WFS.RecHub.R360Shared.ConfigHelpers.ILogger logger, R360ServiceContext serviceContext)
            : this(logger, serviceContext, new SessionRepository(logger, new R360DBConnectionSettings())) 
        { }

        public RAAMExternalLogon(WFS.RecHub.R360Shared.ConfigHelpers.ILogger logger, R360ServiceContext serviceContext, ISessionRepository sessionRepository)
            : base(serviceContext)
        {
            _logger = logger;
            _configuration = new ExternalLogonConfiguration(new ConfigurationManagerProvider());
            _sessionRepository = sessionRepository;

            try
            {
                _configuration.ValidateSettings();
            }
            catch (Exception e)
            {
                _logger.LogWarning(e.Message);
            }

        }

        protected SessionMaintenanceManager SessionMaintenanceServiceClient
        {
            get
            {
                if (_SessionMaintenanceService == null)
                {
                    _SessionMaintenanceService = new SessionMaintenanceManager();
                }
                return _SessionMaintenanceService;
            }
        }

        public ExternalLogonResponse ExternalLogon(ExternalLogonRequest request)
        {
            var permrequest = new PermissionLogonRequest()
            {
                Entity = request.Entity,
                IPAddress = request.IPAddress,
                Password = request.Password,
                Username = request.Username
            };
            return PermissionExternalLogon(permrequest);
        }

        protected override WFS.RecHub.Common.ActivityCodes RequestType
        {
            get { throw new NotImplementedException(); }
        }

        public SessionResponse ValidateSession(SessionRequest request)
        {
            var sessions = _sessionRepository.GetSession(request.Session).GetResult(new List<SessionDto>());
            if (!sessions.Any())
            {
                return new SessionResponse
                {
                    Status = StatusCode.FAIL,
                    SessionStatus = SessionStatus.FAILURE,
                    Errors = new List<String> { "Unable to validate the session with the database." }
                };
            }
                
            if (sessions.First().IsSessionEnded)
            {
                return new SessionResponse
                {
                    Status = StatusCode.FAIL,
                    SessionStatus = SessionStatus.PREVIOUSLY_ENDED,
                    Errors = new List<String> { "The session has already expired." }
                };
            }

            return new SessionResponse
            {
                Status = StatusCode.SUCCESS,
                Session = request.Session,
                SessionStatus = SessionStatus.ALIVE
            };
        }
        
        public SessionResponse EndSession(SessionRequest request)
        {
            var output = new SessionResponse()
            {
                Session = request.Session
            };

            // First validate the session to check the status.
            var validation = ValidateSession(request);
            if (validation.SessionStatus == SessionStatus.FAILURE)
                return validation;

            // Now check to see if the session has already been ended.
            if (validation.SessionStatus == SessionStatus.ENDED_SUCCESSFULLY)
                return validation;

            // The session is active.  First we need to call RAAM and log out.
            WSFederatedAuthentication.SignOutUsingRichClient();
            if (WSFederatedAuthentication.IsAuthenticated)
            {
                output.Errors.Add("Session is Active, but could not log out of RAAM.");
                output.SessionStatus = validation.SessionStatus;
                output.Status = StatusCode.FAIL;
                return output;
            }

            // Logged out of RAAM, let's update our Session in the Session table.
            if (!_sessionRepository.EndSession(request.Session))
            {
                output.Errors.Add("Could not end the Session in the Database.");
                output.SessionStatus = validation.SessionStatus;
                output.Status = StatusCode.FAIL;
                return output;
            }

            // And we're done!
            output.SessionStatus = SessionStatus.ENDED_SUCCESSFULLY;
            output.Status = StatusCode.SUCCESS;

            return output;
        }

        protected override bool ValidateSID(out int userID)
        {
            throw new NotImplementedException();
        }


        public ExternalLogonResponse PermissionExternalLogon(PermissionLogonRequest request)
        {
            var output = new ExternalLogonResponse();

            // The username/password are expected to be in the clear (not encrypted).
            var username = request.Username;
            var password = request.Password;

            string configLocation = ConfigHelpers.GetAppSetting("WCFConfigLocation", null);

            if (string.IsNullOrWhiteSpace(configLocation))
                configLocation = null;

            // First, authenticate with RAAM.
            var callsuccess = WSFederatedAuthentication.AuthenticateUsingRichClient( _configuration.ApplicationURI, request.Entity, username, password, configLocation);

            if (!callsuccess)
            {
                output.Errors.Add("Call Failed to authenticate with RAAM. " 
                    + " ApplicationURI='" + _configuration.ApplicationURI + "'"
                    + " Entity='" + request.Entity + "'"
                    + " UserLogon='" + username + "'");               
                output.Status = StatusCode.FAIL;
                return output;
            }
            
            var raamsuccess = WSFederatedAuthentication.IsAuthenticated;


            //WSFederatedAuthentication.CurrentPrincipal.ApplicationContextSwitch(_configuration.ApplicationURI);

            // Kick out early if we failed RAAM.
            if (!raamsuccess)
            {
                output.Errors.Add("authentication with RAAM failed. "
                    + " ApplicationURI='" + _configuration.ApplicationURI + "'"
                    + " Entity='" + request.Entity + "'"
                    + " UserLogon='" + username + "'");
                output.Status = StatusCode.FAIL;
                WSFederatedAuthentication.SignOutUsingRichClient();
                return output;
            }

            //OnLogEvent("External Logon ServiceContext Claims: \r\n" + _ServiceContext.RAAAMClaims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);

            //OnLogEvent("External Logon claims", this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
            //var claims = WSFederatedAuthentication.ClaimsAccess.GetClaims();
           // OnLogEvent("External Logon WSFederatedAuthentication.ClaimsAccess.GetClaims(): \r\n" + claims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
            
            // Session Work
            var claim = WSFederatedAuthentication.ClaimsAccess.GetClaim(WfsClaimTypes.Sid);

            // Kick out early if we cannot find our SID from the claims.
            if (claim == null || string.IsNullOrWhiteSpace(claim.Value))
            {
                output.Errors.Add("Authenticated Successfully with RAAM, but could not find the SID from the Claims.");
                output.Status = StatusCode.FAIL;
                WSFederatedAuthentication.SignOutUsingRichClient();
                return output;
            }

            Guid sesID;
            var token = WSFederatedAuthentication.SessionAuthentication.GetToken;
            EstablishSessionResponse resp = SessionMaintenanceServiceClient.EstablishSession();
            if (resp.Status == R360Shared.StatusCode.SUCCESS)
            {
                sesID = resp.Data.Session;
            }
            else
            {
                output.Errors.Add("Authenticated Successfully with RAAM, but could not register the Session with the Database.");
                output.Status = StatusCode.FAIL;
                WSFederatedAuthentication.SignOutUsingRichClient();
                return output;
            }

            // Validate the Permission against RAAM, kick out early if we do not have permission.
            if (!string.IsNullOrWhiteSpace(request.Permission) && !R360Permissions.Current.Allowed(request.Permission, request.PermissionType))
            {
                output.Errors.Add("Authenticated Successfully with RAAM, but the user does not have access to the requested permission.");
                output.Status = StatusCode.FAIL;
                WSFederatedAuthentication.SignOutUsingRichClient();
                return output;
            }

            // Kick out early if we could not grab the session from the database, for some reason.
            var sessions = _sessionRepository.GetSession(sesID).GetResult(new List<SessionDto>());
            if (!sessions.Any())
            {
                output.Errors.Add("Authenticated Successfully with RAAM, but could not register the Session with the Database.");
                output.Status = StatusCode.FAIL;
                WSFederatedAuthentication.SignOutUsingRichClient();
                return output;
            };

            // Hey we're finally done!
            output.UserId = sessions.First().UserId;
            output.Session = sesID;
            output.Status = StatusCode.SUCCESS;

            var claims2 = WSFederatedAuthentication.ClaimsAccess.GetClaims();
            if (claims2 != null && claims2.Any())
            {
                //OnLogEvent("External Logon WSFederatedAuthentication.ClaimsAccess.GetClaims(): \r\n" + claims2.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
            }
            else
            {
                OnLogEvent("No claims found", this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
            }
            return output;
        }


        public WFS.RecHub.ExternalLogon.Common.PingResponse Ping()
        {
            var output = new WFS.RecHub.ExternalLogon.Common.PingResponse()
            {
                responseString = "Pong"
            };
            return output;
        }

        #region IDisposable Implementation
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }
        #endregion
    }
}
