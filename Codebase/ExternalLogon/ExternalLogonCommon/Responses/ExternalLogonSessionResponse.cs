﻿using System;
using WFS.RecHub.R360Shared;
using System.Runtime.Serialization;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: Just a POCO model to contain the response data for session calls. 
*
* Modification History
* WI 155343 BLR 07/31/2014
*   Initial 
* ******************************************************************************/

namespace WFS.RecHub.ExternalLogon.Common
{
    [DataContract]
    public class ExternalLogonSessionResponse : BaseResponse
    {
        [DataMember]
        public Guid Session { get; set; }

        [DataMember]
        public SessionStatus SessionStatus { get; set; }
    }
}
