﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;
using System.Runtime.Serialization;

namespace WFS.RecHub.ExternalLogon.Common
{
    [DataContract]
    public class PingResponse : BaseResponse
    {
        public PingResponse()
            : base()
        { }

        [DataMember]
        public string responseString { get; set; }
    }
}
