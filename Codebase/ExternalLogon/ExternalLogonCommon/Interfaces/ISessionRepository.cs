﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ExternalLogon.Common.DTO;

namespace WFS.RecHub.ExternalLogon.Common.Interfaces
{
    public interface ISessionRepository
    {
        PossibleResult<IEnumerable<SessionDto>> GetSession(Guid sessionId);
        bool EndSession(Guid sessionId);
    }
}
