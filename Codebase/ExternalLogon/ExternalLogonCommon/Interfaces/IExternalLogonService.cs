﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: Just a small interface for the service implementation. 
*
* Modification History
* WI 155343 BLR 07/31/2014
*   Initial 
* WI 157583 BLR 08/08/2014
*   Added the permission login. 
* ******************************************************************************/

namespace WFS.RecHub.ExternalLogon.Common
{
     [ServiceContract(Namespace = "urn:wausaufs.com:services:ExternalLogonService")]
    public interface IExternalLogonService
    {
        [OperationContract]
         ExternalLogonResponse ExternalLogon(ExternalLogonRequest request);

        //[OperationContract]
        //ExternalLogonResponse ExternalLogin(PermissionLogonRequest request);

        [OperationContract]
        ExternalLogonResponse PermissionExternalLogon(PermissionLogonRequest request);


        [OperationContract]
        SessionResponse ValidateSession(SessionRequest request);

        [OperationContract]
        SessionResponse EndSession(SessionRequest request);

        [OperationContract]
        PingResponse Ping();
    }
}
