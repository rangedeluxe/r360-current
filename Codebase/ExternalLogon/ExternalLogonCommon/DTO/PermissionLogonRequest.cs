﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 08/08/2014
*
* Purpose: Just a status to contain the response status data session calls. 
*
* Modification History
* WI 157583 BLR 08/08/2014
*  - Initial
* WI 157573 BLR 08/11/2014
*  - Removed application from the request, this exists on the server side now.
* WI 167783 BLR 09/24/2014
*  - Added PermissionTypes to the Permission Requests. 
* ******************************************************************************/

namespace WFS.RecHub.ExternalLogon.Common
{
    [DataContract]
    public class PermissionLogonRequest
    {
        [DataMember]
        public string Permission { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Entity { get; set; }

        [DataMember]
        public string IPAddress { get; set; }

        [DataMember]
        public R360Permissions.ActionType PermissionType { get; set; }
    }
}
