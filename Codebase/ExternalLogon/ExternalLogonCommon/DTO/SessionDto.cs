﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.ExternalLogon.Common.DTO
{
    public class SessionDto
    {
        public Guid SessionId { get; set; }
        public int UserId { get; set; }
        public bool IsSessionEnded { get; set; }
    }
}
