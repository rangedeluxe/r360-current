﻿//using ExternalLogon.Objects;
//using ExternalLogon.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WFS.RecHub.ExternalLogon.Common;

namespace ExternalLogon.Tests
{
    [TestClass]
    public class ExternalLogonTests
    {
        private readonly IExternalLogonService _logonservice;

        public ExternalLogonTests()
        {
            _logonservice = new RAAMExternalLogonService(
                new WFS.RecHub.R360Shared.ConfigHelpers.Logger(
                    x => Console.WriteLine(x),
                    x => Console.WriteLine(x)));
        }

        [TestMethod]
        public void TestValidLogonRAAM()
        {
            // Arrange
            var request = new ExternalLogonRequest()
            {
                Username = "wadama",
                Password = "Wausau#1",
                Application = "Raam",
                Entity = "wfs"
            };

            // Act
            var result = _logonservice.ExternalLogon(request);

            // Assert
            Assert.IsTrue(result.Status);
        }
    }
}
