param(
	[string]$iteration = "",
	[string]$area = "",
	[string]$buildNumber = ""
)

#Shell to Common R360Services Deployment Script, with correct parameters
$thisScript = Split-Path $MyInvocation.MyCommand.Path 
$commonScript = Join-Path $thisScript "..\..\R360Services\ServiceShares\R360BuildDeployment.ps1"
Write-Host "External Logon:  Script: " $commonScript

$commonScript = [System.IO.Path]::GetFullPath($commonScript)

$commandWithParameters = "$commonScript" `
    + " -service RecHub.ExternalLogonService" `
    + " -appConfig:web.config" `
		+ " -configSubFolder:_PublishedWebsites\ExternalLogonService" `
    + " -folderName:RecHubExternalLogonService" `
	+ " -buildNumber:$buildNumber" `
	+ " -iteration:`"$iteration`"" `
	+ " -area:`"$area`"" `
    + " -dlls:`"" `
		+ "DALBase.dll," `
	    + "ExternalLogonAPI.dll," `
		+ "ExternalLogonCommon.dll," `
		+ "ExternalLogonService.dll," `
	    + "ipoCrypto.dll," `
	    + "ipoDB.dll," `
	    + "ipoLib.dll," `
	    + "ipoLog.dll," `
	    + "ltaCommon.dll," `
	    + "ltaLog.dll," `
		+ "Newtonsoft.Json.dll," `
	    + "R360Shared.dll," `
	    + "SessionDAL.dll," `
		+ "SessionMainteanceCommon.dll," `
		+ "SessionMaintenanceServiceClient.dll," `
		+ "Thinktecture.IdentityModel.dll," `
	    + "Wfs.Raam.Core.dll," `
		+ "Wfs.Raam.Service.Authorization.Contracts.dll," `
		+ "WFS.RecHub.ApplicationBlocks.Common.dll," `
		+ "WFS.System.Logging.dll" `
        + "`"" `
    + " -web" `

Write-Host "Invoking Deployment Script with parameters: " $commandWithParameters

invoke-expression $commandWithParameters

Write-Host "Deployment Complete"

