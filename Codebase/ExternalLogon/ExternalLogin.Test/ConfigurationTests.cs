﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.ExternalLogon.API;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: The main implementation for the Authentication Service. All of the
*          logic is stored here. 
*
* Modification History
* WI 157573 BLR 08/11/2014
*  - Initial
* ******************************************************************************/

namespace WFS.RecHub.ExternalLogin.Core.Test
{

    [TestClass]
    public class ConfigurationTests
    {
        private readonly IConfigurationProvider _configurationProvider;
        private ExternalLogonConfiguration _config;

        public ConfigurationTests()
        {
            _configurationProvider = new MockConfigurationProvider();
        }

        [TestMethod]
        public void TestValidConfigurationSettings()
        {
            // Arrange
            _configurationProvider.SetSetting("ApplicationURI", @"https://tempuri.org/FunApplicationName");
            _config = new ExternalLogonConfiguration(_configurationProvider);

            // Act
            try
            {
                // Assert nothing thrown
                _config.ValidateSettings();
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void TestInvalidConfigurationSettings()
        {
            // Arrange
            _configurationProvider.SetSetting("ApplicationURI", @"notavaliduri");
            _config = new ExternalLogonConfiguration(_configurationProvider);

            // Act
            try
            {
                // Assert an exception.
                _config.ValidateSettings();
                Assert.Fail();
            }
            catch
            {
                Assert.IsTrue(true);
            }

        }

        [TestMethod]
        public void TestNonExistentConfigurationSettings()
        {
            // Arrange
            _config = new ExternalLogonConfiguration(_configurationProvider);

            // Act
            try
            {
                // Assert an exception.
                _config.ValidateSettings();
                Assert.Fail();
            }
            catch
            {
                Assert.IsTrue(true);
            }

        }
    }
}
