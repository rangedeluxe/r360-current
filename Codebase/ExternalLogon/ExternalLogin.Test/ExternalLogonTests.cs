﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Security.Claims;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.ExternalLogon.API;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: Just contains some simple tests on the 'Core' or 'Backend' 
*          perspective.
*
* Modification History
* WI 155343 BLR 07/31/2014
*  - Initial 
* WI 157573 BLR 08/11/2014
*  - Removed Application, now exists on the server side.
* WI 165968 TWE 09/16/2014
*  - modified code to take advantage of latest drop (1.9) of RAAM
* ******************************************************************************/

namespace WFS.RecHub.ExternalLogin.Test
{
    [TestClass]
    public class ExternalLoginTests
    {
        private readonly RAAMExternalLogon _loginservice;

        public ExternalLoginTests()
        {
            //ClaimsIdentity
            ClaimsIdentity claimsIdentity = Wfs.Raam.Core.Services.WSFederatedAuthentication.CurrentPrincipal.PrimaryIdentity;

            R360Shared.R360ServiceContext.Init();
            _loginservice = new RAAMExternalLogon(
                new R360Shared.ConfigHelpers.Logger(x => Console.WriteLine(x), x => Console.WriteLine(x)), 
                R360Shared.R360ServiceContext.Current);

            ClaimsIdentity claimsIdentity2 = Wfs.Raam.Core.Services.WSFederatedAuthentication.CurrentPrincipal.PrimaryIdentity;

        }

        [TestMethod, Ignore]
        public void TestValidLoginRAAM()
        {
            // Arrange
            var request = new ExternalLogonRequest()
            {
                //OLFuser  Wausau#1
                Username = "lRntgiRWB9w=",
                Password = "T9AyWrk1+zU4gktpQh6viw==",
                Entity = "WFS",
                IPAddress = "1.1.1.1"
            };

            // Act
            var result = _loginservice.ExternalLogon(request);

            // Assert
            Assert.IsTrue(result.Status == R360Shared.StatusCode.SUCCESS);

            // Cleanup
            _loginservice.EndSession(new SessionRequest()
            {
                Session = result.Session
            });
        }

        [TestMethod, Ignore]
        public void TestValidSystemLevelRAAM()
        {
            // Arrange
            var request = new ExternalLogonRequest()
            {
                //ASystemUser  Wausau#1
                Username = "mY844Jv7ZOyFiWj7MxPmug==",
                Password = "T9AyWrk1+zU4gktpQh6viw==",
                Entity = "",
                IPAddress = "1.1.1.1"
            };

            // Act
            var result = _loginservice.ExternalLogon(request);

            // Assert
            Assert.IsTrue(result.Status == R360Shared.StatusCode.SUCCESS);

            // Cleanup
            _loginservice.EndSession(new SessionRequest()
            {
                Session = result.Session
            });
        }

        [TestMethod, Ignore]
        public void TestValidExtractDesignManagerLogon()
        {
            // Arrange
            var request = new PermissionLogonRequest()
            {
                //bresheske  Wausau#1
                //Username = "8mXmnqTBuoAomZnKYQbVeg==",
                //temery
                Username = "xQSaXhdZjhk=",
                Password = "T9AyWrk1+zU4gktpQh6viw==",
                Entity = "WFS",
                IPAddress = "1.1.1.1",
                Permission = "ExtractWizard",
                PermissionType = R360Permissions.ActionType.View
            };

            // Act
            var result = _loginservice.PermissionExternalLogon(request);

            // Assert
            Assert.IsTrue(result.Status == R360Shared.StatusCode.SUCCESS);

            // Cleanup
            _loginservice.EndSession(new SessionRequest()
            {
                Session = result.Session
            });
        }

        [TestMethod]
        [Ignore]
        public void TestInvalidLoginRAAM()
        {
            // Arrange
            var request = new ExternalLogonRequest()
            {
                Username = "asdf",
                Password = "asdf",
                Entity = "asdf"
            };

            // Act
            var result = _loginservice.ExternalLogon(request);

            // Assert
            Assert.IsTrue(result.Status == R360Shared.StatusCode.FAIL);
        }

        [TestMethod, Ignore]
        public void TestValidSessionValidation()
        {
            // First, just set up the session.

            // Arrange
            var request = new ExternalLogonRequest()
            {
                ////R360Testing  Wausau#1
                //Username = "+0ikL8bBxWvVzglWQA1YBw==",
                //Password = "T9AyWrk1+zU4gktpQh6viw==",
                //Entity = "Dev Company",
                //IPAddress = "1.1.1.1"

                //Administrator  Wausau#1
                Username = "xnVTFZP8T9fp9oHAkj+Mkg==",
                Password = "T9AyWrk1+zU4gktpQh6viw==",
                Entity = "WFS",
                IPAddress = "1.1.1.1"
            };

            // Act
            var result = _loginservice.ExternalLogon(request);

            // Assert
            Assert.IsTrue(result.Status == R360Shared.StatusCode.SUCCESS);

            // Now we can test the session validation call.

            // Act
            var sessionresult = _loginservice.ValidateSession(new SessionRequest()
            {
                Session = result.Session
            });

            // Assert
            Assert.AreEqual(SessionStatus.ALIVE, sessionresult.SessionStatus);

            // Cleanup
            _loginservice.EndSession(new SessionRequest()
            {
                Session = result.Session
            });
        }

        public void TestValidLogout()
        {
            // First, just set up the session.

            // Arrange
            var request = new ExternalLogonRequest()
            {
                Username = "pcL0ws1PBFM=",
                Password = "J9SKZSVP0JNFuezGR8ggxg==",
                Entity = "wfs",
                IPAddress = "1.1.1.1"
            };

            // Act
            var result = _loginservice.ExternalLogon(request);

            // Assert
            Assert.IsTrue(result.Status == R360Shared.StatusCode.SUCCESS);

            // Now we can test the session logout call.

            // TODO 
        }

    }
}
