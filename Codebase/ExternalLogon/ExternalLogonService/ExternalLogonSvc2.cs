﻿using System;
using WFS.RecHub.ExternalLogin;
using WFS.RecHub.ExternalLogin.Core.Objects;
using WFS.RecHub.ExternalLogin.Core.Services;
using WFS.RecHub.ExternalLogin.Objects;
using WFS.RecHub.R360Shared;

namespace ExternalLogin.Service
{

    public class ExternalLogonSvc : LTAService, IExternalLoginService
    {

        public ExternalLoginResponse ExternalLogin(ExternalLoginRequest request)
        {
            return FailOnError<ExternalLoginResponse>((oapi) =>
            {
                var api = (WFS.RecHub.ExternalLogin.Core.Services.IExternalLoginService)oapi;
                return api.ExternalLogin(request);
            });
        }

        public string Ping()
        {
            return "Pong";
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new RAAMExternalLoginService(
                new WFS.RecHub.R360Shared.ConfigHelpers.Logger(
                    x => EventLog.logEvent(x, WFS.RecHub.Common.MessageImportance.Essential),
                    x => EventLog.logError(new Exception(x))),
                serviceContext);
        }

        public WFS.RecHub.ExternalLogin.Objects.SessionResponse ValidateSession(WFS.RecHub.ExternalLogin.Objects.SessionRequest request)
        {
            return FailOnError<SessionResponse>((oapi) =>
            {
                var api = (WFS.RecHub.ExternalLogin.Core.Services.IExternalLoginService)oapi;
                return api.ValidateSession(request);
            });
        }

        public WFS.RecHub.ExternalLogin.Objects.SessionResponse EndSession(WFS.RecHub.ExternalLogin.Objects.SessionRequest request)
        {
            return FailOnError<SessionResponse>((oapi) =>
            {
                var api = (WFS.RecHub.ExternalLogin.Core.Services.IExternalLoginService)oapi;
                return api.EndSession(request);
            });
        }


        public ExternalLoginResponse ExternalLogin(PermissionLoginRequest request)
        {
            return FailOnError<ExternalLoginResponse>((oapi) =>
            {
                var api = (WFS.RecHub.ExternalLogin.Core.Services.IExternalLoginService)oapi;
                return api.ExternalLogin(request);
            });
        }
    }
}