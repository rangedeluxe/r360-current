﻿using System;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.Common;
using WFS.RecHub.ExternalLogon.API;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: The main implementation for the Authentication Service. All of the
*          logic is stored here. 
*
* Modification History
* WI 155343 BLR 07/31/2014
*   Initial 
* ******************************************************************************/

namespace WFS.RecHub.ExternalLogonService
{
    public class ExternalLogonSvc : LTAService, IExternalLogonService
    {
        private BaseResponse LogOnError(Func<APIBase, BaseResponse> operation)
        {
            return FailOnError<BaseResponse>((oapi) =>
            {
                var response = (BaseResponse)operation(oapi);
                response.Errors.ForEach(err =>
                {
                    EventLog.logWarning(err, "External Logon",  MessageImportance.Essential);
                });
                return response;
            });
        }

        public ExternalLogonResponse ExternalLogon(ExternalLogonRequest request)
        {
            return (ExternalLogonResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.ExternalLogon(request);
            });
        }

        public WFS.RecHub.ExternalLogon.Common.PingResponse Ping()
        {
            return (PingResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.Ping();
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new RAAMExternalLogon(
                new WFS.RecHub.R360Shared.ConfigHelpers.Logger(
                    x => EventLog.logEvent(x, MessageImportance.Essential),
                    x => EventLog.logError(new Exception(x))),
                serviceContext);
        }

        public SessionResponse ValidateSession(SessionRequest request)
        {
            return (SessionResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.ValidateSession(request);
            });
        }

        public SessionResponse EndSession(SessionRequest request)
        {
            return (SessionResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.EndSession(request);
            });
        }


        public ExternalLogonResponse PermissionExternalLogon(PermissionLogonRequest request)
        {
            return (ExternalLogonResponse)LogOnError((oapi) =>
            {
                var api = (IExternalLogonService)oapi;
                return api.PermissionExternalLogon(request);
            });
        }
    }

}