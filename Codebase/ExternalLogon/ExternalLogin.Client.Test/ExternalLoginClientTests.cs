﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WFS.RecHub.ExternalLogonService;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Brandon Resheske
* Date: 07/31/2014
*
* Purpose: Contains some tests from the Client perspective. This shows how
*          the thick-clients will be utilizing the service. 
*
* Modification History
* WI 155343 BLR 07/31/2014
*  - Initial 
* WI 157573 BLR 08/11/2014
*  - Removed application from the request, this exists on the server side now.
* WI 165968 TWE 09/16/2014
*  - modified code to take advantage of latest drop (1.9) of RAAM
* WI 167783 BLR 09/24/2014
*  - Added PermissionTypes to the Permission Requests.
* ******************************************************************************/

namespace WFS.RecHub.ExternalLogin.Client.Test
{
    [TestClass]
    public class ExternalLoginClientTests
    {
        private readonly ExternalLogonClient _client;

        public ExternalLoginClientTests()
        {
            _client = new ExternalLogonClient();
        }

        [TestMethod, Ignore]
        public void TestValidExternalLogin()
        {
            // Arrange
            var request = new ExternalLogonRequest()
            {
                // //R360Testing  Wausau#1
                //Username = "+0ikL8bBxWvVzglWQA1YBw==",
                //Password = "T9AyWrk1+zU4gktpQh6viw==",
                //Entity = "Dev Company",
                //IPAddress = "1.1.1.1"

                 //Administrator  Wausau#1
                Username = "xnVTFZP8T9fp9oHAkj+Mkg==",
                Password = "T9AyWrk1+zU4gktpQh6viw==",
                Entity = "WFS",
                IPAddress = "1.1.1.1"

                //Username = "pcL0ws1PBFM=",
                //Password = "T9AyWrk1+zU4gktpQh6viw==",
                //Entity = "wfs",
                //IPAddress = "1.1.1.1"
            };

            // Act
            var result = _client.ExternalLogon(request);


            // Assert
            Assert.IsTrue(result.Status == StatusCode.SUCCESS);
            Assert.AreNotEqual(result.UserId, 0);
            Assert.AreNotEqual(result.Session, Guid.Empty);

            // Cleanup
            _client.EndSession(new SessionRequest()
            {
                Session = result.Session
            });
        }

        [TestMethod, Ignore]
        public void TestInvalidExternalLogin()
        {
            // Arrange
            var request = new ExternalLogonRequest()
            {
                Username = "sdfg",
                Password = "asdf",
                Entity = "qwer",
                IPAddress = "1.1.1.1"
            };

            // Act
            var result = _client.ExternalLogon(request);

            // Assert
            Assert.AreEqual(StatusCode.FAIL, result.Status);

            // Cleanup
            _client.EndSession(new SessionRequest()
            {
                Session = result.Session
            });
        }

        [TestMethod, Ignore]
        public void TestPing()
        {
            // Act
            var response = _client.Ping();

            // Assert
            Assert.AreEqual("Pong", response.responseString);
        }

        [TestMethod, Ignore]
        public void TestValidExtractDesignManagerLogon()
        {
            // Arrange
            var request = new PermissionLogonRequest()
            {
                //bresheske  Wausau#1
                //Username = "8mXmnqTBuoAomZnKYQbVeg==",
                //temery
                Username = "xQSaXhdZjhk=",
                Password = "T9AyWrk1+zU4gktpQh6viw==",
                Entity = "WFS",
                IPAddress = "1.1.1.1",
                Permission = "ExtractWizard",
                PermissionType = R360Permissions.ActionType.View
            };

            // Act
            var result = _client.PermissionExternalLogon(request);

            // Assert
            Assert.IsTrue(result.Status == R360Shared.StatusCode.SUCCESS);

            // Cleanup
            _client.EndSession(new SessionRequest()
            {
                Session = result.Session
            });
        }
    }
}
