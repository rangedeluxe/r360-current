﻿using Serilog;
using SysAnalysis.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebSystemAnalyzer
{
    public class Logger : SysAnalysis.Interfaces.ILogger
    {
        private Serilog.ILogger _logger;
        public Logger()
        {
            _logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();
        }
        public void LogDebug(string data)
        {
            _logger.Debug(data);
        }

        public void LogError(string data)
        {
            _logger.Error(data);
        }

        public void LogInformation(string data)
        {
            _logger.Information(data);
        }

        public void LogWarning(string data)
        {
            _logger.Warning(data);
        }
    }
}
