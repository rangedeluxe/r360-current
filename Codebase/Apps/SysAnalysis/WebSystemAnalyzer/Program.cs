﻿using SysAnalysis;
using SysAnalysis.DTOs;
using SysAnalysis.Settings;
using SysAnalysis.Systems;
using SysAnalysis.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using Serilog;

namespace WebSystemAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            var log = new Logger();

            log.LogInformation("Reading requirements file");
            
            var filecontents = File.ReadAllText(@"Resources/reqs.json");
            var sysanalizer = new WebSysAnalyzer(filecontents,
                new SupportedSystemsProvider(new RegistryProvider(), new ProcessExecutor()),
                new SupportedSettingsProvider(new RegistryProvider()),
                new SysAnalyzerComparer());

            //Print the requirements
            var reqs = sysanalizer.GetRequiredSystems();
            PrintRequirements(reqs);
            //analyze and print the results
            var results = sysanalizer.Analyze();
            PrintResults(results);
            Log.CloseAndFlush();
            Console.ReadLine();
        }
        private static void PrintRequirements(List<SysInfo> reqs)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Requirements");

            foreach (var req in reqs)
            {
                Console.Write($"{req.Name} ");
                Console.WriteLine(req.Version != null ? req.Version.ToString() : "");
            }
        }

        private static void PrintResults(IList<SysInfo> results)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
            Console.WriteLine("Analysis result:");
            foreach (var result in results)
            {
                if (result.IsMinVersionMet)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{result.Name} passed");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{result.Name} failed.");
                }

            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
