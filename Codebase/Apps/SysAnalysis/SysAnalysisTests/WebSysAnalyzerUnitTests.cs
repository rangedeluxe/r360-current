﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SysAnalysis;
using SysAnalysis.DTOs;
using SysAnalysis.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SysAnalysisTests
{
    [TestClass]
    public class WebSysAnalyzerUnitTests
    {
        private string json = File.ReadAllText(@"Resources/reqs.json");

        [TestMethod]
        public void Test_Get_Required()
        {
            var sysprovider = new Mock<ISystemProvider>();
            sysprovider.Setup(x => x.GetSupportedSystems()).Returns(new List<ISystem>
            {
               
            });
            var syscomparer = new Mock<ISysComparer>();
            syscomparer.SetupSequence(x => x.Compare(It.IsAny<SysInfo>(), It.IsAny<SysInfo>())).Returns(new ComparisonResult
            {
                Result = 0
            }).
            Returns(new ComparisonResult
            {
                Result = -1
            }).
            Returns(new ComparisonResult
            {
                Result = 0
            }).
            Returns(new ComparisonResult
            {
                Result = 0
            }).
            Returns(new ComparisonResult
            {
                Result = 0
            }).
            Returns(new ComparisonResult
            {
                Result = 0
            }).
            Returns(new ComparisonResult
            {
                Result = 0
            });
            //var analyzer = new WebSysAnalyzer(json, syscomparer.Object);
            //var response = analyzer.Analyze();

        }
    }
}
