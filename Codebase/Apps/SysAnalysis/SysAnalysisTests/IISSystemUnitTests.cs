using Microsoft.VisualStudio.TestTools.UnitTesting;
using SysAnalysis.Interfaces;
using Moq;
using SysAnalysis.Utils;
using SysAnalysis.Systems;
using System;

namespace SysAnalysisTests
{
    [TestClass]
    public class IISSystemUnitTests
    {
        private IRegistry registryprovider;
        [TestMethod]
        public void Test_IIS_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.KeyExists(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            var iis = new IIS(registry.Object);
            Assert.IsTrue(iis.IsInstalled());
        }
        [TestMethod]
        public void Test_IIS_Not_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.KeyExists(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).Returns(false);
            var iis = new IIS(registry.Object);
            Assert.IsFalse(iis.IsInstalled());
        }

 
        [TestMethod]
        public void Test_Gets_IIS_Version()
        {
            var registryProv = new Mock<IRegistry>();
            registryProv.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns((RegistryStore x, string y, string z)=> 
                {
                    if (z == "MajorVersion") return "1";
                    else if (z == "MinorVersion") return "2";
                    else return "";
                });
            var iis = new IIS(registryProv.Object);
            var iisv = iis.GetVersions()[0].Version;

            Assert.AreEqual(iisv.CompareTo(new System.Version(1, 2)), 0);

        }
        [TestMethod]
        public void Test_Gets_IIS_Version_Not_Found()
        {
            var registryProv = new Mock<IRegistry>();
            registryProv.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns("");
            var iis = new IIS(registryProv.Object);
            var iisv = iis.GetVersions()[0].Version;
            Assert.AreEqual(iisv.CompareTo(new System.Version(0, 0)), 0);

        }
    }
}
