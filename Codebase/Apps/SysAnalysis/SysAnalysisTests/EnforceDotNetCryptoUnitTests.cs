﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SysAnalysis.Interfaces;
using SysAnalysis.Settings;
using SysAnalysis.Utils;

namespace SysAnalysisTests
{
    [TestClass]
    public class EnforceDotNetCryptoUnitTests
    {
        [TestMethod]
        public void Test_Dot_Net_Crypto_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns("1");
            var url = new EnforceDotNetCrypto(registry.Object);
            Assert.IsTrue(url.IsInstalled());
        }

        [TestMethod]
        public void Test_Dot_Net_Cryto_Installed_On_32Bits_Only()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((RegistryStore store, string path, string key) =>
                {
                    if (path.Contains("Wow6432Node"))
                    {
                        return "true";
                    }

                    return "false";
                });
            var url = new EnforceDotNetCrypto(registry.Object);
            Assert.IsFalse(url.IsInstalled());
        }
        [TestMethod]
        public void Test_Dot_Net_Cryto_Installed_On_64Bits_Only()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns((RegistryStore store, string path, string key) =>
                {
                    if (path.Contains("Wow6432Node"))
                    {
                        return "false";
                    }

                    return "true";
                });
            var url = new EnforceDotNetCrypto(registry.Object);
            Assert.IsFalse(url.IsInstalled());
        }

        [TestMethod]
        public void Test_Dot_Net_Crypto_Not_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns("1");
            var url = new EnforceDotNetCrypto(registry.Object);
            Assert.IsTrue(url.IsInstalled());
        }

    }
}
