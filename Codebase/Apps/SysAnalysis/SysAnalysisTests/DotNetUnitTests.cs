﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using SysAnalysis.Interfaces;
using SysAnalysis.Systems;
using SysAnalysis.Utils;
using SysAnalysis.DTOs;

namespace SysAnalysisTests
{
    [TestClass]
    public class DotNetUnitTests
    {
        [TestMethod]
        public void Test_Dot_Net_Get_Two_Versions()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns("4.7.03190");
            var dotnet = new DotNet(registry.Object);
            var versions = dotnet.GetVersions();
            Assert.AreEqual(2, versions.Count);
        }
        [TestMethod]
        public void Test_Dot_Net_Get_No_Versions()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns("");
            var dotnet = new DotNet(registry.Object);
            var versions = dotnet.GetVersions();
            Assert.AreEqual(0, versions.Count);
        }
        [TestMethod]
        public void Test_Dot_Net_Get_One_Versions()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns((RegistryStore store, string path, string key)=>
                {
                    if(path.Contains("Client"))
                    {
                        return "4.7.03190";
                    }
                    return "";
                });
            var dotnet = new DotNet(registry.Object);
            var versions = dotnet.GetVersions();
            Assert.AreEqual(1, versions.Count);
        }
        [TestMethod]
        public void Test_Dot_Net_Has_MultipleVersions()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns("4.7.03190");
            var dotnet = new DotNet(registry.Object);
            Assert.IsTrue(dotnet.HasMultipleVersions());            
        }

        [TestMethod]
        public void Test_Dot_Net_Does_Not_Have_MultipleVersions()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns("");
            var dotnet = new DotNet(registry.Object);
            Assert.IsFalse(dotnet.HasMultipleVersions());
        }

        [TestMethod]
        public void Test_Dot_Net_Is_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns("4.7.03190");
            var dotnet = new DotNet(registry.Object);
            Assert.IsTrue(dotnet.IsInstalled());
        }
        [TestMethod]
        public void Test_Dot_Net_Is_Not_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns("");
            var dotnet = new DotNet(registry.Object);
            Assert.IsFalse(dotnet.IsInstalled());
        }
    }
}
