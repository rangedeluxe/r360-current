﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SysAnalysis.Interfaces;
using SysAnalysis.Settings;
using SysAnalysis.Utils;

namespace SysAnalysisTests
{
    [TestClass]
    public class UrlRewriteUnitTests
    {
        [TestMethod]
        public void Test_UrlReWrite_Is_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns("1");
            var rewrite = new UrlRewrite(registry.Object);
            Assert.IsTrue(rewrite.IsInstalled());
        }

        [TestMethod]
        public void Test_UrlReWrite_Is_Not_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns("");
            var rewrite = new UrlRewrite(registry.Object);
            Assert.IsFalse(rewrite.IsInstalled());
        }
    }
}
