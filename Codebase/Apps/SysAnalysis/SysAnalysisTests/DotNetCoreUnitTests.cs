﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SysAnalysis.DTOs;
using SysAnalysis.Interfaces;
using SysAnalysis.Systems;
using SysAnalysis.Utils;

namespace SysAnalysisTests
{

    [TestClass]
    public class DotNetCoreUnitTests
    {
        private const string DOTNETVERSIONS =
            @"Microsoft.AspNetCore.All 2.1.7 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.All]
                          Microsoft.AspNetCore.All 2.1.8 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.All]
                          Microsoft.AspNetCore.All 2.1.9 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.All]
                          Microsoft.AspNetCore.All 2.2.2 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.All]
                          Microsoft.AspNetCore.All 2.2.4 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.All]
                          Microsoft.AspNetCore.App 2.1.7 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
                          Microsoft.AspNetCore.App 2.1.8 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
                          Microsoft.AspNetCore.App 2.1.9 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
                          Microsoft.AspNetCore.App 2.2.2 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
                          Microsoft.AspNetCore.App 2.2.4 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
                          Microsoft.NETCore.App 2.1.7 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
                          Microsoft.NETCore.App 2.1.8 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
                          Microsoft.NETCore.App 2.1.9 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
                          Microsoft.NETCore.App 2.2.2 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
                          Microsoft.NETCore.App 2.2.4 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]";

        [TestMethod]
        public void Test_Dot_Net_Core_Versions()
        {
            var mockProcess = new Mock<IProcess>();
            mockProcess.Setup(x => x.Start(It.IsAny<string>(), It.IsAny<string>())).Returns(
                (string command, string arguments) =>
                {
                    if (arguments.Contains("info"))
                    {
                        return new ProcessInfo
                        {

                            Output = @".NET Core SDK (reflecting any global.json):
                            Version:   2.2.203
                            Commit:    e5bab63eca",
                            ProcessStarted = true
                        };
                    }
                    return new ProcessInfo
                    {
                        ProcessStarted = true,
                        Output = DOTNETVERSIONS
                    };
                });
            var dotnet = new DotNetCore(mockProcess.Object);
            var version = dotnet.GetVersions();
            Assert.AreEqual(6, version.Count);
        }

        [TestMethod]
        public void Test_Dot_Net_Core_No_Versions()
        {
            var mockProcess = new Mock<IProcess>();
            mockProcess.Setup(x => x.Start(It.IsAny<string>(), It.IsAny<string>())).Returns(new ProcessInfo
            {
                Output = "",
                ProcessStarted = true
            });
            var dotnet = new DotNetCore(mockProcess.Object);
            var version = dotnet.GetVersions();
            Assert.AreEqual(0, version.Count);
        }

        [TestMethod]
        public void Test_Dot_Net_Core_Command_Not_Found()
        {
            var mockProcess = new Mock<IProcess>();
            mockProcess.Setup(x => x.Start(It.IsAny<string>(), It.IsAny<string>())).Returns(new ProcessInfo
            {
                Output = @"'dotnet' is not recognized as an internal or external command,
                            operable program or batch file.",
                ProcessStarted = true
            });
            var dotnet = new DotNetCore(mockProcess.Object);
            var version = dotnet.GetVersions();
            Assert.AreEqual(0, version.Count);
        }

        [TestMethod]
        public void Test_Dot_Net_Core_Process_Not_Found()
        {
            var mockProcess = new Mock<IProcess>();
            mockProcess.Setup(x => x.Start(It.IsAny<string>(), It.IsAny<string>())).Returns(new ProcessInfo
            {
                Output = "",
                ProcessStarted = false
            });
            var dotnet = new DotNetCore(mockProcess.Object);
            var version = dotnet.GetVersions();
            Assert.AreEqual(0, version.Count);
        }

        [TestMethod]
        public void Test_Dot_Net_Core_Has_Multiple_Versions()
        {
            var mockProcess = new Mock<IProcess>();
            mockProcess.Setup(x => x.Start(It.IsAny<string>(), It.IsAny<string>())).Returns(
                (string command, string arguments) =>
                {
                    if (arguments.Contains("info"))
                    {
                        return new ProcessInfo
                        {

                            Output = @".NET Core SDK (reflecting any global.json):
                            Version:   2.2.203
                            Commit:    e5bab63eca",
                            ProcessStarted = true
                        };
                    }
                    return new ProcessInfo
                    {
                        ProcessStarted = true,
                        Output = DOTNETVERSIONS
                    };
                });
            var dotnet = new DotNetCore(mockProcess.Object);
            Assert.IsTrue(dotnet.HasMultipleVersions());
        }

        [TestMethod]
        public void Test_Dot_Net_Core_Has_One_Version()
        {
            var mockProcess = new Mock<IProcess>();
            mockProcess.Setup(x => x.Start(It.IsAny<string>(), It.IsAny<string>())).Returns(
                (string command, string arguments) =>
                {
                    if (arguments.Contains("info"))
                    {
                        return new ProcessInfo
                        {

                            Output = @"
Microsoft .NET Core Shared Framework Host

  Version  : 2.0.7
  Build    : 2d61d0b043915bc948ebf98836fefe9ba942be11",
                            ProcessStarted = true
                        };
                    }
                    return new ProcessInfo
                    {
                        ProcessStarted = true,
                        Output = @"'dotnet' is not recognized as an internal or external command,
                            operable program or batch file."
                    };
                });
            var dotnet = new DotNetCore(mockProcess.Object);
            var dotnetinfo = dotnet.GetVersions()[0];
            Assert.IsTrue(new Version("2.0.7").Equals(dotnetinfo.Version));
        }

        [TestMethod]
        public void Test_Dot_Net_Core_Is_Installed()
        {
            var mockProcess = new Mock<IProcess>();
            mockProcess.Setup(x => x.Start(It.IsAny<string>(), It.IsAny<string>())).Returns(new ProcessInfo
            {
                Output = @".NET Core SDK (reflecting any global.json):
                            Version:   2.2.203
                            Commit:    e5bab63eca",
                ProcessStarted = true
            });
            var dotnet = new DotNetCore(mockProcess.Object);
            Assert.IsTrue(dotnet.IsInstalled());
        }

        [TestMethod]
        public void Test_Dot_Net_Core_Is_Not_Installed()
        {
            var mockProcess = new Mock<IProcess>();
            mockProcess.Setup(x => x.Start(It.IsAny<string>(), It.IsAny<string>())).Returns(new ProcessInfo
            {
                Output = @"'dotnet' is not recognized as an internal or external command,
                            operable program or batch file.",
                ProcessStarted = true
            });
            var dotnet = new DotNetCore(mockProcess.Object);
            Assert.IsFalse(dotnet.IsInstalled());
        }
    }
}
