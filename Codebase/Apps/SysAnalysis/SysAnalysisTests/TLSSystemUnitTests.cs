﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SysAnalysis.Interfaces;
using SysAnalysis.Systems;
using SysAnalysis.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysisTests
{
    [TestClass]
    public class TLSSystemUnitTests
    {
        [TestMethod]
        public void Test_TLS_Name()
        {
            var registry = new Mock<IRegistry>();
            //registry.Setup(x => x.KeyExists(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            var tls = new TLS(registry.Object);
            Assert.AreEqual("TLS", tls.GetName());
        }

        [TestMethod]
        public void Test_TLS_Is_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetAllSubKeys(It.IsAny<RegistryStore>(), It.IsAny<string>())).Returns(new List<string> {
                "TLS1.0",
                "TLS1.1",
                "TLS1.2"
            });

            var tls = new TLS(registry.Object);
            Assert.IsTrue(tls.IsInstalled());
        }

        [TestMethod]
        public void Test_TLS_Is_Not_Installed()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetAllSubKeys(It.IsAny<RegistryStore>(), It.IsAny<string>())).Returns(new List<string>());
            var tls = new TLS(registry.Object);
            Assert.IsFalse(tls.IsInstalled());
        }

        [TestMethod]
        public void Test_TLS_Found_With_No_Versions()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetAllSubKeys(It.IsAny<RegistryStore>(), It.IsAny<string>())).Returns(new List<string> {
                "TLS1.0",
                "TLS1.1",
                "TLS1.2"
            });

            var tls = new TLS(registry.Object);
            var tlsversions = tls.GetVersions();

            Assert.AreEqual(0, tlsversions.Count);

        }

        [TestMethod]
        public void Test_TLS_Found_With_Versions()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetAllSubKeys(It.IsAny<RegistryStore>(), It.IsAny<string>())).Returns(new List<string> {
                "TLS1.0",
                "TLS1.1",
                "TLS1.2"
            });

            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns((RegistryStore s, string path, string key) =>
                {
                    if (path.Contains("Server")) { return "1"; }
                    else return "";
                });
            var tls = new TLS(registry.Object);
            var tlsversions = tls.GetVersions();
            Assert.AreEqual(3, tlsversions.Count);
        }

        [TestMethod]
        public void Test_TLS_Has_Multiple_Versions()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetAllSubKeys(It.IsAny<RegistryStore>(), It.IsAny<string>())).Returns(new List<string> {
                "TLS1.0",
                "TLS1.1",
                "TLS1.2"
            });

            var tls = new TLS(registry.Object);
            Assert.IsTrue(tls.HasMultipleVersions());

        }
        [TestMethod]
        public void Test_TLS_Does_Not_Have_Multiple_Versions()
        {
            var registry = new Mock<IRegistry>();
            var tls = new TLS(registry.Object);
            registry.Setup(x => x.GetAllSubKeys(It.IsAny<RegistryStore>(), It.IsAny<string>())).Returns(new List<string>());
            Assert.IsFalse(tls.HasMultipleVersions());

        }

    }
}
