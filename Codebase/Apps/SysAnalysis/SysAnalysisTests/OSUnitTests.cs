﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SysAnalysis.Interfaces;
using SysAnalysis.Systems;
using SysAnalysis.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysisTests
{
    [TestClass]
    public class OSUnitTests
    {
        [TestMethod]
        public void Test_OS_Version_With_Major_And_Minor_Keys()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).Returns( 
                (RegistryStore store, string path, string key) =>
            {
                if (key.Contains("CurrentMajorVersionNumber"))
                {
                    return "10";
                }
                else if (key.Contains("CurrentMinorVersionNumber"))
                {
                    return "0";
                }
                return "";
            });
            var os = new OS(registry.Object);
            var osversion = os.GetVersions()[0];
            Assert.AreEqual("10.0", osversion.Version.ToString());
        }
        [TestMethod]
        public void Test_OS_Version_With_Version_Key()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).Returns(
                (RegistryStore store, string path, string key) =>
                {
                    if (key.Contains("CurrentMajorVersionNumber") || key.Contains("CurrentMinorVersionNumber"))
                    {
                        return "";
                    }

                    return "6.3";
                });
            var os = new OS(registry.Object);
            var osversion = os.GetVersions()[0];
            Assert.AreEqual("6.3", osversion.Version.ToString());
        }

        [TestMethod]
        public void Test_OS_Version_When_None_Found()
        {
            var registry = new Mock<IRegistry>();
            registry.Setup(x => x.GetValue(It.IsAny<RegistryStore>(), It.IsAny<string>(), It.IsAny<string>())).Returns("");
            var os = new OS(registry.Object);
            var osversion = os.GetVersions();
            Assert.AreEqual(0, osversion.Count);
        }


    }
}
