﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using SysAnalysis.Utils;
using SysAnalysis.DTOs;

namespace SysAnalysisTests
{
    [TestClass]
    public class SysComparerUnitTests
    {
        [TestMethod]
        public void Test_Compare_Systems_Are_Equal()
        {
            var comparer = new SysAnalyzerComparer();
            var sysa = new SysInfo
            {
                Name = "sysa",
                Version = new Version("2.0.6")
            };
            var sysb = new SysInfo
            {
                Name = "sysb",
                Version = new Version("2.0.6")
            };

            var result = comparer.Compare(sysa, sysb);
            Assert.AreEqual(0, result.Result);
        }

        [TestMethod]
        public void Test_Compare_Systems_A_Greater()
        {
            var comparer = new SysAnalyzerComparer();
            var sysa = new SysInfo
            {
                Name = "sysa",
                Version = new Version("2.0")
            };
            var sysb = new SysInfo
            {
                Name = "sysb",
                Version = new Version("1.0")
            };

            var result = comparer.Compare(sysa, sysb);
            Assert.AreEqual(1, result.Result);
        }
        [TestMethod]
        public void Test_Compare_Systems_B_Greater()
        {
            var comparer = new SysAnalyzerComparer();
            var sysa = new SysInfo
            {
                Name = "sysa",
                Version = new Version("2.0")
            };
            var sysb = new SysInfo
            {
                Name = "sysb",
                Version = new Version("3.0")
            };

            var result = comparer.Compare(sysa, sysb);
            Assert.AreEqual(-1, result.Result);
        }
    }
}
