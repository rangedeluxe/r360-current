﻿using SysAnalysis.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.Interfaces
{
    public interface ISystem
    {
        bool IsInstalled();
        string GetName();
        bool HasMultipleVersions();
        IList<SysInfo> GetVersions();

    }
}
