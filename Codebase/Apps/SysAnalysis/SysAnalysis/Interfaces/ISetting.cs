﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.Interfaces
{
    public interface ISetting
    {
	    string GetName();
	    bool IsInstalled();
	 }
}
