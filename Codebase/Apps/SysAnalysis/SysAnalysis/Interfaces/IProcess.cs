﻿using SysAnalysis.DTOs;

namespace SysAnalysis.Interfaces
{
    public interface IProcess
    {
        ProcessInfo Start(string command, string arguments);
    }
}
