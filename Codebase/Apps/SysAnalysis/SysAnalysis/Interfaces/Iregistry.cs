﻿using SysAnalysis.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.Interfaces
{
    public interface IRegistry
    {
        bool KeyExists(RegistryStore store, string path, string key);
        IList<string> GetAllSubKeys(RegistryStore store, string path);
        string GetValue(RegistryStore store, string path, string key);
    }
}
