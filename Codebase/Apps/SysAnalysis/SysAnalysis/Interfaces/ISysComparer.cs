﻿using SysAnalysis.DTOs;

namespace SysAnalysis
{
    public interface ISysComparer
    {
        ComparisonResult Compare(SysInfo systemA, SysInfo systemB);
    }
}