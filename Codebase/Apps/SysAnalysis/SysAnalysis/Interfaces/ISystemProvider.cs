﻿using SysAnalysis.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.Interfaces
{
    public interface ISystemProvider
    {
        IList<ISystem> GetSupportedSystems();
    }
}
