﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.Interfaces
{
    public interface ILogger
    {
        void LogInformation(string data);
        void LogDebug(string data);
        void LogWarning(string data);
        void LogError(string data);
    }
}
