﻿using Newtonsoft.Json;
using SysAnalysis.DTOs;
using SysAnalysis.Interfaces;
using SysAnalysis.Settings;
using SysAnalysis.Systems;
using SysAnalysis.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SysAnalysis
{
    public class WebSysAnalyzer
    {
        private string _minReqs;
        private ISystemProvider _systemProvider;
        private ISysComparer _syscomparer;
        ISettingProvider _settingsProvider;
        private List<SysInfo> userReqs;
        public WebSysAnalyzer(string reqsjson, ISystemProvider systemsProvider, ISettingProvider settingsProvider, ISysComparer comparer)
        {
            _minReqs = reqsjson;
            _systemProvider = systemsProvider;
            _settingsProvider = settingsProvider;
            _syscomparer = comparer;
            userReqs = JsonConvert.DeserializeObject<List<SysInfo>>(_minReqs);

        }
        public List<SysInfo> GetRequiredSystems()
        {
            return userReqs;
        }

        public IList<SysInfo> Analyze()
        {
            var registry = new RegistryProvider();

            var result = new List<SysInfo>();
            //list of all the systems we are checking for 
            var sys = _systemProvider.GetSupportedSystems();
            //list of all the settings we are checking for
            var settings = _settingsProvider.GetSupportedSettings();

            //we go through all the requirements in the user file and compare with our systems and settings            
            foreach (var us in userReqs)
            {
                foreach (var sy in sys)
                {
                    var curName = sy.GetName();
                    if (us.Name.Equals(curName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var latestVersion = sy.GetVersions().FirstOrDefault();
                        if (latestVersion == null || us.Version == null)
                        {
                            result.Add(new SysInfo
                            {
                                IsInstalled = false,
                                IsSetting = false,
                                Name = curName

                            });
                        }
                        else
                        {
                            var compresult = _syscomparer.Compare(latestVersion, us);
                            result.Add(new SysInfo
                            {
                                Name = curName,
                                IsInstalled = true,
                                IsSetting = false,
                                IsMinVersionMet = compresult.Result >= 0
                            });
                        }
                    }
                }

                foreach (var setting in settings)
                {
                    var settingName = setting.GetName();
                    if (us.Name.Equals(settingName))
                    {
                        var setres = setting.IsInstalled();
                        result.Add(new SysInfo
                        {
                            Name = settingName,
                            IsSetting = true,
                            IsInstalled = setres,
                            IsMinVersionMet = setres
                        });
                    }
                }
            }
            return result;
        }

    }
}
