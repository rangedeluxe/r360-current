﻿using System.Collections.Generic;
using Microsoft.Win32;
using SysAnalysis.Interfaces;


namespace SysAnalysis.Utils
{
    public enum RegistryStore
    {
        LocalMachine = 0,
        Users,
        CurrentConfig,
        CurrentUsers,
        ClassesRoot
    }
    public class RegistryProvider : IRegistry
    {
        public IList<string> GetAllSubKeys(RegistryStore store, string path)
        {
            var regstore = GetStore(store);
            try
            {
                var keys = regstore.OpenSubKey(path);
                var subkeys = keys.GetSubKeyNames();
                return new List<string>(subkeys);
            }
            catch
            {
                return new List<string>();
            }
            finally { if (regstore != null) { regstore.Dispose(); } }
        }

        public string GetValue(RegistryStore store, string path, string key)
        {
            var reg = GetStore(store);
            try
            {
                var sub = reg.OpenSubKey(path);
                var revalue = sub.GetValue(key);
                return revalue.ToString();
            }
            catch
            {
                return "";
            }
            finally { if (reg != null) reg.Dispose(); }
        }

        public bool KeyExists(RegistryStore store, string path, string key)
        {
            var reg = GetStore(store);
            try
            {
                var sub = reg.OpenSubKey(path);
                return sub.GetValue(key) != null;
            }
            catch
            {
                return false;
            }
            finally { if (reg != null) reg.Dispose(); }
        }

        private RegistryKey GetStore(RegistryStore store)
        {
            if (store == RegistryStore.LocalMachine) return Registry.LocalMachine;
            else if (store == RegistryStore.Users) return Registry.Users;
            else if (store == RegistryStore.CurrentConfig) return Registry.CurrentConfig;
            else if (store == RegistryStore.ClassesRoot) return Registry.ClassesRoot;
            else if (store == RegistryStore.CurrentUsers) return Registry.CurrentUser;
            return Registry.LocalMachine;
        }
    }
}
