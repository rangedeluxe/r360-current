﻿using System;
using System.Collections.Generic;
using System.Text;
using SysAnalysis.DTOs;
using SysAnalysis.Utils;

namespace SysAnalysis.Utils
{
    public class SysAnalyzerComparer : ISysComparer
    {
        public ComparisonResult Compare(SysInfo systemA, SysInfo systemB)
        {
            var res = systemA.Version.CompareTo(systemB.Version);
            var message = string.Empty;

            if (res == 0)
            {

                message = $"System: {systemA.Name} {systemA.Version.ToString() } " +
                    $"is the same version as {systemB.Name} {systemB.Version.ToString()}";
            }
            else if (res == 1)
            {

                message = $"System: {systemA.Name} {systemA.Version.ToString() } " +
                          $"is greater than {systemB.Name} {systemB.Version.ToString()}";
            }
            else if (res == -1)
            {
                
                message = $"System: {systemA.Name} {systemA.Version.ToString() } " +
                          $"is less than {systemB.Name} {systemB.Version.ToString()}";
            }

            return new ComparisonResult
            {
                SysInfoA = systemA,
                SysInfoB = systemB,
                Result = res,
                Message = message,

            };
        }
    }
}
