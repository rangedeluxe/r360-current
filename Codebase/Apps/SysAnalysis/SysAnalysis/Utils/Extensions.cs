﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SysAnalysis.Utils
{
	public static class Extensions
	{
        public static void AddIfNotNull<T>(this IList<T> list, T value)
        {
            if(value != null)
            {
                list.Add(value);
            }
        }

        public static string GetNumericValue(this string val)
        {
            var value = val.Trim();
            var match = Regex.Match(value, @"\d+(\.\d+)?");
            return match.Value;
        }
        public static string GetValueAfter(this string val, string delimeter)
        {
            try
            {
                var newval = val.Replace(" ", string.Empty);
                var m = val.Substring(newval.IndexOf(delimeter));
                return m;
            }
            catch(Exception )
            {
                return "";
            }

        }

        public static string GetValueWithinDelimeters(this string val, string delimeter1, string delimeter2)
        {
            var r = new Regex($"(?<={delimeter1}).*?(?={delimeter2})", RegexOptions.Singleline);            
            return r.Match(val).Value;
        }

        public static Version GetVersion(this string val)
        {
            Regex pattern = new Regex(@"\d+(\.\d+)+");
            Match m = pattern.Match(val);
            Version nver;
            Version.TryParse(m.Value, out nver);
            return nver;
        }

        public static int TryParseToInt(this string input, int valueIfNotConverted)
        {
            int value;
            if (int.TryParse(input, out value))
            {
                return value;
            }
            return valueIfNotConverted;
        }
    }
}
