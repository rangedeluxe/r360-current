﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using SysAnalysis.DTOs;
using SysAnalysis.Interfaces;

namespace SysAnalysis.Utils
{
    public class ProcessExecutor : IProcess
    {
        private const int WAITTIMETOFINISH = 5000;
        public ProcessInfo Start(string command, string arguments)
        {
            try
            {
                Process process = new Process();
                process.StartInfo.FileName = command;
                process.StartInfo.Arguments = arguments;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                var procStarted = process.Start();
                string output = process.StandardOutput.ReadToEnd();
                process.WaitForExit(WAITTIMETOFINISH);
                return new ProcessInfo { Output = output, ProcessStarted = procStarted };
            }
            catch (Exception e)
            {
                return new ProcessInfo
                {
                    ProcessStarted = false,
                    Output = e.Message
                };
            }
        }
    }
}
