﻿using SysAnalysis.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.Settings
{
    public class SupportedSettingsProvider : ISettingProvider
    {
        IRegistry _registry;
        public SupportedSettingsProvider(IRegistry registry)
        {
            _registry = registry;
        }
        public IList<ISetting> GetSupportedSettings()
        {
            return new List<ISetting>
            {
                new UrlRewrite(_registry),
                new EnforceDotNetCrypto(_registry)
            };
        }
    }
}
