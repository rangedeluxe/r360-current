﻿using System;
using System.Collections.Generic;
using System.Text;
using SysAnalysis.Interfaces;
using SysAnalysis.Utils;

namespace SysAnalysis.Settings
{
	 public class UrlRewrite : ISetting
	 {
		 private const string URLREWRITE = "IIS Rewrite Module";
		 private const string REWRITELOC = @"SOFTWARE\WOW6432Node\Microsoft\IIS Extensions\URL Rewrite\";
		 private const string REWRITEKEY = "Install";
		 private IRegistry _registry;
		 public UrlRewrite(IRegistry registry)
		 {
			 _registry = registry;
		 }
		  public string GetName()
		  {
			  return URLREWRITE;
		  }

		  public bool IsInstalled()
		  {
			  var url = _registry.GetValue(RegistryStore.LocalMachine, REWRITELOC, REWRITEKEY);
			  return url.Equals("1", StringComparison.CurrentCultureIgnoreCase);
		  }
	 }
}
