﻿using System;
using System.Collections.Generic;
using System.Text;
using SysAnalysis.Interfaces;
using SysAnalysis.Utils;

namespace SysAnalysis.Settings
{
    public class EnforceDotNetCrypto : ISetting
    {
        private const string STRONGCRYPTO = "Enforce strong cryptography for .net apps";
        private const string STRONGCRYPTOLOCATION32 = @"SOFTWARE\Wow6432Node\Microsoft\.NetFramework\v4.0.30319";
        private const string STRONGCRYPTOLOCATION = @"SOFTWARE\Microsoft\.NetFramework\v4.0.30319";
        private const string STRONGCRYPTOKEY = "SchUseStrongCrypto";
        private IRegistry _registry;
        public EnforceDotNetCrypto(IRegistry registry)
        {
            _registry = registry;
        }
        public string GetName()
        {
            return STRONGCRYPTO;
        }

        public bool IsInstalled()
        {
            var strong32 = _registry.GetValue(RegistryStore.LocalMachine, STRONGCRYPTOLOCATION32, STRONGCRYPTOKEY);
            var strong = _registry.GetValue(RegistryStore.LocalMachine, STRONGCRYPTOLOCATION, STRONGCRYPTOKEY);
            return strong.Equals("1", StringComparison.CurrentCultureIgnoreCase) && 
                   strong32.Equals("1", StringComparison.CurrentCultureIgnoreCase);

        }
    }
}
