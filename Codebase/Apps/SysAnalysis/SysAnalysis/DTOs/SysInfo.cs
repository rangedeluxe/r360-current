﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.DTOs
{
    public class SysInfo
    {
        public string Name { get; set; }
        public Version Version { get; set; }
        public string Details { get; set; }
        public bool IsSetting { get; set; }
        public bool IsInstalled { get; set; }
        public bool IsMinVersionMet { get; set; }
    }
}
