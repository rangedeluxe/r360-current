﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.DTOs
{
	 public class ProcessInfo
	 {
		 public bool ProcessStarted { get; set; }
		 public string Output { get; set; }
	 }
}
