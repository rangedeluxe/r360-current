﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.DTOs
{
    public class ComparisonResult
    {
        /// <summary>
        /// 0 - Comparison is equal
        /// 1 - A is greater than B
        ///-1 - B is greater than A
        /// </summary>
        public SysInfo SysInfoA { get; set; }
        public SysInfo SysInfoB { get; set; }
        public int Result { get; set; }
        public string Message { get; set; }
        
    }
    
}
