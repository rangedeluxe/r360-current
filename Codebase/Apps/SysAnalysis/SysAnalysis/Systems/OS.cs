﻿using SysAnalysis.DTOs;
using SysAnalysis.Interfaces;
using SysAnalysis.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SysAnalysis.Systems
{
    public class OS : ISystem
    {
        private IRegistry _registry;
        private const string WINDOWSPATH = @"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion";
        private const string CURRENTVERSION = "CurrentVersion";
        private const string MAJORVERSION = "CurrentMajorVersionNumber";
        private const string MINORVERSION = "CurrentMinorVersionNumber";
        private const string PRODUCTNAME = "ProductName";
        public OS(IRegistry registry)
        {
            _registry = registry;
        }

        public bool IsInstalled()
        {
            return true;
        }
        public string GetName()
        {
            return "OS";
        }

        public IList<SysInfo> GetVersions()
        {
            var major = _registry.GetValue(Utils.RegistryStore.LocalMachine, WINDOWSPATH, MAJORVERSION);
            var minor = _registry.GetValue(Utils.RegistryStore.LocalMachine, WINDOWSPATH, MINORVERSION);
            var versions = new List<SysInfo>();

            Version version;
            if (!string.IsNullOrEmpty(major) && !string.IsNullOrEmpty(minor))
            {
                version = new Version(major.TryParseToInt(0), minor.TryParseToInt(0));
            }
            else
            {
                var currVer = _registry.GetValue(Utils.RegistryStore.LocalMachine, WINDOWSPATH, CURRENTVERSION);
                version = !string.IsNullOrEmpty(currVer) ? new Version(currVer) : null;
            }
            var osname = _registry.GetValue(Utils.RegistryStore.LocalMachine, WINDOWSPATH, PRODUCTNAME);
            if (version == null) { return versions; }

            versions.Add(new SysInfo
            {
                Name = osname,
                Version = version
            });

            return versions;
        }

        /// <summary>
        /// For obvious reasons this is always false
        /// </summary>
        /// <returns></returns>
        public bool HasMultipleVersions()
        {
            return false;
        }
    }
}
