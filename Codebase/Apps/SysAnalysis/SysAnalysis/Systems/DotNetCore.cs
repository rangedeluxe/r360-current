﻿using SysAnalysis.DTOs;
using SysAnalysis.Interfaces;
using SysAnalysis.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SysAnalysis.Systems
{
    public class DotNetCore : ISystem
    {
        private const string DOTNETCORENAME = ".Net Core";
        private IProcess _processRunner;
        public DotNetCore(IProcess processRunner)
        {
            _processRunner = processRunner;
        }
        public string GetName()
        {
            return DOTNETCORENAME;
        }

        public IList<SysInfo> GetVersions()
        {
            var versions = new List<SysInfo>();
            var mainversion = GetMainCoreVersion();
            if(mainversion == null)
            {
                return versions;
            }
            versions.Add(mainversion);
            var sdkVersions = GetVersionsFromSDK();
            var newItems = sdkVersions.Where(x => !versions.Any(y => x.Version.Equals(y.Version)));
            foreach(var item in newItems)
            {
                versions.Add(item);
            }
            //versions.AddRange(newItems);
            return versions;
        }

        private SysInfo GetMainCoreVersion()
        {
            SysInfo info;
            var processResponse = _processRunner.Start("dotnet", "--info");
            if (!processResponse.ProcessStarted)
            {
                return null;
            }
            var output = processResponse.Output;
            var parsedVer = output.GetValueAfter("Version:");
            var version = parsedVer.GetVersion();
            if (string.IsNullOrEmpty(parsedVer) || version == null)
            {
                return null;
            }
            info = new SysInfo
            {
                Name = DOTNETCORENAME,
                Version = version
            };
            return info;
        }

        private IList<SysInfo> GetVersionsFromSDK()
        {
            var versions = new List<SysInfo>();
            var procResponse = _processRunner.Start("dotnet", "--list-runtimes");
            if (!procResponse.ProcessStarted)
            {
                return versions;
            }
            foreach (var line in procResponse.Output.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries))
            {
                var currVersion = line.GetVersion();
                if (currVersion != null)
                {
                    versions.Add(new SysInfo
                    {
                        Name = DOTNETCORENAME,
                        Version = currVersion
                    });
                }
            }
            return versions.OrderByDescending(x => x.Version).ToList();
        }

        public bool HasMultipleVersions()
        {
            var versions = GetVersions();
            return versions.Count > 1;
        }

        public bool IsInstalled()
        {
            var versions = GetVersions();
            return versions.Count > 0;
        }
    }
}
