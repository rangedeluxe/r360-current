﻿using SysAnalysis.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SysAnalysis.DTOs;
using System.Text.RegularExpressions;
using SysAnalysis.Utils;

namespace SysAnalysis.Systems
{
    public class TLS : ISystem
    {
        private const string PROTOCOLS = @"SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\";
        private const string PROTOCOL = "TLS";
        private const string ENABLED = "Enabled";
        private const string CLIENTIER = "Client";
        private const string SERVERTIER = "Server";

        private IRegistry _registry;
        public TLS(IRegistry registry)
        {
            _registry = registry;
        }
        public bool IsInstalled()
        {
            var protocols = _registry.GetAllSubKeys(RegistryStore.LocalMachine, PROTOCOLS);
            return protocols.Any(x => x.Contains(PROTOCOL));
        }

        public string GetName()
        {
            return PROTOCOL;
        }

        public IList<SysInfo> GetVersions()
        {
            var pcs = new List<SysInfo>();
            var protocols = _registry.GetAllSubKeys(RegistryStore.LocalMachine, PROTOCOLS).Where(x => x.Contains(PROTOCOL));
            foreach (var prot in protocols)
            {
                //there could be client and server for each ssl version
                var client = GetTLSInfo(prot, CLIENTIER);
                pcs.AddIfNotNull(client);
                var server = GetTLSInfo(prot, SERVERTIER);
                pcs.AddIfNotNull(server);
            }

            return pcs.OrderByDescending(x => x.Version).ToList();
        }

        private SysInfo GetTLSInfo(string protocol, string tier)
        {
            try
            {
                var name = $"{tier} {protocol}";
                //var resultreggex = Regex.Match(protocol, @"\d+(\.\d+)?").Value;
                var version = new Version(protocol.GetNumericValue());
                var enabled = _registry.GetValue(RegistryStore.LocalMachine, $"{PROTOCOLS}{protocol}\\{tier}\\", ENABLED);
                //if we dont find the enabled setting, it's as good as not being installed.
                if (string.IsNullOrEmpty(enabled)) return null;
                return new SysInfo()
                {
                    Name = name,
                    Version = version,
                    Details = enabled == "1" ? "Enabled" : "Disabled"
                };
            }
            catch (Exception)
            {

                return null;
            }
        }

        public bool HasMultipleVersions()
        {
            var protocols = _registry.GetAllSubKeys(Utils.RegistryStore.LocalMachine, PROTOCOLS);
            return protocols.Select(x => x.Contains(PROTOCOL)).Count() > 1;
        }
    }
}
