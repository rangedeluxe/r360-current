﻿using SysAnalysis.DTOs;
using SysAnalysis.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SysAnalysis.Systems
{
    public class DotNet : ISystem
    {
        private const string DOTNETNAME = ".Net Framework";
        //we dont support anything less than 4 so we are going to pretend if 4 is not installed we dont have .net 
        private const string DOTNET4LOCATIONFULL = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\";
        private const string DOTNET4LOCATIONCLIENT = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client\";
        private const string DOTNETVERSION = "Version";
        private IRegistry _registry;

        public DotNet(IRegistry registry)
        {
            _registry = registry;
        }
        public string GetName()
        {
            return DOTNETNAME;
        }

        public IList<SysInfo> GetVersions()
        {
            var versions = new List<SysInfo>();
            var dotnetfull = _registry.GetValue(Utils.RegistryStore.LocalMachine, DOTNET4LOCATIONFULL, DOTNETVERSION);
            if (!string.IsNullOrEmpty(dotnetfull))
            {
                versions.Add(new SysInfo
                {
                    Name = $"{DOTNETNAME} Full",
                    Version = new Version(dotnetfull),
                });
            }
            var dotnetclient = _registry.GetValue(Utils.RegistryStore.LocalMachine, DOTNET4LOCATIONCLIENT, DOTNETVERSION);
            if (!string.IsNullOrEmpty(dotnetclient))
            {
                versions.Add(new SysInfo
                {
                    Name = $"{DOTNETNAME} Client",
                    Version = new Version(dotnetclient),
                });
            }
            return versions.OrderByDescending(x => x.Version).ToList();

        }

        public bool HasMultipleVersions()
        {
            var versions = GetVersions();
            return versions.Count > 1;
        }

        public bool IsInstalled()
        {
            var versions = GetVersions();
            return versions.Count > 0;
        }


    }
}
