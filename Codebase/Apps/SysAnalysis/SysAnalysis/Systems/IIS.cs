﻿using SysAnalysis.DTOs;
using SysAnalysis.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.Systems
{
    public class IIS : ISystem
    {
        private IRegistry _registry;
        private const string IISPATH = @"Software\Microsoft\InetStp";
        private const string IISNAMEKEY = "IISProgramGroup";
        private const string MAJORVERSION = "MajorVersion";
        private const string MINORVERSION = "MinorVersion";
        private const string IISNAME = "IIS";

        public IIS(IRegistry registry)
        {
            _registry = registry;
        }

        public bool IsInstalled()
        {
            return _registry.KeyExists(Utils.RegistryStore.LocalMachine, IISPATH, IISNAMEKEY);
        }
        public string GetName()
        {
            return IISNAME;
        }

        public IList<SysInfo> GetVersions()
        {
            int major = int.TryParse(_registry.GetValue(Utils.RegistryStore.LocalMachine, IISPATH, MAJORVERSION), out major) ? major : 0;
            int minor = int.TryParse(_registry.GetValue(Utils.RegistryStore.LocalMachine, IISPATH, MINORVERSION), out minor) ? minor : 0;
            var name = _registry.GetValue(Utils.RegistryStore.LocalMachine, IISPATH, IISNAMEKEY);
            var sv = new SysInfo
            {
                Name = name,
                Version = new Version(major, minor)
            };
            return new List<SysInfo> { sv };
        }

        //There can only be one IIS version in the same machine
        public bool HasMultipleVersions()
        {
            return false;
        }
    }
}
