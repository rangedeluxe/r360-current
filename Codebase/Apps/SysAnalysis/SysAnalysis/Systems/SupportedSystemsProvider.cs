﻿using SysAnalysis.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SysAnalysis.Systems
{
    public class SupportedSystemsProvider : ISystemProvider
    {
        IRegistry _registry;
        IProcess _process;
        public SupportedSystemsProvider(IRegistry registry, IProcess process)
        {
            _registry = registry;
            _process = process;
        }
        public IList<ISystem> GetSupportedSystems()
        {
            return new List<ISystem>
            {
                 new OS(_registry),
                 new IIS(_registry),
                 new DotNet(_registry),
                 new DotNetCore(_process),
                 new TLS(_registry),

            };
        }
    }
}
