﻿using System;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.Common.Crypto;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:
* Date:
*
* Purpose:
*
* Modification History
* WI 162282 TWE 08/29/2014
*    -Create application to encrypt Entity, Logon and password from RAAM
******************************************************************************/

namespace EncryptDialog
{
    public partial class EncryptDialog : Form
    {
        public EncryptDialog()
        {
            InitializeComponent();
        }

        private string CombinedErrorMessage;

        private void encyrptButton_Click(object sender, EventArgs e)
        {
            CombinedErrorMessage = string.Empty;
            encryptedStrEntity.Text = "";
            encryptedStrLogon.Text = "";
            encryptedStrPassword.Text = "";

            if (strEntity.Text.Length == 0 ||
                strLogon.Text.Length == 0 ||
                strPassword1.Text.Length == 0 ||
                strPassword2.Text.Length == 0)
            {
                MessageBox.Show("All fields must be entered");
                return;
            }

            if (strPassword1.Text != strPassword2.Text)
            {
                MessageBox.Show("The passwords must be equal");
                return;
            }
            
            string strEncryptedEntity = string.Empty;
            string strEncryptedLogon = string.Empty;
            string strEncryptedPassword = string.Empty;

            if (!EncryptString(strEntity.Text, out strEncryptedEntity) ||
                !EncryptString(strLogon.Text, out strEncryptedLogon)   ||
                !EncryptString(strPassword1.Text, out strEncryptedPassword))
            {
                MessageBox.Show("Unable to encrypt string: " + CombinedErrorMessage);
            }

            encryptedStrEntity.Text = strEncryptedEntity;
            encryptedStrLogon.Text = strEncryptedLogon;
            encryptedStrPassword.Text = strEncryptedPassword;

        }

        private bool EncryptString(string fieldIn, out string fieldOut)
        {
            bool returnStatusFlag = true;
            fieldOut = "";

            try
            {
                cCrypto3DES cryp3DES = new cCrypto3DES();

                CombinedErrorMessage = CombinedErrorMessage + "field:";

                if (!cryp3DES.Encrypt(fieldIn, out fieldOut))
                {
                    CombinedErrorMessage = CombinedErrorMessage + "Unable to encrypt string. " + cryp3DES.LastException.Message + "  ";
                    returnStatusFlag = false;
                }
            }
            catch (Exception ex)
            {
                CombinedErrorMessage = CombinedErrorMessage + "Unable to encrypt string. " + ex.Message + "  ";
                MessageBox.Show("Unable to encrypt string " + ex.Message);
                returnStatusFlag = false;
            }
            return returnStatusFlag;
        }


        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
