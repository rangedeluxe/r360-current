﻿namespace EncryptDialog
{
    partial class EncryptDialog
    {
        ///// <summary>
        ///// Required designer variable.
        ///// </summary>
        //private System.ComponentModel.IContainer components = null;



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.encryptedStrEntity = new System.Windows.Forms.TextBox();
            this.strEntity = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.strLogon = new System.Windows.Forms.TextBox();
            this.encryptedStrLogon = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.strPassword1 = new System.Windows.Forms.TextBox();
            this.encryptedStrPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.strPassword2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Location = new System.Drawing.Point(12, 243);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(75, 23);
            this.btnEncrypt.TabIndex = 0;
            this.btnEncrypt.Text = "Encrypt";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.encyrptButton_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(183, 243);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.close_Click);
            // 
            // encryptedStrEntity
            // 
            this.encryptedStrEntity.Location = new System.Drawing.Point(304, 34);
            this.encryptedStrEntity.Name = "encryptedStrEntity";
            this.encryptedStrEntity.ReadOnly = true;
            this.encryptedStrEntity.Size = new System.Drawing.Size(155, 20);
            this.encryptedStrEntity.TabIndex = 2;
            // 
            // strEntity
            // 
            this.strEntity.Location = new System.Drawing.Point(103, 34);
            this.strEntity.Name = "strEntity";
            this.strEntity.Size = new System.Drawing.Size(155, 20);
            this.strEntity.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Input String";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(301, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Encrypted String";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Entity:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "User Logon:";
            // 
            // strLogon
            // 
            this.strLogon.Location = new System.Drawing.Point(103, 73);
            this.strLogon.Name = "strLogon";
            this.strLogon.Size = new System.Drawing.Size(155, 20);
            this.strLogon.TabIndex = 8;
            // 
            // encryptedStrLogon
            // 
            this.encryptedStrLogon.Location = new System.Drawing.Point(304, 73);
            this.encryptedStrLogon.Name = "encryptedStrLogon";
            this.encryptedStrLogon.ReadOnly = true;
            this.encryptedStrLogon.Size = new System.Drawing.Size(155, 20);
            this.encryptedStrLogon.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Password:";
            // 
            // strPassword1
            // 
            this.strPassword1.Location = new System.Drawing.Point(103, 113);
            this.strPassword1.Name = "strPassword1";
            this.strPassword1.PasswordChar = '*';
            this.strPassword1.Size = new System.Drawing.Size(155, 20);
            this.strPassword1.TabIndex = 11;
            // 
            // encryptedStrPassword
            // 
            this.encryptedStrPassword.Location = new System.Drawing.Point(304, 113);
            this.encryptedStrPassword.Name = "encryptedStrPassword";
            this.encryptedStrPassword.ReadOnly = true;
            this.encryptedStrPassword.Size = new System.Drawing.Size(155, 20);
            this.encryptedStrPassword.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 163);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Password:";
            // 
            // strPassword2
            // 
            this.strPassword2.Location = new System.Drawing.Point(103, 157);
            this.strPassword2.Name = "strPassword2";
            this.strPassword2.PasswordChar = '*';
            this.strPassword2.Size = new System.Drawing.Size(155, 20);
            this.strPassword2.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "ReEnter";
            // 
            // EncryptDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 278);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.strPassword2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.strPassword1);
            this.Controls.Add(this.encryptedStrPassword);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.strLogon);
            this.Controls.Add(this.encryptedStrLogon);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.strEntity);
            this.Controls.Add(this.encryptedStrEntity);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnEncrypt);
            this.Name = "EncryptDialog";
            this.Text = "Encrypt Utility";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox encryptedStrEntity;
        private System.Windows.Forms.TextBox strEntity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox strLogon;
        private System.Windows.Forms.TextBox encryptedStrLogon;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox strPassword1;
        private System.Windows.Forms.TextBox encryptedStrPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox strPassword2;
        private System.Windows.Forms.Label label7;

        ///// <summary>
        ///// Required designer variable.
        ///// </summary>
        //private System.ComponentModel.IContainer components = null;

        ///// <summary>
        ///// Clean up any resources being used.
        ///// </summary>
        ///// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing && (components != null))
        //    {
        //        components.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //#region Windows Form Designer generated code

        ///// <summary>
        ///// Required method for Designer support - do not modify
        ///// the contents of this method with the code editor.
        ///// </summary>
        //private void InitializeComponent()
        //{
        //    this.SuspendLayout();
        //    // 
        //    // Form1
        //    // 
        //    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        //    this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        //    this.ClientSize = new System.Drawing.Size(491, 292);
        //    this.Name = "Form1";
        //    this.Text = "Form1";
        //    this.ResumeLayout(false);

        //}

        //#endregion
    }
}

