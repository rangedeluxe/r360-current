﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using WFS.LTA.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     06/19/2013
*
* Purpose:  
*
* Modification History
* WI 106017 JMC 06/24/2013
*    -Initial release version.
* WI 123215 MLH 11/22/2013
*   - Fixed where the Tier 2 assemblies were not getting 'Stamped' with the new build number    
******************************************************************************/
namespace RollupBaseVersion {
    
    class Program {

        private const string RELEASE_MODE = "Release";
        private static List<string> _FileToCheckIn = new List<string>();
        private static string _ReleaseVersion;

        private static ltaLog _ErrorLog;
        private static ltaLog _EventLog;
        private static string _StampVersionPath = string.Empty;

        private static string[] _ProjectFolderWhiteList = { 
            @"Apps\CreateInitialAdminUserUtility",
            @"Apps\CryptoDialog\",
            @"Apps\EncryptIni",
            @"Components",
            @"ComponentsCollection",
            @"DataOutput\CryptoDialog",
            @"DataOutput\ExtractProcessSvc",
            @"DataOutput\ExtractWizard",
            @"DataOutput\LogoMgr",
            @"FileCleanup\FileCleanupSvc",
            @"FileImport\FileImportClientLib",
            @"FileImport\FileImportClientSvc",
            @"FileImport\FileImportDataBuilder",
            @"FileImport\FileImportServices",
            @"FileImport\FileImportWCFLib",
            @"FileImport\fitICONXClient",
            @"FileImport\fitItemProcessingXClient",
            @"OLDecisioning\OLDecisioningServices",
            @"OLF\OLFImageImport",
            @"OLF\OLFImageSvc",
            @"OLF\OLFServices",
            @"OLF\OLFServicesClient",
            @"Online\IPOnline",
           // @"Online\OLLogon",
           // @"OnlineAdmin\OLAdmin",
            @"Purge",
            @"R360Services",
            @"Reporting"
           // @"RA3M\RA3MClient",
           // @"UserSetup\UserSetupServices"
        };


        private static ltaLog ErrorLog {
            get {
                string strLogPath;

                if(_ErrorLog  == null) {
                    if(ConfigurationManager.AppSettings["errorLogPath"] != null && ConfigurationManager.AppSettings["errorLogPath"].Length > 0) {
                        strLogPath = ConfigurationManager.AppSettings["errorLogPath"];
                    } else {
                        strLogPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "ErrLog.txt");
                    }
                    _ErrorLog = new ltaLog(strLogPath, 1024, LTAMessageImportance.Debug);
                    _ErrorLog.InsertBlankLine();
                }
                return (_ErrorLog);
            }
        }

        private static ltaLog EventLog {
            get {
                string strLogPath;

                if(_EventLog == null) {
                    if(ConfigurationManager.AppSettings["eventLogPath"] != null && ConfigurationManager.AppSettings["eventLogPath"].Length > 0) {
                        strLogPath = ConfigurationManager.AppSettings["eventLogPath"];
                    } else {
                        strLogPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "EventLog.txt");
                    }
                    _EventLog = new ltaLog(strLogPath, 1024, LTAMessageImportance.Debug);
                    _EventLog.InsertBlankLine();
                }
                return (_EventLog);
            }
        }

        private static string StampVersionPath {
            get {
                if(_StampVersionPath == string.Empty) {
                    if(ConfigurationManager.AppSettings["stampVersionPath"] != null && ConfigurationManager.AppSettings["stampVersionPath"].Length > 0) {
                        _StampVersionPath = ConfigurationManager.AppSettings["stampVersionPath"];
                    } else {
                        _StampVersionPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "StampVersion.exe");
                    }
                }
                return (_StampVersionPath);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args">
        ///     updatedVersion=Bld_13.06.09.2  
        ///     baseVersion=2.00.00.00
        /// </param>
        static void Main(string[] args) {

            string strWorkspacePath;
            bool bSuccess = false;
            string strCodebaseDirectory;
            List<string> arBaseDLLToUpdate;
            string strBuildVersionTier1;
            string strBuildVersionTier2;
            string strStampVersion;
            string strTier2SharedDependenciesFolder;
            List<string> arDLLsToCopy;

            try {

                if(args.Length != 4) {
                    LTAConsole.ConsoleWriteLine("Please specify three arguments. First is Release Version (2.00.00.00 ) and second is tier 1 version(Bld_13.06.24.2), third is tier 2 version (Bld_13.06.24.2)", LTAConsoleMessageType.Warning);
                    return;
                }

                _ReleaseVersion = args[0];
                strBuildVersionTier1 = args[1];
                strBuildVersionTier2 = args[2];
                strStampVersion = args[3];
                if(ConfigurationManager.AppSettings["workspacePath"] != null && ConfigurationManager.AppSettings["workspacePath"].Length > 0) {
                    strWorkspacePath = ConfigurationManager.AppSettings["workspacePath"];
                } else {
                    strWorkspacePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"C:\work\200RecHub\");
                }

                strTier2SharedDependenciesFolder = Path.Combine(strWorkspacePath, "SharedDependencies", _ReleaseVersion, strBuildVersionTier2);
                strCodebaseDirectory = Path.Combine(strWorkspacePath, @"Codebase");

                // Roll up Tier 1 Components to to all projects.
                arBaseDLLToUpdate = new List<string>();
                arBaseDLLToUpdate.Add("ipoDB.dll");
                arBaseDLLToUpdate.Add("ipoCrypto.dll");
                arBaseDLLToUpdate.Add("ipoLib.dll");
                arBaseDLLToUpdate.Add("ipoLog.dll");
                arBaseDLLToUpdate.Add("ipoWebLib.dll");
                arBaseDLLToUpdate.Add("DALBase.dll");
                UpdateFiles(strCodebaseDirectory, arBaseDLLToUpdate, strBuildVersionTier1);

                

                string strFileVersion = FormatBldNumberToFileVersionNumber(strStampVersion);
                
                //stamp tier 1 components
                StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoCrypto"));
                StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoDB"));
                StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoLib"));
                StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoLog"));
                StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoWebLib"));
                StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ltaWCFLib"));
                StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\DALBase"));
                
                // Build Tier 1 components
                bSuccess = BuildMSBuild(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\Tier1Build.sln"));
                
                //copy Tier 1 components
                arDLLsToCopy = new List<string>();
                arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoCrypto\bin", RELEASE_MODE, "ipoCrypto.dll"));
                arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoDB\bin", RELEASE_MODE, "ipoDB.dll"));
                arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoLib\bin", RELEASE_MODE, "ipoLib.dll"));
                arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoLog\bin", RELEASE_MODE, "ipoLog.dll"));
                arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ipoWebLib\bin", RELEASE_MODE, "ipoWebLib.dll"));
                arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\DALBase\bin", RELEASE_MODE, "DALBase.dll"));
                //arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\Common\ltaWCFLib\bin", RELEASE_MODE, "ltaWCFLib.dll"));
               
                CopyFiles(strTier2SharedDependenciesFolder, arDLLsToCopy);

                ////----------------------------------------------------------------------------------------------------------
                ////Now start working on Tier 2
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\LogonDAL"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\OLFServicesDAL"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\OLLogonServicesDAL"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\PermissionDAL"));
                //StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\PreferencesDAL"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\RecHubCommonDAL"));
                //StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\SessionDAL"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\DAL\UserDAL"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\BusinessCommon\ipoImages"));
                //StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\BusinessCommon\ipoCommon"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\BusinessCommon\ipoPICS"));
                //StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Components\itemProcessingDAL\itemProcessingDAL"));
                //StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"ComponentsCollection\BusinessCommon\ltaBusinessCommonLib"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"OLF\OLFServices\OLFServicesAPI"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Online\OLLogon\OLLogonServicesAPI"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"OLF\OLFServicesClient\OLFServicesClient"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"Online\OLLogon\OLLogonServicesClient"));
                ////StampVersion(strFileVersion, Path.Combine(strCodebaseDirectory, @"RA3M\RA3MClient\RA3MClientLib"));

                //// Build Tier 2 components
                //bSuccess = BuildMSBuild(Path.Combine(strCodebaseDirectory, @"Components\BusinessCommonComponents.sln"));

                //// Copy Tier 2 Components to the SharedDependencies folder.
                //arDLLsToCopy = new List<string>();
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\LogonDAL\bin", RELEASE_MODE, "LogonDAL.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\OLFServicesDAL\bin", RELEASE_MODE, "OLFServicesDAL.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\OLLogonServicesDAL\bin", RELEASE_MODE, "OLLogonServicesDAL.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\PermissionDAL\bin", RELEASE_MODE, "PermissionDAL.dll"));
                //arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\PreferencesDAL\bin", RELEASE_MODE, "PreferencesDAL.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\RecHubCommonDAL\bin", RELEASE_MODE, "RecHubCommonDAL.dll"));
                //arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\SessionDAL\bin", RELEASE_MODE, "SessionDAL.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\DAL\UserDAL\bin", RELEASE_MODE, "UserDAL.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\BusinessCommon\ipoImages\bin", RELEASE_MODE, "ipoImages.dll"));
                //arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\BusinessCommon\ipoCommon\bin", RELEASE_MODE, "ltaCommon.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\BusinessCommon\ipoPICS\bin", RELEASE_MODE, "ipoPICS.dll"));
                //arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Components\itemProcessingDAL\itemProcessingDAL\bin", RELEASE_MODE, "itemProcessingDAL.dll"));
                //arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"ComponentsCollection\BusinessCommon\ltaBusinessCommonLib\bin", RELEASE_MODE, "ltaBusinessCommonLib.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"OLF\OLFServices\OLFServicesAPI\bin", RELEASE_MODE, "OLFServicesAPI.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Online\OLLogon\OLLogonServicesAPI\bin", RELEASE_MODE, "OLLogonServicesAPI.dll"));
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"RA3M\RA3MClient\RA3MClientLib\bin", RELEASE_MODE, "RA3MClientLib.dll"));

                //CopyFiles(strTier2SharedDependenciesFolder, arDLLsToCopy);

                //// Roll up Tier 2 Components to all projects.
                //arBaseDLLToUpdate = new List<string>();
                ////arBaseDLLToUpdate.Add("LogonDAL.dll");
                ////arBaseDLLToUpdate.Add("OLFServicesDAL.dll");
                ////arBaseDLLToUpdate.Add("OLLogonServicesDAL.dll");
                ////arBaseDLLToUpdate.Add("PermissionDAL.dll");
                //arBaseDLLToUpdate.Add("PreferencesDAL.dll");
                ////arBaseDLLToUpdate.Add("RecHubCommonDAL.dll");
                //arBaseDLLToUpdate.Add("SessionDAL.dll");
                ////arBaseDLLToUpdate.Add("UserDAL.dll");
                ////arBaseDLLToUpdate.Add("ipoImages.dll");
                //arBaseDLLToUpdate.Add("ltaCommon.dll");
                ////arBaseDLLToUpdate.Add("ipoPICS.dll");
                //arBaseDLLToUpdate.Add("itemProcessingDAL.dll");
                //arBaseDLLToUpdate.Add("ltaBusinessCommonLib.dll");
                ////arBaseDLLToUpdate.Add("OLFServicesAPI.dll");
                ////arBaseDLLToUpdate.Add("OLLogonServicesAPI.dll");
                ////arBaseDLLToUpdate.Add("RA3MClientLib.dll");
                //UpdateFiles(strCodebaseDirectory, arBaseDLLToUpdate, strBuildVersionTier2);

                ////bSuccess = BuildMSBuild(Path.Combine(strCodebaseDirectory, @"OLF\OLFServicesClient\OLFServicesClient.sln"));
                ////arDLLsToCopy = new List<string>();
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"OLF\OLFServicesClient\OLFServicesClient\bin", RELEASE_MODE, "OLFServicesClient.dll"));
                ////CopyFiles(strTier2SharedDependenciesFolder, arDLLsToCopy);

                ////bSuccess = BuildMSBuild(Path.Combine(strCodebaseDirectory, @"Online\OLLogon\OLLogon.sln"));
                ////arDLLsToCopy = new List<string>();
                ////arDLLsToCopy.Add(Path.Combine(strCodebaseDirectory, @"Online\OLLogon\OLLogonServicesClient\bin", RELEASE_MODE, "OLLogonServicesClient.dll"));
                ////CopyFiles(strTier2SharedDependenciesFolder, arDLLsToCopy);

                DoTestBuilds(strCodebaseDirectory);

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }

            LTAConsole.ConsoleWriteLine(string.Empty);
            LTAConsole.ConsoleWriteLine("Press any key to continue...");
            Console.Read();
        }

        private static string FormatBldNumberToFileVersionNumber(string buildVersion) {
            string strBareVersion = buildVersion.Replace("Bld_", string.Empty);
            string[] arTemp = strBareVersion.Split('.');
            int intTemp;
            string strFileVersion = string.Empty;
            foreach(string str in arTemp) {
                if(strFileVersion.Length > 0) {
                    strFileVersion += ".";
                }
                if(int.TryParse(str, out intTemp)) {
                    strFileVersion += intTemp.ToString();
                } else {
                    strFileVersion += "0";
                }
            }
            return (strFileVersion);
        }

        private static void StampVersion(string updatedVersion, string path) {
            
            // Start the child process.
            Process p = new Process();
            // Redirect the output stream of the child process.
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = StampVersionPath;
            p.StartInfo.Arguments = updatedVersion + " " + path;
            p.Start();
            // Do not wait for the child process to exit before
            // reading to the end of its redirected stream.
            // p.WaitForExit();
            // Read the output stream first and then wait.
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
        }

        private static void DoTestBuilds(string codeBaseDirectory) {

            bool bSuccess;

            try {

                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Apps\CreateInitialAdminUserUtility\CreateInitialAdminUserUtility.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Apps\CryptoDialog\CryptoDialog.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Apps\EncryptIni\EncryptIni.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Apps\EncryptDialog\EncryptDialog.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Apps\PartitionManager\PartitionManager.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"CommonException\CommonException.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"CommonException\CommonExceptionServicesClient\CommonExceptionServicesClient.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Components\BusinessCommonComponents.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Components\itemProcessingDAL\itemProcessingDAL.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Components\ltaFileCollector\ltaFileCollector.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Components\ltaLog\ltaLog.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"ComponentsCollection\Components.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"ComponentsCollection\BusinessCommon\ipoCommon\ltaCommon.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"ComponentsCollection\BusinessCommon\ipoImages\ipoImages.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"ComponentsCollection\BusinessCommon\ltaBusinessCommonLib\ltaBusinessCommonLib.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Consolidator\CDSConsole\CDSConsole.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportClientSvc\DataImportClientSvc.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportDemoClient\DataImportDemoClient.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportICONClientBase\DataImportICONClientBase.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportServices\DataImportServices.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportServices\DataImportTestClient\DataImportTestClient.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportServicesClient\DataImportServicesClient.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportToolConfigurator\DataImportToolConfigurator.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportWCFLib\DataImportWCFLib.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportXClientBase\DataImportXClientBase.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\DataImportSolution\DataImportSolution.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\ditClientSupportLib\ditClientSupportLib.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\ditDemoXClient\ditDemoXClient.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataImport\ditICONXClient\ditICONXClient.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataOutput\CryptoDialog\CryptoDialog.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataOutput\ExtractProcessSvc\ExtractProcessSvc.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataOutput\ExtractWizard\ExtractWizard.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"ExternalLogon\ExternalLogin.Solution\ExternalLogon.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataOutput\ExtractWizard\_obsolete\IntegraPAY_Delivery_Build.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"DataOutput\LogoMgr\LogoMgr.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"FileCleanup\FileCleanupSvc\FileCleanupSvc.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"FileImport\FileImportClientLib\FileImportClientLib.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"FileImport\FileImportClientSvc\FileImportClientSvc.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"FileImport\FileImportDataBuilder\FileImportDataBuilder.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"FileImport\FileImportServices\FileImportServices.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"FileImport\FileImportWCFLib\FileImportWCFLib.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"FileImport\fitICONXClient\fitICONXClient.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"FileImport\fitItemProcessingXClient\fitItemProcessingXClient.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"ICON\ICONCommon\ICONCommon.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"ICON\ICONServices\ICONServices.sln"));
                //bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"OLDecisioning\CheckDTD\CheckDTD.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"OLDecisioning\OLDecisioningServices\OLDecisioningServices.sln"));                
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"OLF\OLFImageSvc\OLFImageSvc.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Online\IPOnline\Online.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"Purge\PurgeSvc\PurgeSvc.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\BillingExtracts\BillingExtractsSolution\BillingExtractsSolution.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\CommonExceptions\CommonExceptionsSolution\CommonExceptionsSolution.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\EventSubmission\EventSolution\EventSolution.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\ExctractSchedule\ExtractScheduleSolution\ExtractScheduleSolution.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\Hub\HubSolution\HubSolution.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\HubConfigService\HubConfigServiceSolution\HubConfigService.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\Payer\PayerSolution\PayerSolution.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\Reporting\ReportingSolution\ReportingSolution.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"R360Services\SessionMaintenance\SessionMaintenanceSolution\SessionMaintenanceSolution.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"UI\UISolution\ExceptionViews\ExceptionViews.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"UI\UISolution\ExtractSchedulerViews\ExtractSchedulerViews.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"UI\UISolution\HubConfigViews\HubConfigViews.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"UI\UISolution\HubViews\HubViews.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"UI\UISolution\RaamProxy\RaamProxy.sln"));
                bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, @"UI\UISolution\ReportingViews\ReportingViews.sln"));

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
        }
        
        private static FileAttributes RemoveAttribute(FileAttributes attributes, FileAttributes attributesToRemove) {

            try {
                return attributes & ~attributesToRemove;
            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
                return (FileAttributes.Normal);
            }
        }

        static void UpdateCSProjFiles(string directory, string dllToSearchFor, string buildVersion) {

            string strMsg;

            try {

                string[] files = Directory.GetFiles(directory, "*.csproj", SearchOption.AllDirectories);

                int year = DateTime.Now.Year;
                foreach(string setupFileName in files) {

                    strMsg = "Setup FileName is " + setupFileName;
                    LTAConsole.ConsoleWriteLine(strMsg);
                    EventLog.logEvent(strMsg, "UpdateCSProjFiles", LTAMessageType.Information);

                    string shortDllName = dllToSearchFor.Remove(dllToSearchFor.Length - 4);
                    shortDllName += ",";
                    StringWriter sw = new StringWriter();
                    using(StreamReader reader = File.OpenText(setupFileName)) {


                        try {
                            while(reader.Peek() >= 0) {
                                string fileLine = reader.ReadLine();

                                if(fileLine.Contains(dllToSearchFor) && fileLine.Contains("SharedDependencies")) {
                                    int endPosition = fileLine.IndexOf("SharedDependencies");
                                    string newString = fileLine.Substring(0, endPosition);
                                    newString += "SharedDependencies\\";
                                    newString += _ReleaseVersion;
                                    newString += "\\";
                                    newString += buildVersion;
                                    newString += "\\";
                                    newString += dllToSearchFor;
                                    newString += "</HintPath>";
                                    if(!_FileToCheckIn.Contains(setupFileName)) {
                                        _FileToCheckIn.Add(setupFileName);
                                    }
                                    sw.Write(newString + (reader.Peek() >= 0?"\n":""));
                                } else if(fileLine.Contains(shortDllName)) {
                                    int endPosition = fileLine.IndexOf(shortDllName);
                                    string newString = fileLine.Substring(0, endPosition);
                                    newString += shortDllName.Remove(shortDllName.Length - 1);
                                    newString += "\">";
                                    sw.Write(newString + (reader.Peek() >= 0 ? "\n" : ""));
                                    ;

                                } else {
                                    sw.Write(fileLine + (reader.Peek() >= 0 ? "\n" : ""));
                                    ;

                                }
                            }
                        } finally {
                            // Close the file otherwise the compile may not work 
                            reader.Close();
                        }
                    }
                    MakeFileWriteable(setupFileName, sw);

                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
        }

        static void UpdateBatchFiles(string directory, string dllToSearchFor, string buildVersion) {

            string strMsg;

            try {

                string[] files = Directory.GetFiles(directory, "*.bat", SearchOption.AllDirectories);

                int year = DateTime.Now.Year;
                foreach(string setupFileName in files) {

                    strMsg = "Setup FileName is " + setupFileName;
                    LTAConsole.ConsoleWriteLine(strMsg);
                    EventLog.logEvent(strMsg, "UpdateBatchFiles", LTAMessageType.Information);

                    string shortDllName = dllToSearchFor.Remove(dllToSearchFor.Length - 4);
                    shortDllName += ",";
                    StringWriter sw = new StringWriter();
                    using(StreamReader reader = File.OpenText(setupFileName)) {


                        try {
                            while(reader.Peek() >= 0) {
                                string fileLine = reader.ReadLine();

                                if(fileLine.Contains(dllToSearchFor) && fileLine.Contains("SharedDependencies")) {
                                    int endPosition = fileLine.IndexOf("SharedDependencies");
                                    string newString = fileLine.Substring(0, endPosition);

                                    int intSubstrPos = fileLine.IndexOf('\\', endPosition);
                                    intSubstrPos = fileLine.IndexOf('\\', intSubstrPos + 1);
                                    intSubstrPos = fileLine.IndexOf('\\', intSubstrPos + 1);
                                    newString += "SharedDependencies\\";
                                    newString += _ReleaseVersion;
                                    newString += "\\";
                                    newString += buildVersion;

                                    newString += fileLine.Substring(intSubstrPos);
                                    
                                    if(!_FileToCheckIn.Contains(setupFileName)) {
                                        _FileToCheckIn.Add(setupFileName);
                                    }
                                    sw.WriteLine(newString);
                                } else if(fileLine.Contains(shortDllName)) {
                                    int endPosition = fileLine.IndexOf(shortDllName);
                                    string newString = fileLine.Substring(0, endPosition);
                                    newString += shortDllName.Remove(shortDllName.Length - 1);
                                    newString += "\">";
                                    sw.WriteLine(newString);
                                    ;

                                } else {
                                    sw.WriteLine(fileLine);
                                    ;

                                }
                            }
                        } finally {
                            // Close the file otherwise the compile may not work 
                            reader.Close();
                        }
                    }
                    MakeFileWriteable(setupFileName, sw);

                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
        }

        private static void MakeFileWriteable(string setupFileName, StringWriter sw) {

            int intRetries = 0;
            bool bolContinue = true;

            while(bolContinue) {
                try {

                    FileAttributes attributes = File.GetAttributes(setupFileName);
                    if((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                        // Make the file RW
                        attributes = RemoveAttribute(attributes, FileAttributes.ReadOnly);
                        File.SetAttributes(setupFileName, attributes);
                    }
                    TextWriter tw = new StreamWriter(setupFileName);
                    try {
                        tw.Write(sw.ToString());
                    } finally {
                        // close the stream 
                        tw.Close();
                    }

                    bolContinue = false;

                } catch(Exception e) {

                    ++intRetries;

                    if(intRetries > 3) {
                        ErrorLog.logError(e);
                        LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
                        bolContinue = false;
                    } else {
                        LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Warning);
                        LTAConsole.ConsoleWriteLine("Retrying (" + intRetries.ToString() + ")...", LTAConsoleMessageType.Warning);
                        System.Threading.Thread.Sleep(500);
                    }
                }
            }
        }

        private static bool BuildMSBuild(string solutionFile) {

            string strMsg;
            bool bErrorExists = false;

            try {
            
                string sysDir = Environment.GetEnvironmentVariable("windir");
                string msBuild = sysDir + "\\Microsoft.NET\\Framework\\v4.0.30319\\MSBuild.exe";

                strMsg = "Building: " + solutionFile;
                LTAConsole.ConsoleWriteLine(strMsg);
                EventLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Information);

                Process myProcess = new Process();
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = msBuild;
                myProcess.StartInfo.Arguments = "\"" + solutionFile + "\" /t:rebuild  /p:Configuration=Release /v:minimal";
                //myProcess.StartInfo.Arguments = "\"" + solutionFile + "\" /t:rebuild  /p:Configuration=Release /v:normal";
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                string output = myProcess.StandardOutput.ReadToEnd();
                myProcess.WaitForExit();
                if(output.Contains("error")) {
                    bErrorExists = true;

                    strMsg = solutionFile + " failed to build";
                    LTAConsole.ConsoleWriteLine(strMsg, LTAConsoleMessageType.Error);
                    EventLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Error);
                    ErrorLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Error);
                } else {
                    strMsg = solutionFile + " Build was successful.";
                    LTAConsole.ConsoleWriteLine(strMsg, LTAConsoleMessageType.Information);
                    EventLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Information);
                }
                return bErrorExists;

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
                return (false);
            }

        }
        private static void UpdateFiles(string codeBasePath, List<string> baseDLLToUpdate, string buildVersion) {

            string strMsg;

            try {

                foreach(string folder in _ProjectFolderWhiteList) {
                    foreach(string dllToSearchFor in baseDLLToUpdate) {
                        strMsg = "Updating project files for dll:[" + dllToSearchFor + "]";
                        LTAConsole.ConsoleWriteLine(strMsg);
                        EventLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Information);
                        UpdateCSProjFiles(Path.Combine(codeBasePath, folder), dllToSearchFor, buildVersion);
                        UpdateBatchFiles(Path.Combine(codeBasePath, folder), dllToSearchFor, buildVersion);
                    }
                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
        }

        private static bool CopyFiles(string sharedDependenciesFolder, List<string> dllsToCopy) {

            bool bolRetVal = false;
            string strMsg;

            try {

                if(!Directory.Exists(sharedDependenciesFolder)) {
                    Directory.CreateDirectory(sharedDependenciesFolder);
                }

                foreach(string dllOutput in dllsToCopy) {
                    string dest = sharedDependenciesFolder + "\\" + Path.GetFileName(dllOutput);
                    strMsg = "Copying " + dllOutput + " --> " + sharedDependenciesFolder;
                    LTAConsole.ConsoleWriteLine(strMsg);
                    EventLog.logEvent(strMsg, "CopyFiles", LTAMessageType.Information);
                    File.Copy(dllOutput, dest, true);
                    bolRetVal = true;
                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
            return (bolRetVal);
        }
    }
}
