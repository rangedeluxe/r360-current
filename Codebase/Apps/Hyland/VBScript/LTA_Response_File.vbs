'*********************************************************************************************************
'*	Title:		LTA Response File
'*
'*	Filename:	LTA_Response_File.vbs
'*
'*	Author:		Jeremy Bartels, WAUSAU, Contact me with any questions.
'*
'*	Date:		04/24/2009
'*	Revised:	05/05/2009 - Added DocTypeName to the response file
'*              01/11/2010 - Added Site ID keyword to the response file in both the header and line items
'*
'*	Purpose:	Parse through the verification report and create a response file for LTA in designated
'*			    directory.  This will be the method LTA gets notified when a batch has been processed.
'*				 
'*
'*			Workflow is running this script.
'*********************************************************************************************************
Option Explicit
'Change the following to the output directory of the queue files
Const ODBC = "O3Server"
Const KEGROUP_NUM = "103"
Const PROCESSING_DATE_KEYWORD_NUM = "155"
Const BANK_ID_KEYWORD_NUM = "156"
Const LOCKBOX_ID_KEYWORD_NUM = "157"
Const BATCH_ID_KEYWORD_NUM = "125"
Const BATCH_SEQUENCE_KEYWORD_NUM = "158"
Const PAGES_KEYWORD_NUM = "163"
Const SITE_ID_KEYWORD_NUM = "71"


Const ForReading = 1, ForWriting = 2, ForAppending = 8 
' Preferred method to write VB scripts with OnBase
Sub Main35()
	On Error Resume Next
	Const TEMPPATH = "C:\VerificationReport.txt"

	' Root level OnBase Automation object
	Dim objApp
	Set objApp = CreateObject("OnBase.Application")

	' Currently active document returned by the root automation object
	' The script will only have one CurrentDocument during each execution
	Dim objDocument
	Set objDocument = objApp.CurrentDocument
	
	Dim intDocHandle
	intDocHandle = objDocument.handle
	'copy the verification report local to parse through
	objDocument.CopyLocal(TEMPPATH)
	
	Dim return, ProcDate, Bank, Lockbox, Batch, SiteID, IMSBatch, WarnErr, InvalErr
	Dim Errors, Archived, Unarchived, ResponseFilePath, ResponseFileName
	
	'Parse file and get the return variables
	return = ParseFile(TEMPPATH, ProcDate, Bank, Lockbox, Batch, SiteID, IMSBatch, WarnErr, InvalErr, _
						Errors, Archived, Unarchived, ResponseFilePath, ResponseFileName)
	
	'write out the xml file using the returned variables
	WriteOutputFile ProcDate, Bank, Lockbox, Batch, SiteID, IMSBatch, WarnErr, InvalErr, Errors, _
					Archived, Unarchived, ResponseFilePath, ResponseFileName, intDocHandle
	objApp.ExecutionStatus = return
	
	Set objDocument = Nothing
	Set objApp = Nothing

	' Error Handling
	If Err.number <> 0 Then
		Dim objWSHShell
		Set objWSHShell = CreateObject("WScript.Shell")
		Call objWSHShell.LogEvent( 1, "LTA Response File: Main35()" & _
			vbNewLine & "For Document Handle: " & intDocHandle & vbNewLine & _
			"Error Number: " & Err.number & vbNewLine & Err.Description)
		Set objWSHShell = Nothing
	End If

End Sub

Function ParseFile(strFilePath, strProcessingDate, strBank, strLockbox, strBatch, strSiteID, strIMSBatch, strWarnErr, _
				strInvalErr, strErrors, strTotals, strUnarchived, strResponseFilePath, strResponseFileName)

	On Error Resume Next
	
	'Initialize Response varialbes as string
	strProcessingDate = ""
	strBank = ""
	strLockbox = ""
	strBatch = ""
	strSiteID = ""
	strIMSBatch = ""
	strWarnErr = ""
	strInvalErr = ""
	strErrors = ""
	strTotals = ""
	strUnarchived = ""
	strResponseFilePath = ""
	strResponseFileName = ""
	
	
	'Declare constants used for search strings to identify sections of the report
	Const ERROR1_IDENT = "Error :"
	Const ERROR2_IDENT = "ERROR"
	Const INVALID_IDENT = "Invalid" 
	Const WARNING_IDENT = "Warning"
	Const TOTALS_IDENT = "TOTALS"
	Const UNARCHIVED_IDENT = "Unarchived Documents  :"
	Const FILE_IDENT = "File                  :"
	Const BATCH_IDENT = "Internal Batch Number     :"
	
	'Set the return value to false (report has errors)
	Dim RetVal
	RetVal = 0
	
	'Open the verification report file
	Dim objfso, objTextFile
	Set objfso = CreateObject("Scripting.FileSystemObject")
	Set objTextFile = objfso.OpenTextFile(strFilePath)
	
	'initalize the counters
	Dim intErrorCount, intInvalidCount, intWarningCount
	intErrorCount = 0
	intInvalidCount = 0
	intWarningCount = 0
	
	'Loop through each line of the report and increment counters if there is an error
	Do While objTextFile.AtEndOfStream = False
		
		strLineInfo = objTextFile.ReadLine
		
		Dim intResult, strLineInfo
		
		intResult = InStr(strLineInfo, ERROR1_IDENT)
		If IntResult <> 0 Then 
			intErrorCount = intErrorCount + 1
		End If

		intResult = InStr(strLineInfo, ERROR2_IDENT)
		If IntResult <> 0 Then 
			intErrorCount = intErrorCount + 1
		End If

		intResult = InStr(strLineInfo, INVALID_IDENT)
		If IntResult <> 0 Then
			intInvalidCount = intInvalidCount + 1
		End If

		intResult = InStr(strLineInfo, WARNING_IDENT)
		If IntResult <> 0 Then
			intWarningCount = intWarningCount + 1
		End If

		'Find the total section and get the value on the report
		intResult = InStr(strLineInfo, TOTALS_IDENT)
		If intResult = 1 Then
			strTotals = Trim(Mid(strLineInfo, 76, 12))
		End If

		'find the Unarchived total and get the value from the report
		intResult = InStr(strLineInfo, UNARCHIVED_IDENT)
		If intResult = 1 Then
			strUnarchived = Trim(Mid(strLineInfo, 24, 20))
		End If

		'Find the file that was processed on the report
		intResult = InStr(strLineInfo, FILE_IDENT)
		If intResult = 1 Then
		
			'Checks to see if the file name is on the same line or next line
			If Not InStr(UCase(strLineInfo), ".DAT") > 0 Then
				'append the next line for the file name
				'MsgBox "append line"
				strLineInfo = strLineInfo & objTextFile.ReadLine
			End If
			
			'parse the file path
			Dim strFile
			strFile = Trim(Mid(strLineInfo, 24, 500))
			Dim intPos 
			intPos = InStrRev(strFile, "\")
			
			'set the response file path the same as the orignal process path and append a new folder
			strResponseFilePath = Trim(Left(strFile, intPos)) & "Responses\"
			
			'parse out the file name and set the response filename
			strFile = Mid(strFile, intPos + 1, 500)
			intPos = InStr(UCase(strFile), ".DAT")
			strResponseFileName = Trim(Left(strFile, intPos - 1))
			'MsgBox "strFile: " & strfile & vbcrlf & "strResponseFilePath: " & strResponseFilePath & vbcrlf & "strResponseFileName: " & strResponseFileName
			
			'create an array and split up the filename to get the ProcessingDate, Bank, Lockbox, Batch
			Dim arrResult
			arrResult = Split(strResponseFileName , "_")
			strProcessingDate = arrResult(0)
			strProcessingDate = Mid(strProcessingDate, 1, 2) & "/" & _
								Mid(strProcessingDate, 3, 2) & "/" & _
								Mid(strProcessingDate, 5, 4)
			strBank = arrResult(1)
			strLockbox = arrResult(2)
			strBatch = arrResult(3)	
			strSiteID = arrResult(4)		
		End If
		
		'find the IMS Batch Number and get the value on the report
		intResult = InStr(strLineInfo, BATCH_IDENT)
		If intResult = 1 Then
			strIMSBatch = Trim(Mid(strLineInfo, 28, 20))
		End If
	Loop
	
	'convert the integers to strings for the response
	strInvalErr = CStr(intInvalidCount)
	strWarnErr = CStr(intWarningCount)
	strErrors = CStr(intErrorCount)
	
	objTextFile.close

	'Set the return value to true if there were no errors else false
	If intErrorCount = 0 And intInvalidCount = 0 And intWarningCount = 0 And Err.number = 0 Then
		RetVal = 1
	Else
		RetVal = 0
	End If

	' Clean up Objects
	objfso.DeleteFile strFilePath
	Set objfso = Nothing 
	Set objTextFile = Nothing
	
	'return the true or false value for workflow scripting
	ParseFile = RetVal
	
	If Err.number <> 0 Then
		Dim objWSHShell
		Set objWSHShell = CreateObject("WScript.Shell")
		Call objWSHShell.LogEvent( 1, "LTA Response File: ParseFile(" & _
			strFilePath & ", " & strProcessingDate & ", " & strBank & ", " & strLockbox & ", " & _
			strBatch & ", " & strSiteID & ", " & strIMSBatch & ", " & strWarnErr & ", " & strInvalErr & ", " & _
			strErrors & ", " & strTotals & ", " & strUnarchived & ", " & strResponseFilePath & _
			", " & strResponseFileName & ")" & _
			vbNewLine & "Error Number: " & Err.number & vbNewLine & Err.Description)
		Set objWSHShell = Nothing
	End If

End Function

Sub WriteOutputFile(ProcessingDate, BankID, LockboxID, BatchID, SiteID, IMSBatch, WarnErr, InvalErr, Errors, _
					Archived, Unarchived, ResponsePath, ResponseFileName, ReportDocHandle)
					
	On Error Resume Next
	
	'Create file objects
	Dim objFso, objTextFile, tempFile, finishedFile
	
	Set objFso = CreateObject("Scripting.FileSystemObject")
	
	tempFile = ResponsePath & ResponseFileName & ".tmp"
	finishedFile = ResponsePath & ResponseFileName & ".xml"
	
	'check if output directory exist.  If not create it
	If Not objFso.FolderExists(ResponsePath) Then objFso.CreateFolder(ResponsePath)
	
	'Open a temporary file for output
	Set objTextFile = objfso.OpenTextFile(tempFile, ForWriting, True)
	
	'Write root node with attributes
	Dim strLine
	strLine = "<Batch ProcessingDate=""" & ProcessingDate & """ BankID=""" & BankID & _
				""" LockboxID=""" & LockboxID & """ BatchID=""" & BatchID & _
				""" SiteID=""" & SiteID & """ IMSBatch=""" & IMSBatch & _
				""" WarnErr=""" & WarnErr & """ InvalErr=""" & InvalErr & _
				""" Errors=""" & Errors & """ Archived=""" & Archived & _
				""" Unarchived=""" & Unarchived & """ ReportDocHandle=""" & ReportDocHandle & """>"
	objTextFile.WriteLine strLine
	
	
	'Lookup data in the database and loop through all records found
	Dim HostDB, SQLStatement, RS
	Set HostDB = CreateObject("ADODB.Connection")
	HostDB.Open "DSN=" & ODBC & "", "hsi", "wstinol"
	SQLStatement = "select DT.itemtypename, KG.itemnum, KG.kg" & BATCH_SEQUENCE_KEYWORD_NUM & _ 
					", KG.kg" & PAGES_KEYWORD_NUM & " from hsi.keygroupdata" & KEGROUP_NUM & _
					" [KG] Join hsi.itemdata [ID] on KG.itemnum = ID.itemnum" & _
					" Join hsi.doctype [DT] on DT.itemtypenum = ID.itemtypenum" & _
					" where KG.kg" & PROCESSING_DATE_KEYWORD_NUM & _
					" = '" & ProcessingDate & "' and KG.kg" & BANK_ID_KEYWORD_NUM & _
					" = '" & BankID & "' and KG.kg" & LOCKBOX_ID_KEYWORD_NUM & _
					" = '" & LockboxID & "' and KG.kg" & BATCH_ID_KEYWORD_NUM & _
					" = '" & BatchID & "'"

	If SiteID <> "0" Then
		SQLStatement = SQLStatement & 	" and KG.kg" & SITE_ID_KEYWORD_NUM & " = '" & SiteID & "'"
	End If

	SQLStatement = SQLStatement & 	" and ID.status != '16' order by KG.kg" & BATCH_SEQUENCE_KEYWORD_NUM 

'	SQLStatement = "select DT.itemtypename, KG.itemnum, KG.kg" & BATCH_SEQUENCE_KEYWORD_NUM & _ 
'					", KG.kg" & PAGES_KEYWORD_NUM & " from hsi.keygroupdata" & KEGROUP_NUM & _
'					" [KG] Join hsi.itemdata [ID] on KG.itemnum = ID.itemnum" & _
'					" Join hsi.doctype [DT] on DT.itemtypenum = ID.itemtypenum" & _
'					" where KG.kg" & PROCESSING_DATE_KEYWORD_NUM & _
'					" = '" & ProcessingDate & "' and KG.kg" & BANK_ID_KEYWORD_NUM & _
'					" = '" & BankID & "' and KG.kg" & LOCKBOX_ID_KEYWORD_NUM & _
'					" = '" & LockboxID & "' and KG.kg" & BATCH_ID_KEYWORD_NUM & _
'					" = '" & BatchID & "' and KG.kg" & SITE_ID_KEYWORD_NUM & _
'					" = '" & SiteID & "' and ID.status != '16' order by KG.kg" & BATCH_SEQUENCE_KEYWORD_NUM 


'	SQLStatement = "select DT.itemtypename, KG.itemnum, KG.kg" & BATCH_SEQUENCE_KEYWORD_NUM & _ 
'					", KG.kg" & PAGES_KEYWORD_NUM & " from hsi.keygroupdata" & KEGROUP_NUM & _
'					" [KG] Join hsi.itemdata [ID] on KG.itemnum = ID.itemnum" & _
'					" Join hsi.doctype [DT] on DT.itemtypenum = ID.itemtypenum" & _
'					" where KG.kg" & PROCESSING_DATE_KEYWORD_NUM & _
'					" = '" & ProcessingDate & "' and KG.kg" & BANK_ID_KEYWORD_NUM & _
'					" = '" & BankID & "' and KG.kg" & LOCKBOX_ID_KEYWORD_NUM & _
'					" = '" & LockboxID & "' and KG.kg" & BATCH_ID_KEYWORD_NUM & _
'					" = '" & BatchID & "' and ID.status != '16' order by KG.kg" & BATCH_SEQUENCE_KEYWORD_NUM 
	'MsgBox SQLStatement
	Set RS = HostDB.Execute(SQLStatement)
	
	If (rs.EOF And rs.BOF) Then
		'MsgBox ("No Data.")
	Else
		Dim DocHandle, BatchSequence, Pages, DocTypeName
		rs.MoveFirst
		Do Until rs.EOF
			'Get the keywords from the results and write the detail item node out with infomration
			DocHandle = Trim(CStr(RS.Fields("itemnum")))
			BatchSequence = Trim(CStr(RS.Fields("kg" & BATCH_SEQUENCE_KEYWORD_NUM)))
			Pages = Trim(CStr(RS.Fields("kg" & PAGES_KEYWORD_NUM)))
			DocTypeName = Trim(CStr(RS.Fields("itemtypename")))	
			strLine = "    <Item DocHandle=""" & DocHandle & """ BatchSequence=""" & BatchSequence & _
							""" DocTypeName=""" & DocTypeName & """ Pages=""" & Trim(Pages) & """/>"
			objTextFile.WriteLine strLine
			RS.MoveNext
		Loop
	End If

	RS.close
	HostDB.Close	
	
	'Write file closing tag of XML
	strLine = "</Batch>"
	objTextFile.WriteLine strLine
	objTextFile.Close

	'Delete the output file if it exists
	Dim objFile
	If objFSO.FileExists(finishedFile) Then
    	Set objFile = objFSO.GetFile(finishedFile)
    	objFile.Delete
	End If
	'rename the file from .tmp to .dat
	objFso.MoveFile tempFile, finishedFile

	'Clean Up
	Set objFile = Nothing
	Set objTextFile = Nothing
	Set objFso = Nothing
	Set RS = Nothing
	Set HostDB = Nothing

	' Error Handling
	If Err.number <> 0 Then
		Dim objWSHShell
		Set objWSHShell = CreateObject("WScript.Shell")
		Call objWSHShell.LogEvent( 1, "LTA Response File: WriteOutputFile(" & _
			ProcessingDate & ", " & BankID & ", " & LockboxID & ", " & BatchID & ", " & SiteID & ", " & _
			IMSBatch & ", " & WarnErr & ", " & InvalErr & ", " & Errors & ", " & _
			Archived & ", " & Unarchived & ", " & ResponsePath & ", " & ResponseFileName & ", " & _
			ReportDocHandle & ")" & vbNewLine & _
			"Error Number: " & Err.number & vbNewLine & Err.Description)
		Set objWSHShell = Nothing
	End If

End Sub