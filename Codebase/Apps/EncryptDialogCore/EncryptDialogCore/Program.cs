﻿using System;
using DpapiCrypto;

namespace EncryptDialogCore
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                CryptoDpapi cryptoDpapi = new CryptoDpapi();
                var response = cryptoDpapi.Encrypt(args[0]);
                Console.WriteLine(response.ReturnedString);
            }
            else
            {
                Console.WriteLine("ERROR:  No string provided to encrypt");
            }
            
        }
    }
}
