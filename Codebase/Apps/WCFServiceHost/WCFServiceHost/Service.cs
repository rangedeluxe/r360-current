﻿using System;
using System.Collections.Generic;
using System.IdentityModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using WCFServiceHost.Options;
using WCFServiceHost.Services;

namespace WCFServiceHost
{
    public class Service
    {
        private ServiceHost _host;
        private readonly Logger _logger;

        public Service()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public void Start()
        {
            try
            {
                _logger.Debug("Service is starting...");
                var args = Environment.GetCommandLineArgs();
                _logger.Debug("All Args: " + string.Join(" ", args));
                var opts = ProgramOptions.Parse(args);
                if (!opts.IsValid)
                {
                    opts.PrintHelp();
                    throw new Exception("Arguments are missing.");
                }

                // Gather our Assemblies.
                var contractprovider = new ContractTypeProvider(opts.DLLLocation);
                var type = contractprovider.LoadContractType(opts.Contract);
                contractprovider.LoadContractConfiguration();

                if (type == null)
                {
                    _logger.Debug("Could not find the requested type.");
                    return;
                }

                // Start the host.
                _host = new ServiceHost(type, opts.URL);
                _host.Start();
                _logger.Debug("Service is listening for requests...");
                _logger.Debug("URL: " + opts.URL);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                // Throw the error so windows knows it's not running.
                throw;
            }
        }

        public void Stop()
        {
            _logger.Debug("Service is stopping...");
            _host.Dispose();
            _logger.Debug("Service stopped.");
        }
    }
}
