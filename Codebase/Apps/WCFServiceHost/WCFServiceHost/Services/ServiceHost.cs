﻿using System;

namespace WCFServiceHost.Services
{
    public class ServiceHost : IDisposable
    {
        private readonly Type _type;
        private readonly string _url;

        private System.ServiceModel.ServiceHost _host;
        private bool _islistening;

        public ServiceHost(Type type, string url)
        {
            _url = url;
            _islistening = false;
            _type = type;
        }

        public void Dispose()
        {
            Stop();
        }

        public void Start()
        {
            if (_islistening)
                throw new Exception("WCF Service Host is already Started.");

            _islistening = true;
            _host = new System.ServiceModel.ServiceHost(_type, new Uri(_url));

            _host.Open();
        }

        public void Stop()
        {
            _islistening = false;
            _host.Close();
        }
    }
}