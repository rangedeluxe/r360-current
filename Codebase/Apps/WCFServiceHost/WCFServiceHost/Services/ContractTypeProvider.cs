﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;

namespace WCFServiceHost.Services
{
    public class ContractTypeProvider
    {
        public ContractTypeProvider(string dll)
        {
            _dlllocation = dll;
        }

        private string _dlllocation { get; set; }

        private Assembly LoadAssembly()
        {
            return Assembly.LoadFrom(_dlllocation);
        }

        private static void ResetConfigMechanism()
        {
            var fieldInfo = typeof(ConfigurationManager)
                .GetField("s_initState", BindingFlags.NonPublic | 
                                         BindingFlags.Static);
            if (fieldInfo != null)
                fieldInfo.SetValue(null, 0);

            var field = typeof(ConfigurationManager)
                .GetField("s_configSystem", BindingFlags.NonPublic | 
                                            BindingFlags.Static);
            if (field != null)
                field.SetValue(null, null);

            var info = typeof(ConfigurationManager)
                .Assembly
                .GetTypes()
                .First(x => x.FullName == "System.Configuration.ClientConfigPaths")
                .GetField("s_current", BindingFlags.NonPublic | 
                                       BindingFlags.Static);
            if (info != null)
                info.SetValue(null, null);
        }

        public Type LoadContractType(string name)
        {
            var assemb = LoadAssembly();
            var contract = assemb.DefinedTypes
                .FirstOrDefault(x => x.Name == name);
            return contract;
        }

        public void LoadContractConfiguration()
        {
            // This resets the Configuration Manager and points to a new Config file.
            // Takes a little hacking, as the .NET framework isn't really meant to do this.
            // See here: http://stackoverflow.com/questions/6150644/change-default-app-config-at-runtime
            ResetConfigMechanism();
            var config = Path.GetFullPath(Path.Combine(_dlllocation, @"..\..\web.config"));
            AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", config);
        }
    }
}