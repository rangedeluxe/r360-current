﻿using System;
using NLog;
using NDesk.Options;

namespace WCFServiceHost.Options
{
    public class ProgramOptions
    {
        private readonly Logger _logger;
        public ProgramOptions()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public string DLLLocation { get; set; }
        public string URL { get; set; }
        public string Contract { get; set; }

        public static ProgramOptions Parse(string[] args)
        {
            var opts = new ProgramOptions();

            var set = new OptionSet()
            {
                {"d=", "Location of the DLL containing the Service type", x => opts.DLLLocation = x},
                {"u=", "URL to host the Service", x => opts.URL = x},
                {"c=", "The class type name of the service", x => opts.Contract = x},
            };
            set.Parse(args);

            return opts;
        }

        public bool IsValid
        {
            get
            {
                return !string.IsNullOrWhiteSpace(DLLLocation) && !string.IsNullOrWhiteSpace(URL) &&
                       !string.IsNullOrWhiteSpace(Contract);
            }
        }

        public void PrintHelp()
        {
            _logger.Debug("All parameters are required.");
            _logger.Debug("Given parameters:");
            PrintOptions();
        }

        public void PrintOptions()
        {
            _logger.Debug("-------------------------------------------------------");
            _logger.Debug("Parameters:");
            _logger.Debug("DLL Location: {0}", DLLLocation);
            _logger.Debug("URL: {0}", URL);
            _logger.Debug("Contract: {0}", Contract);
            _logger.Debug("-------------------------------------------------------");
        }
    }
}