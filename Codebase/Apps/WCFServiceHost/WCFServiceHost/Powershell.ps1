﻿#  ************** WCFServicesHost Running Topshelf **************   
# Needs Administrative permissions to call New-Service
# Note - Topshelf installs, uninstalls, stops, and starts services a little differently.
#      - We also have to alter the ImagePath key manually, as this is a Topshelf Nuance (It doesn't support args).

# These change based on the executing service.
$instanceName = "OLFOnlineServicesHost"
$dllPath = "C:\workspaces\dev-current\Codebase\OLF\OLFServices\OLFServices\bin\OLFServices.dll"
$url = "https://localhost:44308/HostedOLFServices/OLFOnlineServices.svc"
$className = "OLFOnlineService"

# These shouldn't change much.
$serviceName = "WFS-WCFServicesHost-$instanceName"
$exePath = "C:\workspaces\dev-current\Codebase\Apps\WCFServiceHost\WCFServiceHost\bin\Debug\WCFServiceHost.exe"
$args = "-displayname `"$serviceName`" -servicename `"$serviceName`" -d:`"$dllPath`" -u:$url -c:$className"
$imagePath = "`"$exePath`"  $args"

# This deletes the old service.
Write-Host "Removing $serviceName..."
start-process -FilePath "$exePath" -ArgumentList "stop -servicename:$serviceName" -NoNewWindow -Wait | Write-Output
Start-Sleep -s 3
start-process -FilePath "$exePath" -ArgumentList "uninstall -servicename:$serviceName" -NoNewWindow -Wait | Write-Output
Write-Host "$serviceName removed."

# This installs the new service.
Write-Host "Installing $serviceName..."
start-process -FilePath "$exePath" -ArgumentList "install -servicename:$serviceName" -NoNewWindow -Wait | Write-Output
Start-Sleep -s 3
Write-Host "$serviceName Installed."

# Updates the service to include command-line arguments.
Write-Host "Updating ImagePath Keys..."
Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Services\$serviceName" -Name ImagePath -Value $imagePath
Write-Host "ImagePath Keys Updated"

# This starts the service.
Write-Host "Starting $serviceName..."
start-process -FilePath "$exePath" -ArgumentList "start -servicename:$serviceName" -NoNewWindow -Wait | Write-Output
Write-Host "$serviceName Started."