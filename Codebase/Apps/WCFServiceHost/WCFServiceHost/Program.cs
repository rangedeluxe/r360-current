﻿using System;
using Topshelf;
using WCFServiceHost.Options;
using WCFServiceHost.Services;

namespace WCFServiceHost
{
    public class Program
    {
        public static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                var options = new ProgramOptions();
                x.AddCommandLineDefinition("d", str => options.DLLLocation = str);
                x.AddCommandLineDefinition("u", str => options.URL = str);
                x.AddCommandLineDefinition("c", str => options.Contract = str);
                x.ApplyCommandLine();
                x.Service<Service>(s =>
                {
                    s.ConstructUsing(y => new Service());
                    s.WhenStarted(y => y.Start());
                    s.WhenStopped(y => y.Stop());
                });
                x.RunAsLocalSystem();
                x.StartAutomatically();
            });

        }
    }
}