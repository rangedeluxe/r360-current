﻿using System;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.Common.Crypto;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:
* Date:
*
* Purpose:
*
* Modification History
* CR 23161 JCS 06/01/2011
*    -Updated application to use new 3DES encryption class.
******************************************************************************/

namespace IpoCryptoDialog
{
    public partial class CryptoDialog : Form
    {

        public CryptoDialog()
        {
            InitializeComponent();
        }

        private void encyrptButton_Click(object sender, EventArgs e) {
            string strDecryptedValue;
            string strEncryptedValue;

            encryptedString.Text = "";

            try {
                cCrypto3DES cryp3DES = new cCrypto3DES();
                strDecryptedValue = inputString.Text;

                if (!cryp3DES.Encrypt(strDecryptedValue, out strEncryptedValue)) {
                    MessageBox.Show("Unable to encrypt string. " + cryp3DES.LastException.Message);
                }
                encryptedString.Text = strEncryptedValue;
                Clipboard.SetText(strEncryptedValue);
            }
            catch (Exception ex) {
                MessageBox.Show("Unable to encrypt string " + ex.Message);
            }
        }

       

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Decrypt_Click(object sender, EventArgs e)
        {
            string strEncryptedValue;
            string strDecryptedValue;

            inputString.Text = "";

            try {
                cCrypto3DES cryp3DES = new cCrypto3DES();
                strEncryptedValue = encryptedString.Text;
                if (!cryp3DES.Decrypt(strEncryptedValue, out strDecryptedValue)) {
                    MessageBox.Show("Unable to decrypt string " + cryp3DES.LastException.Message);
                }
                inputString.Text = strDecryptedValue;
                Clipboard.SetText(strDecryptedValue);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to decrypt string " + ex.Message);
            }
        }
    }
}
