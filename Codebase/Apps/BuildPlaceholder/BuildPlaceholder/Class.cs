﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildPlaceholder
{
    /// <summary>
    /// This project is a placeholder for our TFS Builds.  This should remain empty.
    /// Currently, the WCFClients.extract.config build is utilizing this project.
    /// </summary>
    class Class
    {
    }
}
