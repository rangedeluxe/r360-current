﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wfs.R360.DeploymentConfigurationUtilities;

namespace DeploymentUnitValidation {
    [TestClass]
    public class DeploymentConfigUtilValidation {
        [TestMethod]
        public void TryMergeTool() {
            ConfigManager cnfgmgr = new ConfigManager();

            cnfgmgr.ParamFileName = @"..\..\Resources\ParamFile.xml";
            cnfgmgr.DetectConfigChanges(@"..\..\Resources", @"..\..\Resources\Backup", "MergeTestA.txt");
        }

        [TestMethod]
        public void DetectConfigChanges_WithDifferentEncryptedValues_ShouldNotSeeDiff() {
            var configManager = new ConfigManager();
            var firstConfig = 
                "<add key=\"localIni\" value=\"R:\\WFSApps\\RecHub\\bin2\\RecHub.ini\"/>\r\n" +
                "<add key=\"SiteKey\" value=\"IPOnline\"/>\r\n" +
                "<add key=\"Entity\" value=\"AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACTJA/Agig/2vhEdD3dKaVeAAAAAASAAACgAAAAEAAAAPYxO4QcRyozDxOKOVS+mi0IAAAAgyL8jA74DhIUAAAA2hBK24/j43eUOYTkk0VCbjwTvuA=\"/>\r\n" +
                "<add key=\"Username\" value=\"AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACDk+Eil0I9il9wXe7jK/FQAAAAAASAAACgAAAAEAAAAM9wwLr7ShU2XcDjld1eIiUIAAAA5BlkKD71d18UAAAAZWYCN6QLMbbzARq2xbe6urvPhlU=\"/>\r\n" +
                "<add key=\"Password\" value=\"AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAABSPubaRKKfs9sxknKOt47lAAAAAASAAACgAAAAEAAAADYe3pvofj2/Nx4fgteBPGEIAAAA+U3aKnuLfmYUAAAAEcxxf8Tc6AJEF8ypntpVTflELFI=\"/>\r\n" +
                "DBConnection2=Password=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAADs3EuBVPk0/Ty69wc69AVzAAAAAASAAACgAAAAEAAAAEamBoY13rs2cRPlX3TrlVsIAAAApcHhphMviOcUAAAAwaL623EHT9NZ/+RqTcJUkc68nzo=;User ID=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACOVaOmwzKVCdCgcgNuSt3OAAAAAASAAACgAAAAEAAAAGkSgPPgNGECFC/dWlejTOAIAAAAW1S3ssyYJvEUAAAAp6Gv2pp6LcP3RkArOqTQcwEL2ZI=;Initial Catalog=WFSDB_R360;Data Source=";
            var secondConfig =
                "<add key=\"localIni\" value=\"R:\\WFSApps\\RecHub\\bin2\\RecHub.ini\"/>\r\n" +
                "<add key=\"SiteKey\" value=\"IPOnline\"/>\r\n" +
                "<add key=\"Entity\" value=\"AQAAANCMnd8BFdERjHoAwE/Cl+s/Agig/2vhEdD3dKaVeAAAAAASAAACgAAAAEAAAAPBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACTJAYxO4QcRyozDxOKOVS+mi0IAAAAgyL8jA74DhIUAAAA2hBK24/j43eUOYTkk0VCbjwTvuA=\"/>\r\n" +
                "<add key=\"Username\" value=\"AQAAANCMnd8BFdERjHoAwE/Clk+Eil0I9il9wXe7jK/FQAAAAAASAAACgAAAAEAAA+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACDAM9wwLr7ShU2XcDjld1eIiUIAAAA5BlkKD71d18UAAAAZWYCN6QLMbbzARq2xbe6urvPhlU=\"/>\r\n" +
                "<add key=\"Password\" value=\"AQAAANCMnd8BFdERjHoAwE/ClPubaRKKfs9sxknKOt47lAAAAAASAAACgAAAAEAAA+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAABSADYe3pvofj2/Nx4fgteBPGEIAAAA+U3aKnuLfmYUAAAAEcxxf8Tc6AJEF8ypntpVTflELFI=\"/>\r\n" +
                "DBConnection2=Password=AQAAANCMnd8BFdERjHoAwE/AwAAAABAAAADs3EuBVPk0/Ty69wcCl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgA69AVzAAAAAASAAACgAAAAEAAAAEamBoY13rs2cRPlX3TrlVsIAAAApcHhphMviOcUAAAAwaL623EHT9NZ/+RqTcJUkc68nzo=;User ID=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACOVaOmwzKVCdCgcgNuStAEAAAAGkSgPPgNGECFC/dWl3OAAAAAASAAACgAAAejTOAIAAAAW1S3ssyYJvEUAAAAp6Gv2pp6LcP3RkArOqTQcwEL2ZI=;Initial Catalog=WFSDB_R360;Data Source=";

            Assert.IsFalse(configManager.AreConfigContentsDifferent(firstConfig, secondConfig));
        }

        [TestMethod]
        public void NormalizeWhitespace_ConvertsNewLineToSpace() {
            var testValue =
                "<add key=\"localIni\"\r\n" + 
                "value=\"R:\\WFSApps\\RecHub\\bin2\\RecHub.ini\"/>";
            var expectedValue =
                "<add key=\"localIni\" value=\"R:\\WFSApps\\RecHub\\bin2\\RecHub.ini\"/>";

            var normalizedValue = ConfigManager.NormalizeWhitespace(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeWhitespace_ReduceMultipleSpacesToASingleSpace() {
            var testValue =
                "<add key=\"localIni\"  value=\"R:\\WFSApps\\RecHub\\bin2\\RecHub.ini\"/>";
            var expectedValue =
                "<add key=\"localIni\" value=\"R:\\WFSApps\\RecHub\\bin2\\RecHub.ini\"/>";

            var normalizedValue = ConfigManager.NormalizeWhitespace(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsEncryptedEntityToPlaceHolder() {
            var configManager = new ConfigManager();
            var testValue =
                "<add key=\"Entity\" value=\"AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACTJA/Agig/2vhEdD3dKaVeAAAAAASAAACgAAAAEAAAAPYxO4QcRyozDxOKOVS+mi0IAAAAgyL8jA74DhIUAAAA2hBK24/j43eUOYTkk0VCbjwTvuA=\"/>";
            var expectedValue =
                "<add key=\"Entity\" value=\"[Encrypted Value]\"/>";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsEncryptedUsernameToPlaceHolder() {
            var configManager = new ConfigManager();
            var testValue =
                "<add key=\"Username\" value=\"AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACDk+Eil0I9il9wXe7jK/FQAAAAAASAAACgAAAAEAAAAM9wwLr7ShU2XcDjld1eIiUIAAAA5BlkKD71d18UAAAAZWYCN6QLMbbzARq2xbe6urvPhlU=\"/>";
            var expectedValue =
                "<add key=\"Username\" value=\"[Encrypted Value]\"/>";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsEncryptedPasswordToPlaceHolder() {
            var configManager = new ConfigManager();
            var testValue =
                "<add key=\"Password\" value=\"AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAABSPubaRKKfs9sxknKOt47lAAAAAASAAACgAAAAEAAAADYe3pvofj2/Nx4fgteBPGEIAAAA+U3aKnuLfmYUAAAAEcxxf8Tc6AJEF8ypntpVTflELFI=\"/>";
            var expectedValue =
                "<add key=\"Password\" value=\"[Encrypted Value]\"/>";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsDBConnectionEncryptedPasswordAndUserIDToPlaceHolders() {
            var configManager = new ConfigManager();
            var testValue =
                "DBConnection2=Password=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAADs3EuBVPk0/Ty69wc69AVzAAAAAASAAACgAAAAEAAAAEamBoY13rs2cRPlX3TrlVsIAAAApcHhphMviOcUAAAAwaL623EHT9NZ/+RqTcJUkc68nzo=;User ID=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACOVaOmwzKVCdCgcgNuSt3OAAAAAASAAACgAAAAEAAAAGkSgPPgNGECFC/dWlejTOAIAAAAW1S3ssyYJvEUAAAAp6Gv2pp6LcP3RkArOqTQcwEL2ZI=;Initial Catalog=WFSDB_R360;Data Source=";
            var expectedValue =
                "DBConnection2=Password=[Encrypted Value];User ID=[Encrypted Value];Initial Catalog=WFSDB_R360;Data Source=";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsDBConnectionEncryptedPasswordAndUserIDToPlaceHolders_WithInitialCatalogFirst() {
            var configManager = new ConfigManager();
            var testValue =
                "DBConnection2=Initial Catalog=WFSDB_R360;Password=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAADs3EuBVPk0/Ty69wc69AVzAAAAAASAAACgAAAAEAAAAEamBoY13rs2cRPlX3TrlVsIAAAApcHhphMviOcUAAAAwaL623EHT9NZ/+RqTcJUkc68nzo=;User ID=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACOVaOmwzKVCdCgcgNuSt3OAAAAAASAAACgAAAAEAAAAGkSgPPgNGECFC/dWlejTOAIAAAAW1S3ssyYJvEUAAAAp6Gv2pp6LcP3RkArOqTQcwEL2ZI=;Data Source=";
            var expectedValue =
                "DBConnection2=Initial Catalog=WFSDB_R360;Password=[Encrypted Value];User ID=[Encrypted Value];Data Source=";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsDBConnectionEncryptedPasswordAndUserIDToPlaceHolders_WithSpaceAfterSimicolon() {
            var configManager = new ConfigManager();
            var testValue =
                "DBConnection2=Initial Catalog=WFSDB_R360; Password=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAADs3EuBVPk0/Ty69wc69AVzAAAAAASAAACgAAAAEAAAAEamBoY13rs2cRPlX3TrlVsIAAAApcHhphMviOcUAAAAwaL623EHT9NZ/+RqTcJUkc68nzo=; User ID=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACOVaOmwzKVCdCgcgNuSt3OAAAAAASAAACgAAAAEAAAAGkSgPPgNGECFC/dWlejTOAIAAAAW1S3ssyYJvEUAAAAp6Gv2pp6LcP3RkArOqTQcwEL2ZI=; Data Source=";
            var expectedValue =
                "DBConnection2=Initial Catalog=WFSDB_R360; Password=[Encrypted Value]; User ID=[Encrypted Value]; Data Source=";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsDBConnectionEncryptedPasswordAndUserIDToPlaceHolders_WithEncryptionAtEndOfLine() {
            var configManager = new ConfigManager();
            var testValue =
                "DBConnection2=Initial Catalog=WFSDB_R360;Data Source=;Password=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAADs3EuBVPk0/Ty69wc69AVzAAAAAASAAACgAAAAEAAAAEamBoY13rs2cRPlX3TrlVsIAAAApcHhphMviOcUAAAAwaL623EHT9NZ/+RqTcJUkc68nzo=;User ID=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAA5kP7Mm41wECWHyc8UuzqHgQAAAACAAAAAAADZgAAwAAAABAAAACOVaOmwzKVCdCgcgNuSt3OAAAAAASAAACgAAAAEAAAAGkSgPPgNGECFC/dWlejTOAIAAAAW1S3ssyYJvEUAAAAp6Gv2pp6LcP3RkArOqTQcwEL2ZI=";
            var expectedValue =
                "DBConnection2=Initial Catalog=WFSDB_R360;Data Source=;Password=[Encrypted Value];User ID=[Encrypted Value]";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsEncryptedWebCredentialsToPlaceHolder() {
            var configManager = new ConfigManager();
            var testValue =
                "<wfs.raam.trustedsubsystem.credential encryptedUsername=\"AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAb1Xw8GNqOkK6tExh+RTIUgQAAAACAAAAAAAQZgAAAAEAACAAAAAb/MHPFp5LxazbDWslJP15g4/3aUZ/nz2PYzjOmx5pgwAAAAAOgAAAAAIAACAAAAASWeaIXNkri6PcshTyrIJkcIx7rtFyiW+LUtj1OoZBdyAAAABuLVGXLixU2c9ZrGC+NctJ3Z6pqlzWMLpZiNmjeWuHIkAAAABcAQyXMDufmsHk0A/RlEXT1pPDUir4Rh2Cekr0uBN8Jrh1vPJg+5pmdmZ+WUpPvZK1PmoqI6XeH9aqP+9mHKSY\"\r\n" +
                                        "encryptedPassword=\"AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAb1Xw8GNqOkK6tExh+RTIUgQAAAACAAAAAAAQZgAAAAEAACAAAACytKBwnQlQ3lpP4W35Of0Rv1iJf7w40ZejeRn9vsA/QgAAAAAOgAAAAAIAACAAAADlEtykXY/T9ZzycioUHz9l7KcTh1adkva03ORSZJuR/yAAAACHJO1iT+T/8YgtjGOAfwRQYq7Zz8B9lFz+OKq0tHWMREAAAAC8JmfSAR3fYLvvEU3xL9SMheLlhvZBPXMuHmN0onVjt0Lj5+oLop/Bywobu3oOnIj0Zy2isfxKjUOZ0ijMZSki\"";
            var expectedValue =
                "<wfs.raam.trustedsubsystem.credential encryptedUsername=\"[Encrypted Value]\"\r\n" +
                                        "encryptedPassword=\"[Encrypted Value]\"";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }

        [TestMethod]
        public void NormalizeEncryptedDataByLine_ConvertsEncryptedAppConfigConnectionString() {
            var configManager = new ConfigManager();
            var testValue =
                "<add key=\"ConnectionString\" value=\"Password=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAI9Fomvg8kUm2GDmz467M4AQAAAACAAAAAAAQZgAAAAEAACAAAABLsn2/l3qydxh5v/ClkdOzol7zUHLCEYe+pD5C8wAX2QAAAAAOgAAAAAIAACAAAABISCw9LnIlK2oqMlQH3yrHZ++R9tUXJuAShGHo2Z3NRhAAAAA2pvWe1biALDmJlF3wAEsCQAAAAJ5kANjl8E1+7Tvv3uhTMcXGpL8GKWwcQDa0IRzFEqr4Vvp96ZR3/vHP+zjZ8NJMHEBYS2uDO4XZS3UHxS1Lnpo=;User ID=AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAI9Fomvg8kUm2GDmz467M4AQAAAACAAAAAAAQZgAAAAEAACAAAAAnhO+k0KblrR7mxBR5da0M6XrsOXrLtgmpx0hKGjCHVQAAAAAOgAAAAAIAACAAAAC6Azt7kVsY3ymrOOMnO3md0Y7eI+grZkUgkaag136dsUAAAAAHcvCsK2xWfjObxE9pG6KTtpo8uaCX3lYrTIvIi8peT4pXxqXnSFa9F/NY+kPuGNuGAPoXf3zlrHwYnmQ6NCeBQAAAANAnEHye4h6hjASJi7yHi0ggEGpS/xcKxONhVZDHfwBCROFycRAp0vV2tt9sW/h59BIJF3tjO/TCLlDWKL90DeA=;Initial Catalog=WFSDB_R360;Data Source=rechubqadb02.qalabs.nwk\"/>";
            var expectedValue =
                "<add key=\"ConnectionString\" value=\"Password=[Encrypted Value];User ID=[Encrypted Value];Initial Catalog=WFSDB_R360;Data Source=rechubqadb02.qalabs.nwk\"/>";

            var normalizedValue = configManager.NormalizeEncryptedDataByLine(testValue);

            Assert.AreEqual(expectedValue, normalizedValue);
        }
    }
}
