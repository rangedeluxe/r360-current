﻿using System.ComponentModel;
using System.IO;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wfs.R360.DeploymentConfigurationUtilities;

namespace DeploymentUnitTests
{
    [TestClass]
    public class ConfigManagerTests
    {
        [TestInitialize]
        public void SetUp()
        {
            File.Delete(ConfigForm.DeploymentParamsPath);
        }

        private static void AssertDeploymentParamsFileExists(bool expected)
        {
            Assert.AreEqual(expected, File.Exists(ConfigForm.DeploymentParamsPath), ConfigForm.DeploymentParamsPath);
        }
        private static string CreateDbParamFileAndGetDbParamValue(string paramName)
        {
            var manager = new ConfigManager {Ui = new FakeConfigUi(okDbConfigForm: true)};
            manager.GetDBParams(prompt: true);
            return manager.GetDBParamValue(paramName);
        }

        [TestMethod]
        public void TestReplaceDriveReferences()
        {
            string config = @"Test D:\WFSApps\Something D:\WFSApps\Logs\ D:\WFSApps\RecHub\ D:\WFSApps\RecHub\Data\LogFiles\ D:\WFSApps\RAAM\Something D:\WFSApps\RAAM\Logs\ Test Test";
            string temp = config;
            string expected = @"Test E:\WFSApps\Something F:\WFSApps\Logs\ E:\WFSApps\RecHub\ F:\WFSApps\RecHub\Data\LogFiles\ E:\WFSApps\RAAM\Something F:\WFSApps\RAAM\Logs\ Test Test";

            var logFolders = new string[] { @"D:\WFSApps\Logs\", @"D:\WFSApps\Raam\Logs\", @"D:\WFSApps\RecHub\Data\Log" };
            var installFolders = new string[] { @"D:\WFSApps\" };

            ConfigManager.ReplaceDriveReferences(ref temp, installFolders, logFolders, "E:");
            ConfigManager.ReplaceDriveReferences(ref temp, logFolders, null, "F:");

            Assert.AreEqual(expected, temp);

            // Try reverse order
            temp = config;
            ConfigManager.ReplaceDriveReferences(ref temp, logFolders, null, "F:");
            ConfigManager.ReplaceDriveReferences(ref temp, installFolders, logFolders, "E:");

            Assert.AreEqual(expected, temp);
        }
        [TestMethod]
        public void GetParams_WithNoPrompt_DoesNotCreateParamFile()
        {
            var manager = new ConfigManager {Ui = new FakeConfigUi()};
            manager.GetParams(prompt: false, tier: "WEB");
            AssertDeploymentParamsFileExists(false);
        }
        [TestMethod]
        public void GetParams_AfterCancel_DoesNotCreateParamFile()
        {
            var manager = new ConfigManager {Ui = new FakeConfigUi(okConfigForm: false)};
            manager.GetParams(prompt: true, tier: "WEB");
            AssertDeploymentParamsFileExists(false);
        }
        [TestMethod]
        public void GetParams_AfterOk_CreatesParamFile()
        {
            var manager = new ConfigManager {Ui = new FakeConfigUi(okConfigForm: true)};
            manager.GetParams(prompt: true, tier: "WEB");
            AssertDeploymentParamsFileExists(true);
        }

        [TestMethod]
        public void GetParamValue_RunAsWinService_ValueShouldBe0() 
        {
            ConfigManager_TestWrapper manager = new ConfigManager_TestWrapper();
            manager.ParameterXMLDoc=XDocument.Parse(Properties.Resources.ParamFile_UseIIS);
            Assert.AreEqual(manager.GetParamValue("RunAsWinService"), "0");
        }

        [TestMethod]
        public void GetParamValue_RunAsWinService_ValueShouldBe1() {
            ConfigManager_TestWrapper manager = new ConfigManager_TestWrapper();
            manager.ParameterXMLDoc = XDocument.Parse(Properties.Resources.ParamFile_UseWindowServices);
            Assert.AreEqual(manager.GetParamValue("RunAsWinService"), "1");
        }

        [TestMethod]
        public void GetDBParams_WithNoPrompt_DoesNotCreateParamFile()
        {
            var manager = new ConfigManager {Ui = new FakeConfigUi()};
            manager.GetDBParams(prompt: false);
            AssertDeploymentParamsFileExists(false);
        }
        [TestMethod]
        public void GetDBParams_AfterCancel_DoesNotCreateParamFile()
        {
            var manager = new ConfigManager {Ui = new FakeConfigUi(okDbConfigForm: false)};
            manager.GetDBParams(prompt: true);
            AssertDeploymentParamsFileExists(false);
        }
        [TestMethod]
        public void GetDBParams_AfterOk_CreatesParamFile()
        {
            var manager = new ConfigManager {Ui = new FakeConfigUi(okDbConfigForm: true)};
            manager.GetDBParams(prompt: true);
            AssertDeploymentParamsFileExists(true);
        }
        [TestMethod]
        public void GetDBParamValue_AfterParamFileInitiallyCreatedByDbDialog_SSIS_CONFIG_DB_NAME()
        {
            var paramValue = CreateDbParamFileAndGetDbParamValue("SSIS_CONFIG_DB_NAME");
            Assert.AreEqual("WFS_SSIS_Configuration", paramValue);
        }
        [TestMethod]
        public void GetDBParamValue_AfterParamFileInitiallyCreatedByDbDialog_SSIS_LOG_DB_NAME()
        {
            var paramValue = CreateDbParamFileAndGetDbParamValue("SSIS_LOG_DB_NAME");
            Assert.AreEqual("WFS_SSIS_Logging", paramValue);
        }
    }
}
