﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Wfs.R360.DeploymentConfigurationUtilities;

namespace DeploymentUnitTests {
    public class ConfigManager_TestWrapper:ConfigManager  {
        public XDocument ParameterXMLDoc {
            get {
                return _paramXDoc;
            }
            set {
                ParamFileName = "Direct Content";
                _paramXDoc = value;
            }
        }
    }
}
