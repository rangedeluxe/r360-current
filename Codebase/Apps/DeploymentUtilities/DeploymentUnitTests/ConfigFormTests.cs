﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wfs.R360.DeploymentConfigurationUtilities;

namespace DeploymentUnitTests
{
    [TestClass]
    public class ConfigFormTests
    {
        [TestMethod]
        public void EachParam_HasAControl()
        {
            using (var form = new ConfigForm(new FakeConfigUi()))
            {
                foreach (var paramPair in ConfigForm.Params)
                {
                    Assert.IsNotNull(form.GetParameterControl(paramPair.Key), paramPair.Key);
                }
            }
        }
    }
}