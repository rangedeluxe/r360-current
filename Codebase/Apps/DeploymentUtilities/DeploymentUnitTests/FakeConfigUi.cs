﻿using System;
using Wfs.R360.DeploymentConfigurationUtilities;

namespace DeploymentUnitTests
{
    public class FakeConfigUi : IConfigUi
    {
        private readonly bool? _okConfigForm;
        private readonly bool? _okDbConfigForm;

        public FakeConfigUi(bool? okConfigForm = null, bool? okDbConfigForm = null)
        {
            _okConfigForm = okConfigForm;
            _okDbConfigForm = okDbConfigForm;
        }

        public void ShowConfigDialog(ConfigForm form)
        {
            if (_okConfigForm == null)
            {
                throw new InvalidOperationException("No expectation was set for ShowConfigDialog");
            }

            form.ConfigForm_Load(null, null);
            if (_okConfigForm.Value)
            {
                form._btnSave_Click(null, null);
            }
            else
            {
                form._btnCancel_Click(null, null);
            }
        }
        public void ShowDbConfigDialog(DBConfigForm form)
        {
            if (_okDbConfigForm == null)
            {
                throw new InvalidOperationException("No expectation was set for ShowDbConfigDialog");
            }

            form.DBConfigForm_Load(null, null);
            if (_okDbConfigForm.Value)
            {
                form._btnSave_Click(null, null);
            }
            else
            {
                form._btnCancel_Click(null, null);
            }
        }
        public void ShowMessage(string message, string title = null)
        {
        }
    }
}