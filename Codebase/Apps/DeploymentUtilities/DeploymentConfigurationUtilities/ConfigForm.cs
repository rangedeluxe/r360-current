﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Wfs.R360.DeploymentConfigurationUtilities
{
	public partial class ConfigForm : Form
	{
        public static string DeploymentParamsPath
        {
            get
            {
                string basePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
                var deploymentParamsPath = Path.Combine(basePath, "DeploymentParams.xml");
                return deploymentParamsPath;
            }
        }
        public string ParamFileName { get; set; }

        public class ParamMap
        {
            public ParamMap(bool encrypt = false, bool encryptOutput = false, bool maskPassword = false)
            {
                Encrypt = encrypt;
                EncryptOutput = encryptOutput;
                MaskPassword = maskPassword;
            }

            public bool Encrypt { get; private set; }
            public bool EncryptOutput { get; private set; }
            public bool MaskPassword { get; private set; }
        }

	    public static readonly IReadOnlyDictionary<string, ParamMap> Params = new Dictionary<string, ParamMap>
	    {
	        {"RAAM_THUMBPRINT", new ParamMap()},
	        {"RAAM_VALIDATION_KEY_512", new ParamMap(encrypt: true)},
	        {"RAAM_DECRYPTION_KEY_256", new ParamMap(encrypt: true)},
	        {"WEB_SERVER", new ParamMap()},
	        {"RAAM_WEB_SERVER", new ParamMap()},
	        {"SERVER_NAME", new ParamMap()},
	        {"APP_SERVER", new ParamMap()},
	        {"NOTIFICATION_SERVICES_SERVER_2.02", new ParamMap()},
	        {"RAAM_APP_SERVER", new ParamMap()},
	        {"RAAM_SYSTEM_USER", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"RAAM_SYSTEM_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"InstallDrive", new ParamMap()},
	        {"LogDrive", new ParamMap()},
	        {"FRAMEWORK_DB_SERVER", new ParamMap()},
	        {"RAAM_DB_SERVER", new ParamMap()},
	        {"RECHUB_DB_SERVER", new ParamMap()},
	        {"RECHUB_DB_NAME", new ParamMap()},
	        {"RECHUB_DBCONN2_USER", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"RECHUB_DBCONN2_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
            {"RECHUB_DBCONNCOMEXCEP_DB_SERVER", new ParamMap()},
            {"RECHUB_DBCONNCOMEXCEP_DB_NAME", new ParamMap()},
            {"RECHUB_DBCONNCOMEXCEP_USER", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"RECHUB_DBCONNCOMEXCEP_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"RECHUB_DBCONNIP_USER", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"RECHUB_DBCONNIP_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"RECHUB_DBCONN2_USER2", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"RECHUB_DBCONN2_PASSWORD2", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"RECHUB_DBCONNIP_USER2", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"RECHUB_DBCONNIP_PASSWORD2", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"RECHUB_SSRS_SERVER", new ParamMap()},
	        {"INTEGRAPAY_DB_SERVER", new ParamMap()},
	        {"INTEGRAPAY_DB_NAME", new ParamMap()},
	        {"AppDomainUser", new ParamMap()},
	        {"AppDomainUserPassword", new ParamMap(encrypt: true, maskPassword: true)},
	        {"MergeUtilityPath", new ParamMap()},
	        {"FILE_SERVER", new ParamMap()},
	        {"SSL_CERT", new ParamMap()},
	        {"SSRS_REPORT_FOLDER", new ParamMap()},
	        {"REPORT_FILE_SHARE", new ParamMap()},
	        {"SSRS_USER", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"SSRS_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"SSRS_USERDOMAIN", new ParamMap(encryptOutput: true)},
	        {"RECHUB_DBCONNEXTRACT_USER", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"RECHUB_DBCONNEXTRACT_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"EXTRACT_SERVICE_ENTITY", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"EXTRACT_SERVICE_LOGON_NAME", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"EXTRACT_SERVICE_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"CLIENT_LOGON_NAME", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"CLIENT_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
	        {"FIT_CLIENT_LOGON_NAME", new ParamMap(encrypt: true, encryptOutput: true)},
	        {"FIT_CLIENT_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
            {"FIT_INTEGRAPAY_DB_SERVER", new ParamMap()},
            {"FIT_INTEGRAPAY_DB_NAME", new ParamMap()},
            {"FIT_INTEGRAPAY_DB_USER", new ParamMap(encrypt: true, encryptOutput: true)},
            {"FIT_INTEGRAPAY_DB_PASSWORD", new ParamMap(encrypt: true, encryptOutput: true, maskPassword: true)},
            {"FIT_CENDS_PATH", new ParamMap()},
            {"ServerNamesWeb", new ParamMap()},
	        {"ServerNamesApp", new ParamMap()},
	        {"ServerNamesFile", new ParamMap()},
            {"FILE_HOST_SERVICES_WITH", new ParamMap()}
	    };
	    private readonly Dictionary<string, TextBox> _parameterControls;
	    private readonly IConfigUi _ui;

        public ConfigForm(IConfigUi ui)
        {
            _ui = ui;

            InitializeComponent();

            const string defaultServerNames = "localhost\r\n127.0.0.1\r\n";
            _serverNamesWeb.TextBox.Text = defaultServerNames;
            _serverNamesApp.TextBox.Text = defaultServerNames;
            _serverNamesFile.TextBox.Text = defaultServerNames;

            _txtParamFile.Text = DeploymentParamsPath;

            _parameterControls = new Dictionary<string, TextBox>
            {
                {"RAAM_THUMBPRINT", _txtThumbPrint},
                {"RAAM_VALIDATION_KEY_512", _txtRaamValidationKey2},
                {"RAAM_DECRYPTION_KEY_256", _txtRaamDecryptionKey2},
                {"WEB_SERVER", _txtWebServerName},
                {"RAAM_WEB_SERVER", _txtRaamWebServerName},
                {"SERVER_NAME", _txtDirectServerName},
                {"APP_SERVER", _txtAppServerName},
                {"NOTIFICATION_SERVICES_SERVER_2.02", _txtNotificationServicesServerName},
                {"RAAM_APP_SERVER", _txtRaamAppServerName},
                {"RAAM_SYSTEM_USER", _txtRaamSystemUserName},
                {"RAAM_SYSTEM_PASSWORD", _txtRaamSystemUserPassword},
                {"InstallDrive", _txtInstallDrive},
                {"LogDrive", _txtLogDrive},
                {"FRAMEWORK_DB_SERVER", _txtDBFrameworkServer},
                {"RAAM_DB_SERVER", _txtDBRaamServer},
                {"RECHUB_DB_SERVER", _txtDBRecHubServer},
                {"RECHUB_DB_NAME", _txtDBRecHubDBName},
                {"RECHUB_DBCONN2_USER", _txtDBConnection2User},
                {"RECHUB_DBCONN2_PASSWORD", _txtDBConnection2Password},
                {"RECHUB_DBCONNCOMEXCEP_DB_SERVER", _txtDBConnectionComExcepServer},
                {"RECHUB_DBCONNCOMEXCEP_DB_NAME", _txtDBConnectionComExcepDBName},
                {"RECHUB_DBCONNCOMEXCEP_USER", _txtDBConnectionComExcepUser},
                {"RECHUB_DBCONNCOMEXCEP_PASSWORD", _txtDBConnectionComExcepPassword},
                {"RECHUB_DBCONNIP_USER", _txtDBConnectionIPUser},
                {"RECHUB_DBCONNIP_PASSWORD", _txtDBConnectionIPassword},
                {"RECHUB_DBCONN2_USER2", _txtDBConnection2User2},
                {"RECHUB_DBCONN2_PASSWORD2", _txtDBConnection2Password2},
                {"RECHUB_DBCONNIP_USER2", _txtDBConnectionIPUser2},
                {"RECHUB_DBCONNIP_PASSWORD2", _txtDBConnectionIPassword2},
                {"RECHUB_SSRS_SERVER", _txtRecHubSsrsServer},
                {"INTEGRAPAY_DB_SERVER", _txtDBIntegraPAYServer},
                {"INTEGRAPAY_DB_NAME", _txtDBIntegraPAYDBName},
                {"AppDomainUser", _txtAppDomainUser},
                {"AppDomainUserPassword", _txtAppDomainUserPassword},
                {"MergeUtilityPath", _txtMergeUtilityPath},
                {"FILE_SERVER", _txtFileServer},
                {"SSL_CERT", _txtSSLCert},
                {"SSRS_REPORT_FOLDER", _txtSSRSFolder},
                {"REPORT_FILE_SHARE", _txtReportsFileShare},
                {"SSRS_USER", _txtSSRSUser},
                {"SSRS_PASSWORD", _txtSSRSPassword},
                {"SSRS_USERDOMAIN", _txtSSRSDomain},
                {"RECHUB_DBCONNEXTRACT_USER", _txtDBConnectionExtractUser},
                {"RECHUB_DBCONNEXTRACT_PASSWORD", _txtDBConnectionExtractPassword},
                {"EXTRACT_SERVICE_ENTITY", _txtExtractEntity},
                {"EXTRACT_SERVICE_LOGON_NAME", _txtExtractUser},
                {"EXTRACT_SERVICE_PASSWORD", _txtExtractPassword},
                {"CLIENT_LOGON_NAME", _txtDitClientUser},
                {"CLIENT_PASSWORD", _txtDitClientPassword},
                {"FIT_CLIENT_LOGON_NAME", _txtFitClientUser},
                {"FIT_CLIENT_PASSWORD", _txtFitClientPassword},
                {"FIT_INTEGRAPAY_DB_SERVER", txtFITIntegraPAYServer},
                {"FIT_INTEGRAPAY_DB_NAME", txtFITIntegraPAYDatabase},
                {"FIT_INTEGRAPAY_DB_USER", txtFITIntegraPAYUser},
                {"FIT_INTEGRAPAY_DB_PASSWORD", txtFITIntegraPAYPassword},
                {"FIT_CENDS_PATH", txtCENDSPath},
                {"ServerNamesWeb", _serverNamesWeb.TextBox},
                {"ServerNamesApp", _serverNamesApp.TextBox},
                {"ServerNamesFile", _serverNamesFile.TextBox},
                {"FILE_HOST_SERVICES_WITH", _txtHostServicesWith}
            };
            SetupComboBox(_cboHostServicesWith, _txtHostServicesWith);
        }

	    public TextBox GetParameterControl(string parameterName)
	    {
	        return _parameterControls[parameterName];
	    }

	    public void ConfigForm_Load(object sender, EventArgs e)
		{
		    foreach (var paramMapPair in Params.Where(paramMapPair => paramMapPair.Value.MaskPassword))
		    {
		        GetParameterControl(paramMapPair.Key).UseSystemPasswordChar = true;
		    }

		    LoadPotentialCerts();

			// Previous Parameters
			try
			{
				if (File.Exists(_txtParamFile.Text))
				{
					DeserializeParams();
				}
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}

			InitCertFormFields();
		}

		public void ValidateParamFile()
		{
			ParamFileName = null;
			try
			{
				if (File.Exists(_txtParamFile.Text))
				{
					// TODO: Validate parameters in some way...? DeserializeParams();
					ParamFileName = _txtParamFile.Text;
				}
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}
		}

		private void RemoveTabs(params TabPage[] pages)
		{
			// Note: If we remove tab pages from the UI containers, it messes up tooltips...  Doing this keeps them "in the container hierarchy"...
			TabControl t = new TabControl();
			t.Visible = false;
			this.Controls.Add(t);
			foreach (var page in pages)
			{
				_tabControl.TabPages.Remove(page);
				t.TabPages.Add(page);
			}
		}

		public void OnlyWebParams()
		{
			RemoveTabs(_tabAppTier, _tabFileTier, _tabClientDit, _tabClientFit, _tabClientExtract);
		}

		public void OnlyAppParams()
		{
			RemoveTabs(_tabWebTier, _tabFileTier, _tabClientDit, _tabClientFit, _tabClientExtract);
		}

		public void OnlyDriveParams()
		{
			RemoveTabs(_tabWebTier, _tabAppTier, _tabFileTier, _tabClientDit, _tabClientFit, _tabClientExtract);
			_grpRaamIntegration.Enabled = false;
		}

		public void OnlyFileParams()
		{
			RemoveTabs(_tabWebTier, _tabAppTier, _tabClientDit, _tabClientFit);
		}

		public void OnlyClientDitParams()
		{
			RemoveTabs(_tabWebTier, _tabAppTier, _tabFileTier, _tabClientFit, _tabClientExtract);
			_grpRaamIntegration.Enabled = false;
		}

		public void OnlyClientFitParams()
		{
			RemoveTabs(_tabWebTier, _tabAppTier, _tabFileTier, _tabClientDit, _tabClientExtract);
			_grpRaamIntegration.Enabled = false;
		}

		public void OnlyClientExtractParams()
		{
			RemoveTabs(_tabWebTier, _tabAppTier, _tabFileTier, _tabClientDit, _tabClientFit);
		}

		#region Button-Click Events

		private void _btnGenerateKeys_Click(object sender, EventArgs e)
		{
			RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider();
			byte[] validation = new byte[64];
			byte[] decrypt = new byte[32];
			rnd.GetBytes(validation);
			rnd.GetBytes(decrypt);
			_txtRaamValidationKey2.Text = BitConverter.ToString(validation).Replace("-", "");
			_txtRaamDecryptionKey2.Text = BitConverter.ToString(decrypt).Replace("-", "");
		}

	    public void _btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				// TODO: Validate all of the parameters - length, allowed characters, format, error on "localhost", etc.
				ParamFileName = _txtParamFile.Text;
				SerializeParams();
				DialogResult = System.Windows.Forms.DialogResult.OK;
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}
		}

	    public void _btnCancel_Click(object sender, EventArgs e)
		{
			ParamFileName = null;
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void _btnLoadParams_Click(object sender, EventArgs e)
		{
			try
			{
				DeserializeParams();
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}
		}

		#endregion

		#region Saving / Loading

		private void SerializeParams()
		{
			using (XmlWriter xWriter = XmlWriter.Create(ParamFileName, new XmlWriterSettings() { OmitXmlDeclaration = true, Indent = true }))
			{
				xWriter.WriteStartElement("Params");
				foreach (var entry in Params)
				{
				    var parameterControl = GetParameterControl(entry.Key);
				    xWriter.WriteElementString(
				        entry.Key,
				        entry.Value.Encrypt
				            ? Encrypt(parameterControl.Text, entry.Key)
				            : parameterControl.Text);
				}
			    xWriter.WriteEndElement();
			}
		}

		private void DeserializeParams()
		{
			XDocument xDoc = XDocument.Load(_txtParamFile.Text);
			foreach (var entry in Params)
			{
				var elem = xDoc.Descendants(entry.Key).FirstOrDefault();
			    if (elem != null)
			    {
			        // XElement normalizes line breaks to \n (even when the file is saved as \r\n).
			        // TextBoxes fail to understand plain \n, so normalize back to \r\n.
			        GetParameterControl(entry.Key).Text = entry.Value.Encrypt
			            ? Decrypt(elem.Value, entry.Key, _ui)
			            : NormalizeLineEndings(elem.Value);
			    }
			}
        }

        private string NormalizeLineEndings(string value)
        {
            return value
                .Replace("\r\n", "\n")
                .Replace("\r", "\n")
                .Replace("\n", "\r\n");
        }

		private static readonly byte[] _entropy = new byte[] { 31, 42, 57, 22 }; // Arbitrary, but determinant...
		internal string Encrypt(string src, string entropy)
		{
			byte[] fullEntropy = _entropy.Concat(Encoding.UTF8.GetBytes(entropy)).ToArray();
			byte[] orig = Encoding.UTF8.GetBytes(src);
			byte[] secure = ProtectedData.Protect(orig, fullEntropy, DataProtectionScope.LocalMachine);
			return BitConverter.ToString(secure).Replace("-", "");
		}

        internal static string Decrypt(string src, string entropy, IConfigUi ui)
        {
            try
            {
                byte[] fullEntropy = _entropy.Concat(Encoding.UTF8.GetBytes(entropy)).ToArray();
                byte[] secured = FromHex(src);
                byte[] orig = ProtectedData.Unprotect(secured, fullEntropy, DataProtectionScope.LocalMachine);
                return Encoding.UTF8.GetString(orig);
            }
            catch
            {
                ui.ShowMessage("Error decrypting value for '" + entropy + "'.", "Decryption error");
                return string.Empty;
            }
        }

		private static byte[] FromHex(string src)
		{
			byte[] result = new byte[src.Length / 2];
			for (int i = 0; i < src.Length; i += 2)
			{
				result[i / 2] = Byte.Parse(src.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
			}
			return result;
		}

		#endregion

		#region Certificate Helpers

		public class CertItem
		{
			public string Description { get; set; }
			public string ID { get; set; }
		}

		private void LoadPotentialCerts()
		{
			// SSL Certificates
			try
			{
				var certs = GetPotentialSSLCerts();
				var certItems = certs.Select(o => new CertItem
				{
					ID = o.Thumbprint,
					Description =
						o.NotAfter.ToString("yyyy-MM-dd") + ": "
						+ (string.IsNullOrEmpty(o.FriendlyName) ? "" : "\"" + o.FriendlyName + "\" ")
						+ o.Subject
						+ " [Issuer:] " + o.Issuer
				}).ToArray();
				_cboSSLCert.Items.Clear();
				_cboSSLCert.Items.Add(new CertItem { Description = "<None>", ID = "" });
				_cboSSLCert.Items.AddRange(certItems);
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}

			// SSL Certificates
			try
			{
				var certs = GetPotentialRAAMCerts();
				var certItems = certs.Select(o => new CertItem
				{
					ID = o.Thumbprint,
					Description =
						o.NotAfter.ToString("yyyy-MM-dd") + ": "
						+ (string.IsNullOrEmpty(o.FriendlyName) ? "" : "\"" + o.FriendlyName + "\" ")
						+ o.Subject
						+ " [Issuer:] " + o.Issuer
				}).ToArray();
				_cboRaamCert.Items.Clear();
				_cboRaamCert.Items.Add(new CertItem { Description = "<Enter Thumbprint Manually>", ID = "" });
				_cboRaamCert.Items.AddRange(certItems);
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}
		}

		private void InitCertFormFields()
		{
			// Synchronize combo box to text box...
			if (_txtSSLCert.Text.Length > 0)
			{
				for (int i = 0; i < _cboSSLCert.Items.Count; i++)
				{
					var item = _cboSSLCert.Items[i] as CertItem;
					if (item.ID == _txtSSLCert.Text)
					{
						_cboSSLCert.SelectedIndex = i;
						break;
					}
				}
			}
			if (_cboSSLCert.SelectedIndex <= 0)
			{
				_txtSSLCert.Text = string.Empty;
				_cboSSLCert.SelectedIndex = 0;
			}

			// Synchronize combo box to text box...
			if (_txtThumbPrint.Text.Length > 0)
			{
				for (int i = 0; i < _cboRaamCert.Items.Count; i++)
				{
					var item = _cboRaamCert.Items[i] as CertItem;
					if (item.ID == _txtThumbPrint.Text)
					{
						_cboRaamCert.SelectedIndex = i;
						break;
					}
				}
			}
			if (_cboRaamCert.SelectedIndex <= 0)
			{
				_cboRaamCert.SelectedIndex = 0;
			}
		}

		private List<X509Certificate2> GetPotentialSSLCerts()
		{
			List<X509Certificate2> availableCerts = new List<X509Certificate2>();
			X509Store certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
			certStore.Open(OpenFlags.ReadOnly);
			try
			{
				foreach (var cert in certStore.Certificates)
				{
					if (cert.HasPrivateKey)
						availableCerts.Add(cert);
				}
			}
			finally
			{
				certStore.Close();
			}

			availableCerts.Sort((o1, o2) => o2.NotAfter.CompareTo(o1.NotAfter));
			return availableCerts;
		}

		private List<X509Certificate2> GetPotentialRAAMCerts()
		{
			// First, include all certs that would have been allowed as an SSL certificate (has a private key...)
			List<X509Certificate2> availableCerts = GetPotentialSSLCerts();

			// Then add any certificates specifically added t T
			X509Store certStore = new X509Store(StoreName.TrustedPeople, StoreLocation.LocalMachine);
			certStore.Open(OpenFlags.ReadOnly);
			try
			{
				foreach (var cert in certStore.Certificates)
				{
					availableCerts.Add(cert);
				}
			}
			finally
			{
				certStore.Close();
			}

			availableCerts.Sort((o1, o2) => o2.NotAfter.CompareTo(o1.NotAfter));
			return availableCerts;
		}

		private void _cboSSLCert_SelectedIndexChanged(object sender, EventArgs e)
		{
			var item = _cboSSLCert.SelectedItem as CertItem;
			_txtSSLCert.Text = item == null ? string.Empty : item.ID;
		}

		private void _cboRaamCert_SelectedIndexChanged(object sender, EventArgs e)
		{
			var item = _cboRaamCert.SelectedItem as CertItem;
			if (item != null && !string.IsNullOrEmpty(item.ID))
				_txtThumbPrint.Text = item.ID;
		}

		#endregion

		#region Duplicated Input Fields

		private void DuplicatedTextBox_TextChanged(object sender, EventArgs e)
		{
			try
			{
				TextBox source = sender as TextBox;
				string sourceName = source.Name;
				string targetName = null;
				Control targetContainer = this;
				foreach (string num in new string[] {"2", "3", "4", "5"})
				{
					if (sourceName.EndsWith(num))
					{
						targetName = sourceName.Substring(0, sourceName.Length - 1);
						targetContainer = _tabAppTier;
						break;
					}
				}
				if (targetName != null)
				{
					TextBox target = targetContainer.Controls.Find(targetName, true).First() as TextBox;
					if (target.Text != source.Text)
						target.Text = source.Text;
				}
				else
				{
					foreach (string num in new string[] {"2", "3", "4", "5"})
					{
						targetName = sourceName + num;
						targetContainer = this;	// Note: if the tab was hidden, "this" won't find it, but we don't really care...
						var matches = targetContainer.Controls.Find(targetName, true);
						if (matches != null && matches.Length > 0)
						{
							TextBox target = matches[0] as TextBox;
							if (target.Text != source.Text)
								target.Text = source.Text;
						}
					}
				}
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString(), "Couldn't copy value - please verify values");
			}
		}

        #endregion

        #region LayoutSupportMethods
        private static void SetupComboBox(ComboBox _cboHostServicesWith, TextBox _txtHostServicesWith) {
            Func<string, string> translateValue = originalValue => originalValue.Equals("IIS", StringComparison.InvariantCultureIgnoreCase) ? "IIS" : "Stand Alone Windows Service";
            _cboHostServicesWith.Text = translateValue(_txtHostServicesWith.Text);
            EventHandler textbox_TextChangedHandler = (sender, e) => {
                //When the text box is populated from the XML file we need to have that value comunicated to the combo box.
                _cboHostServicesWith.Text = translateValue(_txtHostServicesWith.Text);
            };
            _txtHostServicesWith.TextChanged += textbox_TextChangedHandler;
            _cboHostServicesWith.TextChanged += (sender, e) => {
                //Once we are here the combo box has been initialized which is the only reason for the textbox's text changed event handler
                //  So we will remove it
                _txtHostServicesWith.TextChanged -= textbox_TextChangedHandler;
                _txtHostServicesWith.Text = _cboHostServicesWith.Text.Equals("IIS", StringComparison.InvariantCultureIgnoreCase) ? "IIS" : "Stand Alone Service";
            };
        }
        # endregion
	}
}
