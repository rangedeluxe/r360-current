﻿using System.Windows.Forms;

namespace Wfs.R360.DeploymentConfigurationUtilities
{
    public class ConfigUi : IConfigUi
    {
        public void ShowConfigDialog(ConfigForm form)
        {
            form.ShowDialog();
        }
        public void ShowDbConfigDialog(DBConfigForm form)
        {
            form.ShowDialog();
        }
        public void ShowMessage(string message, string title = null)
        {
            if (title != null)
            {
                MessageBox.Show(message, title);
            }
            else
            {
                MessageBox.Show(message);
            }
        }
    }
}