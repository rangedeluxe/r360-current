﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace Wfs.R360.DeploymentConfigurationUtilities
{
    public class ConfigManager
    {
        private const string NOTIFICATION_SERVICES_SERVER = "NOTIFICATION_SERVICES_SERVER";
        private const string RUN_AS_WIN_SERVICE = "RunAsWinService";
        private const string _encryptedValuePattern = @"[A-Za-z0-9\+\/=]{196,}";

        private Dictionary<string, string> _encryptedSettingPatterns = new Dictionary<string, string>() {
            {"ConfigPattern", $"(?<prefix>add\\s+key\\s*=\\s*\"(Username|Password|Entity)\"\\s+value\\s*=\\s*\"){_encryptedValuePattern}(?<suffix>\"\\s*)"    },
            {"IniDbConnPattern", $"(?<=(DBConn.+(=|;)\\s*))(?<prefix>(Password|User ID)=){_encryptedValuePattern}(?<suffix>(;|$|\\r))"                                                  },
            {"AppConfigConnPattern", $"(?<=(<add\\s+key\\s*=\\s*\"ConnectionString\"\\s+value=\"([^\"]+;\\s*)?))(?<prefix>(Password|User ID)=){_encryptedValuePattern}(?<suffix>(;|$|\\r))" },
            {"WebCredentialsPattern", $"(?<prefix>\\b(encryptedUsername|encryptedPassword)=\"){_encryptedValuePattern}(?<suffix>\")"                                                         }
        };

        private string _paramFileName;

        public ConfigManager()
        {
            Ui = new ConfigUi();
            EncryptedSettingsRegexes = _encryptedSettingPatterns.Values.Select(pattern => new Regex(pattern)).ToArray();
        }

        public string ParamFileName
		{
			get { return _paramFileName; }
			set
			{
				_paramFileName = value; 
				_paramXDoc = null;
			}
		}
        public IConfigUi Ui { get; set; }

        private Regex[] EncryptedSettingsRegexes {
            get; set; }

		public void GetParams(bool prompt, string tier)
		{
			// Open Parameter form, and return the name of the parameter file seleted.
			var frm = new ConfigForm(Ui);
			switch (tier.ToUpperInvariant())
			{
				case "WEB":
					frm.OnlyWebParams();
					break;
				case "APP":
					frm.OnlyAppParams();
					break;
				case "FILE":
					frm.OnlyFileParams();
					break;
				case "DIT":
					frm.OnlyClientDitParams();
					break;
				case "FIT":
					frm.OnlyClientFitParams();
					break;
				case "EXTRACT":
					frm.OnlyClientExtractParams();
					break;
				case "DRIVE":
					frm.OnlyDriveParams();
					break;
			}
			if (prompt)
				Ui.ShowConfigDialog(frm);
			else
				frm.ValidateParamFile();

			ParamFileName = frm.ParamFileName ?? "";
		}

		public void GetDBParams(bool prompt)
		{
			// Open Parameter form, and return the name of the parameter file seleted.
			var frm = new DBConfigForm(Ui);
			if (prompt)
                Ui.ShowDbConfigDialog(frm);
			else
				frm.ValidateParamFile();

			ParamFileName = frm.ParamFileName ?? "";
		}

		protected XDocument _paramXDoc;

		private IEnumerable<string> GetParamNames()
        {
            return ConfigForm.Params.Select(param => param.Key)
                .Concat(GetSynthesizedParamNames());
        }
        private IEnumerable<string> GetSynthesizedParamNames()
        {
            yield return NOTIFICATION_SERVICES_SERVER;
            yield return RUN_AS_WIN_SERVICE;
        }
        private string GetSynthesizedParamValue(string key)
        {
            switch (key)
            {
                case NOTIFICATION_SERVICES_SERVER:
                    return GetParamValue("NOTIFICATION_SERVICES_SERVER_2.02");
                case RUN_AS_WIN_SERVICE:
                    return GetParamValue("FILE_HOST_SERVICES_WITH").ToLowerInvariant()=="iis"?"0":"1";
                default:
                    return null;
            }
        }
        public string GetParamValue(string key)
		{
            var synthesizedValue = GetSynthesizedParamValue(key);
            if (synthesizedValue != null)
            {
                return synthesizedValue;
            }

			// Open (potentially cached) parameter file
			if (_paramXDoc == null)
			{
				_paramXDoc = XDocument.Load(ParamFileName);
			}

			var entry = ConfigForm.Params[key];

			// Read parameter
			XElement elem = _paramXDoc.Descendants(key).FirstOrDefault();
			if (elem == null)
				return string.Empty;

			// Decrypt if appropriate
			string value = elem.Value;
			if (entry.Encrypt)
				value = ConfigForm.Decrypt(value, key, Ui);

			// Re-encrypt if appropriate
			if (entry.EncryptOutput)
			{
					value = EncryptUtils.EncryptWithDPAPI(value);
			}

			return value;
		}

		public string GetDBParamValue(string key)
		{
			// Open (potentially cached) parameter file
			if (_paramXDoc == null)
			{
				_paramXDoc = XDocument.Load(ParamFileName);
			}

			// Read parameter
			XElement elem = _paramXDoc.Descendants(key).FirstOrDefault();
			if (elem == null)
				return string.Empty;

			return elem.Value;
		}

		//public void SetAliases(string aliases)
		//{
		//	foreach (var entry in aliases.Split(';').Select(o => o.Split('=')))
		//	{
		//		_aliases[entry[0]] = entry[1];
		//	}
		//}

		//private Dictionary<string, string> _aliases = new Dictionary<string, string>();
		public void ProcessConfigVariables(string oemFileName, string outputFileName)
		{
			try
			{
				// Read current configuration
				string config = File.ReadAllText(oemFileName);
				var logFolders = new string[] { @"D:\WFSApps\Logs\", @"D:\WFSApps\Raam\Logs\", @"D:\WFSApps\RecHub\Data\Log" };
				var installFolders = new string[] { @"D:\WFSApps\" };

				foreach (var paramName in GetParamNames())
				{
					switch (paramName)
					{
						case "InstallDrive":
							{
								string value = GetParamValue(paramName);
								if (string.Compare(value, "D:", StringComparison.OrdinalIgnoreCase) != 0)
								{
									ReplaceDriveReferences(ref config, installFolders, logFolders, value);
								}
							}
							break;
						case "LogDrive":
							{
								string value = GetParamValue(paramName);
								if (string.Compare(value, "D:", StringComparison.OrdinalIgnoreCase) != 0)
								{
									ReplaceDriveReferences(ref config, logFolders, null, value);
								}
							}
							break;
                        case "ServerNamesWeb":
                        case "ServerNamesApp":
                        case "ServerNamesFile":
                            var values = GetParamValue(paramName).Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                            var iniEntries = values.Select(value => "IPOnline=" + value);
                            var iniText = string.Join("\r\n", iniEntries.ToArray());
                            config = config.Replace("@@" + paramName + "@@", iniText);
                            break;
						//case "SERVER_NAME":
						// TODO: Need to comment the XML in...?  And may need multiple SERVER_NAME values duplicated?
						//	goto default;
						default:
							{
								string key = paramName;
								//if (_aliases.ContainsKey(key))
								//	key = _aliases[key];
								string value = GetParamValue(key);
								config = config.Replace("@@" + paramName + "@@", value);
							}
							break;
					}
				}

				// Remove readonly attributes, if necessary
				if (File.Exists(outputFileName))
				{
					var attributes = File.GetAttributes(outputFileName);
					var newattributes = attributes & ~FileAttributes.ReadOnly;
					if (newattributes != attributes)
						File.SetAttributes(outputFileName, newattributes);
				}

				File.WriteAllText(outputFileName, config);
			}
			catch (Exception ex)
			{
                Ui.ShowMessage(ex.ToString());
				throw;
			}
		}

		public void ReplaceString(string sourceFileName, string targetFileName, string[] sources, string[] targets)
		{
			try
			{
				// Validate Parameters
				if (sources == null || targets == null)
					throw new ArgumentNullException("sources or targets");
				if (sources.Length != targets.Length)
					throw new ArgumentException("Different number of sources / targets provided");

				// Read current configuration
				string config = File.ReadAllText(sourceFileName);

				// Replace all source/target pairs
				for (int i = 0; i < sources.Length; i++)
				{
					config = config.Replace(sources[i], targets[i]);
				}

				// Remove readonly attributes, if necessary
				if (File.Exists(targetFileName))
				{
					var attributes = File.GetAttributes(targetFileName);
					var newattributes = attributes & ~FileAttributes.ReadOnly;
					if (newattributes != attributes)
						File.SetAttributes(targetFileName, newattributes);
				}

				File.WriteAllText(targetFileName, config);
			}
			catch (Exception ex)
			{
                Ui.ShowMessage(ex.ToString());
				throw;
			}
		}

		public void ProcessDBVariables(string srcFileName, string outputFileName, string aliases)
		{
			try
			{
				// Read current file
				string config = File.ReadAllText(srcFileName);

				// Replace parameterized values
				//foreach (var entry in _frm._params)
				//{
				//	string value = GetDBParamValue(entry.Key);
				//	config = config.Replace("@@" + entry.Key + "@@", value);
				//}

				// Handle aliased parameters: "DB_SERVER=RAAM_DB_SERVER;SQL_DATA_PATH=RAAM_SQL_DATA_PATH;..."
				if (!string.IsNullOrEmpty(aliases))
				{
					var aliasTable = aliases.Split(';').Select(o => o.Split('=')).ToDictionary(o => o[0], o => o[1]);
					foreach (var entry in aliasTable)
					{
						string value = GetDBParamValue(entry.Value);
						config = config.Replace("@@" + entry.Key + "@@", value);
					}

					// Remove readonly attributes, if necessary
					if (File.Exists(outputFileName))
					{
						var attributes = File.GetAttributes(outputFileName);
						var newattributes = attributes & ~FileAttributes.ReadOnly;
						if (newattributes != attributes)
							File.SetAttributes(outputFileName, newattributes);
					}
				}

				File.WriteAllText(outputFileName, config);
			}
			catch (Exception ex)
			{
                Ui.ShowMessage(ex.ToString());
				throw;
			}
		}

		public void CreatePartitionDisksXml(string outputFileName)
		{
			string xml = @"<StaticData SystemName=""OLTA"" LoaderSchema=""RecHubConfig"">
	<Table Schema=""RecHubSystem"" Name=""PartitionDisks"">
		<ColumnMap>
			<Column Key=""TRUE"" Update=""FALSE"" DataType=""INT"">PartitionManagerID</Column>
			<Column Key=""TRUE"" Update=""FALSE"" DataType=""INT"">DiskOrder</Column>
			<Column Key=""FALSE"" Update=""FALSE"" DataType=""VARCHAR"" Size=""256"">DiskPath</Column>
		</ColumnMap>
	</Table>
	<!-- 
	
	Setup Information
	PartitionManagerID 1 maps to transaction fact data 
	PartitionManagerID 2 maps to notification fact data 
	PartitionManagerID 3 maps to audit fact data 
	DiskOrder defines what order the disk mapping will occur. Must always start with 1 and as many disks can be created as needed. 
	DiskPath defines the path for the given disk order. 
	
	The following three lines state that three disks will be created for transaction fact data.
	<Data Version=""2.00.00.00"" WI=""108874"" PartitionManagerID=""1"" DiskOrder=""1"" DiskPath=""C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\RecHub20\Disk1""/>
	<Data Version=""2.00.00.00"" WI=""108874"" PartitionManagerID=""1"" DiskOrder=""2"" DiskPath=""C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\RecHub20\Disk2""/>
	<Data Version=""2.00.00.00"" WI=""108874"" PartitionManagerID=""1"" DiskOrder=""3"" DiskPath=""C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\RecHub20\Disk3""/>

	The following line states that one disk will be created for notification fact data.
	<Data Version=""2.00.00.00"" WI=""108874"" PartitionManagerID=""2"" DiskOrder=""1"" DiskPath=""C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\RecHub20\Notifications""/>
	
	The following line stats that one disk will be created for audit fact data.
	<Data Version=""2.00.00.00"" WI=""108874"" PartitionManagerID=""3"" DiskOrder=""1"" DiskPath=""C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\RecHub20\Audits""/>
	
	There must always be at least one record for each PartitionManagerID. I.E. there must be a minimum of 3 records in this file.
	
	-->
";
			/*
				<Data Version="2.00.00.00" WI="108874" PartitionManagerID="1" DiskOrder="1" DiskPath="Path to Data facts"/>
				<Data Version="2.00.00.00" WI="108874" PartitionManagerID="2" DiskOrder="1" DiskPath="Path to Notification facts"/>
				<Data Version="2.00.00.00" WI="108874" PartitionManagerID="3" DiskOrder="1" DiskPath="Path to Alert facts"/>
			 */
			string[] partitions = new string[] { "RECHUB_SQL_ITEM_PATH", "RECHUB_SQL_NOTIFICATION_PATH", "RECHUB_SQL_AUDIT_PATH" };
			for (int partitionID = 0; partitionID < partitions.Length; partitionID++)
			{
				string itemPath = GetDBParamValue(partitions[partitionID]);
				string[] itemPaths = itemPath.Split(';');
				for (int i = 0; i < itemPaths.Length; i++)
				{
					xml += string.Format("	<Data Version=\"2.00.00.00\" WI=\"108874\" PartitionManagerID=\"{0}\" DiskOrder=\"{1}\" DiskPath=\"{2}\"/>\r\n",
											partitionID + 1, i + 1, itemPaths[i]);
				}
			}
			xml += "</StaticData>";

			// Remove readonly attributes, if necessary
			if (File.Exists(outputFileName))
			{
				var attributes = File.GetAttributes(outputFileName);
				var newattributes = attributes & ~FileAttributes.ReadOnly;
				if (newattributes != attributes)
					File.SetAttributes(outputFileName, newattributes);
			}

			File.WriteAllText(outputFileName, xml);
		}

		public static void ReplaceDriveReferences(ref string config, string[] targetPaths, string[] excludePaths, string value)
		{
			foreach (var folder in targetPaths)
			{
				int index = config.IndexOf(folder, StringComparison.OrdinalIgnoreCase);
				while (index >= 0)
				{
					// Don't update if it references an excluded folder...
					bool isExcluded = false;
					if (excludePaths != null)
					{
						foreach (var excludeFolder in excludePaths)
						{
							if (config.IndexOf(excludeFolder, index, excludeFolder.Length, StringComparison.OrdinalIgnoreCase) == index)
							{
								isExcluded = true;
								break;
							}
						}
					}

					if (!isExcluded)
						config = config.Substring(0, index) + value + config.Substring(index + 2);

					index = config.IndexOf(folder, index + 2, StringComparison.OrdinalIgnoreCase);
				}
			}
		}

		public void EncryptConfigSection(string folder, string section)
		{
			ExeConfigurationFileMap map = new ExeConfigurationFileMap { ExeConfigFilename = Path.Combine(folder, "web.config") };
			var config = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
			var configSection = config.GetSection(section);
			configSection.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider"); // RsaProtectedConfigurationProvider is the default... But would the DPAPI provider be simpler / better?
			config.Save();
		}

        public string NormalizeConfigContent(string content) {
            // Don't compare encypted lines with varying entropy...
            content = NormalizeEncryptedData(content);

            // Normalize white-space
            return NormalizeWhitespace(content);
        }

        public bool AreConfigContentsDifferent(string contentA, string contentB) {
            return (string.Compare(NormalizeConfigContent(contentA), NormalizeConfigContent(contentB), StringComparison.Ordinal) != 0);
        }

		public string DetectConfigChanges(string currentFolder, string backupFolder, string relativePath)
		{
			string curPath = Path.Combine(currentFolder, relativePath);
			string bakPath = Path.Combine(backupFolder, relativePath);

			// First - check and see if the backup file exists
			if (!File.Exists(bakPath))
				return "<No Backup of: " + relativePath + ">";

			string oldConfig = File.ReadAllText(bakPath);
			string newConfig = File.ReadAllText(curPath);

		    if (!AreConfigContentsDifferent(oldConfig, newConfig)) {
                return "<Unchanged>";
            }

			// Not equal - display in merge utility
			string command = GetParamValue("MergeUtilityPath");
            Match breakout = Regex.Match(command, "^((\"(?<file>[^\"]+)\")|(?<file>[^\\s]+))( +((?<args>[^\\s\"]+)|(\"(?<args>[^\"]+)\")))* *$");
            string path = breakout.Groups["file"].Value;
            string[] args = (from Capture item in breakout.Groups["args"].Captures
                             select item.Value).ToArray();
            if (string.IsNullOrEmpty(breakout.Groups["file"].Value) || !File.Exists(path))
			{
                Ui.ShowMessage("Configuration File has Changed - please compare for differences:\r\n\r\n" + relativePath, "Configuration Change");
			}
			else
			{
				var proc = Process.Start(new ProcessStartInfo
					{
						FileName = path,
						Arguments = string.Join(" ", args) + " \"" + bakPath + "\" \"" + curPath + "\"",
					});

				proc.WaitForExit();

				// Delete standard "backup" files, if created...
				var mergeBakPath = curPath + ".bak";
				if (File.Exists(mergeBakPath))
					File.Delete(mergeBakPath);
			}
			return "Differences detected";
		}

        public string NormalizeEncryptedDataByLine(string line) {
            var result = line;
            var replacePattern = "${prefix}[Encrypted Value]${suffix}";
            
            foreach (var currentRegex in EncryptedSettingsRegexes.Where(regex => regex.IsMatch(line))) {
                result = currentRegex.Replace(line, replacePattern);
            }

            return result;
        }

		private string NormalizeEncryptedData(string config)
		{
			var lines = config.Replace("\r\n", "\n").Split(new [] {'\n'});
		    
            return string.Join("\r\n", lines.Select(NormalizeEncryptedDataByLine));
		}

		public static string NormalizeWhitespace(string text) {
		    string result = text.Replace('\r', ' ').Replace('\n', ' ').Replace('\t', ' ');

			while (result.IndexOf("  ") >= 0)
			{
                result = result.Replace("  ", " ");
			}
		    return result;
		}
    }
}
