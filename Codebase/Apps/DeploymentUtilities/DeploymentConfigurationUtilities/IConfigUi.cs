﻿namespace Wfs.R360.DeploymentConfigurationUtilities
{
    public interface IConfigUi
    {
        void ShowConfigDialog(ConfigForm form);
        void ShowDbConfigDialog(DBConfigForm form);
        void ShowMessage(string message, string title = null);
    }
}