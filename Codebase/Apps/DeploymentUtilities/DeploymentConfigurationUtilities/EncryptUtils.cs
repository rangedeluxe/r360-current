﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Wfs.R360.DeploymentConfigurationUtilities
{
	internal static class EncryptUtils
	{      
		private static readonly byte[] _dpapiEntropy = Convert.FromBase64String("+m5dGpOoAgfSo8P59rPLNSO3sMGK7wR7la/Qj0nKY3SYSJZJ8dpm6pRQupjbCujq5yg=");

		internal static string EncryptWithDPAPI(string value)
		{
			byte[] encrypted = ProtectedData.Protect(System.Text.Encoding.Unicode.GetBytes(value), _dpapiEntropy, DataProtectionScope.LocalMachine);
			return Convert.ToBase64String(encrypted);
		}
	}
}
