﻿namespace Wfs.R360.DeploymentConfigurationUtilities
{
	partial class ConfigForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigForm));
            this.label1 = new System.Windows.Forms.Label();
            this._txtParamFile = new System.Windows.Forms.TextBox();
            this._txtThumbPrint = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this._txtWebServerName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this._txtDirectServerName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this._txtRaamWebServerName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this._txtRaamValidationKey2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this._txtRaamDecryptionKey2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this._btnGenerateKeys = new System.Windows.Forms.Button();
            this._txtRaamAppServerName = new System.Windows.Forms.TextBox();
            this._btnSave = new System.Windows.Forms.Button();
            this._btnCancel = new System.Windows.Forms.Button();
            this._btnLoadParams = new System.Windows.Forms.Button();
            this._txtInstallDrive = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this._txtLogDrive = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this._txtDBFrameworkServer = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this._txtAppDomainUserPassword = new System.Windows.Forms.TextBox();
            this._txtAppDomainUser = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this._txtDBRaamServer = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this._txtDBRecHubServer = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this._txtDBIntegraPAYServer = new System.Windows.Forms.TextBox();
            this._txtDBRecHubDBName = new System.Windows.Forms.TextBox();
            this._txtDBIntegraPAYDBName = new System.Windows.Forms.TextBox();
            this._txtRecHubSsrsServer = new System.Windows.Forms.TextBox();
            this._txtMergeUtilityPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this._txtFileServer = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this._txtSSLCert = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this._cboSSLCert = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.label20 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this._grpWebParams = new System.Windows.Forms.GroupBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this._grpRaamIntegration = new System.Windows.Forms.GroupBox();
            this._txtRaamSystemUserName = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this._txtRaamSystemUserPassword = new System.Windows.Forms.TextBox();
            this._cboRaamCert = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this._txtNotificationServicesServerName = new System.Windows.Forms.TextBox();
            this._txtAppServerName = new System.Windows.Forms.TextBox();
            this._grpDatabaseAccess = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._txtDBConnectionComExcepPassword = new System.Windows.Forms.TextBox();
            this._txtDBConnectionIPassword = new System.Windows.Forms.TextBox();
            this._txtDBConnectionIPUser = new System.Windows.Forms.TextBox();
            this._txtDBConnection2Password = new System.Windows.Forms.TextBox();
            this._txtDBConnection2User = new System.Windows.Forms.TextBox();
            this._txtDBConnectionComExcepUser = new System.Windows.Forms.TextBox();
            this._txtDBConnectionComExcepServer = new System.Windows.Forms.TextBox();
            this._txtDBConnectionComExcepDBName = new System.Windows.Forms.TextBox();
            this._tabControl = new System.Windows.Forms.TabControl();
            this._tabGeneral = new System.Windows.Forms.TabPage();
            this._tabWebTier = new System.Windows.Forms.TabPage();
            this._serverNamesWeb = new Wfs.R360.DeploymentConfigurationUtilities.ServerNamesUserControl();
            this._tabAppTier = new System.Windows.Forms.TabPage();
            this._pnlAppLayout = new System.Windows.Forms.TableLayoutPanel();
            this._grpAppGeneral = new System.Windows.Forms.GroupBox();
            this._serverNamesApp = new Wfs.R360.DeploymentConfigurationUtilities.ServerNamesUserControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this._txtRaamValidationKey = new System.Windows.Forms.TextBox();
            this._txtRaamDecryptionKey = new System.Windows.Forms.TextBox();
            this._grpReports = new System.Windows.Forms.GroupBox();
            this._txtSSRSDomain = new System.Windows.Forms.TextBox();
            this._txtSSRSUser = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this._txtSSRSPassword = new System.Windows.Forms.TextBox();
            this._txtReportsFileShare = new System.Windows.Forms.TextBox();
            this._txtSSRSFolder = new System.Windows.Forms.TextBox();
            this._tabFileTier = new System.Windows.Forms.TabPage();
            this.flwFileTierTab = new System.Windows.Forms.FlowLayoutPanel();
            this._grpFileGeneral = new System.Windows.Forms.GroupBox();
            this._txtFileServer2 = new System.Windows.Forms.TextBox();
            this._grpDatabaseAccessFile = new System.Windows.Forms.GroupBox();
            this._txtDBConnectionIPassword2 = new System.Windows.Forms.TextBox();
            this._txtDBConnectionIPUser2 = new System.Windows.Forms.TextBox();
            this._txtDBConnection2Password2 = new System.Windows.Forms.TextBox();
            this._txtDBConnection2User2 = new System.Windows.Forms.TextBox();
            this._txtDBIntegraPAYDBName2 = new System.Windows.Forms.TextBox();
            this._txtDBRecHubServer2 = new System.Windows.Forms.TextBox();
            this._txtDBRecHubDBName2 = new System.Windows.Forms.TextBox();
            this._txtDBIntegraPAYServer2 = new System.Windows.Forms.TextBox();
            this.grpHostServiceWith = new System.Windows.Forms.GroupBox();
            this.flwHostServiceWith = new System.Windows.Forms.FlowLayoutPanel();
            this._cboHostServicesWith = new System.Windows.Forms.ComboBox();
            this._txtHostServicesWith = new System.Windows.Forms.TextBox();
            this._gprExtractProcessing = new System.Windows.Forms.GroupBox();
            this._txtExtractEntity = new System.Windows.Forms.TextBox();
            this._txtExtractUser = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this._txtExtractPassword = new System.Windows.Forms.TextBox();
            this._serverNamesFile = new Wfs.R360.DeploymentConfigurationUtilities.ServerNamesUserControl();
            this._tabClientDit = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this._txtFileServer3 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this._txtDitClientEntity = new System.Windows.Forms.TextBox();
            this._txtDitClientUser = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this._txtDitClientPassword = new System.Windows.Forms.TextBox();
            this._tabClientFit = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this._txtFileServer4 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this._txtFitClientEntity = new System.Windows.Forms.TextBox();
            this._txtFitClientUser = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this._txtFitClientPassword = new System.Windows.Forms.TextBox();
            this.grpCDSDB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCENDSPath = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.txtFITIntegraPAYDatabase = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.txtFITIntegraPAYPassword = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtFITIntegraPAYUser = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.txtFITIntegraPAYServer = new System.Windows.Forms.TextBox();
            this._tabClientExtract = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this._txtFileServer5 = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this._txtDBConnectionExtractPassword = new System.Windows.Forms.TextBox();
            this._txtDBConnectionExtractUser = new System.Windows.Forms.TextBox();
            this._txtDBRecHubServer5 = new System.Windows.Forms.TextBox();
            this._txtDBRecHubDBName5 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this._grpWebParams.SuspendLayout();
            this._grpRaamIntegration.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this._grpDatabaseAccess.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this._tabControl.SuspendLayout();
            this._tabGeneral.SuspendLayout();
            this._tabWebTier.SuspendLayout();
            this._tabAppTier.SuspendLayout();
            this._pnlAppLayout.SuspendLayout();
            this._grpAppGeneral.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this._grpReports.SuspendLayout();
            this._tabFileTier.SuspendLayout();
            this.flwFileTierTab.SuspendLayout();
            this._grpFileGeneral.SuspendLayout();
            this._grpDatabaseAccessFile.SuspendLayout();
            this.grpHostServiceWith.SuspendLayout();
            this.flwHostServiceWith.SuspendLayout();
            this._gprExtractProcessing.SuspendLayout();
            this._tabClientDit.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this._tabClientFit.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.grpCDSDB.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this._tabClientExtract.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Parameter File:";
            this._toolTips.SetToolTip(this.label1, "If you choose \"Save and Continue\", the values will be saved to this file, and aut" +
        "omatically reloaded the next time you deploy changes.");
            // 
            // _txtParamFile
            // 
            this._txtParamFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtParamFile.Location = new System.Drawing.Point(191, 23);
            this._txtParamFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtParamFile.Name = "_txtParamFile";
            this._txtParamFile.Size = new System.Drawing.Size(1013, 22);
            this._txtParamFile.TabIndex = 1;
            // 
            // _txtThumbPrint
            // 
            this._txtThumbPrint.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtThumbPrint.Location = new System.Drawing.Point(191, 57);
            this._txtThumbPrint.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtThumbPrint.Name = "_txtThumbPrint";
            this._txtThumbPrint.Size = new System.Drawing.Size(1121, 22);
            this._txtThumbPrint.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 60);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "RAAM Cert Thumbprint:";
            this._toolTips.SetToolTip(this.label3, "You can select the RAAM Signing Certificate above to auto-populate this value.  O" +
        "therwise, enter the value manually (copied from somewhere else).");
            // 
            // _txtWebServerName
            // 
            this._txtWebServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtWebServerName.Location = new System.Drawing.Point(200, 23);
            this._txtWebServerName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtWebServerName.Name = "_txtWebServerName";
            this._txtWebServerName.Size = new System.Drawing.Size(1111, 22);
            this._txtWebServerName.TabIndex = 1;
            this._txtWebServerName.Text = "e.g. R360.mybank.com";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Web Server (URL):";
            this._toolTips.SetToolTip(this.label4, "This is the name you will type in the browser to access the Web Site.  It must al" +
        "so be configured as a \"Relying Party\" in RAAM.  e.g. \"R360.mybank.com\"");
            // 
            // _txtDirectServerName
            // 
            this._txtDirectServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDirectServerName.Enabled = false;
            this._txtDirectServerName.Location = new System.Drawing.Point(200, 87);
            this._txtDirectServerName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDirectServerName.Name = "_txtDirectServerName";
            this._txtDirectServerName.Size = new System.Drawing.Size(1111, 22);
            this._txtDirectServerName.TabIndex = 5;
            this._txtDirectServerName.Text = "<Not currently used>";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 91);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Direct Server (URL):";
            this._toolTips.SetToolTip(this.label6, "<Reserved for Future Use with Load-Balanced Environments>");
            // 
            // _txtRaamWebServerName
            // 
            this._txtRaamWebServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtRaamWebServerName.Location = new System.Drawing.Point(200, 55);
            this._txtRaamWebServerName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRaamWebServerName.Name = "_txtRaamWebServerName";
            this._txtRaamWebServerName.Size = new System.Drawing.Size(1111, 22);
            this._txtRaamWebServerName.TabIndex = 3;
            this._txtRaamWebServerName.Text = "e.g. R360.mybank.com";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 59);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(172, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "RAAM Web Server (URL):";
            this._toolTips.SetToolTip(this.label5, resources.GetString("label5.ToolTip"));
            // 
            // _txtRaamValidationKey2
            // 
            this._txtRaamValidationKey2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtRaamValidationKey2.Location = new System.Drawing.Point(200, 119);
            this._txtRaamValidationKey2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRaamValidationKey2.Name = "_txtRaamValidationKey2";
            this._txtRaamValidationKey2.Size = new System.Drawing.Size(1001, 22);
            this._txtRaamValidationKey2.TabIndex = 7;
            this._txtRaamValidationKey2.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 123);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(148, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Server Validation Key:";
            this._toolTips.SetToolTip(this.label7, resources.GetString("label7.ToolTip"));
            // 
            // _txtRaamDecryptionKey2
            // 
            this._txtRaamDecryptionKey2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtRaamDecryptionKey2.Location = new System.Drawing.Point(200, 151);
            this._txtRaamDecryptionKey2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRaamDecryptionKey2.Name = "_txtRaamDecryptionKey2";
            this._txtRaamDecryptionKey2.Size = new System.Drawing.Size(1001, 22);
            this._txtRaamDecryptionKey2.TabIndex = 9;
            this._txtRaamDecryptionKey2.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 155);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(154, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Server Decryption Key:";
            this._toolTips.SetToolTip(this.label9, resources.GetString("label9.ToolTip"));
            // 
            // _btnGenerateKeys
            // 
            this._btnGenerateKeys.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnGenerateKeys.Location = new System.Drawing.Point(1212, 132);
            this._btnGenerateKeys.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._btnGenerateKeys.Name = "_btnGenerateKeys";
            this._btnGenerateKeys.Size = new System.Drawing.Size(100, 28);
            this._btnGenerateKeys.TabIndex = 10;
            this._btnGenerateKeys.Text = "Generate";
            this._btnGenerateKeys.UseVisualStyleBackColor = true;
            this._btnGenerateKeys.Click += new System.EventHandler(this._btnGenerateKeys_Click);
            // 
            // _txtRaamAppServerName
            // 
            this._txtRaamAppServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtRaamAppServerName.Location = new System.Drawing.Point(191, 89);
            this._txtRaamAppServerName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRaamAppServerName.Name = "_txtRaamAppServerName";
            this._txtRaamAppServerName.Size = new System.Drawing.Size(1120, 22);
            this._txtRaamAppServerName.TabIndex = 5;
            this._txtRaamAppServerName.Text = "e.g. R360APP.internal-domain.net";
            // 
            // _btnSave
            // 
            this._btnSave.Location = new System.Drawing.Point(4, 4);
            this._btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._btnSave.Name = "_btnSave";
            this._btnSave.Size = new System.Drawing.Size(177, 28);
            this._btnSave.TabIndex = 5;
            this._btnSave.Text = "Save and Continue";
            this._btnSave.UseVisualStyleBackColor = true;
            this._btnSave.Click += new System.EventHandler(this._btnSave_Click);
            // 
            // _btnCancel
            // 
            this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btnCancel.Location = new System.Drawing.Point(213, 4);
            this._btnCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._btnCancel.Name = "_btnCancel";
            this._btnCancel.Size = new System.Drawing.Size(177, 28);
            this._btnCancel.TabIndex = 6;
            this._btnCancel.Text = "Cancel Installation";
            this._btnCancel.UseVisualStyleBackColor = true;
            this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
            // 
            // _btnLoadParams
            // 
            this._btnLoadParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._btnLoadParams.Location = new System.Drawing.Point(1213, 21);
            this._btnLoadParams.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._btnLoadParams.Name = "_btnLoadParams";
            this._btnLoadParams.Size = new System.Drawing.Size(100, 28);
            this._btnLoadParams.TabIndex = 2;
            this._btnLoadParams.Text = "Load";
            this._btnLoadParams.UseVisualStyleBackColor = true;
            this._btnLoadParams.Click += new System.EventHandler(this._btnLoadParams_Click);
            // 
            // _txtInstallDrive
            // 
            this._txtInstallDrive.Location = new System.Drawing.Point(245, 23);
            this._txtInstallDrive.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtInstallDrive.Name = "_txtInstallDrive";
            this._txtInstallDrive.Size = new System.Drawing.Size(101, 22);
            this._txtInstallDrive.TabIndex = 1;
            this._txtInstallDrive.Text = "D:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 27);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Install Drive:";
            this._toolTips.SetToolTip(this.label10, "The Drive letter where the application should be installed.  e.g. \"D:\"");
            // 
            // _txtLogDrive
            // 
            this._txtLogDrive.Location = new System.Drawing.Point(435, 23);
            this._txtLogDrive.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtLogDrive.Name = "_txtLogDrive";
            this._txtLogDrive.Size = new System.Drawing.Size(109, 22);
            this._txtLogDrive.TabIndex = 3;
            this._txtLogDrive.Text = "D:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(363, 27);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 17);
            this.label13.TabIndex = 2;
            this.label13.Text = "Log Drive:";
            this._toolTips.SetToolTip(this.label13, "The Drive letter where log files should be generated.  e.g. \"D:\"");
            // 
            // _txtDBFrameworkServer
            // 
            this._txtDBFrameworkServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBFrameworkServer.Location = new System.Drawing.Point(115, 100);
            this._txtDBFrameworkServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBFrameworkServer.Name = "_txtDBFrameworkServer";
            this._txtDBFrameworkServer.Size = new System.Drawing.Size(529, 22);
            this._txtDBFrameworkServer.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label14, 3);
            this.label14.Location = new System.Drawing.Point(4, 78);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(142, 17);
            this.label14.TabIndex = 0;
            this.label14.Text = "Framework Database";
            this._toolTips.SetToolTip(this.label14, "The name of the server where WFSPortal is installed.");
            // 
            // _txtAppDomainUserPassword
            // 
            this._txtAppDomainUserPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtAppDomainUserPassword.Location = new System.Drawing.Point(115, 52);
            this._txtAppDomainUserPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtAppDomainUserPassword.Name = "_txtAppDomainUserPassword";
            this._txtAppDomainUserPassword.Size = new System.Drawing.Size(529, 22);
            this._txtAppDomainUserPassword.TabIndex = 17;
            this._txtAppDomainUserPassword.UseSystemPasswordChar = true;
            // 
            // _txtAppDomainUser
            // 
            this._txtAppDomainUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtAppDomainUser.Location = new System.Drawing.Point(115, 22);
            this._txtAppDomainUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtAppDomainUser.Name = "_txtAppDomainUser";
            this._txtAppDomainUser.Size = new System.Drawing.Size(529, 22);
            this._txtAppDomainUser.TabIndex = 15;
            this._txtAppDomainUser.Text = "e.g. MyDomain\\s.WFSAppUser";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 18);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 30);
            this.label18.TabIndex = 14;
            this.label18.Text = "User Name:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label18, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // _txtDBRaamServer
            // 
            this._txtDBRaamServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBRaamServer.Location = new System.Drawing.Point(803, 100);
            this._txtDBRaamServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBRaamServer.Name = "_txtDBRaamServer";
            this._txtDBRaamServer.Size = new System.Drawing.Size(504, 22);
            this._txtDBRaamServer.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label12, 3);
            this.label12.Location = new System.Drawing.Point(692, 78);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 17);
            this.label12.TabIndex = 2;
            this.label12.Text = "RAAM Database";
            this._toolTips.SetToolTip(this.label12, "The name of the server where RAAM, IdentityServerConfiguration, etc. are installe" +
        "d.");
            // 
            // _txtDBRecHubServer
            // 
            this._txtDBRecHubServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBRecHubServer.Location = new System.Drawing.Point(803, 22);
            this._txtDBRecHubServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBRecHubServer.Name = "_txtDBRecHubServer";
            this._txtDBRecHubServer.Size = new System.Drawing.Size(504, 22);
            this._txtDBRecHubServer.TabIndex = 5;
            this._txtDBRecHubServer.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(708, 18);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 30);
            this.label15.TabIndex = 4;
            this.label15.Text = "Server:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label15, "The name of the server where the R360 database is installed.");
            // 
            // _txtDBIntegraPAYServer
            // 
            this._txtDBIntegraPAYServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBIntegraPAYServer.Location = new System.Drawing.Point(115, 148);
            this._txtDBIntegraPAYServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBIntegraPAYServer.Name = "_txtDBIntegraPAYServer";
            this._txtDBIntegraPAYServer.Size = new System.Drawing.Size(529, 22);
            this._txtDBIntegraPAYServer.TabIndex = 9;
            this._txtDBIntegraPAYServer.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _txtDBRecHubDBName
            // 
            this._txtDBRecHubDBName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBRecHubDBName.Location = new System.Drawing.Point(803, 52);
            this._txtDBRecHubDBName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBRecHubDBName.Name = "_txtDBRecHubDBName";
            this._txtDBRecHubDBName.Size = new System.Drawing.Size(504, 22);
            this._txtDBRecHubDBName.TabIndex = 11;
            this._txtDBRecHubDBName.Text = "WFSDB_R360";
            this._txtDBRecHubDBName.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _txtDBIntegraPAYDBName
            // 
            this._txtDBIntegraPAYDBName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBIntegraPAYDBName.Location = new System.Drawing.Point(115, 178);
            this._txtDBIntegraPAYDBName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBIntegraPAYDBName.Name = "_txtDBIntegraPAYDBName";
            this._txtDBIntegraPAYDBName.Size = new System.Drawing.Size(529, 22);
            this._txtDBIntegraPAYDBName.TabIndex = 13;
            this._txtDBIntegraPAYDBName.Text = "IntegraPay";
            this._txtDBIntegraPAYDBName.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _txtRecHubSsrsServer
            // 
            this._txtRecHubSsrsServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtRecHubSsrsServer.Location = new System.Drawing.Point(803, 303);
            this._txtRecHubSsrsServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRecHubSsrsServer.Name = "_txtRecHubSsrsServer";
            this._txtRecHubSsrsServer.Size = new System.Drawing.Size(504, 22);
            this._txtRecHubSsrsServer.TabIndex = 7;
            // 
            // _txtMergeUtilityPath
            // 
            this._txtMergeUtilityPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtMergeUtilityPath.Location = new System.Drawing.Point(191, 55);
            this._txtMergeUtilityPath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtMergeUtilityPath.Name = "_txtMergeUtilityPath";
            this._txtMergeUtilityPath.Size = new System.Drawing.Size(1121, 22);
            this._txtMergeUtilityPath.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 59);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Merge Utility:";
            this._toolTips.SetToolTip(this.label2, "If you supply the path / filename of a Merge Utility (such as WinMerge) the scrip" +
        "ts will compare the current versions of configuration files to the backsups it c" +
        "reates - using that utility.");
            // 
            // _txtFileServer
            // 
            this._txtFileServer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtFileServer.Location = new System.Drawing.Point(196, 20);
            this._txtFileServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtFileServer.Name = "_txtFileServer";
            this._txtFileServer.Size = new System.Drawing.Size(1111, 22);
            this._txtFileServer.TabIndex = 19;
            this._txtFileServer.Text = "e.g. R360FILE.internal-domain.net";
            this._txtFileServer.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 23);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(122, 17);
            this.label22.TabIndex = 18;
            this.label22.Text = "File Server (URL):";
            this._toolTips.SetToolTip(this.label22, "APP-Tier services may use additional services on the FILE-Tier.  Enter the name o" +
        "f the  server here (e.g. \"R360IMG.internal-domain.net\")");
            // 
            // _txtSSLCert
            // 
            this._txtSSLCert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtSSLCert.Location = new System.Drawing.Point(1275, 55);
            this._txtSSLCert.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtSSLCert.Name = "_txtSSLCert";
            this._txtSSLCert.Size = new System.Drawing.Size(37, 22);
            this._txtSSLCert.TabIndex = 6;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(16, 59);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 17);
            this.label24.TabIndex = 4;
            this.label24.Text = "SSL Certificate:";
            this._toolTips.SetToolTip(this.label24, resources.GetString("label24.ToolTip"));
            // 
            // _cboSSLCert
            // 
            this._cboSSLCert.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._cboSSLCert.DisplayMember = "Description";
            this._cboSSLCert.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cboSSLCert.FormattingEnabled = true;
            this._cboSSLCert.Location = new System.Drawing.Point(245, 55);
            this._cboSSLCert.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._cboSSLCert.Name = "_cboSSLCert";
            this._cboSSLCert.Size = new System.Drawing.Size(1021, 24);
            this._cboSSLCert.TabIndex = 5;
            this._cboSSLCert.ValueMember = "ID";
            this._cboSSLCert.SelectedIndexChanged += new System.EventHandler(this._cboSSLCert_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this._txtParamFile);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this._btnLoadParams);
            this.groupBox1.Controls.Add(this._txtMergeUtilityPath);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(8, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(1321, 90);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Deployment Management";
            // 
            // _toolTips
            // 
            this._toolTips.AutomaticDelay = 100;
            this._toolTips.AutoPopDelay = 20000;
            this._toolTips.InitialDelay = 100;
            this._toolTips.ReshowDelay = 20;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 27);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(169, 17);
            this.label20.TabIndex = 0;
            this.label20.Text = "RAAM Signing Certificate:";
            this._toolTips.SetToolTip(this.label20, "If the RAAM Signing Certificate is installed on this machine - select it from the" +
        " list to auto-populate the Thumbprint below.  Otherwise, enter the Thumprint man" +
        "ually (copied from somewhre else).");
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(12, 92);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(168, 17);
            this.label25.TabIndex = 4;
            this.label25.Text = "RAAM App Server (URL):";
            this._toolTips.SetToolTip(this.label25, "The Server Name where the RAAM Identity Server component is installed.  e.g. \"R36" +
        "0APP.internal-domain.net\"");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 92);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 17);
            this.label11.TabIndex = 7;
            this.label11.Text = "App Server (URL):";
            this._toolTips.SetToolTip(this.label11, "All servers use services on the APP Tier.  Enter the server name / VIP to use for" +
        " that here, e.g. \"R360APP.internal-domain.net\"");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 23);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 17);
            this.label8.TabIndex = 18;
            this.label8.Text = "SSRS Folder:";
            this._toolTips.SetToolTip(this.label8, "This is the name of the folder in SSRS that the reports are installed into.  It s" +
        "hould typically be the same for everyone.");
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 52);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(132, 17);
            this.label26.TabIndex = 20;
            this.label26.Text = "Physical File Share:";
            this._toolTips.SetToolTip(this.label26, "In some cases, reports must be stored temporarily to disk.  In load-balanced envi" +
        "ronements, this path may need to be accessible to all APP servers...?");
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(617, 27);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(180, 17);
            this.label27.TabIndex = 22;
            this.label27.Text = "SSRS Windows User Name";
            this._toolTips.SetToolTip(this.label27, "R360 uses Windows Authentication to access SSRS.  Enter the name of the Windows u" +
        "ser that has been granted access to the R360 reports in SSRS.");
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(617, 84);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(195, 17);
            this.label29.TabIndex = 26;
            this.label29.Text = "SSRS Windows User Domain:";
            this._toolTips.SetToolTip(this.label29, "If the user is a domain user, enter the domain name here (can be left blank for a" +
        " local user)");
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(12, 27);
            this.label35.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(132, 17);
            this.label35.TabIndex = 4;
            this.label35.Text = "RecHub DB Server:";
            this._toolTips.SetToolTip(this.label35, "The name of the server where the R360 database is installed.");
            // 
            // label36
            // 
            this.label36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(683, 59);
            this.label36.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(147, 17);
            this.label36.TabIndex = 12;
            this.label36.Text = "IntegraPAY DB Name:";
            this._toolTips.SetToolTip(this.label36, "The name of the IntegraPAY database.");
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(12, 59);
            this.label37.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(152, 17);
            this.label37.TabIndex = 8;
            this.label37.Text = "IntegraPAY DB Server:";
            this._toolTips.SetToolTip(this.label37, "The name of the server where the IntegraPAY database is installed.");
            // 
            // label38
            // 
            this.label38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(683, 27);
            this.label38.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(127, 17);
            this.label38.TabIndex = 10;
            this.label38.Text = "RecHub DB Name:";
            this._toolTips.SetToolTip(this.label38, "The name of the R360 database.  The default name is \"RecHub\", but any name may ha" +
        "ve been used.");
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(12, 23);
            this.label39.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(122, 17);
            this.label39.TabIndex = 18;
            this.label39.Text = "File Server (URL):";
            this._toolTips.SetToolTip(this.label39, resources.GetString("label39.ToolTip"));
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 91);
            this.label30.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 17);
            this.label30.TabIndex = 32;
            this.label30.Text = "RAAM Entity:";
            this._toolTips.SetToolTip(this.label30, "RAAM Users belong to an entity (e.g. \"WFS\").");
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 27);
            this.label31.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(126, 17);
            this.label31.TabIndex = 28;
            this.label31.Text = "RAAM User Name:";
            this._toolTips.SetToolTip(this.label31, "R360 uses RAAM Authentication to access images,etc. for Extracts.  Enter the name" +
        " of the RAAM user that has been configured for Scheduled Extract Processing.");
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(12, 91);
            this.label33.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(90, 17);
            this.label33.TabIndex = 32;
            this.label33.Text = "RAAM Entity:";
            this._toolTips.SetToolTip(this.label33, "RAAM Users belong to an entity (e.g. \"WFS\").");
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(12, 27);
            this.label34.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(126, 17);
            this.label34.TabIndex = 28;
            this.label34.Text = "RAAM User Name:";
            this._toolTips.SetToolTip(this.label34, "R360 uses RAAM Authentication to access images,etc.  Enter the name of the RAAM u" +
        "ser that has been configured for this DIT Client.");
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(12, 23);
            this.label41.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(122, 17);
            this.label41.TabIndex = 18;
            this.label41.Text = "File Server (URL):";
            this._toolTips.SetToolTip(this.label41, "CLIENT-Tier services may use additional services on the FILE-Tier.  Enter the nam" +
        "e of the  server here (e.g. \"R360IMG.internal-domain.net\").\r\n");
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(12, 23);
            this.label42.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(122, 17);
            this.label42.TabIndex = 18;
            this.label42.Text = "File Server (URL):";
            this._toolTips.SetToolTip(this.label42, "CLIENT-Tier services may use additional services on the FILE-Tier.  Enter the nam" +
        "e of the  server here (e.g. \"R360IMG.internal-domain.net\").\r\n");
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(12, 91);
            this.label43.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(90, 17);
            this.label43.TabIndex = 32;
            this.label43.Text = "RAAM Entity:";
            this._toolTips.SetToolTip(this.label43, "RAAM Users belong to an entity (e.g. \"WFS\").");
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(12, 27);
            this.label44.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(126, 17);
            this.label44.TabIndex = 28;
            this.label44.Text = "RAAM User Name:";
            this._toolTips.SetToolTip(this.label44, "R360 uses RAAM Authentication to access images,etc.  Enter the name of the RAAM u" +
        "ser that has been configured for this FIT Client.");
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(12, 23);
            this.label49.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(122, 17);
            this.label49.TabIndex = 18;
            this.label49.Text = "File Server (URL):";
            this._toolTips.SetToolTip(this.label49, "CLIENT-Tier services may use additional services on the FILE-Tier.  Enter the nam" +
        "e of the  server here (e.g. \"R360IMG.internal-domain.net\").");
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(12, 27);
            this.label50.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(132, 17);
            this.label50.TabIndex = 4;
            this.label50.Text = "RecHub DB Server:";
            this._toolTips.SetToolTip(this.label50, "The name of the server where the R360 database is installed.");
            // 
            // label53
            // 
            this.label53.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(683, 27);
            this.label53.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(127, 17);
            this.label53.TabIndex = 10;
            this.label53.Text = "RecHub DB Name:";
            this._toolTips.SetToolTip(this.label53, "The name of the R360 database.  The default name is \"RecHub\", but any name may ha" +
        "ve been used.");
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(12, 124);
            this.label46.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(176, 17);
            this.label46.TabIndex = 32;
            this.label46.Text = "RAAM System User Name:";
            this._toolTips.SetToolTip(this.label46, "R360 uses RAAM Authentication for service-to-service communication.  Enter the na" +
        "me of the RAAM System user that will used for this authentication.  (e.g. \"middl" +
        "etier\")");
            // 
            // _grpWebParams
            // 
            this._grpWebParams.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._grpWebParams.Controls.Add(this.label4);
            this._grpWebParams.Controls.Add(this._txtWebServerName);
            this._grpWebParams.Controls.Add(this.label6);
            this._grpWebParams.Controls.Add(this._txtDirectServerName);
            this._grpWebParams.Controls.Add(this.label5);
            this._grpWebParams.Controls.Add(this._txtRaamWebServerName);
            this._grpWebParams.Controls.Add(this._txtRaamValidationKey2);
            this._grpWebParams.Controls.Add(this.label7);
            this._grpWebParams.Controls.Add(this.label9);
            this._grpWebParams.Controls.Add(this._txtRaamDecryptionKey2);
            this._grpWebParams.Controls.Add(this._btnGenerateKeys);
            this._grpWebParams.Location = new System.Drawing.Point(8, 7);
            this._grpWebParams.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpWebParams.Name = "_grpWebParams";
            this._grpWebParams.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpWebParams.Size = new System.Drawing.Size(1321, 188);
            this._grpWebParams.TabIndex = 3;
            this._grpWebParams.TabStop = false;
            this._grpWebParams.Text = "WEB - TIER: RAAM Relying Party Parameters";
            this._toolTips.SetToolTip(this._grpWebParams, resources.GetString("_grpWebParams.ToolTip"));
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(12, 27);
            this.label48.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(148, 17);
            this.label48.TabIndex = 10;
            this.label48.Text = "Server Validation Key:";
            this._toolTips.SetToolTip(this.label48, resources.GetString("label48.ToolTip"));
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(12, 59);
            this.label51.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(154, 17);
            this.label51.TabIndex = 12;
            this.label51.Text = "Server Decryption Key:";
            this._toolTips.SetToolTip(this.label51, resources.GetString("label51.ToolTip"));
            // 
            // label63
            // 
            this.label63.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(744, 118);
            this.label63.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(73, 17);
            this.label63.TabIndex = 44;
            this.label63.Text = "Password:";
            this._toolTips.SetToolTip(this.label63, "The password for the connection string DBConnectionIP");
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(11, 122);
            this.label64.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(165, 17);
            this.label64.TabIndex = 42;
            this.label64.Text = "DBConnectionIP User ID:";
            this._toolTips.SetToolTip(this.label64, "The user id for connection string DBConnectionIP");
            // 
            // label67
            // 
            this.label67.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(744, 89);
            this.label67.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(73, 17);
            this.label67.TabIndex = 36;
            this.label67.Text = "Password:";
            this._toolTips.SetToolTip(this.label67, "The password for the connection string DBConnection2");
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(11, 97);
            this.label68.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(165, 17);
            this.label68.TabIndex = 34;
            this.label68.Text = "DBConnection2  User ID:";
            this._toolTips.SetToolTip(this.label68, "The user id for connection string DBConnection2");
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(16, 121);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(223, 17);
            this.label61.TabIndex = 9;
            this.label61.Text = "Notification Services server name:";
            this._toolTips.SetToolTip(this.label61, "Used by RAAM, the WEB tier, and the APP tier. Typically Notification Services is " +
        "installed on the FILE tier. Enter the server name / VIP to use for that here, e." +
        "g. \"R360FILE.internal-domain.net\"\r\n");
            // 
            // label62
            // 
            this.label62.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(675, 69);
            this.label62.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(73, 17);
            this.label62.TabIndex = 24;
            this.label62.Text = "Password:";
            this._toolTips.SetToolTip(this.label62, "The Password associated with the database connection string DBConnection2");
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(4, 64);
            this.label65.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(179, 17);
            this.label65.TabIndex = 22;
            this.label65.Text = "Connection String  User ID:";
            this._toolTips.SetToolTip(this.label65, "The UserID associated with the database connection string DBConnection2");
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label73, 3);
            this.label73.Location = new System.Drawing.Point(4, 0);
            this.label73.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(135, 17);
            this.label73.TabIndex = 15;
            this.label73.Text = "IIS Domain Windows";
            this._toolTips.SetToolTip(this.label73, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label74
            // 
            this.label74.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(20, 48);
            this.label74.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(73, 30);
            this.label74.TabIndex = 18;
            this.label74.Text = "Password:";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label74, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(20, 96);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 30);
            this.label16.TabIndex = 36;
            this.label16.Text = "Server:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label16, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label75
            // 
            this.label75.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(708, 96);
            this.label75.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(54, 30);
            this.label75.TabIndex = 37;
            this.label75.Text = "Server:";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label75, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label76, 3);
            this.label76.Location = new System.Drawing.Point(692, 0);
            this.label76.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(124, 17);
            this.label76.TabIndex = 38;
            this.label76.Text = "RecHub Database";
            this._toolTips.SetToolTip(this.label76, "The name of the server where RAAM, IdentityServerConfiguration, etc. are installe" +
        "d.");
            // 
            // label77
            // 
            this.label77.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(708, 48);
            this.label77.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(72, 30);
            this.label77.TabIndex = 39;
            this.label77.Text = "DB Name:";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label77, "The name of the server where the R360 database is installed.");
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label21, 3);
            this.label21.Location = new System.Drawing.Point(692, 282);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 17);
            this.label21.TabIndex = 40;
            this.label21.Text = "RecHub SSRS";
            this._toolTips.SetToolTip(this.label21, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label78, 3);
            this.label78.Location = new System.Drawing.Point(4, 126);
            this.label78.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(144, 17);
            this.label78.TabIndex = 41;
            this.label78.Text = "IntegraPAY Database";
            this._toolTips.SetToolTip(this.label78, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label79
            // 
            this.label79.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(20, 144);
            this.label79.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(54, 30);
            this.label79.TabIndex = 42;
            this.label79.Text = "Server:";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label79, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(20, 174);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 30);
            this.label19.TabIndex = 43;
            this.label19.Text = "DB Name:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label19, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label80
            // 
            this.label80.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(708, 299);
            this.label80.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(54, 30);
            this.label80.TabIndex = 44;
            this.label80.Text = "Server:";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label80, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label17, 3);
            this.label17.Location = new System.Drawing.Point(692, 126);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(156, 17);
            this.label17.TabIndex = 45;
            this.label17.Text = "Database Connection 2";
            this._toolTips.SetToolTip(this.label17, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label81
            // 
            this.label81.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(708, 144);
            this.label81.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(59, 30);
            this.label81.TabIndex = 46;
            this.label81.Text = "User ID:";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label81, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label82
            // 
            this.label82.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(708, 174);
            this.label82.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(69, 30);
            this.label82.TabIndex = 47;
            this.label82.Text = "Password";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label82, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label23, 3);
            this.label23.Location = new System.Drawing.Point(4, 282);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(230, 17);
            this.label23.TabIndex = 48;
            this.label23.Text = "integraPAY Consolidated Database";
            this._toolTips.SetToolTip(this.label23, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label52, 3);
            this.label52.Location = new System.Drawing.Point(4, 204);
            this.label52.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(160, 17);
            this.label52.TabIndex = 49;
            this.label52.Text = "Database Connection IP";
            this._toolTips.SetToolTip(this.label52, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label54
            // 
            this.label54.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(20, 222);
            this.label54.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(59, 30);
            this.label54.TabIndex = 50;
            this.label54.Text = "User ID:";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label54, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label83
            // 
            this.label83.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(20, 252);
            this.label83.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(69, 30);
            this.label83.TabIndex = 51;
            this.label83.Text = "Password";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label83, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label59
            // 
            this.label59.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(20, 359);
            this.label59.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(83, 30);
            this.label59.TabIndex = 55;
            this.label59.Text = "User Name:";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label59, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label60
            // 
            this.label60.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(20, 389);
            this.label60.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(73, 30);
            this.label60.TabIndex = 56;
            this.label60.Text = "Password:";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label60, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // label55
            // 
            this.label55.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(20, 329);
            this.label55.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(72, 30);
            this.label55.TabIndex = 57;
            this.label55.Text = "DB Name:";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label55, "The name of the server where the R360 database is installed.");
            // 
            // label56
            // 
            this.label56.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(20, 299);
            this.label56.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(54, 30);
            this.label56.TabIndex = 58;
            this.label56.Text = "Server:";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._toolTips.SetToolTip(this.label56, "RAAM / Framework use Windows Authentication to access SQL Server.  Enter the Doma" +
        "in\\Name of the Windows user that has been granted access to the RAAM / Framework" +
        " databases.");
            // 
            // _grpRaamIntegration
            // 
            this._grpRaamIntegration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._grpRaamIntegration.Controls.Add(this.label46);
            this._grpRaamIntegration.Controls.Add(this._txtRaamSystemUserName);
            this._grpRaamIntegration.Controls.Add(this.label47);
            this._grpRaamIntegration.Controls.Add(this._txtRaamSystemUserPassword);
            this._grpRaamIntegration.Controls.Add(this.label25);
            this._grpRaamIntegration.Controls.Add(this._cboRaamCert);
            this._grpRaamIntegration.Controls.Add(this.label20);
            this._grpRaamIntegration.Controls.Add(this._txtThumbPrint);
            this._grpRaamIntegration.Controls.Add(this.label3);
            this._grpRaamIntegration.Controls.Add(this._txtRaamAppServerName);
            this._grpRaamIntegration.Location = new System.Drawing.Point(8, 105);
            this._grpRaamIntegration.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpRaamIntegration.Name = "_grpRaamIntegration";
            this._grpRaamIntegration.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpRaamIntegration.Size = new System.Drawing.Size(1321, 185);
            this._grpRaamIntegration.TabIndex = 1;
            this._grpRaamIntegration.TabStop = false;
            this._grpRaamIntegration.Text = "RAAM Integration";
            // 
            // _txtRaamSystemUserName
            // 
            this._txtRaamSystemUserName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtRaamSystemUserName.Location = new System.Drawing.Point(231, 121);
            this._txtRaamSystemUserName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRaamSystemUserName.Name = "_txtRaamSystemUserName";
            this._txtRaamSystemUserName.Size = new System.Drawing.Size(1080, 22);
            this._txtRaamSystemUserName.TabIndex = 33;
            this._txtRaamSystemUserName.Text = "middletier";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(12, 156);
            this.label47.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(200, 17);
            this.label47.TabIndex = 34;
            this.label47.Text = "RAAM System User Password:";
            // 
            // _txtRaamSystemUserPassword
            // 
            this._txtRaamSystemUserPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtRaamSystemUserPassword.Location = new System.Drawing.Point(231, 153);
            this._txtRaamSystemUserPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRaamSystemUserPassword.Name = "_txtRaamSystemUserPassword";
            this._txtRaamSystemUserPassword.Size = new System.Drawing.Size(1080, 22);
            this._txtRaamSystemUserPassword.TabIndex = 35;
            this._txtRaamSystemUserPassword.UseSystemPasswordChar = true;
            // 
            // _cboRaamCert
            // 
            this._cboRaamCert.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._cboRaamCert.DisplayMember = "Description";
            this._cboRaamCert.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cboRaamCert.FormattingEnabled = true;
            this._cboRaamCert.Location = new System.Drawing.Point(191, 23);
            this._cboRaamCert.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._cboRaamCert.Name = "_cboRaamCert";
            this._cboRaamCert.Size = new System.Drawing.Size(1121, 24);
            this._cboRaamCert.TabIndex = 1;
            this._cboRaamCert.ValueMember = "ID";
            this._cboRaamCert.SelectedIndexChanged += new System.EventHandler(this._cboRaamCert_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this._txtNotificationServicesServerName);
            this.groupBox3.Controls.Add(this.label61);
            this.groupBox3.Controls.Add(this._txtAppServerName);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this._txtInstallDrive);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this._txtLogDrive);
            this.groupBox3.Controls.Add(this._cboSSLCert);
            this.groupBox3.Controls.Add(this._txtSSLCert);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Location = new System.Drawing.Point(8, 306);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(1321, 150);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "General Install Parameters";
            // 
            // _txtNotificationServicesServerName
            // 
            this._txtNotificationServicesServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtNotificationServicesServerName.Location = new System.Drawing.Point(245, 118);
            this._txtNotificationServicesServerName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._txtNotificationServicesServerName.Name = "_txtNotificationServicesServerName";
            this._txtNotificationServicesServerName.Size = new System.Drawing.Size(1065, 22);
            this._txtNotificationServicesServerName.TabIndex = 10;
            this._txtNotificationServicesServerName.Text = "e.g. FrameworkApp.internal-domain.net";
            // 
            // _txtAppServerName
            // 
            this._txtAppServerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtAppServerName.Location = new System.Drawing.Point(245, 89);
            this._txtAppServerName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtAppServerName.Name = "_txtAppServerName";
            this._txtAppServerName.Size = new System.Drawing.Size(1065, 22);
            this._txtAppServerName.TabIndex = 8;
            this._txtAppServerName.Text = "e.g. R360APP.internal-domain.net";
            // 
            // _grpDatabaseAccess
            // 
            this._grpDatabaseAccess.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._grpDatabaseAccess.Controls.Add(this.tableLayoutPanel2);
            this._grpDatabaseAccess.Location = new System.Drawing.Point(4, 63);
            this._grpDatabaseAccess.Margin = new System.Windows.Forms.Padding(4, 7, 13, 0);
            this._grpDatabaseAccess.Name = "_grpDatabaseAccess";
            this._grpDatabaseAccess.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpDatabaseAccess.Size = new System.Drawing.Size(1319, 443);
            this._grpDatabaseAccess.TabIndex = 4;
            this._grpDatabaseAccess.TabStop = false;
            this._grpDatabaseAccess.Text = "Database Access";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 537F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this._txtDBConnectionComExcepPassword, 2, 15);
            this.tableLayoutPanel2.Controls.Add(this.label52, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.label78, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this._txtDBIntegraPAYServer, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.label79, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this._txtDBConnectionIPassword, 2, 10);
            this.tableLayoutPanel2.Controls.Add(this.label73, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label18, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this._txtDBConnectionIPUser, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this._txtAppDomainUser, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label74, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this._txtDBIntegraPAYDBName, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this._txtAppDomainUserPassword, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.label16, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this._txtDBFrameworkServer, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.label14, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label76, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this._txtDBConnection2Password, 6, 7);
            this.tableLayoutPanel2.Controls.Add(this.label15, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this._txtDBConnection2User, 6, 6);
            this.tableLayoutPanel2.Controls.Add(this._txtDBRecHubServer, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.label77, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this._txtDBRecHubDBName, 6, 2);
            this.tableLayoutPanel2.Controls.Add(this.label12, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.label75, 5, 4);
            this.tableLayoutPanel2.Controls.Add(this._txtDBRaamServer, 6, 4);
            this.tableLayoutPanel2.Controls.Add(this.label19, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.label21, 4, 11);
            this.tableLayoutPanel2.Controls.Add(this.label17, 4, 5);
            this.tableLayoutPanel2.Controls.Add(this.label81, 5, 6);
            this.tableLayoutPanel2.Controls.Add(this.label82, 5, 7);
            this.tableLayoutPanel2.Controls.Add(this.label23, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.label54, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.label83, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.label80, 5, 12);
            this.tableLayoutPanel2.Controls.Add(this._txtRecHubSsrsServer, 6, 12);
            this.tableLayoutPanel2.Controls.Add(this.label59, 1, 14);
            this.tableLayoutPanel2.Controls.Add(this._txtDBConnectionComExcepUser, 2, 14);
            this.tableLayoutPanel2.Controls.Add(this.label60, 1, 15);
            this.tableLayoutPanel2.Controls.Add(this.label55, 1, 13);
            this.tableLayoutPanel2.Controls.Add(this.label56, 1, 12);
            this.tableLayoutPanel2.Controls.Add(this._txtDBConnectionComExcepServer, 2, 12);
            this.tableLayoutPanel2.Controls.Add(this._txtDBConnectionComExcepDBName, 2, 13);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(16, 0, 8, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 17;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1311, 420);
            this.tableLayoutPanel2.TabIndex = 35;
            // 
            // _txtDBConnectionComExcepPassword
            // 
            this._txtDBConnectionComExcepPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBConnectionComExcepPassword.Location = new System.Drawing.Point(115, 393);
            this._txtDBConnectionComExcepPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionComExcepPassword.Name = "_txtDBConnectionComExcepPassword";
            this._txtDBConnectionComExcepPassword.Size = new System.Drawing.Size(529, 22);
            this._txtDBConnectionComExcepPassword.TabIndex = 25;
            // 
            // _txtDBConnectionIPassword
            // 
            this._txtDBConnectionIPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBConnectionIPassword.Location = new System.Drawing.Point(115, 256);
            this._txtDBConnectionIPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionIPassword.Name = "_txtDBConnectionIPassword";
            this._txtDBConnectionIPassword.Size = new System.Drawing.Size(529, 22);
            this._txtDBConnectionIPassword.TabIndex = 29;
            // 
            // _txtDBConnectionIPUser
            // 
            this._txtDBConnectionIPUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBConnectionIPUser.Location = new System.Drawing.Point(115, 226);
            this._txtDBConnectionIPUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionIPUser.Name = "_txtDBConnectionIPUser";
            this._txtDBConnectionIPUser.Size = new System.Drawing.Size(529, 22);
            this._txtDBConnectionIPUser.TabIndex = 27;
            // 
            // _txtDBConnection2Password
            // 
            this._txtDBConnection2Password.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBConnection2Password.Location = new System.Drawing.Point(803, 178);
            this._txtDBConnection2Password.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnection2Password.Name = "_txtDBConnection2Password";
            this._txtDBConnection2Password.Size = new System.Drawing.Size(504, 22);
            this._txtDBConnection2Password.TabIndex = 21;
            // 
            // _txtDBConnection2User
            // 
            this._txtDBConnection2User.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBConnection2User.Location = new System.Drawing.Point(803, 148);
            this._txtDBConnection2User.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnection2User.Name = "_txtDBConnection2User";
            this._txtDBConnection2User.Size = new System.Drawing.Size(504, 22);
            this._txtDBConnection2User.TabIndex = 19;
            this._txtDBConnection2User.Text = "RecHubUser_User";
            // 
            // _txtDBConnectionComExcepUser
            // 
            this._txtDBConnectionComExcepUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBConnectionComExcepUser.Location = new System.Drawing.Point(115, 363);
            this._txtDBConnectionComExcepUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionComExcepUser.Name = "_txtDBConnectionComExcepUser";
            this._txtDBConnectionComExcepUser.Size = new System.Drawing.Size(529, 22);
            this._txtDBConnectionComExcepUser.TabIndex = 23;
            // 
            // _txtDBConnectionComExcepServer
            // 
            this._txtDBConnectionComExcepServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBConnectionComExcepServer.Location = new System.Drawing.Point(115, 303);
            this._txtDBConnectionComExcepServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionComExcepServer.Name = "_txtDBConnectionComExcepServer";
            this._txtDBConnectionComExcepServer.Size = new System.Drawing.Size(529, 22);
            this._txtDBConnectionComExcepServer.TabIndex = 59;
            // 
            // _txtDBConnectionComExcepDBName
            // 
            this._txtDBConnectionComExcepDBName.Dock = System.Windows.Forms.DockStyle.Fill;
            this._txtDBConnectionComExcepDBName.Location = new System.Drawing.Point(115, 333);
            this._txtDBConnectionComExcepDBName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionComExcepDBName.Name = "_txtDBConnectionComExcepDBName";
            this._txtDBConnectionComExcepDBName.Size = new System.Drawing.Size(529, 22);
            this._txtDBConnectionComExcepDBName.TabIndex = 60;
            // 
            // _tabControl
            // 
            this._tabControl.Controls.Add(this._tabGeneral);
            this._tabControl.Controls.Add(this._tabWebTier);
            this._tabControl.Controls.Add(this._tabAppTier);
            this._tabControl.Controls.Add(this._tabFileTier);
            this._tabControl.Controls.Add(this._tabClientDit);
            this._tabControl.Controls.Add(this._tabClientFit);
            this._tabControl.Controls.Add(this._tabClientExtract);
            this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabControl.Location = new System.Drawing.Point(0, 0);
            this._tabControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabControl.Name = "_tabControl";
            this._tabControl.SelectedIndex = 0;
            this._tabControl.Size = new System.Drawing.Size(1344, 889);
            this._tabControl.TabIndex = 7;
            // 
            // _tabGeneral
            // 
            this._tabGeneral.AutoScroll = true;
            this._tabGeneral.BackColor = System.Drawing.SystemColors.Control;
            this._tabGeneral.Controls.Add(this.groupBox1);
            this._tabGeneral.Controls.Add(this._grpRaamIntegration);
            this._tabGeneral.Controls.Add(this.groupBox3);
            this._tabGeneral.Location = new System.Drawing.Point(4, 25);
            this._tabGeneral.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabGeneral.Name = "_tabGeneral";
            this._tabGeneral.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabGeneral.Size = new System.Drawing.Size(1336, 860);
            this._tabGeneral.TabIndex = 0;
            this._tabGeneral.Text = "General";
            // 
            // _tabWebTier
            // 
            this._tabWebTier.AutoScroll = true;
            this._tabWebTier.BackColor = System.Drawing.SystemColors.Control;
            this._tabWebTier.Controls.Add(this._serverNamesWeb);
            this._tabWebTier.Controls.Add(this._grpWebParams);
            this._tabWebTier.Location = new System.Drawing.Point(4, 25);
            this._tabWebTier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabWebTier.Name = "_tabWebTier";
            this._tabWebTier.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabWebTier.Size = new System.Drawing.Size(1336, 860);
            this._tabWebTier.TabIndex = 1;
            this._tabWebTier.Text = "WEB - TIER";
            // 
            // _serverNamesWeb
            // 
            this._serverNamesWeb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._serverNamesWeb.Location = new System.Drawing.Point(8, 202);
            this._serverNamesWeb.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._serverNamesWeb.Name = "_serverNamesWeb";
            this._serverNamesWeb.Size = new System.Drawing.Size(1321, 129);
            this._serverNamesWeb.TabIndex = 4;
            // 
            // _tabAppTier
            // 
            this._tabAppTier.AutoScroll = true;
            this._tabAppTier.BackColor = System.Drawing.SystemColors.Control;
            this._tabAppTier.Controls.Add(this._pnlAppLayout);
            this._tabAppTier.Location = new System.Drawing.Point(4, 25);
            this._tabAppTier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabAppTier.Name = "_tabAppTier";
            this._tabAppTier.Size = new System.Drawing.Size(1336, 860);
            this._tabAppTier.TabIndex = 2;
            this._tabAppTier.Text = "APP - TIER";
            // 
            // _pnlAppLayout
            // 
            this._pnlAppLayout.AutoSize = true;
            this._pnlAppLayout.ColumnCount = 1;
            this._pnlAppLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._pnlAppLayout.Controls.Add(this._grpAppGeneral, 0, 0);
            this._pnlAppLayout.Controls.Add(this._serverNamesApp, 0, 4);
            this._pnlAppLayout.Controls.Add(this.groupBox7, 0, 3);
            this._pnlAppLayout.Controls.Add(this._grpReports, 0, 2);
            this._pnlAppLayout.Controls.Add(this._grpDatabaseAccess, 0, 1);
            this._pnlAppLayout.Dock = System.Windows.Forms.DockStyle.Top;
            this._pnlAppLayout.Location = new System.Drawing.Point(0, 0);
            this._pnlAppLayout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._pnlAppLayout.Name = "_pnlAppLayout";
            this._pnlAppLayout.RowCount = 5;
            this._pnlAppLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._pnlAppLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._pnlAppLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._pnlAppLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._pnlAppLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this._pnlAppLayout.Size = new System.Drawing.Size(1336, 854);
            this._pnlAppLayout.TabIndex = 9;
            // 
            // _grpAppGeneral
            // 
            this._grpAppGeneral.Controls.Add(this._txtFileServer);
            this._grpAppGeneral.Controls.Add(this.label22);
            this._grpAppGeneral.Location = new System.Drawing.Point(4, 4);
            this._grpAppGeneral.Margin = new System.Windows.Forms.Padding(4, 4, 13, 0);
            this._grpAppGeneral.Name = "_grpAppGeneral";
            this._grpAppGeneral.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpAppGeneral.Size = new System.Drawing.Size(1316, 52);
            this._grpAppGeneral.TabIndex = 5;
            this._grpAppGeneral.TabStop = false;
            this._grpAppGeneral.Text = "General";
            // 
            // _serverNamesApp
            // 
            this._serverNamesApp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._serverNamesApp.Location = new System.Drawing.Point(4, 725);
            this._serverNamesApp.Margin = new System.Windows.Forms.Padding(4, 6, 13, 0);
            this._serverNamesApp.Name = "_serverNamesApp";
            this._serverNamesApp.Size = new System.Drawing.Size(1319, 129);
            this._serverNamesApp.TabIndex = 8;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this._txtRaamValidationKey);
            this.groupBox7.Controls.Add(this.label48);
            this.groupBox7.Controls.Add(this.label51);
            this.groupBox7.Controls.Add(this._txtRaamDecryptionKey);
            this.groupBox7.Location = new System.Drawing.Point(4, 632);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4, 7, 13, 0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox7.Size = new System.Drawing.Size(1316, 87);
            this.groupBox7.TabIndex = 7;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "RAAM Encryption";
            // 
            // _txtRaamValidationKey
            // 
            this._txtRaamValidationKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtRaamValidationKey.Location = new System.Drawing.Point(196, 23);
            this._txtRaamValidationKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRaamValidationKey.Name = "_txtRaamValidationKey";
            this._txtRaamValidationKey.Size = new System.Drawing.Size(1111, 22);
            this._txtRaamValidationKey.TabIndex = 11;
            this._txtRaamValidationKey.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _txtRaamDecryptionKey
            // 
            this._txtRaamDecryptionKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtRaamDecryptionKey.Location = new System.Drawing.Point(196, 55);
            this._txtRaamDecryptionKey.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtRaamDecryptionKey.Name = "_txtRaamDecryptionKey";
            this._txtRaamDecryptionKey.Size = new System.Drawing.Size(1111, 22);
            this._txtRaamDecryptionKey.TabIndex = 13;
            this._txtRaamDecryptionKey.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _grpReports
            // 
            this._grpReports.Controls.Add(this.label29);
            this._grpReports.Controls.Add(this._txtSSRSDomain);
            this._grpReports.Controls.Add(this.label27);
            this._grpReports.Controls.Add(this._txtSSRSUser);
            this._grpReports.Controls.Add(this.label28);
            this._grpReports.Controls.Add(this._txtSSRSPassword);
            this._grpReports.Controls.Add(this._txtReportsFileShare);
            this._grpReports.Controls.Add(this.label26);
            this._grpReports.Controls.Add(this._txtSSRSFolder);
            this._grpReports.Controls.Add(this.label8);
            this._grpReports.Location = new System.Drawing.Point(4, 513);
            this._grpReports.Margin = new System.Windows.Forms.Padding(4, 7, 13, 0);
            this._grpReports.Name = "_grpReports";
            this._grpReports.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpReports.Size = new System.Drawing.Size(1316, 112);
            this._grpReports.TabIndex = 6;
            this._grpReports.TabStop = false;
            this._grpReports.Text = "Reports";
            // 
            // _txtSSRSDomain
            // 
            this._txtSSRSDomain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtSSRSDomain.Location = new System.Drawing.Point(845, 80);
            this._txtSSRSDomain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtSSRSDomain.Name = "_txtSSRSDomain";
            this._txtSSRSDomain.Size = new System.Drawing.Size(463, 22);
            this._txtSSRSDomain.TabIndex = 27;
            // 
            // _txtSSRSUser
            // 
            this._txtSSRSUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtSSRSUser.Location = new System.Drawing.Point(845, 23);
            this._txtSSRSUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtSSRSUser.Name = "_txtSSRSUser";
            this._txtSSRSUser.Size = new System.Drawing.Size(463, 22);
            this._txtSSRSUser.TabIndex = 23;
            this._txtSSRSUser.Text = "e.g. s.WFSReportUser";
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(617, 55);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(208, 17);
            this.label28.TabIndex = 24;
            this.label28.Text = "SSRS Windows User Password:";
            // 
            // _txtSSRSPassword
            // 
            this._txtSSRSPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtSSRSPassword.Location = new System.Drawing.Point(845, 52);
            this._txtSSRSPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtSSRSPassword.Name = "_txtSSRSPassword";
            this._txtSSRSPassword.Size = new System.Drawing.Size(463, 22);
            this._txtSSRSPassword.TabIndex = 25;
            this._txtSSRSPassword.UseSystemPasswordChar = true;
            // 
            // _txtReportsFileShare
            // 
            this._txtReportsFileShare.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtReportsFileShare.Location = new System.Drawing.Point(196, 48);
            this._txtReportsFileShare.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtReportsFileShare.Name = "_txtReportsFileShare";
            this._txtReportsFileShare.Size = new System.Drawing.Size(400, 22);
            this._txtReportsFileShare.TabIndex = 21;
            this._txtReportsFileShare.Text = "D:\\WFSApps\\Reports\\";
            // 
            // _txtSSRSFolder
            // 
            this._txtSSRSFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtSSRSFolder.Location = new System.Drawing.Point(196, 20);
            this._txtSSRSFolder.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtSSRSFolder.Name = "_txtSSRSFolder";
            this._txtSSRSFolder.Size = new System.Drawing.Size(400, 22);
            this._txtSSRSFolder.TabIndex = 19;
            this._txtSSRSFolder.Text = "R360Reports";
            // 
            // _tabFileTier
            // 
            this._tabFileTier.BackColor = System.Drawing.SystemColors.Control;
            this._tabFileTier.Controls.Add(this.flwFileTierTab);
            this._tabFileTier.Location = new System.Drawing.Point(4, 25);
            this._tabFileTier.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabFileTier.Name = "_tabFileTier";
            this._tabFileTier.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabFileTier.Size = new System.Drawing.Size(1336, 860);
            this._tabFileTier.TabIndex = 3;
            this._tabFileTier.Text = "FILE - TIER";
            // 
            // flwFileTierTab
            // 
            this.flwFileTierTab.Controls.Add(this._grpFileGeneral);
            this.flwFileTierTab.Controls.Add(this._grpDatabaseAccessFile);
            this.flwFileTierTab.Controls.Add(this.grpHostServiceWith);
            this.flwFileTierTab.Controls.Add(this._gprExtractProcessing);
            this.flwFileTierTab.Controls.Add(this._serverNamesFile);
            this.flwFileTierTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flwFileTierTab.Location = new System.Drawing.Point(4, 4);
            this.flwFileTierTab.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flwFileTierTab.Name = "flwFileTierTab";
            this.flwFileTierTab.Size = new System.Drawing.Size(1328, 852);
            this.flwFileTierTab.TabIndex = 9;
            // 
            // _grpFileGeneral
            // 
            this._grpFileGeneral.Controls.Add(this._txtFileServer2);
            this._grpFileGeneral.Controls.Add(this.label39);
            this._grpFileGeneral.Location = new System.Drawing.Point(4, 4);
            this._grpFileGeneral.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpFileGeneral.Name = "_grpFileGeneral";
            this._grpFileGeneral.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpFileGeneral.Size = new System.Drawing.Size(1319, 54);
            this._grpFileGeneral.TabIndex = 6;
            this._grpFileGeneral.TabStop = false;
            this._grpFileGeneral.Text = "General";
            // 
            // _txtFileServer2
            // 
            this._txtFileServer2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtFileServer2.Location = new System.Drawing.Point(196, 20);
            this._txtFileServer2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtFileServer2.Name = "_txtFileServer2";
            this._txtFileServer2.Size = new System.Drawing.Size(1113, 22);
            this._txtFileServer2.TabIndex = 19;
            this._txtFileServer2.Text = "e.g. R360FILE.internal-domain.net";
            this._txtFileServer2.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _grpDatabaseAccessFile
            // 
            this._grpDatabaseAccessFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._grpDatabaseAccessFile.Controls.Add(this._txtDBConnectionIPassword2);
            this._grpDatabaseAccessFile.Controls.Add(this.label63);
            this._grpDatabaseAccessFile.Controls.Add(this._txtDBConnectionIPUser2);
            this._grpDatabaseAccessFile.Controls.Add(this.label64);
            this._grpDatabaseAccessFile.Controls.Add(this._txtDBConnection2Password2);
            this._grpDatabaseAccessFile.Controls.Add(this.label67);
            this._grpDatabaseAccessFile.Controls.Add(this._txtDBConnection2User2);
            this._grpDatabaseAccessFile.Controls.Add(this.label68);
            this._grpDatabaseAccessFile.Controls.Add(this.label35);
            this._grpDatabaseAccessFile.Controls.Add(this._txtDBIntegraPAYDBName2);
            this._grpDatabaseAccessFile.Controls.Add(this._txtDBRecHubServer2);
            this._grpDatabaseAccessFile.Controls.Add(this.label36);
            this._grpDatabaseAccessFile.Controls.Add(this.label37);
            this._grpDatabaseAccessFile.Controls.Add(this._txtDBRecHubDBName2);
            this._grpDatabaseAccessFile.Controls.Add(this._txtDBIntegraPAYServer2);
            this._grpDatabaseAccessFile.Controls.Add(this.label38);
            this._grpDatabaseAccessFile.Location = new System.Drawing.Point(4, 66);
            this._grpDatabaseAccessFile.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpDatabaseAccessFile.Name = "_grpDatabaseAccessFile";
            this._grpDatabaseAccessFile.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._grpDatabaseAccessFile.Size = new System.Drawing.Size(1319, 162);
            this._grpDatabaseAccessFile.TabIndex = 5;
            this._grpDatabaseAccessFile.TabStop = false;
            this._grpDatabaseAccessFile.Text = "Database Access";
            // 
            // _txtDBConnectionIPassword2
            // 
            this._txtDBConnectionIPassword2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBConnectionIPassword2.Location = new System.Drawing.Point(847, 118);
            this._txtDBConnectionIPassword2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionIPassword2.Name = "_txtDBConnectionIPassword2";
            this._txtDBConnectionIPassword2.Size = new System.Drawing.Size(461, 22);
            this._txtDBConnectionIPassword2.TabIndex = 45;
            // 
            // _txtDBConnectionIPUser2
            // 
            this._txtDBConnectionIPUser2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBConnectionIPUser2.Location = new System.Drawing.Point(309, 118);
            this._txtDBConnectionIPUser2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionIPUser2.Name = "_txtDBConnectionIPUser2";
            this._txtDBConnectionIPUser2.Size = new System.Drawing.Size(403, 22);
            this._txtDBConnectionIPUser2.TabIndex = 43;
            // 
            // _txtDBConnection2Password2
            // 
            this._txtDBConnection2Password2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBConnection2Password2.Location = new System.Drawing.Point(847, 89);
            this._txtDBConnection2Password2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnection2Password2.Name = "_txtDBConnection2Password2";
            this._txtDBConnection2Password2.Size = new System.Drawing.Size(461, 22);
            this._txtDBConnection2Password2.TabIndex = 37;
            // 
            // _txtDBConnection2User2
            // 
            this._txtDBConnection2User2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBConnection2User2.Location = new System.Drawing.Point(309, 89);
            this._txtDBConnection2User2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnection2User2.Name = "_txtDBConnection2User2";
            this._txtDBConnection2User2.Size = new System.Drawing.Size(403, 22);
            this._txtDBConnection2User2.TabIndex = 35;
            // 
            // _txtDBIntegraPAYDBName2
            // 
            this._txtDBIntegraPAYDBName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBIntegraPAYDBName2.Location = new System.Drawing.Point(848, 55);
            this._txtDBIntegraPAYDBName2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBIntegraPAYDBName2.Name = "_txtDBIntegraPAYDBName2";
            this._txtDBIntegraPAYDBName2.Size = new System.Drawing.Size(461, 22);
            this._txtDBIntegraPAYDBName2.TabIndex = 13;
            this._txtDBIntegraPAYDBName2.Text = "IntegraPay";
            this._txtDBIntegraPAYDBName2.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _txtDBRecHubServer2
            // 
            this._txtDBRecHubServer2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBRecHubServer2.Location = new System.Drawing.Point(196, 23);
            this._txtDBRecHubServer2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBRecHubServer2.Name = "_txtDBRecHubServer2";
            this._txtDBRecHubServer2.Size = new System.Drawing.Size(463, 22);
            this._txtDBRecHubServer2.TabIndex = 5;
            this._txtDBRecHubServer2.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _txtDBRecHubDBName2
            // 
            this._txtDBRecHubDBName2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBRecHubDBName2.Location = new System.Drawing.Point(848, 23);
            this._txtDBRecHubDBName2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBRecHubDBName2.Name = "_txtDBRecHubDBName2";
            this._txtDBRecHubDBName2.Size = new System.Drawing.Size(461, 22);
            this._txtDBRecHubDBName2.TabIndex = 11;
            this._txtDBRecHubDBName2.Text = "WFSDB_R360";
            this._txtDBRecHubDBName2.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _txtDBIntegraPAYServer2
            // 
            this._txtDBIntegraPAYServer2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBIntegraPAYServer2.Location = new System.Drawing.Point(196, 55);
            this._txtDBIntegraPAYServer2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBIntegraPAYServer2.Name = "_txtDBIntegraPAYServer2";
            this._txtDBIntegraPAYServer2.Size = new System.Drawing.Size(463, 22);
            this._txtDBIntegraPAYServer2.TabIndex = 9;
            this._txtDBIntegraPAYServer2.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // grpHostServiceWith
            // 
            this.grpHostServiceWith.Controls.Add(this.flwHostServiceWith);
            this.grpHostServiceWith.Location = new System.Drawing.Point(4, 236);
            this.grpHostServiceWith.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpHostServiceWith.Name = "grpHostServiceWith";
            this.grpHostServiceWith.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpHostServiceWith.Size = new System.Drawing.Size(1315, 58);
            this.grpHostServiceWith.TabIndex = 0;
            this.grpHostServiceWith.TabStop = false;
            this.grpHostServiceWith.Text = "Host Services With";
            // 
            // flwHostServiceWith
            // 
            this.flwHostServiceWith.Controls.Add(this._cboHostServicesWith);
            this.flwHostServiceWith.Controls.Add(this._txtHostServicesWith);
            this.flwHostServiceWith.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flwHostServiceWith.Location = new System.Drawing.Point(4, 19);
            this.flwHostServiceWith.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flwHostServiceWith.Name = "flwHostServiceWith";
            this.flwHostServiceWith.Size = new System.Drawing.Size(1307, 35);
            this.flwHostServiceWith.TabIndex = 9;
            // 
            // _cboHostServicesWith
            // 
            this._cboHostServicesWith.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cboHostServicesWith.FormattingEnabled = true;
            this._cboHostServicesWith.Items.AddRange(new object[] {
            "IIS",
            "Stand Alone Windows Service"});
            this._cboHostServicesWith.Location = new System.Drawing.Point(4, 4);
            this._cboHostServicesWith.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._cboHostServicesWith.Name = "_cboHostServicesWith";
            this._cboHostServicesWith.Size = new System.Drawing.Size(401, 24);
            this._cboHostServicesWith.TabIndex = 0;
            // 
            // _txtHostServicesWith
            // 
            this._txtHostServicesWith.Location = new System.Drawing.Point(413, 4);
            this._txtHostServicesWith.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtHostServicesWith.Name = "_txtHostServicesWith";
            this._txtHostServicesWith.Size = new System.Drawing.Size(132, 22);
            this._txtHostServicesWith.TabIndex = 2;
            this._txtHostServicesWith.Text = "IIS";
            this._txtHostServicesWith.Visible = false;
            // 
            // _gprExtractProcessing
            // 
            this._gprExtractProcessing.Controls.Add(this.label30);
            this._gprExtractProcessing.Controls.Add(this._txtExtractEntity);
            this._gprExtractProcessing.Controls.Add(this.label31);
            this._gprExtractProcessing.Controls.Add(this._txtExtractUser);
            this._gprExtractProcessing.Controls.Add(this.label32);
            this._gprExtractProcessing.Controls.Add(this._txtExtractPassword);
            this._gprExtractProcessing.Location = new System.Drawing.Point(4, 302);
            this._gprExtractProcessing.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._gprExtractProcessing.Name = "_gprExtractProcessing";
            this._gprExtractProcessing.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._gprExtractProcessing.Size = new System.Drawing.Size(1319, 124);
            this._gprExtractProcessing.TabIndex = 7;
            this._gprExtractProcessing.TabStop = false;
            this._gprExtractProcessing.Text = "Extract Processing";
            // 
            // _txtExtractEntity
            // 
            this._txtExtractEntity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtExtractEntity.Location = new System.Drawing.Point(196, 87);
            this._txtExtractEntity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtExtractEntity.Name = "_txtExtractEntity";
            this._txtExtractEntity.Size = new System.Drawing.Size(1113, 22);
            this._txtExtractEntity.TabIndex = 33;
            // 
            // _txtExtractUser
            // 
            this._txtExtractUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtExtractUser.Location = new System.Drawing.Point(196, 23);
            this._txtExtractUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtExtractUser.Name = "_txtExtractUser";
            this._txtExtractUser.Size = new System.Drawing.Size(1113, 22);
            this._txtExtractUser.TabIndex = 29;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(12, 59);
            this.label32.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(150, 17);
            this.label32.TabIndex = 30;
            this.label32.Text = "RAAM User Password:";
            // 
            // _txtExtractPassword
            // 
            this._txtExtractPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtExtractPassword.Location = new System.Drawing.Point(196, 55);
            this._txtExtractPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtExtractPassword.Name = "_txtExtractPassword";
            this._txtExtractPassword.Size = new System.Drawing.Size(1113, 22);
            this._txtExtractPassword.TabIndex = 31;
            this._txtExtractPassword.UseSystemPasswordChar = true;
            // 
            // _serverNamesFile
            // 
            this._serverNamesFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._serverNamesFile.Location = new System.Drawing.Point(3, 432);
            this._serverNamesFile.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this._serverNamesFile.Name = "_serverNamesFile";
            this._serverNamesFile.Size = new System.Drawing.Size(1319, 129);
            this._serverNamesFile.TabIndex = 8;
            // 
            // _tabClientDit
            // 
            this._tabClientDit.BackColor = System.Drawing.SystemColors.Control;
            this._tabClientDit.Controls.Add(this.groupBox4);
            this._tabClientDit.Controls.Add(this.groupBox2);
            this._tabClientDit.Location = new System.Drawing.Point(4, 25);
            this._tabClientDit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabClientDit.Name = "_tabClientDit";
            this._tabClientDit.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabClientDit.Size = new System.Drawing.Size(1336, 860);
            this._tabClientDit.TabIndex = 4;
            this._tabClientDit.Text = "CLIENT-DIT";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this._txtFileServer3);
            this.groupBox4.Controls.Add(this.label41);
            this.groupBox4.Location = new System.Drawing.Point(8, 7);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(1319, 54);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "General";
            // 
            // _txtFileServer3
            // 
            this._txtFileServer3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtFileServer3.Location = new System.Drawing.Point(196, 20);
            this._txtFileServer3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtFileServer3.Name = "_txtFileServer3";
            this._txtFileServer3.Size = new System.Drawing.Size(1113, 22);
            this._txtFileServer3.TabIndex = 19;
            this._txtFileServer3.Text = "e.g. R360FILE.internal-domain.net";
            this._txtFileServer3.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this._txtDitClientEntity);
            this.groupBox2.Controls.Add(this.label34);
            this.groupBox2.Controls.Add(this._txtDitClientUser);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this._txtDitClientPassword);
            this.groupBox2.Location = new System.Drawing.Point(8, 69);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(1319, 124);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Client Credentials";
            // 
            // _txtDitClientEntity
            // 
            this._txtDitClientEntity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDitClientEntity.Location = new System.Drawing.Point(196, 87);
            this._txtDitClientEntity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDitClientEntity.Name = "_txtDitClientEntity";
            this._txtDitClientEntity.Size = new System.Drawing.Size(1113, 22);
            this._txtDitClientEntity.TabIndex = 33;
            // 
            // _txtDitClientUser
            // 
            this._txtDitClientUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDitClientUser.Location = new System.Drawing.Point(196, 23);
            this._txtDitClientUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDitClientUser.Name = "_txtDitClientUser";
            this._txtDitClientUser.Size = new System.Drawing.Size(1113, 22);
            this._txtDitClientUser.TabIndex = 29;
            this._txtDitClientUser.Text = "DITUser";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 59);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(150, 17);
            this.label40.TabIndex = 30;
            this.label40.Text = "RAAM User Password:";
            // 
            // _txtDitClientPassword
            // 
            this._txtDitClientPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDitClientPassword.Location = new System.Drawing.Point(196, 55);
            this._txtDitClientPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDitClientPassword.Name = "_txtDitClientPassword";
            this._txtDitClientPassword.Size = new System.Drawing.Size(1113, 22);
            this._txtDitClientPassword.TabIndex = 31;
            this._txtDitClientPassword.UseSystemPasswordChar = true;
            // 
            // _tabClientFit
            // 
            this._tabClientFit.BackColor = System.Drawing.SystemColors.Control;
            this._tabClientFit.Controls.Add(this.flowLayoutPanel1);
            this._tabClientFit.Location = new System.Drawing.Point(4, 25);
            this._tabClientFit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabClientFit.Name = "_tabClientFit";
            this._tabClientFit.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabClientFit.Size = new System.Drawing.Size(1336, 860);
            this._tabClientFit.TabIndex = 5;
            this._tabClientFit.Text = "CLIENT-FIT";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.groupBox5);
            this.flowLayoutPanel1.Controls.Add(this.groupBox6);
            this.flowLayoutPanel1.Controls.Add(this.grpCDSDB);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(4, 4);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1328, 852);
            this.flowLayoutPanel1.TabIndex = 10;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this._txtFileServer4);
            this.groupBox5.Controls.Add(this.label42);
            this.groupBox5.Location = new System.Drawing.Point(4, 4);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Size = new System.Drawing.Size(1319, 54);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "General";
            // 
            // _txtFileServer4
            // 
            this._txtFileServer4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtFileServer4.Location = new System.Drawing.Point(196, 20);
            this._txtFileServer4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtFileServer4.Name = "_txtFileServer4";
            this._txtFileServer4.Size = new System.Drawing.Size(1113, 22);
            this._txtFileServer4.TabIndex = 19;
            this._txtFileServer4.Text = "e.g. R360FILE.internal-domain.net";
            this._txtFileServer4.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label43);
            this.groupBox6.Controls.Add(this._txtFitClientEntity);
            this.groupBox6.Controls.Add(this.label44);
            this.groupBox6.Controls.Add(this._txtFitClientUser);
            this.groupBox6.Controls.Add(this.label45);
            this.groupBox6.Controls.Add(this._txtFitClientPassword);
            this.groupBox6.Location = new System.Drawing.Point(4, 66);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Size = new System.Drawing.Size(1319, 124);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Client Credentials";
            // 
            // _txtFitClientEntity
            // 
            this._txtFitClientEntity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtFitClientEntity.Location = new System.Drawing.Point(196, 87);
            this._txtFitClientEntity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtFitClientEntity.Name = "_txtFitClientEntity";
            this._txtFitClientEntity.Size = new System.Drawing.Size(1113, 22);
            this._txtFitClientEntity.TabIndex = 33;
            // 
            // _txtFitClientUser
            // 
            this._txtFitClientUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtFitClientUser.Location = new System.Drawing.Point(196, 23);
            this._txtFitClientUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtFitClientUser.Name = "_txtFitClientUser";
            this._txtFitClientUser.Size = new System.Drawing.Size(1113, 22);
            this._txtFitClientUser.TabIndex = 29;
            this._txtFitClientUser.Text = "FITUser";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(12, 59);
            this.label45.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(150, 17);
            this.label45.TabIndex = 30;
            this.label45.Text = "RAAM User Password:";
            // 
            // _txtFitClientPassword
            // 
            this._txtFitClientPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtFitClientPassword.Location = new System.Drawing.Point(196, 55);
            this._txtFitClientPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtFitClientPassword.Name = "_txtFitClientPassword";
            this._txtFitClientPassword.Size = new System.Drawing.Size(1113, 22);
            this._txtFitClientPassword.TabIndex = 31;
            this._txtFitClientPassword.UseSystemPasswordChar = true;
            // 
            // grpCDSDB
            // 
            this.grpCDSDB.Controls.Add(this.tableLayoutPanel1);
            this.grpCDSDB.Location = new System.Drawing.Point(4, 198);
            this.grpCDSDB.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpCDSDB.Name = "grpCDSDB";
            this.grpCDSDB.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grpCDSDB.Size = new System.Drawing.Size(1319, 121);
            this.grpCDSDB.TabIndex = 10;
            this.grpCDSDB.TabStop = false;
            this.grpCDSDB.Text = "Consolidator Database Connection";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 212F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00129F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 233F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.99871F));
            this.tableLayoutPanel1.Controls.Add(this.txtCENDSPath, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label72, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label71, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtFITIntegraPAYDatabase, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label70, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtFITIntegraPAYPassword, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label69, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtFITIntegraPAYUser, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label66, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtFITIntegraPAYServer, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1311, 98);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // txtCENDSPath
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txtCENDSPath, 3);
            this.txtCENDSPath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCENDSPath.Location = new System.Drawing.Point(216, 68);
            this.txtCENDSPath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtCENDSPath.Name = "txtCENDSPath";
            this.txtCENDSPath.Size = new System.Drawing.Size(1091, 22);
            this.txtCENDSPath.TabIndex = 9;
            this.txtCENDSPath.Text = "D:\\WFSApps\\CENDS\\";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label72.Location = new System.Drawing.Point(4, 64);
            this.label72.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(204, 32);
            this.label72.TabIndex = 8;
            this.label72.Text = "CENDS Path:";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label71.Location = new System.Drawing.Point(649, 0);
            this.label71.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(225, 32);
            this.label71.TabIndex = 7;
            this.label71.Text = "IntegraPAY Database Name:";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFITIntegraPAYDatabase
            // 
            this.txtFITIntegraPAYDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFITIntegraPAYDatabase.Location = new System.Drawing.Point(882, 4);
            this.txtFITIntegraPAYDatabase.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFITIntegraPAYDatabase.Name = "txtFITIntegraPAYDatabase";
            this.txtFITIntegraPAYDatabase.Size = new System.Drawing.Size(425, 22);
            this.txtFITIntegraPAYDatabase.TabIndex = 2;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label70.Location = new System.Drawing.Point(649, 32);
            this.label70.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(225, 32);
            this.label70.TabIndex = 4;
            this.label70.Text = "IntegraPAY Database Password:";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFITIntegraPAYPassword
            // 
            this.txtFITIntegraPAYPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFITIntegraPAYPassword.Location = new System.Drawing.Point(882, 36);
            this.txtFITIntegraPAYPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFITIntegraPAYPassword.Name = "txtFITIntegraPAYPassword";
            this.txtFITIntegraPAYPassword.Size = new System.Drawing.Size(425, 22);
            this.txtFITIntegraPAYPassword.TabIndex = 5;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label69.Location = new System.Drawing.Point(4, 32);
            this.label69.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(204, 32);
            this.label69.TabIndex = 2;
            this.label69.Text = "IntegraPAY Database User:";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFITIntegraPAYUser
            // 
            this.txtFITIntegraPAYUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFITIntegraPAYUser.Location = new System.Drawing.Point(216, 36);
            this.txtFITIntegraPAYUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFITIntegraPAYUser.Name = "txtFITIntegraPAYUser";
            this.txtFITIntegraPAYUser.Size = new System.Drawing.Size(425, 22);
            this.txtFITIntegraPAYUser.TabIndex = 3;
            this.txtFITIntegraPAYUser.Text = "IPAdmin";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label66.Location = new System.Drawing.Point(4, 0);
            this.label66.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(204, 32);
            this.label66.TabIndex = 0;
            this.label66.Text = "IntegraPAY Database Server:";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFITIntegraPAYServer
            // 
            this.txtFITIntegraPAYServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFITIntegraPAYServer.Location = new System.Drawing.Point(216, 4);
            this.txtFITIntegraPAYServer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtFITIntegraPAYServer.Name = "txtFITIntegraPAYServer";
            this.txtFITIntegraPAYServer.Size = new System.Drawing.Size(425, 22);
            this.txtFITIntegraPAYServer.TabIndex = 1;
            // 
            // _tabClientExtract
            // 
            this._tabClientExtract.BackColor = System.Drawing.SystemColors.Control;
            this._tabClientExtract.Controls.Add(this.groupBox8);
            this._tabClientExtract.Controls.Add(this.groupBox9);
            this._tabClientExtract.Location = new System.Drawing.Point(4, 25);
            this._tabClientExtract.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabClientExtract.Name = "_tabClientExtract";
            this._tabClientExtract.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._tabClientExtract.Size = new System.Drawing.Size(1336, 860);
            this._tabClientExtract.TabIndex = 6;
            this._tabClientExtract.Text = "CLIENT - EXTRACT";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this._txtFileServer5);
            this.groupBox8.Controls.Add(this.label49);
            this.groupBox8.Location = new System.Drawing.Point(8, 7);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox8.Size = new System.Drawing.Size(1319, 54);
            this.groupBox8.TabIndex = 6;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "General";
            // 
            // _txtFileServer5
            // 
            this._txtFileServer5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtFileServer5.Location = new System.Drawing.Point(196, 20);
            this._txtFileServer5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtFileServer5.Name = "_txtFileServer5";
            this._txtFileServer5.Size = new System.Drawing.Size(1113, 22);
            this._txtFileServer5.TabIndex = 19;
            this._txtFileServer5.Text = "e.g. R360FILE.internal-domain.net";
            this._txtFileServer5.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this._txtDBConnectionExtractPassword);
            this.groupBox9.Controls.Add(this.label62);
            this.groupBox9.Controls.Add(this._txtDBConnectionExtractUser);
            this.groupBox9.Controls.Add(this.label65);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this._txtDBRecHubServer5);
            this.groupBox9.Controls.Add(this._txtDBRecHubDBName5);
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Location = new System.Drawing.Point(8, 69);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox9.Size = new System.Drawing.Size(1319, 102);
            this.groupBox9.TabIndex = 5;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Database Access";
            // 
            // _txtDBConnectionExtractPassword
            // 
            this._txtDBConnectionExtractPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBConnectionExtractPassword.Location = new System.Drawing.Point(757, 60);
            this._txtDBConnectionExtractPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionExtractPassword.Name = "_txtDBConnectionExtractPassword";
            this._txtDBConnectionExtractPassword.Size = new System.Drawing.Size(461, 22);
            this._txtDBConnectionExtractPassword.TabIndex = 25;
            // 
            // _txtDBConnectionExtractUser
            // 
            this._txtDBConnectionExtractUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBConnectionExtractUser.Location = new System.Drawing.Point(196, 60);
            this._txtDBConnectionExtractUser.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBConnectionExtractUser.Name = "_txtDBConnectionExtractUser";
            this._txtDBConnectionExtractUser.Size = new System.Drawing.Size(403, 22);
            this._txtDBConnectionExtractUser.TabIndex = 23;
            this._txtDBConnectionExtractUser.Text = "RecHubExtractWizard_User";
            // 
            // _txtDBRecHubServer5
            // 
            this._txtDBRecHubServer5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBRecHubServer5.Location = new System.Drawing.Point(196, 23);
            this._txtDBRecHubServer5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBRecHubServer5.Name = "_txtDBRecHubServer5";
            this._txtDBRecHubServer5.Size = new System.Drawing.Size(463, 22);
            this._txtDBRecHubServer5.TabIndex = 5;
            this._txtDBRecHubServer5.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // _txtDBRecHubDBName5
            // 
            this._txtDBRecHubDBName5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._txtDBRecHubDBName5.Location = new System.Drawing.Point(848, 23);
            this._txtDBRecHubDBName5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this._txtDBRecHubDBName5.Name = "_txtDBRecHubDBName5";
            this._txtDBRecHubDBName5.Size = new System.Drawing.Size(461, 22);
            this._txtDBRecHubDBName5.TabIndex = 11;
            this._txtDBRecHubDBName5.Text = "WFSDB_R360";
            this._txtDBRecHubDBName5.TextChanged += new System.EventHandler(this.DuplicatedTextBox_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel1.Controls.Add(this._btnSave);
            this.panel1.Controls.Add(this._btnCancel);
            this.panel1.Location = new System.Drawing.Point(479, 6);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 38);
            this.panel1.TabIndex = 8;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 889);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1344, 48);
            this.panel2.TabIndex = 9;
            // 
            // ConfigForm
            // 
            this.AcceptButton = this._btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.CancelButton = this._btnCancel;
            this.ClientSize = new System.Drawing.Size(1344, 937);
            this.Controls.Add(this._tabControl);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MinimumSize = new System.Drawing.Size(594, 294);
            this.Name = "ConfigForm";
            this.Text = "Deployment Configuration Values";
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this._grpWebParams.ResumeLayout(false);
            this._grpWebParams.PerformLayout();
            this._grpRaamIntegration.ResumeLayout(false);
            this._grpRaamIntegration.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this._grpDatabaseAccess.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this._tabControl.ResumeLayout(false);
            this._tabGeneral.ResumeLayout(false);
            this._tabWebTier.ResumeLayout(false);
            this._tabAppTier.ResumeLayout(false);
            this._tabAppTier.PerformLayout();
            this._pnlAppLayout.ResumeLayout(false);
            this._grpAppGeneral.ResumeLayout(false);
            this._grpAppGeneral.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this._grpReports.ResumeLayout(false);
            this._grpReports.PerformLayout();
            this._tabFileTier.ResumeLayout(false);
            this.flwFileTierTab.ResumeLayout(false);
            this._grpFileGeneral.ResumeLayout(false);
            this._grpFileGeneral.PerformLayout();
            this._grpDatabaseAccessFile.ResumeLayout(false);
            this._grpDatabaseAccessFile.PerformLayout();
            this.grpHostServiceWith.ResumeLayout(false);
            this.flwHostServiceWith.ResumeLayout(false);
            this.flwHostServiceWith.PerformLayout();
            this._gprExtractProcessing.ResumeLayout(false);
            this._gprExtractProcessing.PerformLayout();
            this._tabClientDit.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this._tabClientFit.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.grpCDSDB.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this._tabClientExtract.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox _txtParamFile;
		private System.Windows.Forms.TextBox _txtThumbPrint;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox _txtWebServerName;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox _txtDirectServerName;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox _txtRaamWebServerName;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox _txtRaamValidationKey2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox _txtRaamDecryptionKey2;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Button _btnGenerateKeys;
		private System.Windows.Forms.TextBox _txtRaamAppServerName;
		private System.Windows.Forms.Button _btnSave;
		private System.Windows.Forms.Button _btnCancel;
		private System.Windows.Forms.Button _btnLoadParams;
		private System.Windows.Forms.TextBox _txtInstallDrive;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox _txtLogDrive;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox _txtDBFrameworkServer;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox _txtAppDomainUserPassword;
		private System.Windows.Forms.TextBox _txtAppDomainUser;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox _txtDBRaamServer;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox _txtDBRecHubServer;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.TextBox _txtDBIntegraPAYServer;
		private System.Windows.Forms.TextBox _txtDBRecHubDBName;
		private System.Windows.Forms.TextBox _txtDBIntegraPAYDBName;
		private System.Windows.Forms.TextBox _txtRecHubSsrsServer;
		private System.Windows.Forms.TextBox _txtMergeUtilityPath;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox _txtFileServer;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox _txtSSLCert;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.ComboBox _cboSSLCert;
		private System.Windows.Forms.ToolTip _toolTips;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox _grpRaamIntegration;
		private System.Windows.Forms.ComboBox _cboRaamCert;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox _txtAppServerName;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.GroupBox _grpWebParams;
		private System.Windows.Forms.GroupBox _grpDatabaseAccess;
		private System.Windows.Forms.TabControl _tabControl;
		private System.Windows.Forms.TabPage _tabGeneral;
		private System.Windows.Forms.TabPage _tabWebTier;
		private System.Windows.Forms.TabPage _tabAppTier;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.GroupBox _grpAppGeneral;
		private System.Windows.Forms.GroupBox _grpReports;
		private System.Windows.Forms.TextBox _txtSSRSFolder;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.TextBox _txtSSRSDomain;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox _txtSSRSUser;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.TextBox _txtSSRSPassword;
		private System.Windows.Forms.TextBox _txtReportsFileShare;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.TabPage _tabFileTier;
		private System.Windows.Forms.GroupBox _grpFileGeneral;
		private System.Windows.Forms.TextBox _txtFileServer2;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.GroupBox _grpDatabaseAccessFile;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.TextBox _txtDBIntegraPAYDBName2;
		private System.Windows.Forms.TextBox _txtDBRecHubServer2;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.TextBox _txtDBRecHubDBName2;
		private System.Windows.Forms.TextBox _txtDBIntegraPAYServer2;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.GroupBox _gprExtractProcessing;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.TextBox _txtExtractEntity;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.TextBox _txtExtractUser;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.TextBox _txtExtractPassword;
		private System.Windows.Forms.TabPage _tabClientDit;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TextBox _txtFileServer3;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.TextBox _txtDitClientEntity;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.TextBox _txtDitClientUser;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.TextBox _txtDitClientPassword;
		private System.Windows.Forms.TabPage _tabClientFit;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.TextBox _txtFileServer4;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.TextBox _txtFitClientEntity;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.TextBox _txtFitClientUser;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.TextBox _txtFitClientPassword;
		private System.Windows.Forms.TabPage _tabClientExtract;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.TextBox _txtFileServer5;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.TextBox _txtDBRecHubServer5;
		private System.Windows.Forms.TextBox _txtDBRecHubDBName5;
		private System.Windows.Forms.Label label53;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.TextBox _txtRaamSystemUserName;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.TextBox _txtRaamSystemUserPassword;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.TextBox _txtRaamValidationKey;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label51;
		private System.Windows.Forms.TextBox _txtRaamDecryptionKey;
        private System.Windows.Forms.TextBox _txtDBConnectionIPassword;
        private System.Windows.Forms.TextBox _txtDBConnectionIPUser;
        private System.Windows.Forms.TextBox _txtDBConnectionComExcepPassword;
        private System.Windows.Forms.TextBox _txtDBConnectionComExcepUser;
        private System.Windows.Forms.TextBox _txtDBConnection2Password;
        private System.Windows.Forms.TextBox _txtDBConnection2User;
        private System.Windows.Forms.TextBox _txtDBConnectionIPassword2;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.TextBox _txtDBConnectionIPUser2;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox _txtDBConnection2Password2;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox _txtDBConnection2User2;
        private System.Windows.Forms.Label label68;
        private ServerNamesUserControl _serverNamesWeb;
        private ServerNamesUserControl _serverNamesApp;
        private ServerNamesUserControl _serverNamesFile;
        private System.Windows.Forms.TextBox _txtNotificationServicesServerName;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox _txtDBConnectionExtractPassword;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TextBox _txtDBConnectionExtractUser;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TableLayoutPanel _pnlAppLayout;
        private System.Windows.Forms.FlowLayoutPanel flwFileTierTab;
        private System.Windows.Forms.GroupBox grpHostServiceWith;
        private System.Windows.Forms.FlowLayoutPanel flwHostServiceWith;
        private System.Windows.Forms.ComboBox _cboHostServicesWith;
        private System.Windows.Forms.TextBox _txtHostServicesWith;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.GroupBox grpCDSDB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox txtFITIntegraPAYPassword;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox txtFITIntegraPAYUser;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox txtFITIntegraPAYServer;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtFITIntegraPAYDatabase;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txtCENDSPath;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox _txtDBConnectionComExcepServer;
        private System.Windows.Forms.TextBox _txtDBConnectionComExcepDBName;
    }
}