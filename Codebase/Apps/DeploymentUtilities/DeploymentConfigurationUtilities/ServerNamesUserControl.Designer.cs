﻿namespace Wfs.R360.DeploymentConfigurationUtilities
{
    partial class ServerNamesUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerNamesUserControl));
            this.grpServerNames = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblExplanatoryText = new System.Windows.Forms.Label();
            this.txtServerNames = new System.Windows.Forms.TextBox();
            this.grpServerNames.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpServerNames
            // 
            this.grpServerNames.Controls.Add(this.tableLayoutPanel1);
            this.grpServerNames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpServerNames.Location = new System.Drawing.Point(0, 0);
            this.grpServerNames.Name = "grpServerNames";
            this.grpServerNames.Size = new System.Drawing.Size(753, 129);
            this.grpServerNames.TabIndex = 0;
            this.grpServerNames.TabStop = false;
            this.grpServerNames.Text = "Server Names";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.txtServerNames, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblExplanatoryText, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(747, 108);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // lblExplanatoryText
            // 
            this.lblExplanatoryText.AutoSize = true;
            this.lblExplanatoryText.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExplanatoryText.Location = new System.Drawing.Point(278, 6);
            this.lblExplanatoryText.Margin = new System.Windows.Forms.Padding(10, 6, 3, 0);
            this.lblExplanatoryText.Name = "lblExplanatoryText";
            this.lblExplanatoryText.Size = new System.Drawing.Size(466, 102);
            this.lblExplanatoryText.TabIndex = 2;
            this.lblExplanatoryText.Text = resources.GetString("lblExplanatoryText.Text");
            // 
            // txtServerNames
            // 
            this.txtServerNames.AcceptsReturn = true;
            this.txtServerNames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtServerNames.Location = new System.Drawing.Point(3, 3);
            this.txtServerNames.Multiline = true;
            this.txtServerNames.Name = "txtServerNames";
            this.txtServerNames.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtServerNames.Size = new System.Drawing.Size(262, 102);
            this.txtServerNames.TabIndex = 3;
            this.txtServerNames.WordWrap = false;
            // 
            // ServerNamesUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpServerNames);
            this.Name = "ServerNamesUserControl";
            this.Size = new System.Drawing.Size(753, 129);
            this.grpServerNames.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpServerNames;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox txtServerNames;
        private System.Windows.Forms.Label lblExplanatoryText;
    }
}
