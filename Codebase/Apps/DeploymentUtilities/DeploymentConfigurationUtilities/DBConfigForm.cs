﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace Wfs.R360.DeploymentConfigurationUtilities
{
	public partial class DBConfigForm : Form
	{
        private readonly IConfigUi _ui;

        #region Parameter Map

		public string ParamFileName { get; set; }

		internal class ParamMap
		{
			public TextBox Box { get; set; }
		}

		internal readonly Dictionary<string, ParamMap> _params;

        public DBConfigForm(IConfigUi ui)
        {
            _ui = ui;
            InitializeComponent();

			string basePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
			_txtParamFile.Text = Path.Combine(basePath, "DeploymentParams.xml");

			_params = new Dictionary<string, ParamMap>();
			_params["DB_SERVER"] = new ParamMap { Box = _txtDBServer };
			_params["SSRS_URL"] = new ParamMap { Box = _txtSsrsUrl };
			_params["INSTALL_ROOT"] = new ParamMap { Box = _txtInstallRoot };
			_params["LOG_FOLDER"] = new ParamMap { Box = _txtLogFolder };
			_params["PARTITIONED"] = new ParamMap { Box = _txtPartitioned };
			_params["PARTITION_START_DATE"] = new ParamMap { Box = _txtPartitionStartDate };
			_params["PARTITION_TYPE"] = new ParamMap { Box = _txtPartitionType };
			_params["PARTITION_YEARS"] = new ParamMap { Box = _txtPartitionYears };

			_params["RECHUB_DB_NAME"] = new ParamMap { Box = _txtDBRecHubDBName };
			_params["RECHUB_SQL_DATA_PATH"] = new ParamMap { Box = _txtRecHubDataPath };
			_params["RECHUB_SQL_LOG_PATH"] = new ParamMap { Box = _txtRecHubLogPath };
			_params["RECHUB_SQL_ITEM_PATH"] = new ParamMap { Box = _txtRecHubItemPath };
			_params["RECHUB_SQL_NOTIFICATION_PATH"] = new ParamMap { Box = _txtRecHubNotificationPath };
			_params["RECHUB_SQL_AUDIT_PATH"] = new ParamMap { Box = _txtRecHubAuditPath };

			_params["SSIS_CONFIG_DB_NAME"] = new ParamMap { Box = _txtSSISConfigDBName };
			_params["SSIS_CONFIG_SQL_DATA_PATH"] = new ParamMap { Box = _txtSSISConfigDataPath };
			_params["SSIS_CONFIG_SQL_LOG_PATH"] = new ParamMap { Box = _txtSSISConfigLogPath };

			_params["SSIS_LOG_DB_NAME"] = new ParamMap { Box = _txtSSISLogDBName };
			_params["SSIS_LOG_SQL_DATA_PATH"] = new ParamMap { Box = _txtSSISLogDataPath };
			_params["SSIS_LOG_SQL_LOG_PATH"] = new ParamMap { Box = _txtSSISLogLogPath };

			_params["RAAM_DB_NAME"] = new ParamMap { Box = _txtRaamDBName };
			_params["RAAM_SQL_DATA_PATH"] = new ParamMap { Box = _txtRaamDataPath };
			_params["RAAM_SQL_LOG_PATH"] = new ParamMap { Box = _txtRaamLogPath };
			_params["WEB_SERVER"] = new ParamMap { Box = _txtWebServer };

			_params["TOKENCACHE_DB_NAME"] = new ParamMap { Box = _txtTokenCacheDBName };
			_params["TOKENCACHE_SQL_DATA_PATH"] = new ParamMap { Box = _txtTokenCacheDataPath };
			_params["TOKENCACHE_SQL_LOG_PATH"] = new ParamMap { Box = _txtTokenCacheLogPath };

			_params["IDENTITYSERVER_DB_NAME"] = new ParamMap { Box = _txtIdentityServerDBName };
			_params["IDENTITYSERVER_SQL_DATA_PATH"] = new ParamMap { Box = _txtIdentityServerDataPath };
			_params["IDENTITYSERVER_SQL_LOG_PATH"] = new ParamMap { Box = _txtIdentityServerLogPath };

			_params["FRAMEWORK_DB_NAME"] = new ParamMap { Box = _txtFrameworkDBName };
			_params["FRAMEWORK_SQL_DATA_PATH"] = new ParamMap { Box = _txtFrameworkDataPath };
			_params["FRAMEWORK_SQL_LOG_PATH"] = new ParamMap { Box = _txtFrameworkLogPath };

			//_params["INTEGRAPAY_DB_NAME"] = new ParamMap { Box = _txtIntegraPayDBName };

			_params["SSRS_REPORT_FOLDER"] = new ParamMap { Box = _txtRecHubSSRSFolder };
			_params["RAAM_SSRS_REPORT_FOLDER"] = new ParamMap { Box = _txtRaamSsrsReportFolder };
		}

		internal class ComboItem
		{
			public string Text { get; set; }
			public string Value { get; set; }
		}

        public void DBConfigForm_Load(object sender, EventArgs e)
		{
			// Populate combo boxes
			_cboPartitionType.Items.Clear();
			_cboPartitionType.Items.Add(new ComboItem { Text = "Monthly (Recommended)", Value = "2" });
			_cboPartitionType.Items.Add(new ComboItem { Text = "Weekly", Value = "1" });
			_cboPartitionType.SelectedIndex = 0;

			// Populate default paths
			string basePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
			_txtInstallRoot.Text = basePath;
			_txtLogFolder.Text = Path.Combine(basePath, "InstallLogs");

			// Previous Parameters
			try
			{
				if (File.Exists(_txtParamFile.Text))
				{
					DeserializeParams();
				}
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}

			// Ensure hidden text boxes are synchronized
			_chkPartitionsEnabled_CheckedChanged(null, null);
			_numPartitionYears_ValueChanged(null, null);
			_cboPartitionType_SelectedIndexChanged(null, null);
			_dtPartitionStart_ValueChanged(null, null);
		}

		public void ValidateParamFile()
		{
			ParamFileName = null;
			try
			{
				if (File.Exists(_txtParamFile.Text))
				{
					// TODO: Validate parameters in some way...? DeserializeParams();
					ParamFileName = _txtParamFile.Text;
				}
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}
		}

		#endregion

		#region Button-Click Events

        public void _btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				// TODO: Validate all of the parameters - length, allowed characters, format, error on "localhost", etc.
				ParamFileName = _txtParamFile.Text;
				SerializeParams();
				DialogResult = System.Windows.Forms.DialogResult.OK;
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}
		}

        public void _btnCancel_Click(object sender, EventArgs e)
		{
			ParamFileName = null;
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void _btnLoadParams_Click(object sender, EventArgs e)
		{
			try
			{
				DeserializeParams();
			}
			catch (Exception ex)
			{
                _ui.ShowMessage(ex.ToString());
			}
		}

		#endregion

		#region Saving / Loading

		private void SerializeParams()
		{
			using (XmlWriter xWriter = XmlWriter.Create(ParamFileName, new XmlWriterSettings() { OmitXmlDeclaration = true, Indent = true }))
			{
				xWriter.WriteStartElement("Params");
				foreach (var entry in _params)
				{
					xWriter.WriteElementString(entry.Key, entry.Value.Box.Text);
				}
				xWriter.WriteEndElement();
			}
		}

		private void DeserializeParams()
		{
			XDocument xDoc = XDocument.Load(_txtParamFile.Text);
			foreach (var entry in _params)
			{
				var elem = xDoc.Descendants(entry.Key).FirstOrDefault();
				if (elem != null)
					entry.Value.Box.Text =  elem.Value;
			}
		}

		#endregion

		#region Control-to-Textbox Duplication

		private void _chkPartitionsEnabled_CheckedChanged(object sender, EventArgs e)
		{
			string value = _chkPartitionsEnabled.Checked ? "1" : "0";
			if (_txtPartitioned.Text != value)
				_txtPartitioned.Text = value;
		}

		private void _txtPartitioned_TextChanged(object sender, EventArgs e)
		{
			bool value = _txtPartitioned.Text == "1";
			if (_chkPartitionsEnabled.Checked != value)
				_chkPartitionsEnabled.Checked = value;
		}

		private void _txtPartitionStartDate_TextChanged(object sender, EventArgs e)
		{
			string value = _dtPartitionStart.Value.ToString("MM/dd/yyyy");
			if (value != _txtPartitionStartDate.Text)
			{
				DateTime tmp;
				if (DateTime.TryParse(_txtPartitionStartDate.Text, out tmp))
					_dtPartitionStart.Value = tmp;
				else
					_dtPartitionStart_ValueChanged(sender, e);
			}
		}

		private void _dtPartitionStart_ValueChanged(object sender, EventArgs e)
		{
			string value = _dtPartitionStart.Value.ToString("MM/dd/yyyy");
			if (_txtPartitionStartDate.Text != value)
				_txtPartitionStartDate.Text = value;
		}

		private void _txtPartitionType_TextChanged(object sender, EventArgs e)
		{
			string value = (_cboPartitionType.SelectedItem as ComboItem).Value;
			if (value != _txtPartitionType.Text)
			{
				for (int i = 0; i < _cboPartitionType.Items.Count; i++)
				{
					if ((_cboPartitionType.Items[i] as ComboItem).Value == _txtPartitionType.Text)
					{
						_cboPartitionType.SelectedIndex = i;
						break;
					}
				}
				_cboPartitionType_SelectedIndexChanged(sender, e);
			}
		}

		private void _cboPartitionType_SelectedIndexChanged(object sender, EventArgs e)
		{
			string value = (_cboPartitionType.SelectedItem as ComboItem).Value;
			if (value != _txtPartitionType.Text)
			{
				_txtPartitionType.Text = value;
			}
		}

		private void _txtPartitionYears_TextChanged(object sender, EventArgs e)
		{
			string value = _numPartitionYears.Value.ToString();
			if (value != _txtPartitionYears.Text)
			{
				int years;
				if (int.TryParse(_txtPartitionYears.Text, out years))
				{
					_numPartitionYears.Value = years;
				}
				else
				{
					_numPartitionYears_ValueChanged(sender, e);
				}
			}
		}

		private void _numPartitionYears_ValueChanged(object sender, EventArgs e)
		{
			string value = _numPartitionYears.Value.ToString();
			if (value != _txtPartitionYears.Text)
			{
				_txtPartitionYears.Text = value;
			}
		}

		#endregion

		private void _txtRaamDBName_TextChanged(object sender, EventArgs e)
		{
			if (_txtRaamRaamDBName.Text != _txtRaamDBName.Text)
				_txtRaamRaamDBName.Text = _txtRaamDBName.Text;
		}

		private void _txtRaamRaamDBName_TextChanged(object sender, EventArgs e)
		{
			if (_txtRaamRaamDBName.Text != _txtRaamDBName.Text)
				_txtRaamDBName.Text = _txtRaamRaamDBName.Text;
		}
	}
}
