﻿namespace Wfs.R360.DeploymentConfigurationUtilities
{
	partial class DBConfigForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBConfigForm));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this._txtSsrsUrl = new System.Windows.Forms.TextBox();
			this.label24 = new System.Windows.Forms.Label();
			this._txtLogFolder = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this._txtInstallRoot = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this._txtDBServer = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this._txtParamFile = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this._btnLoadParams = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this._txtPartitionYears = new System.Windows.Forms.TextBox();
			this._txtPartitionType = new System.Windows.Forms.TextBox();
			this._txtPartitionStartDate = new System.Windows.Forms.TextBox();
			this._txtPartitioned = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this._numPartitionYears = new System.Windows.Forms.NumericUpDown();
			this.label7 = new System.Windows.Forms.Label();
			this._dtPartitionStart = new System.Windows.Forms.DateTimePicker();
			this._cboPartitionType = new System.Windows.Forms.ComboBox();
			this._chkPartitionsEnabled = new System.Windows.Forms.CheckBox();
			this._toolTips = new System.Windows.Forms.ToolTip(this.components);
			this.label3 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.label32 = new System.Windows.Forms.Label();
			this._txtRecHubAuditPath = new System.Windows.Forms.TextBox();
			this._txtRecHubNotificationPath = new System.Windows.Forms.TextBox();
			this._txtRecHubItemPath = new System.Windows.Forms.TextBox();
			this._txtRecHubLogPath = new System.Windows.Forms.TextBox();
			this._txtRecHubDataPath = new System.Windows.Forms.TextBox();
			this._txtDBRecHubDBName = new System.Windows.Forms.TextBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this._btnSave = new System.Windows.Forms.Button();
			this._btnCancel = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this._tabControl = new System.Windows.Forms.TabControl();
			this._tabRecHub = new System.Windows.Forms.TabPage();
			this._gprRecHub = new System.Windows.Forms.GroupBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this._txtRecHubSSRSFolder = new System.Windows.Forms.TextBox();
			this._txtRaamDBName = new System.Windows.Forms.TextBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this._txtSSISLogLogPath = new System.Windows.Forms.TextBox();
			this._txtSSISLogDataPath = new System.Windows.Forms.TextBox();
			this._txtSSISLogDBName = new System.Windows.Forms.TextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this._txtSSISConfigLogPath = new System.Windows.Forms.TextBox();
			this._txtSSISConfigDataPath = new System.Windows.Forms.TextBox();
			this._txtSSISConfigDBName = new System.Windows.Forms.TextBox();
			this._tabRaam = new System.Windows.Forms.TabPage();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this._txtIdentityServerLogPath = new System.Windows.Forms.TextBox();
			this._txtIdentityServerDataPath = new System.Windows.Forms.TextBox();
			this._txtIdentityServerDBName = new System.Windows.Forms.TextBox();
			this._txtRaamSsrsReportFolder = new System.Windows.Forms.TextBox();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this._txtTokenCacheLogPath = new System.Windows.Forms.TextBox();
			this._txtTokenCacheDataPath = new System.Windows.Forms.TextBox();
			this._txtTokenCacheDBName = new System.Windows.Forms.TextBox();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this._txtWebServer = new System.Windows.Forms.TextBox();
			this._txtRaamLogPath = new System.Windows.Forms.TextBox();
			this._txtRaamDataPath = new System.Windows.Forms.TextBox();
			this._txtRaamRaamDBName = new System.Windows.Forms.TextBox();
			this._tabFramework = new System.Windows.Forms.TabPage();
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this._txtFrameworkLogPath = new System.Windows.Forms.TextBox();
			this.label33 = new System.Windows.Forms.Label();
			this._txtFrameworkDataPath = new System.Windows.Forms.TextBox();
			this.label34 = new System.Windows.Forms.Label();
			this._txtFrameworkDBName = new System.Windows.Forms.TextBox();
			this.label35 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this._numPartitionYears)).BeginInit();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this._tabControl.SuspendLayout();
			this._tabRecHub.SuspendLayout();
			this._gprRecHub.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this._tabRaam.SuspendLayout();
			this.groupBox8.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this._tabFramework.SuspendLayout();
			this.groupBox9.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this._txtSsrsUrl);
			this.groupBox1.Controls.Add(this.label24);
			this.groupBox1.Controls.Add(this._txtLogFolder);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this._txtInstallRoot);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this._txtDBServer);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this._txtParamFile);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this._btnLoadParams);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(1002, 101);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Deployment Management";
			// 
			// _txtSsrsUrl
			// 
			this._txtSsrsUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtSsrsUrl.Location = new System.Drawing.Point(472, 45);
			this._txtSsrsUrl.Name = "_txtSsrsUrl";
			this._txtSsrsUrl.Size = new System.Drawing.Size(519, 20);
			this._txtSsrsUrl.TabIndex = 10;
			this._txtSsrsUrl.Text = "https://localhost/ReportServer";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(353, 48);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(104, 13);
			this.label24.TabIndex = 9;
			this.label24.Text = "SSRS Server (URL):";
			this._toolTips.SetToolTip(this.label24, "The URL to use to call the SSRS web services for installation.  Could be \"https:/" +
        "/localhost/ReportServer\" if installing locallly, or put the servername / IP addr" +
        "ess in if installing remotely.");
			// 
			// _txtLogFolder
			// 
			this._txtLogFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtLogFolder.Location = new System.Drawing.Point(472, 71);
			this._txtLogFolder.Name = "_txtLogFolder";
			this._txtLogFolder.Size = new System.Drawing.Size(520, 20);
			this._txtLogFolder.TabIndex = 8;
			this._txtLogFolder.Text = "D:\\WFSStaging\\DB\\InstallLogs\\";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(354, 74);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(57, 13);
			this.label6.TabIndex = 7;
			this.label6.Text = "Log folder:";
			this._toolTips.SetToolTip(this.label6, "The folder where the installation logs / results should get written.\r\n");
			// 
			// _txtInstallRoot
			// 
			this._txtInstallRoot.Location = new System.Drawing.Point(114, 71);
			this._txtInstallRoot.Name = "_txtInstallRoot";
			this._txtInstallRoot.Size = new System.Drawing.Size(233, 20);
			this._txtInstallRoot.TabIndex = 6;
			this._txtInstallRoot.Text = "D:\\WFSStaging\\DB\\";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(9, 74);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(69, 13);
			this.label5.TabIndex = 5;
			this.label5.Text = "Install Folder:";
			this._toolTips.SetToolTip(this.label5, "The root folder that all of the database installation scripts are in.  (e.g. D:\\W" +
        "FSStaging\\DB\\");
			// 
			// _txtDBServer
			// 
			this._txtDBServer.Location = new System.Drawing.Point(114, 45);
			this._txtDBServer.Name = "_txtDBServer";
			this._txtDBServer.Size = new System.Drawing.Size(233, 20);
			this._txtDBServer.TabIndex = 4;
			this._txtDBServer.Text = "localhost";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(9, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(90, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Database Server:";
			this._toolTips.SetToolTip(this.label2, "The server to install the database changes on.  Could be \"localhost\" or \".\" if in" +
        "stalling locallly, or a servername / IP address if installing remotely.");
			// 
			// _txtParamFile
			// 
			this._txtParamFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtParamFile.Location = new System.Drawing.Point(114, 19);
			this._txtParamFile.Name = "_txtParamFile";
			this._txtParamFile.Size = new System.Drawing.Size(801, 20);
			this._txtParamFile.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(9, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(77, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Parameter File:";
			this._toolTips.SetToolTip(this.label1, "If you choose \"Save and Continue\", the values will be saved to this file, and aut" +
        "omatically reloaded the next time you deploy changes.");
			// 
			// _btnLoadParams
			// 
			this._btnLoadParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnLoadParams.Location = new System.Drawing.Point(921, 17);
			this._btnLoadParams.Name = "_btnLoadParams";
			this._btnLoadParams.Size = new System.Drawing.Size(75, 23);
			this._btnLoadParams.TabIndex = 2;
			this._btnLoadParams.Text = "Load";
			this._btnLoadParams.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this._txtPartitionYears);
			this.groupBox2.Controls.Add(this._txtPartitionType);
			this.groupBox2.Controls.Add(this._txtPartitionStartDate);
			this.groupBox2.Controls.Add(this._txtPartitioned);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this._numPartitionYears);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this._dtPartitionStart);
			this.groupBox2.Controls.Add(this._cboPartitionType);
			this.groupBox2.Controls.Add(this._chkPartitionsEnabled);
			this.groupBox2.Location = new System.Drawing.Point(6, 41);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(276, 72);
			this.groupBox2.TabIndex = 3;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Partitioning";
			// 
			// _txtPartitionYears
			// 
			this._txtPartitionYears.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtPartitionYears.Location = new System.Drawing.Point(105, 17);
			this._txtPartitionYears.Name = "_txtPartitionYears";
			this._txtPartitionYears.Size = new System.Drawing.Size(10, 20);
			this._txtPartitionYears.TabIndex = 16;
			this._txtPartitionYears.Visible = false;
			this._txtPartitionYears.TextChanged += new System.EventHandler(this._txtPartitionYears_TextChanged);
			// 
			// _txtPartitionType
			// 
			this._txtPartitionType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtPartitionType.Location = new System.Drawing.Point(96, 17);
			this._txtPartitionType.Name = "_txtPartitionType";
			this._txtPartitionType.Size = new System.Drawing.Size(10, 20);
			this._txtPartitionType.TabIndex = 15;
			this._txtPartitionType.Visible = false;
			this._txtPartitionType.TextChanged += new System.EventHandler(this._txtPartitionType_TextChanged);
			// 
			// _txtPartitionStartDate
			// 
			this._txtPartitionStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtPartitionStartDate.Location = new System.Drawing.Point(86, 17);
			this._txtPartitionStartDate.Name = "_txtPartitionStartDate";
			this._txtPartitionStartDate.Size = new System.Drawing.Size(10, 20);
			this._txtPartitionStartDate.TabIndex = 14;
			this._txtPartitionStartDate.Visible = false;
			this._txtPartitionStartDate.TextChanged += new System.EventHandler(this._txtPartitionStartDate_TextChanged);
			// 
			// _txtPartitioned
			// 
			this._txtPartitioned.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtPartitioned.Location = new System.Drawing.Point(76, 17);
			this._txtPartitioned.Name = "_txtPartitioned";
			this._txtPartitioned.Size = new System.Drawing.Size(10, 20);
			this._txtPartitioned.TabIndex = 13;
			this._txtPartitioned.Visible = false;
			this._txtPartitioned.TextChanged += new System.EventHandler(this._txtPartitioned_TextChanged);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(223, 46);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(32, 13);
			this.label8.TabIndex = 10;
			this.label8.Text = "years";
			this._toolTips.SetToolTip(this.label8, "The number of years that data can be stored.");
			// 
			// _numPartitionYears
			// 
			this._numPartitionYears.Location = new System.Drawing.Point(171, 44);
			this._numPartitionYears.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
			this._numPartitionYears.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this._numPartitionYears.Name = "_numPartitionYears";
			this._numPartitionYears.Size = new System.Drawing.Size(46, 20);
			this._numPartitionYears.TabIndex = 9;
			this._numPartitionYears.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this._numPartitionYears.ValueChanged += new System.EventHandler(this._numPartitionYears_ValueChanged);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(133, 21);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(32, 13);
			this.label7.TabIndex = 8;
			this.label7.Text = "Start:";
			this._toolTips.SetToolTip(this.label7, "The earliest date data can be imported for.");
			// 
			// _dtPartitionStart
			// 
			this._dtPartitionStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
			this._dtPartitionStart.Location = new System.Drawing.Point(171, 18);
			this._dtPartitionStart.Name = "_dtPartitionStart";
			this._dtPartitionStart.Size = new System.Drawing.Size(99, 20);
			this._dtPartitionStart.TabIndex = 2;
			this._dtPartitionStart.ValueChanged += new System.EventHandler(this._dtPartitionStart_ValueChanged);
			// 
			// _cboPartitionType
			// 
			this._cboPartitionType.DisplayMember = "Text";
			this._cboPartitionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this._cboPartitionType.FormattingEnabled = true;
			this._cboPartitionType.Items.AddRange(new object[] {
            "Monthly (Recommended)",
            "Weekly",
            "None"});
			this._cboPartitionType.Location = new System.Drawing.Point(12, 43);
			this._cboPartitionType.Name = "_cboPartitionType";
			this._cboPartitionType.Size = new System.Drawing.Size(153, 21);
			this._cboPartitionType.TabIndex = 1;
			this._toolTips.SetToolTip(this._cboPartitionType, "Choose the size of the partitions.  Monthly partitions are recommended.");
			this._cboPartitionType.ValueMember = "Value";
			this._cboPartitionType.SelectedIndexChanged += new System.EventHandler(this._cboPartitionType_SelectedIndexChanged);
			// 
			// _chkPartitionsEnabled
			// 
			this._chkPartitionsEnabled.AutoSize = true;
			this._chkPartitionsEnabled.Checked = true;
			this._chkPartitionsEnabled.CheckState = System.Windows.Forms.CheckState.Checked;
			this._chkPartitionsEnabled.Location = new System.Drawing.Point(12, 20);
			this._chkPartitionsEnabled.Name = "_chkPartitionsEnabled";
			this._chkPartitionsEnabled.Size = new System.Drawing.Size(65, 17);
			this._chkPartitionsEnabled.TabIndex = 0;
			this._chkPartitionsEnabled.Text = "Enabled";
			this._toolTips.SetToolTip(this._chkPartitionsEnabled, "It is recommended that Partitioning always be enabled.");
			this._chkPartitionsEnabled.UseVisualStyleBackColor = true;
			this._chkPartitionsEnabled.CheckedChanged += new System.EventHandler(this._chkPartitionsEnabled_CheckedChanged);
			// 
			// _toolTips
			// 
			this._toolTips.AutomaticDelay = 100;
			this._toolTips.AutoPopDelay = 20000;
			this._toolTips.InitialDelay = 100;
			this._toolTips.ReshowDelay = 20;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(291, 18);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(77, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Data File Path:";
			this._toolTips.SetToolTip(this.label3, "The folder where the main database file should be placed.  Just the folder (e.g. " +
        "D:\\SQLData\\Data) -- the file will be automatically named after the database.");
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(291, 42);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(131, 13);
			this.label9.TabIndex = 5;
			this.label9.Text = "Transaction Log File Path:";
			this._toolTips.SetToolTip(this.label9, "The folder where the database transaction log file should be placed.  Just the fo" +
        "lder (e.g. D:\\SQLData\\Log) -- the file will be automatically named after the dat" +
        "abase.");
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(291, 66);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(111, 13);
			this.label10.TabIndex = 7;
			this.label10.Text = "Item Data File Path(s):";
			this._toolTips.SetToolTip(this.label10, "The folder where the transactional data (items, etc.) partitions will be created." +
        "  You can specify multiple folders, semi-colon delimited.\r\n");
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(291, 90);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(144, 13);
			this.label11.TabIndex = 9;
			this.label11.Text = "Notification Data File Path(s):";
			this._toolTips.SetToolTip(this.label11, "The folder where the notification data partitions will be created.  You can speci" +
        "fy multiple folders, semi-colon delimited.");
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(291, 114);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(115, 13);
			this.label12.TabIndex = 11;
			this.label12.Text = "Audit Data File Path(s):";
			this._toolTips.SetToolTip(this.label12, "The folder where the audit data partitions will be created.  You can specify mult" +
        "iple folders, semi-colon delimited.");
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(291, 46);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(131, 13);
			this.label16.TabIndex = 5;
			this.label16.Text = "Transaction Log File Path:";
			this._toolTips.SetToolTip(this.label16, "The folder where the database transaction log file should be placed.  Just the fo" +
        "lder (e.g. D:\\SQLData\\Log) -- the file will be automatically named after the dat" +
        "abase.");
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(291, 22);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(77, 13);
			this.label17.TabIndex = 3;
			this.label17.Text = "Data File Path:";
			this._toolTips.SetToolTip(this.label17, "The folder where the main database file should be placed.  Just the folder (e.g. " +
        "D:\\SQLData\\Data) -- the file will be automatically named after the database.");
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(12, 22);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(87, 13);
			this.label18.TabIndex = 0;
			this.label18.Text = "Database Name:";
			this._toolTips.SetToolTip(this.label18, "The name of the database (e.g. WFS_SSIS_Configuration).");
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(291, 46);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(131, 13);
			this.label13.TabIndex = 5;
			this.label13.Text = "Transaction Log File Path:";
			this._toolTips.SetToolTip(this.label13, "The folder where the database transaction log file should be placed.  Just the fo" +
        "lder (e.g. D:\\SQLData\\Log) -- the file will be automatically named after the dat" +
        "abase.\r\n");
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(291, 22);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(77, 13);
			this.label14.TabIndex = 3;
			this.label14.Text = "Data File Path:";
			this._toolTips.SetToolTip(this.label14, "The folder where the main database file should be placed.  Just the folder (e.g. " +
        "D:\\SQLData\\Data) -- the file will be automatically named after the database.");
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(9, 22);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(87, 13);
			this.label15.TabIndex = 0;
			this.label15.Text = "Database Name:";
			this._toolTips.SetToolTip(this.label15, "The name of the database (e.g. WFS_SSIS_Logging).");
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(9, 22);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(121, 13);
			this.label21.TabIndex = 0;
			this.label21.Text = "RAAM Database Name:";
			this._toolTips.SetToolTip(this.label21, "The name of the RAAM database (e.g. RAAM).");
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(12, 18);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(87, 13);
			this.label19.TabIndex = 2;
			this.label19.Text = "Database Name:";
			this._toolTips.SetToolTip(this.label19, "The name of the database (e.g. RecHub or WFSDB_R360).  This name must be set on t" +
        "he APP / FILE server installs to match.");
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(291, 46);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(131, 13);
			this.label4.TabIndex = 5;
			this.label4.Text = "Transaction Log File Path:";
			this._toolTips.SetToolTip(this.label4, "The folder where the database transaction log file should be placed.  Just the fo" +
        "lder (e.g. D:\\SQLData\\Log) -- the file will be automatically named after the dat" +
        "abase.");
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(291, 22);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(77, 13);
			this.label20.TabIndex = 3;
			this.label20.Text = "Data File Path:";
			this._toolTips.SetToolTip(this.label20, "The folder where the main database file should be placed.  Just the folder (e.g. " +
        "D:\\SQLData\\Data) -- the file will be automatically named after the database.");
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(12, 22);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(87, 13);
			this.label22.TabIndex = 0;
			this.label22.Text = "Database Name:";
			this._toolTips.SetToolTip(this.label22, "The name of the database (e.g. RAAM).");
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(12, 70);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(98, 13);
			this.label23.TabIndex = 7;
			this.label23.Text = "Web Server (URL):";
			this._toolTips.SetToolTip(this.label23, "The name of the web server, used in the web server URL (it must match the SSL cer" +
        "tificate).   e.g. \"R360.my-bank.com\"");
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(291, 46);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(131, 13);
			this.label25.TabIndex = 5;
			this.label25.Text = "Transaction Log File Path:";
			this._toolTips.SetToolTip(this.label25, "The folder where the database transaction log file should be placed.  Just the fo" +
        "lder (e.g. D:\\SQLData\\Log) -- the file will be automatically named after the dat" +
        "abase.");
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(291, 22);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(77, 13);
			this.label26.TabIndex = 3;
			this.label26.Text = "Data File Path:";
			this._toolTips.SetToolTip(this.label26, "The folder where the main database file should be placed.  Just the folder (e.g. " +
        "D:\\SQLData\\Data) -- the file will be automatically named after the database.");
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(12, 22);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(87, 13);
			this.label27.TabIndex = 0;
			this.label27.Text = "Database Name:";
			this._toolTips.SetToolTip(this.label27, "The name of the database (e.g. TokenCache).");
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Location = new System.Drawing.Point(291, 22);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(106, 13);
			this.label28.TabIndex = 2;
			this.label28.Text = "SSRS Report Folder:";
			this._toolTips.SetToolTip(this.label28, "The name of the folder in SSRS that reports will be put in (must be set to match " +
        "on the APP tier RecHub.ini) e.g. \"R360Reports\"");
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Location = new System.Drawing.Point(18, 270);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(106, 13);
			this.label29.TabIndex = 6;
			this.label29.Text = "SSRS Report Folder:";
			this._toolTips.SetToolTip(this.label29, "The name of the folder in SSRS that reports will be put in (must be set to match " +
        "on the APP tier RecHub.ini) e.g. \"R360Reports\"");
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Location = new System.Drawing.Point(291, 46);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(131, 13);
			this.label30.TabIndex = 5;
			this.label30.Text = "Transaction Log File Path:";
			this._toolTips.SetToolTip(this.label30, "The folder where the database transaction log file should be placed.  Just the fo" +
        "lder (e.g. D:\\SQLData\\Log) -- the file will be automatically named after the dat" +
        "abase.");
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Location = new System.Drawing.Point(291, 22);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(77, 13);
			this.label31.TabIndex = 3;
			this.label31.Text = "Data File Path:";
			this._toolTips.SetToolTip(this.label31, "The folder where the main database file should be placed.  Just the folder (e.g. " +
        "D:\\SQLData\\Data) -- the file will be automatically named after the database.");
			// 
			// label32
			// 
			this.label32.AutoSize = true;
			this.label32.Location = new System.Drawing.Point(12, 22);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(87, 13);
			this.label32.TabIndex = 0;
			this.label32.Text = "Database Name:";
			this._toolTips.SetToolTip(this.label32, "The name of the database (e.g. IdentityServerConfiguration).");
			// 
			// _txtRecHubAuditPath
			// 
			this._txtRecHubAuditPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRecHubAuditPath.Location = new System.Drawing.Point(438, 111);
			this._txtRecHubAuditPath.Name = "_txtRecHubAuditPath";
			this._txtRecHubAuditPath.Size = new System.Drawing.Size(537, 20);
			this._txtRecHubAuditPath.TabIndex = 12;
			this._txtRecHubAuditPath.Text = "D:\\SQLData\\Audit";
			// 
			// _txtRecHubNotificationPath
			// 
			this._txtRecHubNotificationPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRecHubNotificationPath.Location = new System.Drawing.Point(438, 87);
			this._txtRecHubNotificationPath.Name = "_txtRecHubNotificationPath";
			this._txtRecHubNotificationPath.Size = new System.Drawing.Size(537, 20);
			this._txtRecHubNotificationPath.TabIndex = 10;
			this._txtRecHubNotificationPath.Text = "D:\\SQLData\\Notification";
			// 
			// _txtRecHubItemPath
			// 
			this._txtRecHubItemPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRecHubItemPath.Location = new System.Drawing.Point(438, 63);
			this._txtRecHubItemPath.Name = "_txtRecHubItemPath";
			this._txtRecHubItemPath.Size = new System.Drawing.Size(537, 20);
			this._txtRecHubItemPath.TabIndex = 8;
			this._txtRecHubItemPath.Text = "D:\\SQLData\\Items";
			// 
			// _txtRecHubLogPath
			// 
			this._txtRecHubLogPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRecHubLogPath.Location = new System.Drawing.Point(438, 39);
			this._txtRecHubLogPath.Name = "_txtRecHubLogPath";
			this._txtRecHubLogPath.Size = new System.Drawing.Size(537, 20);
			this._txtRecHubLogPath.TabIndex = 6;
			this._txtRecHubLogPath.Text = "D:\\SQLData\\Log";
			// 
			// _txtRecHubDataPath
			// 
			this._txtRecHubDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRecHubDataPath.Location = new System.Drawing.Point(438, 15);
			this._txtRecHubDataPath.Name = "_txtRecHubDataPath";
			this._txtRecHubDataPath.Size = new System.Drawing.Size(537, 20);
			this._txtRecHubDataPath.TabIndex = 4;
			this._txtRecHubDataPath.Text = "D:\\SQLData\\Data";
			// 
			// _txtDBRecHubDBName
			// 
			this._txtDBRecHubDBName.Location = new System.Drawing.Point(111, 15);
			this._txtDBRecHubDBName.Name = "_txtDBRecHubDBName";
			this._txtDBRecHubDBName.Size = new System.Drawing.Size(171, 20);
			this._txtDBRecHubDBName.TabIndex = 1;
			this._txtDBRecHubDBName.Text = "WFSDB_R360";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(3, 497);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1002, 39);
			this.panel2.TabIndex = 10;
			// 
			// panel1
			// 
			this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.panel1.Controls.Add(this._btnSave);
			this.panel1.Controls.Add(this._btnCancel);
			this.panel1.Location = new System.Drawing.Point(356, 5);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(300, 31);
			this.panel1.TabIndex = 8;
			// 
			// _btnSave
			// 
			this._btnSave.Location = new System.Drawing.Point(3, 3);
			this._btnSave.Name = "_btnSave";
			this._btnSave.Size = new System.Drawing.Size(133, 23);
			this._btnSave.TabIndex = 5;
			this._btnSave.Text = "Save and Continue";
			this._btnSave.UseVisualStyleBackColor = true;
			this._btnSave.Click += new System.EventHandler(this._btnSave_Click);
			// 
			// _btnCancel
			// 
			this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._btnCancel.Location = new System.Drawing.Point(160, 3);
			this._btnCancel.Name = "_btnCancel";
			this._btnCancel.Size = new System.Drawing.Size(133, 23);
			this._btnCancel.TabIndex = 6;
			this._btnCancel.Text = "Cancel Installation";
			this._btnCancel.UseVisualStyleBackColor = true;
			this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
			// 
			// panel3
			// 
			this.panel3.AutoScroll = true;
			this.panel3.Controls.Add(this._tabControl);
			this.panel3.Controls.Add(this.groupBox1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(3, 3);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(1002, 494);
			this.panel3.TabIndex = 11;
			// 
			// _tabControl
			// 
			this._tabControl.Controls.Add(this._tabRecHub);
			this._tabControl.Controls.Add(this._tabRaam);
			this._tabControl.Controls.Add(this._tabFramework);
			this._tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this._tabControl.Location = new System.Drawing.Point(0, 101);
			this._tabControl.Name = "_tabControl";
			this._tabControl.SelectedIndex = 0;
			this._tabControl.Size = new System.Drawing.Size(1002, 393);
			this._tabControl.TabIndex = 6;
			// 
			// _tabRecHub
			// 
			this._tabRecHub.AutoScroll = true;
			this._tabRecHub.BackColor = System.Drawing.SystemColors.Control;
			this._tabRecHub.Controls.Add(this._gprRecHub);
			this._tabRecHub.Controls.Add(this.groupBox5);
			this._tabRecHub.Controls.Add(this.groupBox4);
			this._tabRecHub.Controls.Add(this.groupBox3);
			this._tabRecHub.Location = new System.Drawing.Point(4, 22);
			this._tabRecHub.Name = "_tabRecHub";
			this._tabRecHub.Padding = new System.Windows.Forms.Padding(3);
			this._tabRecHub.Size = new System.Drawing.Size(994, 367);
			this._tabRecHub.TabIndex = 0;
			this._tabRecHub.Text = "RecHub";
			// 
			// _gprRecHub
			// 
			this._gprRecHub.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._gprRecHub.Controls.Add(this._txtRecHubAuditPath);
			this._gprRecHub.Controls.Add(this.groupBox2);
			this._gprRecHub.Controls.Add(this.label12);
			this._gprRecHub.Controls.Add(this._txtDBRecHubDBName);
			this._gprRecHub.Controls.Add(this._txtRecHubNotificationPath);
			this._gprRecHub.Controls.Add(this.label19);
			this._gprRecHub.Controls.Add(this._txtRecHubDataPath);
			this._gprRecHub.Controls.Add(this.label11);
			this._gprRecHub.Controls.Add(this.label9);
			this._gprRecHub.Controls.Add(this._txtRecHubLogPath);
			this._gprRecHub.Controls.Add(this._txtRecHubItemPath);
			this._gprRecHub.Controls.Add(this.label10);
			this._gprRecHub.Controls.Add(this.label3);
			this._gprRecHub.Location = new System.Drawing.Point(6, 6);
			this._gprRecHub.Name = "_gprRecHub";
			this._gprRecHub.Size = new System.Drawing.Size(981, 139);
			this._gprRecHub.TabIndex = 2;
			this._gprRecHub.TabStop = false;
			this._gprRecHub.Text = "R360 Database";
			// 
			// groupBox5
			// 
			this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox5.Controls.Add(this._txtRecHubSSRSFolder);
			this.groupBox5.Controls.Add(this.label28);
			this.groupBox5.Controls.Add(this._txtRaamDBName);
			this.groupBox5.Controls.Add(this.label21);
			this.groupBox5.Location = new System.Drawing.Point(6, 303);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(981, 49);
			this.groupBox5.TabIndex = 5;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Other";
			// 
			// _txtRecHubSSRSFolder
			// 
			this._txtRecHubSSRSFolder.Location = new System.Drawing.Point(424, 19);
			this._txtRecHubSSRSFolder.Name = "_txtRecHubSSRSFolder";
			this._txtRecHubSSRSFolder.Size = new System.Drawing.Size(140, 20);
			this._txtRecHubSSRSFolder.TabIndex = 3;
			this._txtRecHubSSRSFolder.Text = "R360Reports";
			// 
			// _txtRaamDBName
			// 
			this._txtRaamDBName.Location = new System.Drawing.Point(142, 19);
			this._txtRaamDBName.Name = "_txtRaamDBName";
			this._txtRaamDBName.Size = new System.Drawing.Size(140, 20);
			this._txtRaamDBName.TabIndex = 1;
			this._txtRaamDBName.Text = "RAAM";
			this._txtRaamDBName.TextChanged += new System.EventHandler(this._txtRaamDBName_TextChanged);
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this._txtSSISLogLogPath);
			this.groupBox4.Controls.Add(this.label13);
			this.groupBox4.Controls.Add(this._txtSSISLogDataPath);
			this.groupBox4.Controls.Add(this.label14);
			this.groupBox4.Controls.Add(this._txtSSISLogDBName);
			this.groupBox4.Controls.Add(this.label15);
			this.groupBox4.Location = new System.Drawing.Point(6, 228);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(981, 69);
			this.groupBox4.TabIndex = 4;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "R360 SSIS Logging Database";
			// 
			// _txtSSISLogLogPath
			// 
			this._txtSSISLogLogPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtSSISLogLogPath.Location = new System.Drawing.Point(438, 43);
			this._txtSSISLogLogPath.Name = "_txtSSISLogLogPath";
			this._txtSSISLogLogPath.Size = new System.Drawing.Size(535, 20);
			this._txtSSISLogLogPath.TabIndex = 6;
			this._txtSSISLogLogPath.Text = "D:\\SQLData\\Log";
			// 
			// _txtSSISLogDataPath
			// 
			this._txtSSISLogDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtSSISLogDataPath.Location = new System.Drawing.Point(438, 19);
			this._txtSSISLogDataPath.Name = "_txtSSISLogDataPath";
			this._txtSSISLogDataPath.Size = new System.Drawing.Size(535, 20);
			this._txtSSISLogDataPath.TabIndex = 4;
			this._txtSSISLogDataPath.Text = "D:\\SQLData\\Data";
			// 
			// _txtSSISLogDBName
			// 
			this._txtSSISLogDBName.Location = new System.Drawing.Point(111, 19);
			this._txtSSISLogDBName.Name = "_txtSSISLogDBName";
			this._txtSSISLogDBName.Size = new System.Drawing.Size(171, 20);
			this._txtSSISLogDBName.TabIndex = 1;
            this._txtSSISLogDBName.Text = "WFS_SSIS_Logging";
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this._txtSSISConfigLogPath);
			this.groupBox3.Controls.Add(this.label16);
			this.groupBox3.Controls.Add(this._txtSSISConfigDataPath);
			this.groupBox3.Controls.Add(this.label17);
			this.groupBox3.Controls.Add(this._txtSSISConfigDBName);
			this.groupBox3.Controls.Add(this.label18);
			this.groupBox3.Location = new System.Drawing.Point(6, 151);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(981, 71);
			this.groupBox3.TabIndex = 3;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "R360 SSIS Configuration Database";
			// 
			// _txtSSISConfigLogPath
			// 
			this._txtSSISConfigLogPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtSSISConfigLogPath.Location = new System.Drawing.Point(438, 43);
			this._txtSSISConfigLogPath.Name = "_txtSSISConfigLogPath";
			this._txtSSISConfigLogPath.Size = new System.Drawing.Size(535, 20);
			this._txtSSISConfigLogPath.TabIndex = 6;
			this._txtSSISConfigLogPath.Text = "D:\\SQLData\\Log";
			// 
			// _txtSSISConfigDataPath
			// 
			this._txtSSISConfigDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtSSISConfigDataPath.Location = new System.Drawing.Point(438, 19);
			this._txtSSISConfigDataPath.Name = "_txtSSISConfigDataPath";
			this._txtSSISConfigDataPath.Size = new System.Drawing.Size(535, 20);
			this._txtSSISConfigDataPath.TabIndex = 4;
			this._txtSSISConfigDataPath.Text = "D:\\SQLData\\Data";
			// 
			// _txtSSISConfigDBName
			// 
			this._txtSSISConfigDBName.Location = new System.Drawing.Point(111, 19);
			this._txtSSISConfigDBName.Name = "_txtSSISConfigDBName";
			this._txtSSISConfigDBName.Size = new System.Drawing.Size(171, 20);
			this._txtSSISConfigDBName.TabIndex = 1;
            this._txtSSISConfigDBName.Text = "WFS_SSIS_Configuration";
			// 
			// _tabRaam
			// 
			this._tabRaam.BackColor = System.Drawing.SystemColors.Control;
			this._tabRaam.Controls.Add(this.groupBox8);
			this._tabRaam.Controls.Add(this._txtRaamSsrsReportFolder);
			this._tabRaam.Controls.Add(this.label29);
			this._tabRaam.Controls.Add(this.groupBox7);
			this._tabRaam.Controls.Add(this.groupBox6);
			this._tabRaam.Location = new System.Drawing.Point(4, 22);
			this._tabRaam.Name = "_tabRaam";
			this._tabRaam.Padding = new System.Windows.Forms.Padding(3);
			this._tabRaam.Size = new System.Drawing.Size(994, 367);
			this._tabRaam.TabIndex = 1;
			this._tabRaam.Text = "RAAM";
			// 
			// groupBox8
			// 
			this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox8.Controls.Add(this._txtIdentityServerLogPath);
			this.groupBox8.Controls.Add(this.label30);
			this.groupBox8.Controls.Add(this._txtIdentityServerDataPath);
			this.groupBox8.Controls.Add(this.label31);
			this.groupBox8.Controls.Add(this._txtIdentityServerDBName);
			this.groupBox8.Controls.Add(this.label32);
			this.groupBox8.Location = new System.Drawing.Point(6, 187);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(981, 71);
			this.groupBox8.TabIndex = 8;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "IdentityServer Database";
			// 
			// _txtIdentityServerLogPath
			// 
			this._txtIdentityServerLogPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtIdentityServerLogPath.Location = new System.Drawing.Point(438, 43);
			this._txtIdentityServerLogPath.Name = "_txtIdentityServerLogPath";
			this._txtIdentityServerLogPath.Size = new System.Drawing.Size(535, 20);
			this._txtIdentityServerLogPath.TabIndex = 6;
			this._txtIdentityServerLogPath.Text = "D:\\SQLData\\Log";
			// 
			// _txtIdentityServerDataPath
			// 
			this._txtIdentityServerDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtIdentityServerDataPath.Location = new System.Drawing.Point(438, 19);
			this._txtIdentityServerDataPath.Name = "_txtIdentityServerDataPath";
			this._txtIdentityServerDataPath.Size = new System.Drawing.Size(535, 20);
			this._txtIdentityServerDataPath.TabIndex = 4;
			this._txtIdentityServerDataPath.Text = "D:\\SQLData\\Data";
			// 
			// _txtIdentityServerDBName
			// 
			this._txtIdentityServerDBName.Location = new System.Drawing.Point(111, 19);
			this._txtIdentityServerDBName.Name = "_txtIdentityServerDBName";
			this._txtIdentityServerDBName.Size = new System.Drawing.Size(171, 20);
			this._txtIdentityServerDBName.TabIndex = 1;
			this._txtIdentityServerDBName.Text = "IdentityServerConfiguration";
			// 
			// _txtRaamSsrsReportFolder
			// 
			this._txtRaamSsrsReportFolder.Location = new System.Drawing.Point(151, 267);
			this._txtRaamSsrsReportFolder.Name = "_txtRaamSsrsReportFolder";
			this._txtRaamSsrsReportFolder.Size = new System.Drawing.Size(137, 20);
			this._txtRaamSsrsReportFolder.TabIndex = 7;
			this._txtRaamSsrsReportFolder.Text = "R360Reports";
			// 
			// groupBox7
			// 
			this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox7.Controls.Add(this._txtTokenCacheLogPath);
			this.groupBox7.Controls.Add(this.label25);
			this.groupBox7.Controls.Add(this._txtTokenCacheDataPath);
			this.groupBox7.Controls.Add(this.label26);
			this.groupBox7.Controls.Add(this._txtTokenCacheDBName);
			this.groupBox7.Controls.Add(this.label27);
			this.groupBox7.Location = new System.Drawing.Point(6, 110);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(981, 71);
			this.groupBox7.TabIndex = 5;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "TokenCache Database";
			// 
			// _txtTokenCacheLogPath
			// 
			this._txtTokenCacheLogPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtTokenCacheLogPath.Location = new System.Drawing.Point(438, 43);
			this._txtTokenCacheLogPath.Name = "_txtTokenCacheLogPath";
			this._txtTokenCacheLogPath.Size = new System.Drawing.Size(535, 20);
			this._txtTokenCacheLogPath.TabIndex = 6;
			this._txtTokenCacheLogPath.Text = "D:\\SQLData\\Log";
			// 
			// _txtTokenCacheDataPath
			// 
			this._txtTokenCacheDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtTokenCacheDataPath.Location = new System.Drawing.Point(438, 19);
			this._txtTokenCacheDataPath.Name = "_txtTokenCacheDataPath";
			this._txtTokenCacheDataPath.Size = new System.Drawing.Size(535, 20);
			this._txtTokenCacheDataPath.TabIndex = 4;
			this._txtTokenCacheDataPath.Text = "D:\\SQLData\\Data";
			// 
			// _txtTokenCacheDBName
			// 
			this._txtTokenCacheDBName.Location = new System.Drawing.Point(111, 19);
			this._txtTokenCacheDBName.Name = "_txtTokenCacheDBName";
			this._txtTokenCacheDBName.Size = new System.Drawing.Size(171, 20);
			this._txtTokenCacheDBName.TabIndex = 1;
			this._txtTokenCacheDBName.Text = "TokenCache";
			// 
			// groupBox6
			// 
			this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox6.Controls.Add(this._txtWebServer);
			this.groupBox6.Controls.Add(this.label23);
			this.groupBox6.Controls.Add(this._txtRaamLogPath);
			this.groupBox6.Controls.Add(this.label4);
			this.groupBox6.Controls.Add(this._txtRaamDataPath);
			this.groupBox6.Controls.Add(this.label20);
			this.groupBox6.Controls.Add(this._txtRaamRaamDBName);
			this.groupBox6.Controls.Add(this.label22);
			this.groupBox6.Location = new System.Drawing.Point(6, 6);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(981, 98);
			this.groupBox6.TabIndex = 4;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "RAAM Database";
			// 
			// _txtWebServer
			// 
			this._txtWebServer.Location = new System.Drawing.Point(111, 67);
			this._txtWebServer.Name = "_txtWebServer";
			this._txtWebServer.Size = new System.Drawing.Size(171, 20);
			this._txtWebServer.TabIndex = 8;
			this._txtWebServer.Text = "e.g. R360.my-bank.com";
			// 
			// _txtRaamLogPath
			// 
			this._txtRaamLogPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRaamLogPath.Location = new System.Drawing.Point(438, 43);
			this._txtRaamLogPath.Name = "_txtRaamLogPath";
			this._txtRaamLogPath.Size = new System.Drawing.Size(535, 20);
			this._txtRaamLogPath.TabIndex = 6;
			this._txtRaamLogPath.Text = "D:\\SQLData\\Log";
			// 
			// _txtRaamDataPath
			// 
			this._txtRaamDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRaamDataPath.Location = new System.Drawing.Point(438, 19);
			this._txtRaamDataPath.Name = "_txtRaamDataPath";
			this._txtRaamDataPath.Size = new System.Drawing.Size(535, 20);
			this._txtRaamDataPath.TabIndex = 4;
			this._txtRaamDataPath.Text = "D:\\SQLData\\Data";
			// 
			// _txtRaamRaamDBName
			// 
			this._txtRaamRaamDBName.Location = new System.Drawing.Point(111, 19);
			this._txtRaamRaamDBName.Name = "_txtRaamRaamDBName";
			this._txtRaamRaamDBName.Size = new System.Drawing.Size(171, 20);
			this._txtRaamRaamDBName.TabIndex = 1;
			this._txtRaamRaamDBName.Text = "RAAM";
			this._txtRaamRaamDBName.TextChanged += new System.EventHandler(this._txtRaamRaamDBName_TextChanged);
			// 
			// _tabFramework
			// 
			this._tabFramework.BackColor = System.Drawing.SystemColors.Control;
			this._tabFramework.Controls.Add(this.groupBox9);
			this._tabFramework.Location = new System.Drawing.Point(4, 22);
			this._tabFramework.Name = "_tabFramework";
			this._tabFramework.Padding = new System.Windows.Forms.Padding(3);
			this._tabFramework.Size = new System.Drawing.Size(994, 367);
			this._tabFramework.TabIndex = 2;
			this._tabFramework.Text = "Framework";
			// 
			// groupBox9
			// 
			this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox9.Controls.Add(this._txtFrameworkLogPath);
			this.groupBox9.Controls.Add(this.label33);
			this.groupBox9.Controls.Add(this._txtFrameworkDataPath);
			this.groupBox9.Controls.Add(this.label34);
			this.groupBox9.Controls.Add(this._txtFrameworkDBName);
			this.groupBox9.Controls.Add(this.label35);
			this.groupBox9.Location = new System.Drawing.Point(3, 6);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Size = new System.Drawing.Size(981, 71);
			this.groupBox9.TabIndex = 9;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "WFS Portal Database";
			// 
			// _txtFrameworkLogPath
			// 
			this._txtFrameworkLogPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtFrameworkLogPath.Location = new System.Drawing.Point(438, 43);
			this._txtFrameworkLogPath.Name = "_txtFrameworkLogPath";
			this._txtFrameworkLogPath.Size = new System.Drawing.Size(535, 20);
			this._txtFrameworkLogPath.TabIndex = 6;
			this._txtFrameworkLogPath.Text = "D:\\SQLData\\Log";
			// 
			// label33
			// 
			this.label33.AutoSize = true;
			this.label33.Location = new System.Drawing.Point(291, 46);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(131, 13);
			this.label33.TabIndex = 5;
			this.label33.Text = "Transaction Log File Path:";
			this._toolTips.SetToolTip(this.label33, "The folder where the database transaction log file should be placed.  Just the fo" +
        "lder (e.g. D:\\SQLData\\Log) -- the file will be automatically named after the dat" +
        "abase.");
			// 
			// _txtFrameworkDataPath
			// 
			this._txtFrameworkDataPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtFrameworkDataPath.Location = new System.Drawing.Point(438, 19);
			this._txtFrameworkDataPath.Name = "_txtFrameworkDataPath";
			this._txtFrameworkDataPath.Size = new System.Drawing.Size(535, 20);
			this._txtFrameworkDataPath.TabIndex = 4;
			this._txtFrameworkDataPath.Text = "D:\\SQLData\\Data";
			// 
			// label34
			// 
			this.label34.AutoSize = true;
			this.label34.Location = new System.Drawing.Point(291, 22);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(77, 13);
			this.label34.TabIndex = 3;
			this.label34.Text = "Data File Path:";
			this._toolTips.SetToolTip(this.label34, "The folder where the main database file should be placed.  Just the folder (e.g. " +
        "D:\\SQLData\\Data) -- the file will be automatically named after the database.");
			// 
			// _txtFrameworkDBName
			// 
			this._txtFrameworkDBName.Location = new System.Drawing.Point(111, 19);
			this._txtFrameworkDBName.Name = "_txtFrameworkDBName";
			this._txtFrameworkDBName.Size = new System.Drawing.Size(171, 20);
			this._txtFrameworkDBName.TabIndex = 1;
			this._txtFrameworkDBName.Text = "WFSPortal";
			// 
			// label35
			// 
			this.label35.AutoSize = true;
			this.label35.Location = new System.Drawing.Point(12, 22);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(87, 13);
			this.label35.TabIndex = 0;
			this.label35.Text = "Database Name:";
			this._toolTips.SetToolTip(this.label35, "The name of the database (e.g. WFSPortal).");
			// 
			// DBConfigForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1008, 539);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "DBConfigForm";
			this.Padding = new System.Windows.Forms.Padding(3);
			this.Text = "DB Deployment Parameters";
			this.Load += new System.EventHandler(this.DBConfigForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this._numPartitionYears)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this._tabControl.ResumeLayout(false);
			this._tabRecHub.ResumeLayout(false);
			this._gprRecHub.ResumeLayout(false);
			this._gprRecHub.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this._tabRaam.ResumeLayout(false);
			this._tabRaam.PerformLayout();
			this.groupBox8.ResumeLayout(false);
			this.groupBox8.PerformLayout();
			this.groupBox7.ResumeLayout(false);
			this.groupBox7.PerformLayout();
			this.groupBox6.ResumeLayout(false);
			this.groupBox6.PerformLayout();
			this._tabFramework.ResumeLayout(false);
			this.groupBox9.ResumeLayout(false);
			this.groupBox9.PerformLayout();
			this.ResumeLayout(false);
            this.AcceptButton = this._btnSave;

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox _txtParamFile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button _btnLoadParams;
		private System.Windows.Forms.TextBox _txtDBServer;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ToolTip _toolTips;
		private System.Windows.Forms.TextBox _txtLogFolder;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox _txtInstallRoot;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox _txtRecHubDataPath;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox _txtDBRecHubDBName;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox _chkPartitionsEnabled;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown _numPartitionYears;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.DateTimePicker _dtPartitionStart;
		private System.Windows.Forms.ComboBox _cboPartitionType;
		private System.Windows.Forms.TextBox _txtRecHubAuditPath;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox _txtRecHubNotificationPath;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox _txtRecHubItemPath;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox _txtRecHubLogPath;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox _txtPartitioned;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button _btnSave;
		private System.Windows.Forms.Button _btnCancel;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.TextBox _txtPartitionStartDate;
		private System.Windows.Forms.TextBox _txtPartitionType;
		private System.Windows.Forms.TextBox _txtPartitionYears;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox _txtSSISConfigLogPath;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox _txtSSISConfigDataPath;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox _txtSSISConfigDBName;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TextBox _txtSSISLogLogPath;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox _txtSSISLogDataPath;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox _txtSSISLogDBName;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.TextBox _txtRaamDBName;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TabControl _tabControl;
		private System.Windows.Forms.TabPage _tabRecHub;
		private System.Windows.Forms.GroupBox _gprRecHub;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TabPage _tabRaam;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.TextBox _txtRaamLogPath;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox _txtRaamDataPath;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox _txtRaamRaamDBName;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.TextBox _txtWebServer;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.TextBox _txtTokenCacheLogPath;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.TextBox _txtTokenCacheDataPath;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.TextBox _txtTokenCacheDBName;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.TextBox _txtSsrsUrl;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.TextBox _txtRecHubSSRSFolder;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.TextBox _txtRaamSsrsReportFolder;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.TextBox _txtIdentityServerLogPath;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.TextBox _txtIdentityServerDataPath;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.TextBox _txtIdentityServerDBName;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.TabPage _tabFramework;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.TextBox _txtFrameworkLogPath;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.TextBox _txtFrameworkDataPath;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.TextBox _txtFrameworkDBName;
		private System.Windows.Forms.Label label35;
	}
}