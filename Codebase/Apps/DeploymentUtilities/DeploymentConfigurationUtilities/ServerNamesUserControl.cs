﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Wfs.R360.DeploymentConfigurationUtilities
{
    public partial class ServerNamesUserControl : UserControl
    {
        public ServerNamesUserControl()
        {
            InitializeComponent();
            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime)
                txtServerNames.Text = string.Join("\r\n", new[] { "1", "2", "3", "4", "5", "6" });
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TextBox TextBox
        {
            get { return txtServerNames; }
        }
    }
}
