﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace DeploymentEncryptionUtility
{
	internal static class Utils
	{
		private static bool _saveBackup = true; // TODO: Used for testing only; we should never create backup files in a real situation...

		public static void EncryptForStaging(string fileName, string password, Action<string> addStatus)
		{
			if (string.IsNullOrEmpty(password))
				throw new ArgumentNullException("password");

			// Read the File
			addStatus("Processing: " + fileName);
			var currentConfig = File.ReadAllText(fileName);

			// Remember values to replace
			Dictionary<string, string> replacements = new Dictionary<string, string>();

			// Kludge - we don't want to unescape entities...  Assuming all escaped characters are part of "values" (they should be) this should be OK...?
			var unescapedConfig = currentConfig.Replace("&", "&amp;");

			// Detect values that should be encrypted...  Attribute values will be the easist...
			XDocument xDoc = XDocument.Load(new StringReader(unescapedConfig));
			var xConnectionStrings = xDoc.Element("connectionStrings");
			if (xConnectionStrings != null)
			{
				int count = 0;
				addStatus("Encrypting Connection Strings...");

				var elems = xConnectionStrings.Elements("add");
				foreach (var elem in elems)
				{
					var connectionString = elem.Attribute("connectionString");
					if (connectionString != null)
					{
						// Skip connection strings that already contain replacement variables...
						if (!connectionString.Value.Contains("@@"))
						{
							replacements[connectionString.Value] = Encrypt(connectionString.Value, password);
							count++;
						}
					}
				}
				addStatus(string.Format("...{0} connection strings to update", count));
			}

			// Testing only - save a backup copy
			if (_saveBackup)
			{
				var backup = fileName + ".bak";
				if (File.Exists(backup))
				{
					File.Delete(backup);
					Thread.Sleep(100); // Try to prevent errors from asynchronous file operations...
				}
				File.Copy(fileName, backup);
			}

			// Overwrite configruation file with updates - will this preserve spaces, etc.?
			int replaceCount = 0;
			foreach (var entry in replacements)
			{
				int pos = 0;
				pos = currentConfig.IndexOf(entry.Key, pos);
				while (pos >= 0)
				{
					replaceCount++;
					currentConfig = currentConfig.Substring(0, pos) + entry.Value + currentConfig.Substring(pos + entry.Key.Length);
					pos = currentConfig.IndexOf(entry.Key, pos);
				}
			}
			addStatus(string.Format("Updating: {0} values replaced.", replaceCount));
			File.WriteAllText(fileName, currentConfig);
			addStatus("<File Update Complete>");
		}

		private static AesCryptoServiceProvider _aesEncryptor = new AesCryptoServiceProvider();

		private static SHA1Cng _sha1Hasher = new SHA1Cng();
		private static string Encrypt(string value, string password)
		{
			var key = CreateKey(_aesEncryptor.KeySize, password);
			var iv = CreateIV(_aesEncryptor.BlockSize, password);
			var encryptor = _aesEncryptor.CreateEncryptor(key, iv);

			// Create the streams used for encryption. 
			using (MemoryStream msEncrypt = new MemoryStream())
			{
				using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
				{
					using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
					{

						//Write all data to the stream.
						swEncrypt.Write(value);
					}
					var encrypted = msEncrypt.ToArray();
					return Convert.ToBase64String(encrypted);
				}
			}

		}

		private static byte[] CreateKey(int bits, string password)
		{
			// Duplicate / truncate the password to acheive appropriate length...
			byte[] key = new byte[bits / 8];
			byte[] seed = Encoding.UTF8.GetBytes(password);
			int index = 0;
			while (index < key.Length)
			{
				Array.Copy(seed, 0, key, index, Math.Min(seed.Length, key.Length - index));
				index += seed.Length;
			}
			return key;
		}

		private static byte[] CreateIV(int bits, string password)
		{
			// Need a predictable IV, so base it on the password - but the IV is somewhat "public", so don't use the password in plain text...
			byte[] seed = Encoding.UTF8.GetBytes(password);
			byte[] hash = _sha1Hasher.ComputeHash(seed);
			byte[] iv = new byte[bits / 8];
			int index = 0;
			while (index < iv.Length)
			{
				Array.Copy(hash, 0, iv, index, Math.Min(hash.Length, iv.Length - index));
				index += hash.Length;
			}
			return iv;
		}

	}
}
