﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeploymentEncryptionUtility
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void AddMessage(string message)
		{
			BeginInvoke(new Action<string>(AddMessageUI), message);
		}

		private void AddMessageUI(string message)
		{
			message = DateTime.Now.ToString("HH:mm:ss.fff") + ": " + message;
			if (_txtMessages.Text.Length > 0)
				message = "\r\n" + message;

			_txtMessages.Text = _txtMessages.Text + message;
			_txtMessages.Focus();
			_txtMessages.SelectionStart = _txtMessages.Text.Length;
			_txtMessages.ScrollToCaret();
		}

		private async void _btnEncrypt_Click(object sender, EventArgs e)
		{
			try
			{
				string file = _txtFile.Text;
				string password = _txtPassword.Text;
				Action<string> status = AddMessage;
				await Task.Run(() => Utils.EncryptForStaging(file, password, status));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Unexpected Error");
			}
		}
	}
}
