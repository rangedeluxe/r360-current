﻿namespace DeploymentEncryptionUtility
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this._txtMessages = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this._txtFile = new System.Windows.Forms.TextBox();
			this._btnBrowse = new System.Windows.Forms.Button();
			this._btnEncrypt = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this._txtPassword = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// _txtMessages
			// 
			this._txtMessages.Dock = System.Windows.Forms.DockStyle.Top;
			this._txtMessages.Location = new System.Drawing.Point(0, 0);
			this._txtMessages.Multiline = true;
			this._txtMessages.Name = "_txtMessages";
			this._txtMessages.Size = new System.Drawing.Size(687, 131);
			this._txtMessages.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 163);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(26, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "File:";
			// 
			// _txtFile
			// 
			this._txtFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtFile.Location = new System.Drawing.Point(68, 160);
			this._txtFile.Name = "_txtFile";
			this._txtFile.Size = new System.Drawing.Size(529, 20);
			this._txtFile.TabIndex = 2;
			// 
			// _btnBrowse
			// 
			this._btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnBrowse.Location = new System.Drawing.Point(607, 158);
			this._btnBrowse.Name = "_btnBrowse";
			this._btnBrowse.Size = new System.Drawing.Size(75, 23);
			this._btnBrowse.TabIndex = 3;
			this._btnBrowse.Text = "Browse...";
			this._btnBrowse.UseVisualStyleBackColor = true;
			// 
			// _btnEncrypt
			// 
			this._btnEncrypt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnEncrypt.Location = new System.Drawing.Point(98, 372);
			this._btnEncrypt.Name = "_btnEncrypt";
			this._btnEncrypt.Size = new System.Drawing.Size(75, 23);
			this._btnEncrypt.TabIndex = 4;
			this._btnEncrypt.Text = "Encrypt";
			this._btnEncrypt.UseVisualStyleBackColor = true;
			this._btnEncrypt.Click += new System.EventHandler(this._btnEncrypt_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 137);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(56, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Password:";
			// 
			// _txtPassword
			// 
			this._txtPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtPassword.Location = new System.Drawing.Point(68, 134);
			this._txtPassword.Name = "_txtPassword";
			this._txtPassword.Size = new System.Drawing.Size(614, 20);
			this._txtPassword.TabIndex = 6;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(687, 419);
			this.Controls.Add(this._txtPassword);
			this.Controls.Add(this.label2);
			this.Controls.Add(this._btnEncrypt);
			this.Controls.Add(this._btnBrowse);
			this.Controls.Add(this._txtFile);
			this.Controls.Add(this.label1);
			this.Controls.Add(this._txtMessages);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.Text = "Deployment Encryption Utility";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox _txtMessages;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox _txtFile;
		private System.Windows.Forms.Button _btnBrowse;
		private System.Windows.Forms.Button _btnEncrypt;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox _txtPassword;
	}
}

