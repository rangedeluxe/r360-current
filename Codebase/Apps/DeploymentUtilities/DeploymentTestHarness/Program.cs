﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wfs.R360.DeploymentConfigurationUtilities;

namespace DeploymentTestHarness
{
	class Program
	{
		static void Main(string[] args)
		{
			new ConfigManager().GetParams(true, "");

			new ConfigManager().GetDBParams(true);
		}
	}
}
