﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace WFS.R360.StagingUtilities
{
	public class ConfigManager
	{
		private string _paramFileName;
		public string ParamFileName
		{
			get { return _paramFileName; }
			set
			{
				_paramFileName = value;
				_paramXDoc = null;
			}
		}

		public void GetParams(bool prompt)
		{
			// Open Parameter form, and return the name of the parameter file seleted.
			var frm = new ConfigForm();
			if (prompt)
				frm.ShowDialog();
			else
				frm.ValidateParamFile();

			ParamFileName = frm.ParamFileName ?? "";
		}

		private XDocument _paramXDoc;
		private ConfigForm _frm = new ConfigForm();
		public string GetParamValue(string key)
		{
			// Open (potentially cached) parameter file
			if (_paramXDoc == null)
			{
				_paramXDoc = XDocument.Load(ParamFileName);
			}

			var entry = _frm._params[key];

			// Read parameter
			XElement elem = _paramXDoc.Descendants(key).FirstOrDefault();
			if (elem == null)
				return string.Empty;

			string value = elem.Value;
			if (entry.Encrypt)
				value = _frm.Decrypt(value, key);
			return value;
		}
	}
}
