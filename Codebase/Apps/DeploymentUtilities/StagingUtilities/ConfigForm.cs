﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace WFS.R360.StagingUtilities
{
	public partial class ConfigForm : Form
	{
		public string ParamFileName { get; set; }

		internal class ParamMap
		{
			public TextBox Box { get; set; }
			public bool Encrypt { get; set; }
		}

		internal readonly Dictionary<string, ParamMap> _params;

		public ConfigForm()
		{
			InitializeComponent();

			string basePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
			_txtParamFile.Text = Path.Combine(basePath, "StagingParams.xml");

			_params = new Dictionary<string, ParamMap>();
			_params["FrameworkSourceDir"] = new ParamMap { Box = _txtFrameworkSourceDir, Encrypt = false };
			_params["RaamSourceDir"] = new ParamMap { Box = _txtRAAMSourceDir, Encrypt = false };
			_params["RecHubSourceDir"] = new ParamMap { Box = _txtRecHubSourceDir, Encrypt = false };
			_params["StagingDir"] = new ParamMap { Box = _txtStageDir, Encrypt = false };
		}

		private void _btnLoadParams_Click(object sender, EventArgs e)
		{
			try
			{
				DeserializeParams();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void _btnSave_Click(object sender, EventArgs e)
		{
			try
			{
				// TODO: Validate all of the parameters - length, allowed characters, format, error on "localhost", etc.
				ParamFileName = _txtParamFile.Text;
				SerializeParams();
				DialogResult = System.Windows.Forms.DialogResult.OK;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void _btnCancel_Click(object sender, EventArgs e)
		{
			ParamFileName = null;
			DialogResult = System.Windows.Forms.DialogResult.Cancel;
		}

		private void SerializeParams()
		{
			using (XmlWriter xWriter = XmlWriter.Create(ParamFileName, new XmlWriterSettings() { OmitXmlDeclaration = true, Indent = true }))
			{
				xWriter.WriteStartElement("Params");
				foreach (var entry in _params)
				{
					xWriter.WriteElementString(entry.Key, entry.Value.Encrypt ? Encrypt(entry.Value.Box.Text, entry.Key) : entry.Value.Box.Text);
				}
				xWriter.WriteEndElement();
			}
		}

		private void DeserializeParams()
		{
			XDocument xDoc = XDocument.Load(_txtParamFile.Text);
			foreach (var entry in _params)
			{
				var elem = xDoc.Descendants(entry.Key).FirstOrDefault();
				if (elem != null)
					entry.Value.Box.Text = (entry.Value.Encrypt ? Decrypt(elem.Value, entry.Key) : elem.Value);
			}
		}

		private static readonly byte[] _entropy = new byte[] { 77, 42, 21, 92 }; // Arbitrary, but determinant...
		internal string Encrypt(string src, string entropy)
		{
			byte[] fullEntropy = _entropy.Concat(Encoding.UTF8.GetBytes(entropy)).ToArray();
			byte[] orig = Encoding.UTF8.GetBytes(src);
			byte[] secure = ProtectedData.Protect(orig, fullEntropy, DataProtectionScope.LocalMachine);
			return BitConverter.ToString(secure).Replace("-", "");
		}

		internal string Decrypt(string src, string entropy)
		{
			byte[] fullEntropy = _entropy.Concat(Encoding.UTF8.GetBytes(entropy)).ToArray();
			byte[] secured = FromHex(src);
			byte[] orig = ProtectedData.Unprotect(secured, fullEntropy, DataProtectionScope.LocalMachine);
			return Encoding.UTF8.GetString(orig);
		}

		private byte[] FromHex(string src)
		{
			byte[] result = new byte[src.Length / 2];
			for (int i = 0; i < src.Length; i += 2)
			{
				result[i / 2] = Byte.Parse(src.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
			}
			return result;
		}

		private void ConfigForm_Load(object sender, EventArgs e)
		{
			try
			{
				if (File.Exists(_txtParamFile.Text))
				{
					DeserializeParams();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		public void ValidateParamFile()
		{
			ParamFileName = null;
			try
			{
				if (File.Exists(_txtParamFile.Text))
				{
					// TODO: Validate parameters in some way...? DeserializeParams();
					ParamFileName = _txtParamFile.Text;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}
	}
}
