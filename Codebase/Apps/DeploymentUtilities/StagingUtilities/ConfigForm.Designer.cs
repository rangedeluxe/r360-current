﻿namespace WFS.R360.StagingUtilities
{
	partial class ConfigForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigForm));
			this._txtFrameworkSourceDir = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this._btnLoadParams = new System.Windows.Forms.Button();
			this._txtParamFile = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this._txtStageDir = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this._btnCancel = new System.Windows.Forms.Button();
			this._btnSave = new System.Windows.Forms.Button();
			this._txtRAAMSourceDir = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this._txtRecHubSourceDir = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// _txtFrameworkSourceDir
			// 
			this._txtFrameworkSourceDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtFrameworkSourceDir.Location = new System.Drawing.Point(124, 48);
			this._txtFrameworkSourceDir.Name = "_txtFrameworkSourceDir";
			this._txtFrameworkSourceDir.Size = new System.Drawing.Size(713, 20);
			this._txtFrameworkSourceDir.TabIndex = 70;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(11, 51);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(107, 13);
			this.label2.TabIndex = 69;
			this.label2.Text = "Framework Directory:";
			// 
			// _btnLoadParams
			// 
			this._btnLoadParams.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this._btnLoadParams.Location = new System.Drawing.Point(762, 10);
			this._btnLoadParams.Name = "_btnLoadParams";
			this._btnLoadParams.Size = new System.Drawing.Size(75, 23);
			this._btnLoadParams.TabIndex = 67;
			this._btnLoadParams.Text = "Load";
			this._btnLoadParams.UseVisualStyleBackColor = true;
			this._btnLoadParams.Click += new System.EventHandler(this._btnLoadParams_Click);
			// 
			// _txtParamFile
			// 
			this._txtParamFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtParamFile.Location = new System.Drawing.Point(124, 12);
			this._txtParamFile.Name = "_txtParamFile";
			this._txtParamFile.Size = new System.Drawing.Size(632, 20);
			this._txtParamFile.TabIndex = 66;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(11, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(77, 13);
			this.label1.TabIndex = 65;
			this.label1.Text = "Parameter File:";
			// 
			// _txtStageDir
			// 
			this._txtStageDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtStageDir.Location = new System.Drawing.Point(124, 136);
			this._txtStageDir.Name = "_txtStageDir";
			this._txtStageDir.Size = new System.Drawing.Size(713, 20);
			this._txtStageDir.TabIndex = 72;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(11, 139);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(91, 13);
			this.label3.TabIndex = 71;
			this.label3.Text = "Staging Directory:";
			// 
			// _btnCancel
			// 
			this._btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this._btnCancel.Location = new System.Drawing.Point(281, 171);
			this._btnCancel.Name = "_btnCancel";
			this._btnCancel.Size = new System.Drawing.Size(133, 23);
			this._btnCancel.TabIndex = 74;
			this._btnCancel.Text = "Cancel Staging";
			this._btnCancel.UseVisualStyleBackColor = true;
			this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
			// 
			// _btnSave
			// 
			this._btnSave.Location = new System.Drawing.Point(124, 171);
			this._btnSave.Name = "_btnSave";
			this._btnSave.Size = new System.Drawing.Size(133, 23);
			this._btnSave.TabIndex = 73;
			this._btnSave.Text = "Save and Continue";
			this._btnSave.UseVisualStyleBackColor = true;
			this._btnSave.Click += new System.EventHandler(this._btnSave_Click);
			// 
			// _txtRAAMSourceDir
			// 
			this._txtRAAMSourceDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRAAMSourceDir.Location = new System.Drawing.Point(124, 74);
			this._txtRAAMSourceDir.Name = "_txtRAAMSourceDir";
			this._txtRAAMSourceDir.Size = new System.Drawing.Size(713, 20);
			this._txtRAAMSourceDir.TabIndex = 76;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(11, 77);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(86, 13);
			this.label4.TabIndex = 75;
			this.label4.Text = "RAAM Directory:";
			// 
			// _txtRecHubSourceDir
			// 
			this._txtRecHubSourceDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtRecHubSourceDir.Location = new System.Drawing.Point(124, 100);
			this._txtRecHubSourceDir.Name = "_txtRecHubSourceDir";
			this._txtRecHubSourceDir.Size = new System.Drawing.Size(713, 20);
			this._txtRecHubSourceDir.TabIndex = 78;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(11, 103);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(95, 13);
			this.label5.TabIndex = 77;
			this.label5.Text = "RecHub Directory:";
			// 
			// ConfigForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this._btnCancel;
			this.ClientSize = new System.Drawing.Size(849, 217);
			this.Controls.Add(this._txtRecHubSourceDir);
			this.Controls.Add(this.label5);
			this.Controls.Add(this._txtRAAMSourceDir);
			this.Controls.Add(this.label4);
			this.Controls.Add(this._btnCancel);
			this.Controls.Add(this._btnSave);
			this.Controls.Add(this._txtStageDir);
			this.Controls.Add(this.label3);
			this.Controls.Add(this._txtFrameworkSourceDir);
			this.Controls.Add(this.label2);
			this.Controls.Add(this._btnLoadParams);
			this.Controls.Add(this._txtParamFile);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ConfigForm";
			this.Text = "Staging Configuration Values";
			this.Load += new System.EventHandler(this.ConfigForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();
            this.AcceptButton = this._btnSave;

		}

		#endregion

		private System.Windows.Forms.TextBox _txtFrameworkSourceDir;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button _btnLoadParams;
		private System.Windows.Forms.TextBox _txtParamFile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox _txtStageDir;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button _btnCancel;
		private System.Windows.Forms.Button _btnSave;
		private System.Windows.Forms.TextBox _txtRAAMSourceDir;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox _txtRecHubSourceDir;
		private System.Windows.Forms.Label label5;
	}
}