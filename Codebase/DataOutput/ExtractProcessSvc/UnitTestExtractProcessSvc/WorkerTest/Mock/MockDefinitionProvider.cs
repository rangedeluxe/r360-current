﻿using System;
using System.Collections.Generic;
using System.IO;
using WFS.RecHub.Extract.ExtractProcessSvc;
using WFS.RecHub.Extract.ExtractProcessSvc.Workers;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133362 HRN 03/25/2014 
*   - Initial Version 
* WI 159329 BLR 08/18/2014
*  - Added call to the Definition XML.
******************************************************************************/

namespace UnitTestExtractProcessSvc.WorkerTest.Mock
{
    public class MockDefinitionProvider : IDefinitionProvider
    {

        public IEnumerable<ExtractDefinitionDTO> GetExtractDefinitions(DateTime? changedAfter)
        {
            ExtractDefinitionDTO ExtractDefinitionDTOitem = new ExtractDefinitionDTO();
            ExtractDefinitionDTOitem.ExtractDefinitionId = 1;
            ExtractDefinitionDTOitem.ExtractName = "  Extract 3";
            ExtractDefinitionDTOitem.ExtractDefinition = File.ReadAllText(@"Xml\ExtractDefinition.xml");
            List<ExtractDefinitionDTO> InputData = new List<ExtractDefinitionDTO>();
            InputData.Add(ExtractDefinitionDTOitem);
            IEnumerable<ExtractDefinitionDTO> Expected = InputData;
            return Expected; 
        }

        public string GetExtractDefinitionXML(int id)
        {
            throw new NotImplementedException();
        }
    }
}
