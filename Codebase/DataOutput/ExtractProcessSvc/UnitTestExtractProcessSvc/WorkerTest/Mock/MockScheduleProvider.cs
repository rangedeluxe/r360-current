﻿using System;
using System.Collections.Generic;
using WFS.RecHub.Extract.ExtractProcessSvc;
using WFS.RecHub.Extract.ExtractProcessSvc.Workers;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133362 HRN 03/25/2014 
*   - Initial Version 
******************************************************************************/

namespace UnitTestExtractProcessSvc.WorkerTest.Mock
{
    public class MockScheduleProvider:IScheduleProvider
    {
        public IEnumerable<ExtractScheduleDTO> GetExtractSchedules()
        {
            ExtractScheduleDTO item = new ExtractScheduleDTO();
            item.ExtractScheduleId = 1;
            item.ExtractDefinitionId = 1;
            item.Description = "My scheduled Extract";
            item.DaysOfWeek = "1111111";
            item.ExtractRunArguments = "/p:02/28/2006";
            item.ScheduleTime = DateTime.Now.TimeOfDay;
            List<ExtractScheduleDTO> lstExtractScheduleDTO = new List<ExtractScheduleDTO>();
            lstExtractScheduleDTO.Add(item);
            return lstExtractScheduleDTO as IEnumerable<ExtractScheduleDTO>; 
        }
    }
}
