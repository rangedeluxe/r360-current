﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.Extract.ExtractProcessSvc;
using WFS.RecHub.Extract.ExtractProcessSvc.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date:08/12/2014
*
* Purpose: 
*
* Modification History* 
* WI 158147 BLR 08/12/2014
*   - Initial Version 
******************************************************************************/

namespace UnitTestExtractProcessSvc.WorkerTest
{
    public abstract class BaseRunFixture
    {
        public BaseRunFixture()
        {
            var provider = new MockConfigurationProvider();

            provider.SetSetting("LogFilePath", "ExtractWizardLog.txt");
            provider.SetSetting("LogFileMaxSize", "1024");
            provider.SetSetting("ExceptionLogFile", "ExtractWizardExceptions.txt");
            provider.SetSetting("ExtractRunPath", @"C:\Dev\2.01_Dev\Codebase\ExtractWizard\src\ExtractWizard\ExtractRun\bin\Debug\ExtractRun.exe");
            provider.SetSetting("ExtractRunArguments", "%1");
            provider.SetSetting("DatabasePollInterval", "60");
            provider.SetSetting("SiteKey", "ipoServices");
            provider.SetSetting("localIni", @"C:\WFSApps\RecHub\bin2\RecHub.ini");

            Settings = new ExtractProcessorConfiguration(provider);
            cCommonLogLib.Settings = Settings;
        }

        public ExtractProcessorConfiguration Settings { get; private set; }
    }
}
