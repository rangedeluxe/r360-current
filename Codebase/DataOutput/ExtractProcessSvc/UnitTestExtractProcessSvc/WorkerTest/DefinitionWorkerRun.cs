﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnitTestExtractProcessSvc.WorkerTest.Mock;
using WFS.RecHub.Extract.ExtractProcessSvc;
using WFS.RecHub.Extract.ExtractProcessSvc.Workers;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133362 HRN 03/25/2014 
*   - Initial Version 
* WI 158147 BLR 08/12/2014
*   - Added Settings
* WI 159329 BLR 08/18/2014
*  - Added call to the Definition XML.
******************************************************************************/


namespace UnitTestExtractProcessSvc.WorkerTest
{
    /// <summary>
    /// Class for testing the generated output from extract run matches with the expected output.
    /// </summary>
    [Ignore]
    [TestClass]
    public class DefinitionWorkerRun : BaseRunFixture
    {
       
        [TestMethod]
        public void TestDefinitionWorker()
        {
            // Arrange
            var filename = "out.xdf";

            var defprovider = new UnitTestExtractProcessSvc.WorkerTest.Mock.MockDefinitionProvider();
            var defs = defprovider
                .GetExtractDefinitions(null);
            var schedules = new UnitTestExtractProcessSvc.WorkerTest.Mock.MockScheduleProvider()
                .GetExtractSchedules();
            var runner = new DefinitionWorker(Settings, defprovider);

            // Act
            foreach (var sched in schedules)
            {
                // Make sure the file is available first.
                if (File.Exists(filename))
                    File.Delete(filename);

                var def = defs.First(x => x.ExtractDefinitionId == sched.ExtractDefinitionId);
                runner.ExecuteDefinition(def, sched);

                var expectedoutput = File.ReadAllText(@"Resources\ExpectedOutput.xdf");
                var actualoutput = File.ReadAllText(filename);

                // Assert
                Assert.IsTrue(File.Exists(filename));
                Assert.AreEqual(expectedoutput, actualoutput);
            }
        }
    }
}
