﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.Extract.ExtractProcessSvc;
using WFS.RecHub.Extract.ExtractProcessSvc.Workers;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133362 HRN 03/25/2014 
*   - Initial Version 
* WI 158147 BLR 08/12/2014
*   - Added Settings 
******************************************************************************/

namespace UnitTestExtractProcessSvc.WorkerTest
{
    /// <summary>
    /// This class is for checking extract definition
    /// </summary>
    [Ignore]
    [TestClass]
    public class DefinitionProviderRun : BaseRunFixture
    {
        [TestMethod]
        public void TestDatabaseDefinitionProvider()
        {
            // Arrange
            DefinitionProvider defprovider = new DefinitionProvider(Settings);
            
            // Act
            var defs = defprovider.GetExtractDefinitions(null);

            // Assert
            Assert.IsTrue(defs != null && defs.Any(), string.Format("No Extract Definition available for Last Schedule Load"));
        }
        
    }
}
