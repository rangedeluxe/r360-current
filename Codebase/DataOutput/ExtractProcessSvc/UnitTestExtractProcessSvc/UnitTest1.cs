﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Extract.ExtractProcessSvc.Workers;
using UnitTestExtractProcessSvc.WorkerTest;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133362 HRN 03/25/2014 
*   - Initial Version 
* WI 158147 BLR 08/12/2014
*   - Added Settings 
* WI 159329 BLR 08/18/2014
*  - Added call to the Definition XML.
******************************************************************************/

namespace UnitTestExtractProcessSvc
{
    [TestClass]
    public class UnitTest1 : BaseRunFixture
    {
        [TestMethod]
        public void TestParseDates()
        {
            var target = new DefinitionWorker(Settings, null);
            Assert.IsNotNull(target, "Target is not null");

            Assert.AreEqual("", target.ParseDates(""), "empty string should still be empty");
            Assert.AreEqual("Lorem ipsum dolor sit amet", target.ParseDates("Lorem ipsum dolor sit amet"), "simple string should still same");
            Assert.AreEqual(DateTime.Now.ToShortDateString(), target.ParseDates("{today}"), "simple replacement string should be replaced");
            Assert.AreEqual("Consectetur adipiscing elit " + DateTime.Now.ToShortDateString() + " Phasellus convallis lobortis turpis", target.ParseDates("Consectetur adipiscing elit {today} Phasellus convallis lobortis turpis"), "replacement string should be replaced");
            Assert.AreEqual("Vel sagittis nulla fermentum non. " + DateTime.Now.ToShortDateString() + " Sed auctor enim. " + DateTime.Now.ToShortDateString() +" Integer vitae justo et orci", target.ParseDates("Vel sagittis nulla fermentum non. {today} Sed auctor enim. {today} Integer vitae justo et orci"), "multiple replacement strings should be replaced");

            Assert.AreEqual((DateTime.Now - new TimeSpan(1,0,0,0)).ToShortDateString(), target.ParseDates("{today-1d}"), "yesterday replacement string should be replaced");
            Assert.AreEqual((DateTime.Now - new TimeSpan(3, 0, 0, 0)).ToShortDateString(), target.ParseDates("{today-3d}"), "3 days ago replacement string should be replaced");
            Assert.AreEqual((DateTime.Now + new TimeSpan(1, 0, 0, 0)).ToShortDateString(), target.ParseDates("{today+1d}"), "tomorrow replacement string should be replaced");
            Assert.AreEqual((DateTime.Now - new TimeSpan(7, 0, 0, 0)).ToShortDateString(), target.ParseDates("{today-1w}"), "last week replacement string should be replaced");

        }
    }
}


