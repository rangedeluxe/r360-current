﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DAL;
using System.Data.SqlClient;
using System.Xml;
/*******************************************************************************
*
*  Module: cExtractProcessSvcDal
*  Filename: cExtractProcessSvcDal.cs
*
* Revisions:
*
* ----------------------
* CR Task 135150 DW 04/03/2014
*   -Initial release version.
*   -Code taken from ExtractSchedulesDal to create the new cExtractProcessSvcDal
* WI 159329 BLR 08/18/2014
*  - Added call to the Definition XML.
******************************************************************************/
namespace WFS.RecHub.DAL 
{
    public class cExtractProcessSvcDal : _DALBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="cExtractDal"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
        public cExtractProcessSvcDal(string vSiteKey)
			: base(vSiteKey) {
		}

        public cExtractProcessSvcDal(string vSiteKey, ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }


        #region Extract Definitions
        /// <summary>
        /// Get the active extract definitions
        /// </summary>
        /// <param name="UpdatedSince">Optional, get updates since this datetime</param>
        /// <param name="results">DataTable with selected records</param>
        /// <returns>true = success</returns>
        public bool GetActiveExtractDefinitions(DateTime? UpdatedSince, out DataTable results)
        {
            bool bolRetVal = false;
            results = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                if (UpdatedSince != null)
                    arParms.Add(BuildParameter("@modifiedAfter", SqlDbType.DateTime, UpdatedSince, ParameterDirection.Input));

                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractDefinitions_GetAfterModDate", parms, out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetActiveExtractDefinitions(DateTime? UpdatedSince, out DataTable results)");
            }
            return bolRetVal;
        }

        public bool GetExtractDefinitionFile(int extractID, out string extractName, out XmlDocument xmlDocument)
        {
            bool bRtnval = false;
            xmlDocument = null;
            SqlParameter[] parms;
            SqlParameter parmExtractName;

            extractName = null;

            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, 0, ParameterDirection.Input));
                parmExtractName = BuildParameter("@parmExtractName", SqlDbType.VarChar, null, ParameterDirection.Output);
                arParms.Add(parmExtractName);

                parms = arParms.ToArray();
                DataTable dt;
                bRtnval = Database.executeProcedure("RecHubConfig.usp_ExtractDefinitions_Get_ByExtractDefID", parms, out dt);

                if (bRtnval)
                {
                    extractName = parmExtractName.Value.ToString();
                    xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml((string)dt.Rows[0]["ExtractDefinition"]);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetExtractDefinition(int extractID, out XmlDocument xmlDocument)");
            }

            return bRtnval;
        }
        #endregion

        #region Extract Schedule
        /// <summary>
        /// Get the active extract schedule
        /// </summary>
        /// <param name="results">DataTable with selected records</param>
        /// <returns>true = success</returns>
        public bool GetActiveExtractSchedule(out DataTable results)
        {
            bool bolRetVal = false;
            results = null;
            try
            {
                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractSchedules_GetActiveList", out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetActiveExtractSchedule(out DataTable results)");
            }

            return bolRetVal;
        }

        #endregion
    }
}


