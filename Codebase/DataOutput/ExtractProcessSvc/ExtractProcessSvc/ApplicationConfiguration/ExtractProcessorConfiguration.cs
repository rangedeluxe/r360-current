﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.Common.Crypto;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 08/12/2014
*
* Purpose: 
*
* Modification History
* 
* WI 158147 BLR 08/12/2014
*   - Initial release version.
*******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc.Configuration
{
    public class ExtractProcessorConfiguration : BaseConfigurationSettings
    {
        [ConfigurationSetting]
        public string LogFilePath { get; set; }
        [ConfigurationSetting]
        public string LogFileMaxSize { get; set; }
        [ConfigurationSetting]
        public string ExceptionLogFile { get; set; }
        [ConfigurationSetting]
        public string ExtractRunPath { get; set; }
        [ConfigurationSetting]
        public string ExtractRunArguments { get; set; }
        [ConfigurationSetting]
        public string DatabasePollInterval { get; set; }
        [ConfigurationSetting]
        public string SiteKey { get; set; }
        [ConfigurationSetting]
        public string Entity { get; set; }
        [ConfigurationSetting]
        public string Username { get; set; }
        [ConfigurationSetting]
        public string Password { get; set; }
        [ConfigurationSetting]
        public string LoggingDepth { get; set; }

        public int LogFileMaxSizeInt { get; set; }
        public int DatabasePollIntervalInt { get; set; }
        public LTAMessageImportance LoggingDepthEnum { get; set; }

        public ExtractProcessorConfiguration(IConfigurationProvider provider)
            : base(provider)
        {

        }

        public override void ValidateSettings()
        {
            // Log File Path
            if (string.IsNullOrWhiteSpace(LogFilePath))
                LogFilePath = Environment.CurrentDirectory;

            // Log File Max Size
            var sizeint = LogFileMaxSize.GetInteger();
            if (sizeint <= 0)
                throw new ConfigurationException("LogFileMaxSize");
            else
                LogFileMaxSizeInt = sizeint;

            // Exception Log File
            if (string.IsNullOrWhiteSpace(ExceptionLogFile))
                ExceptionLogFile = Environment.CurrentDirectory;

            // Extract Run Path
            if (string.IsNullOrWhiteSpace(ExtractRunPath)
                || !File.Exists(ExtractRunPath))
                throw new ConfigurationException("ExtractRunPath");

            // Database Poll Interval
            int tempint;
            if (string.IsNullOrWhiteSpace(DatabasePollInterval)
                || !int.TryParse(DatabasePollInterval, out tempint))
                throw new ConfigurationException("DatabasePollInterval");
            else
                DatabasePollIntervalInt = tempint;

            // SiteKey
            if (string.IsNullOrWhiteSpace(SiteKey))
                throw new ConfigurationException("SiteKey");

            // Entity
            string tempstring;
            var crypt = new cCrypto3DES();
            if (string.IsNullOrWhiteSpace(Entity))
                throw new ConfigurationException("Entity");
            if (!crypt.Decrypt(Entity, out tempstring))
                throw new ConfigurationException("Entity");

            // Username
            if (string.IsNullOrWhiteSpace(Username))
                throw new ConfigurationException("Username");
            if (!crypt.Decrypt(Username, out tempstring))
                throw new ConfigurationException("Username");

            // Password
            if (string.IsNullOrWhiteSpace(Password))
                throw new ConfigurationException("Password");
            if (!crypt.Decrypt(Password, out tempstring))
                throw new ConfigurationException("Password");

            // Logging Depth
            if (string.IsNullOrWhiteSpace(LoggingDepth)
                || !int.TryParse(LoggingDepth, out tempint))
                throw new ConfigurationException("LoggingDepth");
            if (!Enum.IsDefined(typeof(LTAMessageImportance), tempint))
                throw new ConfigurationException("LoggingDepth");
            LoggingDepthEnum = (LTAMessageImportance)Enum.Parse(typeof(LTAMessageImportance), LoggingDepth);
        }
    }
}
