﻿using System.Collections.Generic;
using System.Collections.Specialized;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Parker
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* CR 97760 DRP 05/09/2013
*   -Initial release version.
******************************************************************************/
namespace WFS.RecHub.Extract.ExtractProcessSvc
{
    class ServiceOptions : cSiteOptions
    {
        Dictionary<string, string> values = new Dictionary<string, string>();

        //public const string ConfigPollTime = "ConfigPollTime";
        public const string ExtractRunPath = "ExtractRunPath";
        public const string ExtractRunArguments = "ExtractRunArguments";

        public ServiceOptions(string siteKey) : base(siteKey)
        {
            StringCollection colSiteOptions = ipoINILib.GetINISection(siteKey);
            foreach (string value in colSiteOptions)
            {
                int i = value.IndexOf("=");
                if (i > -1)
                {
                    string key = value.Substring(0, i).Trim();
                    string setting = value.Substring(i + 1);
                    values.Add(key, setting);
                }
            }
        }
        public string this[string key] 
        {
            get {
            if (values.ContainsKey(key))
                return values[key];
            else
                return null;
            }
        }
    }
}
