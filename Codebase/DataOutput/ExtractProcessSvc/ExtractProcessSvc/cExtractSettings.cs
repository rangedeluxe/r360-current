﻿
using WFS.LTA.Common;
using System.Text;
using System.IO;
using System;
using System.Reflection;
using System.Configuration;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133363 HRN 03/25/2014 
*   - Initial Version  \
* WI 134616 DJW 3/31/2014
*  - Additional logging
******************************************************************************/
namespace WFS.RecHub.Extract.ExtractProcessSvc
{
    public sealed class cExtractSettings
    {
        #region Members

        public string LogFilePath { get; private set; }

        public int LogFileMaxSize { get; private set; }

        public LTAMessageImportance LoggingDepth { get; private set; }

        public string ExceptionLogFilePath { get; private set; }

        public string SiteKey { get; private set; }

        #endregion

        #region Constructors

        public cExtractSettings()
        {
            LoadSettings();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Load the Application setting
        /// </summary>
        private void LoadSettings()
        {
            LogFileMaxSize = cAppSettingsLib.Get<int>(ExtractAppSettings.LogFileMaxSize);
            LogFilePath = cAppSettingsLib.Get(ExtractAppSettings.LogFilePath);
            SiteKey = cAppSettingsLib.Get(ExtractAppSettings.SiteKey);
            ExceptionLogFilePath = cAppSettingsLib.Get(ExtractAppSettings.ExceptionLogFile);
            
        }

        /// <summary>
        /// Validate required setting values
        /// </summary>
        public bool ValidateSettings()
        {
            var userMessage = new StringBuilder();
            var log = new StringBuilder();
            var tempMsg = new StringBuilder();

            //verify the configuration file exists
            try
            {
                string configPath = cExtractSettings.AppExePath + ".config";
                if (!File.Exists(configPath))
                {
                    string line = "Missing application configuration file at " + cExtractSettings.AppExePath + ".config";
                    log.AppendLine(line);
                }
            }
            catch
            {
                string line = "Missing application configuration file at " + cExtractSettings.AppExePath + ".config";
                log.AppendLine(line);
            }

            if (!Path.IsPathRooted(this.LogFilePath))
                LogFilePath = Path.Combine(cExtractSettings.AppPath, LogFilePath);

            //verify it's a valid log path, and not just a directory
            if( (!cCommonLib.CheckDirectory(Path.GetDirectoryName(LogFilePath))) || ( Directory.Exists( LogFilePath ) ) )
            {
                log.AppendLine("[LogFilePath] - Path not found:");
                log.AppendLine(LogFilePath);
                LogFilePath = Path.Combine(AppPath, "{0:yyyyMMdd}_ExtractProcessSvcLog.txt");
                log.AppendLine("The following default will be used: ");
                log.AppendLine(LogFilePath);
                log.AppendLine();
            }


            if (!Path.IsPathRooted(this.ExceptionLogFilePath))
                ExceptionLogFilePath = Path.Combine(cExtractSettings.AppPath, ExceptionLogFilePath);

            if( (!cCommonLib.CheckDirectory(Path.GetDirectoryName(ExceptionLogFilePath))) || (Directory.Exists( ExceptionLogFilePath ) ) )
            {
                log.AppendLine("[ExceptionLogFilePath] - Path not found:");
                log.AppendLine(ExceptionLogFilePath);
                ExceptionLogFilePath = Path.Combine(AppPath, "{0:yyyyMMdd}_ExtractProcessSvcExceptions.txt");
                log.AppendLine("The following default will be used: ");
                log.AppendLine(LogFilePath);
                log.AppendLine();
            }

            if (LogFileMaxSize < 1024)
            {
                tempMsg.AppendLine("[LogFileMaxSize] - Invalid value: " + LogFileMaxSize.ToString());
                tempMsg.AppendLine("Setting max file size to 1 Megabyte by default.");
                LogFileMaxSize = 1024;
                tempMsg.AppendLine();
            }

            
            //Validate ExtractRun exists
            string strExtractRunPath = cAppSettingsLib.Get(ExtractAppSettings.ExtractRunPath);
            if (string.IsNullOrEmpty(strExtractRunPath))
            {
                tempMsg.AppendLine("[ExtractRunPath] - Value is blank: " + strExtractRunPath);
            }
            else 
            {
                if (!File.Exists(strExtractRunPath))
                {
                   tempMsg.AppendLine("[ExtractRunPath] - The path is not valid: " + strExtractRunPath);
                }
            }

            //Validate ExtractRunArguments
            string strExtractRunArguments=cAppSettingsLib.Get(ExtractAppSettings.ExtractRunArguments);
            if (string.IsNullOrEmpty(strExtractRunArguments))
            {
                tempMsg.AppendLine("[ExtractRunArguments] - Value is blank: " + strExtractRunArguments);
            }


            //Validate DatabasePollInterval
            string strDatabasePollInterval = cAppSettingsLib.Get(ExtractAppSettings.DatabasePollInterval);
            if (string.IsNullOrEmpty(strDatabasePollInterval))
            {
                tempMsg.AppendLine("[DatabasePollInterval] - Value is blank: " + strDatabasePollInterval);
            }
            else
            { 
                if (!cCommonLib.IsNumeric(strDatabasePollInterval))
                {
                    tempMsg.AppendLine("[DatabasePollInterval] - Value is non-numeric: " + strDatabasePollInterval);
                }
            }
         
            if (string.IsNullOrEmpty(SiteKey))
            {
                tempMsg.AppendLine("[SiteKey] - Value for SiteKey is missing: " + SiteKey);
                
            }

            if (tempMsg.Length > 0)
            {
                userMessage.Append(tempMsg.ToString());
                log.Append(tempMsg.ToString());
                tempMsg.Clear();
            }

            if (log.Length > 0)
                cCommonLogLib.LogMessageLTA(log.ToString(), LTAMessageImportance.Essential);

            if (userMessage.Length > 0)
            {
                Exception ex = new Exception("WARNING: Invalid/Missing Application Settings" + Environment.NewLine + Environment.NewLine + userMessage.ToString());
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }

            return (log.Length == 0 && userMessage.Length == 0);
        }


        /// <summary>
        /// Returns the path of the executing assembly including the 
        /// executable name.
        /// </summary>
        public static string AppExePath
        {
            get
            {
                var result = String.Empty;

                if (Assembly.GetEntryAssembly() == null)
                    result = Assembly.GetCallingAssembly().Location;
                else
                    result = Assembly.GetEntryAssembly().Location;
                return result;
            }
        }

        /// <summary>
        /// Returns the executing assembly's executable name without the path.
        /// </summary>
        public static string AppExeName
        {
            get
            {
                return Path.GetFileName(AppExePath);
            }
        }

        /// <summary>
        /// Returns the application's path without the executable name.
        /// </summary>
        public static string AppPath
        {
            get
            {
                return Path.GetDirectoryName(AppExePath);
            }
        }

        #endregion


    }

}