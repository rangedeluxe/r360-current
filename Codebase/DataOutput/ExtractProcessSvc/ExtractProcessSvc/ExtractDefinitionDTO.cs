﻿using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Parker
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* CR 97760 DRP 05/09/2013
*   - Initial release version.
* WI 138658 DJW 05/16/2014  
*   - Added support for Extract Billing 
* WI 159329 BLR 08/18/2014
*  - Added call to the Definition XML.
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc
{
    public class ExtractDefinitionDTO
    {
        public string ExtractDefinition { get; set; }
        public string ExtractName { get; set; }
        public int ExtractDefinitionSizeKb { get; set; }
        public Int64 ExtractDefinitionId { get; set; }
        public ExtractDefinitionType ExtractDefinitionType { get; set; }
    }

    public enum ExtractDefinitionType : int
    {
        Unknown = 0,
        Wizard = 1,
        Billing = 2
    }
}
