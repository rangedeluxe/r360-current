echo Started DmpPostBuildEvent processing.

REM =================================================================
REM Developer Customization Required!
REM Change SUBASSEMBLYLIST to match the list of non-Windows references 
REM for your assembly. You must use the complete path, and use a space 
REM between each assembly listed. Use the value NONE when there are no 
REM sub-assemblies to be merged.
REM =================================================================

REM Get parameters: $(TargetFileName) $(TargetName) $(TargetExt) $(ConfigurationName)
set ASSEMBLYFILE=%1
set ASSEMBLYNAME=%2
set ASSEBMLYTYPE=%3
set BUILDMODE=%4
echo Parameters: %ASSEMBLYFILE% %ASSEMBLYNAME% %ASSEBMLYTYPE% %BUILDMODE% 
                  
set SUBASSEMBLYLIST=.\ipoCrypto.dll ^
                    .\ipoLog.dll ^
                    .\ipoLib.dll ^
                    .\ipoDB.dll ^
                    .\ltaLog.dll ^
					.\DALBase.dll ^
					.\ExtractProcessSvcDal.dll ^
					.\BillingExtractsCommon.dll ^
	   				.\BillingExtractsServicesClient.dll ^
	   				.\R360Shared.dll ^
					.\WFS.RecHub.ApplicationBlocks.Common.dll

REM =================================================================
echo Sub-Assemblies: %SUBASSEMBLYLIST%

REM do not run in debug mode
if "%BUILDMODE%"=="Debug" goto Out

if "%ASSEBMLYTYPE%"==".exe" goto UsingEXE
if "%ASSEBMLYTYPE%"==".EXE" goto UsingEXE
if "%ASSEBMLYTYPE%"==".dll" goto UsingDLL
if "%ASSEBMLYTYPE%"==".DLL" goto UsingDLL

:UsingDLL
set ASSEMBLYEXT=dll
goto UsingExtDone

:UsingEXE
set ASSEMBLYEXT=exe
goto UsingExtDone

:UsingExtDone

echo Deleting previous build ilmerge result files.
if exist ILMergeResults.txt del ILMergeResults.txt

if "%SUBASSEMBLYLIST%"=="NONE" goto SkipILMerge
if "%SUBASSEMBLYLIST%"=="none" goto SkipILMerge

mkdir ILMERGE

echo Running ILMERGE to combine assemblies.
if "%BUILDMODE%"=="Debug" goto ILMergeDebug
if "%BUILDMODE%"=="Release" goto ILMergeRelease
:ILMergeRelease
start /wait C:\Tools\ILMerge_2.12.0803\ilmerge /targetplatform:"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5" /ndebug /internalize %ASSEMBLYNAME%.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:ILMERGE/%ASSEMBLYFILE% /log:ILMergeResults.txt
goto ILMergeDone

:ILMergeDebug
start /wait C:\Tools\ILMerge_2.12.0803\ilmerge /targetplatform:"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5" /ndebug /internalize %ASSEMBLYNAME%.%ASSEMBLYEXT% %SUBASSEMBLYLIST% /out:ILMERGE/%ASSEMBLYFILE% /log:ILMergeResults.txt
goto ILMergeDone


:ILMergeDone

:SkipILMerge

echo Running SN to sign the combined assembly.
if "%BUILDMODE%"=="Debug" goto SnDebug
if "%BUILDMODE%"=="Release" goto SnRelease

:::SnDebug
::start /wait C:\Tools\sn.exe -Vr %ASSEMBLYFILE%
::goto SnDone

:SnRelease
:start /wait C:\Tools\sn.exe -R %ASSEMBLYFILE% ..\..\..\..\..\..\SharedDependencies\keys\publicprivatekeypair.snk
goto SnDone


:SnDone

:Out
echo Completed DmpPostBuildEvent processing.
