﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish
* Date: 
*
* Purpose:  Holds all the common functionality used by the program
*
* Modification History
* WI 133363 HRN 03/25/2014 
******************************************************************************/
namespace WFS.RecHub.Extract.ExtractProcessSvc
{

    public class cCommonLib
    {  
        /// <summary>
        /// Returns the path of the executing assembly including the 
        /// executable name.
        /// </summary>
        public static string AppExePath
        {
            get
            {
                var result = String.Empty;

                if (Assembly.GetEntryAssembly() == null)
                    result = Assembly.GetCallingAssembly().Location;
                else
                    result = Assembly.GetEntryAssembly().Location;

                return result;
                //return System.Reflection.Assembly.GetCallingAssembly().Location;
            }
        }

        /// <summary>
        /// Returns the executing assembly's executable name without the path.
        /// </summary>
        public static string AppExeName
        {
            get
            {
                return Path.GetFileName(AppExePath);
            }
        }

        /// <summary>
        /// Returns the application's path without the executable name.
        /// </summary>
        public static string AppPath
        {
            get
            {
                return Path.GetDirectoryName(AppExePath);
            }
        }
      
        /// <summary>
        /// Checks if a directory exists and tries to create it if it doesn't.
        /// </summary>
        /// <param name="dir">Full directory path and name.</param>
        /// <summary>
        /// Determines if the directory listed under the file exists.  If not it creates the directory.
        /// </summary>
        public static bool CheckDirectory(string dir, bool hideExceptions = true)
        {
            try
            {
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }
            catch (System.IO.IOException ex)
            {
                if (!hideExceptions)
                    throw new Exception("Unable to create/locate directory: " + dir, ex);
            }

            return Directory.Exists(dir);
        }

        /// <summary>
        /// Check if the value is numeric
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNumeric(string value)
        {
            bool retvalue = true;
            foreach (Char c in value.ToCharArray())
            {
                retvalue = retvalue && Char.IsDigit(c);
            }
            return retvalue;
        }
    }
}
