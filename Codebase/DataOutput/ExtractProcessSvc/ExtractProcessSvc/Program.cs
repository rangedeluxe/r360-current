﻿using System;
using System.Reflection;
using System.ServiceProcess;
using System.Configuration.Install;
using System.Security.Principal;
using WFS.RecHub.Extract.ExtractProcessSvc.Configuration;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Parker
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* CR 97760 DRP 05/09/2013
*   -Initial release version.
 *   
* CR 134515 DJW 3/28/2014
* Added Self Installing Service Code
*  
* CR 134621 DJW 3/31/2014
* Change service name to be: “WFSExtractProcessSvc”
*
* CR 133363  DJW 4/1/2014
* Minor logic bug when run from a command line.
* 
* WI 158147 BLR 08/12/2014
*  - Moved configuration settings. 
******************************************************************************/
namespace WFS.RecHub.Extract.ExtractProcessSvc
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        static void Main(string[] args)
        {
            var settings = new ExtractProcessorConfiguration(new ConfigurationManagerProvider());
            cCommonLogLib.Settings = settings;
            var success = true;
            try
            {
                settings.ValidateSettings();
            }
            catch(Exception e)
            {
                cCommonLogLib.LogErrorLTA(e, LTA.Common.LTAMessageImportance.Essential);
                success = false;
            }

            // Could not validate, kick out early.
            if (!success)
            {
                if (Environment.UserInteractive)
                    Console.WriteLine("The application failed initial validation startup. Please check the log file.");
                return;
            }

            //Validation for Application Settings.

            if (Environment.UserInteractive)
            {

                string parameter = string.Concat(args);

                if( parameter != null )
                {
                    parameter = parameter.Trim().ToLower();
                    parameter = parameter.TrimStart(new char[] { '/','-'});
                }
                switch (parameter)
                {
                    case "i":
                    case "install":
                        try
                        {
                            if (IsAdministrator())
                            {
                                ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });

                            }
                            else
                            {
                                Console.WriteLine("In order to install the ExtractProcessSvc, you must run as administrator.");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                        return;

                    case "u":
                    case "uninstall":
                        try
                        {
                            if (IsAdministrator())
                            {
                                ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
                            }
                            else
                            {
                                Console.WriteLine("In order to uninstall the ExtractProcessSvc, you must run as administrator.");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                        return;

                    case "?":
                        Console.WriteLine(@"
                            /i
                            /install        Installs the ExtractProcessSvc

                            /u
                            /uninstall      Uninstalls the ExtractProcessSvc
                            ");
                        return;
                }



                ExtractProcessor ep = new ExtractProcessor(settings);
                Type ExtractProcessorType = typeof(ExtractProcessor);
                MethodInfo onStart = ExtractProcessorType.GetMethod("OnStart", BindingFlags.NonPublic | BindingFlags.Instance); //retrieve the OnStart method so it can be called from here
                MethodInfo onStop = ExtractProcessorType.GetMethod("OnStop", BindingFlags.NonPublic | BindingFlags.Instance); //retrieve the OnStop method so it can be called from here
                MethodInfo onPause = ExtractProcessorType.GetMethod("OnPause", BindingFlags.NonPublic | BindingFlags.Instance); //retrieve the OnStop method so it can be called from here
                MethodInfo onContinue = ExtractProcessorType.GetMethod("OnContinue", BindingFlags.NonPublic | BindingFlags.Instance); //retrieve the OnStop method so it can be called from here

                onStart.Invoke(ep, new object[] { null });  //call the OnStart method

                Console.WriteLine("Press 1 to pause, or any other key to exit.");
                bool pausing = false;
                while (true)
                {
                    ConsoleKeyInfo info = Console.ReadKey();
                    if( (!pausing) && (info.KeyChar == '1') )
                    {
                        Console.WriteLine();
                        Console.WriteLine("Pausing the application.");
                        onPause.Invoke(ep, null);
                        pausing = true;
                        Console.WriteLine("Press 2 to continue, or any other key to exit.");

                    }
                    else if( (pausing) && (info.KeyChar == '2') )
                    {
                        Console.WriteLine();
                        Console.WriteLine("Continuing. Press 1 to pause, or any other key to exit.");
                        onContinue.Invoke(ep, null);
                        pausing = false;
                    }
                    else
                        break;

                }
                onStop.Invoke(ep, null); //call the OnStart method

            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                    new ExtractProcessor(settings) 
                };
                ServiceBase.Run(ServicesToRun);
            }
        }

        internal static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
