﻿using System;
using System.Configuration;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133363 HRN 03/25/2014 
*   - Initial Version  
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc
{
    /// <summary>
    /// Used to reference App.Config appSettings keys.
    /// </summary>
    public enum ExtractAppSettings
    {
        LogFileMaxSize,
        LogFilePath,
        SiteKey,
        ExceptionLogFile,
        DatabasePollInterval,
        ExtractRunPath,
        ExtractRunArguments
      
    }

    /// <summary>
    /// Wraps the web.config appSettings. 
    /// Fail-safe way to retrieve appSettings by Enum or String key and provide default value (optional)
    /// appSettings Keys are stored in a enum for quick/easy reference.
    /// </summary>
    public static class cAppSettingsLib
    {
        /// <summary>
        /// retrieve appSettings by Enum key and provide default value (optional)
        /// </summary>
        public static string Get(ExtractAppSettings key, string defaultValue = "")
        {
            return Get(key.ToString(), defaultValue);
        }

        /// <summary>
        /// retrieve appSettings by String key and provide default value (optional)
        /// </summary>
        public static string Get(string key, string defaultValue = "")
        {
            return Get<string>(key, defaultValue);
        }

        /// <summary>
        /// Get a value cast from String to type 'T"
        /// </summary>
        /// <typeparam name="T">The desired return type.</typeparam>
        /// <returns></returns>
        public static T Get<T>(ExtractAppSettings key)
        {
            return Get<T>(key, default(T));
        }

        /// <summary>
        /// Get a value cast from String to type 'T" providing a default value.
        /// </summary>
        /// <typeparam name="T">The desired return type.</typeparam>
        public static T Get<T>(ExtractAppSettings key, T defaultValue)
        {
            return Get<T>(key.ToString(), defaultValue);
        }

        public static T Get<T>(string key, T defaultValue)
        {
            var result = defaultValue;
            key = key.Trim();

            try
            {
                if (!String.IsNullOrWhiteSpace(key))
                {
                    if (ConfigurationManager.AppSettings.AllKeys.Contains(key))
                    {
                        var s = ConfigurationManager.AppSettings[key];
                        if (!String.IsNullOrWhiteSpace(s))
                            result = (T)Convert.ChangeType(s, typeof(T));
                    }
                }
            }
            catch (Exception)
            {
                return default(T);
            }

            return result;
        }


    }
}