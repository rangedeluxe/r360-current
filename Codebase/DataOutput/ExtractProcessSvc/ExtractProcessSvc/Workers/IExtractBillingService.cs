﻿
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133365 HRN 03/25/2014 
*   - Initial Version 
* WI 138658 DJW 05/16/2014  
*   - Added support for Extract Billing 
* WI 138658 DJW 05/30/2014  
*   - Extended to add logging for Extract Billing
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc.Workers
{
    public interface IExtractBillingService
    {
        void ExecuteBillingDefinition(ExtractDefinitionDTO definition, ExtractScheduleDTO sched );
    }
}
