﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: DWANTA
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133365 DJW 03/25/2014 
*   - Initial Version 
* WI 138658 DJW 05/16/2014  
*   - Added support for Extract Billing 
* WI 138658 DJW 05/30/2014  
*   - Extended to add logging for Extract Billing
******************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common;
using WFS.RecHub.BillingExtracts.ServicesClient;

namespace WFS.RecHub.Extract.ExtractProcessSvc.Workers
{
    public class ExtractBillingService : IExtractBillingService
    {
        public void ExecuteBillingDefinition(ExtractDefinitionDTO definition, ExtractScheduleDTO sched)
        {
            try
            {
                cCommonLogLib.LogMessageLTA("Extract Processor, executing ExecuteBillingDefinition, running ExtractScheduleId:" + sched.ExtractScheduleId + ", " + sched.Description + ", Definition:" + definition.ExtractDefinitionId.ToString() + ", " + definition.ExtractName,  LTAMessageImportance.Essential);
                BillingExtractsManager mgr = new BillingExtractsManager();
                mgr.GenerateFile(definition.ExtractDefinition, sched.ExtractRunArguments);
            }
            catch (Exception ex)
            {
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
        }
    }
}
