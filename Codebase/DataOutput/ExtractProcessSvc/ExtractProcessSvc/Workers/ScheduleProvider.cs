﻿using System;
using System.Collections.Generic;
using System.Data;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.Extract.ExtractProcessSvc.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133362 HRN 03/25/2014 
*   - Initial Version  
* WI 135150 DJW 04/03/2014
*  - Use the new ExtractProcessSvcDal for DB Access
* WI 158147 BLR 08/12/2014
*  - Added configuration settings.
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc.Workers
{
    public class ScheduleProvider : IScheduleProvider
    {
        private readonly ExtractProcessorConfiguration _settings;
        public ScheduleProvider(ExtractProcessorConfiguration settings)
        {
            _settings = settings;
        }

        public IEnumerable<ExtractScheduleDTO> GetExtractSchedules()
        {
            string siteKey = _settings.SiteKey;
            DataTable schedule;
            cExtractProcessSvcDal extractDal;
            Dictionary<Int64, DateTime> lastRunList = new Dictionary<long, DateTime>();
            //save off the last run time so we can reload it.
            extractDal = new cExtractProcessSvcDal(siteKey);
            if (!extractDal.GetActiveExtractSchedule(out schedule))
            {
                cCommonLogLib.LogErrorLTA(new Exception("Extract Processor, fail - schedule failed to load."), LTAMessageImportance.Essential);
                return new List<ExtractScheduleDTO>();
            }

            return schedule.ToList<ExtractScheduleDTO>() as List<ExtractScheduleDTO>;

        }
    }
}
