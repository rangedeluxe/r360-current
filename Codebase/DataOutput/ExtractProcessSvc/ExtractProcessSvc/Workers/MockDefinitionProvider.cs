﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.LTA.Common;
using WFS.RecHub.DAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/24/2014
*
* Purpose: Just a simple mocked definition provider for testing.
*
* Modification History
* WI 158147 BLR 08/12/2014
*  - Initial. 
* WI 159329 BLR 08/18/2014
*  - Added call to the Definition XML.
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc.Workers
{
    public class MockDefinitionProvider : IDefinitionProvider
    {
        public IEnumerable<ExtractDefinitionDTO> GetExtractDefinitions(DateTime? changedAfter)
        {
            return new List<ExtractDefinitionDTO>()
            {
                new ExtractDefinitionDTO()
                {
                    ExtractDefinition = File.ReadAllText(@"C:\Workspace\2.01_Dev\Codebase\DataOutput\ExtractWizard\Test\Resources\BasicBankDefinition.xml"),
                    ExtractDefinitionId = 1,
                    ExtractDefinitionType = ExtractDefinitionType.Wizard,
                    ExtractName = "Sample Name"
                }
            };
        }

        public string GetExtractDefinitionXML(int id)
        {
            XmlDocument file;
            string name;
            var dal = new cExtractProcessSvcDal("IPOnline");
            if (!dal.GetExtractDefinitionFile(id, out name, out file))
            {
                cCommonLogLib.LogMessageLTA("Extract Processor, fail - definition file failed to load: " + id, LTAMessageImportance.Essential);
                throw new Exception("Failed to download XML file: " + id);
            }
            return file.OuterXml;
        }
    }
}
