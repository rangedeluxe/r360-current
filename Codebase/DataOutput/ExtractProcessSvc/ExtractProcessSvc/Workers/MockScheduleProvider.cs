﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/24/2014
*
* Purpose: Just a simple mocked schedule provider for testing.
*
* Modification History
* WI 158147 BLR 08/12/2014
*  - Initial. 
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc.Workers
{
    public class MockScheduleProvider : IScheduleProvider
    {

        public IEnumerable<ExtractScheduleDTO> GetExtractSchedules()
        {
            var tensecondsfromnow = DateTime.Now.AddSeconds(10);
            return new List<ExtractScheduleDTO>()
            {
                
                new ExtractScheduleDTO()
                {
                    DaysOfWeek = "1111111",
                    ScheduleType = ExtractScheduleType.Weekly,
                    ExtractDefinitionId = 1,
                    ExtractRunArguments = "/p:02/28/2006",
                    ScheduleTime = new TimeSpan(tensecondsfromnow.Hour, tensecondsfromnow.Minute, tensecondsfromnow.Second)
                }

            };
        }
    }
}
