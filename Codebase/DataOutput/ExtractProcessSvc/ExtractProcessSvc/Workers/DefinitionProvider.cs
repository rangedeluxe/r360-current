﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.Extract.ExtractProcessSvc.Configuration;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History
* WI 133362 HRN 03/25/2014 
*   - Initial Version 
* WI 135150 DJW 04/03/2014
*  - Use the new ExtractProcessSvcDal for DB Access
* WI 158147 BLR 08/12/2014
*  - Added configuration settings. 
* WI 159329 BLR 08/18/2014
*  - Added call to the Definition XML. 
* WI 162821 BLR 09/03/2014
*  - Removed parsing of Definition XML from the GetAll() call. 
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc.Workers
{
    public class DefinitionProvider: IDefinitionProvider
    {
        private readonly ExtractProcessorConfiguration _settings;

        public DefinitionProvider(ExtractProcessorConfiguration settings)
        {
            _settings = settings;
        }

        public IEnumerable<ExtractDefinitionDTO> GetExtractDefinitions(DateTime? changedAfter)
        {
            DataTable dtExtractDefinition;
            cExtractProcessSvcDal objExtractDal;
            objExtractDal = new cExtractProcessSvcDal(_settings.SiteKey);
            if (!objExtractDal.GetActiveExtractDefinitions(changedAfter, out dtExtractDefinition))
            {
                cCommonLogLib.LogErrorLTA(new Exception("Extract Processor, fail - definitions failed to load."), LTAMessageImportance.Essential);
                return new List<ExtractDefinitionDTO>();
            }

            var list = new List<ExtractDefinitionDTO>();
            foreach (DataRow row in dtExtractDefinition.Rows)
            {
                list.Add(new ExtractDefinitionDTO()
                {
                    ExtractDefinitionId = long.Parse(row["ExtractDefinitionId"].ToString()),
                    ExtractDefinitionSizeKb = int.Parse(row["ExtractDefinitionSizeKb"].ToString()),
                    ExtractName = row["ExtractName"].ToString(),
                    ExtractDefinitionType = (ExtractDefinitionType)Enum.Parse(typeof(ExtractDefinitionType), row["ExtractDefinitionType"].ToString())
                });
            }
            return list;
        }

        public string GetExtractDefinitionXML(int id)
        {
            XmlDocument file;
            string name;
            var dal = new cExtractProcessSvcDal(_settings.SiteKey);
            if (!dal.GetExtractDefinitionFile(id, out name, out file))
            {
                cCommonLogLib.LogErrorLTA(new Exception("Extract Processor, fail - definition file failed to load: " + id), LTAMessageImportance.Essential);
                return String.Empty;
            }
            return file.OuterXml;
        }
    }
}
