﻿using System;
using System.Collections.Generic;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133362 HRN 03/25/2014 
*   - Initial Version  
* WI 138658 DJW 05/16/2014  
*   - Added support for Extract Billing 
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc.Workers
{
    public class TimeScheduleFilter: IScheduleFilter
    {
        public IEnumerable<ExtractScheduleDTO> FilterSchedules(IEnumerable<ExtractScheduleDTO> schedules)        
        {
            int intervalSeconds =ExtractProcessor.INTERVAL/1000; 
            return schedules.Where(sched =>
            {
                //Check 1st by week or by day, so we drop out of the loop earlier
                if (sched.ScheduleType == ExtractScheduleType.Weekly)
                {
                    //check to see if we are on the correct day
                    // DaysOfWeek[] is in format "0000000", with 1's on dates the extract is to run.
                    int today = (int)DateTime.Now.DayOfWeek;
                    if (sched.DaysOfWeek[today] != '1')
                        return false;
                }
                else if (sched.ScheduleType == ExtractScheduleType.Monthly)
                {
                    #region handle the monthly schedule
                    int dayInMonth = sched.DayInMonth;
                   
                    if (dayInMonth == 32)
                    {
                        //check to see if we are on the last day of the month
                        if (!IsEndOfMonth())
                            return false;
                    }
                    else
                    {
                        //check to see if we are on the day of the month to run the schedule
                        if (DateTime.Now.Day != dayInMonth)
                            return false;
                    }
                    #endregion
                }
                else
                {
                    //unknown schedule type
                    return false;
                }

                if (sched.ScheduleTime < DateTime.Now.TimeOfDay )
                {
                    double totalSeconds = (DateTime.Now.TimeOfDay - sched.ScheduleTime).TotalSeconds;
                    if (totalSeconds <= intervalSeconds)
                    {
                            return true;
                    }
                }
                return false;
            });
        }

        private bool IsEndOfMonth()
        {
            //if tomorrow is the first, we are on the last day of the month.
            return DateTime.Now.AddDays(1).Day == 1;
        }
    }
}