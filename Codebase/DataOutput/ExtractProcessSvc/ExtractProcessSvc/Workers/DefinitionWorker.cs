﻿using System;
using System.Diagnostics;
using System.IO;
using WFS.LTA.Common;
using WFS.RecHub.BillingExtracts.ServicesClient;
using WFS.RecHub.Extract.ExtractProcessSvc.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133362 HRN 03/25/2014 
*   - Initial Version  
* WI 138658 DJW 05/16/2014  
*   - Added support for Extract Billing 
* WI 143205 DJW 05/20/2014  
*   - Modified an if statement to make sure "else if" is caught
* WI 138658 DJW 05/30/2014  
*   - Extended to add logging for Extract Billing
* WI 158147 BLR 08/12/2014
*  - Added configuration settings, added Entity, Username, Password to ER call.
* WI 158657 BLR 08/14/2014  
*  - Added a space in the arguments.  
* WI 159329 BLR 08/18/2014
*  - Added call to the Definition XML.
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc.Workers
{
    public class DefinitionWorker:IDefinitionWorker
    {
        private readonly ExtractProcessorConfiguration _settings;
        private readonly IDefinitionProvider _provider;
        public DefinitionWorker(ExtractProcessorConfiguration settings, IDefinitionProvider provider)
        {
            _settings = settings;
            _provider = provider;
        }

        public void ExecuteDefinition(ExtractDefinitionDTO definition, ExtractScheduleDTO sched)
        {
            if (definition.ExtractDefinitionType == ExtractDefinitionType.Billing)
            {
                ExtractBillingService svc = new ExtractBillingService();
                svc.ExecuteBillingDefinition(definition,sched);
            }
            else if (definition.ExtractDefinitionType == ExtractDefinitionType.Wizard)
            {
                ExecuteWizardDefinition(definition, sched);
            }
            else
            {
                string logMessage = "An unknown definition type of [" + definition.ExtractDefinitionType.ToString() + "] was found. The definition ignored, and was not executed.";
                cCommonLogLib.LogMessageLTA(logMessage, LTAMessageImportance.Essential);
            }
        }

        private void ExecuteWizardDefinition(ExtractDefinitionDTO definition, ExtractScheduleDTO sched)
        {
            string tempFileName = Path.GetTempFileName() + ".xml";
            try
            {
                var file = _provider.GetExtractDefinitionXML((int)definition.ExtractDefinitionId);

                Process extract = new Process();
                ProcessStartInfo info = new ProcessStartInfo();
                File.WriteAllText(tempFileName, file);
                info.FileName = _settings.ExtractRunPath;
                info.Arguments = _settings.ExtractRunArguments;
                info.Arguments = info.Arguments.Replace("%1", tempFileName);
                info.Arguments += " " + ParseDates(sched.ExtractRunArguments);
                info.Arguments += string.Format(" /logon:{0} /password:{1} /entity:{2}", _settings.Username, _settings.Password, _settings.Entity);
                cCommonLogLib.LogMessageLTA("Extract Processor, running ExtractScheduleId:" + sched.ExtractScheduleId + ", " + sched.Description + ", arguments=\"" + info.Arguments + "\"", LTAMessageImportance.Essential);
                extract.StartInfo = info;
                info.UseShellExecute = false;
                extract.Start();
                extract.WaitForExit();
                sched.LastTimeRun = DateTime.Now;
            }
            catch (Exception ex)
            {
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            finally
            {
                if (File.Exists(tempFileName))
                    File.Delete(tempFileName);

            }
        }

        public string ParseDates(string p)
        {
            int place = p.IndexOf("{today");
            while (place > -1)
            {
                int last = p.IndexOf("}", place);
                string rest = p.Substring(place + 6, last - place - 6);
                string before = p.Substring(0, place);
                string after = p.Substring(last + 1);
                string replacement;
                if (rest.Length > 0)
                {
                    string unit = rest.Substring(rest.Length - 1, 1).ToLower();
                    int span;
                    if (!int.TryParse(rest.Substring(0, rest.Length - 1), out span))
                        throw new Exception("Invalid date offset:" + rest);
                    if (unit == "d")
                        replacement = (DateTime.Now + new TimeSpan(span, 0, 0, 0)).ToShortDateString();
                    else
                        replacement = (DateTime.Now + new TimeSpan(span * 7, 0, 0, 0)).ToShortDateString();
                }
                else
                {
                    replacement = DateTime.Now.ToShortDateString();
                }
                p = before + replacement + after;

                place = p.IndexOf("{today");
            }
            return p;
        }
    }
}
