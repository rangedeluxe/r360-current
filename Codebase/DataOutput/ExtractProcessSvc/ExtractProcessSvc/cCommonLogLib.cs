﻿using System;
using System.Diagnostics;
using WFS.LTA.Common;
using WFS.RecHub.Extract.ExtractProcessSvc.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Harish Ningireddy
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* WI 133363 HRN 03/25/2014 
*   - Initial Version 
* WI 158147 BLR 08/12/2014
*  - Moved configuration settings.
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc
{
    /// <summary>
    /// Class provides 'factory' methods for easily creating log variables.
    /// </summary>
    public static class cCommonLogLib
    {
        public static ExtractProcessorConfiguration Settings { get; set; }

         ///<summary>
         ///Returns a new ltaLog with the settings from the App.Config file.
         ///</summary>
         ///<param name="importance"></param>
         ///<returns></returns>
        public static ltaLog CreateLTALogMessage()
        {
            return new ltaLog(Settings.LogFilePath,
                              Settings.LogFileMaxSizeInt,
                              Settings.LoggingDepthEnum);
        }

        /// <summary>
        /// Returns a new ltaLog with the settings from the App.Config file.
        /// </summary>
        /// <param name="importance"></param>
        /// <returns></returns>
        public static ltaLog CreateLTALogError()
        {
            return new ltaLog(Settings.ExceptionLogFile,
                              Settings.LogFileMaxSizeInt,
                              Settings.LoggingDepthEnum);
        }

        /// <summary>
        /// Logs an LTA message with the settings from the App.Config file.
        /// Also includes information about the calling stack.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="important"></param>
        public static void LogMessageLTA(string message, LTAMessageImportance importance)
        {
            var log = CreateLTALogMessage();
            var previouscall = new StackTrace()
                .GetFrame(1);
            // Build source string from the call stack.
            var source = string.Format("{0}:{1}",
                previouscall.GetMethod().DeclaringType.Name,
                previouscall.GetMethod().Name);
            log.logEvent(message, source, importance);
        }

        /// </summary>
        /// <param name="ex"></param>
        /// <param name="importance"></param>
        public static void LogErrorLTA(Exception ex, LTAMessageImportance importance)
        {
            LogErrorLTA(ex, String.Empty, importance);
        }

        /// <summary>
        /// Write an Exception's contents to the LTA log.
        /// Also provides ability to append a custom (friendly) message.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="customMessage"></param>
        /// <param name="importance"></param>
        public static void LogErrorLTA(Exception ex, string customMessage, LTAMessageImportance importance)
        {
            if (ex != null)
            {
                var log = CreateLTALogError();
                var msg = String.Empty;
                if (!String.IsNullOrWhiteSpace(customMessage))
                    msg = "CUSTOM MESSAGE: " + customMessage + Environment.NewLine + ex.ToString();
                else
                    msg = ex.ToString();
                msg = Environment.NewLine + new string('=', 80) + Environment.NewLine + msg + Environment.NewLine;
                log.logEvent(msg, ex.Source, importance);
            }
        }
    }
}
