﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceProcess;
using System.Timers;
using System.Threading;
using WFS.LTA.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.Extract.ExtractProcessSvc.Workers;
using WFS.RecHub.Extract.ExtractProcessSvc.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Parker
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* CR 97760 DRP 05/09/2013
*   -Initial release version.
* WI 103466 DRP 05/29/2013 
*   - Added ExtractRunArguments
* WI 133363 HRN 03/25/2014 
*   - Re factor the code for Unit Test. 
* WI 135150 DJW 04/03/2014
*  - Use the new ExtractProcessSvcDal for DB Access
* WI 134938 DJW 4/8/2014
*  - Multithread the executing code for processing the defintion.
* WI 138658 DJW 05/16/2014  
*   - Removed the stub for ExractBilling. Implemented in DefinitionWorker.cs
* WI 138658 DJW 05/16/2014  
*   - Added support for Extract Billing and Monthly support
* WI 158147 BLR 08/12/2014
*  - Moved configuration settings.
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public partial class ExtractProcessor : ServiceBase
    {
        // Constants
        public const int INTERVAL = 60000;

        // Dependency Services
        IDefinitionProvider definitionProvider;
        IScheduleProvider scheduleProvider;
        IScheduleFilter scheduleFilter;
        IExtractBillingService extractBillingService;

        // Private vars.
        
        cExtractProcessSvcDal extractDal;
        Dictionary<Int64, ExtractDefinitionDTO> ExtractDefinitions = null;
        List<ExtractScheduleDTO> ExtractSchedules = null;
        System.Timers.Timer minuteTimer = new System.Timers.Timer();
        DateTime? lastScheduleLoad = null;
        TimeSpan? scheduleCheckInterval = null;
        ExtractProcessorConfiguration settings;

        /// <summary>
        /// Resolves dependencies for the main service process.
        /// </summary>
        private void ResolveDependencies()
        {
            definitionProvider = new DefinitionProvider(settings);
            scheduleProvider = new ScheduleProvider(settings);
            //definitionProvider = new MockDefinitionProvider();
            //scheduleProvider = new MockScheduleProvider();
            scheduleFilter = new TimeScheduleFilter();
            extractBillingService = new ExtractBillingService();
        }

        public ExtractProcessor(ExtractProcessorConfiguration settings)
        {
            this.settings = settings;
            InitializeComponent();
            minuteTimer.Elapsed += minuteTimer_Elapsed;
            ResolveDependencies();
        }

        /// <summary>
        /// Perform most of the service initialization tasks.
        /// </summary>
        private void ServiceInit()
        {

            cCommonLogLib.LogMessageLTA("Extract Processor, Service object created.", LTAMessageImportance.Essential);
            int duration = settings.DatabasePollIntervalInt;
            if (duration == default(int))
                duration = 60;

            scheduleCheckInterval = new TimeSpan(0, duration, 0);
            ExtractSchedules = new List<ExtractScheduleDTO>();
            ExtractDefinitions = new Dictionary<long, ExtractDefinitionDTO>();
            lastScheduleLoad = null;
            extractDal = new cExtractProcessSvcDal(settings.SiteKey);

            // Initiate the schedule.
            RebuildSchedule();
        }

        /// <summary>
        /// Checks for extract work to start
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void minuteTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            minuteTimer.Stop();
            try
            {
                FilterSchedules();
                if (lastScheduleLoad + scheduleCheckInterval < DateTime.Now)
                    RebuildSchedule();
                
            }
            catch(Exception ex)
            {
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }

            minuteTimer.Start();
        }

        /// <summary>
        /// standard service start routine
        /// </summary>
        /// <param name="args">not used</param>
        protected override void OnStart(string[] args)
        {
            ServiceInit();
            cCommonLogLib.LogMessageLTA("Extract Processor, service OnStart", LTAMessageImportance.Essential);

            minuteTimer.Enabled = true;
            minuteTimer.Interval = INTERVAL;
            minuteTimer.Start();
        }

        /// <summary>
        /// the standard service stop routine
        /// </summary>
        protected override void OnStop()
        {
            cCommonLogLib.LogMessageLTA("Extract Processor, service OnStop", LTAMessageImportance.Essential);
            minuteTimer.Stop();
            lastScheduleLoad = null;
            ExtractSchedules = null;
            ExtractDefinitions = null;
        }

        /// <summary>
        /// the standard service pause routine
        /// </summary>
        protected override void OnPause()
        {
            base.OnPause();
            cCommonLogLib.LogMessageLTA("Extract Processor, service onPause", LTAMessageImportance.Essential);
            minuteTimer.Stop();
        }

        /// <summary>
        /// the standard service continue routine
        /// </summary>
        protected override void OnContinue()
        {
            base.OnContinue();
            cCommonLogLib.LogMessageLTA("Extract Processor, service onContinue", LTAMessageImportance.Essential);
            minuteTimer.Stop();
            BuildExtractDefinitions(lastScheduleLoad);
            BuildExtractSchedule();
            minuteTimer.Start();
        }

        /// <summary>
        /// Rebuild the schedule definitions and get new extract definitions.
        /// </summary>
        private void RebuildSchedule()
        {
            minuteTimer.Stop();
            try
            {
                cCommonLogLib.LogMessageLTA("Extract Service, timer event, reload definitions", LTAMessageImportance.Essential);
                BuildExtractDefinitions(lastScheduleLoad);
                lastScheduleLoad = DateTime.Now;
                BuildExtractSchedule();
            }
            catch (Exception ex)
            {
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            finally
            {
                minuteTimer.Start();
            }

        }

        /// <summary>
        /// get new extract definitions
        /// </summary>
        /// <param name="changedAfter"></param>
        private void BuildExtractDefinitions(DateTime? changedAfter)
        {
            var defs = definitionProvider.GetExtractDefinitions(changedAfter);

            foreach (ExtractDefinitionDTO newDef in defs)
            {
                cCommonLogLib.LogMessageLTA("definitions loaded: " + newDef.ExtractDefinitionId + ", " + newDef.ExtractName, LTAMessageImportance.Essential);
                ExtractDefinitions[newDef.ExtractDefinitionId] = newDef;
            }
        }

        /// <summary>
        /// build the extract schedule
        /// </summary>
        private void BuildExtractSchedule()
        {
            ExtractSchedules = scheduleProvider.GetExtractSchedules()
                .ToList();

            Dictionary<Int64, DateTime> lastRunList = new Dictionary<long, DateTime>();
            //save off the last run time so we can reload it.
            foreach (ExtractScheduleDTO sched in ExtractSchedules)
            {
                lastRunList.Add(sched.ExtractScheduleId, sched.LastTimeRun);
            }

            foreach (ExtractScheduleDTO sched in ExtractSchedules)
            {
                cCommonLogLib.LogMessageLTA("schedule loaded: " + sched.ExtractScheduleId + ", extract-" 
                    + sched.ExtractDefinitionId + ", " + sched.Description + ", arguments=\"" 
                    + sched.ExtractRunArguments + "\"", LTAMessageImportance.Essential);
                if (lastRunList.ContainsKey(sched.ExtractScheduleId))
                    sched.LastTimeRun = lastRunList[sched.ExtractScheduleId];
            }
            lastScheduleLoad = DateTime.Now;
        }

        /// <summary>
        /// start all extracts that where scheduled for the last minute.
        /// </summary>
        private void FilterSchedules()
        {
            List<ExtractScheduleDTO> current = scheduleFilter
                .FilterSchedules(ExtractSchedules)
                .ToList();
            foreach (ExtractScheduleDTO sched in current)
            {
                var def = ExtractDefinitions[sched.ExtractDefinitionId];
                StartExtract(sched, def);
            }
        }

        /// <summary>
        /// Start the extract process 
        /// </summary>
        /// <param name="sched"></param> 
        private void StartExtract(ExtractScheduleDTO sched, ExtractDefinitionDTO def)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(delegate
            {
                IDefinitionWorker definitionWorker = new DefinitionWorker(settings, definitionProvider);
                definitionWorker.ExecuteDefinition(def, sched);
            }, null);

        }
    }
}