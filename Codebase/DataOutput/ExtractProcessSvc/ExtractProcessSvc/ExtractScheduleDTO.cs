﻿using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Parker
* Date: 03/24/2014
*
* Purpose: 
*
* Modification History* 
* CR 97760 DRP 05/09/2013
*   -Initial release version.
* WI 103466 DRP 0529/2013 
*   - Added ExtractRunArguments
* WI 138658 DJW 05/16/2014  
*   - Added support for monthly schedules
******************************************************************************/

namespace WFS.RecHub.Extract.ExtractProcessSvc
{
    public class ExtractScheduleDTO
    {
        // these map to the DB table
        public Int64 ExtractScheduleId { get; set; }
        public Int64 ExtractDefinitionId { get; set; }
        public DateTime ModificationDate { get; set; }
        public string DaysOfWeek { get; set; }
        public TimeSpan ScheduleTime { get; set; }
        public string Description { get; set; }
        public string ExtractRunArguments { get; set; }

        public ExtractScheduleType ScheduleType { get; set; }
        public int DayInMonth {get;set;}
        // processing value
        public DateTime LastTimeRun;

        public ExtractScheduleDTO()
        {
            this.LastTimeRun = DateTime.Parse("1/1/1900");
        }

    }

    public enum ExtractScheduleType : int
    {
        Unknown = 0,
        Weekly = 1,
        Monthly = 2
    }
}
