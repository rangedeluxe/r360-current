﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 130642 DLD 02/24/2014 
*   -Replacing CommonLib.CRLF with Environment.NewLine
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 130364 DLD 03/05/2014
*   -Added a method that ensures 'User Id' in the DB connection string is encrypted (ipoCrypto)
* WI 135747 BLR 04/09/2014
*   - Added validation for Password in the connection string.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public class CommonLib {

        /// <summary></summary>
        //public const string CRLF = "\r\n";

        /// <summary></summary>
        public const string INIFILE_NAME = "ExtractWizard.ini";

        /// <summary></summary>
        public const string LOGO_INIFILE_NAME = "ExtractWizard.ini";

        /// <summary></summary>
        public const string INVALID_FILE_NAME_CHARS = @"\/:*?""<>|";

        /// <summary>
        /// Returns the path of the executing assembly including the 
        /// executable name.
        /// </summary>
        public static string AppExePath
        {
            get
            {
                var result = String.Empty;
                
                if (Assembly.GetEntryAssembly() == null)
                    result = Assembly.GetCallingAssembly().Location;
                else
                    result = Assembly.GetEntryAssembly().Location;
                
                return result;
                //return System.Reflection.Assembly.GetCallingAssembly().Location;
            }
        }

        /// <summary>
        /// Returns the executing assembly's executable name without the path.
        /// </summary>
        public static string AppExeName {
            get {
                return Path.GetFileName(AppExePath);
                //string appExePath = CommonLib.AppExePath;
                //int intPos = appExePath.LastIndexOf("\\");
                //return (appExePath.Substring(intPos + 1
                //                          , appExePath.Length - intPos - 1));
            }
        }

        /// <summary>
        /// Returns the application's path without the executable name.
        /// </summary>
        public static string AppPath
        {
            get
            {
                return Path.GetDirectoryName(AppExePath);
                //string appExePath = AppExePath;
                //string appExeName = CommonLib.AppExeName;
                //return appExePath.Substring(0
                //                          , appExePath.Length - appExeName.Length);
            }
        }

        /// <summary>
        /// Accepts a single character and returns a string with that  character 
        /// repeated n number of times.
        /// </summary>
        /// <param name="charValue"></param>
        /// <param name="timesRepeated"></param>
        /// <returns></returns>
        public static string repeatChar(char charValue
                                      , long timesRepeated) {
            string strRetVal = "";

            for(long i = 0;i < timesRepeated;++i) {
                strRetVal += charValue;
            }

            return strRetVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryParse(string s, out Guid result) {
            try {
                Guid gidTemp = new Guid(s);
                result = gidTemp;
                return (true);
            } catch(Exception ex) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                result = Guid.Empty;
                return (false);
            }
        }

        /// <summary>
        /// UserId is to be encrypted in the app.config. The base DAL does not expect UserId to be encrypted 
        /// so we have to decrypt it before using it in the DB connection string.
        /// </summary>
        /// <param name="encryptedConnectionString"></param>
        /// <remarks>WI 130807 DLD 02/26/2014</remarks>
        public static string DecryptUserIdInConnectionString(string encryptedConnectionString)
        {
            var decryptedUserId = String.Empty;
            var builder = new SqlConnectionStringBuilder(encryptedConnectionString);
            var userID = builder.UserID;

            var decrypt = new WFS.RecHub.Common.Crypto.cCrypto3DES();

            try
            {
                decrypt.Decrypt(userID, out decryptedUserId);
                var result = encryptedConnectionString.Replace("User Id=" + builder.UserID, "User Id=" + decryptedUserId);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("The 'UserId' specified in your database connection string does not appear to be in the correct format."
                    + Environment.NewLine + "NOTE: The UserId and Password values must be encrypted (Base64).", ex);
            }
        }

        /// <summary>
        /// WI 135747 : Add validation to the password in the connection string as well.
        /// Like DecryptUserIdInConnectionString, this attempts to decrypt the password for 
        /// validation purposes.  
        /// </summary>
        /// <param name="encryptedConnectionString"></param>
        /// <returns></returns>
        public static bool ValidatePasswordInConnectionString(string encryptedConnectionString)
        {
            try
            {
                var decryptedpass = String.Empty;
                var builder = new SqlConnectionStringBuilder(encryptedConnectionString);
                var pass = builder.Password;

                // Kick out early if we don't have a password. (go into the catch block).
                // Decrypt method will actually return 'true' if we pass it string.Empty.
                if (string.IsNullOrWhiteSpace(pass))
                    throw new InvalidDataException();

                var decrypt = new WFS.RecHub.Common.Crypto.cCrypto3DES();

                var result = decrypt.Decrypt(pass, out decryptedpass);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("The 'Password' specified in your database connection string does not appear to be in the correct format."
                    + Environment.NewLine + "NOTE: The UserId and Password values must be encrypted (Base64).", ex);
            }
        }

        /// <summary>
        /// Checks if a directory exists and tries to create it if it doesn't.
        /// </summary>
        /// <param name="dir">Full directory path and name.</param>
        /// <summary>
        /// Determines if the directory listed under the file exists.  If not it creates the directory.
        /// </summary>
        public static bool CheckDirectory(string dir, bool hideExceptions = true)
        {
            try
            {
                if (Directory == null)
                    Directory = new FileSystemDirectory();

                if (Directory.Exists(dir)) return true;
                
                Directory.CreateDirectory(dir);
            }
            catch (System.IO.IOException ex)
            {
                if (!hideExceptions)
                    throw new Exception("Unable to create/locate directory: " + dir, ex);
            }

            return Directory.Exists(dir);
        }

        public static IDirectory Directory { get; set; }
    }
}
