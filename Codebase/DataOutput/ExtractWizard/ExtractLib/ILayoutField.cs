﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 115856 BLR 06/24/2014
*   -Added IsDataEntry 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public interface ILayoutField {
        string LayoutName {
            get;
        }

        int DataType {
            get;
        }

        string DisplayName {
            get;
            set;
        }

        int ColumnWidth {
            get;
            set;
        }

        char PadChar {
            get;
            set;
        }

        JustifyEnum Justification {
            get;
            set;
        }

        string JustificationDesc {
            get;
        }

        bool Quotes {
            get;
            set;
        }

        bool UseOccursGroups {
            get;
        }

        int OccursCount {
            get;
        }

        string ManualInputValue {
            get;
        }

        string FieldName {
            get;
        }

        string SimpleFieldName {
            get;
        }

        string SimpleTableName {
            get;
        }

        string Format {
            get;
            set;
        }

        bool IsAggregate {
            get;
        }

        bool IsStatic {
            get;
        }

        bool IsFiller {
            get;
        }

        bool IsStandard {
            get;
        }

        bool IsImageField {
            get;
        }

        bool IsFirstLast {
            get;
        }

        bool IsCounter {
            get;
        }

        bool IsRecordCounter {
            get;
        }

        bool IsLineCounter {
            get;
        }

        bool IsPredefField {
            get;
        }

        bool IsDataEntry
        {
            get;
            set;
        }

        string FormatFiller();

        int SiteBankID { get; set; }
        int SiteClientAccountID { get; set; }
        string PaymentSource { get; set; }

        string UniqueFieldName {
            get; set; }
    }
}
