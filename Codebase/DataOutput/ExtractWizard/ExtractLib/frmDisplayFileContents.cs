using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using WFS.LTA.Common;

//using WFS.RecHub.DataOutputToolkit.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 113638 MLH 09/11/2013 
*      Form icons fixed, designer code broken out, Resources added to Project   
* WI 130809
*   -Unselect text in textbox.
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
********************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    /// <summary>
    /// 
    /// </summary>
    public partial class frmDisplayFileContents : Form {

       

        private PrinterSettings _Settings;
        private string _FileName = null;

        /// <summary>
        /// EventHandler to return exception information.
        /// </summary>
        public event OutputExtractErrorEventHandler OutputError;

        private void OnOutputError(object sender, System.Exception e) {
            if (OutputError != null)
            {
                OutputError(sender, e);
            }
            else
            {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filedescription"></param>
        /// <param name="filename"></param>
        /// <param name="settings"></param>
        /// <param name="icon"></param>
        public frmDisplayFileContents(string filedescription, string filename, PrinterSettings settings, Icon icon) {

            InitializeComponent();

            _Settings = settings;
            this.Icon = icon;
            
            LoadTextFile(filedescription, filename);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filedescription"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public bool LoadTextFile(string filedescription, string filename) {

            TextReader tr;

            this.Text = filedescription + " - " + filename;

            if(File.Exists(filename)) {

                tr = File.OpenText(filename);
                this.txtMain.Text = tr.ReadToEnd();
                tr.Close();
                tr.Dispose();

                _FileName = filename;

            } else {

                this.txtMain.Text = "File not found: \n\n" + filename;

                _FileName = null;
            }
            this.txtMain.Select(0, 0);
            return(true);
        }

        private void btnOk_Click(object sender, EventArgs e) {
            this.Close();
        }



       
        private void mnuFileClose_Click(object sender, EventArgs e) {
            this.Dispose();
        }

        private void mnuFilePrint_Click(object sender, EventArgs e) {
            
            PrintDialog dlg;
            DialogResult result;

            try {
                if(File.Exists(_FileName)) {

                    dlg = new PrintDialog();
                    dlg.PrinterSettings = _Settings;

                    result = dlg.ShowDialog();
                    if(result == DialogResult.OK) {
                        _Settings = dlg.PrinterSettings;
                        cPrinter.PrintTextFile(_FileName, _Settings);
                    }

                    dlg.Dispose();

                } else {
                    MessageBox.Show("Could not locate file: [" + _FileName + "].", 
                                    "Error", 
                                    MessageBoxButtons.OK, 
                                    MessageBoxIcon.Error);
                }

            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void mnuFilePrinterSetup_Click(object sender, EventArgs e) {

            PrintDialog dlg;
            DialogResult result;

            dlg = new PrintDialog();
            dlg.PrinterSettings = _Settings;

            result = dlg.ShowDialog();
            if(result == DialogResult.OK) {
                _Settings = dlg.PrinterSettings;
            }

            dlg.Dispose();
        }
    }
}