﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    public class ImageInfo
    {
        public FrameDimension Dimension { get; set; }
        public EncoderParameters EncoderParameters { get; set; }
        public ImageCodecInfo CodeInfo { get; set; }
        public string ImageFileName { get; set; }
    }
}
