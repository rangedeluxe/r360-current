﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    public static class HashGenerator
    {
        public static string SHA512(string input)
        {
            var hash = string.Empty;
            var data = Encoding.UTF8.GetBytes(input);
            using (var sha = new SHA512Managed())
            {
                var hashbytes = sha.ComputeHash(data);
                var sb = new StringBuilder();
                foreach (var b in hashbytes)
                    sb.AppendFormat("{0:x2}", b);
                hash = sb.ToString();
            }
            return hash;
        }
    }
}
