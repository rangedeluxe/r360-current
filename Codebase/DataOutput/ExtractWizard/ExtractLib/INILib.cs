using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Specialized;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    internal class Kernel32 {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [DllImport("kernel32")]
        public static extern long WritePrivateProfileString(string section, 
                                                            string key, 
                                                            string val, 
                                                            string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="def"></param>
        /// <param name="retVal"></param>
        /// <param name="size"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        [DllImport("kernel32")]
        public static extern int GetPrivateProfileString(string section, 
                                                         string key, 
                                                         string def, 
                                                         StringBuilder retVal, 
                                                         int size, 
                                                         string filePath);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lpAppName"></param>
        /// <param name="lpReturnedString"></param>
        /// <param name="nSize"></param>
        /// <param name="lpFileName"></param>
        /// <returns></returns>
        [ DllImport("kernel32")]
        public static extern int GetPrivateProfileSection(string lpAppName, 
                                                          byte[] lpReturnedString, 
                                                          int nSize, 
                                                          string lpFileName);

        [DllImport("kernel32.dll")]
        public static extern uint GetPrivateProfileSectionNames(IntPtr lpszReturnBuffer,
                                                         uint nSize, 
                                                         string lpFileName);

    }

    /// <summary>
    /// ipo ini library.
    /// </summary>
    public class INILib
    {

        /// <summary>
        /// Returns the path of the INI file that the executing assembly will use.
        /// </summary>
        private static string path
        {
            get{ 
                string strFileName;
                string strTemp;

                strTemp=System.IO.Path.Combine(CommonLib.AppPath, CommonLib.INIFILE_NAME);
                if(System.IO.File.Exists(strTemp)) {
                    strFileName=strTemp;
                } else {
                    strFileName=System.Configuration.ConfigurationManager.AppSettings["localIni"];
                }

                return(strFileName);
            }
        }

        /// <summary>
        /// Writes Data to an INI File.
        /// </summary>
        /// <param name="Section">Section to write value to.</param>
        /// <param name="Key">Key to be written to.</param>
        /// <param name="Value">Value to be written.</param>
        public static void IniWriteValue(string Section, 
                                         string Key, 
                                         string Value) {

            Kernel32.WritePrivateProfileString(Section, 
                                               Key, 
                                               Value, 
                                               path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IniFileName"></param>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        public static void IniWriteValue(string IniFileName,
                                         string Section, 
                                         string Key, 
                                         string Value) {

            Kernel32.WritePrivateProfileString(Section
                                    , Key
                                    , Value
                                    , IniFileName);
        }
        
        
        /// <summary>
        /// Reads Data Value From an Ini File.
        /// </summary>
        /// <param name="Section">Section where key is located.</param>
        /// <param name="Key">Key to be found.</param>
        /// <returns>Value underneath specified key and section.</returns>
        public static string IniReadValue(string Section, 
                                          string Key) {

            return(IniReadValue(path,
                                Section, 
                                Key));

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IniFileName"></param>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static string IniReadValue(string IniFileName,
                                          string Section, 
                                          string Key) {

            StringBuilder temp = new StringBuilder(255);

            int i = Kernel32.GetPrivateProfileString(Section, 
                                                     Key, 
                                                     "", 
                                                     temp, 
                                                     255, 
                                                     IniFileName);
            return temp.ToString();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static string IniReadString(string Section,
                                           string Key,
                                           string DefaultValue) {

            return IniReadString(path,
                                 Section,
                                 Key,
                                 DefaultValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IniFileName"></param>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static string IniReadString(string IniFileName,
                                           string Section,
                                           string Key,
                                           string DefaultValue) {

            string strTemp = IniReadValue(IniFileName, Section, Key);
            string strRetVal = DefaultValue;
            
            if(strTemp.Trim().Length == 0) {
                strRetVal = DefaultValue;
            } else {
                strRetVal = strTemp;
            }

            return(strRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static int IniReadInteger(string Section,
                                         string Key,
                                         int DefaultValue) {

            return IniReadInteger(path,
                                  Section,
                                  Key,
                                  DefaultValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IniFileName"></param>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static int IniReadInteger(string IniFileName,
                                         string Section,
                                         string Key,
                                         int DefaultValue) {

            string strTemp = IniReadValue(IniFileName, Section, Key);
            int intRetVal = DefaultValue;

            if(strTemp.Trim().Length == 0 || !int.TryParse(strTemp, out intRetVal)) {
                intRetVal = DefaultValue;
            } else {
                intRetVal = int.Parse(strTemp);
            }

            return(intRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static bool IniReadBool(string Section,
                                       string Key,
                                       bool DefaultValue) {

            return IniReadBool(path,
                               Section,
                               Key,
                               DefaultValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IniFileName"></param>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static bool IniReadBool(string IniFileName,
                                       string Section,
                                       string Key,
                                       bool DefaultValue) {

            string strTemp = IniReadValue(IniFileName, Section, Key);
            bool bolRetVal = DefaultValue;

            if (strTemp.Trim().Length == 0 || !bool.TryParse(strTemp, out bolRetVal)) {
                bolRetVal = DefaultValue;
            } else {
                bolRetVal = bool.Parse(strTemp);
            }
            
            return(bolRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] GetIniSections() {
            return(GetIniSections(path));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IniFileName"></param>
        /// <returns></returns>
        public static string[] GetIniSections(string IniFileName) {

            uint MAX_BUFFER = 32767;
            IntPtr pReturnedString = Marshal.AllocCoTaskMem((int)MAX_BUFFER);
            uint bytesReturned = Kernel32.GetPrivateProfileSectionNames(pReturnedString, MAX_BUFFER, IniFileName);

            if(bytesReturned == 0) {
                return null;
            }

            string local = Marshal.PtrToStringAnsi(pReturnedString, (int)bytesReturned).ToString();
            Marshal.FreeCoTaskMem(pReturnedString);

            // use of Substring below removes terminating null for split
            return local.Substring(0, local.Length - 1).Split('\0');
        }

        /// <summary>
        /// Retrieves a collection of all values from the given .ini section.
        /// </summary>
        /// <param name="Section">section to be returned.</param>
        /// <returns>Collection of section values.</returns>
        public static StringCollection GetINISection(string Section) { // kcf
            return(GetINISection(path, Section));
        }

        /// <summary>
        /// Retrieves a collection of all values from the given .ini section.
        /// </summary>
        /// <param name="IniFileName"></param>
        /// <param name="Section">section to be returned.</param>
        /// <returns>Collection of section values.</returns>
        public static StringCollection GetINISection(string IniFileName, 
                                                     string Section) { // kcf

            StringCollection items = new StringCollection();
            byte[] buffer = new byte[65536];
            int bufLen=0;
            bufLen = Kernel32.GetPrivateProfileSection(Section, buffer
                                            , buffer.GetUpperBound(0)
                                            , IniFileName); // kcf

            if (bufLen > 0) {
                StringBuilder sb = new StringBuilder();
                for(int i=0; i < bufLen; i++) {               

                    if (buffer[i] != 0) {
                        sb.Append((char) buffer[i]);
                    }
                    else {
                        if (sb.Length > 0) {
                            items.Add(sb.ToString());
                            sb = new StringBuilder();
                        }
                    }
                }
            }
            return items;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class cINIFileInterop {

        private string _IniFileName;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IniFileName"></param>
        public cINIFileInterop(string IniFileName) {
            _IniFileName = IniFileName;
            
            //if(!File.Exists(_IniFileName)) {
            //    throw(new Exception("Ini File does not exist or user does not have permission to access the file."));
            //}
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        public void IniWriteValue(string Section, 
                                  string Key, 
                                  string Value) {

            INILib.IniWriteValue(_IniFileName, Section, Key, Value);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public string IniReadValue(string Section, 
                                   string Key) {

            return(INILib.IniReadValue(_IniFileName, Section, Key));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public string IniReadString(string Section,
                                    string Key,
                                    string DefaultValue) {

            return(INILib.IniReadString(_IniFileName, Section, Key, DefaultValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public int IniReadInteger(string Section,
                                  string Key,
                                  int DefaultValue) {

            return(INILib.IniReadInteger(_IniFileName, Section, Key, DefaultValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="Key"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public bool IniReadBool(string Section,
                                string Key,
                                bool DefaultValue) {
     
            return(INILib.IniReadBool(_IniFileName, Section, Key, DefaultValue));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string[] GetIniSections() {

            return(INILib.GetIniSections(_IniFileName));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        public StringCollection GetINISection(string section) {

            return(INILib.GetINISection(_IniFileName, section));
        }
    }
}
