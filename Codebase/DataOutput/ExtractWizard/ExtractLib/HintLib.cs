using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;
using System.IO;
using System.Text;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 130165 DLD 02/19/2014
*   -Expanded Exception handling and message logging. 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    /// <summary>
    /// 
    /// </summary>
    public static class HintLib {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="HintDictionary"></param>
        /// <returns></returns>
        public static bool BuildHintDictionary(out Dictionary<string, StringDictionary> HintDictionary)
        {
            //TODO: WTF
            //WfsINI myHintINI;

            string[] HintSections;
            StringDictionary sdSection;
            StringCollection scSection;
            int iPos;
            bool bolRetVal;

            Dictionary<string, StringDictionary> dicRetVal = new Dictionary<string, StringDictionary>();

            HintSections = INILib.GetIniSections();

            foreach (string section in HintSections)
            {
                sdSection = new StringDictionary();

                scSection = INILib.GetINISection(section);

                foreach (string str in scSection)
                {
                    iPos = str.IndexOf('=');

                    if (iPos > 0 && iPos < str.Length - 1)
                    {
                        sdSection.Add(str.Substring(0, iPos), str.Substring(iPos + 1));
                    }
                }

                dicRetVal.Add(section, sdSection);
            }

            HintDictionary = dicRetVal;

            bolRetVal = true;

            return (bolRetVal);
        }
    }
}
