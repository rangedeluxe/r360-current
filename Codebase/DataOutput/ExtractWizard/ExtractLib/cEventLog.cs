﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 04/14/2014
*
* Purpose: 
*
* Modification History
* WI 134896 BLR 04/14/2014
*   - Initial Version 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    /// <summary>
    /// Contains data for calling the RecHubAlert.usp_EventLog_Ins method.
    /// </summary>
    public class cEventLog
    {
        public string EventName { get; set; }
        public int SiteCodeId { get; set; }
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public string Message { get; set; }
    }
}
