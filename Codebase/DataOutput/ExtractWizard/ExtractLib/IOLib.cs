using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 130364 DLD 02/21/2014
*   -Fixed Directory path handling in Open\Save dialogs
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{

    /// <summary>
    /// 
    /// </summary>
    public static class IOLib
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filter"></param>
        /// <param name="SelectedFilterIndex"></param>
        /// <param name="InitialFilePath"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static bool BrowseFile(string Filter, int SelectedFilterIndex, string InitialFilePath, out string FilePath)
        {
            return (BrowseFile(Filter, SelectedFilterIndex, InitialFilePath, false, out FilePath));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filter"></param>
        /// <param name="SelectedFilterIndex"></param>
        /// <param name="InitialFilePath"></param>
        /// <param name="RequireFileExists"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static bool BrowseFile(string Filter, int SelectedFilterIndex, string InitialFilePath, bool RequireFileExists, out string FilePath)
        {
            // DLD - WI 130364 wrapped in 'using' block and fixed path handling
            using (OpenFileDialog objFileDialog = new OpenFileDialog())
            {
                // Set dialog filter.
                objFileDialog.Filter = Filter;
                objFileDialog.InitialDirectory = SafeFolderPath(InitialFilePath);
                objFileDialog.FileName = String.Empty;
                objFileDialog.CheckFileExists = RequireFileExists;
                objFileDialog.FilterIndex = SelectedFilterIndex;
                DialogResult dr = objFileDialog.ShowDialog();

                if (dr == DialogResult.OK)
                {
                    FilePath = objFileDialog.FileName;
                    return (true);
                }
            }
            FilePath = string.Empty;
            return (false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filter"></param>
        /// <param name="SelectedFilterIndex"></param>
        /// <param name="InitialFilePath"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static bool BrowseFileSave(string Filter, int SelectedFilterIndex, string InitialFilePath, out string FilePath)
        {
            // DLD - WI 130364 wrapped in 'using' block and fixed path handling
            using (SaveFileDialog objSaveDialog = new SaveFileDialog())
            {
                // Set dialog filter.
                objSaveDialog.Filter = Filter;

                // Set initial filename and folder.
                objSaveDialog.InitialDirectory = SafeFolderPath(InitialFilePath);
                objSaveDialog.CheckFileExists = false;
                objSaveDialog.FilterIndex = SelectedFilterIndex;
                DialogResult dr = objSaveDialog.ShowDialog();

                if (dr == DialogResult.OK && !String.IsNullOrWhiteSpace(objSaveDialog.FileName))
                {
                    FilePath = objSaveDialog.FileName;
                    return (true);
                }
                else
                {
                    FilePath = string.Empty;
                    return (false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="InitialFolderPath"></param>
        /// <param name="FolderPath"></param>
        /// <returns></returns>
        public static bool BrowseFolder(string InitialFolderPath, out string FolderPath)
        {
            // DLD - WI 130364 wrapped in 'using' block and fixed path handling
            using (FolderBrowserDialog objFolderDialog = new FolderBrowserDialog())
            {
                objFolderDialog.SelectedPath = SafeFolderPath(InitialFolderPath);

                DialogResult dr = objFolderDialog.ShowDialog();

                if (dr == DialogResult.OK && Directory.Exists(objFolderDialog.SelectedPath))
                {
                    FolderPath = objFolderDialog.SelectedPath;
                    return (true);
                }
                else
                {
                    FolderPath = string.Empty;
                    return (false);
                }
            }
        }

        /// <summary>
        /// Ensures the path can be verified as an existing directory. 
        /// Returns a cleaned up version of the path (if applies). 
        /// Returns String.Empty if path passed cannot be verified.
        /// </summary>
        /// <param name="path">the assumed directory path</param>
        /// <param name="create">attempt to create the folder if it doesn't already exist</param>
        /// <returns>A valid path or String.Empty</returns>
        public static string SafeFolderPath(string path, bool create = false)
        {
            // DLD - WI 130364 - better path handling
            var result = string.Empty;

            if (!String.IsNullOrWhiteSpace(path))
            {
                var di = new DirectoryInfo(path);

                if (!di.Exists && create)
                    di.Create();

                if (di.Exists)
                    result = di.FullName;
            }
            return result;
        }
    }
}
