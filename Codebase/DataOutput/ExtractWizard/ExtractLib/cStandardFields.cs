using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version
* WI 114570 MLH 09/20/2013
*   - Removed CurrentProcessingDate from Standard Fields (for v2.0) 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public class cStandardFields {

        private DateTime _ExtractStarted = DateTime.Now;
        private DateTime _ExtractCompleted = DateTime.MinValue;
        //private DateTime _CurrentProcessingDate = DateTime.MinValue;

        public cStandardFields(/*DateTime vCurrentProcessingDate*/) {
            // WI 114570 removed for v2.0
            //_CurrentProcessingDate = vCurrentProcessingDate;
        }

        public DateTime ExtractStarted {
            get {
                return(_ExtractStarted);
            }
        }

        public DateTime ExtractCompleted {
            get {
                return(_ExtractCompleted);
            }
            set {
                _ExtractCompleted = value;
            }
        }

        // WI 114570 removed for v2.0
        ////public DateTime CurrentProcessingDate {
        ////    get {
        ////        return(_CurrentProcessingDate);
        ////    }
        ////}       
    }
}
