﻿using System;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Parker
* Date: 
*
* Purpose: form to change a user password.
*
* Modification History
* WI 106884-106887 DRP 7/10/2013
*   -Added check for "first time login" and added method to change the user's password.
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public delegate bool ChangePasswordDelegate(string oldPassword, string password, out string userMessage);
    public delegate void OkOnChangeClickDelegate(string oldPassword, string logonName, string password);

    public partial class frmChangePassword : Form {

        public event OkOnChangeClickDelegate OkOnClick;
        public event DialogCancelledDelegate DialogCancelled;

        private bool _requireOldPassword;

        public frmChangePassword(
            OkOnChangeClickDelegate okOnClick,
            DialogCancelledDelegate dialogCancelled,
            bool requireOldPassword)
        {
            
            InitializeComponent();

            if(okOnClick == null || dialogCancelled == null) {
                throw (new Exception("Delegates are required"));
            }

            _requireOldPassword = requireOldPassword;

            OkOnClick = okOnClick;
            DialogCancelled = dialogCancelled;
            if (!requireOldPassword)
            {
                txtOldPassword.Visible = false;
                lblOldPassword.Visible = false;
            }
        }

        private void btnOk_Click(object sender, EventArgs e) {

            if (_requireOldPassword && txtOldPassword.Text.Length == 0)
            {
                this.UserMessage = "Old password is required";
            }
            else
            {

                if (this.txtPassword.Text.Length > 0 && this.txtPasswordConfirm.Text.Length > 0)
                {
                    //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    //this.Close();
                    OkOnClick(
                        this.txtOldPassword.Text,
                        this.txtPassword.Text,
                        this.txtPasswordConfirm.Text);
                }
                else
                {
                    this.UserMessage = "Password and Password Confirm are required";
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            //this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            //DialogCancelled(sender, e);
            //this.Close();
            DialogCancelled();
        }

        public string Password {
            get {
                return (this.txtPassword.Text);
            }
            set
            {
                this.txtPassword.Text = value;
            }
        }

        public string PasswordConfirm {
            get {
                return (this.txtPasswordConfirm.Text);
            }
            set
            {
                this.txtPasswordConfirm.Text = value;
            }
        }
        public string OldPassword
        {
            get { return this.txtOldPassword.Text; }
            set { this.txtOldPassword.Text = value; }
        }

        public string UserMessage {
            get {
                return (this.txtUserMessage.Text);
            }
            set {
                this.txtUserMessage.Text = value;
            }
        }

        private void frmLogon_FormClosing(object sender, FormClosingEventArgs e) {
            if(e.CloseReason == CloseReason.UserClosing) {
                DialogCancelled();
            }
        }
    }

    public class cChangePasswordFormManager {

        private ChangePasswordDelegate _ChangePassword;
        private DialogCancelledDelegate _DialogCancelled;
        frmChangePassword _PasswordForm;

        public cChangePasswordFormManager(
            //System.Windows.Forms.Form owner,
            ChangePasswordDelegate changePasswordDelegate,
            DialogCancelledDelegate dialogCancelledDelegate,
            string initialPrompt,
            bool requireOldPassword)
        {

            _ChangePassword = changePasswordDelegate;
            _DialogCancelled = dialogCancelledDelegate;

            _PasswordForm = new frmChangePassword(this.OkOnClick, this.DialogCancelled, requireOldPassword);
            //DialogResult dr = 
            _PasswordForm.UserMessage = initialPrompt;
            _PasswordForm.ShowDialog();

            //if(dr == DialogResult.OK) {
            //    OkOnClick(_LogonForm.LogonName, _LogonForm.Password);
            //} else {
            //    DialogCancelled();
            //}
        }

        public void OkOnClick(
            string oldPassword,
            string passwordConfirm,
            string password) {

            string strUserMessage;

            if (password != passwordConfirm)
            {
                _PasswordForm.UserMessage = "Passwords entered are not the same. Please reenter.";
                _PasswordForm.Password = "";
                _PasswordForm.PasswordConfirm = "";
            }
            else
            {
                try
                {
                    _PasswordForm.Cursor = Cursors.WaitCursor;
                    if (_ChangePassword(
                        oldPassword,
                        password,
                        out strUserMessage))
                    {

                        _PasswordForm.Dispose();
                    }
                    else
                    {
                        _PasswordForm.OldPassword = "";
                        _PasswordForm.Password = "";
                        _PasswordForm.PasswordConfirm = "";
                        _PasswordForm.UserMessage = strUserMessage;
                    }
                }
                finally
                {
                    _PasswordForm.Cursor = Cursors.Default;
                }
            }
        }

        public void DialogCancelled() { //object sender, EventArgs e) {
            _DialogCancelled(); //sender, e);
            _PasswordForm.Dispose();
        }
    }
}
