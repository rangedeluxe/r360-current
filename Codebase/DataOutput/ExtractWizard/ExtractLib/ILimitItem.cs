﻿

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 162851 BLR 09/03/2014
*   - Exposed 2 properties in the interface, need these for the limit clauses. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public enum LogicalOperatorEnum {

        Equals,               // =
        NotEqualTo,           // <>
        GreaterThan,          // >
        GreaterThanOrEqualTo, // >=
        LessThan,             // <
        LessThanOrEqualTo,    // <=
        Between,              // between
        In,                   // in
        IsNull,               // is Null
        IsNotNull,            // is not Null
        Like                  // Like
    }

    public enum CompoundOperatorEnum {

        None,
        And,
        AndNot,
        Or,
        OrNot
    }

    public interface ILimitItem {
        
        string FieldName {
            get;
        }

        LogicalOperatorEnum LogicalOperator {
            get;
        }
        
        string LogicalOperatorDesc {
            get;
        }
        
        string Value {
            get;
        }
        
        int LeftParenCount {
            get;
        }
        
        int RightParenCount {
            get;
        }
        
        CompoundOperatorEnum CompoundOperator {
            get;
        }
        
        string CompoundOperatorDesc {
            get;
        }

        string Table { get; }
        string Schema { get; }
        string DisplayName { get; }
        string PaymentSource { get; set; }
        int DataType { get; set; }

    }

    public partial class LimitItemLib {

        public static string FormatLogicalOperator(LogicalOperatorEnum LogicalOperator)
        {
            return LogicalOperator.ToLogicalOperatorString();
        }

        public static string FormatCompoundOperator(CompoundOperatorEnum CompoundOperator) {

            string strRetVal = string.Empty;

            switch(CompoundOperator) {
                case CompoundOperatorEnum.And:
                    strRetVal = "And";
                    break;
                case CompoundOperatorEnum.AndNot:
                    strRetVal = "And Not";
                    break;
                case CompoundOperatorEnum.None:
                    strRetVal = string.Empty;
                    break;
                case CompoundOperatorEnum.Or:
                    strRetVal = "Or";
                    break;
                case CompoundOperatorEnum.OrNot:
                    strRetVal = "Or Not";
                    break;
            }

            return (strRetVal);
        }
    }
}
