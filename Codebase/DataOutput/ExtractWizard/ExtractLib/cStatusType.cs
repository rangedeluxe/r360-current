﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public class cStatusType {
        
        private int _StatusTypeID;
        private string _StatusType;
        private int _StatusValue;
        private string _StatusName;
        private string _StatusDisplayName;
        private bool _ResetToAllowed;

        /*public cStatusType(int vStatusTypeID,
                           string vStatusType,
                           int vStatusValue,
                           string vStatusName,
                           string vStatusDisplayName,
                           bool vResetToAllowed) {

            _StatusTypeID = vStatusTypeID;
            _StatusType = vStatusType;
            _StatusValue = vStatusValue;
            _StatusName = vStatusName;
            _StatusDisplayName = vStatusDisplayName;
            _ResetToAllowed = vResetToAllowed;
        }*/

        public cStatusType(DataRow dr) {

            _StatusTypeID = (int)dr["StatusTypeID"];
            _StatusType = dr["StatusType"].ToString();
            _StatusValue = (int)dr["StatusValue"];
            _StatusName = dr["StatusName"].ToString();
            _StatusDisplayName = dr["StatusDisplayName"].ToString();
            _ResetToAllowed = (bool)dr["ResetToAllowed"];
        }
        
        public int StatusTypeID {
            get {
                return(_StatusTypeID);
            }
        }

        public string StatusType {
            get {
                return(_StatusType);
            }
        }

        public int StatusValue {
            get {
                return(_StatusValue);
            }
        }

        public string StatusName {
            get {
                return(_StatusName);
            }
        }

        public string StatusDisplayName {
            get {
                return(_StatusDisplayName);
            }
        }

        public bool ResetToAllowed {
            get {
                return(_ResetToAllowed);
            }
        }

        public static List<cStatusType> LoadStatusTypes(DataTable dt) {
            
            List<cStatusType> objRetVal = new List<cStatusType>();
            foreach(DataRow dr in dt.Rows) {
                objRetVal.Add(new cStatusType(dr));
            }
            return(objRetVal);
        }

        public override string ToString() {
            return(StatusValue.ToString() + " - " + StatusDisplayName);
        }
    }
}
