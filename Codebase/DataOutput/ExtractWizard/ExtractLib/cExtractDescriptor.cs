﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public class cExtractDescriptor {

        private int _ExtractDescriptorKey;
        private string _ExtractDescriptor;

        public cExtractDescriptor(
            int extractDescriptorKey, 
            string extractDescriptor) {
            
            _ExtractDescriptorKey = extractDescriptorKey;
            _ExtractDescriptor = extractDescriptor;
        }

        public int ExtractDescriptorKey {
            get {
                return (_ExtractDescriptorKey);
            }
        }

        public string ExtractDescriptor {
            get {
                return (_ExtractDescriptor);
            }
        }

    }
}
