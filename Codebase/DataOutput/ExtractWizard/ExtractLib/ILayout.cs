﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {
    
    public interface ILayout {

        string LayoutName {
            get;
        }
        LayoutTypeEnum LayoutType {
            get;
        }
        string LayoutTypeDesc {
            get;
        }
        LayoutLevelEnum LayoutLevel {
            get;
        }
        string LayoutLevelDesc {
            get;
        }
        int MaxRepeats {
            get;
        }
        bool UseOccursGroups {
            get;
        }
        bool UsePaymentsOccursGroups {
            get;
        }
        int PaymentsOccursCount {
            get;
        }
        bool UseStubsOccursGroups {
            get;
        }
        
        int StubsOccursCount {
            get;
        }
        
        bool UseDocumentsOccursGroups {
            get;
        }
        
        int DocumentsOccursCount {
            get;
        }
        
        //IExtractDef ExtractDef {
        //    get;
        //}

        ILayoutField[] LayoutFields {
            get;
        }

        //void AddLayoutField(ILayoutField LayoutField);
        
        //void InsertLayoutField(int Index, ILayoutField LayoutField);
        
        void RemoveLayoutField(int Index);
        
        bool IsValidLayout(out string Msg);
    }
}
