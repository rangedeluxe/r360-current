﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    public static class Extensions
    {
        public static string ToLogicalOperatorString(this LogicalOperatorEnum logicalOperator)
        {
            if (logicalOperator == LogicalOperatorEnum.Equals) return "=";
            if (logicalOperator == LogicalOperatorEnum.NotEqualTo) return "<>";
            if (logicalOperator == LogicalOperatorEnum.GreaterThan) return ">";
            if (logicalOperator == LogicalOperatorEnum.GreaterThanOrEqualTo) return ">=";
            if (logicalOperator == LogicalOperatorEnum.LessThan) return "<";
            if (logicalOperator == LogicalOperatorEnum.LessThanOrEqualTo) return "<=";
            if (logicalOperator == LogicalOperatorEnum.Between) return " between";
            if (logicalOperator == LogicalOperatorEnum.In) return " in";
            if (logicalOperator == LogicalOperatorEnum.IsNull) return " IS NULL ";
            if (logicalOperator == LogicalOperatorEnum.IsNotNull) return " IS NOT NULL ";
            if (logicalOperator == LogicalOperatorEnum.Like) return " Like ";

            return string.Empty;

        }

        public static Image CreateNew(this Image sourceImage, Image destinationImage, ImageInfo imageInfo)
        {
            imageInfo.EncoderParameters.Param[1] = 
                new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, Convert.ToInt32(EncoderValue.MultiFrame));

            destinationImage = new Bitmap(sourceImage);
            destinationImage.Save(imageInfo.ImageFileName, imageInfo.CodeInfo, imageInfo.EncoderParameters);
            return destinationImage;
        }

        public static Image AddPage(this Image sourceImage, Image destinationImage, ImageInfo imageInfo)
        {
            imageInfo.EncoderParameters.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag,
                    Convert.ToInt32(EncoderValue.FrameDimensionPage));
            
            using (var img = new Bitmap(sourceImage))
            {
                destinationImage.SaveAdd(img, imageInfo.EncoderParameters);
            }

            return destinationImage;
                
        }

        public static EncoderParameter SetCompression(this Image image)
        {
            // if black and white, use CCITT4 compression, otherwise default to LZW compression
            if (image.PixelFormat == PixelFormat.Format1bppIndexed)
            {
                return new EncoderParameter(System.Drawing.Imaging.Encoder.Compression,
                    Convert.ToInt32(EncoderValue.CompressionCCITT4));
            }
            
            return new EncoderParameter(System.Drawing.Imaging.Encoder.Compression,
                    Convert.ToInt32(EncoderValue.CompressionLZW));
        }

        public static void SaveMultiPageTiff(this IList<Image> images, string destinationImageFileName, ImageCodecInfo imageCodeInfo)
        {
            Image destinationImage = null;
            EncoderParameters ep = new EncoderParameters(2);
            bool createNew = true;

            foreach (var img_src in images)
            {
                Guid guid = img_src.FrameDimensionsList[0];
                FrameDimension dimension = new FrameDimension(guid);
                var imageInfo = new ImageInfo
                {
                    EncoderParameters = ep,
                    ImageFileName = destinationImageFileName,
                    CodeInfo = imageCodeInfo,
                    Dimension = dimension
                };

                destinationImage = img_src.Append(destinationImage, imageInfo, createNew);
                createNew = false;
            }

            destinationImage.FlushImage(ep);
        }

        public static void FlushImage(this Image image, EncoderParameters encoderParameters)
        {
            encoderParameters.Param[1] = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag, Convert.ToInt32(EncoderValue.Flush));

            if (image != null)
                image.SaveAdd(encoderParameters);

            if (encoderParameters != null)
                encoderParameters.Dispose();

            if (image != null)
                image.Dispose();
        }

        public static Image Append(this Image sourceImage, Image destinationImage, ImageInfo imageInfo, bool createNew = false)
        {
            //get the frames from src image file
            for (int j = 0; j < sourceImage.GetFrameCount(imageInfo.Dimension); j++)
            {
                sourceImage.SelectActiveFrame(imageInfo.Dimension, j);
                
                imageInfo.EncoderParameters.Param[0] = sourceImage.SetCompression();
                if (createNew)
                {
                    //1st file, 1st frame, create the master image
                    destinationImage = sourceImage.CreateNew(destinationImage, imageInfo);
                    createNew = false;
                }
                else
                {
                    destinationImage = sourceImage.AddPage(destinationImage, imageInfo);
                }
            }

            return destinationImage;
        }
    }
}
