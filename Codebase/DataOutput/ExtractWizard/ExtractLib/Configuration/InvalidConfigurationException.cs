﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 08/12/2014
*
* Purpose: 
*
* Modification History
* WI 152321 BLR 08/15/2014
*   -Initial 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration
{
    public class InvalidConfigurationException : Exception
    {
        public InvalidConfigurationException(string message)
            : base("Invalid Configuration: " + message)
        {

        }

    }
}
