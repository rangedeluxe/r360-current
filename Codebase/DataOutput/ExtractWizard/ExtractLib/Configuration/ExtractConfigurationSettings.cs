﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.Common;
using System.Configuration;
using System.Collections.Specialized;
using System.Drawing;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 08/12/2014
*
* Purpose: 
*
* Modification History
* WI 152321 BLR 08/15/2014
*   - Initial 
* WI 159295 BLR 08/18/2014
*   - Fixed the file format indexes. 
* WI 167137 BLR 09/22/2014
*   - Added WCFConfigLocation for RAAM.
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration
{
    public class ExtractConfigurationSettings : BaseConfigurationSettings
    {
        [ConfigurationSetting]
        public string ConnectionString { get; set; }
        [ConfigurationSetting]
        public string SetupPath { get; set; }
        [ConfigurationSetting]
        public string LogFilePath { get; set; }
        [ConfigurationSetting]
        public string LogFileMaxSize { get; set; }
        [ConfigurationSetting]
        public string DefaultDefinitionFileFormat { get; set; }
        [ConfigurationSetting]
        public string CreateZeroLengthFile { get; set; }
        [ConfigurationSetting]
        public string LogoPath { get; set; }
        [ConfigurationSetting]
        public string BackgroundColor { get; set; }
        [ConfigurationSetting]
        public string ExceptionLogFile { get; set; }
        [ConfigurationSetting]
        public string LoggingDepth { get; set; }
        [ConfigurationSetting]
        public string SiteKey { get; set; }
        [ConfigurationSetting]
        public string ApplicationURI { get; set; }
        [ConfigurationSetting]
        public string WCFConfigLocation { get; set; }
        [ConfigurationSetting]
        public string LoginAttempts { get; set; }
        [ConfigurationSetting]
        public string XClientExtractFilePath { get; set; }
        [ConfigurationSetting]
        public string XClientExtractAttachmentPath { get; set; }

        public int DefaultDefinitionFileFormatIndex
        {
            get
            {
                return DefaultDefinitionFileFormat.ToLower() == "xml"
                    ? 2
                    : 1;
            }
        }
        public int LogFileMaxSizeInt { get; set; }
        public bool CreateZeroLengthFileBool { get; set; }
        public LTAMessageImportance LoggingDepthEnum { get; set; }
        public event outputErrorEventHandler OutputError;
        public event outputMessageEventHandler OutputMessage;

        private IDirectory directory;

        public ExtractConfigurationSettings(IConfigurationProvider provider)
            : this(provider, new FileSystemDirectory())
        {
            
        }

        public ExtractConfigurationSettings(IConfigurationProvider provider, IDirectory directoryContext)
            : base(provider)
        {
            directory = directoryContext;
        }

        private void OnExceptionOccurred(object sender, Exception e)
        {
            if (OutputError != null)
                OutputError(e);
            else
                cCommonLogLib.LogErrorLTA(e, LTA.Common.LTAMessageImportance.Essential);
        }

        private void OnOutputMessage(string msg, string src, MessageType messageType, MessageImportance messageImportance)
        {
            if (OutputMessage != null)
                OutputMessage(msg, src, messageType, messageImportance);
            else
                cCommonLogLib.LogMessageLTA(msg, (LTAMessageImportance)messageImportance);
        }

        // Carry-Over from the old version.
        private CustomConfigurationSection _customConfig = null;
        private CustomConfigurationSection CustomConfigurations
        {
            get
            {
                return _customConfig ?? (_customConfig = ConfigurationManager.GetSection(CustomConfigurationSection.MyName) as CustomConfigurationSection);
            }
        }
        public List<string> EntryColumnsToRemove
        {
            get
            {
                var cols = new List<string>();
                if (CustomConfigurations != null)
                {
                    foreach (ColumnToRemove col in CustomConfigurations.ColumnsToRemove)
                        cols.Add(col.Name);
                }
                return cols;
            }
        }
        private List<ConfirmationField> _extractStatsColumns = null;
        public List<ConfirmationField> ExtractStatsColumns
        {
            get
            {
                if (_extractStatsColumns == null)
                {
                    _extractStatsColumns = new List<ConfirmationField>();
                    if (CustomConfigurations != null)
                    {
                        foreach (ConfirmationField o in CustomConfigurations.ConfirmationFields)
                            _extractStatsColumns.Add(o);
                        _extractStatsColumns = _extractStatsColumns
                            .OrderBy(c => c.DisplayOrder)
                            .ToList();
                    }
                }
                return _extractStatsColumns;
            }
        }
        public string QualifyPath(string filePath)
        {
            string strParentPath;
            string strRetVal;

            if (filePath.StartsWith("."))
            {
                strParentPath = Directory.GetParent(SetupPath).FullName;
                strRetVal = strParentPath + (strParentPath.EndsWith(@"\") ? string.Empty : @"\") + filePath;
            }
            else
                strRetVal = filePath;

            return (strRetVal);
        }

        public override void ValidateSettings()
        {
            ValidateConnectionString();
            ValidateSetupPath();
            ValidateLogFile();
            ValidateDefaultDefinitionFile();
            ValidateZeroLengthFile();
            ValidateLogo();
            ValidateBackgroundColor();
            ValidateExceptionLogFile();
            ValidateLoggingDepth();
            ValidateLoginAttempts();
            ValidateSiteKey();
            ValidateApplicationUri();
        }

        internal void ValidateSiteKey()
        {
            // SiteKey
            if (string.IsNullOrWhiteSpace(SiteKey))
                throw new InvalidConfigurationException("SiteKey is Invalid.");
        }

        internal void ValidateApplicationUri()
        {
            // ApplicationURI
            if (string.IsNullOrWhiteSpace(ApplicationURI))
                throw new InvalidConfigurationException("ApplicationURI is Invalid.");
        }

        internal void ValidateZeroLengthFile()
        {
            // Create Zero Length File
            var zerofilebool = false;
            if (string.IsNullOrWhiteSpace(CreateZeroLengthFile))
                throw new InvalidConfigurationException("Create Zero Length File is Missing.");

            if (!bool.TryParse(CreateZeroLengthFile, out zerofilebool))
                throw new InvalidConfigurationException("Create Zero Length File is Invalid. Valid values: true, false.");

            CreateZeroLengthFileBool = zerofilebool;
        }

        internal void ValidateDefaultDefinitionFile()
        {
            // Default Definition File Format
            if (string.IsNullOrWhiteSpace(DefaultDefinitionFileFormat))
                throw new InvalidConfigurationException("Default Definition File Format is Missing. Valid values: xml, cxs.");

            if (!DefaultDefinitionFileFormat.Equals("xml", StringComparison.InvariantCultureIgnoreCase) && 
                !DefaultDefinitionFileFormat.Equals("cxs", StringComparison.InvariantCultureIgnoreCase))
                throw new InvalidConfigurationException("Default Definition File Format is Invalid. Valid values: xml, cxs.");
        }

        internal void ValidateLoginAttempts()
        {
            int tempint;
            // LoginAttemps
            if (string.IsNullOrWhiteSpace(LoginAttempts) || !int.TryParse(LoginAttempts, out tempint))
                throw new InvalidConfigurationException("LoginAttempts is Missing or Invalid.");
        }

        internal void ValidateLoggingDepth()
        {
            // Logging Depth
            int tempint;
            if (string.IsNullOrWhiteSpace(LoggingDepth) || !int.TryParse(LoggingDepth, out tempint))
                throw new InvalidConfigurationException("Logging Depth is Missing.");

            if (!Enum.IsDefined(typeof(LTAMessageImportance), tempint))
                throw new InvalidConfigurationException("Logging Depth is Invalid.");

            LoggingDepthEnum = (LTAMessageImportance)Enum.Parse(typeof(LTAMessageImportance), LoggingDepth);

        }

        internal void ValidateConnectionString()
        {
            // Connection String
            if (String.IsNullOrWhiteSpace(ConnectionString))
                throw new InvalidConfigurationException("Connection String is Missing.");
        }

        internal void ValidateExceptionLogFile()
        {
            // Exception Log File
            if (string.IsNullOrEmpty(ExceptionLogFile))
                ExceptionLogFile = Environment.CurrentDirectory;

            if (!Path.IsPathRooted(ExceptionLogFile))
                ExceptionLogFile = Path.Combine(CommonLib.AppPath, ExceptionLogFile);

            if (!CommonLib.CheckDirectory(Path.GetDirectoryName(ExceptionLogFile))
                || directory.Exists(ExceptionLogFile))
                throw new InvalidConfigurationException("Exception Log File Path is Invalid.");
        }

        internal void ValidateSetupPath()
        {
            // Setup Path
            if (string.IsNullOrWhiteSpace(SetupPath))
                SetupPath = Environment.CurrentDirectory;

            if (!Path.IsPathRooted(SetupPath))
                SetupPath = Path.Combine(CommonLib.AppPath, SetupPath);
            if (!CommonLib.CheckDirectory(SetupPath))
                throw new InvalidConfigurationException("Setup Path is Invalid.");
        }

        internal void ValidateLogFile()
        {
            // Log File Path
            if (string.IsNullOrWhiteSpace(LogFilePath))
                LogFilePath = Environment.CurrentDirectory;

            if (!Path.IsPathRooted(this.LogFilePath))
                LogFilePath = Path.Combine(CommonLib.AppPath, LogFilePath);

            if (!CommonLib.CheckDirectory(Path.GetDirectoryName(LogFilePath))
                || directory.Exists(LogFilePath))
                throw new InvalidConfigurationException("Log File Path is Invalid.");

            // Log File Max Size
            var sizeint = LogFileMaxSize.GetInteger();
            if (sizeint <= 0)
                throw new InvalidConfigurationException("Log File Max Size is Invalid.");
            else
                LogFileMaxSizeInt = sizeint;
        }

        internal void ValidateBackgroundColor()
        {
            // Background Color
            if (string.IsNullOrWhiteSpace(BackgroundColor))
                throw new InvalidConfigurationException("Background Color is Missing.");

            try
            {
                ColorTranslator.FromHtml(BackgroundColor);
            }
            catch
            {
                throw new InvalidConfigurationException("Background Color is Invalid.");
            }
        }

        internal void ValidateLogo()
        {
            // Logo Path
            if (!string.IsNullOrWhiteSpace(LogoPath) && !File.Exists(LogoPath))
                throw new InvalidConfigurationException("Logo Path is Invalid.");
        }
    }
}
