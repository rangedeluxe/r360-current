﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 111164 DLD 2/24/2014
*   - Initial Version
* WI 157574 BLR 08/08/2014
*   - Added Entity, as this is required for RAAM. 
* WI 159574 BLR 08/19/2014
*   - Validated Entity. 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public delegate bool LogonCredentialsProvidedDelegate(string entity, string logonName, string password, out string userMessage);
    public delegate void OkOnClickDelegate(string entity, string logonName, string password);
    public delegate void DialogCancelledDelegate(); //object sender, EventArgs e);

    public partial class frmLogon : Form {

        public event OkOnClickDelegate OkOnClick;
        public event DialogCancelledDelegate DialogCancelled;

        public frmLogon(
            OkOnClickDelegate okOnClick,
            DialogCancelledDelegate dialogCancelled) {
            
            InitializeComponent();

            if(okOnClick == null || dialogCancelled == null) {
                throw (new Exception("Delegates are required"));
            }

            OkOnClick = okOnClick;
            DialogCancelled = dialogCancelled;
        }

        private void btnOk_Click(object sender, EventArgs e) {

            if(this.txtLogonName.Text.Length > 0 && 
                this.txtPassword.Text.Length > 0 &&
                this.txtEntity.Text.Length > 0) {
                //this.DialogResult = System.Windows.Forms.DialogResult.OK;
                //this.Close();
                OkOnClick(
                    this.txtEntity.Text,
                    this.txtLogonName.Text,
                    this.txtPassword.Text);
            } else {
                this.UserMessage = "All Fields are Required";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            //this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            //DialogCancelled(sender, e);
            //this.Close();
            DialogCancelled();
        }

        public string LogonName {
            get {
                return (this.txtLogonName.Text);
            }
        }

        public string Password {
            get {
                return (this.txtPassword.Text);
            }
        }

        public string UserMessage {
            get {
                return (this.txtUserMessage.Text);
            }
            set {
                this.txtUserMessage.Text = value;
            }
        }

        private void frmLogon_FormClosing(object sender, FormClosingEventArgs e) {
            if(e.CloseReason == CloseReason.UserClosing) {
                DialogCancelled();
            }
        }

        private void frmLogon_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnOk_Click(sender, new EventArgs());
        }
    }

    public class cLogonFormManager {

        private LogonCredentialsProvidedDelegate _LogonCredentialsProvided;
        private DialogCancelledDelegate _DialogCancelled;
        frmLogon _LogonForm;

        public cLogonFormManager(
            //System.Windows.Forms.Form owner,
            LogonCredentialsProvidedDelegate logonCredentialsProvidedDelegate,
            DialogCancelledDelegate dialogCancelledDelegate) {

            _LogonCredentialsProvided = logonCredentialsProvidedDelegate;
            _DialogCancelled = dialogCancelledDelegate;

            _LogonForm = new frmLogon(this.OkOnClick, this.DialogCancelled);
            //DialogResult dr = 
            _LogonForm.ShowDialog();

            //if(dr == DialogResult.OK) {
            //    OkOnClick(_LogonForm.LogonName, _LogonForm.Password);
            //} else {
            //    DialogCancelled();
            //}
        }

        public void OkOnClick(
            string entity,
            string logonName,
            string password) {

            string strUserMessage;

            if(_LogonCredentialsProvided(
                entity,
                logonName,
                password,
                out strUserMessage)) {

                    _LogonForm.Dispose();
            } else {
                _LogonForm.UserMessage = strUserMessage;
            }
        }

        public void DialogCancelled() { //object sender, EventArgs e) {
            _DialogCancelled(); //sender, e);
            _LogonForm.Dispose();
        }
    }
}
