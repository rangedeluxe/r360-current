﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using WFS.RecHub.Common.Crypto;
using WFS.RecHub.OLFServicesClient;
using Wfs.Raam.Core.Services;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;
using Wfs.Raam.Core;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Derek Davison
* Date:     03/10/2014 
*
* Purpose:  A wrapper for the OLFServices functionality we need.
*
* Modification History
* WI 132206 DLD 03/10/2014
*   - Inital Creation. 
*   - Adding http header values (SiteKey and SessionID) to service call.
* WI 132206 DLD 03/10/2014
*   - XML documentation. 
* WI 132474 DLD 03/11/2014
*   - OLFServices Username and Password required to retrieve OLFService session token.
* WI 129719 BLR 06/09/2014
*   - Just a lot of clean-up work getting this actually communicating with OLF.
*   - SiteID was removed, as OLF doesn't actually use it at present.   
*   - Changed SiteKey to a String, should not have been an Int.   
* WI 138812 BLR 07/11/2014
*   - Removed OLF Username/Password in Preparation for RAAM.
* WI 157574 BLR 08/28/2014
*   - No longer using the service reference, now using the OLFServicesClient.
*******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    /// <summary>
    /// OLFServices wrapper.
    /// </summary>
    public static class cOLFServicesLib
    {

        #region Members

        /// <summary>
        /// Required by calls to webservice
        /// </summary>
        public static string SiteKey { get; set; }

        /// <summary>
        /// Required by calls to webservice
        /// </summary>
        public static string Username { get; set; }

        /// <summary>
        /// Required by calls to webservice
        /// </summary>
        public static string Password { get; set; }

        public static string Entity { get; set; }

        public static ExtractConfigurationSettings Settings { get; set; }

        #endregion

        #region Constructors

        static cOLFServicesLib()
        {
            Load();
        }

        #endregion

        #region Properties
        /// <summary>
        /// Checks that all required values are present and that webservice is Ping-able.
        /// </summary>
        public static bool ServiceIsAvailable
        {
            get
            {
                return Ping();
            }
        }

        private static OLFServiceClient _currentClient = null;
        /// <summary>
        /// A persisted instance of the <typeparamref name="OLFServiceClient"/>
        /// </summary>
        public static OLFServiceClient CurrentClient
        {
            get { return _currentClient ?? (_currentClient = GetClient()); }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Load static class values
        /// </summary>
        private static void Load()
        {
            SiteKey = cAppSettingsLib.Get(ExtractAppSettings.SiteKey);
        }

        /// <summary>
        /// Get an instance of the <typeparamref name="OLFServiceClient" with necessary http headers.
        /// </summary>
        public static OLFServiceClient GetClient()
        {
            var session = Guid.Empty;
            var svc = new OLFServiceClient(SiteKey);
            return svc;
        }

        /// <summary>
        /// Close and destroy the persisted <typeparamref name="OLFServiceClient" reference.
        /// </summary>
        public static void CloseCurrentClient()
        {
            
        }

        /// <summary>
        /// Ping OLFServices. Exceptions are caught and logged.
        /// </summary>
        public static bool Ping()
        {
            return Do(() =>
            {
                return !string.IsNullOrWhiteSpace(CurrentClient.Ping());
            });
        }

        public static bool GetImage(int bankid
                , int clientaccountid
                , int immutabledatekey
                , int depositDateKey
                , long batchid
                , int batchsequence
                , bool ispayment
                , int siteid
                , long sourcebatchid
                , string batchsourceshortname
                , string importtypeshortname
                , out byte[][] imgList
                , out string fileDescriptor
                , out string fileExtension)
        {
            byte[][] imagelist = null;
            string filedesc = null;
            string fileext = null;
            var success = Do(() =>
            {
                var result = CurrentClient.GetImage(bankid, clientaccountid, immutabledatekey, depositDateKey, batchid, 
                    batchsequence, ispayment, siteid, sourcebatchid, batchsourceshortname, importtypeshortname, out imagelist, out filedesc, out fileext);
                return result;
            });

            imgList = imagelist;
            fileDescriptor = filedesc;
            fileExtension = fileext;
            return success;
        }


        private static bool Do(Func<bool> operation)
        {
            // Check the R360Claims, if we don't have any, we'll need to attach them.
            if (!WSFederatedAuthentication.IsAuthenticated || WSFederatedAuthentication.ClaimsAccess.GetClaim(WfsClaimTypes.SessionId) == null)
            {
                var result = WSFederatedAuthentication.AuthenticateUsingRichClient(Settings.ApplicationURI, Entity, Username, Password, Settings.WCFConfigLocation);
                if (!result)
                {
                    cCommonLogLib.LogMessageLTA("Unable to Attach RAAM Claims", LTA.Common.LTAMessageImportance.Essential);
                    return false;
                }
            }

            try
            {
                return operation();
            }
            catch (Exception ex)
            {
                cCommonLogLib.LogErrorLTA(ex, LTA.Common.LTAMessageImportance.Essential);
            }
            return false;
        }

        #endregion
    }

    #region CustomEndpointBehavior to add required Http header values (SiteKey/SessionID)

    /// <summary>
    /// Custom endpoint behavior added to <typeparamref name="OLFServiceClient"/>. 
    /// Adds "SiteKey" and "SessionID" values to http header.
    /// </summary>
    public class cOLFServicesCustomEndpointBehavior : IEndpointBehavior
    {
        private string SiteKey = String.Empty;
        private Guid SessionID = Guid.Empty;

        public cOLFServicesCustomEndpointBehavior(string siteKey)
        {
            SiteKey = siteKey;
        }

        public cOLFServicesCustomEndpointBehavior(string siteKey, Guid sessionID)
        {
            SiteKey = siteKey;
            SessionID = sessionID;
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.ClientMessageInspectors.Add(new cOLFServicesClientMessageInspector(SiteKey, SessionID));
        }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters) { }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }

        public void Validate(ServiceEndpoint endpoint) { }
    }

    /// <summary>
    /// Custom Client Message Inspector.
    /// Being used 'BeforeSendRequest' to add authentication values SiteKey and SessinoID to Http header.
    /// </summary>
    public class cOLFServicesClientMessageInspector : IClientMessageInspector
    {
        private readonly string SITE_KEY = "SiteKey";
        private readonly string SESSION_ID = "SessionID";
        private string SiteKey = String.Empty;
        private Guid SessionID = Guid.Empty;

        public cOLFServicesClientMessageInspector(string siteKey)
        {
            SiteKey = siteKey;
        }

        public cOLFServicesClientMessageInspector(string siteKey, Guid sessionID)
        {
            SiteKey = siteKey;
            SessionID = sessionID;
        }

        /// <summary>
        /// Enables inspection or modification of a message before a request message is sent to a service.
        /// In this case we are adding "SiteKey" and "SessionID" to the Http header.
        /// </summary>
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            HttpRequestMessageProperty httpRequestMessage;
            object httpRequestMessageObject;

            if (request.Properties.TryGetValue(HttpRequestMessageProperty.Name, out httpRequestMessageObject))
            {
                httpRequestMessage = httpRequestMessageObject as HttpRequestMessageProperty;
                if (string.IsNullOrEmpty(httpRequestMessage.Headers[SITE_KEY]))
                {
                    if (SiteKey != String.Empty)
                        httpRequestMessage.Headers[SITE_KEY] = SiteKey;

                    if (SessionID != Guid.Empty)
                        httpRequestMessage.Headers[SESSION_ID] = SessionID.ToString();
                }
            }
            else
            {
                httpRequestMessage = new HttpRequestMessageProperty();

                if (SiteKey != String.Empty)
                    httpRequestMessage.Headers.Add(SITE_KEY, SiteKey);

                if (SessionID != Guid.Empty)
                    httpRequestMessage.Headers.Add(SESSION_ID, SessionID.ToString());

                request.Properties.Add(HttpRequestMessageProperty.Name, httpRequestMessage);
            }
            return null;
        }

        /// <summary>
        /// Enables inspection or modification of a message after a reply message is received but prior to passing it back to the client application.
        /// </summary>
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState) { }
    }

    #endregion

}