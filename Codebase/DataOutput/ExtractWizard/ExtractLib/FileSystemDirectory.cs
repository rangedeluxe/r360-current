﻿using System.IO;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    internal class FileSystemDirectory : IDirectory
    {
        public void CreateDirectory(string directory)
        {
            Directory.CreateDirectory(directory);
        }

        public bool Exists(string directory)
        {
            return Directory.Exists(directory);
        }
    }
}