﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {
    
    public interface IExtractParms {
        
        string SetupFile {
            get;
        }
        
        string TraceField {
            get;
        }
        
        DateTime[] RerunList {
            get;
        }
        
        string TargetFile {
            get;
        }
        
        string ImagePath {
            get;
        }
        
        long[] BankID {
            get;
        }
        
        long[] LockboxID {
            get;
        }
        
        long[] BatchID {
            get;
        }
        
        DateTime ProcessingDateFrom {
            get;
        }
        
        DateTime ProcessingDateTo {
            get;
        }
        
        DateTime DepositDateFrom {
            get;
        }
        
        DateTime DepositDateTo {
            get;
        }
        
        int UserID {
            get;
        }
        
        long ExtractSequenceNumber {
            get;
        }
        
        //long CallerHandle {
        //    get;
        //}
        
        //long CallerThreadID {
        //    get;
        //}
        
        Guid ExtractAuditID {
            get;
        }
        
        string ExtractFileName {
            get;
        }
        
        string LogFilePath {
            get;
        }

        string BankIDString {
            get;
        }

        string LockboxIDString {
            get;
        }

        string BatchIDString {
            get;
        }

        string ProcessingDateString {
            get;
        }

        int ProcessingDateFromKey {
            get;
        }

        int ProcessingDateToKey {
            get;
        }

        string DepositDateString {
            get;
        }

        int DepositDateFromKey {
            get;
        }

        int DepositDateToKey {
            get;
        }
    }
}
