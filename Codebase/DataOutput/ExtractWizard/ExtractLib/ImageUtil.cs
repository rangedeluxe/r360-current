using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using WFS.LTA.Common;
using WFS.RecHub.Common;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 129559 DLD 02/19/2014
*   -Capturing invalid file path exceptions.
* WI 129719 DLD 02/24/2014
*   -Converting image repository from ipoPics.dll to OLFServices 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 149794 BLR 06/24/2014
*   - Added in a check for a zero-length byte[] when saving images. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    /// <summary>
    /// 
    /// </summary>
    public static class ImageUtil {

        public static void CreateMultiPageTiff(List<Image> listSrcFiles, string DestFileName)
        {
            // DLD - WI 129719 - Image processing
            CheckImageDirectory(System.IO.Path.GetDirectoryName(DestFileName));
            //create a image instance from the 1st image
            // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
            var images = listSrcFiles.Where(x => x != null && x.Width > 0 && x.Height > 0);
            images.ToList().SaveMultiPageTiff(DestFileName, GetCodec("image/tiff"));
        }

        public static bool CheckImageDirectory(string directory)
        {
            return CommonLib.CheckDirectory(directory, hideExceptions: false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OriginalImage"></param>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        /// <returns></returns>
        public static Image ResizeImage(Image OriginalImage, int Width, int Height) {

            ImageCodecInfo info = null;
            Encoder encoder;
            EncoderParameters encoderparms;
            Bitmap objNewImage = null;

            info = GetCodec("image/tiff");

            encoder = Encoder.SaveFlag;
            encoderparms = new EncoderParameters(1);
            encoderparms.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.MultiFrame);

            objNewImage = new Bitmap(OriginalImage, Width, Height);

            return(objNewImage);
        } 

        private static ImageCodecInfo GetCodec(string MimeType) {

            ImageCodecInfo info = null;

            //get the codec for tiff files
            foreach(ImageCodecInfo ice in ImageCodecInfo.GetImageEncoders()) {
                if(ice.MimeType == MimeType) {
                    info = ice;
                }
            } //use the save encoder

            return(info);
        }

        /// <summary>
        /// Save an image to the provided path
        /// WI 149794 : Added a check for the byte[] length.
        /// </summary>
        /// <param name="image">The image as a byte array.</param>
        /// <param name="outputFile">The output file name and path. </param>
        /// <returns>The file's path and name with appropriate extention (if not provided)</returns>
        public static string SaveImage(byte[] image, string outputFile)
        {
            try
            {
                if (image != null &&
                image.Length > 0 &&
                !string.IsNullOrEmpty(outputFile))
                {
                    using (var s = new MemoryStream(image))
                    {
                        using (var b = new Bitmap(s))
                        {
                            if (b.RawFormat.Guid == ImageFormat.Tiff.Guid)
                            {
                                if (!outputFile.EndsWith(".tif", StringComparison.CurrentCultureIgnoreCase))
                                    outputFile += ".tif";
                                b.Save(outputFile, ImageFormat.Tiff);
                            }
                            else if (b.RawFormat.Guid == ImageFormat.Jpeg.Guid)
                            {
                                if (!outputFile.EndsWith(".jpg", StringComparison.CurrentCultureIgnoreCase))
                                    outputFile += ".jpg";
                                b.Save(outputFile, ImageFormat.Jpeg);
                            }
                            else
                                b.Save(outputFile);
                        }
                    }
                    return outputFile;
                }
                return string.Empty;
            }
            catch(Exception e)
            {
                return SavePdf(image, outputFile);
            }
        }

        public static string SavePdf(byte[] image, string outputFile)
        {
            try
            {
                return image.SavePdf(outputFile);
            }
            catch (Exception e)
            {
                var message = $"Failed to save image file {outputFile}";
                cCommonLogLib.LogMessageLTA(message, LTAMessageImportance.Essential);
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
                return string.Empty;

            }

        }
    }
}
