﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 132009 BLR 03/06/2014   
*   - Added the 'DisplayName', to allow better field labels as 'ColumnName'
*    is no longer unique.
* WI 115856 BLR 06/24/2014    
*   - Added setters for each property. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {
    public interface IColumn {
        string Schema { get; set; }
        string TableName { get; set; }
        string DisplayName { get; set; }
        string ColumnName { get; set; }
        SqlDbType Type { get; set; }
        int Length { get; set; }
        bool IsDataEntry { get; set; }
    }
}
