﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Text;
using System.Linq;

using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using System.Configuration;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 115356 Derek Davison 02/07/2014
*   - Moving .ini settings into app.config file.
* WI 116442 Derek Davison 02/13/2014
*   - Major rework. No longer inheriting from cSiteOptions. Very few elements of that base class were being utilized.
*     - Several values (ImagePath, ExceptionLogFilePath) were still being pulled from .ini by cSiteOptions base class causing Image Extracts to fail.
*     - This class now stands on its own and gets most of its values from the app.config file.
* WI 130165 DLD 02/19/2014
*   -Expanded Exception handling and message logging. 
* WI 129719 DLD 02/24/2014
*   -Removed ImagePath. Image repository is now OLFServices not a UNC path. 
* WI 130364 DLD 03/06/2014
*   -Validation of appSettings values. 
*   -Moved from ExtractAPI   
* WI 132130 DLD 03/07/2014
*   -Removed WorkstationID setting 
* WI 132206 DLD 03/10/2014
*   - Adding http header values (SiteKey and SessionID) to service call.
* WI 132474 DLD 03/11/2014
*   -Removed SessionID. Added validation for OLFServices Username and Password.
* WI 130642 BLR 03/19/2014
*   - Root the paths before hitting CheckDirectory.  
* WI 134593 BLR 03/31/2014
*   - Changed SiteKey over to a string.
* WI 135747 BLR 04/09/2014
*   - Added validation for Password in the connection string.
* WI 138812 BLR 07/11/2014
*   - Removed OLF Username/Password in Preparation for RAAM.
* WI 152321 BLR 08/15/2014
*   - Moved configuration settings over to ExtractConfigurationSettings. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    public sealed class cExtractSettings //: cSiteOptions
    {
        #region Members

        public event outputErrorEventHandler OutputError;

        public event outputMessageEventHandler OutputMessage;

        public string SetupPath { get; private set; }

        public string ExceptionLogFilePath { get; private set; }

        public string LogFilePath { get; private set; }

        public int LogFileMaxSize { get; private set; }

        //public int WorkstationID { get; private set; }

        public DefFileFormatEnum DefaultDefFileFormat { get; private set; }

        public bool CreateZeroLengthFile { get; private set; }

        public LTAMessageImportance LoggingDepth { get; private set; }

        // WI 134593 : Changed SiteKey over to a string.
        public string SiteKey { get; private set; }

        /// <summary>
        /// Data Access connection string. Set in app.config \ appSettings
        /// </summary>
        public string DbConnectionString { get; private set; }

        private CustomConfigurationSection _customConfig = null;
        /// <summary>
        /// Reference to a custom section in the app.config file we're using to store collection-based settings.
        /// </summary>
        private CustomConfigurationSection CustomConfigurations
        {
            get
            {
                return _customConfig ?? (_customConfig = ConfigurationManager.GetSection(CustomConfigurationSection.MyName) as CustomConfigurationSection);
            }
        }

        private StringCollection _extractStatsColumns = null;
        /// <summary>
        /// A list of columns shown in a statistics report displayed at the end of an ExtractRun.
        /// </summary>
        public StringCollection ExtractStatsColumns
        {
            get
            {
                if (_extractStatsColumns == null)
                {
                    _extractStatsColumns = new StringCollection();
                    if (CustomConfigurations != null)
                    {
                        foreach (ConfirmationField o in CustomConfigurations.ConfirmationFields)
                            _extractStatsColumns.Add(o.Name);
                    }
                }
                return _extractStatsColumns;
            }
        }

        private List<string> _colsToRemove = null;
        /// <summary>
        /// A list of columns that will be removed from table column-name context menus
        /// </summary>
        public List<string> EntryColumnsToRemove
        {
            get
            {
                if (_colsToRemove == null)
                {
                    if (CustomConfigurations != null)
                    {
                        _colsToRemove = new List<string>();

                        foreach (ColumnToRemove col in CustomConfigurations.ColumnsToRemove)
                        {
                            _colsToRemove.Add(col.Name);
                        }
                    }
                }
                return _colsToRemove;
            }
        }

        #endregion

        #region Constructors

        public cExtractSettings()
        {
            LoadSettings();
        }

        #endregion

        #region Methods

        private void LoadSettings()
        {
            DbConnectionString = cAppSettingsLib.Get(ExtractAppSettings.ConnectionString);
            LogFileMaxSize = cAppSettingsLib.Get<int>(ExtractAppSettings.LogFileMaxSize);
            LogFilePath = cAppSettingsLib.Get(ExtractAppSettings.LogFilePath);
            ExceptionLogFilePath = cAppSettingsLib.Get(ExtractAppSettings.ExceptionLogFile);
            SetupPath = cAppSettingsLib.Get(ExtractAppSettings.SetupPath);
            SiteKey = cAppSettingsLib.Get(ExtractAppSettings.SiteKey);
            LoggingDepth = (LTAMessageImportance)cAppSettingsLib.Get<int>(ExtractAppSettings.LoggingDepth);

            var definitionFileFormat = cAppSettingsLib.Get(ExtractAppSettings.DefaultDefinitionFileFormat, "xml");

            if (definitionFileFormat.ToLower().Trim() == "xml")
                DefaultDefFileFormat = DefFileFormatEnum.xml;
            else
                DefaultDefFileFormat = DefFileFormatEnum.cxs;

            var createZeroLengthFile = cAppSettingsLib.Get(ExtractAppSettings.CreateZeroLengthFile, "1");
            CreateZeroLengthFile = createZeroLengthFile.ToLower().Trim() == "1";
        }

        /// <summary>
        /// Validate setting values, notify user, write to logs and set defaults where applicable
        /// </summary>
        public bool ValidateSettings()
        {
            var userMessage = new StringBuilder();
            var log = new StringBuilder();
            var tempMsg = new StringBuilder();
            
            log.Append(ValidateExtractWizardLogFile());
            log.Append(ValidateExceptionLogFile());

            tempMsg.Append(ValidateLogFileSize());
            tempMsg.Append(ValidateDatabaseConnection());
            tempMsg.Append(ValidateSetupPath());
            tempMsg.Append(ValidateLoggingDepth());
            tempMsg.Append(ValidateImageService());

            if (tempMsg.Length > 0)
            {
                userMessage.Append(tempMsg.ToString());
                log.Append(tempMsg.ToString());
                tempMsg.Clear();
            }

            if (log.Length > 0)
                cCommonLogLib.LogMessageLTA(log.ToString(), LTAMessageImportance.Essential);

            if (userMessage.Length > 0)
                OnOutputMessage("WARNING: Invalid/Missing Application Settings" + Environment.NewLine + Environment.NewLine + userMessage.ToString(), this.ToString(), MessageType.Warning, MessageImportance.Essential);

            return (log.Length == 0 && userMessage.Length == 0);
        }

        private string ValidateExtractWizardLogFile()
        {
            var log = new StringBuilder();
            var strDefaultExtractPath = Path.Combine(CommonLib.AppPath, @"..\Extracts");

            if (SetupPath == strDefaultExtractPath && !Directory.Exists(strDefaultExtractPath))
                Directory.CreateDirectory(strDefaultExtractPath);

            // WI 130642 : Root the paths.
            // WI 134593 : Change environment to assembly directory.
            if (!Path.IsPathRooted(this.LogFilePath))
                LogFilePath = Path.Combine(CommonLib.AppPath, LogFilePath);

            if (!CommonLib.CheckDirectory(Path.GetDirectoryName(LogFilePath))
                || Directory.Exists(LogFilePath))
            {
                log.AppendLine("[LogFilePath] - Path not found:");
                log.AppendLine(LogFilePath);
                LogFilePath = Path.Combine(CommonLib.AppPath, "{0:yyyyMMdd}_ExtractWizardLog.txt");
                log.AppendLine("The following default will be used:");
                log.AppendLine(LogFilePath);
                log.AppendLine();
            }

            return log.ToString();
        }

        private string ValidateExceptionLogFile()
        {
            var log = new StringBuilder();
            var strDefaultDataPath = Path.Combine(CommonLib.AppPath, @"..\Data");

            if (ExceptionLogFilePath.StartsWith(strDefaultDataPath) && !Directory.Exists(strDefaultDataPath))
                Directory.CreateDirectory(strDefaultDataPath);

            // WI 130642 : Root the paths.
            // WI 134593 : Change environment to assembly directory.
            if (!Path.IsPathRooted(this.ExceptionLogFilePath))
                ExceptionLogFilePath = Path.Combine(CommonLib.AppPath, ExceptionLogFilePath);

            if (!CommonLib.CheckDirectory(Path.GetDirectoryName(ExceptionLogFilePath))
                || Directory.Exists(ExceptionLogFilePath))
            {
                log.AppendLine("[ExceptionLogFilePath] - Path not found:");
                log.AppendLine(ExceptionLogFilePath);
                ExceptionLogFilePath = Path.Combine(CommonLib.AppPath, "{0:yyyyMMdd}_ExtractWizardExceptionLog.txt");
                log.AppendLine("The following default will be used:");
                log.AppendLine(ExceptionLogFilePath);
                log.AppendLine();
            }

            return log.ToString();
        }
        private string ValidateSetupPath()
        {
            var tempMsg = new StringBuilder();

            // WI 130642 : Root the paths.
            // WI 134593 : Change environment to assembly directory.
            if (!Path.IsPathRooted(this.SetupPath))
                SetupPath = Path.Combine(CommonLib.AppPath, SetupPath);

            if (!CommonLib.CheckDirectory(SetupPath))
            {
                tempMsg.AppendLine("[SetupPath] - Path not found:");
                tempMsg.AppendLine(SetupPath);
                tempMsg.AppendLine();
            }

            return tempMsg.ToString();
        }

        private string ValidateLoggingDepth()
        {
            StringBuilder tempMsg = new StringBuilder();

            if (!Enum.IsDefined(typeof(LTAMessageImportance), LoggingDepth))
            {
                tempMsg.AppendLine("[LoggingDepth] - Value Out of Range: " + LoggingDepth.ToString());
                tempMsg.AppendLine("Valid values are 0 - 2.");
                LoggingDepth = LTAMessageImportance.Essential;
                tempMsg.AppendLine("The following default will be used: " + ((int)LoggingDepth).ToString());
                tempMsg.AppendLine();
            }

            return tempMsg.ToString();
        }

        private string ValidateImageService()
        {
            StringBuilder tempMsg = new StringBuilder();

            // WI 134593 : Changed SiteKey over to a string.
            if (string.IsNullOrWhiteSpace(SiteKey))
            {
                tempMsg.AppendLine("The SiteKey for the image service are missing or invalid:");
                tempMsg.AppendLine("You will not be able to connect to the image repository.");
                tempMsg.AppendLine();

                return tempMsg.ToString();
            }
            
            try
            {
                if (cOLFServicesLib.ServiceIsAvailable == false)
                {
                    tempMsg.AppendLine("Unable to connect to the image repository.");
                    tempMsg.AppendLine("Please check service endpoint configuration in the application's .config file.");
                    tempMsg.AppendLine();
                }
            }
            catch (Exception ex)
            {
                OnExceptionOccurred(this, new Exception("An error occurred testing connectivity to the image repository.", ex));
            }

            return tempMsg.ToString();
        }

        private string ValidateConnectionStringUserId()
        {
            StringBuilder tempMsg = new StringBuilder();

            // Validate the username.
            try
            {
                ExtractLib.CommonLib.DecryptUserIdInConnectionString(DbConnectionString);
            }
            catch (Exception ex)
            {
                tempMsg.AppendLine(ex.Message);
                tempMsg.AppendLine();
            }

            return tempMsg.ToString();
        }

        private string ValidateConnectionStringPassword()
        {
            StringBuilder tempMsg = new StringBuilder();

            // WI 135747 : Validate the password.
            try
            {
                if (!ExtractLib.CommonLib.ValidatePasswordInConnectionString(DbConnectionString))
                {
                    tempMsg.AppendLine("The 'Password' specified in your database connection string does not appear to be in the correct format.");
                    tempMsg.AppendLine("NOTE: The UserId and Password values must be encrypted (Base64).");
                    tempMsg.AppendLine();
                }
            }
            catch (Exception ex)
            {
                tempMsg.AppendLine(ex.Message);
                tempMsg.AppendLine();
            }

            return tempMsg.ToString();
        }

        private string ValidateDatabaseConnection()
        {
            StringBuilder tempMsg = new StringBuilder();

            if (String.IsNullOrWhiteSpace(DbConnectionString))
            {
                tempMsg.AppendLine("[DbConnectionString] - Missing or invalid.");
                tempMsg.AppendLine(DbConnectionString);
                tempMsg.AppendLine();
                return tempMsg.ToString();
            }

            tempMsg.Append(ValidateConnectionStringUserId());
            tempMsg.Append(ValidateConnectionStringPassword());

            return tempMsg.ToString();
        }

        public string ValidateLogFileSize()
        {
            StringBuilder tempMsg = new StringBuilder();

            if (LogFileMaxSize < 1024)
            {
                tempMsg.AppendLine("[LogFileMaxSize] - Invalid value: " + LogFileMaxSize.ToString());
                tempMsg.AppendLine("Setting max file size to 1 Megabyte by default.");
                LogFileMaxSize = 1024;
                tempMsg.AppendLine();
            }

            return tempMsg.ToString();
        }

        public string QualifyPath(string filePath)
        {

            string strParentPath;
            string strRetVal;

            if (filePath.StartsWith("."))
            {
                strParentPath = Directory.GetParent(SetupPath).FullName;
                strRetVal = strParentPath + (strParentPath.EndsWith(@"\") ? string.Empty : @"\") + filePath;
            }
            else
            {
                strRetVal = filePath;
            }

            return (strRetVal);
        }

        private void OnExceptionOccurred(object sender, Exception e)
        {
            if (OutputError != null)
                OutputError(e);
            else
                cCommonLogLib.LogErrorLTA(e, LTA.Common.LTAMessageImportance.Essential);
        }

        private void OnOutputMessage(string msg, string src, MessageType messageType, MessageImportance messageImportance)
        {
            if (OutputMessage != null)
                OutputMessage(msg, src, messageType, messageImportance);
            else
                cCommonLogLib.LogMessageLTA(msg, (LTAMessageImportance)messageImportance);
        }
        
        #endregion
    }

    #region Custom app.config Configuration Section(s) handlers
    // WI 115356 - Derek - Moving ini settings to app.config file

    public class ConfirmationField : ConfigurationElement
    {
        [ConfigurationProperty("Name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return this["Name"].ToString(); }
        }

        [ConfigurationProperty("DisplayOrder")]
        public int DisplayOrder
        {
            get { return Convert.ToInt32(this["DisplayOrder"]); }
        }
    }

    /// <summary>
    /// List of available fields that will be displayed to the User When an Extract has
    /// been generated using ExtractRun or ExtractWizard.  The values will be displayed
    /// to the user ordered by the numerical value of each key.  
    /// Values not located in this list will not be displayed.
    /// </summary>
    public class ConfirmationFields : ConfigurationElementCollection
    {
        public const string MyName = "ConfirmationFields";

        protected override ConfigurationElement CreateNewElement()
        {
            return new ConfirmationField();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ConfirmationField)element).Name;
        }
    }

    public class ColumnToRemove : ConfigurationElement
    {
        [ConfigurationProperty("Name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return this["Name"].ToString(); }
        }
    }

    /// <summary>
    /// List of available fields that will be removed from column selection menus when building an extract.
    /// </summary>
    [ConfigurationCollection(typeof(ColumnToRemove))]
    public class ColumnsToRemove : ConfigurationElementCollection
    {
        public const string MyName = "ColumnsToRemove";

        protected override ConfigurationElement CreateNewElement()
        {
            return new ColumnToRemove();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ColumnToRemove)element).Name;
        }
    }

    /// <summary>
    /// Allows the use of a custom set of configuration values in the app.config file. 
    /// Section name needs to be referenced in the app.config's "<configsections></configsections>"
    /// </summary>
    public class CustomConfigurationSection : ConfigurationSection
    {
        public const string MyName = "CustomConfigs";

        [ConfigurationProperty(ConfirmationFields.MyName)]
        [ConfigurationCollection(typeof(ConfirmationField))]
        public ConfirmationFields ConfirmationFields
        {
            get { return this[ConfirmationFields.MyName] as ConfirmationFields; }
        }

        [ConfigurationProperty(ColumnsToRemove.MyName)]
        [ConfigurationCollection(typeof(ColumnToRemove))]
        public ColumnsToRemove ColumnsToRemove
        {
            get { return this[ColumnsToRemove.MyName] as ColumnsToRemove; }
        }
    }

    #endregion
}