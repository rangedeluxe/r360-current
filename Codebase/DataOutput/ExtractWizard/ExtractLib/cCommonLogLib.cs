﻿using System;
using System.Diagnostics;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/04/2014
*
* Purpose: The purpose of this file is to create a common place to 
*          instantiate log classes, minimizing code repetition.
*
* Modification History
* WI 131640 BLR 03/04/2014
*   - Initial Version 
* WI 131640 DLD 03/05/2014    
*   - ltaLog now pulls its Logging Level from the app.config 
*   - added a few more methods for logging exceptions   
* WI 130364 DLD 03/06/2014
*   - Added reference to cExtractSettings so that logs will have valid paths 
*     to write to even if user enters garbage in the .config file.
* WI 146528 BLR 06/12/2014
*   - Added a few new Log overloads, allowing for a particular log file path. 
* WI 152321 BLR 08/15/2014
*   -Moved Configuration over to ExtractConfigurationSettings. 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    /// <summary>
    /// Class provides 'factory' methods for easily creating log variables.
    /// </summary>
    public static class cCommonLogLib
    {
        private static ExtractConfigurationSettings _settings = null;
        private static ExtractConfigurationSettings Settings
        {
            get
            {
                if (_settings == null)
                {
                    _settings = new ExtractConfigurationSettings(new ConfigurationManagerProvider());
                    try 
                    {
                        _settings.ValidateSettings();
                    }
                    catch (Exception ex)
                    {
                        cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                    }
                }
                return _settings;
            }
        }

        /// <summary>
        /// Returns a new ltaLog with the settings from the App.Config file.
        /// </summary>
        /// <param name="importance"></param>
        /// <returns></returns>
        public static ltaLog CreateLTALogMessage(string filepath)
        {
            return new ltaLog(filepath,
                              Settings.LogFileMaxSizeInt,
                              Settings.LoggingDepthEnum);
        }

        /// <summary>
        /// Returns a new ltaLog with the settings from the App.Config file.
        /// </summary>
        /// <param name="importance"></param>
        /// <returns></returns>
        public static ltaLog CreateLTALogMessage()
        {
            return CreateLTALogMessage(Settings.LogFilePath);
        }

        /// <summary>
        /// Returns a new ltaLog with the settings from the App.Config file.
        /// </summary>
        /// <param name="importance"></param>
        /// <returns></returns>
        public static ltaLog CreateLTALogError(string filepath)
        {
            return new ltaLog(filepath,
                              Settings.LogFileMaxSizeInt,
                              Settings.LoggingDepthEnum);
        }

        /// <summary>
        /// Returns a new ltaLog with the settings from the App.Config file.
        /// </summary>
        /// <param name="importance"></param>
        /// <returns></returns>
        public static ltaLog CreateLTALogError()
        {
            return CreateLTALogError(Settings.ExceptionLogFile);
        }

        /// <summary>
        /// Logs an LTA message with the settings from the App.Config file.
        /// Also includes information about the calling stack.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="important"></param>
        public static void LogMessageLTA(string message, string filepath, LTAMessageImportance importance)
        {
            var log = CreateLTALogMessage(filepath);
            var previouscall = new StackTrace()
                .GetFrame(1);
            // Build source string from the call stack.
            var source = string.Format("{0}:{1}",
                previouscall.GetMethod().DeclaringType.Name,
                previouscall.GetMethod().Name);
            log.logEvent(message, source, importance);
        }

        /// <summary>
        /// Logs an LTA message with the settings from the App.Config file.
        /// Also includes information about the calling stack.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="important"></param>
        public static void LogMessageLTA(string message, LTAMessageImportance importance)
        {
            LogMessageLTA(message, Settings.LogFilePath, importance);
        }

        /// <summary>
        /// Write an Exception's contents to the LTA log.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="importance"></param>
        public static void LogErrorLTA(Exception ex, LTAMessageImportance importance)
        {
            LogErrorLTA(ex, Settings.LogFilePath, importance);
        }

        /// <summary>
        /// Write an Exception's contents to the LTA log.
        /// Also provides ability to append a custom (friendly) message.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="customMessage"></param>
        /// <param name="importance"></param>
        public static void LogErrorLTA(Exception ex, string filepath, LTAMessageImportance importance)
        {
            if (ex != null)
            {
                var log = CreateLTALogError(filepath);
                var msg = ex.ToString();
                msg = Environment.NewLine + new string('=', 80) + Environment.NewLine + msg + Environment.NewLine;
                log.logEvent(msg, ex.Source, importance);
            }
        }
    }
}
