﻿using System;
using System.Collections.Generic;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 02/26/2014
*
* Purpose:  The UI needs friendly names on database fields and tables. This 
*           class will provide an easy way to retrieve those names. 
*           Additionally, this file will provide some consolidation on the 
*           the floating strings found around the current source.
* 
* Modification History
* WI 129674 BLR 02/26/2014
*   - Initial Version
* WI 132009 BLR 03/06/2014
*   - Adding in some more columns, which come from other tables.   
* WI 135766 BLR 04/11/2014
*   - Changed the Lockbox/Customer verbage over to the new. 
*   - Added some missing fields as well.   
* WI 137677 BLR 04/21/2014
*   - Added 2 more columns. 
* WI 143034 BLR 05/22/2014
*   - Changed the BatchID column to the SourceBatchID column.
* WI 129674 BLR 05/22/2014
*   - Changed 'GetFriendly' to simply cut out the Schema and Table names.
*     The mapping system is no longer used.  This was made possible by the 
*     completion of 135749. 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    /// <summary>
    /// In charge of providing a mechanism for to and from conversions between
    /// database constants and friendly strings.
    /// </summary>
    public class cMappingLib
    {
        /// <summary>
        /// Returns a friendly name of a table or field.
        /// </summary>
        /// <param name="full"></param>
        /// <returns></returns>
        public static string GetFriendly(string full)
        {
            if (full == null) return null;

            var index = full.LastIndexOf('.');
            if (index <= 0) return full;

            var friendly = full.Substring(index + 1);

            // Found a friendly, now need to check if it's a table.
            // If so, map it to their new names.
            if (IsTable(full)) return ToFriendlyTableName(friendly);

			// If it's a column, we need to check for some special cases 
            // (SourceBatchID).
            if (IsColumn(full)) return ToFriendlyColumnName(friendly);
            

            return friendly;
        }

        public static string ToFriendlyTableName(string tableName)
        {
            if (tableName == "dimBanks") return "Banks";
            if (tableName == "dimClientAccounts") return "Workgroups";
            if (tableName == "factBatchSummary") return "Batches";
            if (tableName == "factTransactionSummary")return "Transactions";
            if (tableName == "factChecks") return "Payments";
            if (tableName == "factStubs") return "Stubs";
            if (tableName == "factDocuments") return "Documents";

            return tableName;
        }

        public static string ToFriendlyColumnName(string columnName)
        {
            if (columnName == "SourceBatchID") return "BatchID";

            return columnName;
        }

        /// <summary>
        /// Returns true if the fully qualified name is a table.
        /// </summary>
        /// <param name="full"></param>
        /// <returns></returns>
        public static bool IsColumn(string full)
        {
            if (full == null)
                return false;
            if (full.Count(x => x == '.') != 2)
                return false;
            return true;
        }

        /// <summary>
        /// Returns true if the fully qualified name is a table.
        /// </summary>
        /// <param name="full"></param>
        /// <returns></returns>
        public static bool IsTable(string full)
        {
            if (full == null)
                return false;
            if (full.Count(x => x == '.') != 1)
                return false;
            return true;
        }

        #region Friendly--Full Mappings

        /// <summary>
        /// Just a storage model for our mappings.
        /// </summary>
        private class NameMapping
        {
            public string FriendlyName { get; set; }
            public string FullName { get; set; }

            public NameMapping(string fullname, string friendlyname)
            {
                FullName = fullname;
                FriendlyName = friendlyname;
            }
        }

        /// <summary>
        /// To simply remove the repetition of code for the mappings,
        /// found below, just for syntactic sugar.
        /// </summary>
        private class MappingContainer : List<NameMapping>
        {
            public MappingContainer() : base() {}
            public void Add(string full, string friendly)
            {
                this.Add(new NameMapping(full, friendly));
            }
        }

        /// <summary>
        /// Constant storage.
        /// </summary>
        private static MappingContainer mappings = new MappingContainer()
        {
            // Tables
            {"RecHubData.dimBanks", "Banks"},
            {"RecHubData.dimClientAccounts", "Workgroups"},
            {"RecHubData.factBatchSummary", "Batches"},
            {"RecHubData.factTransactionSummary", "Transactions"},
            {"RecHubData.factChecks", "Payments"},
            {"RecHubData.factStubs", "Stubs"},
            {"RecHubData.factDocuments", "Documents"},
            // Bank Fields
            {"RecHubData.dimBanks.ABA", "Bank ABA"},
            {"RecHubData.dimBanks.BankName", "Bank Name"},
            {"RecHubData.dimBanks.CreationDate", "Bank Creation Date"},
            {"RecHubData.dimBanks.ModificationDate", "Bank Modification Date"},
            {"RecHubData.dimBanks.SiteBankID", "Bank Site ID"},
            // Client Account (Workgroup) Fields
            {"RecHubData.dimClientAccounts.CreationDate", "Workgroup Creation Date"},
            {"RecHubData.dimClientAccounts.CutOff", "Workgroup Cut Off"},
            {"RecHubData.dimClientAccounts.DataRetentionDays", "Workgroup Data Retention Days"},
            {"RecHubData.dimClientAccounts.DDA", "Workgroup DDA"},
            {"RecHubData.dimClientAccounts.FileGroup", "Workgroup File Group"},
            {"RecHubData.dimClientAccounts.ImageRetentionDays", "Workgroup Image Retention Days"},
            {"RecHubData.dimClientAccounts.IsActive", "Workgroup Active Status"},
            {"RecHubData.dimClientAccounts.IsCommingled", "Workgroup Commingled Status"},
            {"RecHubData.dimClientAccounts.LongName", "Workgroup Long Name"},
            {"RecHubData.dimClientAccounts.ModificationDate", "Workgroup Modification Date"},
            {"RecHubData.dimClientAccounts.OfflineColorMode", "Workgroup Offline Color Mode"},
            {"RecHubData.dimClientAccounts.POBox", "Workgroup PO Box"},
            {"RecHubData.dimClientAccounts.ShortName", "Workgroup Short Name"},
            {"RecHubData.dimClientAccounts.SiteBankID", "Workgroup Site Bank ID"},
            {"RecHubData.dimClientAccounts.SiteClientAccountID", "Workgroup Site ID"},
            {"RecHubData.dimClientAccounts.SiteCodeID", "Workgroup Site Code ID"},
            // Batch Fields
            {"RecHubData.factBatchSummary.BatchCueID", "Batch Cue ID"},
            {"RecHubData.factBatchSummary.SourceBatchID", "Batch ID"},
            {"RecHubData.factBatchSummary.BatchNumber", "Batch Number"},
            {"RecHubData.factBatchSummary.BatchSiteCode", "Batch Site Code"},
            {"RecHubData.factBatchSummary.CheckCount", "Batch Payment Count"},
            {"RecHubData.factBatchSummary.CheckTotal", "Batch Payment Total"},
            {"RecHubData.factBatchSummary.CreationDate", "Batch Creation Date"},
            {"RecHubData.factBatchSummary.DepositDDA", "Batch Deposit DDA"},
            {"RecHubData.factBatchSummary.DepositStatus", "Batch Deposit Status"},
            {"RecHubData.factBatchSummary.DocumentCount", "Batch Document Count"},
            {"RecHubData.factBatchSummary.IsDeleted", "Batch Deleted Status"},
            {"RecHubData.factBatchSummary.ModificationDate", "Batch Modification Date"},
            {"RecHubData.factBatchSummary.ScannedCheckCount", "Batch Scanned Payment Count"},
            {"RecHubData.factBatchSummary.StubCount", "Batch Stub Count"},
            {"RecHubData.factBatchSummary.StubTotal", "Batch Stub Total"},
            {"RecHubData.factBatchSummary.SystemType", "Batch System Type"},
            {"RecHubData.factBatchSummary.TransactionCount", "Batch Transaction Count"},
            // WI 132009 : Adding in some more columns, which come from other tables.
            {"RecHubData.dimBatchSources.LongName", "Batch Source Long Name"},
            {"RecHubData.dimBatchPaymentTypes.LongName", "Batch Payment Type Long Name"},
            // Transaction Fields
            {"RecHubData.factTransactionSummary.BatchCueID", "Transaction Batch Cue ID"},
            {"RecHubData.factTransactionSummary.SourceBatchID", "Transaction Batch ID"},
            {"RecHubData.factTransactionSummary.BatchNumber", "Transaction Batch Number"},
            {"RecHubData.factTransactionSummary.BatchSiteCode", "Transaction Batch Site Code"},
            {"RecHubData.factTransactionSummary.CheckCount", "Transaction Payment Count"},
            {"RecHubData.factTransactionSummary.CheckTotal", "Transaction Payment Total"},
            {"RecHubData.factTransactionSummary.CreationDate", "Transaction Creation Date"},
            {"RecHubData.factTransactionSummary.DepositStatus", "Transaction Deposit Status"},
            {"RecHubData.factTransactionSummary.DocumentCount", "Transaction Document Count"},
            {"RecHubData.factTransactionSummary.IsDeleted", "Transaction Deleted Status"},
            {"RecHubData.factTransactionSummary.ModificationDate", "Transaction Modification Date"},
            {"RecHubData.factTransactionSummary.OMRCount", "Transaction OMR Count"},
            {"RecHubData.factTransactionSummary.ScannedCheckCount", "Transaction Scanned Payment Count"},
            {"RecHubData.factTransactionSummary.StubCount", "Transaction Stub Count"},
            {"RecHubData.factTransactionSummary.StubTotal", "Transaction Stub Total"},
            {"RecHubData.factTransactionSummary.SystemType", "Transaction System Type"},
            {"RecHubData.factTransactionSummary.TransactionID", "Transaction ID"},
            {"RecHubData.factTransactionSummary.TxnSequence", "Transaction Sequence"},
            // Payment Fields
            {"RecHubData.factChecks.Account", "Payment Account Number"},
            {"RecHubData.factChecks.Amount", "Payment Amount"},
            {"RecHubData.factChecks.BatchCueID", "Payment Batch Cue ID"},
            {"RecHubData.factChecks.SourceBatchID", "Payment Batch ID"},
            {"RecHubData.factChecks.BatchNumber", "Payment Batch Number"},
            {"RecHubData.factChecks.BatchSequence", "Payment Batch Sequence"},
            {"RecHubData.factChecks.BatchSiteCode", "Payment Batch Site Code"},
            {"RecHubData.factChecks.CheckSequence", "Payment Sequence"},
            {"RecHubData.factChecks.CreationDate", "Payment Creation Date"},
            {"RecHubData.factChecks.DepositStatus", "Payment Deposit Status"},
            {"RecHubData.factChecks.IsDeleted", "Payment Deleted Status"},
            {"RecHubData.factChecks.ModificationDate", "Payment Modification Date"},
            {"RecHubData.factChecks.NumericRoutingNumber", "Payment Numeric Routing Number"},
            {"RecHubData.factChecks.NumericSerial", "Payment Numeric Serial"},
            {"RecHubData.factChecks.RemitterName", "Payment Remitter Name"},
            {"RecHubData.factChecks.RoutingNumber", "Payment Routing Number"},
            {"RecHubData.factChecks.SequenceWithinTransaction", "Payment Sequence Within Transaction"},
            {"RecHubData.factChecks.Serial", "Payment Serial"},
            {"RecHubData.factChecks.TransactionCode", "Payment Transaction Code"},
            {"RecHubData.factChecks.TransactionID", "Payment Transaction ID"},
            {"RecHubData.factChecks.TxnSequence", "Payment Transaction Sequence"},
            // Stub Fields
            {"RecHubData.factStubs.AccountNumber", "Stub Account Number"},
            {"RecHubData.factStubs.Amount", "Stub Amount"},
            {"RecHubData.factStubs.BatchCueID", "Stub Batch Cue ID"},
            {"RecHubData.factStubs.SourceBatchID", "Stub Batch ID"},
            {"RecHubData.factStubs.BatchNumber", "Stub Batch Number"},
            {"RecHubData.factStubs.BatchSequence", "Stub Batch Sequence"},
            {"RecHubData.factStubs.BatchSiteCode", "Stub Batch Site Code"},
            {"RecHubData.factStubs.CreationDate", "Stub Creation Date"},
            {"RecHubData.factStubs.DepositStatus", "Stub Deposit Status"},
            {"RecHubData.factStubs.DocumentBatchSequence", "Stub Document Batch Sequence"},
            {"RecHubData.factStubs.IsDeleted", "Stub Deleted Status"},
            {"RecHubData.factStubs.IsOMRDetected", "Stub OMR Detected Status"},
            {"RecHubData.factStubs.ModificationDate", "Stub Modification Date"},
            {"RecHubData.factStubs.SequenceWithinTransaction", "Stub Sequence Within Transaction"},
            {"RecHubData.factStubs.StubSequence", "Stub Sequence"},
            {"RecHubData.factStubs.SystemType", "Stub System Type"},
            {"RecHubData.factStubs.TransactionID", "Stub Transaction ID"},
            {"RecHubData.factStubs.TxnSequence", "Stub Transaction Sequence"},
            // Document Fields
            {"RecHubData.factDocuments.BatchCueID", "Document Batch Cue ID"},
            {"RecHubData.factDocuments.SourceBatchID", "Document Batch ID"},
            {"RecHubData.factDocuments.BatchNumber", "Document Batch Number"},
            {"RecHubData.factDocuments.BatchSequence", "Document Batch Sequence"},
            {"RecHubData.factDocuments.BatchSiteCode", "Document Batch Site Code"},
            {"RecHubData.factDocuments.CreationDate", "Document Creation Date"},
            {"RecHubData.factDocuments.DepositStatus", "Document Deposit Status"},
            {"RecHubData.factDocuments.DocumentSequence", "Document Sequence"},
            {"RecHubData.factDocuments.IsDeleted", "Document Deleted Status"},
            {"RecHubData.factDocuments.ModificationDate", "Document Modification Date"},
            {"RecHubData.factDocuments.SequenceWithinTransaction", "Document Sequence Within Transaction"},
            {"RecHubData.factDocuments.TransactionID", "Document Transaction ID"},
            {"RecHubData.factDocuments.TxnSequence", "Document Transaction Sequence"},
            // WI 132009 : Adding in some more columns, which come from other tables.
            {"RecHubData.dimDocumentTypes.DocumentTypeDescription", "Document Type Description"},
            // WI 129674 : Missed Aggregate/Standard fields in initial version.
            // Aggregate Fields
            {"CountBatches", "Count of Batches"},
            {"CountTransactions", "Count of Transactions"},
            {"CountPayments", "Count of Payments"},
            {"CountDocuments", "Count of Documents"},
            {"CountStubs", "Count of Stubs"},
            {"SumPaymentsAmount", "Sum of Payments"},
            {"SumStubsAmount", "Sum of Stubs"},
            // WI 135766 : Added more missing fields.
            {"RecordCountFile", "Record Count of Files"},
            {"RecordCountBank", "Record Count of Banks"},
            {"RecordCountWorkgroup", "Record Count of Workgroups"},
            {"RecordCountBatch", "Record Count of Batches"},
            {"RecordCountTran", "Record Count of Transactions"},
            {"RecordCountPayment", "Record Count of Payments"},
            {"RecordCountStub", "Record Count of Stubs"},
            {"RecordCountDoc", "Record Count of Documents"},
            {"RecordCountDetail", "Record Count of Details"},
            {"RecordCountTotal", "Record Count Total"},
            // Standard Fields
            {"Counter", "Counter"},
            {"Date", "Date"},
            {"Filler", "Filler"},
            {"FirstLast", "First-Last"},
            {"ImagePaymentPerBatch", "Image Payment Per Batch"},
            {"ImagePaymentPerTransaction", "Image Payment Per Transaction"},
            {"ImagePaymentSingleFront", "Image Payment Single Front"},
            {"ImagePaymentSingleRear", "Image Payment Single Rear"},
            {"LineCounterFile", "Line Counter File"},
            {"LineCounterBank", "Line Counter Bank"},
            {"LineCounterWorkgroup", "Line Counter Workgroup"},
            {"LineCounterBatch", "Line Counter Batch"},
            {"LineCounterTransaction", "Line Counter Transaction"},
            {"LineCounterPayment", "Line Counter Payment"},
            // WI 137677 : Missed 2 Columns.
            {"LineCounterStub", "Line Counter Stub"},
            {"LineCounterDocument", "Line Counter Document"},
            {"ProcessingRunDate", "Processing Run Date"},
            {"Static", "Static"},
            {"Time", "Time"},
        };

        #endregion
    }

    #region Custom Exceptions

    /// <summary>
    /// Exception for when a mapping cannot be found.
    /// </summary>
    public class InvalidMappingException : Exception
    {
        public string Name { get; set; }
        public InvalidMappingException(string name) 
            : base(string.Format("Could not find mapping name '{0}'.", name))
        {
            Name = name;
        }
    }

    #endregion
}
