﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    public interface IExtractDef {

        string ExtractDefFilePath {
            get;
        }
        
        string Version {
            get;
        }
        
        string ExtractPath {
            get;
        }
        
        string LogFilePath {
            get;
        }
        
        string ImagePath {
            get;
        }
        
        bool IncludeFooter {
            get;
        }
        
        bool UseQuotes {
            get;
        }
        
        bool NoPad {
            get;
        }
        
        bool NullAsSpace {
            get;
        }
        
        string TimeStamp {
            get;
        }
        
        string FieldDelim {
            get;
        }
        
        string RecordDelim {
            get;
        }
        
        //ImageFileFormatEnum ImageFileFormat {
        //    get;
        //}
        
        string ImageFileFormatDesc {
            get;
        }
        
        string ImageFileNameSingle {
            get;
        }
        
        string ImageFileNamePerTran {
            get;
        }
        
        string ImageFileNamePerBatch {
            get;
        }
        
        string ImageFileNameSingleDesc {
            get;
        }
        
        string ImageFileNamePerTranDesc {
            get;
        }
        
        string ImageFileNamePerBatchDesc {
            get;
        }
        
        string ImageFileProcDateFormat {
            get;
        }
        
        bool ImageFileZeroPad {
            get;
        }
        
        bool ImageFileBatchTypeFormatFull {
            get;
        }
        
        bool IncludeImageFolderPath {
            get;
        }
        
        bool IncludeNULLRows {
            get;
        }
        
        string PostProcDLLFileName {
            get;
        }

        ILayout[] FileLayouts {
            get;
        }

        ILayout[] BankLayouts {
            get;
        }

        ILayout[] CustomerLayouts {
            get;
        }

        ILayout[] LockboxLayouts {
            get;
        }

        ILayout[] BatchLayouts {
            get;
        }

        ILayout[] TransactionLayouts {
            get;
        }

        ILayout[] PaymentLayouts {
            get;
        }

        ILayout[] StubLayouts {
            get;
        }

        ILayout[] DocumentLayouts {
            get;
        }

        ILayout[] JointDetailLayouts {
            get;
        }

        ILayout[] AllLayouts {
            get;
        }

        IOrderByColumn[] BankOrderByColumns {
            get;
        }

        IOrderByColumn[] LockboxOrderByColumns {
            get;
        }

        IOrderByColumn[] BatchOrderByColumns {
            get;
        }

        IOrderByColumn[] TransactionsOrderByColumns {
            get;
        }

        IOrderByColumn[] PaymentsOrderByColumns {
            get;
        }

        IOrderByColumn[] StubsOrderByColumns {
            get;
        }

        IOrderByColumn[] DocumentsOrderByColumns {
            get;
        }

        ILimitItem[] BankLimitItems {
            get;
        }

        ILimitItem[] LockboxLimitItems {
            get;
        }

        ILimitItem[] BatchLimitItems {
            get;
        }

        ILimitItem[] TransactionsLimitItems {
            get;
        }

        ILimitItem[] PaymentsLimitItems {
            get;
        }

        ILimitItem[] StubsLimitItems {
            get;
        }

        ILimitItem[] DocumentsLimitItems {
            get;
        }
        
        long[] DefaultBankID {
            get;
        }
        
        long[] DefaultLockboxID {
            get;
        }
        
        long[] DefaultBatchID {
            get;
        }
        
        DateTime DefaultProcessingDate {
            get;
        }
        
        DateTime DefaultDepositDate {
            get;
        }
    }
}
