﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 269611 JMC 03/10/2016
*   -Created interface in order to enable Mocking
* WI 269414 JMC 03/10/2016
*   -Modified interface signature for WriteExtractAuditData.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {
    public interface IExtractEngineDAL {

        event outputMessageEventHandler OutputMessage;

        event outputErrorEventHandler OutputError;

        bool GetPreviousRuns(
            string columnName,
            out DataTable dt);

        void BeginTrans();
        void CommitTrans();
        void RollbackTrans();
        bool WriteEventLog(
            string eventName,
            string message,
            int sitecodeid,
            int sitebankid,
            int siteworkgroupid
            );
        bool WriteSystemLevelEventLog(
            string eventName,
            string message
            );

        bool GetBanks(
            IExtractParms parms,
            IExtractDef extractDef,
            Dictionary<string, IColumn> bankTableDef,
            out SqlDataReader drBanks);


        bool GetClientAccounts(
            IExtractParms parms,
            IExtractDef extractDef,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            out DbDataReader drClientAccounts);


        bool GetDepositDates(
            IExtractParms parms,
            IExtractDef extractDef,
            string factTableName,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            out DataTable dtDepositDates);


        bool GetBatches(
            IExtractParms parms,
            IExtractDef extractDef,
            int dteCurrent,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            out SqlDataReader drBatches);


        bool GetTransactions(
            IExtractParms parms,
            IExtractDef extractDef,
            int dteCurrent,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            Dictionary<string, IColumn> transactionsTableDef,
            out SqlDataReader drTransactions);

        bool GetPayments(
            IExtractParms parms,
            IExtractDef extractDef,
            int dteCurrent,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            Dictionary<string, IColumn> transactionsTableDef,
            Dictionary<string, IColumn> paymentsTableDef,
            out SqlDataReader drPayments);

        bool GetStubs(
            IExtractParms parms,
            IExtractDef extractDef,
            int dteCurrent,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            Dictionary<string, IColumn> transactionsTableDef,
            Dictionary<string, IColumn> stubsTableDef,
            out SqlDataReader drStubs);

        bool GetDocuments(
            IExtractParms parms,
            IExtractDef extractDef,
            int dteCurrent,
            Dictionary<string, IColumn> bankTableDef,
            Dictionary<string, IColumn> clientAccountTableDef,
            Dictionary<string, IColumn> extractTracesTableDef,
            Dictionary<string, IColumn> batchTableDef,
            Dictionary<string, IColumn> transactionsTableDef,
            Dictionary<string, IColumn> documentsTableDef,
            out SqlDataReader drDocuments);


        bool InsertExtractAudit(
                Guid extractAuditID,
                int bankID,
                int workgroupID,
                bool isComplete,
                int checkCount,
                int stubCount,
                decimal paymentTotal,
                decimal stubTotal,
                int recordCount,
                string fileName);

        bool WriteAuditFileData(
            IExtractParms parms,
            IExtractDef extractDef,
            cStandardFields standardFields,
            int workstationID);

            bool GetTableDef(
            string tableName,
            out DataTable dtTableDef);

        bool WriteTraceField(
            IExtractParms parms,
            cStandardFields standardFields,
            long batchID);

        bool WriteExtractSequenceNumber(
            IExtractParms parms,
            cBatchKey[] uniqueBatchKeys);

        bool GetCheckInfo(
            cItemKey checkKey,
            out DataTable dtCheckInfo);

        bool GetDocumentInfo(
            cItemKey documentKey,
            out DataTable dtDocumentInfo);

        bool TestConnection();

        void Dispose();
    }




}
