﻿

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 130364 DLD 02/20/2014
*   -Verify database connectivity providing 'friendly' messages to user upon failure.
* WI 130165 DLD 02/19/2014
*   -Expanded Exception handling and message logging. 
* WI 132009 BLR 03/06/2014
*   - Adding in some more columns, which come from other tables.   
* WI 130644 BLR 03/18/2014  
*   - Added additional error messages for SQL failures 
*     (due to invalid def input).
* WI 143040 BLR 05/22/2014  
*   - Removed composite key from cBatchKey 
* WI 162851 BLR 09/03/2014
*   - Added limit constants. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    ////public delegate void OutputStatusMsgEventHandler(string Msg);
    ////public delegate void ImageStatusInitializedEventHandler(int MaxItems);
    ////public delegate void ImageStatusChangedEventHandler(int CurrentItem);
    ////public delegate void PromptForManualInputEventHandler(ref Dictionary<string, string> ManualInputValues);

    public enum JustifyEnum {
        Left = 0,
        Right = 1
    }

    public enum LayoutTypeEnum {
        Header = 0,
        Trailer = 1
    }

    public enum LayoutLevelEnum {
        File = 0,
        Bank = 1,
        ClientAccount = 3,
        Batch = 4,
        Transaction = 5,
        Payment = 6,
        Stub = 7,
        Document = 8,
        JointDetail = 9
    }

    public enum OrderByDirEnum {
        Ascending = 0,
        Descending = 1
    }

    public enum ImageFileFormatEnum {
        DoNotRetrieveImages = 0,
        Tiff = 1,
        SendToPrinter = 2,
        Pdf = 4
    }

    public enum FirstMidLast {
        First,
        Mid,
        Last
    }

    public enum DefFileFormatEnum {
        cxs = 1,
        xml = 2
    }

    public enum ImageFileNameFormatEnum {
        Single,
        Transaction,
        Batch
    }

    public enum DataEntryType {
        PaymentsDataEntry,
        StubsDataEntry
    }

    // TableType Column from RecHubData.dimDataEntryColumns
    public enum DataEntryTableType {
        Payments = 0,
        PaymentsDataEntry = 1,
        Stubs = 2,
        StubsDataEntry = 3
    }

    /// <summary>
    /// 
    /// </summary>
    public delegate void OutputExtractErrorEventHandler(
        object sender,
        System.Exception e);

    /// <summary>
    /// 
    /// </summary>
    public delegate void OutputExtractMessageEventHandler(
        string message,
        string src,
        ExtractMessageType messageType,
        ExtractMessageImportance messageImportance);

    /// <summary>
    /// MessageType enumeration
    /// </summary>
    public enum ExtractMessageType {
        /// <summary></summary>
        Error = 0,
        /// <summary></summary>
        Information = 1,
        /// <summary></summary>
        Warning = 2
    }

    /// <summary>
    /// MessageImportance enumeration
    /// </summary>
    public enum ExtractMessageImportance {
        /// <summary></summary>
        Essential = 0,
        /// <summary></summary>
        Verbose = 1,
        /// <summary></summary>
        Debug = 2
    }

    public static class Constants {

        public const string INI_SECTION_EXTRACTWIZARD = "ExtractWizard";

        public const int EXCEPTION_LOG_FILE_MAX_SIZE = 1024;
        public const int EXTRACT_LOG_FILE_MAX_SIZE = 1024;
        // WI 115064 : Changed .cxr with .xdf. 20140212 - Derek - Changed order of filters to match the corresponding Enum.
        public const string FILE_FILTER_EXTRACT_DEF = "Extract Definition Files (*.xdf)|*.xdf|Xml Files (*.xml)|*.xml";
        // WI 115289 : Removed txt files from results.
        public const string FILE_FILTER_EXTRACT_DATA_FILES = "All Files |*.*|Extract Data Files (*.cxr)|*.cxr";
        // WI 115289 : Removed txt files from logs.
        public const string FILE_FILTER_LOG_FILES = "All Files |*.*|Extract Log Files (*.log)|*.log";
        public const string FILE_FILTER_ZIP_FILES = "Zip Files (*.zip)|*.zip";
        public const string FILE_FILTER_DLL = "Dynamic Link Libraries (*.dll)|*.dll";
        // DLD - WI 130364 - Verify database connectivity providing 'friendly' messages to user upon failure.
        public const string ERROR_MSG_NO_DATABASE_CONNECTION = "Unable to connect to the database.\r\nPlease verify your logon credentials or contact system support.";
        // DLD - WI 130165 - common message displayed to user when an unhandled exception has occurred.
        public const string ERROR_MSG_UNHANDLED = "An unexpected exception has occurred.\r\nThe application may have become unstable and should be closed.\r\nPlease refer to the application logs for more detail.";
        public const string ERROR_MSG_PROCESS_FAILED = "Process failed.\r\nPlease refer to the application log for more detail.";
        // WI 130644 : Added message for a failing SQL statement.
        public const string ERROR_MSG_SQL_FAILURE = "There was an exception when retrieving data from the database. It may be a data validation issue. Please check the extract definition fields";
        public const string ERROR_MSG_SQL_CONNECTION_SRING = "Unable to connect to the database. Please verify the connection string in the configuration file, and that the specified account has permissions to access the database.";
        public const string ERROR_MSG_SQL_ENCRYPTED_USERNAME = "Please verify that the username and password are encrypted.";
    }

    public static class DatabaseConstants {
        
        public const string TABLE_NAME_BATCHTRACE = "dbo.BatchTrace";
        public const string TABLE_NAME_CHECKSDE = "ChecksDataEntry";
        public const string TABLE_NAME_STUBSDE = "StubsDataEntry";

        public const string TABLE_NAME_DIMBANKS = "RecHubData.dimBanks";
        public const string TABLE_NAME_DIMCLIENTACCOUNTS = "RecHubData.dimClientAccounts";
        public const string TABLE_NAME_FACTBATCH = "RecHubData.factBatchSummary";
        public const string TABLE_NAME_FACTTXN = "RecHubData.factTransactionSummary";
        public const string TABLE_NAME_FACTCHECKS = "RecHubData.factChecks";
        public const string TABLE_NAME_PAYMENTDATAENTRY = "RecHubData.factDataEntry";
        public const string TABLE_NAME_FACTSTUBS = "RecHubData.factStubs";
        public const string TABLE_NAME_DIMBATCHPAYMENTTYPES = "RecHubData.dimBatchPaymentTypes";
        public const string TABLE_NAME_DIMBATCHSOURCES = "RecHubData.dimBatchSources";
        public const string TABLE_NAME_DIMDOCUMENTTYPES = "RecHubData.dimDocumentTypes";
        // WI 142972 : Added joined table DimDDAs.
		public const string TABLE_NAME_DIMDDAS = "RecHubData.dimDDAs";
        public const string TABLE_NAME_DIMEXCEPTIONSTATUSES = "RecHubData.dimExceptionStatuses";
        
        public const string TABLE_NAME_STUBSDATAENTRY = "RecHubData.factDataEntry";
        public const string TABLE_NAME_FACTDOCUMENTS = "RecHubData.factDocuments";
        public const string TABLE_NAME_DATAENTRYDETAILS = "RecHubData.factDataEntryDetails";

        public static string[] LIMIT_TABLES_BANKS = new string[] { TABLE_NAME_DIMBANKS };
        public static string[] LIMIT_TABLES_CLIENTACCOUNTS = new string[] { TABLE_NAME_DIMCLIENTACCOUNTS };
        public static string[] LIMIT_TABLES_BATCHES = new string[] { TABLE_NAME_FACTBATCH, TABLE_NAME_DIMBATCHPAYMENTTYPES, TABLE_NAME_DIMBATCHSOURCES };
        public static string[] LIMIT_TABLES_TRANSACTIONS = new string[] { TABLE_NAME_FACTTXN, TABLE_NAME_DIMEXCEPTIONSTATUSES };
        public static string[] LIMIT_TABLES_PAYMENTS = new string[] { TABLE_NAME_FACTCHECKS, TABLE_NAME_DIMDDAS };
        public static string[] LIMIT_TABLES_STUBS = new string[] { TABLE_NAME_FACTSTUBS };
        public static string[] LIMIT_TABLES_DOCUMENTS = new string[] { TABLE_NAME_FACTDOCUMENTS, TABLE_NAME_DIMDOCUMENTTYPES };
    }

    public class cBatchKey 
    {

        private long _BatchID;

        public cBatchKey(long batchID) 
        {
            _BatchID = batchID;
        }

        public long BatchID
        {
            get 
            {
                return (_BatchID);
            }
        }

        public override string ToString() 
        {
            return (BuildUniqueBatchID());
        }

        private string BuildUniqueBatchID() 
        {
            return this.BatchID.ToString();
        }
    }

    public class cItemKey : cBatchKey 
    {

        private int _BatchSequence;

        public cItemKey(long batchID, int batchSequence) 
            : base(batchID) 
        {
            _BatchSequence = batchSequence;
        }

        public int BatchSequence
        {
            get 
            {
                return (_BatchSequence);
            }
        }

        public override string ToString() 
        {
            return (BuildUniqueItemID());
        }

        private string BuildUniqueItemID()
        {
            return (
                this.BatchID.ToString() + "_" +
                this.BatchSequence.ToString());
        }

    }
}
