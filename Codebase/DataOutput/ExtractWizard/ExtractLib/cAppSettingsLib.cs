﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Derek Davison (SC)
* Date: 02/11/2014
*
* Purpose:  Switch over INI functionality to .config file.
*
* Modification History
* WI  114217 Derek Davison 02/11/2014
*   -Initial Version
*   -Updated to allow Generic return types (20140211).
* WI 130165 DLD 02/19/2014
*   -No longer swallowing exceptions while attempting to retrieve an invalid key. 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 131640 DLD 03/05/2014    
*   - Added "LoggingDepth" to enum for use with ltaLog.
* WI 130364 DLD 03/06/2014
*   - Settings validation
* WI 132130 DLD 03/07/2014
*   - Removing WorkstationID
* WI 132206 DLD 03/10/2014
*   - Adding http header values (SiteKey and SessionID) to service call.
* WI 132474 DLD 03/11/2014
*   -Removed SessionID and replaced with OLFServices Username and Password required to retrieve OLFService session token.
* WI 134593 BLR 03/31/2014
*   - Removed exception throw, added logging, default return values. 
* WI 138812 BLR 07/11/2014
*   - Removed OLF Username/Password in Preparation for RAAM.
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    /// <summary>
    /// Used to reference App.Config appSettings keys.
    /// </summary>
    public enum ExtractAppSettings
    {
        ConnectionString,
        SetupPath,
        LogFilePath,
        LogFileMaxSize,
        DefaultDefinitionFileFormat,
        CreateZeroLengthFile,
        LogoPath,
        BackgroundColor,
        ExceptionLogFile,
        RunAsUserId,
        LoggingDepth,
        SiteKey
    }

    /// <summary>
    /// Wraps the web.config appSettings. 
    /// Fail-safe way to retrieve appSettings by Enum or String key and provide default value (optional)
    /// appSettings Keys are stored in a enum for quick/easy reference.
    /// </summary>
    public static class cAppSettingsLib
    {
        /// <summary>
        /// retrieve appSettings by Enum key and provide default value (optional)
        /// </summary>
        public static string Get(ExtractAppSettings key, string defaultValue = "")
        {
            return Get(key.ToString(), defaultValue);
        }

        /// <summary>
        /// retrieve appSettings by String key and provide default value (optional)
        /// </summary>
        public static string Get(string key, string defaultValue = "")
        {
            return Get<string>(key, defaultValue);
        }

        /// <summary>
        /// Get a value cast from String to type 'T"
        /// </summary>
        /// <typeparam name="T">The desired return type.</typeparam>
        /// <returns></returns>
        public static T Get<T>(ExtractAppSettings key)
        {
            return Get<T>(key, default(T));
        }

        /// <summary>
        /// Get a value cast from String to type 'T" providing a default value.
        /// </summary>
        /// <typeparam name="T">The desired return type.</typeparam>
        public static T Get<T>(ExtractAppSettings key, T defaultValue)
        {
            return Get<T>(key.ToString(), defaultValue);
        }

        public static T Get<T>(string key, T defaultValue)
        {
            var result = defaultValue;
            key = key.Trim();

            try
            {
                if (!String.IsNullOrWhiteSpace(key))
                {
                    if (ConfigurationManager.AppSettings.AllKeys.Contains(key))
                    {
                        var s = ConfigurationManager.AppSettings[key];
                        if (!String.IsNullOrWhiteSpace(s))
                            result = (T)Convert.ChangeType(s, typeof(T));
                    }
                }
            }
            catch (Exception)
            {
                // Do nothing. We cannot log in this scope, and the validation will 
                // catch this error if needed.
            }

            return result;
        }


        ///// <summary>
        ///// retrieve appSettings by Enum key and provide default value (optional)
        ///// </summary>
        ///// <typeparam name="T">The enum representing the AppSettings keys.</typeparam>
        ///// <param name="key">The key of the appSettings value you are trying to retrieve</param>
        ///// <param name="defaultValue"></param>
        ///// <returns>Value as String</returns>
        //public static string Get<T>(T key, string defaultValue = "")
        //{
        //    return Get(key.ToString(), defaultValue);
        //}

    }
}