﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Reflection;
using System.Text;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date:
*
* Purpose:
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version
* WI 115356 Derek Davison 02/10/2014
                - Change from INI to config.
* WI 130165 DLD 02/19/2014
*   -Determined while working on this item that LogLib was not being used anywhere. commented out. 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib {

    /// <summary></summary>
    public class cLogo {

        private string _LogoPath = string.Empty;
        private string _BackgroundColor = "#000000";
        private Image _LogoImage = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vLogoPath"></param>
        /// <param name="vBackgroundColor"></param>
        public cLogo(string vLogoPath, string vBackgroundColor) {

            _LogoPath = vLogoPath;
            _BackgroundColor = vBackgroundColor;
        }

        /// <summary></summary>
        public string LogoPath {
            get {
                return(_LogoPath);
            }
            set { _LogoPath = value; }
        }

        /// <summary></summary>
        public Color BackgroundColor {
            get {
                try
                {
                    return ColorTranslator.FromHtml(_BackgroundColor);
                }
                catch
                {
                    return ColorTranslator.FromHtml("#000000");
                }
            }            
        }        

        /// <summary></summary>
        public Image LogoImage {
            get {
                // WI 131640 : Better Exception Handling.
                if(_LogoImage == null 
                    && !string.IsNullOrEmpty(LogoPath)
                    && File.Exists(LogoPath)) {
                    try {
                        _LogoImage = Image.FromFile(this.LogoPath);
                    } catch(Exception ex) {
                        // WI 131640 : Additional exception Logging.
                        cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                        _LogoImage = null;
                    }
                }
                return(_LogoImage);
            }
        }
    }
    ///// <summary></summary>
    //public class LogoLib {

    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="Logo"></param>
    //    /// <returns></returns>
    //    //public static bool GetLogo(out cLogo Logo) {

    //    //    string strLogoPath;
    //    //    //string strBackgroundColor;
    //    //    int intBackgroundColor;

    //    //    //WfsINI myLogoINI;

    //    //    try {

    //    //        // Logo.ini
    //    //        //myLogoINI = new WfsINI("Logo", true);

    //    //        // WI 115356 - Derek - Moving ini settings to app.config file
    //    //        //strLogoPath = myLogoINI.GetINIValue("LOGO", "LogoPath", "");
    //    //        strLogoPath = cAppSettingsLib.Get(ExtractAppSettings.LogoPath);
    //    //        intBackgroundColor = cAppSettingsLib.Get<int>(ExtractAppSettings.BackgroundColor, 0);
    //    //        //strLogoPath = INILib.IniReadString("LOGO", "LogoPath", string.Empty);
    //    //        //strBackgroundColor = myLogoINI.GetINIValue("LOGO", "BackgroundColor", "0");
    //    //        //strBackgroundColor = INILib.IniReadString("LOGO", "BackgroundColor", "0");

    //    //        //if (!int.TryParse(strBackgroundColor, out intBackgroundColor))
    //    //        //    intBackgroundColor = 0;

    //    //        Logo = new cLogo(strLogoPath, intBackgroundColor); 

    //    //        return(true);

    //    //    } catch(Exception) {
    //    //        Logo = null;
    //    //        return(false);
    //    //    }
    //    //}

    //    /// <summary>
    //    /// I don't detect that this method is called from anywhere. 
    //    /// Commenting out this procedure because it attempts to write to an .ini, which we are eliminating.
    //    /// 20140207 - Derek - Beacon Technologies
    //    /// </summary>
    //    /// <param name="Logo"></param>
    //    /// <returns></returns>
    //    //public static bool SaveLogo(cLogo Logo) {

    //    //    string strAssemblyLocation;
    //    //    string strAssemblyPath;
    //    //    string strDefaultIniPath;
    //    //    string strLogoIniPath;

    //    //    //cINIFileInterop WinINI;
    //    //    cINIFileInterop LogoINI;

    //    //    bool bolError;

    //    //    try {

    //    //        //// Win.ini
    //    //        //WinINI = new cINIFileInterop(@"WIN.INI");

    //    //        // Logo.ini
    //    //        if(Assembly.GetEntryAssembly() == null) {
    //    //            strAssemblyLocation = Assembly.GetCallingAssembly().Location;
    //    //        } else {
    //    //            strAssemblyLocation = Assembly.GetEntryAssembly().Location;
    //    //        }

    //    //        strAssemblyPath = strAssemblyLocation.Substring(0, strAssemblyLocation.LastIndexOf(@"\"));
    //    //        strDefaultIniPath = Path.Combine(strAssemblyPath, @"..\Ini");

    //    //        //strLogoIniPath = Path.Combine(WinINI.IniReadString("DMP", "NetParamFName", strDefaultIniPath), "Logo.ini");
    //    //        strLogoIniPath = logoInipath;

    //    //        try {
    //    //            if((File.GetAttributes(logoInipath) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
    //    //                // Show the file.
    //    //                File.SetAttributes(logoInipath, File.GetAttributes(strLogoIniPath) & ~FileAttributes.ReadOnly);
    //    //                Console.WriteLine("The {0} file is no longer read-only.", strLogoIniPath);
    //    //            } 
    //    //            bolError = false;
    //    //        } catch(Exception) {
    //    //            // Unable to remove read-only flag.
    //    //            bolError = true;
    //    //        }

    //    //        if(!bolError) {
    //    //            // TODO - WI 115356 - Derek - Moving ini settings to app.config file
    //    //            LogoINI = new cINIFileInterop(strLogoIniPath);
    //    //            LogoINI.IniWriteValue("LOGO", "LogoPath", Logo.LogoPath);
    //    //            LogoINI.IniWriteValue("LOGO", "BackgroundColor", ColorTranslator.ToWin32(Logo.BackgroundColor).ToString());
    //    //        }

    //    //    } catch(Exception) {
    //    //        Logo = null;
    //    //        bolError = true;
    //    //    }

    //    //    return(!bolError);
    //    //}

    //    /// <summary>
    //    /// Returns the path of the INI file that the executing assembly will use.
    //    /// </summary>
    //    private static string logoInipath
    //    {
    //        get{ 
    //            string strFileName;
    //            // TODO: bresheske - Why have ini file location in both app settings and CS CommonLib?
    //            if(System.IO.File.Exists(System.IO.Path.Combine(CommonLib.AppPath, CommonLib.LOGO_INIFILE_NAME))) {
    //                strFileName = System.IO.Path.Combine(CommonLib.AppPath, CommonLib.LOGO_INIFILE_NAME);
    //            } else if(
    //                !string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["logoIni"]) && 
    //                File.Exists(System.Configuration.ConfigurationManager.AppSettings["logoIni"])) {

    //                strFileName = System.Configuration.ConfigurationManager.AppSettings["logoIni"];
    //            } else {
    //                strFileName=System.Configuration.ConfigurationManager.AppSettings["localIni"];
    //            }

    //            return(strFileName);
    //        }
    //    }
    //}
}
