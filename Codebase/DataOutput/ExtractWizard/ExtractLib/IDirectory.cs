﻿namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractLib
{
    public interface IDirectory
    {
        bool Exists(string directory);
        void CreateDirectory(string directory);
    }
}