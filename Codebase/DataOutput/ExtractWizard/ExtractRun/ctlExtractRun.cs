using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Linq;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using System.Data.SqlClient;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 130165 DLD 02/19/2014
*   -Expanded Exception handling and message logging. 
* WI 130642 DLD 02/24/2014 
*   -Replacing CommonLib.CRLF with Environment.NewLine
* WI 130811 BLR 02/28/2014
*   - Added a 'View Results' button to the frmCompleted.
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added additional logs to catch blocks.
* WI 130644 BLR 03/18/2014  
*   - Added additional error messages for SQL failures 
*     (due to invalid def input).
* WI 130364 BLR 03/20/2014
*   - Changed 'error' icon to 'info' icon for successful runs. 
* WI 137747 BLR 05/16/2014
*   - Bit the bullet and just filted the alerts more by contains on
*     "Attempting retry", there seems to be to many variations. 
* WI 152321 BLR 08/15/2014
*   - Moved configuration settings over to ExtractConfigurationSettings.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {

    public partial class ctlExtractRun : UserControl {

        #region Local Variables
        private ExtractConfigurationSettings _Settings = null;
        private int _UserID = -1;
        
        private ltaLog _ExceptionLog = null;
        private ArrayList _Errors = null;
        private ArrayList _Messages = null;

        private string _ExtractFileName = string.Empty;

        private string _CompletedExtractFileName = string.Empty;
        private string _CompletedLogFilePath = string.Empty;
        
        private string _ExtractScriptPath = string.Empty;
        private string _ImagePath = string.Empty;

        private Thread _ProcessThread = null;
        private System.Windows.Forms.Timer _Timer = new System.Windows.Forms.Timer();
        
        private bool _BankIDLocked = false;
        private bool _ClientAccountIDLocked = false;
        private bool _BatchIDLocked = false;
        private bool _DepositDateLocked = false;
        private bool _ProcessingDateLocked = false;
        
        //private bool _AbortPressed = false;
        
        #endregion Local Variables

        /// <summary>
        /// EventHandler to output status messages.
        /// </summary>
        public event OutputStatusMsgEventHandler OutputStatusMsg;
        public event BeginExtractRunEventHandler BeginExtractRun;
        public event EndExtractRunEventHandler EndExtractRun;
        
        private cExtractDef _ExtractDef;

        #region Constructor
        public ctlExtractRun() {
            InitializeComponent();
        }

        public ctlExtractRun(
            cExtractParms parms, 
            bool runAsTest) {

            cExtractDef objExtractDef;

            InitializeComponent();

            InitializeControl();

            if(parms != null && parms.SetupFile.Length > 0 && File.Exists(parms.SetupFile)) {

                if(LoadExtractDef(parms.SetupFile, out objExtractDef)) {

                    LoadExtractParms(objExtractDef, parms, runAsTest);
                }
            }
        }

        public ctlExtractRun(
            cExtractDef extractDef, 
            cExtractParms parms, 
            bool runAsTest) {

            InitializeComponent();

            InitializeControl();

            extractDef.OutputError += OnOutputError;
            extractDef.OutputStatusMsg += OnOutputStatusMessage;

            LoadExtractParms(extractDef, parms, runAsTest);
        }

        #endregion Constructor

        #region private object properties
        private ltaLog ExceptionLog {
            get {
                if(_ExceptionLog == null) {
                    _ExceptionLog = new ltaLog(
                        Settings.ExceptionLogFile,
                        Settings.LogFileMaxSizeInt,
                        LTAMessageImportance.Verbose );
                }
                return(_ExceptionLog);
            }
        }

        private ExtractConfigurationSettings Settings {
            get {
                if(_Settings == null) {
                    _Settings = new ExtractConfigurationSettings(new ConfigurationManagerProvider());
                    _Settings.OutputError += OnOutputError;
                    _Settings.OutputMessage += OnExtractOutputMessage;
                }
                return(_Settings);
            }
        }
        #endregion private object properties

        #region Public Properties
        public long[] BankID {
            get {
                List<long> arBanks = new List<long>();
                foreach(long lngBankID in cExtractParms.ParseLongs(this.txtBankID.Text)) {
                    if(!arBanks.Contains(lngBankID)) {
                        arBanks.Add(lngBankID);
                    }
                }
                return(arBanks.ToArray());
            }
        }

        public long[] LockboxID {
            get {
                List<long> arLockboxes = new List<long>();
                foreach(long lngLockboxID in cExtractParms.ParseLongs(this.txtLockboxID.Text)) {
                    if(!arLockboxes.Contains(lngLockboxID)) {
                        arLockboxes.Add(lngLockboxID);
                    }
                }
                return(arLockboxes.ToArray());
            }
        }

        public long[] BatchID {
            get {
                List<long> arBatches = new List<long>();
                foreach(long lngBatchID in cExtractParms.ParseLongs(this.txtBatchID.Text)) {
                    if(!arBatches.Contains(lngBatchID)) {
                        arBatches.Add(lngBatchID);
                    }
                }
                return(arBatches.ToArray());
            }
        }

        public DateTime DepositDateFrom {
            get {
            
                DateTime dteDepositDateFrom;
                DateTime dteDepositDateTo;
            
                if(cExtractParms.ParseDateString(this.txtDepositDate.Text, out dteDepositDateFrom, out dteDepositDateTo)) {
                    return(dteDepositDateFrom);
                } else {
                    return(DateTime.MinValue);
                }
            }
        }

        public DateTime DepositDateTo {
            get {
            
                DateTime dteDepositDateFrom;
                DateTime dteDepositDateTo;
            
                if(cExtractParms.ParseDateString(this.txtDepositDate.Text, out dteDepositDateFrom, out dteDepositDateTo)) {
                    return(dteDepositDateTo);
                } else {
                    return(DateTime.MinValue);
                }
            }
        }

        public DateTime ProcessingDateFrom {
            get {
            
                DateTime dteProcessingDateFrom;
                DateTime dteProcessingDateTo;
            
                if(cExtractParms.ParseDateString(this.txtProcessingDate.Text, out dteProcessingDateFrom, out dteProcessingDateTo)) {
                    return(dteProcessingDateFrom);
                } else {
                    return(DateTime.MinValue);
                }
            }
        }

        public DateTime ProcessingDateTo {
            get {
            
                DateTime dteProcessingDateFrom;
                DateTime dteProcessingDateTo;
            
                if(cExtractParms.ParseDateString(this.txtProcessingDate.Text, out dteProcessingDateFrom, out dteProcessingDateTo)) {
                    return(dteProcessingDateTo);
                } else {
                    return(DateTime.MinValue);
                }
            }
        }

        public bool RunAsTest {
            get {
                return(this.chkRunAsTest.Checked);
            }
        }

        public string CompletedLogFilePath {
            get {
                return(_CompletedLogFilePath);
            }
        }

        public string CompletedExtractFileName {
            get {
                return(_CompletedExtractFileName);
            }
        }

        #endregion Public Properties

        public void Clear() {
            InitializeControl();
        }

        #region Message Formatting
        private string GetErrorString() {
            
            string strRetVal = string.Empty;
            
            if(_Errors != null) {
                for(int i=0;i<_Errors.Count;++i) {
                    strRetVal += _Errors[i];
                    if(i<_Errors.Count -1) {
                        strRetVal += "\n\n";
                    }
                }
            }

            return(strRetVal);
        }

        private string GetMessageString() {
            
            string strRetVal = string.Empty;
            
            if(_Messages != null) {
                for(int i=0;i<_Messages.Count;++i) {
                    strRetVal += _Messages[i];
                    if(i<_Messages.Count -1) {
                        strRetVal += "\n\n";
                    }
                }
            }

            return(strRetVal);
        }        
        #endregion Message Formatting

        #region Raise Event Methods

        private void OnExtractOutputMessage(
            string message,
            string src,
            MessageType messageType,
            MessageImportance messageImportance) {

            if(this.InvokeRequired) {
                outputMessageEventHandler d = new outputMessageEventHandler(OnExtractOutputMessage);
                this.Invoke(d, new object[] {message, src, messageType, messageImportance});
            } else {
                if(messageImportance == MessageImportance.Essential) {
                    switch(messageType) {
                        case MessageType.Information:

                            if(_Messages == null) {
                                _Messages = new ArrayList();
                            }

                            _Messages.Add(message);

                            break;
                        case MessageType.Warning:
                            // DLD - WI 130165
                            // WI 137747 - Removing the 'Attempt retry' messages.
                            if (Environment.UserInteractive && !message.Contains("Attempting retry"))
                                MessageBox.Show(message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            cCommonLogLib.LogMessageLTA(message, (LTAMessageImportance)messageImportance);
                            break;
                        case MessageType.Error:
                            OnOutputError(this, new Exception(message));
                            break;
                    }
                }
            }
        }

        private void OnOutputStatusMessage(string statusMsg) {

            if(this.InvokeRequired) {
                OutputStatusMsgEventHandler d = new OutputStatusMsgEventHandler(OnOutputStatusMessage);
                this.Invoke(d, new object[] {statusMsg});
            } else {
                if(OutputStatusMsg != null) {
                    OutputStatusMsg(statusMsg);
                }
                Application.DoEvents();
            }
        }

        private void OnImageStatusChanged(int currentItem) {

            if(this.pgsMain.InvokeRequired) {
                ImageStatusChangedEventHandler d = new ImageStatusChangedEventHandler(OnImageStatusChanged);
                this.Invoke(d, new object[] {currentItem});
            } else {
                if(currentItem > 0 && currentItem <= this.pgsMain.Maximum) {
                    this.pgsMain.Value = currentItem;
                }
                Application.DoEvents();
            }
        }

        private void OnImageStatusInitialized(int maxItems) {

            if(this.pgsMain.InvokeRequired) {           
                ImageStatusInitializedEventHandler d = new ImageStatusInitializedEventHandler(OnImageStatusInitialized);
                this.Invoke(d, new object[] {maxItems});
            } else {
                if(maxItems > 0) {
                    this.pgsMain.Visible = true;
                    this.pgsMain.Maximum = maxItems;
                    this.pgsMain.Value = 0;
                } else {
                    this.pgsMain.Maximum = 100;
                    this.pgsMain.Value = 0;
                    this.pgsMain.Visible = false;
                }
            }
            Application.DoEvents();
        }

        private void OnPromptForManualInput(ref Dictionary<string, string> manualInputValues) {
            frmManualInput objManualInput = new frmManualInput(manualInputValues);
            objManualInput.ShowDialog();
            if(!objManualInput.Cancelled) {
                manualInputValues = objManualInput.formInputValues;
            }
        }

        private void OnOutputError(object sender, Exception e) {
            OnOutputError(e);
        }

        private void OnOutputError(Exception e) {

            if(this.InvokeRequired) {

                outputErrorEventHandler d = new outputErrorEventHandler(OnOutputError);
                this.Invoke(d, new object[] {e});
            
            } else {
                if(_Errors == null) {
                    _Errors = new ArrayList();
                }

                if(e.StackTrace == null) {
                    _Errors.Add(e.Message);
                } else {
                    _Errors.Add(e.StackTrace.TrimStart() + "\n\n" + e.Message);
                }
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
        }

        private void OnExtractCompleted(
            bool isSuccess,
            string extractFileName,
            string logFilePath,
            Guid extractAuditID,
            StringDictionary extractStats)
        {

            // DLD - WI 130165 -  remove stack trace from user message box. errors are captured and logged below. "friendlier" message get displayed to user
            string strMsg;

            if (this.InvokeRequired)
            {

                cExtractRunProcess.ExtractCompletedEventHandler d = new cExtractRunProcess.ExtractCompletedEventHandler(OnExtractCompleted);
                this.Invoke(d, new object[] {
                    isSuccess, 
                    extractFileName, 
                    logFilePath, 
                    extractAuditID, 
                    extractStats
                });

            }
            else
            {
                BuildExtractRunInfo(_ExtractDef);

                if (isSuccess && this.Settings.ExtractStatsColumns.Count > 0)
                {
                    strMsg = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fnil\\fcharset0 Lucida Console;}}\r\n\\viewkind4\\uc1\\pard\\f0\\fs18";
                    if (_Messages != null && _Messages.Count > 0)
                    {
                        strMsg += GetMessageString().Replace(@"\", @"\\") + "\\par\\par\r\n";
                        _Messages = null;
                    }
                    else
                    {
                        strMsg += "Extract Complete.\\par\\par\r\n";
                    }

                    strMsg += Environment.NewLine;
                    strMsg += Environment.NewLine;

                    foreach (var c in this.Settings.ExtractStatsColumns)
                    {
                        var str = c.Name;
                        strMsg += str.PadLeft(25, ' ') + ": ";
                        if (extractStats.ContainsKey(str))
                        {
                            //strMsg += "{\b ";
                            strMsg += "\\b " + extractStats[str].PadRight(20, ' ') + "\\b0";
                            //strMsg += "}";
                        }
                        strMsg += "\\par\r\n";
                    }

                    strMsg += "}";
                    
                    // WI 130811 BLR 02/28/2014 - Added a 'View Results' button to the frmCompleted.
                    frmExtractCompleted.ShowForm(strMsg, _ExtractFileName);
                }
                else if (isSuccess)
                {
                    strMsg = GetMessageString();
                    // WI 130364 : Changed error to info, as this is a successful run.
                    if (Environment.UserInteractive)
                        MessageBox.Show(this, strMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (_Errors != null && _Errors.Count > 0)
                    {
                        // WI 130644 : Some better messages, rather than 'UNHANDLED'. 
                        if (IsSQLError(_Errors))
                        {
                            strMsg = ExtractLib.Constants.ERROR_MSG_SQL_FAILURE;
                            // Append a small 'limit clause' if we have some limit fields.
                            if (_ExtractDef.AllLimitItems != null && _ExtractDef.AllLimitItems.Any())
                                strMsg += " and limit data values";
                            strMsg += ".";
                        }
                        else if (IsConnectionStringError(_Errors))
                            strMsg = ExtractLib.Constants.ERROR_MSG_SQL_CONNECTION_SRING;
                        else if (IsEncryptedUsernameError(_Errors))
                            strMsg = ExtractLib.Constants.ERROR_MSG_SQL_ENCRYPTED_USERNAME;
                        else
                            strMsg = ExtractLib.Constants.ERROR_MSG_UNHANDLED;

                        cCommonLogLib.LogMessageLTA(GetErrorString(), LTAMessageImportance.Essential);
                    }
                    else
                    {
                        strMsg = "Extract failed.";
                        OnOutputStatusMessage(strMsg);
                    }

                    if (Environment.UserInteractive)
                        MessageBox.Show(this, strMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                this.pgsMain.Value = 0;
                this.pgsMain.Visible = false;

                _CompletedExtractFileName = extractFileName;
                _CompletedLogFilePath = logFilePath;

                if (EndExtractRun != null)
                    EndExtractRun();

                _Settings = null;
                _Errors = null;
            }
        }

        /// <summary>
        /// WI 130644
        /// Takes in the list of bubbled-up errors and checks to see if we have an connection string
        /// encrypted username error.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public bool IsEncryptedUsernameError(ArrayList errors)
        {
            return errors.Cast<string>()
                .Any(x => x.StartsWith("at WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.CommonLib.DecryptUserIdInConnectionString(")
                       && x.Contains("The UserId and Password values must be encrypted"));
        }

        /// <summary>
        /// WI 130644
        /// Takes in the list of bubbled-up errors and checks to see if we have an SQL Connection error.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        public bool IsConnectionStringError(ArrayList errors)
        {
            return errors.Cast<string>()
                .Any(x => x.StartsWith("at System.Data.SqlClient.SqlConnectionStringBuilder.set_Item(String keyword, Object value)"));
        }

        /// <summary>
        /// WI 130644
        /// Takes in the list of bubbled-up errors and checks to see if we have an SQL error.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        private bool IsSQLError(ArrayList errors)
        {
            return errors.Cast<string>()
                .Any(x => x.StartsWith("at System.Data.SqlClient.SqlConnection.OnError")
                       || x.StartsWith("An Error occurred while creating SqlDataReader"));
        }

        #endregion Raise Event Methods

        #region Load Extract Parms
        public bool LoadExtractParms(
            cExtractParms parms, 
            bool runAsTest) {

            cExtractDef objExtractDef;

            if(parms != null && parms.SetupFile.Length > 0 && File.Exists(parms.SetupFile)) {

                if(LoadExtractDef(parms.SetupFile, out objExtractDef)) {

                    return(LoadExtractParms(objExtractDef, parms, runAsTest));
                } else {
                    return(false);
                }
            } else {
                return(false);
            }
        }

        public bool LoadExtractParms(
            cExtractDef extractDef,
            cExtractParms parms, 
            bool runAsTest) {

            bool bolRetVal;

            if(parms != null) {

                _ExtractScriptPath = parms.SetupFile;

                if(parms.UserID > 0) {
                    _UserID = parms.UserID;
                }

                if(parms.TargetFile.Length > 0) {
                    _ExtractFileName = parms.TargetFile;
                } else {
                    _ExtractFileName = extractDef.ExtractPath;
                }

                if(parms.ImagePath.Length > 0) {
                    _ImagePath = parms.ImagePath;
                } else {
                    _ImagePath = extractDef.ImagePath;
                }

                // BankID
                if(extractDef.DefaultBankID.Length > 0) {
                    this.txtBankID.Text = FlattenLongs(extractDef.DefaultBankID);
                    _BankIDLocked = this.txtBankID.Text.Length > 0;
                } else if(parms.BankIDString.Length > 0) {
                    this.txtBankID.Text = parms.BankIDString;
                    _BankIDLocked = false;
                } else {
                    this.txtBankID.Text = string.Empty;
                    _BankIDLocked = false;
                }
                this.txtBankID.Enabled = !_BankIDLocked;
                
                // LockboxID
                if(extractDef.DefaultLockboxID.Length > 0) {
                    this.txtLockboxID.Text = FlattenLongs(extractDef.DefaultLockboxID);
                    _ClientAccountIDLocked = this.txtLockboxID.Text.Length > 0;
                } else if(parms.LockboxIDString.Length > 0) {
                    this.txtLockboxID.Text = parms.LockboxIDString;
                    _ClientAccountIDLocked = false;
                } else {
                    this.txtLockboxID.Text = string.Empty;
                    _ClientAccountIDLocked = false;
                }
                this.txtLockboxID.Enabled = !_ClientAccountIDLocked;

                // BatchID
                if(extractDef.DefaultBatchID.Length > 0) {
                    this.txtBatchID.Text = FlattenLongs(extractDef.DefaultBatchID);
                    _BatchIDLocked = this.txtBatchID.Text.Length > 0;
                } else if(parms.BatchIDString.Length > 0) {
                    this.txtBatchID.Text = parms.BatchIDString;
                    _BatchIDLocked = false;
                } else {
                    this.txtBatchID.Text = string.Empty;
                    _BatchIDLocked = false;
                }
                this.txtBatchID.Enabled = !_BatchIDLocked;

                // DepositDate
                if(extractDef.DefaultDepositDate > DateTime.MinValue) {
                    this.txtDepositDate.Text = extractDef.DefaultDepositDate.ToString("MM/dd/yyyy");
                    _DepositDateLocked = this.txtDepositDate.Text.Length > 0;
                } else if(parms.DepositDateFrom > DateTime.MinValue) {
                    this.txtDepositDate.Text = parms.DepositDateFrom.ToString("MM/dd/yyyy");
                    if(parms.DepositDateTo > DateTime.MinValue && parms.DepositDateFrom != parms.DepositDateTo) {
                        this.txtDepositDate.Text += "-" + parms.DepositDateTo.ToString("MM/dd/yyyy");
                    }
                    _DepositDateLocked = false;
                } else if(parms.DepositDateTo > DateTime.MinValue) {
                    this.txtDepositDate.Text = parms.DepositDateTo.ToString("MM/dd/yyyy");
                    _DepositDateLocked = false;
                } else {
                    this.txtDepositDate.Text = string.Empty;
                    _DepositDateLocked = false;
                }
                this.txtDepositDate.Enabled = !_DepositDateLocked;

                // ProcessingDate
                if(extractDef.DefaultProcessingDate > DateTime.MinValue) {
                    this.txtProcessingDate.Text = extractDef.DefaultProcessingDate.ToString("MM/dd/yyyy");
                    _ProcessingDateLocked = this.txtProcessingDate.Text.Length > 0;
                } else if(parms.ProcessingDateFrom > DateTime.MinValue) {
                    this.txtProcessingDate.Text = parms.ProcessingDateFrom.ToString("MM/dd/yyyy");
                    if(parms.ProcessingDateTo > DateTime.MinValue && parms.ProcessingDateFrom != parms.ProcessingDateTo) {
                        this.txtProcessingDate.Text += "-" + parms.ProcessingDateTo.ToString("MM/dd/yyyy");
                    }
                    _ProcessingDateLocked = false;
                } else if(parms.ProcessingDateTo > DateTime.MinValue) {
                    this.txtProcessingDate.Text = parms.ProcessingDateTo.ToString("MM/dd/yyyy");
                    _ProcessingDateLocked = false;
                } else {
                    this.txtProcessingDate.Text = string.Empty;
                    _ProcessingDateLocked = false;
                }
                this.txtProcessingDate.Enabled = !_ProcessingDateLocked;


                this.chkRunAsTest.Checked = runAsTest;

                BuildExtractRunInfo(extractDef);
                
                _ExtractDef = extractDef;
                
                SetCheckBoxState(true);
                
                bolRetVal = true;
               
            } else {
                _ExtractDef = null;

                SetCheckBoxState(false);

                bolRetVal = false;
            }

            return(bolRetVal);
        }

        private void BuildExtractRunInfo(cExtractDef extractDef) {

            cExtractRunInfoGroup objRunInfoGroup;

            if(extractDef.TimeStamp.Length > 0) {
                this.grpRunOptions.Text = "Run Options  Trace Field: " + extractDef.TimeStamp;

                this.lstExtractRunHistory.Items.Clear();

                if(BuildExtractRunInfoGroup(extractDef.TimeStamp, out objRunInfoGroup) && 
                   objRunInfoGroup != null && 
                   objRunInfoGroup.PreviousRuns.Length > 0) {

                    this.lstExtractRunHistory.Items.Add(objRunInfoGroup);
                }
            } else {
                this.grpRunOptions.Text = "Run Options - No Trace Field Defined";
            }
        }

        private bool BuildExtractRunInfoGroup(
            string timeStamp, 
            out cExtractRunInfoGroup extractRunInfoGroup) {

            cExtract objExtract;
            cExtractRunInfo[] arRunInfo;
            cExtractRunInfoGroup objRunInfoGroup;

            objExtract = new cExtract();
            objExtract.OutputError += OnOutputError;
            objExtract.OutputMessage += OnExtractOutputMessage;

            arRunInfo = objExtract.GetBatchRunHistory(timeStamp);

            objExtract.Dispose();

            objRunInfoGroup = null;
            foreach(cExtractRunInfo extractruninfo in arRunInfo) {           
                if(extractruninfo.RunDate > DateTime.MinValue) {
                    if(objRunInfoGroup == null) {
                        objRunInfoGroup = new cExtractRunInfoGroup(timeStamp, extractruninfo.RunDate);
                        objRunInfoGroup.Add(extractruninfo);
                    } else {
                        if(extractruninfo.RunDate == objRunInfoGroup.RunDate) {
                            objRunInfoGroup.Add(extractruninfo);
                        } else {
                            this.lstExtractRunHistory.Items.Add(objRunInfoGroup);
                            objRunInfoGroup = new cExtractRunInfoGroup(timeStamp, extractruninfo.RunDate);
                            objRunInfoGroup.Add(extractruninfo);
                        }
                    }
                }
            }
            
            extractRunInfoGroup = objRunInfoGroup;
            
            return(true);
        }

        public string ExtractPath {
            get {
                return(_ExtractFileName);
            }
            set {
                _ExtractFileName = value;
            }
        }

        public string ImagePath {
            get {
                return(_ImagePath);
            }
            set {
                _ImagePath = value;
            }
        }
        #endregion Load Extract Parms

        #region Load ExtractDef
        private bool LoadExtractDef(
            string extractDefFileName, 
            out cExtractDef extractDef) {

            cExtractDef objExtractDef;
            string strMsg;
            bool bolExtractDefFileIsValid;
            
            if(extractDefFileName.Length == 0) {
                extractDef = null;
                return(false);
            }
            
            if(!File.Exists(extractDefFileName)) {
                if (Environment.UserInteractive)
                    MessageBox.Show("Could not locate ExtractDef file :[" + extractDefFileName + "].", 
                                "Warning", 
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Warning);
                extractDef = null;
                return(false);
            }

            objExtractDef = new cExtractDef(extractDefFileName);
            objExtractDef.OutputError += this.OnOutputError;
            objExtractDef.OutputStatusMsg += this.OnOutputStatusMessage;

            if(extractDefFileName.ToLower().EndsWith(".cxs")) {
                bolExtractDefFileIsValid = objExtractDef.ParseExtractDefFile(extractDefFileName);
            } else {
                bolExtractDefFileIsValid = objExtractDef.ParseExtractDefXmlFile(extractDefFileName);
            }

            if(bolExtractDefFileIsValid) {

                extractDef = objExtractDef;

            } else {

                if(_Errors != null && _Errors.Count > 0) {
                    strMsg = "Could not parse ExtractDef file [" + extractDefFileName + "]: " + GetErrorString();
                    _Errors = null;
                } else {
                    strMsg = "Could not parse ExtractDef file [" + extractDefFileName + "].";
                }
                if (Environment.UserInteractive)
                    MessageBox.Show(strMsg, "Parse ExtractDef File Failed", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                extractDef = null;
            }

            if(_Errors != null && _Errors.Count > 0) {
                // DLD - WI 130165
                if (Environment.UserInteractive)
                    MessageBox.Show(ExtractLib.Constants.ERROR_MSG_PROCESS_FAILED, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                cCommonLogLib.LogMessageLTA(GetErrorString(), LTAMessageImportance.Verbose);
                _Errors = null;
                return(false);
            } else {
                return(true);
            }
        }
        #endregion Load ExtractDef

        #region Array Flattening
        private static string FlattenLongs(long[] longArray) {

            string strRetVal = string.Empty;

            if(longArray.Length > 0) {
                for(int i=0;i<longArray.Length;++i) {
                    strRetVal += longArray[i].ToString();
                    if(i < longArray.Length - 1) {
                        strRetVal += ", ";
                    }
                }
            }
            
            return(strRetVal);
        }
        #endregion Array Flattening


        // ********************************************
        // * Form State Management
        // ********************************************

        #region Control State Management
        private void InitializeControl() {

            this.grpRunOptions.Text = "Run Options";

            this.txtBankID.Text = string.Empty;
            this.txtLockboxID.Text = string.Empty;
            this.txtBatchID.Text = string.Empty;           
            this.txtDepositDate.Text = string.Empty;
            this.txtProcessingDate.Text = string.Empty;
            
            this.chkRerun.Checked = false;

            this.lstExtractRunHistory.Items.Clear();

            _UserID = -1;
            _Errors = null;
            _Messages = null;
            _ExtractScriptPath = string.Empty;
            _ImagePath = string.Empty;
            _ExtractFileName = string.Empty;

            _CompletedExtractFileName = string.Empty;
            _CompletedLogFilePath = string.Empty;
        }

        private void SetControlState(bool controlIsEnabled) {

            if(controlIsEnabled) {
                this.btnGo.Enabled = true;
                this.btnAbort.Enabled = false;
                this.txtBankID.Enabled = !_BankIDLocked;
                this.txtLockboxID.Enabled = !_ClientAccountIDLocked;
                this.txtBatchID.Enabled = !_BatchIDLocked;
                this.txtDepositDate.Enabled = !_DepositDateLocked;
                this.txtProcessingDate.Enabled = !_ProcessingDateLocked;
            } else {
                this.btnGo.Enabled = false;
                this.btnAbort.Enabled = true;
                this.txtBankID.Enabled = false;
                this.txtLockboxID.Enabled = false;
                this.txtBatchID.Enabled = false;
                this.txtDepositDate.Enabled = false;
                this.txtProcessingDate.Enabled = false;
            }

            SetCheckBoxState(controlIsEnabled);
        }
        
        private void SetCheckBoxState(bool controlIsEnabled) {

            if(_ExtractDef == null || _ExtractDef.TimeStamp == string.Empty) {

                this.chkRunAsTest.Enabled = false;
                this.chkRerun.Enabled = false;
                this.lstExtractRunHistory.Enabled = false;
                this.btnBatchInfo.Enabled = false;

                this.chkRerun.Checked = false;

                this.lstExtractRunHistory.Items.Clear();

            } else if(controlIsEnabled) {

                this.chkRunAsTest.Enabled = true;
                
                if(this.chkRunAsTest.Checked) {
                    this.chkRerun.Enabled = false;
                    this.lstExtractRunHistory.Enabled = false;
                    this.btnBatchInfo.Enabled = false;

                    this.chkRerun.Checked = false;

                    // Uncheck all items!
                    foreach(int i in lstExtractRunHistory.CheckedIndices) {
                        lstExtractRunHistory.SetItemCheckState(i, CheckState.Unchecked);
                    }
                    
                    lstExtractRunHistory.ClearSelected();

                } else {
                    this.chkRerun.Enabled = true;                   
                    if(this.chkRerun.Checked) {
                        this.lstExtractRunHistory.Enabled = true;
                        this.btnBatchInfo.Enabled = (this.lstExtractRunHistory.SelectedItems.Count > 0);

                    } else {
                        this.lstExtractRunHistory.Enabled = false;
                        this.btnBatchInfo.Enabled = false;
                    }
                }
            } else {
                this.chkRunAsTest.Enabled = false;
                this.chkRerun.Enabled = false;
                this.lstExtractRunHistory.Enabled = false;
                this.btnBatchInfo.Enabled = false;
            }
        }
        
        #endregion Control State Management

        // ********************************************
        // * Event Handlers
        // ********************************************

        #region ctlExtractRun_Load
        private void ctlExtractRun_Load(object sender, EventArgs e) {

            SetControlState(true);
        }
        #endregion ctlExtractRun_Load

        #region Button Event Handlers
        private void btnBatchInfo_Click(object sender, EventArgs e) {

            if(this.lstExtractRunHistory.SelectedItem == null) {
                if (Environment.UserInteractive)
                    MessageBox.Show("Please make a selection", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            } else {
                string strTemp = string.Empty;
                
                cExtractRunInfoGroup grp = (cExtractRunInfoGroup)this.lstExtractRunHistory.SelectedItem;
                frmBatchTraceInfo ExtractDescriptorInfo = new frmBatchTraceInfo(grp);
                ExtractDescriptorInfo.ShowDialog(this);
                ExtractDescriptorInfo.Dispose();
            }
        }

        private void btnGo_Click(object sender, EventArgs e) {

            RunExtract();
        }
        #endregion Button Event Handlers

        private void RunExtract() {

            cExtractDef objExtractDef;

            if(_ExtractDef == null) {
                // Build the ExtractDef object.
                objExtractDef = new cExtractDef(_ExtractScriptPath);
                objExtractDef.OutputError += OnOutputError;
                objExtractDef.OutputStatusMsg += OnOutputStatusMessage;

                if(objExtractDef.ParseExtractDefFile(_ExtractScriptPath)) {                
                    RunExtract(objExtractDef);
                }

            } else {                         
                RunExtract(_ExtractDef);
            }                    
        }

        private void RunExtract(cExtractDef extractDef) {

            Guid gidExtractAuditID = Guid.Empty;
            ArrayList arRerunList;
            cExtractParms parms;

            try {

                // Build Parms object.
                parms = new cExtractParms();
                parms.SetupFile = _ExtractScriptPath;

                parms.TargetFile = this.ExtractPath;
                parms.ImagePath = this.ImagePath;
                parms.BankID = this.BankID;
                parms.LockboxID = this.LockboxID;
                parms.BatchID = this.BatchID;
                parms.DepositDateFrom = this.DepositDateFrom;
                parms.DepositDateTo = this.DepositDateTo;
                parms.ProcessingDateFrom = this.ProcessingDateFrom;
                parms.ProcessingDateTo = this.ProcessingDateTo;

                parms.UserID = _UserID;

                if(this.chkRunAsTest.Checked) {
                    parms.TraceField = string.Empty;
                } else {
                    parms.TraceField = extractDef.TimeStamp;
                }

                arRerunList = new ArrayList();
                foreach(cExtractRunInfoGroup runinfogroup in this.lstExtractRunHistory.CheckedItems) {
                    arRerunList.Add(runinfogroup.RunDate);
                }
                parms.RerunList = (DateTime[])arRerunList.ToArray(typeof(DateTime));

                if (parms.DepositDateFrom == DateTime.MinValue &&
                   parms.DepositDateTo == DateTime.MinValue &&
                   parms.ProcessingDateFrom == DateTime.MinValue &&
                   parms.ProcessingDateTo == DateTime.MinValue &&
                   arRerunList.Count == 0)
                {
                    if (Environment.UserInteractive)
                        MessageBox.Show("At least one valid date value must be specified in order to generate an Extract.",
                                        "Invalid Date Parameter",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation);

                }
                else
                {

                    // Run the Extract
                    StartExtract(extractDef, parms);
                }

            } catch(Exception ex) {
                ExceptionLog.logError(
                    ex,
                    this.GetType().Name,
                    _UserID.ToString());
                OnOutputError(this, ex);
            }
        }

        private void StartExtract(
            cExtractDef extractDef, 
            cExtractParms parms) {

            cExtractRunProcess objExtractRunProcess;

            try {

                Cursor.Current = Cursors.WaitCursor;

                SetControlState(false);

                if(BeginExtractRun != null) {
                    BeginExtractRun();
                }

                objExtractRunProcess = new cExtractRunProcess(extractDef, parms);
                objExtractRunProcess.ExtractCompleted += OnExtractCompleted;

                objExtractRunProcess.OutputError += OnOutputError;
                objExtractRunProcess.OutputMessage += OnExtractOutputMessage;
                objExtractRunProcess.OutputStatusMsg += OnOutputStatusMessage;
                objExtractRunProcess.ImageStatusChanged += OnImageStatusChanged;
                objExtractRunProcess.ImageStatusInitialized += OnImageStatusInitialized;
                objExtractRunProcess.PromptForManualInput += OnPromptForManualInput;

                _ProcessThread = new Thread(new ThreadStart(objExtractRunProcess.RunExtract));

                _ProcessThread.Start();

                _Timer.Interval = 100;
                _Timer.Tick += new EventHandler(Timer_Tick);
                _Timer.Start();

            } catch(Exception ex) {
                OnOutputError(this, ex);

                _Timer.Stop();
            }
        }

        #region Timer Event Handlers
        private void Timer_Tick(object sender, EventArgs e) {

            if(_ProcessThread == null) {
                _Timer.Stop();
                SetControlState(true);
            } else {
                if(!_ProcessThread.IsAlive) {
                    _ProcessThread.Join();
                    _ProcessThread = null;
                    _Timer.Stop();
                    SetControlState(true);
                }
            }
        }
        #endregion Timer Event Handlers


        #region Check Box Event Handlers
        private void chkRunAsTest_CheckedChanged(object sender, EventArgs e) {
            SetCheckBoxState(true);
        }

        private void chkRerun_CheckedChanged(object sender, EventArgs e) {
            SetCheckBoxState(true);
        }
        #endregion Check Box Event Handlers

        private void btnAbort_Click(object sender, EventArgs e) {

            if(_ProcessThread != null) {
                _ProcessThread.Abort();
                OnExtractCompleted(false, string.Empty, string.Empty, Guid.Empty, null);
            }

            SetControlState(true);
        }

        private void lstExtractRunHistory_SelectedValueChanged(object sender, EventArgs e) {
            SetCheckBoxState(true);
        }

        private void txtProcessingDate_KeyDown(object sender, KeyEventArgs e) {
            if(Keys.Return == e.KeyCode) {
                RunExtract();
            }
        }

        private void txtDepositDate_KeyDown(object sender, KeyEventArgs e) {
            if(Keys.Return == e.KeyCode) {
                RunExtract();
            }
        }
    }

    internal class cExtractRunProcess {

        private cExtractDef _ExtractDef;
        private cExtractParms _Parms;

        public event OutputStatusMsgEventHandler OutputStatusMsg;
        public event outputErrorEventHandler OutputError;
        public event outputMessageEventHandler OutputMessage;
        public event ImageStatusChangedEventHandler ImageStatusChanged;
        public event ImageStatusInitializedEventHandler ImageStatusInitialized;
        public event PromptForManualInputEventHandler PromptForManualInput;
        
        public delegate void ExtractCompletedEventHandler(
            bool isSuccess, 
            string extractFileName, 
            string logFilePath, 
            Guid extractAuditID, 
            StringDictionary extractStats);
        
        public ExtractCompletedEventHandler ExtractCompleted;

        public cExtractRunProcess(
            cExtractDef extractDef, 
            cExtractParms parms) {

            _ExtractDef = extractDef;
            _Parms = parms;
        }
        
        public void RunExtract() {

            cExtract objExtract;
            Guid gidExtractAuditID;
            bool bolIsSuccess;
            StringDictionary arExtractStats;

            objExtract = new cExtract();
            objExtract.OutputStatusMsg += OnOutputStatusMsg;
            objExtract.OutputError += OnOutputError;
            objExtract.OutputMessage += OnOutputMessage;
            objExtract.ImageStatusChanged += OnImageStatusChanged;
            objExtract.ImageStatusInitialized += OnImageStatusInitialized;
            objExtract.PromptForManualInput += OnPromptForManualInput;

            bolIsSuccess = (objExtract.RunExtract(
                _ExtractDef, 
                _Parms, 
                out gidExtractAuditID, 
                out arExtractStats) > 0);
            
            if(ExtractCompleted != null) {
                ExtractCompleted(bolIsSuccess, _Parms.ExtractFileName, _Parms.LogFilePath, gidExtractAuditID, arExtractStats);
            }
        } 

        #region Raise Event Methods

        private void OnOutputStatusMsg(string msg) {
            if(OutputStatusMsg != null) {
                OutputStatusMsg(msg);
            }
        }

        private void OnOutputError(System.Exception e) {
            if(OutputError != null) {
                if(e is System.Threading.ThreadAbortException) {
                    //ExceptionOccurred(sender, new Exception("Extract execution was aborted by the User."));
                } else {
                    OutputError(e);
                }
            }
        }

        private void OnOutputError(object sender, System.Exception e) {
            if(OutputError != null) {
                if(e is System.Threading.ThreadAbortException) {
                    //ExceptionOccurred(sender, new Exception("Extract execution was aborted by the User."));
                } else {
                    OutputError(e);
                }
            }
        }

        private void OnOutputMessage(
            string message,
            string src,
            MessageType messageType,
            MessageImportance messageImportance) {

            if(OutputMessage != null) {
                OutputMessage(
                    message,
                    src,
                    messageType,
                    messageImportance);
            }
        }

        private void OnImageStatusChanged(int currentItem) {
            if(ImageStatusChanged != null) {
                ImageStatusChanged(currentItem);
            }
        }

        private void OnImageStatusInitialized(int maxItems) {
            if(ImageStatusInitialized != null) {
                ImageStatusInitialized(maxItems);
            }
        }

        private void OnPromptForManualInput(
            ref Dictionary<string, string> manualInputValues) {

            if(PromptForManualInput != null) {
                PromptForManualInput(ref manualInputValues);
            }
        }
        #endregion Raise Event Methods
    }
}
