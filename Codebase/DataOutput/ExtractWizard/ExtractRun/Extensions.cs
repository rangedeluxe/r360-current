﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun
{
    public static class Extensions
    {
        /// <summary>
        /// Extension method that will apply the default limit values from
        /// the extract definition to the parameters.  The extract definition limits
        /// will override any existing parameters.
        /// </summary>
        /// <param name="extractParms">Extract parameters object</param>
        /// <param name="extractDefinition">Extract definition object</param>
        /// <returns></returns>
        public static cExtractParms ApplyDefaults(this cExtractParms extractParms, cExtractDef extractDefinition)
        {
            if (extractDefinition.DefaultBankID.Length > 0)
                extractParms.BankID = extractDefinition.DefaultBankID;

            if (extractDefinition.DefaultLockboxID.Length > 0)
                extractParms.LockboxID = extractDefinition.DefaultLockboxID;

            if (extractDefinition.DefaultBatchID.Length > 0)
                extractParms.BatchID = extractDefinition.DefaultBatchID;

            if (extractDefinition.DefaultDepositDate > DateTime.MinValue)
            {
                extractParms.DepositDateFrom = extractDefinition.DefaultDepositDate;
                extractParms.DepositDateTo = extractDefinition.DefaultDepositDate;
            }

            if (extractDefinition.DefaultProcessingDate > DateTime.MinValue)
            {
                extractParms.ProcessingDateFrom = extractDefinition.DefaultProcessingDate;
                extractParms.ProcessingDateTo = extractDefinition.DefaultProcessingDate;
            }

            return extractParms;
        }
    }
}
