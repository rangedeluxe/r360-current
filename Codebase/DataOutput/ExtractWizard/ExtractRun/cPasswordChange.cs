﻿
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Parker
* Date: 
*
* Purpose: class to manage password change form
*
* Modification History
* WI 106884-106887 DRP 7/10/2013
*   -Added check for "first time login" and added method to change the user's password.
* WI 132304 BLR 03/10/2014
*  - Removed password func. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {
    public class cPasswordChange {

        private int _userId;
        private bool _IAmAlive = true;
        private bool _Cancelled = false;

        public const bool OldPasswordRequired = true;
        public const bool OldPasswordNotRequired = false;

        public cPasswordChange()
        {

        }

        //public bool ChangePassword(int intUserID, string initialPrompt, bool requireOldPassword)
        //{
        //    this._userId = intUserID;

        //    cChangePasswordFormManager pwManager = new cChangePasswordFormManager(changePasswordDelegate, DialogCancelled, initialPrompt, requireOldPassword);
            
        //    while (_IAmAlive)
        //    {
        //        continue;
        //    }

        //    if (_Cancelled)
        //    {
        //        return (false);
        //    }
        //    else
        //    {
        //        return (true);
        //    }

        //}

        // WI 132304 : Remove all change password func.
        //private bool changePasswordDelegate(string oldPassword, string password, out string userMessage)
        //{
        //    if (oldPassword.Length > 0) //if passed in, need to confirm
        //    {
        //        if (!ExtractUserSecurity.VerifyPassword(_userId, oldPassword))
        //        {
        //            userMessage = "The old password does not verify";
        //            return (false);
        //        }

        //    }
        //    if (ExtractUserSecurity.ChangePassword(this._userId, oldPassword, password, out userMessage))
        //    {
        //        _IAmAlive = false;
        //        return (true);
        //    }
        //    else
        //    {
        //        return (false);
        //    }
        //}

        public void DialogCancelled() { //object sender, EventArgs e) {
            _Cancelled = true;
            _IAmAlive = false;
        }
    }
}
