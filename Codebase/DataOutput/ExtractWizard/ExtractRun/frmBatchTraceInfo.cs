using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 111438 BLR 02/26/2014
*   -Edited "Batch Trace Info" to "Batch Info".   
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {

    internal partial class frmBatchTraceInfo : Form {
        
        public frmBatchTraceInfo(cExtractRunInfoGroup RunInfo) {
            InitializeComponent();

            // WI 111438 : "Batch Trace Info" removed, as that table does not exist in R360.
            this.Text = "Batch Info";
            this.lblBatchInfoDescription.Text = 
                RunInfo.ExtractDescriptor + 
                " " + 
                RunInfo.RunDate.ToString() + 
                " ";
            
            foreach(cExtractRunInfo runinfo in RunInfo.PreviousRuns) {
                this.lvBatchInfo.Items.Add(new ListViewItem(new string[] {
                    runinfo.BankID.ToString(),
                    runinfo.ClientAccountID.ToString(),
                    runinfo.BatchID.ToString(),
                    runinfo.ImmutableDate.ToString("MM/dd/yyyy"),
                    runinfo.CheckCount.ToString(),
                    runinfo.CheckAmount.ToString("0.00") } ));
            }
        }
    }
}