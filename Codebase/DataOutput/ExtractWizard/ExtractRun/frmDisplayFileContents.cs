using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace IntegraPAY.Delivery.Extract.ExtractRun {

    public partial class frmDisplayFileContents : Form {

        public frmDisplayFileContents(string TextDescription, string FileName) {
            InitializeComponent();

            this.Text = TextDescription + " - " + FileName;

            if(File.Exists(FileName)) {
                TextReader tr = File.OpenText(FileName);
                this.txtMain.Text = tr.ReadToEnd();
                tr.Close();
                tr.Dispose();
            } else {
                this.txtMain.Text = "File not found: \n\n" + FileName;
            }
        }

        private void btnOk_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}