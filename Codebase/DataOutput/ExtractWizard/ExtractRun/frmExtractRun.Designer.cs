/*******************************************************************************
*
*    Module: frmExtractRun
*  Filename: frmExtractRun.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 19480 JMC 04/09/2007
*   -Initial release version.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun
{
    partial class frmExtractRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmExtractRun));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileSetupRpt = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSetupRptPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSetupRptPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSetupRptSave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilePrintSetupFileContents = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilePrinterSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuView = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuViewResults = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuViewLogFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblSaveResultsTo = new System.Windows.Forms.Label();
            this.lblExtractScript = new System.Windows.Forms.Label();
            this.txtExtractPath = new System.Windows.Forms.TextBox();
            this.btnSelectImageDir = new System.Windows.Forms.Button();
            this.btnSelectExtractScriptDir = new System.Windows.Forms.Button();
            this.txtImagePath = new System.Windows.Forms.TextBox();
            this.txtExtractScriptPath = new System.Windows.Forms.TextBox();
            this.lblSaveImagesTo = new System.Windows.Forms.Label();
            this.btnSelectCxrFile = new System.Windows.Forms.Button();
            this.ctlExtractRun1 = new WFS.RecHub.DataOutputToolkit.Extract.ExtractRun.ctlExtractRun();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusMsg});
            this.statusStrip1.Location = new System.Drawing.Point(0, 639);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(544, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusMsg
            // 
            this.toolStripStatusMsg.Name = "toolStripStatusMsg";
            this.toolStripStatusMsg.Size = new System.Drawing.Size(0, 17);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.mnuView,
            this.mnuHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(544, 24);
            this.menuStrip1.TabIndex = 27;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileOpen,
            this.toolStripSeparator1,
            this.mnuFileSetupRpt,
            this.mnuFilePrintSetupFileContents,
            this.mnuFilePrinterSetup,
            this.toolStripSeparator2,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(37, 20);
            this.mnuFile.Text = "&File";
            // 
            // mnuFileOpen
            // 
            this.mnuFileOpen.Image = ((System.Drawing.Image)(resources.GetObject("mnuFileOpen.Image")));
            this.mnuFileOpen.Name = "mnuFileOpen";
            this.mnuFileOpen.Size = new System.Drawing.Size(287, 22);
            this.mnuFileOpen.Text = "Open Extract Definition...";
            this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(284, 6);
            // 
            // mnuFileSetupRpt
            // 
            this.mnuFileSetupRpt.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileSetupRptPreview,
            this.mnuFileSetupRptPrint,
            this.mnuFileSetupRptSave});
            this.mnuFileSetupRpt.Name = "mnuFileSetupRpt";
            this.mnuFileSetupRpt.Size = new System.Drawing.Size(287, 22);
            this.mnuFileSetupRpt.Text = "Setup Report";
            this.mnuFileSetupRpt.Visible = false;
            // 
            // mnuFileSetupRptPreview
            // 
            this.mnuFileSetupRptPreview.Enabled = false;
            this.mnuFileSetupRptPreview.Image = ((System.Drawing.Image)(resources.GetObject("mnuFileSetupRptPreview.Image")));
            this.mnuFileSetupRptPreview.Name = "mnuFileSetupRptPreview";
            this.mnuFileSetupRptPreview.Size = new System.Drawing.Size(124, 22);
            this.mnuFileSetupRptPreview.Text = "Preview...";
            // 
            // mnuFileSetupRptPrint
            // 
            this.mnuFileSetupRptPrint.Enabled = false;
            this.mnuFileSetupRptPrint.Image = ((System.Drawing.Image)(resources.GetObject("mnuFileSetupRptPrint.Image")));
            this.mnuFileSetupRptPrint.Name = "mnuFileSetupRptPrint";
            this.mnuFileSetupRptPrint.Size = new System.Drawing.Size(124, 22);
            this.mnuFileSetupRptPrint.Text = "Print...";
            // 
            // mnuFileSetupRptSave
            // 
            this.mnuFileSetupRptSave.Image = ((System.Drawing.Image)(resources.GetObject("mnuFileSetupRptSave.Image")));
            this.mnuFileSetupRptSave.Name = "mnuFileSetupRptSave";
            this.mnuFileSetupRptSave.Size = new System.Drawing.Size(124, 22);
            this.mnuFileSetupRptSave.Text = "Save...";
            // 
            // mnuFilePrintSetupFileContents
            // 
            this.mnuFilePrintSetupFileContents.Image = ((System.Drawing.Image)(resources.GetObject("mnuFilePrintSetupFileContents.Image")));
            this.mnuFilePrintSetupFileContents.Name = "mnuFilePrintSetupFileContents";
            this.mnuFilePrintSetupFileContents.Size = new System.Drawing.Size(287, 22);
            this.mnuFilePrintSetupFileContents.Text = "Print Contents of Extract Definition File...";
            this.mnuFilePrintSetupFileContents.Click += new System.EventHandler(this.mnuFilePrintSetupFileContents_Click);
            // 
            // mnuFilePrinterSetup
            // 
            this.mnuFilePrinterSetup.Name = "mnuFilePrinterSetup";
            this.mnuFilePrinterSetup.Size = new System.Drawing.Size(287, 22);
            this.mnuFilePrinterSetup.Text = "Printer Setup...";
            this.mnuFilePrinterSetup.Click += new System.EventHandler(this.mnuFilePrinterSetup_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(284, 6);
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Size = new System.Drawing.Size(287, 22);
            this.mnuFileExit.Text = "E&xit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // mnuView
            // 
            this.mnuView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuViewResults,
            this.mnuViewLogFile});
            this.mnuView.Name = "mnuView";
            this.mnuView.Size = new System.Drawing.Size(44, 20);
            this.mnuView.Text = "&View";
            // 
            // mnuViewResults
            // 
            this.mnuViewResults.Name = "mnuViewResults";
            this.mnuViewResults.Size = new System.Drawing.Size(115, 22);
            this.mnuViewResults.Text = "Results";
            this.mnuViewResults.Click += new System.EventHandler(this.mnuViewResults_Click);
            // 
            // mnuViewLogFile
            // 
            this.mnuViewLogFile.Name = "mnuViewLogFile";
            this.mnuViewLogFile.Size = new System.Drawing.Size(115, 22);
            this.mnuViewLogFile.Text = "Log File";
            this.mnuViewLogFile.Click += new System.EventHandler(this.mnuViewLogFile_Click);
            // 
            // mnuHelp
            // 
            this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHelpAbout});
            this.mnuHelp.Name = "mnuHelp";
            this.mnuHelp.Size = new System.Drawing.Size(44, 20);
            this.mnuHelp.Text = "&Help";
            // 
            // mnuHelpAbout
            // 
            this.mnuHelpAbout.Name = "mnuHelpAbout";
            this.mnuHelpAbout.Size = new System.Drawing.Size(152, 22);
            this.mnuHelpAbout.Text = "About";
            this.mnuHelpAbout.Click += new System.EventHandler(this.mnuHelpAbout_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // pnlTop
            // 
            this.pnlTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTop.Controls.Add(this.lblSaveResultsTo);
            this.pnlTop.Controls.Add(this.lblExtractScript);
            this.pnlTop.Controls.Add(this.txtExtractPath);
            this.pnlTop.Controls.Add(this.btnSelectImageDir);
            this.pnlTop.Controls.Add(this.btnSelectExtractScriptDir);
            this.pnlTop.Controls.Add(this.txtImagePath);
            this.pnlTop.Controls.Add(this.txtExtractScriptPath);
            this.pnlTop.Controls.Add(this.lblSaveImagesTo);
            this.pnlTop.Controls.Add(this.btnSelectCxrFile);
            this.pnlTop.Location = new System.Drawing.Point(16, 33);
            this.pnlTop.Margin = new System.Windows.Forms.Padding(4);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(512, 100);
            this.pnlTop.TabIndex = 28;
            // 
            // lblSaveResultsTo
            // 
            this.lblSaveResultsTo.AutoSize = true;
            this.lblSaveResultsTo.Location = new System.Drawing.Point(38, 41);
            this.lblSaveResultsTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSaveResultsTo.Name = "lblSaveResultsTo";
            this.lblSaveResultsTo.Size = new System.Drawing.Size(89, 13);
            this.lblSaveResultsTo.TabIndex = 31;
            this.lblSaveResultsTo.Text = "Save Results To:";
            // 
            // lblExtractScript
            // 
            this.lblExtractScript.AutoSize = true;
            this.lblExtractScript.Location = new System.Drawing.Point(18, 9);
            this.lblExtractScript.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblExtractScript.Name = "lblExtractScript";
            this.lblExtractScript.Size = new System.Drawing.Size(109, 13);
            this.lblExtractScript.TabIndex = 26;
            this.lblExtractScript.Text = "Extract Definition File:";
            // 
            // txtExtractPath
            // 
            this.txtExtractPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExtractPath.Location = new System.Drawing.Point(137, 37);
            this.txtExtractPath.Margin = new System.Windows.Forms.Padding(4);
            this.txtExtractPath.Name = "txtExtractPath";
            this.txtExtractPath.Size = new System.Drawing.Size(325, 20);
            this.txtExtractPath.TabIndex = 30;
            this.txtExtractPath.TextChanged += new System.EventHandler(this.txtExtractPath_TextChanged);
            // 
            // btnSelectImageDir
            // 
            this.btnSelectImageDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectImageDir.Location = new System.Drawing.Point(468, 68);
            this.btnSelectImageDir.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelectImageDir.Name = "btnSelectImageDir";
            this.btnSelectImageDir.Size = new System.Drawing.Size(26, 23);
            this.btnSelectImageDir.TabIndex = 34;
            this.btnSelectImageDir.Text = "...";
            this.btnSelectImageDir.UseVisualStyleBackColor = true;
            this.btnSelectImageDir.Click += new System.EventHandler(this.btnSelectImageDir_Click);
            // 
            // btnSelectExtractScriptDir
            // 
            this.btnSelectExtractScriptDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectExtractScriptDir.Location = new System.Drawing.Point(468, 4);
            this.btnSelectExtractScriptDir.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelectExtractScriptDir.Name = "btnSelectExtractScriptDir";
            this.btnSelectExtractScriptDir.Size = new System.Drawing.Size(26, 23);
            this.btnSelectExtractScriptDir.TabIndex = 1;
            this.btnSelectExtractScriptDir.Text = "...";
            this.btnSelectExtractScriptDir.UseVisualStyleBackColor = true;
            this.btnSelectExtractScriptDir.Click += new System.EventHandler(this.btnSelectExtractScriptDir_Click);
            // 
            // txtImagePath
            // 
            this.txtImagePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImagePath.Location = new System.Drawing.Point(137, 69);
            this.txtImagePath.Margin = new System.Windows.Forms.Padding(4);
            this.txtImagePath.Name = "txtImagePath";
            this.txtImagePath.Size = new System.Drawing.Size(325, 20);
            this.txtImagePath.TabIndex = 33;
            this.txtImagePath.TextChanged += new System.EventHandler(this.txtImagePath_TextChanged);
            // 
            // txtExtractScriptPath
            // 
            this.txtExtractScriptPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExtractScriptPath.Location = new System.Drawing.Point(137, 5);
            this.txtExtractScriptPath.Margin = new System.Windows.Forms.Padding(4);
            this.txtExtractScriptPath.Name = "txtExtractScriptPath";
            this.txtExtractScriptPath.ReadOnly = true;
            this.txtExtractScriptPath.Size = new System.Drawing.Size(325, 20);
            this.txtExtractScriptPath.TabIndex = 0;
            this.txtExtractScriptPath.TabStop = false;
            // 
            // lblSaveImagesTo
            // 
            this.lblSaveImagesTo.AutoSize = true;
            this.lblSaveImagesTo.Location = new System.Drawing.Point(38, 73);
            this.lblSaveImagesTo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSaveImagesTo.Name = "lblSaveImagesTo";
            this.lblSaveImagesTo.Size = new System.Drawing.Size(88, 13);
            this.lblSaveImagesTo.TabIndex = 35;
            this.lblSaveImagesTo.Text = "Save Images To:";
            // 
            // btnSelectCxrFile
            // 
            this.btnSelectCxrFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectCxrFile.Location = new System.Drawing.Point(468, 36);
            this.btnSelectCxrFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelectCxrFile.Name = "btnSelectCxrFile";
            this.btnSelectCxrFile.Size = new System.Drawing.Size(26, 23);
            this.btnSelectCxrFile.TabIndex = 32;
            this.btnSelectCxrFile.Text = "...";
            this.btnSelectCxrFile.UseVisualStyleBackColor = true;
            this.btnSelectCxrFile.Click += new System.EventHandler(this.btnSelectCxrFile_Click);
            // 
            // ctlExtractRun1
            // 
            this.ctlExtractRun1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlExtractRun1.ExtractPath = "";
            this.ctlExtractRun1.ImagePath = "";
            this.ctlExtractRun1.Location = new System.Drawing.Point(16, 140);
            this.ctlExtractRun1.Margin = new System.Windows.Forms.Padding(5);
            this.ctlExtractRun1.Name = "ctlExtractRun1";
            this.ctlExtractRun1.Size = new System.Drawing.Size(512, 494);
            this.ctlExtractRun1.TabIndex = 29;
            this.ctlExtractRun1.OutputStatusMsg += new WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI.OutputStatusMsgEventHandler(this.ctlExtractRun1_OutputStatusMsg);
            this.ctlExtractRun1.BeginExtractRun += new WFS.RecHub.DataOutputToolkit.Extract.ExtractRun.BeginExtractRunEventHandler(this.ctlExtractRun1_BeginExtractRun);
            this.ctlExtractRun1.EndExtractRun += new WFS.RecHub.DataOutputToolkit.Extract.ExtractRun.EndExtractRunEventHandler(this.ctlExtractRun1_EndExtractRun);
            // 
            // frmExtractRun
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(544, 661);
            this.Controls.Add(this.ctlExtractRun1);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmExtractRun";
            this.Text = "Run Extract - [Run Extract]";
            this.Load += new System.EventHandler(this.frmExtractRun_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusMsg;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuFilePrinterSetup;
        private System.Windows.Forms.ToolStripMenuItem mnuFileExit;
        private System.Windows.Forms.ToolStripMenuItem mnuView;
        private System.Windows.Forms.ToolStripMenuItem mnuViewResults;
        private System.Windows.Forms.ToolStripMenuItem mnuViewLogFile;
        private System.Windows.Forms.ToolStripMenuItem mnuHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuHelpAbout;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSetupRpt;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSetupRptPrint;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSetupRptPreview;
        private System.Windows.Forms.ToolStripMenuItem mnuFilePrintSetupFileContents;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSetupRptSave;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Label lblExtractScript;
        private System.Windows.Forms.Button btnSelectExtractScriptDir;
        private System.Windows.Forms.TextBox txtExtractScriptPath;
        private ctlExtractRun ctlExtractRun1;
        private System.Windows.Forms.Label lblSaveResultsTo;
        private System.Windows.Forms.TextBox txtExtractPath;
        private System.Windows.Forms.Button btnSelectImageDir;
        private System.Windows.Forms.TextBox txtImagePath;
        private System.Windows.Forms.Label lblSaveImagesTo;
        private System.Windows.Forms.Button btnSelectCxrFile;
    }
}