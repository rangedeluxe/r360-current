﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractRun.Properties;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 130811 BLR 02/28/2014
*   - Added a 'View Results' button to the frmCompleted.
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added exception logging to catch blocks. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {

    public partial class frmExtractCompleted : Form {

        private string extractFileLocation;

        public static void ShowForm(string msgText, string extract) {
            frmExtractCompleted objExtractCompleted = new frmExtractCompleted(msgText, extract);
            objExtractCompleted.ShowDialog();
            objExtractCompleted.Dispose();
        }

        private frmExtractCompleted(string msgText, string extract) {

            InitializeComponent();

            extractFileLocation = extract;

            try {
                this.txtExtractResults.Rtf = msgText;
            } catch(Exception ex) {
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);;
                this.txtExtractResults.Text = msgText;
            }
        }

        private void btnOk_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void btnViewResultsClick(object sender, EventArgs e)
        {
            frmDisplayFileContents objText;

            if (!string.IsNullOrEmpty(extractFileLocation) && File.Exists(extractFileLocation))
            {
                objText = new frmDisplayFileContents("Extract Results", extractFileLocation, null, this.Icon);
                objText.Show(this);
            }
            else
                MessageBox.Show("Could not find extract file.");
        }
    }
}
