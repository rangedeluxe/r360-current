using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {

    internal class cExtractRunInfoGroup {

        private string _ExtractDescriptor;
        private DateTime _RunDate;
        private ArrayList _PreviousRuns = new ArrayList();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="extractDescriptor"></param>
        /// <param name="vRunDate"></param>
        public cExtractRunInfoGroup(
            string extractDescriptor, 
            DateTime runDate) {
            
            _ExtractDescriptor = extractDescriptor;
            _RunDate = runDate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ExtractRunInfo"></param>
        public void Add(cExtractRunInfo extractRunInfo) {
            if(extractRunInfo != null) {
                _PreviousRuns.Add(extractRunInfo);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public cExtractRunInfo[] PreviousRuns {
            get {
                return((cExtractRunInfo[])_PreviousRuns.ToArray(typeof(cExtractRunInfo)));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string ExtractDescriptor {
            get {
                return (_ExtractDescriptor);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime RunDate {
            get {
                return(_RunDate);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
             return _RunDate.ToString();
        }
    }
}
