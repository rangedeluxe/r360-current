using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/21/2013
*   -Initial Version 
* WI 106884-106887 DRP 7/10/2013
*   -Added check for "first time login" and added method to change the user's password.
* WI 130165 DLD 02/19/2014
*   -Expanded Exception handling and message logging. 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   -Added additional logging to catch blocks.
* WI 132304 BLR 03/10/2014
*  - Removed password func. 
* WI 130364 BLR 03/20/2014
*  - Validated settings upfront.
* WI 152321 BLR 08/15/2014
*   - Moved configuration settings over to ExtractConfigurationSettings.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {

    public partial class frmExtractRun : Form {

        private ExtractConfigurationSettings _Settings = null;
        private int _UserID = -1;

        private enum FormState {
            Init = 0,
            Run = 1
        }

        public frmExtractRun(
            int userID,
            cExtractParms parms, 
            bool runAsTest) {

            InitializeComponent();

            SetFormState(FormState.Init);

            _UserID = userID;

            if(parms != null) {
                LoadExtractParms(
                    parms, 
                    runAsTest);
            }

            Settings.OutputMessage += Settings_OutputMessage;
            // WI 130364 : Validate the config settings.
            Settings.ValidateSettings();
        }

        // WI 130364 : Validate the config settings upfront.
        void Settings_OutputMessage(string message, string src, Common.MessageType messageType, Common.MessageImportance messageImportance)
        {
            OnOutputMessage(message, src, messageType, messageImportance);
        }

        /// <summary>
        /// WI 130364
        /// Private method to handle the settings output for settings validation.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="src"></param>
        /// <param name="messageType"></param>
        /// <param name="messageImportance"></param>
        private void OnOutputMessage(
            string message,
            string src,
            MessageType messageType,
            MessageImportance messageImportance)
        {

            string strCaption;
            MessageBoxIcon objIcon;

            if (messageImportance == MessageImportance.Essential)
            {
                switch (messageType)
                {
                    case MessageType.Warning:
                        strCaption = "Warning";
                        objIcon = MessageBoxIcon.Warning;
                        break;
                    case MessageType.Error:
                        strCaption = "Error";
                        objIcon = MessageBoxIcon.Error;
                        break;
                    default:    // MessageType.Information:
                        strCaption = "Information";
                        objIcon = MessageBoxIcon.Information;
                        break;
                }

                cCommonLogLib.LogMessageLTA(message, (LTAMessageImportance)messageImportance);
                MessageBox.Show(message, strCaption, MessageBoxButtons.OK, objIcon);
                // DLD - WI 130364 - Verify database connectivity providing 'friendly' messages to user upon failure.
                if (message.ToUpper() == ExtractLib.Constants.ERROR_MSG_NO_DATABASE_CONNECTION.ToUpper())
                {
                    var msg = Application.ProductName + " has reached an unstable state and should be closed."
                        + Environment.NewLine + Environment.NewLine + "Would you like to close it now?";

                    var dr = MessageBox.Show(this, msg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                        Close();
                }
            }
        }

        private void frmExtractRun_Load(object sender, EventArgs e) {

            ////try {

            ////    if(!ExtractLicense.CanRun(Settings.SetupPath)) {
            ////        MessageBox.Show(ExtractLicense.CannotRunMsg(), ExtractLicense.LicenseErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            ////        this.Close();
            ////    }

            ////} catch(LicenseFileReadException ex) {
            ////    MessageBox.Show(ex.Message, ExtractLicense.LicenseErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            ////    this.Close();
            ////} catch(Exception ex) {
            ////    throw(ex);
            ////}
        }

        private ExtractConfigurationSettings Settings
        {
            get {
                if(_Settings == null) {
                    _Settings = new ExtractConfigurationSettings(new ConfigurationManagerProvider());
                }
                return(_Settings);
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing) {
                if(components != null) {
                    components.Dispose();
                }

                //// clean up Sentry object
                //Sentry.Dismiss();  //CR11654 4-5-2005 sma
            }
            base.Dispose(disposing);
        }

        private void OnErrorOccurred(
            object sender,
            System.Exception e)
        {
            // DLD - WI 130165 - Don't want to pop a message box if this is runing in 'Silent' mode
            if (Environment.UserInteractive)
                MessageBox.Show(ExtractLib.Constants.ERROR_MSG_UNHANDLED, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
        }

        private void SelectExtractScriptDir() {

            string strFilePath;

            if(IOLib.BrowseFile(Constants.FILE_FILTER_EXTRACT_DEF, Settings.DefaultDefinitionFileFormatIndex, Settings.SetupPath, true, out strFilePath)) {

                cExtractParms Parms = new cExtractParms();

                Parms.SetupFile = strFilePath;
                
                Parms.BankID = this.ctlExtractRun1.BankID;
                Parms.LockboxID = this.ctlExtractRun1.LockboxID;
                Parms.BatchID = this.ctlExtractRun1.BatchID;
                Parms.DepositDateFrom = this.ctlExtractRun1.DepositDateFrom;
                Parms.DepositDateTo = this.ctlExtractRun1.DepositDateTo;
                Parms.ProcessingDateFrom = this.ctlExtractRun1.ProcessingDateFrom;
                Parms.ProcessingDateTo = this.ctlExtractRun1.ProcessingDateTo;

                Parms.UserID = _UserID;

                LoadExtractParms(Parms, this.ctlExtractRun1.RunAsTest);
            }
        }

        private void LoadExtractParms(
            cExtractParms parms, 
            bool runAsTest) {

            if(this.ctlExtractRun1.LoadExtractParms(parms, runAsTest)) {

                _UserID = parms.UserID;
    
                this.txtExtractScriptPath.Text = parms.SetupFile;
                this.Text = "Run Extract - [Run Extract - " + parms.SetupFile + "]";
                this.txtImagePath.Text = this.ctlExtractRun1.ImagePath;
                this.txtExtractPath.Text = this.ctlExtractRun1.ExtractPath;

                SetFormState(FormState.Run);
            } else {
                SetFormState(FormState.Init);
            }
        }

        // ********************************************
        // * Form State Management
        // ********************************************

        private void SetFormState(FormState state) {

            switch(state) {
                case FormState.Init:
                    this.txtExtractScriptPath.Text = string.Empty;
                    this.txtExtractPath.Text = string.Empty;
                    this.txtImagePath.Text = string.Empty;
                    this.ctlExtractRun1.Clear();
                    break;
                case FormState.Run:
                    break;
            }
        }


        // ********************************************
        // * Event Handlers
        // ********************************************

        private void btnSelectExtractScriptDir_Click(object sender, EventArgs e) {
            SelectExtractScriptDir();
        }

        private void btnSelectCxrFile_Click(object sender, EventArgs e) {

            string strInitialPath;
            string strFilePath;
            
            if(this.txtExtractPath.Text.Length > 0) {
                strInitialPath = this.txtExtractPath.Text;
            } else if(this.txtExtractScriptPath.Text.LastIndexOf('.') > -1) {
                strInitialPath = this.txtExtractScriptPath.Text.Substring(0, this.txtExtractScriptPath.Text.LastIndexOf('.')) + ".cxr";
            } else {
                strInitialPath = this.txtExtractScriptPath.Text + ".cxr";
            }
            
            if(strInitialPath.StartsWith(".")) {
                strInitialPath = Path.Combine(Settings.SetupPath, strInitialPath);
            }

            if(IOLib.BrowseFile(Constants.FILE_FILTER_EXTRACT_DATA_FILES, Settings.DefaultDefinitionFileFormatIndex, strInitialPath, out strFilePath)) {
                this.txtExtractPath.Text = strFilePath;
            }
        }

        private void btnSelectImageDir_Click(object sender, EventArgs e) {

            string strInitialPath;
            string strFolderPath;

            if(this.txtImagePath.Text.Trim().Length == 0) {
                if(this.txtExtractPath.Text.Length > 0 && this.txtExtractPath.Text.LastIndexOf(@"\") > -1) {
                    strInitialPath = this.txtExtractPath.Text.Substring(0, this.txtExtractPath.Text.LastIndexOf(@"\"));
                } else {
                    strInitialPath = Settings.SetupPath;
                }
            } else if(this.txtImagePath.Text.StartsWith(".")) {
                strInitialPath = Path.Combine(Settings.SetupPath, this.txtImagePath.Text);
            } else {
                strInitialPath = this.txtImagePath.Text;
            }

            if(IOLib.BrowseFolder(Path.GetFullPath(strInitialPath), out strFolderPath)) {
                this.txtImagePath.Text = strFolderPath;
            }
        }

        private void mnuFileOpen_Click(object sender, EventArgs e) {
            SelectExtractScriptDir();
        }

        private void mnuFilePrintSetupFileContents_Click(object sender, EventArgs e) {
            if(File.Exists(this.txtExtractScriptPath.Text)) {

                DialogResult result = printDialog1.ShowDialog();
                if(result == DialogResult.OK) {
                    cPrinter.PrintTextFile(this.txtExtractScriptPath.Text, this.printDialog1.PrinterSettings);
                }
            } else {
                if (Environment.UserInteractive)
                    MessageBox.Show("Could not locate file: [" + this.txtExtractScriptPath.Text + "].", 
                                "Error", 
                                MessageBoxButtons.OK, 
                                MessageBoxIcon.Error);
            }
        }

        private void mnuFilePrinterSetup_Click(object sender, EventArgs e) {       

            PageSetupDialog dlg;
            DialogResult result;

            dlg = new PageSetupDialog();

            dlg.AllowMargins = false;
            dlg.AllowOrientation = true;
            dlg.AllowPaper = true;
            dlg.AllowPrinter = true;
            dlg.EnableMetric = false;
            dlg.ShowHelp = false;
            dlg.ShowNetwork = true;

            using (var doc = new PrintDocument())
            {
                doc.PrinterSettings = this.printDialog1.PrinterSettings;
                dlg.Document = doc;
                result = dlg.ShowDialog();
                dlg.Dispose();
            }
        }

        private void mnuFileExit_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void mnuViewResults_Click(object sender, EventArgs e) {

            frmDisplayFileContents objText;
            string strFileName;

            try {

                if(this.ctlExtractRun1.CompletedExtractFileName.Length > 0 && File.Exists(this.ctlExtractRun1.CompletedExtractFileName)) {
                    objText = new frmDisplayFileContents("Extract Results", this.ctlExtractRun1.CompletedExtractFileName, this.printDialog1.PrinterSettings, this.Icon);
                    objText.Show(this);
                } else if(IOLib.BrowseFile(Constants.FILE_FILTER_EXTRACT_DATA_FILES, Settings.DefaultDefinitionFileFormatIndex, "", out strFileName)) {
                    objText = new frmDisplayFileContents("Extract Results", strFileName, this.printDialog1.PrinterSettings, this.Icon);
                    objText.Show(this);
                }
            } catch(Exception ex) {
                OnErrorOccurred(sender, ex);
            }
        }

        private void mnuViewLogFile_Click(object sender, EventArgs e) {

            frmDisplayFileContents objText;
            string strFileName;
            
            try {

                if(this.ctlExtractRun1.CompletedLogFilePath.Length > 0 && File.Exists(this.ctlExtractRun1.CompletedLogFilePath)) {
                    objText = new frmDisplayFileContents("Extract Log File", this.ctlExtractRun1.CompletedLogFilePath, this.printDialog1.PrinterSettings, this.Icon);
                    objText.Show(this);
                } else if(IOLib.BrowseFile(Constants.FILE_FILTER_LOG_FILES, Settings.DefaultDefinitionFileFormatIndex, "", out strFileName)) {
                    objText = new frmDisplayFileContents("Extract Log File", strFileName, this.printDialog1.PrinterSettings, this.Icon);
                    objText.Show(this);
                }
            } catch(Exception ex) {
                OnErrorOccurred(sender, ex);
            }
        }

        private void mnuHelpAbout_Click(object sender, EventArgs e) {
            frmAbout objAbout = new frmAbout();
            objAbout.ShowDialog(this);
            objAbout.Dispose();
        }

        private void txtExtractPath_TextChanged(object sender, EventArgs e) {
            this.ctlExtractRun1.ExtractPath = this.txtExtractPath.Text;
        }

        private void txtImagePath_TextChanged(object sender, EventArgs e) {
            this.ctlExtractRun1.ImagePath = this.txtImagePath.Text;
        }

        private void ctlExtractRun1_OutputStatusMsg(string msg) {
            this.toolStripStatusMsg.Text = msg;
            Application.DoEvents();
        }

        private void ctlExtractRun1_EndExtractRun() {

            if(this.InvokeRequired) {           
                EndExtractRunEventHandler d = new EndExtractRunEventHandler(ctlExtractRun1_EndExtractRun);
                this.Invoke(d, new object[] {});
            } else {
                this.Cursor = Cursors.Default;

                this.mnuFile.Enabled = true;
                this.mnuView.Enabled = true;
                this.txtExtractPath.Enabled = true;
                this.txtImagePath.Enabled = true;
                this.btnSelectExtractScriptDir.Enabled = true;
                this.btnSelectCxrFile.Enabled = true;
                this.btnSelectImageDir.Enabled = true;
            }
        }

        private void ctlExtractRun1_BeginExtractRun() {

            this.Cursor = Cursors.WaitCursor;

            this.mnuFile.Enabled = false;
            this.mnuView.Enabled = false;
            this.txtExtractPath.Enabled = false;
            this.txtImagePath.Enabled = false;
            this.btnSelectExtractScriptDir.Enabled = false;
            this.btnSelectCxrFile.Enabled = false;
            this.btnSelectImageDir.Enabled = false;

            Application.DoEvents();
        }

        //private void changePasswordToolStripMenuItem_Click_1(object sender, EventArgs e)
        //{
        //    cPasswordChange passwordChange = new cPasswordChange();
        //    passwordChange.ChangePassword(this._UserID, "Please enter a new password.", cPasswordChange.OldPasswordRequired);
        //}
    }
}