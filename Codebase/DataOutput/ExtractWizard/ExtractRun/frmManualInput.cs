﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {

    public partial class frmManualInput : Form {

        private Dictionary<string, string> _OriginalManualInputValues;
        bool _Cancelled = false;

        public frmManualInput(Dictionary<string, string> manualInputValues) {

            InitializeComponent();

            _OriginalManualInputValues = manualInputValues;
            
            if(manualInputValues != null) {
                foreach(KeyValuePair<string, string> inputvalue in manualInputValues) {
                    this.dataGridView1.Rows.Add(inputvalue.Key, inputvalue.Value);
                }
            }
        }
        
        public Dictionary<string, string> formInputValues {
            get {

                Dictionary<string, string> dicFormInputValues = new Dictionary<string,string>();

                if(_Cancelled) {
                    dicFormInputValues = _OriginalManualInputValues;
                } else {
                    foreach(DataGridViewRow row in this.dataGridView1.Rows) {
                        dicFormInputValues.Add(row.Cells[0].Value.ToString(), row.Cells[1].Value.ToString());
                    }
                }

                return(dicFormInputValues);
            }
        }
        
        public bool Cancelled {
            get {
                return(_Cancelled);
            }
        }

        private void btnOk_Click(object sender, EventArgs e) {
            _Cancelled = false;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            _Cancelled = true;
            this.Close();
        }
    }
}
