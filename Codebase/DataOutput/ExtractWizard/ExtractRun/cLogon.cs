﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WFS.LTA.Common;
using WFS.RecHub.Common.Crypto;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/24/2013
*   -Initial Version 
* WI 106884-106887 DRP 7/10/2013
*   -Added check for "first time login" and added method to change the user's password.
* WI 132304 BLR 03/10/2014
*  - Removed password func. 
* WI 138812 BLR 07/11/2014
*   - Removed the logon bypass in preparation for RAAM.
* WI 157574 BLR 08/08/2014
*   - Added entity to the form for RAAM.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {
    public class cLogon {

        private int _UserID = -1;
        private Guid _SessionID = Guid.Empty;
        private bool _IAmAlive = true;
        private bool _Cancelled = false;

        public cLogon() {

        }

        public int UserID {
            get {
                return (_UserID);
            }
        }

        public Guid SessionID {
            get {
                return (_SessionID);
            }
        }


        public bool SilentLogin(
            string entity,
            string encryptedLogonName,
            string encryptedPassword, 
            int loginattempts,
            out int userID, 
            out Guid sessionID,
            out string userMessage) 
        {

            Guid gidSessionID = Guid.Empty;
            int intUserID = -1;
            string strUserMessage = string.Empty;
            bool bolRetVal = false;

            // It is expected that a silentlogin's information is always encrypted.
            // We'll need to decrypt them here first in order to use the same login mechanism
            // as EDM.

            // First check to see if we actually have the information we need.
            if(encryptedLogonName.ToString().Length == 0 || encryptedPassword.ToString().Length == 0
                || entity.Length == 0) 
            {
                userID = -1;
                sessionID = Guid.Empty;
                userMessage = "Entity, Logon Name, and Password are required for Silent Logon";
                return false;
            } 

            // Check to see if we can decrypt the 3 parts.
            string decryptedentity = string.Empty;
            string decryptedusername = string.Empty;
            string decryptedpassword = string.Empty;
            var encsuccess = true;
            try
            {
                var enc = new cCrypto3DES();
                encsuccess = encsuccess && enc.Decrypt(entity, out decryptedentity);
                encsuccess = encsuccess && enc.Decrypt(encryptedLogonName, out decryptedusername);
                encsuccess = encsuccess && enc.Decrypt(encryptedPassword, out decryptedpassword);
            }
            catch (Exception)
            {
                // Couldn't decrypt (not sure why we're returning true/false if we're just throwing errors anyway?)
                userID = -1;
                sessionID = Guid.Empty;
                userMessage = "Incorrect format for Entity, Username, Password. They must be encypted.";
                return false;
            }

            if (!encsuccess)
            {
                userID = -1;
                sessionID = Guid.Empty;
                userMessage = "Incorrect format for Entity, Username, Password. They must be encypted.";
                return false;
            }

            var loginsuccess = false;
            var attempts = 0;
            userID = -1;
            sessionID = Guid.Empty;
            userMessage = string.Empty;
            while (!loginsuccess && attempts < loginattempts)
            {
                // Now finally attempt the Logon
                if (ExtractServiceSecurity.Logon(decryptedentity, decryptedusername, decryptedpassword, false, out intUserID, out gidSessionID, out strUserMessage))
                {
                    userID = intUserID;
                    sessionID = gidSessionID;
                    userMessage = strUserMessage;
                    bolRetVal = true;
                    loginsuccess = true;
                }
                else 
                {
                    userID = -1;
                    sessionID = Guid.Empty;
                    userMessage = "The provided credentials failed ";
                    bolRetVal = false;
                    attempts++;
                    cCommonLogLib.LogMessageLTA("Warning: Failed to log into RAAM.  Attempt: " + attempts, LTAMessageImportance.Essential);
                    Thread.Sleep(2000);
                }
            }

            if (!bolRetVal)
            {
                cCommonLogLib.LogErrorLTA(new Exception("Error: Failed to log into RAAM."), LTAMessageImportance.Essential);
            }

            return (bolRetVal);
        }

        public bool CollectLogin(
            out int userID, 
            out Guid sessionID) 
        {

            cLogonFormManager objFormManager = new cLogonFormManager(
                this.LogonCredentialsProvided,
                this.DialogCancelled);

            // wat
            while(_IAmAlive) 
            {
                continue;
            }

            if(_Cancelled) 
            {
                sessionID = Guid.Empty;
                userID = -1;
                return (false);
            } 
            else 
            {
                sessionID = _SessionID;
                userID = _UserID;
                return (true);
            }
        }

        private bool LogonCredentialsProvided(
            string entity,
            string logonName,
            string password,
            out string userMessage) 
        {

            Guid gidSessionID;
            int intUserID;

            if (ExtractServiceSecurity.Logon(entity, logonName, password, true, out intUserID, out gidSessionID, out userMessage)) 
            {
                _UserID = intUserID;
                _SessionID = gidSessionID;
                _IAmAlive = false;
                return (true);
            } 
            else 
            {
                return (false);
            }
        }

        public void DialogCancelled() { //object sender, EventArgs e) {
            _Cancelled = true;
            _IAmAlive = false;
        }
    }
}
