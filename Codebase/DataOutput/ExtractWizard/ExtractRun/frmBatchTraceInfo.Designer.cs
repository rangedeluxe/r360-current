/*******************************************************************************
*
*    Module: frmBatchTraceInfo
*  Filename: frmBatchTraceInfo.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 19480 JMC 04/09/2007
*   -Initial release version.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun
{
    partial class frmBatchTraceInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBatchTraceInfo));
            this.label1 = new System.Windows.Forms.Label();
            this.lblBatchInfoDescription = new System.Windows.Forms.Label();
            this.lvBatchInfo = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Batch Info For";
            // 
            // lblBatchInfoDescription
            // 
            this.lblBatchInfoDescription.AutoSize = true;
            this.lblBatchInfoDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatchInfoDescription.Location = new System.Drawing.Point(98, 20);
            this.lblBatchInfoDescription.Name = "lblBatchInfoDescription";
            this.lblBatchInfoDescription.Size = new System.Drawing.Size(41, 13);
            this.lblBatchInfoDescription.TabIndex = 1;
            this.lblBatchInfoDescription.Text = "label2";
            // 
            // lvBatchInfo
            // 
            this.lvBatchInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvBatchInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvBatchInfo.FullRowSelect = true;
            this.lvBatchInfo.GridLines = true;
            this.lvBatchInfo.Location = new System.Drawing.Point(12, 50);
            this.lvBatchInfo.MultiSelect = false;
            this.lvBatchInfo.Name = "lvBatchInfo";
            this.lvBatchInfo.Size = new System.Drawing.Size(470, 371);
            this.lvBatchInfo.TabIndex = 2;
            this.lvBatchInfo.UseCompatibleStateImageBehavior = false;
            this.lvBatchInfo.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "BankID";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "LockboxID";
            this.columnHeader2.Width = 71;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "BatchID";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "ProcessingDate";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader4.Width = 97;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "CheckCount";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader5.Width = 83;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "CheckAmount";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader6.Width = 92;
            // 
            // frmBatchTraceInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 433);
            this.Controls.Add(this.lvBatchInfo);
            this.Controls.Add(this.lblBatchInfoDescription);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBatchTraceInfo";
            this.Text = "Batch Trace Info";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblBatchInfoDescription;
        private System.Windows.Forms.ListView lvBatchInfo;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}