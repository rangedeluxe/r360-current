namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun
{
    partial class ctlExtractRun
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pgsMain = new System.Windows.Forms.ProgressBar();
            this.grpMain = new System.Windows.Forms.GroupBox();
            this.btnAbort = new System.Windows.Forms.Button();
            this.btnGo = new System.Windows.Forms.Button();
            this.grpRunOptions = new System.Windows.Forms.GroupBox();
            this.lstExtractRunHistory = new System.Windows.Forms.CheckedListBox();
            this.chkRunAsTest = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkRerun = new System.Windows.Forms.CheckBox();
            this.btnBatchInfo = new System.Windows.Forms.Button();
            this.grpDataParms = new System.Windows.Forms.GroupBox();
            this.txtDepositDate = new System.Windows.Forms.TextBox();
            this.txtProcessingDate = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtLockboxID = new System.Windows.Forms.TextBox();
            this.txtBatchID = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBankID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.grpMain.SuspendLayout();
            this.grpRunOptions.SuspendLayout();
            this.grpDataParms.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pgsMain
            // 
            this.pgsMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pgsMain.Location = new System.Drawing.Point(3, 387);
            this.pgsMain.Name = "pgsMain";
            this.pgsMain.Size = new System.Drawing.Size(505, 10);
            this.pgsMain.TabIndex = 27;
            this.pgsMain.Visible = false;
            // 
            // grpMain
            // 
            this.grpMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpMain.Controls.Add(this.btnAbort);
            this.grpMain.Controls.Add(this.btnGo);
            this.grpMain.Controls.Add(this.grpRunOptions);
            this.grpMain.Controls.Add(this.grpDataParms);
            this.grpMain.Location = new System.Drawing.Point(3, 3);
            this.grpMain.Name = "grpMain";
            this.grpMain.Size = new System.Drawing.Size(505, 378);
            this.grpMain.TabIndex = 26;
            this.grpMain.TabStop = false;
            // 
            // btnAbort
            // 
            this.btnAbort.Enabled = false;
            this.btnAbort.Location = new System.Drawing.Point(424, 54);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(75, 23);
            this.btnAbort.TabIndex = 18;
            this.btnAbort.Text = "Abort";
            this.btnAbort.UseVisualStyleBackColor = true;
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(424, 25);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(75, 23);
            this.btnGo.TabIndex = 16;
            this.btnGo.Text = "Go";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // grpRunOptions
            // 
            this.grpRunOptions.Controls.Add(this.lstExtractRunHistory);
            this.grpRunOptions.Controls.Add(this.chkRunAsTest);
            this.grpRunOptions.Controls.Add(this.label5);
            this.grpRunOptions.Controls.Add(this.chkRerun);
            this.grpRunOptions.Controls.Add(this.btnBatchInfo);
            this.grpRunOptions.Location = new System.Drawing.Point(16, 212);
            this.grpRunOptions.Name = "grpRunOptions";
            this.grpRunOptions.Size = new System.Drawing.Size(402, 163);
            this.grpRunOptions.TabIndex = 12;
            this.grpRunOptions.TabStop = false;
            this.grpRunOptions.Text = "Run Options";
            // 
            // lstExtractRunHistory
            // 
            this.lstExtractRunHistory.Enabled = false;
            this.lstExtractRunHistory.FormattingEnabled = true;
            this.lstExtractRunHistory.Location = new System.Drawing.Point(100, 76);
            this.lstExtractRunHistory.Name = "lstExtractRunHistory";
            this.lstExtractRunHistory.Size = new System.Drawing.Size(205, 79);
            this.lstExtractRunHistory.TabIndex = 14;
            this.lstExtractRunHistory.SelectedValueChanged += new System.EventHandler(this.lstExtractRunHistory_SelectedValueChanged);
            // 
            // chkRunAsTest
            // 
            this.chkRunAsTest.AutoSize = true;
            this.chkRunAsTest.Location = new System.Drawing.Point(15, 32);
            this.chkRunAsTest.Name = "chkRunAsTest";
            this.chkRunAsTest.Size = new System.Drawing.Size(190, 17);
            this.chkRunAsTest.TabIndex = 12;
            this.chkRunAsTest.Text = "Run as test (ignore timestamp field)";
            this.chkRunAsTest.UseVisualStyleBackColor = true;
            this.chkRunAsTest.CheckedChanged += new System.EventHandler(this.chkRunAsTest_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Time Stamps:";
            // 
            // chkRerun
            // 
            this.chkRerun.AutoSize = true;
            this.chkRerun.Enabled = false;
            this.chkRerun.Location = new System.Drawing.Point(15, 53);
            this.chkRerun.Name = "chkRerun";
            this.chkRerun.Size = new System.Drawing.Size(366, 17);
            this.chkRerun.TabIndex = 13;
            this.chkRerun.Text = "Re-Run - Select the time stamp(s) of all the batches you want to include.";
            this.chkRerun.UseVisualStyleBackColor = true;
            this.chkRerun.CheckedChanged += new System.EventHandler(this.chkRerun_CheckedChanged);
            // 
            // btnBatchInfo
            // 
            this.btnBatchInfo.Enabled = false;
            this.btnBatchInfo.Location = new System.Drawing.Point(311, 76);
            this.btnBatchInfo.Name = "btnBatchInfo";
            this.btnBatchInfo.Size = new System.Drawing.Size(82, 23);
            this.btnBatchInfo.TabIndex = 15;
            this.btnBatchInfo.Text = "Batch Info";
            this.btnBatchInfo.UseVisualStyleBackColor = true;
            this.btnBatchInfo.Click += new System.EventHandler(this.btnBatchInfo_Click);
            // 
            // grpDataParms
            // 
            this.grpDataParms.Controls.Add(this.panel1);
            this.grpDataParms.Location = new System.Drawing.Point(16, 19);
            this.grpDataParms.Name = "grpDataParms";
            this.grpDataParms.Size = new System.Drawing.Size(402, 187);
            this.grpDataParms.TabIndex = 0;
            this.grpDataParms.TabStop = false;
            // 
            // txtDepositDate
            // 
            this.txtDepositDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtDepositDate.Location = new System.Drawing.Point(134, 141);
            this.txtDepositDate.Margin = new System.Windows.Forms.Padding(0);
            this.txtDepositDate.MaxLength = 100;
            this.txtDepositDate.Name = "txtDepositDate";
            this.txtDepositDate.Size = new System.Drawing.Size(129, 20);
            this.txtDepositDate.TabIndex = 11;
            this.txtDepositDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDepositDate_KeyDown);
            // 
            // txtProcessingDate
            // 
            this.txtProcessingDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtProcessingDate.Location = new System.Drawing.Point(134, 115);
            this.txtProcessingDate.Margin = new System.Windows.Forms.Padding(0);
            this.txtProcessingDate.MaxLength = 100;
            this.txtProcessingDate.Name = "txtProcessingDate";
            this.txtProcessingDate.Size = new System.Drawing.Size(129, 20);
            this.txtProcessingDate.TabIndex = 10;
            this.txtProcessingDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProcessingDate_KeyDown);
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Control;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Location = new System.Drawing.Point(292, 40);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.tableLayoutPanel1.SetRowSpan(this.textBox10, 7);
            this.textBox10.Size = new System.Drawing.Size(94, 81);
            this.textBox10.TabIndex = 15;
            this.textBox10.TabStop = false;
            this.textBox10.Text = "Separate multiple entries either with commas (1, 2, 3) or with a dash (1-2).  Do " +
    "not use spaces.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label11, 5);
            this.label11.Location = new System.Drawing.Point(68, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(317, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Disabled edits have been predefined by the Extract Definition File.";
            // 
            // txtLockboxID
            // 
            this.txtLockboxID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtLockboxID.Location = new System.Drawing.Point(134, 63);
            this.txtLockboxID.Margin = new System.Windows.Forms.Padding(0);
            this.txtLockboxID.MaxLength = 2500;
            this.txtLockboxID.Name = "txtLockboxID";
            this.txtLockboxID.Size = new System.Drawing.Size(129, 20);
            this.txtLockboxID.TabIndex = 8;
            // 
            // txtBatchID
            // 
            this.txtBatchID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtBatchID.Location = new System.Drawing.Point(134, 89);
            this.txtBatchID.Margin = new System.Windows.Forms.Padding(0);
            this.txtBatchID.MaxLength = 2500;
            this.txtBatchID.Name = "txtBatchID";
            this.txtBatchID.Size = new System.Drawing.Size(129, 20);
            this.txtBatchID.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label10.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label10, 2);
            this.label10.Location = new System.Drawing.Point(40, 118);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Processing Date: ";
            // 
            // txtBankID
            // 
            this.txtBankID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtBankID.Location = new System.Drawing.Point(134, 37);
            this.txtBankID.Margin = new System.Windows.Forms.Padding(0);
            this.txtBankID.MaxLength = 2500;
            this.txtBankID.Name = "txtBankID";
            this.txtBankID.Size = new System.Drawing.Size(129, 20);
            this.txtBankID.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Location = new System.Drawing.Point(93, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bank: ";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label7.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label7, 2);
            this.label7.Location = new System.Drawing.Point(90, 92);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Batch: ";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label6.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label6, 2);
            this.label6.Location = new System.Drawing.Point(56, 144);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Deposit Date: ";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label4, 2);
            this.label4.Location = new System.Drawing.Point(65, 66);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Workgroup: ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(396, 168);
            this.panel1.TabIndex = 19;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.txtDepositDate, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.label11, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtProcessingDate, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtLockboxID, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtBatchID, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox10, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtBankID, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 9);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(396, 168);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ctlExtractRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pgsMain);
            this.Controls.Add(this.grpMain);
            this.Name = "ctlExtractRun";
            this.Size = new System.Drawing.Size(511, 400);
            this.Load += new System.EventHandler(this.ctlExtractRun_Load);
            this.grpMain.ResumeLayout(false);
            this.grpRunOptions.ResumeLayout(false);
            this.grpRunOptions.PerformLayout();
            this.grpDataParms.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar pgsMain;
        private System.Windows.Forms.GroupBox grpMain;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.GroupBox grpRunOptions;
        private System.Windows.Forms.CheckedListBox lstExtractRunHistory;
        private System.Windows.Forms.CheckBox chkRunAsTest;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkRerun;
        private System.Windows.Forms.Button btnBatchInfo;
        private System.Windows.Forms.GroupBox grpDataParms;
        private System.Windows.Forms.TextBox txtDepositDate;
        private System.Windows.Forms.TextBox txtProcessingDate;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtLockboxID;
        private System.Windows.Forms.TextBox txtBatchID;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBankID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}
