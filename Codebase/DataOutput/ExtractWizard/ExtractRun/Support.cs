using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* TFS 115064 02/06/2014
*   - Changed noted constants for correct file extensions.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun {

    public delegate void BeginExtractRunEventHandler();
    public delegate void EndExtractRunEventHandler();

    internal static class Constants {
        // TFS 115064 : Changed .cxr with .xdf.
        public const string FILE_FILTER_EXTRACT_DEF = "Extract Definition Files (*.xdf)|*.xdf|Xml Files (*.xml)|*.xml";
        public const string FILE_FILTER_EXTRACT_DATA_FILES = "Extract Data Files (*.cxr)|*.cxr|Text Documents (*.txt)|*.txt|All Files |*.*";
        public const string FILE_FILTER_LOG_FILES = "Extract Log Files (*.log)|*.log|Text Documents (*.txt)|*.txt|All Files |*.*";
        public const string FILE_FILTER_DLL = "Dynamic Link Libraries (*.dll)|*.dll";
    }
}
