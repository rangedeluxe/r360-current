using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.DAL.ExtractEngine;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/24/2013
*   - Initial Version 
* WI 130165 DLD 02/19/2014
*   - Expanded Exception handling and message logging. 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added additional exception logging.
*   - Removed ltaLog property and cut over to ExtractLib.cCommonLogLib class   
* WI 129400 BLR 03/19/2014
*   - Added a bypass on the logon form until RAAM is ready.
* WI 157574 BLR 08/08/2014
*   - Added entity for RAAM. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractRun
{

    static class Program
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool FreeConsole();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {

            string arg;
            int intTemp;
            Guid gidTemp;
            DateTime dteTempDateFrom;
            DateTime dteTempDateTo;

            string strLogonName = string.Empty;
            string strPassword = string.Empty;
            string strEntity = string.Empty;
            bool bolRunAsTest = false;
            bool bolLoginSuccess = false;
            bool bolDateParmWasSpecified = false;

            int intUserID;
            Guid gidSessionID;
            string strUserMessage;

            cExtractParms Parms = new cExtractParms();

            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += UnhandledException;

            var settings = new ExtractConfigurationSettings(new ConfigurationManagerProvider());

            try { settings.ValidateSettings(); }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }

            cOLFServicesLib.Settings = settings;

            if (args.Length > 0)
            {

                for (int i = 0; i < args.Length; ++i)
                {

                    arg = args[i].Trim().ToLower();

                    if (arg.StartsWith("-?"))
                    {
                        Usage();
                        return (-1);
                    }
                    else if (arg.StartsWith("/?"))
                    {
                        Usage();
                        return (-1);
                    }

                    if (i == 0)
                    {
                        Parms.SetupFile = args[0].Trim();
                    }
                    else
                    {
                        if (arg.ToLower().StartsWith("/trace:") && arg.Length > arg.IndexOf(':') + 1)
                        {
                            Parms.TraceField = args[i].Trim().Substring(arg.IndexOf(':') + 1);
                        }
                        else if (arg.ToLower().StartsWith("/test"))
                        {
                            bolRunAsTest = true;
                            //} else if((arg.StartsWith("/e:")||arg.StartsWith("/scriptpath:")) && arg.Length > arg.IndexOf(':') + 1) {
                            //    Parms.SetupFile = args[i].Trim().Substring(arg.IndexOf(':') + 1);
                            //} else if(arg.StartsWith("/silent")) {
                            //    bolSilentMode = true;
                        }
                        else if ((arg.StartsWith("/q:") || arg.StartsWith("/sequence:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            if (int.TryParse(arg.Substring(arg.IndexOf(':') + 1), out intTemp))
                            {
                                Parms.ExtractSequenceNumber = intTemp;
                            }
                        }
                        else if ((arg.StartsWith("/i:") || arg.StartsWith("/imagepath:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            Parms.ImagePath = args[i].Trim().Substring(arg.IndexOf(':') + 1);
                        }
                        else if ((arg.StartsWith("/l:") || arg.StartsWith("/logon:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            strLogonName = args[i].Trim().Substring(arg.IndexOf(':') + 1);
                        }
                        else if (arg.StartsWith("/password:") && arg.Length > arg.IndexOf(':') + 1)
                        {
                            strPassword = args[i].Trim().Substring(arg.IndexOf(':') + 1);
                        }
                        else if (arg.StartsWith("/entity:") && arg.Length > arg.IndexOf(':') + 1)
                        {
                            strEntity = args[i].Trim().Substring(arg.IndexOf(':') + 1);
                        }
                        else if (arg.StartsWith("/o:") ||
                                 arg.StartsWith("/outputmode:") && arg.Length > arg.IndexOf(':') + 1)
                        {
                            int outMode = int.TryParse(args[i].Trim().Substring(arg.IndexOf(':') + 1), out outMode) ? outMode : 0;
                            Parms.OutputMode = outMode;
                            cCommonLogLib.LogMessageLTA("Outputmode: " + Parms.OutputMode, LTAMessageImportance.Debug);
                        }
                        else if ((arg.StartsWith("/f:") || arg.StartsWith("/targetfile:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            Parms.TargetFile = args[i].Trim().Substring(arg.IndexOf(':') + 1);


                            //----------- Legacy Parameters Begin -------------------------------

                            //---------- Preferred calling convention for Legacy Parameters ----
                        }
                        else if ((arg.StartsWith("/b:") || arg.StartsWith("/bankid:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            Parms.BankID = cExtractParms.ParseLongs(arg.Substring(arg.IndexOf(':') + 1));
                        }
                        else if ((arg.StartsWith("/x:") || arg.StartsWith("/lockboxid:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            Parms.LockboxID = cExtractParms.ParseLongs(arg.Substring(arg.IndexOf(':') + 1));
                        }
                        else if ((arg.StartsWith("/t:") || arg.StartsWith("/batchid:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            Parms.BatchID = cExtractParms.ParseLongs(arg.Substring(arg.IndexOf(':') + 1));
                        }
                        else if ((arg.StartsWith("/p:") || arg.StartsWith("/procdate:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            if (cExtractParms.ParseDateString(arg.Substring(arg.IndexOf(':') + 1), out dteTempDateFrom, out dteTempDateTo))
                            {
                                Parms.ProcessingDateFrom = dteTempDateFrom;
                                Parms.ProcessingDateTo = dteTempDateTo;
                                bolDateParmWasSpecified = true;
                            }
                        }
                        else if ((arg.StartsWith("/d:") || arg.StartsWith("/depdate:")) && arg.Length > arg.IndexOf(':') + 1)
                        {
                            if (cExtractParms.ParseDateString(arg.Substring(arg.IndexOf(':') + 1), out dteTempDateFrom, out dteTempDateTo))
                            {
                                Parms.DepositDateFrom = dteTempDateFrom;
                                Parms.DepositDateTo = dteTempDateTo;
                                bolDateParmWasSpecified = true;
                            }
                        }
                        else if (arg.StartsWith("/b") && arg.Length > 2)
                        {
                            Parms.BankID = cExtractParms.ParseLongs(arg.Substring(2));
                        }
                        else if (arg.StartsWith("/x") && arg.Length > 2)
                        {
                            Parms.LockboxID = cExtractParms.ParseLongs(arg.Substring(2));
                        }
                        else if (arg.StartsWith("/t") && arg.Length > 2)
                        {
                            Parms.BatchID = cExtractParms.ParseLongs(arg.Substring(2));
                        }
                        else if (arg.StartsWith("/p") && arg.Length > 2)
                        {
                            if (cExtractParms.ParseDateString(arg.Substring(2), out dteTempDateFrom, out dteTempDateTo))
                            {
                                Parms.ProcessingDateFrom = dteTempDateFrom;
                                Parms.ProcessingDateTo = dteTempDateTo;
                                bolDateParmWasSpecified = true;
                            }
                        }
                        else if (arg.StartsWith("/d") && arg.Length > 2)
                        {
                            if (cExtractParms.ParseDateString(arg.Substring(2), out dteTempDateFrom, out dteTempDateTo))
                            {
                                Parms.DepositDateFrom = dteTempDateFrom;
                                Parms.DepositDateTo = dteTempDateTo;
                                bolDateParmWasSpecified = true;
                            }
                        }
                        else if (arg.StartsWith("/a") && arg.Length > 2)
                        {
                            if (CommonLib.TryParse(args[i].Trim().Substring(2), out gidTemp))
                            {
                                Parms.ExtractAuditID = gidTemp;
                            }
                            //---------- Legacy Parameters End ---------------------------------

                        }
                        else
                        {
                            Usage();
                            return (-1);
                        }
                    }
                }

                strUserMessage = string.Empty;

                if (!bolDateParmWasSpecified)
                {
                    cCommonLogLib.LogMessageLTA("Date Param Missing", LTAMessageImportance.Essential);
                    return -1;
                }

                if (new cLogon().SilentLogin(strEntity, strLogonName, strPassword, int.Parse(settings.LoginAttempts), out intUserID, out gidSessionID, out strUserMessage))
                {
                    Parms.UserID = intUserID;
                    RunSilent(Parms, bolRunAsTest);
                }
                else
                {
                    // Send the fatal error message.
                    var alertname = ExtractAPI.Support.ALERT_EXTRACT_FAILURE;
                    var message = "Extract Run failed to connect to RAAM. " 
                        + Environment.NewLine 
                        + "Warning: You may want to verify the extracts were created successfully and have data in them.";
                    var dal = new cExtractEngineDAL(settings.SiteKey, settings.ConnectionString);
                    if (!dal.WriteSystemLevelEventLog(alertname, message))
                    {
                        cCommonLogLib.LogErrorLTA(new Exception("Error: Failed writing alert after extract run could not log into RAAM."), LTAMessageImportance.Essential);
                    }

                    if (strUserMessage.Length > 0)
                    {
                        cCommonLogLib.LogErrorLTA(new Exception(strUserMessage), LTAMessageImportance.Essential);
                        Console.WriteLine(strUserMessage);
                    }
                    else
                    {
                        Usage();
                    }

                    return (-1);
                }
            }
            else
            {

                // This seems to get rid of the console window before it shows
                FreeConsole();

                intUserID = -1;
                if (!bolLoginSuccess)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);

                    bolLoginSuccess = new cLogon().CollectLogin(out intUserID, out gidSessionID);
                    Parms.UserID = intUserID;
                }

                if (bolLoginSuccess)
                {
                    Application.Run(new frmExtractRun(intUserID, Parms, bolRunAsTest));
                }
            }

            return (0);
        }

        private static void RunSilent(cExtractParms parms, bool runAsTest)
        {

            cExtractDef objExtractDef;

            if (parms.SetupFile.Length > 0)
            {

                if (File.Exists(parms.SetupFile))
                {

                    objExtractDef = new cExtractDef(parms.SetupFile);
                    objExtractDef.OutputError += OnErrorOccurred;

                    if (parms.SetupFile.ToLower().EndsWith(".xml"))
                    {
                        if (objExtractDef.ParseExtractDefXmlFile(parms.SetupFile))
                        {
                            RunExtract(parms, objExtractDef, runAsTest);
                        }
                    }
                    else if (objExtractDef.ParseExtractDefFile(parms.SetupFile))
                    {
                        RunExtract(parms, objExtractDef, runAsTest);
                    }

                }
                else
                {
                    Console.WriteLine("\nExtract script : [" + parms.SetupFile + "] file does not exist.'");
                }
            }
            else
            {
                Console.WriteLine("\nUse the  /E tag to specify an Extract file in order to use 'Silent Mode'");
            }
        }

        private static void RunExtract(
            cExtractParms parms,
            cExtractDef extractDef,
            bool runAsTest)
        {

            Guid gidExtractAuditID;
            StringDictionary arExtractStats;

            using (cExtract objExtract = new cExtract())
            {
                objExtract.OutputError += OnErrorOccurred;
                try
                {
                    if (runAsTest)
                    {
                        parms.TraceField = string.Empty;
                        parms.RerunList = new DateTime[] { };
                    }
                    else if (parms.TraceField == null || parms.TraceField.Length == 0)
                    {
                        parms.TraceField = extractDef.TimeStamp;
                    }

                    objExtract.RunExtract(extractDef, parms.ApplyDefaults(extractDef), out gidExtractAuditID, out arExtractStats);
                }
                catch (Exception ex)
                {
                    // DLD - WI 130165
                    OnErrorOccurred("RunExtract", ex);
                }
            }
        }

        private static void Usage()
        {

            StringBuilder sbMsg = new StringBuilder();

            sbMsg.Append("\n");
            sbMsg.Append("************************************************************\n");
            sbMsg.Append("*                                                          *\n");
            sbMsg.Append("* Usage:                                                   *\n");
            sbMsg.Append("*                                                          *\n");
            sbMsg.Append("* ExtractRun ScriptName                                    *\n");
            sbMsg.Append("*            /B<BankID>                                    *\n");
            sbMsg.Append("*            /X<LockboxID>                                 *\n");
            sbMsg.Append("*            /T<BatchID>                                   *\n");
            sbMsg.Append("*            /P<ProcessingDate>                            *\n");
            sbMsg.Append("*            /D<DepositDate(Time)>                         *\n");
            sbMsg.Append("*            /A<ExtractAudiitID>                           *\n");
            sbMsg.Append("*                                                          *\n");
            sbMsg.Append("************************************************************\n");

            Console.WriteLine(sbMsg.ToString());
        }

        private static void OnErrorOccurred(System.Exception e)
        {
            // DLD - WI 130165
            if (e != null)
                OnErrorOccurred(e.Source, e);
        }

        private static void OnErrorOccurred(object sender, System.Exception e)
        {
            if (e != null)
            {
                Console.WriteLine("Exception Occurred -\n\n" + e.StackTrace.TrimStart() + "\n\n" + e.Message);
                // DLD - WI 130165
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
        }

        static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e != null)
                OnErrorOccurred(sender, e.ExceptionObject as Exception);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            if (e != null)
                OnErrorOccurred(sender, e.Exception);
        }
    }
}