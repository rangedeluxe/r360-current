using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

/*******************************************************************************
*
*    Module: frmDisplayText
*  Filename: frmDisplayText.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace IntegraPAY.Delivery.Extract.ExtractAPI {

    public partial class frmDisplayText : Form {

        public frmDisplayText(string TextDescription, string Text) {

            InitializeComponent();

            this.Text = TextDescription;
            this.txtMain.Text = Text;
        }

        private void btnOk_Click(object sender, EventArgs e) {
            this.Close();
        }
    }
}