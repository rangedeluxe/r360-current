using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal partial class ctlDataGrid : UserControl {

        public event ItemDroppedEventHandler ItemDropped;
        public event ItemMovedEventHandler ItemMoved;
        public event ItemDeletedEventHandler ItemDeleted;
        
        public event DataGridViewCellEventHandler CellEndEdit;
        public event DataGridViewCellValidatingEventHandler CellValidating;
        public event DataGridViewEditingControlShowingEventHandler EditingControlShowing;

        private bool _Initializing = false;

        public ctlDataGrid() {
            InitializeComponent();
        }
        
        public void SuspendEvents() {
            _Initializing = true;
        }
        
        public void ResumeEvents() {
            _Initializing = false;
        }

        public int AddRow(DataGridViewRow dataGridViewRow) {
            return(this.dataGridView1.Rows.Add(dataGridViewRow));
        }
        
        public void Clear() {
            this.dataGridView1.Rows.Clear();
        }

        public DataGridViewCell this[int columnIndex, int rowIndex] {
            get {
                return(this.dataGridView1[columnIndex, rowIndex]);
            }
        }

        public DataGridViewColumnCollection Columns {
            get {
                return(this.dataGridView1.Columns);
            }
        }

        public DataGridViewRowCollection Rows {
            get {
                return(this.dataGridView1.Rows);
            }
        }

        public DataGridViewRow SelectedRow {
            get {
                if(this.dataGridView1.SelectedRows.Count > 0) {
                    return(this.dataGridView1.SelectedRows[0]);
                } else {
                    return(null);
                }
            }
        }
        
        public bool Initializing {
            get {
                return(_Initializing);
            }
        }

        public void AddColumnsRange(DataGridViewColumn[] dataGridViewColumns) {
            this.dataGridView1.Columns.AddRange(dataGridViewColumns);
        }

        private void dataGridView1_DragDrop(object sender, DragEventArgs e) {

            if(ItemDropped != null) {
                ItemDropped(e.Data.GetData("System.String").ToString());
            }
        }

        private void dataGridView1_DragEnter(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Copy;
        }

        private void dataGridView1_DragOver(object sender, DragEventArgs e) {
            e.Effect = DragDropEffects.Copy;
        }

        private void dataGridView1_Leave(object sender, EventArgs e) {
            if(sender != null) {
                ((DataGridView)sender).EndEdit();
            }
        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e) {
            if(!_Initializing && ItemDeleted != null) {
                ItemDeleted(e.RowIndex);
            }
        }

        private void btnRowMoveUp_Click(object sender, EventArgs e) {

            int intCurrentIndex;
            
            if(this.dataGridView1.SelectedRows.Count > 0 && this.dataGridView1.SelectedRows[0].Index > 0) {

                intCurrentIndex = this.dataGridView1.SelectedRows[0].Index;

                MoveItem(intCurrentIndex, intCurrentIndex - 1);
            }
        }

        private void btnRowMoveDown_Click(object sender, EventArgs e) {

            int intCurrentIndex;
            
            if(this.dataGridView1.SelectedRows.Count > 0 && 
               this.dataGridView1.SelectedRows[0].Index < this.dataGridView1.Rows.Count -1) {

                intCurrentIndex = this.dataGridView1.SelectedRows[0].Index;

                MoveItem(intCurrentIndex, intCurrentIndex + 1);
            }
        }

        private void btnRowDelete_Click(object sender, EventArgs e) {
            if(this.dataGridView1.SelectedRows.Count > 0) {
                this.dataGridView1.Rows.RemoveAt(this.dataGridView1.SelectedRows[0].Index);
            }
        }

        public void MoveItem(int currentIndex, int newIndex) {

            DataGridViewRow dgRow;

            _Initializing = true;

            // Move the Layout Field
            dgRow = this.dataGridView1.Rows[currentIndex];
            this.dataGridView1.Rows.RemoveAt(currentIndex);
            this.dataGridView1.Rows.Insert(newIndex, dgRow);

            // De-Select all gridrows
            while(dataGridView1.SelectedRows.Count > 0) {
                dataGridView1.SelectedRows[0].Selected = false;
            }

            if(ItemMoved != null) {
                ItemMoved(currentIndex, newIndex);
            }

            // Select moved gridrow
            this.dataGridView1.Rows[newIndex].Selected = true;

            _Initializing = false;
        }

        public void RemoveAt(int index) {
            _Initializing = true;
            this.dataGridView1.Rows.RemoveAt(index);
            _Initializing = false;
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e) {

            if(CellEndEdit != null) {
                CellEndEdit(sender, e);
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {

            if(CellValidating != null) {
                CellValidating(sender, e);
            }
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) {
            if(EditingControlShowing != null) {
                EditingControlShowing(sender, e);
            }
        }

        private void ctlDataGrid_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}
