using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractRun;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/21/2013
*   -Initial Version 
* WI 130165 DLD 02/19/2014, 02/25/2014, 03/05/2014
*   -Expanded Exception handling and message logging. 
*   -Quick bug fix.
*   -Removed ltaLog property and diverted all calls to the new cCommonLogLib class in ExtractLib   
* WI 138812 BLR 07/11/2014
*   - Removed the Logon bypass in preparation for RAAM.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var userID = 0;
            var gidSessionID = Guid.NewGuid();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.ThreadException += Application_ThreadException;
            AppDomain.CurrentDomain.UnhandledException += UnhandledException;

            if (new cLogon().CollectLogin(out userID, out gidSessionID))
            {
                Application.Run(new frmMain(userID));
            }

        }

        static void UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            // DLD - WI 130165
            if (sender != null && e != null)
            {
                var ex = e.ExceptionObject as Exception;
                if (ex != null)
                {
                    cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                    ShowUnhandledExceptionDialog(ex.Message);
                }
            }
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            // DLD - WI 130165
            if (e.Exception != null)
            {
                cCommonLogLib.LogErrorLTA(e.Exception, LTAMessageImportance.Essential);
                ShowUnhandledExceptionDialog(e.Exception.Message);
            }
        }

        internal static void ShowUnhandledExceptionDialog(string msg = "")
        {
            // DLD - WI 130165
            if (String.IsNullOrWhiteSpace(msg))
                msg = ExtractLib.Constants.ERROR_MSG_UNHANDLED;
            else
                msg = ExtractLib.Constants.ERROR_MSG_UNHANDLED + Environment.NewLine + Environment.NewLine + "MESSAGE: " + msg ;
            
            MessageBox.Show(msg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}