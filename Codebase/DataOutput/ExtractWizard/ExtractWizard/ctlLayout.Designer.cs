/*******************************************************************************
*
*    Module: ctlLayout
*  Filename: ctlLayout.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class ctlLayout {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.grpHeaderTrailer = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rdoFileRecordTypeTrailer = new System.Windows.Forms.RadioButton();
            this.rdoFileRecordTypeHeader = new System.Windows.Forms.RadioButton();
            this.colFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colColumnWidth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPadChar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colJustify = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.colQuotes = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colFormat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOccursGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colOccursCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnTestLayout = new System.Windows.Forms.Button();
            this.txtPaymentsOccursCount = new WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.NumericTextBox();
            this.txtStubsOccursCount = new WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.NumericTextBox();
            this.txtDocumentsOccursCount = new WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.NumericTextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chkUseStubsOccursGroups = new System.Windows.Forms.CheckBox();
            this.chkUseDocumentsOccursGroups = new System.Windows.Forms.CheckBox();
            this.chkUsePaymentsOccursGroups = new System.Windows.Forms.CheckBox();
            this.txtMaxRepeats = new WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.NumericTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkUseOccursGroups = new System.Windows.Forms.CheckBox();
            this.txtLayoutLevel = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grpOccursGroupOptions = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ctlDataGrid1 = new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ctlDataGrid();
            this.grpHeaderTrailer.SuspendLayout();
            this.grpOccursGroupOptions.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpHeaderTrailer
            // 
            this.grpHeaderTrailer.Controls.Add(this.label4);
            this.grpHeaderTrailer.Controls.Add(this.rdoFileRecordTypeTrailer);
            this.grpHeaderTrailer.Controls.Add(this.rdoFileRecordTypeHeader);
            this.grpHeaderTrailer.Location = new System.Drawing.Point(223, 0);
            this.grpHeaderTrailer.Margin = new System.Windows.Forms.Padding(4);
            this.grpHeaderTrailer.Name = "grpHeaderTrailer";
            this.grpHeaderTrailer.Padding = new System.Windows.Forms.Padding(4);
            this.grpHeaderTrailer.Size = new System.Drawing.Size(203, 34);
            this.grpHeaderTrailer.TabIndex = 4;
            this.grpHeaderTrailer.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 15);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Record Type:";
            // 
            // rdoFileRecordTypeTrailer
            // 
            this.rdoFileRecordTypeTrailer.AutoSize = true;
            this.rdoFileRecordTypeTrailer.Location = new System.Drawing.Point(143, 13);
            this.rdoFileRecordTypeTrailer.Margin = new System.Windows.Forms.Padding(4);
            this.rdoFileRecordTypeTrailer.Name = "rdoFileRecordTypeTrailer";
            this.rdoFileRecordTypeTrailer.Size = new System.Drawing.Size(54, 17);
            this.rdoFileRecordTypeTrailer.TabIndex = 1;
            this.rdoFileRecordTypeTrailer.TabStop = true;
            this.rdoFileRecordTypeTrailer.Text = "Trailer";
            this.rdoFileRecordTypeTrailer.UseVisualStyleBackColor = true;
            this.rdoFileRecordTypeTrailer.CheckedChanged += new System.EventHandler(this.rdoFileRecordTypeTrailer_CheckedChanged);
            // 
            // rdoFileRecordTypeHeader
            // 
            this.rdoFileRecordTypeHeader.AutoSize = true;
            this.rdoFileRecordTypeHeader.Location = new System.Drawing.Point(84, 13);
            this.rdoFileRecordTypeHeader.Margin = new System.Windows.Forms.Padding(4);
            this.rdoFileRecordTypeHeader.Name = "rdoFileRecordTypeHeader";
            this.rdoFileRecordTypeHeader.Size = new System.Drawing.Size(60, 17);
            this.rdoFileRecordTypeHeader.TabIndex = 0;
            this.rdoFileRecordTypeHeader.TabStop = true;
            this.rdoFileRecordTypeHeader.Text = "Header";
            this.rdoFileRecordTypeHeader.UseVisualStyleBackColor = true;
            this.rdoFileRecordTypeHeader.CheckedChanged += new System.EventHandler(this.rdoFileRecordTypeHeader_CheckedChanged);
            // 
            // colFieldName
            // 
            this.colFieldName.HeaderText = "Field Name";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.ReadOnly = true;
            this.colFieldName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldName.Width = 150;
            // 
            // colDisplayName
            // 
            this.colDisplayName.HeaderText = "Display Name";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colDisplayName.Width = 120;
            // 
            // colColumnWidth
            // 
            this.colColumnWidth.HeaderText = "Column Width";
            this.colColumnWidth.Name = "colColumnWidth";
            this.colColumnWidth.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colColumnWidth.Width = 90;
            // 
            // colPadChar
            // 
            this.colPadChar.HeaderText = "Pad Character";
            this.colPadChar.Name = "colPadChar";
            this.colPadChar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colPadChar.Width = 65;
            // 
            // colJustify
            // 
            this.colJustify.HeaderText = "Justify (L/R)";
            this.colJustify.Name = "colJustify";
            this.colJustify.Width = 55;
            // 
            // colQuotes
            // 
            this.colQuotes.HeaderText = "Quotes";
            this.colQuotes.Name = "colQuotes";
            this.colQuotes.Width = 60;
            // 
            // colFormat
            // 
            this.colFormat.HeaderText = "Format";
            this.colFormat.Name = "colFormat";
            this.colFormat.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFormat.Width = 220;
            // 
            // colOccursGroup
            // 
            this.colOccursGroup.FillWeight = 50F;
            this.colOccursGroup.HeaderText = "Occurs Group";
            this.colOccursGroup.Name = "colOccursGroup";
            this.colOccursGroup.Visible = false;
            // 
            // colOccursCount
            // 
            this.colOccursCount.FillWeight = 50F;
            this.colOccursCount.HeaderText = "Occurs Count";
            this.colOccursCount.Name = "colOccursCount";
            this.colOccursCount.Visible = false;
            // 
            // btnTestLayout
            // 
            this.btnTestLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTestLayout.Location = new System.Drawing.Point(246, 522);
            this.btnTestLayout.Margin = new System.Windows.Forms.Padding(4);
            this.btnTestLayout.Name = "btnTestLayout";
            this.btnTestLayout.Size = new System.Drawing.Size(75, 23);
            this.btnTestLayout.TabIndex = 10;
            this.btnTestLayout.Text = "Test Layout";
            this.btnTestLayout.Click += new System.EventHandler(this.btnTestLayout_Click);
            // 
            // txtPaymentsOccursCount
            // 
            this.txtPaymentsOccursCount.Location = new System.Drawing.Point(110, 55);
            this.txtPaymentsOccursCount.Margin = new System.Windows.Forms.Padding(4);
            this.txtPaymentsOccursCount.MaxLength = 2;
            this.txtPaymentsOccursCount.Name = "txtPaymentsOccursCount";
            this.txtPaymentsOccursCount.Size = new System.Drawing.Size(32, 20);
            this.txtPaymentsOccursCount.TabIndex = 16;
            this.txtPaymentsOccursCount.TextChanged += new System.EventHandler(this.txtPaymentsOccursCount_TextChanged);
            // 
            // txtStubsOccursCount
            // 
            this.txtStubsOccursCount.Location = new System.Drawing.Point(110, 78);
            this.txtStubsOccursCount.Margin = new System.Windows.Forms.Padding(4);
            this.txtStubsOccursCount.MaxLength = 2;
            this.txtStubsOccursCount.Name = "txtStubsOccursCount";
            this.txtStubsOccursCount.Size = new System.Drawing.Size(32, 20);
            this.txtStubsOccursCount.TabIndex = 15;
            this.txtStubsOccursCount.TextChanged += new System.EventHandler(this.txtStubsOccursCount_TextChanged);
            // 
            // txtDocumentsOccursCount
            // 
            this.txtDocumentsOccursCount.Location = new System.Drawing.Point(110, 100);
            this.txtDocumentsOccursCount.Margin = new System.Windows.Forms.Padding(4);
            this.txtDocumentsOccursCount.MaxLength = 2;
            this.txtDocumentsOccursCount.Name = "txtDocumentsOccursCount";
            this.txtDocumentsOccursCount.Size = new System.Drawing.Size(32, 20);
            this.txtDocumentsOccursCount.TabIndex = 14;
            this.txtDocumentsOccursCount.TextChanged += new System.EventHandler(this.txtDocumentsOccursCount_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Location = new System.Drawing.Point(110, 22);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(39, 26);
            this.textBox2.TabIndex = 13;
            this.textBox2.Text = "Occurs Count";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(69, 22);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(35, 26);
            this.textBox1.TabIndex = 12;
            this.textBox1.Text = "Use Occurs";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Stubs:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Documents:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Checks:";
            // 
            // chkUseStubsOccursGroups
            // 
            this.chkUseStubsOccursGroups.AutoSize = true;
            this.chkUseStubsOccursGroups.Location = new System.Drawing.Point(78, 81);
            this.chkUseStubsOccursGroups.Name = "chkUseStubsOccursGroups";
            this.chkUseStubsOccursGroups.Size = new System.Drawing.Size(15, 14);
            this.chkUseStubsOccursGroups.TabIndex = 6;
            this.chkUseStubsOccursGroups.UseVisualStyleBackColor = true;
            this.chkUseStubsOccursGroups.CheckedChanged += new System.EventHandler(this.chkUseStubsOccursGroups_CheckedChanged);
            // 
            // chkUseDocumentsOccursGroups
            // 
            this.chkUseDocumentsOccursGroups.AutoSize = true;
            this.chkUseDocumentsOccursGroups.Location = new System.Drawing.Point(78, 103);
            this.chkUseDocumentsOccursGroups.Name = "chkUseDocumentsOccursGroups";
            this.chkUseDocumentsOccursGroups.Size = new System.Drawing.Size(15, 14);
            this.chkUseDocumentsOccursGroups.TabIndex = 5;
            this.chkUseDocumentsOccursGroups.UseVisualStyleBackColor = true;
            this.chkUseDocumentsOccursGroups.CheckedChanged += new System.EventHandler(this.chkUseDocumentsOccursGroups_CheckedChanged);
            // 
            // chkUsePaymentsOccursGroups
            // 
            this.chkUsePaymentsOccursGroups.AutoSize = true;
            this.chkUsePaymentsOccursGroups.Location = new System.Drawing.Point(78, 58);
            this.chkUsePaymentsOccursGroups.Name = "chkUsePaymentsOccursGroups";
            this.chkUsePaymentsOccursGroups.Size = new System.Drawing.Size(15, 14);
            this.chkUsePaymentsOccursGroups.TabIndex = 4;
            this.chkUsePaymentsOccursGroups.UseVisualStyleBackColor = true;
            this.chkUsePaymentsOccursGroups.CheckedChanged += new System.EventHandler(this.chkUsePaymentsOccursGroups_CheckedChanged);
            // 
            // txtMaxRepeats
            // 
            this.txtMaxRepeats.Location = new System.Drawing.Point(110, 174);
            this.txtMaxRepeats.Margin = new System.Windows.Forms.Padding(4);
            this.txtMaxRepeats.MaxLength = 2;
            this.txtMaxRepeats.Name = "txtMaxRepeats";
            this.txtMaxRepeats.Size = new System.Drawing.Size(32, 20);
            this.txtMaxRepeats.TabIndex = 2;
            this.txtMaxRepeats.TextChanged += new System.EventHandler(this.txtMaxRepeats_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 177);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Max Repeats:";
            // 
            // chkUseOccursGroups
            // 
            this.chkUseOccursGroups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkUseOccursGroups.AutoSize = true;
            this.chkUseOccursGroups.Location = new System.Drawing.Point(871, 17);
            this.chkUseOccursGroups.Name = "chkUseOccursGroups";
            this.chkUseOccursGroups.Size = new System.Drawing.Size(119, 17);
            this.chkUseOccursGroups.TabIndex = 3;
            this.chkUseOccursGroups.Text = "Use Occurs Groups";
            this.chkUseOccursGroups.UseVisualStyleBackColor = true;
            this.chkUseOccursGroups.CheckedChanged += new System.EventHandler(this.chkUseOccursColumns_CheckedChanged);
            // 
            // txtLayoutLevel
            // 
            this.txtLayoutLevel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLayoutLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLayoutLevel.Location = new System.Drawing.Point(86, 15);
            this.txtLayoutLevel.Margin = new System.Windows.Forms.Padding(4);
            this.txtLayoutLevel.Name = "txtLayoutLevel";
            this.txtLayoutLevel.ReadOnly = true;
            this.txtLayoutLevel.Size = new System.Drawing.Size(118, 13);
            this.txtLayoutLevel.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Layout Level:";
            // 
            // grpOccursGroupOptions
            // 
            this.grpOccursGroupOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpOccursGroupOptions.Controls.Add(this.label1);
            this.grpOccursGroupOptions.Controls.Add(this.textBox2);
            this.grpOccursGroupOptions.Controls.Add(this.txtMaxRepeats);
            this.grpOccursGroupOptions.Controls.Add(this.txtDocumentsOccursCount);
            this.grpOccursGroupOptions.Controls.Add(this.textBox1);
            this.grpOccursGroupOptions.Controls.Add(this.label3);
            this.grpOccursGroupOptions.Controls.Add(this.txtPaymentsOccursCount);
            this.grpOccursGroupOptions.Controls.Add(this.label7);
            this.grpOccursGroupOptions.Controls.Add(this.chkUseDocumentsOccursGroups);
            this.grpOccursGroupOptions.Controls.Add(this.txtStubsOccursCount);
            this.grpOccursGroupOptions.Controls.Add(this.chkUseStubsOccursGroups);
            this.grpOccursGroupOptions.Controls.Add(this.chkUsePaymentsOccursGroups);
            this.grpOccursGroupOptions.Controls.Add(this.label6);
            this.grpOccursGroupOptions.Location = new System.Drawing.Point(871, 39);
            this.grpOccursGroupOptions.Name = "grpOccursGroupOptions";
            this.grpOccursGroupOptions.Size = new System.Drawing.Size(165, 252);
            this.grpOccursGroupOptions.TabIndex = 15;
            this.grpOccursGroupOptions.TabStop = false;
            this.grpOccursGroupOptions.Text = "Occurs Groups Options";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtLayoutLevel);
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(213, 34);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // ctlDataGrid1
            // 
            this.ctlDataGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlDataGrid1.Location = new System.Drawing.Point(0, 37);
            this.ctlDataGrid1.Margin = new System.Windows.Forms.Padding(5);
            this.ctlDataGrid1.Name = "ctlDataGrid1";
            this.ctlDataGrid1.Size = new System.Drawing.Size(863, 511);
            this.ctlDataGrid1.TabIndex = 12;
            this.ctlDataGrid1.ItemDropped += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemDroppedEventHandler(this.ctlDataGrid1_ItemDropped);
            this.ctlDataGrid1.ItemMoved += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemMovedEventHandler(this.ctlDataGrid1_ItemMoved);
            this.ctlDataGrid1.ItemDeleted += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemDeletedEventHandler(this.ctlDataGrid1_ItemDeleted);
            this.ctlDataGrid1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlDataGrid1_CellEndEdit);
            this.ctlDataGrid1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ctlDataGrid1_CellValidating);
            this.ctlDataGrid1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.ctlDataGrid1_EditingControlShowing);
            // 
            // ctlLayout
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkUseOccursGroups);
            this.Controls.Add(this.grpOccursGroupOptions);
            this.Controls.Add(this.btnTestLayout);
            this.Controls.Add(this.grpHeaderTrailer);
            this.Controls.Add(this.ctlDataGrid1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ctlLayout";
            this.Size = new System.Drawing.Size(1041, 548);
            this.Load += new System.EventHandler(this.ctlLayout_Load);
            this.grpHeaderTrailer.ResumeLayout(false);
            this.grpHeaderTrailer.PerformLayout();
            this.grpOccursGroupOptions.ResumeLayout(false);
            this.grpOccursGroupOptions.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpHeaderTrailer;
        private System.Windows.Forms.RadioButton rdoFileRecordTypeTrailer;
        private System.Windows.Forms.RadioButton rdoFileRecordTypeHeader;
        private System.Windows.Forms.Button btnTestLayout;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDisplayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colColumnWidth;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPadChar;
        private System.Windows.Forms.DataGridViewComboBoxColumn colJustify;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colQuotes;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFormat;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOccursGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn colOccursCount;
        private WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.NumericTextBox txtMaxRepeats;
        private System.Windows.Forms.Label label1;
        private ctlDataGrid ctlDataGrid1;
        private System.Windows.Forms.TextBox txtLayoutLevel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkUseStubsOccursGroups;
        private System.Windows.Forms.CheckBox chkUseDocumentsOccursGroups;
        private System.Windows.Forms.CheckBox chkUsePaymentsOccursGroups;
        private System.Windows.Forms.CheckBox chkUseOccursGroups;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.NumericTextBox txtPaymentsOccursCount;
        private WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.NumericTextBox txtStubsOccursCount;
        private WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.NumericTextBox txtDocumentsOccursCount;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox grpOccursGroupOptions;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}
