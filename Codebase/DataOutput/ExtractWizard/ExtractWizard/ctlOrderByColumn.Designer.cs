/*******************************************************************************
*
*    Module: ctlOrderByColumn
*  Filename: ctlOrderByColumns.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class ctlOrderByColumn {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblFieldName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboOrderByDir = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblFieldName
            // 
            this.lblFieldName.AutoSize = true;
            this.lblFieldName.Location = new System.Drawing.Point(138, 54);
            this.lblFieldName.Name = "lblFieldName";
            this.lblFieldName.Size = new System.Drawing.Size(0, 13);
            this.lblFieldName.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Asc/Desc:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Field Name:";
            // 
            // cboOrderByDir
            // 
            this.cboOrderByDir.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOrderByDir.FormattingEnabled = true;
            this.cboOrderByDir.Items.AddRange(new object[] {
            "Ascending",
            "Descending"});
            this.cboOrderByDir.Location = new System.Drawing.Point(141, 75);
            this.cboOrderByDir.Name = "cboOrderByDir";
            this.cboOrderByDir.Size = new System.Drawing.Size(121, 21);
            this.cboOrderByDir.TabIndex = 0;
            this.cboOrderByDir.SelectedIndexChanged += new System.EventHandler(this.cboOrderByDir_SelectedIndexChanged);
            // 
            // ctlOrderByColumn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cboOrderByDir);
            this.Controls.Add(this.lblFieldName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ctlOrderByColumn";
            this.Size = new System.Drawing.Size(362, 150);
            this.Load += new System.EventHandler(this.ctlOrderByColumn_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFieldName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboOrderByDir;
    }
}
