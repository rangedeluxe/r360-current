using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting. 
* WI 115856 BLR 06/24/2014
*   - Readonly fields for DataEntry fields, we need the DisplayName to 
*     remain consistent for SQL.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal partial class ctLayoutField : UserControl {

        private bool _IsDirty = false;
        private int _Index;

        public ctLayoutField(int index, ILayoutField layoutField) {

            InitializeComponent();
            LoadLayoutField(index, layoutField);
        }
        
        public int Index {
            get {
                return(_Index);
            }
        }
        
        public string DisplayName {
            get {
                return(this.txtDisplayName.Text);
            }
        }

        public int ColumnWidth {
            get {
                int intTemp;
                
                if(int.TryParse(this.txtColumnWidth.Text, out intTemp)) {
                    return(intTemp);
                } else {
                    return(0);
                }
            }
        }

        public string Format {
            get {
                return(this.txtFormat.Text);
            }
        }

        public JustifyEnum Justification {
            get {
               if(this.cboJustification.Text == "Left") {
                    return(JustifyEnum.Left);
                } else {
                    return(JustifyEnum.Right);
                }
            }
        }

        public char PadChar {
            get {
                if(this.txtPadChar.Text.ToUpper() == "SP" || this.txtPadChar.Text.Length == 0) {
                    return(' ');
                } else {
                    return(this.txtPadChar.Text[0]);
                }
            }
        }

        public bool Quotes {
            get {
                return(this.chkQuotes.Checked);
            }
        }
        
        public void LoadLayoutField(int index, ILayoutField layoutField) {

            _Index = index;

            this.lblFieldName.Text = layoutField.FieldName;
            this.txtDisplayName.Text = layoutField.DisplayName;
            this.txtDisplayName.Enabled = !layoutField.IsDataEntry;
            this.txtColumnWidth.Text = layoutField.ColumnWidth.ToString();
            this.txtFormat.Text = layoutField.Format;

            if(layoutField.Justification == JustifyEnum.Left) {
                this.cboJustification.SelectedIndex = 0;
            } else {
                this.cboJustification.SelectedIndex = 1;
            }

            if(layoutField.PadChar == ' ') {
                this.txtPadChar.Text = "SP";
            } else {
                this.txtPadChar.Text = Convert.ToString(layoutField.PadChar);
            }
            this.chkQuotes.Checked = layoutField.Quotes;
        }
        
        public bool IsDirty {
            get {
                return(_IsDirty);
            }
        }

        private void txtPadChar_Validating(object sender, CancelEventArgs e) {

            if(this.txtPadChar.Text.Length == 0) {
                this.txtPadChar.Text = "SP";
            } else if(this.txtPadChar.Text.Length > 1) {
                if(this.txtPadChar.Text.ToUpper() == "SP") {
                    this.txtPadChar.Text = "SP";
                } else {
                    e.Cancel = true;
                }
            }
        }

        private void txtColumnWidth_Validating(object sender, CancelEventArgs e) {

            int intTemp;
            
            if(!int.TryParse(this.txtColumnWidth.Text, out intTemp)) {
                e.Cancel = true;
            }
        }

        private void txtFormat_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void chkQuotes_CheckedChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void cboJustification_SelectedIndexChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtPadChar_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtColumnWidth_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtDisplayName_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void ctLayoutField_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}
