/*******************************************************************************
*
*    Module: ctlLimitItem
*  Filename: ctlLimitItem.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class ctlLimitItem {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFieldName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtValue = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLeftParenCount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRightParenCount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cboCompoundOperator = new System.Windows.Forms.ComboBox();
            this.cboLogicalOperator = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblFieldName
            // 
            this.lblFieldName.AutoSize = true;
            this.lblFieldName.Location = new System.Drawing.Point(139, 53);
            this.lblFieldName.Name = "lblFieldName";
            this.lblFieldName.Size = new System.Drawing.Size(0, 13);
            this.lblFieldName.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Logical Operator:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Field Name:";
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(165, 100);
            this.txtValue.MaxLength = 100;
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(201, 20);
            this.txtValue.TabIndex = 1;
            this.txtValue.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Value:";
            // 
            // txtLeftParenCount
            // 
            this.txtLeftParenCount.Location = new System.Drawing.Point(165, 126);
            this.txtLeftParenCount.MaxLength = 3;
            this.txtLeftParenCount.Name = "txtLeftParenCount";
            this.txtLeftParenCount.Size = new System.Drawing.Size(39, 20);
            this.txtLeftParenCount.TabIndex = 3;
            this.txtLeftParenCount.TextChanged += new System.EventHandler(this.txtLeftParenCount_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(46, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "(Left:";
            // 
            // txtRightParenCount
            // 
            this.txtRightParenCount.Location = new System.Drawing.Point(165, 152);
            this.txtRightParenCount.MaxLength = 3;
            this.txtRightParenCount.Name = "txtRightParenCount";
            this.txtRightParenCount.Size = new System.Drawing.Size(39, 20);
            this.txtRightParenCount.TabIndex = 4;
            this.txtRightParenCount.TextChanged += new System.EventHandler(this.txtRightParenCount_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 155);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Right):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Operator on Next Row:";
            // 
            // cboCompoundOperator
            // 
            this.cboCompoundOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCompoundOperator.FormattingEnabled = true;
            this.cboCompoundOperator.ItemHeight = 13;
            this.cboCompoundOperator.Location = new System.Drawing.Point(165, 178);
            this.cboCompoundOperator.Name = "cboCompoundOperator";
            this.cboCompoundOperator.Size = new System.Drawing.Size(76, 21);
            this.cboCompoundOperator.TabIndex = 30;
            this.cboCompoundOperator.SelectedIndexChanged += new System.EventHandler(this.cboCompoundOperator_SelectedIndexChanged);
            // 
            // cboLogicalOperator
            // 
            this.cboLogicalOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLogicalOperator.FormattingEnabled = true;
            this.cboLogicalOperator.Location = new System.Drawing.Point(165, 73);
            this.cboLogicalOperator.Name = "cboLogicalOperator";
            this.cboLogicalOperator.Size = new System.Drawing.Size(76, 21);
            this.cboLogicalOperator.TabIndex = 0;
            this.cboLogicalOperator.SelectedIndexChanged += new System.EventHandler(this.cboLogicalOperator_SelectedIndexChanged);
            // 
            // ctlLimitItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cboLogicalOperator);
            this.Controls.Add(this.cboCompoundOperator);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtRightParenCount);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtLeftParenCount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblFieldName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ctlLimitItem";
            this.Size = new System.Drawing.Size(370, 325);
            this.Load += new System.EventHandler(this.ctlLimitItem_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFieldName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtLeftParenCount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRightParenCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboCompoundOperator;
        private System.Windows.Forms.ComboBox cboLogicalOperator;
    }
}
