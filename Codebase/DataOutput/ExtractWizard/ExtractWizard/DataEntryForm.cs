﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager
{
    public partial class DataEntryForm : Form
    {
        private cExtractWizardBLL _bll;
        private DataEntryType _type;
        private bool _orderasc = false;

        public List<cColumn> SelectedColumns { get; private set; }
        public List<cColumn> AllColumns { get; private set; }

        public List<SimpleGridColumnModel> GridColumns { get; private set; }

        public DataEntryForm(ExtractConfigurationSettings settings, DataEntryType type)
            : this()
        {
            _type = type;
            _bll = new cExtractWizardBLL(settings);
            SelectedColumns = new List<cColumn>();

            var title = _type == DataEntryType.PaymentsDataEntry ? "Payments" : "Stubs";
            labelTitle.Text = string.Format("{0} Data Entry", title);

            datagrid.AutoGenerateColumns = true;
            datagrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            LoadDataEntryColumns();
            RefreshGrid();
        }
        public DataEntryForm()
        {
            InitializeComponent();
        }

        private void LoadDataEntryColumns()
        {
            AllColumns = _bll.GetAllDataEntryColumns(_type);
            GridColumns = new List<SimpleGridColumnModel>(AllColumns
                .Select(x => x.ToSimpleGridModel())
                .ToList());
        }

        private void RefreshGrid()
        {
            datagrid.DataSource = GridColumns;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAddSelected_Click(object sender, EventArgs e)
        {
            var rows = datagrid.SelectedRows;
            SelectedColumns.Clear();
            foreach(DataGridViewRow row in rows)
            {
                var data = GridColumns[row.Index];
                SelectedColumns.Add(AllColumns
                    .First(x => x.SiteBankID == data.BankID 
                        && x.SiteClientAccountID == data.WorkgroupID 
                        && x.ColumnName == data.FieldName 
                        && x.DisplayName == data.UILabel 
                        && x.PaymentSource == data.PaymentSource)
                );
            }
            SelectedColumns.Reverse();
            Close();
        }

        public void SortGrid(int columnindex)
        {
            _orderasc = !_orderasc;

            if (_orderasc)
            {
                if (columnindex == 0)
                    GridColumns = GridColumns.OrderBy(x => x.UILabel).ToList();
                else if (columnindex == 1)
                    GridColumns = GridColumns.OrderBy(x => x.FieldName).ToList();
                else if (columnindex == 2)
                    GridColumns = GridColumns.OrderBy(x => x.BankID).ToList();
                else if (columnindex == 3)
                    GridColumns = GridColumns.OrderBy(x => x.BankName).ToList();
                else if (columnindex == 4)
                    GridColumns = GridColumns.OrderBy(x => x.WorkgroupID).ToList();
                else if (columnindex == 5)
                    GridColumns = GridColumns.OrderBy(x => x.WorkgroupName).ToList();
                else if (columnindex == 6)
                    GridColumns = GridColumns.OrderBy(x => x.PaymentSource).ToList();
                else if (columnindex == 7)
                    GridColumns = GridColumns.OrderBy(x => x.IsActive).ToList();
            }
            else
            {
                if (columnindex == 0)
                    GridColumns = GridColumns.OrderByDescending(x => x.UILabel).ToList();
                else if (columnindex == 1)
                    GridColumns = GridColumns.OrderByDescending(x => x.FieldName).ToList();
                else if (columnindex == 2)
                    GridColumns = GridColumns.OrderByDescending(x => x.BankID).ToList();
                else if (columnindex == 3)
                    GridColumns = GridColumns.OrderByDescending(x => x.BankName).ToList();
                else if (columnindex == 4)
                    GridColumns = GridColumns.OrderByDescending(x => x.WorkgroupID).ToList();
                else if (columnindex == 5)
                    GridColumns = GridColumns.OrderByDescending(x => x.WorkgroupName).ToList();
                else if (columnindex == 6)
                    GridColumns = GridColumns.OrderByDescending(x => x.PaymentSource).ToList();
                else if (columnindex == 7)
                    GridColumns = GridColumns.OrderByDescending(x => x.IsActive).ToList();
            }

            RefreshGrid();
        }

        private void datagrid_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SortGrid(e.ColumnIndex);
        }

        public new void Dispose()
        {
            base.Dispose();
            AllColumns.Clear();
            SelectedColumns.Clear();
            GridColumns.Clear();
            datagrid.Dispose();
        }

        private void buttonFilter_Click(object sender, EventArgs e)
        {
            var text = textFilter.Text;
            Filter(text);
        }

        public void Filter(string text)
        {
            text = text.ToLower();
            GridColumns = AllColumns
                .Where(x => (x.BankName != null && x.BankName.ToLower().Contains(text))
                    || (x.ColumnName != null && x.ColumnName.ToLower().Contains(text))
                    || (x.DisplayName != null && x.DisplayName.ToLower().Contains(text))
                    || (x.PaymentSource != null && x.PaymentSource.ToLower().Contains(text))
                    || x.SiteBankID.ToString().ToLower().Contains(text)
                    || x.SiteClientAccountID.ToString().ToLower().Contains(text)
                    || (x.WorkgroupName != null && x.WorkgroupName.ToLower().Contains(text)))
                .Select(x => x.ToSimpleGridModel())
                .ToList();
            RefreshGrid();
        }
    }
}
