﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 
*
* Purpose: 
*
* Modification History
* WI 132205 BLR 03/17/2014
*   - Initial Version 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting
{
    /// <summary>
    /// Concrete impl of ITabSorter.  Sorts the TabIndex of elements, and all child elements,
    /// recursively, of a form container.  Defines Compare as a comparison of X and Y locations
    /// of the controls.  X first, Y Second.
    /// </summary>
    public class TabSorter : ITabSorter
    {
        public void SortTabs(System.Windows.Forms.Control container)
        {
            var controlArraySorted = new List<Control>();
            controlArraySorted.AddRange(container.Controls.Cast<Control>());
            controlArraySorted.Sort(this);

            foreach (var c in controlArraySorted)
            {
                c.TabIndex = controlArraySorted.IndexOf(c);
                if (c.Controls.Count > 0)
                {
                    SortTabs(c);
                }
            }
        }

        public int Compare(System.Windows.Forms.Control x, System.Windows.Forms.Control y)
        {
            if (x.Top < y.Top)
                return -1;
            else if (x.Top > y.Top)
                return 1;
            else
                return (x.Left.CompareTo(y.Left));
        }
    }
}
