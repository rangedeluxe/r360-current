﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 
*
* Purpose: 
*
* Modification History
* WI 132205 BLR 03/17/2014
*   - Initial Version 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting
{
    public interface ITabSorter : IComparer<Control>
    {
        /// <summary>
        /// Sorts the tabs on controls and forms. Only 'TabIndex' will be set.
        /// </summary>
        /// <param name="control"></param>
        void SortTabs(Control control);
    }
}
