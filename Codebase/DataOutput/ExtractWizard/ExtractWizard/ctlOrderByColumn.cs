using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal partial class ctlOrderByColumn : UserControl {

        private bool _IsDirty = false;
        private int _Index;

        public ctlOrderByColumn(int index, cOrderByColumn orderByColumn) {
            InitializeComponent();
            LoadOrderByColumn(index, orderByColumn);
        }

        public int Index {
            get {
                return(_Index);
            }
        }

        public OrderByDirEnum OrderByDir {
            get {
                if(this.cboOrderByDir.SelectedIndex == 0) {
                    return(OrderByDirEnum.Ascending);
                } else {
                    return(OrderByDirEnum.Descending);
                }
            }
        }

        public void LoadOrderByColumn(int index, cOrderByColumn orderByColumn) {

            _Index = index;

            this.lblFieldName.Text = orderByColumn.DisplayName;

            if(orderByColumn.OrderByDir == OrderByDirEnum.Ascending) {
                this.cboOrderByDir.SelectedIndex = 0;
            } else {
                this.cboOrderByDir.SelectedIndex = 1;
            }
        }

        public bool IsDirty {
            get {
                return(_IsDirty);
            }
        }

        private void cboOrderByDir_SelectedIndexChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void ctlOrderByColumn_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}
