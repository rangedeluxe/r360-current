﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using IntegraPAY.Delivery.Extract.ExtractAPI;

namespace IntegraPAY.Delivery.Extract.ExtractWizard {

    public delegate void ImageFileNameChangedEventHandler(string ImageFileName);

    public partial class ctlImageFileNameFormat : UserControl {

        private ImageFileNameFormatEnum _ImageFileNameFormat;
        private bool _ImageFileBatchTypeFormatFull;
        private bool _ImageFileZeroPad;
        private string _ImageFileProcDateFormat;
        private ImageFileFormatEnum _ImageFileFormat;
        
        public event ImageFileNameChangedEventHandler ImageFileNameChanged;
        
        public ctlImageFileNameFormat() {

            InitializeComponent();

            this.ctlDataGrid1.AddColumnsRange(new System.Windows.Forms.DataGridViewColumn[] {
                                              this.colFieldName});

        }
        
        public void InitializeControl(string vImageFileName,
                                      ImageFileNameFormatEnum vImageFileNameFormat,
                                      bool vImageFileBatchTypeFormatFull,
                                      bool vImageFileZeroPad,
                                      string vImageFileProcDateFormat,
                                      ImageFileFormatEnum vImageFileFormat) {
            
            _ImageFileNameFormat = vImageFileNameFormat;
            
            if(vImageFileName == null || vImageFileName.Length == 0) {
                switch(_ImageFileNameFormat) {
                    case ImageFileNameFormatEnum.Single:
                        RefreshFileNameValue(ParseFileName(cExtractDef.DEFAULT_SINGLE_IMAGES_FILE_NAME));
                        break;
                    case ImageFileNameFormatEnum.Transaction:
                        RefreshFileNameValue(ParseFileName(cExtractDef.DEFAULT_IMAGES_PER_TXN_FILE_NAME));
                        break;
                    case ImageFileNameFormatEnum.Batch:
                        RefreshFileNameValue(ParseFileName(cExtractDef.DEFAULT_IMAGES_PER_BATCH_FILE_NAME));
                        break;
                }
            } else {
                RefreshFileNameValue(ParseFileName(vImageFileName));
            }

            _ImageFileBatchTypeFormatFull = vImageFileBatchTypeFormatFull;
            _ImageFileZeroPad = vImageFileZeroPad;
            _ImageFileProcDateFormat = vImageFileProcDateFormat;
            _ImageFileFormat = vImageFileFormat;

            this.lstImageFields.Items.Clear();

            switch(vImageFileNameFormat) {
                case ImageFileNameFormatEnum.Single:
                    this.lstImageFields.Items.AddRange(new object[] {
                                                        "Bank",
                                                        "Lockbox",
                                                        "Batch",
                                                        "Batch Type",
                                                        "Processing Date",
                                                        "Transaction",
                                                        "Document Type",
                                                        "File Counter"});
                    break;
                case ImageFileNameFormatEnum.Transaction:
                    this.lstImageFields.Items.AddRange(new object[] {
                                                        "Bank",
                                                        "Lockbox",
                                                        "Batch",
                                                        "Batch Type",
                                                        "Processing Date",
                                                        "Transaction"});
                    break;
                case ImageFileNameFormatEnum.Batch:
                    this.lstImageFields.Items.AddRange(new object[] {
                                                        "Bank",
                                                        "Lockbox",
                                                        "Batch",
                                                        "Batch Type",
                                                        "Processing Date"});
                    break;
            }
        }

        public string ImageFileName {
            get {
                return(Flatten(BuildFileNameFromGrid()));
            }
        }

        private void OnImageFileNameChanged() {
            if(ImageFileNameChanged != null) {
                ImageFileNameChanged(Flatten(BuildFileNameFromGrid()));
            }
        }

        private List<string> BuildFileNameFromGrid() {
            
            List<string> arRetVal = new List<string>();
            
            foreach(DataGridViewRow row in this.ctlDataGrid1.Rows) {
                if(row.Cells[0].ReadOnly) {
                    arRetVal.Add(row.Cells[0].Tag.ToString());
                } else {
                    arRetVal.Add(row.Cells[0].Value.ToString());
                }
            }
            
            return(arRetVal);
        }

        private void RefreshFileNameValue(List<string> ImageFileNameArray) {

            this.ctlDataGrid1.Rows.Clear();

            foreach(string str in ImageFileNameArray) {
                AddRowToDataGrid(str);
            }
            
            OnImageFileNameChanged();
        }

        private void AddFieldToFormat() {

            if(this.lstImageFields.SelectedItem != null) {
            
                if(this.ctlDataGrid1.Rows.Count > 0) {
                    AddRowToDataGrid("_");
                }

                switch(this.lstImageFields.SelectedItem.ToString()) {
                    case "Bank":
                        AddRowToDataGrid("<BK>");
                        break;
                    case "Lockbox":
                        AddRowToDataGrid("<LB>");
                        break;
                    case "Batch":
                        AddRowToDataGrid("<BH>");
                        break;
                    case "Batch Type":
                        AddRowToDataGrid("<BT>");
                        break;
                    case "Processing Date":
                        AddRowToDataGrid("<PD>");
                        break;
                    case "Transaction":
                        AddRowToDataGrid("<TR>");
                        break;
                    case "Document Type":
                        AddRowToDataGrid("<DT>");
                        break;
                    case "File Counter":
                        AddRowToDataGrid("<FC>");
                        break;
                }

                OnImageFileNameChanged();
            }
        }

        private void AddRowToDataGrid(string FieldName) {

            DataGridViewCell[] cellArray;
            DataGridViewTextBoxCell dgText;
            DataGridViewRow dgRow;
            string strTokenDesc;

            dgText = new DataGridViewTextBoxCell();
            dgText.Tag = FieldName;
            dgText.MaxInputLength = 100;

            cellArray = new DataGridViewCell[1];
            cellArray[0] = dgText;

            // Add Row to DataGridView
            dgRow = new DataGridViewRow();
            dgRow.Cells.AddRange(cellArray);

            if(ParseToken(FieldName, out strTokenDesc)) {
                dgRow.Cells[0].Value = strTokenDesc;
                dgRow.Cells[0].ReadOnly = true;
                dgRow.Cells[0].Style.ForeColor = Color.Maroon;
            } else {
                dgRow.Cells[0].Value = FieldName;
            }

            this.ctlDataGrid1.AddRow(dgRow);
            
            dgRow.Cells[0].Selected = true;
        }

        private void btnAddStaticField_Click(object sender, EventArgs e) {
            AddRowToDataGrid("_");
            OnImageFileNameChanged();
        }

        private void lstImageFields_DoubleClick(object sender, EventArgs e) {
            AddFieldToFormat();
        }

        private void btnApplyDefaultFileName_Click(object sender, EventArgs e) {
            switch(_ImageFileNameFormat) {
                case ImageFileNameFormatEnum.Single:
                    RefreshFileNameValue(ParseFileName(cExtractDef.DEFAULT_SINGLE_IMAGES_FILE_NAME));
                    break;
                case ImageFileNameFormatEnum.Transaction:
                    RefreshFileNameValue(ParseFileName(cExtractDef.DEFAULT_IMAGES_PER_TXN_FILE_NAME));
                    break;
                case ImageFileNameFormatEnum.Batch:
                    RefreshFileNameValue(ParseFileName(cExtractDef.DEFAULT_IMAGES_PER_BATCH_FILE_NAME));
                    break;
            }
        }

        private void ctlDataGrid1_ItemMoved(int CurrentIndex, int NewIndex) {
            OnImageFileNameChanged();
        }

        private void ctlDataGrid1_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
            OnImageFileNameChanged();
        }

        private void ctlDataGrid1_ItemDeleted(int ItemIndex) {
            OnImageFileNameChanged();
        }

        private void btnAddSelectedToken_Click(object sender, EventArgs e) {
            AddFieldToFormat();
        }

        private static bool ParseToken(string Value, out string TokenDesc) {

            string strRetVal;
            bool bolRetVal;

            switch(Value) {
                case "<BK>":
                    strRetVal = "Bank";
                    bolRetVal = true;
                    break;
                case "<LB>":
                    strRetVal = "Lockbox";
                    bolRetVal = true;
                    break;
                case "<BH>":
                    strRetVal = "Batch";
                    bolRetVal = true;
                    break;
                case "<BT>":
                    strRetVal = "Batch Type";
                    bolRetVal = true;
                    break;
                case "<PD>":
                    strRetVal = "Processing Date";
                    bolRetVal = true;
                    break;
                case "<TR>":
                    strRetVal = "Transaction";
                    bolRetVal = true;
                    break;
                case "<DT>":
                    strRetVal = "Document Type";
                    bolRetVal = true;
                    break;
                case "<FC>":
                    strRetVal = "File Counter";
                    bolRetVal = true;
                    break;
                default:
                    strRetVal = string.Empty;
                    bolRetVal = false;
                    break;
            }

            TokenDesc = strRetVal;
            
            return(bolRetVal);
        }

        private static List<string> ParseFileName(string FileName) {

            List<string> arRetVal = new List<string>();
            string strStaticField = string.Empty;
            string strToken;
            bool bolTokenFound;
            
            if(FileName.Length > 0) {

                for(int i=0;i<FileName.Length;++i) {
                
                    bolTokenFound = false;
                
                    for(int j=0;j<8;++j) {

                        switch(j) {
                            case 0:
                                strToken = "<BK>";
                                break;
                            case 1:
                                strToken = "<LB>";
                                break;
                            case 2:
                                strToken = "<BH>";
                                break;
                            case 3:
                                strToken = "<BT>";
                                break;
                            case 4:
                                strToken = "<PD>";
                                break;
                            case 5:
                                strToken = "<TR>";
                                break;
                            case 6:
                                strToken = "<DT>";
                                break;
                            case 7:
                                strToken = "<FC>";
                                break;
                            default:
                                strToken = string.Empty;
                                break;
                        }
                
                        if(strToken.Length > 0 && FileName.Substring(i).ToUpper().StartsWith(strToken)) {
                            if(strStaticField.Length > 0) {
                                arRetVal.Add(strStaticField);
                                strStaticField = string.Empty;
                            }
                            arRetVal.Add(strToken);
                            i += strToken.Length - 1;
                            bolTokenFound = true;
                        }
                        
                        if(i >= FileName.Length) {
                            break;
                        }
                    }

                    if(i < FileName.Length && !bolTokenFound) {
                        strStaticField += FileName[i];
                    }
                }

                if(strStaticField.Length > 0) {
                    arRetVal.Add(strStaticField);
                }
            }

            return(arRetVal);
        }

        private static string Flatten(List<string> FileNameArray) {

            StringBuilder sbRetVal = new StringBuilder();

            foreach(string str in FileNameArray) {
                sbRetVal.Append(str);
            }
            
            return(sbRetVal.ToString());
        }
    }
}
