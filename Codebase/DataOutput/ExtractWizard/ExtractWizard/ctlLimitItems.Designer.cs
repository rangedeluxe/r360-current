using System.Windows.Forms;

/*******************************************************************************
*
*    Module: ctlLimitItems
*  Filename: ctlLimitItems.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class ctlLimitItems {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.txtWhereClause = new System.Windows.Forms.TextBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ctlDataGrid1 = new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ctlDataGrid();
            this.label1 = new System.Windows.Forms.Label();
            this.colFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDisplayName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLogicalOperator = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLeftParen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRightParen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompoundOperator = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtWhereClause
            // 
            this.txtWhereClause.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWhereClause.Location = new System.Drawing.Point(0, 32);
            this.txtWhereClause.Margin = new System.Windows.Forms.Padding(4);
            this.txtWhereClause.Multiline = true;
            this.txtWhereClause.Name = "txtWhereClause";
            this.txtWhereClause.ReadOnly = true;
            this.txtWhereClause.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtWhereClause.Size = new System.Drawing.Size(1213, 135);
            this.txtWhereClause.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ctlDataGrid1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.txtWhereClause);
            this.splitContainer1.Size = new System.Drawing.Size(1215, 628);
            this.splitContainer1.SplitterDistance = 455;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 2;
            // 
            // ctlDataGrid1
            // 
            this.ctlDataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctlDataGrid1.Location = new System.Drawing.Point(0, 0);
            this.ctlDataGrid1.Margin = new System.Windows.Forms.Padding(5);
            this.ctlDataGrid1.Name = "ctlDataGrid1";
            this.ctlDataGrid1.Size = new System.Drawing.Size(1215, 455);
            this.ctlDataGrid1.TabIndex = 0;
            this.ctlDataGrid1.ItemDropped += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemDroppedEventHandler(this.ctlDataGrid1_ItemDropped);
            this.ctlDataGrid1.ItemMoved += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemMovedEventHandler(this.ctlDataGrid1_ItemMoved);
            this.ctlDataGrid1.ItemDeleted += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemDeletedEventHandler(this.ctlDataGrid1_ItemDeleted);
            this.ctlDataGrid1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlDataGrid1_CellEndEdit);
            this.ctlDataGrid1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ctlDataGrid1_CellValidating);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "\"Where Clause\" as text";
            // 
            // colFieldName
            // 
            this.colFieldName.HeaderText = "Field Name";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.ReadOnly = true;
            this.colFieldName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldName.Width = 150;
            // 
            // colDisplayName
            // 
            this.colDisplayName.HeaderText = "Display Name";
            this.colDisplayName.Name = "colDisplayName";
            this.colDisplayName.ReadOnly = true;
            this.colDisplayName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colDisplayName.Width = 150;
            // 
            // colLogicalOperator
            // 
            this.colLogicalOperator.HeaderText = "Logical Operator";
            this.colLogicalOperator.Name = "colLogicalOperator";
            this.colLogicalOperator.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colValue
            // 
            this.colValue.HeaderText = "Value";
            this.colValue.Name = "colValue";
            this.colValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colValue.Width = 200;
            // 
            // colLeftParen
            // 
            this.colLeftParen.HeaderText = "(Left";
            this.colLeftParen.Name = "colLeftParen";
            this.colLeftParen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colLeftParen.Width = 65;
            // 
            // colRightParen
            // 
            this.colRightParen.HeaderText = "Right)";
            this.colRightParen.Name = "colRightParen";
            this.colRightParen.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colRightParen.Width = 65;
            // 
            // colCompoundOperator
            // 
            this.colCompoundOperator.FillWeight = 50F;
            this.colCompoundOperator.HeaderText = "Operator on Next Row";
            this.colCompoundOperator.Name = "colCompoundOperator";
            this.colCompoundOperator.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colCompoundOperator.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ctlLimitItems
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ctlLimitItems";
            this.Size = new System.Drawing.Size(1215, 628);
            this.Load += new System.EventHandler(this.ctlLimitItems_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDisplayName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLogicalOperator;
        private System.Windows.Forms.DataGridViewTextBoxColumn colValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLeftParen;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRightParen;
        private System.Windows.Forms.DataGridViewComboBoxColumn colCompoundOperator;
        private ctlDataGrid ctlDataGrid1;
        private System.Windows.Forms.TextBox txtWhereClause;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private Label label1;
    }
}
