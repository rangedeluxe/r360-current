namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager
{
    partial class frmFormatCurrencyDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSelectedFormat = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTestResult = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkOverpunchPosAndZero = new System.Windows.Forms.CheckBox();
            this.chkOverpunchNeg = new System.Windows.Forms.CheckBox();
            this.txtTestValue = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lstExample = new System.Windows.Forms.ListBox();
            this.lstFormat = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSelectedFormat
            // 
            this.txtSelectedFormat.Location = new System.Drawing.Point(99, 22);
            this.txtSelectedFormat.Name = "txtSelectedFormat";
            this.txtSelectedFormat.Size = new System.Drawing.Size(202, 20);
            this.txtSelectedFormat.TabIndex = 1;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(307, 298);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(57, 23);
            this.btnTest.TabIndex = 10;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selected Format:";
            // 
            // txtTestResult
            // 
            this.txtTestResult.Location = new System.Drawing.Point(99, 324);
            this.txtTestResult.Name = "txtTestResult";
            this.txtTestResult.ReadOnly = true;
            this.txtTestResult.Size = new System.Drawing.Size(202, 20);
            this.txtTestResult.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkOverpunchPosAndZero);
            this.groupBox1.Controls.Add(this.chkOverpunchNeg);
            this.groupBox1.Controls.Add(this.txtTestValue);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtSelectedFormat);
            this.groupBox1.Controls.Add(this.btnTest);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.lstExample);
            this.groupBox1.Controls.Add(this.lstFormat);
            this.groupBox1.Controls.Add(this.txtTestResult);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 361);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // chkOverpunchPosAndZero
            // 
            this.chkOverpunchPosAndZero.AutoSize = true;
            this.chkOverpunchPosAndZero.Location = new System.Drawing.Point(99, 247);
            this.chkOverpunchPosAndZero.Name = "chkOverpunchPosAndZero";
            this.chkOverpunchPosAndZero.Size = new System.Drawing.Size(200, 17);
            this.chkOverpunchPosAndZero.TabIndex = 14;
            this.chkOverpunchPosAndZero.Text = "Overpunch Positive Values and Zero";
            this.chkOverpunchPosAndZero.UseVisualStyleBackColor = true;
            this.chkOverpunchPosAndZero.CheckedChanged += new System.EventHandler(this.chkOverpunchPosAndZero_CheckedChanged);
            // 
            // chkOverpunchNeg
            // 
            this.chkOverpunchNeg.AutoSize = true;
            this.chkOverpunchNeg.Location = new System.Drawing.Point(99, 270);
            this.chkOverpunchNeg.Name = "chkOverpunchNeg";
            this.chkOverpunchNeg.Size = new System.Drawing.Size(160, 17);
            this.chkOverpunchNeg.TabIndex = 13;
            this.chkOverpunchNeg.Text = "Overpunch Negative Values";
            this.chkOverpunchNeg.UseVisualStyleBackColor = true;
            this.chkOverpunchNeg.CheckedChanged += new System.EventHandler(this.chkOverpunchNeg_CheckedChanged);
            // 
            // txtTestValue
            // 
            this.txtTestValue.Location = new System.Drawing.Point(99, 298);
            this.txtTestValue.Name = "txtTestValue";
            this.txtTestValue.Size = new System.Drawing.Size(202, 20);
            this.txtTestValue.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 301);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Test Value:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(198, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Example:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Format:";
            // 
            // lstExample
            // 
            this.lstExample.FormattingEnabled = true;
            this.lstExample.Location = new System.Drawing.Point(201, 70);
            this.lstExample.Name = "lstExample";
            this.lstExample.Size = new System.Drawing.Size(163, 160);
            this.lstExample.TabIndex = 6;
            this.lstExample.SelectedIndexChanged += new System.EventHandler(this.lstExample_SelectedIndexChanged);
            this.lstExample.DoubleClick += new System.EventHandler(this.lstExample_DoubleClick);
            // 
            // lstFormat
            // 
            this.lstFormat.FormattingEnabled = true;
            this.lstFormat.Items.AddRange(new object[] {
            "0",
            "0.00",
            "#,##0",
            "#,##0.00",
            "0*100",
            "#,##0*100"});
            this.lstFormat.Location = new System.Drawing.Point(19, 70);
            this.lstFormat.Name = "lstFormat";
            this.lstFormat.Size = new System.Drawing.Size(165, 160);
            this.lstFormat.TabIndex = 5;
            this.lstFormat.SelectedIndexChanged += new System.EventHandler(this.lstFormat_SelectedIndexChanged);
            this.lstFormat.DoubleClick += new System.EventHandler(this.lstFormat_DoubleClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 327);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Test Result:";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(227, 388);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(101, 388);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 12;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // frmFormatCurrencyDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 423);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFormatCurrencyDialog";
            this.Text = "Format Currency";
            this.Load += new System.EventHandler(this.frmFormatCurrencyDialog_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtSelectedFormat;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTestResult;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lstExample;
        private System.Windows.Forms.ListBox lstFormat;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.CheckBox chkOverpunchPosAndZero;
        private System.Windows.Forms.CheckBox chkOverpunchNeg;
        private System.Windows.Forms.TextBox txtTestValue;
        private System.Windows.Forms.Label label5;
    }
}