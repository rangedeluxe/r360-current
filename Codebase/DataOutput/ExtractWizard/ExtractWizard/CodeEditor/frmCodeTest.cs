﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Wanta
* Date: 
*
* Purpose: 
*
* Modification History
* WI 130551 BLR 03/06/2014
*   -Initial Version 
******************************************************************************/
namespace RTFCodeEditor
{
    public partial class frmCodeTest : Form
    {
        public string SrcCode
        {
            get;
            set;
        }
        public frmCodeTest()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            string sample = @"115/20/2013 11:30:09 AM5/20/2013 11:30:09 AM2660024.24
                505
                135
                63.57
                2325.15
                990
                585
                1855.96
                41928
                1259.59
                224.44
                14900
                255
                400
                4492.5
                200.22
                190
                224819.74";
            textBox1.Text = sample;
            textBox1.SelectionStart = 0;
            textBox1.SelectionLength = 0;

            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            StringReader sr = new StringReader(textBox1.Text);
            string line;
            List<string> fileRows = new List<string>();
            while ((line = sr.ReadLine()) != null)
            {
                fileRows.Add(line);
            }
            Execute(fileRows);
        }

        private void Execute( List<string> fileRows)
        {
            List<string> arRetVal = fileRows;

                // Configure a CompilerParameters that links System.dll
                // and produces the specified executable file.
               string[] arAssReferences = new string[]{ "System.dll" };
              CompilerParameters  objParms = new CompilerParameters(arAssReferences);
                objParms.GenerateInMemory = true;

                // Compile the source file into an executable output file.
                CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");

                // Invoke compilation.
               CompilerResults objResults = provider.CompileAssemblyFromSource(objParms, this.SrcCode);

               if (objResults.Errors.Count > 0)
               {

                   // Display compilation errors.

                   string strMsg = "Errors encountered while building the Post-Processing Code Module.\n\n";

                   foreach (CompilerError err in objResults.Errors)
                   {
                       strMsg += "\r\nError Number: " + err.ErrorNumber.ToString() +
                                 "\r\nLine: " + err.Line.ToString() +
                                 "\r\nColumn: " + err.Column.ToString() +
                                 "\r\nError Text: " + err.ErrorText +
                                 "\n\n";
                   }
                   MessageBox.Show(strMsg);
                   return;
               }


               Assembly objAssembly = objResults.CompiledAssembly;

               // Retrieve an object reference - since this object is 'dynamic' we can't explicitly
               // type it so it's of type Object
               object objInstance = objAssembly.CreateInstance("WFS.RecHub.DataOutputToolkit.Extract.CodeModule.PostProcessing");
               if (objInstance == null)
               {
                   MessageBox.Show("Could not load PostProcessing class in Post-Processing Code Module.");
                   return;
                   //OnOutputError("Could not load PostProcessing class in Post-Processing Code Module.", "ExtractAPI");
                   //bolRetVal = false;
               }

               object[] objCodeParms = new object[1];
               objCodeParms[0] = arRetVal;

               try
               {
                   arRetVal = (List<string>)objInstance.GetType().InvokeMember("ProcessExtract",
                                                                               BindingFlags.InvokeMethod,
                                                                               null,
                                                                               objInstance,
                                                                               objCodeParms);

                   foreach (string s in arRetVal)
                   {
                       textBox2.Text += s + Environment.NewLine;
                   }
               }
               catch (Exception ex)
               {
                   MessageBox.Show(ex.ToString());
                   // DLD - WI 130165 - passing entire Exception object instead of just message
                   //OnOutputError(this, ex);
               }



        }
    }
}
