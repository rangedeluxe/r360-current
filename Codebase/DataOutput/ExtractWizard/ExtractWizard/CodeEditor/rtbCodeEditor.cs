﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Windows.Input;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Dave Wanta
* Date: 
*
* Purpose: 
*
* Modification History
* WI 130551 BLR 03/06/2014
*   - Initial Version 
* WI 133174 DJW 03/19/2014
*   - Added a support for a reset button to the editor.
******************************************************************************/
using HWND = System.IntPtr;

namespace RTFCodeEditor
{
    /// <summary>
    /// This  is a very simple C# code editor. Basically, color code the key words. No intellisense.
    /// 
    /// </summary>
    //TODO: Implement cross line comments
    //TODO: Implement cross line strings
    public partial class CodeEditor : RichTextBoxEx
    {

        #region interop calls to minimize flickering
        private const int WM_SETREDRAW = 0x000B;
        private const int WM_USER = 0x400;
        private const int EM_GETEVENTMASK = (WM_USER + 59);
        private const int EM_SETEVENTMASK = (WM_USER + 69);
        [DllImport("user32", CharSet = CharSet.Auto)]
        private extern static IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, IntPtr lParam);
        IntPtr eventMask = IntPtr.Zero;
        #endregion

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                FormatAll();
            }
        }
        private List<char> csSplitters = new List<char>();

        public CodeEditor()
        {
            InitializeComponent();

            csSplitters.Add(' ');
            csSplitters.Add('\r');
            csSplitters.Add('\n');
            csSplitters.Add(',');
            csSplitters.Add('.');
            csSplitters.Add('-');
            csSplitters.Add('+');
            csSplitters.Add('<');
            csSplitters.Add('>');

            base.AcceptsTab = true;


        }
        public CodeEditor(string intialCodeContent)
            : this()
        {
            this.Text = intialCodeContent;
            this.SelectionStart = 0;
            FormatAll();
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (e.KeyData == Keys.Enter)
            {
                //FormatAll();
            }

            char c = (char)e.KeyData;
            if (csSplitters.Contains(c))
            {
                //format the line
                FormatLine();
            }

        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

        }

        private void FormatLine()
        {
            int cursorPosition = this.SelectionStart;
            bool resetWordWrap = this.WordWrap;

            try
            {
                this.WordWrap = false;
                if ((this.Lines != null) && (this.Lines.Length > 0))
                {
                    // Get the line.
                    int lineIndex = this.GetLineFromCharIndex(cursorPosition);
                    if (lineIndex > -1)
                    {
                        string lineText = this.Lines[lineIndex];

                        if (lineText.Trim().Length > 0)
                        {
                            FormatLine(lineIndex, lineText);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //log any errors
            }

            this.WordWrap = resetWordWrap;
            this.SelectionStart = cursorPosition;
            this.SelectionLength = 0;

        }
        public void FormatLines()
        {
            this.WordWrap = false;
            if ((this.Lines != null) && (this.Lines.Length > 0))
            {
                for (int lineIndex = 0; lineIndex < this.Lines.Length; lineIndex++)
                {
                    string lineText = this.Lines[lineIndex];

                    if (lineText.Trim().Length > 0)
                    {
                        FormatLine(lineIndex, lineText);
                    }
                }
            }
        }
        public void FormatLine(int lineIndex, string lineText)
        {
            // Get the index of the first char
            int firstChar = this.GetFirstCharIndexFromLine(lineIndex);
            ColorText(firstChar, lineText.Length, Color.Black);
            ApplyRegexes(lineText, firstChar);

        }
        private void ApplyRegexes(string content, int rtbIndex)
        {
            ApplyRegex(Regexes.KeyWords, content, rtbIndex, Color.Blue);
            ApplyRegex(Regexes.Regions, content, rtbIndex, Color.Blue);
            ApplyRegex(Regexes.Numbers, content, rtbIndex, Color.Magenta);
            ApplyRegex(Regexes.StringsSingleLine, content, rtbIndex, Color.Maroon);
            ApplyRegex(Regexes.StringsMultiLine, content, rtbIndex, Color.Maroon);
            ApplyRegex(Regexes.CommentsSingleLine, content, rtbIndex, Color.Green);
            ApplyRegex(Regexes.CommentsMultiLine, content, rtbIndex, Color.Green);
            ApplyRegex(Regexes.XmlComments, content, rtbIndex, Color.Gray);

        }
        private void ApplyRegex(Regex r, string content, int rtbIndex, Color color)
        {
            try
            {
                if (r != null)
                {
                    foreach (Match match in r.Matches(content))
                    {
                        string val = match.Value;
                        this.Select(match.Index + rtbIndex, val.Length);
                        this.SelectionColor = color;
                    }
                }
            }
            catch (Exception ex)
            {
                //log any errors
            }
        }
        private void FormatAll()
        {
            SuspendPainting();

            int cursorLoc = SelectionStart;

            ApplyRegexes(this.Text, 0);


            this.SelectionStart = cursorLoc;
            this.SelectionLength = 0;
            ResumePainting();


        }

        public void ColorText(int start, int length, Color color)
        {
            this.Select(start, length);
            this.SelectionColor = color;
            this.SelectionLength = 0;
        }

        public void SuspendPainting()
        {// Stop redrawing:
            SendMessage(this.Handle, WM_SETREDRAW, 0, IntPtr.Zero);
            // Stop sending of events:
            eventMask = SendMessage(this.Handle, EM_GETEVENTMASK, 0, IntPtr.Zero);
        }

        public void ResumePainting()
        {
            // turn on events
            SendMessage(this.Handle, EM_SETEVENTMASK, 0, eventMask);
            // turn on redrawing
            SendMessage(this.Handle, WM_SETREDRAW, 1, IntPtr.Zero);
        }

        private class Regexes
        {
            public static Regex KeyWords;
            public static Regex Regions;
            public static Regex Numbers;
            public static Regex CommentsSingleLine;
            public static Regex CommentsMultiLine;
            public static Regex XmlComments;
            public static Regex StringsSingleLine;
            public static Regex StringsMultiLine;
            static Regexes()
            {
                #region configure all the regexes
                ConfigKeyWords();
                ConfigRegion();
                ConfigNumbers();
                ConfigComments();
                ConfigXmlComments();
                ConfigStrings();
                #endregion
            }

            private static void ConfigStrings()
            {
                StringsMultiLine = new Regex(@"@"".*?""", RegexOptions.Singleline | RegexOptions.Compiled);

                StringsSingleLine = new Regex(@"""""|(?<!@)("".*?[^\\]"")", RegexOptions.Compiled);
            }
            private static void ConfigXmlComments()
            {
                XmlComments = new Regex(@"^\s*///.*?$", RegexOptions.Multiline | RegexOptions.IgnoreCase);
            }
            private static void ConfigComments()
            {
                CommentsSingleLine = new Regex(@"//.*$", RegexOptions.Multiline | RegexOptions.Compiled);
                CommentsMultiLine = new Regex(@"/\*.*?\*/", RegexOptions.Singleline | RegexOptions.Compiled);
            }
            private static void ConfigNumbers()
            {
                //handle numbers with decimals ending in l
                string s = @"\b((\d+[\.]?\d*[df]?)|(\d+[l]?)|(0x[a-f\d]+))\b";
                Numbers = new Regex(s, RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);
            }
            private static void ConfigRegion()
            {
                Regions = new Regex(@"\b#(region|endregion)\b", RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);
            }
            private static void ConfigKeyWords()
            {
                #region keywords

                //list of keywords from http://msdn.microsoft.com/en-us/library/x53a06bb.aspx 
                string s = @"\b(abstract|
                    as|
                    base|
                    bool|
                    break|
                    byte|
                    case|
                    catch|
                    char|
                    checked|
                    class|
                    const|
                    continue|
                    decimal|
                    default|
                    delegate|
                    do|
                    double|
                    else|
                    enum|
                    event|
                    explicit|
                    extern|
                    false|
                    finally|
                    fixed|
                    float|
                    for|
                    foreach|
                    goto|
                    if|
                    implicit|
                    in|
                    int|
                    interface|
                    internal|
                    is|
                    lock|
                    long|
                    namespace|
                    new|
                    null|
                    object|
                    operator|
                    out|
                    override|
                    params|
                    private|
                    protected|
                    public|
                    readonly|
                    ref|
                    return|
                    sbyte|
                    sealed|
                    short|
                    sizeof|
                    stackalloc|
                    static|
                    string|
                    struct|
                    switch|
                    this|
                    throw|
                    true|
                    try|
                    typeof|
                    uint|
                    ulong|
                    unchecked|
                    unsafe|
                    ushort|
                    using|
                    virtual|
                    void|
                    volatile|
                    while|
                    add|
                    alias|
                    ascending|
                    async|
                    await|
                    descending|
                    dynamic|
                    from|
                    get|
                    global|
                    group|
                    into|
                    join|
                    let|
                    orderby|
                    partial|
                    partial|
                    remove|
                    select|
                    set|
                    value|
                    var|
                    where|
                    where
                    yield)\b";
                KeyWords = new Regex(s, RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);
                #endregion
            }
        }
    }


    /**
    public class Win32
    {
        private Win32()
        {
        }

        public const int WM_USER = 0x400;
        public const int WM_PAINT = 0xF;
        public const int WM_KEYDOWN = 0x100;
        public const int WM_KEYUP = 0x101;
        public const int WM_CHAR = 0x102;

        public const int EM_GETSCROLLPOS = (WM_USER + 221);
        public const int EM_SETSCROLLPOS = (WM_USER + 222);

        public const int VK_CONTROL = 0x11;
        public const int VK_UP = 0x26;
        public const int VK_DOWN = 0x28;
        public const int VK_NUMLOCK = 0x90;

        public const short KS_ON = 0x01;
        public const short KS_KEYDOWN = 0x80;

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int x;
            public int y;
        }

        [DllImport("user32")]
        public static extern int SendMessage(HWND hwnd, int wMsg, int wParam, IntPtr lParam);
        [DllImport("user32")]
        public static extern int PostMessage(HWND hwnd, int wMsg, int wParam, int lParam);
        [DllImport("user32")]
        public static extern short GetKeyState(int nVirtKey);
        [DllImport("user32")]
        public static extern int LockWindowUpdate(HWND hwnd);

    }
     * **/
}
