using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History  
* WI  71865 JMC 05/30/2013
*   -Initial Version
* WI 114570 MLH 09/20/2013
*   - Removed CurrentProcessingDate from Standard Fields (for v2.0)
* WI 115064 & WI 115289 02/06/2014
*   - Changed noted constants for correct file extensions.
* WI 116533 - 02/10/2014
*   - Added a few constants to be used in frmMain.cs.
*   - Moved File Extension constants to ExtractLib.Constants. They were being duplicated here and in ExtractWizard.Constants.
* WI 135766 BLR 04/11/2014
*   - Updated constants 'Customer' and 'Lockbox' to 'Organization' and 'Workgroup'.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal delegate void ItemDroppedEventHandler(string ItemText);
    internal delegate void ItemMovedEventHandler(int CurrentIndex, int NewIndex);
    internal delegate void ItemDeletedEventHandler(int ItemIndex);

    internal static class Constants {
    
        public const string NEW_LAYOUT_LABEL_FILE = "New File Layout";
        public const string NEW_LAYOUT_LABEL_BANK = "New Bank Layout";
        public const string NEW_LAYOUT_LABEL_CUSTOMER = "New Customer Layout";
        public const string NEW_LAYOUT_LABEL_LBX = "New Lockbox Layout";
        public const string NEW_LAYOUT_LABEL_BATCH = "New Batch Layout";
        public const string NEW_LAYOUT_LABEL_TXN = "New Transaction Layout";
        public const string NEW_LAYOUT_LABEL_CHECK = "New Check Layout";
        public const string NEW_LAYOUT_LABEL_STUB = "New Stub Layout";
        public const string NEW_LAYOUT_LABEL_DOC = "New Document Layout";
        public const string NEW_LAYOUT_LABEL_JD_BANK = "New Bank Level Joint Detail Layout";
        public const string NEW_LAYOUT_LABEL_JD_CUSTOMER = "New Customer Level Joint Detail Layout";
        public const string NEW_LAYOUT_LABEL_JD_LBX = "New Lockbox Level Joint Detail Layout";
        public const string NEW_LAYOUT_LABEL_JD_BATCH = "New Batch Level Joint Detail Layout";
        public const string NEW_LAYOUT_LABEL_JD_TXN = "New Transaction Level Joint Detail Layout";
        public const string NEW_LAYOUT_LABEL_JD_DETAIL = "New Item Level Joint Detail Layout";

        public const string NODE_LABEL_GENERAL = "General";
        public const string NODE_LABEL_DATA_ENTRY = "Data Entry";

        // Aggregate Fields
        // WI 116533 : Standardize the constant strings in frmMain.cs. 
        //     Added NODE_LABEL_RECORD_COUNT_FILE, NODE_LABEL_RECORD_COUNT_CUSTOMER, NODE_LABEL_RECORD_COUNT_LOCKBOX
        //           NODE_LABEL_RECORD_COUNT_TRANSACTION, NODE_LABEL_RECORD_COUNT_STUB, NODE_LABEL_RECORD_COUNT_TOTAL
        public const string NODE_LABEL_AGGREGATE_FLDS = "Aggregate Fields";
        public const string NODE_LABEL_COUNT_BATCHES = "CountBatches";
        public const string NODE_LABEL_COUNT_TXNS = "CountTransactions";
        public const string NODE_LABEL_COUNT_CHECKS = "CountPayments";
        public const string NODE_LABEL_COUNT_DOCS = "CountDocuments";
        public const string NODE_LABEL_COUNT_STUBS = "CountStubs";
        public const string NODE_LABEL_RECORD_COUNT_TRANSACTIONS = "RecordCountTran";
        public const string NODE_LABEL_RECORD_COUNT_STUB = "RecordCountStub";
        public const string NODE_LABEL_RECORD_COUNT_LOCKBOX = "RecordCountWorkgroup";
        public const string NODE_LABEL_RECORD_COUNT_FILE = "RecordCountFile";
        public const string NODE_LABEL_RECORD_COUNT_BANK = "RecordCountBank";
        public const string NODE_LABEL_RECORD_COUNT_BATCH = "RecordCountBatch";
        public const string NODE_LABEL_RECORD_COUNT_BATCH_DETAIL = "RecordCountBatchDetail";
        public const string NODE_LABEL_RECORD_COUNT_CHECK = "RecordCountPayment";
        public const string NODE_LABEL_RECORD_COUNT_DETAIL = "RecordCountDetail";
        public const string NODE_LABEL_RECORD_COUNT_DOC = "RecordCountDoc";
        public const string NODE_LABEL_RECORD_COUNT_TOTAL = "RecordCountTotal";
        public const string NODE_LABEL_SUM_CHECKS_AMT = "SumPaymentsAmount";
        public const string NODE_LABEL_SUM_CHECKS_DEBILLINGKEYS = "SumPaymentsDEBillingKeys";
        public const string NODE_LABEL_SUM_CHECKS_DEDATAKEYS = "SumPaymentsDEDataKeys";
        public const string NODE_LABEL_SUM_STUBS_AMT = "SumStubsAmount";
        public const string NODE_LABEL_SUM_STUBS_DEBILLINGKEYS = "SumStubsDEBillingKeys";
        public const string NODE_LABEL_SUM_STUBS_DEDATAKEYS = "SumStubsDEDataKeys";
        public const string NODE_LABEL_ADD_AGGREGATE_FIELD = "Add Aggregate Field";

        // Standard Fields
        // WI 116533 : Standardize the constant strings in frmMain.cs. (Added NODE_STANDARD_SECTION)
        public const string NODE_STANDARD_SECTION = "STANDARD";
        public const string NODE_LABEL_STD_FIELDS = "Standard Fields";
        public const string NODE_LABEL_COUNTER = "Counter";
        // WI 114570 removed for v2.0
        //public const string NODE_LABEL_CURRENT_PROC_DTE = "CurrentProcessingDate";
        public const string NODE_LABEL_DATE = "Date";
        public const string NODE_LABEL_FILLER = "Filler";
        public const string NODE_LABEL_FIRSTLAST = "FirstLast";
        public const string NODE_LABEL_IMG_CHECK_PER_BATCH = "ImagePaymentPerBatch";
        public const string NODE_LABEL_IMG_CHECK_PER_TXN = "ImagePaymentPerTransaction";
        public const string NODE_LABEL_IMG_CHECK_SINGLE_FRONT = "ImagePaymentSingleFront";
        public const string NODE_LABEL_IMG_CHECK_SINGLE_REAR = "ImagePaymentSingleRear";
        public const string NODE_LABEL_IMG_FILE_NAME = "ImageFileName";
        public const string NODE_LABEL_IMG_DOC_PER_BATCH = "ImageDocumentPerBatch";
        public const string NODE_LABEL_IMG_DOC_PER_TXN = "ImageDocumentPerTransaction";
        public const string NODE_LABEL_IMG_DOC_SINGLE_FRONT = "ImageDocumentSingleFront";
        public const string NODE_LABEL_IMG_DOC_SINGLE_REAR = "ImageDocumentSingleRear";
        public const string NODE_LABEL_LINE_COUNTER_FILE = "LineCounterFile";
        public const string NODE_LABEL_LINE_COUNTER_BANK = "LineCounterBank";
        public const string NODE_LABEL_LINE_COUNTER_LBX = "LineCounterWorkgroup";
        public const string NODE_LABEL_LINE_COUNTER_BATCH = "LineCounterBatch";
        public const string NODE_LABEL_LINE_COUNTER_TXN = "LineCounterTransaction";
        public const string NODE_LABEL_LINE_COUNTER_CHECK = "LineCounterPayment";
        public const string NODE_LABEL_LINE_COUNTER_STUB = "LineCounterStub";
        public const string NODE_LABEL_LINE_COUNTER_DOC = "LineCounterDocument";
        public const string NODE_LABEL_PROC_RUN_DATE = "ProcessingRunDate";
        public const string NODE_LABEL_STATIC = "Static";
        public const string NODE_LABEL_TIME = "Time";
        public const string NODE_LABEL_ADD_STD_FIELDS = "Add Standard Field";
    }
}
