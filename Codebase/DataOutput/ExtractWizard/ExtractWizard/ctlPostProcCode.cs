﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using RTFCodeEditor;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 130551 BLR 03/05/2014
*   - Stripped out Actipro, added in our own code editor.
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
* WI 133174 DJW 03/19/2014
*   - Added a support for a reset button to the editor.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    public partial class ctlPostProcCode : UserControl {

        private bool _IsDirty = false;

        public ctlPostProcCode(cPostProcCodeModule postProcCodeModule) {
            InitializeComponent();


            LoadPostProcCodeModule(postProcCodeModule);
        }

        public void LoadPostProcCodeModule(cPostProcCodeModule postProcCodeModule) 
        {
            editor.Text = postProcCodeModule.SrcCode;
            if (string.IsNullOrWhiteSpace(postProcCodeModule.SrcCode))
            {
                LoadDefaultSourceCode();
            }
        }

        private void LoadDefaultSourceCode()
        {
            StringBuilder sbSrcCode = new StringBuilder();

            sbSrcCode.Append(@"using System;" + Environment.NewLine);
            sbSrcCode.Append(@"using System.Collections;" + Environment.NewLine);
            sbSrcCode.Append(@"using System.Collections.Generic;" + Environment.NewLine);
            sbSrcCode.Append(Environment.NewLine);
            sbSrcCode.Append(@"/// <summary>" + Environment.NewLine);
            sbSrcCode.Append(@"/// This module exposes the extract file data prior to being written to the" + Environment.NewLine);
            sbSrcCode.Append(@"/// extract file.  Each member of the input array represents a line of text that will" + Environment.NewLine);
            sbSrcCode.Append(@"/// normally be written to the extract file.  The output of the ProcessExtract" + Environment.NewLine);
            // CR 49779 MEH 02-03-2012 To avoid an error after adding a post processing code module, you need to save your extract twice
            // there was a space at the end of this comment that would cause the HashCode to be calculated differently
            // after the first time the extract script is saved because the space would be removed
            sbSrcCode.Append(@"/// function will be the actual text that will be written to the extract file.  The" + Environment.NewLine);
            sbSrcCode.Append(@"/// data can be modified, appended to, or removed as required to produce the desired results." + Environment.NewLine);
            sbSrcCode.Append(@"/// The namespace, class name, and function definition can not be modified" + Environment.NewLine);
            sbSrcCode.Append(@"/// or the code module will not be executed by the extract engine and an error will occur." + Environment.NewLine);
            sbSrcCode.Append(@"/// </summary>" + Environment.NewLine);
            sbSrcCode.Append(@"namespace WFS.RecHub.DataOutputToolkit.Extract.CodeModule {" + Environment.NewLine);
            sbSrcCode.Append(Environment.NewLine);
            sbSrcCode.Append(@"    public class PostProcessing {" + Environment.NewLine);
            sbSrcCode.Append(Environment.NewLine);
            sbSrcCode.Append(@"        public object ProcessExtract(List<string> FileRows) {" + Environment.NewLine);
            sbSrcCode.Append(Environment.NewLine);
            sbSrcCode.Append(@"            List<string> arRetVal = new List<string>();" + Environment.NewLine);
            sbSrcCode.Append(Environment.NewLine);
            sbSrcCode.Append(@"            // Loop through each row of text of the extract file and modify" + Environment.NewLine);
            sbSrcCode.Append(@"            // as necessary" + Environment.NewLine);
            sbSrcCode.Append(@"            foreach(string rowText in FileRows) {" + Environment.NewLine);
            sbSrcCode.Append(@"                arRetVal.Add(rowText);" + Environment.NewLine);
            sbSrcCode.Append(@"            }" + Environment.NewLine);
            sbSrcCode.Append(Environment.NewLine);
            sbSrcCode.Append(@"            return(arRetVal);" + Environment.NewLine);
            sbSrcCode.Append(@"        }" + Environment.NewLine);
            sbSrcCode.Append(@"    }" + Environment.NewLine);
            sbSrcCode.Append(@"}" + Environment.NewLine);

            editor.Text = sbSrcCode.ToString();
            editor.WordWrap = false;
        }

        public bool IsDirty {
            get {
                return(_IsDirty);
            }
        }

        public string SrcCode {
            get {
                return (editor.Text);
            }
        }

        private void editor_TextChanged(object sender, EventArgs e)
        {
            _IsDirty = true;
        }

        private void btnTestCode_Click(object sender, EventArgs e)
        {
            var test = new frmCodeTest();
            test.SrcCode = SrcCode;
            test.ShowDialog();
        }

        // WI 133174 : Added support to the Reset button.
        private void btnResetCode_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you wish to reset your code?", "Code Reset", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                LoadDefaultSourceCode();
                editor.FormatLines();
            }
        }

        private void ctlPostProcCode_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}