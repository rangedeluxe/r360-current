using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
* WI 135749 BLR 04/14/2014
*   - Added in a 'FullName' field. 
* WI 144581 BLR 06/09/2014
*   - Altered to case 2 for the Ascending/Descending columns. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal partial class ctlOrderByColumns : UserControl {

        public event ItemDroppedEventHandler ItemDropped;
        public event ItemMovedEventHandler ItemMoved;
        public event ItemDeletedEventHandler ItemDeleted;

        public ctlOrderByColumns(List<cOrderByColumn> orderByColumns) {

            InitializeComponent();

            this.ctlDataGrid1.AddColumnsRange(
                new System.Windows.Forms.DataGridViewColumn[] {
                this.colFieldName,
                this.colFieldFullName,
                this.colDirection});

            LoadOrderByColumns(orderByColumns);
        }

        public void LoadOrderByColumns(
            List<cOrderByColumn> orderByColumns) {

            this.ctlDataGrid1.SuspendEvents();

            try {

                this.ctlDataGrid1.Clear();

                foreach(cOrderByColumn orderbycolumn in orderByColumns) {
                    AddOrderByColumn(orderbycolumn);            
                }

                this.ctlDataGrid1.ResumeEvents();

            } catch(Exception ex) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                this.ctlDataGrid1.ResumeEvents();
                throw(ex);
            }
        }
        
        internal void AddOrderByColumn(
            cOrderByColumn orderByColumn) {

            DataGridViewCell[] cellArray;
            DataGridViewTextBoxCell dgText;
            DataGridViewComboBoxCell dgCombo;
            DataGridViewRow dgRow;

            // WI 135749 : Added in a 'FullName' field.

            cellArray = new DataGridViewCell[3];

            dgText = new DataGridViewTextBoxCell();
            dgText.Value = orderByColumn.ColumnName;
            dgText.MaxInputLength = 100;
            cellArray[0] = dgText;

            dgText = new DataGridViewTextBoxCell();
            dgText.Value = orderByColumn.DisplayName;
            dgText.MaxInputLength = 100;
            cellArray[1] = dgText;

            dgCombo = new DataGridViewComboBoxCell();
            dgCombo.Items.Add("Ascending");
            dgCombo.Items.Add("Descending");
            if(orderByColumn.OrderByDir == OrderByDirEnum.Ascending) {
                dgCombo.Value = "Ascending";
            } else {
                dgCombo.Value = "Descending";
            }
            cellArray[2] = dgCombo;

            dgRow = new DataGridViewRow();
            dgRow.Cells.AddRange(cellArray);
            
            this.ctlDataGrid1.AddRow(dgRow);
        }

        internal void RemoveAt(int index) {
            this.ctlDataGrid1.RemoveAt(index);
        }

        private void ctlDataGrid1_ItemDeleted(int rowIndex) {

            if(ItemDeleted != null) {
                ItemDeleted(rowIndex);
            }
        }

        private void ctlDataGrid1_ItemDropped(string fieldName) {

            if(ItemDropped != null) {
                ItemDropped(fieldName);
            }
        }

        private void ctlDataGrid1_ItemMoved(int currentIndex, int newIndex) {

            if(ItemMoved != null) {
                ItemMoved(currentIndex, newIndex);
            }
        }

        // WI 144581 : Fixed 'case 1' to 'case 2'. Ascending/Descending is now in index 2.
        private void ctlDataGrid1_CellEndEdit(object sender, DataGridViewCellEventArgs e) {

            if(((frmMain)this.ParentForm).tvOrderData.SelectedNode != null && 
               ((frmMain)this.ParentForm).tvOrderData.SelectedNode.Tag != null && 
               ((frmMain)this.ParentForm).tvOrderData.SelectedNode.Tag is List<cOrderByColumn>) {

                switch(e.ColumnIndex) {
                    case 2:
                        ((List<cOrderByColumn>)
                            ((frmMain)this.ParentForm).tvOrderData.SelectedNode.Tag)
                                [e.RowIndex].OrderByDir = (this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString().ToLower().StartsWith("desc")) 
                                    ? OrderByDirEnum.Descending 
                                    : OrderByDirEnum.Ascending;
                        break;
                }
            }
        }

        private void ctlOrderByColumns_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}
