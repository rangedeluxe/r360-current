//using DMP.Core.Components;

/*******************************************************************************
*
*    Module: frmMain
*  Filename: frmMain.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class frmMain {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing) {
                if(components != null) {
                    components.Dispose();
                }

                //// clean up Sentry object
                //Sentry.Dismiss();  //CR11654 4-5-2005 sma
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tvLayouts = new System.Windows.Forms.TreeView();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.lblExtractScript = new System.Windows.Forms.Label();
            this.btnSelectExtractScriptDir = new System.Windows.Forms.Button();
            this.txtExtractScriptPath = new System.Windows.Forms.TextBox();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.splitBody = new System.Windows.Forms.SplitContainer();
            this.splitLeft = new System.Windows.Forms.SplitContainer();
            this.tabExtractDef = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tvOrderData = new System.Windows.Forms.TreeView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tvLimitData = new System.Windows.Forms.TreeView();
            this.btnAddDataField = new System.Windows.Forms.Button();
            this.tvDataFields = new System.Windows.Forms.TreeView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileNew = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileClose = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileSave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSep2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileSetupRpt = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSetupRptPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFileSetupRptPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilePrint = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuFilePrinterSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSep3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuView = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuViewExtractDefinition = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuViewResults = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuViewLogFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuRun = new System.Windows.Forms.ToolStripMenuItem();
            this.munHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pnlTop.SuspendLayout();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitBody)).BeginInit();
            this.splitBody.Panel1.SuspendLayout();
            this.splitBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitLeft)).BeginInit();
            this.splitLeft.Panel1.SuspendLayout();
            this.splitLeft.Panel2.SuspendLayout();
            this.splitLeft.SuspendLayout();
            this.tabExtractDef.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tvLayouts
            // 
            this.tvLayouts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvLayouts.HideSelection = false;
            this.tvLayouts.LabelEdit = true;
            this.tvLayouts.Location = new System.Drawing.Point(3, 3);
            this.tvLayouts.Name = "tvLayouts";
            this.tvLayouts.Size = new System.Drawing.Size(200, 314);
            this.tvLayouts.TabIndex = 3;
            this.tvLayouts.BeforeLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvLayouts_BeforeLabelEdit);
            this.tvLayouts.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.tvLayouts_AfterLabelEdit);
            this.tvLayouts.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvLayouts_AfterSelect);
            this.tvLayouts.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvLayouts_NodeMouseClick);
            this.tvLayouts.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvLayouts_KeyDown);
            // 
            // pnlTop
            // 
            this.pnlTop.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTop.Controls.Add(this.lblExtractScript);
            this.pnlTop.Controls.Add(this.btnSelectExtractScriptDir);
            this.pnlTop.Controls.Add(this.txtExtractScriptPath);
            this.pnlTop.Location = new System.Drawing.Point(12, 27);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(1026, 31);
            this.pnlTop.TabIndex = 3;
            // 
            // lblExtractScript
            // 
            this.lblExtractScript.AutoSize = true;
            this.lblExtractScript.Location = new System.Drawing.Point(3, 7);
            this.lblExtractScript.Name = "lblExtractScript";
            this.lblExtractScript.Size = new System.Drawing.Size(109, 13);
            this.lblExtractScript.TabIndex = 26;
            this.lblExtractScript.Text = "Extract Definition File:";
            // 
            // btnSelectExtractScriptDir
            // 
            this.btnSelectExtractScriptDir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectExtractScriptDir.Location = new System.Drawing.Point(993, 2);
            this.btnSelectExtractScriptDir.Name = "btnSelectExtractScriptDir";
            this.btnSelectExtractScriptDir.Size = new System.Drawing.Size(28, 23);
            this.btnSelectExtractScriptDir.TabIndex = 1;
            this.btnSelectExtractScriptDir.Text = "...";
            this.btnSelectExtractScriptDir.UseVisualStyleBackColor = true;
            this.btnSelectExtractScriptDir.Click += new System.EventHandler(this.btnSelectExtractScriptDir_Click);
            // 
            // txtExtractScriptPath
            // 
            this.txtExtractScriptPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExtractScriptPath.Location = new System.Drawing.Point(118, 4);
            this.txtExtractScriptPath.Name = "txtExtractScriptPath";
            this.txtExtractScriptPath.ReadOnly = true;
            this.txtExtractScriptPath.Size = new System.Drawing.Size(869, 20);
            this.txtExtractScriptPath.TabIndex = 0;
            this.txtExtractScriptPath.TabStop = false;
            // 
            // pnlMain
            // 
            this.pnlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMain.Controls.Add(this.splitBody);
            this.pnlMain.Location = new System.Drawing.Point(12, 60);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1026, 600);
            this.pnlMain.TabIndex = 4;
            // 
            // splitBody
            // 
            this.splitBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitBody.Location = new System.Drawing.Point(3, 0);
            this.splitBody.Name = "splitBody";
            // 
            // splitBody.Panel1
            // 
            this.splitBody.Panel1.Controls.Add(this.splitLeft);
            // 
            // splitBody.Panel2
            // 
            this.splitBody.Panel2.Leave += new System.EventHandler(this.splitBody_Panel2_Leave);
            this.splitBody.Size = new System.Drawing.Size(1020, 597);
            this.splitBody.SplitterDistance = 214;
            this.splitBody.TabIndex = 1;
            this.splitBody.TabStop = false;
            // 
            // splitLeft
            // 
            this.splitLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitLeft.Location = new System.Drawing.Point(0, 0);
            this.splitLeft.Name = "splitLeft";
            this.splitLeft.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitLeft.Panel1
            // 
            this.splitLeft.Panel1.Controls.Add(this.tabExtractDef);
            this.splitLeft.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // splitLeft.Panel2
            // 
            this.splitLeft.Panel2.Controls.Add(this.btnAddDataField);
            this.splitLeft.Panel2.Controls.Add(this.tvDataFields);
            this.splitLeft.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitLeft.Size = new System.Drawing.Size(214, 597);
            this.splitLeft.SplitterDistance = 346;
            this.splitLeft.TabIndex = 1;
            // 
            // tabExtractDef
            // 
            this.tabExtractDef.Controls.Add(this.tabPage1);
            this.tabExtractDef.Controls.Add(this.tabPage2);
            this.tabExtractDef.Controls.Add(this.tabPage3);
            this.tabExtractDef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabExtractDef.Location = new System.Drawing.Point(0, 0);
            this.tabExtractDef.Name = "tabExtractDef";
            this.tabExtractDef.SelectedIndex = 0;
            this.tabExtractDef.Size = new System.Drawing.Size(214, 346);
            this.tabExtractDef.TabIndex = 2;
            this.tabExtractDef.SelectedIndexChanged += new System.EventHandler(this.tabExtractDef_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tvLayouts);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(206, 320);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Layouts";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tvOrderData);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(206, 320);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Order Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tvOrderData
            // 
            this.tvOrderData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvOrderData.HideSelection = false;
            this.tvOrderData.Location = new System.Drawing.Point(3, 3);
            this.tvOrderData.Name = "tvOrderData";
            this.tvOrderData.Size = new System.Drawing.Size(200, 314);
            this.tvOrderData.TabIndex = 3;
            this.tvOrderData.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvOrderData_AfterSelect);
            this.tvOrderData.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvOrderData_NodeMouseClick);
            this.tvOrderData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvOrderData_KeyDown);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tvLimitData);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(206, 320);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Limit Data";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tvLimitData
            // 
            this.tvLimitData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvLimitData.HideSelection = false;
            this.tvLimitData.Location = new System.Drawing.Point(3, 3);
            this.tvLimitData.Name = "tvLimitData";
            this.tvLimitData.Size = new System.Drawing.Size(200, 314);
            this.tvLimitData.TabIndex = 3;
            this.tvLimitData.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvLimitData_AfterSelect);
            this.tvLimitData.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvLimitData_NodeMouseClick);
            this.tvLimitData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tvLimitData_KeyDown);
            // 
            // btnAddDataField
            // 
            this.btnAddDataField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddDataField.Enabled = false;
            this.btnAddDataField.Location = new System.Drawing.Point(4, 219);
            this.btnAddDataField.Name = "btnAddDataField";
            this.btnAddDataField.Size = new System.Drawing.Size(206, 23);
            this.btnAddDataField.TabIndex = 5;
            this.btnAddDataField.Text = "Add Field";
            this.btnAddDataField.UseVisualStyleBackColor = true;
            this.btnAddDataField.Click += new System.EventHandler(this.btnAddDataField_Click);
            // 
            // tvDataFields
            // 
            this.tvDataFields.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvDataFields.HideSelection = false;
            this.tvDataFields.Location = new System.Drawing.Point(0, 0);
            this.tvDataFields.Name = "tvDataFields";
            this.tvDataFields.ShowNodeToolTips = true;
            this.tvDataFields.Size = new System.Drawing.Size(214, 213);
            this.tvDataFields.TabIndex = 4;
            this.tvDataFields.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.tvDataFields_AfterCollapse);
            this.tvDataFields.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.tvDataFields_AfterExpand);
            this.tvDataFields.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.tvDataFields_ItemDrag);
            this.tvDataFields.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvDataFields_BeforeSelect);
            this.tvDataFields.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvDataFields_AfterSelect);
            this.tvDataFields.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvDataFields_NodeMouseDoubleClick);
            this.tvDataFields.DragEnter += new System.Windows.Forms.DragEventHandler(this.tvDataFields_DragEnter);
            this.tvDataFields.DragOver += new System.Windows.Forms.DragEventHandler(this.tvDataFields_DragOver);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusMsg});
            this.statusStrip1.Location = new System.Drawing.Point(0, 663);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1045, 22);
            this.statusStrip1.TabIndex = 27;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusMsg
            // 
            this.toolStripStatusMsg.Name = "toolStripStatusMsg";
            this.toolStripStatusMsg.Size = new System.Drawing.Size(0, 17);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "mainicon.ico");
            this.imageList1.Images.SetKeyName(1, "openHS.png");
            this.imageList1.Images.SetKeyName(2, "saveHS.png");
            this.imageList1.Images.SetKeyName(3, "PrintPreviewHS.png");
            this.imageList1.Images.SetKeyName(4, "PrintHS.png");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.mnuView,
            this.mnuRun,
            this.munHelp});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1045, 24);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.MenuActivate += new System.EventHandler(this.menuStrip1_MenuActivate);
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileNew,
            this.mnuFileOpen,
            this.mnuFileClose,
            this.mnuSep1,
            this.mnuFileSave,
            this.mnuFileSaveAs,
            this.mnuSep2,
            this.mnuFileSetupRpt,
            this.mnuFilePrint,
            this.mnuFilePrinterSetup,
            this.mnuSep3,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(37, 20);
            this.mnuFile.Text = "&File";
            // 
            // mnuFileNew
            // 
            this.mnuFileNew.Name = "mnuFileNew";
            this.mnuFileNew.Size = new System.Drawing.Size(205, 22);
            this.mnuFileNew.Text = "&New Extract Definition";
            this.mnuFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
            // 
            // mnuFileOpen
            // 
            this.mnuFileOpen.Image = global::WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.Resource1.openHS;
            this.mnuFileOpen.Name = "mnuFileOpen";
            this.mnuFileOpen.Size = new System.Drawing.Size(205, 22);
            this.mnuFileOpen.Text = "&Open Extract Definition...";
            this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
            // 
            // mnuFileClose
            // 
            this.mnuFileClose.Name = "mnuFileClose";
            this.mnuFileClose.Size = new System.Drawing.Size(205, 22);
            this.mnuFileClose.Text = "&Close Extract Definition";
            this.mnuFileClose.Click += new System.EventHandler(this.mnuFileClose_Click);
            // 
            // mnuSep1
            // 
            this.mnuSep1.Name = "mnuSep1";
            this.mnuSep1.Size = new System.Drawing.Size(202, 6);
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Image = global::WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.Resource1.saveHS;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Size = new System.Drawing.Size(205, 22);
            this.mnuFileSave.Text = "&Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSaveAs
            // 
            this.mnuFileSaveAs.Name = "mnuFileSaveAs";
            this.mnuFileSaveAs.Size = new System.Drawing.Size(205, 22);
            this.mnuFileSaveAs.Text = "Save &As...";
            this.mnuFileSaveAs.Click += new System.EventHandler(this.mnuFileSaveAs_Click);
            // 
            // mnuSep2
            // 
            this.mnuSep2.Name = "mnuSep2";
            this.mnuSep2.Size = new System.Drawing.Size(202, 6);
            // 
            // mnuFileSetupRpt
            // 
            this.mnuFileSetupRpt.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileSetupRptPreview,
            this.mnuFileSetupRptPrint});
            this.mnuFileSetupRpt.Name = "mnuFileSetupRpt";
            this.mnuFileSetupRpt.Size = new System.Drawing.Size(205, 22);
            this.mnuFileSetupRpt.Text = "Setup Report";
            this.mnuFileSetupRpt.Visible = false;
            // 
            // mnuFileSetupRptPreview
            // 
            this.mnuFileSetupRptPreview.Image = global::WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.Properties.Resources.PrintPreviewHS;
            this.mnuFileSetupRptPreview.Name = "mnuFileSetupRptPreview";
            this.mnuFileSetupRptPreview.Size = new System.Drawing.Size(124, 22);
            this.mnuFileSetupRptPreview.Text = "Preview...";
            this.mnuFileSetupRptPreview.Click += new System.EventHandler(this.mnuFileSetupRptPreview_Click);
            // 
            // mnuFileSetupRptPrint
            // 
            this.mnuFileSetupRptPrint.Image = global::WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.Properties.Resources.PrintHS;
            this.mnuFileSetupRptPrint.Name = "mnuFileSetupRptPrint";
            this.mnuFileSetupRptPrint.Size = new System.Drawing.Size(124, 22);
            this.mnuFileSetupRptPrint.Text = "Print...";
            this.mnuFileSetupRptPrint.Click += new System.EventHandler(this.mnuFileSetupRptPrint_Click);
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Image = global::WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.Resource1.PrintHS;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Size = new System.Drawing.Size(205, 22);
            this.mnuFilePrint.Text = "&Print...";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFilePrinterSetup
            // 
            this.mnuFilePrinterSetup.Name = "mnuFilePrinterSetup";
            this.mnuFilePrinterSetup.Size = new System.Drawing.Size(205, 22);
            this.mnuFilePrinterSetup.Text = "Printer Setup...";
            this.mnuFilePrinterSetup.Click += new System.EventHandler(this.mnuFilePrinterSetup_Click);
            // 
            // mnuSep3
            // 
            this.mnuSep3.Name = "mnuSep3";
            this.mnuSep3.Size = new System.Drawing.Size(202, 6);
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Size = new System.Drawing.Size(205, 22);
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // mnuView
            // 
            this.mnuView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuViewExtractDefinition,
            this.mnuViewResults,
            this.mnuViewLogFile});
            this.mnuView.Name = "mnuView";
            this.mnuView.Size = new System.Drawing.Size(44, 20);
            this.mnuView.Text = "&View";
            // 
            // mnuViewExtractDefinition
            // 
            this.mnuViewExtractDefinition.Enabled = false;
            this.mnuViewExtractDefinition.Name = "mnuViewExtractDefinition";
            this.mnuViewExtractDefinition.Size = new System.Drawing.Size(164, 22);
            this.mnuViewExtractDefinition.Text = "Extract Definition";
            this.mnuViewExtractDefinition.Click += new System.EventHandler(this.mnuViewExtractDefinition_Click);
            // 
            // mnuViewResults
            // 
            this.mnuViewResults.Name = "mnuViewResults";
            this.mnuViewResults.Size = new System.Drawing.Size(164, 22);
            this.mnuViewResults.Text = "Results";
            this.mnuViewResults.Click += new System.EventHandler(this.mnuViewResults_Click);
            // 
            // mnuViewLogFile
            // 
            this.mnuViewLogFile.Name = "mnuViewLogFile";
            this.mnuViewLogFile.Size = new System.Drawing.Size(164, 22);
            this.mnuViewLogFile.Text = "Log File";
            this.mnuViewLogFile.Click += new System.EventHandler(this.mnuViewLogFile_Click);
            // 
            // mnuRun
            // 
            this.mnuRun.Name = "mnuRun";
            this.mnuRun.Size = new System.Drawing.Size(40, 20);
            this.mnuRun.Text = "&Run";
            this.mnuRun.Click += new System.EventHandler(this.mnuRun_Click);
            // 
            // munHelp
            // 
            this.munHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuHelpAbout});
            this.munHelp.Name = "munHelp";
            this.munHelp.Size = new System.Drawing.Size(44, 20);
            this.munHelp.Text = "&Help";
            // 
            // mnuHelpAbout
            // 
            this.mnuHelpAbout.Name = "mnuHelpAbout";
            this.mnuHelpAbout.Size = new System.Drawing.Size(116, 22);
            this.mnuHelpAbout.Text = "About...";
            this.mnuHelpAbout.Click += new System.EventHandler(this.mnuHelpAbout_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 685);
            this.Controls.Add(this.pnlTop);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pnlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            this.splitBody.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitBody)).EndInit();
            this.splitBody.ResumeLayout(false);
            this.splitLeft.Panel1.ResumeLayout(false);
            this.splitLeft.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitLeft)).EndInit();
            this.splitLeft.ResumeLayout(false);
            this.tabExtractDef.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TreeView tvLayouts;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.SplitContainer splitBody;
        private System.Windows.Forms.Label lblExtractScript;
        private System.Windows.Forms.Button btnSelectExtractScriptDir;
        private System.Windows.Forms.TextBox txtExtractScriptPath;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusMsg;
        private System.Windows.Forms.SplitContainer splitLeft;
        private System.Windows.Forms.TreeView tvDataFields;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuFileNew;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem mnuFileClose;
        private System.Windows.Forms.ToolStripSeparator mnuSep1;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSave;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSaveAs;
        private System.Windows.Forms.ToolStripSeparator mnuSep2;
        private System.Windows.Forms.ToolStripMenuItem mnuFilePrint;
        private System.Windows.Forms.ToolStripMenuItem mnuFilePrinterSetup;
        private System.Windows.Forms.ToolStripSeparator mnuSep3;
        private System.Windows.Forms.ToolStripMenuItem mnuFileExit;
        private System.Windows.Forms.ToolStripMenuItem mnuView;
        private System.Windows.Forms.ToolStripMenuItem mnuViewExtractDefinition;
        private System.Windows.Forms.ToolStripMenuItem mnuViewResults;
        private System.Windows.Forms.ToolStripMenuItem mnuViewLogFile;
        private System.Windows.Forms.ToolStripMenuItem mnuRun;
        private System.Windows.Forms.ToolStripMenuItem munHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuHelpAbout;
        public System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        internal System.Windows.Forms.TabControl tabExtractDef;
        internal System.Windows.Forms.TreeView tvOrderData;
        internal System.Windows.Forms.TreeView tvLimitData;
        private System.Windows.Forms.Button btnAddDataField;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSetupRpt;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSetupRptPrint;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSetupRptPreview;
    }
}

