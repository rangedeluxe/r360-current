using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    public partial class frmEnterText : Form {

        private bool _Cancelled = true;

        public frmEnterText(
            string caption, 
            string labelText, 
            string defaultValue) {
            
            InitializeComponent();
            
            this.Text = caption;
            this.lblCaption.Text = labelText;
            this.txtValue.Text = defaultValue;
        }

        public bool Cancelled {
            get {
                return(_Cancelled);
            }
        }

        public string Value {
            get {
                if(Cancelled) {
                    return(string.Empty);
                } else {
                    return(this.txtValue.Text);
                }
            }
        }

        private void SaveAndClose() {
            _Cancelled = false;
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e) {
            SaveAndClose();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            _Cancelled = true;
            this.Close();
        }

        private void txtValue_TextChanged(object sender, EventArgs e) {
            this.btnOk.Enabled = this.txtValue.Text.Length > 0;
        }

        private void txtLayoutName_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyCode == Keys.Enter) {
                SaveAndClose();
            }
        }

        private void frmEnterText_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}