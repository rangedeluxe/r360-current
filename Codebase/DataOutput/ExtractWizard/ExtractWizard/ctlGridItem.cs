using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using IntegraPAY.Delivery.Extract.ExtractAPI;

/*******************************************************************************
*
*    Module: ctlGridItem
*  Filename: ctlGridItem.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace IntegraPAY.Delivery.Extract.ExtractWizard {

    internal partial class ctlGridItem : UserControl {

        private bool _IsDirty = false;
        private int _Index;

        public ctlGridItem(int vIndex, cGridItem GridItem) {

            InitializeComponent();
            LoadGridItem(vIndex, GridItem);
        }
        
        public int Index {
            get {
                return(_Index);
            }
        }
        
        public string DisplayName {
            get {
                return(this.txtDisplayName.Text);
            }
        }

        public int ColumnWidth {
            get {
                int intTemp;
                
                if(int.TryParse(this.txtColumnWidth.Text, out intTemp)) {
                    return(intTemp);
                } else {
                    return(0);
                }
            }
        }

        public string Format {
            get {
                return(this.txtFormat.Text);
            }
        }

        public JustifyEnum Justification {
            get {
               if(this.cboJustification.Text == "Left") {
                    return(JustifyEnum.Left);
                } else {
                    return(JustifyEnum.Right);
                }
            }
        }

        public char PadChar {
            get {
                if(this.txtPadChar.Text.ToUpper() == "SP" || this.txtPadChar.Text.Length == 0) {
                    return(' ');
                } else {
                    return(this.txtPadChar.Text[0]);
                }
            }
        }

        public bool Quotes {
            get {
                return(this.chkQuotes.Checked);
            }
        }
        
        public void LoadGridItem(int vIndex, cGridItem GridItem) {

            _Index = vIndex;

            this.lblFieldName.Text = GridItem.FieldName;
            this.txtDisplayName.Text = GridItem.DisplayName;
            this.txtColumnWidth.Text = GridItem.ColumnWidth.ToString();
            this.txtFormat.Text = GridItem.Format;

            if(GridItem.Justification == JustifyEnum.Left) {
                this.cboJustification.SelectedIndex = 0;
            } else {
                this.cboJustification.SelectedIndex = 1;
            }

            if(GridItem.PadChar == ' ') {
                this.txtPadChar.Text = "SP";
            } else {
                this.txtPadChar.Text = Convert.ToString(GridItem.PadChar);
            }
            this.chkQuotes.Checked = GridItem.Quotes;
        }
        
        public bool IsDirty {
            get {
                return(_IsDirty);
            }
        }

        private void txtPadChar_Validating(object sender, CancelEventArgs e) {

            if(this.txtPadChar.Text.Length == 0) {
                this.txtPadChar.Text = "SP";
            } else if(this.txtPadChar.Text.Length > 1) {
                if(this.txtPadChar.Text.ToUpper() == "SP") {
                    this.txtPadChar.Text = "SP";
                } else {
                    e.Cancel = true;
                }
            }
        }

        private void txtColumnWidth_Validating(object sender, CancelEventArgs e) {

            int intTemp;
            
            if(!int.TryParse(this.txtColumnWidth.Text, out intTemp)) {
                e.Cancel = true;
            }
        }

        private void txtFormat_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void chkQuotes_CheckedChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void cboJustification_SelectedIndexChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtPadChar_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtColumnWidth_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtDisplayName_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }
    }
}
