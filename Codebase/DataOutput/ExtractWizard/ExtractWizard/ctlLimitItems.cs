using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal partial class ctlLimitItems : UserControl {

        private List<cColumn>[] _Columns = null;

        public event ItemDroppedEventHandler ItemDropped;
        public event ItemMovedEventHandler ItemMoved;
        public event ItemDeletedEventHandler ItemDeleted;

        public ctlLimitItems(
            List<cLimitItem> limitItems, 
            List<cColumn>[] columns) {

            InitializeComponent();

            this.ctlDataGrid1.AddColumnsRange(
                new System.Windows.Forms.DataGridViewColumn[] {
                    this.colFieldName,
                    this.colDisplayName,
                    this.colLogicalOperator,
                    this.colValue,
                    this.colLeftParen,
                    this.colRightParen,
                    this.colCompoundOperator
                });

            LoadLimitItems(limitItems, columns);
        }

        public void LoadLimitItems(
            List<cLimitItem> limitItems, 
            List<cColumn>[] columns) {

            this.ctlDataGrid1.SuspendEvents();;

            try {
                        
                this.ctlDataGrid1.Clear();
           
                _Columns = columns;

                foreach(cLimitItem limititem in limitItems) {
                    AddLimitItem(limititem);            
                }           

                RefreshWhereClauseText(limitItems);

                this.ctlDataGrid1.ResumeEvents();

            } catch(Exception ex) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                this.ctlDataGrid1.ResumeEvents();
                throw(ex);
            }
        }
        
        internal void AddLimitItem(cLimitItem limitItem) {

            DataGridViewCell[] cellArray;
            DataGridViewTextBoxCell dgText;
            DataGridViewComboBoxCell dgCombo;
            DataGridViewRow dgRow;
            SqlDbType sqlDataType;

            cellArray = new DataGridViewCell[7];           

            // Field Name
            dgText = new DataGridViewTextBoxCell();
            dgText.Value = limitItem.FieldName;
            dgText.MaxInputLength = 100;
            cellArray[0] = dgText;

            dgText = new DataGridViewTextBoxCell();
            dgText.Value = limitItem.DisplayName;
            dgText.MaxInputLength = 100;
            cellArray[1] = dgText;

            // Logical Operator
            dgCombo = new DataGridViewComboBoxCell();

            var type = (int)FindDataType(limitItem);

            sqlDataType = (SqlDbType)type;

            dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Equals));
            dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.NotEqualTo));

            if(sqlDataType == SqlDbType.BigInt ||
               sqlDataType == SqlDbType.Bit ||
               sqlDataType == SqlDbType.Decimal ||
               sqlDataType == SqlDbType.Float ||
               sqlDataType == SqlDbType.Int ||
               sqlDataType == SqlDbType.Money ||
               sqlDataType == SqlDbType.Real ||
               sqlDataType == SqlDbType.SmallInt ||
               sqlDataType == SqlDbType.SmallMoney ||
               sqlDataType == SqlDbType.TinyInt ||
               sqlDataType == SqlDbType.DateTime ||
               sqlDataType == SqlDbType.Timestamp) {

                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.GreaterThan));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.GreaterThanOrEqualTo));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.LessThan));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.LessThanOrEqualTo));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Between));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.In));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNull));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNotNull));

            } else {
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.In));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNull));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNotNull));
                dgCombo.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Like));
            }

            if(dgCombo.Items.Contains(cExtractDef.DecodeLogicalOperator(limitItem.LogicalOperator))) {
                dgCombo.Value = cExtractDef.DecodeLogicalOperator(limitItem.LogicalOperator);
            } else {
                dgCombo.Value = string.Empty;
            }
            
            cellArray[2] = dgCombo;

            // Value
            dgText = new DataGridViewTextBoxCell();
            dgText.Value = limitItem.Value;
            dgText.MaxInputLength = 100;
            cellArray[3] = dgText;

            // Left Paren Count
            dgText = new DataGridViewTextBoxCell();
            dgText.Value = limitItem.LeftParenCount.ToString();
            dgText.MaxInputLength = 3;
            cellArray[4] = dgText;

            // Right Paren Count
            dgText = new DataGridViewTextBoxCell();
            dgText.Value = limitItem.RightParenCount.ToString();
            dgText.MaxInputLength = 3;
            cellArray[5] = dgText;

            // Compound Operator
            dgCombo = new DataGridViewComboBoxCell();
            dgCombo.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.None));
            dgCombo.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.And));
            dgCombo.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.AndNot));
            dgCombo.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.Or));
            dgCombo.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.OrNot));

            if(limitItem.CompoundOperator != CompoundOperatorEnum.None &&
               dgCombo.Items.Contains(cExtractDef.DecodeCompoundOperator(limitItem.CompoundOperator))) {
                dgCombo.Value = cExtractDef.DecodeCompoundOperator(limitItem.CompoundOperator);
            } else {
                dgCombo.Value = string.Empty;
            }
            cellArray[6] = dgCombo;

            // Add Row to DataGridView
            dgRow = new DataGridViewRow();
            dgRow.Cells.AddRange(cellArray);

            // Lock any cells that should not be editable.
            if(limitItem.LogicalOperator == LogicalOperatorEnum.IsNull || limitItem.LogicalOperator == LogicalOperatorEnum.IsNotNull) {
                dgRow.Cells[3].Value = string.Empty;
                dgRow.Cells[3].ReadOnly = true;
            }

            this.ctlDataGrid1.AddRow(dgRow);
        }

        internal void RemoveAt(int index) {
            this.ctlDataGrid1.RemoveAt(index);
        }

        private void ctlDataGrid1_ItemDeleted(int itemIndex) {

            if(ItemDeleted != null) {
                ItemDeleted(itemIndex);
            }
        }

        private void ctlDataGrid1_ItemDropped(string itemText) {

            if(ItemDropped != null) {
                ItemDropped(itemText);
            }
        }

        private void ctlDataGrid1_ItemMoved(
            int currentIndex, 
            int newIndex) {

            if(ItemMoved != null) {
                ItemMoved(
                    currentIndex, 
                    newIndex);
            }
        }

        private void ctlDataGrid1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {

            int intTemp;
    
            switch(e.ColumnIndex) {
                case 0:                   
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    if((e.FormattedValue.ToString().Length > 0) && (!int.TryParse(e.FormattedValue.ToString(), out intTemp) && intTemp > -1)) {
                        e.Cancel = true;
                    }
                    break;
                case 5:
                    if((e.FormattedValue.ToString().Length > 0) && (!int.TryParse(e.FormattedValue.ToString(), out intTemp) && intTemp > -1)) {
                        e.Cancel = true;
                    }
                    break;
                case 6:
                    break;
                default:
                    break;
            }
        }

        private void ctlDataGrid1_CellEndEdit(object sender, DataGridViewCellEventArgs e) {

            if(this.ParentForm != null &&
               ((frmMain)this.ParentForm).tvLimitData.SelectedNode != null && 
               ((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag != null && 
               ((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag is List<cLimitItem>) {

                switch(e.ColumnIndex) {
                    case 2:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Between)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.Between;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Equals)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.Equals;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.GreaterThan)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.GreaterThan;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.GreaterThanOrEqualTo)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.GreaterThanOrEqualTo;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.In)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.In;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNotNull)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.IsNotNull;
                            this.ctlDataGrid1[2, e.RowIndex].Value = string.Empty;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = true;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNull)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.IsNull;
                            this.ctlDataGrid1[2, e.RowIndex].Value = string.Empty;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = true;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.LessThan)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.LessThan;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.LessThanOrEqualTo)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.LessThanOrEqualTo;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Like)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.Like;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.NotEqualTo)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LogicalOperator = LogicalOperatorEnum.NotEqualTo;
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        } else {
                            this.ctlDataGrid1[2, e.RowIndex].ReadOnly = false;
                        }
                        break;
                    case 3:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].Value = string.Empty;
                        } else {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].Value = this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString();
                        }
                        break;
                    case 4:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LeftParenCount = 0;
                        } else {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].LeftParenCount = this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString().Length > 0 ? int.Parse(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString()) : 0;
                        }
                        break;
                    case 5:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].RightParenCount = 0;
                        } else {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].RightParenCount = this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString().Length > 0 ? int.Parse(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString()) : 0;
                        }
                        break;
                    case 6:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].CompoundOperator = CompoundOperatorEnum.None;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == string.Empty) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].CompoundOperator = CompoundOperatorEnum.None;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.None)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].CompoundOperator = CompoundOperatorEnum.None;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.And)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].CompoundOperator = CompoundOperatorEnum.And;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.AndNot)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].CompoundOperator = CompoundOperatorEnum.AndNot;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.Or)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].CompoundOperator = CompoundOperatorEnum.Or;
                        } else if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.OrNot)) {
                            ((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag)[e.RowIndex].CompoundOperator = CompoundOperatorEnum.OrNot;
                        }
                        break;
                }

                RefreshWhereClauseText((List<cLimitItem>)((frmMain)this.ParentForm).tvLimitData.SelectedNode.Tag);
            }
        }

        private SqlDbType FindDataType(cLimitItem item) {

            SqlDbType sqlRetVal = SqlDbType.Text;

            // For data entry, we already have the type.
            if (item.Table.ToLower().Contains("dataentry"))
            {
                return item.DataType == 6 ? SqlDbType.Float
                    : item.DataType == 7 ? SqlDbType.Money
                    : item.DataType == 11 ? SqlDbType.DateTime
                    : SqlDbType.Text;
            }

            for(int i=0;i<_Columns.Length;++i) {
                foreach(cColumn column in _Columns[i]) {
                    if($"{column.TableName}.{column.ColumnName}" == $"{item.Table}.${item.FieldName}") {
                        sqlRetVal = column.Type;
                        break;
                    }
                }
            }
            
            return(sqlRetVal);
        }

        internal void RefreshWhereClauseText(List<cLimitItem> limitItems) {

            StringBuilder sbWhereClause;
            bool bolIsFirstItem;
            SqlDbType sqlDataType;
            bool bolUseQuotes;

            this.txtWhereClause.Text = string.Empty;

            sbWhereClause = new StringBuilder();

            bolIsFirstItem = true;

            foreach(cLimitItem LimitItem in limitItems) {

                if(bolIsFirstItem) {
                    bolIsFirstItem = false;
                } else {
                    sbWhereClause.Append("\r\n");
                }

                if(LimitItem.LeftParenCount > 0) {
                    for(int i=0;i<LimitItem.LeftParenCount;++i) {
                        sbWhereClause.Append("(");
                    }
                }

                sbWhereClause.Append(LimitItem.FieldName);
                sbWhereClause.Append(" ");
                sbWhereClause.Append(cExtractDef.DecodeLogicalOperator(LimitItem.LogicalOperator));
                sbWhereClause.Append(" ");

                sqlDataType = FindDataType(LimitItem);

                bolUseQuotes = !(sqlDataType == SqlDbType.BigInt ||
                                 sqlDataType == SqlDbType.Bit ||
                                 sqlDataType == SqlDbType.Decimal ||
                                 sqlDataType == SqlDbType.Float ||
                                 sqlDataType == SqlDbType.Int ||
                                 sqlDataType == SqlDbType.Money ||
                                 sqlDataType == SqlDbType.Real ||
                                 sqlDataType == SqlDbType.SmallInt ||
                                 sqlDataType == SqlDbType.SmallMoney ||
                                 sqlDataType == SqlDbType.TinyInt);

                if(bolUseQuotes) {
                    sbWhereClause.Append("'");
                }

                sbWhereClause.Append(LimitItem.Value);

                if(bolUseQuotes) {
                    sbWhereClause.Append("'");
                }

                if(LimitItem.RightParenCount > 0) {
                    for(int i=0;i<LimitItem.RightParenCount;++i) {
                        sbWhereClause.Append(")");
                    }
                }

                if(LimitItem.CompoundOperator != CompoundOperatorEnum.None) {
                    sbWhereClause.Append(" ");
                    sbWhereClause.Append(cExtractDef.DecodeCompoundOperator(LimitItem.CompoundOperator));
                }
            }

            this.txtWhereClause.Text = sbWhereClause.ToString();
        }

        private void ctlLimitItems_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}
