using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    public partial class frmFormatCurrencyDialog : Form {

        private string _Value = string.Empty;

        public frmFormatCurrencyDialog(string selectedFormat) {
        
            bool bolOverpunchPosAndZero;
            bool bolOverpunchNeg;
        
            InitializeComponent();

            bolOverpunchPosAndZero = (selectedFormat.IndexOf('{') > 0);
            bolOverpunchNeg = (selectedFormat.IndexOf('}') > 0);

            this.txtSelectedFormat.Text = selectedFormat;
            this.txtTestValue.Text = "12345.67";

            this.chkOverpunchPosAndZero.Checked = bolOverpunchPosAndZero;
            this.chkOverpunchNeg.Checked = bolOverpunchNeg;

            foreach(string str in this.lstFormat.Items) {
                this.lstExample.Items.Add(ExtractAPI.CustomFormat.FormatValue(
                    (decimal)12345.67, 
                    str, 
                    bolOverpunchPosAndZero, 
                    bolOverpunchNeg, 
                    0, 
                    ' ', 
                    JustifyEnum.Right, 
                    false));
            }
        }

        public string Value {
            get {
                return(_Value);
            }
        }

        private void btnOk_Click(object sender, EventArgs e) {
            _Value = this.txtSelectedFormat.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            _Value = string.Empty;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnTest_Click(object sender, EventArgs e) {

            decimal decTemp;

            if(decimal.TryParse(this.txtTestValue.Text, out decTemp)) {
                this.txtTestResult.Text = ExtractAPI.CustomFormat.FormatValue(
                    decTemp, 
                    this.txtSelectedFormat.Text, 
                    this.chkOverpunchPosAndZero.Checked, 
                    this.chkOverpunchNeg.Checked,
                    0,
                    ' ', 
                    JustifyEnum.Right,
                    false);
            }
        }

        private void chkOverpunchPosAndZero_CheckedChanged(object sender, EventArgs e) {
            FormatOverPunch();
        }

        private void chkOverpunchNeg_CheckedChanged(object sender, EventArgs e) {
            FormatOverPunch();
        }

        private void FormatOverPunch() {

            while(this.txtSelectedFormat.Text.EndsWith("{") || this.txtSelectedFormat.Text.EndsWith("}")) {
                this.txtSelectedFormat.Text = this.txtSelectedFormat.Text.Substring(0, this.txtSelectedFormat.Text.Length -1);
            }
            if(this.chkOverpunchPosAndZero.Checked) {
                this.txtSelectedFormat.Text += "{";
            }
            if(this.chkOverpunchNeg.Checked) {
                this.txtSelectedFormat.Text += "}";
            }
        }

        private void lstFormat_SelectedIndexChanged(object sender, EventArgs e) {
            this.lstExample.SelectedIndex = this.lstFormat.SelectedIndex;
            this.txtSelectedFormat.Text = this.lstFormat.SelectedItem.ToString();
            FormatOverPunch();
        }

        private void lstExample_SelectedIndexChanged(object sender, EventArgs e) {
            this.lstFormat.SelectedIndex = this.lstExample.SelectedIndex;
        }

        private void lstExample_DoubleClick(object sender, EventArgs e) {
            _Value = this.txtSelectedFormat.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void lstFormat_DoubleClick(object sender, EventArgs e) {
            _Value = this.txtSelectedFormat.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmFormatCurrencyDialog_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}