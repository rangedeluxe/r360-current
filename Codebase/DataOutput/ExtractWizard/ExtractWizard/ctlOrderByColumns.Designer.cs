using System.Windows.Forms;

/*******************************************************************************
*
*    Module: ctlOrderByColumns
*  Filename: ctlOrderByColumns.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class ctlOrderByColumns {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.ctlDataGrid1 = new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ctlDataGrid();
            this.colFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFieldFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDirection = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.SuspendLayout();
            // 
            // ctlDataGrid1
            // 
            this.ctlDataGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlDataGrid1.Location = new System.Drawing.Point(0, 0);
            this.ctlDataGrid1.Margin = new System.Windows.Forms.Padding(5);
            this.ctlDataGrid1.Name = "ctlDataGrid1";
            this.ctlDataGrid1.Size = new System.Drawing.Size(987, 421);
            this.ctlDataGrid1.TabIndex = 0;
            this.ctlDataGrid1.ItemDropped += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemDroppedEventHandler(this.ctlDataGrid1_ItemDropped);
            this.ctlDataGrid1.ItemMoved += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemMovedEventHandler(this.ctlDataGrid1_ItemMoved);
            this.ctlDataGrid1.ItemDeleted += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemDeletedEventHandler(this.ctlDataGrid1_ItemDeleted);
            this.ctlDataGrid1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlDataGrid1_CellEndEdit);
            // 
            // colFieldName
            // 
            this.colFieldName.HeaderText = "Field Name";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.ReadOnly = true;
            this.colFieldName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldName.Width = 350;
            // 
            // colFieldFullName
            // 
            this.colFieldFullName.HeaderText = "Display Name";
            this.colFieldFullName.Name = "colFieldFullName";
            this.colFieldFullName.ReadOnly = true;
            this.colFieldFullName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldFullName.Width = 350;
            // 
            // colDirection
            // 
            this.colDirection.HeaderText = "Asc/Desc";
            this.colDirection.Name = "colDirection";
            // 
            // ctlOrderByColumns
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.ctlDataGrid1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ctlOrderByColumns";
            this.Size = new System.Drawing.Size(989, 427);
            this.Load += new System.EventHandler(this.ctlOrderByColumns_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldFullName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colDirection;
        private ctlDataGrid ctlDataGrid1;
    }
}
