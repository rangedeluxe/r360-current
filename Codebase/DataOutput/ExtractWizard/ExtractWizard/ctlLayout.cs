using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractRun;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 129562 BLR 02/13/2014
*   - Updated hard strings to constant strings for Joint Details. 
* WI 131640 BLR 03/04/2014, DLD 03/05/2014  
*   - Added in additional exception logging.
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
* WI 115856 BLR 06/24/2014
*   - Readonly fields for DataEntry fields, we need the DisplayName to 
*     remain consistent for SQL.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal partial class ctlLayout : UserControl {

        private bool _IsDirty = false;
        private int _Index;

        public event ItemDroppedEventHandler ItemDropped;
        public event ItemMovedEventHandler ItemMoved;
        public event ItemDeletedEventHandler ItemDeleted;

        public ctlLayout(
            int index, 
            cLayout layout) {

            InitializeComponent();

            this.ctlDataGrid1.AddColumnsRange(
                new System.Windows.Forms.DataGridViewColumn[] {
                this.colFieldName,
                this.colDisplayName,
                this.colColumnWidth,
                this.colPadChar,
                this.colJustify,
                this.colQuotes,
                this.colFormat,
                this.colOccursGroup,
                this.colOccursCount});
            
            LoadLayout(index, layout);
        }

        public int Index {
            get {
                return(_Index);
            }
        }

        public LayoutTypeEnum LayoutType {
            get {
                if(this.rdoFileRecordTypeTrailer.Visible && this.rdoFileRecordTypeTrailer.Checked) {
                    return(LayoutTypeEnum.Trailer);
                } else {
                    return(LayoutTypeEnum.Header);
                }
            }
        }

        public bool UseOccursGroups {
            get {
                return(this.chkUseOccursGroups.Checked);
            }
        }

        public bool UsePaymentsOccursGroups {
            get {
                return(this.chkUsePaymentsOccursGroups.Checked);
            }
        }

        public bool UseStubsOccursGroups {
            get {
                return(this.chkUseStubsOccursGroups.Checked);
            }
        }

        public bool UseDocumentsOccursGroups {
            get {
                return(this.chkUseDocumentsOccursGroups.Checked);
            }
        }

        public int PaymentsOccursCount {
            get {
                int intTemp;
                
                if(chkUseOccursGroups.Checked && this.chkUsePaymentsOccursGroups.Checked && 
                   int.TryParse(this.txtPaymentsOccursCount.Text, out intTemp)) {

                    return(intTemp);

                } else {
                    return(0);
                }
            }
        }

        public int StubsOccursCount {
            get {
                int intTemp;
                
                if(chkUseOccursGroups.Checked && this.chkUseStubsOccursGroups.Checked && 
                   int.TryParse(this.txtStubsOccursCount.Text, out intTemp)) {

                    return(intTemp);

                } else {
                    return(0);
                }
            }
        }

        public int DocumentsOccursCount {
            get {
                int intTemp;
                
                if(chkUseOccursGroups.Checked && this.chkUseDocumentsOccursGroups.Checked && 
                   int.TryParse(this.txtDocumentsOccursCount.Text, out intTemp)) {

                    return(intTemp);

                } else {
                    return(0);
                }
            }
        }

        public int MaxRepeats {
            get {
            
                int intTemp;
            
                if(this.txtMaxRepeats.Visible && int.TryParse(this.txtMaxRepeats.Text, out intTemp)) {
                    return(intTemp);
                } else {
                    return(0);
                }
            }
        }

        public bool IsDirty {
            get {
                return(_IsDirty);
            }
        }
        
        public void LoadLayout(
            int index, 
            cLayout layout) {

            _Index = index;
            _IsDirty = false;

            this.ctlDataGrid1.SuspendEvents();

            try {

                this.ctlDataGrid1.Clear();

                this.txtLayoutLevel.Text = cExtractDef.DecodeLayoutLevel(layout.LayoutLevel);

                if(layout.LayoutLevel == LayoutLevelEnum.JointDetail) {

                    this.chkUseOccursGroups.Visible = true;

                    this.chkUseOccursGroups.Checked = layout.UseOccursGroups;
                    
                    ToggleOccursGroupsControls();

                    this.txtPaymentsOccursCount.Text = (layout.PaymentsOccursCount > 0 ? layout.PaymentsOccursCount.ToString() : string.Empty);
                    this.txtStubsOccursCount.Text = (layout.StubsOccursCount > 0 ? layout.StubsOccursCount.ToString() : string.Empty);
                    this.txtDocumentsOccursCount.Text = (layout.DocumentsOccursCount > 0 ? layout.DocumentsOccursCount.ToString() : string.Empty);

                    this.txtPaymentsOccursCount.Enabled = layout.UsePaymentsOccursGroups;
                    this.txtStubsOccursCount.Enabled = layout.UseStubsOccursGroups;
                    this.txtDocumentsOccursCount.Enabled = layout.UseDocumentsOccursGroups;

                    this.chkUsePaymentsOccursGroups.Checked = layout.UsePaymentsOccursGroups;
                    this.chkUseStubsOccursGroups.Checked = layout.UseStubsOccursGroups;
                    this.chkUseDocumentsOccursGroups.Checked = layout.UseDocumentsOccursGroups;

                    this.grpHeaderTrailer.Visible = false;
                } else {

                    this.chkUseOccursGroups.Visible = false;

                    ToggleOccursGroupsControls();

                    if(layout.LayoutLevel == LayoutLevelEnum.Payment ||
                       layout.LayoutLevel == LayoutLevelEnum.Stub ||
                       layout.LayoutLevel == LayoutLevelEnum.Document) {
                       
                       this.grpHeaderTrailer.Visible = false;
                    } else {
                        this.grpHeaderTrailer.Visible = true;

                        if(layout.LayoutType == LayoutTypeEnum.Header) {
                            this.rdoFileRecordTypeHeader.Checked = true;
                        } else {
                            this.rdoFileRecordTypeTrailer.Checked = true;
                        }
                    }
                }

                if(layout.MaxRepeats > 0) {
                    this.txtMaxRepeats.Text = layout.MaxRepeats.ToString();
                } else {
                    this.txtMaxRepeats.Text = string.Empty;
                }

                foreach(cLayoutField layoutfield in layout.LayoutFields) {
                    AddLayoutField(layoutfield);            
                }

                this.ctlDataGrid1.ResumeEvents();

            } catch(Exception ex) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                this.ctlDataGrid1.ResumeEvents();
                throw(ex);
            }
        }
        
        private void ToggleOccursGroupsControls() {
            if(this.chkUseOccursGroups.Visible && this.chkUseOccursGroups.Checked) {
                if(!this.grpOccursGroupOptions.Visible) {
                    this.ctlDataGrid1.Width = this.ctlDataGrid1.Width - this.grpOccursGroupOptions.Width - 7;
                    this.grpOccursGroupOptions.Visible = true;
                }
            } else {
                if(this.grpOccursGroupOptions.Visible) {
                    this.grpOccursGroupOptions.Visible = false;
                    this.ctlDataGrid1.Width = this.ctlDataGrid1.Width + this.grpOccursGroupOptions.Width + 7;
                }
            }
        }
        
        internal void AddLayoutField(cLayoutField layoutField) {

            DataGridViewCell[] cellArray;
            DataGridViewTextBoxCell dgText;
            DataGridViewComboBoxCell dgCombo;
            DataGridViewCheckBoxCell dgCheck;
            DataGridViewTextButtonCell dgTextButton;
            DataGridViewRow dgRow;

            cellArray = new DataGridViewCell[7];           

            dgText = new DataGridViewTextBoxCell();
            dgText.Value = layoutField.FieldName;
            dgText.MaxInputLength = 100;
            cellArray[0] = dgText;

            dgText = new DataGridViewTextBoxCell();
            var tmpdisplayname = dgText;
            dgText.Value = layoutField.DisplayName;
            dgText.MaxInputLength = 100;
            cellArray[1] = dgText;

            dgText = new DataGridViewTextBoxCell();
            dgText.Value = layoutField.ColumnWidth.ToString();
            dgText.MaxInputLength = 5;
            cellArray[2] = dgText;

            dgText = new DataGridViewTextBoxCell();
            if(layoutField.PadChar == ' ') {
                dgText.Value = "SP";
            } else {
                dgText.Value = Convert.ToString(layoutField.PadChar);
            }
            dgText.MaxInputLength = 2;
            cellArray[3] = dgText;

            dgCombo = new DataGridViewComboBoxCell();
            dgCombo.Items.Add("Left");
            dgCombo.Items.Add("Right");

            if(layoutField.Justification == JustifyEnum.Left) {
                dgCombo.Value = "Left";
            } else {
                dgCombo.Value = "Right";
            }
            cellArray[4] = dgCombo;

            dgCheck = new DataGridViewCheckBoxCell();
            dgCheck.Value = layoutField.Quotes;
            cellArray[5] = dgCheck;

            if(layoutField.IsStatic || layoutField.IsFiller) {
                dgText = new DataGridViewTextBoxCell();
                dgText.Value = layoutField.Format;
                cellArray[6] = dgText;
            } else {
                dgTextButton = new DataGridViewTextButtonCell();
                dgTextButton.Value = layoutField.Format;
                cellArray[6] = dgTextButton;
            }

            dgRow = new DataGridViewRow();
            dgRow.Tag = layoutField;
            dgRow.Cells.AddRange(cellArray);
            this.ctlDataGrid1.Rows.Add(dgRow);
            tmpdisplayname.ReadOnly = layoutField.IsDataEntry;
        }

        internal void RemoveAt(int Index) {
            this.ctlDataGrid1.RemoveAt(Index);
        }

        private void ctlDataGrid1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) {
            if(e.Control != null && e.Control is DataGridViewTextButton) {

                // Need to remove the event handler before we add it back.  This prevents the event from being fired
                // multiple times when the DataGridView reuses the control.
                // http://msdn2.microsoft.com/en-us/library/system.windows.forms.datagridviewcomboboxeditingcontrol.aspx
                // http://msdn2.microsoft.com/en-us/library/system.windows.forms.datagridview.editingcontrolshowing.aspx
                ((DataGridViewTextButton)e.Control).ButtonClick -= new EventHandler(DataGridViewTextButtonCell_ButtonClick);

                ((DataGridViewTextButton)e.Control).ButtonClick += new EventHandler(DataGridViewTextButtonCell_ButtonClick);
            }
        }
        
        private void DataGridViewTextButtonCell_ButtonClick(object sender, EventArgs e) {
            
            TFieldType enmFieldType;
            frmFormatDateDialog objFormatDateDialog;
            frmFormatAlphaNumericDialog objFormatAlphaNumericDialog;
            frmFormatCurrencyDialog objFormatCurrencyDialog;
            string strFormat = string.Empty;

            if(LegacySupport.TryParse(((cLayoutField)this.ctlDataGrid1.SelectedRow.Tag).DataType, out enmFieldType)) {
            
                if(enmFieldType == TFieldType.DATE ||
                   enmFieldType == TFieldType.TIME ||
                   enmFieldType == TFieldType.TIMESTAMP ||
                   enmFieldType == TFieldType.DATETIME) {

                    objFormatDateDialog = new frmFormatDateDialog(((DataGridViewTextButton)sender).Value);
                    if(objFormatDateDialog.ShowDialog() == DialogResult.OK) {
                        ((DataGridViewTextButton)sender).Value = objFormatDateDialog.Value;
	                }

                } else if(enmFieldType == TFieldType.FLOAT ||
                          enmFieldType == TFieldType.MONEY ||
                          enmFieldType == TFieldType.BCD) {

                    objFormatCurrencyDialog = new frmFormatCurrencyDialog(((DataGridViewTextButton)sender).Value);
                    if(objFormatCurrencyDialog.ShowDialog() == DialogResult.OK) {
                        ((DataGridViewTextButton)sender).Value = objFormatCurrencyDialog.Value;
	                }

                } else {

                    objFormatAlphaNumericDialog = new frmFormatAlphaNumericDialog(((DataGridViewTextButton)sender).Value);
                    if(objFormatAlphaNumericDialog.ShowDialog() == DialogResult.OK) {
                        ((DataGridViewTextButton)sender).Value = objFormatAlphaNumericDialog.Value;
	                }
                }

            } else {
                objFormatAlphaNumericDialog = new frmFormatAlphaNumericDialog(((DataGridViewTextButton)sender).Value);
                if(objFormatAlphaNumericDialog.ShowDialog() == DialogResult.OK) {
                    ((DataGridViewTextButton)sender).Value = objFormatAlphaNumericDialog.Value;
		        }
            }
        }

        public void AskToReconcileIfNecessary() {
            ////if(JointDetailReconciliationRequired && 
            ////   MessageBox.Show("Joint Detail Layout Items must be contiguous at the detail level.  Would you like to reconcile these items now?", "Reconciliation required", MessageBoxButtons.YesNo) == DialogResult.Yes) {
            ////    ReconcileJointDetailRowPositions();
            ////}
        }

        private bool JointDetailReconciliationRequired {

            get {

                int intChecksIndex = -1;
                int intStubsIndex = -1;
                int intDocsIndex = -1;
                
                bool bolRetVal = false;
                
                if(this.ctlDataGrid1.Rows.Count > 0) {

                    for(int i=0;i<this.ctlDataGrid1.Rows.Count;++i) {
                        if(this.ctlDataGrid1[0, i].Value != null) {
                            // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                            if(this.ctlDataGrid1[0, i].Value.ToString().ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER) ||
                               this.ctlDataGrid1[0, i].Value.ToString().ToLower().StartsWith("checksdataentry.")) {
                                if(intChecksIndex == -1 || intChecksIndex == i - 1) {
                                    intChecksIndex = i;
                                } else if(this.chkUsePaymentsOccursGroups.Checked) {
                                    bolRetVal = true;
                                    break;
                                }
                            }
                            // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                            else if(this.ctlDataGrid1[0, i].Value.ToString().ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER) ||
                                      this.ctlDataGrid1[0, i].Value.ToString().ToLower().StartsWith("stubsdataentry.")) {
                                if(intStubsIndex == -1 || intStubsIndex == i - 1) {
                                    intStubsIndex = i;
                                } else if(this.chkUseStubsOccursGroups.Checked) {
                                    bolRetVal = true;
                                    break;
                                }
                            }
                            // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                            else if(this.ctlDataGrid1[0, i].Value.ToString().ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER)) {
                                if(intDocsIndex == -1 || intDocsIndex == i - 1) {
                                    intDocsIndex = i;
                                } else if(this.chkUseDocumentsOccursGroups.Checked) {
                                    bolRetVal = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                return(bolRetVal);

            }
        }

        private void ReconcileJointDetailRowPositions() {

            int intCurrentIndex;
            bool bolKeepGoing = true;

            while(bolKeepGoing) {

                bolKeepGoing = false;

                for(int i=0;i<=2;++i) {
                
                    intCurrentIndex = -1;
                
                    for(int j=this.ctlDataGrid1.Rows.Count -1;j>=0;--j) {
                        switch(i) {
                            case 0:
                                // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                                if (this.ctlDataGrid1[0, j].Value.ToString().ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER) ||
                                   this.ctlDataGrid1[0, j].Value.ToString().ToLower().StartsWith("checksdataentry.")) {
                                    if(intCurrentIndex == -1) {
                                        intCurrentIndex = j;
                                    } else {
                                        if(j < intCurrentIndex -1) {
                                            this.ctlDataGrid1.MoveItem(intCurrentIndex, j + 1);
                                            intCurrentIndex = -1;
                                            bolKeepGoing = true;
                                            break;
                                        } else {
                                            intCurrentIndex = j;
                                        }
                                    }
                                }
                                break;
                            case 1:
                                // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                                if (this.ctlDataGrid1[0, j].Value.ToString().ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER) ||
                                   this.ctlDataGrid1[0, j].Value.ToString().ToLower().StartsWith("stubsdataentry.")) {
                                    if(intCurrentIndex == -1) {
                                        intCurrentIndex = j;
                                    } else {
                                        if(j < intCurrentIndex -1) {
                                            this.ctlDataGrid1.MoveItem(intCurrentIndex, j + 1);
                                            intCurrentIndex = -1;
                                            bolKeepGoing = true;
                                            break;
                                        } else {
                                            intCurrentIndex = j;
                                        }
                                    }
                                }
                                break;
                            case 2:
                                // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                                if (this.ctlDataGrid1[0, j].Value.ToString().ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER))
                                {
                                    if(intCurrentIndex == -1) {
                                        intCurrentIndex = j;
                                    } else {
                                        if(j < intCurrentIndex -1) {
                                            this.ctlDataGrid1.MoveItem(intCurrentIndex, j + 1);
                                            intCurrentIndex = -1;
                                            bolKeepGoing = true;
                                            break;
                                        } else {
                                            intCurrentIndex = j;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }

        private void ctlDataGrid1_ItemDeleted(int itemIndex) {

            if(ItemDeleted != null) {
                ItemDeleted(itemIndex);
            }
        }

        private void ctlDataGrid1_ItemDropped(string itemText) {

            if(ItemDropped != null) {
                ItemDropped(itemText);
            }
        }

        private void ctlDataGrid1_ItemMoved(
            int currentIndex, 
            int newIndex) {

            if(ItemMoved != null) {
                ItemMoved(currentIndex, newIndex);
            }
        }

        private void ctlDataGrid1_CellEndEdit(object sender, DataGridViewCellEventArgs e) {

            if(e.RowIndex > -1 && e.ColumnIndex == 3) {
                if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null ||
                   this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString().Trim().ToLower() == "sp"||
                   this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString().Trim().Length == 0) {
                   this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value = "SP";
                }
            }

            if(this.ParentForm != null &&
               ((frmMain)this.ParentForm).tvLayouts.SelectedNode != null && 
               ((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag != null && 
               ((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag is cLayout) {

                switch(e.ColumnIndex) {
                    case 1:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null) {
                            ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].DisplayName = string.Empty;
                        } else {
                            ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].DisplayName = this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString();
                        }
                        break;
                    case 2:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null) {
                            ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].ColumnWidth = 0;
                        } else {
                            ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].ColumnWidth = int.Parse(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString());
                        }
                        break;
                    case 3:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null) {
                            ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].PadChar = ' ';
                        } else {
                            ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].PadChar = this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == "SP" ? ' ' : (this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() + " ")[0];
                        }
                        break;
                    case 4:
                        ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].Justification = (this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString() == "Left") ? JustifyEnum.Left : JustifyEnum.Right;
                        break;
                    case 5:
                        ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].Quotes = (bool)this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value;
                        break;
                    case 6:
                        if(this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value == null) {
                            ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].Format = string.Empty;
                        } else {
                            ((cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag).LayoutFields[e.RowIndex].Format = this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value.ToString();
                        }
                        break;
                }
            }
        }

        private void ctlDataGrid1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {

            int intTemp;
    
            switch(e.ColumnIndex) {
                case 0:                   
                    break;
                case 1:
                    break;
                case 2:
                    if(!(int.TryParse(e.FormattedValue.ToString(), out intTemp) && intTemp >= 0)) {
                        e.Cancel = true;
                    }
                    break;
                case 3:
                    if(e.FormattedValue.ToString().Trim().Length == 0) {
                        this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value = "SP";
                    } else if(e.FormattedValue.ToString().Length > 1 && e.FormattedValue.ToString().Trim().ToLower() != "sp") {
                        e.Cancel = true;
                    } else if(e.FormattedValue.ToString().Trim().ToLower() == "sp") {
                        this.ctlDataGrid1[e.ColumnIndex, e.RowIndex].Value = "SP";
                    }
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                default:
                    break;
            }
        }

        private void rdoFileRecordTypeTrailer_CheckedChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void rdoFileRecordTypeHeader_CheckedChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtMaxRepeats_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void btnTestLayout_Click(object sender, EventArgs e) {

            cExtract objExtract;
            string strTestExtract;
            frmDisplayText objText;

            ((frmMain)this.ParentForm).SynchCurrentNodes();

            objExtract = new cExtract();

            if(objExtract.BuildTestLayout(((frmMain)this.ParentForm).GetExtractDefFromForm(), 
                                          (cLayout)((frmMain)this.ParentForm).tvLayouts.SelectedNode.Tag,
                                          out strTestExtract)) {

                objText = new frmDisplayText("Test Extract Layout", strTestExtract, ((frmMain)this.ParentForm).printDialog1.PrinterSettings, this.ParentForm.Icon);
                objText.Show(this);
            }
        }

        private void chkUseOccursColumns_CheckedChanged(object sender, EventArgs e) {
            ToggleOccursGroupsControls();
            _IsDirty = true;
        }

        private void chkUseDocumentsOccursGroups_CheckedChanged(object sender, EventArgs e) {
            this.txtDocumentsOccursCount.Enabled = ((CheckBox)sender).Checked;
            _IsDirty = true;
        }

        private void chkUseStubsOccursGroups_CheckedChanged(object sender, EventArgs e) {
            this.txtStubsOccursCount.Enabled = ((CheckBox)sender).Checked;
            _IsDirty = true;
        }

        private void chkUsePaymentsOccursGroups_CheckedChanged(object sender, EventArgs e) {
            this.txtPaymentsOccursCount.Enabled = ((CheckBox)sender).Checked;
            _IsDirty = true;
        }

        private void txtDocumentsOccursCount_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtStubsOccursCount_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtPaymentsOccursCount_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void ctlLayout_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}
