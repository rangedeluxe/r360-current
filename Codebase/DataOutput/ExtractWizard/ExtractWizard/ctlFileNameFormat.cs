﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 130642 DLD 02/24/2014 
*   - Replacing CommonLib.CRLF with Environment.NewLine
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    public delegate void FileNameChangedEventHandler(string imageFileName);

    partial class ctlFileNameFormat : UserControl {

        private string _FileNameValue;
        private string _DefaultFileNameValue;
        private Dictionary<string, string> _TokenList;
        
        public event FileNameChangedEventHandler FileNameChanged;
        
        public ctlFileNameFormat() {

            InitializeComponent();

            this.ctlDataGrid1.AddColumnsRange(
                new System.Windows.Forms.DataGridViewColumn[] {
                this.colFieldName});

        }
        
        public void InitializeControl(
            string fileNameValue,
            string defaultFileNameValue,
            Dictionary<string, string> tokenList) {
            
            _DefaultFileNameValue = defaultFileNameValue;
            _TokenList = tokenList;

            if(fileNameValue == null || fileNameValue.Length == 0) {
                _FileNameValue = defaultFileNameValue;
                RefreshFileNameValue(ParseFileName(defaultFileNameValue));
            } else {
                _FileNameValue = fileNameValue;
                RefreshFileNameValue(ParseFileName(_FileNameValue));
            }

            this.lstImageFields.Items.Clear();

            this.lstImageFields.Items.AddRange(tokenList.Keys.ToArray());
        }

        public string ImageFileName {
            get {
                return(Flatten(BuildFileNameFromGrid()));
            }
        }

        private void OnFileNameChanged() {
            if(FileNameChanged != null) {
                FileNameChanged(Flatten(BuildFileNameFromGrid()));
            }
        }

        private List<string> BuildFileNameFromGrid() {
            
            List<string> arRetVal = new List<string>();
            
            foreach(DataGridViewRow row in this.ctlDataGrid1.Rows) {
                if(row.Cells[0].ReadOnly) {
                    arRetVal.Add(row.Cells[0].Tag.ToString());
                } else if(row.Cells[0].Value != null) {
                    arRetVal.Add(row.Cells[0].Value.ToString());
                }
            }
            
            return(arRetVal);
        }

        private void RefreshFileNameValue(List<string> imageFileNameArray) {

            this.ctlDataGrid1.Rows.Clear();

            foreach(string str in imageFileNameArray) {
                AddRowToDataGrid(str);
            }
            
            OnFileNameChanged();
        }

        private void AddFieldToFormat() {

            if(this.lstImageFields.SelectedItem != null) {
            
                if(this.ctlDataGrid1.Rows.Count > 0) {
                    AddRowToDataGrid("_");
                }

                if(_TokenList.ContainsKey(this.lstImageFields.SelectedItem.ToString())) {
                    AddRowToDataGrid(_TokenList[this.lstImageFields.SelectedItem.ToString()]);
                    OnFileNameChanged();
                }
            }
        }

        private void AddRowToDataGrid(string fieldName) {

            DataGridViewCell[] cellArray;
            DataGridViewTextBoxCell dgText;
            DataGridViewRow dgRow;
            string strTokenDesc;

            dgText = new DataGridViewTextBoxCell();
            dgText.Tag = fieldName;
            dgText.MaxInputLength = 100;

            cellArray = new DataGridViewCell[1];
            cellArray[0] = dgText;

            // Add Row to DataGridView
            dgRow = new DataGridViewRow();
            dgRow.Cells.AddRange(cellArray);

            if(ParseToken(fieldName, out strTokenDesc)) {
                dgRow.Cells[0].Value = strTokenDesc;
                dgRow.Cells[0].ReadOnly = true;
                dgRow.Cells[0].Style.ForeColor = Color.Maroon;
            } else {
                dgRow.Cells[0].Value = fieldName;
            }

            this.ctlDataGrid1.AddRow(dgRow);
            
            dgRow.Cells[0].Selected = true;
        }

        private void btnAddStaticField_Click(object sender, EventArgs e) {
            AddRowToDataGrid("_");
            OnFileNameChanged();
        }

        private void lstImageFields_DoubleClick(object sender, EventArgs e) {
            AddFieldToFormat();
        }

        private void btnApplyDefaultFileName_Click(object sender, EventArgs e) {
            RefreshFileNameValue(ParseFileName(_DefaultFileNameValue));
        }

        private void ctlDataGrid1_ItemMoved(int CurrentIndex, int NewIndex) {
            OnFileNameChanged();
        }

        private void ctlDataGrid1_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
            OnFileNameChanged();
        }

        private void ctlDataGrid1_ItemDeleted(int itemIndex) {
            OnFileNameChanged();
        }

        private void btnAddSelectedToken_Click(object sender, EventArgs e) {
            AddFieldToFormat();
        }

        private bool ParseToken(string value, out string Key) {

            bool bolRetVal;

            if(_TokenList.Values.Contains(value)) {

                Key = string.Empty;
                bolRetVal = false;

                foreach(System.Collections.Generic.KeyValuePair<string, string> kp in _TokenList) {
                    if(kp.Value.ToString() == value) {
                        Key = kp.Key;
                        bolRetVal = true;
                        break;
                    }
                }
                

                //Key = _TokenList.Single(kp => kp.Value == Value).Key;
                //return(true);

            } else {
                Key = string.Empty;
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        private List<string> ParseFileName(string fileName) {

            List<string> arRetVal = new List<string>();
            string strStaticField = string.Empty;
            bool bolTokenFound;

            if(fileName.Length > 0) {

                for(int i = 0;i < fileName.Length;++i) {
                
                    bolTokenFound = false;
                
                    foreach(string strTokenKey in _TokenList.Keys) {

                        if(strTokenKey.Length > 0 && fileName.Substring(i).ToUpper().StartsWith(_TokenList[strTokenKey])) {
                            if(strStaticField.Length > 0) {
                                arRetVal.Add(strStaticField);
                                strStaticField = string.Empty;
                            }
                            arRetVal.Add(_TokenList[strTokenKey]);
                            i += _TokenList[strTokenKey].Length - 1;
                            bolTokenFound = true;
                        }

                        if(i >= fileName.Length) {
                            break;
                        }
                    }

                    if(i < fileName.Length && !bolTokenFound) {
                        strStaticField += fileName[i];
                    }
                }

                if(strStaticField.Length > 0) {
                    arRetVal.Add(strStaticField);
                }
            }

            return(arRetVal);
        }

        private static string Flatten(List<string> fileNameArray) {

            StringBuilder sbRetVal = new StringBuilder();

            foreach(string str in fileNameArray) {
                sbRetVal.Append(str);
            }
            
            return(sbRetVal.ToString());
        }



        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddStaticField = new System.Windows.Forms.Button();
            this.btnApplyDefaultFileName = new System.Windows.Forms.Button();
            this.colFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lstImageFields = new System.Windows.Forms.ListBox();
            this.ctlDataGrid1 = new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ctlDataGrid();
            this.btnAddSelectedToken = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAddStaticField
            // 
            this.btnAddStaticField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddStaticField.Location = new System.Drawing.Point(249, 173);
            this.btnAddStaticField.Name = "btnAddStaticField";
            this.btnAddStaticField.Size = new System.Drawing.Size(131, 23);
            this.btnAddStaticField.TabIndex = 23;
            this.btnAddStaticField.Text = "Add Text";
            this.btnAddStaticField.UseVisualStyleBackColor = true;
            this.btnAddStaticField.Click += new System.EventHandler(this.btnAddStaticField_Click);
            // 
            // btnApplyDefaultFileName
            // 
            this.btnApplyDefaultFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyDefaultFileName.Location = new System.Drawing.Point(249, 199);
            this.btnApplyDefaultFileName.Name = "btnApplyDefaultFileName";
            this.btnApplyDefaultFileName.Size = new System.Drawing.Size(131, 23);
            this.btnApplyDefaultFileName.TabIndex = 20;
            this.btnApplyDefaultFileName.Text = "Apply Default File Name";
            this.btnApplyDefaultFileName.UseVisualStyleBackColor = true;
            this.btnApplyDefaultFileName.Click += new System.EventHandler(this.btnApplyDefaultFileName_Click);
            // 
            // colFieldName
            // 
            this.colFieldName.HeaderText = "Field Name";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldName.Width = 150;
            // 
            // lstImageFields
            // 
            this.lstImageFields.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lstImageFields.ForeColor = System.Drawing.Color.Maroon;
            this.lstImageFields.FormattingEnabled = true;
            this.lstImageFields.Location = new System.Drawing.Point(249, 19);
            this.lstImageFields.Name = "lstImageFields";
            this.lstImageFields.Size = new System.Drawing.Size(131, 108);
            this.lstImageFields.TabIndex = 15;
            this.lstImageFields.DoubleClick += new System.EventHandler(this.lstImageFields_DoubleClick);
            // 
            // ctlDataGrid1
            // 
            this.ctlDataGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlDataGrid1.Location = new System.Drawing.Point(0, 0);
            this.ctlDataGrid1.MinimumSize = new System.Drawing.Size(243, 254);
            this.ctlDataGrid1.Name = "ctlDataGrid1";
            this.ctlDataGrid1.Size = new System.Drawing.Size(243, 254);
            this.ctlDataGrid1.TabIndex = 7;
            this.ctlDataGrid1.ItemMoved += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemMovedEventHandler(this.ctlDataGrid1_ItemMoved);
            this.ctlDataGrid1.ItemDeleted += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ItemDeletedEventHandler(this.ctlDataGrid1_ItemDeleted);
            this.ctlDataGrid1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlDataGrid1_CellEndEdit);
            this.ctlDataGrid1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.ctlDataGrid1_CellValidating);
            // 
            // btnAddSelectedToken
            // 
            this.btnAddSelectedToken.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddSelectedToken.Location = new System.Drawing.Point(249, 133);
            this.btnAddSelectedToken.Name = "btnAddSelectedToken";
            this.btnAddSelectedToken.Size = new System.Drawing.Size(131, 23);
            this.btnAddSelectedToken.TabIndex = 24;
            this.btnAddSelectedToken.Text = "Add Token";
            this.btnAddSelectedToken.UseVisualStyleBackColor = true;
            this.btnAddSelectedToken.Click += new System.EventHandler(this.btnAddSelectedToken_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(252, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Available Tokens:";
            // 
            // ctlFileNameFormat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddSelectedToken);
            this.Controls.Add(this.ctlDataGrid1);
            this.Controls.Add(this.btnAddStaticField);
            this.Controls.Add(this.btnApplyDefaultFileName);
            this.Controls.Add(this.lstImageFields);
            this.MinimumSize = new System.Drawing.Size(383, 254);
            this.Name = "ctlFileNameFormat";
            this.Size = new System.Drawing.Size(383, 254);
            this.Load += new System.EventHandler(this.ctlFileNameFormat_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddStaticField;
        private System.Windows.Forms.Button btnApplyDefaultFileName;
        private System.Windows.Forms.ListBox lstImageFields;
        private ctlDataGrid ctlDataGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldName;
        private System.Windows.Forms.Button btnAddSelectedToken;
        private System.Windows.Forms.Label label1;

        private void ctlDataGrid1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {

            bool bolIsValid = true;

            if(e != null && e.FormattedValue != null && e.FormattedValue.ToString().Length > 0) {
                foreach(char chr in e.FormattedValue.ToString()) {
                    foreach(char echr in CommonLib.INVALID_FILE_NAME_CHARS) {
                        if(chr == echr) {
                            bolIsValid = false;
                            break;
                        }
                    }

                    if(!bolIsValid) {
                        break;
                    }
                }
            }

            if(!bolIsValid) {
                MessageBox.Show("A file name cannot contain any of the following characters:" + 
                                Environment.NewLine +
                                "         " + FormattedExclusionChars(),
                                "Invalid Input");
                e.Cancel = true;
            }
        }

        private string FormattedExclusionChars() {
            
            string strRetVal = string.Empty;
            bool bolIsFirst = true;

            foreach(char echr in CommonLib.INVALID_FILE_NAME_CHARS) {
                if(bolIsFirst) {
                    bolIsFirst = false;
                } else {
                    strRetVal += ' ';
                }
                strRetVal += echr;
            }
            
            return(strRetVal);
        }

        private void ctlFileNameFormat_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}
