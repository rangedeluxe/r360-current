using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;

using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractRun;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractSetupRpt;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   - Initial Version 
* WI 106884-106887 DRP 7/10/2013
*   - Added check for "first time login" and added method to change the user's password.
* WI 114570 MLH 09/20/2013
*   - Removed CurrentProcessingDate from Standard Fields (for v2.0)
* WI 115152 Derek Davison - Beacon Tech - 02/06/2014
*   - Added RemoveColumns method which is used to prevent unwanted DB columns from being added to menus and nodes (MostRecent)
* WI 116522 Derek Davison - Beacon Tech - 02/06/2014
*   - Added RemoveColumns method which is also used to remove "Key" columns
* WI 116533 - 02/10/2014
*   - Replaced hard-coded aggregates/standards strings with the constants in Support.cs.
* WI 129703 BLR 02/14/2014 
*   - Removed all DE (Data Entry) Fields from the UI.   
* WI 129678 BLR 02/17/2014
*   - Uncommented parent hierarchy data.
* WI 130165 DLD 02/19/2014
*   - Expanded Exception handling and message logging. 
* WI 130364 DLD 02/20/2014
*   - Verify database connectivity providing 'friendly' messages to user upon failure.
*   - Verify paths and file associations. 
* WI 115249 BLR 02/26/2014
*   - Added tablename/schema to limit objects.
* WI 129674 BLR 02/27/2014
*   - Migrating column and table names over to friendly names.
* WI 131640 BLR 03/04/2014, DLD 03/05/2014    
*   - Added in additional exception logging.
*   - Diverted logging calls to new cCommonLogLib in ExtractLib
* WI 130723 BLR 03/05/2014
*   - Removed all DE from UI (missed a few).
* WI 130364 DLD 03/06/2014
*   - Load and validate Settings up-front.
* WI 130723 BLR 03/06/2014
*   - Set the menu item names to display a 'DisplayName' from the column.
* WI 132009 BLR 03/06/2014   
*   - Added in func for joined tables on batch summaries. 
* WI 132304 BLR 03/10/2014
*  - Removed password func. 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
* WI 135749 BLR 04/14/2014
*   - Passed the fully-qualified name down to the IOrderColumn. 
* WI 137747 BLR 04/21/2014
*   - Filtered the message boxes alerts a bit, kicked out early if
*     init is failing.
* WI 137747 BLR 05/16/2014
*   - Bit the bullet and just filtered the alerts more by contains on
*     "Attempting retry", there seems to be to many variations. 
* WI 115658 BLR 06/24/2014
*   - Added in functionality to populate DE fields as well. 
*   - Altered the display names a bit to account for DE fields.   
* WI 143034 BLR 05/22/2014
*   - Changed RemoveColumns from ColumnName over to DisplayName.
* WI 153593 BLR 07/15/2014
*   - Added DepositDateKey and SourceProcessingDateKey to an exception
*     to the key filter.   
* WI 152321 BLR 08/15/2014
*   - Moved configuration settings over to ExtractConfigurationSettings.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    public partial class frmMain : Form {

        #region Local Variables
        private ExtractConfigurationSettings _Settings = null;
        private ArrayList _Errors = null;
        private cExtractWizardBLL _ExtractWizardBLL = null;

        private bool _ItemProcColumnsInitialized = false;

        //----------------------------------------------------------------------------
        // The Item Processing related nodes contain all of the data tables that
        // ExtractWizard pulls from includeing ChecksDataEntry and StubsDataEntry.
        // I've still kept the indexing of the private arrays so the tables match
        // the LayoutLevelEnum enumeration, with 0 being NULL and including positions
        // 9 and 10 for the Data Entry tables.
        private const int ITEMPROC_BANK_IDX = 1;
        private const int ITEMPROC_LBX_IDX = 3;
        private const int ITEMPROC_BATCH_IDX = 4;
        private const int ITEMPROC_TXN_IDX = 5;
        private const int ITEMPROC_PAYMENTS_IDX = 6;
        private const int ITEMPROC_STUBS_IDX = 7;
        private const int ITEMPROC_DOCS_IDX = 8;
        // WI 130723 : Removed DE references.
        //private const int ITEMPROC_PAYMENTSDE_IDX = 9;
        //private const int ITEMPROC_STUBSDE_IDX = 10;
        ////private const int ITEMPROC_REMITTER_IDX = 9;

        private TreeNode[] _ItemProcTreeNodes = new TreeNode[11];   // {NULL, Bank, Lbx, Customer, Batch, Txn, Checks, Stubs, Doc, ChecksDE, StubsDE}
        private MenuItem[] _ItemProcMenuItems = new MenuItem[11];   // {NULL, Bank, Lbx, Customer, Batch, Txn, Checks, Stubs, Doc, ChecksDE, StubsDE}
        //----------------------------------------------------------------------------

        private TreeNode[] _AggregateTreeNodes = new TreeNode[10];  // Correlates to LayoutLevelEnum
        private TreeNode[] _StandardTreeNodes = new TreeNode[10];   // Correlates to LayoutLevelEnum

        private MenuItem[] _AggregateMenuItems = new MenuItem[10];  // Correlates to LayoutLevelEnum
        private MenuItem[] _StandardMenuItems = new MenuItem[10];   // Correlates to LayoutLevelEnum
        
        private List<cColumn> _BankColumns = new List<cColumn>();
        private List<cColumn> _CustomerColumns = new List<cColumn>();
        private List<cColumn> _LockboxColumns = new List<cColumn>();
        private List<cColumn> _BatchColumns = new List<cColumn>();
        private List<cColumn> _TransactionsColumns = new List<cColumn>();
        private List<cColumn> _PaymentsColumns = new List<cColumn>();
        private List<cColumn> _StubsColumns = new List<cColumn>();
        private List<cColumn> _DocumentsColumns = new List<cColumn>();
        private List<cColumn> _PaymentsDataEntryColumns = new List<cColumn>();
        private List<cColumn> _StubsDataEntryColumns = new List<cColumn>();

        
        private string _InputFileName = string.Empty;

        private FormState _CurrentFormState = FormState.Init;
        private TreeNode _CurrentContextNode = null;
        
        private bool _DataFieldsExpanded = false;
        private bool _AggregateFieldsExpanded = false;
        private bool _StandardFieldsExpanded = false;
        
        private Dictionary<string, StringDictionary> _HintDictionary = null;
        
        private int _UserID = -1;
        
        private List<cExtractDescriptor> _ExtractDescriptors = null;

        private enum FormState {
            Init = 0,
            Edit = 1,
            Run = 2
        }
        #endregion Local Variables

        #region Constructor
        public frmMain(int userID) {            
            InitializeComponent();

            _UserID = userID;
            
            SetFormState(FormState.Init);
        }

        private void frmMain_Load(object sender, EventArgs e) 
        {
            _Settings = new ExtractConfigurationSettings(new ConfigurationManagerProvider());

            try { _Settings.ValidateSettings(); }
            catch (Exception ex)
            {
                OnOutputError(ex);
            }

            cOLFServicesLib.Settings = _Settings;

            // Some initial Paths.
            var strDefaultExtractPath = Path.Combine(CommonLib.AppPath, @"..\Extracts");
            var strDefaultDataPath = Path.Combine(CommonLib.AppPath, @"..\Data");

            if (_Settings.SetupPath == strDefaultExtractPath && !Directory.Exists(strDefaultExtractPath))
                Directory.CreateDirectory(strDefaultExtractPath);

            if (_Settings.ExceptionLogFile.StartsWith(strDefaultDataPath) && !Directory.Exists(strDefaultDataPath))
                Directory.CreateDirectory(strDefaultDataPath);

            // Test OLF before we begin.
            try
            {
                if (cOLFServicesLib.ServiceIsAvailable == false)
                    OnOutputMessage("Could not Connect to the Image Repository. Images will be disabled during the execution of Extract Design Manager.", "Extract Design Manager", MessageType.Warning, MessageImportance.Essential);
            }
            catch (Exception ex)
            {
                OnOutputError(this, new Exception("An error occurred testing connectivity to the image repository.", ex));
            }

            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
        #endregion Constructor

        #region private object properties

        private ExtractConfigurationSettings Settings 
        {
            get 
            {
                if(_Settings == null) 
                {
                    _Settings = new ExtractConfigurationSettings(new ConfigurationManagerProvider());
                    _Settings.OutputError += OnOutputError;
                    _Settings.OutputMessage += OnOutputMessage;
                }
                return(_Settings);
            }
        }

        private cExtractWizardBLL ExtractWizardBLL {
            get {
                if(_ExtractWizardBLL == null) {
                    _ExtractWizardBLL = new cExtractWizardBLL(Settings);
                    _ExtractWizardBLL.OutputError += OnOutputError;
                    _ExtractWizardBLL.OutputMessage += OnOutputMessage;
                }
                return(_ExtractWizardBLL);
            }
        }

        private Dictionary<string, StringDictionary> HintDictionary {

            get {
            
                Dictionary<string, StringDictionary> dicHintDictionary;

                if(_HintDictionary == null) {
                    if(HintLib.BuildHintDictionary(out dicHintDictionary)) {
                        _HintDictionary = dicHintDictionary;
                    } else {
                        _HintDictionary = new Dictionary<string, StringDictionary>();
                    }
                }

                return(_HintDictionary);
            }
        }

        private string GetHint(string HintSection, string HintKey) {

            if(HintDictionary.ContainsKey(HintSection)) {
                if(HintDictionary[HintSection].ContainsKey(HintKey)) {
                    return(HintDictionary[HintSection][HintKey]);
                } else {
                    return(string.Empty);
                }
            } else {
                return(string.Empty);
            }
        }


        private List<cExtractDescriptor> ExtractDescriptors {

            get {
            
                List<cExtractDescriptor> arExtractDescriptors;

                if(_ExtractDescriptors == null) {
                    //if(DBUtil.GetTableColumns(Support.TABLE_NAME_BATCHTRACE, out objColumns)) {
                    if(ExtractWizardBLL.GetExtractDescriptors(out arExtractDescriptors)) {
                        _ExtractDescriptors = arExtractDescriptors;
                    } else {
                        _ExtractDescriptors = null;
                    }
                }

                return(_ExtractDescriptors);
            }
        }

        #endregion private object properties

        #region Local Error String Formatting
        private string GetErrorString() {
            
            string strRetVal = string.Empty;
            
            if(_Errors != null) {
                for(int i=0;i<_Errors.Count;++i) {
                    strRetVal += _Errors[i];
                    if(i<_Errors.Count -1) {
                        strRetVal += "\n\n";
                    }
                }
            }

            return(strRetVal);
        }    
        #endregion Local Error String Formatting

        #region Raise Event Methods
        private void OnExtractOutputError(Exception e) {

            if(_Errors == null) {
                _Errors = new ArrayList();
            }

            if(e.StackTrace == null) {
                _Errors.Add(e.Message);
            } else {
                _Errors.Add(e.StackTrace.TrimStart() + "\n\n" + e.Message);
            }
            
            // DLD - WI 130165           
            OnExtractOutputError(e);
        }

        private void OnOutputStatusMessage(string statusMsg) {
            this.toolStripStatusMsg.Text = statusMsg;
            Application.DoEvents();
        }

        private void OnOutputMessage(
            string message,
            string src,
            MessageType messageType,
            MessageImportance messageImportance)
        {

            string strCaption;
            MessageBoxIcon objIcon;

            if (messageImportance == MessageImportance.Essential)
            {
                switch (messageType)
                {
                    case MessageType.Warning:
                        strCaption = "Warning";
                        objIcon = MessageBoxIcon.Warning;
                        break;
                    case MessageType.Error:
                        strCaption = "Error";
                        objIcon = MessageBoxIcon.Error;
                        break;
                    default:    // MessageType.Information:
                        strCaption = "Information";
                        objIcon = MessageBoxIcon.Information;
                        break;
                }

                cCommonLogLib.LogMessageLTA(message, (LTAMessageImportance)messageImportance);

                // WI 137747 : No need to show the stored procedure retry attempts.
                if (!message.Contains("Attempting retry"))
                    MessageBox.Show(message, strCaption, MessageBoxButtons.OK, objIcon);

                // DLD - WI 130364 - Verify database connectivity providing 'friendly' messages to user upon failure.
                if (message.ToUpper() == ExtractLib.Constants.ERROR_MSG_NO_DATABASE_CONNECTION.ToUpper())
                {
                    var msg = Application.ProductName + " has reached an unstable state and should be closed."
                        + Environment.NewLine + Environment.NewLine + "Would you like to close it now?";

                    var dr = MessageBox.Show(this, msg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (dr == System.Windows.Forms.DialogResult.Yes)
                        Close();
                }
            }
        }

        internal void OnOutputError(object sender, System.Exception e)
        {
            // DLD - WI 130165
            if (e != null)
            {
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
                Program.ShowUnhandledExceptionDialog(e.Message);
            }
        }

        internal void OnOutputError(System.Exception e)
        {
            OnOutputError(this, e);
        }

        private void OnExtractSaved(
            string extractFileName, 
            cExtractDef extractDef) {
            
            ExtractWizardBLL.WriteAuditFileData(
                "Extract Definition File Updated",
                extractDef,
                _UserID,
                "Extract definition " + extractFileName + " updated.");
        }

        /// <summary>
        /// Reset the local _BatchTraceDateColumns collection so it is 
        /// rebuilt the next time it is called.
        /// </summary>
        private void OnExtractDescriptorAdded(
            out List<cExtractDescriptor> extractDescriptors) {
            
            _ExtractDescriptors = null;
            extractDescriptors = ExtractDescriptors;
        }

        #endregion Raise Event Methods

        #region Load ExtractDef

        private bool SelectExtractScriptDir() {

            string strFilePath;
            bool bolRetVal;

            if(IOLib.BrowseFile(
                ExtractLib.Constants.FILE_FILTER_EXTRACT_DEF, 
                Settings.DefaultDefinitionFileFormatIndex,
                Settings.SetupPath,
                true,
                out strFilePath)) {

                if(PromptForSaveIfNecessary()) {
                    if(strFilePath.ToLower().EndsWith(".xdf")) {
                        bolRetVal = LoadExtractDef(strFilePath);
                    } else {
                        bolRetVal = LoadXmlExtractDef(strFilePath);
                    }
                } else {
                    bolRetVal = false;
                }

            } else {
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        private bool LoadExtractDef(string extractDefFileName) {

            cExtractDef objExtractDef;
            string strMsg;
            bool bolRetVal;

            if(extractDefFileName.Length == 0) {

                bolRetVal = false;

            } else if(!File.Exists(extractDefFileName)) {

                var msg = String.Format("Could not locate Extract Definition File: [{0}].", extractDefFileName);

                // DLD - WI 130165
                cCommonLogLib.LogMessageLTA(msg, LTAMessageImportance.Essential);

                MessageBox.Show(msg, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                bolRetVal = false;

            } else {

                objExtractDef = new cExtractDef(extractDefFileName);
                objExtractDef.OutputError += this.OnOutputError;
                objExtractDef.OutputStatusMsg += this.OnOutputStatusMessage;
                objExtractDef.OutputMessage += this.OnOutputMessage;
        
                if(objExtractDef.ParseExtractDefFile(extractDefFileName)) {

                    if(LoadExtractDef(objExtractDef)) {

                        this.txtExtractScriptPath.Text = extractDefFileName;
                        this.Text = "Extract Design Manager - [" + extractDefFileName + "]";
                        _InputFileName = extractDefFileName;
                        OnOutputStatusMessage("Extract Definition File loaded: [" + _InputFileName + "]");

                        bolRetVal = true;
                    } else {
                        bolRetVal = false;
                    }

                } else {

                    if(_Errors != null && _Errors.Count > 0) {
                        strMsg = "Could not parse Extract Definition File [" + extractDefFileName + "]: " + GetErrorString();
                        _Errors = null;
                    } else {
                        strMsg = "Could not parse Extract Definition File [" + extractDefFileName + "].";
                    }

                    // DLD - WI 130165
                    cCommonLogLib.LogMessageLTA(strMsg, LTAMessageImportance.Verbose);
                    MessageBox.Show(ExtractLib.Constants.ERROR_MSG_PROCESS_FAILED, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    bolRetVal = false;
                }

                if(_Errors != null && _Errors.Count > 0) {
                    OnOutputError(this, new Exception(GetErrorString()));
                    _Errors = null;
                }
            }

            return(bolRetVal);
        }

        public bool LoadXmlExtractDef(string extractDefFileName)
        {

            bool bolRetVal = false;
            cExtractDef objExtractDef;
            string strMsg;

            if (!String.IsNullOrWhiteSpace(extractDefFileName))
            {

                objExtractDef = new cExtractDef(extractDefFileName);
                objExtractDef.OutputError += this.OnOutputError;
                objExtractDef.OutputStatusMsg += this.OnOutputStatusMessage;
                objExtractDef.OutputMessage += this.OnOutputMessage;

                if (objExtractDef.ParseExtractDefXmlFile(extractDefFileName))
                {

                    if (LoadExtractDef(objExtractDef))
                    {

                        this.txtExtractScriptPath.Text = extractDefFileName;
                        this.Text = "Extract Design Manager - [" + extractDefFileName + "]";
                        _InputFileName = extractDefFileName;
                        OnOutputStatusMessage("Extract Definition File loaded: [" + _InputFileName + "]");

                        bolRetVal = true;
                    }
                }
                else
                {

                    if (_Errors != null && _Errors.Count > 0)
                    {
                        strMsg = "Could not parse Extract Definition File [" + extractDefFileName + "]: " + GetErrorString();
                        _Errors = null;
                    }
                    else
                    {
                        strMsg = "Could not parse Extract Definition File [" + extractDefFileName + "].";
                    }

                    // DLD - WI 130165
                    cCommonLogLib.LogMessageLTA(strMsg, LTAMessageImportance.Essential);
                    MessageBox.Show(ExtractLib.Constants.ERROR_MSG_PROCESS_FAILED, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                if (_Errors != null && _Errors.Count > 0)
                {
                    OnOutputError(this, new Exception(GetErrorString()));
                    _Errors = null;
                }
            }

            return (bolRetVal);
        }

        private bool LoadExtractDef(cExtractDef extractDef)
        {

            bool bolRetVal;
            bool bolTimeStampExistsInDb;
            TreeNode nodeGeneral;

            ClearForm();

            if (!_ItemProcColumnsInitialized)
            {
                InitializeFormValues();
            
            }

            nodeGeneral = this.tvLayouts.Nodes.Add("General", "General");
            nodeGeneral.Tag = extractDef;

            bolTimeStampExistsInDb = false;
            foreach (cExtractDescriptor extractDescriptor in ExtractDescriptors)
            {
                if (extractDescriptor.ExtractDescriptor.ToLower().Trim() == extractDef.TimeStamp.ToLower().Trim())
                {
                    bolTimeStampExistsInDb = true;
                    break;
                }
            }
            if (!bolTimeStampExistsInDb)
            {
                extractDef.TimeStamp = string.Empty;
            }

            BuildLayoutNodes(nodeGeneral, Support.LBL_FILE_LAYOUTS, extractDef.FileLayouts);
            BuildLayoutNodes(nodeGeneral, Support.LBL_BANK_LAYOUTS, extractDef.BankLayouts);
            BuildLayoutNodes(nodeGeneral, Support.LBL_CLIENT_ACCOUNT_LAYOUTS, extractDef.LockboxLayouts);
            BuildLayoutNodes(nodeGeneral, Support.LBL_BATCH_LAYOUTS, extractDef.BatchLayouts);
            BuildLayoutNodes(nodeGeneral, Support.LBL_TXN_LAYOUTS, extractDef.TransactionLayouts);
            BuildLayoutNodes(nodeGeneral, Support.LBL_PAYMENT_LAYOUTS, extractDef.PaymentLayouts);
            BuildLayoutNodes(nodeGeneral, Support.LBL_STUB_LAYOUTS, extractDef.StubLayouts);
            BuildLayoutNodes(nodeGeneral, Support.LBL_DOCUMENT_LAYOUTS, extractDef.DocumentLayouts);
            BuildLayoutNodes(nodeGeneral, Support.LBL_JOINT_DETAIL_LAYOUTS, extractDef.JointDetailLayouts);

            BuildAggregateNodes();
            BuildStandardNodes();

            nodeGeneral.Expand();

            tvLayouts.SelectedNode = nodeGeneral;

            BuildOrderByNodes(Support.LBL_BANK, extractDef.BankOrderByColumns);
            BuildOrderByNodes(Support.LBL_CLIENT_ACCOUNT, extractDef.LockboxOrderByColumns);
            BuildOrderByNodes(Support.LBL_BATCH, extractDef.BatchOrderByColumns);
            BuildOrderByNodes(Support.LBL_TRANSACTION, extractDef.TransactionsOrderByColumns);
            BuildOrderByNodes(Support.LBL_PAYMENTS, extractDef.PaymentsOrderByColumns);
            BuildOrderByNodes(Support.LBL_STUBS, extractDef.StubsOrderByColumns);
            BuildOrderByNodes(Support.LBL_DOCUMENTS, extractDef.DocumentsOrderByColumns);

            BuildLimitDataNodes(Support.LBL_BANK, extractDef.BankLimitItems);
            BuildLimitDataNodes(Support.LBL_CLIENT_ACCOUNT, extractDef.LockboxLimitItems);
            BuildLimitDataNodes(Support.LBL_BATCH, extractDef.BatchLimitItems);
            BuildLimitDataNodes(Support.LBL_TRANSACTION, extractDef.TransactionsLimitItems);
            BuildLimitDataNodes(Support.LBL_PAYMENTS, extractDef.PaymentsLimitItems);
            BuildLimitDataNodes(Support.LBL_STUBS, extractDef.StubsLimitItems);
            BuildLimitDataNodes(Support.LBL_DOCUMENTS, extractDef.DocumentsLimitItems);

            if (extractDef.PostProcCodeModules.Count > 0)
            {
                for (int i = 0; i < extractDef.PostProcCodeModules.Count; ++i)
                {
                    BuildPostProcCodeModuleNode(this.tvLayouts.Nodes["General"], extractDef.PostProcCodeModules[i]);
                }
            }

            return true;
        }

        #endregion Load ExtractDef

        #region Build ExtractDef object from form values
        internal cExtractDef GetExtractDefFromForm() {
            
            cExtractDef objExtractDef;
            cExtractDef objCurrentExtractDef;

            objExtractDef = new cExtractDef(_InputFileName);

            // Ensure that any data modified in the current window has been synched with the active node.
            SynchCurrentNodes();

            if(this.tvLayouts.Nodes["General"] != null && 
               this.tvLayouts.Nodes["General"].Tag != null && 
               this.tvLayouts.Nodes["General"].Tag is cExtractDef) {

                objCurrentExtractDef = (cExtractDef)this.tvLayouts.Nodes["General"].Tag;
                objCurrentExtractDef.OutputStatusMsg += OnOutputStatusMessage;
                objCurrentExtractDef.OutputError += OnOutputError;
                objExtractDef.OutputMessage += this.OnOutputMessage;

                objExtractDef.ExtractPath = objCurrentExtractDef.ExtractPath;
                objExtractDef.ImagePath = objCurrentExtractDef.ImagePath;
                objExtractDef.FieldDelim = objCurrentExtractDef.FieldDelim;
                objExtractDef.ImageFileFormat = objCurrentExtractDef.ImageFileFormat;

                // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                objExtractDef.IncludeFooter = objCurrentExtractDef.IncludeFooter;

                objExtractDef.ImageFileBatchTypeFormatFull = objCurrentExtractDef.ImageFileBatchTypeFormatFull;
                objExtractDef.ImageFileNamePerBatch = objCurrentExtractDef.ImageFileNamePerBatch;
                objExtractDef.ImageFileNamePerTran = objCurrentExtractDef.ImageFileNamePerTran;
                objExtractDef.ImageFileNameSingle = objCurrentExtractDef.ImageFileNameSingle;
                objExtractDef.ImageFileProcDateFormat = objCurrentExtractDef.ImageFileProcDateFormat;
                objExtractDef.ImageFileZeroPad = objCurrentExtractDef.ImageFileZeroPad;
                objExtractDef.IncludeImageFolderPath = objCurrentExtractDef.IncludeImageFolderPath;
                objExtractDef.IncludeNULLRows = objCurrentExtractDef.IncludeNULLRows;
                objExtractDef.LogFilePath = objCurrentExtractDef.LogFilePath;
                objExtractDef.NoPad = objCurrentExtractDef.NoPad;
                objExtractDef.NullAsSpace = objCurrentExtractDef.NullAsSpace;
                objExtractDef.PostProcDLLFileName = objCurrentExtractDef.PostProcDLLFileName;
                objExtractDef.PrinterName = objCurrentExtractDef.PrinterName;
                objExtractDef.RecordDelim = objCurrentExtractDef.RecordDelim;
                objExtractDef.TimeStamp = objCurrentExtractDef.TimeStamp;
                objExtractDef.UseQuotes = objCurrentExtractDef.UseQuotes;
                objExtractDef.Version = objCurrentExtractDef.Version;
                objExtractDef.ZipOutputFiles = objCurrentExtractDef.ZipOutputFiles;
                objExtractDef.ZipFilePath = objCurrentExtractDef.ZipFilePath;

                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes) {
                    if(node.Tag != null &&
                       node.Tag is cPostProcCodeModule) {
                        
                        objExtractDef.PostProcCodeModules.Add((cPostProcCodeModule)node.Tag);
                    }
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes["File Layouts"].Nodes) {
                    objExtractDef.AddLayout((cLayout)node.Tag);
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes[Support.LBL_BANK_LAYOUTS].Nodes) {
                    objExtractDef.AddLayout((cLayout)node.Tag);
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes[Support.LBL_CLIENT_ACCOUNT_LAYOUTS].Nodes) {
                    objExtractDef.AddLayout((cLayout)node.Tag);
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes[Support.LBL_BATCH_LAYOUTS].Nodes) {
                    objExtractDef.AddLayout((cLayout)node.Tag);
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes[Support.LBL_TXN_LAYOUTS].Nodes) {
                    objExtractDef.AddLayout((cLayout)node.Tag);
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes[Support.LBL_PAYMENT_LAYOUTS].Nodes) {
                    objExtractDef.AddLayout((cLayout)node.Tag);
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes[Support.LBL_STUB_LAYOUTS].Nodes) {
                    objExtractDef.AddLayout((cLayout)node.Tag);
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes[Support.LBL_DOCUMENT_LAYOUTS].Nodes) {
                    objExtractDef.AddLayout((cLayout)node.Tag);
                }
                
                foreach(TreeNode node in this.tvLayouts.Nodes["General"].Nodes[Support.LBL_JOINT_DETAIL_LAYOUTS].Nodes) {
                    objExtractDef.AddJointDetailLayout((cLayout)node.Tag);
                }

                foreach(cOrderByColumn orderbycolumn in (List<cOrderByColumn>)this.tvOrderData.Nodes[Support.LBL_BANK].Tag) {
                    objExtractDef.AddOrderByColumn(LayoutLevelEnum.Bank, orderbycolumn);
                }

                foreach(cOrderByColumn orderbycolumn in (List<cOrderByColumn>)this.tvOrderData.Nodes[Support.LBL_CLIENT_ACCOUNT].Tag) {
                    objExtractDef.AddOrderByColumn(LayoutLevelEnum.ClientAccount, orderbycolumn);
                }

                foreach(cOrderByColumn orderbycolumn in (List<cOrderByColumn>)this.tvOrderData.Nodes[Support.LBL_BATCH].Tag) {
                    objExtractDef.AddOrderByColumn(LayoutLevelEnum.Batch, orderbycolumn);
                }

                foreach(cOrderByColumn orderbycolumn in (List<cOrderByColumn>)this.tvOrderData.Nodes[Support.LBL_TRANSACTION].Tag) {
                    objExtractDef.AddOrderByColumn(LayoutLevelEnum.Transaction, orderbycolumn);
                }

                foreach(cOrderByColumn orderbycolumn in (List<cOrderByColumn>)this.tvOrderData.Nodes[Support.LBL_PAYMENTS].Tag) {
                    objExtractDef.AddOrderByColumn(LayoutLevelEnum.Payment, orderbycolumn);
                }

                foreach(cOrderByColumn orderbycolumn in (List<cOrderByColumn>)this.tvOrderData.Nodes[Support.LBL_STUBS].Tag) {
                    objExtractDef.AddOrderByColumn(LayoutLevelEnum.Stub, orderbycolumn);
                }

                foreach(cOrderByColumn orderbycolumn in (List<cOrderByColumn>)this.tvOrderData.Nodes[Support.LBL_DOCUMENTS].Tag) {
                    objExtractDef.AddOrderByColumn(LayoutLevelEnum.Document, orderbycolumn);
                }

                foreach(cLimitItem limititem in (List<cLimitItem>)this.tvLimitData.Nodes[Support.LBL_BANK].Tag) {
                    objExtractDef.AddLimitItem(LayoutLevelEnum.Bank, limititem);
                }

                foreach(cLimitItem limititem in (List<cLimitItem>)this.tvLimitData.Nodes[Support.LBL_CLIENT_ACCOUNT].Tag) {
                    objExtractDef.AddLimitItem(LayoutLevelEnum.ClientAccount, limititem);
                }

                foreach(cLimitItem limititem in (List<cLimitItem>)this.tvLimitData.Nodes[Support.LBL_BATCH].Tag) {
                    objExtractDef.AddLimitItem(LayoutLevelEnum.Batch, limititem);
                }

                foreach(cLimitItem limititem in (List<cLimitItem>)this.tvLimitData.Nodes[Support.LBL_TRANSACTION].Tag) {
                    objExtractDef.AddLimitItem(LayoutLevelEnum.Transaction, limititem);
                }

                foreach(cLimitItem limititem in (List<cLimitItem>)this.tvLimitData.Nodes[Support.LBL_PAYMENTS].Tag) {
                    objExtractDef.AddLimitItem(LayoutLevelEnum.Payment, limititem);
                }

                foreach(cLimitItem limititem in (List<cLimitItem>)this.tvLimitData.Nodes[Support.LBL_STUBS].Tag) {
                    objExtractDef.AddLimitItem(LayoutLevelEnum.Stub, limititem);
                }

                foreach(cLimitItem limititem in (List<cLimitItem>)this.tvLimitData.Nodes[Support.LBL_DOCUMENTS].Tag) {
                    objExtractDef.AddLimitItem(LayoutLevelEnum.Document, limititem);
                }

            }

            return(objExtractDef);
        }
        #endregion Build ExtractDef object from form values

        #region Save Extract ExtractDef Methods
        private bool PromptForSaveIfNecessary() {

            bool bolSaveRequired = false;
            cExtractDef objFileExtractDef;
            cExtractDef objFormExtractDef;
            string strFileName;
            bool bolRetVal;

            if(this.txtExtractScriptPath.Text.Length > 0 && File.Exists(this.txtExtractScriptPath.Text)) {

                objFileExtractDef = new cExtractDef(this.txtExtractScriptPath.Text);
                objFileExtractDef.OutputError += OnOutputError;
                objFileExtractDef.OutputStatusMsg += OnOutputStatusMessage;
                objFileExtractDef.OutputMessage += this.OnOutputMessage;

                if(this.txtExtractScriptPath.Text.ToLower().EndsWith(".xml")) {
                    if(objFileExtractDef.ParseExtractDefXmlFile(this.txtExtractScriptPath.Text)) {
                        objFormExtractDef = GetExtractDefFromForm();
                        
                        if(objFileExtractDef.ToXml().OuterXml != objFormExtractDef.ToXml().OuterXml) {
                            bolSaveRequired = true;
                        }
                    }
                } else {
                    if(objFileExtractDef.ParseExtractDefFile(this.txtExtractScriptPath.Text)) {
                        objFormExtractDef = GetExtractDefFromForm();
                        
                        if(objFileExtractDef.ToXml().OuterXml != objFormExtractDef.ToXml().OuterXml) {
                            bolSaveRequired = true;
                        }
                        
                    } else {
                        bolSaveRequired = true;
                    }
                }

            } else {
                bolSaveRequired = _InputFileName.Length > 0;
            }

            if(bolSaveRequired) {
                switch (MessageBox.Show("Changes have been made to the current extract definition.  Save these changes?", "Extract Design Manager", MessageBoxButtons.YesNoCancel))
                {
                    case DialogResult.Yes:
                        bolRetVal = SaveExtractDef(this.txtExtractScriptPath.Text, out strFileName);
                        break;
                    case DialogResult.Cancel:
                        bolRetVal = false;
                        break;
                    default:    //DialogResult.No:
                        bolRetVal = true;
                        break;
                }
            } else {
                bolRetVal = true;
            }

            return(bolRetVal);
        }

        private bool SaveExtractDef(
            string defaultFilePath, 
            out string filePath) {

            cExtractDef objFormExtractDef;

            string strSaveFilePath;
            bool bolRetVal;

            if(ValidateFormData()) {

                objFormExtractDef = GetExtractDefFromForm();
                objFormExtractDef.OutputError += OnOutputError;
                objFormExtractDef.OutputStatusMsg += OnOutputStatusMessage;
                objFormExtractDef.ExtractSaved += OnExtractSaved;
                objFormExtractDef.OutputMessage += this.OnOutputMessage;
            
                if(defaultFilePath.Length > 0 && File.Exists(defaultFilePath)) {
                    strSaveFilePath = defaultFilePath;
                    if(strSaveFilePath.EndsWith(".xml")) {
                        bolRetVal = objFormExtractDef.SaveAsXml(strSaveFilePath);
                    } else {
                        bolRetVal = objFormExtractDef.SaveAsCxs(strSaveFilePath);
                    }
                } else {

                    if (IOLib.BrowseFileSave(ExtractLib.Constants.FILE_FILTER_EXTRACT_DEF, Settings.DefaultDefinitionFileFormatIndex, Settings.SetupPath, out strSaveFilePath))
                    {
                        if(strSaveFilePath.ToLower().EndsWith(".xml")) 
                            bolRetVal = objFormExtractDef.SaveAsXml(strSaveFilePath);
                        else 
                            bolRetVal = objFormExtractDef.SaveAsCxs(strSaveFilePath);
                    } 
                    else 
                    {
                        strSaveFilePath = string.Empty;
                        bolRetVal = false;
                    }

                }
            
                filePath = strSaveFilePath;

                return(bolRetVal);
            } else {
                filePath = string.Empty;
                return(false);
            }
        }

        private bool ValidateFormData() {

            bool bolRetVal = true;

            List<string> LayoutNames = new List<string>();
            cExtractDef objExtractDef = GetExtractDefFromForm();
            
            foreach(cLayout layout in objExtractDef.AllLayouts) {
                if(LayoutNames.Contains(layout.LayoutName)) {
                    OnOutputMessage(
                        "The Extract Definition cannot contain multiple Layouts with the same name [" + layout.LayoutName + "].", 
                        string.Empty, 
                        MessageType.Warning, 
                        MessageImportance.Essential);
                    bolRetVal = false;
                } else {
                    LayoutNames.Add(layout.LayoutName);
                }
            }

            return(bolRetVal);
        }
        #endregion Save Extract ExtractDef Methods

        #region Static Helper Functions
        private static LayoutLevelEnum DecodeLayoutLevel(
            string layoutLevelString) {

            switch(layoutLevelString.ToLower()) {
                case Support.LBL_BANK_LOWER:
                    return(LayoutLevelEnum.Bank);
                case Support.LBL_CLIENT_ACCOUNT_LOWER:
                    return(LayoutLevelEnum.ClientAccount);
                case Support.LBL_BATCH_LOWER:
                    return(LayoutLevelEnum.Batch);
                case Support.LBL_TRANSACTIONS_LOWER:
                    return(LayoutLevelEnum.Transaction);
                case Support.LBL_TRANSACTION_LOWER:
                    return(LayoutLevelEnum.Transaction);
                case Support.LBL_PAYMENTS_LOWER:
                    return(LayoutLevelEnum.Payment);
                case Support.LBL_PAYMENT_LOWER:
                    return (LayoutLevelEnum.Payment);
                case Support.LBL_STUBS_LOWER:
                    return(LayoutLevelEnum.Stub);
                case Support.LBL_STUB_LOWER:
                    return (LayoutLevelEnum.Stub);
                case Support.LBL_DOCUMENTS_LOWER:
                    return(LayoutLevelEnum.Document);
                case Support.LBL_DOCUMENT_LOWER:
                    return(LayoutLevelEnum.Document);
                default:
                    throw(new Exception("Invalid Layout Level: " + layoutLevelString));
            }
        }

        private static MenuItem DeepCopyMenuItem(
            MenuItem srcMenuItem) {

            MenuItem mnuRetVal;

            mnuRetVal = srcMenuItem.CloneMenu();
            CopyTags(srcMenuItem, mnuRetVal);
            
            return(mnuRetVal);
        }
        
        private static void CopyTags(
            MenuItem srcMenuItem, 
            MenuItem targetMenuItem) {

            targetMenuItem.Tag = srcMenuItem.Tag;
            
            if(srcMenuItem.MenuItems.Count > 0) {
                for(int i=0;i<srcMenuItem.MenuItems.Count;++i) {
                    CopyTags(srcMenuItem.MenuItems[i], targetMenuItem.MenuItems[i]);
                }
            }
        }

        #endregion Static Helper Functions


        // ********************************************
        // * Form Builders
        // ********************************************

        #region Build Layout Nodes
        private static void BuildLayoutNodes(
            TreeNode parentNode,
            string layoutGroupName,
            ILayout[] layouts) {

            TreeNode nodeLayoutGroup;

            nodeLayoutGroup = parentNode.Nodes.Add(layoutGroupName, layoutGroupName);
            foreach(cLayout layout in layouts) {
                BuildLayoutNode(nodeLayoutGroup, layout);
            }

            nodeLayoutGroup.Expand();
        }

        private static TreeNode BuildLayoutNode(
            TreeNode parentNode, 
            cLayout layout) {

            TreeNode nodeLayout;

            nodeLayout = new TreeNode(layout.LayoutName);
            nodeLayout.Tag = layout;
            nodeLayout.ForeColor = Color.Blue;

            foreach(cLayoutField layoutfield in layout.LayoutFields) {
                BuildLayoutFieldNode(nodeLayout, layoutfield);
            }

            parentNode.Nodes.Add(nodeLayout);
            return(nodeLayout);
        }

        internal static TreeNode BuildLayoutFieldNode(
            TreeNode parentNode,
            cLayoutField layoutField) {

            // WI 129674 : Using the display name to show the field, not the field name.
            TreeNode nodeLayoutField = new TreeNode(layoutField.DisplayName);
            nodeLayoutField.Tag = layoutField;
            parentNode.Nodes.Add(nodeLayoutField);

            return(nodeLayoutField);
        }
        #endregion Build Layout Nodes

        #region Build TreeNodes and MenuItems using ItemProcessing Data
        private bool BuildItemProcItems(
            string tableName,
            out List<cColumn> columnsList,
            out TreeNode newTreeNode,
            out MenuItem newMenuItem)
        {

            List<cColumn> objColumns;
            TreeNode objNewTreeNode;
            MenuItem objNewMenuItem;

            if (ExtractWizardBLL.GetTableColumns(tableName, out objColumns))
            {
                RemoveColumns(objColumns);
                // WI 129674 : Grab friendly names.  Handles table names in the bottom left treeview.
                objNewTreeNode = new TreeNode(cMappingLib.GetFriendly(tableName));
                BuildTreeViewColumns(objColumns, objNewTreeNode);
                objNewMenuItem = new MenuItem(cMappingLib.GetFriendly(tableName));
                BuildMenuColumns(objColumns, objNewMenuItem);

                columnsList = objColumns;
                newTreeNode = objNewTreeNode;
                newMenuItem = objNewMenuItem;

                // WI 115856 : Populate DE Columns
                if (tableName == DatabaseConstants.TABLE_NAME_FACTCHECKS || 
                    tableName == DatabaseConstants.TABLE_NAME_FACTSTUBS)
                {
                    var type = tableName == DatabaseConstants.TABLE_NAME_FACTCHECKS
                        ? DataEntryType.PaymentsDataEntry
                        : DataEntryType.StubsDataEntry;

                    // Adding a "button" for data entry search form.
                    var dataentryform = new TreeNode("Data Entry...");
                    dataentryform.Tag = type;
                    newTreeNode.Nodes.Insert(0, dataentryform);

                    var dataentryformitem = new MenuItem("Data Entry...");
                    dataentryformitem.Click += (obj, args) =>
                    {
                        var form = new DataEntryForm(Settings, type);
                        form.FormClosed += DataEntryFormClosed;
                        form.ShowDialog(this);
                    };
                    newMenuItem.MenuItems.Add(0, dataentryformitem);
                }

                return true;
            }
         
            // no assignments made above. set to null 
            columnsList = null;
            newTreeNode = null;
            newMenuItem = null;
            return false;
        }

        /// <summary>
        /// WI 115911, 116522 - Remove unwanted DB columns from nodes/menus (MostRecent). Remove non-user "Key" fields.
        /// Column names to be removed are defined in app.config.
        /// </summary>
        /// <param name="columns"></param>
        private void RemoveColumns(List<cColumn> columns)
        {
            //WI 130165 Clean up Exception handling  
            if ((columns != null) && (columns.Count > 0))
            {
                // first remove any columns that end with "Key"
                columns.RemoveAll(x => x.ColumnName.EndsWith("key", StringComparison.InvariantCultureIgnoreCase)
                    && !string.Equals(x.ColumnName, "depositdatekey", StringComparison.InvariantCultureIgnoreCase)
                    && !string.Equals(x.ColumnName, "sourceprocessingdatekey", StringComparison.InvariantCultureIgnoreCase)
                    && !string.Equals(x.ColumnName, "immutabledatekey", StringComparison.InvariantCultureIgnoreCase));

                // grab list of column names to be removed from .config
                // WI 143034 : Changed this to DisplayName.
                var s = this.Settings.EntryColumnsToRemove;
                columns.RemoveAll(o => s.Contains(o.DisplayName));
                columns.Sort((x, y) => string.Compare(x.DisplayName, y.DisplayName));
            }
        }
  
        #endregion Build TreeNodes and MenuItems using ItemProcessing Data

        #region Build Aggregate Nodes
        // WI 116533 : Utilization of Aggregate Constants instead of hard-coded strings.
        //              Affects entire function where Constants are now used.
        private void BuildAggregateNodes() {

            MenuItem mnuAggregateField;
            
            for(int i=0;i<=2;++i) {
                for(int j=0;j<=9;++j) {
                    switch(i) {
                        case 0:

                            _AggregateTreeNodes[j] = new TreeNode(Constants.NODE_LABEL_AGGREGATE_FLDS);

                            if(j<=ITEMPROC_LBX_IDX) {
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_COUNT_BATCHES, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_COUNT_BATCHES), Constants.NODE_LABEL_COUNT_BATCHES);
                            }

                            if(j<=ITEMPROC_BATCH_IDX) {
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_COUNT_TXNS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_COUNT_TXNS), Constants.NODE_LABEL_COUNT_TXNS);
                            }

                            if(j<=ITEMPROC_TXN_IDX) {
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_COUNT_CHECKS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_COUNT_CHECKS), Constants.NODE_LABEL_COUNT_CHECKS);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_COUNT_DOCS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_COUNT_DOCS), Constants.NODE_LABEL_COUNT_DOCS);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_COUNT_STUBS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_COUNT_STUBS), Constants.NODE_LABEL_COUNT_STUBS);
                            }
                            
                            break;
                        case 1:

                            if(j == (int)LayoutLevelEnum.File) {
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_FILE, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_FILE), Constants.NODE_LABEL_RECORD_COUNT_FILE);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_BANK, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_BANK), Constants.NODE_LABEL_RECORD_COUNT_BANK);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_LOCKBOX, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_LOCKBOX), Constants.NODE_LABEL_RECORD_COUNT_LOCKBOX);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_BATCH, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_BATCH), Constants.NODE_LABEL_RECORD_COUNT_BATCH);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_TRANSACTIONS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_TRANSACTIONS), Constants.NODE_LABEL_RECORD_COUNT_TRANSACTIONS);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_CHECK, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_CHECK), Constants.NODE_LABEL_RECORD_COUNT_CHECK);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_STUB, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_STUB), Constants.NODE_LABEL_RECORD_COUNT_STUB);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_DOC, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_DOC), Constants.NODE_LABEL_RECORD_COUNT_DOC);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_DETAIL, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_DETAIL), Constants.NODE_LABEL_RECORD_COUNT_DETAIL);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_RECORD_COUNT_TOTAL, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_RECORD_COUNT_TOTAL), Constants.NODE_LABEL_RECORD_COUNT_TOTAL);
                            }

                            break;
                        case 2:

                            if(j <= (int)LayoutLevelEnum.Transaction) {
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_SUM_CHECKS_AMT, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_SUM_CHECKS_AMT), Constants.NODE_LABEL_SUM_CHECKS_AMT);
                                AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_SUM_STUBS_AMT, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_SUM_STUBS_AMT), Constants.NODE_LABEL_SUM_STUBS_AMT);
                                // WI 129703 : Removed all DE (Data Entry) Fields from the UI.
                                //AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_SUM_CHECKS_DEBILLINGKEYS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_SUM_CHECKS_DEBILLINGKEYS), Constants.NODE_LABEL_SUM_CHECKS_DEBILLINGKEYS);
                                //AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_SUM_CHECKS_DEDATAKEYS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_SUM_CHECKS_DEDATAKEYS), Constants.NODE_LABEL_SUM_CHECKS_DEDATAKEYS);
                                //AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_SUM_STUBS_DEBILLINGKEYS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_SUM_STUBS_DEBILLINGKEYS), Constants.NODE_LABEL_SUM_STUBS_DEBILLINGKEYS);
                                //AddTreeNode(_AggregateTreeNodes[j], Constants.NODE_LABEL_SUM_STUBS_DEDATAKEYS, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_SUM_STUBS_DEDATAKEYS), Constants.NODE_LABEL_SUM_STUBS_DEDATAKEYS);
                            }

                            break;
                    }
                }
            }

            for(int i=0;i<=9;++i) {
                _AggregateMenuItems[i] = new MenuItem(Constants.NODE_LABEL_ADD_AGGREGATE_FIELD);
                foreach(TreeNode treenode in _AggregateTreeNodes[i].Nodes) {
                    mnuAggregateField = new MenuItem(treenode.Text, new System.EventHandler(this.mnuContext_OnClick));
                    mnuAggregateField.Tag = treenode.Tag;
                    _AggregateMenuItems[i].MenuItems.Add(mnuAggregateField);
                }
            }
        }
        #endregion Build Aggregate Nodes

        #region Build Standard Nodes
        // WI 116533 : Utilization of Standard Constants instead of hard-coded strings.
        //              Affects entire function where Constants are now used.
        private void BuildStandardNodes() {

            MenuItem mnuStandardField;
            
            for(int i=0;i<=9;++i) {

                _StandardTreeNodes[i] = new TreeNode(Constants.NODE_LABEL_STD_FIELDS);

                AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_COUNTER, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_COUNTER), Constants.NODE_LABEL_COUNTER);
                // WI 114570 removed for v2.0
                //AddTreeNode(_StandardTreeNodes[i], "CurrentProcessingDate", GetHint(Constants.NODE_STANDARD_SECTION, "CurrentProcessingDate"), "CurrentProcessingDate");
                AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_DATE, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_DATE), Constants.NODE_LABEL_DATE);
                AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_FILLER, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_FILLER), Constants.NODE_LABEL_FILLER);
                AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_FIRSTLAST, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_FIRSTLAST), Constants.NODE_LABEL_FIRSTLAST);

                if(i==ITEMPROC_PAYMENTS_IDX) {

                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_IMG_CHECK_PER_BATCH, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_IMG_CHECK_PER_BATCH), Constants.NODE_LABEL_IMG_CHECK_PER_BATCH);
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_IMG_CHECK_PER_TXN, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_IMG_CHECK_PER_TXN), Constants.NODE_LABEL_IMG_CHECK_PER_TXN);
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_IMG_CHECK_SINGLE_FRONT, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_IMG_CHECK_SINGLE_FRONT), Constants.NODE_LABEL_IMG_CHECK_SINGLE_FRONT);
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_IMG_CHECK_SINGLE_REAR, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_IMG_CHECK_SINGLE_REAR), Constants.NODE_LABEL_IMG_CHECK_SINGLE_REAR);
                    //AddTreeNode(_StandardTreeNodes[i], "ImageFileSizeFrontC", GetHint(Constants.NODE_STANDARD_SECTION, "ImageFileSizeFrontC"), "ImageFileSizeFrontC");
                    //AddTreeNode(_StandardTreeNodes[i], "ImageFileSizeRearC", GetHint(Constants.NODE_STANDARD_SECTION, "ImageFileSizeRearC"), "ImageFileSizeRearC");
                }

                //if(i==7) {

                //    AddTreeNode(_StandardTreeNodes[i], "ImageFileName", GetHint(Constants.NODE_STANDARD_SECTION, "ImageFileName"), "ImageFileName");
                //    AddTreeNode(_StandardTreeNodes[i], "ImageFileSizeFrontS", GetHint(Constants.NODE_STANDARD_SECTION, "ImageFileSizeFrontS"), "ImageFileSizeFrontS");
                //    AddTreeNode(_StandardTreeNodes[i], "ImageFileSizeRearS", GetHint(Constants.NODE_STANDARD_SECTION, "ImageFileSizeRearS"), "ImageFileSizeRearS");
                //    AddTreeNode(_StandardTreeNodes[i], "ImageStubSingleFront", GetHint(Constants.NODE_STANDARD_SECTION, "ImageStubSingleFront"), "ImageStubSingleFront");
                //    AddTreeNode(_StandardTreeNodes[i], "ImageStubSingleRear", GetHint(Constants.NODE_STANDARD_SECTION, "ImageStubSingleRear"), "ImageStubSingleRear");
                //}

                if(i==ITEMPROC_DOCS_IDX) {

                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_IMG_DOC_PER_BATCH, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_IMG_DOC_PER_BATCH), Constants.NODE_LABEL_IMG_DOC_PER_BATCH);
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_IMG_DOC_PER_TXN, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_IMG_DOC_PER_TXN), Constants.NODE_LABEL_IMG_DOC_PER_TXN);
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_IMG_DOC_SINGLE_FRONT, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_IMG_DOC_SINGLE_FRONT), Constants.NODE_LABEL_IMG_DOC_SINGLE_FRONT);
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_IMG_DOC_SINGLE_REAR, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_IMG_DOC_SINGLE_REAR), Constants.NODE_LABEL_IMG_DOC_SINGLE_REAR);
                    //AddTreeNode(_StandardTreeNodes[i], "ImageDocumentFileSizeFrontD", GetHint(Constants.NODE_STANDARD_SECTION, "ImageDocumentFileSizeFrontD"), "ImageDocumentFileSizeFrontD");
                    //AddTreeNode(_StandardTreeNodes[i], "ImageDocumentFileSizeRearD", GetHint(Constants.NODE_STANDARD_SECTION, "ImageDocumentFileSizeRearD"), "ImageDocumentFileSizeRearD");
                }
                
                AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_LINE_COUNTER_FILE, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_LINE_COUNTER_FILE), Constants.NODE_LABEL_LINE_COUNTER_FILE);

                if(i>=ITEMPROC_BANK_IDX) {
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_LINE_COUNTER_BANK, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_LINE_COUNTER_BANK), Constants.NODE_LABEL_LINE_COUNTER_BANK);
                }

                if(i>=ITEMPROC_LBX_IDX) {
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_LINE_COUNTER_LBX, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_LINE_COUNTER_LBX), Constants.NODE_LABEL_LINE_COUNTER_LBX);
                }

                if(i>=ITEMPROC_BATCH_IDX) {
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_LINE_COUNTER_BATCH, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_LINE_COUNTER_BATCH), Constants.NODE_LABEL_LINE_COUNTER_BATCH);
                }

                if(i>=ITEMPROC_TXN_IDX) {
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_LINE_COUNTER_TXN, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_LINE_COUNTER_TXN), Constants.NODE_LABEL_LINE_COUNTER_TXN);
                }

                if(i==ITEMPROC_PAYMENTS_IDX) {
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_LINE_COUNTER_CHECK, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_LINE_COUNTER_CHECK), Constants.NODE_LABEL_LINE_COUNTER_CHECK);
                }

                if(i==ITEMPROC_STUBS_IDX) {
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_LINE_COUNTER_STUB, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_LINE_COUNTER_STUB), Constants.NODE_LABEL_LINE_COUNTER_STUB);
                }

                if(i==ITEMPROC_DOCS_IDX) {
                    AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_LINE_COUNTER_DOC, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_LINE_COUNTER_DOC), Constants.NODE_LABEL_LINE_COUNTER_DOC);
                }
                
                AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_PROC_RUN_DATE, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_PROC_RUN_DATE), Constants.NODE_LABEL_PROC_RUN_DATE);
                AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_STATIC, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_STATIC), Constants.NODE_LABEL_STATIC);
                AddTreeNode(_StandardTreeNodes[i], Constants.NODE_LABEL_TIME, GetHint(Constants.NODE_STANDARD_SECTION, Constants.NODE_LABEL_TIME), Constants.NODE_LABEL_TIME);
            }

            for(int i=0;i<=9;++i) {
                _StandardMenuItems[i] = new MenuItem(Constants.NODE_LABEL_ADD_STD_FIELDS);
                foreach(TreeNode treenode in _StandardTreeNodes[i].Nodes) {
                    mnuStandardField = new MenuItem(treenode.Text, new System.EventHandler(this.mnuContext_OnClick));
                    mnuStandardField.Tag = treenode.Tag;
                    _StandardMenuItems[i].MenuItems.Add(mnuStandardField);
                }
            }
        }
        #endregion Build Standard Nodes

        #region Build Order By Nodes
        private void BuildOrderByNodes(
            string orderByGroupName,
            IOrderByColumn[] orderByColumns) {

            TreeNode node;
            
            node = tvOrderData.Nodes.Add(orderByGroupName, orderByGroupName);
            node.Tag = new List<cOrderByColumn>();
            node.ForeColor = Color.Blue;
            
            foreach(cOrderByColumn orderbycolumn in orderByColumns) {
                BuildOrderByNode(node, orderbycolumn);
            }

            node.Expand();
        }

        private static TreeNode BuildOrderByNode(
            TreeNode parentNode,
            cOrderByColumn orderByColumn) {

            TreeNode nodeOrderBy = new TreeNode(
                orderByColumn.DisplayName + (orderByColumn.OrderByDir == OrderByDirEnum.Ascending ? "" : " Descending"));

            ((List<cOrderByColumn>)parentNode.Tag).Add(orderByColumn);
            nodeOrderBy.Tag = "OrderByColumn";
            parentNode.Nodes.Add(nodeOrderBy);
            
            return(nodeOrderBy);
        }
        #endregion Build Order By Nodes

        #region Build Limit Data Nodes
        private void BuildLimitDataNodes(
            string limitDataGroupName,
            ILimitItem[] limitItems) {

            TreeNode node;
            
            node = tvLimitData.Nodes.Add(
                limitDataGroupName, 
                limitDataGroupName);

            node.Tag = new List<cLimitItem>();
            node.ForeColor = Color.Blue;
            
            foreach(cLimitItem limititem in limitItems) {
                BuildLimitItemNode(node, limititem);
            }

            node.Expand();
        }

        private static TreeNode BuildLimitItemNode(
            TreeNode parentNode,
            cLimitItem limitItem) {

            TreeNode nodeLimitItem = new TreeNode(limitItem.DisplayName);
            ((List<cLimitItem>)parentNode.Tag).Add(limitItem);
            nodeLimitItem.Tag = "LimitItem";
            parentNode.Nodes.Add(nodeLimitItem);
            
            return(nodeLimitItem);
        }
        #endregion Build Limit Data Nodes

        private static void BuildPostProcCodeModuleNode(
            TreeNode parentNode,
            cPostProcCodeModule postProcCodeModule) {

                if(!parentNode.Nodes.ContainsKey("Code Module")) {
                    parentNode.Nodes.Add("Code Module", "Code Module");
                    parentNode.Nodes["Code Module"].Tag = postProcCodeModule;
            }
        }

        #region TreeNode Builders
        private void BuildTreeViewColumns(
            List<cColumn> columns, 
            TreeNode parentNode) 
        {
                // WI 130723 - Changed ColumnName to DisplayName
                
            foreach(cColumn column in columns) 
            {
                var displayname = column.IsDataEntry
                    ? column.DisplayName + " - " + column.ColumnName
                    : column.DisplayName;
                    AddTreeNode(
                        parentNode, 
                    displayname, 
                        GetHint(
                            column.TableName, 
                            column.ColumnName), 
                        column);
                }
        }

        private static void AddTreeNode(
            TreeNode parentNode, 
            string nodeText, 
            string toolTipText, 
            object tag) {

                TreeNode node;

                node = new TreeNode(nodeText);
                node.Tag = tag;
                node.ToolTipText = toolTipText;
                parentNode.Nodes.Add(node);
        }
        #endregion TreeNode Builders

        #region MenuItem Builders
        private void BuildMenuColumns(
            List<cColumn> columns, 
            MenuItem parentMenu) 
        {

                MenuItem mnuDataColumn;
                // WI 130723 - Changed ColumnName to DisplayName
            foreach(cColumn column in columns) 
            {
                var displayname = column.IsDataEntry
                    ? column.DisplayName + " - " + column.ColumnName
                    : column.DisplayName;
                    mnuDataColumn = new MenuItem(
                    displayname, 
                        new System.EventHandler(this.mnuContext_OnClick));

                    mnuDataColumn.Tag = column;
                    parentMenu.MenuItems.Add(mnuDataColumn);
                }
        }
        #endregion MenuItem Builders

        #region Context Menu Builders
        private bool BuildLayoutsContextMenu(
            string fieldText, 
            out ContextMenu newMenu) {

            ContextMenu mnuContextMenu = null;
            bool bolRetVal = true;

            switch(fieldText) {
                case Support.LBL_FILE_LAYOUTS:
                    BuildContextMenuItem("New " + Support.LBL_FILE + " Layout", out mnuContextMenu, this.mnuNewLayout_OnClick);
                    break;
                case Support.LBL_BANK_LAYOUTS:
                    BuildContextMenuItem("New " + Support.LBL_BANK + " Layout", out mnuContextMenu, this.mnuNewLayout_OnClick);
                    break;
                case Support.LBL_CLIENT_ACCOUNT_LAYOUTS:
                    BuildContextMenuItem("New " + Support.LBL_CLIENT_ACCOUNT + " Layout", out mnuContextMenu, this.mnuNewLayout_OnClick);
                    break;
                case Support.LBL_BATCH_LAYOUTS:
                    BuildContextMenuItem("New " + Support.LBL_BATCH + " Layout", out mnuContextMenu, this.mnuNewLayout_OnClick);
                    break;
                case Support.LBL_TXN_LAYOUTS:
                    BuildContextMenuItem("New " + Support.LBL_TRANSACTION + " Layout", out mnuContextMenu, this.mnuNewLayout_OnClick);
                    break;
                case Support.LBL_PAYMENT_LAYOUTS:
                    BuildContextMenuItem("New " + Support.LBL_PAYMENT + " Layout", out mnuContextMenu, this.mnuNewLayout_OnClick);
                    break;
                case Support.LBL_STUB_LAYOUTS:
                    BuildContextMenuItem("New " + Support.LBL_STUB + " Layout", out mnuContextMenu, this.mnuNewLayout_OnClick);
                    break;
                case Support.LBL_DOCUMENT_LAYOUTS:
                    BuildContextMenuItem("New " + Support.LBL_DOCUMENT + " Layout", out mnuContextMenu, this.mnuNewLayout_OnClick);
                    break;
                case Support.LBL_JOINT_DETAIL_LAYOUTS:
                    BuildContextMenuItem(new string[] {"New " + Support.LBL_JOINT_DETAIL + " Layout"}, 
                                         out mnuContextMenu, 
                                         this.mnuNewLayout_OnClick);
                    break;
                default:
                    bolRetVal = false;
                    break;
            }

            newMenu = mnuContextMenu;
            
            return(bolRetVal);
        }

        private bool BuildLayoutContextMenu(
            cLayout layout, 
            out ContextMenu newMenu) {

            MenuItem mnuAddLayoutField = null;
            MenuItem mnuAddAggregateField = null;
            MenuItem mnuAddStandardField = null;
            ContextMenu mnuContextMenu;
            bool bolRetVal = false;
                    
            mnuContextMenu = new ContextMenu();
            mnuAddLayoutField = new MenuItem("Add Data Field");

            switch(layout.LayoutLevel) {
                case LayoutLevelEnum.File:
                    mnuAddAggregateField = DeepCopyMenuItem(_AggregateMenuItems[0]);
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[0]);
                    break;
                case LayoutLevelEnum.Bank:
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BANK_IDX]));
                    mnuAddAggregateField = DeepCopyMenuItem(_AggregateMenuItems[1]);
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[1]);
                    break;
                case LayoutLevelEnum.ClientAccount:
                    // WI 129678 : Uncommented parent heirarchy data.
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BANK_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_LBX_IDX]));
                    mnuAddAggregateField = DeepCopyMenuItem(_AggregateMenuItems[3]);
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[3]);
                    break;
                case LayoutLevelEnum.Batch:
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BANK_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_LBX_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BATCH_IDX]));
                    mnuAddAggregateField = DeepCopyMenuItem(_AggregateMenuItems[4]);
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[4]);
                    break;
                case LayoutLevelEnum.Transaction:
                    // WI 129678 : Uncommented parent heirarchy data.
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BANK_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_LBX_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BATCH_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_TXN_IDX]));
                    mnuAddAggregateField = DeepCopyMenuItem(_AggregateMenuItems[5]);
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[5]);
                    break;
                case LayoutLevelEnum.Payment:
                    // WI 129678 : Uncommented parent heirarchy data.
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BANK_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_LBX_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BATCH_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_TXN_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_PAYMENTS_IDX]));
                    mnuAddAggregateField = DeepCopyMenuItem(_AggregateMenuItems[6]);
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[6]);
                    break;
                case LayoutLevelEnum.Stub:
                    // WI 129678 : Uncommented parent heirarchy data.
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BANK_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_LBX_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BATCH_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_TXN_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_STUBS_IDX]));
                    mnuAddAggregateField = DeepCopyMenuItem(_AggregateMenuItems[7]);
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[7]);
                    break;
                case LayoutLevelEnum.Document:
                    // WI 129678 : Uncommented parent heirarchy data.
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BANK_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_LBX_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BATCH_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_TXN_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_DOCS_IDX]));
                    mnuAddAggregateField = DeepCopyMenuItem(_AggregateMenuItems[8]);
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[8]);
                    break;
                case LayoutLevelEnum.JointDetail:
                    // WI 129678 : Uncommented parent heirarchy data.
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BANK_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_LBX_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_BATCH_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_TXN_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_PAYMENTS_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_STUBS_IDX]));
                    mnuAddLayoutField.MenuItems.Add(DeepCopyMenuItem(_ItemProcMenuItems[ITEMPROC_DOCS_IDX]));
                    mnuAddStandardField = DeepCopyMenuItem(_StandardMenuItems[5]);
                    break;
            }
            
            if(mnuAddLayoutField != null && mnuAddLayoutField.MenuItems.Count > 0) {
                mnuContextMenu.MenuItems.Add(mnuAddLayoutField);
            }

            if(mnuAddAggregateField != null && mnuAddAggregateField.MenuItems.Count > 0) {
                mnuContextMenu.MenuItems.Add(mnuAddAggregateField);
            }

            if(mnuAddStandardField != null && mnuAddStandardField.MenuItems.Count > 0) {
                mnuContextMenu.MenuItems.Add(mnuAddStandardField);
            }

            mnuContextMenu.MenuItems.Add("-");
            mnuContextMenu.MenuItems.Add("Delete", mnuContext_OnClick);
            mnuContextMenu.MenuItems.Add("Rename", mnuContext_OnClick);

            newMenu = mnuContextMenu;
            bolRetVal = true;
            
            return(bolRetVal);
        }

        private bool BuildLayoutFieldContextMenu(
            out ContextMenu newMenu) {

            return(BuildContextMenuItem(
                "Delete Layout Field", 
                out newMenu, 
                this.mnuContext_OnClick));
        }
    
        private bool BuildGeneralContextMenu(
            out ContextMenu newMenu) {

            ContextMenu mnuContextMenu;
            MenuItem objItem;
            
            if(this.tvLayouts.Nodes["General"].Nodes.ContainsKey("Code Module")) {
                objItem = new MenuItem("Add Post-Processing Code Module");
                objItem.Enabled = false;
            } else {
                objItem = new MenuItem("Add Post-Processing Code Module", new System.EventHandler(this.mnuContext_OnClick));
            }
            
            mnuContextMenu = new ContextMenu();
            mnuContextMenu.MenuItems.Add(objItem);
            newMenu = mnuContextMenu;
            
            return(true);
        }

        private bool BuildOrderByColumnsContextMenu(
            LayoutLevelEnum layoutLevel, 
            out ContextMenu newMenu) {
            
            return(BuildItemProcContextMenu(
                "Add Order By Column", 
                layoutLevel, 
                out newMenu));
        }

        private bool BuildOrderByColumnContextMenu(
            out ContextMenu newMenu) {

            return(BuildContextMenuItem(
                "Delete Order By Column", 
                out newMenu, 
                this.mnuContext_OnClick));
        }
       
        private bool BuildLimitItemsContextMenu(
            LayoutLevelEnum layoutLevel, 
            out ContextMenu newMenu) {
            
            return(BuildItemProcContextMenu(
                "Add Limit Item", 
                layoutLevel, 
                out newMenu));
        }        
        
        private bool BuildLimitItemContextMenu(
            out ContextMenu newMenu) {

            return(BuildContextMenuItem(
                "Delete Limit Item", 
                out newMenu, 
                this.mnuContext_OnClick));
        }

        private bool BuildPostProcCodeModuleContextMenu(
            out ContextMenu newMenu) {
            
            return(BuildContextMenuItem(
                "Delete Post-Processing Code Module", 
                out newMenu, 
                this.mnuContext_OnClick));
        }

        private bool BuildItemProcContextMenu(
            string menuText, 
            LayoutLevelEnum layoutLevel, 
            out ContextMenu newMenu) {

            MenuItem mnuAddLimitItem = null;
            MenuItem mnuAddLimitItemSubItem = null;
            ContextMenu mnuContextMenu;
            bool bolRetVal = false;
                    
            mnuContextMenu = new ContextMenu();
            mnuAddLimitItem = new MenuItem(menuText);
                       
            if(layoutLevel == LayoutLevelEnum.Payment) {
                mnuAddLimitItemSubItem = new MenuItem(Support.LBL_PAYMENTS);
                foreach(MenuItem item in _ItemProcMenuItems[ITEMPROC_PAYMENTS_IDX].MenuItems) {
                    mnuAddLimitItemSubItem.MenuItems.Add(DeepCopyMenuItem(item));
                }
                mnuAddLimitItem.MenuItems.Add(mnuAddLimitItemSubItem);
                // WI 130723 : Removed DE references.
                //mnuAddLimitItemSubItem = new MenuItem(Support.LBL_PAYMENTS_DATA_ENTRY);
                //foreach(MenuItem item in _ItemProcMenuItems[ITEMPROC_PAYMENTSDE_IDX].MenuItems) {
                //    mnuAddLimitItemSubItem.MenuItems.Add(DeepCopyMenuItem(item));
                //}
                //mnuAddLimitItem.MenuItems.Add(mnuAddLimitItemSubItem);
            } else if(layoutLevel == LayoutLevelEnum.Stub) {
                mnuAddLimitItemSubItem = new MenuItem(Support.LBL_STUBS);
                foreach(MenuItem item in _ItemProcMenuItems[ITEMPROC_STUBS_IDX].MenuItems) {
                    mnuAddLimitItemSubItem.MenuItems.Add(DeepCopyMenuItem(item));
                }
                mnuAddLimitItem.MenuItems.Add(mnuAddLimitItemSubItem);
                // WI 130723 : Removed DE references.
                //mnuAddLimitItemSubItem = new MenuItem(Support.LBL_STUBS_DATA_ENTRY);
                //foreach(MenuItem item in _ItemProcMenuItems[ITEMPROC_STUBSDE_IDX].MenuItems) {
                //    mnuAddLimitItemSubItem.MenuItems.Add(DeepCopyMenuItem(item));
                //}
                //mnuAddLimitItem.MenuItems.Add(mnuAddLimitItemSubItem);
            } else {
                foreach(MenuItem item in _ItemProcMenuItems[(int)layoutLevel].MenuItems) {
                    mnuAddLimitItem.MenuItems.Add(DeepCopyMenuItem(item));
                }
            }

            if(mnuAddLimitItem != null && mnuAddLimitItem.MenuItems.Count > 0) {
                mnuContextMenu.MenuItems.Add(mnuAddLimitItem);
            }

            newMenu = mnuContextMenu;
            bolRetVal = true;
            
            return(bolRetVal);
        }        

        private static bool BuildContextMenuItem(
            string menuLabel, 
            out ContextMenu newContextMenu, 
            EventHandler handler) {

            ContextMenu mnuContextMenu;

            try
            {

                mnuContextMenu = new ContextMenu();
                mnuContextMenu.MenuItems.Add(new MenuItem(
                    menuLabel,
                    new System.EventHandler(handler)));

                newContextMenu = mnuContextMenu;

                return (true);

            }
            catch (Exception ex)
            {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                newContextMenu = null;
                return (false);
            }
        }

        private static bool BuildContextMenuItem(
            string[] menuLabel, 
            out ContextMenu newContextMenu, 
            EventHandler handler) {

            ContextMenu mnuContextMenu;
            MenuItem mnuAddLayout;

            try {

                mnuContextMenu = new ContextMenu();

                foreach(string label in menuLabel) {
                    mnuAddLayout = new MenuItem(label, new System.EventHandler(handler));
                    mnuContextMenu.MenuItems.Add(mnuAddLayout);
                }

                newContextMenu = mnuContextMenu;
                
                return(true);

            } catch(Exception ex) 
            {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                newContextMenu = null;
                return(false);
            }
        }

        #endregion Context Menu Builders

        // ********************************************
        // * Form State Management
        // ********************************************

        #region Form State Management
        private void InitializeFormValues() {
            var status = true;
            // WI 137747 : Break out a little early to prevent additional pop-ups if we're failing on SP calls.
            status = status 
                ? BuildItemProcItems(DatabaseConstants.TABLE_NAME_DIMBANKS, out _BankColumns, out _ItemProcTreeNodes[ITEMPROC_BANK_IDX], out _ItemProcMenuItems[ITEMPROC_BANK_IDX])
                : false;
            status = status 
                ? BuildItemProcItems(DatabaseConstants.TABLE_NAME_DIMCLIENTACCOUNTS, out _LockboxColumns, out _ItemProcTreeNodes[ITEMPROC_LBX_IDX], out _ItemProcMenuItems[ITEMPROC_LBX_IDX])
                : false;
            status = status 
                ? BuildItemProcItems(DatabaseConstants.TABLE_NAME_FACTBATCH, out _BatchColumns, out _ItemProcTreeNodes[ITEMPROC_BATCH_IDX], out _ItemProcMenuItems[ITEMPROC_BATCH_IDX])
                : false;
            status = status 
                ? BuildItemProcItems(DatabaseConstants.TABLE_NAME_FACTTXN, out _TransactionsColumns, out _ItemProcTreeNodes[ITEMPROC_TXN_IDX], out _ItemProcMenuItems[ITEMPROC_TXN_IDX])
                : false;
            status = status 
                ? BuildItemProcItems(DatabaseConstants.TABLE_NAME_FACTCHECKS, out _PaymentsColumns, out _ItemProcTreeNodes[ITEMPROC_PAYMENTS_IDX], out _ItemProcMenuItems[ITEMPROC_PAYMENTS_IDX])
                : false;
            status = status 
                ? BuildItemProcItems(DatabaseConstants.TABLE_NAME_FACTSTUBS, out _StubsColumns, out _ItemProcTreeNodes[ITEMPROC_STUBS_IDX], out _ItemProcMenuItems[ITEMPROC_STUBS_IDX])
                : false;
            status = status 
                ? BuildItemProcItems(DatabaseConstants.TABLE_NAME_FACTDOCUMENTS, out _DocumentsColumns, out _ItemProcTreeNodes[ITEMPROC_DOCS_IDX], out _ItemProcMenuItems[ITEMPROC_DOCS_IDX])
                : false;

            _ItemProcColumnsInitialized = true;
        }

        private void SetFormState(FormState state) {

            switch(state) {
                case FormState.Init:

                    ClearForm();

                    for(int i=this.pnlMain.Controls.Count-1;i>=0;--i) {
                        if(this.pnlMain.Controls[i] is ctlExtractRun) {
                            this.pnlMain.Controls.RemoveAt(i);
                        }
                    }

                    ShowSplashScreen();

                    break;

                case FormState.Edit:

                    this.mnuFileClose.Enabled = true;
                    this.mnuFilePrint.Enabled = true;
                    this.mnuFileSetupRpt.Enabled = true;
                    this.mnuFileSave.Enabled = true;
                    this.mnuFileSaveAs.Enabled = true;
                    this.mnuRun.Enabled = true;
                    this.mnuViewExtractDefinition.Enabled = false;

                    for(int i=this.pnlMain.Controls.Count-1;i>=0;--i) {
                        if(this.pnlMain.Controls[i] is ctlExtractRun) {
                            this.pnlMain.Controls.RemoveAt(i);
                        }
                    }

                    HideSplashScreen();

                    break;

                case FormState.Run:

                    this.mnuFileClose.Enabled = true;
                    this.mnuFilePrint.Enabled = false;
                    this.mnuFileSetupRpt.Enabled = false;
                    this.mnuFileSave.Enabled = false;
                    this.mnuFileSaveAs.Enabled = false;
                    this.mnuRun.Enabled = false;
                    this.mnuViewExtractDefinition.Enabled = true;

                    HideSplashScreen();

                    break;
            }

            _CurrentFormState = state;
        }
        
        private void ClearForm() {

            this.tvLayouts.Nodes.Clear();

            _InputFileName = string.Empty;

            while(this.splitBody.Panel2.Controls.Count > 0) {
                this.splitBody.Panel2.Controls[0].Dispose();
            }

            while(this.tvLayouts.Nodes.Count > 0) {
                this.tvLayouts.Nodes.RemoveAt(0);
            }
            
            while(this.tvDataFields.Nodes.Count > 0) {
                this.tvDataFields.Nodes.RemoveAt(0);
            }

            while(this.tvOrderData.Nodes.Count > 0) {
                this.tvOrderData.Nodes.RemoveAt(0);
            }

            while(this.tvLimitData.Nodes.Count > 0) {
                this.tvLimitData.Nodes.RemoveAt(0);
            }
            
            this.tabExtractDef.SelectedIndex = 0;
            this.txtExtractScriptPath.Text = string.Empty;

            this.mnuFileClose.Enabled = false;
            this.mnuFilePrint.Enabled = false;
            this.mnuFileSetupRpt.Enabled = false;
            this.mnuFileSave.Enabled = false;
            this.mnuFileSaveAs.Enabled = false;

            this.mnuViewExtractDefinition.Enabled = false;

            this.mnuRun.Enabled = true;
            
            OnOutputStatusMessage("Ready");
            this.Text = "Extract Design Manager - [No Selection]";
        }

        private void ShowSplashScreen() {

            bool bolSplashSet = false;
            PictureBox objSplash;
            cLogo Logo;
            ResourceManager res;

            foreach(Control ctl in this.pnlMain.Controls) {
                if(ctl is PictureBox) {
                    ctl.Visible = true;
                    bolSplashSet = true;
                    break;
                }
            }

            if(!bolSplashSet) {

                objSplash = new PictureBox();

                string sLogoPath = cAppSettingsLib.Get(ExtractAppSettings.LogoPath);
                string bg = cAppSettingsLib.Get(ExtractAppSettings.BackgroundColor, "#000000");

                if (sLogoPath.Length > 0) {

                    if(!sLogoPath.Contains("\\")) {
                        
                        string sAssemblyLocation = string.Empty;
                        string sBinPath = string.Empty;

                        if (Assembly.GetEntryAssembly() == null)
                            sAssemblyLocation = Assembly.GetCallingAssembly().Location;
                        else
                            sAssemblyLocation = Assembly.GetEntryAssembly().Location;

                        sBinPath = sAssemblyLocation.Substring(0, sAssemblyLocation.LastIndexOf(@"\"));
                            
                        sLogoPath = Path.Combine(sBinPath, sLogoPath);
                    }
                }

                Logo = new cLogo(sLogoPath, bg);

                if (Logo.LogoImage != null) {
                    objSplash.Image = Logo.LogoImage;
                } else {
                    res = new ResourceManager(typeof(frmMain));
                    objSplash.Image = Resource1.wausau_splash;
                }

                objSplash.Dock = DockStyle.Fill;
                objSplash.SizeMode = PictureBoxSizeMode.CenterImage;

                if(Logo != null) {
                    objSplash.BackColor = Logo.BackgroundColor;
                } else {
                    objSplash.BackColor = Color.Black;
                }
                this.pnlMain.Controls.Add(objSplash);
                this.pnlMain.Controls.SetChildIndex(objSplash, 0);
            }
        }

        private void HideSplashScreen() {
            for(int i=0;i<this.pnlMain.Controls.Count;++i) {
                if(this.pnlMain.Controls[i] is PictureBox) {
                    this.pnlMain.Controls[i].Visible = false;
                    break;
                }
            }
        }

        private void BuildRunCtl() {

            cExtractParms Parms;
            ctlExtractRun ExtractRunCtl;

            Parms = new cExtractParms();
            Parms.SetupFile = _InputFileName;

            Parms.UserID = _UserID;

            if(this.pnlMain.Controls[0] is ctlExtractRun) {
            
                Parms.BankID = ((ctlExtractRun)this.pnlMain.Controls[0]).BankID;
                Parms.LockboxID = ((ctlExtractRun)this.pnlMain.Controls[0]).LockboxID;
                Parms.BatchID = ((ctlExtractRun)this.pnlMain.Controls[0]).BatchID;
                Parms.DepositDateFrom = ((ctlExtractRun)this.pnlMain.Controls[0]).DepositDateFrom;
                Parms.DepositDateTo = ((ctlExtractRun)this.pnlMain.Controls[0]).DepositDateTo;
                Parms.ProcessingDateFrom = ((ctlExtractRun)this.pnlMain.Controls[0]).ProcessingDateFrom;
                Parms.ProcessingDateTo = ((ctlExtractRun)this.pnlMain.Controls[0]).ProcessingDateTo;

                ((ctlExtractRun)this.pnlMain.Controls[0]).LoadExtractParms(GetExtractDefFromForm(), Parms, true);

            } else {

                ExtractRunCtl = new ctlExtractRun(GetExtractDefFromForm(),Parms, true);
                
                ExtractRunCtl.OutputStatusMsg += this.OnOutputStatusMessage;               
                ExtractRunCtl.EndExtractRun += this.ExtractRunCtl_EndExtractRun;
                ExtractRunCtl.BeginExtractRun += this.ExtractRunCtl_BeginExtractRun;
                ExtractRunCtl.Dock = DockStyle.Fill;
                this.pnlMain.Controls.Add(ExtractRunCtl);
                this.pnlMain.Controls.SetChildIndex(ExtractRunCtl, 0);
            }
        }
        
        internal void SynchCurrentNodes() {
            if(this.splitBody.Panel2.Controls.Count > 0) {
                if(this.splitBody.Panel2.Controls[0] is ctlGeneral) {
                    if(((ctlGeneral)this.splitBody.Panel2.Controls[0]).IsDirty) {
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ExtractPath = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ExtractFilePath;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).FieldDelim = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).FieldDelimiter;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ImageFileFormat = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ImageFileFormat;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ImagePath = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ImageFilePath;

                        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).IncludeFooter = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).IncludeFooter;
                        //((cExtractDef)this.tvLayouts.Nodes["General"].Tag).CombineImagesAccrossProcDates = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).CombineImagesAccrossProcDates;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ImageFileNamePerBatch = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ImageFileNamePerBatch;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ImageFileNamePerTran = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ImageFileNamePerTran;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ImageFileNameSingle = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ImageFileNameSingle;

                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ImageFileZeroPad = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ImageFileZeroPad;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ImageFileProcDateFormat = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ImageFileProcDateFormat;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ImageFileBatchTypeFormatFull = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ImageFileBatchTypeFormatFull;

                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).IncludeImageFolderPath = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).IncludeImageFolderPath;

                        //((cExtractDef)this.tvLayouts.Nodes["General"].Tag).IncludeNULLRows = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).IncludeNULLRows;

                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).LogFilePath = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).LogFilePath;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).NoPad = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).NoPad;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).NullAsSpace = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).NullAsSpace;

                        if(((ctlGeneral)this.splitBody.Panel2.Controls[0]).UsePostProcessingDLL) {
                            ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).PostProcDLLFileName = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).PostProcessingDLLFileName;
                        } else {
                            ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).PostProcDLLFileName = string.Empty;
                        }

                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).RecordDelim = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).RecordDelimiter;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).TimeStamp = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).TimeStamp;
                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).UseQuotes = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).UseQuotes;

                        ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).PrinterName = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).PrinterName;

                        if(((ctlGeneral)this.splitBody.Panel2.Controls[0]).ZipOutputFiles) {
                            ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ZipOutputFiles = true;
                            ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ZipFilePath = ((ctlGeneral)this.splitBody.Panel2.Controls[0]).ZipFilePath;
                        } else {
                            ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ZipOutputFiles = false;
                            ((cExtractDef)this.tvLayouts.Nodes["General"].Tag).ZipFilePath = string.Empty;
                        }
                        
                    }
                } else if(this.splitBody.Panel2.Controls[0] is ctlLayout) {
                    if(this.tvLayouts.SelectedNode != null &&
                       this.tvLayouts.SelectedNode.Tag != null && 
                       this.tvLayouts.SelectedNode.Tag is cLayout) {

                        if(((ctlLayout)this.splitBody.Panel2.Controls[0]).IsDirty) {
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).LayoutType = ((ctlLayout)this.splitBody.Panel2.Controls[0]).LayoutType;
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).UseOccursGroups = ((ctlLayout)this.splitBody.Panel2.Controls[0]).UseOccursGroups;
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).UsePaymentsOccursGroups = ((ctlLayout)this.splitBody.Panel2.Controls[0]).UsePaymentsOccursGroups;
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).UseStubsOccursGroups = ((ctlLayout)this.splitBody.Panel2.Controls[0]).UseStubsOccursGroups;
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).UseDocumentsOccursGroups = ((ctlLayout)this.splitBody.Panel2.Controls[0]).UseDocumentsOccursGroups;
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).PaymentsOccursCount = ((ctlLayout)this.splitBody.Panel2.Controls[0]).PaymentsOccursCount;
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).StubsOccursCount = ((ctlLayout)this.splitBody.Panel2.Controls[0]).StubsOccursCount;
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).DocumentsOccursCount = ((ctlLayout)this.splitBody.Panel2.Controls[0]).DocumentsOccursCount;
                            ((cLayout)this.tvLayouts.SelectedNode.Tag).MaxRepeats = ((ctlLayout)this.splitBody.Panel2.Controls[0]).MaxRepeats;
                        }

                        ((ctlLayout)this.splitBody.Panel2.Controls[0]).AskToReconcileIfNecessary();
                    }
                } else if(this.splitBody.Panel2.Controls[0] is ctLayoutField) {
                    if(((ctLayoutField)this.splitBody.Panel2.Controls[0]).IsDirty) {
                        ((cLayout)this.tvLayouts.SelectedNode.Parent.Tag).LayoutFields[((ctLayoutField)this.splitBody.Panel2.Controls[0]).Index].DisplayName = ((ctLayoutField)this.splitBody.Panel2.Controls[0]).DisplayName;
                        ((cLayout)this.tvLayouts.SelectedNode.Parent.Tag).LayoutFields[((ctLayoutField)this.splitBody.Panel2.Controls[0]).Index].ColumnWidth = ((ctLayoutField)this.splitBody.Panel2.Controls[0]).ColumnWidth;
                        ((cLayout)this.tvLayouts.SelectedNode.Parent.Tag).LayoutFields[((ctLayoutField)this.splitBody.Panel2.Controls[0]).Index].Format = ((ctLayoutField)this.splitBody.Panel2.Controls[0]).Format;
                        ((cLayout)this.tvLayouts.SelectedNode.Parent.Tag).LayoutFields[((ctLayoutField)this.splitBody.Panel2.Controls[0]).Index].Justification = ((ctLayoutField)this.splitBody.Panel2.Controls[0]).Justification;
                        ((cLayout)this.tvLayouts.SelectedNode.Parent.Tag).LayoutFields[((ctLayoutField)this.splitBody.Panel2.Controls[0]).Index].PadChar = ((ctLayoutField)this.splitBody.Panel2.Controls[0]).PadChar;
                        ((cLayout)this.tvLayouts.SelectedNode.Parent.Tag).LayoutFields[((ctLayoutField)this.splitBody.Panel2.Controls[0]).Index].Quotes = ((ctLayoutField)this.splitBody.Panel2.Controls[0]).Quotes;
                    }                
                } else if(this.splitBody.Panel2.Controls[0] is ctlLimitItems) {
                
                } else if(this.splitBody.Panel2.Controls[0] is ctlOrderByColumns) {

                } else if(this.splitBody.Panel2.Controls[0] is ctlOrderByColumn) {
                    if(((ctlOrderByColumn)this.splitBody.Panel2.Controls[0]).IsDirty) {
                        ((List<cOrderByColumn>)this.tvOrderData.SelectedNode.Parent.Tag)[((ctlOrderByColumn)this.splitBody.Panel2.Controls[0]).Index].OrderByDir = ((ctlOrderByColumn)this.splitBody.Panel2.Controls[0]).OrderByDir;
                    }                
                } else if(this.splitBody.Panel2.Controls[0] is ctlLimitItem) {
                    if(((ctlLimitItem)this.splitBody.Panel2.Controls[0]).IsDirty) {
                        ((List<cLimitItem>)this.tvLimitData.SelectedNode.Parent.Tag)[((ctlLimitItem)this.splitBody.Panel2.Controls[0]).Index].LogicalOperator = ((ctlLimitItem)this.splitBody.Panel2.Controls[0]).LogicalOperator;
                        ((List<cLimitItem>)this.tvLimitData.SelectedNode.Parent.Tag)[((ctlLimitItem)this.splitBody.Panel2.Controls[0]).Index].Value = ((ctlLimitItem)this.splitBody.Panel2.Controls[0]).Value;
                        ((List<cLimitItem>)this.tvLimitData.SelectedNode.Parent.Tag)[((ctlLimitItem)this.splitBody.Panel2.Controls[0]).Index].LeftParenCount = ((ctlLimitItem)this.splitBody.Panel2.Controls[0]).LeftParenCount;
                        ((List<cLimitItem>)this.tvLimitData.SelectedNode.Parent.Tag)[((ctlLimitItem)this.splitBody.Panel2.Controls[0]).Index].RightParenCount = ((ctlLimitItem)this.splitBody.Panel2.Controls[0]).RightParenCount;
                        ((List<cLimitItem>)this.tvLimitData.SelectedNode.Parent.Tag)[((ctlLimitItem)this.splitBody.Panel2.Controls[0]).Index].CompoundOperator = ((ctlLimitItem)this.splitBody.Panel2.Controls[0]).CompoundOperator;
                    }                
                } else if(this.splitBody.Panel2.Controls[0] is ctlPostProcCode) {
                    if(((ctlPostProcCode)this.splitBody.Panel2.Controls[0]).IsDirty) {
                        ((cPostProcCodeModule)this.tvLayouts.SelectedNode.Tag).SrcCode = ((ctlPostProcCode)this.splitBody.Panel2.Controls[0]).SrcCode;
                        ((cPostProcCodeModule)this.tvLayouts.SelectedNode.Tag).HashCode = HashGenerator.SHA512(((ctlPostProcCode)this.splitBody.Panel2.Controls[0]).SrcCode);
                    }
                }
            }
        }

        #endregion Form State Management

        #region tvDataFields Node Management
        private void AddDataFieldsToTv(LayoutLevelEnum layoutLevel) {

            bool bolLayoutShouldBeDisplayed;
            int intInsertPos;

            if(layoutLevel == LayoutLevelEnum.File) {
                if(this.tvDataFields.Nodes.ContainsKey("Data Fields")) {
                    this.tvDataFields.Nodes.RemoveByKey("Data Fields");
                }
            } else {
                if(!this.tvDataFields.Nodes.ContainsKey("Data Fields")) {
                    this.tvDataFields.Nodes.Insert(0, "Data Fields", "Data Fields");
                }

                for(int i=1;i<=10;++i) {

                    // Determine whether the given node should be displayed for the specified LayoutLevel.
                    // WI 130723 : Removed DE references.
                    // Always show the current layout level data fields.
                    if(i == (int)layoutLevel) {
                        bolLayoutShouldBeDisplayed = true;
                    // When showing Checks, also show Checks Data Entry fields.
                    } 
                    //else if(layoutLevel == LayoutLevelEnum.Payment && i == ITEMPROC_PAYMENTSDE_IDX) {
                    //    bolLayoutShouldBeDisplayed = true;
                    // When showing Stubs, also show Stubs Data Entry fields.
                    //} 
                    //else if(layoutLevel == LayoutLevelEnum.Stub && i == ITEMPROC_STUBSDE_IDX) {
                    //    bolLayoutShouldBeDisplayed = true;
                    // Show data fields for all tables above the current level in the heirarchy.
                    // If we are below the transaction level, then it gets a bit trickier.
                    //} 
                    else if(this.tabExtractDef.SelectedIndex == 0 &&
                              i < (int)layoutLevel && i <= ITEMPROC_TXN_IDX) {
                        bolLayoutShouldBeDisplayed = true;
                    // When showing Joint Detail show all Checks, Stubs, and Documents fields.
                    } 
                    else if(this.tabExtractDef.SelectedIndex == 0 &&
                              layoutLevel == LayoutLevelEnum.JointDetail && (
                                    i == ITEMPROC_PAYMENTS_IDX || 
                                    //i == ITEMPROC_PAYMENTSDE_IDX || 
                                    i == ITEMPROC_STUBS_IDX || 
                                    //i == ITEMPROC_STUBSDE_IDX || 
                                    i == ITEMPROC_DOCS_IDX)) {
                        bolLayoutShouldBeDisplayed = true;
                    } else {
                        bolLayoutShouldBeDisplayed = false;
                    }

                    // If the node is not visible, should it be?
                    if(bolLayoutShouldBeDisplayed && !this.tvDataFields.Nodes["Data Fields"].Nodes.Contains(_ItemProcTreeNodes[i])) {

                        intInsertPos = 0;

                        // This is kind of convoluted, but the gist of this is that if we need to insert a data node, we
                        // need to insert it in the correct position.  This would be easy, but in order to keep the indexing
                        // of the _ItemProcTreeNodes local array in line with the LayoutLevelEnum Enumeration, ChecksDataEntry
                        // and StubsDataEntry need to be at the end of the array even though they need to be displayed inline
                        // immediately after the Checks and Stubs table nodes respectively.
                        if(this.tvDataFields.Nodes["Data Fields"].Nodes.Count > 0) {
                            for(int j=0;j<=8;++j) {
                                if(this.tvDataFields.Nodes["Data Fields"].Nodes.Contains(_ItemProcTreeNodes[j])) {
                                    //if(i == ITEMPROC_PAYMENTSDE_IDX && j == ITEMPROC_PAYMENTS_IDX) {
                                    //    intInsertPos = _ItemProcTreeNodes[j].Index + 1;
                                    //    break;
                                //    } 
                                //else if(i == ITEMPROC_STUBSDE_IDX && j == ITEMPROC_STUBS_IDX) {
                                //        intInsertPos = _ItemProcTreeNodes[j].Index + 1;
                                //        break;
                                    //} 
                                if(i < j) {
                                        intInsertPos = _ItemProcTreeNodes[j].Index;
                                        break;
                                    } 
                                else {
                                        //if(_ItemProcTreeNodes[j].Index < this.tvDataFields.Nodes["Data Fields"].Nodes.Count - 1 &&
                                        //   (this.tvDataFields.Nodes["Data Fields"].Nodes[_ItemProcTreeNodes[j].Index + 1] == _ItemProcTreeNodes[ITEMPROC_PAYMENTSDE_IDX] ||
                                        //    this.tvDataFields.Nodes["Data Fields"].Nodes[_ItemProcTreeNodes[j].Index + 1] == _ItemProcTreeNodes[ITEMPROC_STUBSDE_IDX])) {

                                        //    intInsertPos = _ItemProcTreeNodes[j].Index + 2;
                                        //} else {
                                            intInsertPos = _ItemProcTreeNodes[j].Index + 1;
                                        //}
                                    }
                                }
                            }
                        }

                        if (_ItemProcTreeNodes[i] != null)
                            this.tvDataFields.Nodes["Data Fields"].Nodes.Insert(intInsertPos, _ItemProcTreeNodes[i]);

                    // If the node is already visible, should it be?
                    } else if(!bolLayoutShouldBeDisplayed && this.tvDataFields.Nodes["Data Fields"].Nodes.Contains(_ItemProcTreeNodes[i])) {
                        this.tvDataFields.Nodes["Data Fields"].Nodes.Remove(_ItemProcTreeNodes[i]);
                    }
                }

                if(_DataFieldsExpanded) {
                    this.tvDataFields.Nodes["Data Fields"].Expand();
                } else {
                    this.tvDataFields.Nodes["Data Fields"].Collapse();
                }
            }

            SetAddDataFieldButtonEnabledStatus();
        }
        
        private void AddAggregateFieldsToTv(LayoutLevelEnum layoutLevel) {

            int intInsertPos;
            bool bolAggregatesNodeExists = false;

            foreach(TreeNode treenode in this.tvDataFields.Nodes) {

                if(treenode.Text == "Aggregate Fields") {
                    if(treenode == _AggregateTreeNodes[(int)layoutLevel]) {
                        bolAggregatesNodeExists = true;
                    } else {
                        this.tvDataFields.Nodes.Remove(treenode);
                    }
                    break;
                }
            }

            if(!bolAggregatesNodeExists && _AggregateTreeNodes[(int)layoutLevel].Nodes.Count > 0) {

                if(this.tvDataFields.Nodes.Count > 0) {
                    intInsertPos = 1;
                } else {
                    intInsertPos = 0;
                }

                this.tvDataFields.Nodes.Insert(intInsertPos, _AggregateTreeNodes[(int)layoutLevel]);
                if(_AggregateFieldsExpanded) {
                    this.tvDataFields.Nodes[intInsertPos].Expand();
                } else {
                    this.tvDataFields.Nodes[intInsertPos].Collapse();
                }
            }

            SetAddDataFieldButtonEnabledStatus();
        }
        
        private void AddStandardFieldsToTv(LayoutLevelEnum layoutLevel) {

            int intInsertPos;
            bool bolStandardFieldsNodeExists = false;

            foreach(TreeNode treenode in this.tvDataFields.Nodes) {

                if(treenode.Text == Constants.NODE_LABEL_STD_FIELDS) {
                    if(treenode == _StandardTreeNodes[(int)layoutLevel]) {
                        bolStandardFieldsNodeExists = true;
                    } else {
                        this.tvDataFields.Nodes.Remove(treenode);
                    }
                    break;
                }
            }
            
            if(!bolStandardFieldsNodeExists && _StandardTreeNodes[(int)layoutLevel].Nodes.Count > 0) {

                if(this.tvDataFields.Nodes.Count > 1) {
                    intInsertPos = 2;
                } else if(this.tvDataFields.Nodes.Count > 0) {
                    intInsertPos = 1;
                } else {
                    intInsertPos = 0;
                }

                this.tvDataFields.Nodes.Insert(intInsertPos, _StandardTreeNodes[(int)layoutLevel]);
                if(_StandardFieldsExpanded) {
                    this.tvDataFields.Nodes[intInsertPos].Expand();
                } else {
                    this.tvDataFields.Nodes[intInsertPos].Collapse();
                }
            }

            SetAddDataFieldButtonEnabledStatus();
        }
        
        public void DataEntryFormClosed(object form, FormClosedEventArgs args)
        {
            var frm = (DataEntryForm)form;
            var colstoadd = frm.SelectedColumns;

            var currentnode = this.tabExtractDef.SelectedIndex == 0 ? this.tvLayouts.SelectedNode
                : this.tabExtractDef.SelectedIndex == 1 ? this.tvOrderData.SelectedNode
                : this.tvLimitData.SelectedNode;

            foreach (var c in colstoadd)
            {
                if (currentnode.TreeView.Name == "tvOrderData")
                {
                    AddOrderByColumn(currentnode, c);
                }
                else if (currentnode.TreeView.Name == "tvLimitData")
                {
                    AddLimitItem(currentnode, c);
                }
                else
                {
                    AddLayoutField(currentnode, c);
                }
            }

            frm.Dispose();
        }


        private void AddDataFieldFromNode(TreeNode selectedNode) {

            cColumn objColumn;
        
            if(this.splitBody.Panel2.Controls.Count > 0) {

                if(this.splitBody.Panel2.Controls[0] is ctlLayout &&
                   this.tvLayouts.SelectedNode.Tag is cLayout &&
                   selectedNode.Tag != null) {
               
                    if(selectedNode.Tag is cColumn) {

                        objColumn = (cColumn)selectedNode.Tag;
                                                                                                       
                        AddLayoutField(this.tvLayouts.SelectedNode, objColumn);

                    }
                    else if (selectedNode.Tag is DataEntryType)
                    {
                        var form = new DataEntryForm(Settings, (DataEntryType)selectedNode.Tag);
                        form.FormClosed += DataEntryFormClosed;
                        form.ShowDialog(this);
                    }
                    else {
                        AddLayoutField(this.tvLayouts.SelectedNode,
                                    selectedNode.Tag.ToString());
                    }

                } else if(this.splitBody.Panel2.Controls[0] is ctlOrderByColumns &&
                          this.tvOrderData.SelectedNode.Tag is List<cOrderByColumn> &&
                          selectedNode.Tag != null) {

                    if (selectedNode.Tag is cColumn)
                    {
                        AddOrderByColumn(this.tvOrderData.SelectedNode, (cColumn)selectedNode.Tag);
                    }
                    else if (selectedNode.Tag is DataEntryType)
                    {
                        var form = new DataEntryForm(Settings, (DataEntryType)selectedNode.Tag);
                        form.FormClosed += DataEntryFormClosed;
                        form.ShowDialog(this);
                    }


                } else if(this.splitBody.Panel2.Controls[0] is ctlLimitItems &&
                          this.tvLimitData.SelectedNode.Tag is List<cLimitItem> &&
                          selectedNode.Tag != null) {

                    if (selectedNode.Tag is cColumn)
                    {
                        AddLimitItem(this.tvLimitData.SelectedNode, (cColumn)selectedNode.Tag);
                    }
                    else if (selectedNode.Tag is DataEntryType)
                    {
                        var form = new DataEntryForm(Settings, (DataEntryType)selectedNode.Tag);
                        form.FormClosed += DataEntryFormClosed;
                        form.ShowDialog(this);
                    }
                }
            }
        }
        
        private void SetAddDataFieldButtonEnabledStatus() {
            this.btnAddDataField.Enabled = (this.tvDataFields.SelectedNode != null && this.tvDataFields.SelectedNode.Tag != null);
        }

        private void ClearDataFieldNodes() {
            this.tvDataFields.Nodes.Clear();
            SetAddDataFieldButtonEnabledStatus();
        }
        #endregion tvDataFields Node Management

        #region Select Layout Node
        private void SelectLayoutNode(TreeNode selectedNode) {

            cLayout objLayout;
            ctlGeneral GeneralCtl;
            ctlLayout LayoutCtl;
            ctLayoutField LayoutFieldCtl;
            ctlPostProcCode PostProcCodeCtl;
            ContextMenu mnuContext;

            if(selectedNode.Tag is cExtractDef) {

                while(this.splitBody.Panel2.Controls.Count > 0) {
                    this.splitBody.Panel2.Controls[0].Dispose();
                }
                
                ClearDataFieldNodes();

                GeneralCtl = new ctlGeneral(Settings, (cExtractDef)selectedNode.Tag, Settings.SetupPath, ExtractDescriptors);
                GeneralCtl.OutputMessage += OnOutputMessage;
                GeneralCtl.OutputError += OnOutputError;
                GeneralCtl.ExtractDescriptorAdded += OnExtractDescriptorAdded;
                GeneralCtl.Dock = DockStyle.Fill;
                this.splitBody.Panel2.Controls.Add(GeneralCtl);
            } else if(selectedNode.Tag is cLayout) {
                objLayout = (cLayout)selectedNode.Tag;

                AddDataFieldsToTv(objLayout.LayoutLevel);
                AddAggregateFieldsToTv(objLayout.LayoutLevel);
                AddStandardFieldsToTv(objLayout.LayoutLevel);
               
                if(this.splitBody.Panel2.Controls.Count > 0 && this.splitBody.Panel2.Controls[0] is ctlLayout) {
                    LayoutCtl = (ctlLayout)this.splitBody.Panel2.Controls[0];
                    LayoutCtl.LoadLayout(selectedNode.Index, objLayout);

                    if(BuildLayoutContextMenu(objLayout, out mnuContext)) {
                        LayoutCtl.ContextMenu = mnuContext;
                        LayoutCtl.ContextMenu.Popup += new EventHandler(InitializeCurrentContextNode);
                    }
                    
                } else {

                    while(this.splitBody.Panel2.Controls.Count > 0) {
                        this.splitBody.Panel2.Controls[0].Dispose();
                    }

                    LayoutCtl = new ctlLayout(selectedNode.Index, objLayout);

                    LayoutCtl.ItemDropped += new ItemDroppedEventHandler(LayoutCtl_ItemDropped);
                    LayoutCtl.ItemMoved += new ItemMovedEventHandler(LayoutCtl_ItemMoved);
                    LayoutCtl.ItemDeleted += new ItemDeletedEventHandler(LayoutCtl_ItemDeleted);

                    if(BuildLayoutContextMenu(objLayout, out mnuContext)) {
                        LayoutCtl.ContextMenu = mnuContext;
                        LayoutCtl.ContextMenu.Popup += new EventHandler(InitializeCurrentContextNode);
                    }

                    LayoutCtl.Dock = DockStyle.Fill;

                    this.splitBody.Panel2.Controls.Add(LayoutCtl);

                    LayoutCtl.AskToReconcileIfNecessary();
                }

            } else if(selectedNode.Tag != null &&
                      selectedNode.Tag is cLayoutField &&
                      selectedNode.Parent != null &&
                      selectedNode.Parent.Tag != null &&
                      selectedNode.Parent.Tag is cLayout) {

                ClearDataFieldNodes();

                if(this.splitBody.Panel2.Controls.Count > 0 && this.splitBody.Panel2.Controls[0] is ctLayoutField) {
                    LayoutFieldCtl = (ctLayoutField)this.splitBody.Panel2.Controls[0];
                    LayoutFieldCtl.LoadLayoutField(selectedNode.Index, ((cLayout)selectedNode.Parent.Tag).LayoutFields[selectedNode.Index]);
                } else {

                    while(this.splitBody.Panel2.Controls.Count > 0) {
                        this.splitBody.Panel2.Controls[0].Dispose();
                    }

                    LayoutFieldCtl = new ctLayoutField(selectedNode.Index, ((cLayout)selectedNode.Parent.Tag).LayoutFields[selectedNode.Index]);
                    LayoutFieldCtl.Dock = DockStyle.Fill;

                    this.splitBody.Panel2.Controls.Add(LayoutFieldCtl);
                }

            } else if(selectedNode.Tag is cPostProcCodeModule) {

                ClearDataFieldNodes();

                if(this.splitBody.Panel2.Controls.Count > 0 && this.splitBody.Panel2.Controls[0] is ctlPostProcCode) {
                    PostProcCodeCtl = (ctlPostProcCode)this.splitBody.Panel2.Controls[0];
                    PostProcCodeCtl.LoadPostProcCodeModule((cPostProcCodeModule)selectedNode.Tag);
                } else {

                    while(this.splitBody.Panel2.Controls.Count > 0) {
                        this.splitBody.Panel2.Controls[0].Dispose();
                    }

                    PostProcCodeCtl = new ctlPostProcCode((cPostProcCodeModule)selectedNode.Tag);
                    PostProcCodeCtl.Dock = DockStyle.Fill;
                    PostProcCodeCtl.AutoSize = true;
                    
                    this.splitBody.Panel2.Controls.Add(PostProcCodeCtl);
                }

            } else {

                ClearDataFieldNodes();
                
                while(this.splitBody.Panel2.Controls.Count > 0) {
                    this.splitBody.Panel2.Controls[0].Dispose();
                }
            }
        }
        #endregion Select Layout Node

        #region Select Order By Node
        private void SelectOrderDataNode(TreeNode orderDataNode) {
        
            ContextMenu mnuContext;
            LayoutLevelEnum enmLayoutLevel;
            ctlOrderByColumns OrderByColumnsCtl;

            ClearDataFieldNodes();

            if(orderDataNode.Tag is List<cOrderByColumn>) {

                enmLayoutLevel = DecodeLayoutLevel(orderDataNode.Text);
                AddDataFieldsToTv(enmLayoutLevel);

                if(this.splitBody.Panel2.Controls.Count > 0 && this.splitBody.Panel2.Controls[0] is ctlOrderByColumns) {

                    ((ctlOrderByColumns)this.splitBody.Panel2.Controls[0]).LoadOrderByColumns((List<cOrderByColumn>)orderDataNode.Tag);

                    if(BuildOrderByColumnsContextMenu(enmLayoutLevel, out mnuContext)) {
                        ((ctlOrderByColumns)this.splitBody.Panel2.Controls[0]).ContextMenu = mnuContext;
                        ((ctlOrderByColumns)this.splitBody.Panel2.Controls[0]).ContextMenu.Popup += new EventHandler(InitializeCurrentContextNode);
                    }

                } else {

                    while(this.splitBody.Panel2.Controls.Count > 0) {
                        this.splitBody.Panel2.Controls[0].Dispose();
                    }

                    OrderByColumnsCtl = new ctlOrderByColumns((List<cOrderByColumn>)orderDataNode.Tag);
                    OrderByColumnsCtl.ItemDropped += new ItemDroppedEventHandler(OrderByCtl_ItemDropped);
                    OrderByColumnsCtl.ItemMoved += new ItemMovedEventHandler(OrderByCtl_ItemMoved);
                    OrderByColumnsCtl.ItemDeleted += new ItemDeletedEventHandler(OrderByCtl_ItemDeleted);

                    if(BuildOrderByColumnsContextMenu(enmLayoutLevel, out mnuContext)) {
                        OrderByColumnsCtl.ContextMenu = mnuContext;
                        OrderByColumnsCtl.ContextMenu.Popup += new EventHandler(InitializeCurrentContextNode);
                    }

                    OrderByColumnsCtl.Dock = DockStyle.Fill;

                    this.splitBody.Panel2.Controls.Add(OrderByColumnsCtl);
                }

            } else if(
                orderDataNode.Tag is string &&
                orderDataNode.Tag.ToString() == "OrderByColumn" &&
                orderDataNode.Parent != null &&
                orderDataNode.Parent.Tag != null &&
                orderDataNode.Parent.Tag is List<cOrderByColumn>) {

                if(this.splitBody.Panel2.Controls.Count > 0 && this.splitBody.Panel2.Controls[0] is ctlOrderByColumn) {
                    ((ctlOrderByColumn)this.splitBody.Panel2.Controls[0]).LoadOrderByColumn(orderDataNode.Index, ((List<cOrderByColumn>)orderDataNode.Parent.Tag)[orderDataNode.Index]);
                } else {

                    while(this.splitBody.Panel2.Controls.Count > 0) {
                        this.splitBody.Panel2.Controls[0].Dispose();
                    }

                    ctlOrderByColumn OrderByColumnCtl = new ctlOrderByColumn(orderDataNode.Index, ((List<cOrderByColumn>)orderDataNode.Parent.Tag)[orderDataNode.Index]);
                    OrderByColumnCtl.Dock = DockStyle.Fill;
                    this.splitBody.Panel2.Controls.Add(OrderByColumnCtl);
                }
            } else {               

                while(this.splitBody.Panel2.Controls.Count > 0) {
                    this.splitBody.Panel2.Controls[0].Dispose();
                }
            }
        }
        #endregion Select Order By Node

        #region Select Limit Data Node
        private void SelectLimitDataNode(TreeNode limitDataNode) {

            ContextMenu mnuContext;
            ctlLimitItems LimitItemsCtl;
            List<cColumn>[] objColumns = new List<cColumn>[] { };
            LayoutLevelEnum enmLayoutLevel;

            ClearDataFieldNodes();

            if(limitDataNode.Tag is List<cLimitItem>) {

                enmLayoutLevel = DecodeLayoutLevel(limitDataNode.Text);
                AddDataFieldsToTv(enmLayoutLevel);

                switch(enmLayoutLevel) {
                    case LayoutLevelEnum.Bank:
                        objColumns = new List<cColumn>[] { _BankColumns };
                        break;
                    case LayoutLevelEnum.ClientAccount:
                        objColumns = new List<cColumn>[] { _LockboxColumns };
                        break;
                    case LayoutLevelEnum.Batch:
                        objColumns = new List<cColumn>[] { _BatchColumns };
                        break;
                    case LayoutLevelEnum.Transaction:
                        objColumns = new List<cColumn>[] { _TransactionsColumns };
                        break;
                    case LayoutLevelEnum.Payment:
                        objColumns = new List<cColumn>[] { _PaymentsColumns, _PaymentsDataEntryColumns };
                        break;
                    case LayoutLevelEnum.Stub:
                        objColumns = new List<cColumn>[] { _StubsColumns, _StubsDataEntryColumns };
                        break;
                    case LayoutLevelEnum.Document:
                        objColumns = new List<cColumn>[] { _DocumentsColumns };
                        break;
                    default:
                        throw (new Exception("Invalid Layout Level for Limit Items: " + limitDataNode.Text));
                }

                if(this.splitBody.Panel2.Controls.Count > 0 && this.splitBody.Panel2.Controls[0] is ctlLimitItems) {

                    ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).LoadLimitItems((List<cLimitItem>)limitDataNode.Tag, objColumns);

                    if(BuildLimitItemsContextMenu(enmLayoutLevel, out mnuContext)) {
                        ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).ContextMenu = mnuContext;
                        ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).ContextMenu.Popup += new EventHandler(InitializeCurrentContextNode);
                    }

                } else {

                    while(this.splitBody.Panel2.Controls.Count > 0) {
                        this.splitBody.Panel2.Controls[0].Dispose();
                    }

                    LimitItemsCtl = new ctlLimitItems((List<cLimitItem>)limitDataNode.Tag, objColumns);
                    LimitItemsCtl.ItemDropped += new ItemDroppedEventHandler(LimitItemCtl_ItemDropped);
                    LimitItemsCtl.ItemMoved += new ItemMovedEventHandler(LimitItemCtl_ItemMoved);
                    LimitItemsCtl.ItemDeleted += new ItemDeletedEventHandler(LimitItemCtl_ItemDeleted);
                    
                    if(BuildLimitItemsContextMenu(enmLayoutLevel, out mnuContext)) {
                        LimitItemsCtl.ContextMenu = mnuContext;
                        LimitItemsCtl.ContextMenu.Popup += new EventHandler(InitializeCurrentContextNode);
                    }
                    
                    LimitItemsCtl.Dock = DockStyle.Fill;

                    this.splitBody.Panel2.Controls.Add(LimitItemsCtl);
                }

            } else if(
                limitDataNode.Tag is string &&
                limitDataNode.Tag.ToString() == "LimitItem" &&
                limitDataNode.Parent != null &&
                limitDataNode.Parent.Tag != null &&
                limitDataNode.Parent.Tag is List<cLimitItem>) {

                var item = ((List<cLimitItem>)limitDataNode.Parent.Tag)[limitDataNode.Index];
                var table = item.Table.ToLower();
                switch (table)
                {
                    case "dimbanks":
                        objColumns = new List<cColumn>[] { _BankColumns };
                        break;
                    case "dimclientaccounts":
                        objColumns = new List<cColumn>[] { _LockboxColumns };
                        break;
                    case "factbatchsummary":
                    case "dimbatchpaymenttypes":
                    case "dimbatchsources":
                    case "dimbatchexceptionstatuses":
                        objColumns = new List<cColumn>[] { _BatchColumns };
                        break;
                    case "facttransactionsummary":
                    case "dimtransactionexceptionstatuses":
                        objColumns = new List<cColumn>[] { _TransactionsColumns };
                        break;
                    case "factchecks":
                    case "dimddas":
                    case "checksdataentry":
                        objColumns = new List<cColumn>[] { _PaymentsColumns, _PaymentsDataEntryColumns };
                        break;
                    case "factstubs":
                    case "stubsdataentry":
                        objColumns = new List<cColumn>[] { _StubsColumns, _StubsDataEntryColumns };
                        break;
                    case "factdocuments":
                    case "dimdocumenttypes":
                        objColumns = new List<cColumn>[] { _DocumentsColumns };
                        break;
                    default:
                        throw (new Exception("Invalid Layout Level for Limit Items: " + limitDataNode.Text));
                }

                if (this.splitBody.Panel2.Controls.Count > 0 && this.splitBody.Panel2.Controls[0] is ctlLimitItem) {
                    ((ctlLimitItem)this.splitBody.Panel2.Controls[0]).LoadLimitItem(limitDataNode.Index, ((List<cLimitItem>)limitDataNode.Parent.Tag)[limitDataNode.Index]);
                } else {

                    while(this.splitBody.Panel2.Controls.Count > 0) {
                        this.splitBody.Panel2.Controls[0].Dispose();
                    }

                    ctlLimitItem LimitItemCtl = new ctlLimitItem(limitDataNode.Index, ((List<cLimitItem>)limitDataNode.Parent.Tag)[limitDataNode.Index], objColumns);
                    LimitItemCtl.Dock = DockStyle.Fill;
                    this.splitBody.Panel2.Controls.Add(LimitItemCtl);
                }
            } else {
                
                while(this.splitBody.Panel2.Controls.Count > 0) {
                    this.splitBody.Panel2.Controls[0].Dispose();
                }
            }
        }
        #endregion Select Limit Data Node

        #region LayoutField-OrderByColumn-LimitItem Node Management
        private void DeleteLayout(TreeNode layoutNode) {
            if(MessageBox.Show("Delete Layout [" + ((cLayout)layoutNode.Tag).LayoutName + "]?", 
               "Confirm Delete Layout", 
               MessageBoxButtons.OKCancel) == DialogResult.OK) {
                layoutNode.Remove();
            }
        }

        private void DeleteLayoutField(
            TreeNode layoutFieldNode, 
            bool eventIsLocal) {

            if(eventIsLocal &&
               this.splitBody.Panel2.Controls.Count > 0 && 
               this.splitBody.Panel2.Controls[0] is ctlLayout &&
               layoutFieldNode.Parent.IsSelected) {

                ((ctlLayout)this.splitBody.Panel2.Controls[0]).RemoveAt(layoutFieldNode.Index);
            }

            ((cLayout)layoutFieldNode.Parent.Tag).RemoveLayoutField(layoutFieldNode.Index);
            layoutFieldNode.Remove();
        }

        private void DeleteOrderByColumn(
            TreeNode orderByColumnNode, 
            bool eventIsLocal) {

                if(eventIsLocal &&
               this.splitBody.Panel2.Controls.Count > 0 && 
               this.splitBody.Panel2.Controls[0] is ctlOrderByColumns &&
               orderByColumnNode.Parent.IsSelected) {

                ((ctlOrderByColumns)this.splitBody.Panel2.Controls[0]).RemoveAt(orderByColumnNode.Index);
            }

            ((List<cOrderByColumn>)orderByColumnNode.Parent.Tag).RemoveAt(orderByColumnNode.Index);
            orderByColumnNode.Parent.Nodes.RemoveAt(orderByColumnNode.Index);
        }
        
        private void DeleteLimitItem(
            TreeNode limitItemNode, 
            bool eventIsLocal) {

            if(eventIsLocal &&
               this.splitBody.Panel2.Controls.Count > 0 && 
               this.splitBody.Panel2.Controls[0] is ctlLimitItems &&
               limitItemNode.Parent.IsSelected) {

                ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).RemoveAt(limitItemNode.Index);
            }

            ((List<cLimitItem>)limitItemNode.Parent.Tag).RemoveAt(limitItemNode.Index);
            limitItemNode.Parent.Nodes.RemoveAt(limitItemNode.Index);

            if(this.splitBody.Panel2.Controls.Count > 0 && 
               this.splitBody.Panel2.Controls[0] is ctlLimitItems) {
                ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).RefreshWhereClauseText((List<cLimitItem>)this.tvLimitData.SelectedNode.Tag);
            }
        }

        private void AddLayoutField(
            TreeNode layoutNode, 
            cColumn column) {

            if(layoutNode != null &&
               layoutNode.Tag != null &&
               layoutNode.Tag is cLayout) {

                AddLayoutField(
                    layoutNode, 
                    column,
                    (int)LegacySupport.GetTFieldType(column.Type),
                    column.Length);
            }
        }

        private void AddLayoutField(
            TreeNode layoutNode, 
            cColumn column, 
            int dataType, 
            int columnWidth) {

            cLayoutField objLayoutField;

            if(layoutNode != null &&
               layoutNode.Tag != null &&
               layoutNode.Tag is cLayout) {

                objLayoutField = new cLayoutField(
                    (cLayout)layoutNode.Tag, 
                    string.Format("{0}.{1}.{2}", column.Schema, column.TableName, column.ColumnName), 
                    dataType, 
                    columnWidth);
                objLayoutField.IsDataEntry = column.IsDataEntry;
                objLayoutField.DisplayName = column.DisplayName;
                objLayoutField.SiteBankID = column.SiteBankID;
                objLayoutField.SiteClientAccountID = column.SiteClientAccountID;
                objLayoutField.PaymentSource = column.PaymentSource;

                AddLayoutField(layoutNode, objLayoutField);
            }
        }

        private void AddLayoutField(
            TreeNode layoutNode, 
            string fieldName) {

            cLayoutField objLayoutField;

            if(layoutNode != null &&
               layoutNode.Tag != null &&
               layoutNode.Tag is cLayout) {

                objLayoutField = new cLayoutField(
                    (cLayout)layoutNode.Tag, 
                    fieldName);

                AddLayoutField(
                    layoutNode, 
                    objLayoutField);
            }
        }

        private void AddLayoutField(
            TreeNode layoutNode, 
            cLayoutField layoutField) {

            // WI 129674 : Alter the display name.
            if (string.IsNullOrWhiteSpace(layoutField.DisplayName))
            layoutField.DisplayName = cMappingLib.GetFriendly(layoutField.FieldName);

            TreeNode objNewNode = BuildLayoutFieldNode(
                layoutNode, 
                layoutField);
            objNewNode.Parent.Expand();
            objNewNode.EnsureVisible();

            ((cLayout)layoutNode.Tag).AddLayoutField(layoutField);
            
            if(this.splitBody.Panel2.Controls.Count > 0 && 
               this.splitBody.Panel2.Controls[0] is ctlLayout &&
               layoutNode == this.tvLayouts.SelectedNode) {

                ((ctlLayout)this.splitBody.Panel2.Controls[0]).AddLayoutField(layoutField);
            }
        }

        private void AddOrderByColumn(
            TreeNode orderByColumnsNode, 
            cColumn column) {

            cOrderByColumn objOrderByColumn;
            TreeNode objNewNode;

            // WI 135749 : Passing the Fully-qualified column name down.
            var fullname = string.Format("{0}.{1}.{2}", column.Schema, column.TableName, column.ColumnName);
            objOrderByColumn = new cOrderByColumn(column.ColumnName, fullname);
            objOrderByColumn.DisplayName = column.DisplayName;

            // Add the new item to the TreeView.
            objNewNode = BuildOrderByNode(
                orderByColumnsNode, 
                objOrderByColumn);
            objNewNode.Parent.Expand();
            objNewNode.EnsureVisible();

            if(this.splitBody.Panel2.Controls[0] != null && 
               this.splitBody.Panel2.Controls[0] is ctlOrderByColumns &&
               orderByColumnsNode == this.tvOrderData.SelectedNode) {

                ((ctlOrderByColumns)this.splitBody.Panel2.Controls[0]).AddOrderByColumn(objOrderByColumn);
            }
        }
        
        private void AddLimitItem(
            TreeNode limitItemsNode, 
            cColumn column)
        {
            cLimitItem objLimitItem;
            TreeNode objNewNode;

            // 115249 : Added tablename/schema to help validate limit fields.
            objLimitItem = new cLimitItem();
            objLimitItem.FieldName = column.ColumnName;
            objLimitItem.Table = column.TableName;
            objLimitItem.Schema = column.Schema;
            objLimitItem.DisplayName = column.DisplayName;
            objLimitItem.PaymentSource = column.PaymentSource;
            objLimitItem.DataType = (int) LegacySupport.GetTFieldType(column.Type);

            // Add the new item to the TreeView.
            objNewNode = BuildLimitItemNode(
                limitItemsNode, 
                objLimitItem);
            objNewNode.Parent.Expand();
            objNewNode.EnsureVisible();
            
            if(this.splitBody.Panel2.Controls[0] != null && 
               this.splitBody.Panel2.Controls[0] is ctlLimitItems && 
               limitItemsNode == this.tvLimitData.SelectedNode) {

                ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).AddLimitItem(objLimitItem);
                ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).RefreshWhereClauseText((List<cLimitItem>)limitItemsNode.Tag);
            }
        }
        #endregion LayoutField-OrderByColumn-LimitItem Node Management

        private void AddPostProcCodeModule(TreeNode generalNode) {
            
            cPostProcCodeModule PostProcCodeModule = new cPostProcCodeModule();
            BuildPostProcCodeModuleNode(generalNode, PostProcCodeModule);
        }

        private void DeletePostProcCodeModule(TreeNode postProcCodeModuleNode) {

            postProcCodeModuleNode.Parent.Nodes.Remove(postProcCodeModuleNode);
        }

        // ********************************************
        // * Event Handlers
        // ********************************************

        #region Context Menu Event Handlers
        private void mnuNewLayout_OnClick(Object sender, System.EventArgs e) {

            cLayout objLayout;
            frmNewLayoutDialog objNewLayoutDialog;
            bool bolAllowTrailer;
            TreeNode objNewNode;

            if(_CurrentContextNode != null) {

                objLayout = new cLayout((cExtractDef)this.tvLayouts.Nodes["General"].Tag);

                switch(((MenuItem)sender).Text) {
                    case "New " + Support.LBL_FILE + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.File;
                        bolAllowTrailer = true;
                        break;
                    case "New " + Support.LBL_BANK + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.Bank;
                        bolAllowTrailer = true;
                        break;
                    case "New " + Support.LBL_CLIENT_ACCOUNT + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.ClientAccount;
                        bolAllowTrailer = true;
                        break;
                    case "New " + Support.LBL_BATCH + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.Batch;
                        bolAllowTrailer = true;
                        break;
                    case "New " + Support.LBL_TRANSACTION + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.Transaction;
                        bolAllowTrailer = true;
                        break;
                    case "New " + Support.LBL_PAYMENT + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.Payment;
                        bolAllowTrailer = false;
                        break;
                    case "New " + Support.LBL_STUB + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.Stub;
                        bolAllowTrailer = false;
                        break;
                    case "New " + Support.LBL_DOCUMENT + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.Document;
                        bolAllowTrailer = false;
                        break;
                    case "New " + Support.LBL_JOINT_DETAIL + " Layout":
                        objLayout.LayoutLevel = LayoutLevelEnum.JointDetail;
                        bolAllowTrailer = false;
                        break;
                    default:
                        throw(new Exception("Invalid Layout Level: " + ((MenuItem)sender).Text));
                }


                objNewLayoutDialog = new frmNewLayoutDialog(GetNewLayoutDefaultName(),
                                                            bolAllowTrailer);

                objNewLayoutDialog.ShowDialog(this);

                if(!objNewLayoutDialog.Cancelled) {
                
                    if(LayoutNameExistsInTreeView(objNewLayoutDialog.LayoutName)) {
                        OnOutputMessage(
                            "Layout Name already exists in the current Extract Definition.", 
                            string.Empty,
                            MessageType.Warning,
                            MessageImportance.Essential);
                    } else {
                        objLayout.LayoutName = objNewLayoutDialog.LayoutName;
                        objLayout.LayoutType = objNewLayoutDialog.LayoutType;
                        objNewNode = BuildLayoutNode(_CurrentContextNode, objLayout);
                        this.tvLayouts.SelectedNode = objNewNode;
                    }
                }

                objNewLayoutDialog.Dispose();
            }
        }
        
        private string GetNewLayoutDefaultName() {

            string strDefaultLayoutName;
            int intLayoutIndex;

            intLayoutIndex = 0;
            strDefaultLayoutName = string.Empty;

            while(true) {

                strDefaultLayoutName = "New Layout" + (intLayoutIndex == 0 ? string.Empty : "_" + intLayoutIndex.ToString());

                if(!LayoutNameExistsInTreeView(strDefaultLayoutName)) {
                    break;
                }

                ++intLayoutIndex;
            }

            return(strDefaultLayoutName);
        }

        private bool LayoutNameExistsInTreeView(string layoutName) {

            bool bolLayoutExists;

            bolLayoutExists = false;
            foreach(TreeNode nodeLayouts in this.tvLayouts.Nodes["General"].Nodes) {

                foreach(TreeNode nodeLayout in nodeLayouts.Nodes) {

                    if(nodeLayout.Text.Trim().ToLower() == layoutName.Trim().ToLower()) {
                        bolLayoutExists = true;
                        break;
                    }
                }

                if(bolLayoutExists) {
                    break; 
                }
            }

            return(bolLayoutExists);
        }
        
        /// <summary>
        /// This is called when the right-click context menu has a selected item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuContext_OnClick(Object sender, System.EventArgs e) {

            cColumn objColumn;
            TreeNode nodeSrc;

            if(_CurrentContextNode != null &&
               _CurrentContextNode.Tag != null) {

                nodeSrc = _CurrentContextNode;

            } else if(((MenuItem)sender).GetContextMenu() != null &&
                      ((MenuItem)sender).GetContextMenu().SourceControl != null) {
            
                if(((MenuItem)sender).GetContextMenu().SourceControl is ctlLayout && this.tvLayouts.SelectedNode != null) {
                    nodeSrc = this.tvLayouts.SelectedNode;
                } else if(((MenuItem)sender).GetContextMenu().SourceControl is ctlLimitItems && this.tvLimitData.SelectedNode != null) {
                    nodeSrc = this.tvLimitData.SelectedNode;
                } else if(((MenuItem)sender).GetContextMenu().SourceControl is ctlOrderByColumns && this.tvOrderData.SelectedNode != null) {
                    nodeSrc = this.tvOrderData.SelectedNode;
                } else {
                    nodeSrc = null;
                }

            } else {
                nodeSrc = null;
            }
            
            if(nodeSrc != null && nodeSrc.Tag != null) {

               if(nodeSrc.Tag is cLayout) {

                    switch(((MenuItem)sender).Text) {
                        case "Delete": 
                            DeleteLayout(nodeSrc);
                            break;
                        case "Rename": 
                            nodeSrc.BeginEdit();
                            break;
                        default:
                            if(((MenuItem)sender).Tag != null) {
                                if(((MenuItem)sender).Tag is cColumn) {

                                    objColumn = (cColumn)((MenuItem)sender).Tag;

                                    AddLayoutField(nodeSrc, objColumn);
                                } else {
                                    AddLayoutField(nodeSrc, 
                                                ((MenuItem)sender).Tag.ToString());
                                }
                            }
                            break;
                    }

                } else if(nodeSrc.Tag is List<cOrderByColumn>) {

                    if(((MenuItem)sender).Tag != null) {
                        if(((MenuItem)sender).Tag is cColumn) {
                            objColumn = (cColumn)((MenuItem)sender).Tag;
                            AddOrderByColumn(nodeSrc, objColumn);
                        }
                    }
                    
                } else if(nodeSrc.Tag is List<cLimitItem>) {

                    if(((MenuItem)sender).Tag != null) {
                        if(((MenuItem)sender).Tag is cColumn) {
                            objColumn = (cColumn)((MenuItem)sender).Tag;
                            AddLimitItem(nodeSrc, objColumn);
                        }
                    }
                } else if(nodeSrc.Tag.ToString() == "OrderByColumn" &&
                          sender != null &&
                          sender is MenuItem &&
                          ((MenuItem)sender).Text == "Delete Order By Column") {
                    DeleteOrderByColumn(nodeSrc, true);
                } else if(nodeSrc.Tag.ToString() == "LimitItem" &&
                          sender != null &&
                          sender is MenuItem &&
                          ((MenuItem)sender).Text == "Delete Limit Item") {
                    DeleteLimitItem(nodeSrc, true);
                } else if(nodeSrc.Tag != null && 
                          nodeSrc.Tag is cLayoutField &&
                          sender != null &&
                          sender is MenuItem &&
                          ((MenuItem)sender).Text == "Delete Layout Field") {
                    DeleteLayoutField(nodeSrc, true);
                } else if(nodeSrc.Tag is cExtractDef && 
                          sender != null &&
                          sender is MenuItem &&
                          ((MenuItem)sender).Text == "Add Post-Processing Code Module") {
                    AddPostProcCodeModule(nodeSrc);
                } else if(nodeSrc.Tag is cPostProcCodeModule && 
                          sender != null &&
                          sender is MenuItem &&
                          ((MenuItem)sender).Text == "Delete Post-Processing Code Module") {
                    DeletePostProcCodeModule(nodeSrc);
                }
            }
        }
        #endregion Context Menu Event Handlers

        #region File Menu Event Handlers

        //
        // File Menu Items
        //
        private void mnuFileNew_Click(object sender, EventArgs e) {

            string strNewExtractFileName = "New_Extract";

            try {
                
                SetFormState(FormState.Init);
                
                cExtractDef objExtractDef = new cExtractDef();
                
                if(LoadExtractDef(objExtractDef)) {

                    if(_CurrentFormState == FormState.Run) {
                        BuildRunCtl();
                        SetFormState(FormState.Run);
                    } else {
                        SetFormState(FormState.Edit);
                    }

                    this.txtExtractScriptPath.Text = strNewExtractFileName;
                    this.Text = "Extract Design Manager - [" + strNewExtractFileName + "]";
                    _InputFileName = strNewExtractFileName;
                    OnOutputStatusMessage("Extract Definition File loaded: [" + _InputFileName + "]");

                } else {
                    SetFormState(FormState.Init);
                }
                
                _InputFileName = Path.Combine(Settings.SetupPath, strNewExtractFileName);
                
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void mnuFileOpen_Click(object sender, EventArgs e) {           
            try {
                if(SelectExtractScriptDir()) {
                    if(_CurrentFormState == FormState.Run) {
                        BuildRunCtl();
                        SetFormState(FormState.Run);
                    } else {
                        SetFormState(FormState.Edit);
                    }
                }
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void mnuFileClose_Click(object sender, EventArgs e) {
            try {
                if(PromptForSaveIfNecessary()) {
                    SetFormState(FormState.Init);
                }
    
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
       }

        private void mnuFileSave_Click(object sender, EventArgs e) {

            string strFilePath;

            try {
            
                if(SaveExtractDef(this.txtExtractScriptPath.Text, out strFilePath)) {
                    LoadExtractDefFile(strFilePath);
                }

            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void mnuFileSaveAs_Click(object sender, EventArgs e) {

            string strFileName;
            
            try {

                if(SaveExtractDef(string.Empty, out strFileName)) {
                    LoadExtractDefFile(strFileName);
                }

            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }
        
        private void LoadExtractDefFile(string extractDefFileName) {

            if(extractDefFileName.ToLower().EndsWith(".xml")) {
                if(LoadXmlExtractDef(extractDefFileName)) {
                    SetFormState(FormState.Edit);
                } else {
                    SetFormState(FormState.Init);
                }
            } else {
                if(LoadExtractDef(extractDefFileName)) {
                    BuildRunCtl();
                    SetFormState(FormState.Edit);
                } else {
                    SetFormState(FormState.Init);
                }
            }
        }

        private void mnuFilePrint_Click(object sender, EventArgs e) {

            try {
                if(File.Exists(_InputFileName)) {

                    DialogResult result = printDialog1.ShowDialog();
                    if(result == DialogResult.OK) {
                        cPrinter.PrintTextFile(_InputFileName, this.printDialog1.PrinterSettings);
                    }
                } else {
                    MessageBox.Show("Could not locate file: [" + _InputFileName + "].", 
                                    "Error", 
                                    MessageBoxButtons.OK, 
                                    MessageBoxIcon.Error);
                }

            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void mnuFilePrinterSetup_Click(object sender, EventArgs e) {

            PageSetupDialog dlg;
            DialogResult result;
            
            try {
                dlg = new PageSetupDialog();
                dlg.AllowMargins = false;
                dlg.AllowOrientation = true;
                dlg.AllowPaper = true;
                dlg.AllowPrinter = true;
                dlg.EnableMetric = false;
                dlg.ShowHelp = false;
                dlg.ShowNetwork = true;

                using (var doc = new PrintDocument())
                {
                    doc.PrinterSettings = this.printDialog1.PrinterSettings;
                    dlg.Document = doc;
                    result = dlg.ShowDialog();
                }
                
                dlg.Dispose();

            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void mnuFileExit_Click(object sender, EventArgs e) {
            this.Close();
        }

        #endregion File Menu Event Handlers

        #region View Menu Event Handlers

        //
        // View Menu Items
        //
        private void mnuViewExtractDefinition_Click(object sender, EventArgs e) 
        {
            SetFormState(FormState.Edit);
        }

        private void mnuViewResults_Click(object sender, EventArgs e) {

            frmDisplayFileContents objText;
            string strFileName;

            try 
            {
                if(this.pnlMain.Controls[0] is ctlExtractRun &&
                   ((ctlExtractRun)this.pnlMain.Controls[0]).CompletedExtractFileName.Length > 0 &&
                   File.Exists(((ctlExtractRun)this.pnlMain.Controls[0]).CompletedExtractFileName)) 
                {
                    objText = new frmDisplayFileContents("Extract Results", ((ctlExtractRun)this.pnlMain.Controls[0]).CompletedExtractFileName, this.printDialog1.PrinterSettings, this.Icon);
                    objText.Show(this);
                } 
                else if(IOLib.BrowseFile(ExtractLib.Constants.FILE_FILTER_EXTRACT_DATA_FILES, Settings.DefaultDefinitionFileFormatIndex, "", out strFileName)) 
                {
                    objText = new frmDisplayFileContents("Extract Results", strFileName, this.printDialog1.PrinterSettings, this.Icon);
                    objText.Show(this);
                }

            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void mnuViewLogFile_Click(object sender, EventArgs e) {

            frmDisplayFileContents objText;
            string strFileName;

            try {

                if(this.pnlMain.Controls[0] is ctlExtractRun &&
                   ((ctlExtractRun)this.pnlMain.Controls[0]).CompletedLogFilePath.Length > 0 &&
                   File.Exists(((ctlExtractRun)this.pnlMain.Controls[0]).CompletedLogFilePath)) {

                    objText = new frmDisplayFileContents("Extract Log File", ((ctlExtractRun)this.pnlMain.Controls[0]).CompletedLogFilePath, this.printDialog1.PrinterSettings, this.Icon);
                    objText.Show(this);

                   }
                else if (IOLib.BrowseFile(ExtractLib.Constants.FILE_FILTER_LOG_FILES, Settings.DefaultDefinitionFileFormatIndex, "", out strFileName))
                {
                    objText = new frmDisplayFileContents("Extract Log File", strFileName, this.printDialog1.PrinterSettings, this.Icon);
                    objText.Show(this);
                }

            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }
        #endregion View Menu Event Handlers

        #region Run Menu Event Handlers

        //
        // Run Menu Items
        //
        private void mnuRun_Click(object sender, EventArgs e) {

            try {

                if(_InputFileName.Length > 0) {
                    BuildRunCtl();
                    SetFormState(FormState.Run);
                } else {
                
                    if(SelectExtractScriptDir()) {
                        BuildRunCtl();
                        SetFormState(FormState.Run);
                    }
                }

            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }
        #endregion Run Menu Event Handlers

        #region Help Menu Event Handlers

        //
        // Help Menu Items
        //
        private void mnuHelpAbout_Click(object sender, EventArgs e) {

            frmAbout about;

            try {
                about = new frmAbout();
                about.ShowDialog(this);
                about.Dispose();
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void mnuHelpLicense_Click(object sender, EventArgs e) {
            ////try {
            ////    MessageBox.Show("Maximum number of extracts permitted = " + ExtractLicense.ReadMaxExtracts().ToString(), "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            ////} catch(Exception ex) {
            ////    OnOutputError(sender, ex);
            ////}
        }
        #endregion Help Menu Items

        #region Form Button Event Handlers
        private void btnSelectExtractScriptDir_Click(object sender, EventArgs e) {
            
            try {
                if(SelectExtractScriptDir()) {
                    if(_CurrentFormState == FormState.Run) {
                        BuildRunCtl();
                        SetFormState(FormState.Run);
                    } else {
                        SetFormState(FormState.Edit);
                    }
                }
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void btnAddDataField_Click(object sender, EventArgs e) {
            if(this.tvDataFields.SelectedNode != null) {
                AddDataFieldFromNode(this.tvDataFields.SelectedNode);
            }
        }
        #endregion Form Button Handlers

        #region Edit Control Event Handlers
        private void LayoutCtl_ItemDropped(string itemText) {

            cColumn Column;

            if(this.tabExtractDef.SelectedIndex == 0 &&
               _CurrentContextNode != null &&
               _CurrentContextNode.Tag != null &&
               this.tvLayouts.SelectedNode != null &&
               this.tvLayouts.SelectedNode.Tag != null &&
               this.tvLayouts.SelectedNode.Tag is cLayout) {

                if(_CurrentContextNode.Tag is cColumn) {

                    Column = (cColumn)_CurrentContextNode.Tag;

                    AddLayoutField(this.tvLayouts.SelectedNode, Column);

                } else if(_CurrentContextNode.Tag is string) {

                    AddLayoutField(this.tvLayouts.SelectedNode, 
                                _CurrentContextNode.Tag.ToString());
                }
            }

            _CurrentContextNode = null;
        }

        private void OrderByCtl_ItemDropped(string itemText) {

            cColumn Column;
            cOrderByColumn objOrderByColumn;

            if(this.tabExtractDef.SelectedIndex == 1 &&
               _CurrentContextNode != null &&
               _CurrentContextNode.Tag != null &&
               _CurrentContextNode.Tag is cColumn &&
               this.tvOrderData.SelectedNode != null &&
               this.tvOrderData.SelectedNode.Tag != null &&
               this.tvOrderData.SelectedNode.Tag is List<cOrderByColumn>) {

                Column = (cColumn)_CurrentContextNode.Tag;

                // WI 135749 : Passing the Fully-qualified column name down.
                var fullname = string.Format("{0}.{1}.{2}", Column.Schema, Column.TableName, Column.ColumnName);
                objOrderByColumn = new cOrderByColumn(Column.ColumnName, fullname);
                
                BuildOrderByNode(this.tvOrderData.SelectedNode, objOrderByColumn);
                
                if(this.splitBody.Panel2.Controls.Count > 0 && 
                   this.splitBody.Panel2.Controls[0] is ctlOrderByColumns) {

                    ((ctlOrderByColumns)this.splitBody.Panel2.Controls[0]).AddOrderByColumn(objOrderByColumn);
                }
            }

            _CurrentContextNode = null;
        }

        private void LimitItemCtl_ItemDropped(string itemText) {

            cColumn Column;
            cLimitItem objLimitItem;

            if(this.tabExtractDef.SelectedIndex == 2 &&
               _CurrentContextNode != null &&
               _CurrentContextNode.Tag != null &&
               _CurrentContextNode.Tag is cColumn &&
               this.tvLimitData.SelectedNode != null &&
               this.tvLimitData.SelectedNode.Tag != null &&
               this.tvLimitData.SelectedNode.Tag is List<cLimitItem>) {

                Column = (cColumn)_CurrentContextNode.Tag;

                // 115249 : Added tablename/schema to help validate limit fields.
                objLimitItem = new cLimitItem();
                objLimitItem.FieldName = Column.ColumnName;
                objLimitItem.Table = Column.TableName;
                objLimitItem.Schema = Column.Schema;
                
                BuildLimitItemNode(this.tvLimitData.SelectedNode, objLimitItem);
                
                if(this.splitBody.Panel2.Controls.Count > 0 && 
                   this.splitBody.Panel2.Controls[0] is ctlLimitItems) {

                    ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).AddLimitItem(objLimitItem);
                    ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).RefreshWhereClauseText((List<cLimitItem>)this.tvLimitData.SelectedNode.Tag);
                }
            }

            _CurrentContextNode = null;
        }

        private void LayoutCtl_ItemMoved(int currentIndex, int newIndex) {

            cLayoutField objLayoutField;
            TreeNode objNewNode;

           if(this.tabExtractDef.SelectedIndex == 0 &&
              this.tvLayouts.SelectedNode != null &&
              this.tvLayouts.SelectedNode.Tag != null &&
              this.tvLayouts.SelectedNode.Tag is cLayout) {

                // Save off the current Layout Field object
                objLayoutField = (cLayoutField)(((cLayout)this.tvLayouts.SelectedNode.Tag).LayoutFields[currentIndex]);

                ((cLayout)this.tvLayouts.SelectedNode.Tag).RemoveLayoutField(currentIndex);
                this.tvLayouts.SelectedNode.Nodes.RemoveAt(currentIndex);

                // Move the Layout Field from the underlying collection
                ((cLayout)this.tvLayouts.SelectedNode.Tag).InsertLayoutField(newIndex, objLayoutField);

                // Move the TreeView Node
                objNewNode = new TreeNode(objLayoutField.FieldName);
                objNewNode.Tag = objLayoutField;
                this.tvLayouts.SelectedNode.Nodes.Insert(newIndex, objNewNode);
            }
        }

        private void OrderByCtl_ItemMoved(int currentIndex, int newIndex) {

            cOrderByColumn objOrderByColumn;
            TreeNode objNewNode;

            if(this.tabExtractDef.SelectedIndex == 1 &&
               this.tvOrderData.SelectedNode != null &&
               this.tvOrderData.SelectedNode.Tag != null && 
               this.tvOrderData.SelectedNode.Tag is List<cOrderByColumn>) {

                // Save off the current Layout Field object
                objOrderByColumn = ((List<cOrderByColumn>)this.tvOrderData.SelectedNode.Tag)[currentIndex];

                ((List<cOrderByColumn>)this.tvOrderData.SelectedNode.Tag).RemoveAt(currentIndex);
                this.tvOrderData.SelectedNode.Nodes.RemoveAt(currentIndex);

                // Move the Layout Field from the underlying collection
                ((List<cOrderByColumn>)this.tvOrderData.SelectedNode.Tag).Insert(newIndex, objOrderByColumn);

                // Move the TreeView Node
                objNewNode = new TreeNode(objOrderByColumn.ColumnName);
                this.tvOrderData.SelectedNode.Nodes.Insert(newIndex, objNewNode);
            }
        }

        private void LimitItemCtl_ItemMoved(int currentIndex, int newIndex) {

            cLimitItem objLimitItem;
            TreeNode objNewNode;

            if(this.tabExtractDef.SelectedIndex == 2 &&
               this.tvLimitData.SelectedNode != null &&
               this.tvLimitData.SelectedNode.Tag != null && 
               this.tvLimitData.SelectedNode.Tag is List<cLimitItem>) {

                // Save off the current Layout Field object
                objLimitItem = ((List<cLimitItem>)this.tvLimitData.SelectedNode.Tag)[currentIndex];

                ((List<cLimitItem>)this.tvLimitData.SelectedNode.Tag).RemoveAt(currentIndex);
                this.tvLimitData.SelectedNode.Nodes.RemoveAt(currentIndex);

                // Move the Layout Field from the underlying collection
                ((List<cLimitItem>)this.tvLimitData.SelectedNode.Tag).Insert(newIndex, objLimitItem);

                // Move the TreeView Node
                objNewNode = new TreeNode(objLimitItem.FieldName);
                this.tvLimitData.SelectedNode.Nodes.Insert(newIndex, objNewNode);

                if(this.splitBody.Panel2.Controls.Count > 0 && 
                   this.splitBody.Panel2.Controls[0] is ctlLimitItems) {

                    ((ctlLimitItems)this.splitBody.Panel2.Controls[0]).RefreshWhereClauseText((List<cLimitItem>)this.tvLimitData.SelectedNode.Tag);
                }
            }
        }

        private void LayoutCtl_ItemDeleted(int itemIndex) {

            if(this.tabExtractDef.SelectedIndex == 0 &&
               this.tvLayouts.SelectedNode != null &&
               this.tvLayouts.SelectedNode.Tag != null &&
               this.tvLayouts.SelectedNode.Tag is cLayout) {

                DeleteLayoutField(this.tvLayouts.SelectedNode.Nodes[itemIndex], false);
            }
        }

        private void OrderByCtl_ItemDeleted(int itemIndex) {

            if(this.tabExtractDef.SelectedIndex == 1 &&
               this.tvOrderData.SelectedNode != null &&
               this.tvOrderData.SelectedNode.Tag != null &&
               this.tvOrderData.SelectedNode.Tag is List<cOrderByColumn>) {

                DeleteOrderByColumn(this.tvOrderData.SelectedNode.Nodes[itemIndex], false);
            }
        }

        private void LimitItemCtl_ItemDeleted(int itemIndex) {

            if(this.tabExtractDef.SelectedIndex == 2 &&
               this.tvLimitData.SelectedNode != null &&
               this.tvLimitData.SelectedNode.Tag != null &&
               this.tvLimitData.SelectedNode.Tag is List<cLimitItem>) {

                DeleteLimitItem(this.tvLimitData.SelectedNode.Nodes[itemIndex], false);
            }
        }

        #endregion Edit Control Event Handlers

        #region splitBody Event Handlers
        private void splitBody_Panel2_Leave(object sender, EventArgs e) {

           // Ensure that any data modified in the current window has been synched with the active node.
           SynchCurrentNodes();
        }
        #endregion splitBody Event Handlers

        #region tabExtractDef Event Handlers
        private void tabExtractDef_SelectedIndexChanged(object sender, EventArgs e) {

            try {

                while(this.splitBody.Panel2.Controls.Count > 0) {
                    this.splitBody.Panel2.Controls[0].Dispose();
                }
                
                switch(this.tabExtractDef.SelectedIndex) {
                    case 0:
                        if(this.tvLayouts.SelectedNode != null) {
                            SelectLayoutNode(this.tvLayouts.SelectedNode);
                        }
                        break;
                    case 1:
                        if(this.tvOrderData.SelectedNode != null) {
                            SelectOrderDataNode(this.tvOrderData.SelectedNode);
                        }
                        break;
                    case 2:
                        if(this.tvLimitData.SelectedNode != null) {
                            SelectLimitDataNode(this.tvLimitData.SelectedNode);
                        }
                        break;
                }
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }
        #endregion tabExtractDef Event Handlers

        #region tvLayouts Event Handlers
        private void tvLayouts_AfterSelect(object sender, TreeViewEventArgs e) {       

            try {
                SelectLayoutNode(e.Node);
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void tvLayouts_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) {

            ContextMenu mnuContext;

            try {
                if(e.Button == MouseButtons.Right)
                {
                    tvLayouts.SelectedNode = e.Node;
                    if(e.Node.Tag == null) {
                        if(BuildLayoutsContextMenu(e.Node.Text, out mnuContext)) {
                            _CurrentContextNode = e.Node;
                            mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                        }
                    } else if(e.Node.Tag is cLayout) {
                        if(BuildLayoutContextMenu((cLayout)e.Node.Tag, out mnuContext)) {
                            _CurrentContextNode = e.Node;
                            mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                        }
                    } else if(e.Node.Tag != null &&
                              e.Node.Tag is cLayoutField) {
                        if(BuildLayoutFieldContextMenu(out mnuContext)) {
                            _CurrentContextNode = e.Node;
                            mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                        }
                    } else if(e.Node.Tag is cExtractDef) {
                        if(BuildGeneralContextMenu(out mnuContext)) {
                            _CurrentContextNode = e.Node;
                            mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                        }
                    } else if(e.Node.Tag is cPostProcCodeModule) {
                        if(BuildPostProcCodeModuleContextMenu(out mnuContext)) {
                            _CurrentContextNode = e.Node;
                            mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                        }
                    }
                }
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void tvLayouts_KeyDown(object sender, KeyEventArgs e) {

            if(e.KeyCode == Keys.F2 && ((System.Windows.Forms.TreeView)sender).SelectedNode != null) {
                this.tvLayouts.SelectedNode.BeginEdit();
            } else {
                switch(e.KeyValue) {
                    case 46:
                        if(((System.Windows.Forms.TreeView)sender).SelectedNode != null &&
                           ((System.Windows.Forms.TreeView)sender).SelectedNode.Tag != null) {

                            if(((System.Windows.Forms.TreeView)sender).SelectedNode.Tag is cLayout) {
                                DeleteLayout(((System.Windows.Forms.TreeView)sender).SelectedNode);
                            } else if(((System.Windows.Forms.TreeView)sender).SelectedNode.Tag != null && 
                                      ((System.Windows.Forms.TreeView)sender).SelectedNode.Tag is cLayoutField) {
                                DeleteLayoutField(((System.Windows.Forms.TreeView)sender).SelectedNode, true);
                            } else if(((System.Windows.Forms.TreeView)sender).SelectedNode.Tag is cPostProcCodeModule) {
                                DeletePostProcCodeModule(((System.Windows.Forms.TreeView)sender).SelectedNode);
                            }
                        }
                        break;
                }
            }
        }
        #endregion tvLayouts Event Handlers

        #region tvOrderData Event Handlers
        private void tvOrderData_AfterSelect(object sender, TreeViewEventArgs e) {
            
            try {
                SelectOrderDataNode(e.Node);
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void tvOrderData_KeyDown(object sender, KeyEventArgs e) {
            switch(e.KeyValue) {
                case 46:
                    if(((System.Windows.Forms.TreeView)sender).SelectedNode != null &&
                       ((System.Windows.Forms.TreeView)sender).SelectedNode.Tag != null) {

                        if(((System.Windows.Forms.TreeView)sender).SelectedNode.Tag.ToString() == "OrderByColumn") {
                            DeleteOrderByColumn(((System.Windows.Forms.TreeView)sender).SelectedNode, true);
                        }
                    }
                    break;
            }
        }

        private void tvOrderData_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) {

            ContextMenu mnuContext;

            try {
                if(e.Button == MouseButtons.Right) {
                    if(e.Node.Tag != null) {
                        if(e.Node.Tag is List<cOrderByColumn>) {
                            if(BuildOrderByColumnsContextMenu(DecodeLayoutLevel(e.Node.Text), out mnuContext)) {
                                _CurrentContextNode = e.Node;
                                mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                            }
                        } else if(e.Node.Tag.ToString() == "OrderByColumn") {
                            if(BuildOrderByColumnContextMenu(out mnuContext)) {
                                _CurrentContextNode = e.Node;
                                mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                            }
                        }
                    }
                }
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }
        #endregion tvOrderData Event Handlers

        #region tvLimitData Event Handlers
        private void tvLimitData_AfterSelect(object sender, TreeViewEventArgs e) {

            try {
                SelectLimitDataNode(e.Node);
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void tvLimitData_KeyDown(object sender, KeyEventArgs e) {
            switch(e.KeyValue) {
                case 46:
                    if(((System.Windows.Forms.TreeView)sender).SelectedNode != null &&
                       ((System.Windows.Forms.TreeView)sender).SelectedNode.Tag != null) {

                        if(((System.Windows.Forms.TreeView)sender).SelectedNode.Tag.ToString() == "LimitItem") {
                            DeleteLimitItem(((System.Windows.Forms.TreeView)sender).SelectedNode, true);
                        }
                    }
                    break;
            }
        }

        private void tvLimitData_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e) {

            ContextMenu mnuContext;

            try {
                if(e.Button == MouseButtons.Right) {
                    if(e.Node.Tag != null) {
                        if(e.Node.Tag is List<cLimitItem>) {
                            if(BuildLimitItemsContextMenu(DecodeLayoutLevel(e.Node.Text), out mnuContext)) {
                                _CurrentContextNode = e.Node;
                                mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                            }
                        } else if(e.Node.Tag.ToString() == "LimitItem") {
                            if(BuildLimitItemContextMenu(out mnuContext)) {
                                _CurrentContextNode = e.Node;
                                mnuContext.Show((TreeView)sender, new Point(e.X, e.Y));
                            }
                        }
                    }
                }
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }
        #endregion tvLimitData Event Handlers

        #region tvDataFields Event Handlers
        private void tvDataFields_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e) {
            AddDataFieldFromNode(e.Node);
        }

        private void tvDataFields_ItemDrag(object sender, ItemDragEventArgs e) {

            try {

                _CurrentContextNode = (TreeNode)e.Item;

                // DoDragDrop sets things in motion for the drag drop operation by passing the data and the current effect
                // Data can be a bitmap, string or something that implements the IDataObject interface
                this.DoDragDrop(((TreeNode)e.Item).Text, DragDropEffects.All);
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void tvDataFields_DragOver(object sender, DragEventArgs e) {

            try {
                e.Effect = DragDropEffects.Copy;
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void tvDataFields_DragEnter(object sender, DragEventArgs e) {

            try {
                e.Effect = DragDropEffects.Copy;
            } catch(Exception ex) {
                OnOutputError(this, ex);
            }
        }

        private void tvDataFields_AfterSelect(object sender, TreeViewEventArgs e) {
            SetAddDataFieldButtonEnabledStatus();
        }

        private void tvDataFields_BeforeSelect(object sender, TreeViewCancelEventArgs e) {
            SetAddDataFieldButtonEnabledStatus();
        }

        private void tvDataFields_AfterExpand(object sender, TreeViewEventArgs e) {
            switch(e.Node.Text) {
                case "Data Fields":
                    _DataFieldsExpanded = true;
                    break;
                case "Aggregate Fields":
                    _AggregateFieldsExpanded = true;
                    break;
                case Constants.NODE_LABEL_STD_FIELDS:
                    _StandardFieldsExpanded = true;
                    break;
            }
        }

        private void tvDataFields_AfterCollapse(object sender, TreeViewEventArgs e) {
            switch(e.Node.Text) {
                case "Data Fields":
                    _DataFieldsExpanded = false;
                    break;
                case "Aggregate Fields":
                    _AggregateFieldsExpanded = false;
                    break;
                case Constants.NODE_LABEL_STD_FIELDS:
                    _StandardFieldsExpanded = false;
                    break;
            }
        }
        #endregion tvDataFields Event Handlers

        #region ExtractRunCtl Event Handlers
        private void ExtractRunCtl_EndExtractRun() {

            if(this.InvokeRequired) {
                EndExtractRunEventHandler d = new EndExtractRunEventHandler(ExtractRunCtl_EndExtractRun);
                this.Invoke(d, new object[] {});
            } else {
                this.Cursor = Cursors.Default;
                this.btnSelectExtractScriptDir.Enabled = true;
                this.mnuFile.Enabled = true;
                this.mnuView.Enabled = true;
            }
        }
        
        private void ExtractRunCtl_BeginExtractRun() {
            this.Cursor = Cursors.WaitCursor;
            Application.DoEvents();
            this.btnSelectExtractScriptDir.Enabled = false;
            this.mnuFile.Enabled = false;
            this.mnuView.Enabled = false;
        }
        #endregion ExtractRunCtl Event Handlers

        #region frmMain Event Handlers
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e) {
            try {
                e.Cancel = !PromptForSaveIfNecessary();
            } catch(Exception ex) {
                OnOutputError(this, ex);
                e.Cancel = true;
            }
        }
        #endregion frmMain Event Handlers

        private void mnuFileSetupRptPreview_Click(object sender, EventArgs e) {

            cExtractDef objExtractDef = GetExtractDefFromForm();
            frmViewSetupRpt frmRpt = new frmViewSetupRpt(objExtractDef);
            frmRpt.Show();
        }

        private void mnuFileSetupRptPrint_Click(object sender, EventArgs e) {

            cExtractDef objExtractDef = GetExtractDefFromForm();

            DialogResult result = printDialog1.ShowDialog();
            if(result == DialogResult.OK) {
                cExtractSetupRpt rpt = new cExtractSetupRpt();
                rpt.PrintRpt(objExtractDef, printDialog1.PrinterSettings.PrinterName);
                rpt.Dispose();
            }
        }

        private void tvLayouts_AfterLabelEdit(object sender, NodeLabelEditEventArgs e) {
            
            string strNewLayoutName;
            
            if(e.Node != null && e.Node.Tag != null) {
                if(e.Node.Tag is cLayout) {
                    if(e.Label != null && e.Label.Length > 0) {
                        strNewLayoutName = e.Label;
                    } else {
                        strNewLayoutName = e.Node.Text;
                    }
                    
                    if(((cLayout)e.Node.Tag).LayoutName == strNewLayoutName) {
                        e.CancelEdit = true;
                    } else if(((cLayout)e.Node.Tag).LayoutName.Trim().ToLower() == strNewLayoutName.Trim().ToLower()) {
                        ((cLayout)e.Node.Tag).LayoutName = strNewLayoutName;
                    } else if(LayoutNameExistsInTreeView(strNewLayoutName)) {
                        OnOutputMessage(
                            "Layout Name already exists in the current Extract Definition.", 
                            string.Empty,
                            MessageType.Warning,
                            MessageImportance.Essential);
                        e.CancelEdit = true;
                    } else {
                        ((cLayout)e.Node.Tag).LayoutName = strNewLayoutName;
                    }
                }
            }
        }

        private void tvLayouts_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e) {
            if(e.Node.Tag == null) {
                e.CancelEdit = true;
            } else if(e.Node.Tag is cLayout) {
                ((cLayout)e.Node.Tag).LayoutName = e.Node.Text;
            } else {
                e.CancelEdit = true;
            }
        }

        private void menuStrip1_MenuActivate(object sender, EventArgs e) {
            this.Focus();
            this.ValidateChildren();
        }
 
 
        private void InitializeCurrentContextNode(object sender, EventArgs e) {
            _CurrentContextNode = null;
        }

        //private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    cPasswordChange passwordChange = new cPasswordChange();
        //    passwordChange.ChangePassword(this._UserID, "Please enter a new password.", cPasswordChange.OldPasswordRequired);
        //}

    }
}