/*******************************************************************************
*
*    Module: frmFileNameFormatDialog
*  Filename: frmFileNameFormatDialog.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 19480 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class frmFileNameFormatDialog {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if(disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFileNameFormatDialog));
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ctlFileNameFormat1 = new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ctlFileNameFormat();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.ctlFileNameFormat2 = new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ctlFileNameFormat();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ctlFileNameFormat3 = new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.ctlFileNameFormat();
            this.colFieldName1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFieldName2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFieldName3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkZeroPadNumbers = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProcessingDateFormat = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rdoDisplayFirstDigitOnly = new System.Windows.Forms.RadioButton();
            this.rdoDisplayFullNumber = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.dataGridViewColumn1 = new System.Windows.Forms.DataGridViewColumn();
            this.dataGridViewColumn2 = new System.Windows.Forms.DataGridViewColumn();
            this.dataGridViewColumn3 = new System.Windows.Forms.DataGridViewColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.tabMain.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPage1);
            this.tabMain.Controls.Add(this.tabPage2);
            this.tabMain.Controls.Add(this.tabPage3);
            this.tabMain.Location = new System.Drawing.Point(4, 5);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(394, 346);
            this.tabMain.TabIndex = 0;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ctlFileNameFormat1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(386, 320);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Single Images";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ctlFileNameFormat1
            // 
            this.ctlFileNameFormat1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlFileNameFormat1.Location = new System.Drawing.Point(3, 3);
            this.ctlFileNameFormat1.MinimumSize = new System.Drawing.Size(383, 254);
            this.ctlFileNameFormat1.Name = "ctlFileNameFormat1";
            this.ctlFileNameFormat1.Size = new System.Drawing.Size(383, 314);
            this.ctlFileNameFormat1.TabIndex = 8;
            this.ctlFileNameFormat1.FileNameChanged += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.FileNameChangedEventHandler(this.ctlFileNameFormat1_FileNameChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ctlFileNameFormat2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(386, 320);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Images Per Transaction";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // ctlFileNameFormat2
            // 
            this.ctlFileNameFormat2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlFileNameFormat2.Location = new System.Drawing.Point(3, 3);
            this.ctlFileNameFormat2.MinimumSize = new System.Drawing.Size(383, 254);
            this.ctlFileNameFormat2.Name = "ctlFileNameFormat2";
            this.ctlFileNameFormat2.Size = new System.Drawing.Size(383, 314);
            this.ctlFileNameFormat2.TabIndex = 0;
            this.ctlFileNameFormat2.FileNameChanged += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.FileNameChangedEventHandler(this.ctlFileNameFormat2_FileNameChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.ctlFileNameFormat3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(386, 320);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Images Per Batch";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // ctlFileNameFormat3
            // 
            this.ctlFileNameFormat3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlFileNameFormat3.Location = new System.Drawing.Point(3, 3);
            this.ctlFileNameFormat3.MinimumSize = new System.Drawing.Size(383, 254);
            this.ctlFileNameFormat3.Name = "ctlFileNameFormat3";
            this.ctlFileNameFormat3.Size = new System.Drawing.Size(383, 314);
            this.ctlFileNameFormat3.TabIndex = 0;
            this.ctlFileNameFormat3.FileNameChanged += new WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.FileNameChangedEventHandler(this.ctlFileNameFormat3_FileNameChanged);
            // 
            // colFieldName1
            // 
            this.colFieldName1.HeaderText = "Field Name";
            this.colFieldName1.Name = "colFieldName1";
            this.colFieldName1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldName1.Width = 150;
            // 
            // colFieldName2
            // 
            this.colFieldName2.HeaderText = "Field Name";
            this.colFieldName2.Name = "colFieldName2";
            this.colFieldName2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldName2.Width = 150;
            // 
            // colFieldName3
            // 
            this.colFieldName3.HeaderText = "Field Name";
            this.colFieldName3.Name = "colFieldName3";
            this.colFieldName3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldName3.Width = 150;
            // 
            // chkZeroPadNumbers
            // 
            this.chkZeroPadNumbers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkZeroPadNumbers.AutoSize = true;
            this.chkZeroPadNumbers.Location = new System.Drawing.Point(415, 235);
            this.chkZeroPadNumbers.Name = "chkZeroPadNumbers";
            this.chkZeroPadNumbers.Size = new System.Drawing.Size(115, 17);
            this.chkZeroPadNumbers.TabIndex = 5;
            this.chkZeroPadNumbers.Text = "Zero-pad numbers:";
            this.chkZeroPadNumbers.UseVisualStyleBackColor = true;
            this.chkZeroPadNumbers.CheckedChanged += new System.EventHandler(this.chkZeroPadNumbers_CheckedChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 44);
            this.label1.TabIndex = 2;
            this.label1.Text = "Year:    YY or YYYY Month: MM         Day:     DD";
            // 
            // txtProcessingDateFormat
            // 
            this.txtProcessingDateFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProcessingDateFormat.Location = new System.Drawing.Point(11, 16);
            this.txtProcessingDateFormat.Name = "txtProcessingDateFormat";
            this.txtProcessingDateFormat.Size = new System.Drawing.Size(120, 20);
            this.txtProcessingDateFormat.TabIndex = 6;
            this.txtProcessingDateFormat.Text = "MMDDYY";
            this.txtProcessingDateFormat.TextChanged += new System.EventHandler(this.txtProcessingDateFormat_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.rdoDisplayFirstDigitOnly);
            this.groupBox1.Controls.Add(this.rdoDisplayFullNumber);
            this.groupBox1.Location = new System.Drawing.Point(404, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(143, 68);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Batch Type Format";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 9;
            // 
            // rdoDisplayFirstDigitOnly
            // 
            this.rdoDisplayFirstDigitOnly.AutoSize = true;
            this.rdoDisplayFirstDigitOnly.Location = new System.Drawing.Point(11, 42);
            this.rdoDisplayFirstDigitOnly.Name = "rdoDisplayFirstDigitOnly";
            this.rdoDisplayFirstDigitOnly.Size = new System.Drawing.Size(122, 17);
            this.rdoDisplayFirstDigitOnly.TabIndex = 8;
            this.rdoDisplayFirstDigitOnly.TabStop = true;
            this.rdoDisplayFirstDigitOnly.Text = "Display first digit only";
            this.rdoDisplayFirstDigitOnly.UseVisualStyleBackColor = true;
            this.rdoDisplayFirstDigitOnly.CheckedChanged += new System.EventHandler(this.rdoDisplayFirstDigitOnly_CheckedChanged);
            // 
            // rdoDisplayFullNumber
            // 
            this.rdoDisplayFullNumber.AutoSize = true;
            this.rdoDisplayFullNumber.Checked = true;
            this.rdoDisplayFullNumber.Location = new System.Drawing.Point(11, 19);
            this.rdoDisplayFullNumber.Name = "rdoDisplayFullNumber";
            this.rdoDisplayFullNumber.Size = new System.Drawing.Size(113, 17);
            this.rdoDisplayFullNumber.TabIndex = 7;
            this.rdoDisplayFullNumber.TabStop = true;
            this.rdoDisplayFullNumber.Text = "Display full number";
            this.rdoDisplayFullNumber.UseVisualStyleBackColor = true;
            this.rdoDisplayFullNumber.CheckedChanged += new System.EventHandler(this.rdoDisplayFullNumber_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.Location = new System.Drawing.Point(284, 386);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOk.Location = new System.Drawing.Point(194, 386);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 9;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // dataGridViewColumn1
            // 
            this.dataGridViewColumn1.HeaderText = "Field Name";
            this.dataGridViewColumn1.Name = "dataGridViewColumn1";
            // 
            // dataGridViewColumn2
            // 
            this.dataGridViewColumn2.HeaderText = "Field Name";
            this.dataGridViewColumn2.Name = "dataGridViewColumn2";
            // 
            // dataGridViewColumn3
            // 
            this.dataGridViewColumn3.HeaderText = "Field Name";
            this.dataGridViewColumn3.Name = "dataGridViewColumn3";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtProcessingDateFormat);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(404, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(143, 90);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Processing Date Format";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 9;
            // 
            // txtFileName
            // 
            this.txtFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFileName.Location = new System.Drawing.Point(4, 357);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(543, 20);
            this.txtFileName.TabIndex = 11;
            // 
            // frmFileNameFormatDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 420);
            this.ControlBox = false;
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.chkZeroPadNumbers);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFileNameFormatDialog";
            this.Text = "Image File Name Format";
            this.Load += new System.EventHandler(this.frmFileNameFormatDialog_Load);
            this.tabMain.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox chkZeroPadNumbers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtProcessingDateFormat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoDisplayFirstDigitOnly;
        private System.Windows.Forms.RadioButton rdoDisplayFullNumber;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldName1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldName2;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldName3;
        private System.Windows.Forms.DataGridViewColumn dataGridViewColumn1;
        private System.Windows.Forms.DataGridViewColumn dataGridViewColumn2;
        private System.Windows.Forms.DataGridViewColumn dataGridViewColumn3;
        private ctlFileNameFormat ctlFileNameFormat1;
        private ctlFileNameFormat ctlFileNameFormat2;
        private ctlFileNameFormat ctlFileNameFormat3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFileName;
    }
}
