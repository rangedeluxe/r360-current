using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    //internal interface IFormatDialog {

    //    string Value {
    //        get;
    //    }

    //    DialogResult ShowDialog();
    //}

    internal partial class frmFormatDateDialog : Form {

        private string _Value = string.Empty;
        private DateTime _Now;

        public frmFormatDateDialog(string selectedFormat) {
            InitializeComponent();

            _Now = DateTime.Now;

            this.txtSelectedFormat.Text = selectedFormat;

            foreach(string str in this.lstFormat.Items) {
                this.lstExample.Items.Add(ExtractAPI.CustomFormat.FormatValue(_Now, str));
            }
        }

        private void frmFormatDateDialog_Load(object sender, EventArgs e) 
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }

        public string Value {
            get {
                return(_Value);
            }
        }

        private void btnOk_Click(object sender, EventArgs e) {
            _Value = this.txtSelectedFormat.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            _Value = string.Empty;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void lstFormat_SelectedIndexChanged(object sender, EventArgs e) {
            this.lstExample.SelectedIndex = this.lstFormat.SelectedIndex;
            this.txtSelectedFormat.Text = this.lstFormat.SelectedItem.ToString();
        }

        private void lstExample_SelectedIndexChanged(object sender, EventArgs e) {
            this.lstFormat.SelectedIndex = this.lstExample.SelectedIndex;
        }

        private void btnTest_Click(object sender, EventArgs e) {
            this.txtTestResult.Text = ExtractAPI.CustomFormat.FormatValue(_Now, this.txtSelectedFormat.Text);
        }

        private void lstExample_DoubleClick(object sender, EventArgs e) {
            _Value = this.txtSelectedFormat.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void lstFormat_DoubleClick(object sender, EventArgs e) {
            _Value = this.txtSelectedFormat.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}