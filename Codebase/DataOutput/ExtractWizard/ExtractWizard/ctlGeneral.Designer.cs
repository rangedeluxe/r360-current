/*******************************************************************************
*
*    Module: ctlGeneral
*  Filename: ctlGeneral.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
* WI 129978 BLR 02/20/2014
*   -Hid groupbox for post-processing. 
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class ctlGeneral {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblImageFileNameSingle = new System.Windows.Forms.Label();
            this.lblImageFileNamePerTran = new System.Windows.Forms.Label();
            this.lblImageFileNamePerBatch = new System.Windows.Forms.Label();
            this.btnImageFileNames = new System.Windows.Forms.Button();
            this.grpExtractFileOptions = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSelectExtractFilesZipPath = new System.Windows.Forms.Button();
            this.btnSelectImageFilePath = new System.Windows.Forms.Button();
            this.txtZipFilePath = new System.Windows.Forms.TextBox();
            this.btnSelectLogFilePath = new System.Windows.Forms.Button();
            this.chkZipOutputFiles = new System.Windows.Forms.CheckBox();
            this.btnSelectExtractFileLocation = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtImageFilePath = new System.Windows.Forms.TextBox();
            this.txtLogFilePath = new System.Windows.Forms.TextBox();
            this.txtExtractFilePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectPostProcDLLFileName = new System.Windows.Forms.Button();
            this.cboTimeStamp = new System.Windows.Forms.ComboBox();
            this.btnAddField = new System.Windows.Forms.Button();
            this.chkUsePostProcessingDLL = new System.Windows.Forms.CheckBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtPostProcessingDLLFileName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.grpExtractRecordOptions = new System.Windows.Forms.GroupBox();
            this.cboRecordDelimiter = new System.Windows.Forms.ComboBox();
            this.cboFieldDelimiter = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chkShowNULLAsSpace = new System.Windows.Forms.CheckBox();
            this.chkEncloseFieldDataInQuotes = new System.Windows.Forms.CheckBox();
            this.chkOmitPadding = new System.Windows.Forms.CheckBox();
            this.cbIncludeFooter = new System.Windows.Forms.CheckBox();
            this.btnTestExtract = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkIncludeImageFolderPath = new System.Windows.Forms.CheckBox();
            this.rdoImageFileFormatPdf = new System.Windows.Forms.RadioButton();
            this.rdoImageFileFormatSendToPrinter = new System.Windows.Forms.RadioButton();
            this.btnSelectPrinter = new System.Windows.Forms.Button();
            this.rdoImageFileFormatNone = new System.Windows.Forms.RadioButton();
            this.rdoImageFileFormatSingleMultiTIFF = new System.Windows.Forms.RadioButton();
            this.txtPrinterName = new System.Windows.Forms.TextBox();
            this.grpExtractImageOptions = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.grpExtractFileOptions.SuspendLayout();
            this.grpExtractRecordOptions.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpExtractImageOptions.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(17, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "File Name: Single";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "File Name: Per Tran";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "File Name: Per Batch";
            // 
            // lblImageFileNameSingle
            // 
            this.lblImageFileNameSingle.AutoSize = true;
            this.lblImageFileNameSingle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblImageFileNameSingle.ForeColor = System.Drawing.Color.Maroon;
            this.lblImageFileNameSingle.Location = new System.Drawing.Point(130, 22);
            this.lblImageFileNameSingle.Name = "lblImageFileNameSingle";
            this.lblImageFileNameSingle.Size = new System.Drawing.Size(42, 13);
            this.lblImageFileNameSingle.TabIndex = 5;
            this.lblImageFileNameSingle.Text = "NONE";
            // 
            // lblImageFileNamePerTran
            // 
            this.lblImageFileNamePerTran.AutoSize = true;
            this.lblImageFileNamePerTran.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblImageFileNamePerTran.ForeColor = System.Drawing.Color.Maroon;
            this.lblImageFileNamePerTran.Location = new System.Drawing.Point(130, 41);
            this.lblImageFileNamePerTran.Name = "lblImageFileNamePerTran";
            this.lblImageFileNamePerTran.Size = new System.Drawing.Size(42, 13);
            this.lblImageFileNamePerTran.TabIndex = 4;
            this.lblImageFileNamePerTran.Text = "NONE";
            // 
            // lblImageFileNamePerBatch
            // 
            this.lblImageFileNamePerBatch.AutoSize = true;
            this.lblImageFileNamePerBatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblImageFileNamePerBatch.ForeColor = System.Drawing.Color.Maroon;
            this.lblImageFileNamePerBatch.Location = new System.Drawing.Point(130, 62);
            this.lblImageFileNamePerBatch.Name = "lblImageFileNamePerBatch";
            this.lblImageFileNamePerBatch.Size = new System.Drawing.Size(42, 13);
            this.lblImageFileNamePerBatch.TabIndex = 3;
            this.lblImageFileNamePerBatch.Text = "NONE";
            // 
            // btnImageFileNames
            // 
            this.btnImageFileNames.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImageFileNames.Location = new System.Drawing.Point(450, 84);
            this.btnImageFileNames.Name = "btnImageFileNames";
            this.btnImageFileNames.Size = new System.Drawing.Size(81, 21);
            this.btnImageFileNames.TabIndex = 12;
            this.btnImageFileNames.Text = "File Names...";
            this.btnImageFileNames.UseVisualStyleBackColor = true;
            this.btnImageFileNames.Click += new System.EventHandler(this.btnImageFileNames_Click);
            // 
            // grpExtractFileOptions
            // 
            this.grpExtractFileOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpExtractFileOptions.Controls.Add(this.label7);
            this.grpExtractFileOptions.Controls.Add(this.btnSelectExtractFilesZipPath);
            this.grpExtractFileOptions.Controls.Add(this.btnSelectImageFilePath);
            this.grpExtractFileOptions.Controls.Add(this.txtZipFilePath);
            this.grpExtractFileOptions.Controls.Add(this.btnSelectLogFilePath);
            this.grpExtractFileOptions.Controls.Add(this.chkZipOutputFiles);
            this.grpExtractFileOptions.Controls.Add(this.btnSelectExtractFileLocation);
            this.grpExtractFileOptions.Controls.Add(this.label1);
            this.grpExtractFileOptions.Controls.Add(this.txtImageFilePath);
            this.grpExtractFileOptions.Controls.Add(this.txtLogFilePath);
            this.grpExtractFileOptions.Controls.Add(this.txtExtractFilePath);
            this.grpExtractFileOptions.Controls.Add(this.label4);
            this.grpExtractFileOptions.Controls.Add(this.label3);
            this.grpExtractFileOptions.Location = new System.Drawing.Point(4, 3);
            this.grpExtractFileOptions.Name = "grpExtractFileOptions";
            this.grpExtractFileOptions.Size = new System.Drawing.Size(550, 151);
            this.grpExtractFileOptions.TabIndex = 1;
            this.grpExtractFileOptions.TabStop = false;
            this.grpExtractFileOptions.Text = "Output Files";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(52, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Zip File Path:";
            // 
            // btnSelectExtractFilesZipPath
            // 
            this.btnSelectExtractFilesZipPath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectExtractFilesZipPath.Location = new System.Drawing.Point(450, 117);
            this.btnSelectExtractFilesZipPath.Name = "btnSelectExtractFilesZipPath";
            this.btnSelectExtractFilesZipPath.Size = new System.Drawing.Size(26, 21);
            this.btnSelectExtractFilesZipPath.TabIndex = 16;
            this.btnSelectExtractFilesZipPath.Text = "...";
            this.btnSelectExtractFilesZipPath.UseVisualStyleBackColor = true;
            this.btnSelectExtractFilesZipPath.Click += new System.EventHandler(this.btnSelectExtractFilesZipPath_Click);
            // 
            // btnSelectImageFilePath
            // 
            this.btnSelectImageFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectImageFilePath.Location = new System.Drawing.Point(450, 74);
            this.btnSelectImageFilePath.Name = "btnSelectImageFilePath";
            this.btnSelectImageFilePath.Size = new System.Drawing.Size(26, 21);
            this.btnSelectImageFilePath.TabIndex = 5;
            this.btnSelectImageFilePath.Text = "...";
            this.btnSelectImageFilePath.UseVisualStyleBackColor = true;
            this.btnSelectImageFilePath.Click += new System.EventHandler(this.btnSelectImageFilePath_Click);
            // 
            // txtZipFilePath
            // 
            this.txtZipFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtZipFilePath.Location = new System.Drawing.Point(127, 117);
            this.txtZipFilePath.MaxLength = 1024;
            this.txtZipFilePath.Name = "txtZipFilePath";
            this.txtZipFilePath.Size = new System.Drawing.Size(315, 20);
            this.txtZipFilePath.TabIndex = 15;
            this.txtZipFilePath.TextChanged += new System.EventHandler(this.txtZipFilePath_TextChanged);
            // 
            // btnSelectLogFilePath
            // 
            this.btnSelectLogFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectLogFilePath.Location = new System.Drawing.Point(450, 49);
            this.btnSelectLogFilePath.Name = "btnSelectLogFilePath";
            this.btnSelectLogFilePath.Size = new System.Drawing.Size(26, 21);
            this.btnSelectLogFilePath.TabIndex = 6;
            this.btnSelectLogFilePath.Text = "...";
            this.btnSelectLogFilePath.UseVisualStyleBackColor = true;
            this.btnSelectLogFilePath.Click += new System.EventHandler(this.btnSelectLogFilePath_Click);
            // 
            // chkZipOutputFiles
            // 
            this.chkZipOutputFiles.AutoSize = true;
            this.chkZipOutputFiles.Location = new System.Drawing.Point(16, 100);
            this.chkZipOutputFiles.Name = "chkZipOutputFiles";
            this.chkZipOutputFiles.Size = new System.Drawing.Size(101, 17);
            this.chkZipOutputFiles.TabIndex = 14;
            this.chkZipOutputFiles.Text = "Zip Extract Files";
            this.chkZipOutputFiles.UseVisualStyleBackColor = true;
            this.chkZipOutputFiles.CheckedChanged += new System.EventHandler(this.chkZipOutputFiles_CheckedChanged);
            // 
            // btnSelectExtractFileLocation
            // 
            this.btnSelectExtractFileLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectExtractFileLocation.Location = new System.Drawing.Point(450, 23);
            this.btnSelectExtractFileLocation.Name = "btnSelectExtractFileLocation";
            this.btnSelectExtractFileLocation.Size = new System.Drawing.Size(26, 21);
            this.btnSelectExtractFileLocation.TabIndex = 7;
            this.btnSelectExtractFileLocation.Text = "...";
            this.btnSelectExtractFileLocation.UseVisualStyleBackColor = true;
            this.btnSelectExtractFileLocation.Click += new System.EventHandler(this.btnSelectExtractFileLocation_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Image File Path:";
            // 
            // txtImageFilePath
            // 
            this.txtImageFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImageFilePath.Location = new System.Drawing.Point(127, 74);
            this.txtImageFilePath.MaxLength = 1024;
            this.txtImageFilePath.Name = "txtImageFilePath";
            this.txtImageFilePath.Size = new System.Drawing.Size(317, 20);
            this.txtImageFilePath.TabIndex = 2;
            this.txtImageFilePath.TextChanged += new System.EventHandler(this.txtImageFilePath_TextChanged);
            // 
            // txtLogFilePath
            // 
            this.txtLogFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogFilePath.Location = new System.Drawing.Point(127, 49);
            this.txtLogFilePath.MaxLength = 1024;
            this.txtLogFilePath.Name = "txtLogFilePath";
            this.txtLogFilePath.Size = new System.Drawing.Size(317, 20);
            this.txtLogFilePath.TabIndex = 1;
            this.txtLogFilePath.TextChanged += new System.EventHandler(this.txtLogFilePath_TextChanged);
            // 
            // txtExtractFilePath
            // 
            this.txtExtractFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtExtractFilePath.Location = new System.Drawing.Point(127, 23);
            this.txtExtractFilePath.MaxLength = 1024;
            this.txtExtractFilePath.Name = "txtExtractFilePath";
            this.txtExtractFilePath.Size = new System.Drawing.Size(317, 20);
            this.txtExtractFilePath.TabIndex = 0;
            this.txtExtractFilePath.TextChanged += new System.EventHandler(this.txtExtractFilePath_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Extract Data File Path:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Log File Path:";
            // 
            // btnSelectPostProcDLLFileName
            // 
            this.btnSelectPostProcDLLFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectPostProcDLLFileName.Enabled = false;
            this.btnSelectPostProcDLLFileName.Location = new System.Drawing.Point(450, 69);
            this.btnSelectPostProcDLLFileName.Name = "btnSelectPostProcDLLFileName";
            this.btnSelectPostProcDLLFileName.Size = new System.Drawing.Size(26, 21);
            this.btnSelectPostProcDLLFileName.TabIndex = 4;
            this.btnSelectPostProcDLLFileName.Text = "...";
            this.btnSelectPostProcDLLFileName.UseVisualStyleBackColor = true;
            this.btnSelectPostProcDLLFileName.Visible = false;
            this.btnSelectPostProcDLLFileName.Click += new System.EventHandler(this.btnSelectPostProcDLLFileName_Click);
            // 
            // cboTimeStamp
            // 
            this.cboTimeStamp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTimeStamp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTimeStamp.FormattingEnabled = true;
            this.cboTimeStamp.Location = new System.Drawing.Point(197, 19);
            this.cboTimeStamp.Name = "cboTimeStamp";
            this.cboTimeStamp.Size = new System.Drawing.Size(247, 21);
            this.cboTimeStamp.TabIndex = 5;
            this.cboTimeStamp.SelectedIndexChanged += new System.EventHandler(this.cboTimeStamp_SelectedIndexChanged);
            // 
            // btnAddField
            // 
            this.btnAddField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddField.Location = new System.Drawing.Point(450, 19);
            this.btnAddField.Name = "btnAddField";
            this.btnAddField.Size = new System.Drawing.Size(81, 21);
            this.btnAddField.TabIndex = 6;
            this.btnAddField.Text = "Add Field";
            this.btnAddField.UseVisualStyleBackColor = true;
            this.btnAddField.Click += new System.EventHandler(this.btnAddField_Click);
            // 
            // chkUsePostProcessingDLL
            // 
            this.chkUsePostProcessingDLL.AutoSize = true;
            this.chkUsePostProcessingDLL.Location = new System.Drawing.Point(16, 52);
            this.chkUsePostProcessingDLL.Name = "chkUsePostProcessingDLL";
            this.chkUsePostProcessingDLL.Size = new System.Drawing.Size(147, 17);
            this.chkUsePostProcessingDLL.TabIndex = 3;
            this.chkUsePostProcessingDLL.Text = "Use Post-Processing DLL";
            this.chkUsePostProcessingDLL.UseVisualStyleBackColor = true;
            this.chkUsePostProcessingDLL.Visible = false;
            this.chkUsePostProcessingDLL.CheckedChanged += new System.EventHandler(this.chkUsePostProcessingDLL_CheckedChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(18, 19);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(164, 13);
            this.label54.TabIndex = 2;
            this.label54.Text = "At completion, write timestamp to:";
            // 
            // txtPostProcessingDLLFileName
            // 
            this.txtPostProcessingDLLFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPostProcessingDLLFileName.Location = new System.Drawing.Point(197, 69);
            this.txtPostProcessingDLLFileName.MaxLength = 1024;
            this.txtPostProcessingDLLFileName.Name = "txtPostProcessingDLLFileName";
            this.txtPostProcessingDLLFileName.Size = new System.Drawing.Size(247, 20);
            this.txtPostProcessingDLLFileName.TabIndex = 4;
            this.txtPostProcessingDLLFileName.Visible = false;
            this.txtPostProcessingDLLFileName.TextChanged += new System.EventHandler(this.txtPostProcessingDLLFileName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(38, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Post-Processing DLL File Path:";
            this.label2.Visible = false;
            // 
            // grpExtractRecordOptions
            // 
            this.grpExtractRecordOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpExtractRecordOptions.Controls.Add(this.cboRecordDelimiter);
            this.grpExtractRecordOptions.Controls.Add(this.cboFieldDelimiter);
            this.grpExtractRecordOptions.Controls.Add(this.label6);
            this.grpExtractRecordOptions.Controls.Add(this.label5);
            this.grpExtractRecordOptions.Controls.Add(this.chkShowNULLAsSpace);
            this.grpExtractRecordOptions.Controls.Add(this.chkEncloseFieldDataInQuotes);
            this.grpExtractRecordOptions.Controls.Add(this.chkOmitPadding);
            this.grpExtractRecordOptions.Location = new System.Drawing.Point(3, 144);
            this.grpExtractRecordOptions.Name = "grpExtractRecordOptions";
            this.grpExtractRecordOptions.Size = new System.Drawing.Size(550, 95);
            this.grpExtractRecordOptions.TabIndex = 3;
            this.grpExtractRecordOptions.TabStop = false;
            this.grpExtractRecordOptions.Text = "Data Formatting";
            // 
            // cboRecordDelimiter
            // 
            this.cboRecordDelimiter.FormattingEnabled = true;
            this.cboRecordDelimiter.Items.AddRange(new object[] {
            "\\n",
            "\\n\\n",
            "\\t",
            "\\t\\t",
            ",",
            ";",
            ":",
            "|",
            "!",
            "@",
            "#",
            "%"});
            this.cboRecordDelimiter.Location = new System.Drawing.Point(107, 59);
            this.cboRecordDelimiter.Name = "cboRecordDelimiter";
            this.cboRecordDelimiter.Size = new System.Drawing.Size(56, 21);
            this.cboRecordDelimiter.TabIndex = 14;
            this.cboRecordDelimiter.SelectedIndexChanged += new System.EventHandler(this.cboRecordDelimiter_SelectedIndexChanged);
            this.cboRecordDelimiter.TextChanged += new System.EventHandler(this.cboRecordDelimiter_TextChanged);
            // 
            // cboFieldDelimiter
            // 
            this.cboFieldDelimiter.FormattingEnabled = true;
            this.cboFieldDelimiter.Items.AddRange(new object[] {
            ",",
            ";",
            ":",
            "|",
            "!",
            "@",
            "#",
            "%"});
            this.cboFieldDelimiter.Location = new System.Drawing.Point(107, 22);
            this.cboFieldDelimiter.Name = "cboFieldDelimiter";
            this.cboFieldDelimiter.Size = new System.Drawing.Size(56, 21);
            this.cboFieldDelimiter.TabIndex = 13;
            this.cboFieldDelimiter.SelectedIndexChanged += new System.EventHandler(this.cboFieldDelimiter_SelectedIndexChanged);
            this.cboFieldDelimiter.TextChanged += new System.EventHandler(this.cboFieldDelimiter_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Field Delimiter:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Record Delimiter:";
            // 
            // chkShowNULLAsSpace
            // 
            this.chkShowNULLAsSpace.AutoSize = true;
            this.chkShowNULLAsSpace.Location = new System.Drawing.Point(263, 64);
            this.chkShowNULLAsSpace.Name = "chkShowNULLAsSpace";
            this.chkShowNULLAsSpace.Size = new System.Drawing.Size(132, 17);
            this.chkShowNULLAsSpace.TabIndex = 17;
            this.chkShowNULLAsSpace.Text = "Show NULL as Space";
            this.chkShowNULLAsSpace.UseVisualStyleBackColor = true;
            this.chkShowNULLAsSpace.CheckedChanged += new System.EventHandler(this.chkShowNULLAsSpace_CheckedChanged);
            // 
            // chkEncloseFieldDataInQuotes
            // 
            this.chkEncloseFieldDataInQuotes.AutoSize = true;
            this.chkEncloseFieldDataInQuotes.Location = new System.Drawing.Point(263, 41);
            this.chkEncloseFieldDataInQuotes.Name = "chkEncloseFieldDataInQuotes";
            this.chkEncloseFieldDataInQuotes.Size = new System.Drawing.Size(163, 17);
            this.chkEncloseFieldDataInQuotes.TabIndex = 16;
            this.chkEncloseFieldDataInQuotes.Text = "Enclose Field Data in Quotes";
            this.chkEncloseFieldDataInQuotes.UseVisualStyleBackColor = true;
            this.chkEncloseFieldDataInQuotes.CheckedChanged += new System.EventHandler(this.chkEncloseFieldDataInQuotes_CheckedChanged);
            // 
            // chkOmitPadding
            // 
            this.chkOmitPadding.AutoSize = true;
            this.chkOmitPadding.Location = new System.Drawing.Point(263, 16);
            this.chkOmitPadding.Name = "chkOmitPadding";
            this.chkOmitPadding.Size = new System.Drawing.Size(89, 17);
            this.chkOmitPadding.TabIndex = 15;
            this.chkOmitPadding.Text = "Omit Padding";
            this.chkOmitPadding.UseVisualStyleBackColor = true;
            this.chkOmitPadding.CheckedChanged += new System.EventHandler(this.chkOmitPadding_CheckedChanged);
            // 
            // cbIncludeFooter
            // 
            this.cbIncludeFooter.AutoSize = true;
            this.cbIncludeFooter.Location = new System.Drawing.Point(172, 63);
            this.cbIncludeFooter.Name = "cbIncludeFooter";
            this.cbIncludeFooter.Size = new System.Drawing.Size(126, 17);
            this.cbIncludeFooter.TabIndex = 18;
            this.cbIncludeFooter.Text = "Include Image Footer";
            this.cbIncludeFooter.UseVisualStyleBackColor = true;
            this.cbIncludeFooter.Visible = false;
            this.cbIncludeFooter.CheckedChanged += new System.EventHandler(this.cbIncludeFooter_CheckedChanged);
            // 
            // btnTestExtract
            // 
            this.btnTestExtract.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestExtract.Location = new System.Drawing.Point(454, 585);
            this.btnTestExtract.Name = "btnTestExtract";
            this.btnTestExtract.Size = new System.Drawing.Size(81, 21);
            this.btnTestExtract.TabIndex = 8;
            this.btnTestExtract.Text = "Test Extract";
            this.btnTestExtract.UseVisualStyleBackColor = true;
            this.btnTestExtract.Click += new System.EventHandler(this.btnTestExtract_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.chkIncludeImageFolderPath);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.btnImageFileNames);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.lblImageFileNamePerBatch);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lblImageFileNamePerTran);
            this.groupBox1.Controls.Add(this.lblImageFileNameSingle);
            this.groupBox1.Location = new System.Drawing.Point(4, 368);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(549, 110);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Image File Names";
            // 
            // chkIncludeImageFolderPath
            // 
            this.chkIncludeImageFolderPath.AutoSize = true;
            this.chkIncludeImageFolderPath.Location = new System.Drawing.Point(16, 87);
            this.chkIncludeImageFolderPath.Name = "chkIncludeImageFolderPath";
            this.chkIncludeImageFolderPath.Size = new System.Drawing.Size(155, 17);
            this.chkIncludeImageFolderPath.TabIndex = 14;
            this.chkIncludeImageFolderPath.Text = "Include Image Folder Paths";
            this.chkIncludeImageFolderPath.UseVisualStyleBackColor = true;
            this.chkIncludeImageFolderPath.CheckedChanged += new System.EventHandler(this.chkIncludeImageFolderPath_CheckedChanged);
            // 
            // rdoImageFileFormatPdf
            // 
            this.rdoImageFileFormatPdf.AutoSize = true;
            this.rdoImageFileFormatPdf.Location = new System.Drawing.Point(16, 63);
            this.rdoImageFileFormatPdf.Name = "rdoImageFileFormatPdf";
            this.rdoImageFileFormatPdf.Size = new System.Drawing.Size(150, 17);
            this.rdoImageFileFormatPdf.TabIndex = 10;
            this.rdoImageFileFormatPdf.Text = "Generate PDF Documents";
            this.rdoImageFileFormatPdf.UseVisualStyleBackColor = true;
            this.rdoImageFileFormatPdf.CheckedChanged += new System.EventHandler(this.rdoImageFileFormatPdf_CheckedChanged);
            // 
            // rdoImageFileFormatSendToPrinter
            // 
            this.rdoImageFileFormatSendToPrinter.AutoSize = true;
            this.rdoImageFileFormatSendToPrinter.Location = new System.Drawing.Point(16, 86);
            this.rdoImageFileFormatSendToPrinter.Name = "rdoImageFileFormatSendToPrinter";
            this.rdoImageFileFormatSendToPrinter.Size = new System.Drawing.Size(95, 17);
            this.rdoImageFileFormatSendToPrinter.TabIndex = 9;
            this.rdoImageFileFormatSendToPrinter.Text = "Send to Printer";
            this.rdoImageFileFormatSendToPrinter.UseVisualStyleBackColor = true;
            this.rdoImageFileFormatSendToPrinter.CheckedChanged += new System.EventHandler(this.rdoImageFileFormatSendToPrinter_CheckedChanged);
            // 
            // btnSelectPrinter
            // 
            this.btnSelectPrinter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectPrinter.Location = new System.Drawing.Point(450, 87);
            this.btnSelectPrinter.Name = "btnSelectPrinter";
            this.btnSelectPrinter.Size = new System.Drawing.Size(81, 21);
            this.btnSelectPrinter.TabIndex = 12;
            this.btnSelectPrinter.Text = "Select Printer...";
            this.btnSelectPrinter.UseVisualStyleBackColor = true;
            this.btnSelectPrinter.Click += new System.EventHandler(this.btnSelectPrinter_Click);
            // 
            // rdoImageFileFormatNone
            // 
            this.rdoImageFileFormatNone.AutoSize = true;
            this.rdoImageFileFormatNone.Checked = true;
            this.rdoImageFileFormatNone.Location = new System.Drawing.Point(16, 19);
            this.rdoImageFileFormatNone.Name = "rdoImageFileFormatNone";
            this.rdoImageFileFormatNone.Size = new System.Drawing.Size(51, 17);
            this.rdoImageFileFormatNone.TabIndex = 7;
            this.rdoImageFileFormatNone.TabStop = true;
            this.rdoImageFileFormatNone.Text = "None";
            this.rdoImageFileFormatNone.UseVisualStyleBackColor = true;
            this.rdoImageFileFormatNone.CheckedChanged += new System.EventHandler(this.rdoImageFileFormatNone_CheckedChanged);
            // 
            // rdoImageFileFormatSingleMultiTIFF
            // 
            this.rdoImageFileFormatSingleMultiTIFF.AutoSize = true;
            this.rdoImageFileFormatSingleMultiTIFF.Location = new System.Drawing.Point(16, 40);
            this.rdoImageFileFormatSingleMultiTIFF.Name = "rdoImageFileFormatSingleMultiTIFF";
            this.rdoImageFileFormatSingleMultiTIFF.Size = new System.Drawing.Size(118, 17);
            this.rdoImageFileFormatSingleMultiTIFF.TabIndex = 8;
            this.rdoImageFileFormatSingleMultiTIFF.Text = "Generate TIFF Files";
            this.rdoImageFileFormatSingleMultiTIFF.UseVisualStyleBackColor = true;
            this.rdoImageFileFormatSingleMultiTIFF.CheckedChanged += new System.EventHandler(this.rdoImageFileFormatSingleMultiTIFF_CheckedChanged);
            // 
            // txtPrinterName
            // 
            this.txtPrinterName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrinterName.Location = new System.Drawing.Point(127, 87);
            this.txtPrinterName.MaxLength = 1024;
            this.txtPrinterName.Name = "txtPrinterName";
            this.txtPrinterName.ReadOnly = true;
            this.txtPrinterName.Size = new System.Drawing.Size(315, 20);
            this.txtPrinterName.TabIndex = 13;
            // 
            // grpExtractImageOptions
            // 
            this.grpExtractImageOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpExtractImageOptions.Controls.Add(this.cbIncludeFooter);
            this.grpExtractImageOptions.Controls.Add(this.rdoImageFileFormatNone);
            this.grpExtractImageOptions.Controls.Add(this.rdoImageFileFormatPdf);
            this.grpExtractImageOptions.Controls.Add(this.txtPrinterName);
            this.grpExtractImageOptions.Controls.Add(this.rdoImageFileFormatSingleMultiTIFF);
            this.grpExtractImageOptions.Controls.Add(this.rdoImageFileFormatSendToPrinter);
            this.grpExtractImageOptions.Controls.Add(this.btnSelectPrinter);
            this.grpExtractImageOptions.Location = new System.Drawing.Point(4, 245);
            this.grpExtractImageOptions.Name = "grpExtractImageOptions";
            this.grpExtractImageOptions.Size = new System.Drawing.Size(549, 117);
            this.grpExtractImageOptions.TabIndex = 2;
            this.grpExtractImageOptions.TabStop = false;
            this.grpExtractImageOptions.Text = "Image Format";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.btnSelectPostProcDLLFileName);
            this.groupBox3.Controls.Add(this.btnAddField);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.txtPostProcessingDLLFileName);
            this.groupBox3.Controls.Add(this.chkUsePostProcessingDLL);
            this.groupBox3.Controls.Add(this.cboTimeStamp);
            this.groupBox3.Controls.Add(this.label54);
            this.groupBox3.Location = new System.Drawing.Point(4, 484);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(550, 95);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Post-Processing Options";
            // 
            // ctlGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.grpExtractImageOptions);
            this.Controls.Add(this.btnTestExtract);
            this.Controls.Add(this.grpExtractRecordOptions);
            this.Controls.Add(this.grpExtractFileOptions);
            this.Name = "ctlGeneral";
            this.Size = new System.Drawing.Size(557, 611);
            this.Load += new System.EventHandler(this.ctlGeneral_Load);
            this.grpExtractFileOptions.ResumeLayout(false);
            this.grpExtractFileOptions.PerformLayout();
            this.grpExtractRecordOptions.ResumeLayout(false);
            this.grpExtractRecordOptions.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpExtractImageOptions.ResumeLayout(false);
            this.grpExtractImageOptions.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblImageFileNameSingle;
        private System.Windows.Forms.Label lblImageFileNamePerTran;
        private System.Windows.Forms.Label lblImageFileNamePerBatch;
        private System.Windows.Forms.Button btnImageFileNames;
        private System.Windows.Forms.GroupBox grpExtractFileOptions;
        private System.Windows.Forms.ComboBox cboTimeStamp;
        private System.Windows.Forms.TextBox txtImageFilePath;
        private System.Windows.Forms.Button btnAddField;
        private System.Windows.Forms.CheckBox chkUsePostProcessingDLL;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtLogFilePath;
        private System.Windows.Forms.TextBox txtPostProcessingDLLFileName;
        private System.Windows.Forms.TextBox txtExtractFilePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpExtractRecordOptions;
        private System.Windows.Forms.ComboBox cboRecordDelimiter;
        private System.Windows.Forms.ComboBox cboFieldDelimiter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkShowNULLAsSpace;
        private System.Windows.Forms.CheckBox chkEncloseFieldDataInQuotes;
        private System.Windows.Forms.CheckBox chkOmitPadding;
        private System.Windows.Forms.Button btnSelectPostProcDLLFileName;
        private System.Windows.Forms.Button btnSelectImageFilePath;
        private System.Windows.Forms.Button btnSelectLogFilePath;
        private System.Windows.Forms.Button btnSelectExtractFileLocation;
        private System.Windows.Forms.Button btnTestExtract;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkIncludeImageFolderPath;
        private System.Windows.Forms.RadioButton rdoImageFileFormatPdf;
        //private System.Windows.Forms.CheckBox chkCombineImagesAcrossProcessingDates;
        private System.Windows.Forms.RadioButton rdoImageFileFormatSendToPrinter;
        private System.Windows.Forms.Button btnSelectPrinter;
        private System.Windows.Forms.RadioButton rdoImageFileFormatNone;
        private System.Windows.Forms.RadioButton rdoImageFileFormatSingleMultiTIFF;
        private System.Windows.Forms.TextBox txtPrinterName;
        //private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.GroupBox grpExtractImageOptions;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSelectExtractFilesZipPath;
        private System.Windows.Forms.TextBox txtZipFilePath;
        private System.Windows.Forms.CheckBox chkZipOutputFiles;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbIncludeFooter;
    }
}
