using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
* WI 148178 BLR 06/17/2014
*   - Removed 'Batch Type' from the list of tokens. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal partial class frmFileNameFormatDialog : Form {

        private ImageFileFormatEnum _ImageFileFormat;

        public frmFileNameFormatDialog(
            string imageFileNameSingle,
            string imageFileNamePerTran,
            string imageFileNamePerBatch,
            bool imageFileBatchTypeFormatFull,
            bool imageFileZeroPad,
            string imageFileProcDateFormat,
            ImageFileFormatEnum imageFileFormat) {

            Dictionary<string, string> arTokens;

            InitializeComponent();

            _ImageFileFormat = imageFileFormat;

            arTokens = new Dictionary<string, string>();
            arTokens.Add(Support.LBL_BANK, "<BK>");
            arTokens.Add(Support.LBL_CLIENT_ACCOUNT, "<LB>");
            arTokens.Add(Support.LBL_BATCH, "<BH>");
            arTokens.Add(Support.LBL_PROCESSING_DATE, "<PD>");
            this.ctlFileNameFormat3.InitializeControl(imageFileNamePerBatch,
                                                      cExtractDef.DEFAULT_IMAGES_PER_BATCH_FILE_NAME,
                                                      arTokens);

            arTokens.Add(Support.LBL_TRANSACTION, "<TR>");
            this.ctlFileNameFormat2.InitializeControl(imageFileNamePerTran,
                                                      cExtractDef.DEFAULT_IMAGES_PER_TXN_FILE_NAME,
                                                      arTokens);

            arTokens.Add(Support.LBL_DOCUMENT_TYPE, "<DT>");
            arTokens.Add(Support.LBL_FILE_COUNTER, "<FC>");
            this.ctlFileNameFormat1.InitializeControl(imageFileNameSingle,
                                                      cExtractDef.DEFAULT_SINGLE_IMAGES_FILE_NAME,
                                                      arTokens);
          
            if(imageFileBatchTypeFormatFull) {
                this.rdoDisplayFullNumber.Checked = true;
            } else {
                this.rdoDisplayFirstDigitOnly.Checked = true;
            }
            
            this.chkZeroPadNumbers.Checked = imageFileZeroPad;
            this.txtProcessingDateFormat.Text = imageFileProcDateFormat;
        }

        public string ImageFileNameSingle {
            get {
                return(ctlFileNameFormat1.ImageFileName);
            }
        }
        
        public string ImageFileNamePerTran {
            get {
                return(ctlFileNameFormat2.ImageFileName);
            }
        }

        public string ImageFileNamePerBatch {
            get {
                return(ctlFileNameFormat3.ImageFileName);
            }
        }
        
        public bool ImageFileBatchTypeFormatFull {
            get {
                return(this.rdoDisplayFullNumber.Checked);
            }
        }

        public bool ImageFileZeroPad {
            get {
                return(this.chkZeroPadNumbers.Checked);
            }
        }

        public string ImageFileProcDateFormat {
            get {
                return(this.txtProcessingDateFormat.Text);
            }
        }

        private void btnOk_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtProcessingDateFormat_TextChanged(object sender, EventArgs e) {
            RefreshFileNameFormat();
        }

        private void ctlFileNameFormat1_FileNameChanged(string imageFileName) {
            if(this.tabMain.SelectedIndex == 0) {
                RefreshFileNameFormat();
            }
        }

        private void ctlFileNameFormat2_FileNameChanged(string imageFileName) {
            if(this.tabMain.SelectedIndex == 1) {
                RefreshFileNameFormat();
            }
        }

        private void ctlFileNameFormat3_FileNameChanged(string imageFileName) {
            if(this.tabMain.SelectedIndex == 2) {
                RefreshFileNameFormat();
            }
        }

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e) {
            RefreshFileNameFormat();
        }

        private void rdoDisplayFirstDigitOnly_CheckedChanged(object sender, EventArgs e) {
            RefreshFileNameFormat();
        }

        private void rdoDisplayFullNumber_CheckedChanged(object sender, EventArgs e) {
            RefreshFileNameFormat();
        }

        private void chkZeroPadNumbers_CheckedChanged(object sender, EventArgs e) {
            RefreshFileNameFormat();
        }

        private void RefreshFileNameFormat() {
            switch(this.tabMain.SelectedIndex) {
                case 0:
                    this.txtFileName.Text = cExtractDef.DecodeFileNameFormat(
                        ctlFileNameFormat1.ImageFileName, 
                        this.ImageFileBatchTypeFormatFull, 
                        this.ImageFileProcDateFormat, 
                        this.ImageFileZeroPad, 
                        _ImageFileFormat);
                    break;
                case 1:
                    this.txtFileName.Text = cExtractDef.DecodeFileNameFormat(
                        ctlFileNameFormat2.ImageFileName, 
                        this.ImageFileBatchTypeFormatFull, 
                        this.ImageFileProcDateFormat, 
                        this.ImageFileZeroPad, 
                        _ImageFileFormat);
                    break;
                case 2:
                    this.txtFileName.Text = cExtractDef.DecodeFileNameFormat(
                        ctlFileNameFormat3.ImageFileName, 
                        this.ImageFileBatchTypeFormatFull, 
                        this.ImageFileProcDateFormat, 
                        this.ImageFileZeroPad, 
                        _ImageFileFormat);
                    break;
                default:
                    this.txtFileName.Text = string.Empty;
                    break;
            }
        }

        private void frmFileNameFormatDialog_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}