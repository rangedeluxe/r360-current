using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Linq;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal partial class ctlLimitItem : UserControl {

        private List<cColumn>[] _Columns = null;
        private bool _IsDirty = false;
        private int _Index;

        public ctlLimitItem(
            int index, 
            cLimitItem limitItem,
            List<cColumn>[] columns) {
            _Columns = columns;

            InitializeComponent();

            LoadLimitItem(index, limitItem);
        }

        private SqlDbType FindDataType(cLimitItem item)
        {

            SqlDbType sqlRetVal = SqlDbType.Text;

            // For data entry, we already have the type.
            if (item.Table.ToLower().Contains("dataentry"))
            {
                return item.DataType == 6 ? SqlDbType.Float
                    : item.DataType == 7 ? SqlDbType.Money
                    : item.DataType == 11 ? SqlDbType.DateTime
                    : SqlDbType.Text;
            }

            for (int i = 0; i < _Columns.Length; ++i)
            {
                foreach (cColumn column in _Columns[i])
                {
                    if ($"{column.TableName}.{column.ColumnName}" == $"{item.Table}.${item.FieldName}")
                    {
                        sqlRetVal = column.Type;
                        break;
                    }
                }
            }

            return (sqlRetVal);
        }

        public int Index {
            get {
                return(_Index);
            }
        }

        public LogicalOperatorEnum LogicalOperator {
            get {
                var selectedstr = (string)cboLogicalOperator.SelectedItem;
                var alltypes = Enum.GetValues(typeof(LogicalOperatorEnum));
                var match = alltypes
                    .Cast<LogicalOperatorEnum>()
                    .First(t => cExtractDef.DecodeLogicalOperator(t) == selectedstr);

                return match;
            }
        }
        
        public string Value {
            get {
                return(this.txtValue.Text);
            }
        }
        
        public int LeftParenCount {
            get {
                int intTemp;
            
                if(int.TryParse(this.txtLeftParenCount.Text, out intTemp)) {
                    return(intTemp);
                } else {
                    return(0);
                }
            }
        }
        
        public int RightParenCount {
            get {
                int intTemp;
            
                if(int.TryParse(this.txtRightParenCount.Text, out intTemp)) {
                    return(intTemp);
                } else {
                    return(0);
                }
            }
        }
        
        public CompoundOperatorEnum CompoundOperator {
            get {
                switch(this.cboCompoundOperator.SelectedIndex) {
                    case 1:
                        return(CompoundOperatorEnum.And);
                    case 2:
                        return(CompoundOperatorEnum.AndNot);
                    case 3:
                        return(CompoundOperatorEnum.Or);
                    case 4:
                        return(CompoundOperatorEnum.OrNot);
                    default: //case 0:
                        return(CompoundOperatorEnum.None);
                }
            }
        }

        public void LoadLimitItem(
            int index, 
            cLimitItem limitItem) {

            cboLogicalOperator.Items.Clear();
            cboCompoundOperator.Items.Clear();

            var type = (int)FindDataType(limitItem);

            var sqlDataType = (SqlDbType)type;

            this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Equals));
            this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.NotEqualTo));

            if (sqlDataType == SqlDbType.BigInt ||
               sqlDataType == SqlDbType.Bit ||
               sqlDataType == SqlDbType.Decimal ||
               sqlDataType == SqlDbType.Float ||
               sqlDataType == SqlDbType.Int ||
               sqlDataType == SqlDbType.Money ||
               sqlDataType == SqlDbType.Real ||
               sqlDataType == SqlDbType.SmallInt ||
               sqlDataType == SqlDbType.SmallMoney ||
               sqlDataType == SqlDbType.TinyInt ||
               sqlDataType == SqlDbType.DateTime ||
               sqlDataType == SqlDbType.Timestamp)
            {

                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.GreaterThan));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.GreaterThanOrEqualTo));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.LessThan));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.LessThanOrEqualTo));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Between));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.In));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNull));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNotNull));

            }
            else
            {
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.In));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNull));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.IsNotNull));
                this.cboLogicalOperator.Items.Add(cExtractDef.DecodeLogicalOperator(LogicalOperatorEnum.Like));
            }

            this.cboCompoundOperator.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.None));
            this.cboCompoundOperator.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.And));
            this.cboCompoundOperator.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.AndNot));
            this.cboCompoundOperator.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.Or));
            this.cboCompoundOperator.Items.Add(cExtractDef.DecodeCompoundOperator(CompoundOperatorEnum.OrNot));


            _Index = index;

            this.lblFieldName.Text = limitItem.DisplayName;

            this.cboLogicalOperator.SelectedIndex = 
                cboLogicalOperator.Items.IndexOf(cExtractDef.DecodeLogicalOperator(limitItem.LogicalOperator));

            this.txtValue.Text = limitItem.Value;
            this.txtLeftParenCount.Text = limitItem.LeftParenCount.ToString();
            this.txtRightParenCount.Text = limitItem.RightParenCount.ToString();

            switch(limitItem.CompoundOperator) {
                case CompoundOperatorEnum.And:
                    this.cboCompoundOperator.SelectedIndex = 1;
                    break;
                case CompoundOperatorEnum.AndNot:
                    this.cboCompoundOperator.SelectedIndex = 2;
                    break;
                case CompoundOperatorEnum.Or:
                    this.cboCompoundOperator.SelectedIndex = 3;
                    break;
                case CompoundOperatorEnum.OrNot:
                    this.cboCompoundOperator.SelectedIndex = 4;
                    break;
                default:
                    this.cboCompoundOperator.SelectedIndex = 0;
                    break;
            }                    
        }
        
        public bool IsDirty {
            get {
                return(_IsDirty);
            }
        }

        private void cboLogicalOperator_SelectedIndexChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void cboCompoundOperator_SelectedIndexChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtValue_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtLeftParenCount_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtRightParenCount_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void ctlLimitItem_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorter().SortTabs(this);
        }
    }
}

