using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Text;
using System.Windows.Forms;

using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.LTA.Common;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   - Initial Version 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 130642 DLD 03/07/2014
*   - Validating Extract Def file paths. 
* WI 132205 BLR 03/17/2014
*   - Added tab-order sorting.
* WI 152321 BLR 08/15/2014
*   - Moved configuration settings over to ExtractConfigurationSettings.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    internal delegate void ExtractDescriptorAddedEventHandler(out List<cExtractDescriptor> ExtractDescriptor);

    internal partial class ctlGeneral : UserControl {

        private bool _IsDirty = false;

        // Values from Image Format dialog form.
        private bool _ImageFileZeroPad;
        private string _ImageFileProcDateFormat;
        private bool _ImageFileBatchTypeFormatFull = false;

        private string _ImageFileNameSingle = string.Empty;
        private string _ImageFileNamePerTran = string.Empty;
        private string _ImageFileNamePerBatch = string.Empty;

        private string _SetupPath = string.Empty;
        private cExtractWizardBLL _ExtractWizardBLL = null;

        public event outputErrorEventHandler OutputError;
        public event outputMessageEventHandler OutputMessage;
        public event ExtractDescriptorAddedEventHandler ExtractDescriptorAdded;
        
        public bool IsDirty {
            get {
                return(_IsDirty);
            }
        }

        private ExtractConfigurationSettings _Settings; 

        private cExtractWizardBLL ExtractWizardBLL {
            get {
                if(_ExtractWizardBLL == null) {
                    _ExtractWizardBLL = new cExtractWizardBLL(_Settings);
                    _ExtractWizardBLL.OutputError += OnOutputError;
                    _ExtractWizardBLL.OutputMessage += OnOutputMessage;
                }
                return (_ExtractWizardBLL);
            }
        }

        private void OnOutputError(System.Exception e) {
            if(OutputError != null) {
                OutputError(e);
            }
        }

        private void OnOutputError(object sender, System.Exception e) {
            if(OutputError != null) {
                OutputError(e);
            }
        }

        private void OnOutputMessage(
            string Message, 
            string Src, 
            MessageType MessageType, 
            MessageImportance MessageImportance) {

            if(OutputMessage != null) {
                OutputMessage(Message, Src, MessageType, MessageImportance);
            }
        }

        private void OnExtractDescriptorAdded(out List<cExtractDescriptor> extractDescriptors) {

            if(ExtractDescriptorAdded != null) {
                ExtractDescriptorAdded(out extractDescriptors);
            } else {
                extractDescriptors = null;
            }           
        }

        public ctlGeneral(ExtractConfigurationSettings settings, cExtractDef ExtractDef, string SetupPath, List<cExtractDescriptor> extractDescriptors) {

            InitializeComponent();

            _Settings = settings;
            
            _SetupPath = SetupPath;

            LoadExtractDef(ExtractDef, extractDescriptors);
        }

        public string ExtractFilePath {
            get {
                return(this.txtExtractFilePath.Text);
            }
        }
                            
        public string LogFilePath {
            get {
                return(this.txtLogFilePath.Text);
            }
        }
                            
        public string ImageFilePath {
            get {
                return(this.txtImageFilePath.Text);
            }
        }
                            
        public bool UsePostProcessingDLL {
            get {
                return(this.chkUsePostProcessingDLL.Checked);
            }
        }
                            
        public string PostProcessingDLLFileName {
            get {
                return(this.txtPostProcessingDLLFileName.Text);
            }
        }
                            
        public string TimeStamp {
            get {
                return(this.cboTimeStamp.Text);
            }
        }

        public ImageFileFormatEnum ImageFileFormat {
            get {
                if(this.rdoImageFileFormatSingleMultiTIFF.Checked) {
                    return(ImageFileFormatEnum.Tiff);
                } else if(this.rdoImageFileFormatPdf.Checked) {
                    return(ImageFileFormatEnum.Pdf);
                } else if(this.rdoImageFileFormatSendToPrinter.Checked) {
                    return(ImageFileFormatEnum.SendToPrinter);
                } else {
                    return(ImageFileFormatEnum.DoNotRetrieveImages);
                }
            }
        }

        //public bool CombineImagesAccrossProcDates {
        //    get {
        //        return(this.chkCombineImagesAcrossProcessingDates.Checked);
        //    }
        //}

        public string ImageFileNameSingle {
            get {
                return(_ImageFileNameSingle);
            }
        }
                            
        public string ImageFileNamePerTran {
            get {
                return(_ImageFileNamePerTran);
            }
        }
                            
        public string ImageFileNamePerBatch {
            get {
                return(_ImageFileNamePerBatch);
            }
        }
                            
        public string RecordDelimiter {
            get {
                return(this.cboRecordDelimiter.Text);
            }
        }

        public string FieldDelimiter {
            get {
                return(this.cboFieldDelimiter.Text);
            }
        }

        public bool NoPad {
            get {
                return(this.chkOmitPadding.Checked);
            }
        }

        public bool UseQuotes {
            get {
                return(this.chkEncloseFieldDataInQuotes.Checked);
            }
        }

        public bool NullAsSpace {
            get {
                return(this.chkShowNULLAsSpace.Checked);
            }
        }
        
        public bool ImageFileZeroPad {
            get {
                return(_ImageFileZeroPad);
            }
        }
        
        public string ImageFileProcDateFormat {
            get {
                return(_ImageFileProcDateFormat);
            }
        }
        
        public bool ImageFileBatchTypeFormatFull {
            get {
                return(_ImageFileBatchTypeFormatFull);
            }
        }

        public bool IncludeImageFolderPath {
            get {
                return(this.chkIncludeImageFolderPath.Checked);
            }
        }

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        public bool IncludeFooter
        {
            get { return (this.cbIncludeFooter.Checked); }
        }
       
        public string PrinterName {
            get {
                return(this.txtPrinterName.Text);
            }
        }

        public bool ZipOutputFiles {
            get {
                return(this.chkZipOutputFiles.Checked);
            }
        }

        public string ZipFilePath {
            get {
                if(this.ZipOutputFiles) {
                    return(this.txtZipFilePath.Text);
                } else {
                    return(string.Empty);
                }
            }
        }
       
        public bool LoadExtractDef(
            cExtractDef extractDef,
            List<cExtractDescriptor> extractDescriptors) {

            int intPos;
            
            try {

                this.txtExtractFilePath.Text = extractDef.ExtractPath;
                this.txtLogFilePath.Text = extractDef.LogFilePath;
                this.txtImageFilePath.Text = extractDef.ImagePath;

                this.chkUsePostProcessingDLL.Checked = extractDef.PostProcDLLFileName.Length > 0;

                this.txtPostProcessingDLLFileName.Text = extractDef.PostProcDLLFileName;
                
                this.txtPostProcessingDLLFileName.Enabled = this.chkUsePostProcessingDLL.Checked;
                this.btnSelectPostProcDLLFileName.Enabled = this.chkUsePostProcessingDLL.Checked;

                this.cboTimeStamp.SelectedValue = extractDef.TimeStamp;

                // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                this.cbIncludeFooter.Checked = extractDef.IncludeFooter;

                switch(extractDef.ImageFileFormat) {
                    case ImageFileFormatEnum.Tiff:
                        this.rdoImageFileFormatSingleMultiTIFF.Checked = true;
                        break;
                    case ImageFileFormatEnum.SendToPrinter:
                        this.rdoImageFileFormatSendToPrinter.Checked = true;
                        break;
                    case ImageFileFormatEnum.Pdf:
                        this.rdoImageFileFormatPdf.Checked = true;
                        break;
                    default:
                        this.rdoImageFileFormatNone.Checked = true;
                        break;
                }

                this.btnSelectPrinter.Enabled = this.rdoImageFileFormatSendToPrinter.Checked;

                //this.chkCombineImagesAcrossProcessingDates.Checked = ExtractDef.CombineImagesAccrossProcDates;

                _ImageFileNameSingle = extractDef.ImageFileNameSingle;
                _ImageFileNamePerTran = extractDef.ImageFileNamePerTran;
                _ImageFileNamePerBatch = extractDef.ImageFileNamePerBatch;

                intPos = this.cboFieldDelimiter.FindStringExact(extractDef.FieldDelim);
                if(intPos > -1) {
                    this.cboFieldDelimiter.SelectedIndex = intPos;
                } else {
                    this.cboFieldDelimiter.Text = extractDef.FieldDelim;
                }

                intPos = this.cboRecordDelimiter.FindStringExact(extractDef.RecordDelim);
                if(intPos > -1) {
                    this.cboRecordDelimiter.SelectedIndex = intPos;
                } else {
                    this.cboRecordDelimiter.Text = extractDef.RecordDelim;
                }

                this.chkOmitPadding.Checked = extractDef.NoPad;
                this.chkEncloseFieldDataInQuotes.Checked = extractDef.UseQuotes;
                this.chkShowNULLAsSpace.Checked = extractDef.NullAsSpace;

                _ImageFileZeroPad = extractDef.ImageFileZeroPad;
                _ImageFileProcDateFormat = extractDef.ImageFileProcDateFormat;
                _ImageFileBatchTypeFormatFull = extractDef.ImageFileBatchTypeFormatFull;

                this.chkIncludeImageFolderPath.Checked = extractDef.IncludeImageFolderPath;

                this.txtPrinterName.Text = extractDef.PrinterName;
                
                SetImageLabelText();

                LoadTimeStampCombo(extractDescriptors, extractDef.TimeStamp);

                if(extractDef.ZipOutputFiles) {
                    this.chkZipOutputFiles.Checked = true;
                    this.txtZipFilePath.Text = extractDef.ZipFilePath;
                } else {
                    this.chkZipOutputFiles.Checked = false;
                    this.txtZipFilePath.Text = string.Empty;
                }

                _IsDirty = false;


            } catch(Exception e) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);;
                throw;
            }

            return(true);
        }
        
        private void LoadTimeStampCombo(
            List<cExtractDescriptor> extractDescriptors, 
            string selectedValue) {

            int intPos;

            this.cboTimeStamp.Items.Clear();
            this.cboTimeStamp.Items.Add(string.Empty);

            if(extractDescriptors != null) {
                foreach(cExtractDescriptor extractDescriptor in extractDescriptors) {
                    this.cboTimeStamp.Items.Add(extractDescriptor.ExtractDescriptor);
                }
            }

            intPos = this.cboTimeStamp.FindString(selectedValue);
            if(intPos > -1) {
                this.cboTimeStamp.SelectedIndex = intPos;
            }
        }

        private void btnImageFileNames_Click(object sender, EventArgs e) {

            frmFileNameFormatDialog objDialog = new frmFileNameFormatDialog(
                ((cExtractDef)(((frmMain)this.ParentForm).tvLayouts.Nodes["General"].Tag)).ImageFileNameSingle,
                ((cExtractDef)(((frmMain)this.ParentForm).tvLayouts.Nodes["General"].Tag)).ImageFileNamePerTran,
                ((cExtractDef)(((frmMain)this.ParentForm).tvLayouts.Nodes["General"].Tag)).ImageFileNamePerBatch,
                _ImageFileBatchTypeFormatFull,
                _ImageFileZeroPad,
                _ImageFileProcDateFormat,
                this.ImageFileFormat);

            if(objDialog.ShowDialog(this) == DialogResult.OK) {

                ((cExtractDef)(((frmMain)this.ParentForm).tvLayouts.Nodes["General"].Tag)).ImageFileNameSingle = objDialog.ImageFileNameSingle;
                ((cExtractDef)(((frmMain)this.ParentForm).tvLayouts.Nodes["General"].Tag)).ImageFileNamePerTran = objDialog.ImageFileNamePerTran;
                ((cExtractDef)(((frmMain)this.ParentForm).tvLayouts.Nodes["General"].Tag)).ImageFileNamePerBatch = objDialog.ImageFileNamePerBatch;
                ((cExtractDef)(((frmMain)this.ParentForm).tvLayouts.Nodes["General"].Tag)).ImageFileBatchTypeFormatFull = objDialog.ImageFileBatchTypeFormatFull;

                _ImageFileNameSingle = objDialog.ImageFileNameSingle;
                _ImageFileNamePerTran = objDialog.ImageFileNamePerTran;
                _ImageFileNamePerBatch = objDialog.ImageFileNamePerBatch;

                _ImageFileZeroPad = objDialog.ImageFileZeroPad;
                _ImageFileProcDateFormat = objDialog.ImageFileProcDateFormat;
                _ImageFileBatchTypeFormatFull = objDialog.ImageFileBatchTypeFormatFull;

                SetImageLabelText();

                _IsDirty = true;
            }

            objDialog.Dispose();
        }

        private void SetImageLabelText() {

            this.lblImageFileNameSingle.Text = cExtractDef.DecodeFileNameFormat(
                _ImageFileNameSingle, 
                _ImageFileBatchTypeFormatFull, 
                _ImageFileProcDateFormat, 
                _ImageFileZeroPad, 
                this.ImageFileFormat);

            this.lblImageFileNamePerTran.Text = cExtractDef.DecodeFileNameFormat(
                _ImageFileNamePerTran, 
                _ImageFileBatchTypeFormatFull, 
                _ImageFileProcDateFormat, 
                _ImageFileZeroPad, 
                this.ImageFileFormat);

            this.lblImageFileNamePerBatch.Text = cExtractDef.DecodeFileNameFormat(
                _ImageFileNamePerBatch, 
                _ImageFileBatchTypeFormatFull, 
                _ImageFileProcDateFormat, 
                _ImageFileZeroPad, 
                this.ImageFileFormat);

            if(this.ImageFileFormat == ImageFileFormatEnum.Pdf ||
               this.ImageFileFormat == ImageFileFormatEnum.Tiff ||
               this.ImageFileFormat == ImageFileFormatEnum.SendToPrinter) {

                this.lblImageFileNameSingle.ForeColor = Color.Maroon;
                this.lblImageFileNamePerTran.ForeColor = Color.Maroon;
                this.lblImageFileNamePerBatch.ForeColor = Color.Maroon;

                this.chkIncludeImageFolderPath.Enabled = true;
                this.btnImageFileNames.Enabled = true;

            } else {

                this.lblImageFileNameSingle.ForeColor = Color.Black;
                this.lblImageFileNamePerTran.ForeColor = Color.Black;
                this.lblImageFileNamePerBatch.ForeColor = Color.Black;

                this.chkIncludeImageFolderPath.Enabled = false;
                this.btnImageFileNames.Enabled = false;
            }
        }

        private void txtExtractFilePath_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtLogFilePath_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtImageFilePath_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void txtPostProcessingDLLFileName_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void chkUsePostProcessingDLL_CheckedChanged(object sender, EventArgs e) {
            this.txtPostProcessingDLLFileName.Enabled = this.chkUsePostProcessingDLL.Checked;
            this.btnSelectPostProcDLLFileName.Enabled = this.chkUsePostProcessingDLL.Checked;
            _IsDirty = true;
        }

        private void cboTimeStamp_SelectedIndexChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        //private void chkCombineImagesAcrossProcessingDates_CheckedChanged(object sender, EventArgs e) {
        //    _IsDirty = true;
        //}

        private void chkIncludeImageFolderPath_CheckedChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void rdoImageFileFormatNone_CheckedChanged(object sender, EventArgs e) {
            SetImageLabelText();
            _IsDirty = true;
        }

        private void rdoImageFileFormatSingleMultiTIFF_CheckedChanged(object sender, EventArgs e) {
            SetImageLabelText();
            _IsDirty = true;
        }

        private void rdoImageFileFormatPdf_CheckedChanged(object sender, EventArgs e) {
            SetImageLabelText();

            // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
            if (rdoImageFileFormatPdf.Checked)
                cbIncludeFooter.Visible = true;
            else
            {
                cbIncludeFooter.Visible = false;
                cbIncludeFooter.Checked = false;
            }

            _IsDirty = true;
        }

        private void rdoImageFileFormatSendToPrinter_CheckedChanged(object sender, EventArgs e) {
            SetImageLabelText();
            this.btnSelectPrinter.Enabled = this.rdoImageFileFormatSendToPrinter.Checked;
            _IsDirty = true;
        }

        private void cboFieldDelimiter_SelectedIndexChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void cboRecordDelimiter_SelectedIndexChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void chkOmitPadding_CheckedChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void chkEncloseFieldDataInQuotes_CheckedChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void chkShowNULLAsSpace_CheckedChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }
        
        private void SetImageFileFormat() {
            _IsDirty = true;
        }

        private void cboFieldDelimiter_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void cboRecordDelimiter_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void btnSelectExtractFileLocation_Click(object sender, EventArgs e) {

            string strFileName;

            if (IOLib.BrowseFile(ExtractLib.Constants.FILE_FILTER_EXTRACT_DATA_FILES, 0, this.txtExtractFilePath.Text, out strFileName))
            {
                if (CommonLib.CheckDirectory(Path.GetDirectoryName(strFileName)))
                    this.txtExtractFilePath.Text = strFileName;
            }
        }

        private void btnSelectLogFilePath_Click(object sender, EventArgs e) {

            string strFileName;

            if (IOLib.BrowseFile(ExtractLib.Constants.FILE_FILTER_LOG_FILES, 0, this.txtLogFilePath.Text, out strFileName))
            {
                if (CommonLib.CheckDirectory(Path.GetDirectoryName(strFileName)))
                    this.txtLogFilePath.Text = strFileName;
            }
        }

        private void btnSelectImageFilePath_Click(object sender, EventArgs e) {

            string strInitialPath;
            string strFolderPath;

            if(this.txtImageFilePath.Text.Trim().Length == 0) {
                if(this.txtExtractFilePath.Text.Length > 0 && this.txtExtractFilePath.Text.LastIndexOf(@"\") > -1) {
                    strInitialPath = this.txtExtractFilePath.Text.Substring(0, this.txtExtractFilePath.Text.LastIndexOf(@"\"));
                } else {
                    strInitialPath = _SetupPath;
                }
            } else if(this.txtImageFilePath.Text.StartsWith(".")) {
                strInitialPath = Path.Combine(_SetupPath, this.txtImageFilePath.Text);
            } else {
                strInitialPath = this.txtImageFilePath.Text;
            }

            if(IOLib.BrowseFolder(Path.GetFullPath(strInitialPath), out strFolderPath)) 
            {
                if (CommonLib.CheckDirectory(strFolderPath))
                    this.txtImageFilePath.Text = strFolderPath;
            }
        }

        private void btnSelectPostProcDLLFileName_Click(object sender, EventArgs e) {

            string strFileName;

            if (IOLib.BrowseFile(ExtractLib.Constants.FILE_FILTER_DLL, 0, this.txtPostProcessingDLLFileName.Text, true, out strFileName))
            {
                this.txtPostProcessingDLLFileName.Text = strFileName;
            }
        }

        private void btnAddField_Click(object sender, EventArgs e) {

            frmEnterText objEnterText;
            List<cExtractDescriptor> arExtractDescriptors;

            if(MessageBox.Show("WARNING: Fields created cannot be changed or removed except by a Database Administrator.  Do you wish to continue?", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes) {

                objEnterText = new frmEnterText("New Batch Trace Field", "Enter the name of the field to create:", string.Empty);

                objEnterText.ShowDialog(this.ParentForm);

                if(!objEnterText.Cancelled && ExtractWizardBLL.CreateBatchTraceColumn(objEnterText.Value)) {
                    MessageBox.Show("Field Added: " + objEnterText.Value);

                    OnExtractDescriptorAdded(out arExtractDescriptors);

                    LoadTimeStampCombo(arExtractDescriptors, objEnterText.Value);
                }

                objEnterText.Dispose();
            }
        }

        private void btnTestExtract_Click(object sender, EventArgs e) {
            
            cExtract objExtract;
            StringBuilder strTestExtract;
            frmDisplayText objText;

            ((frmMain)this.ParentForm).SynchCurrentNodes();

            objExtract = new cExtract();
            objExtract.OutputError += OnOutputError;
            objExtract.OutputMessage += OnOutputMessage;

            if(objExtract.BuildTestExtract(((frmMain)this.ParentForm).GetExtractDefFromForm(), out strTestExtract)) {

                objText = new frmDisplayText("Test Extract Results", strTestExtract.ToString(), ((frmMain)this.ParentForm).printDialog1.PrinterSettings, this.ParentForm.Icon);
                objText.Show(this);
            }
        }

        private void btnSelectPrinter_Click(object sender, EventArgs e) {
            PrintDialog objDialog = new PrintDialog();
            objDialog.PrinterSettings.PrinterName = this.txtPrinterName.Text;
            if(objDialog.ShowDialog() == DialogResult.OK) {
                this.txtPrinterName.Text = objDialog.PrinterSettings.PrinterName;
                _IsDirty = true;
            }
            objDialog.Dispose();
        }

        private void btnSelectExtractFilesZipPath_Click(object sender, EventArgs e) {
            string strFileName;

            if (IOLib.BrowseFile(ExtractLib.Constants.FILE_FILTER_ZIP_FILES, 0, this.txtZipFilePath.Text, out strFileName))
            {
                if (CommonLib.CheckDirectory(Path.GetDirectoryName(strFileName)))
                    this.txtZipFilePath.Text = strFileName;
            }
        }

        private void chkZipOutputFiles_CheckedChanged(object sender, EventArgs e) {
            this.txtZipFilePath.Enabled = ((CheckBox)sender).Checked;
            this.btnSelectExtractFilesZipPath.Enabled = ((CheckBox)sender).Checked;
            _IsDirty = true;
        }

        private void txtZipFilePath_TextChanged(object sender, EventArgs e) {
            _IsDirty = true;
        }

        private void cbIncludeFooter_CheckedChanged(object sender, EventArgs e)
        {
            _IsDirty = true;
        }

        private void ctlGeneral_Load(object sender, EventArgs e)
        {
            // WI 130551 : Sorting the tab-order in a logical manner.
            new TabSorting.TabSorter().SortTabs(this);
        }
    }
}
