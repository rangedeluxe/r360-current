/*******************************************************************************
*
*    Module: ctlDataGrid
*  Filename: ctlDataGrid.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class ctlDataGrid {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ctlDataGrid));
            this.btnRowMoveDown = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnRowMoveUp = new System.Windows.Forms.Button();
            this.btnRowDelete = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRowMoveDown
            // 
            this.btnRowMoveDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRowMoveDown.ImageIndex = 1;
            this.btnRowMoveDown.ImageList = this.imageList1;
            this.btnRowMoveDown.Location = new System.Drawing.Point(84, 316);
            this.btnRowMoveDown.Name = "btnRowMoveDown";
            this.btnRowMoveDown.Size = new System.Drawing.Size(75, 23);
            this.btnRowMoveDown.TabIndex = 6;
            this.btnRowMoveDown.UseVisualStyleBackColor = true;
            this.btnRowMoveDown.Click += new System.EventHandler(this.btnRowMoveDown_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "arrow-up-big.png");
            this.imageList1.Images.SetKeyName(1, "arrow-down-big.png");
            this.imageList1.Images.SetKeyName(2, "cross.png");
            // 
            // btnRowMoveUp
            // 
            this.btnRowMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRowMoveUp.BackColor = System.Drawing.Color.Transparent;
            this.btnRowMoveUp.ImageIndex = 0;
            this.btnRowMoveUp.ImageList = this.imageList1;
            this.btnRowMoveUp.Location = new System.Drawing.Point(3, 316);
            this.btnRowMoveUp.Name = "btnRowMoveUp";
            this.btnRowMoveUp.Size = new System.Drawing.Size(75, 23);
            this.btnRowMoveUp.TabIndex = 5;
            this.btnRowMoveUp.UseVisualStyleBackColor = false;
            this.btnRowMoveUp.Click += new System.EventHandler(this.btnRowMoveUp_Click);
            // 
            // btnRowDelete
            // 
            this.btnRowDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRowDelete.ImageIndex = 2;
            this.btnRowDelete.ImageList = this.imageList1;
            this.btnRowDelete.Location = new System.Drawing.Point(165, 316);
            this.btnRowDelete.Name = "btnRowDelete";
            this.btnRowDelete.Size = new System.Drawing.Size(75, 23);
            this.btnRowDelete.TabIndex = 7;
            this.btnRowDelete.UseVisualStyleBackColor = true;
            this.btnRowDelete.Click += new System.EventHandler(this.btnRowDelete_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(740, 310);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView1_CellValidating);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            this.dataGridView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragDrop);
            this.dataGridView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragEnter);
            this.dataGridView1.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragOver);
            this.dataGridView1.Leave += new System.EventHandler(this.dataGridView1_Leave);
            // 
            // ctlDataGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRowMoveDown);
            this.Controls.Add(this.btnRowMoveUp);
            this.Controls.Add(this.btnRowDelete);
            this.Controls.Add(this.dataGridView1);
            this.Name = "ctlDataGrid";
            this.Size = new System.Drawing.Size(740, 342);
            this.Load += new System.EventHandler(this.ctlDataGrid_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRowMoveDown;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button btnRowMoveUp;
        private System.Windows.Forms.Button btnRowDelete;
        protected System.Windows.Forms.DataGridView dataGridView1;
    }
}
