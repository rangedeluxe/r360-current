﻿/*******************************************************************************
*
*    Module: ctlPostProcCode.Designer
*  Filename: ctlPostProcCode.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 27121 JMC 05/20/2009
*   -Modified Maximum Length of the Code Module text box to match the allowable
*    .ini buffer (65536).
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager
{
    partial class ctlPostProcCode
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnResetCode = new System.Windows.Forms.Button();
            this.btnTestCode = new System.Windows.Forms.Button();
            this.editor = new RTFCodeEditor.CodeEditor();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.btnResetCode);
            this.panel1.Controls.Add(this.btnTestCode);
            this.panel1.Controls.Add(this.editor);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(628, 492);
            this.panel1.TabIndex = 1;
            // 
            // btnResetCode
            // 
            this.btnResetCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnResetCode.Location = new System.Drawing.Point(3, 454);
            this.btnResetCode.Name = "btnResetCode";
            this.btnResetCode.Size = new System.Drawing.Size(111, 35);
            this.btnResetCode.TabIndex = 2;
            this.btnResetCode.Text = "Reset";
            this.btnResetCode.UseVisualStyleBackColor = true;
            this.btnResetCode.Click += new System.EventHandler(this.btnResetCode_Click);
            // 
            // btnTestCode
            // 
            this.btnTestCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestCode.Location = new System.Drawing.Point(514, 454);
            this.btnTestCode.Name = "btnTestCode";
            this.btnTestCode.Size = new System.Drawing.Size(111, 35);
            this.btnTestCode.TabIndex = 1;
            this.btnTestCode.Text = "Test Code";
            this.btnTestCode.UseVisualStyleBackColor = true;
            this.btnTestCode.Click += new System.EventHandler(this.btnTestCode_Click);
            // 
            // editor
            // 
            this.editor.AcceptsTab = true;
            this.editor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.editor.Location = new System.Drawing.Point(3, 3);
            this.editor.Name = "editor";
            this.editor.Size = new System.Drawing.Size(622, 445);
            this.editor.TabIndex = 0;
            this.editor.Text = "";
            this.editor.TextChanged += new System.EventHandler(this.editor_TextChanged);
            // 
            // ctlPostProcCode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "ctlPostProcCode";
            this.Size = new System.Drawing.Size(628, 492);
            this.Load += new System.EventHandler(this.ctlPostProcCode_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private RTFCodeEditor.CodeEditor editor;
        private System.Windows.Forms.Button btnTestCode;
        private System.Windows.Forms.Button btnResetCode;

    }
}
