﻿namespace IntegraPAY.Delivery.Extract.ExtractWizard
{
    partial class ctlImageFileNameFormat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddStaticField = new System.Windows.Forms.Button();
            this.btnApplyDefaultFileName = new System.Windows.Forms.Button();
            this.colFieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lstImageFields = new System.Windows.Forms.ListBox();
            this.ctlDataGrid1 = new IntegraPAY.Delivery.Extract.ExtractWizard.ctlDataGrid();
            this.btnAddSelectedToken = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnAddStaticField
            // 
            this.btnAddStaticField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddStaticField.Location = new System.Drawing.Point(249, 173);
            this.btnAddStaticField.Name = "btnAddStaticField";
            this.btnAddStaticField.Size = new System.Drawing.Size(131, 23);
            this.btnAddStaticField.TabIndex = 23;
            this.btnAddStaticField.Text = "Add Static Field";
            this.btnAddStaticField.UseVisualStyleBackColor = true;
            this.btnAddStaticField.Click += new System.EventHandler(this.btnAddStaticField_Click);
            // 
            // btnApplyDefaultFileName
            // 
            this.btnApplyDefaultFileName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyDefaultFileName.Location = new System.Drawing.Point(249, 199);
            this.btnApplyDefaultFileName.Name = "btnApplyDefaultFileName";
            this.btnApplyDefaultFileName.Size = new System.Drawing.Size(131, 23);
            this.btnApplyDefaultFileName.TabIndex = 20;
            this.btnApplyDefaultFileName.Text = "Apply Default File Name";
            this.btnApplyDefaultFileName.UseVisualStyleBackColor = true;
            this.btnApplyDefaultFileName.Click += new System.EventHandler(this.btnApplyDefaultFileName_Click);
            // 
            // colFieldName
            // 
            this.colFieldName.HeaderText = "Field Name";
            this.colFieldName.Name = "colFieldName";
            this.colFieldName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colFieldName.Width = 150;
            // 
            // lstImageFields
            // 
            this.lstImageFields.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lstImageFields.ForeColor = System.Drawing.Color.Maroon;
            this.lstImageFields.FormattingEnabled = true;
            this.lstImageFields.Items.AddRange(new object[] {
            "Bank",
            "Lockbox",
            "Batch",
            "Batch Type",
            "Processing Date",
            "Transaction",
            "Document Type",
            "File Counter"});
            this.lstImageFields.Location = new System.Drawing.Point(249, 19);
            this.lstImageFields.Name = "lstImageFields";
            this.lstImageFields.Size = new System.Drawing.Size(131, 108);
            this.lstImageFields.TabIndex = 15;
            this.lstImageFields.DoubleClick += new System.EventHandler(this.lstImageFields_DoubleClick);
            // 
            // ctlDataGrid1
            // 
            this.ctlDataGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ctlDataGrid1.Location = new System.Drawing.Point(0, 0);
            this.ctlDataGrid1.MinimumSize = new System.Drawing.Size(243, 254);
            this.ctlDataGrid1.Name = "ctlDataGrid1";
            this.ctlDataGrid1.Size = new System.Drawing.Size(243, 254);
            this.ctlDataGrid1.TabIndex = 7;
            this.ctlDataGrid1.ItemDeleted += new IntegraPAY.Delivery.Extract.ExtractWizard.ItemDeletedEventHandler(this.ctlDataGrid1_ItemDeleted);
            this.ctlDataGrid1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.ctlDataGrid1_CellEndEdit);
            this.ctlDataGrid1.ItemMoved += new IntegraPAY.Delivery.Extract.ExtractWizard.ItemMovedEventHandler(this.ctlDataGrid1_ItemMoved);
            // 
            // btnAddSelectedToken
            // 
            this.btnAddSelectedToken.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddSelectedToken.Location = new System.Drawing.Point(249, 133);
            this.btnAddSelectedToken.Name = "btnAddSelectedToken";
            this.btnAddSelectedToken.Size = new System.Drawing.Size(131, 23);
            this.btnAddSelectedToken.TabIndex = 24;
            this.btnAddSelectedToken.Text = "Add SelectedToken";
            this.btnAddSelectedToken.UseVisualStyleBackColor = true;
            this.btnAddSelectedToken.Click += new System.EventHandler(this.btnAddSelectedToken_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(252, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Available Tokens:";
            // 
            // ctlImageFileNameFormat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAddSelectedToken);
            this.Controls.Add(this.ctlDataGrid1);
            this.Controls.Add(this.btnAddStaticField);
            this.Controls.Add(this.btnApplyDefaultFileName);
            this.Controls.Add(this.lstImageFields);
            this.MinimumSize = new System.Drawing.Size(383, 254);
            this.Name = "ctlImageFileNameFormat";
            this.Size = new System.Drawing.Size(383, 254);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddStaticField;
        private System.Windows.Forms.Button btnApplyDefaultFileName;
        private System.Windows.Forms.ListBox lstImageFields;
        private ctlDataGrid ctlDataGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFieldName;
        private System.Windows.Forms.Button btnAddSelectedToken;
        private System.Windows.Forms.Label label1;
    }
}
