/*******************************************************************************
*
*    Module: ctlLayoutField
*  Filename: ctlLayoutField.Designer.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 23609 JMC 02/08/2008
*   -Initial release version.
*******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager {

    partial class ctLayoutField {

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {

            if (disposing && (components != null)) {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDisplayName = new System.Windows.Forms.TextBox();
            this.txtColumnWidth = new System.Windows.Forms.TextBox();
            this.txtPadChar = new System.Windows.Forms.TextBox();
            this.txtFormat = new System.Windows.Forms.TextBox();
            this.chkQuotes = new System.Windows.Forms.CheckBox();
            this.cboJustification = new System.Windows.Forms.ComboBox();
            this.lblFieldName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Field Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Display Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Column Width:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pad Char:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(48, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Justify (L/R):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Quotes:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(48, 198);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Format:";
            // 
            // txtDisplayName
            // 
            this.txtDisplayName.Location = new System.Drawing.Point(140, 75);
            this.txtDisplayName.MaxLength = 100;
            this.txtDisplayName.Name = "txtDisplayName";
            this.txtDisplayName.Size = new System.Drawing.Size(201, 20);
            this.txtDisplayName.TabIndex = 0;
            this.txtDisplayName.TextChanged += new System.EventHandler(this.txtDisplayName_TextChanged);
            // 
            // txtColumnWidth
            // 
            this.txtColumnWidth.Location = new System.Drawing.Point(140, 99);
            this.txtColumnWidth.MaxLength = 4;
            this.txtColumnWidth.Name = "txtColumnWidth";
            this.txtColumnWidth.Size = new System.Drawing.Size(43, 20);
            this.txtColumnWidth.TabIndex = 1;
            this.txtColumnWidth.TextChanged += new System.EventHandler(this.txtColumnWidth_TextChanged);
            this.txtColumnWidth.Validating += new System.ComponentModel.CancelEventHandler(this.txtColumnWidth_Validating);
            // 
            // txtPadChar
            // 
            this.txtPadChar.Location = new System.Drawing.Point(140, 123);
            this.txtPadChar.MaxLength = 2;
            this.txtPadChar.Name = "txtPadChar";
            this.txtPadChar.Size = new System.Drawing.Size(31, 20);
            this.txtPadChar.TabIndex = 2;
            this.txtPadChar.TextChanged += new System.EventHandler(this.txtPadChar_TextChanged);
            this.txtPadChar.Validating += new System.ComponentModel.CancelEventHandler(this.txtPadChar_Validating);
            // 
            // txtFormat
            // 
            this.txtFormat.Location = new System.Drawing.Point(140, 195);
            this.txtFormat.MaxLength = 200;
            this.txtFormat.Name = "txtFormat";
            this.txtFormat.Size = new System.Drawing.Size(201, 20);
            this.txtFormat.TabIndex = 6;
            this.txtFormat.TextChanged += new System.EventHandler(this.txtFormat_TextChanged);
            // 
            // chkQuotes
            // 
            this.chkQuotes.AutoSize = true;
            this.chkQuotes.Location = new System.Drawing.Point(140, 174);
            this.chkQuotes.Name = "chkQuotes";
            this.chkQuotes.Size = new System.Drawing.Size(15, 14);
            this.chkQuotes.TabIndex = 5;
            this.chkQuotes.UseVisualStyleBackColor = true;
            this.chkQuotes.CheckedChanged += new System.EventHandler(this.chkQuotes_CheckedChanged);
            // 
            // cboJustification
            // 
            this.cboJustification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJustification.FormattingEnabled = true;
            this.cboJustification.Items.AddRange(new object[] {
            "Left",
            "Right"});
            this.cboJustification.Location = new System.Drawing.Point(140, 147);
            this.cboJustification.Name = "cboJustification";
            this.cboJustification.Size = new System.Drawing.Size(121, 21);
            this.cboJustification.TabIndex = 3;
            this.cboJustification.SelectedIndexChanged += new System.EventHandler(this.cboJustification_SelectedIndexChanged);
            // 
            // lblFieldName
            // 
            this.lblFieldName.AutoSize = true;
            this.lblFieldName.Location = new System.Drawing.Point(137, 54);
            this.lblFieldName.Name = "lblFieldName";
            this.lblFieldName.Size = new System.Drawing.Size(0, 13);
            this.lblFieldName.TabIndex = 18;
            // 
            // ctLayoutField
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblFieldName);
            this.Controls.Add(this.cboJustification);
            this.Controls.Add(this.chkQuotes);
            this.Controls.Add(this.txtFormat);
            this.Controls.Add(this.txtPadChar);
            this.Controls.Add(this.txtColumnWidth);
            this.Controls.Add(this.txtDisplayName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ctLayoutField";
            this.Size = new System.Drawing.Size(364, 234);
            this.Load += new System.EventHandler(this.ctLayoutField_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtDisplayName;
        private System.Windows.Forms.TextBox txtColumnWidth;
        private System.Windows.Forms.TextBox txtPadChar;
        private System.Windows.Forms.TextBox txtFormat;
        private System.Windows.Forms.CheckBox chkQuotes;
        private System.Windows.Forms.ComboBox cboJustification;
        private System.Windows.Forms.Label lblFieldName;
    }
}
