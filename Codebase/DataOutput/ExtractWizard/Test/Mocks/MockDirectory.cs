﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

namespace Test.Mocks
{
    public class MockDirectory : IDirectory
    {
        private bool exists = false;
        private bool canCreate;

        public MockDirectory(bool canCreateDirectory)
        {
            canCreate = canCreateDirectory;
            exists = canCreateDirectory;
        }

        public void CreateDirectory(string directory)
        {
            if (!canCreate)
            {
                exists = false;
                throw new IOException("Unable to create this directory");
            }

            exists = true;

        }

        public bool Exists(string directory)
        {
            return exists;
        }
    }
}
