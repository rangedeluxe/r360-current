﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Fakes;
using System.IO;
using System.IO.Fakes;
using Microsoft.QualityTools.Testing.Fakes;

namespace Test.ExtractLib
{
    [TestClass()]
    public class ImageUtilTests
    {
        private static string outputDirectory = @"Output/ImageUtilTests";

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            Helper.Cleanup(outputDirectory);
        }
        
        [TestMethod()]
        public void WillCreate_MultiPageTiff_FromTwoPageImageFile()
        {
            //arrange
            var images = new List<Image>();
            images.Add(new Bitmap(@"Resources/Multipage.tif"));
            var destinationFileName = $@"{outputDirectory}/TwoPageImage.tiff";
            
            //act
            ImageUtil.CreateMultiPageTiff(images, destinationFileName);

            //assert
            Assert.IsTrue(File.Exists(destinationFileName));
            var fileInfo = new FileInfo(destinationFileName);
            
            Assert.AreEqual(831160, fileInfo.Length);
        }

        [TestMethod()]
        public void WillCreate_MultiPageTiff_FromMultipleImageFiles()
        {
            //arrange
            var images = new List<Image>();
            images.Add(new Bitmap(@"Resources/Multipage.tif"));
            images.Add(new Bitmap(@"Resources/Image.tif"));
            var destinationFileName = $@"{outputDirectory}/MultipleImageFiles.tiff";

            //act
            ImageUtil.CreateMultiPageTiff(images, destinationFileName);

            //assert
            Assert.IsTrue(File.Exists(destinationFileName));
            var fileInfo = new FileInfo(destinationFileName);

            Assert.AreEqual(833046, fileInfo.Length);
        }
    }
}