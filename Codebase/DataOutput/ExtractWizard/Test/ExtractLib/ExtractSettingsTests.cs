﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test.Mocks;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

namespace Test.ExtractLib
{
    [TestClass]
    public class ExtractSettingsTests
    {
        private IConfigurationProvider configProvider = null;
        private const string ApplicationUriSetting = "ApplicationURI";
        private const string WCFConfigLocationSetting = "WCFConfigLocation";
        private const string LoginAttemptsSetting = "LoginAttempts";

        [TestInitialize]
        public void Setup()
        {
            configProvider = new MockConfigurationProvider();
            configProvider.SetSetting(ExtractAppSettings.ConnectionString.ToString(), "testing connection string");
            configProvider.SetSetting(ExtractAppSettings.SetupPath.ToString(), @"D:\Data\Definitions");
            configProvider.SetSetting(ExtractAppSettings.LogFilePath.ToString(), @"D:\WFSApps\RecHub\Data\LogFiles\{0:yyyyMMdd}_ExtractWizardLog.txt");
            configProvider.SetSetting(ExtractAppSettings.LogFileMaxSize.ToString(), "1024");
            configProvider.SetSetting(ExtractAppSettings.DefaultDefinitionFileFormat.ToString(), "xml");
            configProvider.SetSetting(ExtractAppSettings.CreateZeroLengthFile.ToString(), "true");
            configProvider.SetSetting(ExtractAppSettings.LogoPath.ToString(), "");
            configProvider.SetSetting(ExtractAppSettings.BackgroundColor.ToString(), "0");
            configProvider.SetSetting(ExtractAppSettings.RunAsUserId.ToString(), "1");
            configProvider.SetSetting(ExtractAppSettings.ExceptionLogFile.ToString(), @"D:\WFSApps\RecHub\Data\LogFiles\{0:yyyyMMdd}_ExtractWizardExceptionLog.txt");
            configProvider.SetSetting(ExtractAppSettings.LoggingDepth.ToString(), "2");
            configProvider.SetSetting(ExtractAppSettings.SiteKey.ToString(), "IPOnline");
            configProvider.SetSetting(ApplicationUriSetting, "Receivables360Online");
            configProvider.SetSetting(WCFConfigLocationSetting, @"D:\WFSApps\RecHub\bin2\WCFClients.extract.config");
            configProvider.SetSetting(LoginAttemptsSetting, "5");
            CommonLib.Directory = new MockDirectory(true);
        }

        [TestMethod]
        public void WillValidateExtractSettings()
        {
            //arrange
            var expected = false;
            var settings = new cExtractSettings();

            //act
            var actual = settings.ValidateSettings();

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void WillValidateAllExtractConfigurationSettings()
        {
            //arrange
            var expected = true;
            var actual = false;

            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateSettings();
            actual = true;

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfConnectionStringSettingIsMissing()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.ConnectionString.ToString(), "");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateConnectionString();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfLoggingDepthSettingIsMissing()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.LoggingDepth.ToString(), "");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateLoggingDepth();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfLoggingDepthSettingIsNotNumeric()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.LoggingDepth.ToString(), "this is not a number");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateLoggingDepth();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfLoggingDepthSettingIsNotValid()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.LoggingDepth.ToString(), "999");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateLoggingDepth();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfLoginAttemptsSettingIsMissing()
        {
            //arrange
            configProvider.SetSetting(LoginAttemptsSetting, "");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateLoginAttempts();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfLoginAttemptsSettingIsNotNumeric()
        {
            //arrange
            configProvider.SetSetting(LoginAttemptsSetting, "this isn't numeric");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateLoginAttempts();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfSiteKeySettingIsMissing()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.SiteKey.ToString(), "");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateSiteKey();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfApplicationUriSettingIsMissing()
        {
            //arrange
            configProvider.SetSetting(ApplicationUriSetting, "");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateApplicationUri();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfCreateZeroLengthFileSettingIsMissing()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.CreateZeroLengthFile.ToString(), "");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateZeroLengthFile();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfCreateZeroLengthFileSettingIsNotTrueOrFalse()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.CreateZeroLengthFile.ToString(), "this is not a boolean");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateZeroLengthFile();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfDefaultDefinitionFileFormatSettingIsMissing()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.DefaultDefinitionFileFormat.ToString(), null);
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateDefaultDefinitionFile();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfDefaultDefinitionFileFormatSettingIsNotAnExpectedValue()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.DefaultDefinitionFileFormat.ToString(), "json");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateDefaultDefinitionFile();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfBackgroundColorSettingIsMissing()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.BackgroundColor.ToString(), null);
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateBackgroundColor();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfBackgroundColorSettingIsNotValidValue()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.BackgroundColor.ToString(), "this is not a valid background color");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateBackgroundColor();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfLogoSettingIsOnAndTheFileDoesNotExist()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.LogoPath.ToString(), @"D:\WFSApps\RecHub\Data\Logos\UnitTestLogo.gif");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateLogo();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        public void WillUseApplicationDirectoryIfLogFilePathSettingIsMissing()
        {
            //arrange
            var expected = Environment.CurrentDirectory;
            configProvider.SetSetting(ExtractAppSettings.LogFilePath.ToString(), "");
            var settings = new ExtractConfigurationSettings(configProvider, new MockDirectory(false));

            //act
            settings.ValidateLogFile();
            var actual = settings.LogFilePath;

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfLogFileExists()
        {
            //arrange
            var settings = new ExtractConfigurationSettings(configProvider, new MockDirectory(true));

            //act
            settings.ValidateLogFile();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfLogFileMaxSizeIsNotGreaterThanZero()
        {
            //arrange
            configProvider.SetSetting(ExtractAppSettings.LogFileMaxSize.ToString(), "0");
            var settings = new ExtractConfigurationSettings(configProvider);

            //act
            settings.ValidateLogFile();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfUnableToCreateTheLogFilePath()
        {
            //arrange
            CommonLib.Directory = new MockDirectory(false);
            var settings = new ExtractConfigurationSettings(configProvider, new MockDirectory(true));

            //act
            settings.ValidateLogFile();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        public void WillUseApplicationDirectoryIfSetupPathSettingIsMissing()
        {
            //arrange
            var expected = Environment.CurrentDirectory;
            configProvider.SetSetting(ExtractAppSettings.SetupPath.ToString(), "");
            var settings = new ExtractConfigurationSettings(configProvider, new MockDirectory(false));

            //act
            settings.ValidateSetupPath();
            var actual = settings.SetupPath;

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfSetupFileDoesNotExist()
        {
            //arrange
            CommonLib.Directory = new MockDirectory(false);
            var settings = new ExtractConfigurationSettings(configProvider, new MockDirectory(false));

            //act
            settings.ValidateSetupPath();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidConfigurationException))]
        public void WillFailIfUnableToCreateExceptionLogDirectory()
        {
            //arrange
            CommonLib.Directory = new MockDirectory(false);
            var settings = new ExtractConfigurationSettings(configProvider, new MockDirectory(false));

            //act
            settings.ValidateExceptionLogFile();

            //assert
            Assert.Fail("Should have thrown an invalid configuration exception");
        }

        [TestMethod]
        public void WillUseApplicationDirectoryIfExceptionLogFileSettingIsMissing()
        {
            //arrange
            var expected = Environment.CurrentDirectory;
            configProvider.SetSetting(ExtractAppSettings.ExceptionLogFile.ToString(), "");
            var settings = new ExtractConfigurationSettings(configProvider, new MockDirectory(false));

            //act
            settings.ValidateExceptionLogFile();
            var actual = settings.ExceptionLogFile;

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
