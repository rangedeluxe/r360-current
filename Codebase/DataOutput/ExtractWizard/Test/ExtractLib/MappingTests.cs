﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.ExtractLib
{
    [TestClass()]
    public class MappingTests
    {
        [TestMethod()]
        public void WillMapBanksTableToFriendlyName()
        {
            //arrange
            var expected = "Banks";
            var mappingText = "RecHubData.dimBanks";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillMapWorkgroupsTableToFriendlyName()
        {
            //arrange
            var expected = "Workgroups";
            var mappingText = "RecHubData.dimClientAccounts";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillMapBatchesTableToFriendlyName()
        {
            //arrange
            var expected = "Batches";
            var mappingText = "RecHubData.factBatchSummary";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillMapTransactionsTableToFriendlyName()
        {
            //arrange
            var expected = "Transactions";
            var mappingText = "RecHubData.factTransactionSummary";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillMapPaymentsTableToFriendlyName()
        {
            //arrange
            var expected = "Payments";
            var mappingText = "RecHubData.factChecks";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillMapStubsTableToFriendlyName()
        {
            //arrange
            var expected = "Stubs";
            var mappingText = "RecHubData.factStubs";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillMapDocumentwTableToFriendlyName()
        {
            //arrange
            var expected = "Documents";
            var mappingText = "RecHubData.factDocuments";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillReturnNullForFriendlyNameIfNullTextIsPassedIn()
        {
            //arrange
            string expected = null;
            string mappingText = null;

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillHandleTextThatIsNotTableName()
        {
            //arrange
            string expected = "this_is_not_a_table";
            string mappingText = "this_is_not_a_table";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillHandleTextThatIsNotColumnName()
        {
            //arrange
            string expected = "this_is_not_a_column";
            string mappingText = "this_is_not_a_column";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillMapBatchIdFriendlyColumnName()
        {
            //arrange
            string expected = "BatchID";
            string mappingText = "database.table.SourceBatchID";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillMapColumnName()
        {
            //arrange
            string expected = "MyColumn";
            string mappingText = "database.table.MyColumn";

            //act
            var actual = cMappingLib.GetFriendly(mappingText);

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}