﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 2/11/2014
*
* Purpose: 
*
* Modification History
* WI 129315 BLR 2/11/2014
*   -Initial Version 
* WI 129719 DLD 03/05/2014
*   -Removed ExtractAppSettings.ImagePath. Depreciated with cut-over to OLFServices   
******************************************************************************/

namespace Test.ExtractLib
{
    /// <summary>
    /// Fixture in charge of testing configuration settings for ExtractWizard and ExtractRun.
    /// Files include: 
    ///     ExtractLib.cAppSettingsLib.cs, 
    ///     ExtractAPI.cExtractSettings
    /// </summary>
    [TestClass]
    public class ApplicationSettings
    {
        /// <summary>
        /// Tests the standard application settings extraction.  Expected values can be found
        /// in the app.config file.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="expectedvalue"></param>\
        [TestMethod]
        public void SetupPath()
        {
            StandardApplicationSettings(ExtractAppSettings.SetupPath, @"C:\Data\Definitions");
        }
        [TestMethod]
        public void LogFilePath()
        {
            StandardApplicationSettings(ExtractAppSettings.LogFilePath, @"C:\WFSApps\RecHub\Data\LogFiles\{0:yyyyMMdd}_ExtractWizardLog.txt");
        }
        [TestMethod]
        public void ExceptionLogFile()
        {
            StandardApplicationSettings(ExtractAppSettings.ExceptionLogFile, @"C:\WFSApps\RecHub\Data\LogFiles\{0:yyyyMMdd}_ExtractWizardExceptionLog.txt");
        }
        [TestMethod]
        public void DefaultDefinitionFileFormat()
        {
            StandardApplicationSettings(ExtractAppSettings.DefaultDefinitionFileFormat, "xml");
        }
        [TestMethod]
        public void CreateZeroLengthFile()
        {
            StandardApplicationSettings(ExtractAppSettings.CreateZeroLengthFile, "true");
        }
        [TestMethod]
        public void LogoPath()
        {
            StandardApplicationSettings(ExtractAppSettings.LogoPath, "");
        }
        [TestMethod]
        public void BackgroundColor()
        {
            StandardApplicationSettings(ExtractAppSettings.BackgroundColor, "0");
        }
        public void StandardApplicationSettings(ExtractAppSettings key, string expectedvalue)
        {
            // Act
            var actualvalue = cAppSettingsLib.Get(key);

            // Assert
            Assert.AreEqual(expectedvalue, actualvalue);
        }

        [TestMethod]
        public void LogFileMaxSize()
        {
            StandardApplicationSettingsInt(ExtractAppSettings.LogFileMaxSize, 1024);
        }
        public void StandardApplicationSettingsInt(ExtractAppSettings key, int expectedvalue)
        {
            // Act
            var actualvalue = cAppSettingsLib.Get<int>(key, expectedvalue);

            // Assert
            Assert.AreEqual(expectedvalue, actualvalue);
        }

        /// <summary>
        /// Tests the standard application settings, tossing it some garbage values. Expect
        /// to see empty values returning and not thrown errors.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="expectedvalue"></param>
        [TestMethod]
        public void GarbageInput1()
        {
            StandardApplicationSettingsRobustness("GarbageKeyInput", "");
        }
        [TestMethod]
        public void GarbageInput2()
        {
            StandardApplicationSettingsRobustness("PLIUE:LKNL:SDK0e09'\"[]$%^&*()JU)-2-hnpa;", "");
        }
        [TestMethod]
        public void GarbageInput3()
        {
            StandardApplicationSettingsRobustness("", "");
        }
        [TestMethod]
        public void GarbageInput4()
        {
            StandardApplicationSettingsRobustness("WorkstationId", "");
        }

        public void StandardApplicationSettingsRobustness(string key, string expectedvalue)
        {
            // Act
            var actualvalue = cAppSettingsLib.Get(key);

            // Assert
            Assert.AreEqual(expectedvalue, actualvalue);
        }

        /// <summary>
        /// Tests the CustomConfigurationSection ColumnsToRemove.
        /// </summary>
        [TestMethod]
        public void ColumnsToRemoveCollection()
        {
            // Act
            var cols = new cExtractSettings().EntryColumnsToRemove;

            // Assert
            Assert.IsNotNull(cols);
            Assert.IsTrue(cols.Contains("MostRecent"));
            //Assert.AreEqual(1, cols.Count);
            //Assert.AreEqual("MostRecent", cols[0]);
        }

        /// <summary>
        /// Tests the CustomConfigurationSection ConfirmationFields.
        /// </summary>
        [TestMethod]
        public void ConfirmationFieldsCollection()
        {
            // Act
            var flds = new cExtractSettings().ExtractStatsColumns;

            // Assert
            Assert.IsNotNull(flds);
            Assert.AreEqual(16, flds.Count);
        }
    }
}