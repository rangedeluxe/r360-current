﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Test.ExtractWizard;
using WFS.RecHub.DAL.ExtractEngine;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;

namespace Test.ExtractAPI {
    [TestClass]
    public class ExtractEngineDALTests {
        [TestMethod]
        public void ExtractEngineDAL_BuildDataEntryColumnName_ShouldConcatPaymentSourceAndFieldNameWithADotBetween() {
            var expectedValue = "Cloned_2";
            var testFieldList=new string[] {
                "Cloned",
                "Cloned",
                "Cloned"
            };
            var fields = testFieldList.Select(item => new cLayoutField(new cLayout(), item)).ToList();
            SQLExtractAPI.EnsureUniqueFieldNames(fields);

            Assert.AreEqual(fields.Last().UniqueFieldName, expectedValue);
        }
    }
}
