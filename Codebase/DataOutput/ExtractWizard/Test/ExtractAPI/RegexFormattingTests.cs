﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;

namespace Test.ExtractAPI
{
    [TestClass]
    public class RegexFormattingTests
    {

        [TestMethod]
        public void RegexFormatting_ShouldNot_ErrorOnEmptyInput()
        {
            // Arrange
            var input = "%r";
            var testvalue = "TestValue";

            // Act
            var output = CustomFormat.FormatRegex(testvalue, input);

            // Assert
            Assert.AreEqual("TestValue", output);
        }

        [TestMethod]
        public void RegexFormatting_ShouldNot_ErrorOnEmptyPattern()
        {
            // Arrange
            var input = "%rTest";
            var testvalue = "TestValue";

            // Act
            var output = CustomFormat.FormatRegex(testvalue, input);

            // Assert
            Assert.AreEqual("TestValue", output);
        }

        [TestMethod]
        public void RegexFormatting_Should_ReplaceSimpleEmptyInput()
        {
            // Arrange
            var input = "Test%r";
            var testvalue = "TestValue";

            // Act
            var output = CustomFormat.FormatRegex(testvalue, input);

            // Assert
            Assert.AreEqual("Value", output);
        }

        [TestMethod]
        public void RegexFormatting_Should_ReplaceSimpleInput()
        {
            // Arrange
            var input = "Test%rSquirrel";
            var testvalue = "TestValue";

            // Act
            var output = CustomFormat.FormatRegex(testvalue, input);

            // Assert
            Assert.AreEqual("SquirrelValue", output);
        }

        [TestMethod]
        public void RegexFormatting_Should_RemoveDecimalPoints()
        {
            // Arrange
            var input = @"\.%r";
            var testvalue = "10.00";

            // Act
            var output = CustomFormat.FormatRegex(testvalue, input);

            // Assert
            Assert.AreEqual("1000", output);
        }

        [TestMethod]
        public void RegexFormatting_Should_PerformAdvancedRegexReplace()
        {
            // Arrange
            var input = @"(\d+)\.(\d+)%r'$1'.'$2'";
            var testvalue = "10.00";

            // Act
            var output = CustomFormat.FormatRegex(testvalue, input);

            // Assert
            Assert.AreEqual("'10'.'00'", output);
        }
    }
}
