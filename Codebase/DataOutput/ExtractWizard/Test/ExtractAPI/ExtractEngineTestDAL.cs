﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 269611 JMC 03/10/2016
*   -Created Mock database object
******************************************************************************/
namespace Test.ExtractWizard {
    class ExtractEngineTestDAL : IExtractEngineDAL {
        public event outputErrorEventHandler OutputError;
        public event outputMessageEventHandler OutputMessage;

        public struct TestClientAccountRecord {
            public int SiteBankID;
            public int SiteClientAccountID;
            public string LongName;
            public int SiteCodeID;
        }

        private TestClientAccountRecord[] _testClientAccounts = new TestClientAccountRecord[] {};

        public ExtractEngineTestDAL() {
            EventLogMessages = new List<string>();
        }

        public ExtractEngineTestDAL(TestClientAccountRecord[] testClientAccounts):this() {
            _testClientAccounts = testClientAccounts;
        }

        public void BeginTrans() {
        }

        public void CommitTrans() {
        }

        public void Dispose() {
            throw new NotImplementedException();
        }

        public bool GetBanks(IExtractParms parms, IExtractDef extractDef, Dictionary<string, IColumn> bankTableDef, out SqlDataReader drBanks) {
            throw new NotImplementedException();
        }

        public bool GetBatches(IExtractParms parms, IExtractDef extractDef, int dteCurrent, Dictionary<string, IColumn> bankTableDef, Dictionary<string, IColumn> clientAccountTableDef, Dictionary<string, IColumn> extractTracesTableDef, Dictionary<string, IColumn> batchTableDef, out SqlDataReader drBatches) {
            throw new NotImplementedException();
        }

        public bool GetCheckInfo(cItemKey checkKey, out DataTable dtCheckInfo) {
            throw new NotImplementedException();
        }

        public bool GetClientAccounts(IExtractParms parms, IExtractDef extractDef, Dictionary<string, IColumn> bankTableDef, Dictionary<string, IColumn> clientAccountTableDef, out DbDataReader drClientAccounts) {
            var dt = new DataTable();

            dt.Columns.AddRange(new [] {
                new DataColumn("RecHubData.dimClientAccounts.SiteBankID"         , typeof(Int32)),
                new DataColumn("RecHubData.dimClientAccounts.SiteClientAccountID", typeof(Int32)),
                new DataColumn("RecHubData.dimClientAccounts.LongName"           , typeof(String)),
                new DataColumn("RecHubData.dimClientAccounts.SiteCodeID"         , typeof(Int32))
            });
            foreach (var clientAccount in _testClientAccounts) {
                dt.Rows.Add(clientAccount.SiteBankID, 
                            clientAccount.SiteClientAccountID, 
                            clientAccount.LongName, 
                            clientAccount.SiteCodeID);
            }

            drClientAccounts = dt.CreateDataReader();
            return true;
        }

        public bool GetDepositDates(IExtractParms parms, IExtractDef extractDef, string factTableName, Dictionary<string, IColumn> clientAccountTableDef, Dictionary<string, IColumn> extractTracesTableDef, Dictionary<string, IColumn> batchTableDef, out DataTable dtDepositDates) {
            throw new NotImplementedException();
        }

        public bool GetDocumentInfo(cItemKey documentKey, out DataTable dtDocumentInfo) {
            throw new NotImplementedException();
        }

        public bool GetDocuments(IExtractParms parms, IExtractDef extractDef, int dteCurrent, Dictionary<string, IColumn> bankTableDef,  Dictionary<string, IColumn> clientAccountTableDef, Dictionary<string, IColumn> extractTracesTableDef, Dictionary<string, IColumn> batchTableDef, Dictionary<string, IColumn> transactionsTableDef, Dictionary<string, IColumn> documentsTableDef, out SqlDataReader drDocuments) {
            throw new NotImplementedException();
        }

        public bool GetPayments(IExtractParms parms, IExtractDef extractDef, int dteCurrent, Dictionary<string, IColumn> bankTableDef, Dictionary<string, IColumn> clientAccountTableDef, Dictionary<string, IColumn> extractTracesTableDef, Dictionary<string, IColumn> batchTableDef, Dictionary<string, IColumn> transactionsTableDef, Dictionary<string, IColumn> paymentsTableDef, out SqlDataReader drPayments) {
            throw new NotImplementedException();
        }

        public bool GetPreviousRuns(string columnName, out DataTable dt) {
            throw new NotImplementedException();
        }

        public bool GetStubs(IExtractParms parms, IExtractDef extractDef, int dteCurrent, Dictionary<string, IColumn> bankTableDef,  Dictionary<string, IColumn> clientAccountTableDef, Dictionary<string, IColumn> extractTracesTableDef, Dictionary<string, IColumn> batchTableDef, Dictionary<string, IColumn> transactionsTableDef, Dictionary<string, IColumn> stubsTableDef, out SqlDataReader drStubs) {
            throw new NotImplementedException();
        }

        public bool GetTableDef(string tableName, out DataTable dtTableDef) {

            DataTable dt;

            switch (tableName) {
                case "dimBanks":
                    dt = new DataTable();

                    AddTableDefColumnSet(ref dt);

                    dt.Rows.Add("RecHubData", "dimBanks", "BankKey", "int", null);
                    dt.Rows.Add("RecHubData", "dimBanks", "SiteBankID", "int", null);
                    dt.Rows.Add("RecHubData", "dimBanks", "MostRecent", "int", null);
                    dt.Rows.Add("RecHubData", "dimBanks", "EntityID", "int", null);
                    dt.Rows.Add("RecHubData", "dimBanks", "CreationDate", "datetime", null);
                    dt.Rows.Add("RecHubData", "dimBanks", "ModificationDate", "datetime", null);
                    dt.Rows.Add("RecHubData", "dimBanks", "BankName", "varchar", 128);
                    dt.Rows.Add("RecHubData", "dimBanks", "ABA", "varchar", 10);
                    dtTableDef = dt;
                    break;

                case "factBatchSummary":
                    dt = new DataTable();

                    AddTableDefColumnSet(ref dt);

                    dt.Rows.Add("RecHubData", "factBatchSummary", "factBatchSummaryKey", "bigint", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "IsDeleted", "bit", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "BankKey", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "ClientAccountKey", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "DepositDateKey", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "ImmutableDateKey", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "SourceProcessingDateKey", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "BatchID", "bigint", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "SourceBatchID", "bigint", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "BatchNumber", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "BatchSourceKey", "smallint", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "BatchPaymentTypeKey", "tinyint", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "BatchExceptionStatusKey", "tinyint", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "BatchCueID", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "DepositStatus", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "SystemType", "tinyint", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "DepositStatusKey", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "TransactionCount", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "CheckCount", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "ScannedCheckCount", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "StubCount", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "DocumentCount", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "CheckTotal", "money", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "StubTotal", "money", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "CreationDate", "datetime", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "ModificationDate", "datetime", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "BatchSiteCode", "int", null);
                    dt.Rows.Add("RecHubData", "factBatchSummary", "DepositDDA", "varchar", 40);
                    dtTableDef = dt;
                    break;

                case "dimClientAccounts":
                    dt = new DataTable();
                    AddTableDefColumnSet(ref dt);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "ClientAccountKey", "int", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "SiteCodeID", "int", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "SiteBankID", "int", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "SiteClientAccountID", "int", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "MostRecent", "bit", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "IsActive", "tinyint", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "CutOff", "tinyint", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "OnlineColorMode", "tinyint", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "IsCommingled", "bit", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "DataRetentionDays", "smallint", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "ImageRetentionDays", "smallint", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "CreationDate", "datetime", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "ModificationDate", "datetime", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "SiteClientAccountKey", "uniqueidentifier", null);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "ShortName", "varchar", 20);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "LongName", "varchar", 40);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "POBox", "varchar", 32);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "DDA", "varchar", 40);
                    dt.Rows.Add("RecHubData", "dimClientAccounts", "FileGroup", "varchar", 256);
                    dtTableDef = dt;
                    break;

                default:
                    throw new NotImplementedException();
            }

            dtTableDef = null;
            return (false);
        }

        private void AddTableDefColumnSet(ref DataTable dt) {
            dt.Columns.Add(new DataColumn("Schema", typeof(String)));
            dt.Columns.Add(new DataColumn("TableName", typeof(String)));
            dt.Columns.Add(new DataColumn("ColumnName", typeof(String)));
            dt.Columns.Add(new DataColumn("Type", typeof(String)));
            dt.Columns.Add(new DataColumn("Length", typeof(Int32)));
        }

        public bool GetTransactions(IExtractParms parms, IExtractDef extractDef, int dteCurrent, Dictionary<string, IColumn> bankTableDef,  Dictionary<string, IColumn> clientAccountTableDef, Dictionary<string, IColumn> extractTracesTableDef, Dictionary<string, IColumn> batchTableDef, Dictionary<string, IColumn> transactionsTableDef, out SqlDataReader drTransactions) {
            throw new NotImplementedException();
        }

        public bool InsertExtractAudit(
            Guid extractAuditID,
            int bankID,
            int workgroupID,
            bool isComplete, 
            int checkCount, 
            int stubCount, 
            decimal paymentTotal, 
            decimal stubTotal, 
            int recordCount, 
            string fileName) {

            return (true);
        }

        public void RollbackTrans() {
            return;
        }

        public bool TestConnection() {
            return(true);
        }

        public bool WriteAuditFileData(IExtractParms parms, IExtractDef extractDef, cStandardFields standardFields, int workstationID) {
            return (true);
        }

        public bool WriteEventLog(string eventName, string message, int sitecodeid, int sitebankid, int siteworkgroupid) {
            EventLogMessages.Add(message);
            return (true);
        }

        public bool WriteExtractSequenceNumber(IExtractParms parms, cBatchKey[] uniqueBatchKeys) {
            return (true);
        }

        public bool WriteSystemLevelEventLog(string eventName, string message) {
            return (true);
        }

        public bool WriteTraceField(IExtractParms parms, cStandardFields standardFields, long batchID) {
            return (true);
        }

        public List<string> EventLogMessages {
            get; set; }
    }
}
