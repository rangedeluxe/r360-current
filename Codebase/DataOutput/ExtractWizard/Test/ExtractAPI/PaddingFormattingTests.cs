﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;

namespace Test.ExtractAPI
{
    /*
    if (col.DataType == 1)
        - STRING
    else if (col.DataType == 6)
        - FLOAT
    else if (col.DataType == 7)
        - MONEY
    else if (col.DataType == 24)
        - DATETIME
    */

    [TestClass]
    public class PaddingFormattingTests
    {
        public cPayment TestPayment
        {
            get
            {
                var check = new cPayment(null, null, 1, 2, 3.0m, "account", "routingNumber", 4000L, null);
                return check;
            }
        }

        public cStub TestStub
        {
            get
            {
                var stub = new cStub(null, 1, 2, 3.0m, null);
                return stub;
            }
        }

        public cLayoutField TestPaymentField
        {
            get
            {
                var definition = new cExtractDef();
                definition.UseQuotes = false;
                var field = new cLayoutField(new cLayout(definition), "RecHubData.ChecksDataEntry.Sometextfield");
                field.DataType = 1;
                field.ColumnWidth = 5;
                field.PadChar = ' ';
                field.IsDataEntry = true;
                return field;
            }
        }

        public cLayoutField TestStubField
        {
            get
            {
                var definition = new cExtractDef();
                definition.UseQuotes = false;
                var field = new cLayoutField(new cLayout(definition), "RecHubData.StubsDataEntry.Sometextfield");
                field.DataType = 1;
                field.ColumnWidth = 5;
                field.PadChar = ' ';
                field.IsDataEntry = true;
                return field;
            }
        }


        [TestMethod]
        public void Empty_StringDataEntryValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestPaymentField;
            var check = TestPayment;
            check.CheckFields.Add("RecHubData.ChecksDataEntry.Sometextfield", "");

            // Act
            var result = field.FormatLayoutField(null, check, null);

            // Assert
            Assert.AreEqual("     ", result);
        }

        [TestMethod]
        public void Null_PaymentDataEntryValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestPaymentField;
            var check = TestPayment;
            check.CheckFields.Add("RecHubData.ChecksDataEntry.Sometextfield", null);

            // Act
            var result = field.FormatLayoutField(null, check, null);

            // Assert
            Assert.AreEqual("     ", result);
        }

        [TestMethod]
        public void Null_PaymentDataEntryFloatValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestPaymentField;
            var check = TestPayment;
            check.CheckFields.Add("RecHubData.ChecksDataEntry.Sometextfield", null);
            field.DataType = 6;

            // Act
            var result = field.FormatLayoutField(null, check, null);

            // Assert
            Assert.AreEqual("0    ", result);
        }

        [TestMethod]
        public void Null_PaymentDataEntryDecimalValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestPaymentField;
            var check = TestPayment;
            check.CheckFields.Add("RecHubData.ChecksDataEntry.Sometextfield", null);
            field.DataType = 7;
            field.Format = "0.00";

            // Act
            var result = field.FormatLayoutField(null, check, null);

            // Assert
            Assert.AreEqual("0.00 ", result);
        }

        [TestMethod]
        public void Null_PaymentDataEntryDateTimeValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestPaymentField;
            var check = TestPayment;
            check.CheckFields.Add("RecHubData.ChecksDataEntry.Sometextfield", null);
            field.DataType = 24;
            field.Format = "mm-dd-yyyy";

            // Act
            var result = field.FormatLayoutField(null, check, null);

            // Assert
            Assert.AreEqual("     ", result);
        }



        [TestMethod]
        public void Null_StubDataEntryValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestStubField;
            var stub = TestStub;
            stub.StubFields.Add("RecHubData.StubsDataEntry.Sometextfield", null);

            // Act
            var result = field.FormatLayoutField(null, stub, null);

            // Assert
            Assert.AreEqual("     ", result);
        }

        [TestMethod]
        public void Null_StubDataEntryFloatValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestStubField;
            var stub = TestStub;
            stub.StubFields.Add("RecHubData.StubsDataEntry.Sometextfield", null);
            field.DataType = 6;

            // Act
            var result = field.FormatLayoutField(null, stub, null);

            // Assert
            Assert.AreEqual("0    ", result);
        }

        [TestMethod]
        public void Null_StubDataEntryDecimalValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestStubField;
            var stub = TestStub;
            stub.StubFields.Add("RecHubData.StubsDataEntry.Sometextfield", null);
            field.DataType = 7;
            field.Format = "0.00";

            // Act
            var result = field.FormatLayoutField(null, stub, null);

            // Assert
            Assert.AreEqual("0.00 ", result);
        }

        [TestMethod]
        public void Null_StubDataEntryDateTimeValues_With_PaddedFormatting_Should_PadCorrectly()
        {
            // Arrange
            var field = TestStubField;
            var stub = TestStub;
            stub.StubFields.Add("RecHubData.StubsDataEntry.Sometextfield", null);
            field.DataType = 24;
            field.Format = "mm-dd-yyyy";

            // Act
            var result = field.FormatLayoutField(null, stub, null);

            // Assert
            Assert.AreEqual("     ", result);
        }
    }
}
