﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;

namespace Test.ExtractAPI
{
    [TestClass]
    public class LoadDefinitionFromXMLTests
    {

        public XmlNode BuildTestXMLNode(XmlDocument doc, int number)
        {
            var xml =  string.Format("<Layout LayoutName=\"\" RecordCode=\"1\" UseOccursGroups=\"0\">", number)
                 + string.Format(       "<LayoutField FieldName = \"TestField{0}\" DataType = \"1\" ColumnWidth = \"0\" PadChar = \" \" Justification = \"L\" Quotes = \"1\" Format = \"Format{0}\" IsDataEntry = \"True\" DisplayName = \"DisplayName{0}\" SiteBankID = \"{0}\" SiteClientAccountID = \"{0}\" PaymentSource = \"PaymentSource{0}\" />", number)
                 + string.Format(    "</Layout>");
            doc.LoadXml(xml);
            var node = doc.SelectSingleNode("/Layout/LayoutField");
            return node;
        }

        [TestMethod]
        public void ParseLayoutFieldNode_Should_ParseDataEntryKeys()
        {
            // Arrange
            var layout = new cLayout();
            var document = new XmlDocument();
            var node = BuildTestXMLNode(document, 0);
            cLayoutField field;

            // Act
            cExtractDef.ParseLayoutFieldNode(node, layout, out field);

            // Assert
            Assert.AreEqual("TestField0", field.FieldName);
            Assert.AreEqual(true, field.IsDataEntry);
            Assert.AreEqual("DisplayName0", field.DisplayName);
            Assert.AreEqual(0, field.SiteBankID);
            Assert.AreEqual(0, field.SiteClientAccountID);
            Assert.AreEqual("PaymentSource0", field.PaymentSource);
        }
    }
}
