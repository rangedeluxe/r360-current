﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Fakes;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 269611 JMC 03/10/2016
*   -Created simple unit test class to be expanded later
* WI 269414 JMC 03/10/2016
*   -Renamed Test method.
*   -Modified constructor to pass in Mocked DAL
*   -This Unit Test will not be very useful until better fleshed out.
******************************************************************************/
namespace Test.ExtractWizard {
    [TestClass]
    public class ExtractEngine {
        private cBank _bank;
        private cClientAccount _currentWorkgroup;
        private cBatch _currentBatch;
        private bool _extractCompleteAlertGenerated;
        private enum OutputMode
        {
            WriteToDisk = 0,
            SendToNotifications = 1,
            WriteToDiskAndSendToNotifications = 2
        }

        [TestInitialize]
        public void BuildTestClientAccount() {
            _bank = new cBank(8, new IOrderByColumn[] {});
            _currentWorkgroup = new cClientAccount(_bank, 1, 10, 1, "Test Workgroup", new IOrderByColumn[] {});
            _currentBatch = new cBatch(_currentWorkgroup, 8, 10, 0, 20150101, "", "", 0, 20150101, new IOrderByColumn[] {});
            _bank.Lockboxes.Add(10, _currentWorkgroup);
            _currentWorkgroup.Batches.Add(_currentBatch.BatchID, _currentBatch);
            _extractCompleteAlertGenerated = false;
        }

        [TestMethod]
        [Ignore]
        public void TestSimpleExtract() {

            Guid gidExtractAuditID;
            StringDictionary dicExtractStats;

            cExtract objExtract = new cExtract(new ExtractEngineTestDAL());
            objExtract.RunExtract(
                BuildTestExtractDef(),
                BuildTestExtractParms(),
                out gidExtractAuditID,
                out dicExtractStats);
        }

        [TestMethod]
        public void cExtractHasWorkgroupFilters_NoWorkgroupFilters_ShouldReturnFalse() {
            var extractDefinition = new cExtractDef();
            var runParm = new cExtractParms();
            Assert.IsFalse(cExtract.HasWorkgroupFilters(extractDefinition, runParm));
        }

        [TestMethod]
        public void cExtractHasWorkgroupFilters_OnlyWorkgroupFiltersInRunParms_ShouldReturnTrue() {
            var extractDefinition = new cExtractDef();
            var runParm = new cExtractParms() {
                LockboxID = new long[] {100}
            };
            Assert.IsTrue(cExtract.HasWorkgroupFilters(extractDefinition, runParm));
        }

        [TestMethod]
        public void cExtractHasWorkgroupFilters_OnlyWorkgroupFiltersInTheDefinition_ShouldReturnTrue() {
            var extractDefinition = new cExtractDef();
            extractDefinition.ParseExtractDefXmlFile(Path.GetFullPath(Path.Combine(Path.GetDirectoryName(typeof(cExtract).Assembly.Location), @".\Resources\Definition With Workgroup Limits.xml")));
            var runParm = new cExtractParms();
            Assert.IsTrue(cExtract.HasWorkgroupFilters(extractDefinition, runParm));
        }

        [TestMethod]
        public void cExtractGenerateDateFilterTextForAlert_SimpleDateRangeFilterForBothDeplositAndProcessingInRunParms_ShouldReturnHyphenatedDateRange() {
            var runParm = new cExtractParms() {
                DepositDateFrom = new DateTime(2015, 1, 1),
                DepositDateTo = new DateTime(2015, 10, 12),
                ProcessingDateFrom = new DateTime(2015, 10, 10),
                ProcessingDateTo = new DateTime(2015, 10, 11)
            };
            var extractDef = new cExtractDef();
            var expectedString = "Deposit Date: 1/1/2015-10/12/2015. \nProcessing Date: 10/10/2015-10/11/2015. ";
            Assert.AreEqual(cExtract.GenerateDateFilterTextForAlert(runParm), expectedString);
        }

        [TestMethod]
        public void cExtractGenerateDateFilterTextForAlert_SimpleDateRangeFilterInRunParms_ShouldReturnHyphenatedDateRange() {
            var runParm = new cExtractParms() {
                DepositDateFrom = new DateTime(2015, 1, 1),
                DepositDateTo = new DateTime(2015, 10, 12),
            };
            var extractDef = new cExtractDef();
            var expectedString = "Deposit Date: 1/1/2015-10/12/2015. ";
            Assert.AreEqual(cExtract.GenerateDateFilterTextForAlert(runParm), expectedString);
        }

        [TestMethod]
        public void cExtractGenerateDateFilterTextForAlert_InvalidDateRangeFilterInRunParms_ShouldReturnANoDatesMessage() {
            var runParm = new cExtractParms() {
                DepositDateFrom = new DateTime(2015, 10, 1),
                DepositDateTo = new DateTime(2015, 1, 12),
            };
            var extractDef = new cExtractDef();
            var test123 = cExtract.GenerateDateFilterTextForAlert(runParm);
            var expectedString = "Deposit Date: Filter excludes all dates. ";
            Assert.AreEqual(cExtract.GenerateDateFilterTextForAlert(runParm), expectedString);
        }

        [TestMethod]
        public void cExtractGenerateDateFilterTextForAlert_SingleDateFilterInRunParms_ShouldReturnASingleDate() {
            var runParm = new cExtractParms() {
                ProcessingDateFrom = new DateTime(2015, 1, 1),
                ProcessingDateTo = new DateTime(2015, 1, 1),
            };
            var extractDef = new cExtractDef();
            var expectedString = "Processing Date: 1/1/2015. ";
            Assert.AreEqual(cExtract.GenerateDateFilterTextForAlert(runParm), expectedString);
        }

        [TestMethod]
        [Ignore]
        public void cExtractHasWorkgroupFilters_WriteCompleteLogs_WithBothProcessingDateAndDepositDate_ShouldMatchExpectedText() {
            var extractDAL = new ExtractEngineTestDAL();
            var extractEngine = new cExtract(extractDAL);
            
            var callWriteCompleteLogsSuccessful = false;
            var expectedMessageText =
                "Your integrated Receivables extract has been completed:\n" +
                "Deposit Date: 6/17/2016\n" +
                "Processing Date: 6/16/2016\n" +
                "Total Payments: 1, Total Amount: 10.50.\n" +
                "\n" +
                "System Reference Numbers:\n" +
                "Workgroup = 10 - Test Workgroup\n" +
                "Extract File Name: Extract.txt";
            var runParms = new cExtractParms() {
                DepositDateFrom = new DateTime(2016, 6, 17),
                DepositDateTo = new DateTime(2016, 6, 17),
                ProcessingDateFrom = new DateTime(2016, 6, 16),
                ProcessingDateTo = new DateTime(2016, 6, 16)
            };

            AddPaymentsToCurrentBatch((decimal)10.50);

            callWriteCompleteLogsSuccessful = extractEngine.WriteCompleteLogs(new OrderedDictionary() {
                {1000, _bank}
            }, @"C:\Temp\Extract.txt", runParms);
            Assert.IsTrue(callWriteCompleteLogsSuccessful);
            Assert.AreEqual(expectedMessageText, extractDAL.EventLogMessages.FirstOrDefault());
        }

        [TestMethod]
        [Ignore]
        public void cExtractHasWorkgroupFilters_WriteCompleteLogs_WithBothProcessingAndDepositDateNoBatches_ShouldMatchExpected() {
            var extractDAL = new ExtractEngineTestDAL();
            var extractEngine = new cExtract(extractDAL);

            var callWriteCompleteLogsSuccessful = false;
            var expectedMessageText =
                "Your integrated Receivables extract has been completed:\n" +
                "Deposit Date: 6/17/2016\n" +
                "Processing Date: 6/16/2016\n" +
                "Total Payments: 0, Total Amount: 0.00.\n" +
                "\n" +
                "System Reference Numbers:\n" +
                "Workgroup = 10 - Test Workgroup\n" +
                "Extract File Name: Extract.txt";
            var runParms = new cExtractParms() {
                DepositDateFrom = new DateTime(2016, 6, 17),
                DepositDateTo = new DateTime(2016, 6, 17),
                ProcessingDateFrom = new DateTime(2016, 6, 16),
                ProcessingDateTo = new DateTime(2016, 6, 16)
            };
            _currentWorkgroup.Batches.Clear();
            callWriteCompleteLogsSuccessful = extractEngine.WriteCompleteLogs(new OrderedDictionary() {
                {1000, _bank}
            }, @"C:\Temp\Extract.txt", runParms);
            Assert.IsTrue(callWriteCompleteLogsSuccessful);
            Assert.AreEqual(expectedMessageText, extractDAL.EventLogMessages.FirstOrDefault());
        }

        [TestMethod]
        public void Will_GenerateExtractCompleteAlert_WhenOutputModeIsWriteToDisk()
        {
            //arrange
            var extract = new cExtract(GetExtractEngineDAL());

            //act
            var response = extract.WriteCompleteLogs(GetBanks(), "target", GetExtractParms(OutputMode.WriteToDisk));

            //assert
            Assert.AreEqual(true, response);
            Assert.AreEqual(true, _extractCompleteAlertGenerated);
        }

        [TestMethod]
        public void WillNot_GenerateExtractCompleteAlert_WhenOutputModeIsNotWriteToDisk()
        {
            //arrange
            var extract = new cExtract(GetExtractEngineDAL());

            //act
            var response = extract.WriteCompleteLogs(GetBanks(), "target", GetExtractParms(OutputMode.SendToNotifications));

            //assert
            Assert.AreEqual(true, response);
            Assert.AreEqual(false, _extractCompleteAlertGenerated);
        }

        [TestMethod]
        public void Will_HandleException_WhenGeneratingExtractCompleteAlert()
        {
            //arrange
            var dal = GetExtractEngineDAL();
            dal.BeginTrans = () => throw new UnauthorizedAccessException();
            dal.RollbackTrans = () => { };
            var extract = new cExtract(dal);

            //act
            var result = extract.WriteCompleteLogs(GetBanks(), "target", GetExtractParms(OutputMode.WriteToDisk));

            //assert
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void Will_HandleError_WhenGeneratingExtractCompleteAlert()
        {
            //arrange
            var dal = GetExtractEngineDAL();
            dal.WriteEventLogStringStringInt32Int32Int32 = (alertName, message, siteCode, bank, workgroup) =>
            {
                return _extractCompleteAlertGenerated = false;
            };
            var extract = new cExtract(dal);

            //act
            var result = extract.WriteCompleteLogs(GetBanks(), "target", GetExtractParms(OutputMode.WriteToDisk));

            //assert
            Assert.AreEqual(false, result);
        }

        private cExtractDef BuildTestExtractDef() {

            cExtractDef objRetVal = new cExtractDef();
            objRetVal.FieldDelim = "|";

            cLayout objLayout = new cLayout();
            objLayout.LayoutLevel = LayoutLevelEnum.Bank;
            objLayout.LayoutName = "Bank_Layout";
            objLayout.AddLayoutField(new cLayoutField(objLayout, "RecHubData.dimBanks.SiteBankID", 6, 10));
            objLayout.AddLayoutField(new cLayoutField(objLayout, "RecHubData.dimBanks.BankName", 1, 10));
            objRetVal.AddLayout(objLayout);

            objLayout = new cLayout();
            objLayout.LayoutLevel = LayoutLevelEnum.Batch;
            objLayout.LayoutName = "Batch_Layout";
            objLayout.AddLayoutField(new cLayoutField(objLayout, "RecHubData.factBatchSummary.DepositDateKey", 6, 10));
            objLayout.AddLayoutField(new cLayoutField(objLayout, "RecHubData.factTransactionSummary.SourceBatchID", 18, 10));
            objLayout.AddLayoutField(new cLayoutField(objLayout, "RecHubData.factChecks.Amount", 21, 10));
            objRetVal.AddLayout(objLayout);

            return (objRetVal);
        }

        private OrderedDictionary GetBanks()
        {
            var banks = new OrderedDictionary();
            banks.Add("1", _bank);
            return banks;
        }

        private cExtractParms GetExtractParms(OutputMode outputMode)
        {
            return new cExtractParms { OutputMode = (int)outputMode };
        }

        private StubIExtractEngineDAL GetExtractEngineDAL()
        {
            return new StubIExtractEngineDAL()
            {
                BeginTrans = () => { },
                WriteEventLogStringStringInt32Int32Int32 = (alertName, message, siteCode, bank, workgroup) =>
                {
                    return _extractCompleteAlertGenerated = true;
                },
                CommitTrans = () => { }
            };
        }

        private cExtractParms BuildTestExtractParms() {
            cExtractParms objRetVal = new cExtractParms();

            objRetVal.DepositDateFrom = new DateTime(2016, 01, 01);
            objRetVal.DepositDateTo = new DateTime(2016, 03, 10);
            objRetVal.LockboxID = new long[] { 88888888 };

            return (objRetVal);
        }

        private cBatch AddBatchToCurrentWorkgroup(DateTime depositDate, DateTime processingDate) {
            var result = new cBatch(_currentWorkgroup, 
                _bank.BankID, 
                _currentWorkgroup.ClientAccountID, 
                _currentWorkgroup.BatchCount,
                int.Parse(processingDate.ToString("yyyyMMdd")),
                "",
                "",
                0,
                int.Parse(depositDate.ToString("yyyyMMdd")),
                new IOrderByColumn[] {});
            _currentBatch = result;
            return result;
        }

        private cTransaction AddPaymentsToCurrentBatch(params decimal[] paymentAmounts) {
            var result = new cTransaction(_currentBatch, _currentBatch.TransactionCount, new IOrderByColumn[] {});

            foreach (
                var payemnt in
                    paymentAmounts.Select(
                        amount =>
                            new cPayment((ExtractConfigurationSettings) null, 
                                result, 
                                result.Payments.Count, 
                                1, 
                                amount,
                                "", 
                                "", 
                                0, 
                                new IOrderByColumn[] {}))) {
                result.Payments.Add(payemnt.TransactionID, payemnt);
            }
            _currentBatch.Transactions.Add(result.TransactionID, result);
            return result;
        }
    }
}
