﻿using System;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using Document = iTextSharp.text.Document;

namespace Test.ExtractAPI
{
    [TestClass]
    public class ImageTests
    {
        private static string outputDirectory = @"Output/ImageTests";

        [ClassInitialize]
        public static void Setup(TestContext context)
        {
            Helper.Cleanup(outputDirectory);
        }

        [TestMethod]
        public void Can_AppendImages_FromPdf()
        {
            AssertAppendImages(GetBytes(@"Resources/ACHSurrogateDocumentImage.pdf"), "ACHSurrogateDocumentImage");
        }

        [TestMethod]
        public void Can_AppendImages_FromTif()
        {
            //arrange
            AssertAppendImages(GetBytes(@"Resources/Image.tif"), "image");
        }

        private byte[] GetBytes(string imageName)
        {
            return File.ReadAllBytes(imageName);
        }

        private void AssertAppendImages(byte[] imageBytes, string outputFileName)
        {
            //arrange
            Document document = new Document();
            using (Stream stream = new FileStream($@"{outputDirectory}/{outputFileName}.pdf", FileMode.Create))
            {
                var writer = PdfWriter.GetInstance(document, stream);
                document.AddTitle("Unit Test");

                document.Open();

                //act
                DocumentDetails result = document.AppendImages(writer, new cImageType(imageBytes, new cItemKey(1, 1), false));

                document.CloseDocument();

                //assert
                Assert.IsNotNull(result);
                Assert.IsTrue(result.ScaleWidth > 0);
                Assert.IsTrue(result.ScaleHeight > 0);
            }
        }
    }
}
