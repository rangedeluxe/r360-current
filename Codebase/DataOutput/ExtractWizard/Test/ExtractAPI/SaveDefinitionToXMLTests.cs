﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;

namespace Test.ExtractAPI
{
    [TestClass]
    public class SaveDefinitionToXMLTests
    {

        public cLayoutField BuildTestLayoutField(cLayout layout, int number)
        {
            return new cLayoutField(layout, string.Format("TestField{0}", number))
            {
                ColumnWidth = 0,
                DataType = 1,
                DisplayName = string.Format("DisplayName{0}", number),
                Format = string.Format("Format{0}", number),
                IsDataEntry = true,
                Justification = WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.JustifyEnum.Left,
                ManualInputValue = string.Format("ManualInputValue{0}", number),
                PadChar = ' ',
                PaymentSource = string.Format("PaymentSource{0}", number),
                Quotes = true,
                SiteBankID = number,
                SiteClientAccountID = number
            };
        }

        [TestMethod]
        public void LayoutToXML_Should_IncludeDataEntryKeys()
        {
            // Arrange
            var layout = new cLayout();
            layout.AddLayoutField(BuildTestLayoutField(layout, 0));
            var document = new XmlDocument();
            var node = document.CreateNode(XmlNodeType.Element, "root", string.Empty);

            // Act
            cExtractDef.LayoutToXml(layout, ref node);
            var field = node.SelectSingleNode("/Layout/LayoutField");

            // Assert
            Assert.AreEqual(1, node.SelectNodes("/Layout/LayoutField").Count);
            Assert.AreEqual("TestField0", field.Attributes["FieldName"].InnerText);
            Assert.AreEqual("True", field.Attributes["IsDataEntry"].InnerText);
            Assert.AreEqual("DisplayName0", field.Attributes["DisplayName"].InnerText);
            Assert.AreEqual("0", field.Attributes["SiteBankID"].InnerText);
            Assert.AreEqual("0", field.Attributes["SiteClientAccountID"].InnerText);
            Assert.AreEqual("PaymentSource0", field.Attributes["PaymentSource"].InnerText);
        }

    }
}
