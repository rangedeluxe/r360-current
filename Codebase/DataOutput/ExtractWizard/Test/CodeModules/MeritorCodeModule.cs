﻿using System;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// This module exposes the extract file data prior to being written to the
/// extract file.  Each member of the input array represents a line of text that will
/// normally be written to the extract file.  The output of the ProcessExtract
/// function will be the actual text that will be written to the extract file.  The
/// data can be modified, appended to, or removed as required to produce the desired results.
/// The namespace, class name, and function definition can not be modified
/// or the code module will not be executed by the extract engine and an error will occur.
/// </summary>
namespace WFS.RecHub.DataOutputToolkit.Extract.CodeModule
{
    public class MeritorCodeModule
    {
        public List<string> ProcessExtract(List<string> FileRows)
        {
            List<string> arRetVal = new List<string>();
            Int32 stubCount = 1;
            String tracerNum = "000";
            var currentbatchtype = string.Empty;
            for (int i = 0; i < FileRows.Count; i++)
            {
                string rowText = FileRows[i];
                String retString = rowText;
                if (retString.Substring(0, 1) == "1")
                {
                    arRetVal.Add(retString.Trim().PadRight(80, ' '));
                }
                if (retString.Substring(0, 1) == "5")
                {
                    String addStr = retString.Substring(0, 4);

                    // Determine the achcode.
                    currentbatchtype = string.Empty;
                    for(int j = i + 1; j < FileRows.Count; j++)
                    {
                        // We want to loop until we hit a code of either '4' or '6'.
                        // But if we hit a 5, that means a new batch and we should quit.
                        if (FileRows[j].StartsWith("5"))
                        {
                            // We hit a new batch and didn't find the code.
                            break;
                        }
                        else if(FileRows[j].StartsWith("4") || FileRows[j].StartsWith("6"))
                        {
                            // We found a row with the data we need, and we can break out if
                            // it's acceptable.
                            currentbatchtype = FileRows[j].Substring(4, 3);
                            if (currentbatchtype != "CTX" && currentbatchtype != "CCD" && currentbatchtype != "IAT")
                                currentbatchtype = string.Empty;
                            else
                                break;
                        }

                        // Otherwise continue.
                    }

                    if (!string.IsNullOrWhiteSpace(currentbatchtype))
                    {
                        addStr += "ACH0000" + currentbatchtype;
                        addStr += retString.Substring(14, 40);
                    }
                    else
                    {
                        addStr += "LBX";
                        addStr += "0025501";
                        addStr += retString.Substring(14, 6);
                        String dda = retString.Substring(20, 10);
                        if (dda == "0000000000")
                            dda = "1076114196";
                        addStr += dda;
                        addStr += retString.Substring(30, 20);
                    }
                    arRetVal.Add(addStr.Trim().PadRight(80, ' '));
                }
                if (retString.Substring(0, 1) == "7")
                {
                    String addStr = retString.Substring(0, 7);

                    // Determine the achcode.
                    var achcode = currentbatchtype;

                    if (!string.IsNullOrWhiteSpace(achcode))
                    {
                        addStr += "0000" + achcode;
                        addStr += retString.Substring(17, 40);
                    }
                    else
                    {
                        addStr += retString.Substring(10, 50);
                    }
                    arRetVal.Add(addStr.Trim().PadRight(80, ' '));

                    // reset the batch type code, as this is a batch footer.
                    currentbatchtype = string.Empty;
                }
                if (retString.Substring(0, 1) == "8")
                {
                    arRetVal.Add(retString.Trim().PadRight(80, ' '));
                }
                if (retString.Substring(0, 1) == "9")
                {
                    arRetVal.Add(retString.Trim().PadRight(80, ' '));
                }
                if (retString.Substring(0, 1) == "6")
                {
                    var achcode = currentbatchtype;

                    if (achcode == "CTX" || achcode == "CCD" || achcode == "IAT")
                    {
                        tracerNum = retString.Substring(10, 3);
                        String addStr = retString.Substring(0, 1);
                        addStr += retString.Substring(1, 3);
                        addStr += retString.Substring(10, 3);
                        addStr += retString.Substring(13, 10);
                        addStr += AddRTCheckDigit(retString.Substring(23, 8));
                        addStr += retString.Substring(42, 10);
                        addStr += retString.Substring(61, 9);
                        addStr += retString.Substring(70, 6);
                        addStr += retString.Substring(91, 15);
                        arRetVal.Add(addStr.Trim().PadRight(80, ' '));
                    }
                    else
                    {
                        String addStr = retString.Substring(0, 1);
                        addStr += retString.Substring(1, 3);
                        addStr += retString.Substring(7, 3);
                        addStr += retString.Substring(13, 10);
                        addStr += retString.Substring(23, 9);
                        addStr += retString.Substring(32, 10);
                        addStr += retString.Substring(52, 9);
                        addStr += retString.Substring(70, 6);
                        addStr += retString.Substring(76, 15);
                        arRetVal.Add(addStr.Trim().PadRight(80, ' '));
                    }
                    stubCount = 1;
                }
                if (retString.Substring(0, 1) == "4")
                {
                    var achcode = currentbatchtype;

                    if (achcode == "CTX" || achcode == "CCD" || achcode == "IAT")
                    {
                        String addStr = retString.Substring(0, 1);
                        addStr += retString.Substring(1, 3);
                        addStr += tracerNum;
                        addStr += "6";
                        addStr += stubCount.ToString("00");
                        if (retString.Substring(10, 1) == "9")
                            addStr += "9";
                        else
                            addStr += "0";
                        addStr += retString.Substring(27, 16);
                        addStr += retString.Substring(53, 10);
                        addStr += retString.Substring(73, 10);
                        arRetVal.Add(addStr.Trim().PadRight(80, ' '));
                    }
                    else
                    {
                        String addStr = retString.Substring(0, 1);
                        addStr += retString.Substring(1, 3);
                        addStr += retString.Substring(7, 3);
                        addStr += "6";
                        addStr += stubCount.ToString("00");
                        if (retString.Substring(10, 1) == "9")
                            addStr += "9";
                        else
                            addStr += "0";
                        addStr += retString.Substring(11, 16);
                        addStr += retString.Substring(43, 10);
                        addStr += retString.Substring(63, 10);
                        arRetVal.Add(addStr.Trim().PadRight(80, ' '));
                    }
                    stubCount++;
                }
            }
            return (arRetVal);
        }
        public String AddRTCheckDigit(String p_rt)
        {
            String retString = "";
            Int32 product = 0;
            Int32 productTotal = 0;
            for (int i = 0; i < p_rt.Trim().Length; i++)
            {
                Int32 tempNum = Convert.ToInt32(p_rt.Substring(i, 1));
                retString += p_rt.Substring(i, 1);
                if (i == 0)
                    product = 3 * tempNum;
                else if (i == 1)
                    product = 7 * tempNum;
                else if (i == 2)
                    product = 1 * tempNum;
                else if (i == 3)
                    product = 3 * tempNum;
                else if (i == 4)
                    product = 7 * tempNum;
                else if (i == 5)
                    product = 1 * tempNum;
                else if (i == 6)
                    product = 3 * tempNum;
                else if (i == 7)
                    product = 7 * tempNum;
                Int32 moduloValue = product % 10;
                productTotal += moduloValue;
            }
            Int32 CDV = 10 - (productTotal % 10);

            // Added this line incase CDV ends up being '10', which would produce 10 digits for 
            // the output instead of 9.
            if (CDV.ToString().Length > 1)
                CDV = 0;

            retString += CDV.ToString();
            return retString;
        }
    }
}
