﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This module exposes the extract file data prior to being written to the
/// extract file.  Each member of the input array represents a line of text that will
/// normally be written to the extract file.  The output of the ProcessExtract
/// function will be the actual text that will be written to the extract file.  The
/// data can be modified, appended to, or removed as required to produce the desired results.
/// The namespace, class name, and function definition can not be modified
/// or the code module will not be executed by the extract engine and an error will occur.
/// </summary>
namespace WFS.RecHub.DataOutputToolkit.Extract.CodeModule
{
    public class APGCodeModule
    {
        public List<string> ProcessExtract(List<string> FileRows)
        {
            List<string> arRetVal = new List<string>();

            Int32 BatchID = -1;
            var BatchTypeCode = "";
            Int32 TransactionID = -1;

            Int32 PrevBatchID = -1;
            Int32 PrevTransID = -1;

            String prevACHTrace = "";
            String prevpaperAmount = "";
            String prevpaperSerial = "";
            String prevACHIndID = "";
            String prevProcessingDate = "";
            String prevACHSettleDate = "";
            String prevRemitterName = "";
            String prevACHIndName = "";
            String prevRT = "";
            String prevpaperAccount = "";
            String prevACHAccount = "";
            var prevBatchTypeCode = "";

            // Loop through each row of text of the extract file and modify
            // as necessary
            foreach (string rowText in FileRows)
            {
                String retString = rowText;

                BatchID = Convert.ToInt32(retString.Substring(1, 3));
                BatchTypeCode = retString.Substring(4, 3);
                TransactionID = Convert.ToInt32(retString.Substring(7, 6));

                String ACHTrace = retString.Substring(13, 15);
                String paperAmount = retString.Substring(28, 10);
                String paperSerial = retString.Substring(38, 10);
                String ACHIndID = retString.Substring(48, 10);
                String ProcessingDate = retString.Substring(58, 8);
                String ACHSettleDate = retString.Substring(66, 8);
                String RemitterName = retString.Substring(74, 15);
                String ACHIndName = retString.Substring(89, 15);
                String RT = retString.Substring(104, 9);
                String paperAccount = retString.Substring(113, 20);
                String ACHAccount = retString.Substring(133, 20);
                String paperInvoice = retString.Substring(153, 12);
                String ACHInvoice = retString.Substring(165, 12);
                String paperInvAmount = retString.Substring(177, 10);
                String ACHInvAmount = retString.Substring(187, 10);

                if ((BatchID != PrevBatchID) || (TransactionID != PrevTransID))
                {
                    prevACHTrace = ACHTrace;
                    prevpaperAmount = paperAmount;
                    prevpaperSerial = paperSerial;
                    prevACHIndID = ACHIndID;
                    prevProcessingDate = ProcessingDate;
                    prevACHSettleDate = ACHSettleDate;
                    prevRemitterName = RemitterName;
                    prevACHIndName = ACHIndName;
                    prevRT = RT;
                    prevpaperAccount = paperAccount;
                    prevACHAccount = ACHAccount;
                    prevBatchTypeCode = BatchTypeCode;

                    PrevBatchID = BatchID;
                    PrevTransID = TransactionID;

                    prevACHTrace = prevACHTrace.Trim().Substring(prevACHTrace.Trim().Length - 6).PadLeft(6, '0');
                }

                String addString = "";

                if (prevBatchTypeCode == "CTX" || prevBatchTypeCode == "CCD" || prevBatchTypeCode == "IAT")        // ACH Data
                {
                    addString = "2" + "\t";
                    addString += prevBatchTypeCode + "\t";
                    addString += prevACHTrace + "\t";
                    addString += prevpaperAmount + "\t";
                    addString += prevACHIndID + "\t";
                    addString += prevACHSettleDate + "\t";
                    addString += prevACHIndName + "\t";
                    addString += prevRT + "\t";
                    addString += prevACHAccount + "\t";
                    addString += ACHInvoice + "\t";
                    addString += ACHInvAmount + "\t";
                    addString += "          " + "\t";
                    addString += "          " + "\t";
                    addString += "          " + "\t";
                    addString += "          " + "\t";
                    addString += "          " + "\t";
                }
                else                            // Paper
                {
                    addString = "3" + "\t";
                    addString += BatchID.ToString().PadLeft(3, '0') + "\t";
                    addString += TransactionID.ToString().PadLeft(6, '0') + "\t";
                    addString += prevpaperAmount + "\t";
                    addString += prevpaperSerial + "\t";
                    addString += prevProcessingDate + "\t";
                    addString += prevRemitterName + "\t";
                    addString += prevRT + "\t";
                    addString += prevpaperAccount + "\t";
                    addString += paperInvoice + "\t";
                    addString += paperInvAmount + "\t";
                    addString += "          " + "\t";
                    addString += "          " + "\t";
                    addString += "          " + "\t";
                    addString += "          " + "\t";
                    addString += "          " + "\t";
                }

                arRetVal.Add(addString);
            }

            return (arRetVal);
        }
    }
}
