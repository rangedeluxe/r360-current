﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DataOutputToolkit.Extract.CodeModule;

namespace Test.CodeModules
{
    [TestClass]
    public class TestMeritorCodeModule
    {
        protected readonly MeritorCodeModule Module;

        public TestMeritorCodeModule()
        {
            Module = new MeritorCodeModule();
        }


        public List<string> GetSingleBatchMeritorACHSampleData()
        {
            return new List<string>()
            {
                "100007200009600720000961601111615                                               ",
                "5466000       16011110761141960072000096                                        ",
                "6466CTX001014000099764600000000000000000004133784645017947014000000000      000000000000000000000000000000              ",
                "4466CTX0016REF22345601     REF22345601     00000991100000991.1000000000000000110.12                                 ",
                "44660000011REF22345602     REF22345602     00000317330000317.3300000000000000035.26                                 ",
                "44660000019REF22345603     REF22345603     00004953640004953.6400000000000000550.40                                 ",
                "700000000000077032016010010000997646                                              ",
                "8000000002550116011100010000997646                                             ",
                "9000009                                                                         ",
            };
        }

        public List<string> GetSingleBatchMeritorLockboxSampleData()
        {
            return new List<string>()
            {
                "100007200009600720000961601111615                                               ",
                "5466000       16011110761141960072000096                                        ",
                "6466000001014000099764600000000000000000004133784645017947014000000000      000000000000000000000000000000              ",
                "44660000016REF22345601     REF22345601     00000991100000991.1000000000000000110.12                                 ",
                "44660000011REF22345602     REF22345602     00000317330000317.3300000000000000035.26                                 ",
                "44660000019REF22345603     REF22345603     00004953640004953.6400000000000000550.40                                 ",
                "700000000000077032016010010000997646                                              ",
                "8000000002550116011100010000997646                                             ",
                "9000009                                                                         ",
            };
        }

        public List<string> ExecuteMeritorCodeModule(List<string> input)
        {
            return Module.ProcessExtract(input);
        }

        [TestMethod]
        public void MeritorCodeModule_Should_ReplaceBatchTypeCode_ForACHBatchHeaders()
        {
            // Arrange
            var input = GetSingleBatchMeritorACHSampleData();

            // Act
            var output = ExecuteMeritorCodeModule(input);
            var batchline = output.First(x => x.StartsWith("5"));
            var achtype = batchline.Substring(7, 7);

            // Assert
            Assert.AreEqual("0000CTX", achtype);
        }

        [TestMethod]
        public void MeritorCodeModule_Should_ReplaceBatchTypeCode_ForACHBatchFooters()
        {
            // Arrange
            var input = GetSingleBatchMeritorACHSampleData();

            // Act
            var output = ExecuteMeritorCodeModule(input);
            var batchline = output.First(x => x.StartsWith("7"));
            var achtype = batchline.Substring(7, 7);

            // Assert
            Assert.AreEqual("0000CTX", achtype);
        }

        [TestMethod]
        public void MeritorCodeModule_Should_ReplaceBatchTypeCode_ForLockboxBatchHeaders()
        {
            // Arrange
            var input = GetSingleBatchMeritorLockboxSampleData();

            // Act
            var output = ExecuteMeritorCodeModule(input);
            var batchline = output.First(x => x.StartsWith("5"));
            var achtype = batchline.Substring(7, 7);

            // Assert
            Assert.AreEqual("0025501", achtype);
        }

        [TestMethod]
        public void MeritorCodeModule_Should_ReplaceBatchTypeCode_ForLockboxBatchFooters()
        {
            // Arrange
            var input = GetSingleBatchMeritorLockboxSampleData();

            // Act
            var output = ExecuteMeritorCodeModule(input);
            var batchline = output.First(x => x.StartsWith("7"));
            var achtype = batchline.Substring(7, 7);

            // Assert
            Assert.AreEqual("0007703", achtype);
        }

        [TestMethod]
        public void MeritorCodeModule_Should_IncludeDiscountAmounts_ForACHDetails()
        {
            // Arrange
            var input = GetSingleBatchMeritorACHSampleData();

            // Act
            var output = ExecuteMeritorCodeModule(input);
            var lines = output.Where(x => x.StartsWith("4"));

            // Assert
            Assert.AreEqual("0000110.12", lines.ElementAt(0).Substring(37, 10));
            Assert.AreEqual("0000035.26", lines.ElementAt(1).Substring(37, 10));
            Assert.AreEqual("0000550.40", lines.ElementAt(2).Substring(37, 10));
        }

        [TestMethod]
        public void MeritorCodeModule_Should_IncludeDiscountAmounts_ForLockboxDetails()
        {
            // Arrange
            var input = GetSingleBatchMeritorLockboxSampleData();

            // Act
            var output = ExecuteMeritorCodeModule(input);
            var lines = output.Where(x => x.StartsWith("4"));

            // Assert
            Assert.AreEqual("0000000000", lines.ElementAt(0).Substring(37, 10));
            Assert.AreEqual("0000000000", lines.ElementAt(1).Substring(37, 10));
            Assert.AreEqual("0000000000", lines.ElementAt(2).Substring(37, 10));
        }

        [TestMethod]
        public void MeritorCodeModule_Should_IncludeTraceNumber_ForACHDetails()
        {
            // Arrange
            var input = GetSingleBatchMeritorACHSampleData();

            // Act
            var output = ExecuteMeritorCodeModule(input);
            var lines = output.Where(x => x.StartsWith("4"));

            // Assert
            Assert.AreEqual("014", lines.ElementAt(0).Substring(4, 3));
            Assert.AreEqual("014", lines.ElementAt(1).Substring(4, 3));
            Assert.AreEqual("014", lines.ElementAt(2).Substring(4, 3));
        }

        [TestMethod]
        public void MeritorCodeModule_Should_IncludeTransactionNumber_ForLockboxDetails()
        {
            // Arrange
            var input = GetSingleBatchMeritorLockboxSampleData();

            // Act
            var output = ExecuteMeritorCodeModule(input);
            var lines = output.Where(x => x.StartsWith("4"));

            // Assert
            Assert.AreEqual("001", lines.ElementAt(0).Substring(4, 3));
            Assert.AreEqual("001", lines.ElementAt(1).Substring(4, 3));
            Assert.AreEqual("001", lines.ElementAt(2).Substring(4, 3));
        }

        [TestMethod]
        public void MeritorCodeModule_Should_Calculate_RoutingTransitCheckDigit()
        {
            // Arrange
            var input = "07200009";

            // Act
            var output = new MeritorCodeModule().AddRTCheckDigit(input);

            // Assert
            Assert.AreEqual("072000096", output);
        }

    }
}
