﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DataOutputToolkit.Extract.CodeModule;

namespace Test.CodeModules
{
    [TestClass]
    public class TestAPGCodeModule
    {
        protected readonly APGCodeModule Module;

        public TestAPGCodeModule()
        {
            Module = new APGCodeModule();
        }

        public List<string> GetTestData()
        {
            return new List<string>()
            {
                "35150000000010000000000000000000025180691438990000000000000111201600000000           CMA1               000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "35150000000020000000000000000000039209691438990100000000000111201600000000           CMA1               000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "35150000000030000000000000000000061480691438990200000000000111201600000000           CMA1               000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "35150000000040000000000000000000200944691438990300000000000111201600000000           CMA1               000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "35150000000050000000000000000001101143691438990400000000000111201600000000           CMA1               000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "35150000000060000000000000000000491198691438990500000000000111201600000000           CMA1               000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "35150000000070000000000000000001990050691438990600000000000111201600000000           CMA1               000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "35150000000080000000000000000009052933691438990700000000000111201600000000           CMA1               000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
                "3466CTX0000010910000179470140000997646001794701400000000000111201601112016                              00000000000000000000000000000000000000000000000000REF223456010REF2234560100000991100000626207",
                "34660000000010000000000000000000000000000000000000000000000111201600000000                              00000000000000000000000000000000000000000000000000REF223456020REF2234560200000317330000626207",
                "34660000000010000000000000000000000000000000000000000000000111201600000000                              00000000000000000000000000000000000000000000000000REF223456030REF2234560300004953640000626207",
                "35090000000010000000000000000000997646001794801400000000000111201600000000                              000000000000000000000000000000000000000000000000000000000000000000000000000000991100000000000",
                "35090000000010000000000000000000000000000000000000000000000111201600000000                              000000000000000000000000000000000000000000000000000000000000000000000000000000317330000000000",
                "35090000000010000000000000000000000000000000000000000000000111201600000000                              000000000000000000000000000000000000000000000000000000000000000000000000000004953640000000000",
            };
        }

        public List<string> ExecuteAPGCodeModule(List<string> input)
        {
            return Module.ProcessExtract(input);
        }

        [TestMethod]
        public void APGCodeModule_Should_ReplaceRecordTypeCode()
        {
            // Arrange
            var input = GetTestData();

            // Act
            var output = ExecuteAPGCodeModule(input);
            var line = output[8];

            // Assert
            Assert.AreEqual('2', line[0]);
        }

        [TestMethod]
        public void APGCodeModule_Should_PopulateBatchTypeCode()
        {
            // Arrange
            var input = GetTestData();

            // Act
            var output = ExecuteAPGCodeModule(input);
            var lines = output.Skip(8).Take(3);

            // Assert 
            Assert.AreEqual("CTX", lines.ElementAt(0).Substring(2, 3));
            Assert.AreEqual("CTX", lines.ElementAt(1).Substring(2, 3));
            Assert.AreEqual("CTX", lines.ElementAt(2).Substring(2, 3));
        }

        [TestMethod]
        public void APGCodeModule_ShouldNot_PopulateBatchTypeCode_ForPaperData()
        {
            // Arrange
            var input = GetTestData();

            // Act
            var output = ExecuteAPGCodeModule(input);
            var lines = output.Skip(11).Take(3);

            // Assert 
            Assert.AreEqual("509", lines.ElementAt(0).Substring(2, 3));
            Assert.AreEqual("509", lines.ElementAt(1).Substring(2, 3));
            Assert.AreEqual("509", lines.ElementAt(2).Substring(2, 3));
        }
    }
}
