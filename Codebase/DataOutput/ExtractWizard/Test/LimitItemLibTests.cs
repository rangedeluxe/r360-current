﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Test.ExtractLib
{
    [TestClass()]
    public class LimitItemLibTests
    {
        [TestMethod()]
        public void WillConvertEqualsLogicalOperatorToString()
        {
            //arrange
            var expected = "=";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.Equals);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertNotEqualsLogicalOperatorToString()
        {
            //arrange
            var expected = "<>";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.NotEqualTo);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertGreaterThanLogicalOperatorToString()
        {
            //arrange
            var expected = ">";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.GreaterThan);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertGreaterThanOrEqualToLogicalOperatorToString()
        {
            //arrange
            var expected = ">=";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.GreaterThanOrEqualTo);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertLessThanLogicalOperatorToString()
        {
            //arrange
            var expected = "<";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.LessThan);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertLessThanOrEqualToLogicalOperatorToString()
        {
            //arrange
            var expected = "<=";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.LessThanOrEqualTo);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertBetweenLogicalOperatorToString()
        {
            //arrange
            var expected = " between";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.Between);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertInLogicalOperatorToString()
        {
            //arrange
            var expected = " in";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.In);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertIsNullLogicalOperatorToString()
        {
            //arrange
            var expected = " IS NULL ";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.IsNull);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertIsNotNullLogicalOperatorToString()
        {
            //arrange
            var expected = " IS NOT NULL ";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.IsNotNull);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void WillConvertLikeLogicalOperatorToString()
        {
            //arrange
            var expected = " Like ";

            //act
            var actual = LimitItemLib.FormatLogicalOperator(LogicalOperatorEnum.Like);

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}