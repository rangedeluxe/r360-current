﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractRun;

namespace Test.ExtractRun
{
    [TestClass]
    public class ExtensionTests
    {
        [TestMethod]
        public void Will_ApplyDefaultLimits_WhenParmsNotProvided()
        {
            //arrange
            var expectedBank = "123";
            var expectedWorkgroup = "456";
            var expectedBatch = "789";
            var expectedProcessingDate = new DateTime(2018, 12, 31);
            var expectedDepositDate = new DateTime(2018, 12, 31);
            var definition = new cExtractDef();
            definition.AddLimitItem(LayoutLevelEnum.Bank,
                new cLimitItem { FieldName = "SiteBankID", Value = expectedBank });
            definition.AddLimitItem(LayoutLevelEnum.ClientAccount,
                new cLimitItem { FieldName = "SiteClientAccountID", Value = expectedWorkgroup });
            definition.AddLimitItem(LayoutLevelEnum.Batch,
                new cLimitItem { FieldName = "SourceBatchID", Value = expectedBatch });
            definition.AddLimitItem(LayoutLevelEnum.Batch,
                new cLimitItem { FieldName = "ImmutableDateKey", Value = expectedProcessingDate.ToString() });
            definition.AddLimitItem(LayoutLevelEnum.Batch,
                new cLimitItem { FieldName = "DepositDateKey", Value = expectedDepositDate.ToString() });

            var parms = new cExtractParms();

            //act
            var actual = parms.ApplyDefaults(definition);

            //assert
            Assert.AreEqual(expectedBank, actual.BankIDString);
            Assert.AreEqual(expectedWorkgroup, actual.LockboxIDString);
            Assert.AreEqual(expectedBatch, actual.BatchIDString);
            Assert.AreEqual(expectedProcessingDate, actual.ProcessingDateFrom);
            Assert.AreEqual(expectedProcessingDate, actual.ProcessingDateTo);
            Assert.AreEqual(expectedDepositDate, actual.DepositDateFrom);
            Assert.AreEqual(expectedDepositDate, actual.DepositDateTo);
        }

        [TestMethod]
        public void Will_UseDefaultParms_WhenLimitsNotProvided()
        {
            //arrange
            var expectedBank = new long[] { 123 };
            var expectedWorkgroup = new long[] { 456 };
            var expectedBatch = new long[] { 789 };
            var expectedProcessingDate = new DateTime(2018, 12, 31);
            var expectedDepositDate = new DateTime(2018, 12, 31);

            var definition = new cExtractDef();
            var parms = new cExtractParms();
            parms.BankID = expectedBank;
            parms.LockboxID = expectedWorkgroup;
            parms.BatchID = expectedBatch;
            parms.DepositDateFrom = expectedDepositDate;
            parms.DepositDateTo = expectedDepositDate;
            parms.ProcessingDateFrom = expectedProcessingDate;
            parms.ProcessingDateTo = expectedProcessingDate;

            //act
            var actual = parms.ApplyDefaults(definition);

            //assert
            Assert.AreEqual(expectedBank, actual.BankID);
            Assert.AreEqual(expectedWorkgroup, actual.LockboxID);
            Assert.AreEqual(expectedBatch, actual.BatchID);
            Assert.AreEqual(expectedProcessingDate, actual.ProcessingDateFrom);
            Assert.AreEqual(expectedProcessingDate, actual.ProcessingDateTo);
            Assert.AreEqual(expectedDepositDate, actual.DepositDateFrom);
            Assert.AreEqual(expectedDepositDate, actual.DepositDateTo);
        }
    }
}
