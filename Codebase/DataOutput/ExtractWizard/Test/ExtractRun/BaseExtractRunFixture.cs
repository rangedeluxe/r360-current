﻿using System;
using System.Collections.Specialized;
using System.IO;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 2/11/2014
*
* Purpose: 
*
* Modification History
* WI 129313 & WI 129315 BLR 2/11/2014
*   -Initial Version 
* WI 129313 & WI 129315 BLR 2/24/2014
*   -Edited to not crash on file not found, so we can force-test invalid 
*   definitions and settings.
******************************************************************************/

namespace Test.ExtractRun
{
    /// <summary>
    /// Small base class which contains a helper method for running the ExtractRun
    /// project. Test fixtures may then compare the generated output to the expected
    /// output.
    /// </summary>
    public abstract class BaseExtractRunFixture
    {
        protected const string LOG_FILE = "test.log";
        protected const string OUTPUT_FILE = "out.cxr";
        protected const string IMAGE_DIR = "images";

        /// <summary>
        /// Runs the ExtractRun, returns the result as a string.
        /// Recommended processing dates:
        ///   12/02/2003
        ///   02/14/2006
        ///   
        /// </summary>
        /// <param name="inputfile">File location of the input XML.</param>
        /// <param name="processingdate">Processing date for the extract run.</param>
        /// <returns>The full output of the run.</returns>
        protected string RunExtract(string inputfile, string processingdate)
        {
            Guid auditguid;
            StringDictionary stats;
            var error = string.Empty;

            // Make sure we're using a fresh file to eliminate the possibilities of false-passing tests.
            if (File.Exists(OUTPUT_FILE))
                File.Delete(OUTPUT_FILE);

            var runner = new cExtract();
            var def = new cExtractDef();
            def.ParseExtractDefXmlFile(inputfile);

            // Gather processing date info.
            DateTime datefrom;
            DateTime dateto;
            cExtractParms.ParseDateString(processingdate, out datefrom, out dateto);
            var runparams = new cExtractParms()
            {
                ProcessingDateFrom = datefrom,
                ProcessingDateTo = dateto,
                TargetFile = OUTPUT_FILE,
                //TraceField = string.IsNullOrWhiteSpace(def.TimeStamp)
                //    ? datefrom.ToString()
                //    : def.TimeStamp,
                UserID = cAppSettingsLib.Get<int>("RunAsUserId", 1),
                TraceField = def.TimeStamp,
                ImagePath = IMAGE_DIR,
                LogFilePath = LOG_FILE
            };

            // Run & gather results. Return null if errors occurred.
            var success = runner.RunExtract(def, runparams, out auditguid, out stats);
            if (success != 1)
                return null;

            if (File.Exists(OUTPUT_FILE))
                return File.ReadAllText(OUTPUT_FILE);
            return null;
        }
    }
}