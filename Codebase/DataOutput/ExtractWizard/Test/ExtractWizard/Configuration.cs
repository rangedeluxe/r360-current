﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 08/12/2014
*
* Purpose:
*
* Modification History
* WI 152321 BLR 08/15/2014
*   -Initial
******************************************************************************/

namespace Test.ExtractDesignManager
{
    [TestClass]
    public class Configuration
    {
        ExtractConfigurationSettings _settings;

        public Configuration()
        {

        }

        private MockConfigurationProvider BuildProvider()
        {
            // Set up a mocked config provider with valid data.
            var provider = new MockConfigurationProvider();
            provider.SetSetting("ConnectionString", "TestConnectionString");
            provider.SetSetting("SetupPath", "");
            provider.SetSetting("LogFilePath", @"C:\{0:yyyyMMdd}_ExtractWizardLog.txt");
            provider.SetSetting("LogFileMaxSize", "1024");
            provider.SetSetting("DefaultDefinitionFileFormat", "xml");
            provider.SetSetting("CreateZeroLengthFile", "true");
            provider.SetSetting("LogoPath", "");
            provider.SetSetting("BackgroundColor", "0");
            provider.SetSetting("RunAsUserId", "1");
            provider.SetSetting("ExceptionLogFile", @"C:\{0:yyyyMMdd}_ExtractWizardExceptionLog.txt");
            provider.SetSetting("LoggingDepth", "2");
            provider.SetSetting("SiteKey", "IPOnline");
            provider.SetSetting("ApplicationURI", "IPOnline");
            provider.SetSetting("LoginAttempts", "5");

            return provider;
        }

        private void AssertException()
        {
            try
            {
                _settings.ValidateSettings();
                Assert.Fail();
            }
            catch (InvalidConfigurationException)
            {
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }

        private void AssertPass()
        {
            try
            {
                _settings.ValidateSettings();
            }
            catch (InvalidConfigurationException)
            {
                Assert.Fail();
            }
            catch (Exception)
            {
                Assert.Fail();
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestValidConfigurationData()
        {
            // Arrange
            _settings = new ExtractConfigurationSettings(BuildProvider());

            // Assert
            AssertPass();
        }

        [TestMethod]
        public void TestMissingConnectionString()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("ConnectionString", "");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestMissingLogFilePath()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("LogFilePath", "");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestMissingLogFileMaxSize()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("LogFileMaxSize", "");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestInvalidLogFileMaxSize()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("LogFileMaxSize", "-1");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestInvalidLogFileMaxSizeGarbage()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("LogFileMaxSize", "asdf");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestMissingDefinitionFileFormat()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("DefaultDefinitionFileFormat", "");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestInvalidDefinitionFileFormat()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("DefaultDefinitionFileFormat", "notxml");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestValidUpperCaseDefinitionFileFormat()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("DefaultDefinitionFileFormat", "XML");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertPass();
        }

        [TestMethod]
        public void TestMissingZeroLengthFile()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("CreateZeroLengthFile", "");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestInvalidZeroLengthFile()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("CreateZeroLengthFile", "1");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestValidZeroLengthFile()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("CreateZeroLengthFile", "false");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertPass();
        }

        [TestMethod]
        public void TestInvalidLogoPath()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("LogoPath", "not a path");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestMissingBackgroundColor()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("BackgroundColor", "");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestMissingLoggingDepth()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("LoggingDepth", "");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestInvalidLoggingDepth()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("LoggingDepth", "3");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestInvalidLoggingDepthGarbage()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("LoggingDepth", "asdf");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }

        [TestMethod]
        public void TestMissingSiteKey()
        {
            // Arrange
            var provider = BuildProvider();
            provider.SetSetting("SiteKey", "");
            _settings = new ExtractConfigurationSettings(provider);

            // Assert
            AssertException();
        }
    }
}
