﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager.TabSorting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 
*
* Purpose: 
*
* Modification History
* WI 132205 & WI 129315 BLR 03/17/2014
*   - Initial Version 
******************************************************************************/

namespace Test.ExtractDesignManager
{
    [TestClass]
    public class TabSorting
    {

        /// <summary>
        /// Standard test with 1 form and 3 buttons.
        /// </summary>
        [TestMethod]
        public void StandardTabSorting()
        {
            // Arrange
            var form = new Form();
            var button1 = new Button() { Top = 10, Left = 1, TabIndex = 1 };
            var button2 = new Button() { Top = 10, Left = 50, TabIndex = 1 };
            var button3 = new Button() { Top = 50, Left = 1, TabIndex = 1 };

            form.Controls.AddRange(new Control[]
            {
                button1, button2, button3
            });

            // Act
            var sorter = new TabSorter();
            sorter.SortTabs(form);

            // Assert
            Assert.AreEqual(0, button1.TabIndex);
            Assert.AreEqual(1, button2.TabIndex);
            Assert.AreEqual(2, button3.TabIndex);
        }

        /// <summary>
        /// Test with 1 form, 2 panels (horizontal side-by-side), and 3 buttons each.
        /// </summary>
        [TestMethod]
        public void ChildrenRecursiveSorting()
        {
            // Arrange
            var form = new Form();
            
            var button1 = new Button() { Top = 10, Left = 1, TabIndex = 1 };
            var button2 = new Button() { Top = 10, Left = 50, TabIndex = 1 };
            var button3 = new Button() { Top = 50, Left = 1, TabIndex = 1 };
            var container1 = new Panel() { Top = 1, Left = 1 };
            container1.Controls.AddRange(new Control[] { button1, button2, button3 });

            var button4 = new Button() { Top = 10, Left = 1, TabIndex = 1 };
            var button5 = new Button() { Top = 10, Left = 50, TabIndex = 1 };
            var button6 = new Button() { Top = 50, Left = 1, TabIndex = 1 };
            var container2 = new Panel() { Top = 1, Left = 200 };
            container2.Controls.AddRange(new Control[] { button4, button5, button6 });

            form.Controls.AddRange(new Control[] { container1, container2 });

            // Act
            var sorter = new TabSorter();
            sorter.SortTabs(form);

            // Assert
            Assert.AreEqual(0, button1.TabIndex);
            Assert.AreEqual(1, button2.TabIndex);
            Assert.AreEqual(2, button3.TabIndex);
            Assert.AreEqual(0, button4.TabIndex);
            Assert.AreEqual(1, button5.TabIndex);
            Assert.AreEqual(2, button6.TabIndex);
        }
    }
}