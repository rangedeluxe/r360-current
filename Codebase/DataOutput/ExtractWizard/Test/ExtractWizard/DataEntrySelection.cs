﻿using Microsoft.QualityTools.Testing.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI.Fakes;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractDesignManager;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;

namespace Test.ExtractDesignManager
{
    [TestClass]
    public class DataEntrySelection
    {

        List<cColumn> DefaultColumns
        {
            get
            {
                return new List<cColumn>
                {
                    new cColumn("Data", "Payments", "Amount", "Amount", 0, System.Data.SqlDbType.Int, 0)
                    {
                        BankName = "Bank1",
                        SiteBankID = 1,
                        WorkgroupName = "Workgroup1",
                        DisplayName = "DisplayName1"
                    },
                    new cColumn("Data", "Payments", "Account", "Account", 0, System.Data.SqlDbType.Int, 0)
                    {
                        BankName = "Bank2",
                        SiteBankID = 2,
                        WorkgroupName = "Workgroup2",
                        DisplayName = "DisplayName2"
                    },
                };
            }
        }

        public void LoadShimsAndDo(Action<DataEntryForm> method)
        {
            using (ShimsContext.Create())
            {
                ShimcExtractWizardBLL.AllInstances.GetAllDataEntryColumnsDataEntryType = (api, type) =>
                {
                    return DefaultColumns;
                };

                using (var form = new DataEntryForm(
                    new ExtractConfigurationSettings(new MockConfigurationProvider()), DataEntryType.PaymentsDataEntry))
                {
                    method(form);
                }
            }
        }

        [TestMethod]
        public void DataEntryForm_Should_LoadProperly()
        {
            LoadShimsAndDo((form) => { });
        }

        [TestMethod]
        public void DataEntryForm_Should_SortBy_UILabel_Ascending()
        {
            LoadShimsAndDo((form) =>
            {
                // Act : Sort by UILabel
                form.SortGrid(0);

                // Assert
                Assert.AreEqual("DisplayName1", form.GridColumns.First().UILabel);
            });
        }

        [TestMethod]
        public void DataEntryForm_Should_SortBy_UILabel_Descending()
        {
            LoadShimsAndDo((form) =>
            {
                // Arrange : Sort by UILabel ascending
                form.SortGrid(0);

                // Act : Sort by UILabel descending
                form.SortGrid(0);

                // Assert
                Assert.AreEqual("DisplayName2", form.GridColumns.First().UILabel);
            });
        }


    }
}
