using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Text;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.DAL.ExtractEngine;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI.Extensions;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI.Models;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/21/2013
*   -Initial Version 
* WI 114312 MLH 09/18/2013
*   -Extract Wizard Workaround - ltaLog TargetSite null exception   
* WI 129559 DLD 02/18/2014
*   -Added checks to ensure value being returned was a valid image path. 
*    If not, value was set to String.Empty, captured and ignored later.   
* WI 129395�- DLD 20140218
*   - Cleaned-up some of the code in PopulatePayments and PopulateStubs.
*     Replacing "Checks" with "Payments" to follwing current data entity nomenclature.
* WI 130165 DLD 02/19/2014
*   -Expanded Exception handling and message logging. 
* WI 130364 DLD 02/20/2014
*   -Verify database connectivity providing 'friendly' messages to user upon failure.
* WI 129719 DLD 02/24/2014
*   -Converting image repository from ipoPics.dll to OLFServices 
*   -Moved cExtractImageFileDef into its own class   
* WI 130993 BLR 02/28/2014
*   -Migrated from XCeed Zip tools to standard .NET zip tools.
* WI 130807 DLD 02/28/2014
*   -Encrypt/Decrypt UserID in database connection string 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   -Added additional exception logging.
*   -Removed the ltaLog objects being created by the class and diverted their calls to ExtractLib.cCommonLogLig.
*   -Added in joined fields for the batch summaries. 
* WI 130809 BLR 03/06/2014
*   -Updated the summary dialog with the correct information. 
* WI 131640 BLR 03/06/2014
*   -Removed the hard-coded connection string parsing. Was not at all helpful. 
* WI 132130 DLD 03/07/2014
*   -Removed WorkstationID setting 
* WI 130642 DLD 03/07/2014
*   -Validating Extract Def file paths. 
* WI 132206 DLD 03/10/2014
*   -Changed image service call. Added call to close the service after RunExtract()
* WI 115940 BLR 03/13/2014 
*   -Removed DataEntry func. 
* WI 134896 BLR 04/14/2014 
*   -Added functions for calling Alert Events.
* WI 143034 BLR 05/22/2014
*   - Added reference to the cColumnReplacementLib. 
* WI 143040 BLR 05/22/2014
*   - Removed dependence on the composite cBatchKey, only BatchID is utilized.
* WI 143506 BLR 06/02/2014
*   -Altered PopulateDepositDates to accept both ProcessingDate as
*    well as DepositDate.
* WI 146953 BLR 06/12/2014
*   -ExtractComplete eventlogs are now back to the workgroup-level.
* WI 146528 BLR 06/12/2014
*   -Added logs for ExtractDefinition log file. 
* WI 155230 BLR 07/22/2014
*   -Changed a quick status update from 'Lockbox' to 'Workgroup'. 
* WI 152321 BLR 08/15/2014
*   -Moved Configuration over to ExtractConfigurationSettings. 
* WI 162674 BLR 09/04/2014
*   -Added another call to the processing exception event logs in the big catch() block. 
* WI 269611 JMC 03/10/2016
*   -Modified internal DAL reference to use interface in order to enable Mocking
* WI 269414 JMC 03/10/2016
*   - Added new constructor to be used for unit tests.
*   - Modified WriteExtractAuditData to populate one row for each Workgroup being Extracted
*       -If Zero Banks and Zero workgroups are extracted, populate BankID=-1 and WorkgroupID=-1
*       -If One Bank and Zero workgroups are extracted, populate BankID=<BankID> and WorkgroupID=-1
*       -If Many Banks and Zero workgroups are extracted, populate BankID=<First BankID> and WorkgroupID=-1
*       -If One or Many Banks and at least workgroup is extracted, populate BankID=<First BankID> and 
*        WorkgroupID=<WorkgroupID> for each Workgroup that has been extracted
*		    -In this case break Stub and Check Totals and counts down by row
*           -Total Records will always be duplicated for the total # of rows on the extract on each row.
*            This behavior was approved by the Product Owner 
* WI 269837 JMC 03/10/2016
*   - Now saving Extract file path without the path when writing to factExtractAudits
* WI 269406 JMC 03/10/2016
*   - Only write Workgroup-level audits when at least one workgroup element has been 
*     written to the Extract Data File
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    [System.Runtime.InteropServices.Guid("62764EE7-02BC-4A82-BE4C-545AD8D3BB57")]
    public class cExtract : IDisposable
    {
        private const string XCLIENTPLACEHOLDER = "[SharedLocation]";

        private ExtractConfigurationSettings _Settings = null;
        private IExtractEngineDAL _ExtractEngineDAL = null;

        private string _LastErrorMessage = string.Empty;

        private Dictionary<string, IColumn> _BankTableDef = null;
        private Dictionary<string, IColumn> _ClientAccountTableDef = null;
        private Dictionary<string, IColumn> _BatchTableDef = null;
        private Dictionary<string, IColumn> _TransactionsTableDef = null;
        private Dictionary<string, IColumn> _PaymentsTableDef = null;
        private Dictionary<string, IColumn> _ExtractTracesTableDef = null;
        private Dictionary<string, IColumn> _ChecksDETableDef = null;
        private Dictionary<string, IColumn> _StubsTableDef = null;
        private Dictionary<string, IColumn> _StubsDETableDef = null;
        private Dictionary<string, IColumn> _DocumentsTableDef = null;

        private int _UserID = -1;


        /// <summary>
        /// Event handler to output informational text.
        /// </summary>
        public event outputMessageEventHandler OutputMessage;

        /// <summary>
        /// EventHandler to return exception information.
        /// </summary>
        public event outputErrorEventHandler OutputError;

        /// <summary>
        /// EventHandler to output status messages.
        /// </summary>
        public event OutputStatusMsgEventHandler OutputStatusMsg;

        public event ImageStatusInitializedEventHandler ImageStatusInitialized;
        public event ImageStatusChangedEventHandler ImageStatusChanged;

        public event PromptForManualInputEventHandler PromptForManualInput;

        /// <summary>
        /// 
        /// </summary>
        public cExtract()
        {
        }

        public cExtract(IExtractEngineDAL extractEngineDAL)
        {
            _ExtractEngineDAL = extractEngineDAL;
        }

        internal ExtractConfigurationSettings Settings
        {
            get
            {
                if (_Settings == null)
                {
                    _Settings = new ExtractConfigurationSettings(new ConfigurationManagerProvider());
                    _Settings.ValidateSettings();
                    _Settings.OutputError += OnOutputError;
                    _Settings.OutputMessage += OnOutputMessage;
                }
                return (_Settings);
            }
        }

        public IExtractEngineDAL ExtractEngineDAL
        {
            get
            {
                if (_ExtractEngineDAL == null)
                {
                    // DLD - WI 130807
                    var connString = CommonLib.DecryptUserIdInConnectionString(Settings.ConnectionString);
                    // WI 115356 - Derek - Moved connection string into app.config. 
                    _ExtractEngineDAL = new cExtractEngineDAL(WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Constants.INI_SECTION_EXTRACTWIZARD, connString);
                    // DLD - WI 130364 - Verify database connectivity providing 'friendly' messages to user upon failure.
                    if (_ExtractEngineDAL != null)
                    {
                        _ExtractEngineDAL.OutputError += OnOutputError;
                        _ExtractEngineDAL.OutputMessage += OnOutputMessage;
                        var connected = _ExtractEngineDAL.TestConnection();
                        if (!connected)
                        {
                            OnOutputMessage(ExtractLib.Constants.ERROR_MSG_NO_DATABASE_CONNECTION, this.ToString(), MessageType.Error, MessageImportance.Essential);
                            _ExtractEngineDAL = null;
                        }
                    }
                }
                return (_ExtractEngineDAL);
            }
        }

        public cExtractRunInfo[] GetBatchRunHistory(string columnName)
        {

            DataTable dt;
            ArrayList arRetVal = new ArrayList();
            cExtractRunInfo extractruninfo;

            if (columnName.Length > 0)
            {
                if (ExtractEngineDAL.GetPreviousRuns(columnName, out dt))
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        extractruninfo = new cExtractRunInfo();
                        if (extractruninfo.LoadDataRow(dr))
                        {
                            arRetVal.Add(extractruninfo);
                        }
                    }

                    dt.Dispose();
                }
            }

            return ((cExtractRunInfo[])arRetVal.ToArray(typeof(cExtractRunInfo)));
        }

        /// <summary>
        /// 
        /// </summary>
		/// <summary>
        /// 
        /// </summary>
        /// <param name="SetupFile"></param>
        /// <param name="Record"></param>
        /// <returns></returns>
        public int TestExtract(
            string setupFile,
            string record)
        {

            OnOutputError("Function : TestExtract() not implemented.", "cExtract.TestExtract()");
            return (-1);
        }

        /// <summary>
        /// Data this will have to extract:
        ///     General System Data
        ///     Bank Data
        ///     Customer Data
        ///     Lockbox Data
        ///     Batch data
        ///     Transaction Data
        ///     Check Data
        ///     Stub Data
        ///     Document Data
        /// </summary>
        /// <param name="ExtractDef"></param>
        /// <returns></returns>
        public int RunExtract(
            cExtractDef extractDef,
            cExtractParms parms,
            out Guid extractAuditID,
            out StringDictionary extractStats)
        {

            bool bolRetVal = false;
            bool bolSuccess;
            OrderedDictionary banks = null;
            cStandardFields StandardFields;
            Guid gidExtractAuditID = Guid.Empty;
            int intTotalRecords = 0;
            cBatchKey[] arUniqueBatchKeys = new cBatchKey[] { };
            List<string> arFileLines = null;
            string strTempFileName = string.Empty; ;
            cImageFormatOptions objImageFormatOptions = null;
            string strFormattedTargetFileName = string.Empty;
            string strFormattedZipFileName = string.Empty;
            string strFormattedLogFileName = string.Empty;
            string strMsg;
            List<string> arExtractImages = null;
            cCounters objCounters = null;
            StringDictionary arExtractStats = null;

            decimal decSumPaymentsAmount = 0;
            decimal decSumStubsAmount = 0;

            try
            {

                if (extractDef.IsValidExtract(out strMsg))
                {

                    _UserID = parms.UserID;

                    // If the target file is not specified, use the default from the ExtractDef
                    // files.
                    if (parms.TargetFile == string.Empty || parms.TargetFile.Length == 0)
                    {
                        parms.TargetFile = extractDef.ExtractPath;
                    }

                    //TODO: Replace with mockable function
                    cCommonLogLib.LogMessageLTA("Extract - " + parms.SetupFile + " started", extractDef.LogFilePath, LTAMessageImportance.Essential);

                    bolSuccess = GetStandardFields(out StandardFields);

                }
                else
                {
                    StandardFields = null;
                    OnOutputMessage(strMsg, this.ToString(), MessageType.Warning, MessageImportance.Essential);
                    bolSuccess = false;
                }

                // Format Target File Name.
                if (bolSuccess)
                {
                    bolSuccess = FormatFilePath(parms.TargetFile, parms, StandardFields, out strFormattedTargetFileName);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA(
                            string.Format("Target File name '{0}' cannot be formatted.", parms.TargetFile),
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA($"Target File name '{parms.TargetFile}' formatted.",
                            extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }

                // Zip Path
                if (bolSuccess && extractDef.ZipOutputFiles)
                {
                    bolSuccess = FormatFilePath(extractDef.ZipFilePath, parms, StandardFields, out strFormattedZipFileName);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA(
                            string.Format("Zip File name '{0}' cannot be formatted.", extractDef.ZipFilePath),
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA($"Zip File name '{extractDef.ZipFilePath}' formatted.",extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }

                // Log File Path
                if (bolSuccess)
                {
                    bolSuccess = FormatFilePath(extractDef.LogFilePath, parms, StandardFields, out strFormattedLogFileName);
                    if (bolSuccess)
                    {
                        extractDef.LogFilePath = strFormattedLogFileName;
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA(string.Format("Target Log File name '{0}' cannot be formatted.", parms.LogFilePath),
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                    }
                }

                // Format Target Image File Path.
                if (bolSuccess)
                {
                    bolSuccess = GetImageFormatOptions(extractDef, parms, out objImageFormatOptions);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA("Definition has Invalid Image Formatting Options.",
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA("Format Target Image File Path completed.",
                            extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }

                // Get Manual Input Values.
                if (bolSuccess)
                {
                    bolSuccess = GetManualInputValues(extractDef);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA("Definition has Invalid Manual Input Values.",
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA("Definition has Valid Manual Input Values.",
                            extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }

                // Build Data Objects.
                if (bolSuccess)
                {
                    OnOutputStatusMsg("Building data objects.");

                    bolSuccess = PopulateDataObjects(
                        extractDef,
                        parms,
                        out banks);

                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA("Failed to populate data objects.",
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                        OnOutputError("Failed to populate data objects.", "cExtract.RunExtract");
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA("Populate data objects.",extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }

                // Sort Data Objects.
                if (bolSuccess)
                {
                    OnOutputStatusMsg("Sorting data objects.");
                    bolSuccess = SortDataObjects(ref banks);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA("Failed to sort data objects.",
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                        OnOutputError("Failed to sort data objects.", "cExtract.RunExtract");
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA("Sorting data objects completed.",
                            extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }


                // Assign Image File Names.
                if (bolSuccess && extractDef.ImageFileFormat != ImageFileFormatEnum.DoNotRetrieveImages)
                {
                    OnOutputStatusMsg("Assigning image file names.");
                    bolSuccess = AssignImageFileNames(banks);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA("Failed to Assign image file names.",
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                        OnOutputError("Failed to assign image file names.", "cExtract.RunExtract");
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA("Assigned image file names.",
                            extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }


                // Write Extract to ArrayList.
                if (bolSuccess)
                {
                    OnOutputStatusMsg("Building extract text[" + parms.TargetFile + "].");
                    bolSuccess = BuildExtractText(banks,
                                                 extractDef,
                                                 parms,
                                                 StandardFields,
                                                 objImageFormatOptions,
                                                 out intTotalRecords,
                                                 out arUniqueBatchKeys,
                                                 out arFileLines,
                                                 out objCounters);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA("Failed to build extract text.",
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                        OnOutputError("Failed to build extract text.", "cExtract.RunExtract");
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA("Build extract text completed.",
                            extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }

                // Write Extract to temp file.
                if (bolSuccess)
                {
                    OnOutputStatusMsg("Writing extract file[" + parms.TargetFile + "].");
                    bolSuccess = WriteExtractFile(extractDef,
                                                  arFileLines,
                                                  out strTempFileName);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA("Failed to write extract file.",
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                        OnOutputError("Failed to write extract file.", "cExtract.RunExtract");
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA("Write extract file to temp file completed.",
                            extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }


                // Write Image Files (As Pdf or Tiff)
                if (bolSuccess)
                {
                    if (extractDef.ImageFileFormat != ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        OnOutputStatusMsg("Writing image files.");
                        bolSuccess = WriteExtractImages(banks, objImageFormatOptions, out arExtractImages);
                        if (!bolSuccess)
                        {
                            cCommonLogLib.LogMessageLTA("Failed to write image files.",
                                extractDef.LogFilePath, LTAMessageImportance.Essential);
                            OnOutputError("Failed to write image files", "cExtract.RunExtract");
                        }
                        else
                        {
                            cCommonLogLib.LogMessageLTA("Write image files to temp completed.",
                                extractDef.LogFilePath, LTAMessageImportance.Debug);
                        }
                    }
                }

                // Execute Post-Processing DLL.
                if (bolSuccess)
                {
                    if (extractDef.PostProcDLLFileName.Length > 0)
                    {
                        OnOutputStatusMsg("Executing post processing Dll.");
                        bolSuccess = RunPostProcessingDLL(strTempFileName, extractDef, parms);
                        if (!bolSuccess)
                        {
                            cCommonLogLib.LogMessageLTA("Failed to execute post-processing Dll.",
                                extractDef.LogFilePath, LTAMessageImportance.Essential);
                            OnOutputError("Failed to execute post-processing Dll.", "cExtract.RunExtract");
                        }
                        else
                        {
                            cCommonLogLib.LogMessageLTA("Execute post-processing Dll completed.",
                                extractDef.LogFilePath, LTAMessageImportance.Debug);
                        }
                    }
                }


                // Copy temp file to formatted destination path.
                if (bolSuccess)
                {
                    cCommonLogLib.LogMessageLTA($"Time to copy temp file to formatted destination path: OutputMode = {parms.OutputMode}",
                        extractDef.LogFilePath, LTAMessageImportance.Debug);
                    try
                    {
                        if (intTotalRecords > 0 || Settings.CreateZeroLengthFileBool)
                        {
                            //outputMode = 0 regular file output
                            //outputMode = 1 zipped extract file only
                            //outputMode  = 2 both
                            OnOutputStatusMsg("Copying extract File to destination directory.");
                            cCommonLogLib.LogMessageLTA("Running on outputMode: " + parms.OutputMode,
                                extractDef.LogFilePath, LTAMessageImportance.Essential);
                            var fileOutput = parms.OutputMode == 0 || parms.OutputMode == 2;
                            var extractOutput = parms.OutputMode == 1 || parms.OutputMode == 2;

                            if (fileOutput)
                            {
                                // if this is a relative path
                                if (strFormattedTargetFileName.StartsWith("."))
                                {
                                    File.Copy(strTempFileName, Settings.SetupPath + (Settings.SetupPath.EndsWith(@"\") ? "" : @"\") + strFormattedTargetFileName, true);
                                    parms.ExtractFileName = Settings.SetupPath + (Settings.SetupPath.EndsWith(@"\") ? "" : @"\") + strFormattedTargetFileName;

                                }
                                else
                                {
                                    File.Copy(strTempFileName, strFormattedTargetFileName, true);
                                    parms.ExtractFileName = strFormattedTargetFileName;
                                }
                                if (extractDef.ZipOutputFiles)
                                {
                                    if (arExtractImages == null)
                                    {
                                        arExtractImages = new List<string>();
                                    }
                                    bolSuccess = ZipOutputFiles(strFormattedTargetFileName,
                                        arExtractImages,
                                        strFormattedZipFileName);
                                }

                            }
                            
                            //copies data extract file to the attachment folder 
                            if (extractOutput && !string.IsNullOrEmpty(Settings.XClientExtractAttachmentPath) &&
                                !string.IsNullOrEmpty(Settings.XClientExtractFilePath) &&
                                (!Settings.XClientExtractAttachmentPath.Equals(XCLIENTPLACEHOLDER)) && !Settings.XClientExtractFilePath.Equals(XCLIENTPLACEHOLDER))
                            {
                                var outputFile = Settings.XClientExtractAttachmentPath +
                                                 (Settings.XClientExtractAttachmentPath.EndsWith(@"\") ? "" : @"\") +
                                                 Path.GetFileNameWithoutExtension(strFormattedTargetFileName);
                                outputFile += extractDef.ZipOutputFiles ? ".zip" : ".txt";

                                var extractTempFile = Path.GetDirectoryName(strTempFileName) + @"\" +
                                               Path.GetFileName(strFormattedTargetFileName);
                                //makes a temporary copy of the temp file with the right name
                                File.Copy(strTempFileName,extractTempFile , true);
                                if (extractDef.ZipOutputFiles)
                                {
                                    if (!ZipOutputFiles(extractTempFile,
                                        arExtractImages,
                                        outputFile))
                                    {
                                        cCommonLogLib.LogMessageLTA("FIT file was not generated.",
                                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                                        OnOutputStatusMsg("There was a problem writing the FIT file attachment.");
                                    }
                                }
                                else
                                {
                                    File.Copy(extractTempFile, outputFile, true);
                                }
                                if (!GenerateXClientFile(banks, outputFile, extractDef, parms, objCounters))
                                {
                                    cCommonLogLib.LogMessageLTA("FIT file was not generated.",
                                        extractDef.LogFilePath, LTAMessageImportance.Essential);
                                    OnOutputStatusMsg("There was a problem writing FIT File.");

                                    //If the output mode is both an alert and a file to disk, 
                                    //change to alert only so an alert will be generated even if the file did 
                                    //not generate.
                                    if (parms.OutputMode == 2)
                                        parms.OutputMode = 0;
                                }
                                

                                File.Delete(extractTempFile);
                            }

                            

                        }
                        else
                        {
                            cCommonLogLib.LogMessageLTA("There was no data written to the target file.",
                                extractDef.LogFilePath, LTAMessageImportance.Essential);
                            OnOutputStatusMsg("There was no data written to the target file.");
                        }

                        File.Delete(strTempFileName);

                    }
                    catch (Exception e)
                    {
                        bolSuccess = false;
                        cCommonLogLib.LogErrorLTA(e,
                                extractDef.LogFilePath, LTAMessageImportance.Essential);
                        // DLD - WI 130165
                        OnOutputError(this, e);
                        OnOutputMessage("Failed to copy extract file(s) to destination directory: " + strFormattedTargetFileName + " Exception:" + e.Message + e.Data + e.Source,
                            "RunExtract", MessageType.Error, MessageImportance.Essential);
                        parms.ExtractFileName = string.Empty;
                    }
                }
                else
                {
                    parms.ExtractFileName = string.Empty;
                }
                // Write database records for audit stored procedure calls.
                var targetname = extractDef.ZipOutputFiles
                    ? strFormattedZipFileName
                    : strFormattedTargetFileName;

                if (bolSuccess)
                {
                    StandardFields.ExtractCompleted = DateTime.Now;

                    if (parms.ExtractAuditID == Guid.Empty)
                    {
                        gidExtractAuditID = Guid.NewGuid();
                    }
                    else
                    {
                        gidExtractAuditID = parms.ExtractAuditID;
                    }

                    bolSuccess = WriteDb(banks, parms, extractDef, StandardFields, intTotalRecords, arUniqueBatchKeys, gidExtractAuditID, targetname);
                    if (!bolSuccess)
                    {
                        cCommonLogLib.LogMessageLTA("Failed writing Audit records to the database.",
                            extractDef.LogFilePath, LTAMessageImportance.Essential);
                    }
                    else
                    {
                        cCommonLogLib.LogMessageLTA("Writing Audit records to the database completed.",
                            extractDef.LogFilePath, LTAMessageImportance.Debug);
                    }
                }

                arExtractStats = new StringDictionary();

                // WI 139595 & WI 138920 : Adding calls to System-Level events.
                // WI 146953 : Complete alerts are now workgroup-level.
                if (!bolSuccess)
                {
                    cCommonLogLib.LogMessageLTA("Writing failed alert message.",
                        extractDef.LogFilePath, LTAMessageImportance.Debug);
                    WriteAlertLogs(banks, bolSuccess, extractDef.ExtractDefFilePath);
                }
                else
                {
                    cCommonLogLib.LogMessageLTA("Writing alert message.",
                        extractDef.LogFilePath, LTAMessageImportance.Debug);

                    if (intTotalRecords > 0 || Settings.CreateZeroLengthFileBool)
                    {
                        if (HasWorkgroupFilters(extractDef, parms))
                        {
                            WriteCompleteLogs(banks, targetname, parms);
                        }
                    }
                }



                // Finish up.
                if (bolSuccess)
                {

                    if (intTotalRecords > 0)
                        strMsg = "Extract completed successfully: [" + targetname + "].";
                    else
                        strMsg = "There was no data written to the target file.";

                    extractAuditID = gidExtractAuditID;

                    if (banks.Values != null)
                    {
                        foreach (cBank bank in banks.Values)
                        {
                            decSumPaymentsAmount += bank.SumPaymentsAmount;
                            decSumStubsAmount += bank.SumStubsAmount;
                        }
                    }

                    // Populate summary page array
                    // WI 130809 : Cleaned up this dialog a bit. Removed DE Fields, updated corrected terms.
                    var values = new List<KeyValuePair<string, string>>()
                    {
                        new KeyValuePair<string, string>("CountPayments", objCounters.CheckCount.ToString()),
                        new KeyValuePair<string, string>("CountDocuments", objCounters.DocumentCount.ToString()),
                        new KeyValuePair<string, string>("CountStubs", objCounters.StubCount.ToString()),
                        new KeyValuePair<string, string>("CountTransactions", objCounters.TransactionCount.ToString()),
                        new KeyValuePair<string, string>("RecordCountBank", objCounters.TotalBankRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountBatch", objCounters.TotalBatchRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountPayment", objCounters.TotalCheckRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountDetail", objCounters.TotalJointDetailRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountDoc", objCounters.TotalDocumentRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountFile", objCounters.TotalFileRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountWorkgroup", objCounters.TotalLockboxRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountStub", objCounters.TotalStubRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountTotal", objCounters.TotalRecords.ToString()),
                        new KeyValuePair<string, string>("RecordCountTran", objCounters.TotalTransactionRecords.ToString()),
                        new KeyValuePair<string, string>("SumPaymentsAmount", "$ " + decSumPaymentsAmount.ToString("#,##0.00")),
                        new KeyValuePair<string, string>("SumStubsAmount", "$ " + decSumStubsAmount.ToString("#,##0.00")),
                        new KeyValuePair<string, string>("ExtractAuditID", gidExtractAuditID.ToString())
                    };
                    var mapped = new List<KeyValuePair<string, string>>();
                    mapped.AddRange(
                        Settings.ExtractStatsColumns
                            .Select(c =>
                            {
                                var val = values.FirstOrDefault(x => x.Key == c.Name);
                                return new KeyValuePair<string, string>(val.Key, val.Value);
                            })
                        );

                    foreach (var m in mapped)
                        arExtractStats.Add(m.Key, m.Value);

                    OnOutputMessage(
                        //this,
                        strMsg,
                        "cExtract.RunExtract",
                        MessageType.Information,
                        MessageImportance.Essential);

                    //_LastErrorMessage = strMsg;
                    _LastErrorMessage = string.Empty;
                    cCommonLogLib.LogMessageLTA("Extract Completed: " + strMsg,
                                extractDef.LogFilePath, LTAMessageImportance.Essential);
                    OnOutputStatusMsg(strMsg);

                    bolRetVal = true;
                }
                else
                {
                    cCommonLogLib.LogMessageLTA("Extract Failed.",
                                extractDef.LogFilePath, LTAMessageImportance.Essential);
                    OnOutputStatusMsg("Extract Failed.");
                    bolRetVal = false;
                }

                // Write a spacer into the logfile.                
                cCommonLogLib.LogMessageLTA(Environment.NewLine + Environment.NewLine, extractDef.LogFilePath, LTAMessageImportance.Essential);

            }
            catch (Exception e)
            {
                OnOutputError(this, e);
                cCommonLogLib.LogErrorLTA(e,
                                extractDef.LogFilePath, LTAMessageImportance.Essential);
                gidExtractAuditID = Guid.Empty;
                //_ExtractLog = null;
                OnOutputStatusMsg("Extract Failed.");

                // Added an audit for failing extracts here.
                WriteAlertLogs(banks, false, extractDef.ExtractDefFilePath);

                bolRetVal = false;
            }
            finally
            {
                cOLFServicesLib.CloseCurrentClient(); // close the service in the event images were grabbed from the repository
            }
            _UserID = -1;

            extractAuditID = gidExtractAuditID;
            extractStats = arExtractStats;
            return ((bolRetVal ? 1 : 0));
        }

        /// <summary>
        /// Generates a file to use with the extract Xclient for FIT
        /// </summary>
        /// <param name="Banks"></param>
        /// <param name="extractFile"></param>
        /// <param name="extractDef"></param>
        /// <param name="parms"></param>
        /// <param name="targetName"></param>
        /// <param name="objCounters"></param>
        /// <param name="targetFName"></param>
        /// <returns></returns>
       private bool GenerateXClientFile(OrderedDictionary Banks, string extractFile, cExtractDef extractDef,
            cExtractParms parms, cCounters objCounters)
        {
            try
            {
                var paymentTotal = Banks.Values.Cast<cBank>().Sum(x => x.SumPaymentsAmount);
                var stubtotal = Banks.Values.Cast<cBank>().Sum(x => x.SumStubsAmount);
                var message = "Extract File";
                if (objCounters != null)
                {
                   message = "Your extract is complete.  \r\n" +
                             $"{GenerateDateFilterTextForAlert(parms)}  \r\n" +
                             $"Extract File Name: {Path.GetFileName(extractFile)}.  \t\r\n" +
                             "Alert created for Workgroup:  @FullWorkgroupName.  \r\n\r\n" + 
                             "Extract Totals  \r\n" +
                             "===================  \r\n" + 
                             $"Total Payments: {objCounters.CheckCount}.  \r\n" +
                             $"Payment Total: {paymentTotal:C2}.  \r\n\r\n" +
                             $"Total Related Items: {objCounters.StubCount}.  \r\n" +
                             $"Related Item Total: {stubtotal:C2}.  \r\n\r\n";
                }

                OnOutputStatusMsg("Creating File Import Service File");
                var banklist = Banks
                    .Values
                    .Cast<cBank>();

                // Linq for grabbing each unique workgroup extracted.
                var workgroups = banklist
                    .SelectMany(y => y.Lockboxes
                        .Values
                        .Cast<cClientAccount>());
                var wgs = workgroups.Where(w => w.BankID != -1).Select(
                    wg => new SimpleWorkgroup
                    {
                        Workgroup = wg.ClientAccountID,
                        Bank = wg.BankID,
                        WorkgroupName = wg.WorkgroupName
                    }).ToList();

                var xfile = new { Workgroups = wgs, File = extractFile, Message = message };
                var contents = xfile.ToJSON();
                

                File.WriteAllText(Settings.XClientExtractFilePath + (Settings.XClientExtractFilePath.EndsWith(@"\") ? "" : @"\") + Guid.NewGuid() + ".json", contents);
                OnOutputStatusMsg("Done Creating File Import Service File");
                return true;
            }
            catch (Exception ex)
            {
                cCommonLogLib.LogMessageLTA("Failed to write FIT file. " + ex.Message,
                    extractDef.LogFilePath, LTAMessageImportance.Essential);
                OnOutputError("Failed to write FIT file", "cExtract.GenerateXClientFile");
                return false;

            }

        }

        /// <summary>
        /// Evaluates the filters to determine if there are any that apply to workgroups
        /// </summary>
        /// <param name="extractDefinition">
        /// The extract definition 
        /// </param>
        /// <param name="runParms"></param>
        /// <returns></returns>
        public static bool HasWorkgroupFilters(cExtractDef extractDefinition,
            cExtractParms runParms)
        {

            return extractDefinition.LockboxLimitItems.Any() | runParms.LockboxID.Any();
        }

        /// <summary>
        /// Puts together the needed data to construct the text of the alert message
        /// </summary>
        /// <returns>
        /// Alert message text
        /// </returns>
        public static string GenerateExtractCompleteAlertMessage(cClientAccount workgroup, cExtractParms runParms, string filename,
            decimal total, int count, decimal stubtotal, int stubcount)
        {
            return "Your extract is complete.  \r\n" + 
                   $"{GenerateDateFilterTextForAlert(runParms)}  \r\n" +
                   $"Extract File Name:  {Path.GetFileName(filename)}.  \t\r\n" +
                   $"Alert created for Workgroup:  {workgroup.ClientAccountID} - {workgroup.WorkgroupName}.  \r\n\r\n" +
                   "Extract Totals  \r\n" + 
                   "===================  \r\n" + 
                   $"Total Payments: {count}.  \r\n" +
                   $"Payment Total: {total:C2}.  \r\n\r\n" + 
                   $"Total Related Items: {stubcount}.  \r\n" +
                   $"Related Items Total: {stubtotal:C2}.  \r\n\r\n";
        }

        /// <summary>
        /// WI 146953 : ExtractComplete Alert is now a workgroup-level event log.
        /// </summary>
        /// <param name="banks"></param>
        /// <returns></returns>
        public bool WriteCompleteLogs(OrderedDictionary banks, string targetname, cExtractParms runParms)
        {
            var successout = true;

            try
            {

                if (runParms.OutputMode != 0)
                {
                    OnOutputStatusMsg("Extract Complete alert skipped, output mode is not write to disk only.");
                    return successout;
                }

                ExtractEngineDAL.BeginTrans();

                OnOutputStatusMsg("Writing extract workgroup-level alerts.");

                var banklist = banks
                    .Values
                    .Cast<cBank>();

                // Linq for grabbing each unique workgroup extracted.
                var workgroups = banklist
                        .SelectMany(y => y.Lockboxes
                            .Values
                            .Cast<cClientAccount>())
                    .GroupBy(x => string.Format("{0}|{1}", x.BankID, x.ClientAccountID));

                var alertname = Support.ALERT_EXTRACT_COMPLETE;

                foreach (var group in workgroups)
                {
                    var first = group.First();
                    var paymenttotal = group.Sum(x => x.SumPaymentsAmount);
                    var paymentcount = group.Sum(x => x.CheckCount);
                    var stubtotal = group.Sum(x => x.SumStubsAmount);
                    var stubcount = group.Sum(x => x.StubCount);
                    var message = GenerateExtractCompleteAlertMessage(first, runParms, targetname, paymenttotal, paymentcount, stubtotal, stubcount);
                    successout = ExtractEngineDAL.WriteEventLog(alertname, message, first.SiteCodeID, first.BankID, first.ClientAccountID) && successout;
                }

                // Write to AuditFile table.
                if (!successout)
                {
                    OnOutputError("Could not call WriteEventLog", "cExtract:WriteAlertLogs");
                }

                ExtractEngineDAL.CommitTrans();
            }
            catch (Exception ex)
            {
                OnOutputError(this, ex);
                ExtractEngineDAL.RollbackTrans();
                successout = false;
            }

            return (successout);
        }

        /// <summary>
        /// WI 139593 & 138920 : Writing our System-Level event logs.
        /// </summary>
        /// <param name="banks"></param>
        /// <param name="success"></param>
        /// <returns></returns>
        private bool WriteAlertLogs(OrderedDictionary banks, bool success, string targetname)
        {
            var successout = true;

            try
            {
                ExtractEngineDAL.BeginTrans();

                OnOutputStatusMsg("Writing extract system-level alerts.");

                var totalpayments = 0;
                var sumpayments = 0m;
                if (banks != null)
                {
                    totalpayments = banks
                        .Values
                        .Cast<cBank>()
                        .Sum(x => x.CheckCount);
                    sumpayments = banks
                        .Values
                        .Cast<cBank>()
                        .Sum(x => x.SumPaymentsAmount);
                }

                var alertname = success
                    ? Support.ALERT_EXTRACT_COMPLETE
                    : Support.ALERT_EXTRACT_FAILURE;

                var message = success
                    ? string.Format("Extract Completed on file '{0}'. Total Payments: {1}, Total Amount: {2:C}, Executing Machine: {3}", targetname, totalpayments, sumpayments, Environment.MachineName)
                    : string.Format("Extract Failed on file '{0}'. Executing Machine: {1}", targetname, Environment.MachineName);

                successout = ExtractEngineDAL.WriteSystemLevelEventLog(alertname, message);

                // Write to AuditFile table.
                if (!successout)
                {
                    OnOutputError("Could not call WriteEventLog", "cExtract:WriteAlertLogs");
                }

                ExtractEngineDAL.CommitTrans();
            }
            catch (Exception ex)
            {
                OnOutputError(this, ex);
                ExtractEngineDAL.RollbackTrans();
                successout = false;
            }

            return (successout);
        }

        /// <summary>
        /// WI 130993 : Migrating from XCeed to standard .NET zipping frameworks.
        /// </summary>
        /// <param name="extractFilePath"></param>
        /// <param name="extractFiles"></param>
        /// <param name="zipFilePath"></param>
        /// <returns></returns>
        private bool ZipOutputFiles(
            string extractFilePath,
            List<string> extractFiles,
            string zipFilePath)
        {

            List<string> arFilesToZip = new List<string>();
            arFilesToZip.Add(extractFilePath);
            extractFiles = extractFiles ?? new List<string>();
            arFilesToZip.AddRange(extractFiles);

            try
            {
                if (File.Exists(zipFilePath))
                {
                    File.Delete(zipFilePath);
                }

                using (var arch = ZipFile.Open(zipFilePath, ZipArchiveMode.Create))
                {
                    foreach (var file in arFilesToZip)
                    {
                        arch.CreateEntryFromFile(file, Path.GetFileName(file), CompressionLevel.Optimal);
                    }
                }

                return (true);

            }
            catch (Exception ex)
            {
                OnOutputError(this, ex);
                return (false);
            }
        }

        #region Test Extract
        public bool BuildTestExtract(
            cExtractDef extractDef,
            out StringBuilder testExtractFileContents)
        {

            System.Collections.Specialized.OrderedDictionary objBanks;
            cExtractParms objParms;
            cStandardFields objStandardFields;
            cImageFormatOptions objImageFormatOptions;
            int intTotalRecords;
            cBatchKey[] arUniqueBatchKeys;
            List<string> arFileRows;
            string strMsg;
            cCounters objCounters;

            if (extractDef.IsValidExtract(out strMsg))
            {

                BuildTestData(extractDef, out objBanks);

                objParms = new cExtractParms();
                objParms.ExtractFileName = System.IO.Path.GetTempFileName();

                objStandardFields = new cStandardFields(/*DateTime.Now.Date*/);

                objImageFormatOptions = new cImageFormatOptions(Settings, extractDef, objParms);

                if (BuildExtractText(objBanks,
                                    extractDef,
                                    objParms,
                                    objStandardFields,
                                    objImageFormatOptions,
                                    out intTotalRecords,
                                    out arUniqueBatchKeys,
                                    out arFileRows,
                                    out objCounters))
                {

                    testExtractFileContents = new StringBuilder();
                    bool bolIsFirst = true;
                    foreach (string str in arFileRows)
                    {
                        if (bolIsFirst)
                        {
                            bolIsFirst = false;
                        }
                        else
                        {
                            testExtractFileContents.Append(extractDef.FormattedRecordDelim);
                        }
                        testExtractFileContents.Append(str);
                    }
                    return (true);

                }
                else
                {
                    testExtractFileContents = null;
                    return (false);
                }

            }
            else
            {
                OnOutputMessage(strMsg, this.ToString(), MessageType.Warning, MessageImportance.Essential);
                testExtractFileContents = null;
                return (false);
            }
        }

        public bool BuildTestLayout(
            cExtractDef extractDef,
            cLayout layout,
            out string testExtractFileContents)
        {

            System.Collections.Specialized.OrderedDictionary objBanks;
            cExtractParms objParms;
            cStandardFields objStandardFields;
            cImageFormatOptions objImageFormatOptions;
            cCounters objCounters;
            cCounters objTotals;
            string strMsg;

            int intChecksIndex = 0;
            int intStubsIndex = 0;
            int intDocumentsIndex = 0;
            string strLayoutText;
            string[] arLayoutText;
            cBank[] arBanks;
            cBankSummary BankSummary;

            if (extractDef.IsValidExtract(out strMsg))
            {

                BuildTestData(extractDef, out objBanks);

                objParms = new cExtractParms();
                objParms.ExtractFileName = System.IO.Path.GetTempFileName();

                objStandardFields = new cStandardFields(/*DateTime.Now.Date*/);

                objImageFormatOptions = new cImageFormatOptions(Settings, extractDef, objParms);

                objCounters = new cCounters();
                objCounters.InitializeCounts(LayoutLevelEnum.File, 1);
                objCounters.ResetLines(LayoutLevelEnum.File);

                objTotals = new cCounters();
                objTotals.InitializeCounts(LayoutLevelEnum.File, 1);
                objTotals.ResetLines(LayoutLevelEnum.File);

                arBanks = new cBank[objBanks.Count];
                objBanks.Values.CopyTo(arBanks, 0);
                BankSummary = new cBankSummary(arBanks);

                if (layout.LayoutLevel == LayoutLevelEnum.JointDetail)
                {
                    layout.FormatLayout(this,
                                        (cTransaction)((cBatch)((cClientAccount)((cBank)objBanks[0]).Lockboxes[0]).Batches[0]).Transactions[0],
                                        objParms,
                                        objStandardFields,
                                        objImageFormatOptions,
                                        objCounters,
                                        objTotals,
                                        ref intChecksIndex,
                                        ref intStubsIndex,
                                        ref intDocumentsIndex,
                                        out arLayoutText);

                    testExtractFileContents = string.Empty;
                    if (arLayoutText.Length > 0)
                    {
                        for (int i = 0; i < arLayoutText.Length; ++i)
                        {
                            testExtractFileContents += arLayoutText[i];
                            if (i < arLayoutText.Length)
                            {
                                testExtractFileContents += extractDef.FormattedRecordDelim;
                            }
                        }
                    }
                    return (true);

                }
                else
                {
                    switch (layout.LayoutLevel)
                    {
                        case LayoutLevelEnum.File:
                            layout.FormatLayout(
                                BankSummary,
                                extractDef,
                                objParms,
                                objStandardFields,
                                objCounters,
                                out strLayoutText);
                            break;
                        case LayoutLevelEnum.Bank:
                            layout.FormatLayout(
                                this,
                                (cBank)objBanks[0],
                                extractDef,
                                objParms,
                                objStandardFields,
                                objCounters,
                                out strLayoutText);
                            break;
                        case LayoutLevelEnum.ClientAccount:
                            layout.FormatLayout(
                                this,
                                (cClientAccount)((cBank)objBanks[0]).Lockboxes[0],
                                extractDef,
                                objParms,
                                objStandardFields,
                                objCounters,
                                out strLayoutText);
                            break;
                        case LayoutLevelEnum.Batch:
                            layout.FormatLayout(
                                this,
                                (cBatch)((cClientAccount)((cBank)objBanks[0]).Lockboxes[0]).Batches[0],
                                extractDef,
                                objParms,
                                objStandardFields,
                                objCounters,
                                out strLayoutText);
                            break;
                        case LayoutLevelEnum.Transaction:
                            layout.FormatLayout(
                                this,
                                (cTransaction)((cBatch)((cClientAccount)((cBank)objBanks[0]).Lockboxes[0]).Batches[0]).Transactions[0],
                                extractDef,
                                objParms,
                                objStandardFields,
                                objCounters,
                                out strLayoutText);
                            break;
                        case LayoutLevelEnum.Payment:
                            layout.FormatLayout(
                                this,
                                (cPayment)((cTransaction)((cBatch)((cClientAccount)((cBank)objBanks[0]).Lockboxes[0]).Batches[0]).Transactions[0]).Payments[0],
                                extractDef,
                                objParms,
                                objStandardFields,
                                objImageFormatOptions,
                                objCounters,
                                out strLayoutText);
                            break;
                        case LayoutLevelEnum.Stub:
                            layout.FormatLayout(
                                this,
                                (cStub)((cTransaction)((cBatch)((cClientAccount)((cBank)objBanks[0]).Lockboxes[0]).Batches[0]).Transactions[0]).Stubs[0],
                                extractDef,
                                objParms,
                                objStandardFields,
                                objImageFormatOptions,
                                objCounters,
                                out strLayoutText);
                            break;
                        case LayoutLevelEnum.Document:
                            layout.FormatLayout(
                                this,
                                (cDocument)((cTransaction)((cBatch)((cClientAccount)((cBank)objBanks[0]).Lockboxes[0]).Batches[0]).Transactions[0]).Documents[0],
                                extractDef,
                                objParms,
                                objStandardFields,
                                objImageFormatOptions,
                                objCounters,
                                out strLayoutText);
                            break;
                        default:
                            testExtractFileContents = string.Empty;
                            return (false);
                    }

                    testExtractFileContents = strLayoutText;
                    return (true);
                }

            }
            else
            {
                OnOutputMessage(strMsg, this.ToString(), MessageType.Warning, MessageImportance.Essential);
                testExtractFileContents = null;
                return (false);
            }
        }

        private void BuildTestData(
            cExtractDef extractDef,
            out OrderedDictionary banks)
        {

            OrderedDictionary objBanks;

            objBanks = new OrderedDictionary();

            cBank objBank = new cBank(1, extractDef.BankOrderByColumns);
            cClientAccount objClientAccount = new cClientAccount(objBank, 1, 1, 1, "Test Workgroup", extractDef.CustomerOrderByColumns);
            cBatch objBatch = new cBatch(objClientAccount, 1, 1, 0, 0, "BATCHSOURCE", "IMPORTTYPE", 0, 0, extractDef.BatchOrderByColumns);
            cTransaction objTransaction = new cTransaction(objBatch, 1, extractDef.TransactionsOrderByColumns);
            cPayment objCheck = new cPayment(Settings, objTransaction, 1, 1, (decimal)100.00, string.Empty, string.Empty, 0, extractDef.PaymentsOrderByColumns);

            // MLHTODO Why is this hard-coded  "RemitterName"? (MLH 09/18/2013)
            objCheck.CheckFields.Add("Serial", "1234");
            objCheck.CheckFields.Add("RemitterName", "John Adams");

            cStub objStub = new cStub(objTransaction, 1, 1, (decimal)100.00, extractDef.StubsOrderByColumns);
            cDocument objDocument = new cDocument(Settings, objTransaction, 1, 1, 1, "XX", extractDef.DocumentsOrderByColumns);

            objTransaction.Payments.Add(objCheck.ItemKey.ToString(), objCheck);
            objTransaction.Stubs.Add(objStub.UniqueStubKey, objStub);
            objTransaction.Documents.Add(objDocument.ItemKey.ToString(), objDocument);
            objBatch.Transactions.Add(objTransaction.TransactionID, objTransaction);
            objClientAccount.Batches.Add(objBatch.BankID.ToString() + "_" + objBatch.ClientAccountID.ToString() + "_" + objBatch.BatchID.ToString() + "_" + objBatch.ImmutableDateKey.ToString(), objBatch);
            objBank.Lockboxes.Add(objClientAccount.ClientAccountID, objClientAccount);
            objBanks.Add(objBank.BankID, objBank);

            banks = objBanks;
        }

        #endregion

        // WI 139595 : Removed the EventLog from this method.  Will push that back up to the calling method.
        private bool WriteDb(
            OrderedDictionary banks,
            IExtractParms parms,
            IExtractDef extractDef,
            cStandardFields standardFields,
            int totalRecords,
            cBatchKey[] uniqueBatchKeys,
            Guid extractAuditID,
            string formattedTargetFileName)
        {

            Guid gidExtractAuditID;
            bool bolSuccess;
            bool bolRetVal;

            try
            {

                ExtractEngineDAL.BeginTrans();

                // Write to ExtractAudit table
                OnOutputStatusMsg("Writing extract audit information.");
                bolSuccess = WriteExtractAuditData(
                    banks,
                    parms,
                    extractDef,
                    // WI 114570 removed for v2.0
                    /*StandardFields.CurrentProcessingDate, */
                    totalRecords,
                    extractAuditID,
                    formattedTargetFileName);

                // Write to Batch.ExtractSequenceNumber field.
                if (bolSuccess && parms.ExtractSequenceNumber > 0)
                {
                    OnOutputStatusMsg("Writing Extract Sequence Number.");
                    bolSuccess = WriteExtractSequenceNumber(parms, uniqueBatchKeys);
                    if (!bolSuccess)
                    {
                        OnOutputError("Failed to write Extract Sequence Number to Batch.", "cExtract.RunExtract");
                    }
                }

                // Write to BatchTrace table
                if (bolSuccess && parms.TraceField.Length > 0 && parms.RerunList.Length == 0)
                {
                    OnOutputStatusMsg("Writing Trace data.");
                    bolSuccess = WriteTraceField(parms, standardFields, uniqueBatchKeys);
                    if (!bolSuccess)
                    {
                        OnOutputError("Failed to write ExtractDescriptor data.", "cExtract.RunExtract");
                    }
                }

                // Write to AuditFile table.
                if (bolSuccess)
                {
                    OnOutputStatusMsg("Writing audit file information.");
                    bolSuccess = WriteAuditFileData(parms, extractDef, standardFields);
                    if (!bolSuccess)
                    {
                        OnOutputError("Failed to write AuditFile data.", "cExtract.RunExtract");
                    }
                }

                if (bolSuccess)
                {
                    ExtractEngineDAL.CommitTrans();
                    bolRetVal = true;
                }
                else
                {
                    ExtractEngineDAL.RollbackTrans();
                    gidExtractAuditID = Guid.Empty;
                    bolRetVal = false;
                }

            }
            catch (Exception ex)
            {
                gidExtractAuditID = Guid.Empty;
                OnOutputError(this, ex);
                ExtractEngineDAL.RollbackTrans();
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        private bool FormatFilePath(
            string filePath,
            cExtractParms parms,
            cStandardFields standardFields,
            out string formattedFilePath)
        {

            string strFormattedFilePath;
            string strFormattedTargetPath;
            int iPos;
            bool bolRetVal = false;

            if (CustomFormat.ParseFileTokens(
                filePath,
                parms,
                standardFields,
                out strFormattedFilePath))
            {

                for (int i = 0; i < CommonLib.INVALID_FILE_NAME_CHARS.Length; ++i)
                {
                    strFormattedFilePath.Replace(CommonLib.INVALID_FILE_NAME_CHARS.Substring(i, 1), string.Empty);
                }

                bolRetVal = true;
            }
            else
            {
                OnOutputMessage(
                    //this,
                    "Could not parse file path: [" + filePath + "].",
                    "cExtract.FormatFilePath",
                    MessageType.Error,
                    MessageImportance.Essential);

                formattedFilePath = string.Empty;
                return (false);
            }

            iPos = strFormattedFilePath.LastIndexOf(@"\");
            if (iPos > -1)
            {
                strFormattedTargetPath = strFormattedFilePath.Substring(0, iPos);

                // if this is a relative path
                if (strFormattedTargetPath.StartsWith("."))
                {
                    strFormattedTargetPath = Settings.SetupPath + (Settings.SetupPath.EndsWith(@"\") ? "" : @"\") + strFormattedTargetPath;
                }

                if (!Directory.Exists(strFormattedTargetPath))
                {
                    try
                    {
                        Directory.CreateDirectory(strFormattedTargetPath);
                    }
                    catch (Exception e)
                    {
                        // WI 131640 - Better exception logs.
                        cCommonLogLib.LogErrorLTA(e, parms.LogFilePath, LTAMessageImportance.Essential);
                        OnOutputMessage(
                            //this,
                            "Could not create folder: [" + strFormattedTargetPath + "] : " + e.Message,
                            "cExtract.FormatFilePath",
                            MessageType.Error,
                            MessageImportance.Essential);

                        formattedFilePath = string.Empty;
                        bolRetVal = false;
                    }
                }
            }

            // if this is a relative path
            if (strFormattedFilePath.StartsWith("."))
            {
                formattedFilePath = Path.GetFullPath(Settings.SetupPath + (Settings.SetupPath.EndsWith(@"\") ? "" : @"\") + strFormattedFilePath);
            }
            else
            {
                formattedFilePath = Path.GetFullPath(strFormattedFilePath);
            }

            return (bolRetVal);
        }

        private bool GetImageFormatOptions(
            cExtractDef extractDef,
            cExtractParms parms,
            out cImageFormatOptions imageFormatOptions)
        {

            imageFormatOptions = new cImageFormatOptions(
                this.Settings,
                extractDef,
                parms);

            return (true);
        }

        private bool GetManualInputValues(cExtractDef extractDef)
        {

            Dictionary<string, string> dicManualInputValues;

            try
            {

                dicManualInputValues = new Dictionary<string, string>();

                foreach (cLayout layout in extractDef.AllLayouts)
                {
                    foreach (cLayoutField layoutfield in layout.LayoutFields)
                    {
                        if (layoutfield.IsManualInput && (!dicManualInputValues.ContainsKey(layoutfield.Format)))
                        {
                            dicManualInputValues.Add(layoutfield.Format, string.Empty);
                        }
                    }
                }

                if (dicManualInputValues.Count > 0)
                {
                    OnPromptForManualInput(ref dicManualInputValues);

                    foreach (KeyValuePair<string, string> keypair in dicManualInputValues)
                    {
                        foreach (cLayout layout in extractDef.AllLayouts)
                        {
                            foreach (cLayoutField layoutfield in layout.LayoutFields)
                            {
                                if (layoutfield.IsManualInput && layoutfield.Format == keypair.Key)
                                {
                                    layoutfield.ManualInputValue = keypair.Value;
                                }
                            }
                        }
                    }
                }

                return (true);
            }
            catch (Exception ex)
            {
                OnOutputError(this, ex);
                return (false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FilePath"></param>
        /// <param name="Parms"></param>
        /// <returns></returns>
        private int RunExtractFile(
            string setupFile,
            cExtractParms parms)
        {

            cExtractDef ExtractDef;
            int intRetVal = 0;
            string strMsg;
            StringDictionary arExtractStats;
            Guid gidTemp;

            try
            {
                if (File.Exists(setupFile))
                {

                    if (ParseExtractDefFile(setupFile, out ExtractDef))
                    {

                        intRetVal = RunExtract(
                            ExtractDef,
                            parms,
                            out gidTemp,
                            out arExtractStats);

                        parms.ExtractAuditID = gidTemp;
                    }
                    else
                    {
                        strMsg = "Unable to parse extract ExtractDef file: " + setupFile;
                        OnOutputError(strMsg, "cExtract.RunExtractFile");
                        intRetVal = 0;
                    }
                }
                else
                {
                    strMsg = "Unable to access file: " + setupFile;
                    OnOutputError(strMsg, "cExtract.RunExtractFile");
                    intRetVal = 0;
                }
            }
            catch (Exception e)
            {
                // WI 131640 - Better exception logs.
                cCommonLogLib.LogErrorLTA(e, parms.LogFilePath, LTAMessageImportance.Essential);
                //this.handle_ltaLogLogError(ExceptionLog, e);
                intRetVal = 0;
            }

            return (intRetVal);
        }

        private bool ParseExtractDefFile(
            string filePath,
            out cExtractDef extractDef)
        {

            cExtractDef objRetVal = new cExtractDef();
            objRetVal.OutputError += OnOutputError;

            if (objRetVal.ParseExtractDefFile(filePath))
            {
                extractDef = objRetVal;
                return (true);
            }
            else
            {
                extractDef = null;
                return (false);
            }
        }

        private bool PopulateDataObjects(
            cExtractDef extractDef,
            cExtractParms parms,
            out OrderedDictionary banks)
        {

            bool bolRetVal;
            OrderedDictionary dicBanks;
            int[] DepositDates = { };

            bolRetVal = PopulateBanks(extractDef, parms, out dicBanks);

            if (bolRetVal)
            {
                bolRetVal = PopulateLockboxes(extractDef, parms, dicBanks);
            }

            if (bolRetVal)
            {
                if (extractDef.BatchLayouts.Length > 0 ||
                   extractDef.TransactionLayouts.Length > 0 ||
                   extractDef.PaymentLayouts.Length > 0 ||
                   extractDef.StubLayouts.Length > 0 ||
                   extractDef.DocumentLayouts.Length > 0 ||
                   extractDef.JointDetailLayouts.Length > 0)
                {

                    bolRetVal = PopulateDepositDates(
                        extractDef,
                        parms,
                        DatabaseConstants.TABLE_NAME_FACTBATCH,
                        out DepositDates);
                }
            }

            if (bolRetVal)
            {
                bolRetVal = PopulateBatches(
                    extractDef,
                    parms,
                    DepositDates,
                    dicBanks);
            }

            if (bolRetVal)
            {
                bolRetVal = PopulateTransactions(extractDef, parms, DepositDates, dicBanks);
            }

            if (bolRetVal)
            {
                bolRetVal = PopulatePayments(
                    Settings,
                    extractDef,
                    parms,
                    DepositDates,
                    dicBanks);
            }

            if (bolRetVal)
            {
                bolRetVal = PopulateStubs(extractDef, parms, DepositDates, dicBanks);
            }

            if (bolRetVal)
            {
                bolRetVal = PopulateDocuments(
                    Settings,
                    extractDef,
                    parms,
                    DepositDates,
                    dicBanks);
            }

            if (parms.TraceField.Length > 0 && parms.RerunList.Length > 0)
            {
                bolRetVal = DataObjects.FilterNoBatchObjects(dicBanks);
            }

            if (dicBanks == null)
            {
                banks = new OrderedDictionary();
            }
            else
            {
                banks = dicBanks;
            }

            return (bolRetVal);
        }

        private bool SortDataObjects(ref OrderedDictionary banks)
        {

            foreach (cBank bank in banks.Values)
            {

                foreach (cClientAccount lockbox in bank.Lockboxes.Values)
                {

                    foreach (cBatch batch in lockbox.Batches.Values)
                    {

                        foreach (cTransaction transaction in batch.Transactions.Values)
                        {

                            transaction.Payments = SortCollection(transaction.Payments);
                            transaction.Stubs = SortCollection(transaction.Stubs);
                            transaction.Documents = SortCollection(transaction.Documents);
                        }

                        batch.Transactions = SortCollection(batch.Transactions);
                    }

                    lockbox.Batches = SortCollection(lockbox.Batches);
                }

                bank.Lockboxes = SortCollection(bank.Lockboxes);
            }

            banks = SortCollection(banks);

            return (true);
        }


        private OrderedDictionary SortCollection(OrderedDictionary dic)
        {

            OrderedDictionary objRetVal = new OrderedDictionary();

            foreach (DictionaryEntry de in dic)
            {

                //First Item in the return collection
                if (objRetVal.Count == 0)
                {
                    objRetVal.Add(de.Key, de.Value);
                }
                else
                {
                    for (int i = 0; i < objRetVal.Count; ++i)
                    {

                        // If this is the last item in the return collection
                        if (i == objRetVal.Count - 1)
                        {
                            if (((IComparable)de.Value).CompareTo(objRetVal[i]) < 0)
                            {
                                // new item is less than the last item in return collection
                                objRetVal.Insert(i, de.Key, de.Value);
                            }
                            else
                            {
                                // new item is >= the last item in the return collection so 
                                // append it to the end of the collection.
                                objRetVal.Add(de.Key, de.Value);
                            }
                            break;
                        }
                        else
                        {
                            if (((IComparable)de.Value).CompareTo(objRetVal[i]) < 0)
                            {
                                // New item is less than the current item, so insert the
                                // new item and bump up the currect value one position.
                                objRetVal.Insert(i, de.Key, de.Value);
                                break;
                            }
                        }

                    }
                }
            }

            return (objRetVal);
        }

        private bool PopulateBanks(
            cExtractDef extractDef,
            cExtractParms parms,
            out OrderedDictionary banks)
        {

            //string strSQL;
            SqlDataReader drBanks;
            bool bolRetVal = true;
            banks = null;

            if (extractDef.BankLayouts.Length > 0 ||
               extractDef.CustomerLayouts.Length > 0 ||
               extractDef.LockboxLayouts.Length > 0 ||
               extractDef.BatchLayouts.Length > 0 ||
               extractDef.TransactionLayouts.Length > 0 ||
               extractDef.PaymentLayouts.Length > 0 ||
               extractDef.StubLayouts.Length > 0 ||
               extractDef.DocumentLayouts.Length > 0 ||
               extractDef.JointDetailLayouts.Length > 0
                )
            {

                if (ExtractEngineDAL.GetBanks(parms, extractDef, BankTableDef, out drBanks))
                {
                    OnOutputStatusMsg("Populating Bank Data Objects.");
                    banks = DataObjects.PopulateBanks(extractDef,
                                                      drBanks);
                    OnOutputStatusMsg("");
                    drBanks.Close();
                    drBanks.Dispose();
                }
                else
                {
                    bolRetVal = false;
                }
            }

            if (banks == null)
            {
                banks = new OrderedDictionary();
            }

            return (bolRetVal);
        }

        public bool PopulateLockboxes(
            cExtractDef extractDef,
            cExtractParms parms,
            OrderedDictionary banks)
        {

            //string strSQL;
            DbDataReader drClientAccounts;
            bool bolRetVal = true;

            if (extractDef.LockboxLayouts.Length > 0 ||
               extractDef.BatchLayouts.Length > 0 ||
               extractDef.TransactionLayouts.Length > 0 ||
               extractDef.PaymentLayouts.Length > 0 ||
               extractDef.StubLayouts.Length > 0 ||
               extractDef.DocumentLayouts.Length > 0 ||
               extractDef.JointDetailLayouts.Length > 0
                )
            {

                if (ExtractEngineDAL.GetClientAccounts(
                    parms,
                    extractDef,
                    BankTableDef,
                    ClientAccountTableDef,
                    out drClientAccounts))
                {

                    OnOutputStatusMsg("Populating Workgroup Data Objects.");
                    DataObjects.PopulateClientAccounts(drClientAccounts,
                                                  extractDef,
                                                  banks);
                    OnOutputStatusMsg("");
                    drClientAccounts.Close();
                    drClientAccounts.Dispose();
                }
                else
                {
                    bolRetVal = false;
                }
            }

            return (bolRetVal);
        }

        private bool PopulateDepositDates(
            cExtractDef extractDef,
            cExtractParms parms,
            string factTableName,
            out int[] depositDateKeys)
        {

            //string strSQL;
            DataTable dtDepositDateKeys;
            bool bolRetVal = true;
            List<int> arDepositDateKeys = new List<int>();

            if (ExtractEngineDAL.GetDepositDates(
                parms,
                extractDef,
                factTableName,
                ClientAccountTableDef,
                ExtractTracesTableDef,
                BatchTableDef,
                out dtDepositDateKeys))
            {

                foreach (DataRow dr in dtDepositDateKeys.Rows)
                {
                    var column = "DepositDateKey";
                    if (parms.DepositDateFrom == DateTime.MinValue)
                        column = "SourceProcessingDateKey";
                    if (!dr.IsNull(column) && !arDepositDateKeys.Contains(((int)dr[column])))
                    {
                        arDepositDateKeys.Add(((int)dr[column]));
                    }
                }

                dtDepositDateKeys.Dispose();
                bolRetVal = true;
            }
            else
            {
                bolRetVal = false;
            }

            depositDateKeys = arDepositDateKeys.ToArray();

            return (bolRetVal);
        }

        private bool PopulateBatches(
            cExtractDef extractDef,
            cExtractParms parms,
            int[] depositDateKeys,
            OrderedDictionary banks)
        {

            //string strSQL;
            SqlDataReader drBatches;
            bool bolRetVal = true;

            if (extractDef.BatchLayouts.Length > 0 ||
               extractDef.TransactionLayouts.Length > 0 ||
               extractDef.PaymentLayouts.Length > 0 ||
               extractDef.StubLayouts.Length > 0 ||
               extractDef.DocumentLayouts.Length > 0 ||
               extractDef.AggregateCount(LayoutLevelEnum.Batch) > 0 ||
               extractDef.JointDetailLayouts.Length > 0)
            {

                foreach (int dteCurrent in depositDateKeys)
                {
                    if (ExtractEngineDAL.GetBatches(
                        parms,
                        extractDef,
                        dteCurrent,
                        BankTableDef,
                        ClientAccountTableDef,
                        ExtractTracesTableDef,
                        BatchTableDef,
                        out drBatches))
                    {

                        OnOutputStatusMsg("Populating Batch Data Objects.");
                        DataObjects.PopulateBatches(drBatches,
                                                    extractDef,
                                                    banks);
                        OnOutputStatusMsg("");
                        drBatches.Close();
                        drBatches.Dispose();
                    }
                    else
                    {
                        bolRetVal = false;
                        break;
                    }
                }

            }

            return (bolRetVal);
        }

        private bool PopulateTransactions(
            cExtractDef extractDef,
            cExtractParms parms,
            int[] depositDateKeys,
            OrderedDictionary banks)
        {

            //string strSQL;
            SqlDataReader drTransactions;
            bool bolRetVal = true;

            if (extractDef.TransactionLayouts.Length > 0 ||
               extractDef.PaymentLayouts.Length > 0 ||
               extractDef.StubLayouts.Length > 0 ||
               extractDef.DocumentLayouts.Length > 0 ||
               extractDef.JointDetailLayouts.Length > 0 ||
               extractDef.AggregateCount(LayoutLevelEnum.Transaction) > 0)
            {

                foreach (int dteCurrent in depositDateKeys)
                {

                    if (ExtractEngineDAL.GetTransactions(
                        parms,
                        extractDef,
                        dteCurrent,
                        BankTableDef,
                        ClientAccountTableDef,
                        ExtractTracesTableDef,
                        BatchTableDef,
                        TransactionsTableDef,
                        out drTransactions))
                    {

                        OnOutputStatusMsg("Populating Transactions Data Objects.");
                        DataObjects.PopulateTransactions(drTransactions,
                                                         extractDef,
                                                         banks);
                        OnOutputStatusMsg("");
                        drTransactions.Close();
                        drTransactions.Dispose();
                    }
                    else
                    {
                        bolRetVal = false;
                        break;
                    }
                }

            }

            return (bolRetVal);
        }

        private bool PopulatePayments(
            ExtractConfigurationSettings settings,
            cExtractDef extractDef,
            cExtractParms parms,
            int[] depositDateKeys,
            OrderedDictionary banks)
        {

            bool bolRetVal = true;

            if (extractDef.PaymentLayouts.Length > 0 ||
               (extractDef.JointDetailLayouts.Length > 0 && extractDef.JointDetailLayoutFieldCount(LayoutLevelEnum.Payment) > 0) ||
               (extractDef.AggregateCount(LayoutLevelEnum.Payment) > 0))
            {

                foreach (int dteCurrent in depositDateKeys)
                {

                    SqlDataReader drPayments;
                    //SqlDataReader drPaymentsDE;
                    if (ExtractEngineDAL.GetPayments(
                            parms,
                            extractDef,
                            dteCurrent,
                            BankTableDef,
                            ClientAccountTableDef,
                            ExtractTracesTableDef,
                            BatchTableDef,
                            TransactionsTableDef,
                            PaymentsTableDef/*,
                            ChecksDETableDef,
                            RemittersTableDef*/,
                            out drPayments))
                    {

                        OnOutputStatusMsg("Populating Payment Data Objects.");
                        DataObjects.PopulatePayments(
                            settings,
                            drPayments,
                            extractDef,
                            banks);

                        OnOutputStatusMsg("");
                        drPayments.Close();
                        drPayments.Dispose();
                    }
                    else
                    {
                        bolRetVal = false;
                        break;
                    }
                }
            }

            return (bolRetVal);
        }

        private bool PopulateStubs(
            cExtractDef extractDef,
            cExtractParms parms,
            int[] depositDateKeys,
            OrderedDictionary banks)
        {

            bool bolRetVal = true;

            if (extractDef.StubLayouts.Length > 0 ||
               (extractDef.JointDetailLayouts.Length > 0 && extractDef.JointDetailLayoutFieldCount(LayoutLevelEnum.Stub) > 0) ||
               (extractDef.AggregateCount(LayoutLevelEnum.Stub) > 0))
            {

                foreach (int dteCurrent in depositDateKeys)
                {

                    SqlDataReader drStubs;
                    //SqlDataReader drStubsDE;
                    if (ExtractEngineDAL.GetStubs(
                        parms,
                        extractDef,
                        dteCurrent,
                        BankTableDef,
                        ClientAccountTableDef,
                        ExtractTracesTableDef,
                        BatchTableDef,
                        TransactionsTableDef,
                        StubsTableDef/*,
                        StubsDETableDef*/,
                        out drStubs))
                    {

                        OnOutputStatusMsg("Populating Stubs Data Objects.");
                        DataObjects.PopulateStubs(drStubs,
                                                  extractDef,
                                                  banks);
                        OnOutputStatusMsg("");
                        drStubs.Close();
                        drStubs.Dispose();
                    }
                    else
                    {
                        bolRetVal = false;
                        break;
                    }
                }

            }

            return (bolRetVal);
        }

        private bool PopulateDocuments(
            ExtractConfigurationSettings settings,
            cExtractDef extractDef,
            cExtractParms parms,
            int[] depositDateKeys,
            OrderedDictionary banks)
        {

            //string strSQL;
            SqlDataReader drDocuments;
            bool bolRetVal = true;

            if (extractDef.DocumentLayouts.Length > 0 ||
               (extractDef.JointDetailLayouts.Length > 0 && extractDef.JointDetailLayoutFieldCount(LayoutLevelEnum.Document) > 0) ||
               (extractDef.AggregateCount(LayoutLevelEnum.Document) > 0))
            {

                foreach (int dteCurrent in depositDateKeys)
                {

                    if (ExtractEngineDAL.GetDocuments(
                        parms,
                        extractDef,
                        dteCurrent,
                        BankTableDef,
                        ClientAccountTableDef,
                        ExtractTracesTableDef,
                        BatchTableDef,
                        TransactionsTableDef,
                        DocumentsTableDef/*,
                        DocsDETableDef*/,
                        out drDocuments))
                    {

                        OnOutputStatusMsg("Populating Documents Data Objects.");

                        DataObjects.PopulateDocuments(
                            settings,
                            drDocuments,
                            extractDef,
                            banks);

                        OnOutputStatusMsg("");
                        drDocuments.Close();
                        drDocuments.Dispose();
                    }
                    else
                    {
                        bolRetVal = false;
                        break;
                    }
                }

            }

            return (bolRetVal);
        }

        private bool GetStandardFields(out cStandardFields standardFields)
        {

            bool bolRetVal;
            cStandardFields objRetVal;

            objRetVal = new cStandardFields();
            bolRetVal = true;

            standardFields = objRetVal;

            return (bolRetVal);
        }

        private bool AssignImageFileNames(OrderedDictionary banks)
        {

            // When images are saved along with the extract, each is given in indexer in order to keep the image
            // filenames unique.  This indexer is arbitrary and uses an algorithm to assign them.  I've kept the
            // algorithm similar, but there are some differences in the way it orders them from the original 
            // version.

            int intIndex = 1;
            int intTotalTransactions = 0;
            int intCurrentTransactions = 0;
            bool bolRetVal = true;

            foreach (cBank bank in banks.Values)
            {
                intTotalTransactions += bank.TransactionCount;
            }

            OnImageStatusInitialized(intTotalTransactions);

            foreach (cBank bank in banks.Values)
            {

                foreach (cClientAccount lockbox in bank.Lockboxes.Values)
                {

                    foreach (cBatch batch in lockbox.Batches.Values)
                    {

                        foreach (cTransaction transaction in batch.Transactions.Values)
                        {

                            foreach (cPayment check in transaction.Payments.Values)
                            {
                                check.ImageCounter = intIndex++;
                                intIndex++;
                            }

                            foreach (cDocument document in transaction.Documents.Values)
                            {
                                document.ImageCounter = intIndex++;
                                intIndex++;
                            }

                            OnImageStatusChanged(++intCurrentTransactions);
                        }
                    }
                }
            }

            OnImageStatusInitialized(0);

            bolRetVal = true;

            return (bolRetVal);
        }


        private bool BuildExtractText(
            OrderedDictionary banks,
            cExtractDef extractDef,
            cExtractParms parms,
            cStandardFields standardFields,
            cImageFormatOptions imageFormatOptions,
            out int totalRecords,
            out cBatchKey[] uniqueBatchKeys,
            out List<string> fileRows,
            out cCounters counters)
        {

            bool bolRetVal = false;
            List<cBatchKey> arBatches = new List<cBatchKey>();

            totalRecords = 0;
            cCounters objCounters = null;
            cCounters objTotals = null; // object to hold accumulated totals

            cBank[] arBanks = new cBank[banks.Count];
            banks.Values.CopyTo(arBanks, 0);
            cBankSummary BankSummary = new cBankSummary(arBanks);

            List<string> arFileLines = new List<string>();
            List<string> arFileHeader = new List<string>();

            string strLayoutText;
            string[] arLayoutText;

            int intChecksIndex = 0;
            int intStubsIndex = 0;
            int intDocumentsIndex = 0;

            try
            {

                objCounters = new cCounters();
                objCounters.InitializeCounts(LayoutLevelEnum.File, 1);
                ++objCounters.FileIndex;
                objCounters.ResetLines(LayoutLevelEnum.File);

                objTotals = new cCounters();
                objTotals.InitializeCounts(LayoutLevelEnum.File, 1);
                ++objTotals.FileIndex;
                objTotals.ResetLines(LayoutLevelEnum.File);

                // File Headers
                foreach (cLayout layout in extractDef.FileLayouts)
                {
                    if (layout.FormatLayout(BankSummary, extractDef, parms, standardFields, objCounters, out strLayoutText))
                    {
                        if (layout.LayoutType == LayoutTypeEnum.Header)
                        {
                            objCounters.IncrementLines(LayoutLevelEnum.File);
                            objTotals.IncrementLines(LayoutLevelEnum.File);
                        }
                        ++objCounters.TotalFileRecords;
                        ++objTotals.TotalFileRecords;
                    }
                }

                objCounters.InitializeCounts(LayoutLevelEnum.Bank, banks.Count);
                objTotals.InitializeCounts(LayoutLevelEnum.Bank, banks.Count);

                foreach (cBank bank in banks.Values)
                {

                    ++objCounters.BankIndex;
                    objCounters.ResetLines(LayoutLevelEnum.Bank);

                    // Bank Headers
                    foreach (cLayout layout in extractDef.BankLayouts)
                    {
                        if (layout.LayoutType == LayoutTypeEnum.Header)
                        {
                            if (layout.FormatLayout(this, bank, extractDef, parms, standardFields, objCounters, out strLayoutText))
                            {
                                objCounters.IncrementLines(LayoutLevelEnum.Bank);
                                objTotals.IncrementLines(LayoutLevelEnum.Bank);
                                ++objCounters.TotalBankRecords;
                                ++objTotals.TotalBankRecords;

                                arFileLines.Add(strLayoutText);
                            }
                        }
                    }

                    objCounters.InitializeCounts(LayoutLevelEnum.ClientAccount, bank.Lockboxes.Count);
                    objTotals.LockboxCount += bank.Lockboxes.Count;

                    foreach (cClientAccount lockbox in bank.Lockboxes.Values)
                    {

                        ++objCounters.LockboxIndex;
                        objCounters.ResetLines(LayoutLevelEnum.ClientAccount);

                        // Lockbox Headers
                        foreach (cLayout layout in extractDef.LockboxLayouts)
                        {
                            if (layout.LayoutType == LayoutTypeEnum.Header)
                            {
                                if (layout.FormatLayout(this, lockbox, extractDef, parms, standardFields, objCounters, out strLayoutText))
                                {
                                    objCounters.IncrementLines(LayoutLevelEnum.ClientAccount);
                                    ++objCounters.TotalLockboxRecords;
                                    objTotals.IncrementLines(LayoutLevelEnum.ClientAccount);
                                    ++objTotals.TotalLockboxRecords;

                                    arFileLines.Add(strLayoutText);
                                }
                            }
                        }

                        objCounters.InitializeCounts(LayoutLevelEnum.Batch, lockbox.Batches.Count);
                        objTotals.BatchCount += lockbox.Batches.Count;

                        foreach (cBatch batch in lockbox.Batches.Values)
                        {

                            ++objCounters.BatchIndex;
                            objCounters.ResetLines(LayoutLevelEnum.Batch);

                            arBatches.Add(new cBatchKey(batch.BatchID));
                            //, batch.BankID.ToString() + "_" + batch.LockboxID.ToString() + "_" + batch.BatchID.ToString() + "_" + batch.ImmutableDateKey.ToString()

                            // Batch Headers
                            foreach (cLayout layout in extractDef.BatchLayouts)
                            {
                                if (layout.LayoutType == LayoutTypeEnum.Header)
                                {
                                    if (layout.FormatLayout(this, batch, extractDef, parms, standardFields, objCounters, out strLayoutText))
                                    {
                                        objCounters.IncrementLines(LayoutLevelEnum.Batch);
                                        ++objCounters.TotalBatchRecords;
                                        objTotals.IncrementLines(LayoutLevelEnum.Batch);
                                        ++objTotals.TotalBatchRecords;

                                        arFileLines.Add(strLayoutText);
                                    }
                                }
                            }

                            objCounters.InitializeCounts(LayoutLevelEnum.Transaction, batch.Transactions.Count);
                            objTotals.TransactionCount += batch.Transactions.Count;

                            foreach (cTransaction transaction in batch.Transactions.Values)
                            {

                                ++objCounters.TransactionIndex;
                                objCounters.ResetLines(LayoutLevelEnum.Transaction);

                                // Transaction Headers
                                foreach (cLayout layout in extractDef.TransactionLayouts)
                                {
                                    if (layout.LayoutType == LayoutTypeEnum.Header)
                                    {
                                        if (layout.FormatLayout(this, transaction, extractDef, parms, standardFields, objCounters, out strLayoutText))
                                        {
                                            objCounters.IncrementLines(LayoutLevelEnum.Transaction);
                                            ++objCounters.TotalTransactionRecords;
                                            objTotals.IncrementLines(LayoutLevelEnum.Transaction);
                                            ++objTotals.TotalTransactionRecords;

                                            arFileLines.Add(strLayoutText);
                                        }
                                    }
                                }

                                objCounters.InitializeCounts(LayoutLevelEnum.Payment, transaction.Payments.Count);
                                objTotals.CheckCount += transaction.Payments.Count;

                                // Checks Layouts
                                foreach (cPayment check in transaction.Payments.Values)
                                {

                                    ++objCounters.CheckIndex;

                                    foreach (cLayout layout in extractDef.PaymentLayouts)
                                    {
                                        if (layout.FormatLayout(this, check, extractDef, parms, standardFields, imageFormatOptions, objCounters, out strLayoutText))
                                        {
                                            objCounters.IncrementLines(LayoutLevelEnum.Payment);
                                            ++objCounters.TotalCheckRecords;
                                            objTotals.IncrementLines(LayoutLevelEnum.Payment);
                                            ++objTotals.TotalCheckRecords;

                                            arFileLines.Add(strLayoutText);
                                        }
                                    }
                                }

                                objCounters.InitializeCounts(LayoutLevelEnum.Stub, transaction.Stubs.Count);
                                objTotals.StubCount += transaction.Stubs.Count;

                                // Stub Layouts
                                foreach (cStub stub in transaction.Stubs.Values)
                                {

                                    ++objCounters.StubIndex;

                                    foreach (cLayout layout in extractDef.StubLayouts)
                                    {
                                        if (layout.FormatLayout(this, stub, extractDef, parms, standardFields, imageFormatOptions, objCounters, out strLayoutText))
                                        {
                                            objCounters.IncrementLines(LayoutLevelEnum.Stub);
                                            ++objCounters.TotalStubRecords;
                                            objTotals.IncrementLines(LayoutLevelEnum.Stub);
                                            ++objTotals.TotalStubRecords;

                                            arFileLines.Add(strLayoutText);
                                        }
                                    }
                                }

                                objCounters.InitializeCounts(LayoutLevelEnum.Document, transaction.Documents.Count);
                                objTotals.DocumentCount += transaction.Documents.Count;

                                // Document Layouts
                                foreach (cDocument document in transaction.Documents.Values)
                                {

                                    ++objCounters.DocumentIndex;

                                    foreach (cLayout layout in extractDef.DocumentLayouts)
                                    {
                                        if (layout.FormatLayout(this, document, extractDef, parms, standardFields, imageFormatOptions, objCounters, out strLayoutText))
                                        {
                                            objCounters.IncrementLines(LayoutLevelEnum.Document);
                                            ++objCounters.TotalDocumentRecords;
                                            objTotals.IncrementLines(LayoutLevelEnum.Document);
                                            ++objTotals.TotalDocumentRecords;

                                            arFileLines.Add(strLayoutText);
                                        }
                                    }
                                }

                                intChecksIndex = 0;
                                intStubsIndex = 0;
                                intDocumentsIndex = 0;

                                // Joint Detail Detail Layouts
                                foreach (cLayout layout in extractDef.JointDetailLayouts)
                                {

                                    if (layout.FormatLayout(
                                        this,
                                        transaction,
                                        parms,
                                        standardFields,
                                        imageFormatOptions,
                                        objCounters,
                                        objTotals,
                                        ref intChecksIndex,
                                        ref intStubsIndex,
                                        ref intDocumentsIndex,
                                        out arLayoutText))
                                    {

                                        foreach (string str in arLayoutText)
                                        {
                                            arFileLines.Add(str);
                                        }
                                    }
                                }

                                // Transaction Trailers
                                foreach (cLayout layout in extractDef.TransactionLayouts)
                                {
                                    if (layout.LayoutType == LayoutTypeEnum.Trailer)
                                    {
                                        if (layout.FormatLayout(this, transaction, extractDef, parms, standardFields, objCounters, out strLayoutText))
                                        {
                                            objCounters.IncrementLines(LayoutLevelEnum.Transaction);
                                            ++objCounters.TotalTransactionRecords;
                                            objTotals.IncrementLines(LayoutLevelEnum.Transaction);
                                            ++objTotals.TotalTransactionRecords;

                                            arFileLines.Add(strLayoutText);
                                        }
                                    }
                                }
                            }

                            // Batch Trailers
                            foreach (cLayout layout in extractDef.BatchLayouts)
                            {
                                if (layout.LayoutType == LayoutTypeEnum.Trailer)
                                {
                                    if (layout.FormatLayout(this, batch, extractDef, parms, standardFields, objCounters, out strLayoutText))
                                    {
                                        objCounters.IncrementLines(LayoutLevelEnum.Batch);
                                        ++objCounters.TotalBatchRecords;
                                        objTotals.IncrementLines(LayoutLevelEnum.Batch);
                                        ++objTotals.TotalBatchRecords;

                                        arFileLines.Add(strLayoutText);
                                    }
                                }
                            }
                        }

                        // Lockbox Trailers
                        foreach (cLayout layout in extractDef.LockboxLayouts)
                        {
                            if (layout.LayoutType == LayoutTypeEnum.Trailer)
                            {
                                if (layout.FormatLayout(this, lockbox, extractDef, parms, standardFields, objCounters, out strLayoutText))
                                {
                                    objCounters.IncrementLines(LayoutLevelEnum.ClientAccount);
                                    ++objCounters.TotalLockboxRecords;
                                    objTotals.IncrementLines(LayoutLevelEnum.ClientAccount);
                                    ++objTotals.TotalLockboxRecords;

                                    arFileLines.Add(strLayoutText);
                                }
                            }
                        }
                    }

                    // Bank Trailers
                    foreach (cLayout layout in extractDef.BankLayouts)
                    {
                        if (layout.LayoutType == LayoutTypeEnum.Trailer)
                        {
                            if (layout.FormatLayout(this, bank, extractDef, parms, standardFields, objCounters, out strLayoutText))
                            {
                                objCounters.IncrementLines(LayoutLevelEnum.Bank);
                                ++objCounters.TotalBankRecords;
                                objTotals.IncrementLines(LayoutLevelEnum.Bank);
                                ++objTotals.TotalBankRecords;

                                arFileLines.Add(strLayoutText);
                            }
                        }
                    }
                }

                // File Trailers
                foreach (cLayout layout in extractDef.FileLayouts)
                {
                    if (layout.LayoutType == LayoutTypeEnum.Trailer)
                    {
                        if (layout.FormatLayout(BankSummary, extractDef, parms, standardFields, objCounters, out strLayoutText))
                        {
                            objCounters.IncrementLines(LayoutLevelEnum.File);
                            objTotals.IncrementLines(LayoutLevelEnum.File);
                            arFileLines.Add(strLayoutText);
                        }
                    }
                }

                totalRecords = objCounters.TotalRecords;

                objCounters.InitializeCounts(LayoutLevelEnum.File, 1);
                ++objCounters.FileIndex;
                objCounters.ResetLines(LayoutLevelEnum.File);

                // File Headers
                foreach (cLayout layout in extractDef.FileLayouts)
                {
                    if (layout.LayoutType == LayoutTypeEnum.Header)
                    {
                        if (layout.FormatLayout(BankSummary, extractDef, parms, standardFields, objCounters, out strLayoutText))
                        {
                            arFileHeader.Add(strLayoutText);
                        }
                    }
                }

                if (arFileHeader.Count > 0)
                {
                    arFileLines.InsertRange(0, arFileHeader);
                }

                bolRetVal = true;

                if (extractDef.PostProcCodeModules.Count > 0)
                {
                    bolRetVal = PostProcessFileRows(extractDef, arFileLines, out fileRows);
                }
                else
                {
                    fileRows = arFileLines;
                }
            }
            catch (Exception e)
            {
                OnOutputError(this, e);

                fileRows = null;

                bolRetVal = false;
            }

            uniqueBatchKeys = arBatches.ToArray();

            // Reload Counters with accumulated totals
            objCounters.BankCount = objTotals.BankCount;
            objCounters.BatchCount = objTotals.BatchCount;
            objCounters.CheckCount = objTotals.CheckCount;
            objCounters.CustomerCount = objTotals.CustomerCount;
            objCounters.DocumentCount = objTotals.DocumentCount;
            objCounters.FileCount = objTotals.FileCount;
            objCounters.JointDetailCount = objTotals.JointDetailCount;
            objCounters.LockboxCount = objTotals.LockboxCount;
            objCounters.StubCount = objTotals.StubCount;
            objCounters.TransactionCount = objTotals.TransactionCount;

            objCounters.TotalBankRecords = objTotals.TotalBankRecords;
            objCounters.TotalBatchRecords = objTotals.TotalBatchRecords;
            objCounters.TotalCheckRecords = objTotals.TotalCheckRecords;
            objCounters.TotalCustomerRecords = objTotals.TotalCustomerRecords;
            objCounters.TotalDocumentRecords = objTotals.TotalDocumentRecords;
            objCounters.TotalFileRecords = objTotals.TotalFileRecords;
            objCounters.TotalJointDetailRecords = objTotals.TotalJointDetailRecords;
            objCounters.TotalLockboxRecords = objTotals.TotalLockboxRecords;
            objCounters.TotalStubRecords = objTotals.TotalStubRecords;
            objCounters.TotalTransactionRecords = objTotals.TotalTransactionRecords;

            counters = objCounters;

            return (bolRetVal);
        }

        private bool PostProcessFileRows(
            cExtractDef extractDef,
            List<string> fileRows,
            out List<string> processedFileRows)
        {

            string[] arAssReferences;
            CompilerParameters objParms;
            CodeDomProvider provider;
            CompilerResults objResults;
            Assembly objAssembly;
            object objInstance;
            List<string> arRetVal;
            bool bolRetVal = true;
            object[] objCodeParms;
            string strMsg;

            arRetVal = fileRows;

            foreach (cPostProcCodeModule codemodule in extractDef.PostProcCodeModules)
            {
                var hashmatch = false;
                var hashint = 0;

                // Check the new and old hashes for security.
                if (HashGenerator.SHA512(codemodule.SrcCode) == codemodule.HashCode)
                    hashmatch = true;
                if (!hashmatch && int.TryParse(codemodule.HashCode, out hashint) && hashint == codemodule.SrcCode.GetHashCode())
                    hashmatch = true;

                // Error out if the code module changed.
                if (!hashmatch)
                    throw (new Exception("Code module has been modified outside of the Extract Wizard editor.  Please see an administrator."));

                // Configure a CompilerParameters that links System.dll
                // and produces the specified executable file.
                arAssReferences = new string[] { "System.dll" };
                objParms = new CompilerParameters(arAssReferences);
                objParms.GenerateInMemory = true;

                // Compile the source file into an executable output file.
                provider = CodeDomProvider.CreateProvider("CSharp");

                // Invoke compilation.
                objResults = provider.CompileAssemblyFromSource(objParms, codemodule.SrcCode);

                if (objResults.Errors.Count > 0)
                {

                    // Display compilation errors.

                    strMsg = "Errors encountered while building the Post-Processing Code Module.\n\n";

                    foreach (CompilerError err in objResults.Errors)
                    {
                        strMsg += "\r\nError Number: " + err.ErrorNumber.ToString() +
                                  "\r\nLine: " + err.Line.ToString() +
                                  "\r\nColumn: " + err.Column.ToString() +
                                  "\r\nError Text: " + err.ErrorText +
                                  "\n\n";
                    }

                    OnOutputError(this, new Exception(strMsg));
                    bolRetVal = false;

                }
                else
                {

                    objAssembly = objResults.CompiledAssembly;

                    // Retrieve an object reference - since this object is 'dynamic' we can't explicitly
                    // type it so it's of type Object
                    objInstance = objAssembly.CreateInstance("WFS.RecHub.DataOutputToolkit.Extract.CodeModule.PostProcessing");
                    if (objInstance == null)
                    {
                        OnOutputError("Could not load PostProcessing class in Post-Processing Code Module.", "ExtractAPI");
                        bolRetVal = false;
                    }

                    objCodeParms = new object[1];
                    objCodeParms[0] = arRetVal;

                    try
                    {
                        arRetVal = (List<string>)objInstance.GetType().InvokeMember("ProcessExtract",
                                                                                    BindingFlags.InvokeMethod,
                                                                                    null,
                                                                                    objInstance,
                                                                                    objCodeParms);
                    }
                    catch (Exception ex)
                    {
                        // DLD - WI 130165 - passing entire Exception object instead of just message
                        bolRetVal = false;
                        OnOutputError(this, ex);
                    }
                }
            }

            processedFileRows = arRetVal;

            return (bolRetVal);
        }

        private bool WriteExtractFile(
            cExtractDef extractDef,
            List<string> fileLines,
            out string tempFileName)
        {

            string strTempFileName;

            tempFileName = string.Empty;

            strTempFileName = Path.GetTempFileName();
            FileInfo fi = new FileInfo(strTempFileName);
            StreamWriter sw = fi.CreateText();
            string str;

            try
            {

                for (int i = 0; i < fileLines.Count; ++i)
                {
                    str = fileLines[i].ToString();
                    sw.Write(str);
                    //if(i<FileLines.Count -1) {
                    sw.Write(extractDef.FormattedRecordDelim);
                    //}
                }

                sw.Close();
                sw.Dispose();

                tempFileName = strTempFileName;

                return (true);

            }
            catch (Exception e)
            {
                OnOutputError(this, e);

                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }

                if (File.Exists(strTempFileName))
                {
                    // DLD - WI 130165
                    try { File.Delete(strTempFileName); }
                    catch (Exception ex)
                    {
                        OnOutputError(this, ex);
                    }
                }

                return (false);
            }
        }

        private bool WriteExtractImages(
            OrderedDictionary banks,
            cImageFormatOptions imageFormatOptions,
            out List<string> extractImages)
        {

            bool bolRetVal;
            List<cExtractImageFileDef> arExtractImageFileDef;
            string strOutputFileName;
            List<string> arExtractImages = new List<string>();

            int intTotalImages = 0;
            int intCurrentImageIndex = 0;

            if (GetExtractImageFileDef(banks, imageFormatOptions, out arExtractImageFileDef))
            {

                foreach (cExtractImageFileDef filedef in arExtractImageFileDef)
                {
                    intTotalImages += filedef.ImageArray.Count;
                }

                OnImageStatusInitialized(intTotalImages);

                // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                foreach (cExtractImageFileDef filedef in arExtractImageFileDef)
                {
                    if (filedef.ImageArray.Count > 0)
                    {
                        switch (imageFormatOptions.ImageFileFormat)
                        {
                            case ImageFileFormatEnum.Pdf:
                                if (filedef.ImageArray.Count == 1)
                                {
                                    WriteSingleImage(filedef.ImageArray[0], imageFormatOptions, filedef.DestFileName, imageFormatOptions.IncludeFooter, out strOutputFileName);
                                    arExtractImages.Add(strOutputFileName);
                                }
                                else
                                {
                                    WriteMultiPageImage(filedef.ImageArray, imageFormatOptions, filedef.DestFileName, out strOutputFileName);
                                    arExtractImages.Add(strOutputFileName);
                                }
                                break;
                            case ImageFileFormatEnum.Tiff:
                                if (filedef.ImageArray.Count == 1)
                                {
                                    WriteSingleImage(filedef.ImageArray[0], imageFormatOptions, filedef.DestFileName, imageFormatOptions.IncludeFooter, out strOutputFileName);
                                    arExtractImages.Add(strOutputFileName);
                                }
                                else
                                {
                                    WriteMultiPageImage(filedef.ImageArray, imageFormatOptions, filedef.DestFileName, out strOutputFileName);
                                    arExtractImages.Add(strOutputFileName);
                                }
                                break;
                            case ImageFileFormatEnum.SendToPrinter:
                                List<string> listImages = new List<string>();
                                // DLD - TODO - WI 129719 - IMAGES DIRECT TO PRINTER
                                //foreach (cImageType ctImage in filedef.ImageArray)
                                //    listImages.Add(ctImage.OriginalFilePath);
                                //cPrinter.PrintImages(listImages.ToArray(), imageFormatOptions.PrinterName, filedef.DestFileName);
                                var images = new List<byte[]>();
                                filedef.ImageArray.ForEach(o => images.Add(o.myImageAsByte));
                                cPrinter.PrintImages(images, imageFormatOptions.PrinterName, filedef.DestFileName);
                                break;
                            default:
                                break;
                        }

                        filedef.ImageArray.ForEach(i => i?.myImage?.Dispose());
                    }

                    OnImageStatusChanged(intCurrentImageIndex += filedef.ImageArray.Count);
                }
                // END WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF

                extractImages = arExtractImages;
                bolRetVal = true;
            }
            else
            {
                extractImages = null;
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        // WI 129719 - moved to its own class file
        //private class cExtractImageFileDef {
        //}
        // END WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF

        private bool GetExtractImageFileDef(
            OrderedDictionary banks,
            cImageFormatOptions imageFormatOptions,
            out List<cExtractImageFileDef> imageFileDefList)
        {

            List<cImageType> arTxnFiles;
            List<cImageType> arBatchFiles;

            int intTotalImages = 0;
            int intCurrentImageIndex = 0;

            List<cExtractImageFileDef> arRetVal = new List<cExtractImageFileDef>();

            foreach (cBank bank in banks.Values)
            {
                intTotalImages += bank.CheckCount;
                intTotalImages += bank.DocumentCount;
            }

            OnImageStatusInitialized(intTotalImages);

            try
            {
                // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                foreach (cBank bank in banks.Values)
                {

                    foreach (cClientAccount lockbox in bank.Lockboxes.Values)
                    {

                        foreach (cBatch batch in lockbox.Batches.Values)
                        {

                            arBatchFiles = new List<cImageType>();

                            foreach (cTransaction transaction in batch.Transactions.Values)
                            {

                                arTxnFiles = new List<cImageType>();

                                foreach (cPayment check in transaction.Payments.Values)
                                {
                                    if (check.FrontImage != null) // if(Path.GetFileName(check.ImageFPath).Length > 0) {
                                    {
                                        if (check.ExtractImageFront && check.FormattedImageFName.Length > 0)
                                        {
                                            arRetVal.Add(
                                                new cExtractImageFileDef(
                                                    new cImageType(check.FrontImage, check.ItemKey, true),
                                                    check.FormattedImageFName));
                                        }
                                        if (transaction.ExtractTransactionChecks)
                                        {
                                            arTxnFiles.Add(new cImageType(check.FrontImage, check.ItemKey, true));
                                        }
                                        if (batch.ExtractBatchChecks)
                                        {
                                            arBatchFiles.Add(new cImageType(check.FrontImage, check.ItemKey, true));
                                        }
                                    }

                                    if (check.BackImage != null) // if(Path.GetFileName(check.ImageBPath).Length > 0) {
                                    {
                                        if (check.ExtractImageBack && check.FormattedImageBName.Length > 0)
                                        {
                                            arRetVal.Add(
                                                new cExtractImageFileDef(
                                                    new cImageType(check.BackImage, check.ItemKey, true),
                                                    check.FormattedImageBName));
                                        }
                                        if (transaction.ExtractTransactionChecks)
                                        {
                                            arTxnFiles.Add(new cImageType(check.BackImage, check.ItemKey, true));
                                        }
                                        if (batch.ExtractBatchChecks)
                                        {
                                            arBatchFiles.Add(new cImageType(check.BackImage, check.ItemKey, true));
                                        }
                                    }
                                    OnImageStatusChanged(++intCurrentImageIndex);
                                }

                                foreach (cDocument document in transaction.Documents.Values)
                                {
                                    if (document.FrontImage != null)
                                    // if(Path.GetFileName(document.ImageFPath).Length > 0) {
                                    {
                                        if (document.ExtractImageFront && document.FormattedImageFName.Length > 0)
                                        {
                                            arRetVal.Add(
                                                new cExtractImageFileDef(
                                                    new cImageType(document.FrontImage, document.ItemKey, false),
                                                    document.FormattedImageFName));
                                        }
                                        if (transaction.ExtractTransactionDocuments)
                                        {
                                            arTxnFiles.Add(new cImageType(document.FrontImage, document.ItemKey, false));
                                        }
                                        if (batch.ExtractBatchDocuments)
                                        {
                                            arBatchFiles.Add(new cImageType(document.FrontImage, document.ItemKey, false));
                                        }
                                    }

                                    if (document.BackImage != null)
                                    // if(Path.GetFileName(document.ImageBPath).Length > 0) {
                                    {
                                        if (document.ExtractImageBack && document.FormattedImageBName.Length > 0)
                                        {
                                            arRetVal.Add(
                                                new cExtractImageFileDef(
                                                    new cImageType(document.BackImage, document.ItemKey, false),
                                                    document.FormattedImageBName));
                                        }
                                        if (transaction.ExtractTransactionDocuments)
                                        {
                                            arTxnFiles.Add(new cImageType(document.BackImage, document.ItemKey, false));
                                        }
                                        if (batch.ExtractBatchDocuments)
                                        {
                                            arBatchFiles.Add(new cImageType(document.BackImage, document.ItemKey, false));
                                        }
                                    }
                                    OnImageStatusChanged(++intCurrentImageIndex);
                                }

                                if (arTxnFiles.Count > 0)
                                {
                                    arRetVal.Add(new cExtractImageFileDef(arTxnFiles, transaction.FormattedImageName));
                                }
                            }

                            if (arBatchFiles.Count > 0)
                            {
                                arRetVal.Add(new cExtractImageFileDef(arBatchFiles, batch.FormattedImageName));
                            }
                        }
                    }
                }

                // END WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF

                imageFileDefList = arRetVal;

                return (true);

            }
            catch (Exception e)
            {
                OnOutputError(this, e);

                imageFileDefList = null;

                return (false);
            }
        }

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        private void WriteSingleImage(
            cImageType itImage,
            cImageFormatOptions imageFormatOptions,
            string destBaseFileName,
            bool includeFooter,
            out string outputFileName)
        {

            //string strOutputFileName;

            // DLD - TODO - WI 129719 - Image processing
            if (itImage.myImageAsByte != null) // if(File.Exists(itImage.OriginalFilePath)) 
            {
                switch (imageFormatOptions.ImageFileFormat)
                {
                    case ImageFileFormatEnum.Tiff:
                        // DLD - TODO - WI 129719 - Image processing

                        // prepare file save path
                        destBaseFileName = Path.Combine(Settings.QualifyPath(imageFormatOptions.FormattedImagePath), destBaseFileName);
                        // save file. this call detects file type and appends the file extention.
                        outputFileName = ImageUtil.SaveImage(itImage.myImageAsByte, destBaseFileName);

                        break;
                    case ImageFileFormatEnum.Pdf:
                        //// WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF

                        // Append the ".pdf" extension to the base file name.
                        destBaseFileName = (destBaseFileName.Length > 0 ? destBaseFileName + ".pdf" : string.Empty);

                        // Prepend the full image path to the file name.
                        destBaseFileName = Path.Combine(Settings.QualifyPath(imageFormatOptions.FormattedImagePath), destBaseFileName);

                        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                        List<cImageType> listImages = new List<cImageType>();
                        listImages.Add(itImage);
                        PdfLib.PrintImagePdf(
                            listImages,
                            destBaseFileName,
                            destBaseFileName,
                            includeFooter,
                            ExtractEngineDAL);

                        outputFileName = destBaseFileName;
                        break;
                    default:
                        outputFileName = string.Empty;
                        break;
                }
            }
            else
            {
                outputFileName = string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SrcFileName"></param>
        /// <param name="DestDir"></param>
        /// <param name="DestFileName"></param>
        /// <returns></returns>
        private static void CopyFile(
            string srcFileName,
            string destDir,
            string destFileName)
        {

            int iPos;
            string strDestFileName;

            if (File.Exists(srcFileName))
            {
                if (destFileName.Length > 0)
                {
                    if (File.Exists(Path.Combine(destDir, destFileName)))
                    {
                        File.Delete(Path.Combine(destDir, destFileName));
                    }
                    else if (!Directory.Exists(destDir))
                    {
                        Directory.CreateDirectory(destDir);
                    }
                    File.Copy(srcFileName, Path.Combine(destDir, destFileName));
                }
                else
                {
                    iPos = srcFileName.LastIndexOf(@"\") + 1;
                    if (iPos > -1)
                    {
                        strDestFileName = Path.Combine(destDir, srcFileName.Substring(iPos));

                        if (File.Exists(strDestFileName))
                        {
                            File.Delete(strDestFileName);
                        }
                        else if (!Directory.Exists(Path.GetDirectoryName(strDestFileName)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(strDestFileName));
                        }
                        File.Copy(srcFileName, strDestFileName);
                    }
                }
            }
        }

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        private void WriteMultiPageImage(
            List<cImageType> itImages,
            cImageFormatOptions imageFormatOptions,
            string destBaseFileName,
            out string outputFileName)
        {

            string strOutputFileName;

            if (itImages.Count > 0)
            {

                switch (imageFormatOptions.ImageFileFormat)
                {
                    case ImageFileFormatEnum.Tiff:

                        // Append the ".tif" extension to the base file name.
                        strOutputFileName = Path.Combine(Settings.QualifyPath(imageFormatOptions.FormattedImagePath), destBaseFileName) + ".tif";

                        // Prepend the full image path to the file name.
                        strOutputFileName = Path.Combine(Settings.QualifyPath(imageFormatOptions.FormattedImagePath), strOutputFileName);

                        if (File.Exists(strOutputFileName))
                        {
                            File.Delete(strOutputFileName);
                        }

                        //List<string> listImages = new List<string>();
                        // DLD - TODO - WI 129719 - Image processing
                        //foreach (cImageType itImage in itImages)
                        //    listImages.Add(itImage.OriginalFilePath);

                        //ImageUtil.CreateMultiPageTiff(listImages, strOutputFileName);

                        var images = new List<System.Drawing.Image>();
                        itImages.ForEach(o => images.Add(o.myImage));

                        ImageUtil.CreateMultiPageTiff(images, strOutputFileName);
                        outputFileName = strOutputFileName;
                        // WI Added 129559 checks to ensure value being returned was a valid image path. If not, value was set to String.Empty, captured and ignored later.
                        if (String.IsNullOrWhiteSpace(outputFileName))
                            OnOutputMessage(String.Format("Image file path not set. {0} : {1} : {2}", destBaseFileName, outputFileName, itImages[0].ItemKey), "ImageUtil.CreateMultiPageTiff", MessageType.Warning, MessageImportance.Verbose);
                        break;
                    case ImageFileFormatEnum.Pdf:

                        // Append the ".pdf" extension to the base file name.
                        strOutputFileName = Path.Combine(Settings.QualifyPath(imageFormatOptions.FormattedImagePath), destBaseFileName) + ".pdf";

                        // Prepend the full image path to the file name.
                        strOutputFileName = Path.Combine(Settings.QualifyPath(imageFormatOptions.FormattedImagePath), strOutputFileName);

                        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                        PdfLib.PrintImagePdf(itImages,
                                             destBaseFileName,
                                             strOutputFileName,
                                             imageFormatOptions.IncludeFooter,
                                             ExtractEngineDAL);

                        outputFileName = strOutputFileName;
                        break;
                    default:
                        outputFileName = string.Empty;
                        break;
                }
            }
            else
            {
                outputFileName = string.Empty;
            }
        }

        private bool WriteExtractAuditData(
            OrderedDictionary banks,
            IExtractParms parms,
            IExtractDef EextractDef,
            int totalRecords,
            Guid extractAuditID,
            string formattedTargetFileName)
        {

            int intCheckCount = 0;
            int intStubCount = 0;

            Decimal decPaymentTotal = 0;
            Decimal decStubTotal = 0;

            string strFileName;

            bool bolAnyWorkgroupElementWasExtracted = false;
            bool bolRetVal = true;

            try
            {

                foreach (cBank bank in banks.Values)
                {
                    intCheckCount += bank.CheckCount;
                    intStubCount += bank.StubCount;

                    decPaymentTotal += bank.SumPaymentsAmount;
                    decStubTotal += bank.SumStubsAmount;
                }

                // if this is a relative path
                if (formattedTargetFileName.StartsWith("."))
                {
                    strFileName = Path.Combine(Settings.SetupPath, formattedTargetFileName);
                }
                else
                {
                    strFileName = formattedTargetFileName;
                }

                bolAnyWorkgroupElementWasExtracted = banks.Values.Cast<cBank>().Any(x => x.AnyWorkgroupElementWasExtracted);

                if (bolAnyWorkgroupElementWasExtracted)
                {


                    // The name of the object contained in this loop has changed several times.
                    // What was Lockbox was renamed to ClientAccount was renamed to Workgroup.
                    // They are all the same thing, though Workgroup is the most appropriate label.
                    foreach (cClientAccount workgroup in banks.Values.Cast<cBank>()
                        .Where(bankfiltered => bankfiltered.AnyWorkgroupElementWasExtracted)
                                    .SelectMany(workgroup => workgroup.Lockboxes.Values.Cast<cClientAccount>()
                                            .Where(workgroupfiltered => workgroupfiltered.AnyWorkgroupElementWasExtracted)))
                    {

                        bolRetVal = bolRetVal && ExtractEngineDAL.InsertExtractAudit(
                            extractAuditID,
                            workgroup.BankID,
                            workgroup.ClientAccountID,
                            true,
                            workgroup.CheckCount,
                            workgroup.StubCount,
                            workgroup.SumPaymentsAmount,
                            workgroup.SumStubsAmount,
                            totalRecords,
                            Path.GetFileName(strFileName));
                    }
                }
                else
                {

                    bolRetVal = ExtractEngineDAL.InsertExtractAudit(
                        extractAuditID,
                        // There are no Workgroups in the resultset.  Insert -1 for both Workgroup and Bank.
                        -1,
                        -1,
                        true,
                        intCheckCount,
                        intStubCount,
                        decPaymentTotal,
                        decStubTotal,
                        totalRecords,
                        Path.GetFileName(strFileName));
                }

            }
            catch (Exception e)
            {
                OnOutputError(this, e);
                bolRetVal = false;
            }

            return (bolRetVal);
        }


        private bool WriteExtractSequenceNumber(
            IExtractParms parms,
            cBatchKey[] uniqueBatchKeys)
        {

            bool bolRetVal;

            if (parms.ExtractSequenceNumber > 0 && uniqueBatchKeys.Length > 0)
            {
                bolRetVal = ExtractEngineDAL.WriteExtractSequenceNumber(parms, uniqueBatchKeys);
            }
            else
            {
                bolRetVal = true;
            }

            return (bolRetVal);
        }

        private bool WriteTraceField(
            IExtractParms parms,
            cStandardFields standardFields,
            cBatchKey[] uniqueBatchKeys)
        {

            bool bolRetVal = true;

            if (parms.TraceField.Length > 0 && parms.RerunList.Length == 0 && uniqueBatchKeys.Length > 0)
            {
                foreach (cBatchKey cBatchKey in uniqueBatchKeys)
                {
                    bolRetVal = ExtractEngineDAL.WriteTraceField(
                        parms,
                        standardFields,
                        cBatchKey.BatchID);
                }
            }
            else
            {
                bolRetVal = true;
            }

            return (bolRetVal);
        }

        private bool WriteAuditFileData(
            IExtractParms parms,
            IExtractDef extractDef,
            cStandardFields standardFields)
        {

            StringBuilder sbDescription = new StringBuilder();
            //int intRetVal;

            // WI 132130 DLD 03/06/2014 - Removed WorkstationID setting. Hard coded to "-1".
            try
            {
                return (ExtractEngineDAL.WriteAuditFileData(
                    parms,
                    extractDef,
                    standardFields,
                    -1));
            }
            catch (Exception e)
            {
                OnOutputError(this, e);
                return (false);
            }
        }

        //CR10943 03/30/2005 ALH - New Function for hooking in a post processing DLL
        private bool RunPostProcessingDLL(
            string extractFileName,
            cExtractDef extractDef,
            cExtractParms parms)
        {

            bool bolRetVal;
            int iPos;
            int iPos2;
            string strFunctionName;

            string strParms = string.Empty;
            string strErrMsg;

            if (extractDef.PostProcDLLFileName.Length > 0)
            {

                iPos = extractDef.PostProcDLLFileName.LastIndexOf('\\');
                iPos2 = extractDef.PostProcDLLFileName.LastIndexOf('.');

                if (iPos > -1 && iPos2 > -1 && iPos2 > iPos)
                {

                    strFunctionName = extractDef.PostProcDLLFileName.Substring(iPos + 1, extractDef.PostProcDLLFileName.Length - iPos - (extractDef.PostProcDLLFileName.Length - iPos2 + 1));

                    strParms += "<BANKID>" + parms.BankIDString;
                    strParms += "<LOCKBOXID>" + parms.LockboxIDString;
                    strParms += "<BATCHID>" + parms.BatchIDString;

                    if (parms.ProcessingDateString.Length > 0)
                    {
                        strParms += "<PROCDATE>" + parms.ProcessingDateString;
                    }
                    else
                    {
                        strParms += "<PROCDATE>" + parms.DepositDateString;
                    }

                    bolRetVal = Win32Interop.ExecutePostProcDLL(
                        extractDef.PostProcDLLFileName,
                        strFunctionName,
                        extractFileName,
                                                                strParms,
                                                                out strErrMsg);

                    if (!bolRetVal)
                    {
                        OnOutputMessage(
                            //this,
                            "Error executing post-processing dll: " + strErrMsg,
                            "cExtract.RunPostProcessingDLL",
                            MessageType.Error,
                            MessageImportance.Essential);
                    }
                }
                else
                {

                    OnOutputMessage(
                        //this,
                        "Invalid post-processing dll file name specified: [" + extractDef.PostProcDLLFileName + "].",
                        "cExtract.RunPostProcessingDLL",
                        MessageType.Warning,
                        MessageImportance.Essential);
                    bolRetVal = false;
                }
            }
            else
            {
                bolRetVal = true;
            }

            return (bolRetVal);
        }

        private void OnOutputMessage(
            //object sender,
            string msg,
            string src,
            MessageType messageType,
            MessageImportance messageImportance)
        {

            if (OutputMessage != null)
            {
                OutputMessage(
                    msg,
                    src,
                    messageType,
                    messageImportance);
            }
            else
            {
                cCommonLogLib.LogMessageLTA(msg, LTAConvert.ConvertMessageImportanceToIpdLog(messageImportance));
            }

        }

        private void OnOutputDALMessage(
            //object sender,
            string msg,
            string src,
            MessageType messageType,
            MessageImportance messageImportance)
        {
            cCommonLogLib.LogMessageLTA(msg, LTAConvert.ConvertMessageImportanceToIpdLog(messageImportance));
            OnOutputStatusMsg(msg);
        }

        private void OnOutputError(
            string msg,
            string src)
        {

            Exception e = new Exception(msg);
            e.Source = src;

            OnOutputError(this, e);
        }

        private void OnOutputError(System.Exception e)
        {
            OnOutputError(this, e);
        }

        private void OnOutputError(
            object sender,
            System.Exception e)
        {


            if (OutputError != null)
            {
                OutputError(e);
            }
            else
            {
                // WI 131640 - Better exception logs.
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }

            // Log error to Extract logging file
            OnOutputExtractLogMsg(e);

            // WI 114312 Extract Wizard Workaround - ltaLog TargetSite null exception
            // Log error to the general ExceptionLog
            //this.handle_ltaLogLogError(ExceptionLog, e);

            _LastErrorMessage = e.Message;
        }

        private void OnOutputExtractLogMsg(Exception e)
        {
            cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
        }

        private void OnOutputExtractLogMsg(string msg, MessageImportance messageImportance)
        {
            cCommonLogLib.LogMessageLTA(msg, LTAConvert.ConvertMessageImportanceToIpdLog(messageImportance));
        }

        private void OnOutputStatusMsg(string msg)
        {
            if (OutputStatusMsg != null)
            {
                OutputStatusMsg(msg);
            }
        }

        private void OnImageStatusInitialized(int maxItems)
        {
            if (ImageStatusInitialized != null)
            {
                ImageStatusInitialized(maxItems);
            }
        }

        private void OnImageStatusChanged(int currentItem)
        {
            if (ImageStatusChanged != null)
            {
                ImageStatusChanged(currentItem);
            }
        }

        private void OnPromptForManualInput(ref Dictionary<string, string> manualInputValues)
        {

            Dictionary<string, string> dicManualInputValues;

            try
            {
                if (PromptForManualInput != null)
                {
                    PromptForManualInput(ref manualInputValues);
                }
                else
                {
                    dicManualInputValues = new Dictionary<string, string>();
                    if (manualInputValues != null)
                    {
                        foreach (System.Collections.Generic.KeyValuePair<string, string> entry in manualInputValues)
                        {
                            dicManualInputValues.Add(entry.Key, string.Empty);
                        }
                    }
                    manualInputValues = dicManualInputValues;
                }
            }
            catch (Exception ex)
            {
                OnOutputError(this, ex);
                manualInputValues = new Dictionary<string, string>();
            }
        }

        #region IDisposable Members

        // CR 13307 7/19/05 MLT - implement Dispose pattern
        /// <summary>
        /// Dispose - Call this method to explictly cleanup memory
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Place any additional cleanup of internal objects 
                // inside this if statement;
                //
                // Client code has explicitly called the public Dispose method, 
                // the object has not been implicitly finalized by the CLR,
                // so it is safe to access and cleanup these internal objects

                if (_ExtractEngineDAL != null)
                {
                    _ExtractEngineDAL.Dispose();
                }
            }
        }

        #endregion

        #region Database Table Definitions
        internal Dictionary<string, IColumn> BankTableDef
        {
            get
            {
                if (_BankTableDef == null)
                {
                    _BankTableDef = GetTableDef("dimBanks");
                }
                return (_BankTableDef);
            }
        }

        internal Dictionary<string, IColumn> ClientAccountTableDef
        {
            get
            {
                if (_ClientAccountTableDef == null)
                {
                    _ClientAccountTableDef = GetTableDef("dimClientAccounts");
                }
                return (_ClientAccountTableDef);
            }
        }

        internal Dictionary<string, IColumn> BatchTableDef
        {
            get
            {
                if (_BatchTableDef == null)
                {
                    _BatchTableDef = GetTableDef("factBatchSummary");
                }
                return (_BatchTableDef);
            }
        }

        internal Dictionary<string, IColumn> TransactionsTableDef
        {
            get
            {
                if (_TransactionsTableDef == null)
                {
                    _TransactionsTableDef = GetTableDef("factTransactionSummary");
                }
                return (_TransactionsTableDef);
            }
        }

        internal Dictionary<string, IColumn> PaymentsTableDef
        {
            get
            {
                if (_PaymentsTableDef == null)
                {
                    _PaymentsTableDef = GetTableDef("factChecks");
                }
                return (_PaymentsTableDef);
            }
        }

        internal Dictionary<string, IColumn> ExtractTracesTableDef
        {
            get
            {
                if (_ExtractTracesTableDef == null)
                {
                    _ExtractTracesTableDef = GetTableDef("factExtractTraces");
                }
                return (_ExtractTracesTableDef);
            }
        }

        //internal Dictionary<string, IColumn> RemittersTableDef {
        //    get {
        //        if(_RemittersTableDef == null) {
        //            _RemittersTableDef = GetTableDef("dimRemitters");
        //        }
        //        return (_RemittersTableDef);
        //    }
        //}

        // MLH 09/20/13 have req 2.0 say that ChecksDataEntry isnow in factChecks
        internal Dictionary<string, IColumn> ChecksDETableDef
        {
            get
            {
                if (_ChecksDETableDef == null)
                {
                    _ChecksDETableDef = GetTableDef(DatabaseConstants.TABLE_NAME_CHECKSDE);
                    // _ChecksDETableDef = GetTableDef("factChecks");
                }
                return (_ChecksDETableDef);
            }
        }

        internal Dictionary<string, IColumn> StubsTableDef
        {
            get
            {
                if (_StubsTableDef == null)
                {
                    _StubsTableDef = GetTableDef("factStubs");
                }
                return (_StubsTableDef);
            }
        }

        internal Dictionary<string, IColumn> StubsDETableDef
        {
            get
            {
                if (_StubsDETableDef == null)
                {
                    _StubsDETableDef = GetTableDef("StubsDataEntry");
                }
                return (_StubsDETableDef);
            }
        }

        internal Dictionary<string, IColumn> DocumentsTableDef
        {
            get
            {
                if (_DocumentsTableDef == null)
                {
                    _DocumentsTableDef = GetTableDef("factDocuments");
                }
                return (_DocumentsTableDef);
            }
        }

        ////internal Dictionary<string, cColumn> DocsDETableDef {
        ////    get {
        ////        if(_DocsDETableDef == null) {
        ////            _DocsDETableDef = GetTableDef("DocumentsDataEntry");
        ////        }
        ////        return(_DocsDETableDef);
        ////    }
        ////}

        private Dictionary<string, IColumn> GetTableDef(string tableName)
        {

            DataTable dt;
            Dictionary<string, IColumn> dicRetVal;
            IColumn column;

            dicRetVal = new Dictionary<string, IColumn>(StringComparer.CurrentCultureIgnoreCase);
            if (ExtractEngineDAL.GetTableDef(tableName, out dt))
            {

                foreach (DataRow dr in dt.Rows)
                {
                    column = new cColumn(dr);
                    dicRetVal.Add(column.ColumnName, column);
                }

                // WI 132009 : Adding a few extra columns.
                var cols = cJoinedTablesLib.GetJoinedColumns(tableName);
                foreach (var c in cols)
                    dicRetVal.Add(string.Format("{0}.{1}.{2}", c.Schema, c.TableName, c.ColumnName), c);

                // WI 143034 : Change the IColumn fieldname from 'BatchID' to 'SourceBatchID'.
                cColumnReplacementLib.AlterColumns(cols);

                return (dicRetVal);
            }
            else
            {
                return (null);
            }
        }

        public static string DateRangeToString(DateTime minDate, DateTime maxDate)
        {
            var formatPattern = "Filter excludes all dates";
            if (minDate.Equals(maxDate))
            {
                formatPattern = "{0:M/d/yyyy}";
            }
            else if (maxDate > minDate)
            {
                formatPattern = "{0:M/d/yyyy}-{1:M/d/yyyy}";
            }
            return String.Format(formatPattern, minDate, maxDate);
        }

        public static string GenerateDateFilterTextForAlert(cExtractParms runParms)
        {
            var result = new List<string>();

            if (runParms.DepositDateFrom > DateTime.MinValue)
            {
                result.Add($"Deposit Date: {DateRangeToString(runParms.DepositDateFrom, runParms.DepositDateTo)}. ");
            }
            if (runParms.ProcessingDateFrom > DateTime.MinValue)
            {
                result.Add(
                    $"Processing Date: {DateRangeToString(runParms.ProcessingDateFrom, runParms.ProcessingDateTo)}. ");
            }
            return string.Join("\n", result);
        }
        #endregion
    }
}
