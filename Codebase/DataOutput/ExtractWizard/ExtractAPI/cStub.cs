using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    public class cStub : ObjectBase, IComparable, ISummaryFields {

        private cTransaction _Transaction;

        ////private int _GlobalBatchID;
        private int _TransactionID;
        ////private int _GlobalStubID;
        private int _BatchSequence;
        private Decimal _Amount;
        ////private int _DEBillingKeys;
        ////private int _DEDataKeys;

        private Dictionary<string, object> _StubFields;

        private IOrderByColumn[] _OrderByColumns = { };
        
        public cStub(
            cTransaction transaction, 
            ////int vGlobalBatchID,
            int transactionID, 
            ////int vGlobalStubID, 
            int batchSequence, 
            Decimal amount, 
            ////int vDEBillingKeys, 
            ////int vDEDataKeys,
            IOrderByColumn[] orderByColumns) {

            _Transaction = transaction;
            ////_GlobalBatchID = vGlobalBatchID;
            _TransactionID = transactionID;
            ////_GlobalStubID = vGlobalStubID;
            _BatchSequence = batchSequence;
            _Amount = amount;
            ////_DEBillingKeys = vDEBillingKeys;
            ////_DEDataKeys = vDEDataKeys;

            _OrderByColumns = orderByColumns;

            _StubFields = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
        }

        public cTransaction Transaction {
            get {
                return(_Transaction);
            }
        }

        //public int GlobalBatchID {
        //    get {
        //        return(_GlobalBatchID);
        //    }
        //}

        public int TransactionID {
            get {
                return(_TransactionID);
            }
        }
        
        //public int GlobalStubID {
        //    get {
        //        return(_GlobalStubID);
        //    }
        //}

        public string UniqueStubKey {
            get {
                return (Transaction.Batch.BankID.ToString() + "_" + Transaction.Batch.ClientAccountID.ToString() + "_" + Transaction.Batch.BatchID.ToString() + "_" + Transaction.Batch.ImmutableDateKey.ToString() + "_" + BatchSequence.ToString());
            }
        }

        public int BatchSequence {
            get {
                return(_BatchSequence);
            }
        }

        public Decimal Amount {
            get {
                return(_Amount);
            }
        }

        ////public int DEBillingKeys {
        ////    get {
        ////        return(_DEBillingKeys);
        ////    }
        ////}

        ////public int DEDataKeys {
        ////    get {
        ////        return(_DEDataKeys);
        ////    }
        ////}

        public Dictionary<string, object> StubFields {
            get {
                return(_StubFields);
            }
        }

        public static Hashtable DefaultFields() {

            Hashtable htFieldNames = new Hashtable();

            htFieldNames.Add("BankID", string.Empty);
            htFieldNames.Add("CustomerID", string.Empty);
            htFieldNames.Add("LockboxID", string.Empty);
            ////htFieldNames.Add("GlobalBatchID", string.Empty);
            htFieldNames.Add("TransactionID", string.Empty);
            ////htFieldNames.Add("GlobalStubID", string.Empty);
            htFieldNames.Add("BatchSequence", string.Empty);
            htFieldNames.Add("Amount", string.Empty);
            ////htFieldNames.Add("DEBillingKeys", string.Empty);
            ////htFieldNames.Add("DEDataKeys", string.Empty);

            return(htFieldNames);
        }

        public int BatchCount { 
            get { return(1); } 
        }

        public int TransactionCount { 
            get { return(1); } 
        }

        public int CheckCount { 
            get { return(_Transaction.CheckCount); } 
        }

        public int StubCount { 
            get { return(_Transaction.StubCount); } 
        }

        public int DocumentCount { 
            get { return(_Transaction.DocumentCount); } 
        }


        public Decimal SumPaymentsAmount {
            get { return(0); }
        }

        public int SumPaymentsDEBillingKeys {
            get { return(0); }
        }

        public int SumPaymentsDEDataKeys {
            get { return(0); }
        }
        
        public Decimal SumStubsAmount {
            get { return(this.Amount); }
        }

        ////public int SumStubsDEBillingKeys {
        ////    get { return(this.DEBillingKeys); }
        ////}

        ////public int SumStubsDEDataKeys {
        ////    get { return(this.DEDataKeys); }
        ////}

        public int CompareTo(object obj) {

            int intRetVal = 0;

            if(obj is cStub) {

                cStub stub = (cStub) obj;

                foreach(IOrderByColumn orderbycolumn in _OrderByColumns) {
                    
                    switch(orderbycolumn.ColumnName.ToLower()) {
                        ////case "globalbatchid":
                        ////    if(this.GlobalBatchID != stub.GlobalBatchID) {
                        ////        intRetVal = this.GlobalBatchID.CompareTo(stub.GlobalBatchID);
                        ////    }
                        ////    break;
                        case "transactionid":
                            if(this.TransactionID != stub.TransactionID) {
                                intRetVal = this.TransactionID.CompareTo(stub.TransactionID);
                            }
                            break;
                        ////case "globalstubid":
                        ////    if(this.GlobalStubID != stub.GlobalStubID) {
                        ////        intRetVal = this.GlobalStubID.CompareTo(stub.GlobalStubID);
                        ////    }
                        ////    break;
                        case "batchsequence":
                            if(this.BatchSequence != stub.BatchSequence) {
                                intRetVal = this.BatchSequence.CompareTo(stub.BatchSequence);
                            }
                            break;
                        case "amount":
                            if(this.Amount != stub.Amount) {
                                intRetVal = this.Amount.CompareTo(stub.Amount);
                            }
                            break;
                        ////case "debillingkeys":
                        ////    if(this.DEBillingKeys != stub.DEBillingKeys) {
                        ////        intRetVal = this.DEBillingKeys.CompareTo(stub.DEBillingKeys);
                        ////    }
                        ////    break;
                        ////case "dedatakeys":
                        ////    if(this.DEDataKeys != stub.DEDataKeys) {
                        ////        intRetVal = this.DEDataKeys.CompareTo(stub.DEDataKeys);
                        ////    }
                        ////    break;
                        default:
                            foreach(string fieldname in this.StubFields.Keys) {
                                if(orderbycolumn.ColumnName.ToLower() == fieldname.ToLower()) {
                                    intRetVal = Compare(this.StubFields, stub.StubFields, fieldname);

                                    if(intRetVal != 0) {
                                        break;
                                    }
                                }
                            }
                            break;
                    }

                    if(intRetVal != 0) {
                        if(orderbycolumn.OrderByDir == OrderByDirEnum.Descending) {
                            intRetVal*=-1;
                        }
                        break;
                    }
                }

            } else {
                throw new ArgumentException("object is not a Stub");    
            }

            return(intRetVal);
        }
    }
}
