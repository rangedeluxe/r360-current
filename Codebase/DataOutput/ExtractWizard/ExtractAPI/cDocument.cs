using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using WFS.RecHub.Common;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 129559 DLD 02/18/2014 
*   -PICS.getImagePath sometimes fails and returns the base image folder 'path' as the new image file 'name'.   
* WI 129719 DLD 02/24/2014
*   -Converting image repository from ipoPics.dll to OLFServices 
* WI 152321 BLR 08/15/2014
*   -Moved Configuration over to ExtractConfigurationSettings. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    internal class cDocument : cExtractImageBase, IComparable {

        private int _DocumentSequence;
        private Dictionary<string, object> _DocumentFields;
        
        public cDocument(
            ExtractConfigurationSettings settings,
            cTransaction transaction, 
            ////int vGlobalBatchID, 
            int transactionID, 
            ////int vGlobalDocumentID, 
            int batchSequence, 
            int documentSequence, 
            string fileDescriptor,
            IOrderByColumn[] orderByColumns) {

            Settings = settings;

            Transaction = transaction;
            TransactionID = transactionID;
            BatchSequence = batchSequence;
            _DocumentSequence = documentSequence;
            FileDescriptor = fileDescriptor;

            OrderByColumns = orderByColumns;

            _DocumentFields = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
        }

        public int DocumentSequence {
            get {
                return(_DocumentSequence);
            }
        }

        public Dictionary<string, object> DocumentFields {
            get {
                return(_DocumentFields);
            }
        }

        public static Hashtable DefaultFields() {

            Hashtable htFieldNames = new Hashtable();

            htFieldNames.Add("BankID", string.Empty);
            htFieldNames.Add("CustomerID", string.Empty);
            htFieldNames.Add("LockboxID", string.Empty);
            ////htFieldNames.Add("GlobalBatchID", string.Empty);
            htFieldNames.Add("TransactionID", string.Empty);
            ////htFieldNames.Add("GlobalDocumentID", string.Empty);
            htFieldNames.Add("BatchSequence", string.Empty);
            htFieldNames.Add("DocumentSequence", string.Empty);
            htFieldNames.Add("FileDescriptor", string.Empty);

            return(htFieldNames);
        }

        public int CompareTo(object obj) {

            int intRetVal = 0;

            if(obj is cDocument) {

                cDocument doc = (cDocument) obj;

                foreach(IOrderByColumn orderbycolumn in OrderByColumns) {
                    
                    switch(orderbycolumn.ColumnName.ToLower()) {
                        ////case "globalbatchid":
                        ////    if(this.GlobalBatchID != doc.GlobalBatchID) {
                        ////        intRetVal = this.GlobalBatchID.CompareTo(doc.GlobalBatchID);
                        ////    }
                        ////    break;
                        case "transactionid":
                            if(this.TransactionID != doc.TransactionID) {
                                intRetVal = this.TransactionID.CompareTo(doc.TransactionID);
                            }
                            break;
                        ////case "globaldocumentid":
                        ////    if(this.GlobalDocumentID != doc.GlobalDocumentID) {
                        ////        intRetVal = this.GlobalDocumentID.CompareTo(doc.GlobalDocumentID);
                        ////    }
                        ////    break;
                        case "batchsequence":
                            if(this.BatchSequence != doc.BatchSequence) {
                                intRetVal = this.BatchSequence.CompareTo(doc.BatchSequence);
                            }
                            break;
                        case "documentsequence":
                            if(this.DocumentSequence != doc.DocumentSequence) {
                                intRetVal = this.DocumentSequence.CompareTo(doc.DocumentSequence);
                            }
                            break;
                        case "filedescriptor":
                            if(this.FileDescriptor != doc.FileDescriptor) {
                                intRetVal = this.FileDescriptor.CompareTo(doc.FileDescriptor);
                            }
                            break;
                        default:
                            foreach(string fieldname in this.DocumentFields.Keys) {
                                if(orderbycolumn.ColumnName.ToLower() == fieldname.ToLower()) {
                                    intRetVal = Compare(this.DocumentFields, doc.DocumentFields, fieldname);

                                    if(intRetVal != 0) {
                                        break;
                                    }
                                }
                            }
                            break;
                    }

                    if(intRetVal != 0) {
                        if(orderbycolumn.OrderByDir == OrderByDirEnum.Descending) {
                            intRetVal*=-1;
                        }
                        break;
                    }
                }

            } else {
                throw new ArgumentException("object is not a Document");    
            }

            return(intRetVal);
        }
    }
}
