using System;
using System.Collections.Generic;
using System.Text;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    internal class cCounters {

        private int _CheckLines = 1;
        private int _StubLines = 1;
        private int _DocumentLines = 1;

        public cCounters() {
        }

        public int TotalFileRecords = 0;
        public int TotalBankRecords = 0;
        public int TotalCustomerRecords = 0;
        public int TotalLockboxRecords = 0;
        public int TotalBatchRecords = 0;
        public int TotalTransactionRecords = 0;
        public int TotalCheckRecords = 0;
        public int TotalStubRecords = 0;
        public int TotalDocumentRecords = 0;
        public int TotalJointDetailRecords = 0;

        public int TotalRecords {
            get {
                return(TotalFileRecords +
                       TotalBankRecords +
                       TotalCustomerRecords +
                       TotalLockboxRecords +
                       TotalBatchRecords +
                       TotalTransactionRecords +
                       TotalCheckRecords +
                       TotalStubRecords +
                       TotalDocumentRecords +
                       TotalJointDetailRecords);
            }
        }

        public int FileLines = 1;
        public int BankLines = 1;
        public int CustomerLines = 1;
        public int LockboxLines = 1;
        public int BatchLines = 1;
        public int TransactionLines = 1;

        public int CheckLines {
            get {
                //Always return 1 for compatibility with the older version.
                //return(_CheckLines);
                return(1);
            }
        }

        public int StubLines {
            get {
                //Always return 1 for compatibility with the older version.
                //return(_StubLines);
                return(1);
            }
        }
        
        public int DocumentLines {
            get {
                //Always return 1 for compatibility with the older version.
                //return(_DocumentLines);
                return(1);
            }
        }

        public int JointDetailLines = 0;
        
        public int FileIndex = -1;
        public int BankIndex = -1;
        public int CustomerIndex = -1;
        public int LockboxIndex = -1;
        public int BatchIndex = -1;
        public int TransactionIndex = -1;
        public int CheckIndex = -1;
        public int StubIndex = -1;
        public int DocumentIndex = -1;
        public int JointDetailIndex = -1;

        public int FileCount = 0;
        public int BankCount = 0;
        public int CustomerCount = 0;
        public int LockboxCount = 0;
        public int BatchCount = 0;
        public int TransactionCount = 0;
        public int CheckCount = 0;
        public int StubCount = 0;
        public int DocumentCount = 0;
        public int JointDetailCount = 0;

        public void InitializeCounts(
            LayoutLevelEnum layoutLevel, 
            int entityCount) {
            
            switch(layoutLevel) {
                case LayoutLevelEnum.File:
                    FileCount = entityCount;
                    FileIndex = -1;
                    break;
                case LayoutLevelEnum.Bank:
                    BankCount = entityCount;
                    BankIndex = -1;
                    break;
                case LayoutLevelEnum.ClientAccount:
                    LockboxCount = entityCount;
                    LockboxIndex = -1;
                    break;
                case LayoutLevelEnum.Batch:
                    BatchCount = entityCount;
                    BatchIndex = -1;
                    break;
                case LayoutLevelEnum.Transaction:
                    TransactionCount = entityCount;
                    TransactionIndex = -1;
                    break;
                case LayoutLevelEnum.Payment:
                    CheckCount = entityCount;
                    CheckIndex = -1;
                    break;
                case LayoutLevelEnum.Stub:
                    StubCount = entityCount;
                    StubIndex = -1;
                    break;
                case LayoutLevelEnum.Document:
                    DocumentCount = entityCount;
                    DocumentIndex = -1;
                    break;
                case LayoutLevelEnum.JointDetail:
                    JointDetailCount = entityCount;
                    JointDetailIndex = -1;
                    break;
            }
        }

        public FirstMidLast GetPosition(
            LayoutLevelEnum layoutLevel) {

            int intEntityCount = 0;
            int intCurrentIndex = 0;
            FirstMidLast RetVal;
            
            switch(layoutLevel) {
                case LayoutLevelEnum.File:
                    intEntityCount = FileCount;
                    intCurrentIndex = FileIndex;
                    break;
                case LayoutLevelEnum.Bank:
                    intEntityCount = BankCount;
                    intCurrentIndex = BankIndex;
                    break;
                case LayoutLevelEnum.ClientAccount:
                    intEntityCount = LockboxCount;
                    intCurrentIndex = LockboxIndex;
                    break;
                case LayoutLevelEnum.Batch:
                    intEntityCount = BatchCount;
                    intCurrentIndex = BatchIndex;
                    break;
                case LayoutLevelEnum.Transaction:
                    intEntityCount = TransactionCount;
                    intCurrentIndex = TransactionIndex;
                    break;
                case LayoutLevelEnum.Payment:
                    intEntityCount = CheckCount;
                    intCurrentIndex = CheckIndex;
                    break;
                case LayoutLevelEnum.Stub:
                    intEntityCount = StubCount;
                    intCurrentIndex = StubIndex;
                    break;
                case LayoutLevelEnum.Document:
                    intEntityCount = DocumentCount;
                    intCurrentIndex = DocumentIndex;
                    break;
                case LayoutLevelEnum.JointDetail:
                    intEntityCount = JointDetailCount;
                    intCurrentIndex = JointDetailIndex;
                    break;
            }

            if(intCurrentIndex == 0 && intEntityCount > 1) {
                RetVal = FirstMidLast.First;
            } else if(intCurrentIndex > 0 && intCurrentIndex < intEntityCount -1) {
                RetVal = FirstMidLast.Mid;
            } else {
                RetVal = FirstMidLast.Last;
            }

            return(RetVal);
        }

        public void ResetLines(LayoutLevelEnum layoutLevel) {
            
            _CheckLines = 1;
            _StubLines = 1;
            _DocumentLines = 1;
            this.JointDetailLines = 1;

            if(layoutLevel <= LayoutLevelEnum.Transaction) {
                this.TransactionLines = 1;
            }

            if(layoutLevel <= LayoutLevelEnum.Batch) {
                this.BatchLines = 1;
            }

            if(layoutLevel <= LayoutLevelEnum.ClientAccount) {
                this.LockboxLines = 1;
            }

            if(layoutLevel <= LayoutLevelEnum.Bank) {
                this.BankLines = 1;
            }

            if(layoutLevel == LayoutLevelEnum.File) {
                this.FileLines = 1;
            }
        }

        public void IncrementLines(LayoutLevelEnum layoutLevel) {
            
            ++this.FileLines;

            if(layoutLevel >= LayoutLevelEnum.Bank) {
                ++this.BankLines;
            }

            if(layoutLevel >= LayoutLevelEnum.ClientAccount) {
                ++this.LockboxLines;
            }

            if(layoutLevel >= LayoutLevelEnum.Batch) {
                ++this.BatchLines;
            }

            if(layoutLevel >= LayoutLevelEnum.Transaction) {
                ++this.TransactionLines;
            }

            if(layoutLevel == LayoutLevelEnum.Payment) {
                ++_CheckLines;
            }

            if(layoutLevel == LayoutLevelEnum.Stub) {
                ++_StubLines;
            }

            if(layoutLevel == LayoutLevelEnum.Document) {
                ++_DocumentLines;
            }
        }
    }
}
