using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 269406 JMC 03/10/2016
*   - Added WasExtracted property to track whether a Layout was actually 
*     written to the Extract Data File.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    public abstract class ObjectBase {

        public bool WasExtracted = false;

        protected int Compare(
            Dictionary<string, object> table1, 
            Dictionary<string, object> table2, 
            string fieldName) {

            int intRetVal = 0;

            if(table1.ContainsKey(fieldName) && table2.ContainsKey(fieldName)) {
                
                if(table1[fieldName] == null && table2[fieldName] != null) {
                    intRetVal = -1;
                } else if(table1[fieldName] != null && table2[fieldName] == null) {
                    intRetVal = 1;
                } else if(table1[fieldName] == null && table2[fieldName] == null) {
                    intRetVal = 0;
                } else {
                    switch(table1[fieldName].GetType().FullName) {
                        case "System.Boolean":
                            intRetVal = ((bool)table1[fieldName]).CompareTo(((bool)table2[fieldName]));
                            break;
                        case "System.Decimal":
                            intRetVal = ((Decimal)table1[fieldName]).CompareTo(((Decimal)table2[fieldName]));
                            break;
                        case "System.Single":
                            intRetVal = ((Decimal)table1[fieldName]).CompareTo(((Decimal)table2[fieldName]));
                            break;
                        case "System.Double":
                            intRetVal = ((double)table1[fieldName]).CompareTo(((double)table2[fieldName]));
                            break;
                        case "System.Guid":
                            intRetVal = ((Guid)table1[fieldName]).CompareTo(((Guid)table2[fieldName]));
                            break;
                        case "System.DateTime":
                            intRetVal = ((DateTime)table1[fieldName]).CompareTo(((DateTime)table2[fieldName]));
                            break;
                        case "System.Int16":
                            intRetVal = ((Int16)table1[fieldName]).CompareTo(((Int16)table2[fieldName]));
                            break;
                        case "System.Int32":
                            intRetVal = ((Int32)table1[fieldName]).CompareTo(((Int32)table2[fieldName]));
                            break;
                        case "System.Byte":
                            intRetVal = ((byte)table1[fieldName]).CompareTo(((byte)table2[fieldName]));
                            break;
                        case "System.String":
                            intRetVal = table1[fieldName].ToString().CompareTo(table2[fieldName].ToString());
                            break;
                        default:
                            intRetVal = table1[fieldName].ToString().CompareTo(table2[fieldName].ToString());
                            break;
                    }
                }
            }

            return(intRetVal);
        }
    }
}
