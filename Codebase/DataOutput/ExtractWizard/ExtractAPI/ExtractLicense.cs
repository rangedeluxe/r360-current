using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public static class ExtractLicense {

        ////public static bool CanRun(string ExtractPath) {
        ////    return(ExtractFileCount(ExtractPath) <= ReadMaxExtracts());
        ////}
        
        ////public static bool CanSave(string ExtractPath) {
        ////    return(ExtractFileCount(ExtractPath) < ReadMaxExtracts());
        ////}

        private static int ExtractFileCount(string extractPath) {

            int intRetVal = 0;

            foreach(string filename in Directory.GetFiles(extractPath)) {
                if(filename.EndsWith(".cxs")||filename.EndsWith(".xml")) {
                    ++intRetVal;
                }
            }
            
            return(intRetVal);
        }

        ////public static int ReadMaxExtracts() {

        ////    return(ReadMaxExtracts(Path.Combine(Assembly.GetExecutingAssembly().Location.Substring(0,                                                                                                   Assembly.GetExecutingAssembly().Location.LastIndexOf('\\')), 
        ////                           "License.dat")));
        ////}

        ////private static int ReadMaxExtracts(string FilePath) {

        ////    string strLicenseString;
        ////    int intRetVal = -1;
        ////    int intTemp;
        ////    string strMaxExtracts;
        ////    cCrypto objCrypto;

        ////    if(ReadSecurityFile(FilePath, out strLicenseString) && strLicenseString.Length > 0) {

        ////        try {

        ////            objCrypto = new cCrypto();
        ////            strMaxExtracts = Encoding.ASCII.GetString(objCrypto.Decrypt(strLicenseString));

        ////            if(int.TryParse(strMaxExtracts, out intTemp)) {
        ////                intRetVal = intTemp;
        ////            } else {
        ////                intRetVal = -1;
        ////            }

        ////        } catch(Exception ex) {
        ////            string strMsg = ex.Message;
        ////            intRetVal = -1;
        ////        }
        ////    } else {
        ////        intRetVal = -1;
        ////    }
            
        ////    return(intRetVal);
        ////}

        ////private static bool ReadSecurityFile(string FilePath, out string FileText) {

        ////    StreamReader objReader;

        ////    try {
        ////        if(File.Exists(FilePath)) {
        ////            objReader = File.OpenText(FilePath);
        ////            FileText = objReader.ReadToEnd();
        ////            objReader.Close();
        ////            return(true);
        ////        } else {
        ////            FileText = string.Empty;
        ////            throw(new LicenseFileReadException());
        ////        }
        ////    } catch(Exception) {
        ////        FileText = string.Empty;
        ////        throw(new LicenseFileReadException());
        ////    }

        ////}

        ////public static string CannotRunMsg() {
        ////    return("The number of extracts in your extract directory exceeds the number permitted by your license (" + ReadMaxExtracts().ToString() + ").");
        ////}

        ////public static string CannotSaveMsg() {
        ////    return("Unable to save.  Your maximum of " + ReadMaxExtracts().ToString() + " extracts has been met.");
        ////}

        ////public const string LicenseErrorCaption = "License Error";
    }

    ////public class LicenseFileReadException : Exception {

    ////    public override string Message {
    ////        get {
    ////            return("Unable to read license file.  Please contact technical support.");
    ////        }
    ////    }
    ////}
}
