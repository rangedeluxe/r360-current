using System;
using System.Collections.Generic;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    public interface ISummaryFields {

        int BatchCount { get; }
        int TransactionCount { get; }
        int CheckCount { get; }
        int StubCount { get; }
        int DocumentCount { get; }

        Decimal SumPaymentsAmount { get; }
        ////int SumPaymentsDEBillingKeys { get; }
        ////int SumPaymentsDEDataKeys { get; }
        Decimal SumStubsAmount { get; }
        ////int SumStubsDEBillingKeys { get; }
        ////int SumStubsDEDataKeys { get; }
    }
}
