﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    public class DocumentDetails
    {
        public float ScaleHeight { get; set; }
        public float ScaleWidth { get; set; }
    }
}
