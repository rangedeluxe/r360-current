using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    internal class Win32Interop {

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate bool DoWork(string DLLFileName, string Error, string Parms);

        private class Kernel32 {

            [DllImport("kernel32.dll")]
            public static extern IntPtr LoadLibrary(string dllToLoad);

            [DllImport("kernel32.dll")]
            public static extern IntPtr GetProcAddress(IntPtr hModule, string procedureName);

            [DllImport("kernel32.dll")]
            public static extern bool FreeLibrary(IntPtr hModule);
        }

        public static bool ExecutePostProcDLL(
            string dllFileName, 
            string entryPoint, 
            string extractFileName, 
            string parms, 
            out string errMsg) {

            IntPtr pDLL;
            IntPtr pAddressOfFunctionToCall;
            DoWork work;
            string strErr;
            bool bolRetVal = false;

            try {
                pDLL = Kernel32.LoadLibrary(dllFileName);
                if(pDLL == IntPtr.Zero) {
                    errMsg = "Could not execute Win32 post-processing DLL [" + dllFileName + "].";
                    return(false);
                }

                pAddressOfFunctionToCall = Kernel32.GetProcAddress(pDLL, entryPoint);
                if(pAddressOfFunctionToCall == IntPtr.Zero) {
                    errMsg = "Could not locate entry point [" + entryPoint + "] in post-processing DLL [" + dllFileName + "].";
                    Kernel32.FreeLibrary(pDLL);
                    return(false);
                }

                work = (DoWork)Marshal.GetDelegateForFunctionPointer(pAddressOfFunctionToCall, typeof(DoWork));            

                strErr = string.Empty;

                bolRetVal = work(extractFileName, strErr, parms);

                errMsg = strErr;

                Kernel32.FreeLibrary(pDLL);

            } catch(Exception e) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);;
                errMsg = e.Message;
            }
            
            return(bolRetVal);
        }
    }
}