using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Xml;
using System.Linq;

using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   - Initial Version 
* WI 114041 MLH 09/16/2013
*   - Added check for valid Zip file 
* WI 129563 BLR 02/13/2014
*   - Updated Joint detail constants to write to the definition file correctly.
*     Affects occurs groups.   
* WI 115249 BLR 02/26/2014
*   - Updated IsExtractValid to check order/limit options. Added schema
*     and table to the limit fields.   
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 116545 BLR 03/06/2014  
*   - Fixed some hard-coded strings which were causing joint layouts to 
*     to disclude some data.   
* WI 130642 DLD 03/07/2014
*   - Validating Extract Def file paths. 
* WI 130642 BLR 03/10/2014
*   - Rooted the file paths before validation. 
* WI 132157 DLD 03/11/2014
*   - Validating Image Service availability before Run. Allow user to run without images if service is unreachable.
* WI 135748 BLR 04/09/2014
*   - Removed parenthesis before parsing 'in' clause.
* WI 129719 BLR 06/09/2014
*   - Updated for OLFServices integration. 
* WI 115856 BLR 06/24/2014
*   - Added IsDataEntry attribute for Saving and Loading. 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {   

    public delegate void ExtractSavedEventHandler(string ExtractFileName, cExtractDef ExtractDef);

    public class cExtractDef : IExtractDef {

        public const string DEFAULT_SINGLE_IMAGES_FILE_NAME = "ExtractImage<FC>";
        public const string DEFAULT_IMAGES_PER_TXN_FILE_NAME = "<BK>_<LB>_<BH>_<PD>_<TR>";
        public const string DEFAULT_IMAGES_PER_BATCH_FILE_NAME = "<BK>_<LB>_<BH>_<PD>";
        public const string DEFAULT_FILE_PROC_DATE_FORMAT = "MMDDYY";

        // Version
        private string _Version = "";

        // General
        private string _ExtractPath = "";
        private string _LogFilePath = "";

        private string _ImagePath = "";
        private bool _UseQuotes = false;
        private bool _NoPad = false;
        private bool _NullAsSpace = false;
        private string _TimeStamp = "";
        private string _FieldDelim = "";
        private string _RecordDelim = @"\n";
        
        private string _PrinterName = "Acrobat PDFWriter";

        private ImageFileFormatEnum _ImageFileFormat = ImageFileFormatEnum.DoNotRetrieveImages;

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        private bool _IncludeFooter = false;

        private List<cLayout> _FileLayouts = new List<cLayout>();
        private List<cLayout> _BankLayouts = new List<cLayout>();
        private List<cLayout> _CustomerLayouts = new List<cLayout>();
        private List<cLayout> _LockboxLayouts = new List<cLayout>();
        private List<cLayout> _BatchLayouts = new List<cLayout>();
        private List<cLayout> _TransactionLayouts = new List<cLayout>();
        private List<cLayout> _PaymentLayouts = new List<cLayout>();
        private List<cLayout> _StubLayouts = new List<cLayout>();
        private List<cLayout> _DocumentLayouts = new List<cLayout>();

        private List<cLayout> _JointDetailItemLayouts = new List<cLayout>();

        private List<cOrderByColumn> _BankOrderByColumns = new List<cOrderByColumn>();
        private List<cOrderByColumn> _CustomerOrderByColumns = new List<cOrderByColumn>();
        private List<cOrderByColumn> _LockboxOrderByColumns = new List<cOrderByColumn>();
        private List<cOrderByColumn> _BatchOrderByColumns = new List<cOrderByColumn>();
        private List<cOrderByColumn> _TransactionsOrderByColumns = new List<cOrderByColumn>();
        private List<cOrderByColumn> _PaymentsOrderByColumns = new List<cOrderByColumn>();
        private List<cOrderByColumn> _StubsOrderByColumns = new List<cOrderByColumn>();
        private List<cOrderByColumn> _DocumentsOrderByColumns = new List<cOrderByColumn>();

        private List<cLimitItem> _BankLimitItems = new List<cLimitItem>();
        private List<cLimitItem> _LockboxLimitItems = new List<cLimitItem>();
        private List<cLimitItem> _BatchLimitItems = new List<cLimitItem>();
        private List<cLimitItem> _TransactionsLimitItems = new List<cLimitItem>();
        private List<cLimitItem> _PaymentsLimitItems = new List<cLimitItem>();
        private List<cLimitItem> _StubsLimitItems = new List<cLimitItem>();
        private List<cLimitItem> _DocumentsLimitItems = new List<cLimitItem>();

        //private bool _CombineImagesAccrossProcDates = false;
        private string _ImageFileNameSingle = DEFAULT_SINGLE_IMAGES_FILE_NAME;
        private string _ImageFileNamePerTran = DEFAULT_IMAGES_PER_TXN_FILE_NAME;
        private string _ImageFileNamePerBatch = DEFAULT_IMAGES_PER_BATCH_FILE_NAME;
        private string _ImageFileProcDateFormat = DEFAULT_FILE_PROC_DATE_FORMAT;
        private bool _ImageFileZeroPad = false;
        private bool _ImageFileBatchTypeFormatFull = true;
        private bool _IncludeImageFolderPath = true;
        private bool _IncludeNULLRows = true;
        private string _PostProcDLLFileName = "";
        
        private string _ExtractDefFilePath = string.Empty;
        
        private bool _ZipOutputFiles = false;
        private string _ZipFilePath = string.Empty;

        private List<cPostProcCodeModule> _PostProcCodeModules = new List<cPostProcCodeModule>();


        /// <summary>
        /// EventHandler to return exception information.
        /// </summary>
        public event outputErrorEventHandler OutputError;

        /// <summary>
        /// EventHandler to output status messages.
        /// </summary>
        public event OutputStatusMsgEventHandler OutputStatusMsg;
        
        public event outputMessageEventHandler OutputMessage;
        
        public event ExtractSavedEventHandler ExtractSaved;

        public cExtractDef() {

        }

        public cExtractDef(string extractDefFilePath) {
            _ExtractDefFilePath = extractDefFilePath;
        }

        #region private event handlers
        private void OnOutputError(object sender, Exception e) {
            if (OutputError != null)
            {
                OutputError(e);
            }
            else
            {
                // WI 131640 : Additional exception logging.
                cCommonLogLib.LogErrorLTA(e, LTA.Common.LTAMessageImportance.Essential);
            }
        }

        private void OnOutputError(Exception e) {
            if(OutputError != null) {
                OutputError(e);
            }
        }

        private void OnOutputStatusMsg(string msg) {
            if(OutputStatusMsg != null) {
                OutputStatusMsg(msg);
            }
        }
        
        private void OnOutputMessage(
            string msg, 
            string src, 
            MessageType messageType, 
            MessageImportance messageImportance) {

            if(OutputMessage != null) {
                OutputMessage(msg, src, messageType, messageImportance);
            }
        }

        private void OnExtractSaved(string extractFileName) {
            if(ExtractSaved != null) {
                ExtractSaved(extractFileName, this);
            }
        }
        #endregion private event handlers


        #region Object Properties

        public string ExtractDefFilePath {
            get {
                return(_ExtractDefFilePath);
            }
        }

        // Version
        public string Version {
            get {
                return(_Version);
            }
            set {
                _Version = value;
            }
        }

        // General
        public string ExtractPath {
            get {
                return(_ExtractPath);
            }
            set {
                _ExtractPath = value;
            }
        }

        public string LogFilePath {
            get {
                return(_LogFilePath);
            }
            set {
                _LogFilePath = value;
            }
        }
        
        public string ImagePath {
            get {
                return(_ImagePath);
            }
            set {
                _ImagePath = value;
            }
        }

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        public bool IncludeFooter
        {
            get { return (_IncludeFooter); }
            set { _IncludeFooter = value; }
        }

        public bool UseQuotes {
            get {
                return(_UseQuotes);
            }
            set {
                _UseQuotes = value;
            }
        }

        public bool NoPad {
            get {
                return(_NoPad);
            }
            set {
                _NoPad = value;
            }
        }

        public bool NullAsSpace {
            get {
                return(_NullAsSpace);
            }
            set {
                _NullAsSpace = value;
            }
        }

        public string TimeStamp {
            get {
                return(_TimeStamp);
            }
            set {
                _TimeStamp = value;
            }
        }

        public string FieldDelim {
            get {
                return(_FieldDelim);
            }
            set {
                _FieldDelim = value;
            }
        }

        public string RecordDelim {
            get {
                return(_RecordDelim);
            }
            set {
                _RecordDelim = value;
            }
        }

        // Image Files
        public ImageFileFormatEnum ImageFileFormat {
            get {
                return(_ImageFileFormat);
            }
            set {
                _ImageFileFormat = value;
            }
        }

        public string ImageFileFormatDesc {
            get {
                switch(_ImageFileFormat) {
                    case ImageFileFormatEnum.DoNotRetrieveImages:
                        return("Do not retrieve images");
                    case ImageFileFormatEnum.Pdf:
                        return("PDF");
                    case ImageFileFormatEnum.SendToPrinter:
                        return("Send to printer");
                    case ImageFileFormatEnum.Tiff:
                        return("Tiff");
                    default:
                        return("????");
                }
            }
        }

        public string ImageFileNameSingle {
            get {
                return(_ImageFileNameSingle);
            }
            set {
                _ImageFileNameSingle = value;
            }
        }

        public string ImageFileNamePerTran {
            get {
                return(_ImageFileNamePerTran);
            }
            set {
                _ImageFileNamePerTran = value;
            }
        }

        public string ImageFileNamePerBatch {
            get {
                return(_ImageFileNamePerBatch);
            }
            set {
                _ImageFileNamePerBatch = value;
            }
        }

        public string ImageFileNameSingleDesc {
            get {
                return(DecodeFileNameFormat(
                    _ImageFileNameSingle, 
                    _ImageFileBatchTypeFormatFull, 
                    this.ImageFileProcDateFormat, 
                    this._ImageFileZeroPad, 
                    this.ImageFileFormat));
            }
        }

        public string ImageFileNamePerTranDesc {
            get {
                return(DecodeFileNameFormat(
                    _ImageFileNamePerTran, 
                    _ImageFileBatchTypeFormatFull, 
                    this.ImageFileProcDateFormat, 
                    this._ImageFileZeroPad, 
                    this.ImageFileFormat));
            }
        }

        public string ImageFileNamePerBatchDesc {
            get {
                return(DecodeFileNameFormat(
                    _ImageFileNamePerBatch, 
                    _ImageFileBatchTypeFormatFull, 
                    this.ImageFileProcDateFormat, 
                    this._ImageFileZeroPad, 
                    this.ImageFileFormat));
            }
        }

        public string ImageFileProcDateFormat {
            get {
                return(_ImageFileProcDateFormat);
            }
            set {
                _ImageFileProcDateFormat = value;
            }
        }

        public bool ImageFileZeroPad {
            get {
                return(_ImageFileZeroPad);
            }
            set {
                _ImageFileZeroPad = value;
            }
        }

        public bool ImageFileBatchTypeFormatFull {
            get {
                return(_ImageFileBatchTypeFormatFull);
            }
            set {
                _ImageFileBatchTypeFormatFull = value;
            }
        }

        public bool IncludeImageFolderPath {
            get {
                return(_IncludeImageFolderPath);
            }
            set {
                _IncludeImageFolderPath = value;
            }
        }

        public bool IncludeNULLRows {
            get {
                return(_IncludeNULLRows);
            }
            set {
                _IncludeNULLRows = value;
            }
        }

      
        //Post Processing
        public string PostProcDLLFileName {
            get {
                return(_PostProcDLLFileName);
            }
            set {
                _PostProcDLLFileName = value;
            }
        }

        // Layouts
        public ILayout[] FileLayouts { 
            get {
                return(_FileLayouts.ToArray());
            }
        }

        public ILayout[] BankLayouts { 
            get {
                return(_BankLayouts.ToArray());
            }
        }

        public ILayout[] CustomerLayouts { 
            get {
                return(_CustomerLayouts.ToArray());
            }
        }

        public ILayout[] LockboxLayouts { 
            get {
                return(_LockboxLayouts.ToArray());
            }
        }

        public ILayout[] BatchLayouts { 
            get {
                return(_BatchLayouts.ToArray());
            }
        }

        public ILayout[] TransactionLayouts { 
            get {
                return(_TransactionLayouts.ToArray());
            }
        }

        public ILayout[] PaymentLayouts { 
            get {
                return(_PaymentLayouts.ToArray());
            }
        }

        public ILayout[] StubLayouts { 
            get {
                return(_StubLayouts.ToArray());
            }
        }

        public ILayout[] DocumentLayouts { 
            get {
                return(_DocumentLayouts.ToArray());
            }
        }

        public ILayout[] JointDetailLayouts {
            get {
                return(_JointDetailItemLayouts.ToArray());
            }
        }

        public ILayout[] AllLayouts { 
            get {

                List<ILayout> arRetVal = new List<ILayout>();

                if(FileLayouts.Length > 0) { arRetVal.AddRange(FileLayouts); }
                if(BankLayouts.Length > 0) { arRetVal.AddRange(BankLayouts); }
                if(CustomerLayouts.Length > 0) { arRetVal.AddRange(CustomerLayouts); }
                if(LockboxLayouts.Length > 0) { arRetVal.AddRange(LockboxLayouts); }
                if(BatchLayouts.Length > 0) { arRetVal.AddRange(BatchLayouts); }
                if(TransactionLayouts.Length > 0) { arRetVal.AddRange(TransactionLayouts); }
                if(PaymentLayouts.Length > 0) { arRetVal.AddRange(PaymentLayouts); }
                if(StubLayouts.Length > 0) { arRetVal.AddRange(StubLayouts); }
                if(DocumentLayouts.Length > 0) { arRetVal.AddRange(DocumentLayouts); }

                if(JointDetailLayouts.Length > 0) { arRetVal.AddRange(JointDetailLayouts); }

                return(arRetVal.ToArray());
            }
        }

        // WI 115249 : Added all limit items for ease of validation.
        public ILimitItem[] AllLimitItems
        {
            get
            {
                var output = new List<ILimitItem>();
                if (BankLimitItems.Length > 0) output.AddRange(BankLimitItems);
                if (LockboxLimitItems.Length > 0) output.AddRange(LockboxLimitItems);
                if (BatchLimitItems.Length > 0) output.AddRange(BatchLimitItems);
                if (TransactionsLimitItems.Length > 0) output.AddRange(TransactionsLimitItems);
                if (PaymentsLimitItems.Length > 0) output.AddRange(PaymentsLimitItems);
                if (StubsLimitItems.Length > 0) output.AddRange(StubsLimitItems);
                if (DocumentsLimitItems.Length > 0) output.AddRange(DocumentsLimitItems);
                return output.ToArray();
            }
        }

        public IOrderByColumn[] BankOrderByColumns {
            get {
                return(_BankOrderByColumns.ToArray());
            }
        }

        public IOrderByColumn[] CustomerOrderByColumns {
            get {
                return(_CustomerOrderByColumns.ToArray());
            }
        }

        public IOrderByColumn[] LockboxOrderByColumns {
            get {
                return(_LockboxOrderByColumns.ToArray());
            }
        }

        public IOrderByColumn[] BatchOrderByColumns {
            get {
                return(_BatchOrderByColumns.ToArray());
            }
        }

        public IOrderByColumn[] TransactionsOrderByColumns {
            get {
                return(_TransactionsOrderByColumns.ToArray());
            }
        }

        public IOrderByColumn[] PaymentsOrderByColumns {
            get {
                return(_PaymentsOrderByColumns.ToArray());
            }
        }

        public IOrderByColumn[] StubsOrderByColumns {
            get {
                return(_StubsOrderByColumns.ToArray());
            }
        }

        public IOrderByColumn[] DocumentsOrderByColumns {
            get {
                return(_DocumentsOrderByColumns.ToArray());
            }
        }


        public ILimitItem[] BankLimitItems {
            get {
                return(_BankLimitItems.ToArray());
            }
        }

        public ILimitItem[] LockboxLimitItems {
            get {
                return(_LockboxLimitItems.ToArray());
            }
        }

        public ILimitItem[] BatchLimitItems {
            get {
                return(_BatchLimitItems.ToArray());
            }
        }

        public ILimitItem[] TransactionsLimitItems {
            get {
                return(_TransactionsLimitItems.ToArray());
            }
        }

        public ILimitItem[] PaymentsLimitItems {
            get {
                return(_PaymentsLimitItems.ToArray());
            }
        }

        public ILimitItem[] StubsLimitItems {
            get {
                return(_StubsLimitItems.ToArray());
            }
        }

        public ILimitItem[] DocumentsLimitItems {
            get {
                return(_DocumentsLimitItems.ToArray());
            }
        }

        public long[] DefaultBankID {
            get {
                return(GetDefaultIDs(_BankLimitItems, "SiteBankID", long.MinValue));
            }
        }

        public long[] DefaultLockboxID {
            get {
                return (GetDefaultIDs(_LockboxLimitItems, "SiteClientAccountID", long.MinValue));
            }
        }

        public long[] DefaultBatchID {
            get {
                return(GetDefaultIDs(_BatchLimitItems, "SourceBatchID", 1));
            }
        }
        
        public DateTime DefaultProcessingDate {
            get {
                return(GetDefaultDate(_BatchLimitItems, "ImmutableDateKey"));
            }
        }
        
        public DateTime DefaultDepositDate {
            get {
                return(GetDefaultDate(_BatchLimitItems, "DepositDateKey"));
            }
        }

        public string PrinterName {
            get {
                return(_PrinterName);
            }
            set {
                _PrinterName = value;
            }
        }

        public bool ZipOutputFiles {
            get {
                return(_ZipOutputFiles);
            }
            set {
                _ZipOutputFiles = value;
            }
        }

        public string ZipFilePath {
            get {
                return(_ZipFilePath);
            }
            set {
                _ZipFilePath = value;
            }
        }

        public List<cPostProcCodeModule> PostProcCodeModules {
            get {
                return(_PostProcCodeModules);
            }
        }

        internal string FormattedRecordDelim {

            get {
                string strRetVal = string.Empty;

                switch(RecordDelim) {
                    case @"\n":
                        strRetVal += (char)13;
                        strRetVal += (char)10;
                        break;
                    case @"\n\n":
                        strRetVal += (char)13;
                        strRetVal += (char)10;
                        strRetVal += (char)13;
                        strRetVal += (char)10;
                        break;
                    case @"\t":
                        strRetVal += (char)11;
                        break;
                    case @"\t\t":
                        strRetVal += (char)11;
                        strRetVal += (char)11;
                        break;
                    default:
                        strRetVal = RecordDelim;
                        break;
                }

                return(strRetVal);
            }
        }

        internal int JointDetailLayoutFieldCount(LayoutLevelEnum layoutLevel) {
            
            int intRetVal = 0;
            
            foreach(cLayout layout in JointDetailLayouts) {
                foreach(cLayoutField layoutfield in layout.LayoutFields) {
                    switch(layoutLevel) {
                        // WI 116545 - Bug causing Joint layouts to be discluding some data. Fixed the hard-coded strings.
                        case LayoutLevelEnum.Bank:
                            if(layoutfield.SimpleTableName.ToLower() == "rechubdata.dimbanks") 
                            {
                                ++intRetVal;
                            }
                            break;
                        case LayoutLevelEnum.ClientAccount:
                            if (layoutfield.SimpleTableName.ToLower() == "rechubdata.dimclientaccounts")
                            {
                                ++intRetVal;
                            }
                            break;
                        case LayoutLevelEnum.Batch:
                            if (layoutfield.SimpleTableName.ToLower() == "rechubdata.factbatchsummary")
                            {
                                ++intRetVal;
                            }
                            break;
                        case LayoutLevelEnum.Transaction:
                            if (layoutfield.SimpleTableName.ToLower() == "rechubdata.facttransactionsummary")
                            {
                                ++intRetVal;
                            }
                            break;
                        case LayoutLevelEnum.Payment:
                            if (layoutfield.SimpleTableName.ToLower() == "rechubdata.factchecks"
                                || layoutfield.SimpleTableName.ToLower() == "rechubdata.checksdataentry")
                            {
                                ++intRetVal;
                            }
                            break;
                        case LayoutLevelEnum.Stub:
                            if (layoutfield.SimpleTableName.ToLower() == "rechubdata.factstubs"
                                || layoutfield.SimpleTableName.ToLower() == "rechubdata.stubsdataentry")
                            {
                                ++intRetVal;
                            }
                            break;
                        case LayoutLevelEnum.Document:
                            if (layoutfield.SimpleTableName.ToLower() == "rechubdata.factdocuments")
                            {
                                ++intRetVal;
                            }
                            break;
                    }
                }
            }

            return(intRetVal);
        }

        internal int AggregateCount(LayoutLevelEnum layoutLevel) {

            int intRetVal = 0;

            if(layoutLevel == LayoutLevelEnum.Batch ||
               layoutLevel == LayoutLevelEnum.Transaction ||
               layoutLevel == LayoutLevelEnum.Payment ||
               layoutLevel == LayoutLevelEnum.Stub ||
               layoutLevel == LayoutLevelEnum.Document) {

                foreach(cLayout layout in this.AllLayouts) {
                    foreach(cLayoutField layoutfield in layout.LayoutFields) {
                        switch(layoutLevel) {
                            case LayoutLevelEnum.Batch:
                                if(layoutfield.IsBatchAggregate) {
                                    ++intRetVal;
                                }
                                break;
                            case LayoutLevelEnum.Transaction:
                                if(layoutfield.IsTransactionsAggregate) {
                                    ++intRetVal;
                                }
                                break;
                            case LayoutLevelEnum.Payment:
                                if(layoutfield.IsChecksAggregate) {
                                    ++intRetVal;
                                }
                                break;
                            case LayoutLevelEnum.Stub:
                                if(layoutfield.IsStubsAggregate) {
                                    ++intRetVal;
                                }
                                break;
                            case LayoutLevelEnum.Document:
                                if(layoutfield.IsDocumentsAggregate) {
                                    ++intRetVal;
                                }
                                break;
                        }
                    }
                }
            }
            
            return(intRetVal);
        }

        private long[] GetDefaultIDs(
            List<cLimitItem> limitItems, 
            string fieldName,
            long minValue) {

            long lngTemp;
            // WI 135748 : Moved to IEnumerable for simple Linq support.
            IEnumerable<string> arValues;
            
            List<long> arRetVal = new List<long>();

            foreach(cLimitItem limititem in limitItems) {
                try {
                    if(limititem.FieldName.ToLower() == fieldName.ToLower()) {

                        if(limititem.LogicalOperator == LogicalOperatorEnum.Equals) {
                            if(long.TryParse(limititem.Value, out lngTemp) && lngTemp >= minValue) {
                                if(!arRetVal.Contains(lngTemp)) {
                                    arRetVal.Add(lngTemp);
                                }
                            }
                        } else if(limititem.LogicalOperator == LogicalOperatorEnum.In) {
                            // WI 135748 : Ignore all parenthesis, as they were causing long.TryParse errors.
                            arValues = limititem.Value
                                .Split(',')
                                .Select(x => x.Replace("(", "").Replace(")", ""));
                            foreach(string value in arValues) {
                                if(long.TryParse(value, out lngTemp) && lngTemp >= minValue) {
                                    if(!arRetVal.Contains(lngTemp)) {
                                        arRetVal.Add(lngTemp);
                                    }
                                }
                            }
                        }
                    }
                } catch(Exception e) {
                    OnOutputError(this, e);
                }
            }
            
            return(arRetVal.ToArray());
        }

        private DateTime GetDefaultDate(
            List<cLimitItem> limitItems,
            string fieldName) {

            DateTime dteTemp;
            DateTime dteRetVal = DateTime.MinValue;

            foreach(cLimitItem limititem in limitItems) {
                try {
                    if(limititem.FieldName.ToLower() == fieldName.ToLower()) {
                        if(limititem.LogicalOperator == LogicalOperatorEnum.Equals) {
                            if(DateTime.TryParse(limititem.Value, out dteTemp)) {
                                dteRetVal = dteTemp;
                                break;
                            }
                        }
                    }
                } catch(Exception e) {
                    OnOutputError(this, e);
                }
            }
            
            return(dteRetVal);
        }

        #endregion Object Properties


        #region CXS Input Methods
        public bool ParseExtractDefFile(string filePath) {

            cINIFileInterop objIni;
            string strExtractPath = string.Empty;
            StringCollection records;
            StringCollection rows;
            cLayout layout;
            string strRecordCode;
            int intImageFileFormat;
            bool bolRetVal = false;

            LayoutTypeEnum enmLayoutType;
            LayoutLevelEnum enmLayoutLevel;
            bool bolIsJointDetail;
            int intMaxRepeats;

            try {

                OnOutputStatusMsg("Parsing Extract ExtractDef file: [" + filePath + "]");

                objIni = new cINIFileInterop(filePath);

                // Version
                _Version = objIni.IniReadString("VERSION", "Version", "");

                // General
                strExtractPath = objIni.IniReadString("GENERAL", "ExtractPath", "");

                _ExtractPath = strExtractPath;

                _LogFilePath = objIni.IniReadString("GENERAL", "LogFilePath", "");
                if(_LogFilePath.Trim().Length == 0) {
                    _LogFilePath = GetDefaultLogFilePath();
                }

                _UseQuotes = (objIni.IniReadInteger("GENERAL", "UseQuotes", 0) == 1);
                _NoPad = (objIni.IniReadInteger("GENERAL", "NoPad", 0) == 1);
                _NullAsSpace = (objIni.IniReadInteger("GENERAL", "NullAsSpace", 0) == 1);
                _TimeStamp = objIni.IniReadString("GENERAL", "TimeStamp", "");
                _FieldDelim = objIni.IniReadString("GENERAL", "FieldDelim", "");
                _RecordDelim = objIni.IniReadString("GENERAL", "RecordDelim", "");
                _PrinterName = objIni.IniReadString("GENERAL", "PrinterName", "Acrobat PDFWriter");

                // Image Files
                intImageFileFormat = objIni.IniReadInteger("IMAGEFILES", "ImageFileFormatAsPDF", (int)ImageFileFormatEnum.DoNotRetrieveImages); // CR 9916 10/13/2004 ALH - change to an int instead of bool.

                // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                _IncludeFooter = (objIni.IniReadInteger("IMAGEFILES", "IncludeFooter", 0) == 1);

                if(intImageFileFormat == ((int)ImageFileFormatEnum.DoNotRetrieveImages) || 
                   intImageFileFormat == ((int)ImageFileFormatEnum.Tiff) ||
                   intImageFileFormat == ((int)ImageFileFormatEnum.SendToPrinter) ||
                   intImageFileFormat == ((int)ImageFileFormatEnum.Pdf)) {
                    _ImageFileFormat = (ImageFileFormatEnum)intImageFileFormat;
                }

                _ImagePath = objIni.IniReadString("GENERAL", "ImagePath", "");
                if(_ImagePath.Trim().Length == 0) {
                    _ImagePath = GetDefaultImagePath();
                }

                _ImageFileNameSingle = objIni.IniReadString("IMAGEFILES", "ImageFileNameSingle", "");
                if(_ImageFileNameSingle.Trim().Length == 0) {
                    _ImageFileNameSingle = DEFAULT_SINGLE_IMAGES_FILE_NAME;
                }

                _ImageFileNamePerTran = objIni.IniReadString("IMAGEFILES", "ImageFileNamePerTran", "");
                if(_ImageFileNamePerTran.Trim().Length == 0) {
                    _ImageFileNamePerTran = DEFAULT_IMAGES_PER_TXN_FILE_NAME;
                }

                _ImageFileNamePerBatch = objIni.IniReadString("IMAGEFILES", "ImageFileNamePerBatch", "");
                if(_ImageFileNamePerBatch.Trim().Length == 0) {
                    _ImageFileNamePerBatch = DEFAULT_IMAGES_PER_BATCH_FILE_NAME;
                }

                _ImageFileProcDateFormat = objIni.IniReadString("IMAGEFILES", "ImageFileProcDateFormat", DEFAULT_FILE_PROC_DATE_FORMAT);

                ImageFileZeroPad = (objIni.IniReadInteger("IMAGEFILES", "ImageFileZeroPad", 0) == 1);
                ImageFileBatchTypeFormatFull = (objIni.IniReadInteger("IMAGEFILES", "ImageFileBatchTypeFormatFull", 0) == 1);
                IncludeImageFolderPath = (objIni.IniReadInteger("IMAGEFILES", "IncludeImageFolderPath", 1) != 0);

                PostProcDLLFileName = objIni.IniReadString("POSTDLL", "FileName", ""); //CR10943 03/30/2005 ALH

                if(objIni.IniReadInteger("GENERAL", "ZipOutputFiles", 0) == 1) {
                    _ZipOutputFiles = true;
                    _ZipFilePath = objIni.IniReadString("GENERAL", "ZipFilePath", "");
                } else {
                    _ZipOutputFiles = false;
                    _ZipFilePath = string.Empty;
                }

                records = objIni.GetINISection("RECORDS");

                foreach(string record in records) {

                    if(record.IndexOf('=') > 0) {

                        layout = new cLayout(this);
                        layout.OutputError += OnOutputError;

                        if(record.Length > record.IndexOf('=') - 1) {

                            layout.LayoutName = record.Substring(0, record.IndexOf('='));

                            strRecordCode = record.Substring(record.IndexOf('=') + 1).TrimEnd();
                            //if((strRecordCode.Length == 3)||(strRecordCode.Length == 4 && strRecordCode.Substring(1, 1) == "1")) {
                            //    layout.LayoutType = LayoutTypeEnum.Trailer;
                            //} else {
                            //    layout.LayoutType = LayoutTypeEnum.Header;
                            //}

                            ParseLayoutRecord(strRecordCode,
                                              out enmLayoutType, 
                                              out enmLayoutLevel, 
                                              out bolIsJointDetail, 
                                              out intMaxRepeats);
                            
                            layout.LayoutType = enmLayoutType;
                            layout.LayoutLevel = enmLayoutLevel;
                            
                            rows = objIni.GetINISection(layout.LayoutName + "_GRID");
                            foreach(string row in rows) {
                                if(row.IndexOf('=') > 0) {
                                    AddLayoutField(layout, row.Substring(row.IndexOf('=') + 1)); 
                                }
                            }

                            if(layout.LayoutLevel == LayoutLevelEnum.JointDetail) {
                                if(layout.UseOccursGroups) {
                                    layout.MaxRepeats = intMaxRepeats;
                                } else {
                                    layout.MaxRepeats = 0;
                                }
                                AddJointDetailLayout(layout);
                            } else {
                                layout.MaxRepeats = 0;
                                AddLayout(layout);
                            }
                        }
                    }
                }

                _BankOrderByColumns = GetOrderByColumns(objIni, "Bank_Order");
                _CustomerOrderByColumns = GetOrderByColumns(objIni, "Customer_Order");
                _LockboxOrderByColumns = GetOrderByColumns(objIni, "Lockbox_Order");
                _BatchOrderByColumns = GetOrderByColumns(objIni, "Batch_Order");
                _TransactionsOrderByColumns = GetOrderByColumns(objIni, "Transactions_Order");
                _PaymentsOrderByColumns = GetOrderByColumns(objIni, "Checks_Order");
                _StubsOrderByColumns = GetOrderByColumns(objIni, "Stubs_Order");
                _DocumentsOrderByColumns = GetOrderByColumns(objIni, "Documents_Order");

                _BankLimitItems = GetLimitItems(objIni, "Bank_Limits");
                _LockboxLimitItems = GetLimitItems(objIni, "Lockbox_Limits");
                _BatchLimitItems = GetLimitItems(objIni, "Batch_Limits");
                _TransactionsLimitItems = GetLimitItems(objIni, "Transactions_Limits");
                _PaymentsLimitItems = GetLimitItems(objIni, "Checks_Limits");
                _StubsLimitItems = GetLimitItems(objIni, "Stubs_Limits");
                _DocumentsLimitItems = GetLimitItems(objIni, "Documents_Limits");

                _ExtractDefFilePath = filePath;

                _PostProcCodeModules = GetPostProcCodeModules(objIni);

                OnOutputStatusMsg("Extract ExtractDef file loaded: [" + filePath + "]");

                bolRetVal = true;

            } catch(Exception e) {
                OnOutputError(this, e);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        public bool IsValidExtract(out string msg) {

            string strMsg;
            //bool bolRetVal = true;
            List<string> arMsg = new List<string>();

            // WI 130642 : Root the paths.
            if (!Path.IsPathRooted(this.ExtractPath))
                ExtractPath = Path.Combine(Environment.CurrentDirectory, ExtractPath);

            if (!Path.IsPathRooted(this.ImagePath))
                ImagePath = Path.Combine(Environment.CurrentDirectory, ImagePath);

            if (!Path.IsPathRooted(this.LogFilePath))
                LogFilePath = Path.Combine(Environment.CurrentDirectory, LogFilePath);

            if (!Path.IsPathRooted(this.ZipFilePath))
                ZipFilePath = Path.Combine(Environment.CurrentDirectory, ZipFilePath);

            if (!CommonLib.CheckDirectory(Path.GetDirectoryName(this.ExtractPath)))
                arMsg.Add(NotifyInvalidPath("Extract Data File Path", this.ExtractPath));

            if (!CommonLib.CheckDirectory(Path.GetDirectoryName(this.ImagePath)))
                arMsg.Add(NotifyInvalidPath("Image File Path", this.ImagePath));

            if (!CommonLib.CheckDirectory(Path.GetDirectoryName(this.LogFilePath)))
                arMsg.Add(NotifyInvalidPath("Log File Path", this.LogFilePath));

            if (!CommonLib.CheckDirectory(Path.GetDirectoryName(this.ZipFilePath)))
                arMsg.Add(NotifyInvalidPath("Zip File Path", this.ZipFilePath));

            if (this.ExtractPath.Trim().Length == 0)
            {
                arMsg.Add("Invalid Extract File Path.");
            }
            if (this.ZipOutputFiles && this.ZipFilePath.Trim().Length == 0) {

                arMsg.Add("Zip Output File was requested, but the path is invalid.");
            }

            foreach(cLayout layout in this.AllLayouts) {
                if(!layout.IsValidLayout(out strMsg)) {
                    arMsg.Add(strMsg);
                }
            }


            if (ImageFileFormat != ImageFileFormatEnum.DoNotRetrieveImages)
            {
                // Check Service first. 
                var serviceavail = cOLFServicesLib.ServiceIsAvailable;
                if (!serviceavail)
                {
                    OnOutputMessage("The image repository is currently unavailable." + Environment.NewLine + "The Extract will be run with image outputs disabled.", this.ToString(), MessageType.Warning, MessageImportance.Essential);
                    ImageFileFormat = ImageFileFormatEnum.DoNotRetrieveImages;
                }
            }


            if(arMsg.Count > 0) {

                msg = "Extract is not valid: " + System.Environment.NewLine;
                
                foreach(string str in arMsg) {
                    msg += System.Environment.NewLine;
                    msg += str;
                }
            } else {
                msg = string.Empty;
            }
            
            return msg == string.Empty;
        }

        private string NotifyInvalidPath(string settingName, string path)
        {
            return String.Format("Extract Definition Setting: '{0}'{1}Invalid or Unreachable Path:{1}{2}{1}", settingName, Environment.NewLine, path);
            //if (!this.Disposing)
            //    OnOutputMessage(String.Format("Extract Definition Setting: '{0}'{1}{1}Invalid or Unreachable Path:{1}{2}", settingName, Environment.NewLine, path), this.ToString(), MessageType.Warning, MessageImportance.Essential);
        }


        private void AddLayoutField(
            cLayout layout, 
            string layoutFields) {

            string[] arLayoutFields;
            cLayoutField objLayoutField;            
            
            int intTemp;
            char chrTemp;
            bool bolTemp;
            
            int intDataType;
            int intColumnWidth;
            
            string strFormattedLayoutFields;

            strFormattedLayoutFields = layoutFields;
            if(strFormattedLayoutFields.StartsWith("'")) {
                strFormattedLayoutFields = strFormattedLayoutFields.Substring(1);
            }
            if(strFormattedLayoutFields.EndsWith("'")) {
                strFormattedLayoutFields = strFormattedLayoutFields.Substring(0, strFormattedLayoutFields.Length - 1);
            }

            arLayoutFields = strFormattedLayoutFields.Split('|');
            
            if(arLayoutFields.Length >= 8) {

                // Data Type
                if(int.TryParse(arLayoutFields[1], out intTemp)) {
                    intDataType = intTemp;
                } else {
                    intDataType = 0;
                }
                
                // Column Width
                if(int.TryParse(arLayoutFields[3], out intTemp)) {
                    intColumnWidth = intTemp;
                } else {
                    intColumnWidth = 0;
                }

                objLayoutField = new cLayoutField(layout, arLayoutFields[0], intDataType, intColumnWidth);
                objLayoutField.OutputError += OnOutputError;

                // Display Name
                objLayoutField.DisplayName = arLayoutFields[2];
                
                // Pad Character
                if(arLayoutFields[4].Trim().Length == 0 || arLayoutFields[4].Trim().ToUpper() == "SP") {
                    objLayoutField.PadChar = ' ';
                } else if(char.TryParse(arLayoutFields[4], out chrTemp)) {
                    objLayoutField.PadChar = chrTemp;
                } else {
                    objLayoutField.PadChar = ' ';
                }

                // Justification
                if(arLayoutFields[5].Trim().ToUpper() == "R") {
                    objLayoutField.Justification = JustifyEnum.Right;
                } else {
                    objLayoutField.Justification = JustifyEnum.Left;
                }

                // Use Quotes?
                if(arLayoutFields[6].Trim().ToLower() == "x") {
                    objLayoutField.Quotes = true;
                } else if(bool.TryParse(arLayoutFields[6], out bolTemp)) {
                    objLayoutField.Quotes = bolTemp;
                } else {
                    objLayoutField.Quotes = false;
                }
                
                // Format
                objLayoutField.Format = arLayoutFields[7];

                if(layout.LayoutLevel == LayoutLevelEnum.JointDetail) {

                    // Occurs Columns
                    // WI 129563 : Updated constant strings for the current data model.
                    if(arLayoutFields.Length >= 10 && (objLayoutField.FieldName.ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER)||
                                                       objLayoutField.FieldName.ToLower().StartsWith("checksdataentry.")||
                                                       objLayoutField.FieldName.ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER)||
                                                       objLayoutField.FieldName.ToLower().StartsWith("stubsdataentry.")||
                                                       objLayoutField.FieldName.ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER))) {
                        // WI 129563 : Updated constant strings for the current data model.
                        if(int.TryParse(arLayoutFields[8], out intTemp) && intTemp >= 0) {
                            if(objLayoutField.FieldName.ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER)||
                               objLayoutField.FieldName.ToLower().StartsWith("checksdataentry.")) {
                                layout.UsePaymentsOccursGroups = true;
                            } else if(objLayoutField.FieldName.ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER)||
                                      objLayoutField.FieldName.ToLower().StartsWith("stubsdataentry.")) {
                                layout.UseStubsOccursGroups = true;
                            } else if(objLayoutField.FieldName.ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER)) {
                                layout.UseDocumentsOccursGroups = true;
                            }
                        }
                        // WI 129563 : Updated constant strings for the current data model.
                        if(int.TryParse(arLayoutFields[9], out intTemp)) {
                            if(objLayoutField.FieldName.ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER)||
                               objLayoutField.FieldName.ToLower().StartsWith("checksdataentry.")) {
                                layout.PaymentsOccursCount = intTemp;
                            } else if(objLayoutField.FieldName.ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER)||
                                      objLayoutField.FieldName.ToLower().StartsWith("stubsdataentry.")) {
                                layout.StubsOccursCount = intTemp;
                            } else if(objLayoutField.FieldName.ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER)) {
                                layout.DocumentsOccursCount = intTemp;
                            }
                        }

                        layout.UseOccursGroups = true;
                    }
                }

                layout.AddLayoutField(objLayoutField);
            }
        }

        private static List<cOrderByColumn> GetOrderByColumns(
            cINIFileInterop iniLib, 
            string sectionName) {

            StringCollection records;
            int iPos;
            cOrderByColumn objOrderByColumn;
            string[] arOrderByColumns;
            string strColumnName;
            List<cOrderByColumn> arRetVal = new List<cOrderByColumn>();
            string strRecord;

            records = iniLib.GetINISection(sectionName);
            if(records.Count > 0) {
                foreach(string str in records) {
                    iPos = str.IndexOf('=');
                    if(iPos > -1) {
                    
                        strRecord = str.Substring(iPos + 1);
                        if(strRecord.StartsWith("'")) {
                            strRecord = strRecord.Substring(1);
                        }
                        if(strRecord.EndsWith("'")) {
                            strRecord = strRecord.Substring(0, strRecord.Length -1);
                        }
                    
                        arOrderByColumns = strRecord.Split('|');

                        if(arOrderByColumns.Length > 0) {

                            strColumnName = arOrderByColumns[0];
                            if(strColumnName.StartsWith("'")) {
                                strColumnName = strColumnName.Substring(1);
                            }

                            if(strColumnName.EndsWith("'")) {
                                strColumnName = strColumnName.Substring(0, strColumnName.Length -1);
                            }

                            objOrderByColumn = new cOrderByColumn(strColumnName, strColumnName);

                            if(arOrderByColumns.Length > 1) {
                                if(arOrderByColumns[1] == "DESC") {
                                    objOrderByColumn.OrderByDir = OrderByDirEnum.Descending;
                                } else {
                                    objOrderByColumn.OrderByDir = OrderByDirEnum.Ascending;
                                }
                            }
                            
                            arRetVal.Add(objOrderByColumn);
                        }
                    }
                }
            }
            
            return(arRetVal);
        }

        private List<cLimitItem> GetLimitItems(
            cINIFileInterop iniLib, 
            string sectionName) {

            StringCollection records;
            int iPos;
            cLimitItem objLimitItem;
            string strLimitFields;
            string[] arLimitFields;

            List<cLimitItem> arRetVal = new List<cLimitItem>();

            records = iniLib.GetINISection(sectionName);
            if(records.Count > 0) {
                foreach(string str in records) {
                    iPos = str.IndexOf('=');
                    if(iPos > -1) {
                        strLimitFields = str.Substring(iPos + 1);
                        if(strLimitFields.StartsWith("'")) {
                            strLimitFields = strLimitFields.Substring(1);
                        } 

                        if(strLimitFields.EndsWith("'")) {
                            strLimitFields = strLimitFields.Substring(0, strLimitFields.Length -1);
                        } 

                        arLimitFields = strLimitFields.Split('|');

                        try {
                            objLimitItem = new cLimitItem();

                            if(arLimitFields.Length == 6) {

                                if(arLimitFields[0].Length > 0) {
                                    objLimitItem.FieldName = arLimitFields[0];
                                } else {
                                    throw(new Exception("Invalid Field Name."));
                                }

                                switch(arLimitFields[1].ToLower().Trim()) {
                                    case "=":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.Equals;
                                        break;
                                    case "<>":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.NotEqualTo;
                                        break;
                                    case ">":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.GreaterThan;
                                        break;
                                    case ">=":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.GreaterThanOrEqualTo;
                                        break;
                                    case "<":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.LessThan;
                                        break;
                                    case "<=":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.LessThanOrEqualTo;
                                        break;
                                    case "between":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.Between;
                                        break;
                                    case "in":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.In;
                                        break;
                                    case "is null":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.IsNull;
                                        break;
                                    case "is not null":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.IsNotNull;
                                        break;
                                    case "like":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.Like;
                                        break;
                                    default:
                                        throw(new Exception("Invalid Logical Operator."));
                                }

                                objLimitItem.Value = arLimitFields[2];
                                objLimitItem.LeftParenCount = arLimitFields[3].Length;
                                objLimitItem.RightParenCount = arLimitFields[4].Length;
                                
                                switch(arLimitFields[5].ToLower().Trim()) {
                                    case "and":
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.And;
                                        break;
                                    case "and not":
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.AndNot;
                                        break;
                                    case "or":
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.Or;
                                        break;
                                    case "or not":
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.OrNot;
                                        break;
                                    default:
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.None;
                                        break;
                                }
                                
                                arRetVal.Add(objLimitItem);
                            }
                        } catch(Exception e) {
                            OnOutputError(this, e);
                        }
                    }
                }
            }
            
            return(arRetVal);
        }

        private static List<cPostProcCodeModule> GetPostProcCodeModules(cINIFileInterop iniLib) {

            string[] arSections = iniLib.GetIniSections();
            StringCollection scSection;
            string strSrc;
            int intTemp;
            string strhashcode = string.Empty;
            int intPos;

            List<cPostProcCodeModule> objRetVal = new List<cPostProcCodeModule>();

            foreach(string section in arSections) {

                if(section.ToUpper().StartsWith("CODE_MODULE")) {

                    scSection = iniLib.GetINISection(section);
                    strSrc = string.Empty;

                    for(int i=0;i<scSection.Count;++i) {

                        if(scSection[i].StartsWith("LINE")) {
                            intPos = scSection[i].IndexOf('=');

                            if(intPos > -1 && scSection[i].Length > intPos) {
                                if(scSection[i].Substring(intPos + 1).StartsWith("'")) {
                                    if(scSection[i].Length > intPos + 1) {
                                        strSrc += scSection[i].Substring(intPos + 2);
                                    }
                                } else {
                                    strSrc += scSection[i].Substring(intPos + 1);
                                }
                            }

                            if(i<scSection.Count) {
                                strSrc += "\r\n"; 
                            }
                        } else if(scSection[i].StartsWith("HashCode")) {
                            intPos = scSection[i].IndexOf('=');

                            if(intPos > -1 && scSection[i].Length > intPos) {
                                strhashcode = scSection[i].Substring(intPos + 1);
                            }
                        }
                    }
                    
                    objRetVal.Add(new cPostProcCodeModule(strSrc, strhashcode));
                }
            }

            return(objRetVal);
        }

        #endregion CXS Input Methods

        
        #region CXS Output Methods

        public bool SaveAsCxs(string fileName) {

            cINIFileInterop objIni;
            bool bolRetVal;
            
            try {

                if(fileName == null || fileName.Trim() == string.Empty) {
                    throw(new Exception("File Name not specified for save."));
                }

                objIni = new cINIFileInterop(fileName);
                
                objIni.IniWriteValue("VERSION", "Version", this.Version);
                
                objIni.IniWriteValue("GENERAL", "ExtractPath", this.ExtractPath);
                objIni.IniWriteValue("GENERAL", "LogFilePath", this.LogFilePath);
                objIni.IniWriteValue("GENERAL", "ImagePath", this.ImagePath);
                objIni.IniWriteValue("GENERAL", "FieldDelim", this.FieldDelim);
                objIni.IniWriteValue("GENERAL", "RecordDelim", this.RecordDelim);
                objIni.IniWriteValue("GENERAL", "NoPad", (this.NoPad ? "1" : "0"));
                objIni.IniWriteValue("GENERAL", "UseQuotes", (this.UseQuotes ? "1" : "0"));
                objIni.IniWriteValue("GENERAL", "NullAsSpace", (this.NullAsSpace ? "1" : "0"));
                objIni.IniWriteValue("GENERAL", "TimeStamp", this.TimeStamp);
                objIni.IniWriteValue("GENERAL", "IncludeStubRowsWithAllNulls", (this.IncludeNULLRows ? "1" : "0"));
                objIni.IniWriteValue("GENERAL", "PrinterName", _PrinterName);
                objIni.IniWriteValue("GENERAL", "ZipOutputFiles", (this.ZipOutputFiles ? "1" : "0"));
                objIni.IniWriteValue("GENERAL", "ZipFilePath", this.ZipFilePath);


                // Image Files
                objIni.IniWriteValue("IMAGEFILES", "ImageFileFormatAsPDF", ((int)this.ImageFileFormat).ToString());
                //objIni.IniWriteValue("IMAGEFILES", "CombineImagesAccrossProcDates", (this.CombineImagesAccrossProcDates ? "1" : "0"));
                objIni.IniWriteValue("IMAGEFILES", "ImageFileNameSingle", this.ImageFileNameSingle);
                objIni.IniWriteValue("IMAGEFILES", "ImageFileNamePerTran", this.ImageFileNamePerTran);
                objIni.IniWriteValue("IMAGEFILES", "ImageFileNamePerBatch", this.ImageFileNamePerBatch);
                objIni.IniWriteValue("IMAGEFILES", "ImageFileProcDateFormat", this.ImageFileProcDateFormat);
                objIni.IniWriteValue("IMAGEFILES", "ImageFileZeroPad", (this.ImageFileZeroPad ? "1" : "0"));
                objIni.IniWriteValue("IMAGEFILES", "ImageFileBatchTypeFormatFull", (this.ImageFileBatchTypeFormatFull ? "1" : "0"));
                objIni.IniWriteValue("IMAGEFILES", "IncludeImageFolderPath", (this.IncludeImageFolderPath ? "1" : "0"));

                // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                objIni.IniWriteValue("IMAGEFILES", "IncludeFooter", (this.IncludeFooter ? "1" : "0"));

                objIni.IniWriteValue("POSTDLL", "FileName", this.PostProcDLLFileName);

                DeleteSectionItems(objIni, "RECORDS");

                foreach(cLayout layout in this.AllLayouts) {
                    WriteLayoutCXS(objIni, layout);
                }

                WriteOrderByColumnCXS(objIni, Support.LBL_BANK, this.BankOrderByColumns);
                WriteOrderByColumnCXS(objIni, Support.LBL_CLIENT_ACCOUNT, this.LockboxOrderByColumns);
                WriteOrderByColumnCXS(objIni, Support.LBL_BATCH, this.BatchOrderByColumns);
                WriteOrderByColumnCXS(objIni, Support.LBL_TRANSACTIONS, this.TransactionsOrderByColumns);
                WriteOrderByColumnCXS(objIni, Support.LBL_PAYMENTS, this.PaymentsOrderByColumns);
                WriteOrderByColumnCXS(objIni, Support.LBL_STUBS, this.StubsOrderByColumns);
                WriteOrderByColumnCXS(objIni, Support.LBL_DOCUMENTS, this.DocumentsOrderByColumns);

                WriteLimitItemCXS(objIni, Support.LBL_BANK, this.BankLimitItems);
                WriteLimitItemCXS(objIni, Support.LBL_CLIENT_ACCOUNT, this.LockboxLimitItems);
                WriteLimitItemCXS(objIni, Support.LBL_BATCH, this.BatchLimitItems);
                WriteLimitItemCXS(objIni, Support.LBL_TRANSACTIONS, this.TransactionsLimitItems);
                WriteLimitItemCXS(objIni, Support.LBL_PAYMENTS, this.PaymentsLimitItems);
                WriteLimitItemCXS(objIni, Support.LBL_STUBS, this.StubsLimitItems);
                WriteLimitItemCXS(objIni, Support.LBL_DOCUMENTS, this.DocumentsLimitItems);

                WriteCodeModulesCXS(objIni);

                DeleteBadSections(objIni);

                _ExtractDefFilePath = fileName;

                OnExtractSaved(fileName);

                bolRetVal = true;
            } catch(Exception ex) {
                OnOutputError(this, ex);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        private void DeleteBadSections(cINIFileInterop iniObject) {

            bool bolSectionShouldExist;
            string[] arSections = iniObject.GetIniSections();
            foreach(string section in arSections) {

                bolSectionShouldExist = false;

                switch(section.ToUpper()) {
                    case "RECORDS":
                        bolSectionShouldExist = true;
                        break;
                    case "VERSION":
                        bolSectionShouldExist = true;
                        break;
                    case "GENERAL":
                        bolSectionShouldExist = true;
                        break;
                    case "HTML":
                        bolSectionShouldExist = true;
                        break;
                    case "IMAGEFILES":
                        bolSectionShouldExist = true;
                        break;
                    case "POSTDLL":
                        bolSectionShouldExist = true;
                        break;
                    case "BANK_ORDER":
                        bolSectionShouldExist = (this.BankOrderByColumns.Length > 0);
                        break;
                    case "CUSTOMER_ORDER":
                        bolSectionShouldExist = (this.CustomerOrderByColumns.Length > 0);
                        break;
                    case "LOCKBOX_ORDER":
                        bolSectionShouldExist = (this.LockboxOrderByColumns.Length > 0);
                        break;
                    case "BATCH_ORDER":
                        bolSectionShouldExist = (this.BatchOrderByColumns.Length > 0);
                        break;
                    case "TRANSACTIONS_ORDER":
                        bolSectionShouldExist = (this.TransactionsOrderByColumns.Length > 0);
                        break;
                    case "CHECKS_ORDER":
                        bolSectionShouldExist = (this.PaymentsOrderByColumns.Length > 0);
                        break;
                    case "STUBS_ORDER":
                        bolSectionShouldExist = (this.StubsOrderByColumns.Length > 0);
                        break;
                    case "DOCUMENTS_ORDER":
                        bolSectionShouldExist = (this.DocumentsOrderByColumns.Length > 0);
                        break;
                    case "BANK_LIMITS":
                        bolSectionShouldExist = (this.BankLimitItems.Length > 0);
                        break;
                    case "LOCKBOX_LIMITS":
                        bolSectionShouldExist = (this.LockboxLimitItems.Length > 0);
                        break;
                    case "BATCH_LIMITS":
                        bolSectionShouldExist = (this.BatchLimitItems.Length > 0);
                        break;
                    case "TRANSACTIONS_LIMITS":
                        bolSectionShouldExist = (this.TransactionsLimitItems.Length > 0);
                        break;
                    case "CHECKS_LIMITS":
                        bolSectionShouldExist = (this.PaymentsLimitItems.Length > 0);
                        break;
                    case "STUBS_LIMITS":
                        bolSectionShouldExist = (this.StubsLimitItems.Length > 0);
                        break;
                    case "DOCUMENTS_LIMITS":
                        bolSectionShouldExist = (this.DocumentsLimitItems.Length > 0);
                        break;
                    // CR 
                    case "CODE_MODULE":
                        bolSectionShouldExist = true;
                        break;
                    default:
                        foreach(cLayout layout in this.AllLayouts) {
                            if(section.ToUpper() == (layout.LayoutName + "_Grid").ToUpper()) {
                                bolSectionShouldExist = true;
                                break;
                            } else if(section.ToUpper().StartsWith("CODE_MODULE")) {
                                bolSectionShouldExist = true;
                            }
                        }
                        break;
                }

                if(!bolSectionShouldExist) {
                    iniObject.IniWriteValue(section, null, null);
                }
            }
        }

        private void WriteCodeModulesCXS(cINIFileInterop iniObject) {

            string[] arSections;
            int intCodeSection = 0;
            int intCodeModuleLine;
            List<string> arSrcCode;

            // Delete any existing CODE_MODULE sections.
            arSections = iniObject.GetIniSections();
            foreach(string section in arSections) {
                if(section.ToUpper().StartsWith("CODE_MODULE")) {
                    iniObject.IniWriteValue(section, null, null);
                }
            }

            foreach(cPostProcCodeModule PostProcCodeModule in this.PostProcCodeModules) {

                // Break the code module into lines of text.
                arSrcCode = new List<string>();
                arSrcCode.AddRange(PostProcCodeModule.SrcCode.Split('\n'));
                intCodeModuleLine = 0;

                // Remove empty lines of text at the end of the code module.
                if(arSrcCode.Count > 0) {
                    for(int i=arSrcCode.Count-1;i>=0;--i) {
                        if(arSrcCode[i].Trim().Length == 0) {
                            arSrcCode.RemoveAt(i);
                        } else {
                            break;
                        }
                    }
                }

                // Write each line of text from the code module to the .CXS file.
                if(arSrcCode.Count > 0) {
                    iniObject.IniWriteValue("CODE_MODULE" + (intCodeSection == 0 ? "" : "_" + intCodeSection.ToString()), "HashCode", PostProcCodeModule.SrcCode.GetHashCode().ToString());
                    for(int i=0;i<arSrcCode.Count;++i) {
                        iniObject.IniWriteValue("CODE_MODULE" + (intCodeSection == 0 ? "" : "_" + intCodeSection.ToString()), "LINE" + intCodeModuleLine.ToString(), "'" + arSrcCode[i].TrimEnd());
                        ++intCodeModuleLine;
                    }
                }
            }
        }

        private void WriteLayoutCXS(cINIFileInterop iniObject, cLayout layout) {

            int intRowIndex = 1;
            StringBuilder sbLayoutFieldText;

            // Delete any existing Layout Fields
            DeleteSectionItems(iniObject, layout.LayoutName + "_Grid");

            iniObject.IniWriteValue("RECORDS", layout.LayoutName, EncodeLayoutRecord(layout));

            foreach(cLayoutField layoutfield in layout.LayoutFields) {

                sbLayoutFieldText = new StringBuilder();

                sbLayoutFieldText.Append("'");
                sbLayoutFieldText.Append(layoutfield.FieldName + "|");
                sbLayoutFieldText.Append(layoutfield.DataType + "|");
                sbLayoutFieldText.Append(layoutfield.DisplayName + "|"); 
                sbLayoutFieldText.Append(layoutfield.ColumnWidth + "|");

                sbLayoutFieldText.Append((layoutfield.PadChar == ' ' ? "SP" : Convert.ToString(layoutfield.PadChar)) + "|");
                sbLayoutFieldText.Append((layoutfield.Justification == JustifyEnum.Left ? "L" : "R") + "|");
                sbLayoutFieldText.Append((layoutfield.Quotes ? "X": "") + "|");
                sbLayoutFieldText.Append(layoutfield.Format);

                if(layout.LayoutLevel == LayoutLevelEnum.JointDetail && layout.UseOccursGroups) {

                    sbLayoutFieldText.Append("|");

                    // WI 129563 : Updated constant strings for the current data model.
                    if(layoutfield.FieldName.ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER)||
                       layoutfield.FieldName.ToLower().StartsWith("checksdataentry.")) {
                        sbLayoutFieldText.Append((layout.UsePaymentsOccursGroups ? "0" : string.Empty) + "|");
                        sbLayoutFieldText.Append((layout.PaymentsOccursCount > 0 ? layout.PaymentsOccursCount.ToString() : string.Empty));
                    }
                    // WI 129563 : Updated constant strings for the current data model.
                    else if(layoutfield.FieldName.ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER)||
                        layoutfield.FieldName.ToLower().StartsWith("stubsdataentry.")) {
                        sbLayoutFieldText.Append((layout.UseStubsOccursGroups ? "1" : string.Empty) + "|");
                        sbLayoutFieldText.Append((layout.StubsOccursCount > 0 ? layout.StubsOccursCount.ToString() : string.Empty));
                    }
                    // WI 129563 : Updated constant strings for the current data model.
                    else if(layoutfield.FieldName.ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER)) {
                        sbLayoutFieldText.Append((layout.UseDocumentsOccursGroups ? "2" : string.Empty) + "|");
                        sbLayoutFieldText.Append((layout.DocumentsOccursCount > 0 ? layout.DocumentsOccursCount.ToString() : string.Empty));
                    }                    
                }

                sbLayoutFieldText.Append("'");

                iniObject.IniWriteValue(
                    layout.LayoutName + "_Grid", 
                    "Row" + intRowIndex.ToString(), 
                    sbLayoutFieldText.ToString());
                ++intRowIndex;
            }
        }

        private void WriteOrderByColumnCXS(
            cINIFileInterop iniObject, 
            string sectionPrefix, 
            IOrderByColumn[] orderByColumns) {

            int intRowIndex = 1;
            StringBuilder sbLayoutFieldText;

            // Delete any existing Order By Columns
            DeleteSectionItems(iniObject, sectionPrefix + "_Order");

            foreach(cOrderByColumn orderbycolumn in orderByColumns) {

                sbLayoutFieldText = new StringBuilder();

                sbLayoutFieldText.Append("'");
                sbLayoutFieldText.Append(orderbycolumn.ColumnName + "|");
                sbLayoutFieldText.Append(orderbycolumn.OrderByDir == OrderByDirEnum.Descending ? "DESC" : "ASC");
                sbLayoutFieldText.Append("'");

                iniObject.IniWriteValue(sectionPrefix + "_Order",
                                     "Row" + intRowIndex.ToString(), 
                                     sbLayoutFieldText.ToString());
                ++intRowIndex;
            }
        }

        private void WriteLimitItemCXS(
            cINIFileInterop iniObject, 
            string sectionPrefix, 
            ILimitItem[] limitItems) {

            int intRowIndex = 1;
            StringBuilder sbLayoutFieldText;

            DeleteSectionItems(iniObject, sectionPrefix + "_Limits");

            foreach(cLimitItem limititem in limitItems) {

                sbLayoutFieldText = new StringBuilder();

                sbLayoutFieldText.Append("'");
                sbLayoutFieldText.Append(limititem.FieldName + "|");
                sbLayoutFieldText.Append(DecodeLogicalOperator(limititem.LogicalOperator) + "|");
                sbLayoutFieldText.Append(limititem.Value + "|");

                // Write a '(' character limititem.LeftParenCount number of times.
                if(limititem.LeftParenCount > 0) {
                    for(int i=0;i<limititem.LeftParenCount;++i) {
                        sbLayoutFieldText.Append('(');
                    }
                }
                sbLayoutFieldText.Append('|');

                // Write a ')' character limititem.RightParenCount number of times.
                if(limititem.RightParenCount > 0) {
                    for(int i=0;i<limititem.RightParenCount;++i) {
                        sbLayoutFieldText.Append(')');
                    }
                }
                sbLayoutFieldText.Append('|');

                sbLayoutFieldText.Append(DecodeCompoundOperator(limititem.CompoundOperator));
                sbLayoutFieldText.Append("'");

                iniObject.IniWriteValue(
                    sectionPrefix + "_Limits",
                    "Row" + intRowIndex.ToString(), 
                    sbLayoutFieldText.ToString());
                ++intRowIndex;
            }
        }

        private void DeleteSectionItems(
            cINIFileInterop iniObject, 
            string sectionName) {

            StringCollection scSection = iniObject.GetINISection(sectionName);

            foreach(string key in scSection) {
                if(key.IndexOf('=') > -1) {
                    iniObject.IniWriteValue(sectionName, key.Substring(0, key.IndexOf('=')), null);
                } else {
                    iniObject.IniWriteValue(sectionName, key, null);
                }
            }
        }

        #endregion CXS Output Methods

        
        #region Xml Input Methods

        public bool ParseExtractDefXmlFile(string filePath) {

            XmlDocument docXml;
            bool bolRetVal = false;

            OnOutputStatusMsg("Parsing Extract ExtractDef file: [" + filePath + "]");

            try {

                docXml = new XmlDocument();
                docXml.Load(filePath);

                if(ParseExtractDefDoc(docXml)) {

                    _ExtractDefFilePath = filePath;

                    OnOutputStatusMsg("Extract ExtractDef file loaded: [" + filePath + "]");

                    bolRetVal = true;
                }
            } catch(Exception ex) {
                OnOutputError(this, ex);
                bolRetVal = false;
            }
            
            return(bolRetVal);
        }

        private bool ParseExtractDefDoc(XmlDocument extractDefDoc) {

            cLayout layout;
            string strRecordCode;
            bool bolRetVal = false;
            XmlNode nodeSectionHeader;
            cLayoutField objLayoutField;
            string strExtractPath;
            int intTemp;

            LayoutTypeEnum enmLayoutType;
            LayoutLevelEnum enmLayoutLevel;
            bool bolIsJointDetail;
            int intMaxRepeats;

            try {

                // Version
                nodeSectionHeader = extractDefDoc.DocumentElement.SelectSingleNode("VERSION");
                if(nodeSectionHeader != null) {
                    if(nodeSectionHeader.SelectSingleNode("Version") != null) {
                        _Version = nodeSectionHeader.SelectSingleNode("Version").InnerText;
                    }
                }

                // General
                nodeSectionHeader = extractDefDoc.DocumentElement.SelectSingleNode("GENERAL");
                if(nodeSectionHeader != null) {

                    if(nodeSectionHeader.SelectSingleNode("ExtractPath") != null) {
                        strExtractPath = nodeSectionHeader.SelectSingleNode("ExtractPath").InnerText;
                    } else {
                        strExtractPath = string.Empty;
                    }

                    _ExtractPath = strExtractPath;

                    // If the Logfile path is not specified, use the extract file name appended with
                    // the .log extension
                    if(nodeSectionHeader.SelectSingleNode("LogFilePath") != null && nodeSectionHeader.SelectSingleNode("LogFilePath").InnerText.Length > 0) {
                        _LogFilePath = nodeSectionHeader.SelectSingleNode("LogFilePath").InnerText;
                    } else {
                        _LogFilePath = GetDefaultLogFilePath();
                    }

                    // If the Image path is not specified, use the extract file's directory.
                    if(nodeSectionHeader.SelectSingleNode("ImagePath") != null && nodeSectionHeader.SelectSingleNode("ImagePath").InnerText.Length > 0) {
                        _ImagePath = nodeSectionHeader.SelectSingleNode("ImagePath").InnerText;
                    } else {
                        _ImagePath = GetDefaultImagePath();
                    }

                    if(nodeSectionHeader.SelectSingleNode("UseQuotes") != null) {
                        _UseQuotes = nodeSectionHeader.SelectSingleNode("UseQuotes").InnerText == "1";
                    }

                    if(nodeSectionHeader.SelectSingleNode("NoPad") != null) {
                        _NoPad = nodeSectionHeader.SelectSingleNode("NoPad").InnerText == "1";
                    }

                    if(nodeSectionHeader.SelectSingleNode("NullAsSpace") != null) {
                        _NullAsSpace = nodeSectionHeader.SelectSingleNode("NullAsSpace").InnerText == "1";
                    }

                    if(nodeSectionHeader.SelectSingleNode("TimeStamp") != null) {
                        _TimeStamp = nodeSectionHeader.SelectSingleNode("TimeStamp").InnerText;
                    }

                    if(nodeSectionHeader.SelectSingleNode("FieldDelim") != null) {
                        _FieldDelim = nodeSectionHeader.SelectSingleNode("FieldDelim").InnerText;
                    }

                    if(nodeSectionHeader.SelectSingleNode("RecordDelim") != null) {
                        _RecordDelim = nodeSectionHeader.SelectSingleNode("RecordDelim").InnerText;
                    }

                    if(nodeSectionHeader.SelectSingleNode("IncludeStubRowsWithAllNulls") != null) {
                        IncludeNULLRows = nodeSectionHeader.SelectSingleNode("IncludeStubRowsWithAllNulls").InnerText == "1";
                    }

                    if(nodeSectionHeader.SelectSingleNode("PrinterName") != null) {
                        _PrinterName = nodeSectionHeader.SelectSingleNode("PrinterName").InnerText;
                    } else {
                        _PrinterName = "Acrobat PDFWriter";
                    }

                    if(nodeSectionHeader.SelectSingleNode("ZipOutputFiles") != null && nodeSectionHeader.SelectSingleNode("ZipOutputFiles").InnerText == "1") {
                        _ZipOutputFiles = true;
                        if(nodeSectionHeader.SelectSingleNode("ZipFilePath") != null) {
                            _ZipFilePath = nodeSectionHeader.SelectSingleNode("ZipFilePath").InnerText;
                        } else {
                            _ZipFilePath = string.Empty;
                        }
                    } else {
                        _ZipOutputFiles = false;
                        _ZipFilePath = string.Empty;
                    }
                } 

                // Image Files
                nodeSectionHeader = extractDefDoc.DocumentElement.SelectSingleNode("IMAGEFILES");
                if(nodeSectionHeader != null) {

                    if(nodeSectionHeader.SelectSingleNode("ImageFileFormat") != null) {
                        if(nodeSectionHeader.SelectSingleNode("ImageFileFormat").InnerText == ((int)ImageFileFormatEnum.Tiff).ToString()) {
                            _ImageFileFormat = ImageFileFormatEnum.Tiff;
                        } else if(nodeSectionHeader.SelectSingleNode("ImageFileFormat").InnerText == ((int)ImageFileFormatEnum.Pdf).ToString()) {
                            _ImageFileFormat = ImageFileFormatEnum.Pdf;
                        } else if(nodeSectionHeader.SelectSingleNode("ImageFileFormat").InnerText == ((int)ImageFileFormatEnum.SendToPrinter).ToString()) {
                            _ImageFileFormat = ImageFileFormatEnum.SendToPrinter;
                        } else {
                            _ImageFileFormat = ImageFileFormatEnum.DoNotRetrieveImages;
                        }
                    }

                    // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                    if (nodeSectionHeader.SelectSingleNode("IncludeFooter") != null)
                    {
                        _IncludeFooter = nodeSectionHeader.SelectSingleNode("IncludeFooter").InnerText == "1";
                    }

                    if(nodeSectionHeader.SelectSingleNode("ImageFileNameSingle") != null) {
                        ImageFileNameSingle = nodeSectionHeader.SelectSingleNode("ImageFileNameSingle").InnerText;
                    }

                    if(nodeSectionHeader.SelectSingleNode("ImageFileNamePerTran") != null) {
                        ImageFileNamePerTran = nodeSectionHeader.SelectSingleNode("ImageFileNamePerTran").InnerText;
                    }

                    if(nodeSectionHeader.SelectSingleNode("ImageFileNamePerBatch") != null) {
                        ImageFileNamePerBatch = nodeSectionHeader.SelectSingleNode("ImageFileNamePerBatch").InnerText;
                    }

                    if(nodeSectionHeader.SelectSingleNode("ImageFileProcDateFormat") != null) {
                        ImageFileProcDateFormat = nodeSectionHeader.SelectSingleNode("ImageFileProcDateFormat").InnerText;
                    }
                    if(ImageFileProcDateFormat.Length == 0) {
                        ImageFileProcDateFormat = DEFAULT_FILE_PROC_DATE_FORMAT;
                    }

                    if(nodeSectionHeader.SelectSingleNode("ImageFileZeroPad") != null) {
                        ImageFileZeroPad = nodeSectionHeader.SelectSingleNode("ImageFileZeroPad").InnerText == "1";
                    }

                    if(nodeSectionHeader.SelectSingleNode("ImageFileBatchTypeFormatFull") != null) {
                        ImageFileBatchTypeFormatFull = nodeSectionHeader.SelectSingleNode("ImageFileBatchTypeFormatFull").InnerText == "1";
                    }

                    if(nodeSectionHeader.SelectSingleNode("IncludeImageFolderPath") != null) {
                        ImageFileBatchTypeFormatFull = nodeSectionHeader.SelectSingleNode("IncludeImageFolderPath").InnerText == "1";
                    }

                    if(nodeSectionHeader.SelectSingleNode("IncludeNULLRows") != null) {
                        IncludeNULLRows = nodeSectionHeader.SelectSingleNode("IncludeNULLRows").InnerText == "1";
                    }
                }

                nodeSectionHeader = extractDefDoc.DocumentElement.SelectSingleNode("POSTDLL");
                if(nodeSectionHeader != null) {
                    if(nodeSectionHeader.SelectSingleNode("ImageFileNamePerBatch") != null) {
                        PostProcDLLFileName = nodeSectionHeader.SelectSingleNode("FileName").InnerText;
                    }
                }

                foreach(XmlNode nodeLayout in extractDefDoc.DocumentElement.SelectNodes("Layouts/Layout")) {
                    
                    if(nodeLayout.Attributes["LayoutName"] != null && nodeLayout.Attributes["RecordCode"] != null) {

                        layout = new cLayout(this);
                        layout.OutputError += OnOutputError;

                        layout.LayoutName = nodeLayout.Attributes["LayoutName"].InnerText;

                        strRecordCode = nodeLayout.Attributes["RecordCode"].InnerText.Trim();
                        if((strRecordCode.Length == 3)||(strRecordCode.Length == 4 && strRecordCode.Substring(1, 1) == "1")) {
                            layout.LayoutType = LayoutTypeEnum.Trailer;
                        } else {
                            layout.LayoutType = LayoutTypeEnum.Header;
                        }

                        ParseLayoutRecord(strRecordCode,
                                          out enmLayoutType, 
                                          out enmLayoutLevel, 
                                          out bolIsJointDetail, 
                                          out intMaxRepeats);
                        
                        layout.LayoutType = enmLayoutType;
                        layout.LayoutLevel = enmLayoutLevel;
                        
                        //TODO: Sort Nodes by row index
                        foreach(XmlNode nodeLayoutField in nodeLayout.SelectNodes("LayoutField")) {
                            if(ParseLayoutFieldNode(nodeLayoutField, layout, out objLayoutField)) {
                                layout.AddLayoutField(objLayoutField);
                            }
                        }
                        
                        if(nodeLayout.Attributes["UseOccursGroups"] != null) {
                            if(int.TryParse(nodeLayout.Attributes["UseOccursGroups"].InnerText.Trim(), out intTemp)) {
                                layout.UseOccursGroups = intTemp > 0;
                            } else {
                                layout.UseOccursGroups = false;
                            }
                        }
                        
                        if(layout.LayoutLevel == LayoutLevelEnum.JointDetail) {
                            if(layout.UseOccursGroups) {
                                layout.MaxRepeats = intMaxRepeats;
                            } else {
                                layout.MaxRepeats = 0;
                            }
                            AddJointDetailLayout(layout);
                        } else {
                            layout.MaxRepeats = 0;
                            AddLayout(layout);
                        }
                    }
                }

                _BankOrderByColumns = GetOrderByColumns(extractDefDoc.DocumentElement.SelectNodes("OrderByColumns[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Bank) + "']"));
                _LockboxOrderByColumns = GetOrderByColumns(extractDefDoc.DocumentElement.SelectNodes("OrderByColumns[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.ClientAccount) + "']"));
                _BatchOrderByColumns = GetOrderByColumns(extractDefDoc.DocumentElement.SelectNodes("OrderByColumns[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Batch) + "']"));
                _TransactionsOrderByColumns = GetOrderByColumns(extractDefDoc.DocumentElement.SelectNodes("OrderByColumns[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Transaction) + "']"));
                _PaymentsOrderByColumns = GetOrderByColumns(extractDefDoc.DocumentElement.SelectNodes("OrderByColumns[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Payment) + "']"));
                _StubsOrderByColumns = GetOrderByColumns(extractDefDoc.DocumentElement.SelectNodes("OrderByColumns[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Stub) + "']"));
                _DocumentsOrderByColumns = GetOrderByColumns(extractDefDoc.DocumentElement.SelectNodes("OrderByColumns[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Document) + "']"));

                _BankLimitItems = GetLimitItems(extractDefDoc.DocumentElement.SelectNodes("LimitItems[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Bank) + "']"));
                _LockboxLimitItems = GetLimitItems(extractDefDoc.DocumentElement.SelectNodes("LimitItems[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.ClientAccount) + "']"));
                _BatchLimitItems = GetLimitItems(extractDefDoc.DocumentElement.SelectNodes("LimitItems[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Batch) + "']"));
                _TransactionsLimitItems = GetLimitItems(extractDefDoc.DocumentElement.SelectNodes("LimitItems[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Transaction) + "']"));
                _PaymentsLimitItems = GetLimitItems(extractDefDoc.DocumentElement.SelectNodes("LimitItems[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Payment) + "']"));
                _StubsLimitItems = GetLimitItems(extractDefDoc.DocumentElement.SelectNodes("LimitItems[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Stub) + "']"));
                _DocumentsLimitItems = GetLimitItems(extractDefDoc.DocumentElement.SelectNodes("LimitItems[@LayoutLevel='" + DecodeLayoutLevel(LayoutLevelEnum.Document) + "']"));

                //TODO: Refactor!
                //_DefaultBankID = GetDefaultIDs(ExtractDefDoc.DocumentElement.SelectNodes("Bank_Limits"), "BankID", 1);
                //_DefaultCustomerID = GetDefaultIDs(ExtractDefDoc.DocumentElement.SelectNodes("Customer_Limits"), "CustomerID", 0);
                //_DefaultLockboxID = GetDefaultIDs(ExtractDefDoc.DocumentElement.SelectNodes("Lockbox_Limits"), "LockboxID", 1);
                //_DefaultBatchID = GetDefaultIDs(ExtractDefDoc.DocumentElement.SelectNodes("Batch_Limits"), "BatchID", 1);

                _PostProcCodeModules = GetPostProcCodeModules(extractDefDoc.DocumentElement.SelectNodes("PostProcCodeModules/PostProcCodeModule"));

                bolRetVal = true;

            } catch(Exception e) {
                OnOutputError(this, e);
                bolRetVal = false;
            }

            return(bolRetVal);
        }

        public static bool ParseLayoutFieldNode(
            XmlNode nodeLayoutField, 
            cLayout layout, 
            out cLayoutField layoutField) {
            
            cLayoutField objLayoutField = null;
            int intTemp;
            char chrTemp;
            bool bolRetVal;

            int intDataType;
            int intColumnWidth;

            try {

                if(nodeLayoutField == null) {
                    bolRetVal = false;
                } else {

                    bolRetVal = true;

                    // Data Type
                    if(nodeLayoutField.Attributes["DataType"] != null && int.TryParse(nodeLayoutField.Attributes["DataType"].InnerText, out intTemp)) {
                        intDataType = intTemp;
                    } else {
                        intDataType = 0;
                    }
                    
                    // Column Width
                    if(nodeLayoutField.Attributes["ColumnWidth"] != null && int.TryParse(nodeLayoutField.Attributes["ColumnWidth"].InnerText, out intTemp)) {
                        intColumnWidth = intTemp;
                    } else {
                        intColumnWidth = 0;
                    }

                    // Field Name
                    if(nodeLayoutField.Attributes["FieldName"] != null) {
                        objLayoutField = new cLayoutField(layout, TranslateFieldNameToRecHub(nodeLayoutField.Attributes["FieldName"].InnerText), intDataType, intColumnWidth);
                    } else {
                        layoutField = null;
                        return(false);
                    }

                    // Display Name
                    if(nodeLayoutField.Attributes["DisplayName"] != null) {
                        objLayoutField.DisplayName = nodeLayoutField.Attributes["DisplayName"].InnerText;
                    } else {
                        bolRetVal = false;
                    }
                    
                    // Pad Character
                    if(nodeLayoutField.Attributes["PadChar"] != null && char.TryParse(nodeLayoutField.Attributes["PadChar"].InnerText, out chrTemp)) {
                        objLayoutField.PadChar = chrTemp;
                    } else {
                        objLayoutField.PadChar = ' ';
                    }

                    // Justification
                    if(nodeLayoutField.Attributes["Justification"] != null && nodeLayoutField.Attributes["Justification"].InnerText == "R") {
                        objLayoutField.Justification = JustifyEnum.Right;
                    } else {
                        objLayoutField.Justification = JustifyEnum.Left;
                    }

                    // Use Quotes?
                    if(nodeLayoutField.Attributes["Quotes"] != null && nodeLayoutField.Attributes["Quotes"].InnerText == "1") {
                        objLayoutField.Quotes = true;
                    } else {
                        objLayoutField.Quotes = false;
                    }
                    
                    // Format
                    if(nodeLayoutField.Attributes["Format"] != null) {
                        objLayoutField.Format = nodeLayoutField.Attributes["Format"].InnerText;
                    }

                    // Occurs Columns
                    if(nodeLayoutField.Attributes["OccursGroup"] != null && int.TryParse(nodeLayoutField.Attributes["OccursGroup"].InnerText, out intTemp)) {

                        if(objLayoutField.FieldName.ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER)||
                           objLayoutField.FieldName.ToLower().StartsWith("rechubdata.checksdataentry") ||
                           objLayoutField.FieldName.ToLower().StartsWith("rechubdata.dimddas")) {
                            layout.UsePaymentsOccursGroups = true;
                        }
                        else if(objLayoutField.FieldName.ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER)||
                                  objLayoutField.FieldName.ToLower().StartsWith("rechubdata.stubsdataentry")) {
                            layout.UseStubsOccursGroups = true;
                        }
                        else if(objLayoutField.FieldName.ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER)||
                            objLayoutField.FieldName.ToLower().StartsWith("rechubdata.dimdocumenttypes")) {
                            layout.UseDocumentsOccursGroups = true;
                        }                    
                    }
                    if(nodeLayoutField.Attributes["OccursCount"] != null && int.TryParse(nodeLayoutField.Attributes["OccursCount"].InnerText, out intTemp)) {

                        if(objLayoutField.FieldName.ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER)||
                           objLayoutField.FieldName.ToLower().StartsWith("rechubdata.checksdataentry") ||
                           objLayoutField.FieldName.ToLower().StartsWith("rechubdata.dimddas")) {
                            layout.PaymentsOccursCount = intTemp;
                        }
                        else if(objLayoutField.FieldName.ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER)||
                                  objLayoutField.FieldName.ToLower().StartsWith("rechubdata.stubsdataentry")) {
                            layout.StubsOccursCount = intTemp;
                        }
                        else if(objLayoutField.FieldName.ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER)||
                            objLayoutField.FieldName.ToLower().StartsWith("rechubdata.dimdocumenttypes")) {
                            layout.DocumentsOccursCount = intTemp;
                        }
                    }

                    // Data entry values.
                    bool tmpboolval;
                    if (nodeLayoutField.Attributes["IsDataEntry"] != null && bool.TryParse(nodeLayoutField.Attributes["IsDataEntry"].InnerText, out tmpboolval))
                        objLayoutField.IsDataEntry = tmpboolval;
                    else
                        objLayoutField.IsDataEntry = false;

                    if (nodeLayoutField.Attributes["DisplayName"] != null)
                        objLayoutField.DisplayName = nodeLayoutField.Attributes["DisplayName"].InnerText;

                    if (nodeLayoutField.Attributes["SiteBankID"] != null && int.TryParse(nodeLayoutField.Attributes["SiteBankID"].InnerText, out intTemp))
                        objLayoutField.SiteBankID = intTemp;

                    if (nodeLayoutField.Attributes["SiteClientAccountID"] != null && int.TryParse(nodeLayoutField.Attributes["SiteClientAccountID"].InnerText, out intTemp))
                        objLayoutField.SiteClientAccountID = intTemp;

                    if (nodeLayoutField.Attributes["PaymentSource"] != null)
                        objLayoutField.PaymentSource = nodeLayoutField.Attributes["PaymentSource"].InnerText;

                    layoutField = objLayoutField;
                }
            
            } catch(Exception) {
                bolRetVal = false;
            }
            
            if(bolRetVal) {
                layoutField = objLayoutField;
            } else {
                layoutField = null;
            }
            
            return(bolRetVal);
        }

        private List<cOrderByColumn> GetOrderByColumns(XmlNodeList orderByColumnSections) {

            List<cOrderByColumn> arRetVal = new List<cOrderByColumn>();
            cOrderByColumn objOrderByColumn;

            if(orderByColumnSections != null) {

                foreach(XmlNode nodeOrderByColumnSection in orderByColumnSections) {

                    foreach(XmlNode nodeOrderByColumn in nodeOrderByColumnSection.ChildNodes) {

                        if(nodeOrderByColumn.Attributes["ColumnName"] != null && nodeOrderByColumn.Attributes["ColumnName"].InnerText.Length > 0) 
                        {
                            objOrderByColumn = new cOrderByColumn(TranslateFieldNameToRecHub(nodeOrderByColumn.Attributes["ColumnName"].InnerText),
                                TranslateFieldNameToRecHub(nodeOrderByColumn.Attributes["FullColumnName"].InnerText));

                            if (nodeOrderByColumn.Attributes["DisplayName"] != null)
                            {
                                objOrderByColumn.DisplayName = nodeOrderByColumn.Attributes["DisplayName"].InnerText;
                            }
                            else
                            {
                                // older version of the xml format. Just load in the fullcolumnname instead.
                                objOrderByColumn.DisplayName = objOrderByColumn.ColumnName;
                            }

                            if (nodeOrderByColumn.Attributes["OrderByDir"] != null && nodeOrderByColumn.Attributes["OrderByDir"].InnerText == "DESC") 
                            {
                                objOrderByColumn.OrderByDir = OrderByDirEnum.Descending;
                            } 
                            else 
                            {
                                objOrderByColumn.OrderByDir = OrderByDirEnum.Ascending;
                            }

                            arRetVal.Add(objOrderByColumn);
                        }
                    }
                }
            }
            
            return(arRetVal);
        }

        private List<cLimitItem> GetLimitItems(XmlNodeList limitItemSections) {

            List<cLimitItem> arRetVal = new List<cLimitItem>();
            cLimitItem objLimitItem;
            int intTemp;

            if(limitItemSections != null) {

                foreach(XmlNode nodeLimitItemSection in limitItemSections) {

                    foreach(XmlNode nodeLimitItem in nodeLimitItemSection.ChildNodes) {

                        try {
                            objLimitItem = new cLimitItem();

                            if(nodeLimitItem.Attributes["FieldName"] != null && nodeLimitItem.Attributes["FieldName"].InnerText.Length > 0) {
                                objLimitItem.FieldName = TranslateFieldNameToRecHub(nodeLimitItem.Attributes["FieldName"].InnerText);
                            } else {
                                throw(new Exception("Invalid Field Name."));
                            }

                            if(nodeLimitItem.Attributes["LogicalOperator"] != null) {

                                switch(nodeLimitItem.Attributes["LogicalOperator"].InnerText.ToLower().Trim()) {
                                    case "=":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.Equals;
                                        break;
                                    case "<>":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.NotEqualTo;
                                        break;
                                    case ">":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.GreaterThan;
                                        break;
                                    case ">=":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.GreaterThanOrEqualTo;
                                        break;
                                    case "<":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.LessThan;
                                        break;
                                    case "<=":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.LessThanOrEqualTo;
                                        break;
                                    case "between":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.Between;
                                        break;
                                    case "in":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.In;
                                        break;
                                    case "is null":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.IsNull;
                                        break;
                                    case "is not null":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.IsNotNull;
                                        break;
                                    case "like":
                                        objLimitItem.LogicalOperator = LogicalOperatorEnum.Like;
                                        break;
                                    //default:
                                    //    throw(new Exception("Invalid Logical Operator."));
                                }
                            }

                            if(nodeLimitItem.Attributes["Value"] != null) {
                                objLimitItem.Value = nodeLimitItem.Attributes["Value"].InnerText;
                            }

                            // WI 115249 : added schema and table.
                            if (nodeLimitItem.Attributes["Schema"] != null)
                            {
                                objLimitItem.Schema = nodeLimitItem.Attributes["Schema"].InnerText;
                            }
                            if (nodeLimitItem.Attributes["Table"] != null)
                            {
                                objLimitItem.Table = nodeLimitItem.Attributes["Table"].InnerText;
                            }
                            if (nodeLimitItem.Attributes["PaymentSource"] != null)
                            {
                                objLimitItem.PaymentSource = nodeLimitItem.Attributes["PaymentSource"].InnerText;
                            }
                            if (nodeLimitItem.Attributes["DataType"] != null)
                            {
                                if (int.TryParse(nodeLimitItem.Attributes["DataType"].InnerText, out intTemp))
                                {
                                    objLimitItem.DataType = intTemp;
                                }
                                else
                                {
                                    objLimitItem.DataType = 0;
                                }
                            }
                            if (nodeLimitItem.Attributes["DisplayName"] != null)
                            {
                                objLimitItem.DisplayName = nodeLimitItem.Attributes["DisplayName"].InnerText;
                            }
                            else
                            {
                                // older version of the xml format. Just load in the fullcolumnname instead.
                                objLimitItem.DisplayName = objLimitItem.FieldName;
                            }

                            if (nodeLimitItem.Attributes["LeftParenCount"] != null) {
                                if(int.TryParse(nodeLimitItem.Attributes["LeftParenCount"].InnerText, out intTemp)) {
                                    objLimitItem.LeftParenCount = intTemp;
                                } else {
                                    objLimitItem.LeftParenCount = 0;
                                }
                            }

                            if(nodeLimitItem.Attributes["RightParenCount"] != null) {
                                if(int.TryParse(nodeLimitItem.Attributes["RightParenCount"].InnerText, out intTemp)) {
                                    objLimitItem.RightParenCount = intTemp;
                                } else {
                                    objLimitItem.RightParenCount = 0;
                                }
                            }

                            if(nodeLimitItem.Attributes["CompoundOperator"] != null) {
                                switch(nodeLimitItem.Attributes["CompoundOperator"].InnerText) {
                                    case "and":
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.And;
                                        break;
                                    case "and not":
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.AndNot;
                                        break;
                                    case "or":
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.Or;
                                        break;
                                    case "or not":
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.OrNot;
                                        break;
                                    default:
                                        objLimitItem.CompoundOperator = CompoundOperatorEnum.None;
                                        break;
                                }
                            }
                            
                            arRetVal.Add(objLimitItem);

                        } catch(Exception e) {
                            OnOutputError(this, e);
                        }
                    }
                }
            }
            
            return(arRetVal);
        }

        private List<cPostProcCodeModule> GetPostProcCodeModules(XmlNodeList postProcCodeModules) {

            List<cPostProcCodeModule> objRetVal = new List<cPostProcCodeModule>();
            XmlAttribute attSrcCode;
            XmlAttribute attHashCode;
            string strhashcode;

            foreach(XmlNode nodePostProcCodeModule in postProcCodeModules) {
                attSrcCode = nodePostProcCodeModule.Attributes["SrcCode"];
                if(attSrcCode != null) {

                    attHashCode = nodePostProcCodeModule.Attributes["HashCode"];
                    if(attHashCode != null) {
                        strhashcode = attHashCode.Value;
                    } else {
                        strhashcode = string.Empty;
                    }

                    objRetVal.Add(new cPostProcCodeModule(attSrcCode.Value, strhashcode));
                }
            }
            
            return(objRetVal);
        }
        
        #endregion Xml Input Methods


        #region Xml Output Methods
        
        public bool SaveAsXml(string fileName) {

            XmlDocument docXml;
            bool bolRetVal;

            try {
                docXml = this.ToXml();
                docXml.Save(fileName);

                _ExtractDefFilePath = fileName;

                OnExtractSaved(fileName);

                bolRetVal = true;
            } catch(Exception ex) {
                OnOutputError(this, ex);
                bolRetVal = false;
            }
            
            return(bolRetVal);
        }

        public XmlDocument ToXml() {

            XmlDocument docRetVal;
            XmlNode nodeRoot;
            XmlNode nodeSectionHeader;

            docRetVal = new XmlDocument();
            
            nodeRoot = docRetVal.CreateElement("ExtractDef");


            // VERSION
            nodeSectionHeader = ipoXmlLib.addElement(nodeRoot, "VERSION", string.Empty);
            ipoXmlLib.addElement(nodeSectionHeader, "Version", this.Version);


            // GENERAL
            nodeSectionHeader = ipoXmlLib.addElement(nodeRoot, "GENERAL", string.Empty);
            ipoXmlLib.addElement(nodeSectionHeader, "ExtractPath", this.ExtractPath);
            ipoXmlLib.addElement(nodeSectionHeader, "LogFilePath", this.LogFilePath);
            ipoXmlLib.addElement(nodeSectionHeader, "ImagePath", this.ImagePath);
            ipoXmlLib.addElement(nodeSectionHeader, "UseQuotes", (this.UseQuotes ? "1" : "0"));
            ipoXmlLib.addElement(nodeSectionHeader, "NoPad", (this.NoPad ? "1" : "0"));
            ipoXmlLib.addElement(nodeSectionHeader, "NullAsSpace", (this.NullAsSpace ? "1" : "0"));
            ipoXmlLib.addElement(nodeSectionHeader, "TimeStamp", this.TimeStamp);
            ipoXmlLib.addElement(nodeSectionHeader, "FieldDelim", this.FieldDelim);
            ipoXmlLib.addElement(nodeSectionHeader, "RecordDelim", this.RecordDelim);
            ipoXmlLib.addElement(nodeSectionHeader, "IncludeStubRowsWithAllNulls", (this.IncludeNULLRows ? "1" : "0"));
            ipoXmlLib.addElement(nodeSectionHeader, "PrinterName", this.PrinterName);
            ipoXmlLib.addElement(nodeSectionHeader, "ZipOutputFiles", (this.ZipOutputFiles ? "1" : "0"));
            ipoXmlLib.addElement(nodeSectionHeader, "ZipFilePath", this.ZipFilePath);


            // IMAGEFILES
            nodeSectionHeader = ipoXmlLib.addElement(nodeRoot, "IMAGEFILES", string.Empty);
            ipoXmlLib.addElement(nodeSectionHeader, "ImageFileFormat", ((int)this.ImageFileFormat).ToString());
            //ipoXmlLib.addElement(nodeSectionHeader, "CombineImagesAccrossProcDates", (this.CombineImagesAccrossProcDates ? "1" : "0"));
            ipoXmlLib.addElement(nodeSectionHeader, "ImageFileNameSingle", this.ImageFileNameSingle);
            ipoXmlLib.addElement(nodeSectionHeader, "ImageFileNamePerTran", this.ImageFileNamePerTran);
            ipoXmlLib.addElement(nodeSectionHeader, "ImageFileNamePerBatch", this.ImageFileNamePerBatch);

            ipoXmlLib.addElement(nodeSectionHeader, "ImageFileProcDateFormat", this.ImageFileProcDateFormat);
            ipoXmlLib.addElement(nodeSectionHeader, "ImageFileZeroPad", (this.ImageFileZeroPad ? "1" : "0"));
            ipoXmlLib.addElement(nodeSectionHeader, "ImageFileBatchTypeFormatFull", (this.ImageFileBatchTypeFormatFull ? "1" : "0"));
            ipoXmlLib.addElement(nodeSectionHeader, "IncludeImageFolderPath", (this.IncludeImageFolderPath ? "1" : "0"));

            // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
            ipoXmlLib.addElement(nodeSectionHeader, "IncludeFooter", (this.IncludeFooter ? "1" : "0"));

            // Applies to Stubs only.
            ipoXmlLib.addElement(nodeSectionHeader, "IncludeNULLRows", (this.IncludeNULLRows ? "1" : "0"));


            // POSTDLL
            nodeSectionHeader = ipoXmlLib.addElement(nodeRoot, "POSTDLL", string.Empty);
            ipoXmlLib.addElement(nodeSectionHeader, "PostProcDLLFileName", this.PostProcDLLFileName);

            // Layout Sections
            nodeSectionHeader = ipoXmlLib.addElement(nodeRoot, "Layouts", string.Empty);

            foreach(cLayout layout in this.AllLayouts) {
                LayoutToXml(layout, ref nodeSectionHeader);
            }

            OrderByColumnsToXml(BankOrderByColumns, LayoutLevelEnum.Bank, ref nodeRoot);
            OrderByColumnsToXml(LockboxOrderByColumns, LayoutLevelEnum.ClientAccount, ref nodeRoot);
            OrderByColumnsToXml(BatchOrderByColumns, LayoutLevelEnum.Batch, ref nodeRoot);
            OrderByColumnsToXml(TransactionsOrderByColumns, LayoutLevelEnum.Transaction, ref nodeRoot);
            OrderByColumnsToXml(PaymentsOrderByColumns, LayoutLevelEnum.Payment, ref nodeRoot);
            OrderByColumnsToXml(StubsOrderByColumns, LayoutLevelEnum.Stub, ref nodeRoot);
            OrderByColumnsToXml(DocumentsOrderByColumns, LayoutLevelEnum.Document, ref nodeRoot);

            LimitItemsToXml(BankLimitItems, LayoutLevelEnum.Bank, ref nodeRoot);
            LimitItemsToXml(LockboxLimitItems, LayoutLevelEnum.ClientAccount, ref nodeRoot);
            LimitItemsToXml(BatchLimitItems, LayoutLevelEnum.Batch, ref nodeRoot);
            LimitItemsToXml(TransactionsLimitItems, LayoutLevelEnum.Transaction, ref nodeRoot);
            LimitItemsToXml(PaymentsLimitItems, LayoutLevelEnum.Payment, ref nodeRoot);
            LimitItemsToXml(StubsLimitItems, LayoutLevelEnum.Stub, ref nodeRoot);
            LimitItemsToXml(DocumentsLimitItems, LayoutLevelEnum.Document, ref nodeRoot);
            
            CodeModulesToXml(PostProcCodeModules, ref nodeRoot);
            
            docRetVal.AppendChild(nodeRoot);

            return(docRetVal);
        }

        public static void LayoutToXml(cLayout layout, ref XmlNode nodeRoot) {

            XmlNode nodeLayout;
            XmlNode nodeLayoutField;

            nodeLayout = ipoXmlLib.addElement(nodeRoot, "Layout", string.Empty);
            ipoXmlLib.addAttribute(nodeLayout, "LayoutName", layout.LayoutName);
            ipoXmlLib.addAttribute(nodeLayout, "RecordCode", EncodeLayoutRecord(layout));
            ipoXmlLib.addAttribute(nodeLayout, "UseOccursGroups", (layout.UseOccursGroups ? "1" : "0"));

            foreach(cLayoutField layoutfield in layout.LayoutFields) {

                nodeLayoutField = ipoXmlLib.addElement(nodeLayout, "LayoutField", "");
                
                ipoXmlLib.addAttribute(nodeLayoutField, "FieldName", layoutfield.FieldName);
                ipoXmlLib.addAttribute(nodeLayoutField, "DataType", layoutfield.DataType.ToString());
                ipoXmlLib.addAttribute(nodeLayoutField, "DisplayName", layoutfield.DisplayName);
                ipoXmlLib.addAttribute(nodeLayoutField, "ColumnWidth", layoutfield.ColumnWidth.ToString());
                ipoXmlLib.addAttribute(nodeLayoutField, "PadChar", layoutfield.PadChar.ToString());
                ipoXmlLib.addAttribute(nodeLayoutField, "Justification", (layoutfield.Justification == JustifyEnum.Right ? "R" : "L"));
                ipoXmlLib.addAttribute(nodeLayoutField, "Quotes", (layoutfield.Quotes ? "1" : "0"));
                ipoXmlLib.addAttribute(nodeLayoutField, "Format", layoutfield.Format);
                ipoXmlLib.addAttribute(nodeLayoutField, "IsDataEntry", layoutfield.IsDataEntry.ToString());
                ipoXmlLib.addAttribute(nodeLayoutField, "DisplayName", layoutfield.DisplayName);
                ipoXmlLib.addAttribute(nodeLayoutField, "SiteBankID", layoutfield.SiteBankID.ToString());
                ipoXmlLib.addAttribute(nodeLayoutField, "SiteClientAccountID", layoutfield.SiteClientAccountID.ToString());
                ipoXmlLib.addAttribute(nodeLayoutField, "PaymentSource", layoutfield.PaymentSource);

                if (layout.LayoutLevel == LayoutLevelEnum.JointDetail) {

                    if(layoutfield.FieldName.ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER) ||
                       layoutfield.FieldName.ToLower().StartsWith("rechubdata.checksdataentry") ||
                       layoutfield.FieldName.ToLower().StartsWith("rechubdata.dimddas")
                       ) {
                        ipoXmlLib.addAttribute(nodeLayoutField, "OccursGroup", (layout.UsePaymentsOccursGroups ? "0" : string.Empty));
                        ipoXmlLib.addAttribute(nodeLayoutField, "OccursCount", (layout.PaymentsOccursCount > -1 ? layout.PaymentsOccursCount.ToString() : string.Empty));
                    }
                    else if(layoutfield.FieldName.ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER) ||
                        layoutfield.FieldName.ToLower().StartsWith("rechubdata.stubsdataentry")
                        ) {
                        ipoXmlLib.addAttribute(nodeLayoutField, "OccursGroup", (layout.UseStubsOccursGroups ? "1" : string.Empty));
                        ipoXmlLib.addAttribute(nodeLayoutField, "OccursCount", (layout.StubsOccursCount > -1 ? layout.StubsOccursCount.ToString() : string.Empty));
                    }
                    else if(layoutfield.FieldName.ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER) ||
                         layoutfield.FieldName.ToLower().StartsWith("rechubdata.dimdocumenttypes")
                        ) {
                        ipoXmlLib.addAttribute(nodeLayoutField, "OccursGroup", (layout.UseDocumentsOccursGroups ? "2" : string.Empty));
                        ipoXmlLib.addAttribute(nodeLayoutField, "OccursCount", (layout.DocumentsOccursCount > -1 ? layout.DocumentsOccursCount.ToString() : string.Empty));
                    }

                }
            }
        }

        private static void OrderByColumnsToXml(
            IOrderByColumn[] orderByColumns, 
            LayoutLevelEnum layoutLevel, 
            ref XmlNode nodeRoot) {

            XmlNode nodeOrderByColumns;
            XmlNode nodeOrderByColumn;

            if(orderByColumns.Length > 0) {

                nodeOrderByColumns = ipoXmlLib.addElement(nodeRoot, "OrderByColumns", "");

                ipoXmlLib.addAttribute(nodeOrderByColumns, "LayoutLevel", DecodeLayoutLevel(layoutLevel));

                foreach(cOrderByColumn OrderByColumn in orderByColumns) {
                    nodeOrderByColumn = ipoXmlLib.addElement(nodeOrderByColumns, "OrderByColumn", "");
                    ipoXmlLib.addAttribute(nodeOrderByColumn, "ColumnName", OrderByColumn.ColumnName);
                    ipoXmlLib.addAttribute(nodeOrderByColumn, "DisplayName", OrderByColumn.DisplayName);
                    ipoXmlLib.addAttribute(nodeOrderByColumn, "FullColumnName", OrderByColumn.FullName);
                    ipoXmlLib.addAttribute(nodeOrderByColumn, "OrderByDir", (OrderByColumn.OrderByDir == OrderByDirEnum.Ascending ? "ASC" : "DESC"));
                }
            }
        }

        private static void LimitItemsToXml(
            ILimitItem[] limitItems, 
            LayoutLevelEnum layoutLevel, 
            ref XmlNode nodeRoot) {

            XmlNode nodeLimitItems;
            XmlNode nodeLimitItem;
            
            if(limitItems.Length > 0) {
                nodeLimitItems = ipoXmlLib.addElement(nodeRoot, "LimitItems", "");

                ipoXmlLib.addAttribute(nodeLimitItems, "LayoutLevel", DecodeLayoutLevel(layoutLevel));

                foreach(cLimitItem LimitItem in limitItems) {
                    nodeLimitItem = ipoXmlLib.addElement(nodeLimitItems, "LimitItem", "");

                    ipoXmlLib.addAttribute(nodeLimitItem, "FieldName", LimitItem.FieldName);
                    // WI 115249 : Added Tablename/Schemaname.
                    ipoXmlLib.addAttribute(nodeLimitItem, "Schema", LimitItem.Schema);
                    ipoXmlLib.addAttribute(nodeLimitItem, "Table", LimitItem.Table);
                    ipoXmlLib.addAttribute(nodeLimitItem, "DisplayName", LimitItem.DisplayName);
                    ipoXmlLib.addAttribute(nodeLimitItem, "PaymentSource", LimitItem.PaymentSource);
                    ipoXmlLib.addAttribute(nodeLimitItem, "DataType", LimitItem.DataType.ToString());
                    ipoXmlLib.addAttribute(nodeLimitItem, "LogicalOperator", DecodeLogicalOperator(LimitItem.LogicalOperator));
                    ipoXmlLib.addAttribute(nodeLimitItem, "Value", LimitItem.Value);
                    ipoXmlLib.addAttribute(nodeLimitItem, "LeftParenCount", LimitItem.LeftParenCount.ToString());
                    ipoXmlLib.addAttribute(nodeLimitItem, "RightParenCount", LimitItem.RightParenCount.ToString());
                    ipoXmlLib.addAttribute(nodeLimitItem, "CompoundOperator", DecodeCompoundOperator(LimitItem.CompoundOperator));
                }
            }
        }

        private void CodeModulesToXml(
            List<cPostProcCodeModule> postProcCodeModules, 
            ref XmlNode nodeRoot) {

            XmlNode nodePostProcCodeModules;
            XmlNode nodePostProcCodeModule;
            
            if(postProcCodeModules.Count > 0) {
                nodePostProcCodeModules = ipoXmlLib.addElement(nodeRoot, "PostProcCodeModules", "");

                foreach(cPostProcCodeModule postproccodemodule in postProcCodeModules) {
                    nodePostProcCodeModule = ipoXmlLib.addElement(nodePostProcCodeModules, "PostProcCodeModule", "");

                    ipoXmlLib.addAttribute(nodePostProcCodeModule, "SrcCode", postproccodemodule.SrcCode);
                    ipoXmlLib.addAttribute(nodePostProcCodeModule, "HashCode", HashGenerator.SHA512(postproccodemodule.SrcCode));
                }
            }
        }
        #endregion Xml Output Methods


        #region Add Object Methods

        public void AddLayout(cLayout layout) {

            switch(layout.LayoutLevel) {
                case LayoutLevelEnum.File:
                    _FileLayouts.Add(layout);
                    break;
                case LayoutLevelEnum.Bank:
                    _BankLayouts.Add(layout);
                    break;
                case LayoutLevelEnum.ClientAccount:
                    _LockboxLayouts.Add(layout);
                    break;
                case LayoutLevelEnum.Batch:
                    _BatchLayouts.Add(layout);
                    break;
                case LayoutLevelEnum.Transaction:
                    _TransactionLayouts.Add(layout);
                    break;
                case LayoutLevelEnum.Payment:
                    _PaymentLayouts.Add(layout);
                    break;
                case LayoutLevelEnum.Stub:
                    _StubLayouts.Add(layout);
                    break;
                case LayoutLevelEnum.Document:
                    _DocumentLayouts.Add(layout);
                    break;
                case LayoutLevelEnum.JointDetail:
                    throw(new Exception("Invalid Layout Level; Calling method should use AddJointDetailLayout() method instead."));
            }
        }

        public void AddJointDetailLayout(cLayout layout) {

            switch(layout.LayoutLevel) {
                case LayoutLevelEnum.JointDetail:
                    _JointDetailItemLayouts.Add(layout);
                    break;
                default:
                    throw(new Exception("Invalid Layout Level; Calling method should use AddLayout() method instead."));
            }
        }

        public void AddOrderByColumn(
            LayoutLevelEnum layoutLevel, 
            cOrderByColumn orderByColumn) {

                switch(layoutLevel) {
                case LayoutLevelEnum.Bank:
                    _BankOrderByColumns.Add(orderByColumn);
                    break;   
                case LayoutLevelEnum.ClientAccount:
                    _LockboxOrderByColumns.Add(orderByColumn);
                    break;    
                case LayoutLevelEnum.Batch:
                    _BatchOrderByColumns.Add(orderByColumn);
                    break;    
                case LayoutLevelEnum.Transaction:
                    _TransactionsOrderByColumns.Add(orderByColumn);
                    break;    
                case LayoutLevelEnum.Payment:
                    _PaymentsOrderByColumns.Add(orderByColumn);
                    break;    
                case LayoutLevelEnum.Stub:
                    _StubsOrderByColumns.Add(orderByColumn);
                    break;    
                case LayoutLevelEnum.Document:
                    _DocumentsOrderByColumns.Add(orderByColumn);
                    break;    
                default:
                    // Invalid
                    break;
            }
        }
        
        public void AddLimitItem(
            LayoutLevelEnum layoutLevel, 
            cLimitItem limitItem) {
            
            switch(layoutLevel) {
                case LayoutLevelEnum.Bank:
                    _BankLimitItems.Add(limitItem);
                    break;    
                case LayoutLevelEnum.ClientAccount:
                    _LockboxLimitItems.Add(limitItem);
                    break;    
                case LayoutLevelEnum.Batch:
                    _BatchLimitItems.Add(limitItem);
                    break;    
                case LayoutLevelEnum.Transaction:
                    _TransactionsLimitItems.Add(limitItem);
                    break;    
                case LayoutLevelEnum.Payment:
                    _PaymentsLimitItems.Add(limitItem);
                    break;    
                case LayoutLevelEnum.Stub:
                    _StubsLimitItems.Add(limitItem);
                    break;    
                case LayoutLevelEnum.Document:
                    _DocumentsLimitItems.Add(limitItem);
                    break;    
                default:
                    // Invalid
                    break;
            }
        }
        
        public void AddPostProcCodeModule(cPostProcCodeModule postProcCodeModule) {
            _PostProcCodeModules.Add(postProcCodeModule);
        }
        #endregion Add Object Methods


        #region Record Code methods
        private bool ParseLayoutRecord(
            string recordCode, 
            out LayoutTypeEnum layoutType,
            out LayoutLevelEnum layoutLevel, 
            out bool isJointDetail, 
            out int maxRepeats) {

            bool bolRetVal = true;
            string strPaddedRecordCode;
            int intTemp;
            
            strPaddedRecordCode = recordCode.PadLeft(6, '0');
            
            if(strPaddedRecordCode.Substring(0, 2) != "00" && int.TryParse(strPaddedRecordCode.Substring(0, 2), out intTemp)) {
                maxRepeats = intTemp;
            } else {
                maxRepeats = 0;
            }

            //if(strPaddedRecordCode.Substring(2, 2) == "01") {
            //    LayoutType = LayoutTypeEnum.Trailer;
            //} else {
            //    LayoutType = LayoutTypeEnum.Header;
            //}

            if((recordCode.Length == 3)||(recordCode.Length == 4 && recordCode.Substring(1, 1) == "1")) {
                layoutType = LayoutTypeEnum.Trailer;
            } else {
                layoutType = LayoutTypeEnum.Header;
            }

            if(strPaddedRecordCode.Substring(2, 2) == "10") {

                isJointDetail = true;
            
                switch(strPaddedRecordCode.Substring(4, 2)) {
                    case "01":
                        layoutLevel = LayoutLevelEnum.File;
                        
                        // Cannot have Joint Detail record at the File level, so correct this automatically.
                        isJointDetail = false;

                        break;
                    case "02":
                        layoutLevel = LayoutLevelEnum.Bank;
                        break;
                    case "04":
                        layoutLevel = LayoutLevelEnum.ClientAccount;
                        break;
                    case "05":
                        layoutLevel = LayoutLevelEnum.Batch;
                        break;
                    case "06":
                        layoutLevel = LayoutLevelEnum.Transaction;
                        break;
                    case "10":
                        layoutLevel = LayoutLevelEnum.JointDetail;
                        break;
                    default:
                        layoutLevel = LayoutLevelEnum.JointDetail;
                        break;
                }
            } else {

                isJointDetail = false;

                switch(strPaddedRecordCode[5]) {
                    case '1':
                        layoutLevel = LayoutLevelEnum.File;
                        break;
                    case '2':
                        layoutLevel = LayoutLevelEnum.Bank;
                        break;
                    case '4':
                        layoutLevel = LayoutLevelEnum.ClientAccount;
                        break;
                    case '5':
                        layoutLevel = LayoutLevelEnum.Batch;
                        break;
                    case '6':
                        layoutLevel = LayoutLevelEnum.Transaction;
                        break;
                    case '7':
                        layoutLevel = LayoutLevelEnum.Payment;
                        break;
                    case '8':
                        layoutLevel = LayoutLevelEnum.Stub;
                        break;
                    case '9':
                        layoutLevel = LayoutLevelEnum.Document;
                        break;
                    default:
                        layoutLevel = LayoutLevelEnum.File;
                        bolRetVal = false;
                        break;
                }
            }

            return(bolRetVal);
        }
        
        private static string EncodeLayoutRecord(cLayout layout) {

            string strRetVal = string.Empty;

            if(layout.LayoutLevel == LayoutLevelEnum.JointDetail) {

                if(layout.MaxRepeats > 0) {
                    strRetVal += layout.MaxRepeats.ToString("00");
                }

                switch(layout.LayoutLevel) {
                    case LayoutLevelEnum.File:
                        strRetVal += "1001";
                        break;
                    case LayoutLevelEnum.Bank:
                        strRetVal += "1002";
                        break;
                    case LayoutLevelEnum.ClientAccount:
                        strRetVal += "1004";
                        break;
                    case LayoutLevelEnum.Batch:
                        strRetVal += "1005";
                        break;
                    case LayoutLevelEnum.Transaction:
                        strRetVal += "1006";
                        break;
                    case LayoutLevelEnum.JointDetail:
                        if(layout.UseOccursGroups) {
                            strRetVal += "1010";
                        } else {
                            strRetVal += "101010";
                        }
                        break;
                    default:
                        // Invalid
                        break;
                }
            } else {

                if(layout.LayoutType == LayoutTypeEnum.Trailer) {
                    strRetVal += "10";
                }

                switch(layout.LayoutLevel) {
                    case LayoutLevelEnum.File:
                        strRetVal += "1";
                        break;
                    case LayoutLevelEnum.Bank:
                        strRetVal += "2";
                        break;
                    case LayoutLevelEnum.ClientAccount:
                        strRetVal += "4";
                        break;
                    case LayoutLevelEnum.Batch:
                        strRetVal += "5";
                        break;
                    case LayoutLevelEnum.Transaction:
                        strRetVal += "6";
                        break;
                    case LayoutLevelEnum.Payment:
                        strRetVal += "7";
                        break;
                    case LayoutLevelEnum.Stub:
                        strRetVal += "8";
                        break;
                    case LayoutLevelEnum.Document:
                        strRetVal += "9";
                        break;
                    default:
                        // Invalid
                        break;
                }
            }

            return(strRetVal);
        }
        #endregion Record Code methods


        #region Static Formatting Methods
        public static string DecodeLogicalOperator(LogicalOperatorEnum logicalOperator) {

            string strRetVal;

            switch(logicalOperator) {
                case LogicalOperatorEnum.Equals:
                    strRetVal = "=";
                    break;
                case LogicalOperatorEnum.NotEqualTo:
                    strRetVal = "<>";
                    break;
                case LogicalOperatorEnum.GreaterThan:
                    strRetVal = ">";
                    break;
                case LogicalOperatorEnum.GreaterThanOrEqualTo:
                    strRetVal = ">=";
                    break;
                case LogicalOperatorEnum.LessThan:
                    strRetVal = "<";
                    break;
                case LogicalOperatorEnum.LessThanOrEqualTo:
                    strRetVal = "<=";
                    break;
                case LogicalOperatorEnum.Between:
                    strRetVal = "between";
                    break;
                case LogicalOperatorEnum.In:
                    strRetVal = "in";
                    break;
                case LogicalOperatorEnum.IsNull:
                    strRetVal = "is null";
                    break;
                case LogicalOperatorEnum.IsNotNull:
                    strRetVal = "is not null";
                    break;
                case LogicalOperatorEnum.Like:
                    strRetVal = "like";
                    break;
                default:
                    strRetVal = string.Empty;
                    break;
            }
            
            return(strRetVal);
        }
        
        public static string DecodeCompoundOperator(CompoundOperatorEnum compoundOperator) {

            string strRetVal;

            switch(compoundOperator) {
                case CompoundOperatorEnum.And:
                    strRetVal = "and";
                    break;
                case CompoundOperatorEnum.AndNot:
                    strRetVal = "and not";
                    break;
                case CompoundOperatorEnum.Or:
                    strRetVal = "or";
                    break;
                case CompoundOperatorEnum.OrNot:
                    strRetVal = "or not";
                    break;
                default:
                    strRetVal = string.Empty;
                    break;
            }
            
            return(strRetVal);
        }

        public static string DecodeLayoutLevel(LayoutLevelEnum layoutLevel) {

            string strRetVal;

            switch(layoutLevel) {
                case LayoutLevelEnum.File:
                    strRetVal = Support.LBL_FILE;
                    break;
                case LayoutLevelEnum.Bank:
                    strRetVal = Support.LBL_BANK;
                    break;
                case LayoutLevelEnum.ClientAccount:
                    strRetVal = Support.LBL_CLIENT_ACCOUNT;
                    break;
                case LayoutLevelEnum.Batch:
                    strRetVal = Support.LBL_BATCH;
                    break;
                case LayoutLevelEnum.Transaction:
                    strRetVal = Support.LBL_TRANSACTION;
                    break;
                case LayoutLevelEnum.Payment:
                    strRetVal = Support.LBL_PAYMENT;
                    break;
                case LayoutLevelEnum.Stub:
                    strRetVal = Support.LBL_STUB;
                    break;
                case LayoutLevelEnum.Document:
                    strRetVal = Support.LBL_DOCUMENT;
                    break;
                case LayoutLevelEnum.JointDetail:
                    strRetVal = Support.LBL_JOINT_DETAIL;
                    break;
                default:
                    strRetVal = string.Empty;
                    break;
            }

            return(strRetVal);
        }
                       
        public static string DecodeFileNameFormat(
            string fileName, 
            bool batchTypeFormatFull, 
            string processingDateFormat, 
            bool zeroPadNumbers, 
            ImageFileFormatEnum imageFileFormat) {

            string strRetVal;

            if(imageFileFormat == ImageFileFormatEnum.Pdf ||
               imageFileFormat == ImageFileFormatEnum.Tiff ||
               imageFileFormat == ImageFileFormatEnum.SendToPrinter) {

                strRetVal = fileName;

                strRetVal = strRetVal.Replace("<BK>", "BANK" + (zeroPadNumbers ? "[0]" : string.Empty));
                strRetVal = strRetVal.Replace("<LB>", "BOX" + (zeroPadNumbers ? "[0]" : string.Empty));
                strRetVal = strRetVal.Replace("<BH>", "BATCH" + (zeroPadNumbers ? "[0]" : string.Empty));
                strRetVal = strRetVal.Replace("<BT>", "BATCHTYPE[" +
                                                      (batchTypeFormatFull ? (zeroPadNumbers ? "0" : string.Empty) + "F" : "1") + 
                                                      "]");
                strRetVal = strRetVal.Replace("<PD>", "PDATE[" + processingDateFormat + "]");
                strRetVal = strRetVal.Replace("<TR>", "TRAN" + (zeroPadNumbers ? "[0]" : string.Empty));
                strRetVal = strRetVal.Replace("<DT>", "DOCTYPE");
                strRetVal = strRetVal.Replace("<FC>", "COUNTER" + (zeroPadNumbers ? "[0]" : string.Empty));

                if(strRetVal.LastIndexOf('.') > -1) {
                    strRetVal = strRetVal.Substring(0, strRetVal.LastIndexOf('.'));
                }

                switch(imageFileFormat) {
                    case ImageFileFormatEnum.Tiff:
                        strRetVal += ".tif";
                        break;
                    case ImageFileFormatEnum.Pdf:
                        strRetVal += ".pdf";
                        break;
                }

            } else {
                strRetVal = "None";
            }

            return(strRetVal);
        }

        #endregion Static Formatting Methods


        private string GetDefaultLogFilePath() {

            string strLogFileDir = string.Empty;
            string strLogFileName = string.Empty;
            string strTemp;
            string strRetVal;

            if(this.ExtractPath.Length > 0) {

                strTemp = Path.GetFileNameWithoutExtension(this.ExtractPath);
                if(strTemp.Length > 0) {
                    strLogFileName = strTemp + ".log";
                } else {
                    strLogFileName = "log.txt";
                }

                strTemp = Path.GetDirectoryName(this.ExtractPath);
                if(strTemp.Length > 0) {
                    strRetVal = Path.Combine(strTemp, strLogFileName);
                } else {
                    strRetVal = strLogFileName;
                }

                return(strRetVal);

            } else {
                return(string.Empty);
            }
        }

        private string GetDefaultImagePath() {

            string strLogFileDir = string.Empty;
            string strTemp;
            string strRetVal;

            if(this.ExtractPath.Length > 0) {

                strTemp = Path.GetDirectoryName(this.ExtractPath);
                if(strTemp.Length > 0) {
                    strRetVal = strTemp;
                } else {
                    strRetVal = string.Empty;
                }

                return(strRetVal);

            } else {
                return(string.Empty);
            }
        }

        private static string TranslateFieldNameToRecHub(string fieldName) {

            string strRetVal;

            if(fieldName.TrimStart().ToLower().StartsWith("bank.bankid")) {
                strRetVal = "RecHubData.dimBanks.SiteBankID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("lockbox.bankid")) {
                strRetVal = "RecHubData.dimClientAccounts.SiteBankID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("lockbox.lockboxid")) {
                strRetVal = "RecHubData.dimClientAccounts.SiteClientAccountID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("batch.bankid")) {
                strRetVal = "RecHubData.dimClientAccounts.SiteBankID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("batch.lockboxid")) {
                strRetVal = "RecHubData.dimClientAccounts.SiteClientAccountID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("batch.batchid")) {
                strRetVal = "RecHubData.factBatchSummary.BatchID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("transactions.transactionid")) {
                strRetVal = "RecHubData.factTransactionSummary.TransactionID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("checks.transactionid")) {
                strRetVal = "RecHubData.factChecks.TransactionID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("checks.batchsequence")) {
                strRetVal = "RecHubData.factChecks.BatchSequence";
            } else if(fieldName.TrimStart().ToLower().StartsWith("checks.rt")) {
                strRetVal = "RecHubData.factChecks.RoutingNumber";
            } else if(fieldName.TrimStart().ToLower().StartsWith("checks.account")) {
                strRetVal = "RecHubData.factChecks.Account";
            } else if(fieldName.TrimStart().ToLower().StartsWith("stubs.transactionid")) {
                strRetVal = "RecHubData.factStubs.TransactionID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("stubs.batchsequence")) {
                strRetVal = "RecHubData.factStubs.BatchSequence";
            } else if(fieldName.TrimStart().ToLower().StartsWith("documents.transactionid")) {
                strRetVal = "RecHubData.factDocuments.TransactionID";
            } else if(fieldName.TrimStart().ToLower().StartsWith("documents.batchsequence")) {
                strRetVal = "RecHubData.factDocuments.BatchSequence";
            } else if(fieldName.ToLower() == "bankid") {
                strRetVal = "SiteBankID";
            } else if(fieldName.ToLower() == "lockboxid") {
                strRetVal = "SiteClientAccountID";
            } else if(fieldName.ToLower() == "depositdate") {
                strRetVal = "DepositDateKey";
            } else if(fieldName.ToLower() == "static") {
                strRetVal = fieldName;
            } else {
                strRetVal = fieldName;
            }

            return (strRetVal);
        }
    }

    public class cOrderByColumn : IOrderByColumn {
    
        private string _ColumnName = string.Empty;
        private string _FullName = string.Empty;
        private OrderByDirEnum _OrderByDir = OrderByDirEnum.Ascending;
    
        public cOrderByColumn(string vColumnName, string vColumnFullName) {
            _ColumnName = vColumnName;
            _FullName = vColumnFullName;
        }
    
        public string ColumnName { get { return _ColumnName; } }
        public string FullName { get { return _FullName; } }
        public string DisplayName { get; set; }

        public OrderByDirEnum OrderByDir {
            get {
                return(_OrderByDir);
            }
            set {
                _OrderByDir = value;
            }
        }
        
        public string OrderByDirDesc {
            get {
                if(OrderByDir == OrderByDirEnum.Descending) {
                    return("Descending");
                } else {
                    return("Ascending");
                }
            }
        }

        public override string ToString() {
            return this.ColumnName;
        }
    }
}
