using System;
using System.Collections.Generic;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 129562 BLR 02/13/2014
*   -Added constants for the column names for the current data model.   
* WI 129719 DLD 02/24/2014
*   -Moved cImageType into its own class   
*   -Created common method for retrieving OLFService client   
* WI 134896 BLR 04/14/2014
*   -Added constant for Alert Event Types. 
* WI 115856 BLR 06/19/2014
*   -Added some constants for the dimDataEntryColumns table. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public delegate void OutputStatusMsgEventHandler(string msg);
    public delegate void ImageStatusInitializedEventHandler(int maxItems);
    public delegate void ImageStatusChangedEventHandler(int currentItem);
    public delegate void PromptForManualInputEventHandler(ref Dictionary<string, string> manualInputValues);

    internal class ColumnDefNotAvailable : Exception {

        private string _TableName;
        private string _ColumnName;

        public ColumnDefNotAvailable(string tableName, string columnName) {
            _TableName = tableName;
            _ColumnName = columnName;
        }

        public ColumnDefNotAvailable(IColumn column) {
            _TableName = column.TableName;
            _ColumnName = column.ColumnName;
        }

        public override string Message {
            get {
                if(_ColumnName == null || _ColumnName.Length == 0) {
                    return("Could not locate column definition.");
                } else if(_TableName == null || _TableName.Length == 0) {
                    return("Could not locate column definition for [" + _ColumnName + "].");
                } else {
                    return("Could not locate column definition for [" + _TableName + "." + _ColumnName + "].");
                }
            }
        }
    }

    public static class Support {
        
        // WI 115856 : These map to the dimDataEntryColumns.TableName field.
        public const string DATA_ENTRY_TABLE_CHECKS = "ChecksDataEntry";
        public const string DATA_ENTRY_TABLE_STUBS = "StubsDataEntry";

        // WI 134896 : Added constant for EventLog type.
        //             This is referenced in the RecHubAlert.Events table.
        public const string ALERT_EXTRACT_COMPLETE = "EXTRACTCOMPLETE";
        public const string ALERT_EXTRACT_FAILURE = "PROCESSINGEXCEPTION";

        // WI 129562 : Added constants for the column names for the current data model.
        public const string PAYMENT_COLUMN_STRING_STARTER = "rechubdata.factchecks";
        public const string STUB_COLUMN_STRING_STARTER = "rechubdata.factstubs";
        public const string DOCUMENT_COLUMN_STRING_STARTER = "rechubdata.factdocuments";

        public const string LBL_BANKS = "Banks";
        public const string LBL_CLIENT_ACCOUNTS = "Workgroups";
        public const string LBL_BATCHES = "Batches";
        public const string LBL_TRANSACTIONS = "Transactions";
        public const string LBL_PAYMENTS = "Payments";
        public const string LBL_PAYMENTS_DATA_ENTRY = "Payments Data Entry";
        public const string LBL_STUBS = "Stubs";
        public const string LBL_STUBS_DATA_ENTRY = "Stubs Data Entry";
        public const string LBL_DOCUMENTS = "Documents";

        public const string LBL_BANKS_LOWER = "banks";
        public const string LBL_CLIENT_ACCOUNTS_LOWER = "workgroups";
        public const string LBL_BATCHES_LOWER = "batches";
        public const string LBL_TRANSACTIONS_LOWER = "transactions";
        public const string LBL_PAYMENTS_LOWER = "payments";
        public const string LBL_PAYMENTS_DATA_ENTRY_LOWER = "payments data entry";
        public const string LBL_STUBS_LOWER = "stubs";
        public const string LBL_STUBS_DATA_ENTRY_LOWER = "stubs data entry";
        public const string LBL_DOCUMENTS_LOWER = "documents";

        public const string LBL_FILE = "File";
        public const string LBL_BANK = "Bank";
        public const string LBL_CLIENT_ACCOUNT = "Workgroup";
        public const string LBL_BATCH = "Batch";
        public const string LBL_TRANSACTION = "Transaction";
        public const string LBL_PAYMENT = "Payment";
        public const string LBL_PAYMENT_DATA_ENTRY = "Payment Data Entry";
        public const string LBL_STUB = "Stub";
        public const string LBL_STUB_DATA_ENTRY = "Stub Data Entry";
        public const string LBL_DOCUMENT = "Document";
        public const string LBL_JOINT_DETAIL = "Joint Detail";
        public const string LBL_BATCH_TYPE = "Batch Type";
        public const string LBL_PROCESSING_DATE = "Processing Date";
        public const string LBL_DOCUMENT_TYPE = "Document Type";
        public const string LBL_FILE_COUNTER = "File Counter";

        public const string LBL_BANK_LOWER = "bank";
        public const string LBL_CLIENT_ACCOUNT_LOWER = "workgroup";
        public const string LBL_BATCH_LOWER = "batch";
        public const string LBL_TRANSACTION_LOWER = "transaction";
        public const string LBL_PAYMENT_LOWER = "payment";
        public const string LBL_PAYMENT_DATA_ENTRY_LOWER = "payment data entry";
        public const string LBL_STUB_LOWER = "stub";
        public const string LBL_STUB_DATA_ENTRY_LOWER = "stub data entry";
        public const string LBL_DOCUMENT_LOWER = "document";

        public const string LBL_FILE_LAYOUTS = "File Layouts";
        public const string LBL_BANK_LAYOUTS = "Bank Layouts";
        public const string LBL_CLIENT_ACCOUNT_LAYOUTS = "Workgroup Layouts";
        public const string LBL_BATCH_LAYOUTS = "Batch Layouts";
        public const string LBL_TXN_LAYOUTS = "Transaction Layouts";
        public const string LBL_PAYMENT_LAYOUTS = "Payment Layouts";
        public const string LBL_STUB_LAYOUTS = "Stub Layouts";
        public const string LBL_DOCUMENT_LAYOUTS = "Document Layouts";
        public const string LBL_JOINT_DETAIL_LAYOUTS = "Joint Detail Layouts";
    }
}
