

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 115249 BLR 02/26/2014
*   -Added function to validate limit items.  
* 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    //public enum LogicalOperatorEnum {

    //    Equals,               // =
    //    NotEqualTo,           // <>
    //    GreaterThan,          // >
    //    GreaterThanOrEqualTo, // >=
    //    LessThan,             // <
    //    LessThanOrEqualTo,    // <=
    //    Between,              // between
    //    In,                   // in
    //    IsNull,               // is Null
    //    IsNotNull,            // is not Null
    //    Like                  // Like
    //}

    //public enum CompoundOperatorEnum {

    //    None,
    //    And,
    //    AndNot,
    //    Or,
    //    OrNot
    //}

    public class cLimitItem : ILimitItem {

        private string _FieldName;
        private LogicalOperatorEnum _LogicalOperator;
        private string _Value;
        private int _LeftParenCount;
        private int _RightParenCount;
        private CompoundOperatorEnum _CompoundOperator;

        public string Schema { get; set; }
        public string Table { get; set; }
        public string DisplayName { get; set; }
        public string PaymentSource { get; set; }
        public int DataType { get; set; }

        public cLimitItem() {

            InitializeObject();
        }

        public cLimitItem(
            string fieldName,
            LogicalOperatorEnum logicalOperator,
            string value,
            int leftParenCount,
            int rightParenCount,
            CompoundOperatorEnum compoundOperator) {

            _FieldName = fieldName;
            _LogicalOperator = logicalOperator;
            _Value = value;
            _LeftParenCount = leftParenCount;
            _RightParenCount = rightParenCount;
            _CompoundOperator = compoundOperator;
        }
        
        private void InitializeObject() {
            
            _FieldName = string.Empty;
            DisplayName = string.Empty;
            PaymentSource = string.Empty;
            DataType = 0;
            _LogicalOperator = LogicalOperatorEnum.Equals;
            _Value = string.Empty;
            _LeftParenCount = 0;
            _RightParenCount = 0;
            _CompoundOperator = CompoundOperatorEnum.None;
        }

        // WI 115249 : Added function to validate limit items.
        public bool IsValidLimit(cExtractDef def, out string msg)
        {
            var isvalid = false;
            msg = string.Empty;

            // Check to see if the limit field actually exists in the layout.
            foreach (var layout in def.AllLayouts)
                foreach(var field in layout.LayoutFields)
                    if (field.FieldName.ToLower() == string.Format("{0}.{1}.{2}", Schema, Table, FieldName).ToLower())
                        isvalid = true;
            if (!isvalid)
                msg = string.Format("Error while applying limit: Field {0} does not exist in the definition.", this.FieldName);

            return isvalid;
        }
        
        public string FieldName {
            get {
                return(_FieldName);
            }
            set {
                _FieldName = value;
            }
        }

        public LogicalOperatorEnum LogicalOperator {
            get {
                return(_LogicalOperator);
            }
            set {
                _LogicalOperator = value;
            }
        }

        public string LogicalOperatorDesc {
            get {
                return (LimitItemLib.FormatLogicalOperator(_LogicalOperator));
            }
        }

        public string Value {
            get {
                return(_Value);
            }
            set {
                _Value = value;
            }
        }

        public int LeftParenCount {
            get {
                return(_LeftParenCount);
            }
            set {
                _LeftParenCount = value;
            }
        }

        public int RightParenCount {
            get {
                return(_RightParenCount);
            }
            set {
                _RightParenCount = value;
            }
        }

        public CompoundOperatorEnum CompoundOperator {
            get {
                return(_CompoundOperator);
            }
            set {
                _CompoundOperator = value;
            }
        }

        public string CompoundOperatorDesc {
            get {
                return (LimitItemLib.FormatCompoundOperator(_CompoundOperator));
            }
        }
    }
}
