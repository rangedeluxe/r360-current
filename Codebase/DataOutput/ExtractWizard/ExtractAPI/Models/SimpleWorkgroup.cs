﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI.Models
{
    public class SimpleWorkgroup
    {
        public int Bank { get; set; }
        public int Workgroup { get; set; }
        public string WorkgroupName { get; set; }
    }
}
