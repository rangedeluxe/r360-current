﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 05/27/2014
*
* Purpose: To contain small functionality to alter tables after they have come
*          back from the stored procedures detailing their definitions. 
*
* Modification History
* WI 143034 BLR 05/27/2014
*   -Initial Version
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    /// <summary>
    /// Static utility class to provide some assistance with altering the tables
    /// after coming back from the database.
    /// </summary>
    public static class cColumnReplacementLib
    {
        /// <summary>
        /// Main function to simply loop through the columns and make any changes
        /// we need.
        /// </summary>
        /// <param name="columns"></param>
        public static void AlterColumns(IEnumerable<IColumn> columns)
        {
            foreach (var col in columns)
            {
                if (col.ColumnName.ToLower() == "batchid")
                {
                    // BatchID needs the label of BatchID, but the value of the SourceBatchID Column.
                    col.ColumnName = "SourceBatchID";
                }
            }
        }
    }
}
