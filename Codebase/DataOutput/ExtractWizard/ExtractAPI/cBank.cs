using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cBank : ObjectBase, IComparable, ISummaryFields {

        private int _BankID;
        private Dictionary<string, object> _BankFields;
        private OrderedDictionary _Lockboxes;

        private IOrderByColumn[] _OrderByColumns = {};

        public cBank(
            int bankID,
            IOrderByColumn[] orderByColumns) {


            _BankID = bankID;

            _OrderByColumns = orderByColumns;

            _BankFields = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            _Lockboxes = new OrderedDictionary();
        }

        public bool AnyWorkgroupElementWasExtracted {
            get {
                return (this.Lockboxes.Values.Cast<cClientAccount>().Any(x => x.AnyWorkgroupElementWasExtracted));
            }
        }

        public int BankID {
            get {
                return(_BankID);
            }
        }

        public Dictionary<string, object> BankFields{
            get {
                return(_BankFields);
            }
        }

        public OrderedDictionary Lockboxes
        {
            get
            {
                return (_Lockboxes);
            }
            set
            {
                _Lockboxes = value;
            }
        }

        public int BatchCount { 
            get {

                int intBatchCount = 0;

                foreach (cClientAccount lockbox in this.Lockboxes.Values)
                {
                    intBatchCount += lockbox.BatchCount;
                }

                return (intBatchCount);
            } 
        }

        public int TransactionCount { 
            get {

                int intTxnCount = 0;

                foreach (cClientAccount lockbox in this.Lockboxes.Values)
                {
                    intTxnCount += lockbox.TransactionCount;
                }

                return (intTxnCount);
            } 
        }

        public int CheckCount { 
            get {

                int intCheckCount = 0;

                foreach (cClientAccount lockbox in this.Lockboxes.Values)
                {
                    intCheckCount += lockbox.CheckCount;
                }

                return (intCheckCount);
            } 
        }

        public int StubCount { 
            get {
                int intStubCount = 0;

                foreach (cClientAccount lockbox in this.Lockboxes.Values)
                {
                    intStubCount += lockbox.StubCount;
                }

                return (intStubCount);
            } 
        }

        public int DocumentCount { 
            get {

                int intDocCount = 0;

                foreach (cClientAccount lockbox in this.Lockboxes.Values)
                {
                    intDocCount += lockbox.DocumentCount;
                }

                return (intDocCount);
            } 
        }



        public Decimal SumPaymentsAmount {
            get {
                Decimal decRetVal = 0;

                foreach (cClientAccount lockbox in this.Lockboxes.Values)
                {
                    decRetVal += lockbox.SumPaymentsAmount;
                }

                return (decRetVal);
            }
        }
        
        public Decimal SumStubsAmount {
            get {
                Decimal decRetVal = 0;

                foreach (cClientAccount lockbox in this.Lockboxes.Values)
                {
                    decRetVal += lockbox.SumStubsAmount;
                }

                return(decRetVal);
            }
        }

        public static Hashtable DefaultFields() {

            Hashtable htFieldNames = new Hashtable();

            htFieldNames.Add("BankID", string.Empty);

            return(htFieldNames);
        }

        public int CompareTo(object obj) {

            int intRetVal = 0;

            if(obj is cBank) {

                cBank bank = (cBank) obj;
                
                foreach(IOrderByColumn orderbycolumn in _OrderByColumns) {
                    
                    switch(orderbycolumn.ColumnName.ToLower()) {
                        case "bankid":
                            if(this.BankID != bank.BankID) {
                                intRetVal = this.BankID.CompareTo(bank.BankID);
                            }
                            break;
                        default:
                            foreach(string fieldname in this.BankFields.Keys) {
                                if(orderbycolumn.ColumnName.ToLower() == fieldname.ToLower()) {
                                    intRetVal = Compare(this.BankFields, bank.BankFields, fieldname);

                                    if(intRetVal != 0) {
                                        break;
                                    }
                                }
                            }
                            break;
                    }

                    if(intRetVal != 0) {
                        if(orderbycolumn.OrderByDir == OrderByDirEnum.Descending) {
                            intRetVal*=-1;
                        }
                        break;
                    }
                }

            } else {
                throw new ArgumentException("object is not a Bank");    
            }

            return(intRetVal);
        }
    }

    internal class cBankSummary : ISummaryFields {

        private cBank[] _Banks;
        private int _BatchCount = -1;
        private int _TxnCount = -1;
        private int _CheckCount = -1;
        private int _StubCount = -1;
        private int _DocumentCount = -1;
        private Decimal _SumPaymentsAmount = -1;
        private Decimal _SumStubsAmount = -1;

        public cBankSummary(cBank[] banks) {
            _Banks = banks;
        }

        public int BatchCount { 
            get { 
                if(_BatchCount == -1) {
                    _BatchCount = 0;
                    foreach(cBank bank in _Banks) {
                        _BatchCount += bank.BatchCount;
                    }
                }

                return(_BatchCount); 
            } 
        }

        public int TransactionCount { 
            get { 
                if(_TxnCount == -1) {
                    _TxnCount = 0;
                    foreach(cBank bank in _Banks) {
                        _TxnCount += bank.TransactionCount;
                    }
                }

                return(_TxnCount); 
            } 
        }

        public int CheckCount { 
            get { 
                if(_CheckCount == -1) {
                    _CheckCount = 0;
                    foreach(cBank bank in _Banks) {
                        _CheckCount += bank.CheckCount;
                    }
                }

                return(_CheckCount); 
            } 
        }

        public int StubCount { 
            get { 
                if(_StubCount == -1) {
                    _StubCount = 0;
                    foreach(cBank bank in _Banks) {
                        _StubCount += bank.StubCount;
                    }
                }

                return(_StubCount); 
            } 
        }

        public int DocumentCount { 
            get { 
                if(_DocumentCount == -1) {
                    _DocumentCount = 0;
                    foreach(cBank bank in _Banks) {
                        _DocumentCount += bank.DocumentCount;
                    }
                }

                return(_DocumentCount); 
            } 
        }

        public Decimal SumPaymentsAmount {
            get { 
                if(_SumPaymentsAmount == -1) {
                    _SumPaymentsAmount = 0;
                    foreach(cBank bank in _Banks) {
                        _SumPaymentsAmount += bank.SumPaymentsAmount;
                    }
                }

                return(_SumPaymentsAmount); 
            } 
        }
        
        public Decimal SumStubsAmount {
            get { 
                if(_SumStubsAmount == -1) {
                    _SumStubsAmount = 0;
                    foreach(cBank bank in _Banks) {
                        _SumStubsAmount += bank.SumStubsAmount;
                    }
                }

                return(_SumStubsAmount); 
            } 
        }
    }
}
