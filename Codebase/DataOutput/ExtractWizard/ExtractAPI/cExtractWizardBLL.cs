using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;

using WFS.RecHub.DAL.ExtractEngine;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.Common;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/21/2013
*   -Initial Version 
* WI 115356 Derek 02/12/2014
*   -Moved connection string into app.config. 
* WI 130364 DLD 02/20/2014
*   -Verify database connectivity providing 'friendly' messages to user upon failure.
* WI 130807 DLD 02/28/2014
*   -Encrypt/Decrypt UserID in database connection string 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 132009 BLR 03/06/2014
*   - Adding in some more columns, which come from other tables.
* WI 132130 DLD 03/07/2014
*   - Removed WorkstationID setting 
* WI 143034 BLR 05/22/2014
*   - Added reference to the cColumnReplacementLib. 
* WI 115856 BLR 06/24/2014
*   - Added GetAllDataEntryColumns function for Data Entry functionality. 
* WI 152321 BLR 08/15/2014
*   -Moved Configuration over to ExtractConfigurationSettings. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cExtractWizardBLL {

        private ExtractConfigurationSettings _Settings = null;
        private cExtractEngineDAL _ExtractEngineDAL = null;

        public event outputMessageEventHandler OutputMessage;
        public event outputErrorEventHandler OutputError;

        public cExtractWizardBLL(ExtractConfigurationSettings settings) {
            _Settings = settings;
        }

        public cExtractEngineDAL ExtractEngineDAL
        {
            get
            {
                if (_ExtractEngineDAL == null)
                {
                    // DLD - WI 130807
                    var connString = CommonLib.DecryptUserIdInConnectionString(_Settings.ConnectionString);
                    // WI 115356 - Derek - Moved connection string into app.config. 
                    _ExtractEngineDAL = new cExtractEngineDAL(Constants.INI_SECTION_EXTRACTWIZARD, connString);
                    // DLD - WI 130364 - Verify database connectivity providing 'friendly' messages to user upon failure.
                    if (_ExtractEngineDAL != null)
                    {
                        _ExtractEngineDAL.OutputError += OnOutputError;
                        _ExtractEngineDAL.OutputMessage += OnOutputMessage;
                        var connected = _ExtractEngineDAL.TestConnection();
                        if (!connected)
                        {
                            OnOutputMessage(ExtractLib.Constants.ERROR_MSG_NO_DATABASE_CONNECTION, this.ToString(), MessageType.Error, MessageImportance.Essential);
                            _ExtractEngineDAL = null;
                        }
                    }
                }
                return (_ExtractEngineDAL);
            }
        }
        
        //TODO: REMOVE THIS!!!!
        // wat.
        private void OnOutputError(System.Exception e) {
            if(OutputError != null) {
                OutputError(e);
            }
        }

        private void OnOutputError(
            object sender, 
            System.Exception e) {
            
            if(OutputError != null) {
                OutputError(e);
            }
        }

        private void OnOutputMessage(
            //object sender,
            string msg, 
            string src, 
            MessageType messageType, 
            MessageImportance messageImportance) {

            if(OutputMessage != null) {
                OutputMessage(msg, src, messageType, messageImportance);
            }
        }

        public bool CreateBatchTraceColumn(string newColumnName) {

            //return(Database.executeNonQuery(SQLExtractDescriptor.SQL_CreateBatchTraceDateColumn(NewColumnName)) >= 0);
            return (ExtractEngineDAL.CreateExtractTraceColumn(newColumnName));
        }
 
        /// <summary>
        /// Gets all data entry columns for the tablename.
        /// </summary>
        /// <param name="tablename">
        /// </param>
        /// <returns></returns>
        public List<cColumn> GetAllDataEntryColumns(DataEntryType type)
        {
            var output = new List<cColumn>();

            var detype = type == DataEntryType.PaymentsDataEntry
                ? "ChecksDataEntry"
                : "StubsDataEntry";

            DataTable dt;
            if (ExtractEngineDAL.GetTableColumns(type, out dt))
                foreach( DataRow derow in dt.Rows )
                {
                    var col = new cColumn(derow);
                    col.Schema = "RecHubData";
                    col.TableName = detype;
                    col.IsDataEntry = true;
                    output.Add(col);
                }
            output = output.OrderBy(x => x.DisplayName)
                .ThenBy(x => x.ColumnName)
                .ToList();

            return output;
        }

        public bool GetTableColumns(
            string tableName, 
            out List<cColumn> columns) {

            List<cColumn> objColumns;
            DataTable dt;

            if(ExtractEngineDAL.GetTableDef(tableName, out dt)) {

                objColumns = new List<cColumn>();

                foreach(DataRow dr in dt.Rows) {
                    objColumns.Add(new cColumn(dr));
                }
                dt.Dispose();

                // WI 132009 : Adding in some more columns, which come from other tables.
                objColumns.AddRange(cJoinedTablesLib.GetJoinedColumns(tableName));

                // WI 143034 : Change the IColumn fieldname from 'BatchID' to 'SourceBatchID'.
                cColumnReplacementLib.AlterColumns(objColumns);

                columns = objColumns;
                return(true);

            } else {
                columns = null;
                return(false);
            }
        }

        public bool GetTableColumns(
            DataEntryType dataEntryType, 
            out List<cColumn> columns) {

            List<cColumn> objColumns;
            DataTable dt;
            HashSet<string> hsColumnNames;

            if(ExtractEngineDAL.GetTableColumns(dataEntryType, out dt)) {

                hsColumnNames = new HashSet<string>();

                objColumns = new List<cColumn>();

                foreach(DataRow dr in dt.Rows) {
                    if(!hsColumnNames.Contains(dr["ColumnName"].ToString())) {
                        objColumns.Add(new cColumn(dr));
                        hsColumnNames.Add(dr["ColumnName"].ToString());
                    }
                }
                dt.Dispose();

                columns = objColumns;
                return (true);
            } else {
                columns = null;
                return (false);
            }

        }


                 
        public bool WriteAuditFileData(
            string eventType,
            cExtractDef dxtractDef, 
            int userID,
            string description) {

            StringBuilder sbDescription = new StringBuilder();
            bool bolRetVal;

            try {
                // WI 132130 DLD 03/06/2014 - Removed WorkstationID setting. Hard coded to "-1".
                bolRetVal = ExtractEngineDAL.WriteAuditFileData(eventType, -1, userID, description);
                return (bolRetVal);

            } catch(Exception e) {
                // WI 131640 : Additional exception logging.
                cCommonLogLib.LogErrorLTA(e, LTA.Common.LTAMessageImportance.Essential);
                OnOutputError(this, e);
                return(false);
            }
        }

        public string GetSystemSetupSetting(
            string section, 
            string key) {

                return (ExtractEngineDAL.GetSystemSetupSetting(section, key));
        }

        public bool GetExtractDescriptors(out List<cExtractDescriptor> extractDescriptors) {
            return (ExtractEngineDAL.GetExtractDescriptors(out extractDescriptors));
        }

    }
}