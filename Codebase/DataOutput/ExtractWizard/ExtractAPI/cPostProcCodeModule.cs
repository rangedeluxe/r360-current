﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cPostProcCodeModule {

        private string _SrcCode = string.Empty;
        private string _HashCode = string.Empty;

        public cPostProcCodeModule(
            string srcCode, 
            string hashCode) {

            _SrcCode = srcCode;
            _HashCode = hashCode;
        }

        public cPostProcCodeModule() {

            StringBuilder sbSrcCode = new StringBuilder();
            const string WHITESPACE = @"" + "\r\n";

            sbSrcCode.Append(@"using System;" + "\r\n");
            sbSrcCode.Append(@"using System.Collections;" + "\r\n");
            sbSrcCode.Append(@"using System.Collections.Generic;" + "\r\n");
            sbSrcCode.Append(WHITESPACE);
            sbSrcCode.Append(@"/// <summary>" + "\r\n");
            sbSrcCode.Append(@"/// This module exposes the extract file data prior to being written to the" + "\r\n");
            sbSrcCode.Append(@"/// extract file.  Each member of the input array represents a line of text that will" + "\r\n");
            sbSrcCode.Append(@"/// normally be written to the extract file.  The output of the ProcessExtract" + "\r\n");
	    // CR 49779 MEH 02-03-2012 To avoid an error after adding a post processing code module, you need to save your extract twice
	    // there was a space at the end of this comment that would cause the HashCode to be calculated differently
	    // after the first time the extract script is saved because the space would be removed
            sbSrcCode.Append(@"/// function will be the actual text that will be written to the extract file.  The" + "\r\n");
            sbSrcCode.Append(@"/// data can be modified, appended to, or removed as required to produce the desired results." + "\r\n");
            sbSrcCode.Append(@"/// The namespace, class name, and function definition can not be modified" + "\r\n");
            sbSrcCode.Append(@"/// or the code module will not be executed by the extract engine and an error will occur." + "\r\n");
            sbSrcCode.Append(@"/// </summary>" + "\r\n");
            sbSrcCode.Append(@"namespace WFS.RecHub.DataOutputToolkit.Extract.CodeModule {" + "\r\n");
            sbSrcCode.Append(WHITESPACE);
            sbSrcCode.Append(@"    public class PostProcessing {" + "\r\n");
            sbSrcCode.Append(WHITESPACE);
            sbSrcCode.Append(@"        public object ProcessExtract(List<string> FileRows) {" + "\r\n");
            sbSrcCode.Append(WHITESPACE);
            sbSrcCode.Append(@"            List<string> arRetVal = new List<string>();" + "\r\n");
            sbSrcCode.Append(WHITESPACE);
            sbSrcCode.Append(@"            // Loop through each row of text of the extract file and modify" + "\r\n");
            sbSrcCode.Append(@"            // as necessary" + "\r\n");
            sbSrcCode.Append(@"            foreach(string rowText in FileRows) {" + "\r\n");
            sbSrcCode.Append(@"                arRetVal.Add(rowText);" + "\r\n");
            sbSrcCode.Append(@"            }" + "\r\n");
            sbSrcCode.Append(WHITESPACE);
            sbSrcCode.Append(@"            return(arRetVal);" + "\r\n");
            sbSrcCode.Append(@"        }" + "\r\n");
            sbSrcCode.Append(@"    }" + "\r\n");
            sbSrcCode.Append(@"}" + "\r\n");
            
            _SrcCode = sbSrcCode.ToString();
            _HashCode = HashGenerator.SHA512(_SrcCode);
        }
        
        public string SrcCode {
            get {
                return(_SrcCode);
            }
            set {
                _SrcCode = value;
            }
        }
        
        public string HashCode {
            get {
                return(_HashCode);
            }
            set {
                _HashCode = value;
            }
        }
    }
}
