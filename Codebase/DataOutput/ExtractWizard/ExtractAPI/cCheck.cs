using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Text;
using WFS.RecHub.Common;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   -Initial Version 
* WI 129559 DLD 02/18/2014
*   -PICS.getImagePath sometimes fails and returns the base file 'path' as 
*   the new image file 'name'.  
* WI 129719 DLD 02/24/2014
*   -Converting image repository from ipoPics.dll to OLFServices 
* WI 152321 BLR 08/15/2014
*   -Moved Configuration over to ExtractConfigurationSettings. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cPayment : cExtractImageBase, IComparable {

        private Decimal _Amount;
        private string _Account;
        private string _RoutingNumber;
        private long _NumericRoutingNumber;
        private Dictionary<string, object> _CheckFields;
        
        public cPayment(
            ExtractConfigurationSettings settings,
            cTransaction transaction, 
            int transactionID, 
            int batchSequence, 
            Decimal amount, 
            string account,
            string routingNumber,
            long numericRoutingNumber,
            IOrderByColumn[] orderByColumns) {

            Settings = settings;

            Transaction = transaction;
            TransactionID = transactionID;
            BatchSequence = batchSequence;
            _Amount = amount;
            _Account = account;
            _RoutingNumber = routingNumber;
            _NumericRoutingNumber = numericRoutingNumber;
            OrderByColumns = orderByColumns;

            _CheckFields = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
        }

        public override string FileDescriptor { get { return ("C"); } }

        public Dictionary<string, object> CheckFields {
            get {
                return(_CheckFields);
            }
        }

        public Decimal Amount {
            get {
                return(_Amount);
            }
        }
        public string Account {
            get {
                return (_Account);
            }
        }

        public string RoutingNumber {
            get {
                return (_RoutingNumber);
            }
        }

        public long NumericRoutingNumber {
            get {
                return (_NumericRoutingNumber);
            }
        }

        public static Hashtable DefaultFields() {

            Hashtable htFieldNames = new Hashtable();

            htFieldNames.Add("BankID", string.Empty);
            htFieldNames.Add("CustomerID", string.Empty);
            htFieldNames.Add("LockboxID", string.Empty);
            htFieldNames.Add("TransactionID", string.Empty);
            htFieldNames.Add("BatchSequence", string.Empty);
            htFieldNames.Add("Amount", string.Empty);

            return(htFieldNames);
        }

        public override Decimal SumPaymentsAmount {
            get { return(this.Amount); }
        }

        public int CompareTo(object obj) {

            int intRetVal = 0;

            if(obj is cPayment) {

                cPayment check = (cPayment) obj;

                foreach(IOrderByColumn orderbycolumn in OrderByColumns) {
                    
                    switch(orderbycolumn.ColumnName.ToLower()) {
                        case "transactionid":
                            if(this.TransactionID != check.TransactionID) {
                                intRetVal = this.TransactionID.CompareTo(check.TransactionID);
                            }
                            break;
                        case "batchsequence":
                            if(this.BatchSequence != check.BatchSequence) {
                                intRetVal = this.BatchSequence.CompareTo(check.BatchSequence);
                            }
                            break;
                        case "amount":
                            if(this.Amount != check.Amount) {
                                intRetVal = this.Amount.CompareTo(check.Amount);
                            }
                            break;
                        default:
                            foreach(string fieldname in this.CheckFields.Keys) {
                                if(orderbycolumn.ColumnName.ToLower() == fieldname.ToLower()) {
                                    intRetVal = Compare(this.CheckFields, check.CheckFields, fieldname);

                                    if(intRetVal != 0) {
                                        break;
                                    }
                                }
                            }
                            break;
                    }

                    if(intRetVal != 0) {
                        if(orderbycolumn.OrderByDir == OrderByDirEnum.Descending) {
                            intRetVal*=-1;
                        }
                        break;
                    }
                }

            } else {
                throw new ArgumentException("object is not a Check");    
            }

            return(intRetVal);
        }
    }
}
