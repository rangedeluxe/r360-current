using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    public class cTransaction : ObjectBase, IComparable, ISummaryFields {

        private bool _ExtractTransactionChecks = false;
        private bool _ExtractTransactionDocuments = false;
        private cBatch _Batch;
        ////private int _GlobalBatchID;
        private int _TransactionID;
        private Dictionary<string, object> _TransactionFields;
        private OrderedDictionary _Payments;
        private OrderedDictionary _Stubs;
        private OrderedDictionary _Documents;
        private Hashtable _TransactionImageFiles = null;
        private string _FormattedImageName = string.Empty;

        private IOrderByColumn[] _OrderByColumns = { };
        
        public cTransaction(
            cBatch batch, 
            ////int vGlobalBatchID, 
            int transactionID,
            IOrderByColumn[] orderByColumns) {

            _Batch = batch;

            ////_GlobalBatchID = vGlobalBatchID;
            _TransactionID = transactionID;

            _OrderByColumns = orderByColumns;

            _TransactionFields = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            _Payments = new OrderedDictionary();
            _Stubs = new OrderedDictionary();
            _Documents = new OrderedDictionary();
        }

        public bool AnyWorkgroupElementWasExtracted {
            get {
                return (
                    this.WasExtracted ||
                    (this.Payments.Values.Cast<cPayment>().Any(x => x.WasExtracted)) ||
                    (this.Stubs.Values.Cast<cStub>().Any(x => x.WasExtracted)) ||
                    (this.Documents.Values.Cast<cDocument>().Any(x => x.WasExtracted)));
            }
        }

        public bool ExtractTransactionChecks {
            get {
                return(_ExtractTransactionChecks);
            }
            set {
                _ExtractTransactionChecks = value;
            }
        }

        public bool ExtractTransactionDocuments {
            get {
                return(_ExtractTransactionDocuments);
            }
            set {
                _ExtractTransactionDocuments = value;
            }
        }

        public cBatch Batch {
            get {
                return(_Batch);
            }
        }

        public int TransactionID {
            get {
                return(_TransactionID);
            }
        }
    
        public Dictionary<string, object> TransactionFields {
            get {
                return(_TransactionFields);
            }
        }
        
        public OrderedDictionary Payments {
            get {
                return (_Payments);
            }
            set {
                _Payments = value;
            }
        }

        public OrderedDictionary Stubs {
            get {
                return(_Stubs);
            }
            set {
                _Stubs = value;
            }
        }

        public OrderedDictionary Documents {
            get {
                return(_Documents);
            }
            set {
                _Documents = value;
            }
        }

        public int BatchCount { 
            get {
                return(0);
            } 
        }

        public int TransactionCount { 
            get {
                return(1);
            } 
        }

        public int CheckCount { 
            get {
                return (this.Payments.Count);
            } 
        }

        public int StubCount { 
            get {
                return(this.Stubs.Count);
            } 
        }

        public int DocumentCount { 
            get {
                return(this.Documents.Count);
            } 
        }

        public Decimal SumPaymentsAmount {
            get {
                Decimal decRetVal = 0;

                foreach(cPayment check in this.Payments.Values) {
                    decRetVal+=check.Amount;
                }

                return(decRetVal);
            }
        }

        ////public int SumPaymentsDEBillingKeys {
        ////    get {
        ////        int intRetVal = 0;
                
        ////        foreach(cPayment check in this.Checks.Values) {
        ////            intRetVal+=check.DEBillingKeys;
        ////        }

        ////        return(intRetVal);
        ////    }
        ////}

        ////public int SumPaymentsDEDataKeys {
        ////    get {
        ////        int intRetVal = 0;
                
        ////        foreach(cPayment check in this.Checks.Values) {
        ////            intRetVal+=check.DEDataKeys;
        ////        }

        ////        return(intRetVal);
        ////    }
        ////}
        
        public Decimal SumStubsAmount {
            get {
                Decimal decRetVal = 0;
                
                foreach(cStub stub in this.Stubs.Values) {
                    decRetVal+=stub.Amount;
                }

                return(decRetVal);
            }
        }

        ////public int SumStubsDEBillingKeys {
        ////    get {
        ////        int intRetVal = 0;
                
        ////        foreach(cStub stub in this.Stubs.Values) {
        ////            intRetVal+=stub.DEBillingKeys;
        ////        }

        ////        return(intRetVal);
        ////    }
        ////}

        ////public int SumStubsDEDataKeys {
        ////    get {
        ////        int intRetVal = 0;
                
        ////        foreach(cStub stub in this.Stubs.Values) {
        ////            intRetVal+=stub.DEDataKeys;
        ////        }

        ////        return(intRetVal);
        ////    }
        ////}

        public Hashtable TransactionImageFiles {
            get {
                if(_TransactionImageFiles == null) {
                    _TransactionImageFiles = new Hashtable();
                }
                return(_TransactionImageFiles);
            }
        }

        public string FormattedImageName {
            get {
                return(_FormattedImageName);
            }
            set {
                _FormattedImageName = value;
            }
        }

        public static Hashtable DefaultFields() {

            Hashtable htFieldNames = new Hashtable();

            htFieldNames.Add("SiteBankID", string.Empty);
            htFieldNames.Add("SiteClientAccountID", string.Empty);
            htFieldNames.Add("TransactionID", string.Empty);

            return(htFieldNames);
        }

        public int CompareTo(object obj) {

            int intRetVal = 0;

            if(obj is cTransaction) {

                cTransaction txn = (cTransaction) obj;

                foreach(IOrderByColumn orderbycolumn in _OrderByColumns) {
                    
                    switch(orderbycolumn.ColumnName.ToLower()) {
                        ////case "globalbatchid":
                        ////    if(this.GlobalBatchID != txn.GlobalBatchID) {
                        ////        intRetVal = this.GlobalBatchID.CompareTo(txn.GlobalBatchID);
                        ////    }
                        ////    break;
                        case "transactionid":
                            if(this.TransactionID != txn.TransactionID) {
                                intRetVal = this.TransactionID.CompareTo(txn.TransactionID);
                            }
                            break;
                        default:
                            foreach(string fieldname in this.TransactionFields.Keys) {
                                if(orderbycolumn.ColumnName.ToLower() == fieldname.ToLower()) {
                                    intRetVal = Compare(this.TransactionFields, txn.TransactionFields, fieldname);

                                    if(intRetVal != 0) {
                                        break;
                                    }
                                }
                            }
                            break;
                    }

                    if(intRetVal != 0) {
                        if(orderbycolumn.OrderByDir == OrderByDirEnum.Descending) {
                            intRetVal*=-1;
                        }
                        break;
                    }
                }

            } else {
                throw new ArgumentException("object is not a Transaction");    
            }

            return(intRetVal);
        }
    }
}
