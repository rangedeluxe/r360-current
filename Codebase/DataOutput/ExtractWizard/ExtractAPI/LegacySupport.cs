using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WFS.LTA.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public enum TFieldType {

          UNKNOWN        = 0,
          ZSTRING        = 1,               // Null terminated string
          DATE           = 2,               // Date     (32 bit)
          BLOB           = 3,               // Blob
          BOOL           = 4,               // Boolean  (16 bit)
          INT16          = 5,               // 16 bit signed number
          INT32          = 6,               // 32 bit signed number
          FLOAT          = 7,               // 64 bit floating point
          BCD            = 8,               // BCD
          BYTES          = 9,               // Fixed number of bytes
          TIME           = 10,              // Time        (32 bit)
          TIMESTAMP      = 11,              // Time-stamp  (64 bit)
          UINT16         = 12,              // Unsigned 16 bit integer
          UINT32         = 13,              // Unsigned 32 bit integer
          FLOATIEEE      = 14,              // 80-bit IEEE float
          VARBYTES       = 15,              // Length prefixed var bytes
          LOCKINFO       = 16,              // Look for LOCKINFO typedef
          CURSOR         = 17,              // For Oracle Cursor type
          INT64          = 18,              // 64 bit signed number
          UINT64         = 19,              // Unsigned 64 bit integer
          ADT            = 20,              // Abstract datatype (structure)
          ARRAY          = 21,              // Array field type
          REF            = 22,              // Reference to ADT
          TABLE          = 23,              // Nested table (reference)
          DATETIME       = 24,              // DateTime structure field
          FMTBCD         = 25,              // BCD Variant type: required by Midas, same as BCD for DBExpress}
          //MAXLOGFLDTYPES     = 26,              // Number of logical fieldtypes
          MONEY          = 21,              // Money
          MEMO           = 22,              // Text Memo
          BINARY         = 23,              // Binary data
          FMTMEMO        = 24,              // Formatted Text
          OLEOBJ         = 25,              // OLE object (Paradox)
          GRAPHIC        = 26,              // Graphics object
          DBSOLEOBJ      = 27,              // dBASE OLE object
          TYPEDBINARY    = 28,              // Typed Binary data
          ACCOLEOBJ      = 30,              // Access OLE object
          HMEMO          = 33,              // CLOB
          HBINARY        = 34,              // BLOB
          BFILE          = 36,              // BFILE
          PASSWORD       = 1,               // Password
          FIXED          = 31,              // CHAR type
          UNICODE        = 32,              // Unicode
          AUTOINC        = 29,
          ADTNestedTable = 35,             // ADT for nested table (has no name)
          ADTDATE        = 37              // DATE (OCIDate) with in an ADT
    }

    public static class LegacySupport {

        public static TFieldType GetTFieldType(SqlDbType dBType) {

            TFieldType enmRetVal;

            switch(dBType) {
                case SqlDbType.Bit:
                    enmRetVal = TFieldType.BOOL;
                    break;
                case SqlDbType.SmallInt:
                    enmRetVal = TFieldType.INT16;
                    break;
                case SqlDbType.Int:
                    enmRetVal = TFieldType.INT32;
                    break;
                case SqlDbType.Real:
                    enmRetVal = TFieldType.FLOAT;
                    break;
                case SqlDbType.Decimal:
                    enmRetVal = TFieldType.FLOAT;
                    break;
                case SqlDbType.Float:
                    enmRetVal = TFieldType.FLOAT;
                    break;
                case SqlDbType.Money:
                    enmRetVal = TFieldType.MONEY;
                    break;
                case SqlDbType.SmallDateTime:
                    enmRetVal = TFieldType.DATE;
                    break;
                case SqlDbType.Timestamp:
                    enmRetVal = TFieldType.DATETIME;
                    break;
                case SqlDbType.DateTime:
                    enmRetVal = TFieldType.DATETIME;
                    break;
                case SqlDbType.BigInt:
                    enmRetVal = TFieldType.INT64;
                    break;
                case SqlDbType.Char:
                    enmRetVal = TFieldType.FIXED;
                    break;
                case SqlDbType.NChar:
                    enmRetVal = TFieldType.FIXED;
                    break;
                case SqlDbType.NText:
                    enmRetVal = TFieldType.ZSTRING;
                    break;
                case SqlDbType.NVarChar:
                    enmRetVal = TFieldType.ZSTRING;
                    break;
                case SqlDbType.SmallMoney:
                    enmRetVal = TFieldType.MONEY;
                    break;
                case SqlDbType.TinyInt:
                    enmRetVal = TFieldType.BYTES;
                    break;
                case SqlDbType.Text:
                    enmRetVal = TFieldType.ZSTRING;
                    break;
                case SqlDbType.UniqueIdentifier:
                    enmRetVal = TFieldType.ZSTRING;
                    break;
                case SqlDbType.VarChar:
                    enmRetVal = TFieldType.ZSTRING;
                    break;
                //case SqlDbType.Binary:
                //    intRetVal = ;
                //    break;
                //case SqlDbType.Image:
                //    intRetVal = ;
                //    break;
                //case SqlDbType.Udt:
                //    intRetVal = ;
                //    break;
                //case SqlDbType.VarBinary:
                //    intRetVal = ;
                //    break;
                //case SqlDbType.Variant:
                //    intRetVal = ;
                //    break;
                //case SqlDbType.Xml:
                //    intRetVal = ;
                //    break;
                default:
                    enmRetVal = TFieldType.UNKNOWN;
                    break;                    
            }
            
            return(enmRetVal);
        }

        public static bool TryParse(int value, out TFieldType fieldType) {

            TFieldType enmFieldType;
            bool bolRetVal;

            try {
                enmFieldType = (TFieldType)value;
                bolRetVal = true;
            } catch(Exception e) {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);;
                enmFieldType = TFieldType.UNKNOWN;
                bolRetVal = false;
            }
            
            fieldType = enmFieldType;

            return(bolRetVal);
        }
    }
}
