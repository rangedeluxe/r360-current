﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WFS.LTA.Common;
using WFS.RecHub.Common;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {
    internal class LTAConvert {

        public static LTAMessageType ConvertMessageTypeToIpdLog(MessageType messageType) {
            switch(messageType) {
                case MessageType.Error:
                    return (LTAMessageType.Error);
                case MessageType.Information:
                    return (LTAMessageType.Information);
                case MessageType.Warning:
                    return (LTAMessageType.Warning);
                default:
                    return (LTAMessageType.Information);
            }
        }

        public static LTAMessageImportance ConvertMessageImportanceToIpdLog(MessageImportance messageImportance) {
            switch(messageImportance) {
                case MessageImportance.Debug:
                    return (LTAMessageImportance.Debug);
                case MessageImportance.Verbose:
                    return (LTAMessageImportance.Verbose);
                case MessageImportance.Essential:
                    return (LTAMessageImportance.Essential);
                default:
                    return (LTAMessageImportance.Essential);
            }
        }
    }
}
