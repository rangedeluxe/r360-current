using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Common;
using System.Data.SqlClient;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 114209 MLH 09/18/2013 
*   - ExtractWizard not handling int64 for SQL data types
* WI 115192 MLH 09/27/2013
*   - Wrong Schema names in Check and Stub population
* WI 129395 DLD 20140218
*   - Cleaned-up some of the code in PopulatePayments and PopulateStubs 
*     following standard nomenclature and replacing "Checks" with "Payments" in areas I was working in for this work item.
* WI 129986 BLR 02/24/2014
*   - Edited to allow parent hierarchy fields.
* WI 130819 BLR 02/25/2014
*   - Edited Int64 function to actually pull an Int64 from the DB.
*   - was receiving an invalid cast exception.   
* WI 129986 BLR 02/25/2014
*   - Uncommented hierarchy populate calls, as they are needed for hierarchy fields.
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 132009 BLR 03/06/2014   
*   - Added in func for joined tables on batch summaries. 
* WI 134896 BLR 04/14/2014   
*   - Added in SiteCodeId for Alert Events.   
* WI 143040 BLR 05/22/2014
*   - Removed dependence on the composite cBatchKey, only BatchID is utilized. 
*   - Changed all BatchIDs to long. 
* WI 130821 BLR 05/14/2014  
*   - Removed the additional logging for the GetOrdinal functions near the bottom.
*     It is acceptable to be catching an error when the extract definition
*     does not contain a particular column and we try to gain the value from it.
*     These are not errors and are expected behavior.   
* WI 142972 BLR 05/22/2014
*   - Populated joined table DimDDAs while populating Payments. 
* WI 133224 BLR 06/10/2014
*   - Added a key-check before adding checks, stubs, and documents to 
*     their dictionary collections. These checks already exist for the 
*     other tables, and some joins may cause duplicate records.
* WI 115658 BLR 06/24/2014
*   - Added functionality for DE fields. Altered 'PopulateDataFields' to allow
*     this. 
* WI 152321 BLR 08/15/2014
*   -Moved Configuration over to ExtractConfigurationSettings. 
* WI 153574 BLR 08/19/2014
*   -Joined Transaction Exception Status. 
* WI 162549 BLR 09/02/2014
*   - Added Immutable date back in, as we need this for requesting images from OLF. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{

    internal static class DataObjects
    {

        public static OrderedDictionary PopulateBanks(
            cExtractDef extractDef,
            SqlDataReader banks)
        {

            OrderedDictionary slRetVal = new OrderedDictionary();

            int intBankID;

            while (banks.Read())
            {

                intBankID = banks.GetInt32(banks.GetOrdinal("RecHubData.dimBanks.SiteBankID"));

                AddUpdateBank(intBankID, extractDef.BankOrderByColumns, slRetVal);

                PopulateDataFields("RecHubData.dimBanks.", ((cBank)slRetVal[(object)intBankID]).BankFields, banks);
            }

            return (slRetVal);
        }
        
        public static void PopulateClientAccounts(
            DbDataReader clientAccountsDR,
            cExtractDef extractDef,
            OrderedDictionary banks)
        {

            int intBankID;
            int intClientAccountID;
            // WI 134896 : Added SiteCodeID as well as populating it for Event Alerts.
            int intSiteCodeID;
            string workgroupName;

            while (clientAccountsDR.Read())
            {

                intBankID = clientAccountsDR.GetInt32(clientAccountsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteBankID"));
                intClientAccountID = clientAccountsDR.GetInt32(clientAccountsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteClientAccountID"));
                intSiteCodeID = clientAccountsDR.GetInt32(clientAccountsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteCodeID"));
                workgroupName = SQLLib.GetOrdinalString(clientAccountsDR, "RecHubData.dimClientAccounts.LongName", "");

                cBank bank = (cBank)banks[(object)intBankID];
                if (bank == null)
                    continue;

                AddUpdateBank(intBankID, extractDef.BankOrderByColumns, banks);
                AddUpdateLockbox(intBankID,  intClientAccountID, intSiteCodeID, workgroupName, extractDef.LockboxOrderByColumns, banks);

                PopulateDataFields("RecHubData.dimClientAccounts.", ((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intClientAccountID]).LockboxFields, clientAccountsDR);
            }
        }

        public static void PopulateBatches(
            SqlDataReader batchesDR,
            cExtractDef extractDef,
            OrderedDictionary banks)
        {

            int intBankID;
            int intLockboxID;
            // WI 134896 : Added SiteCodeID as well as populating it for Event Alerts.
            int intSiteCodeID;
            long intBatchID;
            int immutablebatchdatekey;
            int intdepositdate;
            long sourcebatchid;
            string importtype;
            string batchsource;
            string workgroupName;

            while (batchesDR.Read())
            {

                intBankID = batchesDR.GetInt32(batchesDR.GetOrdinal("RecHubData.dimClientAccounts.SiteBankID"));
                intLockboxID = batchesDR.GetInt32(batchesDR.GetOrdinal("RecHubData.dimClientAccounts.SiteClientAccountID"));
                workgroupName = SQLLib.GetOrdinalString(batchesDR, "RecHubData.dimClientAccounts.LongName", "");
                intSiteCodeID = batchesDR.GetInt32(batchesDR.GetOrdinal("RecHubData.dimClientAccounts.SiteCodeID"));
                intBatchID = batchesDR.GetInt64(batchesDR.GetOrdinal("RecHubData.factBatchSummary.BatchID"));
                immutablebatchdatekey = batchesDR.GetInt32(batchesDR.GetOrdinal("RecHubData.factBatchSummary.ImmutableDateKey"));
                intdepositdate = batchesDR.GetInt32(batchesDR.GetOrdinal("RecHubData.factBatchSummary.DepositDateKey"));
                importtype = batchesDR.GetString(batchesDR.GetOrdinal("RecHubData.dimImportTypes.ShortName"));
                batchsource = batchesDR.GetString(batchesDR.GetOrdinal("RecHubData.dimBatchSources.ShortName"));
                sourcebatchid = batchesDR.GetInt64(batchesDR.GetOrdinal("RecHubData.factBatchSummary.SourceBatchID"));

                cBank bank = (cBank)banks[(object)intBankID];
                if (bank == null)
                    continue;
                cClientAccount clientAccount = (cClientAccount)bank.Lockboxes[(object)intLockboxID];
                if (clientAccount == null)
                    continue;

                AddUpdateBank(intBankID, extractDef.BankOrderByColumns, banks);
                AddUpdateLockbox(intBankID,  intLockboxID, intSiteCodeID, workgroupName, extractDef.LockboxOrderByColumns, banks);
                AddUpdateBatch(intBankID,
                               intLockboxID,
                               intBatchID,
                               immutablebatchdatekey,
                               batchsource,
                               importtype,
                               sourcebatchid,
                               intdepositdate,
                               extractDef.BatchOrderByColumns,
                               banks);

                PopulateDataFields("RecHubData.factBatchSummary.", ((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[(object)(intBatchID.ToString())]).BatchFields, batchesDR);
                PopulateDataFields("RecHubData.dimBatchPaymentTypes.", ((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[intBatchID.ToString()]).BatchFields, batchesDR);
                PopulateDataFields("RecHubData.dimBatchSources.", ((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[(object)(intBatchID.ToString())]).BatchFields, batchesDR);
                PopulateDataFields("RecHubData.dimBatchExceptionStatuses.", ((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[intBatchID.ToString()]).BatchFields, batchesDR);
            }
        }

        public static void PopulateTransactions(
            SqlDataReader transactionsDR,
            cExtractDef extractDef,
            OrderedDictionary banks)
        {

            int intBankID;
            int intLockboxID;
            string workgroupName;
            int intSiteCodeID;

            int intImmutableDateKey;
            long intBatchID;
            int intTransactionID;
            int immutablebatchdatekey;
            long sourcebatchid;

            while (transactionsDR.Read())
            {

                intBankID = transactionsDR.GetInt32(transactionsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteBankID"));
                intLockboxID = transactionsDR.GetInt32(transactionsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteClientAccountID"));
                workgroupName = SQLLib.GetOrdinalString(transactionsDR, "RecHubData.dimClientAccounts.LongName", "");
                intSiteCodeID = transactionsDR.GetInt32(transactionsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteCodeID"));
                intImmutableDateKey = SQLLib.GetOrdinalDateKey(transactionsDR, "RecHubData.factTransactionSummary.ImmutableDateKey", 0);
                intBatchID = transactionsDR.GetInt64(transactionsDR.GetOrdinal("RecHubData.factTransactionSummary.BatchID"));
                intTransactionID = transactionsDR.GetInt32(transactionsDR.GetOrdinal("RecHubData.factTransactionSummary.TransactionID"));
                immutablebatchdatekey = transactionsDR.GetInt32(transactionsDR.GetOrdinal("RecHubData.factTransactionSummary.ImmutableDateKey"));
                sourcebatchid = transactionsDR.GetInt64(transactionsDR.GetOrdinal("RecHubData.factTransactionSummary.SourceBatchID"));

                cBank bank = (cBank)banks[(object)intBankID];
                if (bank == null)
                    continue;
                cClientAccount clientAccount = (cClientAccount)bank.Lockboxes[(object)intLockboxID];
                if (clientAccount == null)
                    continue;
                cBatch batch = (cBatch)clientAccount.Batches[new cBatchKey(intBatchID).ToString()];
                if (batch == null)
                    continue;

                AddUpdateBank(intBankID, extractDef.BankOrderByColumns, banks);
                AddUpdateLockbox(intBankID,  intLockboxID, intSiteCodeID, workgroupName, extractDef.LockboxOrderByColumns, banks);
                AddUpdateTransaction(intBankID,
                                     intLockboxID,
                                     intBatchID,
                                     intTransactionID,
                                     extractDef.TransactionsOrderByColumns,
                                     banks);

                PopulateDataFields("RecHubData.dimTransactionExceptionStatuses.", ((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).TransactionFields, transactionsDR);
                PopulateDataFields("RecHubData.factTransactionSummary.", ((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).TransactionFields, transactionsDR);
            }
        }

        public static void PopulatePayments(
            ExtractConfigurationSettings settings,
            SqlDataReader drPayments,
            cExtractDef extractDef,
            OrderedDictionary banks)
        {

            int intBankID;
            int intLockboxID;
            string workgroupName;
            int intSiteCodeID;
            long intBatchID;
            int intTransactionID;
            int intBatchSequence;
            Decimal decAmount;
            string strAccount;
            string strRoutingNumber;
            long lngNumericRoutingNumber;
            int immutablebatchdatekey;

            while (drPayments.Read())
            {

                intBankID = drPayments.GetInt32(drPayments.GetOrdinal("RecHubData.dimClientAccounts.SiteBankID"));
                intLockboxID = drPayments.GetInt32(drPayments.GetOrdinal("RecHubData.dimClientAccounts.SiteClientAccountID"));
                workgroupName = SQLLib.GetOrdinalString(drPayments, "RecHubData.dimClientAccounts.LongName", "");
                intSiteCodeID = drPayments.GetInt32(drPayments.GetOrdinal("RecHubData.dimClientAccounts.SiteCodeID"));
                intBatchID = drPayments.GetInt64(drPayments.GetOrdinal("RecHubData.factChecks.BatchID"));
                intTransactionID = drPayments.GetInt32(drPayments.GetOrdinal("RecHubData.factChecks.TransactionID"));
                intBatchSequence = SQLLib.GetOrdinalInt32(drPayments, "RecHubData.factChecks.BatchSequence", -1);
                decAmount = SQLLib.GetOrdinalDecimal(drPayments, "RecHubData.factChecks.Amount", (decimal)0.0);
                strAccount = SQLLib.GetOrdinalString(drPayments, "RecHubData.factChecks.Account", string.Empty);
                immutablebatchdatekey = SQLLib.GetOrdinalDateKey(drPayments, "RecHubData.factChecks.ImmutableDateKey", 0);
                strRoutingNumber = SQLLib.GetOrdinalString(drPayments, "RecHubData.factChecks.RoutingNumber", string.Empty);
                lngNumericRoutingNumber = SQLLib.GetOrdinalInt64(drPayments, "RecHubData.factChecks.NumericRoutingNumber", -1);

                object bankID = (object)intBankID;
                object lockBoxID = (object)intLockboxID;
                object batchKey = (object)(intBatchID.ToString());
                object transactionID = (object)intTransactionID;

                cBank bank = (cBank)banks[(object)bankID];
                if (bank == null)
                    continue;
                cClientAccount clientAccount = (cClientAccount)bank.Lockboxes[(object)lockBoxID];
                if (clientAccount == null)
                    continue;
                cBatch batch = (cBatch)clientAccount.Batches[(object)batchKey];
                if (batch == null)
                    continue;
                cTransaction transaction = (cTransaction)batch.Transactions[(object)transactionID];
                if (transaction == null)
                    continue;

                cPayment payment = new cPayment(settings, transaction, intTransactionID, intBatchSequence,
                    decAmount, strAccount, strRoutingNumber, lngNumericRoutingNumber, extractDef.PaymentsOrderByColumns);

                AddUpdateBank(intBankID, extractDef.BankOrderByColumns, banks);
                AddUpdateLockbox(intBankID,  intLockboxID, intSiteCodeID, workgroupName, extractDef.LockboxOrderByColumns, banks);

                var payments = ((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).Payments;
                var newKey = new cItemKey(intBatchID, intBatchSequence).ToString();
                if (!payments.Contains(newKey))
                    payments.Add(newKey, payment);

                PopulateDataFields(DatabaseConstants.TABLE_NAME_FACTCHECKS + ".", ((cPayment)((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).Payments[new cItemKey(intBatchID, intBatchSequence).ToString()]).CheckFields, drPayments);
                PopulateDataFields(DatabaseConstants.TABLE_NAME_DIMDDAS + ".", ((cPayment)((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).Payments[new cItemKey(intBatchID, intBatchSequence).ToString()]).CheckFields, drPayments);
            }
        }


        public static void PopulateStubs(
            SqlDataReader stubsDR,
            cExtractDef extractDef,
            OrderedDictionary banks)
        {

            int intBankID;
            int intLockboxID;
            string workgroupName;
            int intSiteCodeID;
            long intBatchID;
            int intTransactionID;
            int intBatchSequence;
            Decimal decAmount;

            cStub stub;

            while (stubsDR.Read())
            {

                intBankID = stubsDR.GetInt32(stubsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteBankID"));
                intLockboxID = stubsDR.GetInt32(stubsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteClientAccountID"));
                workgroupName = SQLLib.GetOrdinalString(stubsDR, "RecHubData.dimClientAccounts.LongName", "");
                intSiteCodeID = stubsDR.GetInt32(stubsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteCodeID"));
                intBatchID = stubsDR.GetInt64(stubsDR.GetOrdinal("RecHubData.factStubs.BatchID"));
                intTransactionID = stubsDR.GetInt32(stubsDR.GetOrdinal("RecHubData.factStubs.TransactionID"));
                intBatchSequence = stubsDR.GetInt32(stubsDR.GetOrdinal("RecHubData.factStubs.BatchSequence"));
                decAmount = SQLLib.GetOrdinalDecimal(stubsDR, "RecHubData.factStubs.Amount", (Decimal)0.0);


                cBank bank = (cBank)banks[(object)intBankID];
                if (bank == null)
                    continue;
                cClientAccount clientAccount = (cClientAccount)bank.Lockboxes[(object)intLockboxID];
                if (clientAccount == null)
                    continue;
                cBatch batch = (cBatch)clientAccount.Batches[new cBatchKey(intBatchID).ToString()];
                if (batch == null)
                    continue;
                cTransaction transaction = (cTransaction)batch.Transactions[(object)intTransactionID];
                if (transaction == null)
                    continue;

                stub = new cStub((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[(object)(intBatchID.ToString())]).Transactions[(object)intTransactionID],
                                 intTransactionID,
                                 intBatchSequence,
                                 decAmount,
                                 extractDef.StubsOrderByColumns);

                var stubs = ((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).Stubs;
                var newKey = new cItemKey(intBatchID, intBatchSequence).ToString();

                AddUpdateBank(intBankID, extractDef.BankOrderByColumns, banks);
                AddUpdateLockbox(intBankID,  intLockboxID, intSiteCodeID, workgroupName, extractDef.LockboxOrderByColumns, banks);
                if (!stubs.Contains(newKey))
                    stubs.Add(newKey, stub);

                PopulateDataFields("RecHubData.factStubs.", ((cStub)((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).Stubs[new cItemKey(intBatchID, intBatchSequence).ToString()]).StubFields, stubsDR);
            }
        }

        public static void PopulateDocuments(
            ExtractConfigurationSettings settings,
            SqlDataReader documentsDR,
            cExtractDef extractDef,
            OrderedDictionary banks)
        {

            int intBankID;
            int intLockboxID;
            string workgroupName;
            int intSiteCodeID;
            long intBatchID;
            int intTransactionID;
            int intBatchSequence;
            int intDocumentSequence;
            string strFileDescriptor;

            cDocument document;
            while (documentsDR.Read())
            {

                intBankID = documentsDR.GetInt32(documentsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteBankID"));
                intLockboxID = documentsDR.GetInt32(documentsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteClientAccountID"));
                workgroupName = SQLLib.GetOrdinalString(documentsDR, "RecHubData.dimClientAccounts.LongName", ""); //documentsDR.GetString(documentsDR.GetOrdinal("RecHubData.dimClientAccounts.LongName"));
                intSiteCodeID = documentsDR.GetInt32(documentsDR.GetOrdinal("RecHubData.dimClientAccounts.SiteCodeID"));
                intBatchID = documentsDR.GetInt64(documentsDR.GetOrdinal("RecHubData.factDocuments.BatchID"));
                intTransactionID = documentsDR.GetInt32(documentsDR.GetOrdinal("RecHubData.factDocuments.TransactionID"));
                intBatchSequence = documentsDR.GetInt32(documentsDR.GetOrdinal("RecHubData.factDocuments.BatchSequence"));
                strFileDescriptor = SQLLib.GetOrdinalString(documentsDR, "RecHubData.dimDocumentTypes.FileDescriptor", string.Empty);
                intDocumentSequence = SQLLib.GetOrdinalInt32(documentsDR, "RecHubData.factDocuments.DocumentSequence", 0);

                cBank bank = (cBank)banks[(object)intBankID];
                if (bank == null)
                    continue;
                cClientAccount clientAccount = (cClientAccount)bank.Lockboxes[(object)intLockboxID];
                if (clientAccount == null)
                    continue;
                cBatch batch = (cBatch)clientAccount.Batches[new cBatchKey(intBatchID).ToString()];
                if (batch == null)
                    continue;
                cTransaction transaction = (cTransaction)batch.Transactions[(object)intTransactionID];
                if (transaction == null)
                    continue;

                document = new cDocument(
                    settings,
                    (cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID],
                    intTransactionID,
                    intBatchSequence,
                    intDocumentSequence,
                    strFileDescriptor,
                    extractDef.DocumentsOrderByColumns);

                var documents = ((cTransaction)((cBatch)((cClientAccount)((cBank)
                    banks[(object)intBankID])
                        .Lockboxes[(object)intLockboxID])
                        .Batches[new cBatchKey(intBatchID).ToString()])
                        .Transactions[(object)intTransactionID])
                        .Documents;

                AddUpdateBank(intBankID, extractDef.BankOrderByColumns, banks);
                AddUpdateLockbox(intBankID,  intLockboxID, intSiteCodeID, workgroupName, extractDef.LockboxOrderByColumns, banks);

                var newkey = new cItemKey(intBatchID, intBatchSequence).ToString();
                if (!documents.Contains(newkey))
                    documents.Add(newkey, document);

                PopulateDataFields("RecHubData.factDocuments.", ((cDocument)((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).Documents[new cItemKey(intBatchID, intBatchSequence).ToString()]).DocumentFields, documentsDR);
                PopulateDataFields("RecHubData.dimDocumentTypes.", ((cDocument)((cTransaction)((cBatch)((cClientAccount)((cBank)banks[(object)intBankID]).Lockboxes[(object)intLockboxID]).Batches[new cBatchKey(intBatchID).ToString()]).Transactions[(object)intTransactionID]).Documents[new cItemKey(intBatchID, intBatchSequence).ToString()]).DocumentFields, documentsDR);
            }
        }

        private static void AddUpdateBank(
            int bankID,
            IOrderByColumn[] orderByColumns,
            OrderedDictionary banks)
        {

            cBank bank;

            if (!banks.Contains(bankID))
            {
                bank = new cBank(bankID, orderByColumns);
                banks.Add(bankID, bank);
            }
        }

        // WI 134896 : Added SiteCodeID for Event Alerts.
        private static void AddUpdateLockbox(
            int bankID,
            int lockboxID,
            int sitecodeID,
            string workgroupName,
            IOrderByColumn[] orderByColumns,
            OrderedDictionary banks)
        {

            cClientAccount objLockbox;

            if (!((cBank)banks[(object)bankID]).Lockboxes.Contains(lockboxID))
            {
                objLockbox = new cClientAccount((cBank)banks[(object)bankID], bankID, lockboxID, sitecodeID, workgroupName, orderByColumns);
                ((cBank)banks[(object)bankID]).Lockboxes.Add(lockboxID, objLockbox);
            }
        }

        private static void AddUpdateBatch(
            int bankID,
            int lockboxID,
            long batchID,
            int immutabledatekey,
            string batchsource,
            string importtypeshortname,
            long sourcebatchid,
            int depositdatekey,
            IOrderByColumn[] orderByColumns,
            OrderedDictionary banks)
        {

            cBatch objBatch;

            if (!((cClientAccount)((cBank)banks[(object)bankID]).Lockboxes[(object)lockboxID]).Batches.Contains(batchID.ToString()))
            {
                objBatch = new cBatch(((cClientAccount)((cBank)banks[(object)bankID]).Lockboxes[(object)lockboxID]),
                                   bankID,
                                   lockboxID,
                                   batchID,
                                   immutabledatekey,
                                   batchsource,
                                   importtypeshortname,
                                   sourcebatchid,
                                   depositdatekey,
                                   orderByColumns);
                ((cClientAccount)((cBank)banks[(object)bankID]).Lockboxes[(object)lockboxID]).Batches.Add(batchID.ToString(), objBatch);
            }
        }

        private static void AddUpdateTransaction(
            int bankID,
            int lockboxID,
            long batchID,
            int transactionID,
            IOrderByColumn[] orderByColumns,
            OrderedDictionary banks)
        {

            cTransaction objTransaction;

            if (!((cBatch)((cClientAccount)((cBank)banks[(object)bankID]).Lockboxes[(object)lockboxID]).Batches[(object)batchID.ToString()]).Transactions.Contains(transactionID))
            {
                objTransaction = new cTransaction(((cBatch)((cClientAccount)((cBank)banks[(object)bankID]).Lockboxes[(object)lockboxID]).Batches[(object)batchID.ToString()]),
                                               transactionID,
                                               orderByColumns);
                ((cBatch)((cClientAccount)((cBank)banks[(object)bankID]).Lockboxes[(object)lockboxID]).Batches[(object)batchID.ToString()]).Transactions.Add(transactionID, objTransaction);
            }
        }

        private static void PopulateDataFields(
            string tablePrefix,
            Dictionary<string, object> fields,
            DbDataReader dr)
        {

            string strColumnName;
            
            if (dr.FieldCount > 0)
            {
                for (int i = 0; i < dr.FieldCount; ++i)
                {
                    strColumnName = dr.GetName(i);
                    if ((strColumnName.StartsWith("RecHubData.ChecksDataEntry.") || strColumnName.StartsWith("RecHubData.StubsDataEntry.") || strColumnName.ToLower().StartsWith(tablePrefix.ToLower())) &&
                        !fields.ContainsKey(strColumnName.Substring(strColumnName.IndexOf('.') + 1)))
                    {
                        if (!fields.ContainsKey(strColumnName))
                        {
                            fields.Add(strColumnName, GetDataField(dr, strColumnName));
                        }
                    }
                }
            }
        }

        private static void PopulateStubsDataEntryFields(
            cStub stub,
            SqlDataReader StubsDEDR)
        {

            while (StubsDEDR.Read())
            {

                int intBankID = Convert.ToInt32(GetDataField(StubsDEDR, "dimClientAccounts.SiteBankID"));
                int intClientAccountID = Convert.ToInt32(GetDataField(StubsDEDR, "dimClientAccounts.SiteClientAccountID"));
                int intBatchID = Convert.ToInt32(GetDataField(StubsDEDR, "factDataEntryDetails.BatchID"));
                int intImmutableDateKey = Convert.ToInt32(GetDataField(StubsDEDR, "factDataEntryDetails.ImmutableDateKey"));
                int intBatchSequence = Convert.ToInt32(GetDataField(StubsDEDR, "factDataEntryDetails.BatchSequence"));

                if (stub.Transaction.Batch.BankID == intBankID &&
                   stub.Transaction.Batch.ClientAccountID == intClientAccountID &&
                   stub.Transaction.Batch.BatchID == intBatchID &&
                   stub.Transaction.Batch.ImmutableDateKey == intImmutableDateKey &&
                   stub.BatchSequence == intBatchSequence)
                {


                    /*
                       !!!     Temporaryily disabled       !!!  MLH 09/27/2013
                       WI 114989 Wizard not handling Data Entry Correctly.  In order to make progress, commenting this out
                       to fix bugs firing before it gets to here.  CheckFields DataTable.Column definitions are pulled from RecHubData.factChecks
                       they need to be coming from the related dynamic columns in dimDataEntryColumns/factDataEntryDetails...
                   */
                    if (!stub.StubFields.ContainsKey(StubsDEDR["dimDataEntryColumns.FldName"].ToString()))
                    {
                        stub.StubFields.Add(StubsDEDR["dimDataEntryColumns.FldName"].ToString(), StubsDEDR["factDataEntryDetails.DataEntryValue"].ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Parses out Banks, Customers, and Lockboxes that do not contain Batches.
        /// </summary>
        /// <param name="Banks"></param>
        /// <returns></returns>
        internal static bool FilterNoBatchObjects(OrderedDictionary banks)
        {

            if (banks.Count > 0)
            {
                for (int i = banks.Count - 1; i >= 0; --i)
                {
                    if (((cBank) banks[i]).Lockboxes.Count > 0)
                    {
                        for (int j = ((cBank) banks[i]).Lockboxes.Count - 1; j >= 0; --j)
                        {
                            if (((cClientAccount) ((cBank) banks[i]).Lockboxes[j]).Batches.Count == 0)
                            {
                                ((cBank) banks[i]).Lockboxes.RemoveAt(j);
                            }
                        }
                    }
                    if (((cBank) banks[i]).Lockboxes.Count == 0)
                    {
                        banks.RemoveAt(i);
                    }
                }
            }

            return (true);
        }

        private static object GetDataField(
            SqlDataReader dataFieldDR,
            string fieldName) {
            DbDataReader dbDataReader = dataFieldDR;

            return GetDataField(dbDataReader, fieldName);
        }

        private static object GetDataField(
            DbDataReader dataFieldDR,
            string fieldName)
            {

            object objRetVal;
            int intOrdinal;

            intOrdinal = dataFieldDR.GetOrdinal(fieldName);

            if (intOrdinal < 0)
            {
                objRetVal = null;
            }
            else if (dataFieldDR.IsDBNull(intOrdinal))
            {
                objRetVal = null;
            }
            else
            {

                switch (dataFieldDR.GetFieldType(intOrdinal).FullName)
                {
                    case "System.Boolean":
                        objRetVal = dataFieldDR.GetBoolean(intOrdinal);
                        break;
                    case "System.Decimal":
                        objRetVal = dataFieldDR.GetDecimal(intOrdinal);
                        break;
                    case "System.Single":
                        objRetVal = dataFieldDR.GetDecimal(intOrdinal);
                        break;
                    case "System.Double":
                        objRetVal = dataFieldDR.GetDouble(intOrdinal);
                        break;
                    case "System.Guid":
                        objRetVal = dataFieldDR.GetGuid(intOrdinal);
                        break;
                    case "System.DateTime":
                        //// For some strange reason, datetime values with 0 hours, 0 
                        //// .minutes, and 0 seconds are formatted differently than other dates.
                        //// This makes no sense, but here we go anyway.
                        ////if(((DateTime)dr[FieldName]).ToString("hh:mm:ss.ttt") == "00:00:00.000") {
                        //if(((DateTime)dr[FieldName]).TimeOfDay == new TimeSpan(0, 0, 0)) {
                        //    strRetVal = ((DateTime)dr[FieldName]).ToString("M/d/yyyyHHmmfffff");
                        //} else {
                        //    strRetVal = ((DateTime)dr[FieldName]).ToString("M/d/yyyy h:mm:ss tt");
                        //} 
                        ////"00000000"
                        ////strRetVal = ((DateTime)dr[FieldName]).ToString("M/d/yyyyhhmmfffff");
                        ////strRetVal = ((DateTime)dr[FieldName]).ToString("M/d/yyyy");
                        ////strRetVal = ((DateTime)dr[FieldName]).ToString("M/d/yyyy h:mm:ss tt");
                        objRetVal = dataFieldDR.GetDateTime(intOrdinal);
                        break;
                    case "System.Int16":
                        objRetVal = dataFieldDR.GetInt16(intOrdinal);
                        break;
                    case "System.Int32":
                        objRetVal = dataFieldDR.GetInt32(intOrdinal);
                        break;
                    case "System.Int64":
                        objRetVal = dataFieldDR.GetInt64(intOrdinal);
                        break;
                    case "System.Byte":
                        objRetVal = dataFieldDR.GetByte(intOrdinal);
                        break;
                    case "System.String":
                        objRetVal = dataFieldDR.GetString(intOrdinal).TrimEnd();
                        break;
                    default:
                        objRetVal = dataFieldDR.GetString(intOrdinal).TrimEnd();
                        break;
                }
            }

            return (objRetVal);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class SQLLib
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="ColumnName"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static decimal GetOrdinalDecimal(
            SqlDataReader dr,
            string columnName,
            decimal defaultValue)
        {

            int intOrdinal;
            decimal decRetVal;

            try
            {
                intOrdinal = dr.GetOrdinal(columnName);
                if (dr.IsDBNull(intOrdinal))
                {
                    decRetVal = defaultValue;
                }
                else
                {
                    decRetVal = dr.GetDecimal(intOrdinal);
                }
            }
            catch (IndexOutOfRangeException e)
            {
                decRetVal = defaultValue;
            }

            return (decRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="ColumnName"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static string GetOrdinalString(
            DbDataReader dr,
            string columnName,
            string defaultValue)
        {

            int intOrdinal;
            string strRetVal;

            try
            {
                intOrdinal = dr.GetOrdinal(columnName);
                if (dr.IsDBNull(intOrdinal))
                {
                    strRetVal = defaultValue;
                }
                else
                {
                    strRetVal = dr.GetString(intOrdinal);
                }
            }
            catch (IndexOutOfRangeException e)
            {
                strRetVal = defaultValue;
            }

            return (strRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="ColumnName"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static string GetOrdinalString(
            SqlDataReader dr,
            string columnName,
            string defaultValue) 
            {
            DbDataReader dbDataReader = dr;
            return GetOrdinalString(dbDataReader, columnName, defaultValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="ColumnName"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static int GetOrdinalInt32(
            SqlDataReader dr,
            string columnName,
            int defaultValue)
        {

            int intOrdinal;
            int intRetVal;

            try
            {
                intOrdinal = dr.GetOrdinal(columnName);
                if (dr.IsDBNull(intOrdinal))
                {
                    intRetVal = defaultValue;
                }
                else
                {
                    intRetVal = dr.GetInt32(intOrdinal);
                }
            }
            catch (IndexOutOfRangeException e)
            {
                intRetVal = defaultValue;
            }

            return (intRetVal);
        }

        public static long GetOrdinalInt64(
            SqlDataReader dr,
            string columnName,
            long defaultValue)
        {

            int intOrdinal;
            long lngRetVal;

            try
            {
                intOrdinal = dr.GetOrdinal(columnName);
                if (dr.IsDBNull(intOrdinal))
                {
                    lngRetVal = defaultValue;
                }
                else
                {
                    // WI 130819 : changed to 64, was receiving an invalid cast exception.
                    lngRetVal = dr.GetInt64(intOrdinal);
                }
            }
            catch (IndexOutOfRangeException e)
            {
                lngRetVal = defaultValue;
            }

            return (lngRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="ColumnName"></param>
        /// <param name="DefaultValue"></param>
        /// <returns></returns>
        public static int GetOrdinalDateKey(
            SqlDataReader dr,
            string columnName,
            int defaultValue)
        {

            int intOrdinal;
            int intRetVal;

            try
            {
                intOrdinal = dr.GetOrdinal(columnName);
                if (dr.IsDBNull(intOrdinal))
                {
                    intRetVal = defaultValue;
                }
                else
                {
                    intRetVal = dr.GetInt32(intOrdinal);
                }
            }
            catch (IndexOutOfRangeException e)
            {
                intRetVal = defaultValue;
            }

            return (intRetVal);
        }
    }
}
