//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.IO;
//using System.Reflection;
//using System.Collections;
//using System.Text;
//using System.Linq;

//using WFS.RecHub.Common;
//using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
//using System.Configuration;

///******************************************************************************
//** WAUSAU Financial Systems (WFS)
//** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
//*******************************************************************************
//*******************************************************************************
//** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
//*******************************************************************************
//* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
//* other trademarks cited herein are property of their respective owners.
//* These materials are unpublished confidential and proprietary information
//* of WFS and contain WFS trade secrets. These materials may not be used,
//* copied, modified or disclosed except as expressly permitted in writing by
//* WFS (see the WFS license agreement for details). All copies, modifications
//* and derivative works of these materials are property of WFS.
//*
//* Author: Joel Caples
//* Date: 
//*
//* Purpose: 
//*
//* Modification History
//* WI  71865 JMC 05/30/2013
//*   -Initial Version 
//* WI 115356 Derek Davison 02/07/2014
//*   - Moving .ini settings into app.config file.
//* WI 116442 Derek Davison 02/13/2014
//*   - Major rework. No longer inheriting from cSiteOptions. Very few elements of that base class were being utilized.
//*     - Several values (ImagePath, ExceptionLogFilePath) were still being pulled from .ini by cSiteOptions base class causing Image Extracts to fail.
//*     - This class now stands on its own and gets most of its values from the app.config file.
//* WI 130165 DLD 02/19/2014
//*   -Expanded Exception handling and message logging. 
//* WI 129719 DLD 02/24/2014
//*   -Removed ImagePath. Image repository is now OLFServices not a UNC path. 
//* WI 130364 DLD 02/19/2014
//*   -Validation of appSettings values. 
//*   -Moved class to ExtractLib 
//******************************************************************************/
//namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
//{
//    public sealed class cExtractSettings //: cSiteOptions
//    {
//        #region Members

//        public event outputErrorEventHandler OutputError;

//        public string SetupPath { get; private set; }

//        public string ExceptionLogFilePath { get; private set; }

//        public string LogFilePath { get; private set; }

//        public int LogFileMaxSize { get; private set; }

//        public int WorkstationID { get; private set; }

//        public DefFileFormatEnum DefaultDefFileFormat { get; private set; }

//        public bool CreateZeroLengthFile { get; private set; }

//        //public string ImagePath { get; private set; }

//        /// <summary>
//        /// Data Access connection string. Set in app.config \ appSettings
//        /// </summary>
//        public string DbConnectionString { get; private set; }

//        private CustomConfigurationSection _customConfig = null;
//        /// <summary>
//        /// Reference to a custom section in the app.config file we're using to store collection-based settings.
//        /// </summary>
//        private CustomConfigurationSection CustomConfigurations
//        {
//            get
//            {
//                return _customConfig ?? (_customConfig = ConfigurationManager.GetSection(CustomConfigurationSection.MyName) as CustomConfigurationSection);
//            }
//        }

//        private StringCollection _extractStatsColumns = null;
//        /// <summary>
//        /// A list of columns shown in a statistics report displayed at the end of an ExtractRun.
//        /// </summary>
//        public StringCollection ExtractStatsColumns
//        {
//            get
//            {
//                if (_extractStatsColumns == null)
//                {
//                    _extractStatsColumns = new StringCollection();
//                    if (CustomConfigurations != null)
//                    {
//                        foreach (ConfirmationField o in CustomConfigurations.ConfirmationFields)
//                            _extractStatsColumns.Add(o.Name);
//                    }
//                }
//                return _extractStatsColumns;
//            }
//        }

//        private List<string> _colsToRemove = null;
//        /// <summary>
//        /// A list of columns that will be removed from table column-name context menus
//        /// </summary>
//        public List<string> EntryColumnsToRemove
//        {
//            get
//            {
//                if (_colsToRemove == null)
//                {
//                    if (CustomConfigurations != null)
//                    {
//                        _colsToRemove = new List<string>();

//                        foreach (ColumnToRemove col in CustomConfigurations.ColumnsToRemove)
//                        {
//                            _colsToRemove.Add(col.Name);
//                        }
//                    }
//                }
//                return _colsToRemove;
//            }
//        }

//        #endregion

//        #region Constructors

//        public cExtractSettings()
//        {
//            LoadSettings();
//        }

//        #endregion

//        #region Methods

//        private void LoadSettings()
//        {
//            //string strAssemblyLocation =  CommonLib.AppExePath;
//            string strAssemblyPath = CommonLib.AppPath;

//            string strDefaultExtractPath;
//            string strDefaultDataPath;

//            //if (Assembly.GetEntryAssembly() == null)
//            //    strAssemblyLocation = Assembly.GetCallingAssembly().Location;
//            //else
//            //    strAssemblyLocation = Assembly.GetEntryAssembly().Location;

//            //strAssemblyPath = strAssemblyLocation.Substring(0, strAssemblyLocation.LastIndexOf(@"\"));
//            strDefaultExtractPath = Path.Combine(strAssemblyPath, @"..\Extracts");
//            strDefaultDataPath = Path.Combine(strAssemblyPath, @"..\Data");

//            WorkstationID = cAppSettingsLib.Get<int>(ExtractAppSettings.WorkstationID, -1);
//            DbConnectionString = cAppSettingsLib.Get(ExtractAppSettings.ConnectionString);
//            LogFileMaxSize = cAppSettingsLib.Get<int>(ExtractAppSettings.LogFileMaxSize, 0);
//            //ImagePath = cAppSettingsLib.Get(ExtractAppSettings.ImagePath);
//            LogFilePath = cAppSettingsLib.Get(ExtractAppSettings.LogFilePath);
//            ExceptionLogFilePath = cAppSettingsLib.Get(ExtractAppSettings.ExceptionLogFile);
//            SetupPath = cAppSettingsLib.Get(ExtractAppSettings.SetupPath, strDefaultExtractPath);

//            if (SetupPath == strDefaultExtractPath && !Directory.Exists(strDefaultExtractPath))
//                Directory.CreateDirectory(strDefaultExtractPath);

//            if (ExceptionLogFilePath.StartsWith(strDefaultDataPath) && !Directory.Exists(strDefaultDataPath))
//                Directory.CreateDirectory(strDefaultDataPath);

//            var strTemp = cAppSettingsLib.Get(ExtractAppSettings.DefaultDefinitionFileFormat, "xml");
//            if (strTemp.ToLower().Trim() == "xml")
//                DefaultDefFileFormat = DefFileFormatEnum.xml;
//            else
//                DefaultDefFileFormat = DefFileFormatEnum.cxs;

//            strTemp = cAppSettingsLib.Get(ExtractAppSettings.CreateZeroLengthFile, "1");
//            CreateZeroLengthFile = strTemp.ToLower().Trim() == "1";

//            ValidateSettings();
//        }

//        private void ValidateSettings()
//        {
//            var userMessage = new StringBuilder();
//            var log = new StringBuilder();
//            var tempMsg = new StringBuilder();

//            if (LogFileMaxSize < 1024)
//            {
//                tempMsg.AppendLine("The appSetting 'LogFileMaxSize' specified in your .config file is invalid: " + LogFileMaxSize.ToString());
//                tempMsg.AppendLine("Setting max file size to 1 Megabyte by default.");
//                LogFileMaxSize = 1024;
//            }

//            if (String.IsNullOrWhiteSpace(DbConnectionString))
//            {
//                tempMsg.AppendLine("The appSetting 'DbConnectionString' specified in your .config file is missing or invalid.");
//                tempMsg.AppendLine(DbConnectionString);
//                tempMsg.AppendLine(Environment.NewLine);
//                userMessage.Append(tempMsg.ToString());
//                log.Append(tempMsg.ToString());
//            }
//            else
//            {
//                try { ExtractLib.CommonLib.DecryptUserIdInConnectionString(DbConnectionString); }
//                catch (Exception ex)
//                {
//                    tempMsg.AppendLine(ex.Message);
//                    tempMsg.AppendLine(Environment.NewLine);
//                }
//            }

//            if (tempMsg.Length > 0)
//            {
//                    userMessage.AppendLine(tempMsg.ToString());
//                    log.AppendLine(tempMsg.ToString());
//                    tempMsg.Clear();
//            }

//            if (!CommonLib.CheckDirectory(SetupPath))
//            {
//                tempMsg.Clear();

//                tempMsg.AppendLine("The appSetting 'SetupPath' specified in your .config file cannot be found:");
//                tempMsg.AppendLine(SetupPath);
//                tempMsg.AppendLine(Environment.NewLine);

//                userMessage.AppendLine(tempMsg.ToString());
//                log.AppendLine(tempMsg.ToString());
//            }

//            if (!CommonLib.CheckDirectory(LogFilePath))
//            {
//                tempMsg.Clear();

//                tempMsg.AppendLine("The appSetting 'LogFilePath' specified in your .config file cannot be found:");
//                tempMsg.AppendLine(LogFilePath);
//                LogFilePath = Path.Combine(CommonLib.AppPath, "{0:yyyyMMdd}_ExtractWizardLog.txt");
//                if (CommonLib.CheckDirectory(LogFilePath))
//                {
//                    tempMsg.AppendLine("The following default will be used: " + LogFilePath);
//                }
//                tempMsg.AppendLine(Environment.NewLine);
//                log.AppendLine(tempMsg.ToString());
//            }

//            if (!CommonLib.CheckDirectory(ExceptionLogFilePath))
//            {
//                tempMsg.Clear();

//                tempMsg.AppendLine("The appSetting 'ExceptionLogFilePath' specified in your .config file cannot be found:");
//                tempMsg.AppendLine(ExceptionLogFilePath);
//                if (CommonLib.CheckDirectory(Path.Combine(CommonLib.AppPath, "{0:yyyyMMdd}_ExtractWizardExceptionLog.txt")))
//                {
//                    tempMsg.AppendLine("The following default will be used: " + ExceptionLogFilePath);
//                }
//                tempMsg.AppendLine(Environment.NewLine);
//                log.AppendLine(tempMsg.ToString());
//            }
//            cCommonLogLib.LogMessageLTA(log.ToString(), LTA.Common.LTAMessageImportance.Essential);
//            // DLD TODO - show user message
//        }

//        public string QualifyPath(string filePath)
//        {

//            string strParentPath;
//            string strRetVal;

//            if (filePath.StartsWith("."))
//            {
//                strParentPath = Directory.GetParent(SetupPath).FullName;
//                strRetVal = strParentPath + (strParentPath.EndsWith(@"\") ? string.Empty : @"\") + filePath;
//            }
//            else
//            {
//                strRetVal = filePath;
//            }

//            return (strRetVal);
//        }

//        private void OnExceptionOccurred(object sender, Exception e)
//        {
//            if (OutputError != null)
//            {
//                OutputError(e);
//            }
//        }

//        #endregion

//    }

//    #region Custom app.config Configuration Section(s) handlers
//    // WI 115356 - Derek - Moving ini settings to app.config file

//    public class ConfirmationField : ConfigurationElement
//    {
//        [ConfigurationProperty("Name", IsKey = true, IsRequired = true)]
//        public string Name
//        {
//            get { return this["Name"].ToString(); }
//        }

//        [ConfigurationProperty("DisplayOrder")]
//        public int DisplayOrder
//        {
//            get { return Convert.ToInt32(this["DisplayOrder"]); }
//        }
//    }

//    /// <summary>
//    /// List of available fields that will be displayed to the User When an Extract has
//    /// been generated using ExtractRun or ExtractWizard.  The values will be displayed
//    /// to the user ordered by the numerical value of each key.  
//    /// Values not located in this list will not be displayed.
//    /// </summary>
//    public class ConfirmationFields : ConfigurationElementCollection
//    {
//        public const string MyName = "ConfirmationFields";

//        protected override ConfigurationElement CreateNewElement()
//        {
//            return new ConfirmationField();
//        }

//        protected override object GetElementKey(ConfigurationElement element)
//        {
//            return ((ConfirmationField)element).Name;
//        }
//    }

//    class ColumnToRemove : ConfigurationElement
//    {
//        [ConfigurationProperty("Name", IsKey = true, IsRequired = true)]
//        public string Name
//        {
//            get { return this["Name"].ToString(); }
//        }
//    }

//    /// <summary>
//    /// List of available fields that will be removed from column selection menus when building an extract.
//    /// </summary>
//    [ConfigurationCollection(typeof(ColumnToRemove))]
//    class ColumnsToRemove : ConfigurationElementCollection
//    {
//        public const string MyName = "ColumnsToRemove";

//        protected override ConfigurationElement CreateNewElement()
//        {
//            return new ColumnToRemove();
//        }

//        protected override object GetElementKey(ConfigurationElement element)
//        {
//            return ((ColumnToRemove)element).Name;
//        }
//    }

//    /// <summary>
//    /// Allows the use of a custom set of configuration values in the app.config file. 
//    /// Section name needs to be referenced in the app.config's "<configsections></configsections>"
//    /// </summary>
//    class CustomConfigurationSection : ConfigurationSection
//    {
//        public const string MyName = "CustomConfigs";

//        [ConfigurationProperty(ConfirmationFields.MyName)]
//        [ConfigurationCollection(typeof(ConfirmationField))]
//        public ConfirmationFields ConfirmationFields
//        {
//            get { return this[ConfirmationFields.MyName] as ConfirmationFields; }
//        }

//        [ConfigurationProperty(ColumnsToRemove.MyName)]
//        [ConfigurationCollection(typeof(ColumnToRemove))]
//        public ColumnsToRemove ColumnsToRemove
//        {
//            get { return this[ColumnsToRemove.MyName] as ColumnsToRemove; }
//        }
//    }

//    #endregion
//}