using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 114570 MLH 09/20/2013
*   - Removed CurrentProcessingDate from Standard Fields (for v2.0)
* WI 129719 DLD 02/24/2014
*   -Converting image repository from ipoPics.dll to OLFServices 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{

    public static class CustomFormat
    {

        public static string FormatValue(
            int value,
            string format)
        {

            string strRetVal = string.Empty;

            if (format.Length > 0)
            {

                if (format == value.ToString(format))
                {
                    strRetVal = value.ToString();
                }
                else {
                    strRetVal = value.ToString(format);
                }
            }
            else {
                strRetVal = value.ToString();
            }

            return (strRetVal);
        }

        public static string FormatValue(
            DateTime value,
            string format)
        {

            string strRetVal = string.Empty;
            string strFormat = format;

            if (format.Contains("%r"))
                strRetVal = CustomFormat.FormatRegex(value.ToString(), format);
            else
            {
                switch (strFormat)
                {
                    case "mmmm d, yyyy":        // "October 13, 2005"
                        strRetVal = value.ToString("MMMM d, yyyy");
                        break;
                    case "iii":                 // "iii"
                        strRetVal = value.ToString("iii");
                        break;
                    case "yyiii":               // "05iii"
                        strRetVal = value.ToString("yyiii");
                        break;
                    case "h:n:s":               // "0:0:0"
                        strRetVal = value.ToString("H:m:s");
                        break;
                    case "h:n:s am/pm":         // "12:0:0 am"
                        strRetVal = value.ToString("h:m:s tt").ToLower();
                        break;
                    default:                    // "10/13/2005"
                        if (strFormat.Length > 0)
                        {
                            while (strFormat.IndexOf("YYYY") > -1)
                            {
                                strFormat = strFormat.Replace("YYYY", "yyyy");
                            }
                            while (strFormat.IndexOf("YY") > -1)
                            {
                                strFormat = strFormat.Replace("YY", "yy");
                            }
                            while (strFormat.IndexOf("DD") > -1)
                            {
                                strFormat = strFormat.Replace("DD", "dd");
                            }
                            while (strFormat.IndexOf("mmmm") > -1)
                            {
                                strFormat = strFormat.Replace("mmmm", "MMMM");
                            }
                            while (strFormat.IndexOf("mmm") > -1)
                            {
                                strFormat = strFormat.Replace("mmm", "MMM");
                            }
                            while (strFormat.IndexOf("mm") > -1)
                            {
                                strFormat = strFormat.Replace("mm", "MM");
                            }
                            while (strFormat.IndexOf("hh") > -1)
                            {
                                strFormat = strFormat.Replace("hh", "HH");
                            }
                            while (strFormat.IndexOf("nn") > -1)
                            {
                                strFormat = strFormat.Replace("nn", "mm");
                            }
                            while (strFormat.IndexOf("NN") > -1)
                            {
                                strFormat = strFormat.Replace("NN", "mm");
                            }
                            while (strFormat.IndexOf("SS") > -1)
                            {
                                strFormat = strFormat.Replace("SS", "ss");
                            }
                            while (strFormat.IndexOf("am/pm") > -1)
                            {
                                strFormat = strFormat.Replace("am/pm", "tt");
                            }
                            while (strFormat.IndexOf("jjj") > -1)
                            {
                                strFormat = strFormat.Replace("jjj", (value.DayOfYear).ToString());
                            }

                            if (strFormat.StartsWith("m/"))
                            {
                                strFormat = "M/" + strFormat.Substring(2);
                            }

                            if (strFormat.StartsWith("D/"))
                            {
                                strFormat = "d/" + strFormat.Substring(2);
                            }

                            try
                            {
                                strRetVal = value.ToString(strFormat);
                            }
                            catch (Exception e)
                            {
                                // WI 131640 : Additional exception logging.
                                cCommonLogLib.LogErrorLTA(e, LTA.Common.LTAMessageImportance.Essential);
                                strRetVal = strFormat;
                            }
                        }
                        else {
                            if (value.TimeOfDay == new TimeSpan(0, 0, 0))
                            {
                                strRetVal = value.ToString("M/d/yyyyHHmmfffff");
                            }
                            else {
                                strRetVal = value.ToString("M/d/yyyy h:mm:ss tt");
                            }
                        }

                        break;
                }
            }


            return (strRetVal);
        }

        /// <summary>
        /// This function is really intended to return a decimal value formatted based on the
        /// specified format string.  This one gets a bit tricky with negative values though.
        /// For negative values left padded with an integer (hopefully a zero), the negative sign
        /// should be at the far left as the first character.  Padding with any other character
        /// should return the negative sign immediately preceeding the first integer in the string.
        /// Note that overpunching negative values overrides this behavior.
        /// 
        /// Some examples:
        /// 
        /// Value	ColumnWidth	UseQuotes	Justification	PadChar	Formatted Value	
        /// 123.45	10	FALSE	Right	0	"0000123.45"	
        /// 123.45	10	FALSE	Right	9	"9999123.45"	Left-Padding with integers (other than zero) makes no sense.
        /// 123.45	10	FALSE	Right	SP	"    123.45"	
        /// 123.45	10	FALSE	Right	X	"XXXX123.45"	
        /// 123.45	10	FALSE	Right	~	"~~~~123.45"	
        /// 123.45	10	TRUE	Right	0	""00123.45""	
        /// 123.45	10	TRUE	Right	9	""99123.45""	
        /// 123.45	10	TRUE	Right	SP	""  123.45""	
        /// 123.45	10	TRUE	Right	X	""XX123.45""	
        /// 123.45	10	TRUE	Right	~	""~~123.45""	
        /// 123.45	10	FALSE	Left	0	"123.450000"	
        /// 123.45	10	FALSE	Left	9	"123.459999"	
        /// 123.45	10	FALSE	Left	SP	"123.45    "	
        /// 123.45	10	FALSE	Left	X	"123.45XXXX"	
        /// 123.45	10	FALSE	Left	~	"123.45~~~~"	
        /// 123.45	10	TRUE	Left	0	""123.4500""	
        /// 123.45	10	TRUE	Left	9	""123.4599""	
        /// 123.45	10	TRUE	Left	SP	""123.45  ""	
        /// 123.45	10	TRUE	Left	X	""123.45XX""	
        /// 123.45	10	TRUE	Left	~	""123.45~~""	
        /// 123.45	0	TRUE	n/a	n/a	""123.45""	
        /// 123.45	0	FALSE	n/a	n/a	"123.45"	
        /// -123.45	10	FALSE	Right	0	"-000123.45"	
        /// -123.45	10	FALSE	Right	9	"-999123.45"	
        /// -123.45	10	FALSE	Right	SP	"   -123.45"	
        /// -123.45	10	FALSE	Right	X	"XXX-123.45"	
        /// -123.45	10	FALSE	Right	~	"~~~-123.45"	
        /// -123.45	10	TRUE	Right	0	""-0123.45""	
        /// -123.45	10	TRUE	Right	9	""-9123.45""	
        /// -123.45	10	TRUE	Right	SP	"" -123.45""	
        /// -123.45	10	TRUE	Right	X	""X-123.45""	
        /// -123.45	10	TRUE	Right	~	""~-123.45""	
        /// -123.45	10	FALSE	Left	0	"-123.45000"	
        /// -123.45	10	FALSE	Left	9	"-123.45999"	
        /// -123.45	10	FALSE	Left	SP	"-123.45   "	
        /// -123.45	10	FALSE	Left	X	"-123.45XXX"	
        /// -123.45	10	FALSE	Left	~	"-123.45~~~"	
        /// -123.45	10	TRUE	Left	0	""-123.450""	
        /// -123.45	10	TRUE	Left	9	""-123.459""	
        /// -123.45	10	TRUE	Left	SP	""-123.45 ""	
        /// -123.45	10	TRUE	Left	X	""-123.45X""	
        /// -123.45	10	TRUE	Left	~	""-123.45~""	
        /// -123.45	0	TRUE	n/a	n/a	""-123.45""	
        /// -123.45	0	FALSE	n/a	n/a	"-123.45"	
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Format"></param>
        /// <param name="OverPunchPos"></param>
        /// <param name="OverPunchNeg"></param>
        /// <param name="ColumnWidth"></param>
        /// <param name="PadChar"></param>
        /// <param name="Justification"></param>
        /// <param name="UseQuotes"></param>
        /// <returns></returns>
        public static string FormatValue(
            Decimal value,
            string format,
            bool overPunchPos,
            bool overPunchNeg,
            int columnWidth,
            char padChar,
            JustifyEnum justification,
            bool useQuotes)
        {

            Decimal decTemp = value;
            string strRetVal = string.Empty;
            int iPos;
            string strMultiplier = string.Empty;
            char chrReplacement;

            string strBareFormat;
            string strFormatPrefix;
            string strFormatSuffix;

            const string VALID_NUMERIC_FORMAT_CHARS = "0123456789#,.";
            const string SPECIAL_CHARS = "{}*";

            if (format.Contains("%r"))
                strRetVal = CustomFormat.FormatRegex(value.ToString(), format);
            else
            {
                iPos = format.IndexOf('*');
                if (iPos > -1)
                {
                    for (int i = iPos + 1; i < format.Length; ++i)
                    {
                        if ((char)format[i] >= 48 && (char)format[i] <= 57)
                        {
                            strMultiplier += format[i];
                        }
                        else {
                            break;
                        }
                    }
                    if (strMultiplier.Length > 0)
                    {
                        decTemp *= int.Parse(strMultiplier);
                    }
                }

                if (value < 0)
                {
                    decTemp *= -1;
                }

                //JMC CR 26521 01/29/2009
                //    -In addition to removing the '{' and '}' characters
                //     from the string, we need to save any leading or trailing
                //     characters that are not part of the numeric format.
                //     These characters need to be saved and applied to the final
                //     value after it has been formatted by a valid numeric
                //     format string.
                //
                //     Overpunch characters and Multiplier operators (*) 
                //     are not valid leading or trailing characters
                //     since they denote special behaviour.

                strBareFormat = string.Empty;
                strFormatPrefix = string.Empty;
                strFormatSuffix = string.Empty;

                if (format.Length > 0)
                {
                    for (int i = 0; i < format.Length; ++i)
                    {
                        if (VALID_NUMERIC_FORMAT_CHARS.IndexOf(format[i]) > -1)
                        {
                            strBareFormat += format[i];
                        }
                        else {
                            if (SPECIAL_CHARS.IndexOf(format[i]) == -1)
                            {
                                if (strBareFormat.Length > 0)
                                {
                                    strFormatSuffix += format[i];
                                }
                                else {
                                    strFormatPrefix += format[i];
                                }
                            }
                        }
                    }
                }

                if (strBareFormat.EndsWith("0.00") && strBareFormat.StartsWith("#,##"))
                {
                    strRetVal = decTemp.ToString("#,##0.00");
                }
                else if (strBareFormat.EndsWith(".00") && strBareFormat.StartsWith("#,##"))
                {
                    strRetVal = decTemp.ToString("#,###.00");
                }
                else if (strBareFormat.EndsWith("0.00"))
                {
                    strRetVal = decTemp.ToString("0.00");
                }
                else if (strBareFormat.EndsWith(".00"))
                {
                    strRetVal = decTemp.ToString(".00");
                }
                else if (strBareFormat.StartsWith("#,##0"))
                {
                    strRetVal = decTemp.ToString("#,##0");
                }
                else if (strBareFormat.StartsWith("#,###"))
                {
                    strRetVal = decTemp.ToString("#,###");
                }
                else if (strBareFormat == "0")
                {
                    strRetVal = Math.Round(decTemp).ToString("0");
                }
                else {
                    strRetVal = decTemp.ToString("0.##");
                }

                if (overPunchNeg && value < 0)
                {
                    switch (strRetVal[strRetVal.Length - 1])
                    {
                        case '0': chrReplacement = '}'; break;
                        case '1': chrReplacement = 'J'; break;
                        case '2': chrReplacement = 'K'; break;
                        case '3': chrReplacement = 'L'; break;
                        case '4': chrReplacement = 'M'; break;
                        case '5': chrReplacement = 'N'; break;
                        case '6': chrReplacement = 'O'; break;
                        case '7': chrReplacement = 'P'; break;
                        case '8': chrReplacement = 'Q'; break;
                        default /*'9'*/: chrReplacement = 'R'; break;
                    }
                    strRetVal = strRetVal.Substring(0, strRetVal.Length - 1);
                    strRetVal += chrReplacement;
                }
                else if (overPunchPos && value >= 0)
                {
                    switch (strRetVal[strRetVal.Length - 1])
                    {
                        case '0': chrReplacement = '{'; break;
                        case '1': chrReplacement = 'A'; break;
                        case '2': chrReplacement = 'B'; break;
                        case '3': chrReplacement = 'C'; break;
                        case '4': chrReplacement = 'D'; break;
                        case '5': chrReplacement = 'E'; break;
                        case '6': chrReplacement = 'F'; break;
                        case '7': chrReplacement = 'G'; break;
                        case '8': chrReplacement = 'H'; break;
                        default /*'9'*/: chrReplacement = 'I'; break;
                    }
                    strRetVal = strRetVal.Substring(0, strRetVal.Length - 1);
                    strRetVal += chrReplacement;
                }
                else if (!overPunchNeg && value < 0)
                {

                    // If the Padding Character is numeric [0-9]
                    if (justification == JustifyEnum.Right && padChar >= 48 && padChar <= 57)
                    {
                        // -2 for the "\"\"" characters, and -1 for the "-" character we are wanting
                        // to add for a total of -3.
                        if (useQuotes && strRetVal.Length - 3 - strFormatPrefix.Length - strFormatSuffix.Length < columnWidth)
                        {
                            strRetVal = "-" + strRetVal.PadLeft(columnWidth - 3 - strFormatPrefix.Length - strFormatSuffix.Length, padChar);
                        }
                        else if (strRetVal.Length - 1 - strFormatPrefix.Length - strFormatSuffix.Length < columnWidth)
                        {
                            strRetVal = "-" + strRetVal.PadLeft(columnWidth - 1 - strFormatPrefix.Length - strFormatSuffix.Length, padChar);
                        }
                        else {
                            strRetVal = "-" + strRetVal;
                        }
                    }
                    else {
                        strRetVal = "-" + strRetVal;
                    }
                }
                strRetVal = strFormatPrefix.TrimStart() + strRetVal + strFormatSuffix.TrimEnd();
            }

            return (strRetVal);
        }

        public static string FormatRegex(
            string value,
            string format)
        {
            if (!format.Contains("%r"))
                return value;

            // Means we're going to allow regular expressions replacement.
            var split = format.Split(new string[] { "%r" }, StringSplitOptions.None);
            var pattern = split[0];
            var replace = split[1];
            return string.IsNullOrWhiteSpace(pattern)
                ? value
                : System.Text.RegularExpressions.Regex.Replace(value, pattern, replace);
        }

        public static string FormatValue(string value, string format)
        {
            if (format.IndexOf("%s") > -1)
            {
                return (format.Replace("%s", value));
            }
            else
            {
                return (value);
            }
        }

        internal static string FormatImageFileName(
            IImage image,
            string format,
            string processingDateFormat,
            bool zeroPadNumbers,
            bool imageFileBatchTypeFormatFull)
        {

            string strRetVal;

            strRetVal = format;

            if (zeroPadNumbers)
            {
                strRetVal = strRetVal.Replace("<BK>", image.Transaction.Batch.BankID.ToString().PadLeft(10, '0'));
                strRetVal = strRetVal.Replace("<LB>", image.Transaction.Batch.ClientAccountID.ToString().PadLeft(10, '0'));
                strRetVal = strRetVal.Replace("<BH>", image.Transaction.Batch.SourceBatchID.ToString().PadLeft(10, '0'));

                strRetVal = strRetVal.Replace("<PD>", FormatValue(image.Transaction.Batch.ProcessingDate, processingDateFormat));   //"MMDDYY"));
                strRetVal = strRetVal.Replace("<TR>", image.Transaction.TransactionID.ToString().PadLeft(10, '0'));
                strRetVal = strRetVal.Replace("<DT>", image.FileDescriptor);
                strRetVal = strRetVal.Replace("<FC>", image.ImageCounter.ToString().PadLeft(10, '0'));
            }
            else
            {
                strRetVal = strRetVal.Replace("<BK>", image.Transaction.Batch.BankID.ToString());
                strRetVal = strRetVal.Replace("<LB>", image.Transaction.Batch.ClientAccountID.ToString());
                strRetVal = strRetVal.Replace("<BH>", image.Transaction.Batch.SourceBatchID.ToString());

                strRetVal = strRetVal.Replace("<PD>", FormatValue(image.Transaction.Batch.ProcessingDate, processingDateFormat));    //"MMDDYY"));
                strRetVal = strRetVal.Replace("<TR>", image.Transaction.TransactionID.ToString());
                strRetVal = strRetVal.Replace("<DT>", image.FileDescriptor);
                strRetVal = strRetVal.Replace("<FC>", image.ImageCounter.ToString());
            }

            return (strRetVal);
        }

        internal static string FormatImageFileName(
            IImage image,
            string format,
            string processingDateFormat,
            bool zeroPadNumbers,
            bool imageFileBatchTypeFormatFull,
            int page)
        {

            string strRetVal;

            strRetVal = format;

            if (zeroPadNumbers)
            {
                strRetVal = strRetVal.Replace("<BK>", image.Transaction.Batch.BankID.ToString().PadLeft(10, '0'));
                strRetVal = strRetVal.Replace("<LB>", image.Transaction.Batch.ClientAccountID.ToString().PadLeft(10, '0'));
                strRetVal = strRetVal.Replace("<BH>", image.Transaction.Batch.SourceBatchID.ToString().PadLeft(10, '0'));

                strRetVal = strRetVal.Replace("<PD>", FormatValue(image.Transaction.Batch.ProcessingDate, processingDateFormat));   //"MMDDYY"));
                strRetVal = strRetVal.Replace("<TR>", image.Transaction.TransactionID.ToString().PadLeft(10, '0'));
                strRetVal = strRetVal.Replace("<DT>", image.FileDescriptor);
                strRetVal = strRetVal.Replace("<FC>", (page == 1 ? (image.ImageCounter + 1).ToString().PadLeft(10, '0') : image.ImageCounter.ToString().PadLeft(10, '0')));
            }
            else {
                strRetVal = strRetVal.Replace("<BK>", image.Transaction.Batch.BankID.ToString());
                strRetVal = strRetVal.Replace("<LB>", image.Transaction.Batch.ClientAccountID.ToString());
                strRetVal = strRetVal.Replace("<BH>", image.Transaction.Batch.SourceBatchID.ToString());

                strRetVal = strRetVal.Replace("<PD>", FormatValue(image.Transaction.Batch.ProcessingDate, processingDateFormat));    //"MMDDYY"));
                strRetVal = strRetVal.Replace("<TR>", image.Transaction.TransactionID.ToString());
                strRetVal = strRetVal.Replace("<DT>", image.FileDescriptor);
                strRetVal = strRetVal.Replace("<FC>", (page == 1 ? (image.ImageCounter + 1).ToString() : image.ImageCounter.ToString()));
            }

            strRetVal += (page == 1 ? "B" : "F");

            return (strRetVal);
        }

        internal static bool ParseFileTokens(
            string filePath,
            cExtractParms parms,
            cStandardFields standardFields,
            out string formattedFilePath)
        {

            string StaticPath;
            string RemPath;
            string Cookie;
            int iPos;
            string strRetVal;

            try
            {

                StaticPath = string.Empty;
                RemPath = filePath;

                while (RemPath.IndexOf('@') > -1)
                {

                    iPos = RemPath.IndexOf('@');
                    StaticPath += RemPath.Substring(0, iPos);

                    if (RemPath.IndexOf(']') > -1)
                    {
                        Cookie = RemPath.Substring(iPos + 1, RemPath.IndexOf(']') - iPos);
                    }
                    else {
                        Cookie = RemPath.Substring(iPos + 1, RemPath.Length);
                    }

                    StaticPath += GetCookieValue(Cookie, parms, standardFields);

                    if (RemPath.Length > Cookie.Length)
                    {
                        RemPath = RemPath.Substring(iPos + 1 + Cookie.Length);
                    }
                    else {
                        RemPath = string.Empty;
                    }
                }
            }
            catch (Exception e)
            {
                // WI 131640 : Additional exception logging.
                cCommonLogLib.LogErrorLTA(e, LTA.Common.LTAMessageImportance.Essential);
                formattedFilePath = string.Empty;
                return (false);
            }

            strRetVal = StaticPath + RemPath;

            formattedFilePath = strRetVal;

            return (true);
        }

        private static string GetCookieValue(
            string cookie,
            cExtractParms parms,
            cStandardFields standardFields)
        {

            string Exp;
            string Format;
            int Index;

            string strRetVal = string.Empty;

            try
            {

                if (cookie.IndexOf("[") > -1)
                {

                    Index = cookie.IndexOf("[");

                    if (cookie.IndexOf("]") > -1)
                    {
                        Format = cookie.Substring(Index + 1, cookie.IndexOf("]") - Index - 1);
                    }
                    else {
                        Format = cookie.Substring(Index + 1, cookie.Length);
                    }
                    Exp = cookie.Substring(0, Index);
                }
                else {
                    Exp = cookie;
                    Format = string.Empty;
                }

                if (Exp.ToUpper() == "DATE")
                {
                    if (Format.Length == 0)
                    {
                        strRetVal = CustomFormat.FormatValue(standardFields.ExtractStarted, "mmddyy");
                    }
                    else {
                        strRetVal = CustomFormat.FormatValue(standardFields.ExtractStarted, Format);
                    }
                }
                else if (Exp.ToUpper() == "TIME")
                {
                    if (Format.Length == 0)
                    {
                        strRetVal = CustomFormat.FormatValue(standardFields.ExtractStarted, "hhnnss");
                    }
                    else {
                        strRetVal = CustomFormat.FormatValue(standardFields.ExtractStarted, Format);
                    }
                    // WI 114570 removed for v2.0
                    //} else if(Exp.ToUpper() == "CURRENTPROCESSINGDATE") {
                    //    if(Format.Length == 0) {
                    //        strRetVal = CustomFormat.FormatValue(StandardFields.CurrentProcessingDate, "mmddyy");
                    //    } else {
                    //        strRetVal = CustomFormat.FormatValue(StandardFields.CurrentProcessingDate, Format);
                    //    }
                }
                else if (Exp.ToUpper() == "PROCESSINGRUNDATE")
                {
                    if (Format.Length == 0)
                    {
                        strRetVal = CustomFormat.FormatValue(standardFields.ExtractStarted, "mmddyy");
                    }
                    else {
                        strRetVal = CustomFormat.FormatValue(standardFields.ExtractStarted, Format);
                    }
                }
                else if (Exp.ToUpper() == "BANK")
                {
                    if (parms.BankID.Length > 0)
                    {
                        strRetVal = parms.BankID[0].ToString();
                    }
                    else {
                        return (string.Empty);
                    }
                }
                else if (Exp.ToUpper() == "LOCKBOX")
                {
                    if (parms.LockboxID.Length > 0)
                    {
                        strRetVal = parms.LockboxID[0].ToString();
                    }
                    else {
                        return (string.Empty);
                    }
                }
                else if (Exp.ToUpper() == "BATCH")
                {
                    if (parms.BatchID.Length > 0)
                    {
                        strRetVal = parms.BatchID[0].ToString();
                    }
                    else {
                        return (string.Empty);
                    }
                }
                else if (Exp.ToUpper() == "BATCHPROCDATE")
                {
                    strRetVal = Format.Length == 0
                        ? CustomFormat.FormatValue(parms.ProcessingDateFrom, "mmddyy")
                        : CustomFormat.FormatValue(parms.ProcessingDateFrom, Format);
                    if (parms.ProcessingDateFrom != parms.ProcessingDateTo)
                    {
                        var formattedto = (Format.Length == 0)
                                ? CustomFormat.FormatValue(parms.ProcessingDateTo, "mmddyy")
                                : CustomFormat.FormatValue(parms.ProcessingDateTo, Format);
                        strRetVal += "-" + formattedto;
                    }
                }

            }
            catch (Exception e)
            {
                // WI 131640 : Additional exception logging.
                cCommonLogLib.LogErrorLTA(e, LTA.Common.LTAMessageImportance.Essential);
                strRetVal = string.Empty;
            }

            return strRetVal;
        }
    }
}