﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 129719 DLD 02/24/2014
*   -Initial Version. Was previously hidden as a "private" class within cExtract 
*   -Converting image repository from ipoPics.dll to OLFServices 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    internal class cExtractImageFileDef
    {
        // DLD - WI 129719 - Image processing
        public List<cImageType> ImageArray { get; private set; }
        public string DestFileName { get; private set; }

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        public cExtractImageFileDef(List<cImageType> images, string destFileName)
        {
            ImageArray = images;
            DestFileName = destFileName;
        }

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        public cExtractImageFileDef(cImageType image, string destFileName)
        {
            ImageArray = new List<cImageType> { image };
            DestFileName = destFileName;
        }

    }
}
