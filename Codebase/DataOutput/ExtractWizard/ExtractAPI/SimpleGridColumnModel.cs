﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    public class SimpleGridColumnModel
    {
        public string UILabel { get; set; }
        public string FieldName { get; set; }
        public int BankID { get; set; }
        public string BankName { get; set; }
        public int WorkgroupID { get; set; }
        public string WorkgroupName { get; set; }
        public string PaymentSource { get; set; }
        public bool IsActive { get; set; }
    }
}
