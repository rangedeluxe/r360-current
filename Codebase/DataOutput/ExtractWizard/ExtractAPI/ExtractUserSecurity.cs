﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.Common.Crypto;
using WFS.LTA.Common;
using WFS.RecHub.ExternalLogonService;
using WFS.RecHub.ExternalLogon.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/21/2013
*   -Initial Version 
* WI 106884-106887 DRP 7/09/2013
*   -Added check for "first time login" and added method to change the user's password.
* WI 113508 MLH 09/09/2013
*  - Modified ChangePassword and MustChangePassword to use RecHub.BusinessCommon for loading system preferences during logon process
*    instead of RecHub.Common.  Used explicit NameSpace for changes because I wasn't sure how
*    changing the ordered list of NameSpaces on this file would affect behavior of other classes
* WI 131640 BLR 03/04/2014, DLD 03/05/2014
*  - Added logs for black-boxed calls & additional catch logging.
* WI 132304 BLR 03/10/2014
*  - Removed password func. 
* WI 138812 BLR 07/11/2014
*  - Edited the Logon method in preparation for RAAM. 
* WI 157574 BLR 08/08/2014
*  - Edited the Logon to connect to RAAM through the External Login Service. 
* WI 157573 BLR 08/11/2014
*  - Removed application from the request, this exists on the server side now. 
* WI 138914 BLR 08/20/2014
*  - Removed bypass for RAAM Authentication.  
* WI 167137 BLR 09/25/2014
*  - Added permissions to the External Login Call. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public static class ExtractServiceSecurity {

        public static bool Logon(
            string entity,
            string logonName,
            string password,
            bool requirespermission,
            out int userID,
            out Guid sessionID,
            out string userMessage) 
        {
            var client = new ExternalLogonClient();
            var request = new PermissionLogonRequest()
            {
                Username = logonName,
                Password = password,
                IPAddress = Environment.MachineName,
                Entity = entity,
                Permission = requirespermission
                    ? R360Permissions.Perm_ExtractDesignManager
                    : string.Empty,
                PermissionType = R360Permissions.ActionType.Manage
            };

            var response = client.PermissionExternalLogon(request);

            // If we couldn't log in, we'll kick out early.
            if (response == null || response.Status == StatusCode.FAIL || response.Session == null)
            {
                userID = 0;
                sessionID = Guid.Empty;
                userMessage = "Logon Failed";
                return false;
            }

            cOLFServicesLib.Entity = entity;
            cOLFServicesLib.Username = logonName;
            cOLFServicesLib.Password = password;

            userID = response.UserId;
            sessionID = response.Session;
            userMessage = "Logon Successful";
            return true;
        }
    }
}
