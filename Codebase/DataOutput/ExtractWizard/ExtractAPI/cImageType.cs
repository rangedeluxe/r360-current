﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Derek Davidson
* Date:     02/24/2014
*
* Purpose:  WI 129719. Was previously a class within Support.cs
*           Converting image repository from ipoPics.dll to OLFServices 
* Modification History
* WI 129719 DLD 02/24/2014
*   -Initial Version 
* WI 149794 BLR 06/24/2014  
*   -Added a check for a zero-length byte array. 
******************************************************************************/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    // DLD - WI 129719 - Image processing
    internal class cImageType
    {
        public cImageType(byte[] image, cItemKey itemKey, bool isCheck)
        {
            myImageAsByte = image;
            ItemKey = itemKey;
            IsCheck = isCheck;
        }

        public cItemKey ItemKey { get; private set; }

        public bool IsCheck { get; private set; }

        public byte[] myImageAsByte { get; private set; }

        private Image _image = null;
        public Image myImage
        {
            get
            {
                try
                {
                    if (_image == null)
                    {

                        if (myImageAsByte != null && myImageAsByte.Length > 0)
                        {
                            using (var s = new MemoryStream(myImageAsByte))
                                _image = Image.FromStream(s);
                        }
                    }
                }
                catch {
                    _image = null;
                }
                
                return _image;
            }
        }
    }
}