using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   - Initial Version 
* WI 134896 BLR 04/14/2014
*   - Added SiteCodeID, we need this for an EventLog call. 
* WI 269406 JMC 03/10/2016
*   - Added AnyWorkgroupElementWasExtracted method to determine whether any elements 
*     at this level or below were actually written to the Extract Data File.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cClientAccount : ObjectBase, IComparable, ISummaryFields {
        
        private cBank _Bank;
        private int _BankId;
        private int _ClientAccountID;
        // WI 134896 : Added SiteCodeID for an EventLog.
        private int _SiteCodeID;
        private Dictionary<string, object> _LockboxFields;
        private OrderedDictionary _Batches;

        private IOrderByColumn[] _OrderByColumns = { };
        private string _WorkgroupName;

        public cClientAccount(
            cBank bank,
            int bankId,
            int clientAccountID,
            int siteCodeId,
            string workgroupName,
            IOrderByColumn[] orderByColumns) {

            _Bank = bank;
            _BankId = bankId;
            _ClientAccountID = clientAccountID;
            _SiteCodeID = siteCodeId;
            _WorkgroupName = workgroupName;

            _OrderByColumns = orderByColumns;

            _LockboxFields = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            _Batches = new OrderedDictionary();
        }

        public bool AnyWorkgroupElementWasExtracted {
            get {
                return (this.WasExtracted || this.Batches.Values.Cast<cBatch>().Any(x => x.AnyWorkgroupElementWasExtracted));
            }
        }

        public int SiteCodeID
        {
            get
            {
                return _SiteCodeID;
            }
        }

        public int BankID {
            get {
                return(_BankId);
            }
        }

        public cBank Bank
        {
            get
            {
                return (_Bank);
            }
        }

        public int ClientAccountID {
            get {
                return (_ClientAccountID);
            }
        }

        public string WorkgroupName {
            get { return _WorkgroupName; }
        }

        public Dictionary<string, object> LockboxFields{
            get {
                return(_LockboxFields);
            }
        }

        public OrderedDictionary Batches {
            get {
                return(_Batches);
            }
            set {
                _Batches = value;
            }
        }

        public int BatchCount { 
            get {
                return(this.Batches.Count);
            } 
        }

        public int TransactionCount { 
            get {

                int intTxnCount = 0;

                foreach(cBatch batch in this.Batches.Values) {
                    intTxnCount += batch.TransactionCount;
                }

                return(intTxnCount);
            } 
        }

        public int CheckCount { 
            get {

                int intCheckCount = 0;

                foreach(cBatch batch in this.Batches.Values) {
                    intCheckCount += batch.CheckCount;
                }

                return(intCheckCount);
            } 
        }

        public int StubCount { 
            get {
                int intStubCount = 0;

                foreach(cBatch batch in this.Batches.Values) {
                    intStubCount += batch.StubCount;
                }

                return(intStubCount);
            } 
        }

        public int DocumentCount { 
            get {

                int intDocCount = 0;

                foreach(cBatch batch in this.Batches.Values) {
                    intDocCount += batch.DocumentCount;
                }

                return(intDocCount);
            } 
        }



        public Decimal SumPaymentsAmount {
            get {
                Decimal decRetVal = 0;

                foreach(cBatch batch in this.Batches.Values) {
                    decRetVal += batch.SumPaymentsAmount;
                }

                return(decRetVal);
            }
        }
        
        public Decimal SumStubsAmount {
            get {
                Decimal decRetVal = 0;

                foreach(cBatch batch in this.Batches.Values) {
                    decRetVal += batch.SumStubsAmount;
                }

                return(decRetVal);
            }
        }

        public static Hashtable DefaultFields() {

            Hashtable htFieldNames = new Hashtable();

            htFieldNames.Add("BankID", string.Empty);
            htFieldNames.Add("CustomerID", string.Empty);
            htFieldNames.Add("LockboxID", string.Empty);

            return(htFieldNames);
        }

        public int CompareTo(object obj) {

            int intRetVal = 0;

            if(obj is cClientAccount) {

                cClientAccount lockbox = (cClientAccount) obj;

                foreach(IOrderByColumn orderbycolumn in _OrderByColumns) {
                    
                    switch(orderbycolumn.ColumnName.ToLower()) {
                        case "bankid":
                            if(this.BankID != lockbox.BankID) {
                                intRetVal = this.BankID.CompareTo(lockbox.BankID);
                            }
                            break;
                        case "lockboxid":
                            if(this.ClientAccountID != lockbox.ClientAccountID) {
                                intRetVal = this.ClientAccountID.CompareTo(lockbox.ClientAccountID);
                            }
                            break;
                        default:
                            foreach(string fieldname in this.LockboxFields.Keys) {
                                if(orderbycolumn.ColumnName.ToLower() == fieldname.ToLower()) {
                                    intRetVal = Compare(this.LockboxFields, lockbox.LockboxFields, fieldname);

                                    if(intRetVal != 0) {
                                        break;
                                    }
                                }
                            }
                            break;
                    }

                    if(intRetVal != 0) {
                        if(orderbycolumn.OrderByDir == OrderByDirEnum.Descending) {
                            intRetVal*=-1;
                        }
                        break;
                    }
                }

            } else {
                throw new ArgumentException("object is not a Lockbox");    
            }

            return(intRetVal);
        }
    }
}
