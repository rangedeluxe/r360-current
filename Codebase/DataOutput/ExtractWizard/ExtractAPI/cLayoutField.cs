using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;

using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   - Initial Version 
* WI 114148 MLH 09/18/2013
*   - Test Extract Button fails on column definition factBatchSummary.SumPaymentsAmount
* WI 114570 MLH 09/20/2013
*   - Removed CurrentProcessingDate from Standard Fields (for v2.0)
* 115810 02/06/2014
*   - Changed case "sumchecksamount" to "sumpaymentsamount" and corrected
*     the variable usage on sumstubsamount.
* WI 129986 BLR 02/24/2014
*   - Edited SQL to allow parent hierarchy fields. Found some bugs
*     in this file in the process.  (Incorrect table names)   
* WI 129719 DLD 02/24/2014
*   - Converting image repository from ipoPics.dll to OLFServices 
* WI 132009 BLR 03/06/2014   
*   - Added in func for joined tables on batch summaries. 
* WI 135766 BLR 04/11/2014
*   - Updated constants 'Customer' and 'Lockbox' to 'Organization' and 'Workgroup'.
* WI 130821 BLR 05/14/2014
*   - Trimmed the value before formatting to ignore whitespace characters. 
* WI 142972 BLR 05/22/2014
*   - Added DimDDAs to the list of formatting tables for Payments.  
* WI 146631 BLR 06/10/2014
*   - Constant string of 'LineCounterCheck' to 'LineCounterPayment', 
*     throughout file.
* WI 115658 BLR 06/24/2014
*   - Added functionality for formatting of DE Fields in the payment
*     and stub formatting functions. 
* WI 153593 BLR 07/15/2014
*   - Added DepositDateKey and SourceProcessingDateKey to an exception
*     to the key filter.   Removed some default formatting.
* WI 153574 BLR 08/19/2014
*   -Joined Transaction Exception Status. 
* WI 269406 JMC 03/10/2016
*   - JD only - Update the local "WasExtracted" bit when a Layout element has been written
*     to the Extract Data File.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{

    public class cLayoutField : ILayoutField
    {

        private cLayout _Layout;
        private string _Format = string.Empty;

        private string _FieldName = string.Empty;

        private int _DataType = -1;
        private string _DisplayName = string.Empty;
        private int _ColumnWidth = 0;
        private char _PadChar = ' ';
        private JustifyEnum _Justification = JustifyEnum.Left;
        private bool _Quotes = false;
        private string _ManualInputValue = string.Empty;

        internal event outputErrorEventHandler OutputError;

        public string LayoutName
        {
            get
            {
                if (_Layout != null)
                {
                    return (_Layout.LayoutName);
                }
                else {
                    return (string.Empty);
                }
            }
        }

        public bool IsDataEntry { get; set; }

        public int DataType
        {
            get
            {
                return (_DataType);
            }
            set
            {
                _DataType = value;
            }
        }

        public string DisplayName
        {
            get
            {
                return (_DisplayName);
            }
            set
            {
                _DisplayName = value;
            }
        }

        public int ColumnWidth
        {
            get
            {
                return (_ColumnWidth);
            }
            set
            {
                _ColumnWidth = value;
            }
        }

        public char PadChar
        {
            get
            {
                return (_PadChar);
            }
            set
            {
                _PadChar = value;
            }
        }

        public JustifyEnum Justification
        {
            get
            {
                return (_Justification);
            }
            set
            {
                _Justification = value;
            }
        }

        public string JustificationDesc
        {
            get
            {
                switch (this.Justification)
                {
                    case JustifyEnum.Right:
                        return ("Right");
                    default:
                        return ("Left");
                }
            }
        }

        public bool Quotes
        {
            get
            {
                return (_Quotes);
            }
            set
            {
                _Quotes = value;
            }
        }

        public bool UseOccursGroups
        {

            get
            {

                bool bolUseOccursGroup;

                if (string.Compare(this.SimpleTableName, "RecHubData.factchecks", true) == 0)
                {
                    bolUseOccursGroup = _Layout.UsePaymentsOccursGroups;
                }
                else if (string.Compare(this.SimpleTableName, "RecHubData.factstubs", true) == 0)
                {
                    bolUseOccursGroup = _Layout.UseStubsOccursGroups;
                }
                else if (string.Compare(this.SimpleTableName, "RecHubData.factdocuments", true) == 0)
                {
                    bolUseOccursGroup = _Layout.UseDocumentsOccursGroups;
                }
                else {
                    bolUseOccursGroup = false;
                }

                return (bolUseOccursGroup);
            }
        }

        public int OccursCount
        {

            get
            {

                int intOccursCount;

                if (string.Compare(this.SimpleTableName, "RecHubData.factchecks", true) == 0)
                {
                    intOccursCount = _Layout.PaymentsOccursCount;
                }
                else if (string.Compare(this.SimpleTableName, "RecHubData.factstubs", true) == 0)
                {
                    intOccursCount = _Layout.StubsOccursCount;
                }
                else if (string.Compare(this.SimpleTableName, "RecHubData.factdocuments", true) == 0)
                {
                    intOccursCount = _Layout.DocumentsOccursCount;
                }
                else {
                    intOccursCount = -1;
                }

                return (intOccursCount);
            }
        }

        public string ManualInputValue
        {
            get
            {
                return (_ManualInputValue);
            }
            set
            {
                _ManualInputValue = value;
            }
        }

        public cLayoutField(
            cLayout layout,
            string fieldName)
        {

            _Layout = layout;
            _FieldName = fieldName;
            UniqueFieldName = fieldName;
            switch (fieldName.ToLower())
            {

                // Counts
                case "countbatches":
                    InitProperties("Count(Batch)", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "counttransactions":
                    InitProperties("Count(Transactions)", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "countpayments":
                    InitProperties("Count(Checks)", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "countdocuments":
                    InitProperties("Count(Documents)", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "countstubs":
                    InitProperties("Count(Stubs)", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 10, '0', JustifyEnum.Right, string.Empty);
                    break;

                // Record Counts
                case "recordcountfile":
                    InitProperties("FileRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcountbank":
                    InitProperties("BankRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcountworkgroup":
                    InitProperties("LockboxRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcountbatch":
                    InitProperties("BatchRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcounttran":
                    InitProperties("TranRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcountpayment":
                    InitProperties("CheckRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcountstub":
                    InitProperties("StubRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcountdoc":
                    InitProperties("DocRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcountdetail":
                    InitProperties("DetailRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "recordcounttotal":
                    InitProperties("TotalRecords", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 15, '0', JustifyEnum.Right, string.Empty);
                    break;

                // Aggregates
                // TODO Verify that sumchecksamount is depricated in 2.0 and is now sumpaymentsamount (MLH 09/18/2013)
                case "sumpaymentsamount":
                    InitProperties("Sum(Checks.Amount)", (int)LegacySupport.GetTFieldType(SqlDbType.Money), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "sumchecksdebillingkeys":
                    InitProperties("Sum(Checks.DEBillingKeys)", (int)LegacySupport.GetTFieldType(SqlDbType.Money), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "sumchecksdedatakeys":
                    InitProperties("Sum(Checks.DEDataKeys)", (int)LegacySupport.GetTFieldType(SqlDbType.Money), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "sumstubsamount":
                    InitProperties("Sum(Stubs.Amount)", (int)LegacySupport.GetTFieldType(SqlDbType.Money), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "sumstubsdebillingkeys":
                    InitProperties("Sum(Stubs.DEBillingKeys)", (int)LegacySupport.GetTFieldType(SqlDbType.Money), 10, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "sumstubsdedatakeys":
                    InitProperties("Sum(Stubs.DEDataKeys)", (int)LegacySupport.GetTFieldType(SqlDbType.Money), 10, '0', JustifyEnum.Right, string.Empty);
                    break;

                // Standard Fields
                case "counter":
                    InitProperties("Counter", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 4, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "currentprocessingdate":
                    InitProperties("CurrentProcessingDate", (int)LegacySupport.GetTFieldType(SqlDbType.DateTime), 8, '0', JustifyEnum.Left, "mm/dd/yy");
                    break;
                case "date":
                    InitProperties("Date", (int)LegacySupport.GetTFieldType(SqlDbType.DateTime), 8, '0', JustifyEnum.Left, "mm/dd/yy");
                    break;
                case "filler":
                    InitProperties("Filler", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 10, ' ', JustifyEnum.Left, string.Empty);
                    break;
                case "firstlast":
                    InitProperties("FirstLast", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 10, ' ', JustifyEnum.Left, "First/Mid/Last");
                    break;
                case "processingrundate":
                    InitProperties("ProcessingRunDate", (int)LegacySupport.GetTFieldType(SqlDbType.DateTime), 8, '0', JustifyEnum.Left, "mm/dd/yy");
                    break;
                case "static":
                    InitProperties("Static", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 10, ' ', JustifyEnum.Left, string.Empty);
                    break;
                case "time":
                    InitProperties("Time", (int)LegacySupport.GetTFieldType(SqlDbType.DateTime), 10, '0', JustifyEnum.Left, "hh:nn:ss");
                    break;

                // Image fields
                case "imagepaymentperbatch":
                case "imagecheckperbatch":
                    InitProperties("ImagePaymentPerBatch", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;
                case "imagepaymentpertransaction":
                case "imagecheckpertransaction":
                    InitProperties("ImagePaymentPerTransaction", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;
                case "imagepaymentsinglefront":
                case "imagechecksinglefront":
                    InitProperties("ImagePaymentSingleFront", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;
                case "imagepaymentsinglerear":
                case "imagechecksinglerear":
                    InitProperties("ImagePaymentSingleRear", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;

                // deprecated
                case "imagefilesizefrontc":
                    InitProperties("ImageFileSizeFrontC", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 32, '0', JustifyEnum.Right, string.Empty);
                    break;

                // deprecated
                case "imagefilesizerearc":
                    InitProperties("ImageFileSizeRearC", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 32, '0', JustifyEnum.Right, string.Empty);
                    break;

                // deprecated
                case "imagefilename":
                    InitProperties("ImageFileName", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 255, ' ', JustifyEnum.Left, string.Empty);
                    break;

                // deprecated
                case "imagefilesizefronts":
                    InitProperties("ImageFileSizeFrontS", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 32, '0', JustifyEnum.Right, string.Empty);
                    break;

                // deprecated
                case "imagefilesizerears":
                    InitProperties("ImageFileSizeRearS", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 32, '0', JustifyEnum.Right, string.Empty);
                    break;

                // deprecated
                case "imagestubsinglefront":
                    InitProperties("ImageStubSingleFront", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;

                // deprecated
                case "imagestubsinglerear":
                    InitProperties("ImageStubSingleRear", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;

                case "imagedocumentperbatch":
                    InitProperties("ImageDocumentPerBatch", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;
                case "imagedocumentpertransaction":
                    InitProperties("ImageDocumentPerTransaction", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;
                case "imagedocumentsinglefront":
                    InitProperties("ImageDocumentSingleFront", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;
                case "imagedocumentsinglerear":
                    InitProperties("ImageDocumentSingleRear", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 60, ' ', JustifyEnum.Left, string.Empty);
                    break;

                // deprecated
                case "imagedocumentfilesizefrontd":
                    InitProperties("ImageFileSizeFrontD", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 32, '0', JustifyEnum.Right, string.Empty);
                    break;

                // deprecated
                case "imagedocumentfilesizereard":
                    InitProperties("ImageFileSizeRearD", (int)LegacySupport.GetTFieldType(SqlDbType.VarChar), 32, '0', JustifyEnum.Right, string.Empty);
                    break;


                case "linecounterfile":
                    InitProperties("LineCounterFile", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 5, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "linecounterbank":
                    InitProperties("LineCounterBank", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 5, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "linecounterworkgroup":
                    InitProperties("LineCounterWorkgroup", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 5, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "linecounterbatch":
                    InitProperties("LineCounterBatch", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 5, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "linecountertransaction":
                    InitProperties("LineCounterTransaction", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 5, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "linecounterpayment":
                    InitProperties("LineCounterPayment", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 5, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "linecounterstub":
                    InitProperties("LineCounterStub", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 5, '0', JustifyEnum.Right, string.Empty);
                    break;
                case "linecounterdocument":
                    InitProperties("LineCounterDocument", (int)LegacySupport.GetTFieldType(SqlDbType.Int), 5, '0', JustifyEnum.Right, string.Empty);
                    break;
                default:
                    break;
            }
        }

        public cLayoutField(
            cLayout layout,
            string fieldName,
            int dataType,
            int columnWidth)
        {

            TFieldType enmFieldType;

            _Layout = layout;
            _FieldName = fieldName;
            UniqueFieldName = fieldName;

            this.DisplayName = fieldName;
            this.ColumnWidth = columnWidth;
            this.DataType = (int)dataType;
            this.Quotes = false;

            if (LegacySupport.TryParse(dataType, out enmFieldType))
            {
                switch (enmFieldType)
                {
                    case TFieldType.INT16:
                        this.Justification = JustifyEnum.Right;
                        break;
                    case TFieldType.INT32:
                        this.Justification = JustifyEnum.Right;
                        break;
                    case TFieldType.FLOAT:
                        this.Justification = JustifyEnum.Right;
                        break;
                    case TFieldType.UINT16:
                        this.Justification = JustifyEnum.Right;
                        break;
                    case TFieldType.UINT32:
                        this.Justification = JustifyEnum.Right;
                        break;
                    case TFieldType.INT64:
                        this.Justification = JustifyEnum.Right;
                        break;
                    case TFieldType.UINT64:
                        this.Justification = JustifyEnum.Right;
                        break;
                    case TFieldType.MONEY:
                        this.Justification = JustifyEnum.Right;
                        break;
                    default:
                        this.Justification = JustifyEnum.Left;
                        break;
                }
            }
            else {
                this.Justification = JustifyEnum.Left;
            }

            this.Format = string.Empty;
            this.PadChar = '0';
        }

        private void InitProperties(
            string displayName,
            int dataType,
            int columnWidth,
            char padChar,
            JustifyEnum justification,
            string format)
        {

            this.DisplayName = displayName;
            this.DataType = dataType;
            this.ColumnWidth = columnWidth;
            this.Justification = justification;
            this.Format = format;
            this.PadChar = padChar;
        }

        public string FieldName
        {
            get
            {
                return (_FieldName);
            }
        }

        public string SimpleFieldName
        {
            get
            {
                string strFieldName;
                int iPos;

                iPos = this.FieldName.LastIndexOf('.');
                if (iPos > -1)
                {
                    strFieldName = this.FieldName.Substring(iPos + 1);
                }
                else {
                    strFieldName = this.FieldName;
                }

                return (strFieldName);
            }
        }

        public string SimpleTableName
        {
            get
            {
                string strTableName;
                int iPos;

                iPos = this.FieldName.LastIndexOf('.');
                if (iPos > -1)
                {
                    strTableName = this.FieldName.Substring(0, iPos);
                }
                else {
                    strTableName = string.Empty;
                }

                switch (strTableName.ToLower())
                {
                    case "checksdataentry":
                        return ("RecHubData.factChecks");
                    case "stubsdataentry":
                        return ("RecHubData.factStubs");
                    case "documentsdataentry":
                        return ("RecHubData.factDocuments");
                    default:
                        return (strTableName);
                }
            }
        }

        public string Format
        {
            get
            {
                if (_Format.Trim().Length == 0)
                {
                    if (this.IsAggregate && this.FieldName.ToLower().StartsWith("sum"))
                    {
                        return ("0.00");
                    }
                    else if (string.Compare(this.FieldName.Trim(), "currentprocessingdate", true) == 0)
                    {
                        return ("MM/dd/yy");
                    }
                    else if (string.Compare(this.FieldName.Trim(), "processingrundate", true) == 0)
                    {
                        return ("MM/dd/yy");
                        //} else if(this.FieldName.ToLower().Trim() == "processingdate") {
                        //    return("M/d/yyyy");
                    }
                    else {
                        return (_Format);
                    }
                }
                else {
                    return (_Format);
                }
            }
            set
            {
                _Format = value;
            }
        }

        private bool OverPunchPos
        {
            get
            {
                return (this.Format.IndexOf('{') > -1);
            }
        }

        private bool OverPunchNeg
        {
            get
            {
                return (this.Format.IndexOf('}') > -1);
            }
        }

        public string UniqueFieldName {
            get;
            set;
        }

        #region "Is" Properties
        public bool IsAggregate
        {

            get
            {

                return (
                    this.IsBatchAggregate ||
                    this.IsTransactionsAggregate ||
                    this.IsChecksAggregate ||
                    this.IsStubsAggregate ||
                    this.IsDocumentsAggregate);
            }
        }

        internal bool IsBatchAggregate
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "countbatches":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        internal bool IsTransactionsAggregate
        {
            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "counttransactions":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        internal bool IsChecksAggregate
        {

            get
            {
                // TODO Verify that sumchecksamount is depricated in 2.0 and is now sumpaymentsamount (MLH 09/18/2013)
                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "countpayments":
                        break;
                    case "sumchecksamount":
                        break;
                    case "sumpaymentsamount":
                        break;
                    case "sumchecksdebillingkeys":
                        break;
                    case "sumchecksdedatakeys":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        internal bool IsStubsAggregate
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "countstubs":
                        break;
                    case "sumstubsamount":
                        break;
                    case "sumstubsdebillingkeys":
                        break;
                    case "sumstubsdedatakeys":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        internal bool IsDocumentsAggregate
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "countdocuments":
                        break;
                    case "batchtotaldocscount":
                        //TODO: VERIFY THAT THIS THE RIGHT VALUE!!!
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsStatic
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "static":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsFiller
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "filler":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsStandard
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    // WI 114570 removed for v2.0
                    //case "currentprocessingdate":
                    //    break;
                    case "date":
                        break;
                    case "processingrundate":
                        break;
                    case "time":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsImageField
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "imagepaymentperbatch":
                    case "imagecheckperbatch":
                        break;
                    case "imagepaymentpertransaction":
                    case "imagecheckpertransaction":
                        break;
                    case "imagepaymentsinglefront":
                    case "imagechecksinglefront":
                        break;
                    case "imagepaymentsinglerear":
                    case "imagechecksinglerear":
                        break;
                    case "imagedocumentperbatch":
                        break;
                    case "imagedocumentpertransaction":
                        break;
                    case "imagedocumentsinglefront":
                        break;
                    case "imagedocumentsinglerear":
                        break;

                    // deprecated
                    case "imagefilesizefrontc":
                        break;

                    // deprecated
                    case "imagefilesizerearc":
                        break;

                    // deprecated
                    case "imagefilesizefrontd":
                        break;

                    // deprecated
                    case "imagefilesizereard":
                        break;

                    // deprecated
                    case "imagefilename":
                        break;

                    // deprecated
                    case "imagefilesizefronts":
                        break;

                    // deprecated
                    case "imagefilesizerears":
                        break;

                    // deprecated
                    case "imagestubsinglefront":
                        break;

                    // deprecated
                    case "imagestubsinglerear":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsFirstLast
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "firstlast":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsCounter
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "counter":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsRecordCounter
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "recordcountbank":
                        break;
                    case "recordcountbatch":
                        break;
                    case "recordcountbatchdetail":
                        break;
                    case "RecordCountPayment":
                        break;
                    case "recordcountdetail":
                        break;
                    case "recordcountdoc":
                        break;
                    case "recordcountfile":
                        break;
                    case "recordcountworkgroup":
                        break;
                    case "recordcountstub":
                        break;
                    case "recordcounttotal":
                        break;
                    case "recordcounttran":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsLineCounter
        {

            get
            {

                bool bolRetVal = true;

                switch (this.FieldName.ToLower())
                {
                    case "linecounterbank":
                        break;
                    case "linecounterbatch":
                        break;
                    case "linecounterpayment":
                        break;
                    case "linecounterdocument":
                        break;
                    case "linecounterfile":
                        break;
                    case "linecounterworkgroup":
                        break;
                    case "linecountertransaction":
                        break;
                    case "linecounterstub":
                        break;
                    default:
                        bolRetVal = false;
                        break;
                }

                return (bolRetVal);
            }
        }

        public bool IsPredefField
        {

            get
            {

                return (
                    this.IsAggregate ||
                    this.IsStatic ||
                    this.IsFiller ||
                    this.IsStandard ||
                    this.IsImageField ||
                    this.IsFirstLast ||
                    this.IsCounter ||
                    this.IsLineCounter ||
                    this.IsRecordCounter);
            }
        }

        internal bool IsManualInput
        {
            get
            {
                return (this.Format.StartsWith("@") && (!this.Format.StartsWith("@@")));
            }
        }

        public int SiteBankID { get; set; }

        public int SiteClientAccountID { get; set; }

        public string PaymentSource { get; set; }

        #endregion "Is" Properties

        public string FormatStaticField()
        {

            string strRetVal = string.Empty;
            string strFormat;

            switch (this.SimpleFieldName.ToLower())
            {
                case "static":
                    if (this.IsManualInput)
                    {
                        strFormat = this.ManualInputValue;
                    }
                    else if (this.Format.Trim().StartsWith("@") && this.Format.Trim().Length > 1)
                    {
                        strFormat = this.Format.Trim().Substring(1);
                    }
                    else {
                        strFormat = this.Format.Trim();
                    }
                    strRetVal = this.FormatGridField(strFormat, this.PadChar, (_Layout.ExtractDef.UseQuotes || this.Quotes), false);
                    break;
            }

            return (strRetVal);
        }

        public string FormatFiller()
        {

            string strRetVal = string.Empty;

            switch (this.SimpleFieldName.ToLower())
            {
                case "filler":
                    strRetVal = this.FormatGridField(string.Empty, this.PadChar, (_Layout.ExtractDef.UseQuotes || this.Quotes), false);
                    break;
            }

            return (strRetVal);
        }

        public string FormatAggregateField(ISummaryFields summaryFields)
        {

            string strRetVal = string.Empty;

            switch (this.FieldName.ToLower())
            {
                case "countbatches":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(summaryFields.BatchCount, this.Format));
                    break;
                case "counttransactions":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(summaryFields.TransactionCount, this.Format));
                    break;
                case "countpayments":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(summaryFields.CheckCount, this.Format));
                    break;
                case "countstubs":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(summaryFields.StubCount, this.Format));
                    break;
                case "countdocuments":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(summaryFields.DocumentCount, this.Format));
                    break;
                // WI 115810 : "sumchecksamount" was used, changed to expected "sumpaymentsamount".
                case "sumpaymentsamount":
                    strRetVal = this.FormatGridField(summaryFields.SumPaymentsAmount);
                    break;
                ////case "sumchecksdebillingkeys":
                ////    strRetVal = this.FormatGridField(CustomFormat.FormatValue(SummaryFields.SumPaymentsDEBillingKeys, this.Format));
                ////    break;
                ////case "sumchecksdedatakeys":
                ////    strRetVal = this.FormatGridField(CustomFormat.FormatValue(SummaryFields.SumPaymentsDEDataKeys, this.Format));
                ////    break;
                case "sumstubsamount":
                    // WI 115810 : summaryFields.SumPaymentsAmount was used, changed to SumStubsAmount.
                    strRetVal = this.FormatGridField(summaryFields.SumStubsAmount);
                    break;
                ////case "sumstubsdebillingkeys":
                ////    strRetVal = this.FormatGridField(CustomFormat.FormatValue(SummaryFields.SumStubsDEBillingKeys, this.Format));
                ////    break;
                ////case "sumstubsdedatakeys":
                ////    strRetVal = this.FormatGridField(CustomFormat.FormatValue(SummaryFields.SumStubsDEDataKeys, this.Format));
                ////    break;
                case "batchtotaldocscount":
                    //TODO: VERIFY THAT THIS THE RIGHT VALUE!!!
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(summaryFields.DocumentCount, this.Format));
                    break;
                default:
                    break;
            }

            return (strRetVal);
        }

        internal string FormatStandardField(
            cExtractParms parms,
            cStandardFields standardFields)
        {

            string strRetVal = string.Empty;

            switch (this.FieldName.ToLower())
            {
                // WI 114570 removed for v2.0
                //case "currentprocessingdate":
                //    strRetVal = this.FormatGridField(StandardFields.CurrentProcessingDate);
                //    break;
                case "date":
                    strRetVal = this.FormatGridField(standardFields.ExtractStarted);
                    break;
                case "processingrundate":
                    strRetVal = this.FormatGridField(parms.ProcessingDateFrom);
                    break;
                case "time":
                    strRetVal = this.FormatGridField(standardFields.ExtractStarted);
                    break;
                default:
                    strRetVal = this.FormatGridField("?????????????????????????????????????");
                    break;
            }

            return (strRetVal);
        }

        internal string FormatFirstLastField(FirstMidLast value)
        {
            return (FormatGridField(value));
        }

        private string FormatEmbeddedImageField()
        {
            return (FormatGridField("<ISIZTAG>" + this.ColumnWidth.ToString("000")));
        }

        internal string FormatCounter(int index)
        {
            string strRetVal = string.Empty;

            switch (this.SimpleFieldName.ToLower())
            {
                case "counter":
                    strRetVal = this.FormatGridField((++index));
                    break;
                default:
                    strRetVal = this.FormatGridField("?????????????????????????????????????");
                    break;
            }

            return (strRetVal);
        }

        internal string FormatLineCounter(cCounters counters)
        {
            string strRetVal = string.Empty;

            switch (this.SimpleFieldName.ToLower())
            {
                case "linecounterbank":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.BankLines, this.Format));
                    break;
                case "linecounterbatch":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.BatchLines, this.Format));
                    break;
                case "linecounterpayment":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.CheckLines, this.Format));
                    break;
                case "linecounterdocument":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.DocumentLines, this.Format));
                    break;
                case "linecounterfile":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.FileLines, this.Format));
                    break;
                case "linecounterworkgroup":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.LockboxLines, this.Format));
                    break;
                case "linecountertransaction":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TransactionLines, this.Format));
                    break;
                case "linecounterstub":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.StubLines, this.Format));
                    break;
                default:
                    strRetVal = this.FormatGridField("?????????????????????????????????????");
                    break;
            }

            return (strRetVal);
        }

        internal string FormatRecordCounter(cCounters counters)
        {
            string strRetVal = string.Empty;

            switch (this.SimpleFieldName.ToLower())
            {
                case "recordcountbank":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalBankRecords, this.Format));
                    break;
                case "recordcountbatch":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalBatchRecords, this.Format));
                    break;
                case "recordcountpayment":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalCheckRecords, this.Format));
                    break;
                case "recordcountdetail":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalJointDetailRecords, this.Format));
                    break;
                case "recordcountdoc":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalDocumentRecords, this.Format));
                    break;
                case "recordcountfile":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalFileRecords, this.Format));
                    break;
                case "recordcountworkgroup":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalLockboxRecords, this.Format));
                    break;
                case "recordcountstub":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalStubRecords, this.Format));
                    break;
                case "recordcounttotal":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalRecords, this.Format));
                    break;
                case "recordcounttran":
                    strRetVal = this.FormatGridField(CustomFormat.FormatValue(counters.TotalTransactionRecords, this.Format));
                    break;
                default:
                    strRetVal = this.FormatGridField("?????????????????????????????????????");
                    break;
            }

            return (strRetVal);
        }

        internal string FormatValue(string value)
        {
            return (FormatGridField(value));
        }

        internal string FormatLayoutField(
            cExtract extractBase,
            cBank bank)
        {

            string strRetVal = string.Empty;
            IColumn column;

            switch (this.SimpleFieldName.ToLower())
            {
                case "sitebankid":
                    strRetVal = this.FormatGridField(bank.BankID);
                    break;
                default:
                    if (string.Compare(this.SimpleTableName, "RecHubData.dimbanks", true) == 0 ||
                       this.SimpleTableName.Trim().Length == 0)
                    {

                        if (GetColumnDef(extractBase.BankTableDef, out column))
                        {
                            strRetVal = this.FormatGridField(bank.BankFields, column);
                        }
                        else {
                            throw (new ColumnDefNotAvailable("RecHubData.dimBanks", this.SimpleFieldName));
                        }
                    }
                    else {
                        // Don't throw an error if a table that cannot be accessed at this
                        // level has been specified.  -- Compatibility with old version.
                        return (FormatGridField(string.Empty));
                    }
                    break;
            }

            return (strRetVal);
        }

        internal string FormatLayoutField(
            cExtract extractBase,
            cClientAccount lockbox)
        {

            string strRetVal = string.Empty;
            IColumn column;

            switch (this.SimpleFieldName.ToLower())
            {
                case "sitebankid":
                    strRetVal = this.FormatGridField(lockbox.BankID);
                    break;
                case "siteclientaccountid":
                    strRetVal = this.FormatGridField(lockbox.ClientAccountID);
                    break;
                default:
                    switch (this.SimpleTableName.ToLower())
                    {
                        case "rechubdata.dimbanks":
                            strRetVal = FormatLayoutField(extractBase, lockbox.Bank);
                            break;
                        default:
                            if (string.Compare(this.SimpleTableName, "RecHubData.dimClientAccounts") == 0 ||
                               this.SimpleTableName.Trim().Length == 0)
                            {

                                if (GetColumnDef(extractBase.ClientAccountTableDef, out column))
                                {
                                    strRetVal = this.FormatGridField(lockbox.LockboxFields, column);
                                }
                                else {
                                    throw (new ColumnDefNotAvailable("RecHubData.dimClientAccounts", this.SimpleFieldName));
                                }
                            }
                            else {
                                // Don't throw an error if a table that cannot be accessed at this
                                // level has been specified.  -- Compatibility with old version.
                                return (FormatGridField(string.Empty));
                            }
                            break;
                    }
                    break;
            }

            return (strRetVal);
        }

        internal string FormatLayoutField(
            cExtract extractBase,
            cBatch batch)
        {

            string strRetVal = string.Empty;
            IColumn column;

            switch (this.SimpleFieldName.ToLower())
            {
                case "sitebankid":
                    strRetVal = this.FormatGridField(batch.BankID);
                    break;
                case "siteclientaccountid":
                    strRetVal = this.FormatGridField(batch.ClientAccountID);
                    break;
                case "immutabledatekey":
                    strRetVal = this.FormatGridField(batch.ProcessingDate);
                    break;
                case "batchid":
                    strRetVal = this.FormatGridField(batch.BatchID);
                    break;
                default:
                    switch (this.SimpleTableName.ToLower())
                    {
                        case "rechubdata.dimbanks":
                            strRetVal = FormatLayoutField(extractBase, batch.Lockbox.Bank);
                            break;
                        case "rechubdata.dimclientaccounts":
                            strRetVal = FormatLayoutField(extractBase, batch.Lockbox);
                            break;
                        default:
                            // WI 132009 : Added formatting for these 2 new fields.
                            if (string.Compare(this.SimpleTableName, "RecHubData.factbatchsummary", true) == 0
                                || string.Compare(this.SimpleTableName, "RecHubData.dimBatchPaymentTypes", true) == 0
                                || string.Compare(this.SimpleTableName, "RecHubData.dimBatchSources", true) == 0
                                || string.Compare(this.SimpleTableName, "RecHubData.dimBatchExceptionStatuses", true) == 0
                                || this.SimpleTableName.Trim().Length == 0)
                            {

                                if (GetColumnDef(extractBase.BatchTableDef, out column))
                                {
                                    strRetVal = this.FormatGridField(batch.BatchFields, column);
                                }
                                else {
                                    throw (new ColumnDefNotAvailable("RecHubData.factBatchSummary", this.SimpleFieldName));
                                }
                            }
                            else
                            {
                                // Don't throw an error if a table that cannot be accessed at this
                                // level has been specified.  -- Compatibility with old version.
                                return (FormatGridField(string.Empty));
                            }
                            break;
                    }
                    break;
            }

            return (strRetVal);
        }

        internal string FormatLayoutField(
            cExtract extractBase,
            cTransaction transaction)
        {

            string strRetVal = string.Empty;
            IColumn column;

            switch (this.SimpleFieldName.ToLower())
            {
                case "immutabledatekey":
                    strRetVal = this.FormatGridField(transaction.Batch.ProcessingDate);
                    break;
                case "batchid":
                    strRetVal = this.FormatGridField(transaction.Batch.BatchID);
                    break;
                case "transactionid":
                    strRetVal = this.FormatGridField(transaction.TransactionID);
                    break;
                default:
                    switch (this.SimpleTableName.ToLower())
                    {
                        case "rechubdata.dimbanks":
                            strRetVal = FormatLayoutField(extractBase, transaction.Batch.Lockbox.Bank);
                            break;
                        case "rechubdata.dimclientaccounts":
                            strRetVal = FormatLayoutField(extractBase, transaction.Batch.Lockbox);
                            break;

                        case "rechubdata.dimbatchsources":
                        case "rechubdata.dimbatchpaymenttypes":
                        case "rechubdata.factbatchsummary":
                        case "rechubdata.dimbatchexceptionstatuses":
                            strRetVal = FormatLayoutField(extractBase, transaction.Batch);
                            break;
                        default:
                            if (string.Compare(this.SimpleTableName.ToLower(), "RecHubData.facttransactionsummary", true) == 0
                                || string.Compare(this.SimpleTableName, "RecHubData.dimtransactionexceptionstatuses", true) == 0
                                || this.SimpleTableName.Trim().Length == 0)
                            {

                                if (GetColumnDef(extractBase.TransactionsTableDef, out column))
                                {
                                    strRetVal = this.FormatGridField(transaction.TransactionFields, column);
                                }
                                else {
                                    throw (new ColumnDefNotAvailable("RecHubData.factTransactionSummary", this.SimpleFieldName));
                                }
                            }
                            else {
                                // Don't throw an error if a table that cannot be accessed at this
                                // level has been specified.  -- Compatibility with old version.
                                return (FormatGridField(string.Empty));
                            }
                            break;
                    }
                    break;
            }

            return (strRetVal);
        }

        public string FormatLayoutField(
            cExtract extractBase,
            cPayment check,
            cImageFormatOptions imageFormatOptions)
        {

            string strRetVal = string.Empty;
            IColumn column;
            // DLD - TODO - WI 129719 - 1 Code Trace
            switch (this.SimpleFieldName.ToLower())
            {
                case "immutabledatekey":
                    strRetVal = this.FormatGridField(check.Transaction.Batch.ProcessingDate);
                    break;
                case "batchid":
                    strRetVal = this.FormatGridField(check.Transaction.Batch.BatchID);
                    break;
                case "transactionid":
                    strRetVal = this.FormatGridField(check.TransactionID);
                    break;
                case "batchsequence":
                    strRetVal = this.FormatGridField(check.BatchSequence);
                    break;
                case "amount":
                    strRetVal = this.FormatGridField(check.Amount);
                    break;
                case "account":
                    strRetVal = this.FormatGridField(check.Account);
                    break;
                case "routingnumber":
                    strRetVal = this.FormatGridField(check.RoutingNumber);
                    break;
                case "numericroutingnumber":
                    strRetVal = this.FormatGridField(check.NumericRoutingNumber);
                    break;
                case "imagepaymentperbatch":
                case "imagecheckperbatch":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        // DLD - TODO - Image Processing
                        if (check.FrontImage != null) // if(check.ImageFPath.Length > 0 && File.Exists(check.ImageFPath)) {
                        {
                            check.Transaction.Batch.FormattedImageName = CustomFormat.FormatImageFileName(
                                check,
                                imageFormatOptions.ImageFileNamePerBatch,
                                imageFormatOptions.ImageFileProcDateFormat,
                                imageFormatOptions.ImageFileZeroPad,
                                imageFormatOptions.ImageFileBatchTypeFormatFull);

                            strRetVal = FormatGridField(
                                imageFormatOptions.ImageFileFormat,
                                imageFormatOptions.IncludeImageFolderPath,
                                imageFormatOptions.QualifiedImagePath,
                                check.Transaction.Batch.FormattedImageName);

                            check.Transaction.Batch.ExtractBatchChecks = true;
                        }
                        else {
                            strRetVal = FormatGridField(string.Empty);
                        }
                    }
                    break;
                case "imagepaymentpertransaction":
                case "imagecheckpertransaction":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        // DLD - TODO - WI 129719 - Image processing
                        if (check.FrontImage != null) // if (check.ImageFPath.Length > 0 && File.Exists(check.ImageFPath))
                        {

                            check.Transaction.FormattedImageName = CustomFormat.FormatImageFileName(
                                check,
                                imageFormatOptions.ImageFileNamePerTran,
                                imageFormatOptions.ImageFileProcDateFormat,
                                imageFormatOptions.ImageFileZeroPad,
                                imageFormatOptions.ImageFileBatchTypeFormatFull);//);

                            strRetVal = FormatGridField(
                                imageFormatOptions.ImageFileFormat,
                                imageFormatOptions.IncludeImageFolderPath,
                                imageFormatOptions.QualifiedImagePath,
                                check.Transaction.FormattedImageName);

                            check.Transaction.ExtractTransactionChecks = true;
                        }
                        else {
                            strRetVal = FormatGridField(string.Empty);
                        }
                    }
                    break;
                case "imagepaymentsinglefront":
                case "imagechecksinglefront":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        // DLD - TODO - WI 129719 - Image processing
                        if (check.FrontImage != null) // if (check.ImageFPath.Length > 0 && File.Exists(check.ImageFPath))
                        {
                            check.FormattedImageFName = CustomFormat.FormatImageFileName(
                                check,
                                imageFormatOptions.ImageFileNameSingle,
                                imageFormatOptions.ImageFileProcDateFormat,
                                imageFormatOptions.ImageFileZeroPad,
                                imageFormatOptions.ImageFileBatchTypeFormatFull,
                                0);

                            strRetVal = FormatGridField(
                                imageFormatOptions.ImageFileFormat,
                                imageFormatOptions.IncludeImageFolderPath,
                                imageFormatOptions.QualifiedImagePath,
                                check.FormattedImageFName);

                            check.ExtractImageFront = true;
                        }
                        else {
                            strRetVal = FormatGridField(string.Empty);
                        }
                    }
                    break;
                case "imagepaymentsinglerear":
                case "imagechecksinglerear":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        // DLD - TODO - WI 129719 - Image processing
                        if (check.BackImage != null) // if (check.ImageBPath.Length > 0 && File.Exists(check.ImageBPath))
                        {

                            check.FormattedImageBName = CustomFormat.FormatImageFileName(
                                check,
                                imageFormatOptions.ImageFileNameSingle,
                                imageFormatOptions.ImageFileProcDateFormat,
                                imageFormatOptions.ImageFileZeroPad,
                                imageFormatOptions.ImageFileBatchTypeFormatFull,
                                1);

                            strRetVal = FormatGridField(
                                imageFormatOptions.ImageFileFormat,
                                imageFormatOptions.IncludeImageFolderPath,
                                imageFormatOptions.QualifiedImagePath,
                                check.FormattedImageBName);

                            check.ExtractImageBack = true;
                        }
                        else {
                            strRetVal = FormatGridField(string.Empty);
                        }
                    }
                    break;

                // deprecated
                case "imagefilesizefrontc":
                    strRetVal = FormatEmbeddedImageField();
                    break;

                // deprecated
                case "imagefilesizerearc":
                    strRetVal = FormatEmbeddedImageField();
                    break;

                default:
                    switch (this.SimpleTableName.ToLower())
                    {
                        case "rechubdata.dimbanks":
                            strRetVal = FormatLayoutField(extractBase, check.Transaction.Batch.Lockbox.Bank);
                            break;
                        case "rechubdata.dimclientaccounts":
                            strRetVal = FormatLayoutField(extractBase, check.Transaction.Batch.Lockbox);
                            break;
                        case "rechubdata.factbatchsummary":
                        case "rechubdata.dimbatchpaymenttypes":
                        case "rechubdata.dimbatchsources":
                        case "rechubdata.dimbatchexceptionstatuses":
                            strRetVal = FormatLayoutField(extractBase, check.Transaction.Batch);
                            break;
                        case "rechubdata.facttransactionsummary":
                        case "rechubdata.facttransactionexceptionstatuses":
                            strRetVal = FormatLayoutField(extractBase, check.Transaction);
                            break;
                        default:
                            // WI 142972 : Added dimDDA joined table.
                            if (string.Compare(this.SimpleTableName.ToLower(), "RecHubData.factchecks", true) == 0 ||
                               string.Compare(this.SimpleTableName.ToLower(), "RecHubData.ChecksDataEntry", true) == 0 ||
                               string.Compare(this.SimpleTableName.ToLower(), "RecHubData.dimDDAs", true) == 0 ||
                               this.SimpleTableName.Trim().Length == 0)
                            {

                                if (this.SimpleTableName.StartsWith("RecHubData.ChecksDataEntry"))
                                {
                                    var field = check.CheckFields.ContainsKey(this.UniqueFieldName)
                                        ? check.CheckFields[this.UniqueFieldName]
                                        : null;

                                    if (this.DataType == 24)
                                        strRetVal = field != null
                                            ? this.FormatGridField((DateTime)field)
                                            : this.FormatGridField(string.Empty);
                                    else if (this.DataType == 7)
                                        strRetVal = field != null
                                            ? this.FormatGridField((decimal)field)
                                            : this.FormatGridField(default(decimal));
                                    else if (this.DataType == 6)
                                        strRetVal = field != null
                                            ? this.FormatGridField((float)field)
                                            : this.FormatGridField(default(float));
                                    else
                                        strRetVal = field != null
                                            ? this.FormatGridField(field.ToString())
                                            : this.FormatGridField(string.Empty);

                                }
                                else if (GetColumnDef(extractBase.PaymentsTableDef, out column))
                                {
                                    strRetVal = this.FormatGridField(check.CheckFields, column);
                                }
                                else
                                {
                                    throw (new ColumnDefNotAvailable("RecHubData.factChecks", this.SimpleFieldName));
                                }
                            }
                            else
                            {
                                // Don't throw an error if a table that cannot be accessed at this
                                // level has been specified.  -- Compatibility with old version.
                                return (FormatGridField(string.Empty));
                            }
                            break;
                    }
                    break;
            }

            return (strRetVal);
        }

        public string FormatLayoutField(
            cExtract extractBase,
            cStub stub,
            cImageFormatOptions imageFormatOptions)
        {

            string strRetVal = string.Empty;
            IColumn column;

            switch (this.SimpleFieldName.ToLower())
            {
                case "immutabledatekey":
                    strRetVal = this.FormatGridField(stub.Transaction.Batch.ProcessingDate);
                    break;
                case "batchid":
                    strRetVal = this.FormatGridField(stub.Transaction.Batch.BatchID);
                    break;
                case "transactionid":
                    strRetVal = this.FormatGridField(stub.TransactionID);
                    break;
                case "batchsequence":
                    strRetVal = this.FormatGridField(stub.BatchSequence);
                    break;
                case "amount":
                    strRetVal = this.FormatGridField(stub.Amount);
                    break;
                case "imagefilename":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        strRetVal = this.FormatGridField(string.Empty);
                    }
                    break;
                case "imagefilesizefronts":
                    strRetVal = FormatEmbeddedImageField();
                    break;
                case "imagefilesizerears":
                    strRetVal = FormatEmbeddedImageField();
                    break;

                // deprecated
                case "imagestubsinglefront":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        strRetVal = this.FormatGridField(string.Empty);
                    }
                    break;
                case "imagestubsinglerear":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        strRetVal = this.FormatGridField(string.Empty);
                    }
                    break;
                default:
                    switch (this.SimpleTableName.ToLower())
                    {
                        case "rechubdata.dimbanks":
                            strRetVal = FormatLayoutField(extractBase, stub.Transaction.Batch.Lockbox.Bank);
                            break;
                        case "rechubdata.dimclientaccounts":
                            strRetVal = FormatLayoutField(extractBase, stub.Transaction.Batch.Lockbox);
                            break;
                        case "rechubdata.factbatchsummary":
                        case "rechubdata.dimbatchsources":
                        case "rechubdata.dimbatchpaymenttypes":
                        case "rechubdata.dimbatchexceptionstatuses":
                            strRetVal = FormatLayoutField(extractBase, stub.Transaction.Batch);
                            break;
                        case "rechubdata.facttransactionsummary":
                        case "rechubdata.dimtransactionexceptionstatuses":
                            strRetVal = FormatLayoutField(extractBase, stub.Transaction);
                            break;
                        default:
                            if (string.Compare(this.SimpleTableName, "RecHubData.factStubs", true) == 0 ||
                               string.Compare(this.SimpleTableName, "RecHubData.StubsDataEntry", true) == 0 ||
                               this.SimpleTableName.Trim().Length == 0)
                            {

                                if (this.SimpleTableName.StartsWith("RecHubData.StubsDataEntry"))
                                {
                                    var field = stub.StubFields.ContainsKey(this.UniqueFieldName)
                                        ? stub.StubFields[this.UniqueFieldName]
                                        : null;

                                    if (this.DataType == 24)
                                        strRetVal = field != null
                                            ? this.FormatGridField((DateTime)field)
                                            : this.FormatGridField(string.Empty);
                                    else if (this.DataType == 7)
                                        strRetVal = field != null
                                            ? this.FormatGridField((decimal)field)
                                            : this.FormatGridField(default(decimal));
                                    else if (this.DataType == 6)
                                        strRetVal = field != null
                                            ? this.FormatGridField((float)field)
                                            : this.FormatGridField(default(float));
                                    else
                                        strRetVal = field != null
                                            ? this.FormatGridField(field.ToString())
                                            : this.FormatGridField(string.Empty);
                                }
                                else if (GetColumnDef(extractBase.StubsTableDef, out column))
                                {
                                    strRetVal = this.FormatGridField(stub.StubFields, column);
                                }
                                else
                                {
                                    throw (new ColumnDefNotAvailable("RecHubData.factStubs", this.SimpleFieldName));
                                }
                            }
                            else {
                                // Don't throw an error if a table that cannot be accessed at this
                                // level has been specified.  -- Compatibility with old version.
                                return (FormatGridField(string.Empty));
                            }
                            break;
                    }
                    break;
            }

            return (strRetVal);
        }

        internal string FormatLayoutField(
            cExtract extractBase,
            cDocument document,
            cImageFormatOptions imageFormatOptions)
        {

            string strRetVal = string.Empty;
            IColumn column;

            switch (this.SimpleFieldName.ToLower())
            {
                case "immutabledatekey":
                    strRetVal = this.FormatGridField(document.Transaction.Batch.ProcessingDate);
                    break;
                case "batchid":
                    strRetVal = this.FormatGridField(document.Transaction.Batch.BatchID);
                    break;
                case "transactionid":
                    strRetVal = this.FormatGridField(document.TransactionID);
                    break;
                case "batchsequence":
                    strRetVal = this.FormatGridField(document.BatchSequence);
                    break;
                case "documentsequence":
                    strRetVal = this.FormatGridField(document.DocumentSequence);
                    break;
                case "filedescriptor":
                    strRetVal = this.FormatGridField(document.FileDescriptor);
                    break;
                case "imagedocumentperbatch":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        // DLD - TODO - WI 129719 - Image processing
                        if (document.FrontImage != null) // if (document.ImageFPath.Length > 0 && File.Exists(document.ImageFPath))
                        {

                            document.Transaction.Batch.FormattedImageName = CustomFormat.FormatImageFileName(
                                document,
                                imageFormatOptions.ImageFileNamePerBatch,
                                imageFormatOptions.ImageFileProcDateFormat,
                                imageFormatOptions.ImageFileZeroPad,
                                imageFormatOptions.ImageFileBatchTypeFormatFull);

                            strRetVal = FormatGridField(
                                imageFormatOptions.ImageFileFormat,
                                imageFormatOptions.IncludeImageFolderPath,
                                imageFormatOptions.QualifiedImagePath,
                                document.Transaction.Batch.FormattedImageName);

                            document.Transaction.Batch.ExtractBatchDocuments = true;
                        }
                        else {
                            strRetVal = FormatGridField(string.Empty);
                        }
                    }
                    break;
                case "imagedocumentpertransaction":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        // DLD - TODO - WI 129719 - Image processing
                        if (document.FrontImage != null) // if (document.ImageFPath.Length > 0 && File.Exists(document.ImageFPath))
                        {

                            document.Transaction.FormattedImageName = CustomFormat.FormatImageFileName(
                                document,
                                imageFormatOptions.ImageFileNamePerTran,
                                imageFormatOptions.ImageFileProcDateFormat,
                                imageFormatOptions.ImageFileZeroPad,
                                imageFormatOptions.ImageFileBatchTypeFormatFull);

                            strRetVal = FormatGridField(
                                imageFormatOptions.ImageFileFormat,
                                imageFormatOptions.IncludeImageFolderPath,
                                imageFormatOptions.QualifiedImagePath,
                                document.Transaction.FormattedImageName);

                            document.Transaction.ExtractTransactionDocuments = true;
                        }
                        else {
                            strRetVal = FormatGridField(string.Empty);
                        }
                    }
                    break;
                case "imagedocumentsinglefront":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        // DLD - TODO - WI 129719 - Image processing
                        if (document.FrontImage != null) // if (document.ImageFPath.Length > 0 && File.Exists(document.ImageFPath))
                        {

                            document.FormattedImageFName = CustomFormat.FormatImageFileName(
                                document,
                                imageFormatOptions.ImageFileNameSingle,
                                imageFormatOptions.ImageFileProcDateFormat,
                                imageFormatOptions.ImageFileZeroPad,
                                imageFormatOptions.ImageFileBatchTypeFormatFull,
                                0);

                            strRetVal = FormatGridField(
                                imageFormatOptions.ImageFileFormat,
                                imageFormatOptions.IncludeImageFolderPath,
                                imageFormatOptions.QualifiedImagePath,
                                document.FormattedImageFName);

                            document.ExtractImageFront = true;
                        }
                        else {
                            strRetVal = FormatGridField(string.Empty);
                        }
                    }
                    break;
                case "imagedocumentsinglerear":
                    if (imageFormatOptions.ImageFileFormat == ImageFileFormatEnum.DoNotRetrieveImages)
                    {
                        strRetVal = this.FormatGridField("Non Image Extract");
                    }
                    else {
                        // DLD - TODO - WI 129719 - Image processing
                        if (document.BackImage != null) // if (document.ImageBPath.Length > 0 && File.Exists(document.ImageBPath))
                        {

                            document.FormattedImageBName = CustomFormat.FormatImageFileName(
                                document,
                                imageFormatOptions.ImageFileNameSingle,
                                imageFormatOptions.ImageFileProcDateFormat,
                                imageFormatOptions.ImageFileZeroPad,
                                imageFormatOptions.ImageFileBatchTypeFormatFull,
                                1);

                            strRetVal = FormatGridField(
                                imageFormatOptions.ImageFileFormat,
                                imageFormatOptions.IncludeImageFolderPath,
                                imageFormatOptions.QualifiedImagePath,
                                document.FormattedImageBName);

                            document.ExtractImageBack = true;
                        }
                        else {
                            strRetVal = FormatGridField(string.Empty);
                        }
                    }
                    break;
                case "imagefilesizefrontd":
                    strRetVal = FormatEmbeddedImageField();
                    break;
                case "imagefilesizereard":
                    strRetVal = FormatEmbeddedImageField();
                    break;
                default:
                    switch (this.SimpleTableName.ToLower())
                    {
                        case "rechubdata.dimbanks":
                            strRetVal = FormatLayoutField(extractBase, document.Transaction.Batch.Lockbox.Bank);
                            break;
                        case "rechubdata.dimclientaccounts":
                            strRetVal = FormatLayoutField(extractBase, document.Transaction.Batch.Lockbox);
                            break;
                        case "rechubdata.factbatchsummary":
                        case "rechubdata.dimbatchsources":
                        case "rechubdata.dimbatchpaymenttypes":
                        case "rechubdata.dimbatchexceptionstatuses":
                            strRetVal = FormatLayoutField(extractBase, document.Transaction.Batch);
                            break;
                        case "rechubdata.facttransactionsummary":
                        case "rechubdata.dimtransactionexceptionstatuses":
                            strRetVal = FormatLayoutField(extractBase, document.Transaction);
                            break;
                        default:
                            // WI 132009 : Added document types.
                            if (string.Compare(this.SimpleTableName, "RecHubData.factdocuments", true) == 0
                                || string.Compare(this.SimpleTableName, "RecHubData.dimDocumentTypes", true) == 0
                                || this.SimpleTableName.Trim().Length == 0)
                            {

                                if (GetColumnDef(extractBase.DocumentsTableDef/*, ExtractBase.DocsDETableDef*/, out column))
                                {
                                    strRetVal = this.FormatGridField(document.DocumentFields, column);
                                }
                                else {
                                    throw (new ColumnDefNotAvailable("RecHubData.factDocuments", this.SimpleFieldName));
                                }
                            }
                            else {
                                // Don't throw an error if a table that cannot be accessed at this
                                // level has been specified.  -- Compatibility with old version.
                                return (FormatGridField(string.Empty));
                            }
                            break;
                    }
                    break;
            }

            return (strRetVal);
        }

        internal StringBuilder FormatJointDetailLayoutField(
            cExtract extractBase,
            OrderedDictionary items,
            int itemIndex,
            cImageFormatOptions imageFormatOptions)
        {

            StringBuilder sbRetVal = new StringBuilder();

            if ((items.Count > 0 && itemIndex > -1) &&
               (itemIndex < items.Count || (!this.UseOccursGroups && (_Layout.UsePaymentsOccursGroups ||
                                                                      _Layout.UseStubsOccursGroups ||
                                                                      _Layout.UseDocumentsOccursGroups))))
            {

                if (itemIndex >= items.Count)
                {
                    itemIndex = items.Count - 1;
                }

                if (items[itemIndex] is cPayment)
                {
                    sbRetVal.Append(this.FormatLayoutField(extractBase, (cPayment)items[itemIndex], imageFormatOptions));
                    ((ObjectBase)items[itemIndex]).WasExtracted = true;
                }
                else if (items[itemIndex] is cStub)
                {
                    sbRetVal.Append(this.FormatLayoutField(extractBase, (cStub)items[itemIndex], imageFormatOptions));
                    ((ObjectBase)items[itemIndex]).WasExtracted = true;
                } else if (items[itemIndex] is cDocument)
                {
                    sbRetVal.Append(this.FormatLayoutField(extractBase, (cDocument)items[itemIndex], imageFormatOptions));
                    ((ObjectBase)items[itemIndex]).WasExtracted = true;
                } else {
                    throw (new Exception("Invalid object type."));
                }

            }
            else if (_Layout.ExtractDef.NullAsSpace)
            {
                sbRetVal.Append(this.FormatGridField(string.Empty, ' '));
            }
            else if (_Layout.UseOccursGroups)
            {
                // To mimic the behaviour of the original version, omit quotes and
                // always pad with a space when writing an empty field.
                sbRetVal.Append(this.FormatGridField(string.Empty, ' '));
            }
            else {
                sbRetVal.Append(this.FormatGridField(string.Empty));
            }

            return (sbRetVal);
        }

        private bool GetColumnDef(
            Dictionary<string, IColumn> tableDef,
            out IColumn column)
        {

            bool bolRetVal;

            if (tableDef.ContainsKey(this.SimpleFieldName))
            {
                column = tableDef[this.SimpleFieldName];
                bolRetVal = true;
            }
            else if (tableDef.ContainsKey(this.FieldName))
            {
                column = tableDef[this.FieldName];
                bolRetVal = true;
            }
            else
            {
                column = null;
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        private bool GetColumnDef(
            Dictionary<string, IColumn> tableDef1,
            Dictionary<string, IColumn> tableDef2,
            out IColumn column)
        {

            bool bolRetVal;

            bolRetVal = GetColumnDef(tableDef1, out column);
            if (!bolRetVal)
            {
                bolRetVal = GetColumnDef(tableDef2, out column);
            }

            return (bolRetVal);
        }

        private string FormatGridField(
            ImageFileFormatEnum imageFileFormat,
            bool includeImageFolderPath,
            string baseDestPath,
            string formattedImageName)
        {

            string strRetVal = string.Empty;

            switch (imageFileFormat)
            {
                case ImageFileFormatEnum.Tiff:
                    if (includeImageFolderPath)
                    {
                        strRetVal = FormatGridField(Path.GetFullPath(Path.Combine(baseDestPath, formattedImageName) + ".tif"));
                    }
                    else {
                        strRetVal = FormatGridField(formattedImageName + ".tif");
                    }
                    break;
                case ImageFileFormatEnum.Pdf:
                    if (includeImageFolderPath)
                    {
                        strRetVal = FormatGridField(Path.GetFullPath(Path.Combine(baseDestPath, formattedImageName) + ".pdf"));
                    }
                    else {
                        strRetVal = FormatGridField(formattedImageName + ".pdf");
                    }
                    break;
                default:
                    strRetVal = FormatGridField("Non Image Extract");
                    break;
            }

            return (strRetVal);
        }

        private string FormatGridField(
            Dictionary<string, object> fields,
            IColumn columnDef)
        {

            string strRetVal = string.Empty;

            if (columnDef == null)
            {
                throw (new ColumnDefNotAvailable(columnDef));
            }
            else if (!fields.ContainsKey(columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName))
            {
                strRetVal = FormatGridField(string.Empty);
            }
            else if (fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName] == null)
            {
                strRetVal = FormatGridField(string.Empty);
            }
            else {

                if (fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName].ToString().Length > 0)
                {

                    switch (columnDef.Type)
                    {
                        case SqlDbType.BigInt:
                            strRetVal = FormatGridField((Int64)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        //case SqlDbType.Binary:
                        //    byte[]
                        case SqlDbType.Bit:
                            strRetVal = FormatGridField((bool)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        //case SqlDbType.Char:
                        //    None
                        case SqlDbType.DateTime:
                            strRetVal = FormatGridField((DateTime)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        case SqlDbType.Decimal:
                            strRetVal = FormatGridField((Decimal)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        case SqlDbType.Float:
                            strRetVal = FormatGridField((double)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        //case SqlDbType.Image:
                        //    None
                        case SqlDbType.Int:
                            strRetVal = FormatGridField((int)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        case SqlDbType.Money:
                            strRetVal = FormatGridField((Decimal)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        //case SqlDbType.NChar:
                        //    String, Char[]
                        //case SqlDbType.NText:
                        //    None
                        //case SqlDbType.NVarChar:
                        //    String, Char[]
                        case SqlDbType.Real:
                            strRetVal = FormatGridField((Single)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        case SqlDbType.SmallDateTime:
                            strRetVal = FormatGridField((DateTime)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        case SqlDbType.SmallInt:
                            strRetVal = FormatGridField((Int16)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        case SqlDbType.SmallMoney:
                            strRetVal = FormatGridField((Decimal)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        //case SqlDbType.Text:
                        //    None
                        //case SqlDbType.Timestamp:
                        //    None
                        case SqlDbType.TinyInt:
                            strRetVal = FormatGridField((Byte)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        //case SqlDbType.Udt:
                        //    Same class that is bound to the user-defined type in the same assembly or a dependent assembly.
                        case SqlDbType.UniqueIdentifier:
                            strRetVal = FormatGridField((Guid)fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName]);
                            break;
                        //case SqlDbType.VarBinary:
                        //    byte, Byte[]
                        //case SqlDbType.VarChar:
                        //    None
                        //case SqlDbType.Variant:
                        //    Object 
                        //case SqlDbType.Xml:
                        //    None
                        default:
                            strRetVal = FormatGridField(fields[columnDef.Schema + "." + columnDef.TableName + "." + columnDef.ColumnName].ToString());
                            break;
                    }
                }
                else {
                    strRetVal = FormatGridField(string.Empty);
                }
            }

            return (strRetVal);
        }

        private string FormatGridField(Int16 value)
        {
            return (FormatGridField(value.ToString()));
        }

        private string FormatGridField(Int32 value)
        {
            return (FormatGridField(value.ToString()));
        }

        private string FormatGridField(Int64 value)
        {
            return (FormatGridField(value.ToString()));
        }

        private string FormatGridField(bool value)
        {
            return (FormatGridField((value ? "True" : "False")));
        }

        private string FormatGridField(DateTime value)
        {
            return (FormatGridField(CustomFormat.FormatValue(value, this.Format)));
        }

        private string FormatGridField(Decimal value)
        {

            string strRetVal;

            strRetVal = FormatGridField(CustomFormat.FormatValue(
                value,
                this.Format,
                this.OverPunchPos,
                this.OverPunchNeg,
                this.ColumnWidth,
                this.PadChar,
                this.Justification,
                this.Quotes));

            return (strRetVal);
        }

        private string FormatGridField(Single value)
        {
            return (FormatGridField(value.ToString()));
        }

        private string FormatGridField(Double value)
        {
            return (FormatGridField(value.ToString()));
        }

        private string FormatGridField(Byte value)
        {
            return (FormatGridField(value.ToString()));
        }

        private string FormatGridField(Guid value)
        {
            return (FormatGridField(value.ToString("B").ToUpper()));
        }

        private string FormatGridField(FirstMidLast value)
        {

            string[] arValues = this.Format.Split('/');
            string strRetVal = string.Empty;

            if (arValues.Length > 0)
            {
                switch (value)
                {
                    case FirstMidLast.First:
                        strRetVal = arValues[0];
                        break;
                    case FirstMidLast.Mid:
                        if (arValues.Length > 1)
                        {
                            strRetVal = arValues[1];
                        }
                        else {
                            strRetVal = arValues[0];
                        }
                        break;
                    case FirstMidLast.Last:
                        if (arValues.Length > 2)
                        {
                            strRetVal = arValues[2];
                        }
                        else if (arValues.Length > 1)
                        {
                            strRetVal = arValues[1];
                        }
                        else {
                            strRetVal = arValues[0];
                        }
                        break;
                }
            }

            strRetVal = FormatGridField(
                strRetVal,
                this.PadChar,
                (_Layout.ExtractDef.UseQuotes || this.Quotes),
                true);

            return (strRetVal);
        }

        // WI 130821 : Trimmed the value before formatting to ignore whitespace characters.
        private string FormatGridField(string value)
        {
            value = value.Trim();
            return (FormatGridField(value,
                this.PadChar,
                (_Layout.ExtractDef.UseQuotes || this.Quotes),
                true));
        }

        // WI 130821 : Trimmed the value before formatting to ignore whitespace characters.
        private string FormatGridField(
            string value,
            char padCharOverride)
        {
            value = value.Trim();
            return (FormatGridField(
                value,
                padCharOverride,
                (_Layout.ExtractDef.UseQuotes || this.Quotes),
                true));
        }

        // WI 130821 : Trimmed the value before formatting to ignore whitespace characters.
        public string FormatGridField(
            string value,
            char padCharOverride,
            bool useQuotesOverride,
            bool allowFormatSubstitute)
        {
            value = value.Trim();

            string strRetVal = null;
            char chrPadChar;

            strRetVal = CustomFormat.FormatRegex(value, this.Format);

            // Now, onto the rest of the formatting rules.
            if (this.IsFiller)
            {
                strRetVal = string.Empty;
                for (int i = 0; i < this.ColumnWidth; ++i)
                {
                    strRetVal += this.PadChar;
                }
            }
            else
            {
                if (strRetVal == null || strRetVal.Length == 0)
                {
                    if (_Layout.ExtractDef.NullAsSpace)
                    {
                        strRetVal = " ";
                        chrPadChar = ' ';
                    }
                    else
                    {
                        strRetVal = string.Empty;
                        chrPadChar = padCharOverride;
                    }
                }
                else if (allowFormatSubstitute)
                {
                    strRetVal = CustomFormat.FormatValue(strRetVal, this.Format);
                    chrPadChar = padCharOverride;
                }
                else
                {
                    chrPadChar = padCharOverride;
                }

                if (_Layout.ExtractDef.NoPad)
                {
                    if (useQuotesOverride)
                    {
                        strRetVal = (char)34 + strRetVal + (char)34;
                    }
                }
                else
                {
                    if (this.Justification == JustifyEnum.Left)
                    {
                        strRetVal = strRetVal.PadRight(this.ColumnWidth, chrPadChar);

                        if (this.ColumnWidth > 0)
                        {
                            strRetVal = strRetVal.Substring(0, this.ColumnWidth);
                        }

                        if (useQuotesOverride)
                        {
                            if (this.ColumnWidth > 2)
                            {
                                strRetVal = ((char)34 + strRetVal).Substring(0, this.ColumnWidth - 1) + (char)34;
                            }
                            else if (this.ColumnWidth == 2)
                            {
                                strRetVal = "\"\"";
                            }
                            else if (this.ColumnWidth == 1)
                            {
                                strRetVal = "\"";
                            }
                            else
                            {
                                strRetVal = (char)34 + strRetVal + (char)34;
                            }
                        }
                    }
                    else
                    {
                        strRetVal = strRetVal.PadLeft(this.ColumnWidth, chrPadChar);

                        if (this.ColumnWidth > 0)
                        {
                            strRetVal = strRetVal.Substring(strRetVal.Length - this.ColumnWidth);
                        }

                        if (useQuotesOverride)
                        {
                            if (this.ColumnWidth > 2)
                            {
                                strRetVal = (char)34 + strRetVal.Substring(strRetVal.Length - this.ColumnWidth + 2) + (char)34;
                            }
                            else if (this.ColumnWidth == 2)
                            {
                                strRetVal = "\"\"";
                            }
                            else if (this.ColumnWidth == 1)
                            {
                                strRetVal = "\"";
                            }
                            else
                            {
                                strRetVal = (char)34 + strRetVal + (char)34;
                            }
                        }
                    }
                }
            }

            return (strRetVal);
        }

        private void OnExceptionOccurred(Exception e)
        {
            if (OutputError != null)
            {
                OutputError(e);
            }
        }
    }
}
