using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 143040 BLR 05/22/2014
*   - Removed dependence on the composite cBatchKey, only BatchID is utilized. 
* WI 162549 BLR 09/03/2014
*   - Added immutable date key back in, need this for OLF. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cBatch : ObjectBase, IComparable, ISummaryFields {
    
        private cClientAccount _Lockbox;
        private int _BankID;
        private int _CustomerID;
        private int _ClientAccountID;
        private int _DepositDateKey;
        private int _ImmutableDateKey;
        private long _BatchID;
        private Dictionary<string, object> _BatchFields;
        private OrderedDictionary _Transactions;
        private Hashtable _BatchImageFiles = null;
        private string _FormattedImageName = string.Empty;
        private bool _ExtractBatchChecks = false;
        private bool _ExtractBatchDocuments = false;

        private IOrderByColumn[] _OrderByColumns = { };

        private CultureInfo _EN_US = new CultureInfo("en-US"); 

        public cBatch(
            cClientAccount lockbox, 
            int bankID, 
            int clientAccountID, 
            long batchID,
            int immutabledatekey,
            string batchsource,
            string importtype,
            long sourcebatchid,
            int depositdatekey,
            IOrderByColumn[] orderByColumns) {

            _Lockbox = lockbox;
            _BankID = bankID;
            _ClientAccountID = clientAccountID;
            _ImmutableDateKey = immutabledatekey;
            _BatchID = batchID;
            _DepositDateKey = depositdatekey;
            ImportTypeShortName = importtype;
            SourceBatchID = sourcebatchid;
            BatchSourceShortName = batchsource;

            _OrderByColumns = orderByColumns;

            _BatchFields = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            _Transactions = new OrderedDictionary();
        }

        public bool AnyWorkgroupElementWasExtracted {
            get {
                return(this.WasExtracted || this.Transactions.Values.Cast<cTransaction>().Any(x => x.AnyWorkgroupElementWasExtracted));
            }
        }

        public long SourceBatchID { get; set; }
        public string BatchSourceShortName { get; set; }
        public string ImportTypeShortName { get; set; }

        public bool ExtractBatchChecks {
            get {
                return(_ExtractBatchChecks);
            }
            set {
                _ExtractBatchChecks = value;
            }
        }

        public bool ExtractBatchDocuments {
            get {
                return(_ExtractBatchDocuments);
            }
            set {
                _ExtractBatchDocuments = value;
            }
        }

        public cClientAccount Lockbox {
            get {
                return(_Lockbox);
            }
        }

        public int BankID {
            get {
                return(_BankID);
            }
        }

        public int CustomerID {
            get {
                return(_CustomerID);
            }
        }

        public int ClientAccountID {
            get {
                return(_ClientAccountID);
            }
        }

        public int DepositDateKey {
            get {
                return (_DepositDateKey);
            }
        }

        public DateTime DepositDate {
            get {
                DateTime dteTemp;
                if(_DepositDateKey > 0 && DateTime.TryParseExact(_DepositDateKey.ToString(), "yyyyMMdd", _EN_US, DateTimeStyles.None, out dteTemp)) {
                    return (dteTemp);
                } else {
                    return (DateTime.MinValue);
                }
            }
        }

        public int ImmutableDateKey {
            get {
                return (_ImmutableDateKey);
            }
        }

        public DateTime ProcessingDate {
            get {
                DateTime dteTemp;
                if(_ImmutableDateKey > 0 && DateTime.TryParseExact(_ImmutableDateKey.ToString(), "yyyyMMdd", _EN_US, DateTimeStyles.None, out dteTemp)) {
                    return (dteTemp);
                } else {
                    return (DateTime.MinValue);
                }
            }
        }

        public long BatchID {
            get {
                return(_BatchID);
            }
        }
   
    
        public Dictionary<string, object> BatchFields{
            get {
                return(_BatchFields);
            }
        }
        
        public OrderedDictionary Transactions {
            get {
                return(_Transactions);
            }
            set {
                _Transactions = value;
            }
        }
        
        public int BatchCount { 
            get {
                return(0);
            } 
        }

        public int TransactionCount { 
            get {
                return(this.Transactions.Count);
            } 
        }

        public int CheckCount { 
            get {

                int intCheckCount = 0;

                foreach(cTransaction txn in this.Transactions.Values) {
                    intCheckCount += txn.CheckCount;
                }

                return(intCheckCount);
            } 
        }

        public int StubCount { 
            get {
                int intStubCount = 0;

                foreach(cTransaction txn in this.Transactions.Values) {
                    intStubCount += txn.StubCount;
                }

                return(intStubCount);
            } 
        }

        public int DocumentCount { 
            get {

                int intDocCount = 0;

                foreach(cTransaction txn in this.Transactions.Values) {
                    intDocCount += txn.DocumentCount;
                }

                return(intDocCount);
            } 
        }



        public Decimal SumPaymentsAmount {
            get {
                Decimal decRetVal = 0;

                foreach(cTransaction txn in this.Transactions.Values) {
                    decRetVal += txn.SumPaymentsAmount;
                }

                return(decRetVal);
            }
        }
        
        public Decimal SumStubsAmount {
            get {
                Decimal decRetVal = 0;

                foreach(cTransaction txn in this.Transactions.Values) {
                    decRetVal += txn.SumStubsAmount;
                }

                return(decRetVal);
            }
        }

        public Hashtable BatchImageFiles {
            get {
                if(_BatchImageFiles == null) {
                    _BatchImageFiles = new Hashtable();
                }
                return(_BatchImageFiles);
            }
        }

        public string FormattedImageName {
            get {
                return(_FormattedImageName);
            }
            set {
                _FormattedImageName = value;
            }
        }

        public static Hashtable DefaultFields() {

            Hashtable htFieldNames = new Hashtable();

            htFieldNames.Add("BankID", string.Empty);
            htFieldNames.Add("CustomerID", string.Empty);
            htFieldNames.Add("LockboxID", string.Empty);
            htFieldNames.Add("GlobalBatchID", string.Empty);
            htFieldNames.Add("DepositDate", string.Empty);
            htFieldNames.Add("ProcessingDate", string.Empty);
            htFieldNames.Add("BatchID", string.Empty);
            htFieldNames.Add("BatchTypeCode", string.Empty);
            htFieldNames.Add("SourceBatchID", string.Empty);

            return(htFieldNames);
        }

        public int CompareTo(object obj) {

            int intRetVal = 0;

            if(obj is cBatch) {

                cBatch batch = (cBatch) obj;

                foreach(IOrderByColumn orderbycolumn in _OrderByColumns) {
                    
                    switch(orderbycolumn.ColumnName.ToLower()) {
                        case "bankid":
                            if(this.BankID != batch.BankID) {
                                intRetVal = this.BankID.CompareTo(batch.BankID);
                            }
                            break;
                        case "customerid":
                            if(this.CustomerID != batch.CustomerID) {
                                intRetVal = this.CustomerID.CompareTo(batch.CustomerID);
                            }
                            break;
                        case "lockboxid":
                            if(this.ClientAccountID != batch.ClientAccountID) {
                                intRetVal = this.ClientAccountID.CompareTo(batch.ClientAccountID);
                            }
                            break;
                        case "depositdate":
                            if(this.DepositDateKey != batch.DepositDateKey) {
                                intRetVal = this.DepositDateKey.CompareTo(batch.DepositDateKey);
                            }
                            break;
                        case "ImmutableDateKey":
                            if(this.ImmutableDateKey != batch.ImmutableDateKey) {
                                intRetVal = this.ImmutableDateKey.CompareTo(batch.ImmutableDateKey);
                            }
                            break;
                        case "batchid":
                            if(this.BatchID != batch.BatchID) {
                                intRetVal = this.BatchID.CompareTo(batch.BatchID);
                            }
                            break;
                        default:
                            foreach(string fieldname in this.BatchFields.Keys) {
                                if(orderbycolumn.ColumnName.ToLower() == fieldname.ToLower()) {
                                    intRetVal = Compare(this.BatchFields, batch.BatchFields, fieldname);

                                    if(intRetVal != 0) {
                                        break;
                                    }
                                }
                            }
                            break;
                    }

                    if(intRetVal != 0) {
                        if(orderbycolumn.OrderByDir == OrderByDirEnum.Descending) {
                            intRetVal*=-1;
                        }
                        break;
                    }
                }

            } else {
                throw new ArgumentException("object is not a Batch");    
            }

            return(intRetVal);
        }
    }
}
