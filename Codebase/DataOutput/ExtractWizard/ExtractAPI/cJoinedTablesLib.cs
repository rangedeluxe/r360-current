﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/07/2014
*
* Purpose: To contain small data on joining columns from another table
*          to common tables.  This is to be done 'behind the scenes', without
*          user knowledge.
*
* Modification History
* WI 132009 BLR 03/07/2014
*   -Initial Version
* WI 132972 BLR 05/22/2014
*   -Joined the DimDDA table and exposed DDA and ABA.
* WI 153574 BLR 08/19/2014
*   -Joined Transaction Exception Status. 
******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    /// <summary>
    /// A static utility class to help out joining of tables.
    /// </summary>
    public static class cJoinedTablesLib
    {
        /// <summary>
        /// Returns a new list of columns to be added into the current
        /// table definition.
        /// </summary>
        /// <param name="tablename"></param>
        /// <returns></returns>
        public static List<cColumn> GetJoinedColumns(string tablename)
        {
            var output = new List<cColumn>();

            if (tablename.ToLower().Contains("factbatchsummary"))
            {
                output.Add(new cColumn(
                    "RecHubData",
                    "dimBatchPaymentTypes",
                    "LongName",
                    "PaymentTypeLongName",
                    0,
                    SqlDbType.VarChar,
                    20));
                output.Add(new cColumn(
                    "RecHubData",
                    "dimBatchSources",
                    "LongName",
                    "SourceLongName",
                    0,
                    SqlDbType.VarChar,
                    20));
                output.Add(new cColumn(
                    "RecHubData",
                    "dimBatchExceptionStatuses",
                    "ExceptionStatusName",
                    "ExceptionStatusName",
                    0,
                    SqlDbType.VarChar,
                    20));
            }
            else if (tablename.ToLower().Contains("factdocuments"))
            {
                output.Add(new cColumn(
                    "RecHubData",
                    "dimDocumentTypes",
                    "DocumentTypeDescription",
                    "DocumentTypeDescription",
                    0,
                    SqlDbType.VarChar,
                    20));
            }
            else if (tablename.ToLower().Contains("factchecks"))
            {
                output.Add(new cColumn(
                    "RecHubData",
                    "dimDDAs",
                    "DDA",
                    "DDA",
                    0,
                    SqlDbType.VarChar,
                    20));
                output.Add(new cColumn(
                    "RecHubData",
                    "dimDDAs",
                    "ABA",
                    "ABA",
                    0,
                    SqlDbType.VarChar,
                    20));
            }
            else if (tablename.ToLower().Contains("facttransactionsummary"))
            {
                output.Add(new cColumn(
                    "RecHubData",
                    "dimTransactionExceptionStatuses",
                    "ExceptionStatusName",
                    "ExceptionStatusName",
                    0,
                    SqlDbType.VarChar,
                    20));
            }

            return output;
        }
    }
}