﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Derek Davison
* Date:     02/24/2014 
*
* Purpose:  Wrapped-up all common functionality between cCheck and cDocument. 
*
* Modification History
* WI 129719 DLD 02/24/2014
*    -Inital Creation. 
* WI 130364 DLD 03/06/2014
*    -Moved OLFService to ExtractLib to be able to capture connectivity issues
* WI 132206 DLD 03/07/2014
*   - Added SiteKey value from Settings to GetMyImages()
* WI 132206 DLD 03/10/2014
*   - Adding http header values (SiteKey and SessionID) to service call.
* WI 129719 BLR 06/09/2014
*   - Updated for OLFServices integration.
* WI 147626 BLR 06/13/2014
*   - Wrapped the OLFServices calls to GetImages in a try-catch, and are now
*     gracefully handling exceptions where images are not found on the server.
*   - Changed 'GetMyImages' to 'GetImages'.  Never understood why the 'my' word
*     is at all relevant.    
* WI 143040 BLR 05/22/2014
*   - Removed dependence on the composite cBatchKey, only BatchID is utilized. 
*   - Changed all BatchIDs to long. 
* WI 152321 BLR 08/15/2014
*   -Moved Configuration over to ExtractConfigurationSettings. 
*******************************************************************************/

namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{
    public abstract class cExtractImageBase : ObjectBase, IImage, ISummaryFields
    {
        #region Original Members

        protected ExtractConfigurationSettings Settings = null;

        public cTransaction Transaction { get; protected set; }

        public int TransactionID { get; protected set; }

        public int BatchSequence { get; protected set; }

        protected string _FormattedImageFName = String.Empty;
        public string FormattedImageFName { get; set; }

        protected string _FormattedImageBName = String.Empty;
        public string FormattedImageBName { get; set; }

        public int ImageCounter { get; set; }

        public bool ExtractImageFront { get; set; }
        public bool ExtractImageBack { get; set; }

        public virtual string FileDescriptor { get; protected set; }

        public int BatchCount { get { return 1; } }

        public int TransactionCount { get { return 1; } }

        public int CheckCount { get { return Transaction.CheckCount; } }

        public int StubCount { get { return Transaction.StubCount; } }

        public int DocumentCount { get { return Transaction.DocumentCount; } }

        public virtual Decimal SumPaymentsAmount { get { return 0; } }

        public Decimal SumStubsAmount { get { return 0; } }

        protected IOrderByColumn[] OrderByColumns = { };

        public cItemKey ItemKey
        {
            get
            {
                return (new cItemKey(
                    Transaction.Batch.BatchID,
                    BatchSequence));
            }
        }

        #endregion

        #region New Members

        /// <summary>
        /// A flag to indicate whether or not the Call was made to load this item's images from the repository.
        /// </summary>
        private bool ImagesLoaded { get; set; }
        public string FileExtension { get; protected set; }
        private List<byte[]> Images { get; set; }

        public bool HasImages
        {
            get
            {
                if (!ImagesLoaded)
                    GetImages();

                return Images != null && Images.Any();
            }
        }

        /// <summary>
        /// Front image as byte array.
        /// </summary>
        public byte[] FrontImage
        {
            get
            {
                if (HasImages)
                    return Images[0];
                else
                    return null;
            }
        }

        /// <summary>
        /// Back image as byte array.
        /// </summary>
        public byte[] BackImage
        {
            get
            {
                if (HasImages && Images.Count > 1)
                    return Images[1];
                else
                    return null;
            }
        }

        #endregion

        /// <summary>
        /// Retrieve this item's image(s) from the image repository.
        /// WI 147626 : Wrapped the GetImage call in a Try-Catch and are logging the
        /// images not found on server exceptions.
        /// </summary>
        /// <returns></returns>
        protected bool GetImages()
        {
            bool result = false;
            byte[][] imgList = null;
            string fileDescriptor = string.Empty;
            string fileExtension = string.Empty;
            ImagesLoaded = true;

            // reset data properties set by this method
            FileExtension = String.Empty;
            FileDescriptor = String.Empty;

            Images = null;

            var svc = cOLFServicesLib.CurrentClient;

            result = cOLFServicesLib.GetImage(this.Transaction.Batch.BankID
                , this.Transaction.Batch.ClientAccountID
                , this.Transaction.Batch.ImmutableDateKey
                , Convert.ToInt32(this.Transaction.Batch.DepositDate.ToString("yyyyMMdd"))
                , this.Transaction.Batch.BatchID
                , this.BatchSequence
                , (this is cPayment)
                , 0
                , this.Transaction.Batch.SourceBatchID
                , this.Transaction.Batch.BatchSourceShortName
                , this.Transaction.Batch.ImportTypeShortName
                , out imgList
                , out fileDescriptor
                , out fileExtension);

            if (result)
            {
                result = false;
                Images = imgList.ToList();

                if (imgList != null)
                {
                    FileDescriptor = fileDescriptor;
                    FileExtension = fileExtension;
                    result = true;
                }
            }

            return result;
        }
    }
}
