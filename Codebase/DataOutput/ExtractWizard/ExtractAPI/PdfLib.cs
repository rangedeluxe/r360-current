using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;

using iTextSharp.text;
using iTextSharp.text.pdf;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 129719 DLD 02/24/2014
*   -Converting image repository from ipoPics.dll to OLFServices 
* WI 131640 BLR 03/04/2014, DLD 03/05/20104  
*   - Added in additional exception logging.
* WI 269611 JMC 03/10/2016
*   -Modified internal DAL reference to use interface in order to enable Mocking
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI
{

    internal static class PdfLib
    {

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        private const int PDF_PAGE_MARGIN_LEFT = 50;
        private const float PHRASE_SPACING = 10;
        private const string RPT_FONT_NAME = "times-roman";

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        public static bool PrintImagePdf(
            List<WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI.cImageType> images,
            string documentName,
            string destFileName,
            bool includeFooter,
            IExtractEngineDAL extractEngineDAL)
        {
            var imgs = images.Where(x => x.myImageAsByte.Length > 0);
            if (!imgs.Any())
            {
                // We're going to kick out very early if we don't actually have any images to write out.
                return true;
            }

            string strTempFileName;
            float intScaleWidth;
            float intScaleHeight;
            bool bolRetVal;
            string strDestinationFolder;
            // Page counter, because document.PageCount is inaccessable. (why!?!)
            int pagecount = 0;

            // step 1: creation of a document-object
            iTextSharp.text.Document document = new iTextSharp.text.Document();

            strTempFileName = System.IO.Path.GetTempFileName();

            // step 2:
            // we create a writer that listens to the document
            // and directs a PDF-stream to a file
            // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
            PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, new FileStream(strTempFileName, FileMode.Create));

            document.AddTitle(documentName);

            // step 3: we open the document
            document.Open();

            // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
            PdfContentByte cbDetail = writer.DirectContent;

            try
            {
                foreach (cImageType itImage in imgs)
                {
                    try
                    {
                        // step 4: we add content
                        // DLD - TODO - WI 129719 - Image processing
                        var details = document.AppendImages(writer, itImage);
                        intScaleWidth = details.ScaleWidth;
                        intScaleHeight = details.ScaleHeight;
                        
                        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
                        if (includeFooter)
                        {
                            if (itImage.IsCheck)
                            {
                                // get DepositDate, Lockbox, Batch, Transaction, RT, Account, Serial, Amount
                                DataTable dt;

                                if (extractEngineDAL.GetCheckInfo(itImage.ItemKey, out dt))
                                {

                                    foreach (DataRow myRow in dt.Rows)
                                    {
                                        // add the table
                                        Chunk chnkDisplayTitle2 = null;
                                        Chunk chnkDisplayText2 = null;
                                        Chunk chnkDisplayTitle3 = null;
                                        Chunk chnkDisplayText3 = null;
                                        Phrase p2;
                                        Phrase p3;

                                        // Create table with two columns.
                                        PdfPTable objTable = new PdfPTable(2);

                                        objTable.TotalWidth = document.PageSize.Width - (PDF_PAGE_MARGIN_LEFT * 2);
                                        objTable.LockedWidth = true;
                                        objTable.DefaultCell.BorderWidth = 0;
                                        objTable.DefaultCell.Padding = 0;
                                        objTable.DefaultCell.PaddingBottom = 2;

                                        Font ReportFontBold = FontFactory.GetFont(RPT_FONT_NAME, 10, iTextSharp.text.Font.BOLD);
                                        Font ReportFont = FontFactory.GetFont(RPT_FONT_NAME, 10, iTextSharp.text.Font.NORMAL);

                                        p2 = new Phrase(PHRASE_SPACING);
                                        p3 = new Phrase(PHRASE_SPACING);

                                        chnkDisplayTitle2 = new Chunk("Deposit Date: ", ReportFontBold);
                                        chnkDisplayText2 = new Chunk(Convert.ToDateTime(myRow["DepositDate"]).ToShortDateString(), ReportFont);

                                        p2.Add(chnkDisplayTitle2);
                                        p2.Add(chnkDisplayText2);

                                        chnkDisplayTitle3 = new Chunk("R/T: ", ReportFontBold);
                                        chnkDisplayText3 = new Chunk(myRow["RoutingNumber"].ToString(), ReportFont);

                                        p3.Add(chnkDisplayTitle3);
                                        p3.Add(chnkDisplayText3);

                                        objTable.AddCell(p2);
                                        objTable.AddCell(p3);

                                        objTable.CompleteRow();

                                        p2 = new Phrase(PHRASE_SPACING);
                                        p3 = new Phrase(PHRASE_SPACING);

                                        chnkDisplayTitle2 = new Chunk("Workgroup ID: ", ReportFontBold);
                                        chnkDisplayText2 = new Chunk(myRow["SiteClientAccountID"].ToString(), ReportFont);

                                        p2.Add(chnkDisplayTitle2);
                                        p2.Add(chnkDisplayText2);

                                        chnkDisplayTitle3 = new Chunk("Account Number: ", ReportFontBold);
                                        chnkDisplayText3 = new Chunk(myRow["Account"].ToString(), ReportFont);

                                        p3.Add(chnkDisplayTitle3);
                                        p3.Add(chnkDisplayText3);

                                        objTable.AddCell(p2);
                                        objTable.AddCell(p3);

                                        objTable.CompleteRow();

                                        p2 = new Phrase(PHRASE_SPACING);
                                        p3 = new Phrase(PHRASE_SPACING);

                                        chnkDisplayTitle2 = new Chunk("Batch: ", ReportFontBold);
                                        chnkDisplayText2 = new Chunk(myRow["BatchID"].ToString(), ReportFont);

                                        p2.Add(chnkDisplayTitle2);
                                        p2.Add(chnkDisplayText2);

                                        chnkDisplayTitle3 = new Chunk("Check Number: ", ReportFontBold);
                                        chnkDisplayText3 = new Chunk(myRow["Serial"].ToString(), ReportFont);

                                        p3.Add(chnkDisplayTitle3);
                                        p3.Add(chnkDisplayText3);

                                        objTable.AddCell(p2);
                                        objTable.AddCell(p3);

                                        objTable.CompleteRow();

                                        p2 = new Phrase(PHRASE_SPACING);
                                        p3 = new Phrase(PHRASE_SPACING);

                                        chnkDisplayTitle2 = new Chunk("Transaction: ", ReportFontBold);
                                        chnkDisplayText2 = new Chunk(myRow["TransactionID"].ToString(), ReportFont);

                                        p2.Add(chnkDisplayTitle2);
                                        p2.Add(chnkDisplayText2);

                                        chnkDisplayTitle3 = new Chunk("Amount: ", ReportFontBold);
                                        chnkDisplayText3 = new Chunk(Convert.ToDouble(myRow["Amount"]).ToString("C"), ReportFont);

                                        p3.Add(chnkDisplayTitle3);
                                        p3.Add(chnkDisplayText3);

                                        objTable.AddCell(p2);
                                        objTable.AddCell(p3);

                                        objTable.CompleteRow();

                                        objTable.WriteSelectedRows(0, -1, 50, document.Top - (intScaleHeight + 20), cbDetail);
                                    }

                                    dt.Dispose();
                                }
                            }
                            else
                            {
                                // get DepositDate, Lockbox, Batch, Transaction
                                DataTable dt;

                                if (extractEngineDAL.GetDocumentInfo(itImage.ItemKey, out dt))
                                {

                                    foreach (DataRow myRow in dt.Rows)
                                    {
                                        // add the table
                                        Chunk chnkDisplayTitle2 = null;
                                        Chunk chnkDisplayText2 = null;
                                        Phrase p2;

                                        // Create table with two columns.
                                        PdfPTable objTable = new PdfPTable(2);

                                        objTable.TotalWidth = document.PageSize.Width - (PDF_PAGE_MARGIN_LEFT * 2);
                                        objTable.LockedWidth = true;
                                        objTable.DefaultCell.BorderWidth = 0;
                                        objTable.DefaultCell.Padding = 0;
                                        objTable.DefaultCell.PaddingBottom = 2;

                                        Font ReportFontBold = FontFactory.GetFont(RPT_FONT_NAME, 10, iTextSharp.text.Font.BOLD);
                                        Font ReportFont = FontFactory.GetFont(RPT_FONT_NAME, 10, iTextSharp.text.Font.NORMAL);

                                        p2 = new Phrase(PHRASE_SPACING);

                                        chnkDisplayTitle2 = new Chunk("Deposit Date: ", ReportFontBold);
                                        chnkDisplayText2 = new Chunk(Convert.ToDateTime(myRow["DepositDate"]).ToShortDateString(), ReportFont);

                                        p2.Add(chnkDisplayTitle2);
                                        p2.Add(chnkDisplayText2);

                                        objTable.AddCell(p2);
                                        objTable.CompleteRow();

                                        p2 = new Phrase(PHRASE_SPACING);

                                        chnkDisplayTitle2 = new Chunk("Workgroup:  ", ReportFontBold);
                                        chnkDisplayText2 = new Chunk(myRow["SiteClientAccountID"].ToString(), ReportFont);

                                        p2.Add(chnkDisplayTitle2);
                                        p2.Add(chnkDisplayText2);

                                        objTable.AddCell(p2);
                                        objTable.CompleteRow();

                                        p2 = new Phrase(PHRASE_SPACING);

                                        chnkDisplayTitle2 = new Chunk("Batch: ", ReportFontBold);
                                        chnkDisplayText2 = new Chunk(myRow["BatchID"].ToString(), ReportFont);

                                        p2.Add(chnkDisplayTitle2);
                                        p2.Add(chnkDisplayText2);

                                        objTable.AddCell(p2);
                                        objTable.CompleteRow();

                                        p2 = new Phrase(PHRASE_SPACING);

                                        chnkDisplayTitle2 = new Chunk("Transaction: ", ReportFontBold);
                                        chnkDisplayText2 = new Chunk(myRow["TransactionID"].ToString(), ReportFont);

                                        p2.Add(chnkDisplayTitle2);
                                        p2.Add(chnkDisplayText2);

                                        objTable.AddCell(p2);
                                        objTable.CompleteRow();

                                        objTable.WriteSelectedRows(0, -1, 50, document.Top - (intScaleHeight + 20), cbDetail);
                                    }

                                    dt.Dispose();
                                }
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        // WI 131640 : Additional exception Logging.
                        cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential); ;
                        throw;
                    }
                }

                try
                {                    
                    document.Close();
                }
                catch (Exception e)
                {
                    // WI 131640 : Additional exception Logging.
                    cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential); ;
                }

                if (File.Exists(destFileName))
                    File.Delete(destFileName);

                strDestinationFolder = string.Empty;
                try
                {
                    strDestinationFolder = System.IO.Path.GetDirectoryName(destFileName);

                    if (!Directory.Exists(strDestinationFolder))
                        Directory.CreateDirectory(strDestinationFolder);
                }
                catch (Exception)
                {
                    throw (new Exception("Could not create Image File Path, or part of the path name was invalid: " + strDestinationFolder));
                }

                File.Move(strTempFileName, destFileName);

                bolRetVal = true;
            }
            catch (iTextSharp.text.DocumentException ex)
            {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                try
                {
                    // step 5: we close the document
                    document.Close();
                }
                catch (Exception e)
                {
                    // WI 131640 : Additional exception Logging.
                    cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential); ;
                }

                ////Console.Error.WriteLine(de.Message);
                //bolRetVal = false;

                throw (ex);
            }
            catch (IOException ex)
            {
                // WI 131640 : Additional exception Logging.
                cCommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
                try
                {
                    // step 5: we close the document
                    document.Close();
                }
                catch (Exception e)
                {
                    // WI 131640 : Additional exception Logging.
                    cCommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential); ;
                }

                ////Console.Error.WriteLine(ioe.Message);
                //bolRetVal = false;

                throw (ex);
            }

            return (bolRetVal);
        }

        public static DocumentDetails AppendImages(this Document document, PdfWriter writer, cImageType imageType)
        {
            float intScaleWidth = 0;
            float intScaleHeight = 0;

            try
            {
                Image image = Image.GetInstance(imageType.myImageAsByte);
                GetImageScale(image, document.PageSize.Width - document.LeftMargin - document.RightMargin,
                    document.PageSize.Height - document.TopMargin - document.BottomMargin, out intScaleWidth,
                    out intScaleHeight);

                image.ScaleAbsolute(intScaleWidth, intScaleHeight);

                document.NewPage();
                document.Add(image);

                return new DocumentDetails
                {
                    ScaleWidth = intScaleWidth,
                    ScaleHeight = intScaleHeight
                };

            }
            catch
            {
                //it may already be a pdf byte array which isn't a recognized format, so let's try and append the pdf byte array
                return document.AppendImages(writer, imageType.myImageAsByte);
            }
        }

        public static DocumentDetails AppendImages(this Document document, PdfWriter writer, byte[] pdfBytes)
        {
            float intScaleWidth = 0;
            float intScaleHeight = 0;

            var pdfReader = new PdfReader(pdfBytes);
            Int32 pages = pdfReader.NumberOfPages;
            for (Int32 pageNum = 1; pageNum <= pages; pageNum++)
            {
                document.NewPage();
                var page = writer.GetImportedPage(pdfReader, pageNum);
                var image = Image.GetInstance(page);
                GetImageScale(image, document.PageSize.Width - document.LeftMargin - document.RightMargin,
                    document.PageSize.Height - document.TopMargin - document.BottomMargin, out intScaleWidth,
                    out intScaleHeight);

                image.ScaleAbsolute(intScaleWidth, intScaleHeight);
                document.Add(image);
            }

            return new DocumentDetails
            {
                ScaleWidth = intScaleWidth,
                ScaleHeight = intScaleHeight
            };
        }

        private static void GetImageScale(
            iTextSharp.text.Image original,
            float maxWidth,
            float maxHeight,
            out float scaleWidth,
            out float scaleHeight)
        {

            float nWidth = 0;
            float nHeight = 0;
            float oWidth;
            float oHeight;

            if (original != null)
            {

                nWidth = maxWidth;
                nHeight = maxHeight;
                oWidth = original.Width;
                oHeight = original.Height;

                if ((original.Width / maxWidth) > (original.Height / maxHeight))
                {
                    double Ratio = (double)((double)(oHeight) / (double)(oWidth));
                    double Final = (nWidth) * Ratio;
                    nHeight = (int)Final;

                }
                else
                {
                    double Ratio = (double)((double)(oWidth) / (double)(oHeight));
                    double Final = (nHeight) * Ratio;
                    nWidth = (int)Final;
                }
            }

            scaleWidth = nWidth;
            scaleHeight = nHeight;
        }
    }
}
