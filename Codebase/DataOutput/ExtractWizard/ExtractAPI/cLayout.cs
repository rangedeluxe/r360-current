using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

using WFS.RecHub.Common;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 116545 BLR 02/12/2014
*   -Updated 'checks', 'stubs', and 'documents' hard-coded strings to match
*    the new data model.
* WI 129562 BLR 02/13/2014
*   -Moved some hard strings over to constant strings, fixed IsLayoutValid()
*    with said constants.   
* WI 269406 JMC 03/10/2016
*   - Update the local "WasExtracted" bit when a Layout element has been written 
*     to the Extract Data File.
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cLayout : ILayout {

        private cExtractDef _ExtractDef;
        private ArrayList _LayoutFields;
        private string _LayoutName = string.Empty;
        private LayoutTypeEnum _LayoutType = LayoutTypeEnum.Header;
        private LayoutLevelEnum _LayoutLevel;
        private int _MaxRepeats = 0;

        private bool _UseOccursGroups = false;

        private bool _UsePaymentsOccursGroups = false;
        private int _PaymentsOccursCount;

        private bool _UseStubsOccursGroups = false;
        private int _StubsOccursCount;

        private bool _UseDocumentsOccursGroups = false;
        private int _DocumentsOccursCount;

        internal event outputErrorEventHandler OutputError;

        /// <summary>
        /// For the use of a simple POCO.
        /// </summary>
        public cLayout()
        {
            _LayoutFields = new ArrayList();
        }

        public cLayout(cExtractDef extractDef) : this() {
            _ExtractDef = extractDef;
        }

        public string LayoutName {
            get {
                return(_LayoutName);
            }
            set {
                _LayoutName = value;
            }
        }

        public LayoutTypeEnum LayoutType {
            get {
                return(_LayoutType);
            }
            set {
                _LayoutType = value;
            }
        }

        public string LayoutTypeDesc {
            get {
                switch(this.LayoutType) {
                    case LayoutTypeEnum.Trailer:
                        return("Trailer");
                    default:
                        return("Header");
                }
            }
        }

        public LayoutLevelEnum LayoutLevel {
            get {
                return(_LayoutLevel);
            }
            set {
                _LayoutLevel = value;
            }
        }

        public string LayoutLevelDesc {
            get {
                return(cExtractDef.DecodeLayoutLevel(this.LayoutLevel));
            }
        }

        public int MaxRepeats {
            get {
                if(this.LayoutLevel == LayoutLevelEnum.JointDetail && UseOccursGroups) {
                    return(_MaxRepeats);
                } else {
                    return(0);
                }
            }
            set {
                if(value > 0 && this.LayoutLevel != LayoutLevelEnum.JointDetail) {
                    //throw(new Exception("Max Repeats are only valid for Joint Detail layouts."));
                } else if(value > 0 && !UseOccursGroups) {
                    //throw(new Exception("Max Repeats are only valid for Joint Detail layouts that utilize Occurs Groups."));
                } else {
                    _MaxRepeats = value;
                }
            }
        }

        public bool UseOccursGroups {
            get {
                if(this.LayoutLevel == LayoutLevelEnum.JointDetail) {
                    return(_UseOccursGroups);
                } else {
                    return(false);
                }
            }
            set {
                if(_UseOccursGroups == true && this.LayoutLevel != LayoutLevelEnum.JointDetail) {
                    //throw(new Exception("Occurs Groups can only be utilized for Joint Detail layouts."));
                } else {
                    _UseOccursGroups = value;
                }
            }
        }

        public bool UsePaymentsOccursGroups {
            get { return _UseOccursGroups && _UsePaymentsOccursGroups; }
            set {
                _UsePaymentsOccursGroups = value;
            }
        }

        public int PaymentsOccursCount {
            get {
                return(_PaymentsOccursCount);
            }
            set {
                _PaymentsOccursCount = value;
            }
        }

        public bool UseStubsOccursGroups
        {
            get { return _UseOccursGroups && _UseStubsOccursGroups; }
            set {
                _UseStubsOccursGroups = value;
            }
        }

        public int StubsOccursCount {
            get {
                return(_StubsOccursCount);
            }
            set {
                _StubsOccursCount = value;
            }
        }

        public bool UseDocumentsOccursGroups
        {
            get { return _UseOccursGroups && _UseDocumentsOccursGroups; }
            set {
                _UseDocumentsOccursGroups = value;
            }
        }

        public int DocumentsOccursCount {
            get {
                return(_DocumentsOccursCount);
            }
            set {
                _DocumentsOccursCount = value;
            }
        }

        public cExtractDef ExtractDef {
            get {
                return(_ExtractDef);
            }
        }

        public ILayoutField[] LayoutFields {
            get {
                return((cLayoutField[])_LayoutFields.ToArray(typeof(cLayoutField)));
            }
        }

        public void AddLayoutField(cLayoutField LayoutField) {

            _LayoutFields.Add(LayoutField);
        }

        public void InsertLayoutField(
            int index, 
            cLayoutField layoutField) {

            _LayoutFields.Insert(
                index, 
                layoutField);
        }
        
        public void RemoveLayoutField(int index) {
            _LayoutFields.RemoveAt(index);
        }


        public bool IsValidLayout(out string msg) {
            
            SortedList<int, int> slChecksLayoutFieldPos = new SortedList<int, int>();
            SortedList<int, int> slStubsLayoutFieldPos = new SortedList<int, int>();
            SortedList<int, int> slDocumentsLayoutFieldPos = new SortedList<int, int>();

            bool bolRetVal;
            StringBuilder sbMsg = new StringBuilder();

            if(this.LayoutLevel == LayoutLevelEnum.JointDetail && this.LayoutFields.Length > 0) {

                for(int i=0;i<LayoutFields.Length;++i) {
                    // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                    if(LayoutFields[i].FieldName.ToLower().StartsWith(Support.PAYMENT_COLUMN_STRING_STARTER) || 
                       LayoutFields[i].FieldName.ToLower().StartsWith("checksdataentry.")) {
                                               
                        slChecksLayoutFieldPos.Add(i, i);

                    }
                    // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                    else if(LayoutFields[i].FieldName.ToLower().StartsWith(Support.STUB_COLUMN_STRING_STARTER) || 
                              LayoutFields[i].FieldName.ToLower().StartsWith("stubsdataentry.")) {

                        slStubsLayoutFieldPos.Add(i, i);

                    }
                    // WI 129562 : Updated hard strings to constant strings for Joint Details. 
                    else if(LayoutFields[i].FieldName.ToLower().StartsWith(Support.DOCUMENT_COLUMN_STRING_STARTER)) {

                        slDocumentsLayoutFieldPos.Add(i, i);
                    }                    
                }

                if(UseOccursGroups && UsePaymentsOccursGroups && slChecksLayoutFieldPos.Count > 1) {
                    for(int i=1;i<slChecksLayoutFieldPos.Count;++i) {
                        if(slChecksLayoutFieldPos.Values[i] != slChecksLayoutFieldPos.Values[i - 1] + 1 && PaymentsOccursCount > 1) {
                            sbMsg.Append("\nOccurs Groups must be contiguous for all items belonging to the same detail level [Checks / ChecksDataEntry] when Occurs Count is greater than 1.");
                            break;
                        }
                    }
                }

                if(UseOccursGroups && UseStubsOccursGroups && slStubsLayoutFieldPos.Count > 1) {
                    for(int i=1;i<slStubsLayoutFieldPos.Count;++i) {
                        if(slStubsLayoutFieldPos.Values[i] != slStubsLayoutFieldPos.Values[i - 1] + 1 && StubsOccursCount > 1) {
                            sbMsg.Append("\nOccurs Groups must be contiguous for all items belonging to the same detail level [Stubs / StubsDataEntry] when Occurs Count is greater than 1.");
                            break;
                        }
                    }
                }

                if(UseOccursGroups && UseDocumentsOccursGroups && slDocumentsLayoutFieldPos.Count > 1) {
                    for(int i=1;i<slDocumentsLayoutFieldPos.Count;++i) {
                        if(slDocumentsLayoutFieldPos.Values[i] != slDocumentsLayoutFieldPos.Values[i - 1] + 1 && DocumentsOccursCount > 1) {
                            sbMsg.Append("\nOccurs Groups must be contiguous for all items belonging to the same detail level [Documents] when Occurs Count is greater than 1.");
                            break;
                        }
                    }
                }

                if(UseOccursGroups) {
                    if((slChecksLayoutFieldPos.Count > 0 && !UsePaymentsOccursGroups && slStubsLayoutFieldPos.Count > 0 && !UseStubsOccursGroups) ||
                       (slChecksLayoutFieldPos.Count > 0 && !UsePaymentsOccursGroups && slDocumentsLayoutFieldPos.Count > 0 && !UseDocumentsOccursGroups) ||
                       (slStubsLayoutFieldPos.Count > 0 && !UseStubsOccursGroups && slDocumentsLayoutFieldPos.Count > 0 && !UseDocumentsOccursGroups)) {

                        sbMsg.Append("\nWhen using Occurs Groups, only one set of field types [Checks, Stubs, Documents] are allowed to NOT have Occurs defined.");
                    }
                }

                if(UsePaymentsOccursGroups && PaymentsOccursCount < 1) {
                    sbMsg.Append("\nWhen Occurs Groups are defined for a detail table [Checks / ChecksDataEntry], Occurs Count must be defined.");
                }

                if(UseStubsOccursGroups && StubsOccursCount < 1) {
                    sbMsg.Append("\nWhen Occurs Groups are defined for a detail table [Stubs / StubsDataEntry], Occurs Count must be defined.");
                }

                if(UseDocumentsOccursGroups && DocumentsOccursCount < 1) {
                    sbMsg.Append("\nWhen Occurs Groups are defined for a detail table [Documents], Occurs Count must be defined.");
                }

                if(sbMsg.Length > 0) {
                    sbMsg.Insert(0, "Layout [" + this.LayoutName + "] contains invalid Occurs Groups.\n");
                    bolRetVal = false;
                } else {
                    bolRetVal = true;
                }

            } else {
                bolRetVal = true;
            }

            if(bolRetVal) {
                msg = string.Empty;
            } else {
                msg = sbMsg.ToString();
            }

            return(bolRetVal);
        }

        internal bool FormatLayout(
            ISummaryFields bankSummary,
            cExtractDef extractDef, 
            cExtractParms parms, 
            cStandardFields standardFields, 
            cCounters counters,
            out string layoutText) {

            StringBuilder sbGridLine = new StringBuilder();
            bool bolIsFirstLayoutField = true;

            foreach(cLayoutField layoutfield in this.LayoutFields) {

                if(!bolIsFirstLayoutField) {
                    sbGridLine.Append(ExtractDef.FieldDelim);
                }

                if(layoutfield.IsStatic) {
                    sbGridLine.Append(layoutfield.FormatStaticField());
                } else if(layoutfield.IsFiller) {
                    sbGridLine.Append(layoutfield.FormatFiller());
                } else if(layoutfield.IsAggregate) {
                    sbGridLine.Append(layoutfield.FormatAggregateField(bankSummary));
                } else if(layoutfield.IsStandard) {
                    //TODO: Correct Layout Level
                    sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                } else if(layoutfield.IsFirstLast) {
                    sbGridLine.Append(layoutfield.FormatFirstLastField(counters.GetPosition(LayoutLevelEnum.File)));
                } else if(layoutfield.IsCounter) {
                    sbGridLine.Append(layoutfield.FormatCounter(counters.FileIndex));
                } else if(layoutfield.IsLineCounter) {
                    sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                } else if(layoutfield.IsRecordCounter) {
                    sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                } else {
                    sbGridLine.Append(layoutfield.FormatValue("???????????????????????"));
                }

                bolIsFirstLayoutField = false;
            }

            layoutText = sbGridLine.ToString();

            return(true);
        }

        internal bool FormatLayout(
            cExtract extractBase,
            cBank bank,
            cExtractDef extractDef, 
            cExtractParms parms, 
            cStandardFields standardFields, 
            cCounters counters,
            out string layoutText) {

            StringBuilder sbGridLine = new StringBuilder();
            bool bolIsFirstLayoutField = true;

            foreach(cLayoutField layoutfield in this.LayoutFields) {

                if(!bolIsFirstLayoutField) {
                    sbGridLine.Append(ExtractDef.FieldDelim);
                }

                if(layoutfield.IsStatic) {
                    sbGridLine.Append(layoutfield.FormatStaticField());
                } else if(layoutfield.IsFiller) {
                    sbGridLine.Append(layoutfield.FormatFiller());
                } else if(layoutfield.IsAggregate) {
                    sbGridLine.Append(layoutfield.FormatAggregateField(bank));
                } else if(layoutfield.IsStandard) {
                    sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                } else if(layoutfield.IsFirstLast) {
                    sbGridLine.Append(layoutfield.FormatFirstLastField(counters.GetPosition(LayoutLevelEnum.Bank)));
                } else if(layoutfield.IsCounter) {
                    sbGridLine.Append(layoutfield.FormatCounter(counters.BankIndex));
                } else if(layoutfield.IsLineCounter) {
                    sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                } else if(layoutfield.IsRecordCounter) {
                    sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                } else {
                    sbGridLine.Append(layoutfield.FormatLayoutField(extractBase, bank));
                }

                bank.WasExtracted = true;
                bolIsFirstLayoutField = false;
            }

            layoutText = sbGridLine.ToString();

            return(true);
        }

        internal bool FormatLayout(
            cExtract extractBase,
            cClientAccount lockbox, 
            cExtractDef extractDef, 
            cExtractParms parms, 
            cStandardFields standardFields, 
            cCounters counters,
            out string layoutText) {

            StringBuilder sbGridLine = new StringBuilder();
            bool bolIsFirstLayoutField = true;

            foreach(cLayoutField layoutfield in this.LayoutFields) {

                if(!bolIsFirstLayoutField) {
                    sbGridLine.Append(ExtractDef.FieldDelim);
                }

                if(layoutfield.IsStatic) {
                    sbGridLine.Append(layoutfield.FormatStaticField());
                } else if(layoutfield.IsFiller) {
                    sbGridLine.Append(layoutfield.FormatFiller());
                } else if(layoutfield.IsAggregate) {
                    sbGridLine.Append(layoutfield.FormatAggregateField(lockbox));
                } else if(layoutfield.IsStandard) {
                    sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                } else if(layoutfield.IsFirstLast) {
                    sbGridLine.Append(layoutfield.FormatFirstLastField(counters.GetPosition(LayoutLevelEnum.ClientAccount)));
                } else if(layoutfield.IsCounter) {
                    sbGridLine.Append(layoutfield.FormatCounter(counters.LockboxIndex));
                } else if(layoutfield.IsLineCounter) {
                    sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                } else if(layoutfield.IsRecordCounter) {
                    sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                } else {
                    sbGridLine.Append(layoutfield.FormatLayoutField(extractBase, lockbox));
                }

                lockbox.WasExtracted = true;
                bolIsFirstLayoutField = false;
            }

            layoutText = sbGridLine.ToString();

            return(true);
        }

        internal bool FormatLayout(
            cExtract extractBase,
            cBatch batch, 
            cExtractDef extractDef, 
            cExtractParms parms, 
            cStandardFields standardFields, 
            cCounters counters,
            out string layoutText) {

            StringBuilder sbGridLine = new StringBuilder();
            bool bolIsFirstLayoutField = true;

            foreach(cLayoutField layoutfield in this.LayoutFields) {

                if(!bolIsFirstLayoutField) {
                    sbGridLine.Append(ExtractDef.FieldDelim);
                }

                if(layoutfield.IsStatic) {
                    sbGridLine.Append(layoutfield.FormatStaticField());
                } else if(layoutfield.IsFiller) {
                    sbGridLine.Append(layoutfield.FormatFiller());
                } else if(layoutfield.IsAggregate) {
                    sbGridLine.Append(layoutfield.FormatAggregateField(batch));
                } else if(layoutfield.IsStandard) {
                    sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                } else if(layoutfield.IsFirstLast) {
                    sbGridLine.Append(layoutfield.FormatFirstLastField(counters.GetPosition(LayoutLevelEnum.Batch)));
                } else if(layoutfield.IsCounter) {
                    sbGridLine.Append(layoutfield.FormatCounter(counters.BatchIndex));
                } else if(layoutfield.IsLineCounter) {
                    sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                } else if(layoutfield.IsRecordCounter) {
                    sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                } else {
                    sbGridLine.Append(layoutfield.FormatLayoutField(extractBase, batch));
                }

                batch.WasExtracted = true;
                bolIsFirstLayoutField = false;
            }

            layoutText = sbGridLine.ToString();

            return(true);
        }

        internal bool FormatLayout(
            cExtract extractBase,
            cTransaction transaction, 
            cExtractDef extractDef, 
            cExtractParms parms, 
            cStandardFields standardFields, 
            cCounters counters,
            out string layoutText) {

            StringBuilder sbGridLine = new StringBuilder();
            bool bolIsFirstLayoutField = true;

            foreach(cLayoutField layoutfield in this.LayoutFields) {

                if(!bolIsFirstLayoutField) {
                    sbGridLine.Append(ExtractDef.FieldDelim);
                }

                if(layoutfield.IsStatic) {
                    sbGridLine.Append(layoutfield.FormatStaticField());
                } else if(layoutfield.IsFiller) {
                    sbGridLine.Append(layoutfield.FormatFiller());
                } else if(layoutfield.IsAggregate) {
                    sbGridLine.Append(layoutfield.FormatAggregateField(transaction));
                } else if(layoutfield.IsStandard) {
                    sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                } else if(layoutfield.IsFirstLast) {
                    sbGridLine.Append(layoutfield.FormatFirstLastField(counters.GetPosition(LayoutLevelEnum.Transaction)));
                } else if(layoutfield.IsCounter) {
                    sbGridLine.Append(layoutfield.FormatCounter(counters.TransactionIndex));
                } else if(layoutfield.IsLineCounter) {
                    sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                } else if(layoutfield.IsRecordCounter) {
                    sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                } else {
                    sbGridLine.Append(layoutfield.FormatLayoutField(extractBase, transaction));
                }

                transaction.WasExtracted = true;
                bolIsFirstLayoutField = false;
            }

            layoutText = sbGridLine.ToString();

            return(true);
        }

        internal bool FormatLayout(
            cExtract extractBase,
            cPayment check,
            cExtractDef extractDef,
            cExtractParms parms,
            cStandardFields standardFields,
            cImageFormatOptions imageFormatOptions,
            cCounters counters,
            out string layoutText) {

            StringBuilder sbGridLine = new StringBuilder();
            bool bolIsFirstLayoutField = true;

            foreach(cLayoutField layoutfield in this.LayoutFields) {

                if(!bolIsFirstLayoutField) {
                    sbGridLine.Append(ExtractDef.FieldDelim);
                }

                if(layoutfield.IsStatic) {
                    sbGridLine.Append(layoutfield.FormatStaticField());
                } else if(layoutfield.IsFiller) {
                    sbGridLine.Append(layoutfield.FormatFiller());
                } else if(layoutfield.IsAggregate) {
                    sbGridLine.Append(layoutfield.FormatAggregateField(check));
                } else if(layoutfield.IsStandard) {
                    sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                } else if(layoutfield.IsFirstLast) {
                    sbGridLine.Append(layoutfield.FormatFirstLastField(counters.GetPosition(LayoutLevelEnum.Payment)));
                } else if(layoutfield.IsCounter) {
                    sbGridLine.Append(layoutfield.FormatCounter(counters.CheckIndex));
                } else if(layoutfield.IsLineCounter) {
                    sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                } else if(layoutfield.IsRecordCounter) {
                    sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                } else {
                    sbGridLine.Append(layoutfield.FormatLayoutField(extractBase, check, imageFormatOptions));
                }

                check.WasExtracted = true;
                bolIsFirstLayoutField = false;
            }

            layoutText = sbGridLine.ToString();

            return(true);
        }

        internal bool FormatLayout(
            cExtract extractBase,
            cStub stub, 
            cExtractDef extractDef, 
            cExtractParms parms, 
            cStandardFields standardFields, 
            cImageFormatOptions imageFormatOptions,
            cCounters counters,
            out string layoutText) {

            StringBuilder sbGridLine = new StringBuilder();
            bool bolIsFirstLayoutField = true;

            foreach(cLayoutField layoutfield in this.LayoutFields) {

                if(!bolIsFirstLayoutField) {
                    sbGridLine.Append(ExtractDef.FieldDelim);
                }

                if(layoutfield.IsStatic) {
                    sbGridLine.Append(layoutfield.FormatStaticField());
                } else if(layoutfield.IsFiller) {
                    sbGridLine.Append(layoutfield.FormatFiller());
                } else if(layoutfield.IsAggregate) {
                    sbGridLine.Append(layoutfield.FormatAggregateField(stub));
                } else if(layoutfield.IsStandard) {
                    sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                } else if(layoutfield.IsFirstLast) {
                    sbGridLine.Append(layoutfield.FormatFirstLastField(counters.GetPosition(LayoutLevelEnum.Stub)));
                } else if(layoutfield.IsCounter) {
                    sbGridLine.Append(layoutfield.FormatCounter(counters.StubIndex));
                } else if(layoutfield.IsLineCounter) {
                    sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                } else if(layoutfield.IsRecordCounter) {
                    sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                } else {
                    sbGridLine.Append(layoutfield.FormatLayoutField(extractBase, stub, imageFormatOptions));
                }

                stub.WasExtracted = true;
                bolIsFirstLayoutField = false;
            }

            layoutText = sbGridLine.ToString();

            return(true);
        }

        internal bool FormatLayout(
            cExtract extractBase,
            cDocument document, 
            cExtractDef extractDef, 
            cExtractParms parms, 
            cStandardFields standardFields, 
            cImageFormatOptions imageFormatOptions,
            cCounters counters,
            out string layoutText) {

            StringBuilder sbGridLine = new StringBuilder();
            bool bolIsFirstLayoutField = true;

            foreach(cLayoutField layoutfield in this.LayoutFields) {

                if(!bolIsFirstLayoutField) {
                    sbGridLine.Append(ExtractDef.FieldDelim);
                }

                if(layoutfield.IsStatic) {
                    sbGridLine.Append(layoutfield.FormatStaticField());
                } else if(layoutfield.IsFiller) {
                    sbGridLine.Append(layoutfield.FormatFiller());
                } else if(layoutfield.IsAggregate) {
                    sbGridLine.Append(layoutfield.FormatAggregateField(document));
                } else if(layoutfield.IsStandard) {
                    sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                } else if(layoutfield.IsFirstLast) {
                    sbGridLine.Append(layoutfield.FormatFirstLastField(counters.GetPosition(LayoutLevelEnum.Document)));
                } else if(layoutfield.IsCounter) {
                    sbGridLine.Append(layoutfield.FormatCounter(counters.DocumentIndex));
                } else if(layoutfield.IsLineCounter) {
                    sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                } else if(layoutfield.IsRecordCounter) {
                    sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                } else {
                    sbGridLine.Append(layoutfield.FormatLayoutField(extractBase, document, imageFormatOptions));
                }

                document.WasExtracted = true;
                bolIsFirstLayoutField = false;
            }

            layoutText = sbGridLine.ToString();

            return(true);
        }

        internal bool FormatLayout(
            cExtract extractBase,
            cTransaction transaction, 
            cExtractParms parms, 
            cStandardFields standardFields,
            cImageFormatOptions imageFormatOptions,
            cCounters counters,
            cCounters totals,
            ref int checksIndex,
            ref int stubsIndex,
            ref int documentsIndex,
            out string[] layoutText) {

            StringBuilder sbGridLine;           // Return Value.
            bool bolIsFirstLayoutField = true;  // Is this the first field written for the row? (Used for column delimiters)
            cLayoutField layoutfield;
            int intCurrentLine = 0;             // How many lines have been written?           
            bool bolContinue;

            bool bolChecksFieldsWritten;
            bool bolStubsFieldsWritten;
            bool bolDocumentsFieldsWritten;

            FirstMidLast enmFirstMidLast;

            List<string> arLayoutText = new List<string>();
            
            // Establish the number of Layout Columns which have been defined for the 
            // Checks, Stubs, and Documents tables that are part of this Layout.
            //int intLocalChecksLayoutFieldCount = GetLocalLayoutItemCount(new string[]{"checks.", "checksdataentry."});
            //int intLocalStubsLayoutFieldCount = GetLocalLayoutItemCount(new string[]{"stubs.", "stubsdataentry."});
            //int intLocalDocumentsLayoutFieldCount = GetLocalLayoutItemCount(new string[]{"documents."});
            // WI 116545 : Hard-coded strings where incorrect with the current data model.
            int intLocalChecksLayoutFieldCount = GetLocalLayoutItemCount(new string[] { Support.PAYMENT_COLUMN_STRING_STARTER, "RecHubData.ChecksDataEntry.", "RecHubData.dimDDAs." });
            int intLocalStubsLayoutFieldCount = GetLocalLayoutItemCount(new string[] { Support.STUB_COLUMN_STRING_STARTER, "RecHubData.StubsDataEntry." });
            int intLocalDocumentsLayoutFieldCount = GetLocalLayoutItemCount(new string[] { Support.DOCUMENT_COLUMN_STRING_STARTER, "RecHubData.dimDocumentTypes." });

            // If there are Layout Columns defined for Checks, and Checks rows which have not yet been written.
            if(intLocalChecksLayoutFieldCount > 0 && checksIndex < transaction.Payments.Count) {
                bolContinue = true;
            // If there are Layout Columns defined for Stubs, and Stubs rows which have not yet been written.
            } else if(intLocalStubsLayoutFieldCount > 0 && stubsIndex < transaction.Stubs.Count) {
                bolContinue = true;
            // If there are Layout Columns defined for Documents, and Documents rows which have not yet been written.
            } else if(intLocalDocumentsLayoutFieldCount > 0 && documentsIndex < transaction.Documents.Count) {
                bolContinue = true;
            // If there are Layout Columns defined for Checks, and Use Checks Occurs has been selected, AND 
            // there are Layout Columns defined for Stubs, and Use Stubs Occurs has NOT been selected.
            } else if(intLocalChecksLayoutFieldCount > 0 && UsePaymentsOccursGroups && 
                      intLocalStubsLayoutFieldCount > 0 && !UseStubsOccursGroups) {
                bolContinue = true;
            // If there are Layout Columns defined for Checks, and Use Checks Occurs has been selected, AND
            // there are Layout Columns defined for Documents, and Use Documents Occurs has NOT been selected.
            } else if(intLocalChecksLayoutFieldCount > 0 && UsePaymentsOccursGroups && 
                      intLocalDocumentsLayoutFieldCount > 0 && !UseDocumentsOccursGroups) {
                bolContinue = true;
            // If there are Layout Columns defined for Stubs, and Use Stubs Occurs has been selected, AND
            // there are Layout Columns defined for Checks, and Use Checks Occurs has NOT been selected.
            } else if(intLocalStubsLayoutFieldCount > 0 && UseStubsOccursGroups && 
                      intLocalChecksLayoutFieldCount > 0 && !UsePaymentsOccursGroups) {
                bolContinue = true;
            // If there are Layout Columns defined for Stubs, and Use Stubs Occurs has been selected, AND
            // there are Layout Columns defined for Documents, and Use Documents Occurs has NOT been selected.
            } else if(intLocalStubsLayoutFieldCount > 0 && UseStubsOccursGroups && 
                      intLocalDocumentsLayoutFieldCount > 0 && !UseDocumentsOccursGroups) {
                bolContinue = true;
            // If there are Layout Columns defined for Documents, and Use Documents Occurs has been selected, AND
            // there are Layout Columns defined for Checks, and Use Checks Occurs has NOT been selected.
            } else if(intLocalDocumentsLayoutFieldCount > 0 && UseDocumentsOccursGroups && 
                      intLocalChecksLayoutFieldCount > 0 && !UsePaymentsOccursGroups) {
                bolContinue = true;
            // If there are Layout Columns defined for Documents, and Use Documents Occurs has been selected, AND
            // there are Layout Columns defined for Stubs, and Use Stubs Occurs has NOT been selected.
            } else if(intLocalDocumentsLayoutFieldCount > 0 && UseDocumentsOccursGroups && 
                      intLocalStubsLayoutFieldCount > 0 && !UseStubsOccursGroups) {                
                bolContinue = true;
            } else {
                bolContinue = false;
            }

            sbGridLine = new StringBuilder();

            while(bolContinue) {

                bolContinue = false;

                bolChecksFieldsWritten = false;
                bolStubsFieldsWritten = false;
                bolDocumentsFieldsWritten = false;

                if((intLocalChecksLayoutFieldCount <= 0 || checksIndex + (this.PaymentsOccursCount == 0 ? 1 : this.PaymentsOccursCount) >= transaction.Payments.Count) &&
                   (intLocalStubsLayoutFieldCount <= 0 || stubsIndex + (this.StubsOccursCount == 0 ? 1 : this.StubsOccursCount) >= transaction.Stubs.Count) &&
                   (intLocalDocumentsLayoutFieldCount <= 0 || documentsIndex + (this.DocumentsOccursCount == 0 ? 1 : this.DocumentsOccursCount) >= transaction.Documents.Count)) {
                    enmFirstMidLast = FirstMidLast.Last;
                } else if((intLocalChecksLayoutFieldCount == 0 || (checksIndex == 0 && transaction.Payments.Count > 0)) &&
                          (intLocalStubsLayoutFieldCount== 0 || (stubsIndex == 0 && transaction.Stubs.Count > 0)) &&
                          (intLocalDocumentsLayoutFieldCount == 0 || (documentsIndex == 0 && transaction.Documents.Count > 0))) {
                    enmFirstMidLast = FirstMidLast.First;
                } else {
                    enmFirstMidLast = FirstMidLast.Mid;
                }

                //TODO: Should this REALLY be Transaction Level?
                counters.IncrementLines(LayoutLevelEnum.Transaction);
                ++counters.TotalJointDetailRecords;
                // Populate accumulated totals object
                totals.IncrementLines(LayoutLevelEnum.Transaction);
                ++totals.TotalJointDetailRecords;

                if (this.LayoutFields.Length > 0) {
                    for(int i=0;i<this.LayoutFields.Length;++i) {

                        layoutfield = (cLayoutField)this.LayoutFields[i];

                        if(bolIsFirstLayoutField) {
                            bolIsFirstLayoutField = false;
                        } else {
                            sbGridLine.Append(ExtractDef.FieldDelim);
                        }

                        // Write Static Data
                        if(layoutfield.IsStatic) {
                            sbGridLine.Append(layoutfield.FormatStaticField());
                        } else if(layoutfield.IsFiller) {
                            sbGridLine.Append(layoutfield.FormatFiller());
                        // Write Aggregate Field
                        } else if(layoutfield.IsAggregate) {
                            sbGridLine.Append(layoutfield.FormatAggregateField(transaction));
                        // Write Standard Field
                        } else if(layoutfield.IsStandard) {
                            sbGridLine.Append(layoutfield.FormatStandardField(parms, standardFields));
                        // Write First/Mid/Last Field
                        } else if(layoutfield.IsFirstLast) {
                            sbGridLine.Append(layoutfield.FormatFirstLastField(enmFirstMidLast));
                        // Write Counter Field
                        } else if(layoutfield.IsCounter) {
                            sbGridLine.Append(layoutfield.FormatCounter(intCurrentLine));
                        // Write Line Counter Field
                        } else if(layoutfield.IsLineCounter) {
                            sbGridLine.Append(layoutfield.FormatLineCounter(counters));
                        // Write Record Counter Field
                        } else if(layoutfield.IsRecordCounter) {
                            sbGridLine.Append(layoutfield.FormatRecordCounter(counters));
                        } else if(layoutfield.UseOccursGroups) {
                            if (layoutfield.SimpleTableName.ToLower() == Support.PAYMENT_COLUMN_STRING_STARTER)
                            {

                                sbGridLine.Append(WriteOccursGroups(
                                    extractBase,
                                    imageFormatOptions,
                                    transaction.Payments,
                                    ref i,
                                    ref checksIndex,
                                    ref bolIsFirstLayoutField));

                            }
                            else if (layoutfield.SimpleTableName.ToLower() == Support.STUB_COLUMN_STRING_STARTER)
                            {

                                sbGridLine.Append(WriteOccursGroups(
                                    extractBase,
                                    imageFormatOptions,
                                    transaction.Stubs,
                                    ref i,
                                    ref stubsIndex,
                                    ref bolIsFirstLayoutField));

                            }
                            else if (layoutfield.SimpleTableName.ToLower() == Support.DOCUMENT_COLUMN_STRING_STARTER
                                || layoutfield.SimpleTableName.ToLower() == "rechubdata.dimdocumenttypes")
                            {

                                sbGridLine.Append(WriteOccursGroups(
                                    extractBase,
                                    imageFormatOptions,
                                    transaction.Documents,
                                    ref i,
                                    ref documentsIndex,
                                    ref bolIsFirstLayoutField));
                            }

                        } else {
                            if (layoutfield.SimpleTableName.ToLower() == Support.PAYMENT_COLUMN_STRING_STARTER
                                || layoutfield.SimpleTableName.ToLower() == "rechubdata.checksdataentry"
                                || layoutfield.SimpleTableName.ToLower() == "rechubdata.dimddas")
                            {

                                sbGridLine.Append(layoutfield.FormatJointDetailLayoutField(
                                    extractBase,
                                    transaction.Payments, 
                                    checksIndex, 
                                    imageFormatOptions));
                                bolChecksFieldsWritten = true;
                            }
                            else if (layoutfield.SimpleTableName.ToLower() == Support.STUB_COLUMN_STRING_STARTER
                                || layoutfield.SimpleTableName.ToLower() == "rechubdata.stubsdataentry")
                            {

                                sbGridLine.Append(layoutfield.FormatJointDetailLayoutField(
                                    extractBase, 
                                    transaction.Stubs, 
                                    stubsIndex, 
                                    imageFormatOptions));
                                bolStubsFieldsWritten = true;

                            }
                            else if (layoutfield.SimpleTableName.ToLower() == Support.DOCUMENT_COLUMN_STRING_STARTER
                                || layoutfield.SimpleTableName.ToLower() == "rechubdata.dimdocumenttypes")
                            {

                                sbGridLine.Append(layoutfield.FormatJointDetailLayoutField(
                                    extractBase, 
                                    transaction.Documents, 
                                    documentsIndex, 
                                    imageFormatOptions));
                                bolDocumentsFieldsWritten = true;

                            } else {
                                sbGridLine.Append(layoutfield.FormatLayoutField(
                                    extractBase, 
                                    transaction));
                            }
                        }
                    }
                }

                if(bolChecksFieldsWritten && checksIndex < transaction.Payments.Count) {
                    ++checksIndex;
                }
                if(bolStubsFieldsWritten && stubsIndex < transaction.Stubs.Count) {
                    ++stubsIndex;
                }
                if(bolDocumentsFieldsWritten && documentsIndex < transaction.Documents.Count) {
                    ++documentsIndex;
                }

                ++intCurrentLine;

                if((intLocalChecksLayoutFieldCount > 0 && checksIndex < transaction.Payments.Count) ||
                   (intLocalStubsLayoutFieldCount > 0 && stubsIndex < transaction.Stubs.Count)||
                   (intLocalDocumentsLayoutFieldCount > 0 && documentsIndex < transaction.Documents.Count)) {

                    bolContinue = true;
                }

                arLayoutText.Add(sbGridLine.ToString());
                sbGridLine = new StringBuilder();

                if(bolContinue) {
                    if(this.MaxRepeats > 0 && intCurrentLine >= this.MaxRepeats) {
                        bolContinue = false;
                    } else {
                        bolIsFirstLayoutField = true;
                    }
                }
            }

            if(!this.HasGlobalOccursGroups) {
                checksIndex = 0;
                stubsIndex = 0;
                documentsIndex = 0;
            }

            layoutText = arLayoutText.ToArray();

            return(arLayoutText.Count > 0);
        }

        private int GetLocalLayoutItemCount(string[] tablePrefix) {

            int intRetVal = 0;

            foreach(cLayoutField layoutfield in this.LayoutFields) {
                for(int i=0;i<tablePrefix.Length;++i) {
                    if(layoutfield.FieldName.ToLower().StartsWith(tablePrefix[i].ToLower())) {
                        ++intRetVal;
                        break;
                    }
                }
            }

            return(intRetVal);
        }

        private bool HasGlobalOccursGroups {
            get {

                bool bolRetVal = false;

                foreach(cLayout layout in ExtractDef.JointDetailLayouts) {
                    //foreach(cLayoutField layoutfield in layout.LayoutFields) {
                    //    if(layoutfield.OccursGroup > -1) {
                    //        bolRetVal = true;
                    //        break;
                                          
                    //    }
                    //}
                    //if(bolRetVal) {
                    //    break;
                    //}
                    if(layout.UseOccursGroups) {
                        bolRetVal = true;
                        break;
                    }
                }
                
                return(bolRetVal);
            }
        }

        private StringBuilder WriteOccursGroups(
            cExtract extractBase,
            cImageFormatOptions imageFormatOptions,
            OrderedDictionary items,
            ref int layoutFieldIndex,
            ref int itemIndex,
            ref bool isFirstLayoutField) {

            int k=layoutFieldIndex;
            StringBuilder sbRetVal = new StringBuilder();
            bool bolSupressIncrement = false;
            
            for(int j=0;j<this.LayoutFields[layoutFieldIndex].OccursCount;++j) {
                k=layoutFieldIndex;
                while(k <= this.LayoutFields.Length -1 && 
                      this.LayoutFields[k].SimpleTableName.ToLower() == this.LayoutFields[layoutFieldIndex].SimpleTableName.ToLower()) {

                    if(!isFirstLayoutField && (k > layoutFieldIndex||j > 0)) {
                        sbRetVal.Append(ExtractDef.FieldDelim);
                    } else {
                        isFirstLayoutField = false;
                    }

                    sbRetVal.Append(((cLayoutField)this.LayoutFields[k]).FormatJointDetailLayoutField(extractBase, items, itemIndex, imageFormatOptions));

                    ++k;
                }

                for(int l=k;l<this.LayoutFields.Length;++l) {
                    if(this.LayoutFields[layoutFieldIndex].SimpleTableName.ToLower() == this.LayoutFields[l].SimpleTableName.ToLower()) {
                        bolSupressIncrement = true;
                    }
                }

                if(!bolSupressIncrement) {
                    ++itemIndex;
                }
            }
            
            layoutFieldIndex=k-1;
            
            return(sbRetVal);
        }

        public override string ToString() {
            return this.LayoutName;
        }

        private void OnExceptionOccurred(object sender, Exception e) {
            if(OutputError != null) {
                OutputError(e);
            }
        }
    }
}
