﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 152321 BLR 08/15/2014
*   -Moved Configuration over to ExtractConfigurationSettings. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cImageFormatOptions {

        private ImageFileFormatEnum _ImageFileFormat = ImageFileFormatEnum.DoNotRetrieveImages;
        private string _ImageFileNameSingle = string.Empty;
        private string _ImageFileNamePerTran = string.Empty;
        private string _ImageFileNamePerBatch = string.Empty;
        private string _ImageFileProcDateFormat = string.Empty;
        private bool _ImageFileZeroPad = false;
        private bool _ImageFileBatchTypeFormatFull = false;
        private bool _IncludeImageFolderPath = false;
        private string _PrinterName = string.Empty;
        private string _FormattedImagePath = string.Empty;
        private string _QualifiedImagePath = string.Empty;

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        private bool _IncludeFooter = false;

        public cImageFormatOptions(
            ExtractConfigurationSettings settings,
            cExtractDef extractDef,
            cExtractParms parms) {

            _ImageFileFormat = extractDef.ImageFileFormat;
            _ImageFileNameSingle = extractDef.ImageFileNameSingle;
            _ImageFileNamePerTran = extractDef.ImageFileNamePerTran;
            _ImageFileNamePerBatch = extractDef.ImageFileNamePerBatch;
            _ImageFileProcDateFormat = extractDef.ImageFileProcDateFormat;
            _ImageFileZeroPad = extractDef.ImageFileZeroPad;
            _ImageFileBatchTypeFormatFull = extractDef.ImageFileBatchTypeFormatFull;
            _IncludeImageFolderPath = extractDef.IncludeImageFolderPath;
            _PrinterName = extractDef.PrinterName;

            _FormattedImagePath = FormatImageFilePath(extractDef, parms);
            _QualifiedImagePath = QualifyImagePath(settings, _FormattedImagePath);

            // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
            _IncludeFooter = extractDef.IncludeFooter;
        }

        public ImageFileFormatEnum ImageFileFormat {
            get {
                return(_ImageFileFormat);
            }
        }

        public string ImageFileNameSingle {
            get {
                return(_ImageFileNameSingle);
            }
        }

        public string ImageFileNamePerTran {
            get {
                return(_ImageFileNamePerTran);
            }
        }

        public string ImageFileNamePerBatch {
            get {
                return(_ImageFileNamePerBatch);
            }
        }

        public string ImageFileProcDateFormat {
            get {
                return(_ImageFileProcDateFormat);
            }
        }

        public bool ImageFileZeroPad {
            get {
                return(_ImageFileZeroPad);
            }
        }

        public bool ImageFileBatchTypeFormatFull {
            get {
                return(_ImageFileBatchTypeFormatFull);
            }
        }
        
        public bool IncludeImageFolderPath {
            get {
                return(_IncludeImageFolderPath);
            }
        }
        
        public string PrinterName {
            get {
                return(_PrinterName);
            }
        }
        
        public string FormattedImagePath {
            get {
                return(_FormattedImagePath);
            }
        }

        public string QualifiedImagePath {
            get {
                return(_QualifiedImagePath);
            }
        }

        private string FormatImageFilePath(
            cExtractDef extractDef,
            cExtractParms parms) {

            string strRetVal;

            if(parms.ImagePath.Length > 0) {
                strRetVal = parms.ImagePath;
            } else {
                strRetVal = extractDef.ImagePath;
            }

            return(strRetVal);
        }

        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        public bool IncludeFooter
        {
            get
            {
                return (_IncludeFooter);
            }
        }

        private string QualifyImagePath(
            ExtractConfigurationSettings settings, 
            string formattedImagePath) {
            
            return(settings.QualifyPath(formattedImagePath));
        }
    }
}
