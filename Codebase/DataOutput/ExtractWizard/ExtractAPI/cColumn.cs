using System;
using System.Data;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 05/30/2013
*   -Initial Version 
* WI 132009 BLR 03/06/2014
*   -Added DisplayName & Manual constructor. 
* WI 115856 BLR 06/24/2014    
*   -Added setters for each property.
*   -Altered the defaults for the displayName property - now only sets the
*    displayname while setting the columnname if the displayname has not 
*    already been set.    
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

	/// <summary>
	/// Summary description for cColumn.
	/// </summary>
	public class cColumn : IColumn {

        private string _TableName = string.Empty;
        private string _Schema = string.Empty;
        private string _ColumnName = string.Empty;
        private int _ColumnOrdinal = 0;
        private SqlDbType _Type = SqlDbType.VarChar;
        private int _Length = 0;
        private string _OverrideValue = string.Empty;
        // WI 132009 : Added displayname so we can differentiate same column names to different tables.
        private string _DisplayName = string.Empty;

        // WI 132009 : Added so we can intercept datareaders, and add in some manual data.
        public cColumn(string schema, string tablename, string columnname,
            string displayname, int columnordinal, SqlDbType type, int length)
        {
            _Schema = schema;
            _TableName = tablename;
            _ColumnName = columnname;
            _DisplayName = displayname;
            _ColumnOrdinal = columnordinal;
            _Type = type;
            _Length = length;
            IsDataEntry = false;
        }

        public cColumn(DataRow dr) 
        {

            if(dr.Table.Columns.Count > 0) 
            {
                foreach(DataColumn dc in dr.Table.Columns) 
                {
                    if(!dr.IsNull(dc)) 
                    {
                        switch(dc.ColumnName.ToLower()) 
                        {
                            case ("schema"):
                                _Schema = dr[dc].ToString();
                                break;
                            case ("tablename"):
                                _TableName = dr[dc].ToString();
                                break;
                            case("columnname"):
                                _ColumnName = dr[dc].ToString();
                                _DisplayName = string.IsNullOrWhiteSpace(_DisplayName) 
                                    ? dr[dc].ToString()
                                    : _DisplayName;
                                break;
                            case("displayname"):
                                _DisplayName = dr[dc].ToString();
                                break;
                            case("columnordinal"):
                                _ColumnOrdinal = (int)dr[dc];
                                break;
                            case("type"):
                                _Type = GetDbType(dr[dc].ToString());
                                break;
                            case("length"):
                                if((dr[dc]).GetType() == typeof(byte)) {
                                    _Length = Convert.ToInt32((byte)dr[dc]);
                                } else {

                                        _Length = (int)dr[dc];
                                        break;
                                }
                                break;
                            case ("sitebankid"):
                                SiteBankID = (int)dr[dc];
                                break;
                            case ("siteclientaccountid"):
                                SiteClientAccountID = (int)dr[dc];
                                break;
                            case ("paymentsource"):
                                PaymentSource = dr[dc].ToString();
                                break;
                            case ("workgroupname"):
                                WorkgroupName = dr[dc].ToString();
                                break;
                            case ("bankname"):
                                BankName = dr[dc].ToString();
                                break;
                            case ("isactive"):
                                IsActive = (bool)dr[dc];
                                break;
                        } // switch
                    }
                } // foreach
            } // if
		}

        public int SiteBankID { get; set; }
        public int SiteClientAccountID { get; set; }
        public string PaymentSource { get; set; }
        public string WorkgroupName { get; set; }
        public string BankName { get; set; }
        public bool IsActive { get; set; }


        public bool IsDataEntry { get; set; }

        public string Schema {
            get {
                return (_Schema);
            }
            set
            {
                _Schema = value;
            }
        }

        public string TableName
        {
            get
            {
                return (_TableName);
            }
            set
            {
                _TableName = value;
            }
        }

        // WI 132009 : Added displayname so we can differentiate same column names to different tables.
        public string DisplayName
        {
            get { return _DisplayName; }
            set { _DisplayName = value; }
        }

        public string ColumnName {
            get {
                return (_ColumnName);
            }
            set
            {
                _ColumnName = value;
            }
        }
        
        public SqlDbType Type {
            get {
                return (_Type);
            }
            set
            {
                _Type = value;
            }
        }

        public int Length {
            get {
                return (_Length);
            }
            set
            {
                _Length = value;
            }
        }

        private static SqlDbType GetDbType(string dbType) {

            SqlDbType retVal = SqlDbType.VarChar;

            switch(dbType.TrimEnd().ToUpper()) {
                case "INT":
                    retVal = SqlDbType.Int;
                    break;
                case "UNIQUEIDENTIFIER":
                    retVal = SqlDbType.UniqueIdentifier;
                    break;
                case "CHAR":
                    retVal = SqlDbType.Char;
                    break;
                case "DATETIME":
                    retVal = SqlDbType.DateTime;
                    break;
                case "TINYINT":
                    retVal = SqlDbType.TinyInt;
                    break;
                case "SMALLINT":
                    retVal = SqlDbType.SmallInt;
                    break;
                case "BIGINT":
                    retVal = SqlDbType.BigInt;
                    break;
                case "VARCHAR":
                    retVal = SqlDbType.VarChar;
                    break;
                case "T_OPERATORID":
                    retVal = SqlDbType.VarChar;
                    break;
                case "BIT":
                    retVal = SqlDbType.Bit;
                    break;
                case "MONEY":
                    retVal = SqlDbType.Money;
                    break;
                case "IMAGE":
                    retVal = SqlDbType.Image;
                    break;
                case "FLOAT":
                    retVal = SqlDbType.Float;
                    break;
                case "TIMESTAMP":
                    retVal = SqlDbType.Timestamp;
                    break;
                case "XML":
                    retVal = SqlDbType.Xml;
                    break;
                default:
                    retVal = SqlDbType.VarChar;
                    break;
            }
            
            return(retVal);
        }

        public SimpleGridColumnModel ToSimpleGridModel()
        {
            return new SimpleGridColumnModel()
            {
                BankID = this.SiteBankID,
                FieldName = this.ColumnName,
                BankName = this.BankName,
                PaymentSource = this.PaymentSource,
                UILabel = this.DisplayName,
                WorkgroupID = this.SiteClientAccountID,
                WorkgroupName = this.WorkgroupName,
                IsActive = this.IsActive
            };
        }
    }
}
