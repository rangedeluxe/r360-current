using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI 71865 JMC 06/21/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cExtractParms : IExtractParms {

        private string _SetupFile = string.Empty;
        private string _TraceField = string.Empty;
        private DateTime[] _RerunList = { };
        //private string Record = string.Empty;
        private string _TargetFile = string.Empty;
        private string _ImagePath = string.Empty;
        private long[] _BankID = { };
        private long[] _CustomerID = { };
        private long[] _ClientAccountID = { };
        private long[] _BatchID = { };
        private DateTime _ProcessingDateFrom;
        private DateTime _ProcessingDateTo;
        private DateTime _DepositDateFrom;
        private DateTime _DepositDateTo;
        private int _UserID = -1;
        private long _ExtractSequenceNumber; // CR 13806 10/04/2005 DJK
        //private long CallerHandle;
        //private long CallerThreadID;
        private Guid _ExtractAuditID = Guid.Empty; // CR 14003 9/17/2005 DJK - Added this property
        private string _ExtractFileName = string.Empty;
        private string _LogFilePath = string.Empty;
        public int OutputMode { get; set; }
        public string SetupFile {
            get {
                return (_SetupFile);
            }
            set {
                _SetupFile = value;
            }
        }

        public string TraceField {
            get {
                return (_TraceField);
            }
            set {
                _TraceField = value;
            }
        }

        public DateTime[] RerunList {
            get {
                return (_RerunList);
            }
            set {
                _RerunList = value;
            }
        }

        //public string Record = string.Empty;
        public string TargetFile {
            get {
                return (_TargetFile);
            }
            set {
                _TargetFile = value;
            }
        }

        public string ImagePath {
            get {
                return (_ImagePath);
            }
            set {
                _ImagePath = value;
            }
        }

        public long[] BankID {
            get {
                return (_BankID);
            }
            set {
                _BankID = value;
            }
        }

        public long[] LockboxID {
            get {
                return (_ClientAccountID);
            }
            set {
                _ClientAccountID = value;
            }
        }

        public long[] BatchID {
            get {
                return (_BatchID);
            }
            set {
                _BatchID = value;
            }
        }

        public DateTime ProcessingDateFrom {
            get {
                return (_ProcessingDateFrom);
            }
            set {
                _ProcessingDateFrom = value;
            }
        }

        public DateTime ProcessingDateTo {
            get {
                return (_ProcessingDateTo);
            }
            set {
                _ProcessingDateTo = value;
            }
        }

        public DateTime DepositDateFrom {
            get {
                return (_DepositDateFrom);
            }
            set {
                _DepositDateFrom = value;
            }
        }

        public DateTime DepositDateTo {
            get {
                return (_DepositDateTo);
            }
            set {
                _DepositDateTo = value;
            }
        }

        public int UserID {
            get {
                return (_UserID);
            }
            set {
                _UserID = value;
            }
        }

        public long ExtractSequenceNumber {
            get {
                return (_ExtractSequenceNumber);
            }
            set {
                _ExtractSequenceNumber = value;
            }
        }

        //public long CallerHandle;
        //public long CallerThreadID;
        public Guid ExtractAuditID {
            get {
                return (_ExtractAuditID);
            }
            set {
                _ExtractAuditID = value;
            }
        }

        public string ExtractFileName {
            get {
                return (_ExtractFileName);
            }
            set {
                _ExtractFileName = value;
            }
        }

        public string LogFilePath {
            get {
                return (_LogFilePath);
            }
            set {
                _LogFilePath = value;
            }
        }


        public string BankIDString {
            get {
                string strRetVal = string.Empty;
                if(this.BankID.Length > 0) {
                    for(int i=0;i<this.BankID.Length;++i) {
                        strRetVal += this.BankID[i].ToString();
                        if(i < this.BankID.Length - 1) {
                            strRetVal += ", ";
                        }
                    }
                }
                return(strRetVal);
            }
        }

        public string LockboxIDString {
            get {
                string strRetVal = string.Empty;
                if(this.LockboxID.Length > 0) {
                    for(int i=0;i<this.LockboxID.Length;++i) {
                        strRetVal += this.LockboxID[i].ToString();
                        if(i < this.LockboxID.Length - 1) {
                            strRetVal += ", ";
                        }
                    }
                }
                return(strRetVal);
            }
        }

        public string BatchIDString {
            get {
                string strRetVal = string.Empty;
                if(this.BatchID.Length > 0) {
                    for(int i=0;i<this.BatchID.Length;++i) {
                        strRetVal += this.BatchID[i].ToString();
                        if(i < this.BatchID.Length - 1) {
                            strRetVal += ", ";
                        }
                    }
                }
                return(strRetVal);
            }
        }

        public string ProcessingDateString {
            get {
                string strRetVal = string.Empty;

                string strProcessingDateFrom;
                string strProcessingDateTo;

                if(this.ProcessingDateFrom.TimeOfDay == TimeSpan.Zero) {
                    strProcessingDateFrom = this.ProcessingDateFrom.ToString("M/d/yyyy");
                } else {
                    strProcessingDateFrom = this.ProcessingDateFrom.ToString("M/d/yyyy hh:mm:ss tt");
                }

                if(this.ProcessingDateTo.TimeOfDay == TimeSpan.Zero) {
                    strProcessingDateTo = this.ProcessingDateTo.ToString("M/d/yyyy");
                } else {
                    strProcessingDateTo = this.ProcessingDateTo.ToString("M/d/yyyy hh:mm:ss tt");
                }

                if(this.ProcessingDateFrom > DateTime.MinValue && this.ProcessingDateTo == DateTime.MinValue) {
                    strRetVal = strProcessingDateFrom;
                } else if(this.ProcessingDateFrom > DateTime.MinValue && this.ProcessingDateTo > DateTime.MinValue) {
                    strRetVal = strProcessingDateFrom + "-" + strProcessingDateTo;
                } else if(this.ProcessingDateFrom == DateTime.MinValue && this.ProcessingDateTo > DateTime.MinValue) {
                    strRetVal = strProcessingDateTo;
                } else {
                    strRetVal = string.Empty;
                }
                return(strRetVal);
            }
        }

        public int ProcessingDateFromKey {
            get {
                if(ProcessingDateFrom != null && ProcessingDateFrom > DateTime.MinValue) {
                    return (Convert.ToInt32(ProcessingDateFrom.ToString("yyyyMMdd")));
                } else {
                    return (0);
                }
            }
        }

        public int ProcessingDateToKey {
            get {
                if(ProcessingDateTo != null && ProcessingDateTo > DateTime.MinValue) {
                    return (Convert.ToInt32(ProcessingDateTo.ToString("yyyyMMdd")));
                } else {
                    return (0);
                }
            }
        }

        public string DepositDateString {
            get {
                string strRetVal = string.Empty;
                string strDepositDateFrom;
                string strDepositDateTo;

                if(this.DepositDateFrom.TimeOfDay == TimeSpan.Zero) {
                    strDepositDateFrom = this.DepositDateFrom.ToString("M/d/yyyy");
                } else {
                    strDepositDateFrom = this.DepositDateFrom.ToString("M/d/yyyy hh:mm:ss tt");
                }

                if(this.DepositDateTo.TimeOfDay == TimeSpan.Zero) {
                    strDepositDateTo = this.DepositDateTo.ToString("M/d/yyyy");
                } else {
                    strDepositDateTo = this.DepositDateTo.ToString("M/d/yyyy hh:mm:ss tt");
                }

                if(this.DepositDateFrom > DateTime.MinValue && this.DepositDateTo == DateTime.MinValue) {
                    strRetVal = strDepositDateFrom;
                } else if(this.DepositDateFrom > DateTime.MinValue && this.DepositDateTo > DateTime.MinValue) {
                    strRetVal = strDepositDateFrom + "-" + strDepositDateTo;
                } else if(this.DepositDateFrom == DateTime.MinValue && this.DepositDateTo > DateTime.MinValue) {
                    strRetVal = strDepositDateTo;
                } else {
                    strRetVal = string.Empty;
                }
                return(strRetVal);
            }
        }

        public int DepositDateFromKey {
            get {
                if(DepositDateFrom != null && DepositDateFrom > DateTime.MinValue) {
                    return (Convert.ToInt32(DepositDateFrom.ToString("yyyyMMdd")));
                } else {
                    return (0);
                }
            }
        }

        public int DepositDateToKey {
            get {
                if(DepositDateTo != null && DepositDateTo > DateTime.MinValue) {
                    return (Convert.ToInt32(DepositDateTo.ToString("yyyyMMdd")));
                } else {
                    return (0);
                }
            }
        }

        public static bool ParseDateString(string DateString, out DateTime StartDate, out DateTime EndDate) {

            DateTime dteTemp;
            DateTime dteStartDate = DateTime.MinValue;
            DateTime dteEndDate = DateTime.MinValue;
            int iPos;
            bool bolRetVal = false;

            if(DateTime.TryParse(DateString, out dteTemp)) {
                dteStartDate = dteTemp;
                dteEndDate = dteTemp;
                bolRetVal = true;
            } else {
                iPos = DateString.IndexOf('-');
                if(iPos > -1) {
                    if(DateTime.TryParse(DateString.Substring(0, iPos) , out dteTemp)) {
                        dteStartDate = dteTemp;
                    }
                    if(DateTime.TryParse(DateString.Substring(iPos + 1) , out dteTemp)) {
                        dteEndDate = dteTemp;
                    }
                    bolRetVal = true;
                }
            }

            StartDate = dteStartDate;
            EndDate = dteEndDate;
            
            return(bolRetVal);
        }

        public static long[] ParseLongs(string StringOfLongs) {

            ArrayList arRetVal = new ArrayList();
            string strTemp = string.Empty;

            if(StringOfLongs.Length > 0) {
                for(int i=0;i<StringOfLongs.Length;++i) {
                    // Added in 45 for the '-' character.  Need to handle negative IDs as well.
                    if( (char)StringOfLongs[i] == 45 || ((char)StringOfLongs[i] >= 48 && (char)StringOfLongs[i] <=57)) {
                        strTemp += StringOfLongs[i];
                    } else {
                        if(strTemp.Length > 0) {
                            arRetVal.Add(long.Parse(strTemp));
                            strTemp = string.Empty;
                        }
                    }
                }

                if(strTemp.Length > 0) {
                    arRetVal.Add(long.Parse(strTemp));
                }
            }

            return((long[])arRetVal.ToArray(typeof(long)));
        }

        public static DateTime[] ParseDates(string stringOfDates) {

            ArrayList arRetVal = new ArrayList();
            DateTime dteTemp;
            
            string[] arTemp = stringOfDates.Split(',');
            foreach(string datestring in arTemp) {
                if(DateTime.TryParse(datestring, out dteTemp)) {
                    arRetVal.Add(dteTemp);
                }
            }

            return((DateTime[])arRetVal.ToArray(typeof(DateTime)));
        }
    }
}
