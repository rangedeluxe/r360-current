using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
* WI 143040 BLR 05/22/2014
*   - Removed dependence on the composite cBatchKey, only BatchID is utilized. 
*   - Changed all BatchIDs to long. 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI {

    public class cExtractRunInfo {

        private DateTime _RunDate;
        private int _BankID;
        private int _ClientAccountID;
        private long _BatchID;
        private int _ImmutableDateKey;
        private int _CheckCount;
        private decimal _CheckAmount;

        private CultureInfo _EN_US = new CultureInfo("en-US"); 

        public DateTime RunDate {
            get {
                return (_RunDate);
            }
        }

        public long BankID {
            get {
                return (_BankID);
            }
        }

        public int ClientAccountID {
            get {
                return (_ClientAccountID);
            }
        }

        public long BatchID {
            get {
                return (_BatchID);
            }
        }

        public int ImmutableDateKey {
            get {
                return (_ImmutableDateKey);
            }
        }

        public DateTime ImmutableDate {
            get {
                DateTime dteTemp;
                if(_ImmutableDateKey > 0 && DateTime.TryParseExact(_ImmutableDateKey.ToString(), "yyyyMMdd", _EN_US, DateTimeStyles.None, out dteTemp)) {
                    return (dteTemp);
                } else {
                    return (DateTime.MinValue);
                }
            }
        }

        public int CheckCount {
            get {
                return (_CheckCount);
            }
        }

        public decimal CheckAmount {
            get {
                return (_CheckAmount);
            }
        }

        /// <summary>
        /// Loads class with values contained within an ADO.NET DataRow object.
        /// </summary>
        /// <param name="dr">DataRow containing data used to populate this object</param>
        /// <returns>Boolean indicating success or failure.</returns>
        public bool LoadDataRow(DataRow dr) {
            if(dr.Table.Columns.Count>0) {
                foreach(DataColumn col in dr.Table.Columns) {
                    if(!dr.IsNull(col)) {
                        switch(col.ColumnName.ToLower()) {
                            case("rundate"):
                                _RunDate=(DateTime)dr[col];
                                break;
                            case("sitebankid"):
                                _BankID = (int)dr[col];
                                break;
                            case("siteclientaccountid"):
                                _ClientAccountID = (int)dr[col];
                                break;
                            case("batchid"):
                                _BatchID = (long)dr[col];
                                break;
                            case("immutabledatekey"):
                                _ImmutableDateKey = (int)dr[col];
                                break;
                            case("checkcount"):
                                _CheckCount = (int)dr[col];
                                break;
                            case("checkamount"):
                                _CheckAmount = (Decimal)dr[col];
                                break;
                         }
                    }
                }
                return(true);
            } else {
                return(false);
            }
        }
    }
}
