﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Microsoft.Reporting.WinForms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractSetupRpt {

    public partial class frmViewSetupRpt : Form {

        private cExtractDef _ExtractDef;
        private int _Index = 0;
        private int _OrderByColumnIndex = 0;
        private int _LimitItemIndex = 0;

        public frmViewSetupRpt(cExtractDef extractDef) {
          
            InitializeComponent();

            _ExtractDef = extractDef;
        }

        private void frmViewSetupRpt_Load(object sender, EventArgs e) {

            this.cExtractDefBindingSource.DataSource = _ExtractDef;
            
            this.reportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessingEventHandler);

            this.reportViewer1.RefreshReport();
            
            this.reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
        }

        private void LocalReport_SubreportProcessingEventHandler(
            object sender, 
            SubreportProcessingEventArgs e) {

            List<CustomParm> arTableNames;

            switch(e.ReportPath) {
                case "rptLayout":
                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLayout", _ExtractDef.AllLayouts));
                    _Index = 0;
                    break;
                case "rptLayoutSummary":
                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLayout", _ExtractDef.AllLayouts));
                    break;
                case "rptLayoutFields":
                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLayoutField", _ExtractDef.AllLayouts[_Index].LayoutFields));
                    ++_Index;
                    break;
                case "rptOrderByColumns":
                
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Bank && _ExtractDef.BankOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.BankOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Bank;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.ClientAccount && _ExtractDef.LockboxOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.LockboxOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.ClientAccount;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Batch && _ExtractDef.BatchOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.BatchOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Batch;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Transaction && _ExtractDef.TransactionsOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.TransactionsOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Transaction;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Payment && _ExtractDef.PaymentsOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.PaymentsOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Payment;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Stub && _ExtractDef.StubsOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.StubsOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Stub;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Document && _ExtractDef.DocumentsOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.DocumentsOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Document;
                        return;
                    }

                    break;

                case "rptOrderByColumnsPanel":

                    arTableNames = new List<CustomParm>();
                    
                    if(_ExtractDef.BankOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Bank"));
                    }
                    if(_ExtractDef.CustomerOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Customer"));
                    }
                    if(_ExtractDef.LockboxOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Lockbox"));
                    }
                    if(_ExtractDef.BatchOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Batch"));
                    }
                    if(_ExtractDef.TransactionsOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Transactions"));
                    }
                    if(_ExtractDef.PaymentsOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Checks"));
                    }
                    if(_ExtractDef.StubsOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Stubs"));
                    }
                    if(_ExtractDef.DocumentsOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Documents"));
                    }
                    
                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractSetupRpt_CustomParm", arTableNames));

                    break;

                case "rptLimitItems":
               
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Bank && _ExtractDef.BankLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.BankLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Bank;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.ClientAccount && _ExtractDef.LockboxLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.LockboxLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.ClientAccount;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Batch && _ExtractDef.BatchLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.BatchLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Batch;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Transaction && _ExtractDef.TransactionsLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.TransactionsLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Transaction;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Payment && _ExtractDef.PaymentsLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.PaymentsLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Payment;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Stub && _ExtractDef.StubsLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.StubsLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Stub;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Document && _ExtractDef.DocumentsLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.DocumentsLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Document;
                        return;
                    }

                    break;

                case "rptLimitItemsPanel":

                    arTableNames = new List<CustomParm>();

                    if(_ExtractDef.BankLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Bank"));
                    }
                    if(_ExtractDef.LockboxLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Lockbox"));
                    }
                    if(_ExtractDef.BatchLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Batch"));
                    }
                    if(_ExtractDef.TransactionsLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Transactions"));
                    }
                    if(_ExtractDef.PaymentsLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Checks"));
                    }
                    if(_ExtractDef.StubsLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Stubs"));
                    }
                    if(_ExtractDef.DocumentsLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Documents"));
                    }

                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractSetupRpt_CustomParm", arTableNames));

                    break;

                default:
                    break;
            }
        }
    }

    public class CustomParm {

        private string _Value = string.Empty;

        public CustomParm(string stringValue) {
            _Value = stringValue;
        }

        public string Value {
            get {
                return(_Value);
            }
        }
    }
}
