﻿using System;
using System.IO;
using System.Data;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

using WFS.RecHub.DataOutputToolkit.Extract.ExtractAPI;
using WFS.RecHub.DataOutputToolkit.Extract.ExtractLib;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Extract.ExtractSetupRpt {

    public class cExtractSetupRpt {

        private int _PageIndex;
        private IList<Stream> _Streams;
        private List<string> _StreamFileNames = new List<string>();

        private cExtractDef _ExtractDef;
        private int _Index = 0;
        private int _OrderByColumnIndex = 0;
        private int _LimitItemIndex = 0;

        private System.Windows.Forms.BindingSource _ExtractDefBindingSource;

        // Create a local report for Report.rdlc, load the data,
        //    export the report to an .emf file, and print it.
        public void PrintRpt(
            cExtractDef extractDef, 
            string printerName) {

            Export(GenerateReport(extractDef));

            _PageIndex = 0;

            Print(printerName);
        }

        private LocalReport GenerateReport(
            cExtractDef extractDef) {

            LocalReport report;
            ReportDataSource reportDataSource1;

            report = new LocalReport();
            report.ReportEmbeddedResource = "WFS.RecHub.DataOutputToolkit.Extract.ExtractSetupRpt.rptExtractSetup.rdlc";

            _ExtractDef = extractDef;

            // 
            // _ExtractDefBindingSource
            // 
            _ExtractDefBindingSource = new System.Windows.Forms.BindingSource();
            ((System.ComponentModel.ISupportInitialize)_ExtractDefBindingSource).BeginInit();

            // Bind the cExtractDef object to the DataSource.
            _ExtractDefBindingSource.DataSource = _ExtractDef;

            ((System.ComponentModel.ISupportInitialize)_ExtractDefBindingSource).EndInit();

            reportDataSource1 = new ReportDataSource();
            reportDataSource1.Name = "IntegraPAY_Delivery_Extract_ExtractAPI_cExtractDef";
            reportDataSource1.Value = _ExtractDefBindingSource;
            report.DataSources.Add(reportDataSource1);

            report.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessingEventHandler);

            return(report);
        }

        private void LocalReport_SubreportProcessingEventHandler(
            object sender, 
            SubreportProcessingEventArgs e) {

            List<CustomParm> arTableNames;

            switch(e.ReportPath) {
                case "rptLayout":
                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLayout", _ExtractDef.AllLayouts));
                    _Index = 0;
                    break;
                case "rptLayoutSummary":
                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLayout", _ExtractDef.AllLayouts));
                    break;
                case "rptLayoutFields":
                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLayoutField", _ExtractDef.AllLayouts[_Index].LayoutFields));
                    ++_Index;
                    break;
                case "rptOrderByColumns":
                
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Bank && _ExtractDef.BankOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.BankOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Bank;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.ClientAccount && _ExtractDef.LockboxOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.LockboxOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.ClientAccount;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Batch && _ExtractDef.BatchOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.BatchOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Batch;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Transaction && _ExtractDef.TransactionsOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.TransactionsOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Transaction;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Payment && _ExtractDef.PaymentsOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.PaymentsOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Payment;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Stub && _ExtractDef.StubsOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.StubsOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Stub;
                        return;
                    }
                    if(_OrderByColumnIndex < (int)LayoutLevelEnum.Document && _ExtractDef.DocumentsOrderByColumns.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cOrderByColumn", _ExtractDef.DocumentsOrderByColumns));
                        _OrderByColumnIndex = (int)LayoutLevelEnum.Document;
                        return;
                    }

                    break;

                case "rptOrderByColumnsPanel":

                    arTableNames = new List<CustomParm>();
                    
                    if(_ExtractDef.BankOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Bank"));
                    }
                    if(_ExtractDef.CustomerOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Customer"));
                    }
                    if(_ExtractDef.LockboxOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Lockbox"));
                    }
                    if(_ExtractDef.BatchOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Batch"));
                    }
                    if(_ExtractDef.TransactionsOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Transactions"));
                    }
                    if(_ExtractDef.PaymentsOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Checks"));
                    }
                    if(_ExtractDef.StubsOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Stubs"));
                    }
                    if(_ExtractDef.DocumentsOrderByColumns.Length > 0) {
                        arTableNames.Add(new CustomParm("Documents"));
                    }
                    
                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractSetupRpt_CustomParm", arTableNames));

                    break;

                case "rptLimitItems":
               
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Bank && _ExtractDef.BankLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.BankLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Bank;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.ClientAccount && _ExtractDef.LockboxLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.LockboxLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.ClientAccount;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Batch && _ExtractDef.BatchLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.BatchLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Batch;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Transaction && _ExtractDef.TransactionsLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.TransactionsLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Transaction;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Payment && _ExtractDef.PaymentsLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.PaymentsLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Payment;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Stub && _ExtractDef.StubsLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.StubsLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Stub;
                        return;
                    }
                    if(_LimitItemIndex < (int)LayoutLevelEnum.Document && _ExtractDef.DocumentsLimitItems.Length > 0) {
                        e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractAPI_cLimitItem", _ExtractDef.DocumentsLimitItems));
                        _LimitItemIndex = (int)LayoutLevelEnum.Document;
                        return;
                    }

                    break;

                case "rptLimitItemsPanel":

                    arTableNames = new List<CustomParm>();

                    if(_ExtractDef.BankLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Bank"));
                    }
                    if(_ExtractDef.LockboxLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Lockbox"));
                    }
                    if(_ExtractDef.BatchLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Batch"));
                    }
                    if(_ExtractDef.TransactionsLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Transactions"));
                    }
                    if(_ExtractDef.PaymentsLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Checks"));
                    }
                    if(_ExtractDef.StubsLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Stubs"));
                    }
                    if(_ExtractDef.DocumentsLimitItems.Length > 0) {
                        arTableNames.Add(new CustomParm("Documents"));
                    }

                    e.DataSources.Add(new ReportDataSource("IntegraPAY_Delivery_Extract_ExtractSetupRpt_CustomParm", arTableNames));

                    break;

                default:
                    break;
            }
        }

        // Routine to provide to the report renderer, in order to
        //    save an image for each page of the report.
        private Stream CreateStream(
            string name,
            string fileNameExtension, 
            Encoding encoding,
            string mimeType, 
            bool willSeek) {

            Stream stream;
            string strTempFileName;

            strTempFileName = Path.GetTempFileName();

            stream = new FileStream(strTempFileName, FileMode.Create);

            _Streams.Add(stream);
            _StreamFileNames.Add(strTempFileName);

            return stream;
        }

        // Export the given report as an EMF (Enhanced Metafile) file.
        private void Export(LocalReport report) {

            Warning[] warnings;

            string deviceInfo = "<DeviceInfo>" +
                                "  <OutputFormat>EMF</OutputFormat>" +
                                "  <PageWidth>8.5in</PageWidth>" +
                                "  <PageHeight>11in</PageHeight>" +
                                "  <MarginTop>0.25in</MarginTop>" +
                                "  <MarginLeft>0.25in</MarginLeft>" +
                                "  <MarginRight>0.25in</MarginRight>" +
                                "  <MarginBottom>0.25in</MarginBottom>" +
                                "</DeviceInfo>";

            _Streams = new List<Stream>();

            report.Render("Image", deviceInfo, CreateStream, out warnings);

            foreach (Stream stream in _Streams) {
                stream.Position = 0;
            }
        }

        // Handler for PrintPageEvents
        private void PrintPage(
            object sender, 
            PrintPageEventArgs ev) {

            using (var pageImage = new Metafile(_Streams[_PageIndex]))
            {
                ev.Graphics.DrawImage(pageImage, ev.PageBounds);
            }

            _PageIndex++;
            ev.HasMorePages = (_PageIndex < _Streams.Count);
        }

        private void Print(string printerName) {

            string msg;

            if(_Streams == null || _Streams.Count == 0) {
                return;
            }

            using (var printDoc = new PrintDocument())
            {
                printDoc.PrinterSettings.PrinterName = printerName;

                if (!printDoc.PrinterSettings.IsValid)
                {

                    msg = String.Format("Can't find printer \"{0}\".", printerName);
                    MessageBox.Show(msg, "Print Error");

                    return;
                }

                printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                printDoc.Print();
            }
        }


        public void Dispose() {

            if (_Streams != null) {

                foreach(Stream stream in _Streams) {
                    stream.Close();
                }

                _Streams = null;
            }

            foreach(string filename in _StreamFileNames) {
                if(File.Exists(filename)) {
                    File.Delete(filename);
                }
            }
        }
    }
}
