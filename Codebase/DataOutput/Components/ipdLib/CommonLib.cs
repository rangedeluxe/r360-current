using System;
using System.IO;
using System.Xml;
using System.Drawing;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    /// <summary>
    /// ipo Common function library.
    /// </summary>
    public class CommonLib {
    
        /// <summary></summary>
        public const string CRLF = "\r\n";

        /// <summary></summary>
        public const string INISECTION_IMAGE_SERVICE = "ipoImageSvc";
        /// <summary></summary>
        public const string INISECTION_DECISIONING_API = "ipoDecisioningAPI";
        /// <summary></summary>
        public const string INIFILE_NAME = "IPOnline.ini";
        /// <summary></summary>
        public const string LOGO_INIFILE_NAME = "IPOnline.ini";
        /// <summary></summary>
        public const string INVALID_FILE_NAME_CHARS = @"\/:*?""<>|";

        /// <summary>
        /// Returns the path of the executing assembly including the 
        /// executable name.
        /// </summary>
        public static string AppExePath {
            get { 
                return System.Reflection.Assembly.GetCallingAssembly().Location; 
            }
        }

        /// <summary>
        /// Returns the executing assembly's executable name without the path.
        /// </summary>
        public static string AppExeName
        { 
            get 
            { 
                int intPos=AppExePath.LastIndexOf("\\");
                return(AppExePath.Substring(intPos+1
                                          , AppExePath.Length-intPos-1));
            }
        }
 
        /// <summary>
        /// Returns the application's path without the executable name.
        /// </summary>
        public static string AppPath { 
            get { 
                return AppExePath.Substring(0
                                          , AppExePath.Length - AppExeName.Length); 
            }
        }

        /// <summary>
        /// Accepts a single character and returns a string with that  character 
        /// rapeated n number of times.
        /// </summary>
        /// <param name="charValue"></param>
        /// <param name="timesRepeated"></param>
        /// <returns></returns>
        public static string repeatChar(char charValue
                                      , long timesRepeated) {
            string strRetVal="";

            for(long i=0;i<timesRepeated;++i)
                { strRetVal+=charValue; }

            return strRetVal;
        }


        /// <summary>
        /// Accepts the full path to a readable (presumably text) file.  returns 
        /// the number of lines contained in that file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static long countFileLines(string filePath) {

            string strTemp = "";
            long lngLineCount=0;

            System.IO.TextReader tr = System.IO.File.OpenText(filePath);

            while((strTemp = tr.ReadLine()) != null)   //(tr.Peek())
            {
                ++lngLineCount;
            } 

            tr.Close();
            tr=null;

            return lngLineCount;
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static long NVL(ref System.Data.SqlClient.SqlDataReader dr
                               , string fieldName
                               , long alternateVal) {
            if (!(dr.IsDBNull(dr.GetOrdinal(fieldName))))
                { return dr.GetInt64(dr.GetOrdinal(fieldName)); }
            else
                { return alternateVal; }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static string NVL(ref System.Data.SqlClient.SqlDataReader dr
                               , string fieldName
                               , string alternateVal) {
            if (!(dr.IsDBNull(dr.GetOrdinal(fieldName))))
                { return dr.GetString(dr.GetOrdinal(fieldName)); }
            else
                { return alternateVal; }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static int NVL(ref System.Data.SqlClient.SqlDataReader dr
                            , string fieldName
                            , int alternateVal)
        {
            if (!(dr.IsDBNull(dr.GetOrdinal(fieldName))))
                { return dr.GetInt32(dr.GetOrdinal(fieldName)); }
            else
                { return alternateVal; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static short NVL(ref System.Data.DataRow dr
                            , string fieldName
                            , short alternateVal)
        {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (short)dr[fieldName]; }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static string NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , string alternateVal) {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (string) dr[fieldName]; }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static Decimal NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , Decimal alternateVal) {

            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (Decimal) dr[fieldName]; }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static Byte NVL(ref System.Data.DataRow dr
                             , string fieldName
                             , Byte alternateVal) {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (Byte) dr[fieldName]; }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original 
        /// value is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static bool NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , bool alternateVal) {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (bool) dr[fieldName]; }
        }



        /// <summary>
        /// Inspects the value, and returns an alternate value if the original value 
        /// is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static int NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , int alternateVal) {
            if (dr.IsNull(fieldName))
                { return alternateVal; }
            else
                { return (int) dr[fieldName]; }
        }

        /// <summary>
        /// Inspects the value, and returns an alternate value if the original value 
        /// is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static Guid NVL(ref System.Data.DataRow dr
                               , string fieldName
                               , Guid alternateVal) {
            if (dr.IsNull(fieldName))
                { return(alternateVal); }
            else
                { return new Guid(dr[fieldName].ToString()); }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original value 
        /// is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static DateTime NVL(ref System.Data.DataRow dr
                                 , string fieldName
                                 , DateTime alternateVal) {
            if (dr.IsNull(fieldName))
                { return(alternateVal); }
            else
                { return System.Convert.ToDateTime(dr[fieldName]); }
        }


        /// <summary>
        /// Inspects the value, and returns an alternate value if the original value 
        /// is evaluated as NULL.
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="fieldName"></param>
        /// <param name="alternateVal"></param>
        /// <returns></returns>
        public static double NVL(ref System.Data.DataRow dr
                                 , string fieldName
                                 , double alternateVal) {
            if (dr.IsNull(fieldName))
                { return(alternateVal); }
            else
                { return System.Convert.ToDouble(dr[fieldName]); }
        }


        /// <summary>
        /// Ensures that a UNC path is closed with a backslash "\".
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string cleanPath(string path) {
            string strRetVal = path;

            if (path.Length > 0)
            {
                if (path.Substring(path.Length-1,1) != "\\") 
                    { strRetVal += "\\"; }
            }
            else
                { strRetVal="\\"; }

            return strRetVal;
        }


        /// <summary>
        /// Ensures that a GUID variable is opened and closed by curly braces. {}
        /// </summary>
        /// <param name="GUID"></param>
        /// <param name="keepCurlyBraces"></param>
        /// <returns></returns>
        public static string cleanGUID(string GUID
                                     , bool keepCurlyBraces) {
            string strRetVal = GUID;

            try
            {
                if (keepCurlyBraces)
                {
                    if (GUID.Length>2)
                    {
                        if (GUID.Substring(0,1) != "{")
                            { strRetVal="{"+strRetVal; }

                        if (GUID.Substring(GUID.Length-1,1)!="}")
                            { strRetVal+="}"; }
                    }
                }
                else
                {
                    if (GUID.Length > 1)
                    {
                        if (strRetVal.Substring(0,1) == "{")
                            { strRetVal=strRetVal.Substring(1,strRetVal.Length-2); }
        
                        if (strRetVal.Length > 1)
                        {
                            if (strRetVal.Substring(strRetVal.Length-1,1) == "}")
                                { strRetVal=strRetVal.Substring(0,strRetVal.Length-2); }
                        }
                    }
                }
            }
            catch
            {
                strRetVal = "";
            }

            return strRetVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static long LOF(string filePath) {
            long lngRetVal = 0;
            FileStream fs = null;

            if (getFile(filePath, out fs))
            {
                lngRetVal = fs.Length;
                fs.Close();
                fs=null;
            }

            return lngRetVal;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fs"></param>
        /// <returns></returns>
        public static bool getFile(string filePath
                                 , out System.IO.FileStream fs) {   
            fs=null;
            bool bolRetVal=false;
            const int MAX_TRIES = 10;

            for(int i=0;i<MAX_TRIES;++i)   //((tw==null) && (intTries < 10))
            {
                try
                {
                    if (System.IO.File.Exists(filePath))
                    {
                        fs = new FileStream(filePath
                                          , FileMode.Open
                                          , FileAccess.Read
                                          , FileShare.Read
                                          , 16384 );
                        bolRetVal=true;
                    }                                       
                    break;
                }
                catch
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }

            return bolRetVal;
        }

        /// <summary>
        /// Retrieves an Xml document from an assembly.
        /// </summary>
        /// <param name="identifier">Key required to locate the resource within
        /// the assembly.</param>
        /// <param name="ass">Assembly from which the resource is embedded.
        /// </param>
        /// <returns>Embedded Xml document object.</returns>
        public static XmlDocument getXmlResource(string identifier
                                               , System.Reflection.Assembly ass) {
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            
            // use the strIdentifier argument to retrieve the 
            // appropriate resource from the assembly
            System.IO.Stream s = ass.GetManifestResourceStream(identifier);
            StreamReader sr = new StreamReader(s);

            // load the document from the returned stream    
            xmlDoc.Load(sr.BaseStream);
            sr.Close();

            // return the document
            return(xmlDoc);
        }

        /// <summary>
        /// Retrieves a binary stream from an assembly.
        /// </summary>
        /// <param name="identifier">Key required to locate the resource within
        /// the assembly.</param>
        /// <param name="ass">Assembly from which the resource is embedded.
        /// </param>
        /// <returns>Embedded stream from an assembly.</returns>
        public static StreamReader getTextResource(string identifier
                                                 , System.Reflection.Assembly ass) {
            // use the strIdentifier argument to retrieve the 
            // appropriate resource from the assembly
            StreamReader sr = new StreamReader(ass.GetManifestResourceStream(identifier));

            // return the document
            return(sr);
        }

        /// <summary>
        /// Retrieves a binary stream from an assembly.
        /// </summary>
        /// <param name="identifier">Key required to locate the resource within
        /// the assembly.</param>
        /// <param name="ass">Assembly from which the resource is embedded.
        /// </param>
        /// <returns>Embedded bitmap from an assembly.</returns>
        public static Bitmap getBitmapResource(string identifier
                                             , System.Reflection.Assembly ass) {
            return((Bitmap)Image.FromStream(ass.GetManifestResourceStream(identifier)));
        }

        /// <summary>
        /// Retrieves a binary stream from an assembly.
        /// </summary>
        /// <param name="identifier">Key required to locate the resource within
        /// the assembly.</param>
        /// <param name="ass">Assembly from which the resource is embedded.
        /// </param>
        /// <returns>Binary stream containaing the embedded resource.</returns>
        public static System.IO.Stream getStreamResource(string identifier
                                                       , System.Reflection.Assembly ass) {
            return(ass.GetManifestResourceStream(identifier));
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public static bool TryParse(string s, out Guid result) {
            try {
                Guid gidTemp = new Guid(s);
                result = gidTemp;
                return(true);
            } catch(Exception) {
                result = Guid.Empty;
                return(false);
            }
        }


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="exp"></param>
        ///// <param name="truepart"></param>
        ///// <param name="falsepart"></param>
        ///// <returns></returns>
        //public static string IIf(bool exp, string truepart, string falsepart) {
        //    if(exp) {
        //        return(truepart);
        //    } else {
        //        return(falsepart);
        //    }
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="exp"></param>
        ///// <param name="truepart"></param>
        ///// <param name="falsepart"></param>
        ///// <returns></returns>
        //public static int IIf(bool exp, int truepart, int falsepart) {
        //    if(exp) {
        //        return(truepart);
        //    } else {
        //        return(falsepart);
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RepeatChar"></param>
        /// <param name="RepeatCount"></param>
        /// <returns></returns>
        public string Repeat(char RepeatChar, int RepeatCount) {

            StringBuilder sbRetVal = new StringBuilder();

            if(RepeatCount > 0) {
                for(int i=0;i<RepeatCount;++i) {
                    sbRetVal.Append(RepeatChar);
                }
            }
            
            return(sbRetVal.ToString());
        }
    }
}
