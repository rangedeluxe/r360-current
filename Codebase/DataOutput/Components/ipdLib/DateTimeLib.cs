using System;
using System.Runtime.InteropServices;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    [StructLayoutAttribute(LayoutKind.Sequential)]
    struct SystemTime {

        public short year;
        public short month;
        public short dayOfWeek;
        public short day;
        public short hour;
        public short minute;
        public short second;
        public short milliseconds;
    }

    /// <summary>
    /// 
    /// </summary>
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct TimeZoneInformation {

        /// <summary></summary>
        public int bias;

        /// <summary></summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string standardName;

        SystemTime standardDate;

        /// <summary></summary>
        public int standardBias;

        /// <summary></summary>
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string daylightName;

        SystemTime daylightDate;

        /// <summary></summary>
        public int daylightBias;
    }

    /// <summary>
    /// 
    /// </summary>
    public class DateTimeLib {

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        private static extern int GetTimeZoneInformation(out TimeZoneInformation lpTimeZoneInformation);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static int GetTimeZoneBias() {

            const int TIME_ZONE_ID_UNKNOWN = 0;
            const int TIME_ZONE_ID_STANDARD = 1;
            const int TIME_ZONE_ID_DAYLIGHT = 2;

            // create struct instance
            TimeZoneInformation tziInfo;

            int intRetVal;

            // retrieve timezone info
            int intCurrentTimeZone = GetTimeZoneInformation(out tziInfo);

            switch(intCurrentTimeZone) {
                case TIME_ZONE_ID_UNKNOWN:
                    intRetVal = tziInfo.bias;
                    break;
                case TIME_ZONE_ID_STANDARD:
                    intRetVal = tziInfo.bias + tziInfo.standardBias;
                    break;
                case TIME_ZONE_ID_DAYLIGHT:
                    intRetVal = tziInfo.bias + tziInfo.daylightBias;
                    break;
                default:
                    intRetVal = tziInfo.bias;
                    break;
            }

            return intRetVal;
        }
    }
}
