using System;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {
    
    /// <summary>
    /// ipo Xml Library
    /// </summary>
	public class ipoXmlLib {

        /// <summary>
        /// Adds custom attribute to an Xml node.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="node"></param>
        /// <param name="prefix"></param>
        /// <param name="localName"></param>
        /// <param name="namespaceURI"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static XmlAttribute createAttribute(ref XmlDocument doc
                                                 , ref XmlNode node
                                                 , string prefix
                                                 , string localName
                                                 , string namespaceURI
                                                 , string attributeValue) {

            XmlAttribute attribute = doc.CreateAttribute(prefix, localName, namespaceURI);
            attribute.Value = attributeValue;
            node.Attributes.Append(attribute);

            return attribute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attributeName"></param>
        /// <param name="attributeValue"></param>
        /// <returns></returns>
        public static XmlNode addAttribute(XmlNode node, string attributeName, string attributeValue) {

            XmlNode nodeTemp;

            nodeTemp = node.OwnerDocument.CreateNode(XmlNodeType.Attribute, attributeName, node.NamespaceURI);
            nodeTemp.Value=attributeValue;
            node.Attributes.Append((XmlAttribute)nodeTemp);

            return(nodeTemp);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="nodeName"></param>
        /// <param name="nodeValue"></param>
        /// <returns></returns>
        public static XmlNode addElement(XmlNode parentNode, string nodeName, string nodeValue) {

            XmlNode nodeTemp;

            nodeTemp = parentNode.OwnerDocument.CreateNode(XmlNodeType.Element, nodeName, parentNode.NamespaceURI);
            nodeTemp.InnerText = nodeValue;
            parentNode.AppendChild(nodeTemp);

            return(nodeTemp);
        }
	}
}
