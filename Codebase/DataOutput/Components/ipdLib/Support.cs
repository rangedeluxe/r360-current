﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    /// <summary>
    /// 
    /// </summary>
    public enum ImageStorageMode {
        /// <summary></summary>
        PICS = 0,
        /// <summary></summary>
        HYLAND = 1,
        /// <summary>
        /// Custom image option
        /// </summary>
        CUSTOM = 2,
        /// <summary></summary>
        FILEGROUP = 3
    }    

    /// <summary>
    /// 
    /// </summary>
    public enum LogonMethodType {
        /// <summary>CBXOnline authentication (default, if key is missing) </summary>
        Standard = 0,
        /// <summary>CORBA authentication</summary>
        Corba = 1,
        /// <summary>ExternalID1 Logon</summary>
        CustomerExtID1Logon = 2,
        /// <summary>Fundtech webACCESS authentication</summary>
        WebAccess = 3,
        /// <summary>Multi-Factor Authentication</summary>
        MultiFactor = 4,
        /// <summary>Single Sign-On</summary>
        SingleSignOn = 5
    }

    ///// <summary>
    ///// 
    ///// </summary>
    //public delegate void OutputMessageEventHandler(
    //    object sender,
    //    string message, 
    //    string src, 
    //    MessageType messageType, 
    //    MessageImportance messageImportance);

    /// <summary>
    /// 
    /// </summary>
    public delegate void OutputErrorEventHandler(
        object sender,
        System.Exception e);

    /// <summary>
    /// 
    /// </summary>
    public delegate void OutputMessageEventHandler(
        string message,
        string src,
        MessageType messageType,
        MessageImportance messageImportance);
    
    ///// <summary>
    ///// 
    ///// </summary>
    //public delegate void ExceptionOccurredEventHandler(
    //    System.Exception e);

    /// <summary>
    /// MessageType enumeration
    /// </summary>
    public enum MessageType {
        /// <summary></summary>
        Error = 0,
        /// <summary></summary>
        Information = 1,
        /// <summary></summary>
        Warning = 2
    }

    /// <summary>
    /// MessageImportance enumeration
    /// </summary>
    public enum MessageImportance {
        /// <summary></summary>
        Essential = 0,
        /// <summary></summary>
        Verbose = 1,
        /// <summary></summary>
        Debug = 2
    }
}
