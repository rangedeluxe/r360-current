using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    /// <summary>
    /// 
    /// </summary>
    public static class ImageUtil {

        /// <summary>
        /// define the encoderparameter, 
        /// when the 1st page, will be EncoderValue.MultiFrame.
        /// when the other pages, will be EncoderValue.FrameDimensionPage.
        /// when all pages saved, will be the EncoderValue.Flush.
        /// </summary>
        /// <param name="listSrcFiles"></param>
        /// <param name="DestFileName"></param>
        /// 
        // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
        public static void CreateMultiPageTiff(List<string> listSrcFiles, string DestFileName) {

            ImageCodecInfo info = null;
            EncoderParameters ep;
	        bool bolIsFirst = true;         // Is the first Image
	        Image img = null;
            Image img_src = null;
            string strDestinationFolder;

            strDestinationFolder = string.Empty;
            try {
                strDestinationFolder = System.IO.Path.GetDirectoryName(DestFileName);

                if(!Directory.Exists(strDestinationFolder)) {
                    Directory.CreateDirectory(strDestinationFolder);
                }
            } catch(Exception) {
                throw(new Exception("Could not create Image File Path, or part of the path name was invalid: " + strDestinationFolder));
            }


	        info = GetCodec("image/tiff");

	        ep = new EncoderParameters(2);

	        //create a image instance from the 1st image
            // WI 60239 Modify Extract Wizard to provide the ability to add footer information to checks and documents in the output PDF
            foreach(string sImage in listSrcFiles)
            {
		        //get image from src file
                img_src = Image.FromFile(sImage);

		        Guid guid = img_src.FrameDimensionsList[0];
		        FrameDimension dimension = new FrameDimension(guid);

		        //get the frames from src file
		        for(int j=0;j<img_src.GetFrameCount(dimension);j++) {

			        img_src.SelectActiveFrame(dimension, j);
        			
			        // if black and white, use CCITT4 compression
			        // else use LZW compression
			        if(img_src.PixelFormat == PixelFormat.Format1bppIndexed) {
				        ep.Param[0] = new EncoderParameter(Encoder.Compression, Convert.ToInt32(EncoderValue.CompressionCCITT4));
			        } else {
				        ep.Param[0] = new EncoderParameter(Encoder.Compression, Convert.ToInt32(EncoderValue.CompressionLZW));
			        }

			        if(bolIsFirst) {

				        //1st file, 1st frame, create the master image
				        img = img_src;

				        ep.Param[1] = new EncoderParameter(Encoder.SaveFlag, Convert.ToInt32(EncoderValue.MultiFrame));
				        img.Save(DestFileName, info, ep);

				        bolIsFirst = false;
				        continue;
			        }

			        ep.Param[1] = new EncoderParameter(Encoder.SaveFlag, Convert.ToInt32(EncoderValue.FrameDimensionPage));
			        img.SaveAdd(img_src, ep);
		        }
	        }

	        ep.Param[1] = new EncoderParameter(Encoder.SaveFlag, Convert.ToInt32(EncoderValue.Flush));

	        img.SaveAdd(ep);

            if(img_src != null) {
                img_src.Dispose();
            }

            if(ep != null) {
                ep.Dispose();
            }

            if(img != null) {
                img.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OriginalImage"></param>
        /// <param name="Width"></param>
        /// <param name="Height"></param>
        /// <returns></returns>
        public static Image ResizeImage(Image OriginalImage, int Width, int Height) {

            ImageCodecInfo info = null;
            Encoder encoder;
            EncoderParameters encoderparms;
            Bitmap objNewImage = null;

            info = GetCodec("image/tiff");

            encoder = Encoder.SaveFlag;
            encoderparms = new EncoderParameters(1);
            encoderparms.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.MultiFrame);

            objNewImage = new Bitmap(OriginalImage, Width, Height);

            return(objNewImage);
        } 

        private static ImageCodecInfo GetCodec(string MimeType) {

            ImageCodecInfo info = null;

            //get the codec for tiff files
            foreach(ImageCodecInfo ice in ImageCodecInfo.GetImageEncoders()) {
                if(ice.MimeType == MimeType) {
                    info = ice;
                }
            } //use the save encoder

            return(info);
        }
    }
}
