using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    /// <summary>
    /// 
    /// </summary>
    public class cPrinter {
        
        private string[] _ImagesToPrint = new string[] {};
        private int _CurrentImageIndex = 0;
        private string _TextToPrint;
        private StreamReader _StreamToPrint;
        private Font _PrintFont;

        private cPrinter(string[] Images) {
            _ImagesToPrint = Images;
        }

        private cPrinter(StreamReader StreamToPrint, Font PrintFont) {
            _StreamToPrint = StreamToPrint;
            _PrintFont = PrintFont;
        }

        private cPrinter(string TextToPrint, Font PrintFont) {
            _TextToPrint = TextToPrint;
            _PrintFont = PrintFont;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Images"></param>
        /// <param name="PrinterName"></param>
        /// <param name="DocumentName"></param>
        /// <returns></returns>
        public static bool PrintImages(string[] Images, 
                                       string PrinterName, 
                                       string DocumentName) {

            PrintDocument printdoc;
            cPrinter objPrinter;

            objPrinter = new cPrinter(Images);

            try {
                using(printdoc = new PrintDocument()) {
                    printdoc.PrinterSettings.PrinterName = PrinterName; //"Acrobat PDFWriter";
                    printdoc.DocumentName = DocumentName;
                    printdoc.PrintPage += new PrintPageEventHandler(objPrinter.printdoc_PrintPageImages);
                    printdoc.Print();
                }
                
                return(true);
            } catch(Exception) {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TextToPrint"></param>
        /// <param name="Settings"></param>
        /// <returns></returns>
        public static bool PrintText(string TextToPrint, PrinterSettings Settings) {

            string strTempFileName;
            StreamWriter sw;

            try {

                strTempFileName = Path.GetTempFileName();
                using(sw = new StreamWriter(strTempFileName)) {
                    sw.Write(TextToPrint);
                    sw.Close();
                }

                PrintTextFile(strTempFileName, Settings);
                
                File.Delete(strTempFileName);
                
                return(true);

            } catch(Exception) {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TextFileName"></param>
        /// <param name="Settings"></param>
        /// <returns></returns>
        public static bool PrintTextFile(string TextFileName, PrinterSettings Settings) {

            PrintDocument printdoc;
            cPrinter objPrinter;

            StreamReader streamToPrint;
            Font printFont;

            try {

                streamToPrint = new StreamReader(TextFileName);
                printFont = new Font("Courier New", 10);

                objPrinter = new cPrinter(streamToPrint, printFont);

                try {
                    printdoc = new PrintDocument();
                    printdoc.PrinterSettings = Settings;
                    printdoc.DocumentName = TextFileName.Substring(TextFileName.LastIndexOf("\\"));
                    printdoc.PrintPage += new PrintPageEventHandler(objPrinter.printdoc_PrintPageText);
                    printdoc.Print();
                    printdoc.Dispose();

                    return(true);
                }  
                finally {
                    streamToPrint.Close();
                }
            }  
            catch(Exception) {
                throw;
            }
        }

        // The PrintPage event is raised for each page to be printed.
        private void printdoc_PrintPageImages(object sender, 
                                              PrintPageEventArgs ev)  {

            float yPos = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string strImage;
            Image objImage;

            if(_CurrentImageIndex < _ImagesToPrint.Length) {
                strImage = _ImagesToPrint[_CurrentImageIndex];
                yPos = topMargin;

                objImage = ScaleImage(Image.FromFile(strImage), 
                                      (int)ev.MarginBounds.Width, 
                                      (int)ev.MarginBounds.Height, 
                                      true);

                ev.Graphics.DrawImage(objImage, leftMargin, yPos);
                objImage.Dispose();

                ++_CurrentImageIndex;

                ev.HasMorePages = (_CurrentImageIndex < _ImagesToPrint.Length -1);
            }
        }
 
        private Image ScaleImage(Image Original, 
                                int Width, 
                                int Height, 
                                bool bKeepAspectRatio) {

            Image Thumbnail = null;
        		
	        if(Original != null) {

		        int nWidth  = Width;
		        int nHeight = Height;
		        int oWidth   = Original.Width;
		        int oHeight = Original.Height;

		        if(bKeepAspectRatio) {

			        if(oWidth > oHeight) {
				        double Ratio = (double) ((double) (oHeight) / (double)(oWidth));
                        double Final = (nWidth) * Ratio;
				        nHeight = (int) Final;

			        } else {
				        double Ratio = (double) ((double) (oWidth) / (double)(oHeight));
				        double Final = (nHeight) * Ratio;
				        nWidth = (int) Final;
			        }
		        }
        				
                Thumbnail = ImageUtil.ResizeImage(Original, nWidth, nHeight);
	        }

	        return Thumbnail;
        }

        // The PrintPage event is raised for each page to be printed.
        private void printdoc_PrintPageText(object sender, 
                                            PrintPageEventArgs ev)  {

            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;
     
            // Calculate the number of lines per page.
            linesPerPage = ev.MarginBounds.Height / _PrintFont.GetHeight(ev.Graphics);
     
            // Print each line of the file.
            while(count < linesPerPage && ((line=_StreamToPrint.ReadLine()) != null)) {

                yPos = topMargin + (count * _PrintFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, _PrintFont, Brushes.Black, leftMargin, yPos, new StringFormat());
                count++;
            }
     
            // If more lines exist, print another page.
            if(line != null) {
              ev.HasMorePages = true;
            } else {
              ev.HasMorePages = false;
            }
        }
    }
}
