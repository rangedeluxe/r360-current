using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    /// <summary>
    /// 
    /// </summary>
	public class DataGridViewTextButtonCell : DataGridViewTextBoxCell {

		private DataGridViewTextButton _EditCtl;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ButtonClick;

        /// <summary>
        /// 
        /// </summary>
		public DataGridViewTextButtonCell() {
			_EditCtl = new DataGridViewTextButton();
			_EditCtl.ButtonClick += new EventHandler(_EditCtl_ButtonClick);
		}

        /// <summary>
        /// 
        /// </summary>
		public override Type EditType {
			get {
				return typeof(DataGridViewTextButton);
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _EditCtl_ButtonClick(object sender, EventArgs e) {
            if(ButtonClick != null) {
                ButtonClick(sender, e);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="graphics"></param>
        /// <param name="cellStyle"></param>
        /// <param name="rowIndex"></param>
        /// <param name="constraintSize"></param>
        /// <returns></returns>
		protected override Size GetPreferredSize(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex, Size constraintSize) {
			try {
				if(this.IsInEditMode)
					return _EditCtl.Size;
				else
					return base.GetPreferredSize(graphics, cellStyle, rowIndex, constraintSize);
			} catch {
				return base.GetPreferredSize(graphics, cellStyle, rowIndex, constraintSize);
			}
		}
	}

    /// <summary>
    /// 
    /// </summary>
	public class DataGridViewTextButtonColumn : DataGridViewColumn {

        /// <summary>
        /// 
        /// </summary>
		public DataGridViewTextButtonColumn() {
			this.CellTemplate = new DataGridViewTextButtonCell();
		}
	}

    /// <summary>
    /// 
    /// </summary>
	public class DataGridViewTextButton : UserControl, IDataGridViewEditingControl {

		private DataGridView _DataGridView = null;
		private int _RowIndex = 0;
		private bool _ValueChanged = false;
		private string _PrevText = null;

		private System.Windows.Forms.TextBox txtValue;
		private System.Windows.Forms.Button btnBrowse;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ButtonClick;

        /// <summary>
        /// 
        /// </summary>
		public DataGridViewTextButton() {
			InitializeComponent();
			this.txtValue.LostFocus += new EventHandler(txtValue_LostFocus);
		}
		
        /// <summary>
        /// 
        /// </summary>
		public string Value {
            get {
                return(this.txtValue.Text);
            }
            set {
                this.txtValue.Text = value;
			    NotifyChange();
            }
		}

		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {

            this.txtValue = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtValue
            // 
            this.txtValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValue.Location = new System.Drawing.Point(0, 1);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(88, 20);
            this.txtValue.TabIndex = 0;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnBrowse.Location = new System.Drawing.Point(88, 0);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(25, 21);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "...";
            this.btnBrowse.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // ctlDateFormat
            // 
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtValue);
            this.Name = "ctlDateFormat";
            this.Size = new System.Drawing.Size(113, 24);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private void txtValue_LostFocus(object sender, EventArgs e) {
			NotifyChange();
		}

		private void btnBrowse_Click(object sender, EventArgs e) {

            if(ButtonClick != null) {
                ButtonClick(this, e);
            }

            /*frmFormatDateDialog objDateDialog;
            string strFormat = string.Empty;

			try {

				_ValueChanged = false;

                objDateDialog = new frmFormatDateDialog();

                if(objDateDialog.ShowDialog() == DialogResult.OK) {
                    this.txtValue.Text = objDateDialog.Value;
				    NotifyChange();
			    }

			} catch(Exception ex) {
                string strMsg = ex.Message;
				//TODO: Utilities.ShowMessageBox(ex.ToString());
			}*/
		}

		private void NotifyChange() {

			if(this.txtValue.Text != _PrevText) {
				_ValueChanged = true;
				_DataGridView.NotifyCurrentCellDirty(true);
			}
		}

		#region IDataGridViewEditingControl Members

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataGridViewCellStyle"></param>
		public void ApplyCellStyleToEditingControl(DataGridViewCellStyle dataGridViewCellStyle) {
			// Do nothing
		}

        /// <summary>
        /// 
        /// </summary>
		public Cursor EditingControlCursor {
			get {
				return Cursors.IBeam;
			}
		}

        /// <summary>
        /// 
        /// </summary>
		public Cursor EditingPanelCursor {
			get {
				return Cursors.IBeam;
			}
		}

        /// <summary>
        /// 
        /// </summary>
		public DataGridView EditingControlDataGridView {
			get {
				return _DataGridView;
			}
			set {
				_DataGridView = value;
			}
		}

        /// <summary>
        /// 
        /// </summary>
		public object EditingControlFormattedValue {
			get {
				return this.txtValue.Text;
			}
			set {
				this.txtValue.Text = value.ToString();
			}
		}

        /// <summary>
        /// 
        /// </summary>
		public int EditingControlRowIndex {
			get {
				return _RowIndex;
			}
			set {
				_RowIndex = value;
			}
		}

        /// <summary>
        /// 
        /// </summary>
		public bool EditingControlValueChanged {
			get {
				return _ValueChanged;
			} 
			set {
				_ValueChanged = value;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyData"></param>
        /// <param name="dataGridViewWantsInputKey"></param>
        /// <returns></returns>
		public bool EditingControlWantsInputKey(Keys keyData, bool dataGridViewWantsInputKey) {

			switch (keyData) {

				case Keys.Tab:
					return true;
				case Keys.Home:
				case Keys.End:
				case Keys.Left:
					if (this.txtValue.SelectionLength == this.txtValue.Text.Length)
						return false;
					else
						return true;
				case Keys.Right:
					return true;
				case Keys.Delete:
					this.txtValue.Text = "";
					return true;
				case Keys.Enter:
					NotifyChange();
					return false;
				default:
					return false;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
		public object GetEditingControlFormattedValue(DataGridViewDataErrorContexts context) {
			return this.txtValue.Text;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectAll"></param>
		public void PrepareEditingControlForEdit(bool selectAll) {

			if(this._DataGridView.CurrentCell.Value == null) {
				this.txtValue.Text = "";
			} else {
				this.txtValue.Text = this._DataGridView.CurrentCell.Value.ToString();
			}

			if(selectAll) {
				this.txtValue.SelectAll();
            }

			_PrevText = this.txtValue.Text;
		}

        /// <summary>
        /// 
        /// </summary>
		public bool RepositionEditingControlOnValueChange {
			get {
				return false;
			}
		}

		#endregion
	}
}
