﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    /// <summary>
    /// 
    /// </summary>
    public class NumericTextBox : TextBox {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyPress(KeyPressEventArgs e) {
            if((e.KeyChar < (char)'0' || e.KeyChar > (char)'9') && e.KeyChar != 8) {
                e.Handled = true;
            }
            base.OnKeyPress(e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m) {

            string strResult;
            int intTemp;

            switch(m.Msg) {
                case 0x302: //WM_PASTE
                    
                    if((strResult = Clipboard.GetText()) != null) {
                        if(int.TryParse(strResult, out intTemp)) {
                            base.WndProc(ref m);
                        }
                    }
                    
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
    }
}
