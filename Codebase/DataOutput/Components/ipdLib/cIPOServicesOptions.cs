using System;
using System.Collections;
using System.Collections.Specialized;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    /// <summary>
    /// Base options class for use by any ipo application.
    /// </summary>
    public class cIPOServicesOptions {

        private const int DEFAULT_QUERY_RETRY_ATTEMPTS = 3;

        /// <summary></summary>
        protected string _LogFilePath = string.Empty;
        /// <summary></summary>
        protected int _LogFileMaxSize = -1;  // 10 megs.
        /// <summary></summary>
        protected string _ConnectionString = string.Empty;
        /// <summary></summary>
        protected int _LoggingDepth = 0; // Essential by default
        /// <summary></summary>
        protected bool _SetDeadlockPriority = false;

        /// <summary>
        /// the local variable is a short initialized as a -1.  This is the
        /// sentinal value that indicates that the value for this property
        /// has not been initialized.  The public property exposes a byte.
        /// </summary>
        protected short _QueryRetryAttempts = -1;

        /// <summary></summary>
        protected bool _LogFilePathIsDefined = false;
        /// <summary></summary>
        protected bool _LogFileMaxSizeIsDefined = false;
        /// <summary></summary>
        protected bool _ConnectionStringIsDefined = false;
        /// <summary></summary>
        protected bool _LoggingDepthIsDefined = false;
        /// <summary></summary>
        protected bool _SetDeadlockPriorityIsDefined = false;
        /// <summary></summary>
        protected bool _QueryRetryAttemptsIsDefined = false;

        /// <summary>
        /// Parameterless constructor required for overloading.
        /// </summary>
        /// <param name="Section"></param>
        public cIPOServicesOptions(string Section) {
            ////const string INISECTION_IPO_SERVICES = "ipoServices";
            loadSiteOptions(Section);
        }

        /// <summary>
        /// Loads values from the ini file for the given section.
        /// </summary>
        /// <param name="Section"></param>
        protected virtual void loadSiteOptions(string Section) {

            string strKey;
            string strValue;

            const string INIKEY_LOG_FILE = "LogFile";
            const string INIKEY_LOG_FILE_MAX_SIZE = "LogFileMaxSize";
            const string INIKEY_LOGGING_DEPTH = "loggingDepth";
            const string INIKEY_DBCONNECTION2 = "DBConnection2";
            const string INIKEY_SET_DEADLOCK_PRIORITY = "SetDeadlockPriority";
            const string INIKEY_QUERY_RETRY_ATTEMPTS = "QueryRetryAttempts";


            StringCollection colSiteOptions;
            System.Collections.IEnumerator myEnumerator;

            int intTemp;
            Int16 int16Temp;

            //Load section options.
            colSiteOptions = INILib.GetINISection(Section);
            myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
            while ( myEnumerator.MoveNext() ) {

                strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                if(strKey.ToLower() == INIKEY_LOG_FILE.ToLower()) {
                    _LogFilePath = strValue;
                    _LogFilePathIsDefined = (strValue.Trim().Length > 0);
                }
                else if(strKey.ToLower() == INIKEY_LOG_FILE_MAX_SIZE.ToLower()) {
                    if(int.TryParse(strValue, out intTemp)) {
                        _LogFileMaxSize = intTemp;
                        _LogFileMaxSizeIsDefined = true;
                    } else {
                        _LogFileMaxSize = -1;
                    }
                }
                else if(strKey.ToLower() == INIKEY_LOGGING_DEPTH.ToLower()) {
                    if(int.TryParse(strValue, out intTemp)) {
                        _LoggingDepth = intTemp;
                        _LoggingDepthIsDefined = true;
                    } else {
                        _LoggingDepth = 0;
                    }
                }
                else if(strKey.ToLower() == INIKEY_DBCONNECTION2.ToLower()) {
                    _ConnectionString = strValue;
                    _ConnectionStringIsDefined = (strValue.Trim().Length > 0);
                }
                else if(strKey.ToLower() == INIKEY_SET_DEADLOCK_PRIORITY.ToLower()) {
                    if(Int16.TryParse(strValue, out int16Temp)) {
                        _SetDeadlockPriority = (int16Temp > 0);
                        _SetDeadlockPriorityIsDefined = true;
                    } else {
                        _SetDeadlockPriority = false;
                    }
                }
                else if(strKey.ToLower() == INIKEY_QUERY_RETRY_ATTEMPTS.ToLower()) {
                    if(Int16.TryParse(strValue, out int16Temp)) {
                        _QueryRetryAttempts = int16Temp;
                        _QueryRetryAttemptsIsDefined = true;
                    } else {
                        _QueryRetryAttempts = -1;
                    }
                }
            }
        }

        /// <summary>
        /// Returns full path to the logfile that the eventLog class should 
        /// write to based on the local INI file.
        /// If logfile is not specified in INI, returns an appropriate file path.
        /// </summary>
        public virtual string logFilePath {
            get {
                bool bolIsPath = true;
                string strTemp = "";
                try {
                    if (_LogFilePath.Trim() == "")
                        bolIsPath = false;

                    if (bolIsPath) {
                        if (CheckAccess(_LogFilePath))
                            return _LogFilePath;

                        int len = _LogFilePath.LastIndexOf("\\");
                        if (len <= 0)
                            bolIsPath = false;
                        else {
                            strTemp = _LogFilePath.Substring(0, len + 1);
                            bolIsPath = System.IO.Directory.Exists(strTemp);
                            if (bolIsPath)
                                strTemp = System.IO.Path.Combine(strTemp, "ipo_log.txt");
                                
                            bolIsPath = CheckAccess(strTemp);
                        }
                    }

                    if (!bolIsPath) {
                        strTemp = AppDomain.CurrentDomain.BaseDirectory;
                        System.IO.DirectoryInfo df = new System.IO.DirectoryInfo(strTemp);

                        if (df.Parent.Parent.Exists) {

                            //check to see if data folder exists and try to create new log file if not already exists.
                            strTemp = System.IO.Path.Combine(df.Parent.Parent.FullName, "data");

                            bolIsPath = System.IO.Directory.Exists(strTemp);
                            if (bolIsPath) {
                                strTemp = System.IO.Path.Combine(strTemp, "ipo_log.txt");
                                bolIsPath = CheckAccess(strTemp);
                            }

                            //check to see if bin folder exists and try to create new log file if not already exists.
                            if (!bolIsPath) {
                                strTemp = System.IO.Path.Combine(df.Parent.Parent.FullName, "bin");
                                bolIsPath = System.IO.Directory.Exists(strTemp);
                                if (bolIsPath) {
                                    strTemp = System.IO.Path.Combine(strTemp, "ipo_log.txt");
                                    bolIsPath = CheckAccess(strTemp);
                                }
                            }

                            //check to see if IPOnline folder exists and try to create new log file if not already exists.
                            if (!bolIsPath) {
                                strTemp = System.IO.Path.Combine(df.Parent.Parent.FullName, "ipo_log.txt");
                                bolIsPath = CheckAccess(strTemp);
                            }

                            //check to see if wwwroot folder exists and try to create new log file if not already exists.
                            if (!bolIsPath) {
                                strTemp = System.IO.Path.Combine(df.Parent.FullName, "ipo_log.txt");
                                bolIsPath = CheckAccess(strTemp);
                            }

                        }
                    }

                    if (bolIsPath)
                        _LogFilePath = strTemp;
                }
                catch { }

                return _LogFilePath;
            }
        }

        /// <summary>
        /// Checks for the file access permisions to write log file.
        /// </summary>
        private bool CheckAccess(string filePath) {

            string strDecodedFilePath;
            System.IO.FileInfo fi;
            string strDirPath;
            bool bolHasAccess = false;
            DirectorySecurity sec;
            AuthorizationRuleCollection dacls;
            SecurityIdentifier sid;

            try {

                if(DecodeLogFilePath(filePath, out strDecodedFilePath)) {

                    fi = new System.IO.FileInfo(strDecodedFilePath);
                    strDirPath = fi.Directory.ToString();

                    sec = System.IO.Directory.GetAccessControl(strDirPath, System.Security.AccessControl.AccessControlSections.Access);
                    dacls = sec.GetAccessRules(true, true, typeof(System.Security.Principal.SecurityIdentifier));

                    foreach (System.Security.AccessControl.FileSystemAccessRule dacl in dacls) {

                        sid = (System.Security.Principal.SecurityIdentifier)dacl.IdentityReference;
                        
                        if (((dacl.FileSystemRights & System.Security.AccessControl.FileSystemRights.CreateFiles) == System.Security.AccessControl.FileSystemRights.CreateFiles) ||
                           ((dacl.FileSystemRights & System.Security.AccessControl.FileSystemRights.Write) == System.Security.AccessControl.FileSystemRights.Write)) {

                            if ((sid.IsAccountSid() && System.Security.Principal.WindowsIdentity.GetCurrent().User == sid) ||
                                (!sid.IsAccountSid() && System.Security.Principal.WindowsIdentity.GetCurrent().Groups.Contains(sid))) {

                                //If this is a deny right then the user has no access
                                if (dacl.AccessControlType == System.Security.AccessControl.AccessControlType.Deny) {
                                    bolHasAccess = false;
                                } else {
                                    //Allowed, for now    
                                    bolHasAccess = true;
                                }
                            }
                        }
                    }

                } else {
                    bolHasAccess = false;
                }
            }
            catch(Exception ex) {
                Console.WriteLine("Exception Occurred in " + this.GetType().Name + ": " + ex.Message);
                Console.WriteLine("Could not verify access to file: " + filePath);
                bolHasAccess = false;
            }

            return bolHasAccess;
        }

        private static bool DecodeLogFilePath(string encodedLogFilePath, out string decodedLogFilePath) {

            Regex objRegEx;
            bool bolUseRollingLogFile;
            string strDecodedLogFilePath;
            bool bolRetVal;

            const string LOGFILE_TIMESTAMP_TOKEN = @"\{0:[yMHhdms].*\}";

            try {

                // Check if logfile path contains composite date/time formatting for rolling log file.
                objRegEx = new Regex (LOGFILE_TIMESTAMP_TOKEN, RegexOptions.IgnoreCase);
                bolUseRollingLogFile = objRegEx.IsMatch(encodedLogFilePath);

                if (bolUseRollingLogFile) {
                    strDecodedLogFilePath = string.Format(encodedLogFilePath, DateTime.Now);
                } else {
                    strDecodedLogFilePath = encodedLogFilePath;
                }

                bolRetVal = true;

            } catch(Exception ex) {
                Console.WriteLine("Exception Occurred in " + Assembly.GetEntryAssembly().GetType().Name + ": " + ex.Message);
                Console.WriteLine("Could not decode LogFilePath: " + encodedLogFilePath);
                strDecodedLogFilePath = string.Empty;
                bolRetVal = false;
            }

            decodedLogFilePath = strDecodedLogFilePath;
            return(bolRetVal);
        }

        /// <summary>
        /// Returns the maximum size (in megaBytes) that the eventLog's logfile 
        /// can rech before it should be truncated.  Value is from the local INI 
        /// file.
        /// </summary>
        public virtual int logFileMaxSize {
            get {
                return _LogFileMaxSize;
            }
        }


        /// <summary>
        /// Determines what level of logging is to actually be recorded.
        /// </summary>
        public virtual int loggingDepth {
            get {
                return(_LoggingDepth);
            }
        }

        /// <summary>
        /// Returns a value for the database connection string based on
        /// the local INI file.
        /// </summary>
        public virtual string connectionString {
            get {
                return _ConnectionString;
            }
        }

        /// <summary></summary>
        public virtual bool SetDeadlockPriority {
            get {
                return(_SetDeadlockPriority);
            }
        }

        /// <summary>
        /// The QueryRetryAttempts property exposes a Byte, not a short which is the internal
        /// variable type.  This value indicates the number of times the database component 
        /// should RE-try to execute any SQL.  The database component will initially try once
        /// and retry the number of times indicated by the QueryRetryAttempts property.
        /// </summary>
        public virtual byte QueryRetryAttempts {
            get { 
                if((_QueryRetryAttempts >= System.Byte.MinValue) && (_QueryRetryAttempts <= System.Byte.MaxValue)) {
                    return(Convert.ToByte(_QueryRetryAttempts)); 
                } else {
                    return (DEFAULT_QUERY_RETRY_ATTEMPTS);
                }
            }
        }

        /// <summary></summary>
        protected bool LogFilePathIsDefined {
            get {
                return(_LogFilePathIsDefined);
            }
        }

        /// <summary></summary>
        protected bool LogFileMaxSizeIsDefined {
            get {
                return(_LogFileMaxSizeIsDefined);
            }
        }

        /// <summary></summary>
        protected bool ConnectionStringIsDefined {
            get {
                return(_ConnectionStringIsDefined);
            }
        }

        /// <summary></summary>
        protected bool LoggingDepthIsDefined {
            get {
                return(_LoggingDepthIsDefined);
            }
        }

        /// <summary></summary>
        protected bool SetDeadlockPriorityIsDefined {
            get {
                return(_SetDeadlockPriorityIsDefined);
            }
        }

        /// <summary></summary>
        protected bool QueryRetryAttemptsIsDefined {
            get {
                return(_QueryRetryAttemptsIsDefined);
            }
        }

    }
}
