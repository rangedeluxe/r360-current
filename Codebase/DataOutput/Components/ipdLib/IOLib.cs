using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* WI  71865 JMC 05/30/2013
*   -Initial Version 
******************************************************************************/
namespace WFS.RecHub.DataOutputToolkit.Common {

    /// <summary>
    /// 
    /// </summary>
    public static class IOLib {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filter"></param>
        /// <param name="SelectedFilterIndex"></param>
        /// <param name="InitialFilePath"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static bool BrowseFile(string Filter, int SelectedFilterIndex, string InitialFilePath, out string FilePath) {
            return(BrowseFile(Filter, SelectedFilterIndex, InitialFilePath, false, out FilePath));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filter"></param>
        /// <param name="SelectedFilterIndex"></param>
        /// <param name="InitialFilePath"></param>
        /// <param name="RequireFileExists"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static bool BrowseFile(string Filter, int SelectedFilterIndex, string InitialFilePath, bool RequireFileExists, out string FilePath) {

            int intPos;
            OpenFileDialog objFileDialog = new OpenFileDialog();
        
            // Set dialog filter.
            objFileDialog.Filter = Filter;

            // Set initial filename and folder.
            intPos = InitialFilePath.LastIndexOf('\\');
            if(intPos > 0) {
                objFileDialog.InitialDirectory = InitialFilePath.Substring(0, intPos);
                objFileDialog.FileName = InitialFilePath.Substring(intPos + 1);
            } else {
                objFileDialog.InitialDirectory = InitialFilePath;
                objFileDialog.FileName = InitialFilePath;
            }

            objFileDialog.CheckFileExists = RequireFileExists;
            objFileDialog.FilterIndex = SelectedFilterIndex;
            DialogResult dr = objFileDialog.ShowDialog();
            
            if(dr == DialogResult.OK && objFileDialog.FileName.Length > 0) {
                FilePath = objFileDialog.FileName;
                return(true);
            } else {
                FilePath = string.Empty;
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filter"></param>
        /// <param name="SelectedFilterIndex"></param>
        /// <param name="InitialFilePath"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static bool BrowseFileSave(string Filter, int SelectedFilterIndex, string InitialFilePath, out string FilePath) {

            int intPos;
            SaveFileDialog objSaveDialog = new SaveFileDialog();
        
            // Set dialog filter.
            objSaveDialog.Filter = Filter;

            // Set initial filename and folder.
            intPos = InitialFilePath.LastIndexOf('\\');
            if(intPos > 0) {
                objSaveDialog.InitialDirectory = InitialFilePath.Substring(0, intPos);
                objSaveDialog.FileName = InitialFilePath.Substring(intPos + 1);
            } else {
                objSaveDialog.InitialDirectory = InitialFilePath;
                objSaveDialog.FileName = InitialFilePath;
            }

            objSaveDialog.CheckFileExists = false;
            objSaveDialog.FilterIndex = SelectedFilterIndex;
            DialogResult dr = objSaveDialog.ShowDialog();
            
            if(dr == DialogResult.OK && objSaveDialog.FileName.Length > 0) {
                FilePath = objSaveDialog.FileName;
                return(true);
            } else {
                FilePath = string.Empty;
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="InitialFolderPath"></param>
        /// <param name="FolderPath"></param>
        /// <returns></returns>
        public static bool BrowseFolder(string InitialFolderPath, out string FolderPath) {

            FolderBrowserDialog objFolderDialog = new FolderBrowserDialog();

            objFolderDialog.SelectedPath = InitialFolderPath;

            DialogResult dr = objFolderDialog.ShowDialog();
            
            if(dr == DialogResult.OK && objFolderDialog.SelectedPath.Length > 0 && Directory.Exists(objFolderDialog.SelectedPath)) {
                FolderPath = objFolderDialog.SelectedPath;
                return(true);
            } else {
                FolderPath = string.Empty;
                return(false);
            }           
        }
    }
}
