﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Kyle Colden
* Date: 2/17/2014
*
* Purpose: Work Configuration Data Entry Field DTO
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
	[DataContract]
	public enum FormatType
	{
		[EnumMember]
		Any = 1,
		[EnumMember]
		Number = 2,
		[EnumMember]
		NumberDashes = 3,
		[EnumMember]
		Amount = 4,
		//[EnumMember]
		//OtherAmount = 5,
		[EnumMember]
		Alphanumeric = 6,
		[EnumMember]
		NumberAndSpecialChar = 7,
		[EnumMember]
		AlphanumericAndSpecialChar = 8,
		[EnumMember]
		Date = 9,
		[EnumMember]
		CustomDate = 10,
		[EnumMember]
		Custom = 11,
	}

	[DataContract]
	public class BusinessRulesDto
	{
		[DataMember]
		public ETableType TableType;

		[DataMember]
		public string FieldName;
		
		[DataMember]
		public int? BatchSourceKey;

		[DataMember]
		public int? BatchPaymentTypeKey;

		[DataMember]
		public int? BatchPaymentSubTypeKey;

		[DataMember]
		public int? ExceptionDisplayOrder;

		[DataMember]
		public short MinLength;

		[DataMember]
		public short MaxLength;

		[DataMember]
		public bool Required;

		[DataMember]
		public bool UserCanEdit;

		[DataMember]
		public string RequiredMessage;

		[DataMember]
		public FormatType FormatType = FormatType.Any;

		[DataMember]
		public string Mask;

		[DataMember]
		public CheckDigitRoutineDto CheckDigitRoutine;

		[DataMember]
		public FieldValidationsDto FieldValidations;
	}
}
