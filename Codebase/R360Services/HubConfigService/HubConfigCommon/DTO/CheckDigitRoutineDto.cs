﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Kyle Colden
* Date: 2/17/2014
*
* Purpose: Work Configuration Check Digit Routine DTO
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
	[DataContract]
	public enum CheckDigitMethod
	{
		[EnumMember]
		SumOfDigits = 0,
		[EnumMember]
		OnesDigit = 1,
		[EnumMember]
		ProductAddition = 2,
		//TODO: Custom?
	}

	[DataContract]
	public enum WeightPatternDirection
	{
		[EnumMember]
		LeftToRight = 0,
		[EnumMember]
		RightToLeft = 1,
	}

	[DataContract]
	public class CheckDigitRoutineDto
	{
		public long Key;

		[DataMember]
		public int Offset;

		[DataMember]
		public CheckDigitMethod Method;

		[DataMember]
		public int Modulus;

		[DataMember]
		public int Compliment;

		[DataMember]
		public WeightPatternDirection WeightsDirection;

		[DataMember]
		public int[] Weights;

		[DataMember]
		public bool IgnoreSpaces;

		[DataMember]
		public int Rem10Replacement;

		[DataMember]
		public int Rem11Replacement;

		[DataMember]
		public string FailureMessage;

		[DataMember]
		public Dictionary<char, int?> ReplacementValues;
	}
}
