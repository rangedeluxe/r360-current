﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Kyle Colden
* Date: 4/8/2014
*
* Purpose: Electronic Account DTO
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
	[DataContract]
	public class ElectronicAccountDto
	{
		[DataMember]
		public int DDAKey;

		[DataMember]
		public int SiteBankID;

		[DataMember]
		public int SiteClientAccountID;
	}
}
