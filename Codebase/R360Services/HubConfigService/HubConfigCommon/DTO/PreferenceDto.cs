﻿using System;
using System.Runtime.Serialization;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Eric Smasal
* Date: 07/25/2014
*
* Purpose: Preference DTO
*
* Modification History
*********************************************************************************/

namespace WFS.RecHub.HubConfig
{
    [DataContract]
    public class PreferenceDto
    {
        [DataMember]
        public Guid ID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool IsOnline { get; set; }
        [DataMember]
        public bool IsSystem { get; set; }
        [DataMember]
        public string DefaultSetting { get; set; }
        [DataMember]
        public byte AppType { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public int FieldDataTypeEnum { get; set; }
        [DataMember]
        public int FieldSize { get; set; }
        [DataMember]
        public int FieldMaxLength { get; set; }
        [DataMember]
        public string FieldRegEx { get; set; }
        [DataMember]
        public int FieldMinValue { get; set; }
        [DataMember]
        public int FieldMaxValue { get; set; }
        [DataMember]
        public bool EncodeOnSerialization { get; set; }
    }
}
        