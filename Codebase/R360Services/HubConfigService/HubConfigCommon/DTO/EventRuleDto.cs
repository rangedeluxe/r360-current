﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using WFS.RecHub.HubConfig.DTO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Eric Smasal
* Date: 06/16/2014
*
* Purpose: Event Rule DTO
*
* Modification History
*Added EventLongName for friendly ui displaying. MA
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
    [DataContract]
    public class EventRuleDto
    {
        [DataMember]
        public long ID { get; set; }

        [DataMember]
        public long EventRuleID { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int EventID { get; set; }

        [DataMember]
        public string EventName { get; set; }
        [DataMember]
        public string EventLongName { get; set; }

        [DataMember]
        public int EventType { get; set; }

        [DataMember]
        public int SiteCodeID { get; set; }

        [DataMember]
        public int SiteBankID { get; set; }

        [DataMember]
        public int SiteClientAccountID { get; set; }

        [DataMember]
        public bool IsActive { get; set; }

        [DataMember]
        public string Operator { get; set; }

        [DataMember]
        public string EventValue { get; set; }

        [DataMember]
        public List<int> UserIDs { get; set; }

        [DataMember]
        public List<string> UserRA3MSIDs { get; set; }

        [DataMember]
        public string Table { get; set; }

        [DataMember]
        public string Column { get; set; }

        [DataMember]
        public string MessageDefault { get; set; }

        [DataMember]
        public string EventTypeName { get; set; }

        [DataMember]
        public int? EntityID { get; set; }

        [DataMember]
        public string WorkgroupName { get; set; }
        [DataMember]
        public IEnumerable<EventRuleUser> AssociatedUsers { get; set; }

    }
}
