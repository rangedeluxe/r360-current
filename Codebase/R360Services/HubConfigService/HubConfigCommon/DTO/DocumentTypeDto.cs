﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.HubConfig
{
    [DataContract]
    public class DocumentTypeDto
    {
        [DataMember]
        public string FileDescriptor { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public int DocumentTypeKey { get; set; }
        [DataMember]
        public bool IsReadOnly { get; set; }

    }
}
