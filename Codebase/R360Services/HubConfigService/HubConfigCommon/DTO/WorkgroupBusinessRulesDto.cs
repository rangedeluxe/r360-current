﻿using System.Runtime.Serialization;

namespace WFS.RecHub.HubConfig
{
    [DataContract]
    public class WorkgroupBusinessRulesDto
    {
        [DataMember]
        public bool InvoiceRequired { get; set; }
        [DataMember]
        public bool PreDepositBalancingRequired { get; set; }
        [DataMember]
        public bool PostDepositBalancingRequired { get; set; }
        [DataMember]
        public bool PostDepositPayerRequired { get; set; }
    }
}