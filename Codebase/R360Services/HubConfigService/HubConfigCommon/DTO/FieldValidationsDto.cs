﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Kyle Colden
* Date: 2/17/2014
*
* Purpose: Work Configuration Check Digit Routine DTO
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
	[DataContract]
	public class FieldValidationsDto
	{
		public long Key;

		[DataMember]
		public string FailureMessage;

		[DataMember]
		public int ValidationTypeKey;

		[DataMember]
		public List<string> FieldValues;

		[DataMember]
		public string ModificationDateTime;

		[DataMember]
		public int ValueCount;
	}
}
