﻿using System.Runtime.Serialization;
using WFS.RecHub.HubConfig.Enums;


namespace WFS.RecHub.HubConfig.DTO
{
    [DataContract]
    public class AuditConfigDto
    {
        [DataMember]
        public string ApplicationName { get; set; }
        [DataMember]
        public AuditOperations AuditType { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string AuditValue { get; set; }
    }
}
