﻿using System.Collections.Generic;
using System.Runtime.Serialization;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Kyle Colden
* Date: 2/17/2014
*
* Purpose: Work Configuration Workgroup DTO
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
	[DataContract]
	public class WorkgroupDto
	{
		[DataMember]
		public int ClientAccountKey;

		[DataMember]
		public int SiteBankID;

		[DataMember]
		public int SiteClientAccountID;

		[DataMember]
		public bool MostRecent;

		[DataMember]
		public short IsActive;

		[DataMember]
		public short DataRetentionDays;

		[DataMember]
		public short ImageRetentionDays;

		[DataMember]
		public string ShortName;

		[DataMember]
		public string LongName;

		[DataMember]
		public string FileGroup;

		[DataMember]
		public int? EntityID;

        [DataMember]
        public string EntityName;

		[DataMember]
		public int? ViewingDays;

		[DataMember]
		public int? MaximumSearchDays;

		[DataMember]
		public ImageDisplayModes CheckImageDisplayMode;

		[DataMember]
		public ImageDisplayModes DocumentImageDisplayMode;

		[DataMember]
		public bool HOA;

		[DataMember]
		public bool? DisplayBatchID;

		[DataMember]
		public BalancingOptions InvoiceBalancing;

		[DataMember]
		public List<DDADto> DDAs;

		[DataMember]
		public List<DataEntryColumnDto> DataEntryColumns;

		[DataMember]
		public DataEntrySetupDto DataEntrySetup;

		[DataMember]
		public string BillingAccount;

		[DataMember]
		public string BillingField1;

		[DataMember]
		public string BillingField2;

        [DataMember]
        public WorkgroupBusinessRulesDto BusinessRules { get; set; }
    }
}
