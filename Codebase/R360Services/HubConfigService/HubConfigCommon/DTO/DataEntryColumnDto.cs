﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Kyle Colden
* Date: 4/14/2014
*
* Purpose: Data Entry Column DTO
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
    [DataContract]
    public class DataEntryColumnDto
    {
        [DataMember]
        public int DataEntryColumnKey;

        [DataMember]
        public ETableType TableType;

        [DataMember]
        public EDataType DataType;

        [DataMember]
        public short FieldLength;

        [DataMember]
        public short ScreenOrder;

        [DataMember]
        public string TableName;

        [DataMember]
        public string FieldName;

        [DataMember]
        public string DisplayName;

        [DataMember]
        public bool HubCreated;

        [DataMember]
        public bool MarkSense;

        [DataMember]
        public bool IsActive;

        [DataMember]
        public bool IsRequired;

        [DataMember]
        public string PaymentSource;

        [DataMember]
        public string UILabel;

        [DataMember]
        public bool isCheck;

        [DataMember]
        public int BatchSourceKey;

        [DataMember]
        public string BatchSourceShortName;
    }
}
