﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Ryan Schwantes
* Date: 5/15/2014
*
* Purpose: Bank Maintenance Bank DTO
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
	[DataContract]
	public class RecHubEntityDto
	{
		[DataMember]
		public int EntityID;
        
        [DataMember]
        public string EntityName;

		[DataMember]
		public bool DisplayBatchID;

		[DataMember]
		public ImageDisplayModes PaymentImageDisplayMode;

		[DataMember]
		public ImageDisplayModes DocumentImageDisplayMode;

		[DataMember]
		public int ViewingDays;

		[DataMember]
		public int MaximumSearchDays;

		[DataMember]
		public string BillingAccount;

		[DataMember]
		public string BillingField1;

		[DataMember]
		public string BillingField2;
	}
}
