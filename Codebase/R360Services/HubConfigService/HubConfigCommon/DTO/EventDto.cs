﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.HubConfig
{
    public class EventDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string EventLongName { get; set; }
        public int Type { get; set; }
        public int Level { get; set; }
        public bool IsActive { get; set; }
        public string Message { get; set; }
        public string Schema { get; set; }
        public string Table { get; set; }
        public string Column { get; set; }
        public List<string> Operators { get; set; }
    }
}
