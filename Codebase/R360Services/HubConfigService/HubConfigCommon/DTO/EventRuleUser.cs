﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.HubConfig.DTO
{
    [DataContract]
    public class EventRuleUser
    {
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Entity { get; set; }
        [DataMember]
        public int EntityId { get; set; }
    }
}
