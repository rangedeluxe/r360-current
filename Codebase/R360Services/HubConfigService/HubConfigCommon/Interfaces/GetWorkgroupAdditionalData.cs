﻿using System;

namespace WFS.RecHub.HubConfig
{
    [Flags]
    public enum GetWorkgroupAdditionalData
    {
        None = 0,
        Ddas = 1 << 0,
        DataEntryColumns = 1 << 1,
        BusinessRules = 1 << 2,
    }
}