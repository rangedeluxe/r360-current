﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using WFS.RecHub.HubConfig.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.HubConfig
{
    [ServiceContract(Namespace = "urn:wausaufs.com:services:HubConfigService")]
    public interface IHubConfig
    {
		[OperationContract]
		HubConfigResponse<string> Ping();

		[OperationContract]
		HubConfigResponse<List<WorkgroupDto>> GetWorkgroups(int entityID);

		[OperationContract]
		HubConfigResponse<List<WorkgroupDto>> GetUnassignedWorkgroups(int fiEntityID);

		[OperationContract]
		HubConfigResponse<int> GetUnassignedWorkgroupCount(int fiEntityID);

        [OperationContract]
        HubConfigResponse<WorkgroupDto> GetWorkgroup(int clientAccountKey, GetWorkgroupAdditionalData additionalData);

        [OperationContract]
        HubConfigResponse<WorkgroupDto> GetWorkgroupByWorkgroupAndBank(int siteClientAccountId, int siteBankId,
            GetWorkgroupAdditionalData additionalData);

        [OperationContract]
		HubConfigResponse<int> AddWorkgroup(WorkgroupDto workgroup);

		[OperationContract]
		HubConfigResponse<int> UpdateWorkgroup(WorkgroupDto workgroup);

		[OperationContract]
		HubConfigResponse<bool> SaveAssignments(List<WorkgroupDto> workgroups);

		[OperationContract]
		HubConfigResponse<bool> RemoveWorkgroupFromEntity(int clientAccountKey);

		[OperationContract]
		HubConfigResponse<ElectronicAccountDto> GetElectronicAccountByDDA(DDADto dda);

		[OperationContract]
		HubConfigResponse<List<Tuple<int, string>>> GetValidationTableTypes();

		[OperationContract]
		HubConfigResponse<List<Tuple<int, string>>> GetBatchSources(int fiEntityID);

		[OperationContract]
        HubConfigResponse<List<PaymentSourceDto>> GetPaymentSources();

        [OperationContract]
        HubConfigResponse<PaymentSourceDto> GetPaymentSource(int batchSourceKey);

        [OperationContract]
        HubConfigResponse<int> AddPaymentSource(PaymentSourceDto paymentSource);

        [OperationContract]
        BaseResponse UpdatePaymentSource(PaymentSourceDto paymentSource);

        [OperationContract]
        BaseResponse DeletePaymentSource(PaymentSourceDto paymentSource);

		[OperationContract]
		HubConfigResponse<List<Tuple<int, string>>> GetBatchPaymentTypes();

		[OperationContract]
		HubConfigResponse<List<BatchPaymentSubTypeDto>> GetBatchPaymentSubTypes();

        [OperationContract]
        HubConfigResponse<List<Tuple<int, string>>> GetSystemTypes();

		[OperationContract]
		HubConfigResponse<List<BankDto>> GetBanks();

		[OperationContract]
		HubConfigResponse<List<BankDto>> GetBanksForFI(int fiEntityID);

		[OperationContract]
		HubConfigResponse<BankDto> GetBank(int bankKey);

		[OperationContract]
		HubConfigResponse<int> AddBank(BankDto bank);

		[OperationContract]
		HubConfigResponse<int> UpdateBank(BankDto bank);

        [OperationContract]
        HubConfigResponse<RecHubEntityDto> GetEntity(int entityId);

        [OperationContract]
        HubConfigResponse<RecHubEntityDto> GetOrCreateEntityWorkgroupDefaults(int entityId);

		[OperationContract]
		BaseResponse AddUpdateEntity(RecHubEntityDto entity);

        [OperationContract]
        HubConfigResponse<List<EventRuleDto>> GetEventRules();

        [OperationContract]
        HubConfigResponse<EventRuleDto> GetEventRule(int eventRuleID);

        [OperationContract]
        HubConfigResponse<List<EventDto>> GetEvents();

        [OperationContract]
        HubConfigResponse<int> AddEventRule(EventRuleDto eventRule);

        [OperationContract]
        BaseResponse UpdateEventRule(EventRuleDto eventRule);

        [OperationContract]
        HubConfigResponse<List<PreferenceDto>> GetPreferences();

        [OperationContract]
        BaseResponse UpdatePreferences(List<PreferenceDto> preferences);

		[OperationContract]
		HubConfigResponse<int> GetSystemMaxRetentionDays();

		[OperationContract]
		HubConfigResponse<List<DataEntryTemplateDto>> GetDataEntryTemplates();

        [OperationContract]
        HubConfigResponse<List<DocumentTypeDto>> GetDocumentTypes();

        [OperationContract]
        HubConfigResponse<bool> UpdateDocumentType(DocumentTypeDto type);

        [OperationContract]
        HubConfigResponse<List<NotificationTypeDto>> GetNotificationTypes();

        [OperationContract]
        HubConfigResponse<bool> UpdateNotificationType(NotificationTypeDto type);

        [OperationContract]
        HubConfigResponse<bool> InsertNotificationType(NotificationTypeDto type);

		[OperationContract]
		HubConfigResponse<List<BusinessRulesDto>> GetFieldValidationSettings(int siteBankID, int workgroupId);

        [OperationContract]
        HubConfigResponse<bool> WriteAuditConfigReport(AuditConfigDto dto);
    }
}
