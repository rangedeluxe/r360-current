﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.HubConfig.Enums
{
    [DataContract]
    public enum AuditOperations
    {
        [EnumMember]
        DEL = 1,
        [EnumMember]
        INS = 2,
        [EnumMember]
        UPD = 3
    }
}
