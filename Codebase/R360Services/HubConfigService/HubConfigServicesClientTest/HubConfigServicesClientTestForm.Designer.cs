﻿namespace HubConfigServicesClientTest
{
	partial class HubConfigServicesClientTestForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this._btnGetWorkgroups = new System.Windows.Forms.Button();
			this._lvWorkgroups = new System.Windows.Forms.ListView();
			this._colLongName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this._colShortName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this._colSiteClientAccountID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this._btnPing = new System.Windows.Forms.Button();
			this._btnAddWorkgroup = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this._tabPing = new System.Windows.Forms.TabPage();
			this._tabWorkgroups = new System.Windows.Forms.TabPage();
			this._btnUpdateWorkgroup = new System.Windows.Forms.Button();
			this.tabStaticData = new System.Windows.Forms.TabPage();
			this._lvStaticData = new System.Windows.Forms.ListView();
			this._colID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this._colDescription = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this._btnValidationTableTypes = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this._tabPing.SuspendLayout();
			this._tabWorkgroups.SuspendLayout();
			this.tabStaticData.SuspendLayout();
			this.SuspendLayout();
			// 
			// _btnGetWorkgroups
			// 
			this._btnGetWorkgroups.Location = new System.Drawing.Point(3, 6);
			this._btnGetWorkgroups.Name = "_btnGetWorkgroups";
			this._btnGetWorkgroups.Size = new System.Drawing.Size(124, 23);
			this._btnGetWorkgroups.TabIndex = 1;
			this._btnGetWorkgroups.Text = "Get Workgroups";
			this._btnGetWorkgroups.UseVisualStyleBackColor = true;
			this._btnGetWorkgroups.Click += new System.EventHandler(this._btnGetWorkgroups_Click);
			// 
			// _lvWorkgroups
			// 
			this._lvWorkgroups.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._lvWorkgroups.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._colLongName,
            this._colShortName,
            this._colSiteClientAccountID});
			this._lvWorkgroups.Location = new System.Drawing.Point(6, 35);
			this._lvWorkgroups.Name = "_lvWorkgroups";
			this._lvWorkgroups.Size = new System.Drawing.Size(634, 285);
			this._lvWorkgroups.TabIndex = 2;
			this._lvWorkgroups.UseCompatibleStateImageBehavior = false;
			this._lvWorkgroups.View = System.Windows.Forms.View.Details;
			// 
			// _colLongName
			// 
			this._colLongName.Text = "Long Name";
			this._colLongName.Width = 195;
			// 
			// _colShortName
			// 
			this._colShortName.Text = "Short Name";
			this._colShortName.Width = 100;
			// 
			// _colSiteClientAccountID
			// 
			this._colSiteClientAccountID.Text = "Site Client Account ID";
			this._colSiteClientAccountID.Width = 133;
			// 
			// _btnPing
			// 
			this._btnPing.Location = new System.Drawing.Point(6, 6);
			this._btnPing.Name = "_btnPing";
			this._btnPing.Size = new System.Drawing.Size(124, 23);
			this._btnPing.TabIndex = 3;
			this._btnPing.Text = "Ping Service";
			this._btnPing.UseVisualStyleBackColor = true;
			this._btnPing.Click += new System.EventHandler(this._btnPing_Click);
			// 
			// _btnAddWorkgroup
			// 
			this._btnAddWorkgroup.Location = new System.Drawing.Point(133, 6);
			this._btnAddWorkgroup.Name = "_btnAddWorkgroup";
			this._btnAddWorkgroup.Size = new System.Drawing.Size(124, 23);
			this._btnAddWorkgroup.TabIndex = 4;
			this._btnAddWorkgroup.Text = "Add Workgroup";
			this._btnAddWorkgroup.UseVisualStyleBackColor = true;
			this._btnAddWorkgroup.Click += new System.EventHandler(this._btnAddWorkgroup_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this._tabPing);
			this.tabControl1.Controls.Add(this._tabWorkgroups);
			this.tabControl1.Controls.Add(this.tabStaticData);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(658, 354);
			this.tabControl1.TabIndex = 5;
			// 
			// _tabPing
			// 
			this._tabPing.Controls.Add(this._btnPing);
			this._tabPing.Location = new System.Drawing.Point(4, 22);
			this._tabPing.Name = "_tabPing";
			this._tabPing.Padding = new System.Windows.Forms.Padding(3);
			this._tabPing.Size = new System.Drawing.Size(650, 328);
			this._tabPing.TabIndex = 0;
			this._tabPing.Text = "Ping";
			this._tabPing.UseVisualStyleBackColor = true;
			// 
			// _tabWorkgroups
			// 
			this._tabWorkgroups.Controls.Add(this._btnUpdateWorkgroup);
			this._tabWorkgroups.Controls.Add(this._btnGetWorkgroups);
			this._tabWorkgroups.Controls.Add(this._btnAddWorkgroup);
			this._tabWorkgroups.Controls.Add(this._lvWorkgroups);
			this._tabWorkgroups.Location = new System.Drawing.Point(4, 22);
			this._tabWorkgroups.Name = "_tabWorkgroups";
			this._tabWorkgroups.Padding = new System.Windows.Forms.Padding(3);
			this._tabWorkgroups.Size = new System.Drawing.Size(650, 328);
			this._tabWorkgroups.TabIndex = 1;
			this._tabWorkgroups.Text = "Workgroups";
			this._tabWorkgroups.UseVisualStyleBackColor = true;
			// 
			// _btnUpdateWorkgroup
			// 
			this._btnUpdateWorkgroup.Location = new System.Drawing.Point(263, 6);
			this._btnUpdateWorkgroup.Name = "_btnUpdateWorkgroup";
			this._btnUpdateWorkgroup.Size = new System.Drawing.Size(124, 23);
			this._btnUpdateWorkgroup.TabIndex = 5;
			this._btnUpdateWorkgroup.Text = "Update Workgroup";
			this._btnUpdateWorkgroup.UseVisualStyleBackColor = true;
			this._btnUpdateWorkgroup.Click += new System.EventHandler(this._btnUpdateWorkgroup_Click);
			// 
			// tabStaticData
			// 
			this.tabStaticData.Controls.Add(this._lvStaticData);
			this.tabStaticData.Controls.Add(this._btnValidationTableTypes);
			this.tabStaticData.Location = new System.Drawing.Point(4, 22);
			this.tabStaticData.Name = "tabStaticData";
			this.tabStaticData.Padding = new System.Windows.Forms.Padding(3);
			this.tabStaticData.Size = new System.Drawing.Size(650, 328);
			this.tabStaticData.TabIndex = 2;
			this.tabStaticData.Text = "Static Data";
			this.tabStaticData.UseVisualStyleBackColor = true;
			// 
			// _lvStaticData
			// 
			this._lvStaticData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._colID,
            this._colDescription});
			this._lvStaticData.Location = new System.Drawing.Point(263, 24);
			this._lvStaticData.Name = "_lvStaticData";
			this._lvStaticData.Size = new System.Drawing.Size(379, 296);
			this._lvStaticData.TabIndex = 2;
			this._lvStaticData.UseCompatibleStateImageBehavior = false;
			this._lvStaticData.View = System.Windows.Forms.View.Details;
			// 
			// _colID
			// 
			this._colID.Text = "ID";
			this._colID.Width = 55;
			// 
			// _colDescription
			// 
			this._colDescription.Text = "Description";
			this._colDescription.Width = 293;
			// 
			// _btnValidationTableTypes
			// 
			this._btnValidationTableTypes.Location = new System.Drawing.Point(23, 53);
			this._btnValidationTableTypes.Name = "_btnValidationTableTypes";
			this._btnValidationTableTypes.Size = new System.Drawing.Size(234, 23);
			this._btnValidationTableTypes.TabIndex = 1;
			this._btnValidationTableTypes.Text = "Load Validation Table Types";
			this._btnValidationTableTypes.UseVisualStyleBackColor = true;
			this._btnValidationTableTypes.Click += new System.EventHandler(this._btnValidationTableTypes_Click);
			// 
			// HubConfigServicesClientTestForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(658, 354);
			this.Controls.Add(this.tabControl1);
			this.Name = "HubConfigServicesClientTestForm";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.HubConfigServicesTestForm_Load);
			this.tabControl1.ResumeLayout(false);
			this._tabPing.ResumeLayout(false);
			this._tabWorkgroups.ResumeLayout(false);
			this.tabStaticData.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button _btnGetWorkgroups;
		private System.Windows.Forms.ListView _lvWorkgroups;
		private System.Windows.Forms.ColumnHeader _colLongName;
		private System.Windows.Forms.ColumnHeader _colShortName;
		private System.Windows.Forms.ColumnHeader _colSiteClientAccountID;
		private System.Windows.Forms.Button _btnPing;
		private System.Windows.Forms.Button _btnAddWorkgroup;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage _tabPing;
		private System.Windows.Forms.TabPage _tabWorkgroups;
		private System.Windows.Forms.Button _btnUpdateWorkgroup;
		private System.Windows.Forms.TabPage tabStaticData;
		private System.Windows.Forms.ListView _lvStaticData;
		private System.Windows.Forms.ColumnHeader _colID;
		private System.Windows.Forms.ColumnHeader _colDescription;
		private System.Windows.Forms.Button _btnValidationTableTypes;
	}
}

