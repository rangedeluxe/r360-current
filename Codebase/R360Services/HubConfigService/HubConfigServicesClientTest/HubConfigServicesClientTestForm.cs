﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFS.RecHub.R360Shared;
using WFS.RecHub.HubConfig;

namespace HubConfigServicesClientTest
{
	public partial class HubConfigServicesClientTestForm : Form
	{
		public HubConfigServicesClientTestForm()
		{
			InitializeComponent();
			if (LogManager.IsDefault) LogManager.Logger = new ConfigHelpers.Logger(o => MessageBox.Show(o, "Information"), o => MessageBox.Show(o, "Warning"));
			R360ServiceContext.PerformImpersonation();
		}

		private void _btnGetWorkgroups_Click(object sender, EventArgs e)
		{
			LoadWorkgroups();
		}
		private void LoadWorkgroups()
		{
			_lvWorkgroups.Items.Clear();

			var manager = new HubConfigServicesManager();
			var response = manager.GetWorkgroups(1);
			if (response.Status == StatusCode.SUCCESS)
			{
				foreach (var workgroup in response.Data)
				{
					ListViewItem lvi = new ListViewItem(workgroup.LongName);
					lvi.SubItems.Add(workgroup.ShortName);
					lvi.SubItems.Add(workgroup.SiteClientAccountID.ToString());
					lvi.Tag = workgroup;
					_lvWorkgroups.Items.Add(lvi);
				}
			}
		}

		private void HubConfigServicesTestForm_Load(object sender, EventArgs e)
		{

		}

		private void _btnPing_Click(object sender, EventArgs e)
		{
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.Ping();
				if (response.Status == StatusCode.SUCCESS)
					MessageBox.Show(response.Data);
				else if (response.Errors != null && response.Errors.Count > 0)
					MessageBox.Show(string.Join("\r\n", response.Errors.ToArray()));
				else
					MessageBox.Show("Ping failed for an unknown reason!");
			}
			catch (Exception ex)
			{
				MessageBox.Show("Exception thrown calling Ping method\r\n" + ex.ToString());
			}
		}

		private void _btnAddWorkgroup_Click(object sender, EventArgs e)
		{
			WorkgroupEditForm form = new WorkgroupEditForm(null);
			if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				LoadWorkgroups();
		}

		private void _btnUpdateWorkgroup_Click(object sender, EventArgs e)
		{
			if (_lvWorkgroups.SelectedItems.Count == 0)
			{
				MessageBox.Show("You must select a workgroup to edit!");
				return;
			}
			var workgroup = _lvWorkgroups.SelectedItems[0].Tag as WorkgroupDto;
			WorkgroupEditForm form = new WorkgroupEditForm(workgroup);
			if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
				LoadWorkgroups();
		}

		private void LoadStaticDataListView(List<Tuple<int, string>> data)
		{
			try
			{
				_lvStaticData.BeginUpdate();
				_lvStaticData.Items.Clear();
				_lvStaticData.Items.AddRange(data.ConvertAll(o => new ListViewItem(new string[] { o.Item1.ToString(), o.Item2 })).ToArray());
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				_lvStaticData.EndUpdate();
			}
		}

		private void _btnValidationTableTypes_Click(object sender, EventArgs e)
		{
			try
			{
				var manager = new HubConfigServicesManager();
				var response = manager.GetValidationTableTypes();
				if (response.Status == StatusCode.SUCCESS)
				{
					LoadStaticDataListView(response.Data);
				}
				else if (response.Errors != null && response.Errors.Count > 0)
					MessageBox.Show(string.Join("\r\n", response.Errors.ToArray()));
				else
					MessageBox.Show("GetValidationTableTypes failed for an unknown reason!");
			}
			catch (Exception ex)
			{
				MessageBox.Show("Exception thrown calling GetValidationTableTypes method\r\n" + ex.ToString());
			}
		}
	}
}
