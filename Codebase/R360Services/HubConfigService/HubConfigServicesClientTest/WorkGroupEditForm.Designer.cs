﻿namespace HubConfigServicesClientTest
{
	partial class WorkgroupEditForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this._lblClientAccountKey = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this._txtSiteBankID = new System.Windows.Forms.TextBox();
			this._txtClientAccountID = new System.Windows.Forms.TextBox();
			this._txtDataRetentionDays = new System.Windows.Forms.TextBox();
			this._txtImageRetentionDays = new System.Windows.Forms.TextBox();
			this._txtShortName = new System.Windows.Forms.TextBox();
			this._txtLongName = new System.Windows.Forms.TextBox();
			this._txtFileGroup = new System.Windows.Forms.TextBox();
			this._btnOK = new System.Windows.Forms.Button();
			this._btnCancel = new System.Windows.Forms.Button();
			this._lblMostRecent = new System.Windows.Forms.Label();
			this._chkIsActive = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(24, 32);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Client Account Key:";
			// 
			// _lblClientAccountKey
			// 
			this._lblClientAccountKey.AutoSize = true;
			this._lblClientAccountKey.Location = new System.Drawing.Point(164, 32);
			this._lblClientAccountKey.Name = "_lblClientAccountKey";
			this._lblClientAccountKey.Size = new System.Drawing.Size(0, 13);
			this._lblClientAccountKey.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(24, 55);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Site Bank ID:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(24, 81);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(114, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Site Client Account ID:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(24, 107);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(71, 13);
			this.label4.TabIndex = 4;
			this.label4.Text = "Most Recent:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(24, 133);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(51, 13);
			this.label5.TabIndex = 5;
			this.label5.Text = "Is Active:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(24, 159);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(109, 13);
			this.label6.TabIndex = 6;
			this.label6.Text = "Data Retention Days:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(24, 185);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(115, 13);
			this.label7.TabIndex = 7;
			this.label7.Text = "Image Retention Days:";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(24, 211);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(66, 13);
			this.label8.TabIndex = 8;
			this.label8.Text = "Short Name:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(24, 237);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(65, 13);
			this.label9.TabIndex = 9;
			this.label9.Text = "Long Name:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(24, 263);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(58, 13);
			this.label10.TabIndex = 10;
			this.label10.Text = "File Group:";
			// 
			// _txtSiteBankID
			// 
			this._txtSiteBankID.Location = new System.Drawing.Point(164, 52);
			this._txtSiteBankID.Name = "_txtSiteBankID";
			this._txtSiteBankID.Size = new System.Drawing.Size(100, 20);
			this._txtSiteBankID.TabIndex = 11;
			// 
			// _txtClientAccountID
			// 
			this._txtClientAccountID.Location = new System.Drawing.Point(164, 78);
			this._txtClientAccountID.Name = "_txtClientAccountID";
			this._txtClientAccountID.Size = new System.Drawing.Size(100, 20);
			this._txtClientAccountID.TabIndex = 12;
			// 
			// _txtDataRetentionDays
			// 
			this._txtDataRetentionDays.Location = new System.Drawing.Point(164, 156);
			this._txtDataRetentionDays.Name = "_txtDataRetentionDays";
			this._txtDataRetentionDays.Size = new System.Drawing.Size(100, 20);
			this._txtDataRetentionDays.TabIndex = 15;
			// 
			// _txtImageRetentionDays
			// 
			this._txtImageRetentionDays.Location = new System.Drawing.Point(164, 182);
			this._txtImageRetentionDays.Name = "_txtImageRetentionDays";
			this._txtImageRetentionDays.Size = new System.Drawing.Size(100, 20);
			this._txtImageRetentionDays.TabIndex = 16;
			// 
			// _txtShortName
			// 
			this._txtShortName.Location = new System.Drawing.Point(164, 208);
			this._txtShortName.Name = "_txtShortName";
			this._txtShortName.Size = new System.Drawing.Size(320, 20);
			this._txtShortName.TabIndex = 17;
			// 
			// _txtLongName
			// 
			this._txtLongName.Location = new System.Drawing.Point(164, 234);
			this._txtLongName.Name = "_txtLongName";
			this._txtLongName.Size = new System.Drawing.Size(320, 20);
			this._txtLongName.TabIndex = 18;
			// 
			// _txtFileGroup
			// 
			this._txtFileGroup.Location = new System.Drawing.Point(164, 260);
			this._txtFileGroup.Name = "_txtFileGroup";
			this._txtFileGroup.Size = new System.Drawing.Size(320, 20);
			this._txtFileGroup.TabIndex = 19;
			// 
			// _btnOK
			// 
			this._btnOK.Location = new System.Drawing.Point(178, 317);
			this._btnOK.Name = "_btnOK";
			this._btnOK.Size = new System.Drawing.Size(75, 23);
			this._btnOK.TabIndex = 20;
			this._btnOK.Text = "OK";
			this._btnOK.UseVisualStyleBackColor = true;
			this._btnOK.Click += new System.EventHandler(this._btnOK_Click);
			// 
			// _btnCancel
			// 
			this._btnCancel.Location = new System.Drawing.Point(259, 317);
			this._btnCancel.Name = "_btnCancel";
			this._btnCancel.Size = new System.Drawing.Size(75, 23);
			this._btnCancel.TabIndex = 21;
			this._btnCancel.Text = "Cancel";
			this._btnCancel.UseVisualStyleBackColor = true;
			this._btnCancel.Click += new System.EventHandler(this._btnCancel_Click);
			// 
			// _lblMostRecent
			// 
			this._lblMostRecent.AutoSize = true;
			this._lblMostRecent.Location = new System.Drawing.Point(164, 107);
			this._lblMostRecent.Name = "_lblMostRecent";
			this._lblMostRecent.Size = new System.Drawing.Size(0, 13);
			this._lblMostRecent.TabIndex = 22;
			// 
			// _chkIsActive
			// 
			this._chkIsActive.AutoSize = true;
			this._chkIsActive.Location = new System.Drawing.Point(167, 132);
			this._chkIsActive.Name = "_chkIsActive";
			this._chkIsActive.Size = new System.Drawing.Size(15, 14);
			this._chkIsActive.TabIndex = 23;
			this._chkIsActive.UseVisualStyleBackColor = true;
			// 
			// WorkgroupEditForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(552, 361);
			this.Controls.Add(this._chkIsActive);
			this.Controls.Add(this._lblMostRecent);
			this.Controls.Add(this._btnCancel);
			this.Controls.Add(this._btnOK);
			this.Controls.Add(this._txtFileGroup);
			this.Controls.Add(this._txtLongName);
			this.Controls.Add(this._txtShortName);
			this.Controls.Add(this._txtImageRetentionDays);
			this.Controls.Add(this._txtDataRetentionDays);
			this.Controls.Add(this._txtClientAccountID);
			this.Controls.Add(this._txtSiteBankID);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this._lblClientAccountKey);
			this.Controls.Add(this.label1);
			this.Name = "WorkgroupEditForm";
			this.Text = "WorkgroupEditForm";
			this.Load += new System.EventHandler(this.WorkgroupEditForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label _lblClientAccountKey;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox _txtSiteBankID;
		private System.Windows.Forms.TextBox _txtClientAccountID;
		private System.Windows.Forms.TextBox _txtDataRetentionDays;
		private System.Windows.Forms.TextBox _txtImageRetentionDays;
		private System.Windows.Forms.TextBox _txtShortName;
		private System.Windows.Forms.TextBox _txtLongName;
		private System.Windows.Forms.TextBox _txtFileGroup;
		private System.Windows.Forms.Button _btnOK;
		private System.Windows.Forms.Button _btnCancel;
		private System.Windows.Forms.Label _lblMostRecent;
		private System.Windows.Forms.CheckBox _chkIsActive;
	}
}