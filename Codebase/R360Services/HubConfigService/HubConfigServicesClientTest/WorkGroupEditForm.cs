﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFS.RecHub.R360Shared;
using WFS.RecHub.HubConfig;

namespace HubConfigServicesClientTest
{
	public partial class WorkgroupEditForm : Form
	{
		public WorkgroupDto _workgroup = null;

		public WorkgroupEditForm(WorkgroupDto workgroup)
		{
			_workgroup = workgroup;
			InitializeComponent();
		}

		private void WorkgroupEditForm_Load(object sender, EventArgs e)
		{
			bool bAdd = _workgroup == null;
			if (!bAdd)
			{
				_lblClientAccountKey.Text = _workgroup.ClientAccountKey.ToString();
				_txtSiteBankID.Text = _workgroup.SiteBankID.ToString();
				_txtSiteBankID.Enabled = false;
				_txtClientAccountID.Text = _workgroup.SiteClientAccountID.ToString();
				_txtClientAccountID.Enabled = false;
				_lblMostRecent.Text = _workgroup.MostRecent ? "Yes" : "No";
				_chkIsActive.Checked = _workgroup.IsActive == 1;
				_txtDataRetentionDays.Text = _workgroup.DataRetentionDays.ToString();
				_txtImageRetentionDays.Text = _workgroup.ImageRetentionDays.ToString();
				_txtShortName.Text = _workgroup.ShortName;
				_txtLongName.Text = _workgroup.LongName;
				_txtFileGroup.Text = _workgroup.FileGroup;
			}
		}

		private void _btnOK_Click(object sender, EventArgs e)
		{
			var manager = new HubConfigServicesManager();

			WorkgroupDto workgroup;
			if (_workgroup == null)
			{
				try
				{
					workgroup = new WorkgroupDto()
					{
						SiteBankID = int.Parse(_txtSiteBankID.Text),
						SiteClientAccountID = int.Parse(_txtClientAccountID.Text),
						IsActive = (short)(_chkIsActive.Checked ? 1 : 0),
						DataRetentionDays = (short)(_txtDataRetentionDays.Text != string.Empty ? short.Parse(_txtDataRetentionDays.Text) : 0),
						ImageRetentionDays = (short)(_txtImageRetentionDays.Text != string.Empty ? short.Parse(_txtImageRetentionDays.Text) : 0),
						ShortName = _txtShortName.Text,
						LongName = _txtLongName.Text,
						FileGroup = _txtFileGroup.Text
					};

					var response = manager.AddWorkgroup(workgroup);
					if (response.Status == StatusCode.SUCCESS)
					{
						MessageBox.Show("Added workgroup: " + response.Data.ToString());
						DialogResult = DialogResult.OK;
					}
					else
					{
						MessageBox.Show("Failed to add workgroup");
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error occurred adding workgroup\r\n" + ex.ToString());
				}
			}
			else
			{
				try
				{
					workgroup = new WorkgroupDto()
					{
						ClientAccountKey = _workgroup.ClientAccountKey,
						SiteBankID = _workgroup.SiteBankID,
						SiteClientAccountID = _workgroup.SiteClientAccountID,
						IsActive = (short)(_chkIsActive.Checked ? 1 : 0),
						DataRetentionDays = (short)(_txtDataRetentionDays.Text != string.Empty ? short.Parse(_txtDataRetentionDays.Text) : 0),
						ImageRetentionDays = (short)(_txtImageRetentionDays.Text != string.Empty ? short.Parse(_txtImageRetentionDays.Text) : 0),
						ShortName = _txtShortName.Text,
						LongName = _txtLongName.Text,
						FileGroup = _txtFileGroup.Text
					};

					var response = manager.UpdateWorkgroup(workgroup);
					if (response.Status == StatusCode.SUCCESS)
					{
						MessageBox.Show("Updated workgroup");
						DialogResult = DialogResult.OK;
					}
					else
					{
						MessageBox.Show("Failed to update workgroup");
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error occurred update workgroup\r\n" + ex.ToString());
				}
			}
		}

		private void _btnCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}
	}
}
