﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using WFS.RecHub.HubConfig.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.HubConfig
{
	public class HubConfigService : LTAService, IHubConfig
	{
		#region IHubConfig

		public HubConfigResponse<string> Ping()
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.Ping(OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString());
			});
		}

		public HubConfigResponse<List<WorkgroupDto>> GetWorkgroups(int entityID)
		{
			return FailOnError((oapi) =>
				{
					var api = (HubConfigAPI)oapi;
				return api.GetWorkgroups(entityID);
			});
		}

        public HubConfigResponse<WorkgroupDto> GetWorkgroupByWorkgroupAndBank(int siteClientAccountId, int siteBankId,
            GetWorkgroupAdditionalData additionalData)
        {
            return FailOnError(oapi =>
            {
                var api = (HubConfigAPI) oapi;
                return api.GetWorkgroup(siteClientAccountId, siteBankId, additionalData);
            });
        }

        public HubConfigResponse<List<WorkgroupDto>> GetUnassignedWorkgroups(int fiEntityID)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetUnassignedWorkgroups(fiEntityID);
			});
		}

		public HubConfigResponse<int> GetUnassignedWorkgroupCount(int fiEntityID)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetUnassignedWorkgroupCount(fiEntityID);
			});
		}

        public HubConfigResponse<WorkgroupDto> GetWorkgroup(int clientAccountKey, GetWorkgroupAdditionalData additionalData)
        {
            return FailOnError(oapi =>
            {
                var api = (HubConfigAPI) oapi;
                return api.GetWorkgroup(clientAccountKey, additionalData);
            });
        }

        public HubConfigResponse<int> AddWorkgroup(WorkgroupDto workgroup)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.AddWorkgroup(workgroup);
			});
		}

		public HubConfigResponse<int> UpdateWorkgroup(WorkgroupDto workgroup)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.UpdateWorkgroup(workgroup);
			});
		}

		public HubConfigResponse<bool> SaveAssignments(List<WorkgroupDto> workgroups)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.SaveAssignments(workgroups);
			});
		}

		public HubConfigResponse<bool> RemoveWorkgroupFromEntity(int clientAccountKey)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.RemoveWorkgroupFromEntity(clientAccountKey);
			});
		}

		public HubConfigResponse<ElectronicAccountDto> GetElectronicAccountByDDA(DDADto dda)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetElectronicAccountByDDA(dda);
			});
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetValidationTableTypes()
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetValidationTableTypes();
			});
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchSources(int fiEntityID)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetBatchSources(fiEntityID);
			});
		}

        public HubConfigResponse<List<PaymentSourceDto>> GetPaymentSources()
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetPaymentSources();
            });
        }

        public HubConfigResponse<PaymentSourceDto> GetPaymentSource(int batchSourceKey)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetPaymentSource(batchSourceKey);
            });
        }

        public HubConfigResponse<int> AddPaymentSource(PaymentSourceDto paymentSource)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.AddPaymentSource(paymentSource);
            });
        }

        public BaseResponse UpdatePaymentSource(PaymentSourceDto paymentSource)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.UpdatePaymentSource(paymentSource);
            });
        }

        public BaseResponse DeletePaymentSource(PaymentSourceDto paymentSource)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.DeletePaymentSource(paymentSource);
            });
        }

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchPaymentTypes()
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetBatchPaymentTypes();
			});
		}

		public HubConfigResponse<List<BatchPaymentSubTypeDto>> GetBatchPaymentSubTypes()
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetBatchPaymentSubTypes();
			});
		}

        public HubConfigResponse<List<Tuple<int, string>>> GetSystemTypes()
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetSystemTypes();
            });
        }

		public HubConfigResponse<List<BankDto>> GetBanks()
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetBanks();
			});
		}

		public HubConfigResponse<List<BankDto>> GetBanksForFI(int fiEntityID)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetBanksForFI(fiEntityID);
			});
		}

		public HubConfigResponse<BankDto> GetBank(int bankKey)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetBank(bankKey);
			});
		}

		public HubConfigResponse<int> AddBank(BankDto bank)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.AddBank(bank);
			});
		}

		public HubConfigResponse<int> UpdateBank(BankDto bank)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.UpdateBank(bank);
			});
		}

        public HubConfigResponse<RecHubEntityDto> GetEntity(int entityId)
        {
            return FailOnError(oapi =>
            {
                var api = (HubConfigAPI) oapi;
                return api.GetEntity(entityId);
            });
        }

        public HubConfigResponse<RecHubEntityDto> GetOrCreateEntityWorkgroupDefaults(int entityId)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI) oapi;
                return api.GetOrCreateEntityWorkgroupDefaults(entityId);
            });
        }

		public BaseResponse AddUpdateEntity(RecHubEntityDto entity)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.AddUpdateEntity(entity);
			});
		}

		public HubConfigResponse<List<EventRuleDto>> GetEventRules()
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetEventRules();
            });
        }

        public HubConfigResponse<EventRuleDto> GetEventRule(int eventRuleID)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetEventRule(eventRuleID);
            });
        }

        public HubConfigResponse<List<EventDto>> GetEvents()
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetEvents();
            });
        }

        public HubConfigResponse<int> AddEventRule(EventRuleDto eventRule)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.AddEventRule(eventRule);
            });
        }

        public BaseResponse UpdateEventRule(EventRuleDto eventRule)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.UpdateEventRule(eventRule);
            });
        }

        public HubConfigResponse<List<PreferenceDto>> GetPreferences()
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetPreferences();
            });
        }

        public BaseResponse UpdatePreferences(List<PreferenceDto> preferences)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.UpdatePreferences(preferences);
            });
        }

		public HubConfigResponse<int> GetSystemMaxRetentionDays()
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetSystemMaxRetentionDays();
			});
		}

		public HubConfigResponse<List<DataEntryTemplateDto>> GetDataEntryTemplates()
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetDataEntryTemplates();
			});
		}

        public HubConfigResponse<List<DocumentTypeDto>> GetDocumentTypes()
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetDocumentTypes();
            });
        }

        public HubConfigResponse<bool> UpdateDocumentType(DocumentTypeDto type)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.UpdateDocumentType(type);
            });
        }

        public HubConfigResponse<List<NotificationTypeDto>> GetNotificationTypes()
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.GetNotificationTypes();
            });
        }

        public HubConfigResponse<bool> UpdateNotificationType(NotificationTypeDto type)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.UpdateNotificationType(type);
            });
        }

        public HubConfigResponse<bool> InsertNotificationType(NotificationTypeDto type)
        {
            return FailOnError((oapi) =>
            {
                var api = (HubConfigAPI)oapi;
                return api.InsertNotificationType(type);
            });
        }

		public HubConfigResponse<List<BusinessRulesDto>> GetFieldValidationSettings(int siteBankID, int workgroupId)
		{
			return FailOnError((oapi) =>
			{
				var api = (HubConfigAPI)oapi;
				return api.GetFieldValidationSettings(siteBankID, workgroupId);
			});
		}

	    public HubConfigResponse<bool> WriteAuditConfigReport(AuditConfigDto dto)
	    {
	        return FailOnError(oapi =>
	        {
	            var api = (HubConfigAPI)oapi;
	            return api.WriteAuditConfigReport(dto);
	        });
	    }
        #endregion

        #region LTAService Overrides

        protected override APIBase NewAPIInst(R360Shared.R360ServiceContext serviceContext)
		{
			return new HubConfigAPI(serviceContext);
		}

		#endregion
	}
}
