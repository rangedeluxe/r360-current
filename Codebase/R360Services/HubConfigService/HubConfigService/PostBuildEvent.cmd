REM TargetPath = %1 
REM TargetDir = %2
REM ProjectDir = %3
REM OutDir = %4

if %4=="bin\Debug\." goto CreateDebuggingConfig
goto End

:CreateDebuggingConfig
echo Info: Setting Debugging Configuration
copy %1.config %2\GenericWcfServiceHost.exe.config
copy %3\..\..\ServiceShares\GenericWcfServiceHost\%4\GenericWcfServiceHost.exe %2
REM %3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\GenericWcfServiceHost.exe.config /replace:localhost:9501/RecHubConfigService;;;localhost:8732/Design_Time_Addresses/RecHubConfigService

:End
if not exist %3\PostBuildEventCustom.cmd goto End2
%3\PostBuildEventCustom.cmd %1 %2 %3 %4

:End2
echo Info: Post Build Event Complete