#Write-Host �Deploying Build To QA Pending <Current> Folder"
#Write-Host $MyInvocation.MyCommand.Path
#$scriptPath = Split-Path -Parent -Path $MyInvocation.MyCommand.Path
#
#$sourcePathRoot = Join-Path $scriptPath "..\..\..\..\..\Binaries\*"
#$sourcePathRootDir = Join-Path $scriptPath "..\..\..\..\..\Binaries\"
#Write-Host "Copying From: " $sourcePathRoot
#
#$targetPathRoot = "\\172.27.2.160\ProductDevelopment\Legacy\QA Pending\RecHub_Build_Location\2014\2.01\Current_Build\RecHub.R360_Service.HubConfigService"
#Write-Host "Copying To: " $targetPathRoot
#
##wwwroot target folder
#$serviceRoot = Join-Path $targetPathRoot "wwwroot\R360Services\HubConfigService\"
#
## web.config
#$configTarget = Join-Path $serviceRoot "web.config.oem"
#Copy-Item -path $sourcePathRoot -destination $configTarget -Verbose -include "HubConfigService.dll.config"
#
## bin
#$serviceRoot = Join-Path $serviceRoot "bin\"
#$serviceDlls = `
#	"HubConfigAPI.dll", `
#	"HubConfigCommon.dll", `
#	"HubConfigDAL.dll", `
#	"HubConfigService.dll", `
#	"DALBase.dll", `
#	"ipoCrypto.dll", `
#	"ipoDB.dll", `
#	"ipoLib.dll", `
#	"ipoLog.dll", `
#	"R360Shared.dll", `
#	"SessionDAL.dll"
#Copy-Item -path $sourcePathRoot -destination $serviceRoot -Verbose -include $serviceDlls
#
## Do not include the local/temp IPOnline.ini in the service build
##Copy-Item -path $sourcePathRoot -destination $serviceRoot -Verbose -include HubConfigService.IPOnline.ini
#
#
##HubConfigClient target folder
#$clientBin = Join-Path $targetPathRoot "HubConfigClient\bin\"
#$clientDlls = `
#	"HubConfigCommon.dll", `
#	"HubConfigServicesClient.dll", `
#	"ipoLib.dll", `
#	"ipoLog.dll", `
#	"ltaLog.dll", `
#	"R360Shared.dll"
#Copy-Item -path $sourcePathRoot -destination $clientBin -Verbose -include $clientDlls



#Shell to Common R360Services Deployment Script, with correct parameters
$thisScript = Split-Path $MyInvocation.MyCommand.Path 
$commonScript = Join-Path $thisScript "..\..\ServiceShares\R360BuildDeployment.ps1"
$commonScript = [System.IO.Path]::GetFullPath($commonScript)

$commandWithParameters = "$commonScript" `
    + " -service RecHub.R360_Service.HubConfigService" `
    + " -appConfig:HubConfigService.dll.config" `
    + " -folderName:RecHubConfigService" `
    + " -dlls:`"" `
		+ "HubConfigAPI.dll," `
	    + "HubConfigCommon.dll," `
	    + "HubConfigDAL.dll," `
	    + "HubConfigService.dll," `
	    + "DALBase.dll," `
	    + "ipoCrypto.dll," `
	    + "ipoDB.dll," `
	    + "ipoLib.dll," `
	    + "ipoLog.dll," `
		+ "ltaLog.dll," `
		+ "R360RaamClient.dll," `
	    + "R360Shared.dll," `
	    + "SessionDAL.dll," `
		+ "SessionMainteanceCommon.dll," `
		+ "SessionMaintenanceServiceClient.dll," `
	    + "Wfs.Raam.Core.dll," `
		+ "Wfs.Raam.Service.Authorization.Contracts.dll," `
	    + "Thinktecture.IdentityModel.dll," `
		+ "WFS.System.Logging.dll," `
		+ "WFS.RecHub.ApplicationBlocks.RAAM.dll" `
        + "`"" `
    + " -web" `

Write-Host "Invoking Deployment Script with parameters: " $commandWithParameters

invoke-expression $commandWithParameters

Write-Host "Deployment Complete"

