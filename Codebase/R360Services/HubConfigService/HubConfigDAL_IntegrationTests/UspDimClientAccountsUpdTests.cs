﻿using System.Linq;
using Dapper;
using DatabaseIntegrationTestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.HubConfig;
// ReSharper disable RedundantArgumentDefaultValue

namespace HubConfigDAL_IntegrationTests
{
    [TestClass]
    public class UspDimClientAccountsUpdTests
    {
        private const string PrefixDataEntryAdded = "Added dimWorkgroupDataEntryColumns";
        private const string PrefixDataEntryUpdated = "Updated dimWorkgroupDataEntryColumns";
        private const string PrefixPostDepositExceptions = "Post-Deposit Exceptions";

        #region Setup / Teardown
        [ClassInitialize]
        public static void ClassSetUp(TestContext context)
        {
            Database = IntegrationDatabase.CreateNew();
            WorkgroupMaintenanceEnvironment.MakeDatabaseObjects(Database);
        }
        [ClassCleanup]
        public static void ClassTearDown()
        {
            Database?.Dispose();
            Database = null;
        }
        [TestInitialize]
        public void SetUp()
        {
            Database.ClearTables();
        }
        #endregion

        private static IntegrationDatabase Database { get; set; }

        private static DataEntryColumnDto CreateDataEntryFieldDto(WorkgroupMaintenanceEnvironment environment,
            string fieldName = null, string uiLabel = null, bool isRequired = false,
            int dataEntryColumnKey = 0)
        {
            return new DataEntryColumnDto
            {
                DataEntryColumnKey = dataEntryColumnKey,
                BatchSourceKey = environment.BatchSourceKey,
                FieldName = fieldName,
                UILabel = uiLabel,
                IsActive = true,
                IsRequired = isRequired,
            };
        }

        [TestMethod]
        public void EditWorkgroup_AddsWorkgroupRecordAndMakesItMostRecent()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(), longName: "New Long Name");

            var mostRecentQuery = Database.Connection.Query(
                "SELECT ClientAccountKey, MostRecent " +
                "FROM RecHubData.dimClientAccounts " +
                "ORDER BY ClientAccountKey")
                .ToList();
            Assert.AreEqual(2, mostRecentQuery.Count, "Record count");
            Assert.AreEqual(workgroup.GetInsertedId(), mostRecentQuery[0].ClientAccountKey, "Old record: key");
            Assert.IsFalse(mostRecentQuery[0].MostRecent, "Old record: MostRecent");
            Assert.AreNotEqual(workgroup.GetInsertedId(), mostRecentQuery[1].ClientAccountKey, "New record: key");
            Assert.IsTrue(mostRecentQuery[1].MostRecent, "New record: MostRecent");
        }
        [TestMethod]
        public void EditWorkgroup_AddTwoDataEntryFields_DataEntryAuditMessages()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();
            var dataEntryFieldOne = CreateDataEntryFieldDto(environment, "Field 1", isRequired: true);
            var dataEntryFieldTwo = CreateDataEntryFieldDto(environment, "Field 2", isRequired: false);

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(),
                dataEntryFields: new[] {dataEntryFieldOne, dataEntryFieldTwo});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixDataEntryAdded).Containing(
                $"BatchSourceKey = {environment.BatchSourceKey}",
                "FieldName = Field 1",
                "IsActive = 1",
                "IsRequired = 1");
            auditMessages.AssertHasMessageStarting(PrefixDataEntryAdded).Containing(
                $"BatchSourceKey = {environment.BatchSourceKey}",
                "FieldName = Field 2",
                "IsActive = 1",
                "IsRequired = 0");
            auditMessages.AssertNoMoreMessagesStarting(PrefixDataEntryAdded);
        }
        [TestMethod]
        public void EditWorkgroup_EditDataEntryField_SavesUiLabelChangesButNotFieldNameChanges()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();
            var existingDataEntryColumn = environment.InsertDimWorkgroupDataEntryColumn("Old Field Name");
            var dataEntryField = CreateDataEntryFieldDto(environment, "New Field Name",
                uiLabel: "New UI Label", dataEntryColumnKey: existingDataEntryColumn.GetInsertedId());

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(),
                dataEntryFields: new[] {dataEntryField});

            var dataEntryFields = Database.Connection.Query(
                "SELECT FieldName, UILabel FROM RecHubData.dimWorkgroupDataEntryColumns")
                .ToList();
            Assert.AreEqual(1, dataEntryFields.Count, "Data Entry field count");
            Assert.AreEqual("Old Field Name", dataEntryFields[0].FieldName);
            Assert.AreEqual("New UI Label", dataEntryFields[0].UILabel);
        }
        [TestMethod]
        public void EditWorkgroup_EditOneAndOnlyDataEntryField_DataEntryAuditMessage()
        {
            //TODO: Update this test to edit two DE fields, if/when DE-update auditing is fixed

            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();
            var existingDataEntryColumn = environment.InsertDimWorkgroupDataEntryColumn("Old Field Name");
            var dataEntryField = CreateDataEntryFieldDto(environment, uiLabel: "New UI Label",
                isRequired: true, dataEntryColumnKey: existingDataEntryColumn.GetInsertedId());

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(),
                dataEntryFields: new[] {dataEntryField});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.Debug();
            auditMessages.AssertHasMessageStarting(PrefixDataEntryUpdated).Containing(
                $"BatchSourceKey = {environment.BatchSourceKey}",
                "FieldName = Old Field Name",
                "IsActive = 1",
                "IsRequired = 1");
        }
        [TestMethod]
        public void EditWorkgroup_AddTwoDataEntryFields_GeneratesAuditMessagesForIsRequired()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();
            var dataEntryFieldOne = CreateDataEntryFieldDto(environment, "Field 1", uiLabel: "UILabel 1");
            var dataEntryFieldTwo = CreateDataEntryFieldDto(environment, "Field 2", uiLabel: "UILabel 2");

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(),
                dataEntryFields: new[] {dataEntryFieldOne, dataEntryFieldTwo});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing(
                WorkgroupMaintenanceEnvironment.UserDisplayName,
                "the \"Field Is Required\" Exception business rule",
                $"Bank {WorkgroupMaintenanceEnvironment.SiteBankId}",
                $"Workgroup {WorkgroupMaintenanceEnvironment.SiteClientAccountId}-{WorkgroupMaintenanceEnvironment.WorkgroupLongName}",
                "Data entry field name Field 1",
                "Data entry UI Label UILabel 1",
                $"payment source {WorkgroupMaintenanceEnvironment.PaymentSourceLongName}");
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing(
                WorkgroupMaintenanceEnvironment.UserDisplayName,
                "the \"Field Is Required\" Exception business rule",
                $"Bank {WorkgroupMaintenanceEnvironment.SiteBankId}",
                $"Workgroup {WorkgroupMaintenanceEnvironment.SiteClientAccountId}-{WorkgroupMaintenanceEnvironment.WorkgroupLongName}",
                "Data entry field name Field 2",
                "Data entry UI Label UILabel 2",
                $"payment source {WorkgroupMaintenanceEnvironment.PaymentSourceLongName}");
            auditMessages.AssertNoMoreMessagesStarting(PrefixPostDepositExceptions);
        }
        [TestMethod]
        public void EditWorkgroup_AddDataEntryField_IsRequiredIsFalse_GeneratesAuditMessagesForIsRequired()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();
            var dataEntryField = CreateDataEntryFieldDto(environment, isRequired: false);

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(),
                dataEntryFields: new[] {dataEntryField});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing("has turned off");
            auditMessages.AssertNoMoreMessagesStarting(PrefixPostDepositExceptions);
        }
        [TestMethod]
        public void EditWorkgroup_AddDataEntryField_IsRequiredIsTrue_GeneratesAuditMessageForIsRequired()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();
            var dataEntryField = CreateDataEntryFieldDto(environment, isRequired: true);

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(),
                dataEntryFields: new[] {dataEntryField});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing("has turned on");
            auditMessages.AssertNoMoreMessagesStarting(PrefixPostDepositExceptions);
        }
        [TestMethod]
        public void EditWorkgroup_EditDataEntryField_IsRequiredChanges_GeneratesAuditMessagesForIsRequired()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();
            var dataEntryField = environment.InsertDimWorkgroupDataEntryColumn("Field 1", isRequired: false);
            var dataEntryFieldDto = CreateDataEntryFieldDto(environment, isRequired: true,
                dataEntryColumnKey: dataEntryField.GetInsertedId());

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(),
                dataEntryFields: new[] {dataEntryFieldDto});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing("has turned on");
            auditMessages.AssertNoMoreMessagesStarting(PrefixPostDepositExceptions);
        }
        [TestMethod]
        public void EditWorkgroup_EditDataEntryField_IsRequiredUnchanged_DoesNotAuditIsRequired()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var workgroup = environment.InsertDimClientAccount();
            var dataEntryField = environment.InsertDimWorkgroupDataEntryColumn("Field 1", isRequired: false);
            var dataEntryFieldDto = CreateDataEntryFieldDto(environment, isRequired: false,
                dataEntryColumnKey: dataEntryField.GetInsertedId());

            environment.UspDimClientAccountsUpd(workgroup.GetInsertedId(),
                dataEntryFields: new[] {dataEntryFieldDto});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertNoMoreMessagesStarting(PrefixPostDepositExceptions);
        }
    }
}