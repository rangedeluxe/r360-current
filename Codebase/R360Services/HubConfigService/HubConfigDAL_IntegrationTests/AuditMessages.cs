﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DatabaseIntegrationTestHelpers;
using DatabaseIntegrationTestHelpers.R360;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HubConfigDAL_IntegrationTests
{
    public class AuditMessages
    {
        private AuditMessages(IEnumerable<string> messages)
        {
            Messages = messages.ToList();
        }

        private IList<string> Messages { get; }

        public static AuditMessages Load(IntegrationDatabase database)
        {
            var messages = RecHubAudit.GetAuditMessages(database).ToList();
            return new AuditMessages(messages);
        }

        private IEnumerable<string> GetMessagesStartingWith(string startsWith)
        {
            var regex = "^" + Regex.Escape(startsWith) + "\\b";
            var messages = Messages.Where(m => Regex.IsMatch(m, regex));
            return messages;
        }
        public AuditMessageAssert AssertHasMessageStarting(string startsWith)
        {
            var message = GetMessagesStartingWith(startsWith).FirstOrDefault();
            if (message == null)
            {
                Assert.Fail(
                    $"Expected to find audit message starting with \"{startsWith}\".\r\n{DescribeActualMessages()}");
            }

            Messages.Remove(message);
            return new AuditMessageAssert(message);
        }
        [PublicAPI]
        public AuditMessages Debug()
        {
            Console.WriteLine($"Audit messages: (count = {Messages.Count})");
            foreach (var message in Messages)
            {
                Console.WriteLine($"** {message}");
            }
            return this;
        }
        private string DescribeActualMessages()
        {
            if (!Messages.Any())
                return "No remaining audit messages.";

            var bulletedMessages = Messages.Select(m => $"** {m}");
            return $"Remaining audit messages (count = {Messages.Count}):\r\n" + string.Join("\r\n", bulletedMessages);
        }
        public void AssertNoMoreMessages()
        {
            if (Messages.Any())
            {
                Assert.Fail("Expected no remaining audit messages.\r\n" + DescribeActualMessages());
            }
        }
        public void AssertNoMoreMessagesStarting(string startsWith)
        {
            if (GetMessagesStartingWith(startsWith).Any())
            {
                Assert.Fail(
                    $"Expected no remaining audit messages starting with \"{startsWith}\".\r\n" +
                    DescribeActualMessages());
            }
        }
    }
}