﻿using System;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HubConfigDAL_IntegrationTests
{
    public class AuditMessageAssert
    {
        public AuditMessageAssert(string message)
        {
            Message = message;
        }

        private string Message { get; }

        [PublicAPI]
        public AuditMessageAssert Containing(params string[] substrings)
        {
            foreach (var substring in substrings)
            {
                var regex = "\\b" + Regex.Escape(substring) + "\\b";
                if (!Regex.IsMatch(Message, regex))
                {
                    Assert.Fail($"Expected audit message to contain \"{substring}\" but was: \"{Message}\"");
                }
            }
            return this;
        }
        [PublicAPI]
        public AuditMessageAssert Debug()
        {
            Console.WriteLine($"Audit message: \"{Message}\"");
            return this;
        }
    }
}