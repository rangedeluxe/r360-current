﻿using System;
using System.Collections.Generic;
using System.Linq;
using DatabaseIntegrationTestHelpers;
using DatabaseIntegrationTestHelpers.R360;
using WFS.RecHub.HubConfig;

namespace HubConfigDAL_IntegrationTests
{
    public class WorkgroupMaintenanceEnvironment
    {
        public const int EntityId = 45;
        private const string PaymentSourceShortName = "Payment Source 1";
        public const string PaymentSourceLongName = "Payment Source 1 - Long Name";
        public const int SiteBankId = 30;
        public const int SiteClientAccountId = 101;
        private const int SiteCodeId = -1;
        public const string UserDisplayName = "User U. User";
        public const string UserLogonName = "User1";
        private const string WorkgroupShortName = "Workgroup 1";
        public const string WorkgroupLongName = "Workgroup 1 - Long Name";

        private readonly Lazy<int> _getBatchSourceKey;

        private WorkgroupMaintenanceEnvironment(IntegrationDatabase database, int userId,
            Func<int> getBatchSourceKey)
        {
            Database = database;
            UserId = userId;
            _getBatchSourceKey = new Lazy<int>(getBatchSourceKey);
        }

        public int BatchSourceKey => _getBatchSourceKey.Value;
        private IntegrationDatabase Database { get; }
        private int UserId { get; }

        public static void MakeDatabaseObjects(IntegrationDatabase database)
        {
            database.MakeTable("RecHubUser", "OLEntities");
            database.MakeTable("RecHubUser", "OLWorkgroups");
            database.MakeTable("RecHubUser", "Users");
            database.MakeTable("RecHubData", "dimBanks");
            database.MakeTable("RecHubData", "dimBatchSources");
            database.MakeTable("RecHubData", "dimDDAs");
            database.MakeTable("RecHubData", "dimSiteCodes");
            database.MakeTable("RecHubData", "dimWorkgroupDataEntryColumns");
            database.MakeTable("RecHubData", "ElectronicAccounts");
            database.MakeTable("RecHubData", "dimClientAccounts");
            database.MakeView("RecHubData", "dimBanksView");
            database.MakeDataType("RecHubCommon", "AuditValueChangeTable");
            database.MakeStoredProcedure("RecHubCommon", "usp_WfsRethrowException");
            database.MakeStoredProcedure("RecHubData", "usp_ClientAccountsDataEntryColumns_Merge");
            database.MakeStoredProcedure("RecHubData", "usp_DimClientAccounts_Ins");
            database.MakeStoredProcedure("RecHubData", "usp_DimClientAccounts_Upd");
            database.MakeStoredProcedure("RecHubData", "usp_ElectronicAccounts_Merge");
            database.MakeStoredProcedure("RecHubUser", "usp_OLWorkgroups_Merge");

            // Audit tables/procs
            database.MakeTable("RecHubData", "dimDates");
            database.MakeTable("RecHubAudit", "dimAuditApplications");
            database.MakeTable("RecHubAudit", "dimAuditColumns");
            database.MakeTable("RecHubAudit", "factDataAuditDetails", IgnoreAuditPartitions);
            database.MakeTable("RecHubAudit", "factDataAuditMessages", IgnoreAuditPartitions);
            database.MakeTable("RecHubAudit", "factDataAuditSummary", IgnoreAuditPartitions);
            database.MakeStoredProcedure("RecHubCommon", "usp_WFS_DataAudit_Ins");
            database.MakeStoredProcedure("RecHubCommon", "usp_WFS_DataAudit_Ins_WithDetails");
            database.MakeStoredProcedure("RecHubAudit", "usp_dimAuditApplications_Get");
            database.MakeStoredProcedure("RecHubAudit", "usp_dimAuditColumns_Get");
            database.MakeStoredProcedure("RecHubAudit", "usp_factDataAuditDetails_Ins");
            database.MakeStoredProcedure("RecHubAudit", "usp_factDataAuditMessages_Ins");
            database.MakeStoredProcedure("RecHubAudit", "usp_factDataAuditSummary_Ins");
        }
        private static string IgnoreAuditPartitions(string sql)
        {
            return sql.Replace("$(OnAuditPartition)", "");
        }
        /// <summary>
        /// Populates tables with background data in preparation for inserting or updating a workgroup.
        /// Assumes that the database objects have already been created via <see cref="MakeDatabaseObjects"/>.
        /// </summary>
        /// <param name="database">The integration database.</param>
        /// <returns>A new instance of <see cref="WorkgroupMaintenanceEnvironment"/>.</returns>
        public static WorkgroupMaintenanceEnvironment Initialize(IntegrationDatabase database)
        {
            database.ClearTables();
            RecHubData.InsertDimDates(database, DateTime.Today);

            var user = RecHubUser.InsertUser(database, logonName: UserLogonName);

            RecHubAudit.InsertAuditColumn(database, 0, "Undefined", "Undefined", "Undefined");
            RecHubUser.InsertOlEntity(database, EntityId);
            RecHubData.InsertDimBank(database, SiteBankId, EntityId);
            RecHubData.InsertDimSiteCode(database, SiteCodeId);

            return new WorkgroupMaintenanceEnvironment(database, userId: user.GetInsertedId(),
                getBatchSourceKey: () =>
                {
                    var batchSource = RecHubData.InsertBatchSource(database,
                        shortName: PaymentSourceShortName, longName: PaymentSourceLongName);
                    return batchSource.GetInsertedId();
                });
        }
        public InsertResult InsertDimClientAccount()
        {
            return RecHubData.InsertDimClientAccount(Database, SiteClientAccountId, SiteBankId, SiteCodeId,
                shortName: WorkgroupShortName, longName: WorkgroupLongName);
        }
        public InsertResult InsertDimWorkgroupDataEntryColumn(string fieldName, bool isRequired = false)
        {
            return RecHubData.InsertDimWorkgroupDataEntryColumn(Database, BatchSourceKey, fieldName,
                isRequired: isRequired);
        }
        public void UspDimClientAccountsIns(IEnumerable<DataEntryColumnDto> dataEntryFields = null)
        {
            RecHubData.UspDimClientAccountsIns(Database, SiteClientAccountId, SiteBankId, EntityId, UserId,
                userDisplayName: UserDisplayName, shortName: WorkgroupShortName, longName: WorkgroupLongName,
                dataEntryFieldsXml: dataEntryFields?.ToList().ToDBXml());
        }
        public void UspDimClientAccountsUpd(int clientAccountKey,
            string shortName = null, string longName = null,
            IEnumerable<DataEntryColumnDto> dataEntryFields = null)
        {
            RecHubData.UspDimClientAccountsUpd(Database, clientAccountKey: clientAccountKey, userId: UserId,
                entityId: EntityId, userDisplayName: UserDisplayName,
                shortName: shortName ?? WorkgroupShortName,
                longName: longName ?? WorkgroupLongName,
                dataEntryFieldsXml: dataEntryFields?.ToList().ToDBXml());
        }
    }
}