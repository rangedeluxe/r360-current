﻿using Dapper;
using DatabaseIntegrationTestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.HubConfig;
// ReSharper disable RedundantArgumentDefaultValue

namespace HubConfigDAL_IntegrationTests
{
    [TestClass]
    public class UspDimClientAccountsInsTests
    {
        private const string PrefixDataEntryAdded = "Added dimWorkgroupDataEntryColumns";
        private const string PrefixPostDepositExceptions = "Post-Deposit Exceptions";

        #region Setup / Teardown
        [ClassInitialize]
        public static void ClassSetUp(TestContext context)
        {
            Database = IntegrationDatabase.CreateNew();
            WorkgroupMaintenanceEnvironment.MakeDatabaseObjects(Database);
        }
        [ClassCleanup]
        public static void ClassTearDown()
        {
            Database?.Dispose();
            Database = null;
        }
        [TestInitialize]
        public void SetUp()
        {
            Database.ClearTables();
        }
        #endregion

        private static IntegrationDatabase Database { get; set; }

        private static DataEntryColumnDto CreateDataEntryFieldDto(WorkgroupMaintenanceEnvironment environment,
            string fieldName = null, string uiLabel = null, bool isRequired = false)
        {
            return new DataEntryColumnDto
            {
                BatchSourceKey = environment.BatchSourceKey,
                FieldName = fieldName,
                IsActive = true,
                IsRequired = isRequired,
                UILabel = uiLabel ?? "UILabel",
            };
        }

        [TestMethod]
        public void AddWorkgroup()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);

            environment.UspDimClientAccountsIns();

            var count = Database.Connection.QuerySingle<int>("SELECT COUNT(*) FROM RecHubData.dimClientAccounts");
            Assert.AreEqual(1, count, "Record count");
        }
        [TestMethod]
        public void AddWorkgroup_NoDataEntryFields_AuditMessages()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);

            environment.UspDimClientAccountsIns();

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting("Added dimClientAccounts").Containing(
                $"EntityID: {WorkgroupMaintenanceEnvironment.EntityId}",
                $"SiteBankID = {WorkgroupMaintenanceEnvironment.SiteBankId}",
                $"WorkgroupID = {WorkgroupMaintenanceEnvironment.SiteClientAccountId}",
                "IsActive = 1");
            auditMessages.AssertHasMessageStarting("Workgroup Maintenance: Added OLWorkgroups").Containing(
                $"SiteBankID: {WorkgroupMaintenanceEnvironment.SiteBankId}",
                $"WorkgroupID: {WorkgroupMaintenanceEnvironment.SiteClientAccountId}");
            auditMessages.AssertNoMoreMessages();
        }
        [TestMethod]
        public void AddWorkgroup_TwoDataEntryFields_DataEntryAuditMessages()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var dataEntryFieldOne = CreateDataEntryFieldDto(environment, "Field 1", isRequired: true);
            var dataEntryFieldTwo = CreateDataEntryFieldDto(environment, "Field 2", isRequired: false);

            environment.UspDimClientAccountsIns(new[] {dataEntryFieldOne, dataEntryFieldTwo});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixDataEntryAdded).Debug().Containing(
                $"BatchSourceKey = {environment.BatchSourceKey}",
                "FieldName = Field 1",
                "IsActive = 1",
                "IsRequired = 1");
            auditMessages.AssertHasMessageStarting(PrefixDataEntryAdded).Debug().Containing(
                $"BatchSourceKey = {environment.BatchSourceKey}",
                "FieldName = Field 2",
                "IsActive = 1",
                "IsRequired = 0");
            auditMessages.AssertNoMoreMessagesStarting(PrefixDataEntryAdded);
        }
        [TestMethod]
        public void AddWorkgroup_TwoDataEntryFields_GeneratesAuditMessagesForIsRequired()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var dataEntryFieldOne = CreateDataEntryFieldDto(environment, "Field 1", uiLabel: "UILabel 1");
            var dataEntryFieldTwo = CreateDataEntryFieldDto(environment, "Field 2", uiLabel: "UILabel 2");

            environment.UspDimClientAccountsIns(new[] {dataEntryFieldOne, dataEntryFieldTwo});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing(
                WorkgroupMaintenanceEnvironment.UserDisplayName,
                "the \"Field Is Required\" Exception business rule",
                $"Bank {WorkgroupMaintenanceEnvironment.SiteBankId}",
                $"Workgroup {WorkgroupMaintenanceEnvironment.SiteClientAccountId}-{WorkgroupMaintenanceEnvironment.WorkgroupLongName}",
                "Data entry field name Field 1",
                "Data entry UI Label UILabel 1",
                $"payment source {WorkgroupMaintenanceEnvironment.PaymentSourceLongName}");
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing(
                WorkgroupMaintenanceEnvironment.UserDisplayName,
                "the \"Field Is Required\" Exception business rule",
                $"Bank {WorkgroupMaintenanceEnvironment.SiteBankId}",
                $"Workgroup {WorkgroupMaintenanceEnvironment.SiteClientAccountId}-{WorkgroupMaintenanceEnvironment.WorkgroupLongName}",
                "Data entry field name Field 2",
                "Data entry UI Label UILabel 2",
                $"payment source {WorkgroupMaintenanceEnvironment.PaymentSourceLongName}");
            auditMessages.AssertNoMoreMessagesStarting(PrefixPostDepositExceptions);
        }
        [TestMethod]
        public void AddWorkgroup_DataEntryField_IsRequiredIsFalse_GeneratesAuditMessagesForIsRequired()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var dataEntryField = CreateDataEntryFieldDto(environment, isRequired: false);

            environment.UspDimClientAccountsIns(new[] {dataEntryField});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing("has turned off");
            auditMessages.AssertNoMoreMessagesStarting(PrefixPostDepositExceptions);
        }
        [TestMethod]
        public void AddWorkgroup_DataEntryField_IsRequiredIsTrue_GeneratesAuditMessagesForIsRequired()
        {
            var environment = WorkgroupMaintenanceEnvironment.Initialize(Database);
            var dataEntryField = CreateDataEntryFieldDto(environment, isRequired: true);

            environment.UspDimClientAccountsIns(new[] {dataEntryField});

            var auditMessages = AuditMessages.Load(Database);
            auditMessages.AssertHasMessageStarting(PrefixPostDepositExceptions).Containing("has turned on");
            auditMessages.AssertNoMoreMessagesStarting(PrefixPostDepositExceptions);
        }
    }
}
