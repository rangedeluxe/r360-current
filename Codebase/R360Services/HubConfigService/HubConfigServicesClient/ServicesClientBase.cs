﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WFS.LTA.Common;

namespace WFS.RecHub.HubConfig
{
	public abstract class ServicesClientBase
	{
		private ltaLog _EventLog = null;
		private string _SiteKey;

		public ServicesClientBase(string vSiteKey, ltaLog log)
		{
			_SiteKey = vSiteKey;
			_EventLog = log;
		}

		/// <summary>
		/// Determines the section of the local .ini file to be used for local 
		/// options.
		/// </summary>
		protected string SiteKey
		{
			get { return _SiteKey; }
		}

		protected ltaLog EventLog
		{
			get { return _EventLog; }
		}
	}
}
