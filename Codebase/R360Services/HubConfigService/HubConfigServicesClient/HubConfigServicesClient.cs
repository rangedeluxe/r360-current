﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using WFS.LTA.Common;
using WFS.RecHub.HubConfig.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.HubConfig
{
	internal class HubConfigServicesClient : ServicesClientBase, IHubConfig
	{
		private readonly IHubConfig _HubConfigService = null;

		public HubConfigServicesClient(string vSiteKey, ltaLog log)
			: base(vSiteKey, log)
		{
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger("HubConfigSvcsClient");
			_HubConfigService = R360ServiceFactory.Create<IHubConfig>();
		}

		private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
		{
			// All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
			try
			{
				using (new OperationContextScope((IContextChannel)_HubConfigService))
				{
					// Set context inside of operation scope
					R360ServiceContext.Init(this.SiteKey);

					return operation();
				}
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, "HubConfigSvcsClient", methodName);
				throw;
			}
		}

		public HubConfigResponse<string> Ping()
		{
			return Do(_HubConfigService.Ping);
		}

		public HubConfigResponse<List<WorkgroupDto>> GetWorkgroups(int entityID)
		{
			return Do(() => _HubConfigService.GetWorkgroups(entityID));
		}

		public HubConfigResponse<List<WorkgroupDto>> GetUnassignedWorkgroups(int fiEntityID)
		{
			return Do(() => _HubConfigService.GetUnassignedWorkgroups(fiEntityID));
		}

		public HubConfigResponse<int> GetUnassignedWorkgroupCount(int fiEntityID)
		{
			return Do(() => _HubConfigService.GetUnassignedWorkgroupCount(fiEntityID));
		}

        public HubConfigResponse<WorkgroupDto> GetWorkgroup(int clientAccountKey, GetWorkgroupAdditionalData additionalData)
        {
            return Do(() => _HubConfigService.GetWorkgroup(clientAccountKey, additionalData));
        }

        public HubConfigResponse<WorkgroupDto> GetWorkgroupByWorkgroupAndBank(int siteClientAccountId, int siteBankId,
            GetWorkgroupAdditionalData additionalData)
        {
            return Do(() => _HubConfigService.GetWorkgroupByWorkgroupAndBank(siteClientAccountId, siteBankId, additionalData));
        }

        public HubConfigResponse<int> AddWorkgroup(WorkgroupDto workgroup)
		{
			return Do(() => _HubConfigService.AddWorkgroup(workgroup));
		}

		public HubConfigResponse<int> UpdateWorkgroup(WorkgroupDto workgroup)
		{
			return Do(() => _HubConfigService.UpdateWorkgroup(workgroup));
		}

		public HubConfigResponse<bool> SaveAssignments(List<WorkgroupDto> workgroups)
		{
			return Do(() => _HubConfigService.SaveAssignments(workgroups));
		}

		public HubConfigResponse<bool> RemoveWorkgroupFromEntity(int clientAccountKey)
		{
			return Do(() => _HubConfigService.RemoveWorkgroupFromEntity(clientAccountKey));
		}

		public HubConfigResponse<ElectronicAccountDto> GetElectronicAccountByDDA(DDADto dda)
		{
			return Do(() => _HubConfigService.GetElectronicAccountByDDA(dda));
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetValidationTableTypes()
		{
			return Do(() => _HubConfigService.GetValidationTableTypes());
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchSources(int fiEntityID)
		{
			return Do(() => _HubConfigService.GetBatchSources(fiEntityID));
		}

        public HubConfigResponse<List<PaymentSourceDto>> GetPaymentSources()
        {
            return Do(() => _HubConfigService.GetPaymentSources());
        }

        public HubConfigResponse<PaymentSourceDto> GetPaymentSource(int batchSourceKey)
        {
            return Do(() => _HubConfigService.GetPaymentSource(batchSourceKey));
        }

        public HubConfigResponse<int> AddPaymentSource(PaymentSourceDto paymentSource)
        {
            return Do(() => _HubConfigService.AddPaymentSource(paymentSource));
        }

        public BaseResponse UpdatePaymentSource(PaymentSourceDto paymentSource)
        {
            return Do(() => _HubConfigService.UpdatePaymentSource(paymentSource));
        }

        public BaseResponse DeletePaymentSource(PaymentSourceDto paymentSource)
        {
            return Do(() => _HubConfigService.DeletePaymentSource(paymentSource));
        }

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchPaymentTypes()
		{
			return Do(() => _HubConfigService.GetBatchPaymentTypes());
		}

		public HubConfigResponse<List<BatchPaymentSubTypeDto>> GetBatchPaymentSubTypes()
		{
			return Do(() => _HubConfigService.GetBatchPaymentSubTypes());
		}

        public HubConfigResponse<List<Tuple<int, string>>> GetSystemTypes()
        {
            return Do(() => _HubConfigService.GetSystemTypes());
        }

		public HubConfigResponse<List<BankDto>> GetBanks()
		{
			return Do(() => _HubConfigService.GetBanks());
		}

		public HubConfigResponse<List<BankDto>> GetBanksForFI(int fiEntityID)
		{
			return Do(() => _HubConfigService.GetBanksForFI(fiEntityID));
		}

		public HubConfigResponse<BankDto> GetBank(int bankKey)
		{
			return Do(() => _HubConfigService.GetBank(bankKey));
		}

		public HubConfigResponse<int> AddBank(BankDto bank)
		{
			return Do(() => _HubConfigService.AddBank(bank));
		}

		public HubConfigResponse<int> UpdateBank(BankDto bank)
		{
			return Do(() => _HubConfigService.UpdateBank(bank));
		}

        public HubConfigResponse<RecHubEntityDto> GetEntity(int entityId)
        {
            return Do(() => _HubConfigService.GetEntity(entityId));
        }

        public HubConfigResponse<RecHubEntityDto> GetOrCreateEntityWorkgroupDefaults(int entityId)
        {
            return Do(() => _HubConfigService.GetOrCreateEntityWorkgroupDefaults(entityId));
        }

		public BaseResponse AddUpdateEntity(RecHubEntityDto entity)
		{
			return Do(() => _HubConfigService.AddUpdateEntity(entity));
		}

        public HubConfigResponse<List<EventRuleDto>> GetEventRules()
        {
            return Do(() => _HubConfigService.GetEventRules());
        }

        public HubConfigResponse<List<EventDto>> GetEvents()
        {
            return Do(() => _HubConfigService.GetEvents());
        }

        public HubConfigResponse<int> AddEventRule(EventRuleDto eventRule)
        {
            return Do(() => _HubConfigService.AddEventRule(eventRule));
        }

        public BaseResponse UpdateEventRule(EventRuleDto eventRule)
        {
            return Do(() => _HubConfigService.UpdateEventRule(eventRule));
        }

        public HubConfigResponse<EventRuleDto> GetEventRule(int eventRuleID)
        {
            return Do(() => _HubConfigService.GetEventRule(eventRuleID));
        }

        public HubConfigResponse<List<PreferenceDto>> GetPreferences()
        {
            return Do(() => _HubConfigService.GetPreferences());
        }

        public BaseResponse UpdatePreferences(List<PreferenceDto> preferences)
        {
            return Do(() => _HubConfigService.UpdatePreferences(preferences));
        }

		public HubConfigResponse<int> GetSystemMaxRetentionDays()
		{
			return Do(() => _HubConfigService.GetSystemMaxRetentionDays());
		}

		public HubConfigResponse<List<DataEntryTemplateDto>> GetDataEntryTemplates()
		{
			return Do(() => _HubConfigService.GetDataEntryTemplates());
		}

        public HubConfigResponse<List<DocumentTypeDto>> GetDocumentTypes()
        {
            return Do(() => _HubConfigService.GetDocumentTypes());
        }

        public HubConfigResponse<bool> UpdateDocumentType(DocumentTypeDto type)
        {
            return Do(() => _HubConfigService.UpdateDocumentType(type));
        }

        public HubConfigResponse<List<NotificationTypeDto>> GetNotificationTypes()
        {
            return Do(() => _HubConfigService.GetNotificationTypes());
        }

        public HubConfigResponse<bool> UpdateNotificationType(NotificationTypeDto type)
        {
            return Do(() => _HubConfigService.UpdateNotificationType(type));
        }

        public HubConfigResponse<bool> InsertNotificationType(NotificationTypeDto type)
        {
            return Do(() => _HubConfigService.InsertNotificationType(type));
        }

		public HubConfigResponse<List<BusinessRulesDto>> GetFieldValidationSettings(int siteBankID, int workgroupId)
		{
			return Do(() => _HubConfigService.GetFieldValidationSettings(siteBankID, workgroupId));
		}

	    public HubConfigResponse<bool> WriteAuditConfigReport(AuditConfigDto dto)
	    {
	        return Do(() => _HubConfigService.WriteAuditConfigReport(dto));
	    }
	}
}
