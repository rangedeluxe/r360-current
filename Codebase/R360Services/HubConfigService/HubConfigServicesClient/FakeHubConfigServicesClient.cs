﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.HubConfig.DTO;

namespace WFS.RecHub.HubConfig
{
	class FakeHubConfigServicesClient : IHubConfig
	{
		private string _siteKey;
		private LTA.Common.ltaLog _eventLog;

		public FakeHubConfigServicesClient(string _siteKey, LTA.Common.ltaLog _eventLog)
		{
			this._siteKey = _siteKey;
			this._eventLog = _eventLog;
		}

		public HubConfigResponse<string> Ping()
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<WorkgroupDto>> GetWorkgroups(int entityID)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<WorkgroupDto>> GetUnassignedWorkgroups(int fiEntityID)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<int> GetUnassignedWorkgroupCount(int fiEntityID)
		{
			throw new NotImplementedException();
		}

        public HubConfigResponse<WorkgroupDto> GetWorkgroup(int clientAccountKey, GetWorkgroupAdditionalData additionalData)
		{
			throw new NotImplementedException();
		}


        public HubConfigResponse<WorkgroupDto> GetWorkgroupByWorkgroupAndBank(int siteClientAccountId, int siteBankId,
            GetWorkgroupAdditionalData additionalData)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> AddWorkgroup(WorkgroupDto workgroup)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<int> UpdateWorkgroup(WorkgroupDto workgroup)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<bool> SaveAssignments(List<WorkgroupDto> workgroups)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<bool> RemoveWorkgroupFromEntity(int clientAccountKey)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<ElectronicAccountDto> GetElectronicAccountByDDA(DDADto dda)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetValidationTableTypes()
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchSources(int fiEntityID)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchPaymentTypes()
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<BatchPaymentSubTypeDto>> GetBatchPaymentSubTypes()
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<BankDto>> GetBanks()
		{
			return new HubConfigResponse<List<BankDto>>()
			{
				Data = new BankDto[] 
				{ 
					new BankDto() { BankKey = 442, SiteBankID = 42, BankName = "Fake Bank 42", FIEntityID = 0, MostRecent = true }, 
					new BankDto() { BankKey = 443, SiteBankID = 43, BankName = "Fake Bank 43", FIEntityID = 1, MostRecent = true },
					new BankDto() { BankKey = 444, SiteBankID = 44, BankName = "Fake Bank 44", FIEntityID = 1, MostRecent = true },
					new BankDto() { BankKey = 445, SiteBankID = 45, BankName = "Fake Bank 45", FIEntityID = 1, MostRecent = true }, 
					new BankDto() { BankKey = 446, SiteBankID = 46, BankName = "Fake Bank 46", FIEntityID = 2, MostRecent = true },
					new BankDto() { BankKey = 447, SiteBankID = 47, BankName = "Fake Bank 47", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 448, SiteBankID = 48, BankName = "Fake Bank 48", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 449, SiteBankID = 49, BankName = "Fake Bank 49", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 450, SiteBankID = 50, BankName = "Fake Bank 50", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 451, SiteBankID = 51, BankName = "Fake Bank 51", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 452, SiteBankID = 52, BankName = "Fake Bank 52", FIEntityID = 3, MostRecent = true }, 
					new BankDto() { BankKey = 453, SiteBankID = 53, BankName = "Fake Bank 53", FIEntityID = 3, MostRecent = true }, 
					new BankDto() { BankKey = 454, SiteBankID = 54, BankName = "Fake Bank 54", FIEntityID = 3, MostRecent = true }, 
				}.ToList(),
				Errors = null,
				Status = R360Shared.StatusCode.SUCCESS,
			};
		}

		public HubConfigResponse<List<BankDto>> GetBanksForFI(int fiEntityID)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<BankDto> GetBank(int bankKey)
		{
			return new HubConfigResponse<BankDto>()
			{
				Data = GetBanks().Data.Find(o => o.BankKey == bankKey),
				Errors = null,
				Status = R360Shared.StatusCode.SUCCESS,
			};
		}

		public HubConfigResponse<int> AddBank(BankDto bank)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<int> UpdateBank(BankDto bank)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<PaymentSourceDto>> GetPaymentSources()
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetSystemTypes()
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<int> AddPaymentSource(PaymentSourceDto paymentSource)
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<PaymentSourceDto> GetPaymentSource(int batchSourceKey)
		{
			throw new NotImplementedException();
		}

		public R360Shared.BaseResponse UpdatePaymentSource(PaymentSourceDto paymentSource)
		{
			throw new NotImplementedException();
		}

        public R360Shared.BaseResponse DeletePaymentSource(PaymentSourceDto paymentSource)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<RecHubEntityDto> GetEntity(int entityId)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<RecHubEntityDto> GetOrCreateEntityWorkgroupDefaults(int entityId)
        {
            var entities = new[]
            {
                new RecHubEntityDto { EntityID = 1, DisplayBatchID = false, DocumentImageDisplayMode = ImageDisplayModes.FrontOnly, PaymentImageDisplayMode = ImageDisplayModes.Both, ViewingDays = 42, MaximumSearchDays = 30, BillingAccount = string.Empty, BillingField1 = string.Empty, BillingField2 = string.Empty },
                new RecHubEntityDto { EntityID = 2, DisplayBatchID = true, DocumentImageDisplayMode = ImageDisplayModes.Both, PaymentImageDisplayMode = ImageDisplayModes.Both, ViewingDays = 420, MaximumSearchDays = 300, BillingAccount = string.Empty, BillingField1 = string.Empty, BillingField2 = string.Empty },
                new RecHubEntityDto { EntityID = 0, DisplayBatchID = false, DocumentImageDisplayMode = ImageDisplayModes.UseInheritedSetting, PaymentImageDisplayMode = ImageDisplayModes.UseInheritedSetting, ViewingDays = 0, MaximumSearchDays = 0, BillingAccount = string.Empty, BillingField1 = string.Empty, BillingField2 = string.Empty },
            };
            var entity = entities.FirstOrDefault(o => o.EntityID == entityId);
            if (entity == null)
            {
                entity = entities.First(o => o.EntityID == 0);
                entity.EntityID = entityId;
            }

            return new HubConfigResponse<RecHubEntityDto>()
            {
                Data = entity,
                Errors = null,
                Status = R360Shared.StatusCode.SUCCESS,
            };
        }

		public R360Shared.BaseResponse AddUpdateEntity(RecHubEntityDto entity)
		{
			throw new NotImplementedException();
		}

        public HubConfigResponse<List<EventRuleDto>> GetEventRules()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<EventDto>> GetEvents()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<EventDto> GetEvent(EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<int> AddEventRule(EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

        public R360Shared.BaseResponse UpdateEventRule(EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<EventRuleDto> GetEventRule(int eventRuleID)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<PreferenceDto>> GetPreferences()
        {
            throw new NotImplementedException();
        }

        public R360Shared.BaseResponse UpdatePreferences(List<PreferenceDto> preferences)
        {
            throw new NotImplementedException();
        }

		public HubConfigResponse<int> GetSystemMaxRetentionDays()
		{
			throw new NotImplementedException();
		}

		public HubConfigResponse<List<DataEntryTemplateDto>> GetDataEntryTemplates()
		{
			throw new NotImplementedException();
		}

        public HubConfigResponse<List<DocumentTypeDto>> GetDocumentTypes()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> UpdateDocumentType(DocumentTypeDto type)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<List<NotificationTypeDto>> GetNotificationTypes()
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> UpdateNotificationType(NotificationTypeDto type)
        {
            throw new NotImplementedException();
        }

        public HubConfigResponse<bool> InsertNotificationType(NotificationTypeDto type)
        {
            throw new NotImplementedException();
        }

		public HubConfigResponse<List<BusinessRulesDto>> GetFieldValidationSettings(int siteBankID, int workgroupId)
		{
			throw new NotImplementedException();
		}

	    public HubConfigResponse<bool> WriteAuditConfigReport(AuditConfigDto dto)
	    {
	        throw new NotImplementedException();
	    }
	}
}
