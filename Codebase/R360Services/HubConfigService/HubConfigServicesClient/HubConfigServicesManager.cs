﻿using System;
using System.Collections.Generic;
using System.Configuration;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.HubConfig.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.HubConfig
{
	public class HubConfigServicesManager : IHubConfig
    {
		// Initialization parameters
		private const string LogSource = "HubConfigManager";
		private static ltaLog _eventLog;
		private static string _siteKey;
		private static cSiteOptions _SiteOptions = null;

		private readonly IHubConfig _client;

		/// <summary>
		/// Uses SiteKey to retrieve site options from the local .ini file.
		/// </summary>
		protected static cSiteOptions SiteOptions
		{
			get
			{
				if (_SiteOptions == null)
				{
					_SiteOptions = new cSiteOptions(_siteKey);
				}
				return _SiteOptions;
			}
		}

		/// <summary>
		/// Logging component using settings from the local siteOptions object.
		/// </summary>
		protected static ltaLog EventLog
		{
			get
			{
				if (_eventLog == null)
				{
					_eventLog = new ltaLog(SiteOptions.logFilePath,
											  SiteOptions.logFileMaxSize,
											  (LTAMessageImportance)SiteOptions.loggingDepth);
				}

				return _eventLog;
			}
		}

		protected static string SiteKey
		{
			get
			{
				return (_siteKey);
			}
			set
			{
				_siteKey = value;
			}
		}


		static HubConfigServicesManager()
        {
			// Make sure the site key is initialized before any other properties are used
			GetSiteKey();
        }

		public HubConfigServicesManager()
        {
            // Create connection context
            try
            {
				bool fakeClient = false;
				if (LogManager.IsDefault) LogManager.Logger = EventLog.LTAtoILogger(LogSource);
				bool.TryParse(ConfigHelpers.GetAppSetting("UseFakeHubConfigClient", "false"), out fakeClient);
				if (fakeClient)
					_client = new FakeHubConfigServicesClient(_siteKey, EventLog);
				else
					_client = new HubConfigServicesClient(_siteKey, EventLog);
            }
            catch (Exception ex)
            {
				EventLog.logError(ex, LogSource, ".ctor");

                // Constructors should not throw exceptions -- just initialize to null and report errors later...
                _client = null;
            }
        }

		// Specialied method for site key that properly initializes the EventLog after the site key has been set
		private static void GetSiteKey()
		{
			string defaultValue = "IPOnline";
			try
			{
				string value = ConfigurationManager.AppSettings["siteKey"];
				if (string.IsNullOrEmpty(value))
					value = defaultValue;

				_siteKey = value;

				EventLog.logEvent(string.Format("Using SiteKey = '{0}'", value), LogSource, LTA.Common.LTAMessageImportance.Verbose);
			}
			catch (Exception ex)
			{
				_siteKey = defaultValue;

				EventLog.logWarning(string.Format("Error reading configuration setting 'siteKey' - {0}", ex.Message), LogSource, LTA.Common.LTAMessageImportance.Verbose);
			}
		}

		private void ValidateClient()
		{
			if (_client == null)
				throw new Exception("Configuration Error - check log for details");
		}

		#region IHubConfigService Members

		public HubConfigResponse<string> Ping()
		{
			ValidateClient();
			return _client.Ping();
		}

		public HubConfigResponse<List<WorkgroupDto>> GetWorkgroups(int entityID)
		{
			ValidateClient();
			return _client.GetWorkgroups(entityID);
		}

		public HubConfigResponse<List<WorkgroupDto>> GetUnassignedWorkgroups(int fiEntityID)
		{
			ValidateClient();
			return _client.GetUnassignedWorkgroups(fiEntityID);
		}

		public HubConfigResponse<int> GetUnassignedWorkgroupCount(int fiEntityID)
		{
			ValidateClient();
			return _client.GetUnassignedWorkgroupCount(fiEntityID);
		}

        public HubConfigResponse<WorkgroupDto> GetWorkgroup(int clientAccountKey, GetWorkgroupAdditionalData additionalData)
        {
            ValidateClient();
            return _client.GetWorkgroup(clientAccountKey, additionalData);
        }

        public HubConfigResponse<WorkgroupDto> GetWorkgroupByWorkgroupAndBank(int siteClientAccountId, int siteBankId,
            GetWorkgroupAdditionalData additionalData)
        {
            ValidateClient();
            return _client.GetWorkgroupByWorkgroupAndBank(siteClientAccountId, siteBankId, additionalData);
        }

        public HubConfigResponse<int> AddWorkgroup(WorkgroupDto workgroup)
		{
			ValidateClient();
			return _client.AddWorkgroup(workgroup);
		}

		public HubConfigResponse<int> UpdateWorkgroup(WorkgroupDto workgroup)
		{
			ValidateClient();
			return _client.UpdateWorkgroup(workgroup);
		}

		public HubConfigResponse<bool> SaveAssignments(List<WorkgroupDto> workgroups)
		{
			ValidateClient();
			return _client.SaveAssignments(workgroups);
		}

		public HubConfigResponse<bool> RemoveWorkgroupFromEntity(int clientAccountKey)
		{
			ValidateClient();
			return _client.RemoveWorkgroupFromEntity(clientAccountKey);
		}

		public HubConfigResponse<ElectronicAccountDto> GetElectronicAccountByDDA(DDADto dda)
		{
			ValidateClient();
			return _client.GetElectronicAccountByDDA(dda);
		}

		public void GetBatchDataEntrySetup(/*TODO*/)
		{
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetValidationTableTypes()
		{
			ValidateClient();
			return _client.GetValidationTableTypes();
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchSources(int fiEntityID)
		{
			ValidateClient();
			return _client.GetBatchSources(fiEntityID);
		}

        public HubConfigResponse<List<PaymentSourceDto>> GetPaymentSources()
        {
            ValidateClient();
            return _client.GetPaymentSources();
        }

        public HubConfigResponse<PaymentSourceDto> GetPaymentSource(int batchSourceKey)
        {
            ValidateClient();
            return _client.GetPaymentSource(batchSourceKey);
        }

        public HubConfigResponse<int> AddPaymentSource(PaymentSourceDto paymentSource)
        {
            ValidateClient();
            return _client.AddPaymentSource(paymentSource);
        }

        public BaseResponse UpdatePaymentSource(PaymentSourceDto paymentSource)
        {
            ValidateClient();
            return _client.UpdatePaymentSource(paymentSource);
        }

        public BaseResponse DeletePaymentSource(PaymentSourceDto paymentSource)
        {
            ValidateClient();
            return _client.DeletePaymentSource(paymentSource);
        }

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchPaymentTypes()
		{
			ValidateClient();
			return _client.GetBatchPaymentTypes();
		}

		public HubConfigResponse<List<BatchPaymentSubTypeDto>> GetBatchPaymentSubTypes()
		{
			ValidateClient();
			return _client.GetBatchPaymentSubTypes();
		}

        public HubConfigResponse<List<Tuple<int, string>>> GetSystemTypes()
        {
            ValidateClient();
            return _client.GetSystemTypes();
        }

		public HubConfigResponse<List<BankDto>> GetBanks()
		{
			ValidateClient();
			return _client.GetBanks();
		}

		public HubConfigResponse<List<BankDto>> GetBanksForFI(int fiEntityID)
		{
			ValidateClient();
			return _client.GetBanksForFI(fiEntityID);
		}

		public HubConfigResponse<BankDto> GetBank(int bankKey)
		{
			ValidateClient();
			return _client.GetBank(bankKey);
		}

		public HubConfigResponse<int> AddBank(BankDto bank)
		{
			ValidateClient();
			return _client.AddBank(bank);
		}

		public HubConfigResponse<int> UpdateBank(BankDto bank)
		{
			ValidateClient();
			return _client.UpdateBank(bank);
		}

        public HubConfigResponse<RecHubEntityDto> GetEntity(int entityId)
        {
            ValidateClient();
            return _client.GetEntity(entityId);
        }

        public HubConfigResponse<RecHubEntityDto> GetOrCreateEntityWorkgroupDefaults(int entityId)
        {
            ValidateClient();
            return _client.GetOrCreateEntityWorkgroupDefaults(entityId);
        }

		public BaseResponse AddUpdateEntity(RecHubEntityDto entity)
		{
			ValidateClient();
			return _client.AddUpdateEntity(entity);
		}

        public HubConfigResponse<List<EventRuleDto>> GetEventRules()
        {
            ValidateClient();
            return _client.GetEventRules();
        }

        public HubConfigResponse<List<EventDto>> GetEvents()
        {
            ValidateClient();
            return _client.GetEvents();
        }

        public HubConfigResponse<int> AddEventRule(EventRuleDto eventRule)
        {
            ValidateClient();
            return _client.AddEventRule(eventRule);
        }

        public BaseResponse UpdateEventRule(EventRuleDto eventRule)
        {
            ValidateClient();
            return _client.UpdateEventRule(eventRule);
        }

        public HubConfigResponse<EventRuleDto> GetEventRule(int eventRuleID)
        {
            ValidateClient();
            return _client.GetEventRule(eventRuleID);
        }

        public HubConfigResponse<List<PreferenceDto>> GetPreferences()
        {
            ValidateClient();
            return _client.GetPreferences();
        }

        public BaseResponse UpdatePreferences(List<PreferenceDto> preferences)
        {
            ValidateClient();
            return _client.UpdatePreferences(preferences);
        }

		public HubConfigResponse<int> GetSystemMaxRetentionDays()
		{
			ValidateClient();
			return _client.GetSystemMaxRetentionDays();
		}

		public HubConfigResponse<List<DataEntryTemplateDto>> GetDataEntryTemplates()
		{
			ValidateClient();
			return _client.GetDataEntryTemplates();
		}

        public HubConfigResponse<List<DocumentTypeDto>> GetDocumentTypes()
        {
            ValidateClient();
            return _client.GetDocumentTypes();
        }

        public HubConfigResponse<bool> UpdateDocumentType(DocumentTypeDto type)
        {
            ValidateClient();
            return _client.UpdateDocumentType(type);
        }

        public HubConfigResponse<List<NotificationTypeDto>> GetNotificationTypes()
        {
            ValidateClient();
            return _client.GetNotificationTypes();
        }

        public HubConfigResponse<bool> UpdateNotificationType(NotificationTypeDto type)
        {
            ValidateClient();
            return _client.UpdateNotificationType(type);
        }

        public HubConfigResponse<bool> InsertNotificationType(NotificationTypeDto type)
        {
            ValidateClient();
            return _client.InsertNotificationType(type);
        }

		public HubConfigResponse<List<BusinessRulesDto>> GetFieldValidationSettings(int siteBankID, int workgroupId)
		{
			ValidateClient();
			return _client.GetFieldValidationSettings(siteBankID, workgroupId);
		}

	    public HubConfigResponse<bool> WriteAuditConfigReport(AuditConfigDto dto)
	    {
	        ValidateClient();
	        return _client.WriteAuditConfigReport(dto);
	    }

	    #endregion
    }
}
