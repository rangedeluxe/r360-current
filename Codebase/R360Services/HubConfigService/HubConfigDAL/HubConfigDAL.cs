﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using WFS.RecHub.DAL;
using WFS.RecHub.HubConfig.DTO;

namespace WFS.RecHub.HubConfig
{
	public class HubConfigDAL : _DALBase, IHubConfigDAL
    {
		public HubConfigDAL(string vSiteKey)
			: base(vSiteKey, ConnectionType.RecHubData)
		{
		}

		public bool GetUserIDBySID(Guid SID, out int userID)
		{
			bool bReturnValue = false;

			userID = -1;
			SqlParameter[] parms;
			DataTable results;
			try
			{
				parms = new SqlParameter[]
				{
                    BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input)
                };

				bReturnValue = Database.executeProcedure("RecHubUser.usp_Users_UserID_Get_BySID", parms, out results);
				if (bReturnValue)
					userID = Convert.ToInt32(results.Rows[0]["UserID"]);
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "GetUserIDBySID(Guid SID, out DataTable results)");
				bReturnValue = false;
			}

			return bReturnValue;
		}

		public bool GetWorkgroups(int entityID, out List<WorkgroupDto> workgroups)
		{
			workgroups = null;

			SqlParameter[] parms;
			DataTable dt;

			try
			{
				parms = new SqlParameter[]
				{
					BuildParameter("@parmEntityID", SqlDbType.Int, entityID, ParameterDirection.Input),
				};

				if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_ByEntityID", parms, out dt))
				{
					//Convert to DTO Type
					workgroups = new List<WorkgroupDto>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
                        workgroups.Add(row.ConvertToWorkgroupDto(DalWorkgroupAdditionalData.None));
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetUnassignedWorkgroupCount(int fiEntityID, out int count)
		{
			count = 0;

			SqlParameter[] parms;

			try
			{
				SqlParameter parmCount = BuildParameter("@parmUnassignedCount", SqlDbType.Int, DBNull.Value, ParameterDirection.Output);
				parms = new SqlParameter[]
				{
					BuildParameter("@parmFIEntityID", SqlDbType.Int, fiEntityID, ParameterDirection.Input),
					parmCount
				};

				if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_UnassignedCount", parms))
				{
					if (parmCount.Value != DBNull.Value)
						count = (int)parmCount.Value;
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetUnassignedWorkgroups(int fiEntityID, out List<WorkgroupDto> workgroups)
		{
			workgroups = null;

			SqlParameter[] parms;
			DataTable dt;

			try
			{
				parms = new SqlParameter[] { BuildParameter("@parmFIEntityID", SqlDbType.Int, fiEntityID, ParameterDirection.Input) };

				if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_Unassigned", parms, out dt))
				{
					//Convert to DTO Type
					workgroups = new List<WorkgroupDto>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
                        workgroups.Add(row.ConvertToWorkgroupDto(DalWorkgroupAdditionalData.None));
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

        public bool GetWorkgroup(int clientAccountKey, DalWorkgroupAdditionalData additionalData,
            out WorkgroupDto workgroup)
		{
			workgroup = null;

			DataTable dt;

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmClientAccountKey", SqlDbType.Int, clientAccountKey, ParameterDirection.Input));

				if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_HubConfiguration", parms.ToArray(), out dt))
				{
					//Convert to DTO Type
					if (dt.Rows.Count > 0)
						workgroup = dt.Rows[0].ConvertToWorkgroupDto(additionalData);
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

        public bool GetWorkgroup(int siteClientAccountId, int siteBankId, DalWorkgroupAdditionalData additionalData,
            out WorkgroupDto workgroup)
        {
            workgroup = null;
            int workGroupId = 0;

            DataTable dt;

            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountId, ParameterDirection.Input));

                if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID",
                    parms.ToArray(), out dt))
                {
                    // Get the workgroup id
                    if (dt.Rows.Count > 0)
                        workGroupId = Convert.ToInt32(dt.Rows[0][0]);
                }

                return GetWorkgroup(workGroupId, additionalData, out workgroup);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

        public bool GetWorkgroupDDAs(int siteBankID, int siteClientAccountID, out List<DDADto> ddas)
		{
			ddas = null;

			DataTable dt;

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountID, ParameterDirection.Input));

				if (Database.executeProcedure("RecHubData.usp_ElectronicAccounts_Get_BySiteBankIDClientAccountID", parms.ToArray(), out dt))
				{
					//Convert to DTO Type
					ddas = new List<DDADto>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
						ddas.Add(row.ConvertToDDADto());
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetWorkgroupDataEntryColumns(int clientAccountKey, out List<DataEntryColumnDto> deColumns)
		{
			deColumns = null;

			DataTable dt;

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmClientAccountKey", SqlDbType.Int, clientAccountKey, ParameterDirection.Input));

				if (Database.executeProcedure("RecHubData.usp_dimDataEntryColumns_Get_ByClientAccountKey", parms.ToArray(), out dt))
				{
					//Convert to DTO Type
					deColumns = new List<DataEntryColumnDto>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
						deColumns.Add(row.ConvertToDataEntryColumnDto());
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetDataEntryTemplates(out List<DataEntryTemplateDto> deTemplates)
		{
			deTemplates = null;

			DataTable dt;

			try
			{
				if (Database.executeProcedure("RecHubData.usp_DataEntryTemplates_Get", out dt))
				{
					deTemplates = new List<DataEntryTemplateDto>();
					foreach (DataRow row in dt.Rows)
					{
						var template = row.ConvertToDataEntryTemplateDto();
						List<DataEntryColumnDto> columns;
						if (!GetDataEntryColumns(template.ShortName, out columns))
							return false;
						template.DEColumns = columns;

						deTemplates.Add(template);
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		private bool GetDataEntryColumns(string shortName, out List<DataEntryColumnDto> columns)
		{
			columns = null;

			DataTable dt;
			try
			{
				SqlParameter[] parms = new SqlParameter[] { BuildParameter("@parmTemplateShortName", SqlDbType.VarChar, shortName, ParameterDirection.Input) };
				if (Database.executeProcedure("RecHubData.usp_DataEntryTemplateColumns_Get", parms, out dt))
				{
					columns = new List<DataEntryColumnDto>();
					foreach (DataRow row in dt.Rows)
					{
						var deColumn = row.ConvertToDataEntryColumnDto();
						//unfortunately the template tables do not include TableType, so we'll set that manually
                        deColumn.TableType = deColumn.TableName == "Checks" ? ETableType.Payments : ETableType.Invoice;

						columns.Add(deColumn);
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

        public bool AddWorkgroup(int userId, string userDisplayName, IList<int> fiEntityIds,
            WorkgroupDto workgroup, out string errorMessage)
		{
			errorMessage = null;
			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, workgroup.SiteBankID, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, workgroup.SiteClientAccountID, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmIsActive", SqlDbType.SmallInt, workgroup.IsActive, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDataRetentionDays", SqlDbType.SmallInt, workgroup.DataRetentionDays, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmImageRetentionDays", SqlDbType.SmallInt, workgroup.ImageRetentionDays, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmShortName", SqlDbType.VarChar, workgroup.ShortName, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmLongName", SqlDbType.VarChar, workgroup.LongName, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmFileGroup", SqlDbType.VarChar, workgroup.FileGroup ?? (object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmEntityID", SqlDbType.Int, workgroup.EntityID??(object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmViewingDays", SqlDbType.Int, (object)workgroup.ViewingDays ?? DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmMaximumSearchDays", SqlDbType.Int, (object)workgroup.MaximumSearchDays ?? DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmCheckImageDisplayMode", SqlDbType.TinyInt, (byte)(int)workgroup.CheckImageDisplayMode, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDocumentImageDisplayMode", SqlDbType.TinyInt, (byte)(int)workgroup.DocumentImageDisplayMode, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmHOA", SqlDbType.Bit, workgroup.HOA, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDisplayBatchID", SqlDbType.Bit, workgroup.DisplayBatchID ?? (object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmInvoiceBalancingOption", SqlDbType.TinyInt, (byte)(int)workgroup.InvoiceBalancing, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmUserDisplayName", SqlDbType.VarChar, userDisplayName, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDDAs", SqlDbType.Xml, workgroup.DDAs.ToDBXml(), ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDataEntryColumns", SqlDbType.Xml, workgroup.DataEntryColumns == null ? (object)DBNull.Value : workgroup.DataEntryColumns.ToDBXml(), ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDeadLineDay", SqlDbType.SmallInt, workgroup.DataEntrySetup == null ? (object)DBNull.Value : workgroup.DataEntrySetup.DeadLineDay ?? (object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmRequiresInvoice", SqlDbType.Bit, workgroup.DataEntrySetup == null ? false : workgroup.DataEntrySetup.RequiresInvoice, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmBusinessRules", SqlDbType.Xml, workgroup.DataEntrySetup == null || workgroup.DataEntrySetup.BusinessRules == null ? (object)DBNull.Value : workgroup.DataEntrySetup.BusinessRules.ToDBXml(), ParameterDirection.Input));
				parms.Add(BuildParameter("@parmFIs", SqlDbType.Xml, GetFIsAsXml(fiEntityIds), ParameterDirection.Input));
				parms.Add(BuildParameter("@parmBillingAccount", SqlDbType.VarChar, 20, workgroup.BillingAccount ?? string.Empty));
				parms.Add(BuildParameter("@parmBillingField1", SqlDbType.VarChar, 20, workgroup.BillingField1 ?? string.Empty));
				parms.Add(BuildParameter("@parmBillingField2", SqlDbType.VarChar, 20, workgroup.BillingField2 ?? string.Empty));
                parms.Add(BuildParameter("@parmBusinessRulesInvoiceRequired", SqlDbType.Bit,
                    workgroup.BusinessRules?.InvoiceRequired ?? (object) DBNull.Value, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmBusinessRulesBalancingRequired", SqlDbType.Bit,
                    workgroup.BusinessRules?.PreDepositBalancingRequired ?? (object)DBNull.Value, ParameterDirection.Input));
			    parms.Add(BuildParameter("@parmBusinessRulesPDBalancingRequired", SqlDbType.Bit,
			        workgroup.BusinessRules?.PostDepositBalancingRequired ?? (object)DBNull.Value, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmBusinessRulesPDPayerRequired", SqlDbType.Bit,
                    workgroup.BusinessRules?.PostDepositPayerRequired ?? (object)DBNull.Value, ParameterDirection.Input));

                SqlParameter parmClientAccountKey = BuildParameter("@parmClientAccountKey", SqlDbType.Int, DBNull.Value, ParameterDirection.Output);
				SqlParameter parmErrorCode = BuildParameter("@parmErrorCode", SqlDbType.Int, DBNull.Value, ParameterDirection.Output);
                parms.Add(BuildParameter("@parmEntityName", SqlDbType.VarChar, 50, workgroup.EntityName ?? string.Empty));
				parms.Add(parmClientAccountKey);
				parms.Add(parmErrorCode);

				if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Ins", parms.ToArray()))
				{
					if (parmErrorCode.Value != DBNull.Value)
					{
						switch ((int)parmErrorCode.Value)
						{
							case 1: // Duplicate ID
								errorMessage = "Unable to add workgroup.  Workgroup ID " + workgroup.SiteClientAccountID.ToString() + " already exists.";
								break;
							default:
								// Don't know what message to return...
								break;
						}
						return false;
					}
					else if (parmClientAccountKey.Value != DBNull.Value)
					{
						workgroup.ClientAccountKey = (int)parmClientAccountKey.Value;
					}

					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

        public bool UpdateWorkgroup(int userId, string userDisplayName, IList<int> fiEntityIds,
            WorkgroupDto workgroup, out bool raamNotificationRequired)
		{
			List<SqlParameter> parms;

			raamNotificationRequired = false;

			try
			{
				parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmClientAccountKey", SqlDbType.Int, workgroup.ClientAccountKey, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmIsActive", SqlDbType.SmallInt, workgroup.IsActive, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDataRetentionDays", SqlDbType.SmallInt, workgroup.DataRetentionDays, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmImageRetentionDays", SqlDbType.SmallInt, workgroup.ImageRetentionDays, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmShortName", SqlDbType.VarChar, workgroup.ShortName, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmLongName", SqlDbType.VarChar, workgroup.LongName ?? (object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmFileGroup", SqlDbType.VarChar, workgroup.FileGroup ?? (object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmEntityID", SqlDbType.Int, workgroup.EntityID??(object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmViewingDays", SqlDbType.Int, (object)workgroup.ViewingDays ?? DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmMaximumSearchDays", SqlDbType.Int, (object)workgroup.MaximumSearchDays ?? DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmCheckImageDisplayMode", SqlDbType.TinyInt, (byte)(int)workgroup.CheckImageDisplayMode, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDocumentImageDisplayMode", SqlDbType.TinyInt, (byte)(int)workgroup.DocumentImageDisplayMode, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmHOA", SqlDbType.Bit, workgroup.HOA, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDisplayBatchID", SqlDbType.Bit, workgroup.DisplayBatchID ?? (object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmInvoiceBalancingOption", SqlDbType.TinyInt, (byte)(int)workgroup.InvoiceBalancing, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmUserDisplayName", SqlDbType.VarChar, userDisplayName, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDDAs", SqlDbType.Xml, workgroup.DDAs.ToDBXml(), ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDataEntryColumns", SqlDbType.Xml, workgroup.DataEntryColumns == null ? (object)DBNull.Value :  workgroup.DataEntryColumns.ToDBXml(), ParameterDirection.Input));
				parms.Add(BuildParameter("@parmDeadLineDay", SqlDbType.SmallInt, workgroup.DataEntrySetup == null ? (object)DBNull.Value : workgroup.DataEntrySetup.DeadLineDay ?? (object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmRequiresInvoice", SqlDbType.Bit, workgroup.DataEntrySetup == null ? false : workgroup.DataEntrySetup.RequiresInvoice, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmBusinessRules", SqlDbType.Xml, workgroup.DataEntrySetup == null || workgroup.DataEntrySetup.BusinessRules == null ? (object)DBNull.Value : workgroup.DataEntrySetup.BusinessRules.ToDBXml(), ParameterDirection.Input));
				parms.Add(BuildParameter("@parmFIs", SqlDbType.Xml, GetFIsAsXml(fiEntityIds), ParameterDirection.Input));
				parms.Add(BuildParameter("@parmBillingAccount", SqlDbType.VarChar, 20, workgroup.BillingAccount ?? string.Empty));
				parms.Add(BuildParameter("@parmBillingField1", SqlDbType.VarChar, 20, workgroup.BillingField1 ?? string.Empty));
				parms.Add(BuildParameter("@parmBillingField2", SqlDbType.VarChar, 20, workgroup.BillingField2 ?? string.Empty));
                parms.Add(BuildParameter("@parmBusinessRulesInvoiceRequired", SqlDbType.Bit,
                    workgroup.BusinessRules?.InvoiceRequired ?? (object) DBNull.Value, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmBusinessRulesBalancingRequired", SqlDbType.Bit, 
                    workgroup.BusinessRules?.PreDepositBalancingRequired ?? (object)DBNull.Value, ParameterDirection.Input));
			    parms.Add(BuildParameter("@parmBusinessRulesPDBalancingRequired", SqlDbType.Bit,
			        workgroup.BusinessRules?.PostDepositBalancingRequired ?? (object)DBNull.Value, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmBusinessRulesPDPayerRequired", SqlDbType.Bit,
                    workgroup.BusinessRules?.PostDepositPayerRequired ?? (object)DBNull.Value, ParameterDirection.Input));

                SqlParameter parmNewClientAccountKey = BuildParameter("@parmNewClientAccountKey", SqlDbType.Int, DBNull.Value, ParameterDirection.Output);
				SqlParameter parmRAAMNotificationRequired = BuildParameter("@parmRAAMNotificationRequried", SqlDbType.Bit, DBNull.Value, ParameterDirection.Output);
                parms.Add(BuildParameter("@parmEntityName", SqlDbType.VarChar, 50, workgroup.EntityName ?? string.Empty));
				parms.Add(parmNewClientAccountKey);
				parms.Add(parmRAAMNotificationRequired);

				if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Upd", parms.ToArray()))
				{
					//Based on the changes made, the previous record may have become historical and as such
					// we have a new most recent record with a new client account key, return that new key
					if (parmNewClientAccountKey.Value != DBNull.Value)
						workgroup.ClientAccountKey = (int)parmNewClientAccountKey.Value;
					if (parmRAAMNotificationRequired.Value != DBNull.Value)
						raamNotificationRequired = (bool)parmRAAMNotificationRequired.Value;
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool UpdateWorkgroupsEntity(int userId, WorkgroupDto workgroup)
		{
			List<SqlParameter> parms;

			try
			{
				parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, workgroup.SiteBankID, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, workgroup.SiteClientAccountID, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmEntityID", SqlDbType.Int, workgroup.EntityID??(object)DBNull.Value, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
				if (Database.executeProcedure("RecHubUser.usp_OLWorkgroups_Merge_EntityID", parms.ToArray()))
					return true;
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetElectronicAccountByDDA(DDADto dda, out ElectronicAccountDto electronicAccount)
		{
			electronicAccount = null;

			DataTable dt;

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmABA", SqlDbType.VarChar, 10, dda.RT));
				parms.Add(BuildParameter("@parmDDA", SqlDbType.VarChar, 35, dda.DDA));

				if (Database.executeProcedure("RecHubData.usp_ElectronicAccounts_Get_ByDDA", parms.ToArray(), out dt))
				{
					//Convert to DTO Type
					if (dt.Rows.Count > 0)
						electronicAccount = dt.Rows[0].ConvertToElectronicAccountDto();
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetDataEntrySetup(int siteBankID, int workgroupId, out DataEntrySetupDto dataEntrySetup)
		{
			dataEntrySetup = null;

			DataTable dt;

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				SqlParameter parmDeadLineDay = BuildParameter("@parmDeadLineDay", SqlDbType.SmallInt, DBNull.Value, ParameterDirection.Output);
				SqlParameter parmRequiresInvoice = BuildParameter("@parmRequiresInvoice", SqlDbType.Bit, DBNull.Value, ParameterDirection.Output);

				parms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmSiteWorkgroupID", SqlDbType.Int, workgroupId, ParameterDirection.Input));
				parms.Add(parmDeadLineDay);
				parms.Add(parmRequiresInvoice);

				if (Database.executeProcedure("RecHubException.usp_BusinessRules_Get_BySiteBankIDSiteWorkgroupID", parms.ToArray(), out dt))
				{
					dataEntrySetup = new DataEntrySetupDto();
					if (parmDeadLineDay.Value != DBNull.Value)
						dataEntrySetup.DeadLineDay = (short)parmDeadLineDay.Value;
					dataEntrySetup.RequiresInvoice = parmRequiresInvoice.Value == DBNull.Value ? false : (bool)parmRequiresInvoice.Value;

					if (dt != null)
					{
						//Convert to DTO Type
						dataEntrySetup.BusinessRules = new List<BusinessRulesDto>(dt.Rows.Count);
						foreach (DataRow row in dt.Rows)
						{
							BusinessRulesDto br = row.ConvertToBusinessRulesDto();
							if (br.CheckDigitRoutine != null)
							{
								//get the check digit replacement values
								Dictionary<char, int?> replacementValues;
								if (!GetCheckDigitReplacementValues(br.CheckDigitRoutine.Key, out replacementValues))
									return false;
								br.CheckDigitRoutine.ReplacementValues = replacementValues;
							}
							if (br.FieldValidations != null)
							{
								//get the field values
								List<string> values;
								if (!GetFieldValidationValues(br.FieldValidations.Key, out values))
									return false;
								br.FieldValidations.FieldValues = values;
							}

							dataEntrySetup.BusinessRules.Add(br);
						}
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetCheckDigitReplacementValues(long cdKey, out Dictionary<char, int?> replacementValues)
		{
			replacementValues = null;

			DataTable dt;	

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmCheckDigitRoutineKey", SqlDbType.BigInt, cdKey, ParameterDirection.Input));

				if (Database.executeProcedure("RecHubException.usp_CheckDigitReplacementValues_Get", parms.ToArray(), out dt))
				{
					replacementValues = new Dictionary<char, int?>();

					//Convert to DTO Type
					foreach (DataRow row in dt.Rows)
					{
						KeyValuePair<char, int?> pair = row.ConvertToCDReplacementValue();
						replacementValues.Add(pair.Key, pair.Value);
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetFieldValidationValues(long fvKey, out List<string> values)
		{
			values = null;

			DataTable dt;

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmFieldValidationKey", SqlDbType.BigInt, fvKey, ParameterDirection.Input));

				if (Database.executeProcedure("RecHubException.usp_FieldValidationValues_Get", parms.ToArray(), out dt))
				{
					values = new List<string>();

					//Convert to DTO Type
					foreach (DataRow row in dt.Rows)
					{
						values.Add(row.ConvertToFieldValue());
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

        public bool GetPaymentSources(out List<PaymentSourceDto> paymentSources)
        {
            paymentSources = null;

            DataTable dt;

            List<SqlParameter> parms = new List<SqlParameter>();
            parms.Add(BuildParameter("@parmActiveOnly", SqlDbType.Bit, false, ParameterDirection.Input));

            try
            {
                if (Database.executeProcedure("RecHubData.usp_dimBatchSources_Get", parms.ToArray(), out dt))
                {
                    //Convert to DTO Type
                    paymentSources = new List<PaymentSourceDto>(dt.Rows.Count);
                    foreach (DataRow row in dt.Rows)
                    {
                        paymentSources.Add(row.ConvertToPaymentSourceDto());
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return false;
        }

        public bool GetPaymentSource(int batchSourceKey, out PaymentSourceDto paymentSource)
        {
            paymentSource = null;

            DataTable dt;

            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batchSourceKey, ParameterDirection.Input));

                if (Database.executeProcedure("RecHubData.usp_dimBatchSources_Get_ByBatchSourceKey", parms.ToArray(), out dt))
                {
                    //Convert to DTO Type
                    if (dt.Rows.Count > 0)
                        paymentSource = dt.Rows[0].ConvertToPaymentSourceDto();
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

		public bool AddPaymentSource(int userId, ref PaymentSourceDto paymentSource, out string errorMessage)
        {
			errorMessage = null;
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmIsActive", SqlDbType.Bit, paymentSource.IsActive, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmShortName", SqlDbType.VarChar, paymentSource.ShortName, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmLongName", SqlDbType.VarChar, paymentSource.LongName, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmEntityID", SqlDbType.Int, paymentSource.FIEntityID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmSystemType", SqlDbType.VarChar, paymentSource.SystemType, ParameterDirection.Input));

                SqlParameter parmBatchSourceKey = BuildParameter("@outBatchSourceKey", SqlDbType.SmallInt, DBNull.Value, ParameterDirection.Output);
				SqlParameter parmErrorCode = BuildParameter("@parmErrorCode", SqlDbType.Int, null, ParameterDirection.Output);
                parms.Add(parmBatchSourceKey);
				parms.Add(parmErrorCode);

                if (Database.executeProcedure("RecHubData.usp_dimBatchSources_Upsert", parms.ToArray()))
                {
					if (parmErrorCode.Value != DBNull.Value)
					{
						switch ((int)parmErrorCode.Value)
						{
							case 1: // Duplicate ID
								errorMessage = "Unable to add payment source.  Short name '" + paymentSource.ShortName + "' already exists.";
								break;
							default:
								// Don't know what message to return...
								break;
						}
						return false;
					}
					
					if (parmBatchSourceKey.Value != DBNull.Value)
						paymentSource.BatchSourceKey = Convert.ToInt32(parmBatchSourceKey.Value);
                
                    return true;
               }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

        public bool UpdatePaymentSource(int userId, ref PaymentSourceDto paymentSource)
        {
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, paymentSource.BatchSourceKey, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmIsActive", SqlDbType.Bit, paymentSource.IsActive, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmShortName", SqlDbType.VarChar, paymentSource.ShortName, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmLongName", SqlDbType.VarChar, paymentSource.LongName, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmEntityID", SqlDbType.Int, paymentSource.FIEntityID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmSystemType", SqlDbType.VarChar, paymentSource.SystemType, ParameterDirection.Input));

                if (Database.executeProcedure("RecHubData.usp_dimBatchSources_Upsert", parms.ToArray()))              
                    return true;
            }
            catch (Exception ex)
            {
				LogError(ex);
            }
            return false;
        }

        public bool DeletePaymentSource(int userId, ref PaymentSourceDto paymentSource, out string errorMessage)
        {
            errorMessage = String.Empty;

            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, paymentSource.BatchSourceKey, ParameterDirection.Input));

                SqlParameter parmErrorMessage = BuildParameter("@parmErrorMessage", SqlDbType.VarChar, DBNull.Value, ParameterDirection.Output);
                parms.Add(parmErrorMessage);

                if (Database.executeProcedure("RecHubData.usp_dimBatchSources_Del_ByBatchSourceKey", parms.ToArray()))
                    return true;
                else
                {
                    if (parmErrorMessage.Value != DBNull.Value)
                        errorMessage = parmErrorMessage.Value.ToString();

                    return false;
                }
            }
            catch (Exception ex)
            {
				LogError(ex);
            }

            return false;
        }

		public bool GetBanksForFI(int fiEntityID, out List<BankDto> banks)
		{
			banks = new List<BankDto>();
			try
			{
				SqlParameter[] parms = new SqlParameter[] {
					BuildParameter("@parmFIEntityID", SqlDbType.Int, fiEntityID <= 0 ? (object)DBNull.Value : fiEntityID, ParameterDirection.Input),
				};

				DataTable dt;
				if (Database.executeProcedure("RecHubData.usp_dimBanks_Get_ByFI", parms, out dt))
				{
					//Convert to DTO Type
					foreach (DataRow row in dt.Rows)
					{
						banks.Add(row.ConvertToBank());
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}


		public bool GetBanks(int userId, IList<int> fiEntityIds, out List<BankDto> banks)
		{
			banks = new List<BankDto>();
			try
			{
				SqlParameter[] parms = new SqlParameter[] {
					BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input),
					BuildParameter("@parmFIs", SqlDbType.Xml, GetFIsAsXml(fiEntityIds), ParameterDirection.Input),
				};

				DataTable dt;
				if (Database.executeProcedure("RecHubData.usp_dimBanks_Get_ByUserFIs", parms, out dt))
				{
					//Convert to DTO Type
					foreach (DataRow row in dt.Rows)
					{
						banks.Add(row.ConvertToBank());
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetBank(int userId, IList<int> fiEntityIds, int bankKey, out BankDto bank)
		{
			bank = null;
			try
			{
				SqlParameter[] parms = new SqlParameter[] {
					BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input),
					BuildParameter("@parmFIs", SqlDbType.Xml, GetFIsAsXml(fiEntityIds), ParameterDirection.Input),
					BuildParameter("@parmBankKey", SqlDbType.Int, bankKey, ParameterDirection.Input),
				};

				DataTable dt;
				if (Database.executeProcedure("RecHubData.usp_dimBanks_Get_ByBankKeyFIs", parms, out dt))
				{
					if (dt.Rows.Count == 0)
						throw new Exception(string.Format("No rows returned for Bank = {0}, User = {1}", bankKey, userId));

					//Convert to DTO Type
					bank = dt.Rows[0].ConvertToBank();
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

	    public bool AuditConfigurationReport(int userId, AuditConfigDto dto)
	    {
	        try
	        {
	            
                SqlParameter[] parms = new SqlParameter[] {
                    BuildParameter("@parmApplicationName", SqlDbType.VarChar, dto.ApplicationName, ParameterDirection.Input),
                    BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input),
                    BuildParameter("@parmAuditType", SqlDbType.VarChar, dto.AuditType.ToString(), ParameterDirection.Input),
                    BuildParameter("@parmAuditMessage", SqlDbType.VarChar, dto.Message, ParameterDirection.Input),
                    BuildParameter("@parmAuditValue", SqlDbType.VarChar, dto.AuditValue, ParameterDirection.Input)
                };
                DataTable dt;
	            if (Database.executeProcedure("RecHubAudit.usp_Report_ConfigurationAudit_Ins", parms, out dt))
	            {
	                return true;
	            }
	        }
	        catch (Exception ex)
	        {
                LogError(ex);
            }
	        return false;
	    }

		public bool AddBank(int userId, ref BankDto bank, out string errorMessage)
		{
			errorMessage = null;

			try
			{
				var parmBankKey = BuildParameter("@parmBankKey", SqlDbType.Int, null, ParameterDirection.Output);
				var parmErrorCode = BuildParameter("@parmErrorCode", SqlDbType.Int, null, ParameterDirection.Output);

				SqlParameter[] parms = new SqlParameter[] {
					BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input),
					BuildParameter("@parmFIEntityID", SqlDbType.Int, bank.FIEntityID, ParameterDirection.Input),
					BuildParameter("@parmSiteBankID", SqlDbType.Int, bank.SiteBankID, ParameterDirection.Input),
					BuildParameter("@parmBankName", SqlDbType.VarChar, 128, bank.BankName),
					parmBankKey,
					parmErrorCode,
				};

				if (Database.executeProcedure("RecHubData.usp_dimBanks_Ins", parms))
				{
					if (parmErrorCode.Value != DBNull.Value)
					{
						switch ((int)parmErrorCode.Value)
						{
							case 1: // Duplicate ID
								errorMessage = "Cannot Add ID " + bank.SiteBankID.ToString();
								break;
							default:
								// Don't know what message to return...
								break;
						}

						return false;
					}
					else
					{
						bank.BankKey = (int)parmBankKey.Value;
						return true;
					}
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool UpdateBank(int userId, BankDto bank)
		{
			try
			{
				var parmBankKey = BuildParameter("@parmBankKey", SqlDbType.Int, bank.BankKey, ParameterDirection.InputOutput);
				SqlParameter[] parms = new SqlParameter[] {
					BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input),
					BuildParameter("@parmFIEntityID", SqlDbType.Int, bank.FIEntityID, ParameterDirection.Input),	// Only updatable if currently NULL
					//BuildParameter("@parmSiteBankID", SqlDbType.Int, bank.SiteBankID, ParameterDirection.Input),	Not updatable
					BuildParameter("@parmBankName", SqlDbType.VarChar, 128, bank.BankName),
					parmBankKey,
				};

				if (Database.executeProcedure("RecHubData.usp_dimBanks_Upd", parms))
				{
					bank.BankKey = (int)parmBankKey.Value;
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetEntity(int userId, int entityId, out RecHubEntityDto entity)
		{
			entity = null;
			try
			{
				SqlParameter[] parms = new SqlParameter[] {
					BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input),
					BuildParameter("@parmEntityId", SqlDbType.Int, entityId, ParameterDirection.Input),
				};

				DataTable dt;
				if (Database.executeProcedure("RecHubUser.usp_OLEntities_Get_ByEntityID", parms, out dt))
				{
					if (dt.Rows.Count > 0)
					{
						//Convert to DTO Type
						entity = dt.Rows[0].ConvertToOLEntity();
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool AddUpdateEntity(int userId, RecHubEntityDto entity)
		{
			try
			{
				SqlParameter[] parms = new SqlParameter[] {
					BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input),
					BuildParameter("@parmEntityId", SqlDbType.Int, entity.EntityID, ParameterDirection.Input),
					BuildParameter("@parmDisplayBatchId", SqlDbType.Bit, entity.DisplayBatchID, ParameterDirection.Input),
					BuildParameter("@parmPaymentImageDisplayMode", SqlDbType.Int, (int)entity.PaymentImageDisplayMode, ParameterDirection.Input),
					BuildParameter("@parmDocumentImageDisplayMode", SqlDbType.Int, (int)entity.DocumentImageDisplayMode, ParameterDirection.Input),
					BuildParameter("@parmViewingDays", SqlDbType.Int, entity.ViewingDays, ParameterDirection.Input),
					BuildParameter("@parmMaximumSearchDays", SqlDbType.Int, entity.MaximumSearchDays, ParameterDirection.Input),
					BuildParameter("@parmBillingAccount", SqlDbType.VarChar, 20, entity.BillingAccount ?? string.Empty),
					BuildParameter("@parmBillingField1", SqlDbType.VarChar, 20, entity.BillingField1 ?? string.Empty),
					BuildParameter("@parmBillingField2", SqlDbType.VarChar, 20, entity.BillingField2 ?? string.Empty),
                    BuildParameter("@paramEntityName", SqlDbType.VarChar, 50, entity.EntityName ?? string.Empty),
                    

				};

				if (Database.executeProcedure("RecHubUser.usp_OLEntities_Upsert", parms))
				{
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetEventRules(Guid sessionID, out List<EventRuleDto> eventRules)
        {
            eventRules = null;

            DataTable dt;

            try
            {
				SqlParameter[] parms = new SqlParameter[] {
					BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input)
				};

                if (Database.executeProcedure("RecHubAlert.usp_EventRules_Get", parms, out dt))
                {
                    //Convert to DTO Type
                    eventRules = new List<EventRuleDto>(dt.Rows.Count);
                    foreach (DataRow row in dt.Rows)
                    {
                        eventRules.Add(row.ConvertToEventRuleDto());
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return false;
        }

		public bool GetEventRule(int eventRuleID, Guid sessionID, out EventRuleDto eventRule)
        {
            eventRule = null;

            DataTable dt;

            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();

				parms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmEventRuleID", SqlDbType.Int, eventRuleID, ParameterDirection.Input));

                if (Database.executeProcedure("RecHubAlert.usp_EventRules_Get_ByEventRuleID", parms.ToArray(), out dt))
                {
                    //Convert to DTO Type
                    eventRule = dt.Rows[0].ConvertToEventRuleDto();
                    
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return false;
        }

        public bool GetUsersForEventRule(int eventRuleID, ref EventRuleDto eventRule)
        {
            DataTable dt;

            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();
                parms.Add(BuildParameter("@parmEventRuleID", SqlDbType.Int, eventRuleID, ParameterDirection.Input));

                if (Database.executeProcedure("RecHubUser.usp_Users_Get_RA3MSID_ByEventRuleID", parms.ToArray(), out dt))
                {
                    eventRule.UserRA3MSIDs = new List<string>(dt.Rows.Count);
                    foreach (DataRow row in dt.Rows)
                    {
                        eventRule.UserRA3MSIDs.Add(row["RA3MSID"].ToString());
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

		public bool GetEvents(out List<EventDto> events)
        {
            events = null;

            DataTable dt;

            try
            {
                if (Database.executeProcedure("RecHubAlert.usp_Events_Get", out dt))
                {
                    //Convert to DTO Type
                    events = new List<EventDto>(dt.Rows.Count);
                    foreach (DataRow row in dt.Rows)
                    {
                        events.Add(row.ConvertToEventDto());
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return false;
        }

		public bool AddEventRule(int userID, Guid sessionID, ref EventRuleDto eventRule)
        {
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();

				parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmEventID", SqlDbType.Int, eventRule.EventID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, eventRule.SiteBankID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, eventRule.SiteClientAccountID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmIsActive", SqlDbType.Bit, eventRule.IsActive, ParameterDirection.Input));

                parms.Add(BuildParameter("@parmDescription", SqlDbType.VarChar, eventRule.Description == "" ? null : eventRule.Description, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmOperator", SqlDbType.VarChar, eventRule.Operator, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmValue", SqlDbType.VarChar, eventRule.EventValue, ParameterDirection.Input));

                parms.Add(BuildParameter("@parmUsers", SqlDbType.Xml, GetUsersXml(eventRule.UserIDs), ParameterDirection.Input));
                
                SqlParameter parmEventRuleID = BuildParameter("@parmEventRuleID", SqlDbType.Int, DBNull.Value, ParameterDirection.Output);
                parms.Add(parmEventRuleID);

                if (Database.executeProcedure("RecHubAlert.usp_EventRules_Ins", parms.ToArray()))
                {
                    if (parmEventRuleID.Value != DBNull.Value)
                        eventRule.EventRuleID = Convert.ToInt32(parmEventRuleID.Value);

                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

		public bool UpdateEventRule(int userID, Guid sessionID, ref EventRuleDto eventRule)
        {
            try
            {
                List<SqlParameter> parms = new List<SqlParameter>();

				parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmEventRuleID", SqlDbType.Int, eventRule.EventRuleID, ParameterDirection.Input)); 
                parms.Add(BuildParameter("@parmEventID", SqlDbType.Int, eventRule.EventID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, eventRule.SiteBankID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, eventRule.SiteClientAccountID, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmIsActive", SqlDbType.Bit, eventRule.IsActive, ParameterDirection.Input));

                parms.Add(BuildParameter("@parmDescription", SqlDbType.VarChar, eventRule.Description == "" ? null : eventRule.Description, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmOperator", SqlDbType.VarChar, eventRule.Operator, ParameterDirection.Input));
                parms.Add(BuildParameter("@parmValue", SqlDbType.VarChar, eventRule.EventValue, ParameterDirection.Input));

                parms.Add(BuildParameter("@parmUsers", SqlDbType.Xml, GetUsersXml(eventRule.UserIDs), ParameterDirection.Input));

                if (Database.executeProcedure("RecHubAlert.usp_EventRules_Upd", parms.ToArray()))
                    return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

        public string GetViewingDaysPreference()
        {
            try
            {
                var parms = new[]
                {
                    BuildParameter("@parmPreferenceName", SqlDbType.VarChar, "ViewingDays", ParameterDirection.Input),
                };
                DataTable dt;
                if (Database.executeProcedure("RecHubUser.usp_OLPreferences_Get_Default_ByPreferenceName", parms, out dt) &&
                    dt != null && dt.Rows.Count >= 1)
                {
                    var row = dt.Rows[0];
                    return row["DefaultSetting"].ToString();
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return null;
        }

        public bool GetPreferences(out List<PreferenceDto> preferences)
        {
            preferences = null;

            DataTable dt;

            try
            {
                if (Database.executeProcedure("RecHubUser.usp_OLPreferences_Get_WithDescriptions", out dt))
                {
                    //Convert to DTO Type
                    preferences = new List<PreferenceDto>(dt.Rows.Count);
                    foreach (DataRow row in dt.Rows)
                    {
                        preferences.Add(row.ConvertToPreferenceDto());
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return false;
        }

        public bool UpdatePreferences(int userId, ref List<PreferenceDto> preferences)
        {
            try
            {
                BeginTrans();
                foreach (PreferenceDto preference in preferences)
                {
                    List<SqlParameter> parms = new List<SqlParameter>();
                    parms.Add(BuildParameter("@parmOLPreferenceID", SqlDbType.UniqueIdentifier, preference.ID, ParameterDirection.Input));
                    parms.Add(BuildParameter("@parmDefaultSetting", SqlDbType.VarChar, preference.DefaultSetting, ParameterDirection.Input));
                    parms.Add(BuildParameter("@parmIsOnLine", SqlDbType.Bit, preference.IsOnline, ParameterDirection.Input));
                    parms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));

                    SqlParameter parmRowsReturned = BuildParameter("@parmRowsReturned", SqlDbType.Int, DBNull.Value, ParameterDirection.Output);
                    parms.Add(parmRowsReturned);

                    if (!Database.executeProcedure("RecHubUser.usp_OLPreferences_Upd", parms.ToArray()))
                    {
                        RollbackTrans();
                        return false;
                    }
                }

                CommitTrans();
                return true;
            }
            catch (Exception ex)
            {
                RollbackTrans();
                LogError(ex);
            }
            return false;
        }


		public bool GetSystemMaxRetentionDays(out int maxRetentionDays)
        {
            maxRetentionDays = 0;

            DataTable dt;

            try
            {
				SqlParameter[] parms = new SqlParameter[] 
				{
					BuildParameter("@parmSection", SqlDbType.VarChar, "Rec Hub Table Maintenance", ParameterDirection.Input),
					BuildParameter("@parmSetupKey", SqlDbType.VarChar, "MaxRetentionDays", ParameterDirection.Input)
				};

				if (Database.executeProcedure("RecHubConfig.usp_SystemSetup_Get_BySetupKey", parms, out dt))
                {
					if (dt.Rows.Count > 0)
						maxRetentionDays = Convert.ToInt32(dt.Rows[0]["Value"]);
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return false;
        }

        public bool GetDocumentTypes(out List<DocumentTypeDto> doctypes)
        {
            DataTable dt;
            try
            {
                SqlParameter[] parms = new SqlParameter[] 
				{
					BuildParameter("@parmMostRecent", SqlDbType.Bit, 1, ParameterDirection.Input),
				};
                if (Database.executeProcedure("RecHubData.usp_dimDocumentTypes_Get", parms, out dt))
                {
                    doctypes = new List<DocumentTypeDto>();
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                            doctypes.Add(new DocumentTypeDto()
                                {
                                    Description = row["Description"].ToString(),
                                    FileDescriptor = row["FileDescriptor"].ToString(),
                                    DocumentTypeKey = int.Parse(row["DocumentTypeKey"].ToString()),
                                    IsReadOnly = bool.Parse(row["IsReadOnly"].ToString())
                                });
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            doctypes = null;
            return false;
        }

        public bool UpdateDocumentType(DocumentTypeDto type, int userId)
        {
            DataTable dt;
            try
            {
                SqlParameter[] parms = new SqlParameter[] 
				{
					BuildParameter("@parmFileDescriptor", SqlDbType.VarChar, type.FileDescriptor, ParameterDirection.Input),
					BuildParameter("@parmDescription", SqlDbType.VarChar, type.Description, ParameterDirection.Input),
                    BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input)
				};
                if (Database.executeProcedure("RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription", parms, out dt))
                    return true;
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

        public bool GetNotificationTypes(out List<NotificationTypeDto> doctypes)
        {
            DataTable dt;
            try
            {
                if (Database.executeProcedure("RecHubData.usp_dimNotificationFileTypes_Get", out dt))
                {
                    doctypes = new List<NotificationTypeDto>();
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                            doctypes.Add(new NotificationTypeDto()
                            {
                                FileTypeDescription = row["FileTypeDescription"].ToString(),
                                FileType = row["FileType"].ToString(),
                                NotificationFileTypeKey = int.Parse(row["NotificationFileTypeKey"].ToString())
                            });
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            doctypes = null;
            return false;
        }

        public bool InsertNotificationType(NotificationTypeDto item, int userId)
        {
            DataTable dt;
            try
            {
                SqlParameter[] parms = new SqlParameter[] 
				{
					BuildParameter("@parmFileType", SqlDbType.VarChar, item.FileType, ParameterDirection.Input),
					BuildParameter("@parmFileTypeDesc", SqlDbType.VarChar, item.FileTypeDescription, ParameterDirection.Input),
                    BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input)
				};
                if (Database.executeProcedure("RecHubData.usp_dimNotificationFileTypes_Ins", parms, out dt))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

        public bool UpdateNotificationType(NotificationTypeDto item, int userId)
        {
            DataTable dt;
            try
            {
                SqlParameter[] parms = new SqlParameter[] 
				{
					BuildParameter("@parmFileType", SqlDbType.VarChar, item.FileType, ParameterDirection.Input),
					BuildParameter("@parmFileTypeDesc", SqlDbType.VarChar, item.FileTypeDescription, ParameterDirection.Input),
                    BuildParameter("@parmUserId", SqlDbType.Int, userId, ParameterDirection.Input)
				};
                if (Database.executeProcedure("RecHubData.usp_dimNotificationFileTypes_Upd", parms, out dt))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

		public bool GetFieldValidationSettings(int siteBankID, int workgroupId, Guid sessionID, out List<BusinessRulesDto> businessRules)
		{
			businessRules = null;

			DataTable dt;

			try
			{
				SqlParameter[] parms = new SqlParameter[]
				{
					BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input),
					BuildParameter("@parmSiteWorkgroupID", SqlDbType.Int, workgroupId, ParameterDirection.Input),
					BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input)
				};

				if (Database.executeProcedure("RecHubException.usp_FieldValidations_Get_BySiteBankIDSiteWorkgroupID", parms, out dt))
				{
					if (dt != null)
					{
						//Convert to DTO Type
						businessRules = new List<BusinessRulesDto>();
						foreach (DataRow row in dt.Rows)
						{
							businessRules.Add(row.ConvertToBusinessRulesDto());
						}
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}


		#region Static Data Loads

		public bool GetValidationTableTypes(out List<Tuple<int, string>> tableTypes)
		{
			tableTypes = null;

			DataTable dt;

			try
			{
				if (Database.executeProcedure("RecHubException.usp_FieldValidationTypes_Get", out dt))
				{
					//Convert to DTO Type
					tableTypes = new List<Tuple<int, string>>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
						tableTypes.Add(row.ConvertIdDescRowToTuple("FieldValidationTypeKey", "TypeDescription"));
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetBatchSources(int fiEntityID, out List<Tuple<int, string>> batchSources)
		{
			batchSources = null;

			DataTable dt;

			try
			{
				SqlParameter[] parms = new SqlParameter[]
				{
					BuildParameter("@parmEntityID", SqlDbType.Int, fiEntityID, ParameterDirection.Input),
				};

				if (Database.executeProcedure("RecHubData.usp_dimBatchSources_Get_ByEntityID", parms, out dt))
				{
					//Convert to DTO Type
					batchSources = new List<Tuple<int, string>>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
						batchSources.Add(row.ConvertIdDescRowToTuple("BatchSourceKey", "LongName"));
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}


		public bool GetBatchPaymentTypes(out List<Tuple<int, string>> paymentTypes)
		{
			paymentTypes = null;

			DataTable dt;

			try
			{
				if (Database.executeProcedure("RecHubData.usp_dimBatchPaymentTypes_Get", out dt))
				{
					//Convert to DTO Type
					paymentTypes = new List<Tuple<int, string>>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
						paymentTypes.Add(row.ConvertIdDescRowToTuple("BatchPaymentTypeKey", "LongName"));
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

		public bool GetBatchPaymentSubTypes(out List<BatchPaymentSubTypeDto> paymentSubTypes)
		{
			paymentSubTypes = null;

			DataTable dt;
			
			try
			{
				if (Database.executeProcedure("RecHubData.usp_dimBatchPaymentSubTypes_Get", out dt))
				{
					//Convert to DTO Type
					paymentSubTypes = new List<BatchPaymentSubTypeDto>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
						paymentSubTypes.Add(row.ConvertToBatchPaymentSubTypeDto());
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			return false;
		}

        public bool GetSystemTypes(out List<Tuple<int, string>> systemTypes)
        {
            systemTypes = null;

            DataTable dt;

            try
            {
               // if (Database.executeProcedure("", out dt))
               // {


                //***********************************************************
                //***********************************************************

                dt = new DataTable();
                dt.Columns.Add(new DataColumn("SystemTypeID", System.Type.GetType("System.Int32")));
                dt.Columns.Add(new DataColumn("SystemTypeName", System.Type.GetType("System.String")));

                DataRow integraPayRow = dt.NewRow();
                integraPayRow["SystemTypeID"] = 1;
                integraPayRow["SystemTypeName"] = "integraPAY";
                dt.Rows.Add(integraPayRow);

                DataRow imageRPSRow = dt.NewRow();
                imageRPSRow["SystemTypeID"] = 2;
                imageRPSRow["SystemTypeName"] = "integraPAY";
                dt.Rows.Add(imageRPSRow);

                DataRow ditRow = dt.NewRow();
                ditRow["SystemTypeID"] = 3;
                ditRow["SystemTypeName"] = "integraPAY";
                dt.Rows.Add(ditRow);

                //***********************************************************
                //***********************************************************





                    //Convert to DTO Type
                    systemTypes = new List<Tuple<int, string>>(dt.Rows.Count);
                    foreach (DataRow row in dt.Rows)
                    {
                        systemTypes.Add(row.ConvertIdDescRowToTuple("SystemTypeID", "SystemTypeName"));
                    }
                    return true;
              // }
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return false;
        }

		#endregion

		#region Helpers

		private string GetFIsAsXml(IList<int> fiEntityIds)
		{
			StringBuilder sb = new StringBuilder();
			XmlWriter xw = XmlWriter.Create(sb, new XmlWriterSettings() { OmitXmlDeclaration = true });
			xw.WriteStartElement("FIs");
			if (fiEntityIds != null)
				foreach (var id in fiEntityIds)
				{
					xw.WriteElementString("ID", id.ToString());
				}
			xw.WriteEndElement();
			xw.Flush();
			return sb.ToString();
		}

        private string GetUsersXml(List<int> userIDs)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriter xw = XmlWriter.Create(sb, new XmlWriterSettings() { OmitXmlDeclaration = true });
            xw.WriteStartElement("users");
            if (userIDs != null)
                foreach (var id in userIDs)
                {
                    xw.WriteStartElement("user");
                    xw.WriteAttributeString("ID", id.ToString());
                    xw.WriteEndElement();
                }
            xw.WriteEndElement();
            xw.Flush();
            return sb.ToString();
        }

        private string GetPreferencesXml(List<PreferenceDto> preferences)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriter xw = XmlWriter.Create(sb, new XmlWriterSettings() { OmitXmlDeclaration = true });
            xw.WriteStartElement("preferences");
            if (preferences != null)
                foreach (var preference in preferences)
                {
                    xw.WriteStartElement("preference");
                    xw.WriteAttributeString("ID", preference.ID.ToString());
                    xw.WriteAttributeString("DefaultSetting", preference.DefaultSetting);
                    xw.WriteAttributeString("IsOnline", Convert.ToByte(preference.IsOnline).ToString());
                    xw.WriteEndElement();
                }
            xw.WriteEndElement();
            xw.Flush();
            return sb.ToString();
        }

		private void LogError(Exception ex, [CallerMemberName] string procName = null)
		{
			EventLog.logError(ex, this.GetType().Name, procName);
		}

		#endregion
	}
}
