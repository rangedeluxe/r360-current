﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.HubConfig
{
	public static class HubConfigDALFactory
	{
		public static IHubConfigDAL Create(string siteKey)
		{
			// TODO: Better support for mocking
			if (string.Compare(ConfigurationManager.AppSettings["MockDAL"], "true", true) == 0)
				return new MockHubConfigDAL(siteKey);

			return new HubConfigDAL(siteKey);
		}
	}
}
