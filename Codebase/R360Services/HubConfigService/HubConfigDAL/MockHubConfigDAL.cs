﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.HubConfig.DTO;

namespace WFS.RecHub.HubConfig
{
	internal class MockHubConfigDAL : IHubConfigDAL
	{
		private static object _WGLock = new object();

		internal static List<WorkgroupDto> _workgroups = new List<WorkgroupDto>()
		{
			new WorkgroupDto() { ClientAccountKey = 1, SiteBankID = 1, SiteClientAccountID = 13, MostRecent = true, IsActive = 1, DataRetentionDays = 100, ImageRetentionDays = 50, ShortName = "Accounts Receivable", LongName = "Accounts Receivable", FileGroup = "What", BillingAccount = "", BillingField1 = "", BillingField2 = "" },
			new WorkgroupDto() { ClientAccountKey = 2, SiteBankID = 20, SiteClientAccountID = 6000, MostRecent = true, IsActive = 1, DataRetentionDays = 200, ImageRetentionDays = 100, ShortName = "Rent Payments", LongName = "Rent Payments", FileGroup = "What", BillingAccount = "", BillingField1 = "", BillingField2 = "" },
			new WorkgroupDto() { ClientAccountKey = 3, SiteBankID = 300, SiteClientAccountID = 500, MostRecent = true, IsActive = 1, DataRetentionDays = 300, ImageRetentionDays = 150, ShortName = "Auto and Home", LongName = "Auto and Home Premiums", FileGroup = "What", BillingAccount = "", BillingField1 = "", BillingField2 = "" },
			new WorkgroupDto() { ClientAccountKey = 4, SiteBankID = 300, SiteClientAccountID = 501, MostRecent = true, IsActive = 1, DataRetentionDays = 300, ImageRetentionDays = 150, ShortName = "Life", LongName = "Life Premiums", FileGroup = "What", BillingAccount = "", BillingField1 = "", BillingField2 = "" },
			new WorkgroupDto() { ClientAccountKey = 5, SiteBankID = 4000, SiteClientAccountID = 40, MostRecent = true, IsActive = 1, DataRetentionDays = 400, ImageRetentionDays = 200, ShortName = "Property Taxes", LongName = "Property Taxes Payments", FileGroup = "What", BillingAccount = "", BillingField1 = "", BillingField2 = "" },
			new WorkgroupDto() { ClientAccountKey = 6, SiteBankID = 4000, SiteClientAccountID = 41, MostRecent = true, IsActive = 1, DataRetentionDays = 400, ImageRetentionDays = 200, ShortName = "Pet Licenses", LongName = "Pet License Payments", FileGroup = "What", BillingAccount = "", BillingField1 = "", BillingField2 = "" },
			new WorkgroupDto() { ClientAccountKey = 7, SiteBankID = 4000, SiteClientAccountID = 42, MostRecent = true, IsActive = 1, DataRetentionDays = 400, ImageRetentionDays = 200, ShortName = "Water Payments", LongName = "Water Payments", FileGroup = "What", BillingAccount = "", BillingField1 = "", BillingField2 = "" }
		};

		public MockHubConfigDAL(string siteKey)
		{

		}

		public bool GetUserIDBySID(Guid SID, out int userID)
		{
			//TODO: Add additional mock options
			userID = 1;
			return true;
		}

		public bool GetWorkgroups(int entityID, out List<WorkgroupDto> workgroups)
		{
			workgroups = _workgroups;
			return true;
		}

		public bool GetUnassignedWorkgroups(int fiEntityID, out List<WorkgroupDto> workgroups)
		{
			throw new NotImplementedException();
		}

		public bool GetUnassignedWorkgroupCount(int fiEntityID, out int count)
		{
			throw new NotImplementedException();
		}

        public bool GetWorkgroup(int clientAccountKey, DalWorkgroupAdditionalData additionalData,
            out WorkgroupDto workgroup)
		{
			var workgroups = _workgroups;
			workgroup = workgroups.FirstOrDefault(o => o.ClientAccountKey == clientAccountKey);
			return true;
		}

        public bool GetWorkgroup(int siteClientAccountId, int siteBankId, DalWorkgroupAdditionalData additionalData,
            out WorkgroupDto workgroup)
        {
            throw new NotImplementedException();
        }

        public bool GetWorkgroupDDAs(int siteBankID, int siteClientAccountID, out List<DDADto> ddas)
		{
			throw new NotImplementedException();
		}

		public bool GetWorkgroupDataEntryColumns(int clientAccountKey, out List<DataEntryColumnDto> deColumns)
		{
			throw new NotImplementedException();
		}

        public bool AddWorkgroup(int userId, string userDisplayName, IList<int> fiEntityIds,
            WorkgroupDto workgroup, out string errorMessage)
		{
			errorMessage = null;
			lock (_WGLock)
			{
				var tmp = new List<WorkgroupDto>(_workgroups);
				int newId = tmp.Max(o => o.ClientAccountKey) + 1;
				workgroup.ClientAccountKey = newId;
				tmp.Add(workgroup);
				_workgroups = tmp;
			}

			return true;
		}

        public bool UpdateWorkgroup(int userId, string userDisplayName, IList<int> fiEntityIds,
            WorkgroupDto workgroup, out bool raamNotificationRequired)
		{
			raamNotificationRequired = false;
			lock (_WGLock)
			{
				var tmp = new List<WorkgroupDto>(_workgroups);
				int origIndex = tmp.FindIndex(o => o.ClientAccountKey == workgroup.ClientAccountKey);
				tmp.RemoveAt(origIndex);
				tmp.Insert(origIndex, workgroup);
				_workgroups = tmp;
			}
			return true;
		}

		public bool UpdateWorkgroupsEntity(int userId, WorkgroupDto workgroup)
		{
			throw new NotImplementedException();
		}

		public bool GetElectronicAccountByDDA(DDADto dda, out ElectronicAccountDto electronicAccount)
		{
			throw new NotImplementedException();
		}

		public bool GetDataEntrySetup(int siteBankID, int workgroupID, out DataEntrySetupDto dataEntrySetup)
		{
			throw new NotImplementedException();
		}

		public bool GetValidationTableTypes(out List<Tuple<int, string>> types)
		{
			types = new List<Tuple<int, string>>()
			{
				new Tuple<int, string>(1, "Stop"),
				new Tuple<int, string>(2, "Accept")
			};

			return true;
		}

		public bool GetBatchSources(int fiEntityID, out List<Tuple<int, string>> sources)
		{
			throw new NotImplementedException();
		}

        public bool GetPaymentSources(out List<PaymentSourceDto> paymentSources)
        {
            throw new NotImplementedException();
        }

        public bool GetPaymentSource(int batchSourceKey, out PaymentSourceDto paymentSource)
        {
            throw new NotImplementedException();
        }

		public bool AddPaymentSource(int userId, ref PaymentSourceDto paymentSource, out string errorMessage)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePaymentSource(int userId, ref PaymentSourceDto paymentSource)
        {
            throw new NotImplementedException();
        }

        public bool DeletePaymentSource(int userId, ref PaymentSourceDto paymentSource, out string errorMessage)
        {
            throw new NotImplementedException();
        }

		public bool GetBatchPaymentTypes(out List<Tuple<int, string>> paymentTypes)
		{
			throw new NotImplementedException();
		}

		public bool GetBatchPaymentSubTypes(out List<BatchPaymentSubTypeDto> paymentSubTypes)
		{
			throw new NotImplementedException();
		}

        public bool GetSystemTypes(out List<Tuple<int, string>> systemTypes)
        {
            throw new NotImplementedException();
        }

		public bool GetBanksForFI(int fiEntityID, out List<BankDto> banks)
		{
			throw new NotImplementedException();
		}

		public bool GetBanks(int userId, IList<int> fiEntityIds, out List<BankDto> banks)
		{
			banks = new BankDto[] 
				{ 
					new BankDto() { BankKey = 442, SiteBankID = 42, BankName = "Fake Bank 42", FIEntityID = 0, MostRecent = true }, 
					new BankDto() { BankKey = 443, SiteBankID = 43, BankName = "Fake Bank 43", FIEntityID = 1, MostRecent = true },
					new BankDto() { BankKey = 444, SiteBankID = 44, BankName = "Fake Bank 44", FIEntityID = 1, MostRecent = true },
					new BankDto() { BankKey = 445, SiteBankID = 45, BankName = "Fake Bank 45", FIEntityID = 1, MostRecent = true }, 
					new BankDto() { BankKey = 446, SiteBankID = 46, BankName = "Fake Bank 46", FIEntityID = 2, MostRecent = true },
					new BankDto() { BankKey = 447, SiteBankID = 47, BankName = "Fake Bank 47", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 448, SiteBankID = 48, BankName = "Fake Bank 48", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 449, SiteBankID = 49, BankName = "Fake Bank 49", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 450, SiteBankID = 50, BankName = "Fake Bank 50", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 451, SiteBankID = 51, BankName = "Fake Bank 51", FIEntityID = 2, MostRecent = true }, 
					new BankDto() { BankKey = 452, SiteBankID = 52, BankName = "Fake Bank 52", FIEntityID = 3, MostRecent = true }, 
					new BankDto() { BankKey = 453, SiteBankID = 53, BankName = "Fake Bank 53", FIEntityID = 3, MostRecent = true }, 
					new BankDto() { BankKey = 454, SiteBankID = 54, BankName = "Fake Bank 54", FIEntityID = 3, MostRecent = true }, 
				}.Where(o => o.FIEntityID <= 0 || fiEntityIds.Contains(o.FIEntityID) ).ToList();

			return true;
		}

		public bool GetBank(int userId, IList<int> fiEntityIds, int bankKey, out BankDto bank)
		{
			List<BankDto> banks;
			GetBanks(userId, fiEntityIds, out banks);
			bank = banks.Find(o => o.BankKey == bankKey);
			return true;
		}

		public bool AddBank(int userId, ref BankDto bank, out string errorMessage)
		{
			throw new NotImplementedException();
		}

		public bool UpdateBank(int userId, BankDto bank)
		{
			throw new NotImplementedException();
		}

		public bool GetEntity(int userId, int entityId, out RecHubEntityDto entity)
		{
			Random r = new Random(entityId);
			entity = new RecHubEntityDto()
			{
				DisplayBatchID = (r.Next(2)) == 0,
				DocumentImageDisplayMode = (ImageDisplayModes)(r.Next(3)),
				EntityID = entityId,
				MaximumSearchDays = r.Next(100),
				ViewingDays = r.Next(100),
				PaymentImageDisplayMode = (ImageDisplayModes)(r.Next(3)),
				BillingAccount = string.Empty,
				BillingField1 = string.Empty,
				BillingField2 = string.Empty,
			};
			return true;
		}

		public bool AddUpdateEntity(int userId, RecHubEntityDto entity)
		{
			throw new NotImplementedException();
		}

        public bool GetEventRules(Guid sessionID, out List<EventRuleDto> eventRules)
        {
            throw new NotImplementedException();
        }

		public bool GetEventRule(int eventRuleID, Guid sessionID, out EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

        public bool GetEvents(out List<EventDto> events)
        {
            throw new NotImplementedException();
        }

		public bool AddEventRule(int userID, Guid sessionID, ref EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

		public bool UpdateEventRule(int userID, Guid sessionID, ref EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

        public bool GetUsersForEventRule(int eventRuleID, ref EventRuleDto eventRule)
        {
            throw new NotImplementedException();
        }

        public string GetViewingDaysPreference()
        {
            throw new NotImplementedException();
        }

        public bool GetPreferences(out List<PreferenceDto> preferences)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePreferences(int userId, ref List<PreferenceDto> preferences)
        {
            throw new NotImplementedException();
        }

		public bool GetDataEntryTemplates(out List<DataEntryTemplateDto> templates)
		{
			throw new NotImplementedException();
		}

		public bool GetSystemMaxRetentionDays(out int maxRetentionDays)
		{
			throw new NotImplementedException();
		}

        public bool GetDocumentTypes(out List<DocumentTypeDto> doctypes)
        {
            throw new NotImplementedException();
        }

        public bool UpdateDocumentType(DocumentTypeDto type, int userId)
        {
            throw new NotImplementedException();
        }

        public bool GetNotificationTypes(out List<NotificationTypeDto> doctypes)
        {
            throw new NotImplementedException();
        }

        public bool InsertNotificationType(NotificationTypeDto item, int userId)
        {
            throw new NotImplementedException();
        }

        public bool UpdateNotificationType(NotificationTypeDto item, int userId)
        {
            throw new NotImplementedException();
        }

		public bool GetFieldValidationSettings(int siteBankID, int workgroupId, Guid sessionID, out List<BusinessRulesDto> businessRules)
		{
			throw new NotImplementedException();
		}

	    public bool AuditConfigurationReport(int userId, AuditConfigDto dto)
	    {
	        throw new NotImplementedException();
	    }
	}
}
