﻿using System;
using System.Collections.Generic;
using WFS.RecHub.HubConfig.DTO;

namespace WFS.RecHub.HubConfig
{
	public interface IHubConfigDAL
	{
		bool GetUserIDBySID(Guid SID, out int userID);

		bool GetWorkgroups(int entityID, out List<WorkgroupDto> workgroups);
		bool GetUnassignedWorkgroups(int fiEntityID, out List<WorkgroupDto> workgroups);
		bool GetUnassignedWorkgroupCount(int fiEntityID, out int count);
        bool GetWorkgroup(int clientAccountKey, DalWorkgroupAdditionalData additionalData,
            out WorkgroupDto workgroup);
        bool GetWorkgroup(int siteClientAccountId, int siteBankId, DalWorkgroupAdditionalData additionalData,
            out WorkgroupDto workgroup);
        bool AddWorkgroup(int userId, string userDisplayName, IList<int> fiEntityIds,
            WorkgroupDto workgroup, out string errorMessage);
        bool UpdateWorkgroup(int userId, string userDisplayName, IList<int> fiEntityIds,
            WorkgroupDto workgroup, out bool raamNotificationRequired);
		bool UpdateWorkgroupsEntity(int userId, WorkgroupDto workgroup);

		bool GetWorkgroupDDAs(int siteBankID, int siteClientAccountID, out List<DDADto> ddas);
		bool GetElectronicAccountByDDA(DDADto dda, out ElectronicAccountDto electronicAccount);

		bool GetWorkgroupDataEntryColumns(int clientAccountKey, out List<DataEntryColumnDto> deColumns);
		bool GetDataEntrySetup(int siteBankId, int workGroupId, out DataEntrySetupDto dataEntrySetup);

        bool GetPaymentSources(out List<PaymentSourceDto> paymentSources);
        bool GetPaymentSource(int batchSourceKey, out PaymentSourceDto paymentSource);
        bool AddPaymentSource(int userId, ref PaymentSourceDto paymentSource, out string errorMessage);
        bool UpdatePaymentSource(int userId, ref PaymentSourceDto paymentSource);
        bool DeletePaymentSource(int userId, ref PaymentSourceDto paymentSource, out string errorMessage);

		bool GetBanksForFI(int fiEntityID, out List<BankDto> banks);
		bool GetBanks(int userId, IList<int> fiEntityIds, out List<BankDto> banks);
		bool GetBank(int userId, IList<int> fiEntityIds, int bankKey, out BankDto bank);
		bool AddBank(int userId, ref BankDto bank, out string errorMessage);
		bool UpdateBank(int userId, BankDto bank);

		bool GetEntity(int userId, int entityId, out RecHubEntityDto entity);
		bool AddUpdateEntity(int userId, RecHubEntityDto entity);

		bool GetEventRules(Guid sessionID, out List<EventRuleDto> eventRules);
		bool GetEventRule(int eventRuleID, Guid sessionID, out EventRuleDto eventRule);
        bool GetEvents(out List<EventDto> events);
        bool AddEventRule(int userID, Guid sessionID, ref EventRuleDto eventRule);
		bool UpdateEventRule(int userID, Guid sessionID, ref EventRuleDto eventRule);
        bool GetUsersForEventRule(int eventRuleID, ref EventRuleDto eventRule);

        string GetViewingDaysPreference();
        bool GetPreferences(out List<PreferenceDto> preferences);
        bool UpdatePreferences(int userId, ref List<PreferenceDto> preferences);

		bool GetSystemMaxRetentionDays(out int maxRetentionDays);

		bool GetDataEntryTemplates(out List<DataEntryTemplateDto> templates);

		#region Static Data Loads

		bool GetValidationTableTypes(out List<Tuple<int, string>> types);
		bool GetBatchSources(int fiEntityID, out List<Tuple<int, string>> sources);
		bool GetBatchPaymentTypes(out List<Tuple<int, string>> paymentTypes);
		bool GetBatchPaymentSubTypes(out List<BatchPaymentSubTypeDto> paymentSubTypes);
        bool GetSystemTypes(out List<Tuple<int, string>> systemTypes);

		#endregion

        bool GetDocumentTypes(out List<DocumentTypeDto> doctypes);
        bool UpdateDocumentType(DocumentTypeDto type, int userId);

        bool GetNotificationTypes(out List<NotificationTypeDto> doctypes);
        bool InsertNotificationType(NotificationTypeDto item, int userId);
        bool UpdateNotificationType(NotificationTypeDto item, int userId);

		bool GetFieldValidationSettings(int siteBankID, int workgroupId, Guid sessionID, out List<BusinessRulesDto> businessRules);
	    bool AuditConfigurationReport(int userId, AuditConfigDto dto);
	}
}
