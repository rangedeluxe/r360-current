﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml;
using WFS.RecHub.Common;

namespace WFS.RecHub.HubConfig
{
    public static class HubConfigDALExtensions
	{
		#region DataRow => DTO Conversion

		public static WorkgroupDto ConvertToWorkgroupDto(this DataRow dr, DalWorkgroupAdditionalData additionalData)
		{
			WorkgroupDto workgroup = null;

			if (dr.Table.Columns.Count > 0)
			{
				workgroup = new WorkgroupDto();
                if (additionalData.HasFlag(DalWorkgroupAdditionalData.BusinessRules))
                    workgroup.BusinessRules = new WorkgroupBusinessRulesDto();

                //workgroup.BusinessRules = new WorkgroupBusinessRulesDto();
                //workgroup.BusinessRules.PreDepositBalancingRequired = true;

                foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						//dimClientAccounts columns
						case "clientaccountkey":
							workgroup.ClientAccountKey = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "sitebankid":
							workgroup.SiteBankID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "siteclientaccountid":
							workgroup.SiteClientAccountID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "mostrecent":
							workgroup.MostRecent = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "isactive":
							workgroup.IsActive = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "dataretentiondays":
							workgroup.DataRetentionDays = ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "imageretentiondays":
							workgroup.ImageRetentionDays = ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "shortname":
							workgroup.ShortName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "longname":
							workgroup.LongName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "filegroup":
							workgroup.FileGroup = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;


						//OLWorkgroups columns
						case "entityid":
							workgroup.EntityID = dr.IsNull(dc.ColumnName) ? null : (int?)dr[dc.ColumnName];
							break;
						case "viewingdays":
							workgroup.ViewingDays = dr.IsNull(dc.ColumnName) ? null : (int?)dr[dc.ColumnName];
							break;
						case "maximumsearchdays":
							workgroup.MaximumSearchDays = dr.IsNull(dc.ColumnName) ? null : (int?)dr[dc.ColumnName];
							break;
						case "checkimagedisplaymode":
							workgroup.CheckImageDisplayMode = (ImageDisplayModes)ipoLib.NVL(ref dr, dc.ColumnName, (byte)ImageDisplayModes.UseInheritedSetting);
							break;
						case "documentimagedisplaymode":
							workgroup.DocumentImageDisplayMode = (ImageDisplayModes)ipoLib.NVL(ref dr, dc.ColumnName, (byte)ImageDisplayModes.UseInheritedSetting);
							break;
						case "hoa":
							workgroup.HOA = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "displaybatchid":
							workgroup.DisplayBatchID = dr.IsNull(dc.ColumnName) ? null : (bool?)dr[dc.ColumnName];
							break;
						case "invoicebalancingoption":
							workgroup.InvoiceBalancing = (BalancingOptions)ipoLib.NVL(ref dr, dc.ColumnName, (byte)BalancingOptions.NotRequired);
							break;
						case "billingaccount":
							workgroup.BillingAccount = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "billingfield1":
							workgroup.BillingField1 = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "billingfield2":
							workgroup.BillingField2 = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;

                        // dimWorkgroupBusinessRules columns
                        case "invoicerequired":
                            if (workgroup.BusinessRules != null)
                                workgroup.BusinessRules.InvoiceRequired = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "balancingrequired":
					        if (workgroup.BusinessRules != null)
					            workgroup.BusinessRules.PreDepositBalancingRequired = ipoLib.NVL(ref dr, dc.ColumnName, false);
					        break;
                        case "postdepositbalancingrequired":
                            if (workgroup.BusinessRules != null)
                                workgroup.BusinessRules.PostDepositBalancingRequired =
                                    ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "postdepositpayerrequired":
                            if (workgroup.BusinessRules != null)
                                workgroup.BusinessRules.PostDepositPayerRequired =
                                    ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;

                    }
                }
			}
			return workgroup;
		}

		public static DDADto ConvertToDDADto(this DataRow dr)
		{
			DDADto dda = null;

			if (dr.Table.Columns.Count > 0)
			{
				dda = new DDADto();
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "aba":
							dda.RT = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "dda":
							dda.DDA = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
					}
				}
			}
			return dda;
		}

		public static ElectronicAccountDto ConvertToElectronicAccountDto(this DataRow dr)
		{
			ElectronicAccountDto ea = null;

			if (dr.Table.Columns.Count > 0)
			{
				ea = new ElectronicAccountDto();
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "ddakey":
							ea.DDAKey = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "sitebankid":
							ea.SiteBankID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "siteclientaccountid":
							ea.SiteClientAccountID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
					}
				}
			}
			return ea;
		}

		public static DataEntryColumnDto ConvertToDataEntryColumnDto(this DataRow dr)
		{
			DataEntryColumnDto deColumn = null;

			if (dr.Table.Columns.Count > 0)
			{
				deColumn = new DataEntryColumnDto();
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "tabletype":
							deColumn.TableType = (ETableType)ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "datatype":
							deColumn.DataType = (EDataType)ipoLib.NVL(ref dr, dc.ColumnName, (int)0);
							break;
						case "fldlength":
							deColumn.FieldLength = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "screenorder":
							deColumn.ScreenOrder = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "tablename":
							deColumn.TableName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
                        case "fieldname":
						case "fldname":
							deColumn.FieldName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "displayname":
							deColumn.DisplayName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "hubcreated":
							deColumn.HubCreated = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "marksense":
							deColumn.MarkSense = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "dataentrycolumnkey":
							deColumn.DataEntryColumnKey = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
                        case "isactive":
                            deColumn.IsActive = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "isrequired":
                            deColumn.IsRequired = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "paymentsource":
                            deColumn.PaymentSource = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "batchsourceshortname":
                            deColumn.BatchSourceShortName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "uilabel":
                            deColumn.UILabel = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "batchsourcekey":
                            deColumn.BatchSourceKey = ipoLib.NVL(ref dr, dc.ColumnName,0);
                            break;
					}
				}
			}
			return deColumn;
		}

		public static DataEntryTemplateDto ConvertToDataEntryTemplateDto(this DataRow dr)
		{
			DataEntryTemplateDto deTemplate = null;

			if (dr.Table.Columns.Count > 0)
			{
				deTemplate = new DataEntryTemplateDto();
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "templateshortname":
							deTemplate.ShortName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "templatedescription":
							deTemplate.Description = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
					}
				}
			}
			return deTemplate;
		}

		public static BusinessRulesDto ConvertToBusinessRulesDto(this DataRow dr)
		{
			BusinessRulesDto rules = null;
			CheckDigitRoutineDto cdr = null;
			FieldValidationsDto fv = null;

			if (dr.Table.Columns.Count > 0)
			{
				rules = new BusinessRulesDto();
				cdr = new CheckDigitRoutineDto() { Key = 0 };
				fv = new FieldValidationsDto() { Key = 0 };

				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "tabletype":
							rules.TableType = (ETableType)ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "fldname":
							rules.FieldName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "batchsourcekey":
							if (dr[dc.ColumnName] != DBNull.Value)
								rules.BatchSourceKey = (short)dr[dc.ColumnName];
							break;
						case "batchpaymenttypekey":
							if (dr[dc.ColumnName] != DBNull.Value)
								rules.BatchPaymentTypeKey = (byte)dr[dc.ColumnName];
							break;
						case "batchpaymentsubtypekey":
							if (dr[dc.ColumnName] != DBNull.Value)
								rules.BatchPaymentSubTypeKey = (byte)dr[dc.ColumnName];
							break;
						case "exceptiondisplayorder":
							if (dr[dc.ColumnName] != DBNull.Value)
								rules.ExceptionDisplayOrder = (byte)dr[dc.ColumnName];
							break;
						case "minlength":
							rules.MinLength =  ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "maxlength":
							rules.MaxLength = ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "required":
							rules.Required = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "usercanedit":
							rules.UserCanEdit = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "formattype":
							rules.FormatType = (FormatType)ipoLib.NVL(ref dr, dc.ColumnName, (byte)1);
							break;
						case "mask":
							rules.Mask = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "requiredmessage":
							rules.RequiredMessage = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;


						case "checkdigitroutinekey":
							cdr.Key = dr.IsNull(dc.ColumnName) ? 0L : (long)dr[dc.ColumnName];
							break;
						case "offset":
							cdr.Offset = ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "method":
							cdr.Method = (CheckDigitMethod)ipoLib.NVL(ref dr, dc.ColumnName, (int)CheckDigitMethod.SumOfDigits);
							break;
						case "modulus":
							cdr.Modulus = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "compliment":
							cdr.Compliment = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "weightpatterndirection":
							cdr.WeightsDirection = (WeightPatternDirection)ipoLib.NVL(ref dr, dc.ColumnName, (int)WeightPatternDirection.LeftToRight);
							break;
						case "weightpatternvalue":
							cdr.Weights = HubConfigDALExtensions.ParseCommaSeparatedIntegers(ipoLib.NVL(ref dr, dc.ColumnName, string.Empty));
							break;
						case "ignorespaces":
							cdr.IgnoreSpaces = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "remainder10replacement":
							cdr.Rem10Replacement = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "remainder11replacement":
							cdr.Rem11Replacement = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "checkdigitfailuremessage":
							cdr.FailureMessage = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;


						case "fieldvalidationkey":
							fv.Key = dr.IsNull(dc.ColumnName) ? 0L : (long)dr[dc.ColumnName];
							break;
						case "fieldvalidationtypekey":
							fv.ValidationTypeKey = ipoLib.NVL(ref dr, dc.ColumnName, (byte)1);
							break;
						case "fieldvalidationfailuremessage":
							fv.FailureMessage = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "fieldvalidationmodificationdatetime":
							if (dr[dc.ColumnName] == DBNull.Value)
								fv.ModificationDateTime = string.Empty;
							else
								fv.ModificationDateTime = ((DateTime)dr[dc.ColumnName]).ToUniversalTime().ToString("u");
							break;
						case "fieldvalidationvaluecount":
							fv.ValueCount = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
					}

					if (cdr.Key != 0)
						rules.CheckDigitRoutine = cdr;
					if (fv.Key != 0)
						rules.FieldValidations = fv;
				}
			}
			return rules;
		}

        public static PaymentSourceDto ConvertToPaymentSourceDto(this DataRow dr)
        {
            PaymentSourceDto paymentSource = null;

            if (dr.Table.Columns.Count > 0)
            {
                paymentSource = new PaymentSourceDto();

                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case "batchsourcekey":
                            paymentSource.BatchSourceKey = Convert.ToInt32(dr["batchsourcekey"].ToString()); //ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "isactive":
                            paymentSource.IsActive = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "shortname":
                            paymentSource.ShortName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "longname":
                            paymentSource.LongName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "systemtype":
                            paymentSource.SystemType = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "entityid":
                            paymentSource.FIEntityID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "fientityname":
                            paymentSource.FIEntityName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "hasbatchassociation":
                            paymentSource.HasBatchAssociation = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                    }
                }
            }
            return paymentSource;
        }

		public static KeyValuePair<char, int?> ConvertToCDReplacementValue(this DataRow dr)
		{
			char key = '\0';
			int? value = null;

			if (dr.Table.Columns.Count > 0)
			{
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "originalvalue":
							key = ((string)dr[dc.ColumnName])[0];//not allowed to be null and better not be an empty string, that's not valid either
							break;
						case "replacementvalue":
							if (dr[dc.ColumnName] != DBNull.Value)
								value = (byte)dr[dc.ColumnName];
							break;
					}
				}
			}
			return new KeyValuePair<char, int?>(key, value);
		}

		public static string ConvertToFieldValue(this DataRow dr)
		{
			string textValue = string.Empty;

			if (dr.Table.Columns.Count > 0)
			{
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "value":
							//TODO: Handle SQL Variant appropriately...
							textValue = dr[dc.ColumnName].ToString();
							break;
					}
				}
			}
			return textValue;
		}

		public static BankDto ConvertToBank(this DataRow dr)
		{
			BankDto bank = new BankDto()
			{
				BankKey = Convert.ToInt32(dr["BankKey"]),
				BankName = Convert.ToString(dr["BankName"]),
				SiteBankID = Convert.ToInt32(dr["SiteBankID"]),
				FIEntityID = ipoLib.NVL(ref dr, "EntityID", 0),
				MostRecent = true,
			};
			return bank;
		}

		public static RecHubEntityDto ConvertToOLEntity(this DataRow dr)
		{
			RecHubEntityDto entity = new RecHubEntityDto()
			{
				EntityID = Convert.ToInt32(dr["EntityID"]),
				DisplayBatchID = ipoLib.NVL(ref dr, "DisplayBatchID", false),
				PaymentImageDisplayMode = (ImageDisplayModes)ipoLib.NVL(ref dr, "PaymentImageDisplayMode", (byte)0),
				DocumentImageDisplayMode = (ImageDisplayModes)ipoLib.NVL(ref dr, "DocumentImageDisplayMode", (byte)0),
				ViewingDays = ipoLib.NVL(ref dr, "ViewingDays", 0),
				MaximumSearchDays = ipoLib.NVL(ref dr, "MaximumSearchDays", 0),
				BillingAccount = ipoLib.NVL(ref dr, "BillingAccount", string.Empty),
				BillingField1 = ipoLib.NVL(ref dr, "BillingField1", string.Empty),
				BillingField2 = ipoLib.NVL(ref dr, "BillingField2", string.Empty),
			};
			return entity;
		}

        public static EventRuleDto ConvertToEventRuleDto(this DataRow dr)
        {
            EventRuleDto eventRule = null;

            if (dr.Table.Columns.Count > 0)
            {
                eventRule = new EventRuleDto();

                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case "eventruleid":
                            eventRule.EventRuleID = Convert.ToInt64(dr[dc.ColumnName].ToString());
                            break;
                        case "description":
                            eventRule.Description = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventid":
                            eventRule.EventID = Convert.ToInt16(dr[dc.ColumnName].ToString());
                            break;
                        case "eventname":
                            eventRule.EventName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventlongname":
                            eventRule.EventLongName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventtype":
                            eventRule.EventType = Convert.ToInt16(dr[dc.ColumnName].ToString());
                            break;
                        case "sitecodeid":
                            eventRule.SiteCodeID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "sitebankid":
                            eventRule.SiteBankID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "siteclientaccountid":
                            eventRule.SiteClientAccountID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "isactive":
                            eventRule.IsActive = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "eventruleoperator":
                            eventRule.Operator = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventrulevalue":
                            eventRule.EventValue = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventtable":
                            eventRule.Table = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventcolumn":
                            eventRule.Column = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "messagedefault":
                            eventRule.MessageDefault = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventtypename":
                            eventRule.EventTypeName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "entityid":
							eventRule.EntityID = dr[dc.ColumnName] == DBNull.Value ? null : (int?)dr[dc.ColumnName];
                            break;
                        case "displaylabel":
							eventRule.WorkgroupName = dr[dc.ColumnName] == DBNull.Value ? null : (string)dr[dc.ColumnName];
                            break;
                    }
                }
            }
            return eventRule;
        }

        public static EventDto ConvertToEventDto(this DataRow dr)
        {
            EventDto eventDto = null;

            if (dr.Table.Columns.Count > 0)
            {
                eventDto = new EventDto();

                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case "eventid":
                            eventDto.ID = Convert.ToInt16(dr[dc.ColumnName].ToString());
                            break;
                        case "eventname":
                            eventDto.Name = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventlongname":
                            eventDto.EventLongName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventtype":
                            eventDto.Type = Convert.ToInt16(dr[dc.ColumnName].ToString());
                            break;
                        case "eventlevel":
                            eventDto.Level = Convert.ToInt16(dr[dc.ColumnName].ToString());
                            break;
                        case "messagedefault":
                            eventDto.Message = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventschema":
                            eventDto.Schema = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventtable":
                            eventDto.Table = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventcolumn":
                            eventDto.Column = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "eventoperators":
                            string[] ops = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty).Split(',');
                            eventDto.Operators = new List<string>();

                            if (ops.Length >= 1 && ops[0] != String.Empty)                                
                                eventDto.Operators.AddRange(ops.ToList());

                            break;
                        case "isactive":
                            eventDto.IsActive = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                    }
                }
            }
            return eventDto;
        }

        public static PreferenceDto ConvertToPreferenceDto(this DataRow dr)
        {
            PreferenceDto preferenceDto = null;

            if (dr.Table.Columns.Count > 0)
            {
                preferenceDto = new PreferenceDto();

                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case "olpreferenceid":
                            preferenceDto.ID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case "preferencename":
                            preferenceDto.Name = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "description":
                            preferenceDto.Description = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "isonline":
                            preferenceDto.IsOnline = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "issystem":
                            preferenceDto.IsSystem = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "defaultsetting":
                            preferenceDto.DefaultSetting = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "apptype":
                            preferenceDto.AppType = ipoLib.NVL(ref dr, dc.ColumnName, byte.MinValue);
                            break;
                        case "preferencegroup":
                            preferenceDto.Group = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "flddatatypeenum":
                            preferenceDto.FieldDataTypeEnum = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "fldsize":
                            preferenceDto.FieldSize = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "fldmaxlength":
                            preferenceDto.FieldMaxLength = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "fldexp":
                            preferenceDto.FieldRegEx = ipoLib.NVL(ref dr, dc.ColumnName, String.Empty);
                            break;
                        case "fldintminvalue":
                             preferenceDto.FieldMinValue = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "fldintmaxvalue":
                            preferenceDto.FieldMaxValue = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "encodeonserialization":
                            preferenceDto.EncodeOnSerialization = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                    }
                }
            }
            return preferenceDto;
        }

		#endregion

		#region XML Helpers

		public static string ToDBXml(this List<DDADto> ddas)
		{
			//format the xml
			StringBuilder sb = new StringBuilder();
			XmlWriterSettings xws = new XmlWriterSettings();
			xws.OmitXmlDeclaration = true;
			XmlWriter xw = XmlWriter.Create(sb, xws);
			xw.WriteStartElement("ddas");

			if (ddas != null)
			{
				foreach (var dda in ddas)
				{
					xw.WriteStartElement("dda");
					xw.WriteAttributeString("rt", dda.RT);
					xw.WriteAttributeString("dda", dda.DDA);
					xw.WriteEndElement();
				}
			}

			xw.WriteEndElement();
			xw.Close();
			
			return sb.ToString();
		}

		public static string ToDBXml(this List<DataEntryColumnDto> deColumns)
		{
			//format the xml
			StringBuilder sb = new StringBuilder();
			XmlWriterSettings xws = new XmlWriterSettings();
			xws.OmitXmlDeclaration = true;
			XmlWriter xw = XmlWriter.Create(sb, xws);
			xw.WriteStartElement("decolumns");

			if (deColumns != null)
			{
				foreach (var deColumn in deColumns)
				{
					xw.WriteStartElement("decolumn");
                    xw.WriteAttributeString("ischeck", deColumn.isCheck ? "1" : "0" );
                    xw.WriteAttributeString("isactive", deColumn.IsActive ? "1" : "0");
                    xw.WriteAttributeString("isrequired", deColumn.IsRequired ? "1" : "0");
                    xw.WriteAttributeString("marksense", deColumn.MarkSense ? "1" : "0");
					xw.WriteAttributeString("datatype", ((int)deColumn.DataType).ToString());
					xw.WriteAttributeString("screenorder", deColumn.ScreenOrder.ToString());
                    xw.WriteAttributeString("uilabel", deColumn.UILabel);
                    xw.WriteAttributeString("fieldname", deColumn.FieldName);
                    xw.WriteAttributeString("batchsourcekey", deColumn.BatchSourceKey.ToString());
                    xw.WriteAttributeString("dataentrycolumnkey", deColumn.DataEntryColumnKey.ToString());
                    //TODO: Investigate if these can be deleted.
                    xw.WriteAttributeString("fldlength", deColumn.FieldLength.ToString());
                    xw.WriteAttributeString("tabletype", ((int)deColumn.TableType).ToString());
					xw.WriteAttributeString("tablename", deColumn.TableName);
					xw.WriteAttributeString("displayname", deColumn.DisplayName);
					xw.WriteEndElement();
				}
			}

			xw.WriteEndElement();
			xw.Close();

			return sb.ToString();
		}

		public static string ToDBXml(this List<BusinessRulesDto> businessRules)
		{
			//format the xml
			StringBuilder sb = new StringBuilder();
			XmlWriterSettings xws = new XmlWriterSettings();
			xws.OmitXmlDeclaration = true;
			XmlWriter xw = XmlWriter.Create(sb, xws);
			xw.WriteStartElement("brs");

			if (businessRules != null)
			{
				foreach (var br in businessRules)
				{
					xw.WriteStartElement("br");
					xw.WriteAttributeString("tabletype", ((int)br.TableType).ToString());
					xw.WriteAttributeString("fldname", br.FieldName);
					if (br.BatchSourceKey != null)
						xw.WriteAttributeString("batchsourcekey", br.BatchSourceKey.Value.ToString());
					if (br.BatchPaymentTypeKey != null)
						xw.WriteAttributeString("batchpaymenttypekey", br.BatchPaymentTypeKey.Value.ToString());
					if (br.BatchPaymentSubTypeKey != null)
						xw.WriteAttributeString("batchpaymentsubtypekey", br.BatchPaymentSubTypeKey.Value.ToString());
					if (br.ExceptionDisplayOrder != null)
						xw.WriteAttributeString("exceptiondisplayorder", br.ExceptionDisplayOrder.Value.ToString());
					xw.WriteAttributeString("minlength", br.MinLength.ToString());
					xw.WriteAttributeString("maxlength", br.MaxLength.ToString());
					xw.WriteAttributeString("required", br.Required ? "1" : "0");
					xw.WriteAttributeString("usercanedit", br.UserCanEdit ? "1" : "0");
					xw.WriteAttributeString("requiredmessage", br.RequiredMessage);
					xw.WriteAttributeString("formattype", ((int)br.FormatType).ToString());
					xw.WriteAttributeString("mask", br.Mask);

					if (br.CheckDigitRoutine != null)
					{
						xw.WriteStartElement("cd");
						xw.WriteAttributeString("offset", br.CheckDigitRoutine.Offset.ToString());
						xw.WriteAttributeString("method", ((int)br.CheckDigitRoutine.Method).ToString());
						xw.WriteAttributeString("modulus", br.CheckDigitRoutine.Modulus.ToString());
						xw.WriteAttributeString("compliment", br.CheckDigitRoutine.Compliment.ToString());
						xw.WriteAttributeString("weightsdirection", ((int)br.CheckDigitRoutine.WeightsDirection).ToString());
						xw.WriteAttributeString("weights", string.Join(",", br.CheckDigitRoutine.Weights.Select(o => o.ToString())));
						xw.WriteAttributeString("ignorespaces", br.CheckDigitRoutine.IgnoreSpaces ? "1" : "0");
						xw.WriteAttributeString("rem10replacement", br.CheckDigitRoutine.Rem10Replacement.ToString());
						xw.WriteAttributeString("rem11replacement", br.CheckDigitRoutine.Rem11Replacement.ToString());
						xw.WriteAttributeString("failuremessage", br.CheckDigitRoutine.FailureMessage);
						
						if (br.CheckDigitRoutine.ReplacementValues != null && br.CheckDigitRoutine.ReplacementValues.Count > 0)
						{
							xw.WriteStartElement("rvs");
							foreach (var replace in br.CheckDigitRoutine.ReplacementValues)
							{
								xw.WriteStartElement("rv");
								xw.WriteAttributeString("orig", replace.Key.ToString());
								if (replace.Value.HasValue)
									xw.WriteAttributeString("repl", replace.Value.ToString());
								xw.WriteEndElement();
							}
							xw.WriteEndElement();
						}
						xw.WriteEndElement();
					}

					if (br.FieldValidations != null)
					{
						xw.WriteStartElement("fv");
						xw.WriteAttributeString("validationtypekey", br.FieldValidations.ValidationTypeKey.ToString());
						xw.WriteAttributeString("failuremessage", br.FieldValidations.FailureMessage);
						
						if (br.FieldValidations.FieldValues != null && br.FieldValidations.FieldValues.Count > 0)
						{
							xw.WriteStartElement("vv");
							foreach (var value in br.FieldValidations.FieldValues)
							{
								xw.WriteStartElement("v");
								xw.WriteAttributeString("val", value);
								xw.WriteEndElement();
							}
							xw.WriteEndElement();
						}
						xw.WriteEndElement();
					}

					xw.WriteEndElement();
				}
			}


			xw.WriteEndElement();
			xw.Close();

			return sb.ToString();
		}

		#endregion

		#region Enum - Tuple Conversion
		public static Tuple<int, string> ConvertIdDescRowToTuple(this DataRow dr, string idColName, string descColName)
		{
			Tuple<int, string> idDesc = null;
			int? id = null;
			string desc = null;

			string lowerIdColName = idColName.ToLower();
			string lowerDescColName = descColName.ToLower();

			if (dr.Table.Columns.Count > 0)
			{
				foreach (DataColumn dc in dr.Table.Columns)
				{
					Type colType = dr[dc.ColumnName].GetType();
					string colName = dc.ColumnName.ToLower();

					if (colName == lowerIdColName)
					{
						if (colType == typeof(byte))
							id = (byte)dr[dc.ColumnName];
						else if (colType == typeof(short))
							id = (short)dr[dc.ColumnName];
						else if (colType == typeof(int))
							id = (int)dr[dc.ColumnName];
						else
							throw new ApplicationException(idColName + "'s data type for static data is not a supported type.");
					}
					else if (colName == lowerDescColName)
					{
						if (colType == typeof(string))
							desc = (string)dr[dc.ColumnName];
						else
							throw new ApplicationException(descColName + "'s data type for static data is not a supported type.");
					}
				}

				if (id.HasValue && desc != null)
					idDesc = new Tuple<int, string>(id.Value, desc);
			}
			return idDesc;
		}

		public static BatchPaymentSubTypeDto ConvertToBatchPaymentSubTypeDto(this DataRow dr)
		{
			BatchPaymentSubTypeDto subType = null;

			if (dr.Table.Columns.Count > 0)
			{
				subType = new BatchPaymentSubTypeDto();
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "batchpaymentsubtypekey":
							subType.BatchPaymentSubTypeKey = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "batchpaymenttypekey":
							subType.BatchPaymentTypeKey = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "subtypedescription":
							subType.SubTypeDescription = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
					}
				}
			}
			return subType;
		}

		#endregion

		#region CSV Helper
		private static int[] ParseCommaSeparatedIntegers(string values)
		{
			if (values == null || values.Length == 0)
				return null;

			return values.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(o => int.Parse(o)).ToArray();
		}
		#endregion
	}
}
