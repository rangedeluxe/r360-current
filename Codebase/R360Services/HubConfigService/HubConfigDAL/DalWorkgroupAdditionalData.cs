﻿using System;

namespace WFS.RecHub.HubConfig
{
    [Flags]
    public enum DalWorkgroupAdditionalData
    {
        None = 0,
        BusinessRules = 1 << 0,
    }
}