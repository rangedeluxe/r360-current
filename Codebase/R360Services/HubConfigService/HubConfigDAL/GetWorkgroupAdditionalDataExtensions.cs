﻿namespace WFS.RecHub.HubConfig
{
    public static class GetWorkgroupAdditionalDataExtensions
    {
        public static DalWorkgroupAdditionalData ToDal(this GetWorkgroupAdditionalData additionalData)
        {
            var result = DalWorkgroupAdditionalData.None;
            if (additionalData.HasFlag(GetWorkgroupAdditionalData.BusinessRules))
                result |= DalWorkgroupAdditionalData.BusinessRules;
            return result;
        }
    }
}