﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.SessionMaintenance.ServiceClient;
using WFS.RecHub.ApplicationBlocks.RAAM.Extensions;
using WFS.RecHub.HubConfig.DTO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Kyle Colden
* Date: 2/17/2014
*
* Purpose: Work Configuration Business Logic Class
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.HubConfig
{
    public class HubConfigAPI : APIBase
    {
        private IRaamClient _raamClient;
        private IHubConfigDAL _workConfigurationDAL;

        public HubConfigAPI(IServiceContext serviceContext)
            : base(serviceContext)
        {
        }

		#region APIBase

		protected override bool ValidateSID(out int userID)
		{
			userID = -1;
			return HubConfigDAL.GetUserIDBySID(_ServiceContext.GetSID(), out userID);
		}

		protected virtual T ValidateUserIdAndDo<T>(Action<T, int> operation, [CallerMemberName] string procName = null)
			where T : BaseResponse, new()
		{
			// Initialize result
			T result = new T();
			result.Status = StatusCode.FAIL;

			try
			{
				OnLogEvent("Current User Claims: \r\n" +_ServiceContext.RAAMClaims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);

				// Validate / get the user ID
				int userID;
				if (ValidateSIDAndLog(procName, out userID))
				{
					// Perform the requested operation
					operation(result, userID);
				}
				else
				{
					result.Errors.Add("Invalid Session");
				}
			}
			catch (Exception ex)
			{
				// Log exception, and return a generic message since we don't know what it says
				result.Errors.Add("Unexpected error in " + procName);
				OnErrorEvent(ex, this.GetType().Name, procName);
			}

			return result;
		}

		protected override ActivityCodes RequestType
		{
			get
			{
				return ActivityCodes.acHubReportWebServiceCall;//TODO: Replace with approriate code onces ipoLib is updated to have new one
			}
		}

        private IList<int> GetUserFIEntityIds()
        {
            return RaamClient.GetUsersFIEntitiesIds();
        }

        #endregion

        public IHubConfigDAL HubConfigDAL
        {
            private get
            {
                if (_workConfigurationDAL == null)
                    _workConfigurationDAL = HubConfigDALFactory.Create(_ServiceContext.SiteKey ?? string.Empty);

                return _workConfigurationDAL;
            }
            set { _workConfigurationDAL = value; }
        }
        public IRaamClient RaamClient
        {
            private get
            {
                if (_raamClient == null)
                {
                    _raamClient = new RaamClient(new ConfigHelpers.Logger(
                        o => OnLogEvent(o, GetType().Name, MessageType.Information, MessageImportance.Verbose),
                        o => OnLogEvent(o, GetType().Name, MessageType.Warning, MessageImportance.Essential)));
                }
                return _raamClient;
            }
            set { _raamClient = value; }
        }

		public HubConfigResponse<string> Ping(string logMessage)
		{
			OnLogEvent("Ping: " + logMessage, typeof(HubConfigAPI).Name, MessageType.Information, MessageImportance.Debug);
			return new HubConfigResponse<string>() { Data = "Pong", Status = StatusCode.SUCCESS };
		}

		public HubConfigResponse<List<WorkgroupDto>> GetWorkgroups(int entityID)
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<WorkgroupDto>>>((response, userId) =>
			{
				response.Data = null;

				List<WorkgroupDto> workgroups = null;

				if (HubConfigDAL.GetWorkgroups(entityID, out workgroups))
				{
					response.Data = workgroups;

                    // Function stored in application blocks.
                    var authorizedwgs = RaamClient.GetAuthorizedWorkgroupsForEntity(entityID, r =>
                    {
                        return new WorkgroupDto()
                        {
                            SiteBankID = int.Parse(r.ExternalReferenceID.Split('|')[0]),
                            SiteClientAccountID = int.Parse(r.ExternalReferenceID.Split('|')[1]),
                            EntityID = r.EntityID
                        };
                    });

                    // Filter by the data we have access to.
                    response.Data = response.Data
                        .Where(x => authorizedwgs
                            .Any(auth => auth.SiteBankID == x.SiteBankID && auth.SiteClientAccountID == x.SiteClientAccountID))
                        .ToList();

					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public HubConfigResponse<List<WorkgroupDto>> GetUnassignedWorkgroups(int fiEntityID)
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<WorkgroupDto>>>((response, userId) =>
			{
				response.Data = null;

				List<WorkgroupDto> workgroups = null;

				if (HubConfigDAL.GetUnassignedWorkgroups(fiEntityID, out workgroups))
				{
					response.Data = workgroups;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public HubConfigResponse<int> GetUnassignedWorkgroupCount(int fiEntityID)
		{
			return ValidateUserIdAndDo<HubConfigResponse<int>>((response, userId) =>
			{
				response.Data = 0;

				int count = 0;

				if (HubConfigDAL.GetUnassignedWorkgroupCount(fiEntityID, out count))
				{
					response.Data = count;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

        public HubConfigResponse<WorkgroupDto> GetWorkgroup(int clientAccountKey, GetWorkgroupAdditionalData additionalData)
        {
            return GetWorkgroupInternal(additionalData, () =>
            {
                WorkgroupDto workgroup;
                if (HubConfigDAL.GetWorkgroup(clientAccountKey, additionalData.ToDal(), out workgroup))
                    return workgroup;
                return null;
            });
        }
        public HubConfigResponse<WorkgroupDto> GetWorkgroup(int siteClientAccountId, int siteBankId,
            GetWorkgroupAdditionalData additionalData)
        {
            return GetWorkgroupInternal(additionalData, () =>
            {
                WorkgroupDto workgroup;
                if (HubConfigDAL.GetWorkgroup(siteClientAccountId, siteBankId, additionalData.ToDal(), out workgroup))
                    return workgroup;
                return null;
            });
        }
        private HubConfigResponse<WorkgroupDto> GetWorkgroupInternal(GetWorkgroupAdditionalData additionalData,
            Func<WorkgroupDto> getWorkgroupFromDal)
        {
            return ValidateUserIdAndDo<HubConfigResponse<WorkgroupDto>>((response, userId) =>
            {
                response.Data = null;

                Guid sessionID = GetSession();
                WorkgroupDto workgroup = getWorkgroupFromDal();
                if (workgroup == null)
                    return;

                if (!RaamClient.IsAuthorizedForWorkgroup(workgroup.SiteBankID, workgroup.SiteClientAccountID))
                {
                    response.Errors.Add("Access denied for this workgroup.");
                    return;
                }

                if (additionalData.HasFlag(GetWorkgroupAdditionalData.Ddas))
                {
                    if (workgroup.SiteBankID > 0 && workgroup.SiteClientAccountID > 0)
                    {
                        List<DDADto> ddas = null;
                        if (!HubConfigDAL.GetWorkgroupDDAs(workgroup.SiteBankID, workgroup.SiteClientAccountID, out ddas))
                            return;

                        workgroup.DDAs = ddas;
                    }
                }

                if (additionalData.HasFlag(GetWorkgroupAdditionalData.DataEntryColumns))
                {
                    if (workgroup.ClientAccountKey > 0)
                    {
                        List<DataEntryColumnDto> deColumns = null;
                        if (!HubConfigDAL.GetWorkgroupDataEntryColumns(workgroup.ClientAccountKey, out deColumns))
                            return;

                        workgroup.DataEntryColumns = deColumns;
                    }
                }

                    //if (workgroup.SiteBankID > 0 && workgroup.SiteClientAccountID > 0 && workgroup.DataEntryColumns != null)
                    //{
                    //    DataEntrySetupDto dataEntrySetup;
                    //    if (!HubConfigDAL.GetDataEntrySetup((int)workgroup.SiteBankID, workgroup.SiteClientAccountID, out dataEntrySetup))
                    //        return;
                    //    workgroup.DataEntrySetup = dataEntrySetup;
                    //}

                response.Data = workgroup;
                response.Status = StatusCode.SUCCESS;
            });
        }

        private string GetUserDisplayName()
        {
            return _ServiceContext.RAAMClaims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
        }
        public HubConfigResponse<int> AddWorkgroup(WorkgroupDto workgroup)
		{
			return ValidateUserIdAndDo<HubConfigResponse<int>>((response, userId) =>
			{
				response.Data = 0;
				string errorMessage;

				//TODO: ensure the workgroup's bank is the parent FI for the workgroup's entity

				if (HubConfigDAL.AddWorkgroup(userId, GetUserDisplayName(), GetUserFIEntityIds(), workgroup, out errorMessage))
				{
					response.Data = workgroup.ClientAccountKey;

					if (RaamClient.CreateOrUpdateWorkgroupResource(workgroup.EntityID.Value, workgroup.SiteBankID, workgroup.SiteClientAccountID, workgroup.LongName))
					{
						response.Status = StatusCode.SUCCESS;

						if (!RefreshCurrentUsersEntitlements())
							response.Errors.Add("The workgroup was created successfully, however your current session was not updated and you may not see the workgroup until you log out and log back in.");
					}
					else
						response.Errors.Add("The workgroup was created successfully, however it could not be assigned to the entity.");

					response.Status = StatusCode.SUCCESS;
				}
				else if (!string.IsNullOrWhiteSpace(errorMessage))
				{
					response.Errors.Add(errorMessage);
				}
			});
		}

		public HubConfigResponse<int> UpdateWorkgroup(WorkgroupDto workgroup)
		{
			return ValidateUserIdAndDo<HubConfigResponse<int>>((response, userId) =>
			{
                if (!RaamClient.IsAuthorizedForWorkgroup(workgroup.SiteBankID, workgroup.SiteClientAccountID))
                {
                    response.Errors.Add("Access denied for this workgroup.");
                    return;
                }

				bool raamNotificationRequired;
				if (HubConfigDAL.UpdateWorkgroup(userId, GetUserDisplayName(), GetUserFIEntityIds(), workgroup, out raamNotificationRequired))
				{
					bool raamUpdateSucceeded = true;
					if (raamNotificationRequired)
					{
                        if (!RaamClient.CreateOrUpdateWorkgroupResource(workgroup.EntityID.Value, workgroup.SiteBankID, workgroup.SiteClientAccountID, workgroup.LongName))
							raamUpdateSucceeded = false;
					}

					if (raamUpdateSucceeded)
					{
						response.Data = workgroup.ClientAccountKey;
						response.Status = StatusCode.SUCCESS;
					}
					else
						response.Errors.Add("The workgroup was updated successfully, however the name change was not replicated throughout the system.");
				}
			});
		}

		public HubConfigResponse<bool> SaveAssignments(List<WorkgroupDto> workgroups)
		{
			return ValidateUserIdAndDo<HubConfigResponse<bool>>((response, userId) =>
			{
				response.Data = false;//haven't partially succeeded at anything yet

				bool somethingSucceeded = false;
				bool partialFailure = false;
				bool entitlementsChanged = false;

				try
				{
					foreach (var workgroup in workgroups)
					{
						if (workgroup.EntityID == null)
						{
							OnLogEvent("Unable to associate workgroup, no entity id provided", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
							partialFailure = true;
							continue;
						}

						//TODO: ensure the workgroup's bank is the parent FI for the workgroup's entity
						//TODO: test for existence of resource before assigning in HUB
						//		if it exists and the entity id matches, then keep going just skip the resource create
						//		if it exists but the entity id does not match, then log, skip and return partial success
						var resource = RaamClient.GetWorkgroupResource(workgroup.SiteBankID, workgroup.SiteClientAccountID);
						if (resource != null && resource.EntityID != workgroup.EntityID)
						{
							OnLogEvent(string.Format("Attempting to assign workgroup {0} to entity {1} but raam already has it assigned to entity {2}",
										workgroup.ClientAccountKey, workgroup.EntityID.Value, resource.EntityID),
										this.GetType().Name, MessageType.Error, MessageImportance.Essential);
							partialFailure = true;
							continue;
						}

						if (!HubConfigDAL.UpdateWorkgroupsEntity(userId, workgroup))
							return;

						somethingSucceeded = true;
						response.Data = true;//partially succeeded at whatever we are doing

						if (!RaamClient.CreateOrUpdateWorkgroupResource(workgroup.EntityID.Value, workgroup.SiteBankID, workgroup.SiteClientAccountID, workgroup.LongName))
							return;
						entitlementsChanged = true;
					}
				}
				finally
				{
					if (entitlementsChanged)
						if (!RefreshCurrentUsersEntitlements())
							response.Errors.Add("Your current session was not updated with the workgroup changes.  Workgroups may not appear correctly until you log out and log back in.");
				}


				response.Data = partialFailure;//everything succeeded, don't flag as partial
				response.Status = somethingSucceeded ? StatusCode.SUCCESS : StatusCode.FAIL;
			});
		}

		public HubConfigResponse<bool> RemoveWorkgroupFromEntity(int clientAccountKey)
		{
			return ValidateUserIdAndDo<HubConfigResponse<bool>>((response, userId) =>
			{
				response.Data = false;//haven't partially succeeded at anything yet

				//get the workgroup, we need the entity id for it
				WorkgroupDto workgroup;
                if (!HubConfigDAL.GetWorkgroup(clientAccountKey, DalWorkgroupAdditionalData.None, out workgroup))
                    return;

                if (!RaamClient.IsAuthorizedForWorkgroup(workgroup.SiteBankID, workgroup.SiteClientAccountID))
                {
                    response.Errors.Add("Access denied for this workgroup.");
                    return;
                }

				//we may be able to change this in the future so we don't need the entity passed in
				// for now, it helps us actually find the resource in RAAM so it can be deleted
				if (workgroup.EntityID != null)
				{
					RaamClient.RemoveWorkgroupResource(workgroup.EntityID.Value, workgroup.SiteBankID, workgroup.SiteClientAccountID);

					//updating RAAM succeeded, now update the OLWorkgroups table
					workgroup.EntityID = null;
					if (!HubConfigDAL.UpdateWorkgroupsEntity(userId, workgroup))
						response.Data = true;//partial failure

					//refresh entitlements, we don't care if this fails as proper database coding should eliminate the workgroup from being displayed
					RefreshCurrentUsersEntitlements();
				}

				response.Status = StatusCode.SUCCESS;
			});
		}

		public HubConfigResponse<ElectronicAccountDto> GetElectronicAccountByDDA(DDADto dda)
		{
			return ValidateUserIdAndDo<HubConfigResponse<ElectronicAccountDto>>((response, userId) =>
			{
				response.Data = null;

				ElectronicAccountDto ea = null;

				if (!HubConfigDAL.GetElectronicAccountByDDA(dda, out ea))
					return;

				response.Data = ea;
				response.Status = StatusCode.SUCCESS;
			});
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetValidationTableTypes()
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<Tuple<int, string>>>>((response, userId) =>
			{
				response.Data = null;

				List<Tuple<int, string>> types = null;

				if (HubConfigDAL.GetValidationTableTypes(out types))
				{
					response.Data = types;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchSources(int fiEntityID)
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<Tuple<int, string>>>>((response, userId) =>
			{
				response.Data = null;

				List<Tuple<int, string>> sources = null;

				if (HubConfigDAL.GetBatchSources(fiEntityID, out sources))
				{
					response.Data = sources;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

        public HubConfigResponse<List<PaymentSourceDto>> GetPaymentSources()
        {
            return ValidateUserIdAndDo<HubConfigResponse<List<PaymentSourceDto>>>((response, userId) =>
            {
                response.Data = null;

                List<PaymentSourceDto> sources = null;

                if (HubConfigDAL.GetPaymentSources(out sources))
                {
                    response.Data = sources;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<PaymentSourceDto> GetPaymentSource(int batchSourceKey)
        {
            return ValidateUserIdAndDo<HubConfigResponse<PaymentSourceDto>>((response, userId) =>
            {
                response.Data = null;

                PaymentSourceDto source = null;

                if (HubConfigDAL.GetPaymentSource(batchSourceKey, out source))
                {
                    response.Data = source;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<int> AddPaymentSource(PaymentSourceDto paymentSource)
        {
            return ValidateUserIdAndDo<HubConfigResponse<int>>((response, userId) =>
            {
				string errorMessage;

				response.Data = 0;

				if (HubConfigDAL.AddPaymentSource(userId, ref paymentSource, out errorMessage))
				{
					response.Data = paymentSource.BatchSourceKey;
					response.Status = StatusCode.SUCCESS;
				}
				else if (!string.IsNullOrWhiteSpace(errorMessage))
				{
					response.Errors.Add(errorMessage);
				}
            });
        }

        public BaseResponse UpdatePaymentSource(PaymentSourceDto paymentSource)
        {
            return ValidateUserIdAndDo<BaseResponse>((response, userId) =>
            {
                if (HubConfigDAL.UpdatePaymentSource(userId, ref paymentSource))
                {
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseResponse DeletePaymentSource(PaymentSourceDto paymentSource)
        {
            string errorMessage;

            return ValidateUserIdAndDo<BaseResponse>((response, userId) =>
            {
                if (HubConfigDAL.DeletePaymentSource(userId, ref paymentSource, out errorMessage))
                {
                    response.Status = StatusCode.SUCCESS;
                }
                else
                {
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { errorMessage };
                }
            });
        }

		public HubConfigResponse<List<Tuple<int, string>>> GetBatchPaymentTypes()
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<Tuple<int, string>>>>((response, userId) =>
			{
				response.Data = null;

				List<Tuple<int, string>> types = null;

				if (HubConfigDAL.GetBatchPaymentTypes(out types))
				{
					response.Data = types;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public HubConfigResponse<List<BatchPaymentSubTypeDto>> GetBatchPaymentSubTypes()
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<BatchPaymentSubTypeDto>>>((response, userId) =>
			{
				response.Data = null;

				List<BatchPaymentSubTypeDto> subTypes = null;

				if (HubConfigDAL.GetBatchPaymentSubTypes(out subTypes))
				{
					response.Data = subTypes;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

        public HubConfigResponse<List<Tuple<int, string>>> GetSystemTypes()
        {
            return ValidateUserIdAndDo<HubConfigResponse<List<Tuple<int, string>>>>((response, userId) =>
            {
                response.Data = null;

                List<Tuple<int, string>> types = null;

                if (HubConfigDAL.GetSystemTypes(out types))
                {
                    response.Data = types;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

		public HubConfigResponse<List<BankDto>> GetBanksForFI(int fiEntityID)
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<BankDto>>>((response, userId) =>
			{
				response.Data = null;
				List<BankDto> banks = null;

				if (HubConfigDAL.GetBanksForFI(fiEntityID, out banks))
				{
					response.Data = banks;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public HubConfigResponse<List<BankDto>> GetBanks()
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<BankDto>>>((response, userId) =>
			{
				response.Data = null;
				List<BankDto> banks = null;

				if (HubConfigDAL.GetBanks(userId, GetUserFIEntityIds(), out banks))
				{
					response.Data = banks;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public HubConfigResponse<BankDto> GetBank(int bankKey)
		{
			return ValidateUserIdAndDo<HubConfigResponse<BankDto>>((response, userId) =>
			{
				response.Data = null;
				BankDto bank = null;

				if (HubConfigDAL.GetBank(userId, GetUserFIEntityIds(), bankKey, out bank))
				{
					response.Data = bank;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public HubConfigResponse<int> AddBank(BankDto bank)
		{
			return ValidateUserIdAndDo<HubConfigResponse<int>>((response, userId) =>
			{
				response.Data = 0;
				string errorMessage;

				// Validate Permissions to the FI
				if (!GetUserFIEntityIds().Contains(bank.FIEntityID))
				{
					if (response.Errors == null)
						response.Errors = new List<string>();
					response.Errors.Add("Unrecognized Entity/FI");
					response.Status = StatusCode.FAIL;
				}

				// Perform Add
				else if (HubConfigDAL.AddBank(userId, ref bank, out errorMessage))
				{
					response.Data = bank.BankKey;
					response.Status = StatusCode.SUCCESS;
				}
				else if (!string.IsNullOrWhiteSpace(errorMessage))
				{
					response.Errors.Add(errorMessage);
				}
			});
		}

		public HubConfigResponse<int> UpdateBank(BankDto bank)
		{
			return ValidateUserIdAndDo<HubConfigResponse<int>>((response, userId) =>
			{
				// Validate Permissions to the FI
				if (!GetUserFIEntityIds().Contains(bank.FIEntityID))
				{
					if (response.Errors == null)
						response.Errors = new List<string>();
					response.Errors.Add("Unrecognized Entity/FI");
					response.Status = StatusCode.FAIL;
				}

				// Perform Update
				else if (HubConfigDAL.UpdateBank(userId, bank))
				{
					response.Data = bank.BankKey;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

        public HubConfigResponse<RecHubEntityDto> GetEntity(int entityId)
        {
            return ValidateUserIdAndDo<HubConfigResponse<RecHubEntityDto>>((response, userId) =>
            {
                response.Data = null;
                RecHubEntityDto entity;

                if (HubConfigDAL.GetEntity(userId, entityId, out entity))
                {
                    response.Data = entity;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<RecHubEntityDto> GetOrCreateEntityWorkgroupDefaults(int entityId)
        {
            return ValidateUserIdAndDo<HubConfigResponse<RecHubEntityDto>>((response, userId) =>
            {
                response.Data = null;
                RecHubEntityDto entity;

                HubConfigDAL.GetEntity(userId, entityId, out entity);
                if (entity == null)
                {
                    var viewingDaysString = HubConfigDAL.GetViewingDaysPreference();
                    int viewingDays;
                    int.TryParse(viewingDaysString, out viewingDays);
                    if (viewingDays <= 0)
                        viewingDays = 365;

                    entity = new RecHubEntityDto
                    {
                        EntityID = entityId,
                        EntityName = "",
                        DocumentImageDisplayMode = ImageDisplayModes.UseInheritedSetting,
                        PaymentImageDisplayMode = ImageDisplayModes.UseInheritedSetting,
                        DisplayBatchID = false,
                        ViewingDays = viewingDays,
                        MaximumSearchDays = 365,
                    };
                    var saveResponse = AddUpdateEntity(entity);
                    if (saveResponse.Status != StatusCode.SUCCESS)
                    {
                        throw new InvalidOperationException(
                            "Error saving entity workgroup defaults: " + string.Join("\r\n", saveResponse.Errors));
                    }
                }

                response.Data = entity;
                response.Status = StatusCode.SUCCESS;
            });
        }

		public BaseResponse AddUpdateEntity(RecHubEntityDto entity)
		{
			return ValidateUserIdAndDo<BaseResponse>((response, userId) =>
			{
                var ent = RaamClient.GetEntityHierarchy(entity.EntityID);
                entity.EntityName = ent.Name;
				// Perform Update
				if (HubConfigDAL.AddUpdateEntity(userId, entity))
				{
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

        public HubConfigResponse<List<EventRuleDto>> GetEventRules()
        {
            return ValidateUserIdAndDo<HubConfigResponse<List<EventRuleDto>>>((response, userId) =>
            {
                response.Data = null;

                List<EventRuleDto> rules = null;

                if (HubConfigDAL.GetEventRules(GetSession(), out rules))
                {
                    response.Data = rules;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<EventRuleDto> GetEventRule(int eventRuleID)
        {
            return ValidateUserIdAndDo<HubConfigResponse<EventRuleDto>>((response, userId) =>
            {
                response.Data = null;

                EventRuleDto rule = null;

                if (HubConfigDAL.GetEventRule(eventRuleID, GetSession(), out rule))
                {
                    if (HubConfigDAL.GetUsersForEventRule(eventRuleID, ref rule))
                    {
                        response.Data = rule;
                        response.Status = StatusCode.SUCCESS;
                        var users = new List<EventRuleUser>();
                        foreach (var usid in rule.UserRA3MSIDs)
                        {
                            Guid current;
                            Guid.TryParse(usid, out current);
                            var user = RaamClient.GetUserByUserSID(current);
                            users.Add(new EventRuleUser
                            {
                                EntityId = user.EntityId,
                                Entity = RaamClient.GetEntityName(user.EntityId),
                                Name = user.Person.FirstName + " " + user.Person.LastName,
                                Id = usid
                            });
                        }
                        rule.AssociatedUsers = users.ToList();
                    }
                }
            });
        }

        public HubConfigResponse<List<EventDto>> GetEvents()
        {
            return ValidateUserIdAndDo<HubConfigResponse<List<EventDto>>>((response, userId) =>
            {
                response.Data = null;

                List<EventDto> events = null;

                if (HubConfigDAL.GetEvents(out events))
                {
                    response.Data = events;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<int> AddEventRule(EventRuleDto eventRule)
        {
            return ValidateUserIdAndDo<HubConfigResponse<int>>((response, userId) =>
            {
                response.Data = 0;
                             
                eventRule.UserIDs = ConvertUserIds(eventRule.UserRA3MSIDs);

				if (HubConfigDAL.AddEventRule(userId, GetSession(), ref eventRule))
                {
                    response.Data = (int)eventRule.EventRuleID;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseResponse UpdateEventRule(EventRuleDto eventRule)
        {
            return ValidateUserIdAndDo<BaseResponse>((response, userId) =>
            {
                eventRule.UserIDs = ConvertUserIds(eventRule.UserRA3MSIDs);

				if (HubConfigDAL.UpdateEventRule(userId, GetSession(), ref eventRule))
                {
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }



		public HubConfigResponse<List<DataEntryTemplateDto>> GetDataEntryTemplates()
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<DataEntryTemplateDto>>>((response, userId) =>
			{
				List<DataEntryTemplateDto> templates;
				if (HubConfigDAL.GetDataEntryTemplates(out templates))
				{
					response.Data = templates;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}


		private bool RefreshCurrentUsersEntitlements()
		{
			try
			{
				var sessionManager = new SessionMaintenanceManager();
				var response = sessionManager.RefreshEntitlements();
				
				if (response.Status != StatusCode.SUCCESS)
				{
					OnLogEvent("Refresh of entitlements failed.  " + response.Errors.FirstOrDefault() ?? string.Empty, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				OnErrorEvent(ex, this.GetType().Name, "RefreshCurrentUsersEntitlements");
				return false;
			}
		}

        public HubConfigResponse<List<PreferenceDto>> GetPreferences()
        {
            return ValidateUserIdAndDo<HubConfigResponse<List<PreferenceDto>>>((response, userId) =>
            {
                response.Data = null;
                List<PreferenceDto> preferences = null;

                if (HubConfigDAL.GetPreferences(out preferences))
                {
                    response.Data = preferences;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseResponse UpdatePreferences(List<PreferenceDto> preferences)
        {
            return ValidateUserIdAndDo<BaseResponse>((response, userId) =>
            {
                if (HubConfigDAL.UpdatePreferences(userId, ref preferences))
                {
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

		public HubConfigResponse<int> GetSystemMaxRetentionDays()
		{
			return ValidateUserIdAndDo<HubConfigResponse<int>>((response, userId) =>
			{
				int maxRetentionDays;
				if (HubConfigDAL.GetSystemMaxRetentionDays(out maxRetentionDays))
				{
					response.Data = maxRetentionDays;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}
        
        public HubConfigResponse<List<DocumentTypeDto>> GetDocumentTypes()
        {
            return ValidateUserIdAndDo<HubConfigResponse<List<DocumentTypeDto>>>((response, userId) =>
            {
                List<DocumentTypeDto> doctypes;
                if (HubConfigDAL.GetDocumentTypes(out doctypes))
                {
                    response.Data = doctypes;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<bool> UpdateDocumentType(DocumentTypeDto type)
        {
            return ValidateUserIdAndDo<HubConfigResponse<bool>>((response, userId) =>
            {
                if (HubConfigDAL.UpdateDocumentType(type, userId))
                {
                    response.Data = true;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<List<NotificationTypeDto>> GetNotificationTypes()
        {
            return ValidateUserIdAndDo<HubConfigResponse<List<NotificationTypeDto>>>((response, userId) =>
            {
                List<NotificationTypeDto> types;
                if (HubConfigDAL.GetNotificationTypes(out types))
                {
                    response.Data = types;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<bool> UpdateNotificationType(NotificationTypeDto type)
        {
            return ValidateUserIdAndDo<HubConfigResponse<bool>>((response, userId) =>
            {
                if (HubConfigDAL.UpdateNotificationType(type, userId))
                {
                    response.Data = true;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public HubConfigResponse<bool> InsertNotificationType(NotificationTypeDto type)
        {
            return ValidateUserIdAndDo<HubConfigResponse<bool>>((response, userId) =>
            {
                if (HubConfigDAL.InsertNotificationType(type, userId))
                {
                    response.Data = true;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

		public HubConfigResponse<List<BusinessRulesDto>> GetFieldValidationSettings(int siteBankID, int workgroupId)
		{
			return ValidateUserIdAndDo<HubConfigResponse<List<BusinessRulesDto>>>((response, userId) =>
			{
				List<BusinessRulesDto> fieldValidations;
				if (HubConfigDAL.GetFieldValidationSettings(siteBankID, workgroupId, GetSession(), out fieldValidations))
				{
					response.Data = fieldValidations;
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

        private List<int> ConvertUserIds(List<string> userSids)
        {
            // convert the RAAMSSID to User Ids
            var users = new List<int>();

            if (userSids == null)
                return users;

            foreach (var sid in userSids)
            {
                int id = 0;
                HubConfigDAL.GetUserIDBySID(new Guid(sid), out id);
                users.Add(id);
            }

            return users;
        }

        public HubConfigResponse<bool> WriteAuditConfigReport(AuditConfigDto dto)
        {
            return ValidateUserIdAndDo<HubConfigResponse<bool>>((response, userId) =>
            {
                if (HubConfigDAL.AuditConfigurationReport(userId, dto))
                {
                    response.Data = true;
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }
    }
}
