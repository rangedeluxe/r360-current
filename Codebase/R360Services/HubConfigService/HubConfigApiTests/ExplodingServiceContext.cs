﻿using System;
using WFS.RecHub.R360Shared;

namespace HubConfigApiTests
{
    public class ExplodingServiceContext : IServiceContext
    {
        public virtual string SiteKey => throw new NotSupportedException();
        public virtual SimpleClaim[] RAAMClaims => throw new NotSupportedException();

        public virtual Guid GetSessionID()
        {
            throw new NotSupportedException();
        }
        public virtual Guid GetSID()
        {
            throw new NotSupportedException();
        }
        public virtual string GetLoginNameForAuditing()
        {
            throw new NotSupportedException();
        }
    }
}