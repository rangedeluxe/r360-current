﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;

namespace HubConfigApiTests
{
    [TestClass]
    public class HubConfigApiTests
    {
        #region Mocks

        private class FakeHubConfigDal : ExplodingHubConfigDal
        {
            private readonly Func<int, RecHubEntityDto, bool> _addUpdateEntity;
            private readonly Func<Tuple<RecHubEntityDto, bool>> _getEntity;
            private readonly Func<string> _getViewingDaysPreference;

            public FakeHubConfigDal(Func<int, RecHubEntityDto, bool> addUpdateEntity,
                Func<Tuple<RecHubEntityDto, bool>> getEntity, Func<string> getViewingDaysPreference)
            {
                _addUpdateEntity = addUpdateEntity;
                _getEntity = getEntity;
                _getViewingDaysPreference = getViewingDaysPreference;
            }

            public override bool AddUpdateEntity(int userId, RecHubEntityDto entity)
            {
                if (_addUpdateEntity != null)
                    return _addUpdateEntity(userId, entity);
                throw new NotSupportedException();
            }
            public override bool GetUserIDBySID(Guid sid, out int userId)
            {
                userId = TestUserId;
                return true;
            }
            public override bool GetEntity(int userId, int entityId, out RecHubEntityDto entity)
            {
                var entityTuple = _getEntity();
                entity = entityTuple.Item1;
                return entityTuple.Item2;
            }
            public override string GetViewingDaysPreference()
            {
                if (_getViewingDaysPreference != null)
                    return _getViewingDaysPreference();
                throw new NotSupportedException();
            }
        }

        private class FakeRaamClient : ExplodingRaamClient
        {
            public override EntityDto GetEntityHierarchy(int entityId)
            {
                Assert.AreEqual(TestEntityId, entityId, "EntityId requested via IRaamClient.GetEntityHierarchy");
                return new EntityDto {ID = entityId, Name = TestEntityName};
            }
        }

        private class FakeServiceContext : ExplodingServiceContext
        {
            public override SimpleClaim[] RAAMClaims => new[]
            {
                new SimpleClaim {Type = "MyType", Value = "MyValue"},
            };

            public override Guid GetSID()
            {
                return new Guid();
            }
        }

        #endregion

        private const int TestUserId = 1;
        private const int TestEntityId = 99;
        private const string TestEntityName = "MyEntity";

        private static void AssertNoErrors(HubConfigResponse<RecHubEntityDto> response)
        {
            Assert.AreEqual("", string.Join("\r\n", response.Errors), "Errors");
            Assert.AreEqual(StatusCode.SUCCESS, response.Status, "Response status");
        }
        private void CheckNewDtoValues(RecHubEntityDto savedDto, int expectedViewingDays)
        {
            Assert.AreEqual(TestEntityName, savedDto.EntityName, "EntityName");
            Assert.AreEqual(TestEntityId, savedDto.EntityID, "EntityId");
            Assert.AreEqual(ImageDisplayModes.UseInheritedSetting, savedDto.DocumentImageDisplayMode,
                "DocumentImageDisplayMode");
            Assert.AreEqual(ImageDisplayModes.UseInheritedSetting, savedDto.PaymentImageDisplayMode,
                "PaymentImageDisplayMode");
            Assert.IsFalse(savedDto.DisplayBatchID, "DisplayBatchID");
            Assert.AreEqual(expectedViewingDays, savedDto.ViewingDays, "ViewingDays");
            Assert.AreEqual(365, savedDto.MaximumSearchDays, "MaximumSearchDays");
            Assert.IsNull(savedDto.BillingAccount, "BillingAccount");
            Assert.IsNull(savedDto.BillingField1, "BillingField1");
            Assert.IsNull(savedDto.BillingField2, "BillingField2");
        }
        private static HubConfigAPI CreateHubConfigApi(IHubConfigDAL hubConfigDal)
        {
            return new HubConfigAPI(CreateServiceContext())
            {
                HubConfigDAL = hubConfigDal,
                RaamClient = CreateRaamClient(),
            };
        }
        private static IHubConfigDAL CreateHubConfigDal(Func<Tuple<RecHubEntityDto, bool>> getEntity,
            Func<string> getViewingDaysPreference = null, Func<int, RecHubEntityDto, bool> addUpdateEntity = null)
        {
            return new FakeHubConfigDal(addUpdateEntity, getEntity, getViewingDaysPreference);
        }
        private static IRaamClient CreateRaamClient()
        {
            return new FakeRaamClient();
        }
        private static IServiceContext CreateServiceContext()
        {
            return new FakeServiceContext();
        }
        private static HubConfigResponse<RecHubEntityDto> GetOrCreateEntityWorkgroupDefaults(HubConfigAPI api,
            int entityId)
        {
            var response = api.GetOrCreateEntityWorkgroupDefaults(entityId);
            AssertNoErrors(response);
            return response;
        }

        [TestMethod]
        public void GetOrCreateEntityWorkgroupDefaults_EntityAlreadyExists()
        {
            var hubConfigDal = CreateHubConfigDal(
                getEntity: () => Tuple.Create(new RecHubEntityDto {BillingAccount = "MyBillingAccount"}, true));
            var api = CreateHubConfigApi(hubConfigDal);

            var response = GetOrCreateEntityWorkgroupDefaults(api, TestEntityId);

            Assert.AreEqual("MyBillingAccount", response.Data.BillingAccount, "BillingAccount");
        }
        [TestMethod]
        public void GetOrCreateEntityWorkgroupDefaults_EntityDoesNotExist_ReturnsNewEntityWorkgroupDefaultsDto()
        {
            const int systemViewingDays = 42;
            var hubConfigDal = CreateHubConfigDal(
                getEntity: () => Tuple.Create((RecHubEntityDto) null, false),
                getViewingDaysPreference: () => systemViewingDays.ToString(),
                addUpdateEntity: (userId, dto) => true);
            var api = CreateHubConfigApi(hubConfigDal);

            var response = GetOrCreateEntityWorkgroupDefaults(api, TestEntityId);

            Assert.IsNotNull(response.Data, "Expected non-null DTO to be returned");
            CheckNewDtoValues(response.Data, expectedViewingDays: systemViewingDays);
        }
        [TestMethod]
        public void GetOrCreateEntityWorkgroupDefaults_EntityDoesNotExist_SavesNewEntityWorkgroupDefaultsToDatabase()
        {
            const int systemViewingDays = 42;
            RecHubEntityDto savedDto = null;
            var hubConfigDal = CreateHubConfigDal(
                getEntity: () => Tuple.Create((RecHubEntityDto) null, false),
                getViewingDaysPreference: () => systemViewingDays.ToString(),
                addUpdateEntity: (userId, dto) =>
                {
                    savedDto = dto;
                    return true;
                });
            var api = CreateHubConfigApi(hubConfigDal);

            GetOrCreateEntityWorkgroupDefaults(api, TestEntityId);

            Assert.IsNotNull(savedDto, "Expected DTO to be saved");
            CheckNewDtoValues(savedDto, expectedViewingDays: systemViewingDays);
        }
        [TestMethod]
        public void GetOrCreateEntityWorkgroupDefaults_EntityDoesNotExist_DefaultViewingDaysIsNotReturned_SavesAndReturns()
        {
            // We don't have requirements for what value we should use if "default viewing days" isn't present
            // in the database - but we do know that we should still save and return some defaults, not error out.

            RecHubEntityDto savedDto = null;
            var hubConfigDal = CreateHubConfigDal(
                getEntity: () => Tuple.Create((RecHubEntityDto) null, false),
                getViewingDaysPreference: () => null,
                addUpdateEntity: (userId, dto) =>
                {
                    savedDto = dto;
                    return true;
                });
            var api = CreateHubConfigApi(hubConfigDal);

            var response = GetOrCreateEntityWorkgroupDefaults(api, TestEntityId);

            Assert.IsNotNull(response.Data, "Expected non-null DTO to be returned");
            Assert.IsNotNull(savedDto, "Expected DTO to be saved");
        }
        [TestMethod]
        public void GetOrCreateEntityWorkgroupDefaults_EntityDoesNotExist_DefaultViewingDaysIsNonNumeric_SavesAndReturns()
        {
            // We don't have requirements for what value we should use if "default viewing days" isn't a valid
            // numeric value - but we do know that we should still save and return some defaults, not error out.

            RecHubEntityDto savedDto = null;
            var hubConfigDal = CreateHubConfigDal(
                getEntity: () => Tuple.Create((RecHubEntityDto) null, false),
                getViewingDaysPreference: () => "abc",
                addUpdateEntity: (userId, dto) =>
                {
                    savedDto = dto;
                    return true;
                });
            var api = CreateHubConfigApi(hubConfigDal);

            var response = GetOrCreateEntityWorkgroupDefaults(api, TestEntityId);

            Assert.IsNotNull(response.Data, "Expected non-null DTO to be returned");
            Assert.IsNotNull(savedDto, "Expected DTO to be saved");
        }
    }
}
