﻿using System;
using System.Collections.Generic;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;

namespace HubConfigApiTests
{
    public class ExplodingRaamClient : IRaamClient
    {
        public virtual EntityDto GetAncestorEntityHierarchy(int entityId)
        {
            throw new NotSupportedException();
        }
        public virtual EntityDto GetAncestorFIEntity(int entityId)
        {
            throw new NotSupportedException();
        }
        public virtual EntityDto GetAuthorizedEntitiesAndWorkgroups()
        {
            throw new NotSupportedException();
        }
        public virtual IList<EntityDto> GetFIEntities()
        {
            throw new NotSupportedException();
        }
        public virtual EntityDto GetLoggedInEntity()
        {
            throw new NotSupportedException();
        }
        public virtual IList<int> GetUsersFIEntitiesIds()
        {
            throw new NotSupportedException();
        }
        public virtual ResourceDto GetWorkgroupResource(int siteBankId, int workgroupId)
        {
            throw new NotSupportedException();
        }
        public virtual EntityDto GetEntitiesWithPermission(int rootEntityId, string asset, R360Permissions.ActionType action)
        {
            throw new NotSupportedException();
        }
        public virtual string GetEntityBreadcrumb(int entityId)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetEntityBreadcrumbList(int entityId, List<EntityBreadcrumbDTO> entityBreadcrumbs, bool includeHierarchy)
        {
            throw new NotSupportedException();
        }
        public virtual IEnumerable<UserDto> GetUserHierarchy()
        {
            throw new NotSupportedException();
        }
        public virtual EntityDto GetAuthorizedEntities()
        {
            throw new NotSupportedException();
        }
        public virtual EntityDto GetEntityGroupHierarchy()
        {
            throw new NotSupportedException();
        }
        public virtual IEnumerable<string> GetEventTypes()
        {
            throw new NotSupportedException();
        }
        public virtual UserDto GetUserByUserSID(Guid userSid)
        {
            throw new NotSupportedException();
        }
        public virtual IEnumerable<ResourceDto> GetWorkgroupsForEntity(int entityid)
        {
            throw new NotSupportedException();
        }
        public virtual bool CreateOrUpdateWorkgroupResource(int entityId, int siteBankId, int workgroupId, string name)
        {
            throw new NotSupportedException();
        }
        public virtual void RemoveWorkgroupResource(int entityId, int siteBankId, int workgroupId)
        {
            throw new NotSupportedException();
        }
        public virtual EntityDto GetEntityHierarchy(int entityId)
        {
            throw new NotSupportedException();
        }
        public virtual IEnumerable<EntityDto> GetAuthorizedEntityList()
        {
            throw new NotSupportedException();
        }
        public virtual string GetEntityName(int entityId)
        {
            throw new NotSupportedException();
        }
        public virtual string GetNameFromSid(Guid sid)
        {
            throw new NotSupportedException();
        }
    }
}