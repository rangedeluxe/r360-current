﻿using System;
using System.Collections.Generic;
using WFS.RecHub.HubConfig;
using WFS.RecHub.HubConfig.DTO;

namespace HubConfigApiTests
{
    public class ExplodingHubConfigDal : IHubConfigDAL
    {
        public virtual bool GetUserIDBySID(Guid sid, out int userId)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetWorkgroups(int entityId, out List<WorkgroupDto> workgroups)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetUnassignedWorkgroups(int fiEntityId, out List<WorkgroupDto> workgroups)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetUnassignedWorkgroupCount(int fiEntityId, out int count)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetWorkgroup(int clientAccountKey, DalWorkgroupAdditionalData additionalData,
            out WorkgroupDto workgroup)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetWorkgroup(int siteClientAccountId, int siteBankId, DalWorkgroupAdditionalData additionalData,
            out WorkgroupDto workgroup)
        {
            throw new NotSupportedException();
        }
        public virtual bool AddWorkgroup(int userId, string userDisplayName, IList<int> fiEntityIds, WorkgroupDto workgroup,
            out string errorMessage)
        {
            throw new NotSupportedException();
        }
        public virtual bool UpdateWorkgroup(int userId, string userDisplayName, IList<int> fiEntityIds, WorkgroupDto workgroup,
            out bool raamNotificationRequired)
        {
            throw new NotSupportedException();
        }
        public virtual bool UpdateWorkgroupsEntity(int userId, WorkgroupDto workgroup)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetWorkgroupDDAs(int siteBankId, int siteClientAccountId, out List<DDADto> ddas)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetElectronicAccountByDDA(DDADto dda, out ElectronicAccountDto electronicAccount)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetWorkgroupDataEntryColumns(int clientAccountKey, out List<DataEntryColumnDto> deColumns)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetDataEntrySetup(int siteBankId, int workGroupId, out DataEntrySetupDto dataEntrySetup)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetPaymentSources(out List<PaymentSourceDto> paymentSources)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetPaymentSource(int batchSourceKey, out PaymentSourceDto paymentSource)
        {
            throw new NotSupportedException();
        }
        public virtual bool AddPaymentSource(int userId, ref PaymentSourceDto paymentSource, out string errorMessage)
        {
            throw new NotSupportedException();
        }
        public virtual bool UpdatePaymentSource(int userId, ref PaymentSourceDto paymentSource)
        {
            throw new NotSupportedException();
        }
        public virtual bool DeletePaymentSource(int userId, ref PaymentSourceDto paymentSource, out string errorMessage)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetBanksForFI(int fiEntityId, out List<BankDto> banks)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetBanks(int userId, IList<int> fiEntityIds, out List<BankDto> banks)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetBank(int userId, IList<int> fiEntityIds, int bankKey, out BankDto bank)
        {
            throw new NotSupportedException();
        }
        public virtual bool AddBank(int userId, ref BankDto bank, out string errorMessage)
        {
            throw new NotSupportedException();
        }
        public virtual bool UpdateBank(int userId, BankDto bank)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetEntity(int userId, int entityId, out RecHubEntityDto entity)
        {
            throw new NotSupportedException();
        }
        public virtual bool AddUpdateEntity(int userId, RecHubEntityDto entity)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetEventRules(Guid sessionId, out List<EventRuleDto> eventRules)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetEventRule(int eventRuleId, Guid sessionId, out EventRuleDto eventRule)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetEvents(out List<EventDto> events)
        {
            throw new NotSupportedException();
        }
        public virtual bool AddEventRule(int userId, Guid sessionId, ref EventRuleDto eventRule)
        {
            throw new NotSupportedException();
        }
        public virtual bool UpdateEventRule(int userId, Guid sessionId, ref EventRuleDto eventRule)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetUsersForEventRule(int eventRuleId, ref EventRuleDto eventRule)
        {
            throw new NotSupportedException();
        }
        public virtual string GetViewingDaysPreference()
        {
            throw new NotSupportedException();
        }
        public virtual bool GetPreferences(out List<PreferenceDto> preferences)
        {
            throw new NotSupportedException();
        }
        public virtual bool UpdatePreferences(int userId, ref List<PreferenceDto> preferences)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetSystemMaxRetentionDays(out int maxRetentionDays)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetDataEntryTemplates(out List<DataEntryTemplateDto> templates)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetValidationTableTypes(out List<Tuple<int, string>> types)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetBatchSources(int fiEntityId, out List<Tuple<int, string>> sources)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetBatchPaymentTypes(out List<Tuple<int, string>> paymentTypes)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetBatchPaymentSubTypes(out List<BatchPaymentSubTypeDto> paymentSubTypes)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetSystemTypes(out List<Tuple<int, string>> systemTypes)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetDocumentTypes(out List<DocumentTypeDto> doctypes)
        {
            throw new NotSupportedException();
        }
        public virtual bool UpdateDocumentType(DocumentTypeDto type, int userId)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetNotificationTypes(out List<NotificationTypeDto> doctypes)
        {
            throw new NotSupportedException();
        }
        public virtual bool InsertNotificationType(NotificationTypeDto item, int userId)
        {
            throw new NotSupportedException();
        }
        public virtual bool UpdateNotificationType(NotificationTypeDto item, int userId)
        {
            throw new NotSupportedException();
        }
        public virtual bool GetFieldValidationSettings(int siteBankId, int workgroupId, Guid sessionId,
            out List<BusinessRulesDto> businessRules)
        {
            throw new NotSupportedException();
        }
        public virtual bool AuditConfigurationReport(int userId, AuditConfigDto dto)
        {
            throw new NotSupportedException();
        }
    }
}
