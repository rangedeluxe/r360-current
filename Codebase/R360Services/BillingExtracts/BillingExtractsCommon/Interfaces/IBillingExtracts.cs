﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BillingExtracts.Common
{
	[ServiceContract(Namespace = "urn:wausaufs.com:services:BillingExtractsService")]
	public interface IBillingExtracts
	{
		[OperationContract]
		PingResponse Ping();

		[OperationContract]
		BillingFormatsResponse GetBillingFormats();

		[OperationContract]
		GenerateFileResponse GenerateFile(string xmlParameters);
	}
}
