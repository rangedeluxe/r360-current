﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Ryan Schwantes
 * * Date: 04/09/2014
 * *
 * * Purpose: Common Reponse Format DTO
 * *
 * * Modification History
 * WI 100422 CEJ 05/24/2013 Created
* ******************************************************************************/
namespace WFS.RecHub.BillingExtracts.Common
{
	[DataContract]
	public class BillingFormat
	{
		/// <summary>
		/// Display Name is intended for displaying in the UI for selection, etc.
		/// </summary>
		[DataMember]
		public string DisplayName { get; set; }

		/// <summary>
		/// The Format "Class" is a unique identifier for this format type, that could be stored to the database, etc.
		/// </summary>
		[DataMember]
		public string FormatClass { get; set; }

		/// <summary>
		/// The Base Parameters are an XML formatted string containing the Format Class.
		/// The call to generate a file could either build a parameter string using the FormatClass, or just start with the Base Parameters and add XML attributes
		/// </summary>
		[DataMember]
		public string BaseParameters { get; set; }
	}
}
