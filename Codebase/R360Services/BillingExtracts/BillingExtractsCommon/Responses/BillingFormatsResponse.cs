﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Ryan Schwantes
 * * Date: 04/09/2014
 * *
 * * Purpose: Common Reponse Format DTO
 * *
 * * Modification History
 * WI 100422 CEJ 05/24/2013 Created
 * ******************************************************************************/
namespace WFS.RecHub.BillingExtracts.Common
{
	[DataContract]
	public class BillingFormatsResponse : BaseResponse
	{
		public BillingFormatsResponse()
			: base()
		{
		}

		[DataMember]
		public List<BillingFormat> formatList { get; set; }
	}
}
