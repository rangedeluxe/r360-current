﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.GenericCSV
{
	public class GenericLineItem
	{
		const string _dateFormat = "yyyy-MM-dd";

		public int FIEntityID { get; set; }
		public int SiteBankID { get; set; }
		public int EntityID { get; set; }
		public int WorkgroupID { get; set; }
		public string WorkgroupName { get; set; }
		public Analysis.ActivityInfo Activity { get; set; }
		public int Value { get; set; }

		/* Generic CSV example:
			"Start Date","End Date","FI Entity ID","Site Bank ID","Entity ID","Workgroup ID","Workgroup ShortName","Activity Code","Activity Description","Value"
			2015-01-01,2015-01-31,42,103,142,521,"My Workgroup",5001,"Total Workgroups",10000
		*/

		public static void WriteHeaders(StreamWriter output)
		{
			output.WriteLine("\"Start Date\",\"End Date\",\"FI Entity ID\",\"Site Bank ID\",\"Entity ID\",\"Workgroup ID\",\"Workgroup Name\",\"Activity Code\",\"Activity Description\",\"Value\"");
		}

		public void ToCSV(SharedExtractParametersBase extractParams, StreamWriter output)
		{
			// Build activity description string
			string activityDescription = Activity.Description.Replace("\"", "");
			if (Activity.Retention != null)
			{
				if (Activity.Retention.Value == int.MaxValue)
					activityDescription += " (Max)";
				else
					activityDescription += " (" + Activity.Retention.Value.ToString() + " " + Activity.Retention.Units.ToString() + ")";
			}

			output.WriteLine("{0},{1},{2},{3},{4},{5},\"{6}\",{7},\"{8}\",{9}",
				extractParams.DateRange.Item1.ToString(_dateFormat, CultureInfo.InvariantCulture),
				extractParams.DateRange.Item2.ToString(_dateFormat, CultureInfo.InvariantCulture),
				FIEntityID, 
				SiteBankID, 
				EntityID,
				WorkgroupID,
				WorkgroupName.Replace("\"", ""),		// Remove quotes from names, if applicable, to prevent CSV corruption
				Activity.ActivityCode,
				activityDescription,
				Value);									// Value is an int; This will have to be updated if we need to support dollar amounts...
		}
	}
}
