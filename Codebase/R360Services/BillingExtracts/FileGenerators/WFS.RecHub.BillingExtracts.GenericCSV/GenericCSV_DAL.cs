﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.GenericCSV
{
	// The "DALBase" class is used to aid faking for unit tests...  But since this DAL contains no private logic right now, we can leave it out...
	//internal abstract class DALBase : SharedDALBase
	//{
	//	protected DALBase(ConfigParameters config) : base(config) { }
	//}

	internal class GenericCSV_DAL : SharedDALBase //DALBase
	{
		public GenericCSV_DAL(/*ConfigParameters*/ SharedConfigParametersBase config) : base(config)
		{
		}
	}
}
