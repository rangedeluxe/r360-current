﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.GenericCSV
{
	class ExtractParameters : SharedExtractParametersBase
	{
		public ExtractParameters(ConfigParameters config, string xmlParameters)
			: base (config, xmlParameters)
		{
			_config.API.LogEvent(string.Format("Generating File for Date Range: {0} - {1}, for Entity '{2}'", 
				DateRange.Item1.ToShortDateString(), DateRange.Item2.ToShortDateString(), TargetEntity),
				typeof(GenericCSVGenerator).Name, IFileGeneratorAPI_MessageType.Information, IFileGeneratorAPI_MessageImportance.Verbose);
		}
	}
}
