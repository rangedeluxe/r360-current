﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;
using RVUnit = WFS.RecHub.BillingExtracts.API.Analysis.RetentionValue.Unit;
using AggType = WFS.RecHub.BillingExtracts.API.Analysis.AggregationType;

namespace WFS.RecHub.BillingExtracts.GenericCSV
{
	class GenericLineItemDefs
	{
		/// <summary>
		/// This is the list of defined Activity Codes for this file format.
		/// </summary>
		public static readonly Analysis.ActivityInfo[] ActivityCodeList;
		static GenericLineItemDefs()
		{
			// Activity Codes:
			//	1000s = Group by FI
			//	2000s = Group by Customer
			//	3000s = Group by Workgroup

			// Build Activity Code list
			List<Analysis.ActivityInfo> list = new List<Analysis.ActivityInfo>();
			Add(list, new Analysis.ActivityInfo()
			{
				ActivityCode = 1000,
				Description = "Configured FI",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.FI_Entity,
				VolumeTarget = Analysis.DetailLevel.FI_Entity,
				Aggregation = AggType.CountDistinct,
			});
			Add(list, new Analysis.ActivityInfo()
			{
				ActivityCode = 1001,
				Description = "Count of Customers",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.FI_Entity,
				VolumeTarget = Analysis.DetailLevel.Customer_Entity,
				Aggregation = AggType.CountDistinct,
			});
			Add(list, new Analysis.ActivityInfo()
			{
				ActivityCode = 2000,
				Description = "Configured Customer",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Customer_Entity,
				Aggregation = AggType.CountDistinct,
			});
			Add(list, new Analysis.ActivityInfo()
			{
				ActivityCode = 2001,
				Description = "Count of Workgroups",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Workgroup,
				Aggregation = AggType.CountDistinct,
			});
			Add(list, new Analysis.ActivityInfo() 
			{
				ActivityCode =    2010,
				Description = "Count of Items Stored (C)",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.TotalByRetention,
				Retention =								RV(13, RVUnit.Months), },
					RVC(	Code: 2011,		Retention:  RV( 3, RVUnit.Years) ),
					RVC(	Code: 2012,		Retention:  RV( 7, RVUnit.Years) )
			);
			Add(list, new Analysis.ActivityInfo() 
			{
				ActivityCode =    3010,
				Description = "Count of Items Stored (W)",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.TotalByRetention,
				Retention =								RV(13, RVUnit.Months), },
					RVC(	Code: 3011,		Retention:  RV( 3, RVUnit.Years) ),
					RVC(	Code: 3012,		Retention:  RV( 7, RVUnit.Years) )
			);
			Add(list, new Analysis.ActivityInfo() 
			{
				ActivityCode =    2020,
				Description = "Count of Payment Items Stored (C)",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.PaymentItem,
				Aggregation = AggType.TotalByRetention,
				Retention =								RV(13, RVUnit.Months), },
					RVC(	Code: 2021,		Retention:  RV( 3, RVUnit.Years) ),
					RVC(	Code: 2022,		Retention:  RV( 7, RVUnit.Years) )
			);
			Add(list, new Analysis.ActivityInfo() 
			{
				ActivityCode =    3020,
				Description = "Count of Payment Items Stored (W)",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.PaymentItem,
				Aggregation = AggType.TotalByRetention,
				Retention =								RV(13, RVUnit.Months), },
					RVC(	Code: 3021,		Retention:  RV( 3, RVUnit.Years) ),
					RVC(	Code: 3022,		Retention:  RV( 7, RVUnit.Years) )
			);
			Add(list, new Analysis.ActivityInfo()
			{
				ActivityCode = 2030,
				Description = "Count of Non-Payment Items Stored (C)",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.NonPaymentItem,
				Aggregation = AggType.TotalByRetention,
				Retention = RV(13, RVUnit.Months),
			},
					RVC(Code: 2031, Retention: RV(3, RVUnit.Years)),
					RVC(Code: 2032, Retention: RV(7, RVUnit.Years))
			);
			Add(list, new Analysis.ActivityInfo()
			{
				ActivityCode = 3030,
				Description = "Count of Non-Payment Items Stored (W)",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.NonPaymentItem,
				Aggregation = AggType.TotalByRetention,
				Retention = RV(13, RVUnit.Months),
			},
					RVC(Code: 3031, Retention: RV(3, RVUnit.Years)),
					RVC(Code: 3032, Retention: RV(7, RVUnit.Years))
			);
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 1100,
				Description = "Count of Customers (Exceptions)",
				AnalysisKind = Analysis.Kind.Exceptions,
				Grouping = Analysis.GroupingLevel.FI_Entity,
				VolumeTarget = Analysis.DetailLevel.Customer_Entity,
				Aggregation = AggType.CountDistinct,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2100,
				Description = "Configured Customer (Exceptions)",
				AnalysisKind = Analysis.Kind.Exceptions,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Customer_Entity,
				Aggregation = AggType.CountDistinct,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2110,
				Description = "Count of Transactions (Exceptions) (C)",
				AnalysisKind = Analysis.Kind.Exceptions,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Transaction,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3110,
				Description = "Count of Transactions (Exceptions) (W)",
				AnalysisKind = Analysis.Kind.Exceptions,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Transaction,
				Aggregation = AggType.Total,
			});
			//list.Add(new Analysis.ActivityInfo() 
			//{
			//	ActivityCode = 5243,
			//	Description = "Fee Per Item Rejected (Exceptions)",
			//	AnalysisKind = Analysis.Kind.Exception_Rejects,
			//	VolumeTarget = Analysis.DetailLevel.Item,
			//	Aggregation = AggType.Count,
			//});
			//list.Add(new Analysis.ActivityInfo()		Not supporting this code for 2.01
			//{
			//	ActivityCode = 5230,
			//	Description = "Fee Per Workgroup for Early (pre-EOD) Image Viewing",
			//	AnalysisKind = Analysis.Kind.EarlySameDayViewings,
			//	VolumeTarget = Analysis.DetailLevel.Workgroup,
			//	Aggregation = AggType.Count,
			//});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2200,
				Description = "ZIP Files downloaded",
				AnalysisKind = Analysis.Kind.BulkDownloadZip,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2201,
				Description = "TIFFs downloaded in ZIP files",
				AnalysisKind = Analysis.Kind.BulkDownloadZip,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3200,
				Description = "ZIP Files downloaded (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadZip,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3201,
				Description = "TIFFs downloaded in ZIP files (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadZip,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2210,
				Description = "CSV Files downloaded (format 1)",
				AnalysisKind = Analysis.Kind.BulkDownloadCsv1,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2211,
				Description = "Items downloaded in CSV files (format 1)",
				AnalysisKind = Analysis.Kind.BulkDownloadCsv1,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3210,
				Description = "CSV Files downloaded (format 1) (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadCsv1,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3211,
				Description = "Items downloaded in CSV files (format 1) (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadCsv1,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2220,
				Description = "CSV Files downloaded (format 2)",
				AnalysisKind = Analysis.Kind.BulkDownloadCsv2,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2221,
				Description = "Items downloaded in CSV files (format 2)",
				AnalysisKind = Analysis.Kind.BulkDownloadCsv2,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3220,
				Description = "CSV Files downloaded (format 2) (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadCsv2,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3221,
				Description = "Items downloaded in CSV files (format 2) (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadCsv2,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2230,
				Description = "HTML Views created",
				AnalysisKind = Analysis.Kind.BulkDownloadHtml,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2231,
				Description = "Items included in HTML views",
				AnalysisKind = Analysis.Kind.BulkDownloadHtml,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3230,
				Description = "HTML Views created (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadHtml,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3231,
				Description = "Items included in HTML views (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadHtml,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2240,
				Description = "PDF Views created",
				AnalysisKind = Analysis.Kind.BulkDownloadPdfView,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2241,
				Description = "Items included in PDF views",
				AnalysisKind = Analysis.Kind.BulkDownloadPdfView,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3240,
				Description = "PDF Views created (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadPdfView,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3241,
				Description = "Items included in PDF views (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadPdfView,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2250,
				Description = "Print views created",
				AnalysisKind = Analysis.Kind.BulkDownloadPrintView,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2251,
				Description = "Items included in Print views",
				AnalysisKind = Analysis.Kind.BulkDownloadPrintView,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3250,
				Description = "Print views created (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadPrintView,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3251,
				Description = "Items included in Print views (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadPrintView,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2260,
				Description = "XML Files created",
				AnalysisKind = Analysis.Kind.BulkDownloadXml,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 2261,
				Description = "Items included in XML files",
				AnalysisKind = Analysis.Kind.BulkDownloadXml,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3260,
				Description = "XML Files created (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadXml,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 3261,
				Description = "Items included in XML files (W)",
				AnalysisKind = Analysis.Kind.BulkDownloadXml,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});

			ActivityCodeList = list.ToArray();
		}

		/// <summary>
		/// This method adds an activity to the list, and if applicable creates duplicate activities that differ only in the analysis code and retention value
		/// </summary>
		/// <param name="firstActivity">The first (fully specified) activity</param>
		/// <param name="additionalRetentionLevels">A list of additional retention levels, with the associated analysis code</param>
		/// <returns></returns>
		private static void Add(List<Analysis.ActivityInfo> list, Analysis.ActivityInfo activity, params Tuple<int, Analysis.RetentionValue>[] additionalRetentionLevels)
		{
			// Keep the first activity
			list.Add(activity);

			// Create copies for additional retention levels
			foreach (var level in additionalRetentionLevels)
			{
				list.Add(new Analysis.ActivityInfo()
					{
						ActivityCode = level.Item1,
						Aggregation = activity.Aggregation,
						AnalysisKind = activity.AnalysisKind,
						Description = activity.Description,
						Retention = level.Item2,
						Grouping = activity.Grouping,
						VolumeTarget = activity.VolumeTarget,
					});
			}
		}

		/// <summary>
		/// This is just an abbreviation for the RetentionValue constructor
		/// </summary>
		private static Analysis.RetentionValue RV(int value, RVUnit unit)
		{
			return new Analysis.RetentionValue(value, unit);
		}

		/// <summary>
		/// This is an abbreviation for creating an AnalysisCode / Retention Tuple with labeled parameters
		/// </summary>
		/// <param name="Code"></param>
		/// <param name="Retention"></param>
		/// <returns></returns>
		private static Tuple<int, Analysis.RetentionValue> RVC(int Code, Analysis.RetentionValue Retention)
		{
			return Tuple.Create(Code, Retention);
		}
	}
}
