﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.GenericCSV
{
	internal class ConfigParameters : SharedConfigParametersBase
	{
		public ConfigParameters() : base("BillngGnrcCSV") { }

		protected override void LoadConfiguration()
		{
			// Use a well-known configuration file name matching the assembly name
			string configFileName = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath + ".config";
			XDocument configDoc = XDocument.Load(configFileName);

			/*
			 <>
				<OutputFolder>...</OutputFolder>
				<OutputFileNamePattern>...</OutputFileNamePattern>
			 </>
			*/

			OutputFolder = API.GetRequiredString(configDoc, "OutputFolder", configFileName, ModuleName);
			OutputFileNamePattern = API.GetRequiredString(configDoc, "OutputFileNamePattern", configFileName, ModuleName);
		}

		//public new DALBase DAL
		//{
		//	get
		//	{
		//		return base.DAL as DALBase;
		//	}
		//}

		public override SharedDALBase CreateDAL()
		{
			return new GenericCSV_DAL(this);
		}

		public override void SetExtractParameters(string xmlParameters)
		{
			ExtractParams = new ExtractParameters(this, xmlParameters);
		}
	}
}
