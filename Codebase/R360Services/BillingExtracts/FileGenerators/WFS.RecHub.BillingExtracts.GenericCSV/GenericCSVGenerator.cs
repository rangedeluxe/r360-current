﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.GenericCSV
{
	public class GenericCSVGenerator : SharedGeneratorBase<GenericLineItem>, ICustomFileGenerator
	{
		public string GetDisplayName()
		{
			return "Generic CSV Billing Extract";
		}

		public void PerformScheduledWork(IFileGeneratorAPI api, IEnumerable<BillingBackgroundTaskTypes> scheduledTaskTypes)
		{
			BackgroundTasks.CheckForWork(api, scheduledTaskTypes);
		}

		protected override SharedConfigParametersBase CreateConfigParameters()
		{
			return new ConfigParameters();
		}

		public void GenerateFile(IFileGeneratorAPI api, string xmlParameters, out List<string> errors)
		{
			errors = new List<string>();

			// Assemble parameters
			_config.SetAPI(api);
			_config.SetExtractParameters(xmlParameters);

			// Load LineItem data
			List<GenericLineItem> lineItems = LoadLineItems(GenericLineItemDefs.ActivityCodeList);

			// Put them in a determinative order
			lineItems = lineItems.OrderBy(o => o.Activity.ActivityCode).ThenBy(o => o.FIEntityID).ThenBy(o => o.SiteBankID).ThenBy(o => o.EntityID).ThenBy(o => o.WorkgroupID).ToList();

			// Create the output file
			using (var fs = _config.GetNewOutputStream())
			{
				using (StreamWriter outputFile = new StreamWriter(fs))
				{
					GenericLineItem.WriteHeaders(outputFile);

					// Write out line items...
					foreach (var lineItem in lineItems)
					{
						lineItem.ToCSV(_config.ExtractParams, outputFile);
					}
				}
			}
		}

		protected override void HandleSimpleCountLineItems(List<GenericLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Simple counts create one line item per grouping key, with an associated volume count (1 per "X")
			//   (Note: Since the count must be "distinct", it matters what "X" is - but that is handled at the next level...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "count" the same "thing", but may have different grouping levels
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exists:
					activityInfos.ForEach(o => VerifyTargetIn(o, new Analysis.DetailLevel[] 
						{ Analysis.DetailLevel.Workgroup, Analysis.DetailLevel.Customer_Entity, Analysis.DetailLevel.FI_Entity }, 
						"Stat: Distinct Configuration Entries"));

					CreateLineItemsWithCounts(lineItems, activityInfos, _config.DAL.GetCustomers);
					break;

				case Analysis.Kind.Exceptions:
					activityInfos.ForEach(o => VerifyTarget(o, Analysis.DetailLevel.Customer_Entity, "Stat: Exception Enabled Customers"));
					CreateLineItemsWithCounts(lineItems, activityInfos, _config.DAL.GetExceptionsEnabledCustomers);
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for simple count aggregation: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void HandleSimpleTotalledLineItems(List<GenericLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Simple totalled creates one line item per grouping key, with an associated volume total
			//   (Note: Since we are totalling everything under the grouping level, it doesn't particularly matter what the detail level returned by the DAL is, as long as it can be totalled...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "total" the same "thing", but may have different detail levels and/or grouping levels
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exceptions:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.Transaction }.ToList(), "Stat: Exceptions");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetExceptionCounts);
					break;

				case Analysis.Kind.BulkDownloadZip:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk File Download (ZIP)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadZipCounts);
					break;

				case Analysis.Kind.BulkDownloadCsv1:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk Download (CSV v1)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadCsv1Counts);
					break;

				case Analysis.Kind.BulkDownloadCsv2:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk Download (CSV v2)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadCsv2Counts);
					break;

				case Analysis.Kind.BulkDownloadHtml:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk Download (HTML)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadHtmlCounts);
					break;

				case Analysis.Kind.BulkDownloadPdfView:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk Download (PDF View)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadPdfViewCounts);
					break;

				case Analysis.Kind.BulkDownloadPrintView:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk Download (Print View)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadPrintViewCounts);
					break;

				case Analysis.Kind.BulkDownloadXml:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk Download (XML)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadXmlCounts);
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for simple total aggregation: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void HandleRetentionTotalsLineItems(List<GenericLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Retention totals create one line item *per retention partition* per grouping key, with an associated volume total
			//   (Note: Since we are totalling everything under the grouping level, it doesn't particularly matter what the detail level returned by the DAL is, as long as it can be totalled...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "total" the same "thing", but may have different detail levels and/or grouping levels, and will be partitioned into different retention periods
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exists:
					switch (activityInfos[0].VolumeTarget)
					{
						case Analysis.DetailLevel.Item:
							CreateCumulativeTierRetentionCounts(lineItems, activityInfos, null, _config.DAL.GetItemCountsByRetention);
							break;

						case Analysis.DetailLevel.PaymentItem:
							CreateCumulativeTierRetentionCounts(lineItems, activityInfos, null, _config.DAL.GetPaymentItemCountsByRetention);
							break;

						case Analysis.DetailLevel.NonPaymentItem:
							CreateCumulativeTierRetentionCounts(lineItems, activityInfos, null, _config.DAL.GetNonPaymentItemCountsByRetention);
							break;

						default:
							_config.API.LogEvent("Unexpected Target for Retention-based existence counts: " + activityInfos[0].VolumeTarget, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
							break;
					}
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for Retention-based counts: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void CreateLineItems(List<GenericLineItem> lineItems, Dictionary<GenericGroupingKeys, GenericCounts> data, IList<Analysis.ActivityInfo> activityInfos)
		{
			foreach (var entry in data)
			{
				for (int i = 0; i < activityInfos.Count; i++)
				{
					lineItems.Add(new GenericLineItem()
					{
						Activity = activityInfos[i],
						FIEntityID = entry.Key.FIEntityID,
						SiteBankID = entry.Key.SiteBankID,
						EntityID = entry.Key.EntityID,
						WorkgroupID = entry.Key.WorkgroupID,
						WorkgroupName = entry.Key.WorkgroupID > 0 ? entry.Value.WorkgroupName : "",
						Value = entry.Value.Counts[i],
					});
				}
			}
		}
	}
}
