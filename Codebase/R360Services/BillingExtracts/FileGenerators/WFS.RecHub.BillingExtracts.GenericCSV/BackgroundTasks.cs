﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.GenericCSV
{
	internal static class BackgroundTasks
	{
		public static void CheckForWork(IFileGeneratorAPI api, IEnumerable<BillingBackgroundTaskTypes> scheduledTasks)
		{
			api.LogEvent("Checking for scheduled tasks...", "GenericCSVBkrnd", IFileGeneratorAPI_MessageType.Information, IFileGeneratorAPI_MessageImportance.Debug);

			// No work is defined specific to the Generic CSV format at this time
			// Note: Generic CSV *is* dependent on several standard scheduled tasks
			BillingBackgroundTaskTypes[] requiredTasks = new BillingBackgroundTaskTypes[] 
			{
				BillingBackgroundTaskTypes.SummarizePaymentFacts,
				BillingBackgroundTaskTypes.SummarizeStubFacts,
				BillingBackgroundTaskTypes.SummarizeDocumentFacts,
				BillingBackgroundTaskTypes.SummarizeActiveWorkgroups,
				BillingBackgroundTaskTypes.SummarizeBulkFileRecordCounts_PrintView,
				BillingBackgroundTaskTypes.SummarizeBulkFileRecordCounts_PdfView,
				BillingBackgroundTaskTypes.SummarizeBulkFileRecordCounts_Csv,
				BillingBackgroundTaskTypes.SummarizeBulkFileRecordCounts_ZipDownload,
				BillingBackgroundTaskTypes.SummarizeBulkFileRecordCounts_DownloadCsv,
				BillingBackgroundTaskTypes.SummarizeBulkFileRecordCounts_DownloadXml,
				BillingBackgroundTaskTypes.SummarizeBulkFileRecordCounts_DownloadHtml,
				BillingBackgroundTaskTypes.SummarizeExceptionsEnabledCustomers,
				BillingBackgroundTaskTypes.SummarizeExceptionCounts,
			};

			foreach (var task in requiredTasks)
			{
				if (!scheduledTasks.Contains(task))
				{
					api.LogEvent("Configuration Error: Billing Schedule Manager must be configured to run task of type '" + task.ToString() + "'", "GenericCSVBkrnd", IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
				}
			}
		}
	}
}
