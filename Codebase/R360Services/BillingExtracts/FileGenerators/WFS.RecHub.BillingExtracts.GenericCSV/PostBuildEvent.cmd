REM TargetPath = %1 
REM TargetDir = %2
REM ProjectDir = %3
REM OutDir = %4

if %4=="bin\Debug\." goto CreateDebuggingConfigAndCopy
if %4=="bin\Release\." goto Copy
goto End

:CreateDebuggingConfigAndCopy
echo Info: Setting Debugging Configuration
%3\..\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %1.config "/replace:<MockDAL>false;;;<MockDAL>true"

:Copy
IF NOT EXIST %3\..\..\BillingExtractsService\%4 GOTO NoServiceBuildDir
cd %3\..\..\BillingExtractsService\%4

IF EXIST "CustomFileGenerators" GOTO HasCustomDir
md "CustomFileGenerators"

:HasCustomDir
copy %1 %3\..\..\BillingExtractsService\%4\CustomFileGenerators\
copy %1.config %3\..\..\BillingExtractsService\%4\CustomFileGenerators\

:NoServiceBuildDir

:End
echo Info: Post Build Event Complete