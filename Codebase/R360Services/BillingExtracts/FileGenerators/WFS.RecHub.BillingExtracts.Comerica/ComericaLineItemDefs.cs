﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;
using RVUnit = WFS.RecHub.BillingExtracts.API.Analysis.RetentionValue.Unit;
using AggType = WFS.RecHub.BillingExtracts.API.Analysis.AggregationType;

namespace WFS.RecHub.BillingExtracts.Comerica
{
	class ComericaLineItemDefs
	{
		/// <summary>
		/// This is the list of defined Activity Codes for this file format.
		/// </summary>
		public static readonly Analysis.ActivityInfo[] ActivityCodeList;
		static ComericaLineItemDefs()
		{
			// For Comerica: Billing Field 1 = the Region / Bank Number
			// All output values are grouped by Region / Billing Account
			// But those are properties of a Workgroup or Customer -- so set the grouping level to "Workgroup" or "Customer", and re-aggregate at the end...

			// Build Activity Code list
			List<Analysis.ActivityInfo> list = new List<Analysis.ActivityInfo>();
			//list.Add(new Analysis.ActivityInfo()
			//{
			//	ActivityCode = 1000,
			//	Description = "New Customer Setup Fee (No Output)",
			//	AnalysisKind = Analysis.Kind.Setup,
			//	Grouping = Analysis.GroupingLevel.Customer_Entity,
			//	VolumeTarget = Analysis.DetailLevel.Customer_Entity,
			//	Aggregation = AggType.CountDistinct,
			//});
			//list.Add(new Analysis.ActivityInfo()
			//{
			//	ActivityCode = 2000,
			//	Description = "New Customer Setup Fee (With Output)",
			//	AnalysisKind = Analysis.Kind.Setup,
			//	Grouping = Analysis.GroupingLevel.Customer_Entity,
			//	VolumeTarget = Analysis.DetailLevel.Customer_Entity,
			//	Aggregation = AggType.CountDistinct,
			//});
			//list.Add(new Analysis.ActivityInfo()
			//{
			//	ActivityCode = 3000,
			//	Description = "New Customer Setup Fee (Custom)",
			//	AnalysisKind = Analysis.Kind.Setup,
			//	Grouping = Analysis.GroupingLevel.Customer_Entity,
			//	VolumeTarget = Analysis.DetailLevel.Customer_Entity,
			//	Aggregation = AggType.CountDistinct,
			//}); 
			Add(list, new Analysis.ActivityInfo()
			{
				ActivityCode = 8263,
				Description = "Monthly Maintenance Fee Per Customer",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Customer_Entity,
				Aggregation = AggType.CountDistinct,
			});
			Add(list, new Analysis.ActivityInfo() 
			{
				ActivityCode =    8254,
				Description = "Item Storage Fee (13 Months)",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.TotalByRetention,
				Retention =								RV(13, RVUnit.Months), },
					RVC(	Code: 8255,		Retention:  RV( 3, RVUnit.Years),		Description: "Extended Retention (3 Years)"),
					RVC(	Code: 8256,		Retention:  RV( 7, RVUnit.Years),		Description: "Extended Retention (7 Years)" )
			);
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 8259,
				Description = "Monthly Online Exceptions Maintenance",
				AnalysisKind = Analysis.Kind.Exceptions,
				Grouping = Analysis.GroupingLevel.Customer_Entity,
				VolumeTarget = Analysis.DetailLevel.Customer_Entity,
				Aggregation = AggType.CountDistinct,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 4100,
				Description = "Exception Transactions",
				AnalysisKind = Analysis.Kind.Exceptions,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Transaction,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 5000,
				Description = "ZIP Files downloaded",
				AnalysisKind = Analysis.Kind.BulkDownloadZip,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 5001,
				Description = "TIFFs downloaded in ZIP files",
				AnalysisKind = Analysis.Kind.BulkDownloadZip,
				Grouping = Analysis.GroupingLevel.Workgroup,
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});

			//list.Add(new Analysis.ActivityInfo()
			//{
			//	ActivityCode = 8263,
			//	Description = "Data File Extract Maintenance Fee",
			//	AnalysisKind = Analysis.Kind.Manual,
			//	Grouping = Analysis.GroupingLevel.Customer_Entity,
			//	VolumeTarget = Analysis.DetailLevel.Customer_Entity,
			//	Aggregation = AggType.Total,
			//});
			//list.Add(new Analysis.ActivityInfo()
			//{
			//	ActivityCode = 8264,
			//	Description = "Image File Extract Maintenance Fee",
			//	AnalysisKind = Analysis.Kind.Manual,
			//	Grouping = Analysis.GroupingLevel.Customer_Entity,
			//	VolumeTarget = Analysis.DetailLevel.Customer_Entity,
			//	Aggregation = AggType.Total,
			//});
			//list.Add(new Analysis.ActivityInfo()
			//{
			//	ActivityCode = 9000,
			//	Description = "Report File Maintenance Fee",
			//	AnalysisKind = Analysis.Kind.Manual,
			//	Grouping = Analysis.GroupingLevel.Customer_Entity,
			//	VolumeTarget = Analysis.DetailLevel.Customer_Entity,
			//	Aggregation = AggType.Total,
			//}); 

			ActivityCodeList = list.ToArray();
		}

		/// <summary>
		/// This method adds an activity to the list, and if applicable creates duplicate activities that differ only in the analysis code and retention value
		/// </summary>
		/// <param name="firstActivity">The first (fully specified) activity</param>
		/// <param name="additionalRetentionLevels">A list of additional retention levels, with the associated analysis code</param>
		/// <returns></returns>
		private static void Add(List<Analysis.ActivityInfo> list, Analysis.ActivityInfo activity, params Tuple<int, Analysis.RetentionValue, string>[] additionalRetentionLevels)
		{
			// Keep the first activity
			list.Add(activity);

			// Create copies for additional retention levels
			foreach (var level in additionalRetentionLevels)
			{
				list.Add(new Analysis.ActivityInfo()
					{
						ActivityCode = level.Item1,
						Aggregation = activity.Aggregation,
						AnalysisKind = activity.AnalysisKind,
						Description = level.Item3 ?? activity.Description,
						Grouping = activity.Grouping,
						Retention = level.Item2,
						VolumeTarget = activity.VolumeTarget,
					});
			}
		}

		/// <summary>
		/// This is just an abbreviation for the RetentionValue constructor
		/// </summary>
		private static Analysis.RetentionValue RV(int value, RVUnit unit)
		{
			return new Analysis.RetentionValue(value, unit);
		}

		/// <summary>
		/// This is an abbreviation for creating an AnalysisCode / Retention Tuple with labeled parameters
		/// </summary>
		/// <param name="Code"></param>
		/// <param name="Retention"></param>
		/// <returns></returns>
		private static Tuple<int, Analysis.RetentionValue, string> RVC(int Code, Analysis.RetentionValue Retention, string Description)
		{
			return Tuple.Create(Code, Retention, Description);
		}
	}
}
