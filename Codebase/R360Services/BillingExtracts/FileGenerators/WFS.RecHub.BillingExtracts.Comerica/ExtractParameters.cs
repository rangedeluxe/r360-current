﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.Comerica
{
	class ExtractParameters : SharedExtractParametersBase
	{
		public string TargetFileType { get; private set; }
		public string TargetFileTypeDesc { get { return TargetFileType.Replace(';', '_'); } }
		public string[] TargetFileTypes { get { return TargetFileType.Split(';'); } }

		public ExtractParameters(ConfigParameters config, string xmlParameters) 
			: base(config, xmlParameters)
		{
			// Comerica-specific parameter: FileType
			TargetFileType = config.API.GetOptionalString(_xml.Root, "FileTypes", ConfigParameters.c_XAT + ";" + ConfigParameters.c_FB80 + ";" + ConfigParameters.c_PRN);

			config.API.LogEvent(string.Format("Generating File for Date Range: {0} - {1}, for Entity '{2}', with FileTypes: '{3}'",
				DateRange.Item1.ToShortDateString(), DateRange.Item2.ToShortDateString(), TargetEntity, TargetFileType),
				typeof(ComericaGenerator).Name, IFileGeneratorAPI_MessageType.Information, IFileGeneratorAPI_MessageImportance.Verbose);

		}
	}
}
