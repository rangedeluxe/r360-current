﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.Comerica
{
	internal class ConfigParameters : SharedConfigParametersBase
	{
		public ConfigParameters() : base("BillngComerica") { }

		public const string c_XAT = "XAT";
		public const string c_FB80 = "FB80";
		public const string c_PRN = "PRN";

		const string c_appSourceId = "460";
		const string c_appCode = "INR TM";

		public static readonly string[] c_fileTypes = new string[] { c_XAT, c_FB80, c_PRN };
		
		protected override void LoadConfiguration()
		{
			// Use a well-known configuration file name matching the assembly name
			string configFileName = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath + ".config";
			XDocument configDoc = XDocument.Load(configFileName);

			/*
			 <>
				<OutputFolder>...</OutputFolder>
				<OutputFileNamePattern>...</OutputFileNamePattern>
			 </>
			*/

			OutputFolder = API.GetRequiredString(configDoc, "OutputFolder", configFileName, ModuleName);
			foreach (var fileType in c_fileTypes)
				_outputFileNamePatterns[fileType] = API.GetRequiredString(configDoc, "OutputFileNamePattern" + fileType, configFileName, ModuleName);

			AppSourceId = API.GetOptionalString(configDoc, "AppSourceID", c_appSourceId, ModuleName);
			AppCode = API.GetOptionalString(configDoc, "AppCode", c_appCode, ModuleName);
		}

		private readonly Dictionary<string, string> _outputFileNamePatterns = new Dictionary<string, string>();
		public IReadOnlyDictionary<string, string> OutputFileNamePatterns
		{
			get
			{
				return _outputFileNamePatterns;
			}
		}

		public string AppSourceId { get; private set; }
		public string AppCode { get; private set; }

		//public new DALBase DAL
		//{
		//	get
		//	{
		//		return base.DAL as DALBase;
		//	}
		//}

		public override SharedDALBase CreateDAL()
		{
			return new Comerica_DAL(this);
		}

		// This custom formatter generates multiple files simultaneously, so it doesn't use the base methods which only handle a single file at a time...
		public Dictionary<string, Stream> MockOutputStreams { get; set; }
		public Stream GetNewOutputStream(string fileType)
		{
			if (!c_fileTypes.Contains(fileType))
				throw new Exception("File Type " + fileType + " is not recognized");

			if (MockOutputStreams != null && MockOutputStreams.ContainsKey(fileType))
				return MockOutputStreams[fileType];
			else
			{
				if (!OutputFileNamePatterns.ContainsKey(fileType) || OutputFileNamePatterns[fileType] == null)
					throw new Exception("Invalid configuration for filetype = " + fileType);

				// Format filename, including parameters such as the date range
				string filename = string.Format(OutputFileNamePatterns[fileType], ExtractParams.DateRange.Item1, ExtractParams.DateRange.Item2, DateTime.Now);

				// Call base class, so that it can handle things like redirecting to another server...
				return OpenFile(filename);

				// Note: The caller is responsible for disposing this FileStream...
			}
		}

		public override void SetExtractParameters(string xmlParameters)
		{
			ExtractParams = new ExtractParameters(this, xmlParameters);
		}
	}
}
