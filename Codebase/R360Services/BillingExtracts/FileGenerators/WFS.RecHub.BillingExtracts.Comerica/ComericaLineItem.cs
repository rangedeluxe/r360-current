﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.Comerica
{
	public class ComericaLineItem
	{
		const string _dateFormat = "MMddyyyy";

		public Analysis.ActivityInfo Activity { get; set; }
		public int BankNumber { get; set; }
		public long AccountNumber { get; set; }
		public int Units { get; set; }

		internal ComericaLineItem Clone()
		{
			return (ComericaLineItem)base.MemberwiseClone();
		}

		/* XAT (summary) example:
			00202010312013460INR TM  08263Data File Extract Maintenance Fee       00000000000001 M000000000000000 00000000     
		*/
		internal void ToXATFixedLine(ConfigParameters configParams, StreamWriter output)
		{
			// Build activity description string
			int bankNumber = BankNumber % 1000; // Ensure it does not exceed 3 digits
			string cycleDate = ToCycleDateString(configParams.ExtractParams.DateRange.Item2);
			string description = Activity.Description.Length <= 40 ? Activity.Description : Activity.Description.Substring(0, 40);

			output.WriteLine("{0:000}020{1}{2:-3}{3,-8}{4:00000}{5,-40}{6:00000000000000}{7}M000000000000000 00000000{8,-30}",
				bankNumber,
				cycleDate,
				configParams.AppSourceId,
				configParams.AppCode,
				Activity.ActivityCode,
				description,
				Math.Abs(Units),
				(Units < 0 ? "-" : " "),
				string.Empty);	// End with filler...
		}

		/* FB80 (detail) example:
			002018812319460201031201310312013INR TM  082630000001 460                       
		*/
		internal void ToFB80FixedLine(ConfigParameters configParams, StreamWriter output)
		{
			// Build activity description string
			int bankNumber = BankNumber % 1000; // Ensure it does not exceed 3 digits
			string cycleDate = ToCycleDateString(configParams.ExtractParams.DateRange.Item2);

			output.WriteLine("{0:000}0{1:0000000000}{2}{3}{4,-8}{5:00000}{6:0000000}{7}{8:-3}{9,-23}",
				bankNumber,
				AccountNumber,
				cycleDate,
				cycleDate,	// Same date for Posting as Effective
				configParams.AppCode,
				Activity.ActivityCode,
				Math.Abs(Units),
				(Units < 0 ? "-" : " "),
				configParams.AppSourceId,
				string.Empty);	// End with filler...
		}

		/* PRN (summary) example:
			Run Date: 10/23/2013
			Run Time: 15:31:35
			Source: 460
			Control ID: INR TM
			Run Bank: 002                                                           Page   1
                                                                                
													 INR
										 XAA INTERFACE DETAIL REPORT
											  WORK OF 10/31/2013
                                                                                
			ACCOUNT             SERVICE
			NUMBER              CODE                 UNITS
			--------------      ------------      --------
			1881231946          8263                     1
								8264                     1
								8253                     1
								8254                    20
								8255                    20
								8256                    20
								8259                     1
								8261                     1
								------------      --------
								Total Units             65
                                                                                
			<FF>
			Run Date: 10/23/2013
			Run Time: 15:31:35
			Source: 460
			Control ID: INR TM
			Run Bank: 002                                                           Page   2
                                                                                
													 INR
										 XAA INTERFACE SUMMARY REPORT
											  WORK OF 10/31/2013
                                                                                
			SERVICE          SERVICE CODE
			CODE             DESCRIPTION                                       UNITS
			-----------      ----------------------------------------      ---------
			8263             Data File Extract Maintenance Fee                     1
			8264             Image File Extract Maintenance Fee                    1
			8253             Monthly Maintenance Fee Per Customer                  1
			8254             R360 Per Item Import                                 20
			8255             Extended Record Retention/Availability (             20
			8256             Extended Record Retention/Availability (             20
			8259             Monthly Online Exceptions Maintenance                 1
			8261             Online Invoice Matching Maintenance Fee               1
							 ----------------------------------------      ---------
							 GRAND TOTAL                                          65
			<FF>
			Run Date: 10/23/2013			
		*/
		internal void WritePRNPageHeader(DateTime runTime, ConfigParameters configParams, StreamWriter output, int pageNumber, bool detail)
		{
			int bankNumber = BankNumber % 1000; // Ensure it does not exceed 3 digits

			if (pageNumber > 1) 
				output.Write('\x0C'); // Form Feed

			output.WriteLine("Run Date: {0:MM/dd/yyyy}", runTime);
			output.WriteLine("Run Time: {0:HH:mm:ss}", runTime);
			output.WriteLine("Source: {0}", configParams.AppSourceId);
			output.WriteLine("Control ID: {0}", configParams.AppCode);
			output.WriteLine("Run Bank: {0:000} {1,62} {2,3}", bankNumber, "Page", pageNumber);
			output.WriteLine();
			output.WriteLine("{0,44}", "INR");
			if (detail)
			{
				output.WriteLine("{0,56}", "XAA INTERFACE DETAIL REPORT");
			}
			else
			{
				output.WriteLine("{0,57}", "XAA INTERFACE SUMMARY REPORT");
			}
			
			output.WriteLine("{0,41} {1:MM/dd/yyyy}", "WORK OF", configParams.ExtractParams.DateRange.Item2);
			output.WriteLine();

			if (detail)
			{
				output.WriteLine("ACCOUNT             SERVICE");
				output.WriteLine("NUMBER              CODE                 UNITS");
				output.WriteLine("--------------      ------------      --------");
			}
			else
			{
				output.WriteLine("SERVICE          SERVICE CODE");
				output.WriteLine("CODE             DESCRIPTION                                       UNITS");
				output.WriteLine("-----------      ----------------------------------------      ---------");
			}
		}

		public void ToPRNSummaryLine(StreamWriter output)
		{
			string description = Activity.Description.Length <= 40 ? Activity.Description : Activity.Description.Substring(0, 40);
			output.WriteLine("{0,-12}     {1,-40}      {2,9}", Activity.ActivityCode, description, Units);
		}

		public void ToPRNDetailLine(StreamWriter output, long lastAccount)
		{
			output.WriteLine("{0,-14}      {1,-12}      {2,8}", AccountNumber == lastAccount ? "" : AccountNumber.ToString(),  Activity.ActivityCode, Units);
		}

		public static void WritePRNPageFooter(StreamWriter output, int totalUnits, bool detail)
		{
			if (detail)
			{
				output.WriteLine("                    ------------      --------");
				output.WriteLine("                    Total Units       {0,8}", totalUnits);
				output.WriteLine();
			}
			else
			{
				output.WriteLine("                 ----------------------------------------      ---------");
				output.WriteLine("                 GRAND TOTAL                                   {0,9}", totalUnits);
			}
		}

		internal string ToCycleDateString(DateTime date)
		{
			// TODO: Do we need to adjust for business dates?
			return date.ToString(_dateFormat, CultureInfo.InvariantCulture);
		}
	}
}
