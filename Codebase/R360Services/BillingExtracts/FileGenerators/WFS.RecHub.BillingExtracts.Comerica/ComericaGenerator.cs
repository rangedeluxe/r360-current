﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.Comerica
{
	public class ComericaGenerator : SharedGeneratorBase<ComericaLineItem>, ICustomFileGenerator
	{
		public string GetDisplayName()
		{
			return "Comerica Billing Extracts";
		}

		public void PerformScheduledWork(IFileGeneratorAPI api, IEnumerable<BillingBackgroundTaskTypes> scheduledTaskTypes)
		{
			BackgroundTasks.CheckForWork(api, scheduledTaskTypes);
		}

		protected override SharedConfigParametersBase CreateConfigParameters()
		{
			return new ConfigParameters();
		}

		internal new ConfigParameters _config
		{
			get { return base._config as ConfigParameters; }
		}

		public void GenerateFile(IFileGeneratorAPI api, string xmlParameters, out List<string> errors)
		{
			errors = new List<string>();

			// Assemble parameters
			_config.SetAPI(api);
			_config.SetExtractParameters(xmlParameters);

			// Load LineItem data
			List<ComericaLineItem> lineItems = LoadLineItems(ComericaLineItemDefs.ActivityCodeList);

			// Sort the line items into order so we can do summary aggregation on detail data
			Sort(ref lineItems);

			// Aggregate data for summary / detail
			List<ComericaLineItem> detailLines = new List<ComericaLineItem>();
			List<ComericaLineItem> summaryLines = new List<ComericaLineItem>();

			if (lineItems.Count > 0)
			{
				ComericaLineItem currentDetail = null;

				// Write out line items to each file...
				for (int i = 0; i < lineItems.Count; i++)
				{
					var lineItem = lineItems[i];

					// Detail - need to accumulate in case multiple workgroups contain the same Billing Account?
					if (currentDetail != null &&
						currentDetail.BankNumber == lineItem.BankNumber &&
						currentDetail.Activity.ActivityCode == lineItem.Activity.ActivityCode &&
						currentDetail.AccountNumber == lineItem.AccountNumber)
					{
						// Need to accumulate
						currentDetail = currentDetail.Clone();
						currentDetail.Units += lineItem.Units;
					}
					else
					{
						// Push, and Start new accumulator
						if (currentDetail != null)
							detailLines.Add(currentDetail);
						currentDetail = lineItem;
					}

					// Summary
					var summary = summaryLines.Find(o => o.BankNumber == lineItem.BankNumber && o.Activity.ActivityCode == lineItem.Activity.ActivityCode);
					if (summary == null)
					{
						// Start new accumulator
						summary = lineItem.Clone();
						summaryLines.Add(summary);
					}
					else
					{
						// Accumulate
						summary.Units += lineItem.Units;
					}
				}

				// Push final detail line
				detailLines.Add(currentDetail);
			}

			// Create the output file(s)
			ExtractParameters extractParams = _config.ExtractParams as ExtractParameters;
			if (extractParams.TargetFileTypes.Contains(ConfigParameters.c_XAT))
			{
				using (var output = new StreamWriter(_config.GetNewOutputStream(ConfigParameters.c_XAT)))
				{
					foreach (var line in summaryLines)
					{
						line.ToXATFixedLine(_config, output);
					}
				}
			}

			if (extractParams.TargetFileTypes.Contains(ConfigParameters.c_FB80))
			{
				using (var output = new StreamWriter(_config.GetNewOutputStream(ConfigParameters.c_FB80)))
				{
					foreach (var line in detailLines)
					{
						line.ToFB80FixedLine(_config, output);
					}
				}
			}

			if (extractParams.TargetFileTypes.Contains(ConfigParameters.c_PRN))
			{
				using (var output = new StreamWriter(_config.GetNewOutputStream(ConfigParameters.c_PRN)))
				{
					if (detailLines.Count > 0)
					{
						// Initialize state
						DateTime runTime = DateTime.Now;
						int pageNumber = 1;
						int bank = detailLines[0].BankNumber;
						int totalUnits = 0;
						long lastAccount = -1;
						int summaryIndex = 0;

						// Start first page
						detailLines[0].WritePRNPageHeader(runTime, _config, output, pageNumber, true);
						for (int detailIndex = 0; detailIndex < detailLines.Count; detailIndex++)
						{
							var line = detailLines[detailIndex];
							if (line.BankNumber != bank)
							{
								// Finish detail page
								ComericaLineItem.WritePRNPageFooter(output, totalUnits, true);

								// Write the summary page for this bank
								pageNumber++;
								summaryLines[summaryIndex].WritePRNPageHeader(runTime, _config, output, pageNumber, false);
								while (summaryLines[summaryIndex].BankNumber == bank)
								{
									summaryLines[summaryIndex].ToPRNSummaryLine(output);
									summaryIndex++;
								}
								ComericaLineItem.WritePRNPageFooter(output, totalUnits, false);

								// Next detail page
								bank = line.BankNumber;
								pageNumber++;
								totalUnits = 0;
								lastAccount = -1;
								line.WritePRNPageHeader(runTime, _config, output, pageNumber, true);
							}

							totalUnits += line.Units;
							line.ToPRNDetailLine(output, lastAccount);
							lastAccount = line.AccountNumber;
						}

						// Finish last detail page
						ComericaLineItem.WritePRNPageFooter(output, totalUnits, true);

						// Write the summary page for this bank
						pageNumber++;
						summaryLines[summaryIndex].WritePRNPageHeader(runTime, _config, output, pageNumber, false);
						while (summaryIndex < summaryLines.Count)
						{
							summaryLines[summaryIndex].ToPRNSummaryLine(output);
							summaryIndex++;
						}
						ComericaLineItem.WritePRNPageFooter(output, totalUnits, false);
					}
				}
			}
		}

		protected override void HandleSimpleCountLineItems(List<ComericaLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Simple counts create one line item per grouping key, with an associated volume count (1 per "X")
			//   (Note: Since the count must be "distinct", it matters what "X" is - but that is handled at the next level...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "count" the same "thing", but may have different grouping levels
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exists:
					activityInfos.ForEach(o => VerifyTargetIn(o, new Analysis.DetailLevel[] { Analysis.DetailLevel.Workgroup, Analysis.DetailLevel.Customer_Entity, Analysis.DetailLevel.FI_Entity },
						"Stat: Distinct Configuration Entries"));

					CreateLineItemsWithCounts(lineItems, activityInfos, _config.DAL.GetCustomers);
					break;

				case Analysis.Kind.Exceptions:
					activityInfos.ForEach(o => VerifyTarget(o, Analysis.DetailLevel.Customer_Entity, "Stat: Exception Enabled Customers"));
					CreateLineItemsWithCounts(lineItems, activityInfos, _config.DAL.GetExceptionsEnabledCustomers);
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for simple count aggregation: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void HandleSimpleTotalledLineItems(List<ComericaLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Simple totalled creates one line item per grouping key, with an associated volume total
			//   (Note: Since we are totalling everything under the grouping level, it doesn't particularly matter what the detail level returned by the DAL is, as long as it can be totalled...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "total" the same "thing", but may have different detail levels and/or grouping levels
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exceptions:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.Transaction }.ToList(), "Stat: Exceptions");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetExceptionCounts);
					break;

				case Analysis.Kind.BulkDownloadZip:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk File Download (ZIP)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadZipCounts);
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for simple total aggregation: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void HandleRetentionTotalsLineItems(List<ComericaLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Retention totals create one line item *per retention partition* per grouping key, with an associated volume total
			//   (Note: Since we are totalling everything under the grouping level, it doesn't particularly matter what the detail level returned by the DAL is, as long as it can be totalled...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "total" the same "thing", but may have different detail levels and/or grouping levels, and will be partitioned into different retention periods
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exists:
					switch (activityInfos[0].VolumeTarget)
					{
						case Analysis.DetailLevel.Item:
							CreateCumulativeTierRetentionCounts(lineItems, activityInfos, null, _config.DAL.GetItemCountsByRetention);
							break;

						default:
							_config.API.LogEvent("Unexpected Target for Retention-based existence counts: " + activityInfos[0].VolumeTarget, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
							break;
					}
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for Retention-based counts: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void CreateLineItems(List<ComericaLineItem> lineItems, Dictionary<GenericGroupingKeys, GenericCounts> data, IList<Analysis.ActivityInfo> activityInfos)
		{
			// TODO: For certain stats (e.g. number of customers) we can't trust the Billing Account on the workgroup...
			// How do we detect and replace that...?

			// Combine aggregated values; they are grouped by workgroup, we need to go one level "higher"
			Dictionary<Tuple<int, long>, GenericCounts> accountData = new Dictionary<Tuple<int, long>, GenericCounts>();
			foreach (var entry in data)
			{
				// Grouping: Bank/Region and Billing Account
				int bank;
				long account;

				// If this is a "customer" level activity, and the (random) associated workgroup did not belong to the customer entity...
				if (activityInfos[0].VolumeTarget == Analysis.DetailLevel.Customer_Entity &&
					entry.Value.EntityID != entry.Value.CustomerEntityID)
				{
					// It looks like the workgroup was configured against a "line of business" entity...
					// The DAL didn't know that, so it may have returned Billing fields from that entity
					// Make another call to the DAL, to get the Billing fields for this customer entity...
					string billingAccount, billingField1, billingField2;
					GetBillingFields(entry.Value.CustomerEntityID, out billingAccount, out billingField1, out billingField2);
					int.TryParse(billingField1, out bank);
					long.TryParse(billingAccount, out account);
				}
				else
				{
					// DAL should have returned best match
					int.TryParse(entry.Value.BillingField1, out bank);
					long.TryParse(entry.Value.BillingAccount, out account);
				}

				var key = Tuple.Create(bank, account);
				Sum(accountData, key, entry.Value);
			}

			// Create the line items
			foreach (var entry in accountData)
			{
				for (int i = 0; i < activityInfos.Count; i++)
				{
					lineItems.Add(new ComericaLineItem()
					{
						Activity = activityInfos[i],
						BankNumber = entry.Key.Item1,
						AccountNumber = entry.Key.Item2,
						Units = entry.Value.Counts[i],
					});
				}
			}
		}

		internal void Sort(ref List<ComericaLineItem> lineItems)
		{
			// TODO: Performance tune this if necessary...
			lineItems = lineItems.OrderBy(o => o.BankNumber).ThenBy(o => o.AccountNumber).ThenBy(o => o.Activity.ActivityCode).ToList();
		}
	}
}
