﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.Comerica
{
	// The "DALBase" class is used to aid faking for unit tests...  But since this DAL contains no private logic right now, we can leave it out...
	//internal abstract class DALBase : SharedDALBase
	//{
	//	protected DALBase(ConfigParameters config) : base(config) { }

	//	// TODO: Manually added statistics...
	//	//public abstract List<GenericValues> GetCustomerLevelSetups();
	//	//public abstract List<GenericValues> GetExtractCounts();
	//}

	internal class Comerica_DAL : SharedDALBase
	{
		public Comerica_DAL(/*ConfigParameters*/ SharedConfigParametersBase config) : base(config)
		{
		}
	}
}
