﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.WFSGP
{
    public class WFSGPGenerator : SharedGeneratorBase<WFSGPLineItem>, ICustomFileGenerator
	{
		public string GetDisplayName()
		{
			return "WFS Great Plains Billing Extract";
		}

		public void PerformScheduledWork(IFileGeneratorAPI api, IEnumerable<BillingBackgroundTaskTypes> scheduledTaskTypes)
		{
			WFSGPBackgroundTasks.CheckForWork(api, scheduledTaskTypes);
		}

		protected override SharedConfigParametersBase CreateConfigParameters()
		{
			return new ConfigParameters();
		}

		private new ConfigParameters _config
		{
			get { return base._config as ConfigParameters; }
		}

		public void GenerateFile(IFileGeneratorAPI api, string xmlParameters, out List<string> errors)
		{
			errors = new List<string>();

			// Assemble parameters
			_config.SetAPI(api);
			_config.SetExtractParameters(xmlParameters);

			// Load LineItem data
			List<WFSGPLineItem> lineItems = LoadLineItems(WFSGPLineItemDefs.ActivityCodeList);

			// Put them in a determinative order
			lineItems = lineItems.OrderBy(o => o.InstitutionID).ThenBy(o => o.Activity.ActivityCode).ToList();

			// Create the output file
			using (var fs = _config.GetNewOutputStream()) 
			{
				/* WFS GP interface uses the following file specification:
					<LineItems>
					  <LineItem>
						...
					  </LineItem>
					  ...
					</LineItems>
				*/
				using (XmlWriter outputFile = XmlWriter.Create(fs, new XmlWriterSettings() { OmitXmlDeclaration = true, Indent = true }))
				{
					outputFile.WriteStartElement("LineItems");

					// Write out line items...
					foreach (var lineItem in lineItems)
					{
						lineItem.ToXml(outputFile);
					}

					outputFile.WriteEndElement();
				}
			}
		}

		protected override void HandleSimpleCountLineItems(List<WFSGPLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Simple counts create one line item per grouping key, with an associated volume count (1 per "X")
			//   (Note: Since the count must be "distinct", it matters what "X" is - but that is handled at the next level...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "count" the same "thing", but may have different grouping levels
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exists:
					activityInfos.ForEach(o => VerifyTargetIn(o, new Analysis.DetailLevel[] { Analysis.DetailLevel.Workgroup, Analysis.DetailLevel.Customer_Entity, Analysis.DetailLevel.FI_Entity },
						"Stat: Distinct Configuration Entries"));

					CreateLineItemsWithCounts(lineItems, activityInfos, _config.DAL.GetCustomers);
					break;

				case Analysis.Kind.Exceptions:
					activityInfos.ForEach(o => VerifyTarget(o, Analysis.DetailLevel.Customer_Entity, "Stat: Exception Enabled Customers"));
					CreateLineItemsWithCounts(lineItems, activityInfos, _config.DAL.GetExceptionsEnabledCustomers);
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for simple count aggregation: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void HandleSimpleTotalledLineItems(List<WFSGPLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Simple totalled creates one line item per grouping key, with an associated volume total
			//   (Note: Since we are totalling everything under the grouping level, it doesn't particularly matter what the detail level returned by the DAL is, as long as it can be totalled...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "total" the same "thing", but may have different detail levels and/or grouping levels
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exceptions:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.Transaction }.ToList(), "Stat: Exceptions");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetExceptionCounts);
					break;

				case Analysis.Kind.BulkDownloadZip:
					VerifyTargets(activityInfos, new Analysis.DetailLevel[] { Analysis.DetailLevel.File, Analysis.DetailLevel.Item }.ToList(), "Stat: Bulk File Download (ZIP)");
					CreateLineItemsWithTotals(lineItems, activityInfos, _config.DAL.GetBulkDownloadZipCounts);
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for simple total aggregation: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void HandleRetentionTotalsLineItems(List<WFSGPLineItem> lineItems, List<Analysis.ActivityInfo> activityInfos)
		{
			// Retention totals create one line item *per retention partition* per grouping key, with an associated volume total
			//   (Note: Since we are totalling everything under the grouping level, it doesn't particularly matter what the detail level returned by the DAL is, as long as it can be totalled...)

			// This method maps the various statistics to the correct DAL data-getter method

			// The list of activityInfos will be to "total" the same "thing", but may have different detail levels and/or grouping levels, and will be partitioned into different retention periods
			switch (activityInfos[0].AnalysisKind)
			{
				case Analysis.Kind.Exists:
					switch (activityInfos[0].VolumeTarget)
					{
						case Analysis.DetailLevel.Item:
							CreateCumulativeTierRetentionCounts(lineItems, activityInfos, null, _config.DAL.GetItemCountsByRetention);
							break;

						default:
							_config.API.LogEvent("Unexpected Target for Retention-based existence counts: " + activityInfos[0].VolumeTarget, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
							break;
					}
					break;

				default:
					_config.API.LogEvent("Unexpected AnalysisKind for Retention-based counts: " + activityInfos[0].AnalysisKind, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
					break;
			}
		}

		protected override void CreateLineItems(List<WFSGPLineItem> lineItems, Dictionary<GenericGroupingKeys, GenericCounts> data, IList<Analysis.ActivityInfo> activityInfos)
		{
			// Combine aggregated values; they are grouped by bank, we need to go one level "higher"
			Dictionary<string, GenericCounts> customerData = new Dictionary<string, GenericCounts>();
			foreach (var entry in data)
			{
				string wfsCustomer = GetWFSCustomerID(entry.Key.SiteBankID);
				Sum(customerData, wfsCustomer, entry.Value);
			}

			// Create the line items
			foreach (var entry in customerData)
			{
				for (int i = 0; i < activityInfos.Count; i++)
				{
					lineItems.Add(new WFSGPLineItem()
					{
						Activity = activityInfos[i],
						ActivityDate = _config.ExtractParams.DateRange.Item2,
						Amount = 0,
						InstitutionID = entry.Key,
						Volume = entry.Value.Counts[i],
					});
				}
			}
		}

		private string GetWFSCustomerID(int siteBankID)
		{
			// Return configured value
			if (_config.WFSIDs.ContainsKey(siteBankID))
				return _config.WFSIDs[siteBankID];

			_config.API.LogEvent("No ID configured for SiteBankID = " + siteBankID.ToString(), "WFSGP", IFileGeneratorAPI_MessageType.Warning, IFileGeneratorAPI_MessageImportance.Essential);

			// Return a placeholder...
			return "ZZ" + (siteBankID % 1000).ToString("000");
		}
	}
}
