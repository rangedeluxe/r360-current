﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.WFSGP
{
	public class WFSGPLineItem
	{
		public DateTime ActivityDate { get; set; }
		public string InstitutionID { get; set; }
		public Analysis.ActivityInfo Activity { get; set; }
		public decimal Amount { get; set; }
		public int Volume { get; set; }

		/* WFS GP interface uses the following file specification:
			<LineItems>
				<LineItem>
					<ActivityDate>20131231</ActivityDate>
					<InstitutionID>Z999</InstitutionID>
					<ProviderID></ProviderID>
					<ActivityCode>5011</ActivityCode>
					<ActivityAmount>1.00</ActivityAmount>
					<ActivityVolume>1</ActivityVolume>
				</LineItem>
				...
			</LineItems>
		*/
		public void ToXml(XmlWriter output)
		{
			output.WriteStartElement("LineItem");
			output.WriteElementString("ActivityDate", ActivityDate.ToString("yyyyMMdd"));
			output.WriteElementString("InstitutionID", InstitutionID);
			output.WriteElementString("ProviderID", "");
			output.WriteElementString("ActivityCode", Activity.ActivityCode.ToString());
			output.WriteElementString("ActivityAmount", Amount.ToString("0.00"));
			output.WriteElementString("ActivityVolume", Volume.ToString());
			output.WriteEndElement();
		}
	}
}
