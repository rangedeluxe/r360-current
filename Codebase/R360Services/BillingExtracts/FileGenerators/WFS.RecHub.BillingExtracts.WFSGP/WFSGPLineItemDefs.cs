﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;
using RVUnit = WFS.RecHub.BillingExtracts.API.Analysis.RetentionValue.Unit;
using AggType = WFS.RecHub.BillingExtracts.API.Analysis.AggregationType;

namespace WFS.RecHub.BillingExtracts.WFSGP
{
	internal static class WFSGPLineItemDefs
	{
		/// <summary>
		/// This is the list of defined Activity Codes for this file format.
		/// </summary>
		public static readonly Analysis.ActivityInfo[] ActivityCodeList;
		static WFSGPLineItemDefs()
		{
			// Build Activity Code list
			List<Analysis.ActivityInfo> list = new List<Analysis.ActivityInfo>();
			Add(list, new Analysis.ActivityInfo()
			{
				ActivityCode = 5220,
				Description = "Maintenance Fee Per Customer",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Bank, // Really WFS Customer - but that is determined by Bank
				VolumeTarget = Analysis.DetailLevel.Customer_Entity,
				Aggregation = AggType.CountDistinct,
			});
			Add(list, new Analysis.ActivityInfo() 
			{
				ActivityCode = 5226,
				Description = "Fee Per Item Imported / Stored",
				AnalysisKind = Analysis.Kind.Exists,
				Grouping = Analysis.GroupingLevel.Bank, // Really WFS Customer - but that is determined by Bank
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.TotalByRetention,
				Retention =								RV(13, RVUnit.Months), },
					RVC(	Code: 5227,		Retention:  RV( 3, RVUnit.Years) ),
					RVC(	Code: 5228,		Retention:  RV( 7, RVUnit.Years) )
			);
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 5240,
				Description = "Fee Per Customer (Exceptions)",
				AnalysisKind = Analysis.Kind.Exceptions,
				Grouping = Analysis.GroupingLevel.Bank, // Really WFS Customer - but that is determined by Bank
				VolumeTarget = Analysis.DetailLevel.Customer_Entity,
				Aggregation = AggType.CountDistinct,
			});
			list.Add(new Analysis.ActivityInfo()	
			{
				ActivityCode = 5241,
				Description = "Fee Per Transaction (Exceptions)",
				AnalysisKind = Analysis.Kind.Exceptions,
				Grouping = Analysis.GroupingLevel.Bank, // Really WFS Customer - but that is determined by Bank
				VolumeTarget = Analysis.DetailLevel.Transaction,
				Aggregation = AggType.Total,
			});
			//list.Add(new Analysis.ActivityInfo()		Not supporting this code for 2.01
			//{
			//	ActivityCode = 5230,
			//	Description = "Fee Per Workgroup for Early (pre-EOD) Image Viewing",
			//	AnalysisKind = Analysis.Kind.EarlySameDayViewings,
			//	VolumeTarget = Analysis.DetailLevel.Workgroup,
			//	Aggregation = AggType.Count,
			//});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 5233,
				Description = "Fee Per ZIP File downloaded",
				AnalysisKind = Analysis.Kind.BulkDownloadZip,
				Grouping = Analysis.GroupingLevel.Bank, // Really WFS Customer - but that is determined by Bank
				VolumeTarget = Analysis.DetailLevel.File,
				Aggregation = AggType.Total,
			});
			list.Add(new Analysis.ActivityInfo()
			{
				ActivityCode = 5234,
				Description = "Fee Per TIFF downloaded in ZIP file",
				AnalysisKind = Analysis.Kind.BulkDownloadZip,
				Grouping = Analysis.GroupingLevel.Bank, // Really WFS Customer - but that is determined by Bank
				VolumeTarget = Analysis.DetailLevel.Item,
				Aggregation = AggType.Total,
			});
		
			ActivityCodeList = list.ToArray();
		}

		/// <summary>
		/// This method adds an activity to the list, and if applicable creates duplicate activities that differ only in the analysis code and retention value
		/// </summary>
		/// <param name="firstActivity">The first (fully specified) activity</param>
		/// <param name="additionalRetentionLevels">A list of additional retention levels, with the associated analysis code</param>
		/// <returns></returns>
		private static void Add(List<Analysis.ActivityInfo> list, Analysis.ActivityInfo activity, params Tuple<int, Analysis.RetentionValue>[] additionalRetentionLevels)
		{
			// Keep the first activity
			list.Add(activity);

			// Create copies for additional retention levels
			foreach (var level in additionalRetentionLevels)
			{
				list.Add(new Analysis.ActivityInfo()
					{
						ActivityCode = level.Item1,
						Aggregation = activity.Aggregation,
						AnalysisKind = activity.AnalysisKind,
						Description = activity.Description,
						Retention = level.Item2,
						Grouping = activity.Grouping,
						VolumeTarget = activity.VolumeTarget,
					});
			}
		}

		/// <summary>
		/// This is just an abbreviation for the RetentionValue constructor
		/// </summary>
		private static Analysis.RetentionValue RV(int value, RVUnit unit)
		{
			return new Analysis.RetentionValue(value, unit);
		}

		/// <summary>
		/// This is an abbreviation for creating an AnalysisCode / Retention Tuple with labeled parameters
		/// </summary>
		/// <param name="Code"></param>
		/// <param name="Retention"></param>
		/// <returns></returns>
		private static Tuple<int, Analysis.RetentionValue> RVC(int Code, Analysis.RetentionValue Retention)
		{
			return Tuple.Create(Code, Retention);
		}
	}
}
