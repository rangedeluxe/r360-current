﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.WFSGP
{
	internal class ConfigParameters : SharedConfigParametersBase
	{
		public ConfigParameters() : base("WFSGP") { }

		protected override void LoadConfiguration()
		{
			// Use a well-known configuration file name matching the assembly name
			string configFileName = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath + ".config";
			XDocument configDoc = XDocument.Load(configFileName);

			/*
			 <>
				<OutputFolder>...</OutputFolder>
				<OutputFileNamePattern>...</OutputFileNamePattern>
			 </>
			*/

			OutputFolder = API.GetRequiredString(configDoc, "OutputFolder", configFileName, ModuleName);
			OutputFileNamePattern = API.GetRequiredString(configDoc, "OutputFileNamePattern", configFileName, ModuleName);

			// Load BankID-to-WFSID mapping
			_WFSIDs.Clear();
			string ids = API.GetOptionalString(configDoc, "IDs", "", ModuleName);
			if (!string.IsNullOrWhiteSpace(ids))
			{
				string[] list = ids.Split(';');
				foreach (var entry in list)
				{
					string[] components = entry.Split('=');
					if (components.Length == 2)
					{
						int bankId;
						if (int.TryParse(components[0], out bankId))
						{
							_WFSIDs[bankId] = components[1];
							continue;
						}
					}

					API.LogEvent(string.Format("Unrecongized Format for IDs: '{0}'", entry), ModuleName, IFileGeneratorAPI_MessageType.Warning, IFileGeneratorAPI_MessageImportance.Essential);
				}
			}
		}

		//public new DALBase DAL
		//{
		//	get
		//	{
		//		return base.DAL as DALBase;
		//	}
		//}

		public override SharedDALBase CreateDAL()
		{
			return new WFSGP_DAL(this);
		}

		private readonly Dictionary<int, string> _WFSIDs = new Dictionary<int,string>();
		public IReadOnlyDictionary<int, string> WFSIDs
		{
			get { return _WFSIDs; }
		}

		public override void SetExtractParameters(string xmlParameters)
		{
			ExtractParams = new ExtractParameters(this, xmlParameters);
		}
	}
}
