﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.WFSGP
{
	internal static class WFSGPBackgroundTasks
	{
		public static void CheckForWork(IFileGeneratorAPI api, IEnumerable<BillingBackgroundTaskTypes> scheduledTasks)
		{
			api.LogEvent("Checking for scheduled tasks...", typeof(WFSGPBackgroundTasks).Name, IFileGeneratorAPI_MessageType.Information, IFileGeneratorAPI_MessageImportance.Debug);

			// No work is defined specific to the WFS GP format at this time
			// Note: WFS GP *is* dependent on several standard scheduled tasks
			BillingBackgroundTaskTypes[] requiredTasks = new BillingBackgroundTaskTypes[] 
			{
				BillingBackgroundTaskTypes.SummarizePaymentFacts,
				BillingBackgroundTaskTypes.SummarizeStubFacts,
				BillingBackgroundTaskTypes.SummarizeDocumentFacts,
				BillingBackgroundTaskTypes.SummarizeActiveWorkgroups,
				BillingBackgroundTaskTypes.SummarizeBulkFileRecordCounts_ZipDownload,
				BillingBackgroundTaskTypes.SummarizeExceptionsEnabledCustomers,
				BillingBackgroundTaskTypes.SummarizeExceptionCounts,
			};

			foreach (var task in requiredTasks)
			{
				if (!scheduledTasks.Contains(task))
				{
					api.LogEvent("Configuration Error: Billing Schedule Manager must be configured to run task of type '" + task.ToString() + "'", typeof(WFSGPBackgroundTasks).Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
				}
			}
		}
	}
}
