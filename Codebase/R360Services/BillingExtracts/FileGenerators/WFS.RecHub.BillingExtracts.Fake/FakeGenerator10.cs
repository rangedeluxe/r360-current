﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.Fake
{
	public class FakeGenerator10 : ICustomFileGenerator
    {
		public string GetDisplayName()
		{
			return "Fake Format 10";
		}

		public void GenerateFile(IFileGeneratorAPI api, string xmlParameters, out List<string> errors)
		{
			throw new NotImplementedException();
		}

		public void PerformScheduledWork(IFileGeneratorAPI api, IEnumerable<BillingBackgroundTaskTypes> scheduledTasks)
		{
		}
	}
}
