﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BillingExtracts.DAL
{
	public static class BillingDALFactory
	{
		public static IBillingDAL Create(string siteKey)
		{
			// TODO: Support Mocking
			return new BillingDAL(siteKey);
		}
	}
}
