﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.DAL;

namespace WFS.RecHub.BillingExtracts.DAL
{
    internal class BillingDAL : _DALBase, IBillingDAL
    {
		internal const string ISO8601DATEFORMAT_SQL127 = "yyyy-MM-ddTHH:mm:ss.fffZ";
		public BillingDAL(string vSiteKey) : base(vSiteKey, ConnectionType.RecHubBilling)
		{
		}

		public bool GetUserIDBySID(Guid SID, out DataTable results)
		{
			bool bReturnValue = false;

			results = null;
			SqlParameter[] parms;
			try
			{
				parms = new SqlParameter[] {
                    BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input)
                };
				bReturnValue = Database.executeProcedure("RecHubUser.usp_Users_UserID_Get_BySID",
						parms, out results);
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "GetUserIDBySID(Guid SID, out DataTable results)");
				bReturnValue = false;
			}

			return bReturnValue;
		}

		public bool ExecuteBillingProc(string procName, IEnumerable<SqlParameter> parameters, out DataTable results)
		{
			bool bReturnValue = false;

			results = null;
			try
			{
				if (parameters == null)
					bReturnValue = Database.executeProcedure("RecHubBilling." + procName, out results);
				else
					bReturnValue = Database.executeProcedure("RecHubBilling." + procName, parameters.ToArray(), out results);
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, procName);
				bReturnValue = false;
			}

			return bReturnValue;
		}

		public bool GetLastRunTime(string taskDescription, out DateTime? utcRunTime)
		{
			const string procName = "usp_Billing_ScheduledTaskRunTimes_GetByDescription";
			bool bReturnValue = false;
			utcRunTime = null;

			try
			{
				DataTable results;
				List<SqlParameter> parameters = new List<SqlParameter>();
				parameters.Add(BuildParameter("@parmTaskDescription", SqlDbType.VarChar, 50, taskDescription));

				bReturnValue = Database.executeProcedure("RecHubBilling." + procName, parameters.ToArray(), out results);
				if (bReturnValue)
				{
					if (results.Rows.Count > 0)
					{
						utcRunTime = new DateTime(((DateTime)results.Rows[0][0]).Ticks, DateTimeKind.Utc);	// Return time explicitly marked as UTC
					}
				}
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, procName);
				bReturnValue = false;
			}

			return bReturnValue;
		}

		public bool BeginTask(out bool execute, string taskDescription)
		{
			bool bReturnValue = false;

			try
			{
				List<SqlParameter> parameters = new List<SqlParameter>();
				parameters.Add(BuildParameter("@parmTaskDescription", SqlDbType.VarChar, 50, taskDescription));
				var parmExecuted = BuildParameter("@parmExecuted", SqlDbType.Bit, null, ParameterDirection.Output);
				parameters.Add(parmExecuted);

				bReturnValue = Database.executeProcedure("RecHubBilling.usp_Billing_ScheduledTaskLocks_Aquire", parameters.ToArray());
				if (bReturnValue)
					execute = (bool)parmExecuted.Value;
				else
					execute = false;
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "usp_Billing_ScheduledTaskLocks_Aquire");
				bReturnValue = false;
				execute = false;
			}

			return bReturnValue;
		}

		public bool UnlockTask(string taskDescription)
		{
			bool bReturnValue = false;

			try
			{
				List<SqlParameter> parameters = new List<SqlParameter>();
				parameters.Add(BuildParameter("@parmTaskDescription", SqlDbType.VarChar, 50, taskDescription));

				bReturnValue = Database.executeProcedure("RecHubBilling.usp_Billing_ScheduledTaskLocks_Unlock", parameters.ToArray());
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "usp_Billing_ScheduledTaskLocks_Unlock");
				bReturnValue = false;
			}

			return bReturnValue;
		}

		public bool SummarizeFacts(out bool executed, string taskId, string taskDescription, DateTime targetDate, int? recheckDays = null)
		{
			bool bReturnValue = false;

			try
			{
				List<SqlParameter> parameters = new List<SqlParameter>();
				parameters.Add(BuildParameter("@parmTaskDescription", SqlDbType.VarChar, 50, taskDescription));
				parameters.Add(BuildParameter("@parmTaskId", SqlDbType.VarChar, 50, taskId));
				parameters.Add(BuildParameter("@parmTargetDateKey", SqlDbType.Int, int.Parse(targetDate.ToString("yyyyMMdd")), ParameterDirection.Input));
				if (recheckDays.HasValue)
					parameters.Add(BuildParameter("@parmRecheckDays", SqlDbType.Int, recheckDays.Value, ParameterDirection.Input));
				var parmExecuted = BuildParameter("@parmExecuted", SqlDbType.Bit, null, ParameterDirection.Output);
				parameters.Add(parmExecuted);

				bReturnValue = Database.executeProcedure("RecHubBilling.usp_Billing_ExecuteScheduledTask", parameters.ToArray());
				if (bReturnValue)
					executed = (bool)parmExecuted.Value;
				else
					executed = false;
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "usp_Billing_ExecuteScheduledTask");
				bReturnValue = false;
				executed = false;
			}

			return bReturnValue;
		}

		public bool StoreExceptionCustomers(string taskDescription, IEnumerable<int> customerEntities)
		{
			bool bReturnValue = false;

			try
			{
				List<SqlParameter> parameters = new List<SqlParameter>();
				parameters.Add(BuildParameter("@parmTaskDescription", SqlDbType.VarChar, 50, taskDescription));

				StringBuilder sb = new StringBuilder();
				XmlWriter xw = XmlWriter.Create(sb, new XmlWriterSettings { OmitXmlDeclaration = true });
				xw.WriteStartElement("list");
				foreach (var entity in customerEntities)
				{
					xw.WriteStartElement("c");
					xw.WriteAttributeString("id", entity.ToString());
					xw.WriteEndElement();
				}
				xw.WriteEndElement();
				xw.Close();
				parameters.Add(BuildParameter("@parmEntities", SqlDbType.Xml, sb.ToString(), ParameterDirection.Input));

				bReturnValue = Database.executeProcedure("RecHubBilling.usp_Billing_StoreExceptionCustomers", parameters.ToArray());
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "usp_Billing_StoreExceptionCustomers");
				bReturnValue = false;
			}

			return bReturnValue;
		}
    }
}
