﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BillingExtracts.DAL
{
	public interface IBillingDAL
	{
		bool GetUserIDBySID(Guid SID, out DataTable results);
		bool ExecuteBillingProc(string procName, IEnumerable<SqlParameter> parameters, out DataTable results);

		bool GetLastRunTime(string taskDescription, out DateTime? utcRunTime);
		bool BeginTask(out bool execute, string taskDescription);
		bool UnlockTask(string taskDescription);
		bool SummarizeFacts(out bool executed, string taskId, string taskDescription, DateTime targetDate, int? recheckDays = null);
		bool StoreExceptionCustomers(string taskDescription, IEnumerable<int> customerEntities);
	}
}
