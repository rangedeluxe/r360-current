﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WFS.LTA.Common;
using WFS.RecHub.BillingExtracts.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.BillingExtracts.ServicesClient
{
	internal class BillingExtractsConnect : _ServicesClientBase, IBillingExtracts
	{
		private readonly IBillingExtracts _BillingExtractsService = null;

		public BillingExtractsConnect(string vSiteKey, ltaLog log)
			: base(vSiteKey, log)
		{
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
			_BillingExtractsService = R360ServiceFactory.Create<IBillingExtracts>();
		}

		private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
		{
			// All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
			try
			{
				using (new OperationContextScope((IContextChannel)_BillingExtractsService))
				{
					// Set context inside of operation scope
					R360ServiceContext.Init(this.SiteKey);

					return operation();
				}
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw;
			}
		}

		public PingResponse Ping()
		{
			return Do(_BillingExtractsService.Ping);
		}

		public BillingFormatsResponse GetBillingFormats()
		{
			return Do(_BillingExtractsService.GetBillingFormats);
		}

		public GenerateFileResponse GenerateFile(string xmlParameters)
		{
			return Do(() => _BillingExtractsService.GenerateFile(xmlParameters));
		}
	}
}
