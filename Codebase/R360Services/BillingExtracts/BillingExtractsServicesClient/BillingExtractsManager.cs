﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.LTA.Common;
using WFS.RecHub.BillingExtracts.Common;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.BillingExtracts.ServicesClient
{
	public class BillingExtractsManager : IBillingExtracts
	{
		#region Construction / Initialization

		// Initialization parameters
		private const string LogSource = "BillingExtrctMngr";
		private static ltaLog _eventLog;
		private static string _siteKey;
		private static cSiteOptions _SiteOptions = null;
		private readonly BillingExtractsConnect _connection;

		/// <summary>
		/// Uses SiteKey to retrieve site options from the local .ini file.
		/// </summary>
		protected static cSiteOptions SiteOptions
		{
			get
			{
				if (_SiteOptions == null)
				{
					_SiteOptions = new cSiteOptions(_siteKey);
				}
				return _SiteOptions;
			}
		}

		/// <summary>
		/// Logging component using settings from the local siteOptions object.
		/// </summary>
		protected static ltaLog EventLog
		{
			get
			{
				if (_eventLog == null)
				{
					_eventLog = new ltaLog(SiteOptions.logFilePath,
											  SiteOptions.logFileMaxSize,
											  (LTAMessageImportance)SiteOptions.loggingDepth);
				}

				return _eventLog;
			}
		}

		protected static string SiteKey
		{
			get
			{
				return (_siteKey);
			}
			set
			{
				_siteKey = value;
			}
		}

		static BillingExtractsManager()
		{
			// Make sure the site key is initialized before any other properties are used
			GetSiteKey();
		}

		public BillingExtractsManager()
		{
			// Create connection context
			try
			{
				_connection = new BillingExtractsConnect(_siteKey, _eventLog);
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, LogSource, ".ctor");

				// Consructors should not throw exceptions -- just initialize to null and report errors later...
				_connection = null;
			}
		}

		// Specialied method for site key that properly initializes the EventLog after the site key has been set
		private static void GetSiteKey()
		{
			string defaultValue = "IPOnline";
			try
			{
				string value = ConfigurationManager.AppSettings["siteKey"];
				if (string.IsNullOrEmpty(value))
					value = defaultValue;

				_siteKey = value;

				EventLog.logEvent(string.Format("Using SiteKey = '{0}'", value), LogSource, LTA.Common.LTAMessageImportance.Verbose);
			}
			catch (Exception ex)
			{
				_siteKey = defaultValue;

				EventLog.logWarning(string.Format("Error reading configuration setting 'siteKey' - {0}", ex.Message), LogSource, LTA.Common.LTAMessageImportance.Verbose);
			}
		}

		private void ValidateConnection()
		{
			if (_connection == null)
				throw new Exception("Configuration Error - check log for details");
		}

		#endregion

		public PingResponse Ping()
		{
			ValidateConnection();
			return _connection.Ping();
		}

		public BillingFormatsResponse GetBillingFormats()
		{
			ValidateConnection();
			return _connection.GetBillingFormats();
		}

		public GenerateFileResponse GenerateFile(string xmlParameters)
		{
			ValidateConnection();
			return _connection.GenerateFile(xmlParameters);
		}

		public GenerateFileResponse GenerateFile(string xmlParameters, string nameValueParameters)
		{
			ValidateConnection();

			// Compose parameters into one XML string
			if (!string.IsNullOrWhiteSpace(nameValueParameters))
			{
				// Compose XML parameters...
				XDocument xDoc = XDocument.Load(new StringReader(xmlParameters));
				var xRoot = xDoc.Root;
				var attributes = xDoc.Root.Attributes();
				var newAttributes = ParseNameValue(nameValueParameters);
				foreach (var entry in newAttributes)
				{
					if (!attributes.Any(x => x.Name == entry.Key))
					{
						xRoot.SetAttributeValue(entry.Key, entry.Value);
					}
				}
				xmlParameters = xDoc.ToString();

				// No errors are logged on parsing errors - so log final values...
				EventLog.logEvent("Generating for: " + xmlParameters, LogSource, LTAMessageType.Information, LTAMessageImportance.Verbose);
			}

			return _connection.GenerateFile(xmlParameters);
		}

		private Dictionary<string, string> ParseNameValue(string args)
		{
			Dictionary<string, string> argTable = new Dictionary<string, string>();

			// Name="Value" Name="Value"
			int startIndex = 0;

			while (true)
			{
				// Find =
				int index = args.IndexOf('=', startIndex);
				if (index > startIndex && index < args.Length - 2)
				{
					// Verify Quote
					char quote = args[index + 1];
					if (quote == '"' || quote == '\'')
					{
						// Find end quote
						int endIndex = args.IndexOf(quote, index + 2);
						if (endIndex > index)
						{
							// Save values
							argTable[args.Substring(startIndex, index - startIndex).Trim()] = args.Substring(index + 2, endIndex - index - 2);
							startIndex = endIndex + 1;
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}
				}
				else
				{
					break;
				}
			}

			return argTable;
		}
	}
}
