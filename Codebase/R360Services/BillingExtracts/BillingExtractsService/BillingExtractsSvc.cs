﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFS.RecHub.BillingExtracts.API;
using WFS.RecHub.BillingExtracts.Common;
using WFS.RecHub.R360Shared;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Ryan Schwantes
* Date: 2/4/2014
*
* Purpose: Billing Extracts WCF Interface
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.BillingExtracts
{
	public class BillingExtractsSvc : LTAService, IBillingExtracts
	{
		private static readonly BillingExtractsSvc _anonymousInstance;
		static BillingExtractsSvc()
		{
			// Generate an instance of this service that is always "running";
			_anonymousInstance = new BillingExtractsSvc();
			
			// Perform startup initialization
			BillingAPI.Init(_anonymousInstance.EventLog.logEvent);
		}

		public BillingExtractsSvc()
		{
		}

		protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
		{
			return new BillingAPI(serviceContext);
		}

		// Ping
		public PingResponse Ping()
		{
            return FailOnError((oapi) =>
			{
				var api = (BillingAPI)oapi;
				api.Ping(OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString());
                PingResponse response = new PingResponse();
				response.responseString = "Pong";
				return response;
			});
		}

		public BillingFormatsResponse GetBillingFormats()
		{
			return FailOnError((oapi) =>
			{
				var api = (BillingAPI)oapi;
				BillingFormatsResponse response = new BillingFormatsResponse();
				api.GetBillingFormats(response);
				return response;
			});
		}

		public GenerateFileResponse GenerateFile(string xmlParameters)
		{
			return FailOnError((oapi) =>
			{
				var api = (BillingAPI)oapi;
				GenerateFileResponse response = new GenerateFileResponse();
				api.GenerateFile(xmlParameters, response);
				return response;
			});
		}
	}
}
