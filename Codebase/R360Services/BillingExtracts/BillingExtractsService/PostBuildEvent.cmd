REM TargetPath = %1 
REM TargetDir = %2
REM ProjectDir = %3
REM OutDir = %4

if %4=="bin\Debug\." goto CreateDebuggingConfig
goto End

:CreateDebuggingConfig
echo Info: Setting Debugging Configuration
copy %1.config %2\GenericWcfServiceHost.exe.config
copy %3\..\..\ServiceShares\GenericWcfServiceHost\%4\GenericWcfServiceHost.exe %2
%3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\GenericWcfServiceHost.exe.config /replace:D:\WFSApps\RecHub\bin2\RecHub.ini;;;.\BillingExtractsService.RecHub.ini /replace:"key=""RerunAllScheduledTasksOnStartup"" value=""false""/>;;;key=""RerunAllScheduledTasksOnStartup"" value=""true""/>"

cd %2
IF EXIST "CustomFileGenerators" GOTO HasCustomDir
md "CustomFileGenerators"

:HasCustomDir
copy %2\StandardScheduledTasks.config %2\CustomFileGenerators\
%3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\CustomFileGenerators\StandardScheduledTasks.config /replace:"<!--BEGIN Bulk File Activitites;;;<!--BEGIN Bulk File Activitites-->" /replace:"END Bulk File Activities-->;;;<!--END Bulk File Activities-->"

:End
if not exist %3\PostBuildEventCustom.cmd goto End2
%3\PostBuildEventCustom.cmd %1 %2 %3 %4

:End2
echo Info: Post Build Event Complete