﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BillingExtracts.API
{
	public abstract class SharedConfigParametersBase
	{
		protected readonly string ModuleName;

		protected SharedConfigParametersBase(string moduleName)
		{
			ModuleName = moduleName;
		}

		private IFileGeneratorAPI _api;
		public IFileGeneratorAPI API
		{
			get
			{
				if (_api == null)
					throw new Exception("Action attempted without first setting the Billing API instance");
				return _api;
			}
		}

		public void SetAPI(IFileGeneratorAPI api)
		{
			_api = api;

			LoadConfiguration();
		}

		protected abstract void LoadConfiguration();

		private string _outputFolder;
		public string OutputFolder
		{
			get
			{
				if (_outputFolder == null)
					throw new Exception("Invalid Configuration");
				return _outputFolder;
			}
			protected set
			{
				_outputFolder = value;
			}
		}

		private string _outputFileNamePattern;
		public string OutputFileNamePattern
		{
			get
			{
				if (_outputFileNamePattern == null)
					throw new Exception("Invalid Configuration");
				return _outputFileNamePattern;
			}
			protected set
			{
				_outputFileNamePattern = value;
			}
		}

		public Stream MockOutputStream { get; set; }
		public Stream GetNewOutputStream()
		{
			if (MockOutputStream != null)
				return MockOutputStream;
			else
			{
				// Format filename, including parameters such as the date range
				string filename = string.Format(OutputFileNamePattern,
					ExtractParams.DateRange.Item1, ExtractParams.DateRange.Item2, DateTime.Now, ExtractParams.TargetEntityDesc);

				return OpenFile(filename);

				// Note: The caller is responsible for disposing this FileStream...
			}
		}

		protected Stream OpenFile(string filename)
		{
			string fullPath = Path.Combine(OutputFolder, filename);

			// Open with exclusive access, so no one can read an incomplete file
			return new FileStream(fullPath, FileMode.Create, FileAccess.Write, FileShare.None);
		}

		private SharedExtractParametersBase _extractParams;
		public SharedExtractParametersBase ExtractParams
		{
			get { return _extractParams; }
			protected set { _extractParams = value; }
		}

		public abstract void SetExtractParameters(string xmlParameters);

		public SharedDALBase MockDAL { get; set; }
		private SharedDALBase _dal;
		public SharedDALBase DAL
		{
			get
			{
				if (MockDAL != null)
					return MockDAL;

				if (_dal == null)
					_dal = CreateDAL();

				return _dal;
			}
		}

		public abstract SharedDALBase CreateDAL();
	}
}
