﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BillingExtracts.API
{
	public static class Analysis
	{
		/// <summary>
		/// This enumeration defines how to aggregate results during analysis
		/// </summary>
		public enum AggregationType
		{
			/// <summary>
			/// None means there is no aggregation; typically used when the count target is the highest level (e.g. institution)
			/// </summary>
			None = 0,

			/// <summary>
			/// CountDistinct means to simply count the number of (distinct) target instances, and group by the highest level (e.g. institution)
			/// </summary>
			CountDistinct,

			/// <summary>
			/// Total is used when the volume target is already numeric and needs to be totalled; it could be a Count - but we may be summing pre-aggregated counts
			/// </summary>
			Total,

			/// <summary>
			/// TotalByRetention is similar to Total, but there is a secondary grouping on the retention levels
			/// </summary>
			TotalByRetention,
		}

		/// <summary>
		/// This enumeration defines what activity is being analyzed
		/// </summary>
		public enum Kind
		{
			// One Time Fees
			Setup,

			// Recurring Fees (Monthly)
			Exists,
			//ExceptionsEnabled,
			//Exception_Rejects,
			//DIT_Import,
			Exceptions,
			//EarlySameDayViewings,
			BulkDownloadZip,

			BulkDownloadPrintView,
			BulkDownloadPdfView,
			BulkDownloadCsv1,
			BulkDownloadCsv2,
			BulkDownloadXml,
			BulkDownloadHtml,
		}

		/// <summary>
		/// This enumeration defines "what is being counted"; it is the "detail" level of the analysis
		/// </summary>
		public enum DetailLevel
		{
			FI_Entity,
			Customer_Entity,
			Workgroup,
			File,
			Batch,
			Transaction,
			Item,
			PaymentItem,
			NonPaymentItem,

			/// <summary>
			/// Event means that we are targeting occurrences of a specific type of audited event (type based on analysis kind)
			/// </summary>
			Event,
		}

		/// <summary>
		/// This enumeration defines the grouping level - output should be one row per unique grouping key
		/// </summary>
		public enum GroupingLevel
		{
			NotSet = 0,
			FI_Entity = 1,
			Bank = 2,
			Customer_Entity = 3,
			Workgroup = 4,
		}

		public class RetentionValue : IComparable<RetentionValue>, IComparable
		{
			public enum Unit
			{
				None = 0,
				Days = 1,
				Months = 2,
				Years = 3,
			}

			public int Value { get; private set; }
			public Unit Units { get; private set; }

			public int ApproximateDays
			{
				get
				{
					// This is approximate...
					//	3 months = 90 days
					//	4 months = 120 days
					//	12 months != 1 year -- but 1 year < 13 months
					//	1 year = 366 days
					switch (Units)
					{
						case Unit.Months:
							return Value * 30;

						case Unit.Years:
							return Value * 366;

						default:
							return Value;
					}
				}
			}
			public RetentionValue(int value, string unit)
			{
				Value = value;
				Units = (Unit)Enum.Parse(typeof(Unit), unit, true);
			}
			public RetentionValue(int value, Unit unit)
			{
				Value = value;
				Units = unit;
			}

			public override bool Equals(object obj)
			{
				// Perform value equality
				var o2 = obj as RetentionValue;
				if (o2 != null)
					return (Value == o2.Value && Units == o2.Units);

				return base.Equals(obj);
			}

			public override int GetHashCode()
			{
				return (((int)Units) << 28) ^ Value;
			}

			public int CompareTo(RetentionValue other)
			{
				if (other == null) return 1;
				return ApproximateDays.CompareTo(other.ApproximateDays);
			}

			public int CompareTo(object obj)
			{
				return CompareTo((RetentionValue)obj);
			}
		}

		public class ActivityInfo
		{
			public string Description { get; set; }
			public int ActivityCode { get; set; }
			public Kind AnalysisKind { get; set; }
			public GroupingLevel Grouping { get; set; }
			public DetailLevel VolumeTarget { get; set; }
			public AggregationType Aggregation { get; set; }
			public RetentionValue Retention { get; set; }
		}
	}
}
