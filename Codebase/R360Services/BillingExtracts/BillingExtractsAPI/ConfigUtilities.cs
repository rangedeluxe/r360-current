﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WFS.RecHub.BillingExtracts.API
{
	public static class ConfigUtilities
	{
		public static string GetOptionalString(XElement xmlElement, string attribute, string defaultValue)
		{
			// Check for parameters in the XML
			var attrib = xmlElement.Attribute(attribute);
			if (attrib != null)
				return attrib.Value;

			// If not found, return default
			return defaultValue;
		}

		public static Tuple<DateTime, DateTime> GetOptionalDates(XElement xmlElement, string date1, string date2, Func<Tuple<DateTime, DateTime>> defaultProvider)
		{
			// Check for parameters in the XML
			var date1Attrib = xmlElement.Attribute(date1);
			if (date1Attrib != null)
			{
				var date2Attrib = xmlElement.Attribute(date2);
				if (date2Attrib != null)
				{
					DateTime dtDate1, dtDate2;
					if (DateTime.TryParse(date1Attrib.Value, out dtDate1) || DateTime.TryParseExact(date1Attrib.Value, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out dtDate1))
						if (DateTime.TryParse(date2Attrib.Value, out dtDate2) || DateTime.TryParseExact(date2Attrib.Value, "yyyyMMdd", null, System.Globalization.DateTimeStyles.None, out dtDate2))
							return Tuple.Create(dtDate1, dtDate2);
				}
			}

			// If anything went wrong getting the values from the XML, just return the default values
			return defaultProvider();
		}

		public static Tuple<DateTime, DateTime> GetLastMonth()
		{
			DateTime startDate = DateTime.Today.AddMonths(-1); // Date one month ago
			startDate = startDate.AddDays(1 - startDate.Day); // First of the month
			DateTime endDate = startDate.AddMonths(1).AddDays(-1); // Last of the month
			return Tuple.Create(startDate, endDate);
		}

		public static string GetRequiredString(XDocument doc, string elementName, string configFileName, string moduleName, IFileGeneratorAPI api)
		{
			var elem = doc.Root.Element(elementName);
			if (elem == null)
			{
				api.LogEvent("Required configuration parameter '" + elementName + "' was not found in configuration file: " + configFileName, moduleName,
					IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
				return null;
			}

			return elem.Value.Trim();
		}

		public static string GetOptionalString(XDocument doc, string elementName, string defaultValue, string moduleName, IFileGeneratorAPI api)
		{
			string result = defaultValue;
			var elem = doc.Root.Element(elementName);
			if (elem != null)
			{
				result = elem.Value;
			}
			if (result != defaultValue)
			{
				api.LogEvent("Configuration parameter '" + elementName + "' has been set to: " + result, moduleName,
					IFileGeneratorAPI_MessageType.Information, IFileGeneratorAPI_MessageImportance.Verbose);
			}
			return result;
		}

		public static bool GetOptionalBoolean(XDocument doc, string elementName, bool defaultValue, string moduleName, IFileGeneratorAPI api)
		{
			bool result = defaultValue;
			var elem = doc.Root.Element(elementName);
			if (elem != null)
			{
				string value = elem.Value.Trim();
				if (value.Length > 0)
				{
					if (!bool.TryParse(value, out result))
					{
						result = defaultValue;
						switch (value[0])
						{
							case 'y':
							case 'Y':
							case 't':
							case 'T':
								result = true;
								break;
							case 'n':
							case 'N':
							case 'f':
							case 'F':
								result = false;
								break;
						}
					}
				}
			}
			if (result != defaultValue)
			{
				api.LogEvent("Configuration parameter '" + elementName + "' has been set to: " + result.ToString(), moduleName,
					IFileGeneratorAPI_MessageType.Information, IFileGeneratorAPI_MessageImportance.Verbose);
			}
			return result;
		}
	}
}
