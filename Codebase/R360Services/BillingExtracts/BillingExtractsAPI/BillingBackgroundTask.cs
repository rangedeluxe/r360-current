﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.RecHub.Common;

namespace WFS.RecHub.BillingExtracts.API
{
	public enum BillingBackgroundTaskTypes
	{
		TestWithDelay,

		SummarizePaymentFacts,
		SummarizeStubFacts,
		SummarizeDocumentFacts,
		
		SummarizeActiveWorkgroups,
		SummarizeExceptionsEnabledCustomers,

		SummarizeBulkFileRecordCounts_PrintView,
		SummarizeBulkFileRecordCounts_PdfView,
		SummarizeBulkFileRecordCounts_Csv,
		SummarizeBulkFileRecordCounts_ZipDownload,
		SummarizeBulkFileRecordCounts_DownloadCsv,
		SummarizeBulkFileRecordCounts_DownloadXml,
		SummarizeBulkFileRecordCounts_DownloadHtml,

		SummarizeExceptionCounts,
	}

	internal class BillingBackgroundTask
	{
		#region Properties / Initialization

		// <... Description="..." Type="SummarizePaymentFacts" TargetDay="-3" RunDaysOfWeek="2,3,4,5,6" RunTimeOfDay="23:30" />
		public static BillingBackgroundTask FromXML(XElement taskConfig)
		{
			BillingBackgroundTask task = new BillingBackgroundTask()
			{
				TaskType = GetRequiredEnum<BillingBackgroundTaskTypes>(taskConfig, "Type"),
			};

			task.Description = GetOptionalString(taskConfig, "Description", task.TaskType.ToString());
			task.TargetDay = GetOptionalInt(taskConfig, "TargetDay", -1);

			task.RunDays = ConvertDaysOfWeek(GetOptionalString(taskConfig, "RunDaysOfWeek", "*"));
			task.RunTimeOfDay = GetOptionalTimeSpan(taskConfig, "RunTimeOfDay", new TimeSpan(23, 59, 0));
			return task;
		}

		public string Description { get; private set; }
		public BillingBackgroundTaskTypes TaskType { get; private set; }
		public int TargetDay { get; private set; }
		public List<DayOfWeek> RunDays { get; private set; }
		public TimeSpan RunTimeOfDay { get; private set; }

		#endregion

		#region XML / Type Helpers

		private static T GetRequiredEnum<T>(XElement xml, string attributeName) where T : struct
		{
			var att = xml.Attribute(attributeName);
			if (att == null)
			{
				ScheduledTaskManager._api.Log("Required configuration parameter '" + attributeName + "' was not found for task configuration", "BillingSched",
					MessageType.Error, MessageImportance.Essential);
				throw new Exception("Background Task - required configuration parameter missing");
			}

			T result;
			if (!Enum.TryParse(att.Value.Trim(), out result))
			{
				ScheduledTaskManager._api.Log("Required configuration parameter '" + attributeName + "' = '" + att.Value.Trim() + "' was not understood", "BillingSched",
					MessageType.Error, MessageImportance.Essential);
				throw new Exception("Background Task - required configuration parameter incorrect");
			}

			return result;
		}

		private static string GetOptionalString(XElement xml, string attributeName, string defaultValue)
		{
			string result = defaultValue;
			var att = xml.Attribute(attributeName);
			if (att != null)
			{
				result = att.Value.Trim();
			}
			//if (result != defaultValue)
			//{
			//	ScheduledTaskManager._api.Log("Configuration parameter '" + attributeName + "' has been set to: " + result, "BillingSched", MessageType.Information, MessageImportance.Debug);
			//}
			return result;
		}

		private static int GetOptionalInt(XElement xml, string attributeName, int defaultValue)
		{
			int result = defaultValue;
			var att = xml.Attribute(attributeName);
			if (att != null)
			{
				if (!int.TryParse(att.Value.Trim(), out result))
				{
					result = defaultValue;
					ScheduledTaskManager._api.Log("Configuration parameter '" + attributeName + "' could not be parsed: " + att.Value.Trim(), "BillingSched", MessageType.Error, MessageImportance.Essential);
				}
			}
			//if (result != defaultValue)
			//{
			//	ScheduledTaskManager._api.Log("Configuration parameter '" + attributeName + "' has been set to: " + result, "BillingSched", MessageType.Information, MessageImportance.Debug);
			//}
			return result;
		}

		private static TimeSpan GetOptionalTimeSpan(XElement xml, string attributeName, TimeSpan defaultValue)
		{
			TimeSpan result = defaultValue;
			var att = xml.Attribute(attributeName);
			if (att != null)
			{
				if (!TimeSpan.TryParse(att.Value.Trim(), System.Globalization.CultureInfo.InvariantCulture, out result))
				{
					result = defaultValue;
					ScheduledTaskManager._api.Log("Configuration parameter '" + attributeName + "' could not be parsed: " + att.Value.Trim(), "BillingSched", MessageType.Error, MessageImportance.Essential);
				}
			}
			//if (result != defaultValue)
			//{
			//	ScheduledTaskManager._api.Log("Configuration parameter '" + attributeName + "' has been set to: " + result, "BillingSched", MessageType.Information, MessageImportance.Debug);
			//}
			return result;
		}

		internal static List<DayOfWeek> ConvertDaysOfWeek(string value)
		{
			List<DayOfWeek> daysOfWeek = new List<DayOfWeek>();
			value = value.Trim();
			if (value.Length == 0 || value == "*")
			{
				foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
				{
					daysOfWeek.Add(day);
				}
			}
			else
			{
				var rawVals = value.Split(',').Select(o => o.Trim());
				foreach (var rawVal in rawVals)
				{
					if (rawVal.Length == 1)
					{
						if (char.IsDigit(rawVal[0]))
						{
							daysOfWeek.Add((DayOfWeek)(int.Parse(rawVal) - 1));
						}
						else
						{
							ScheduledTaskManager._api.Log("Day of Week value '" + rawVal + "' (in '" + value + "') was not understood.", "BillingSched", MessageType.Error, MessageImportance.Essential);
						}
					}
					else
					{
						ScheduledTaskManager._api.Log("Day of Week value '" + rawVal + "' (in '" + value + "') was not understood.", "BillingSched", MessageType.Error, MessageImportance.Essential);
					}
				}
			}

			return daysOfWeek;
		}

		#endregion

		public void DoWork(IBillingAPI api)
		{
			bool workToDo = BillingBackgroundTaskRunner.CheckForWork(this, api, DateTime.Now);
			if (workToDo)
			{
				BillingBackgroundTaskRunner.ExecuteTask(this, api);
			}
		}

	}
}
