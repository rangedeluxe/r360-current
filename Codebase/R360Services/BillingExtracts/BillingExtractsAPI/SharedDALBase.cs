﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BillingExtracts.API
{
	public abstract class SharedDALBase
	{
		protected readonly SharedConfigParametersBase _config;

		protected SharedDALBase(SharedConfigParametersBase config)
		{
			_config = config;
		}

		protected List<SqlParameter> GetDateRangeParams()
		{
			if (_config.ExtractParams.DateRange == null)
				throw new Exception("Error - Date range was not set before accessing the DAL");

			List<SqlParameter> sqlParams = new List<SqlParameter>();
			sqlParams.Add(new SqlParameter("@parmStartDateKey", SqlDbType.Int) { Value = int.Parse(_config.ExtractParams.DateRange.Item1.ToString("yyyyMMdd")) });
			sqlParams.Add(new SqlParameter("@parmEndDateKey", SqlDbType.Int) { Value = int.Parse(_config.ExtractParams.DateRange.Item2.ToString("yyyyMMdd")) });

			return sqlParams;
		}

		protected void AddTargetParams(List<SqlParameter> sqlParams)
		{
			// Add target entity as xml
			string entityList = string.Empty;
			if (_config.ExtractParams.TargetEntity == null)
				throw new Exception("Error - Target Entity(s) was not set before accessing the DAL");

			foreach (var entity in _config.ExtractParams.TargetEntities)
			{
				entityList += "<e id='" + entity + "' />";
			}
			sqlParams.Add(new SqlParameter("@parmEntities", SqlDbType.Xml) { Value = "<list>" + entityList + "</list>" });
		}

		protected void AddRetentionCutoffs(List<SqlParameter> sqlParams, IList<Analysis.RetentionValue> retentionCutoffs)
		{
			// Add retentionCutoffs as xml
			string cutoffs = string.Empty;
			if (retentionCutoffs != null)
			{
				for (int i = 0; i < retentionCutoffs.Count; i++)
				{
					cutoffs += "<c id='" + i.ToString() + "' val='" + retentionCutoffs[i].Value.ToString() + "' unit='" + retentionCutoffs[i].Units.ToString() + "' />";
				}
			}
			sqlParams.Add(new SqlParameter("@parmRetentionCutoffs", SqlDbType.Xml) { Value = "<list>" + cutoffs + "</list>" });
		}

		public virtual int GetFirstBank(int fiEntityID)
		{
			int result = 0;

			string spName = "usp_Billing_Extract_GetFirstBank";
			List<SqlParameter> sqlParams = new List<SqlParameter>();
			sqlParams.Add(new SqlParameter("@parmFiEntityId", SqlDbType.Int) { Value = fiEntityID });

			// Execute sproc
			DataTable dt;
			bool success = _config.API.ExecuteBillingProc(spName, sqlParams, out dt);
			if (!success)
				throw new Exception("Database error calling " + spName + " - file generation aborted");

			// Get result
			if (dt != null && dt.Rows.Count > 0)
			{
				result = Convert.ToInt32(dt.Rows[0][0]);
			}

			return result;
		}

		public virtual void GetBillingFields(int entityID, out string billingAccount, out string billingField1, out string billingField2)
		{
			string spName = "usp_Billing_Extract_GetBillingFields";
			List<SqlParameter> sqlParams = new List<SqlParameter>();
			sqlParams.Add(new SqlParameter("@parmEntityId", SqlDbType.Int) { Value = entityID });

			// Execute sproc
			DataTable dt;
			bool success = _config.API.ExecuteBillingProc(spName, sqlParams, out dt);
			if (!success)
				throw new Exception("Database error calling " + spName + " - file generation aborted");

			// Get result
			if (dt != null && dt.Rows.Count > 0)
			{
				billingAccount = Convert.ToString(dt.Rows[0][0]);
				billingField1 = Convert.ToString(dt.Rows[0][1]);
				billingField2 = Convert.ToString(dt.Rows[0][2]);
			}
			else
			{
				billingAccount = string.Empty;
				billingField1 = string.Empty;
				billingField2 = string.Empty;
			}
		}

		#region Stored Procedure Template Methods

		protected DataTable ExecuteProc(string spName, IList<Analysis.RetentionValue> retentionCutoffs = null)
		{
			// Provide the date range and targets as parameters
			List<SqlParameter> sqlParams = GetDateRangeParams();
			AddTargetParams(sqlParams);

			// Include retention if applicable
			if (retentionCutoffs != null)
				AddRetentionCutoffs(sqlParams, retentionCutoffs);

			// Execute sproc
			DataTable dt;
			bool success = _config.API.ExecuteBillingProc(spName, sqlParams, out dt);
			if (!success)
				throw new Exception("Database error calling " + spName + " - file generation aborted");

			return dt;
		}

		protected List<GenericCounts> GetWorkgroupList(string spName)
		{
			// Return a list of customers (entities)
			List<GenericCounts> entities = new List<GenericCounts>();

			// Execute stored procedure
			DataTable dt = ExecuteProc(spName);

			// Result set contains all grouping key values
			if (dt != null && dt.Rows.Count > 0)
			{
				foreach (DataRow row in dt.Rows)
				{
					GenericCounts entity = new GenericCounts(
						fiEntityID: Convert.ToInt32(row[0]),
						siteBankID: Convert.ToInt32(row[1]),
						entityID: Convert.ToInt32(row[2]),
						workgroupID: Convert.ToInt32(row[3]))
						{
							WorkgroupName = Convert.ToString(row[4]),
							BillingAccount = Convert.ToString(row[5]),
							BillingField1 = Convert.ToString(row[6]),
							BillingField2 = Convert.ToString(row[7]),
							Count = 1
						};
					entities.Add(entity);
				}
			}
			return entities;
		}

		protected List<GenericCounts> GetWorkgroupListWithMultipleCounts(string spName)
		{
			// Return a list of customers (entities)
			List<GenericCounts> entities = new List<GenericCounts>();

			// Execute stored procedure
			DataTable dt = ExecuteProc(spName);

			// Result set contains FI Entity ID, Logon Entity ID, all other grouping key values, and one or more volume counts
			if (dt != null && dt.Rows.Count > 0)
			{
				List<int> counts = new List<int>();
				foreach (DataRow row in dt.Rows)
				{
					// Get list of counts
					counts.Clear();
					for (int i = 9; i < dt.Columns.Count; i++)
						counts.Add(Convert.ToInt32(row[i]));

					GenericCounts entity = new GenericCounts(counts.ToArray(),
						fiEntityID: Convert.ToInt32(row[0]),
						siteBankID: Convert.ToInt32(row[2]),
						entityID: Convert.ToInt32(row[3]),
						workgroupID: Convert.ToInt32(row[4]))
						{
							WorkgroupName = Convert.ToString(row[5]),
							BillingAccount = Convert.ToString(row[6]),
							BillingField1 = Convert.ToString(row[7]),
							BillingField2 = Convert.ToString(row[8]),

							LogonEntityID = Convert.ToInt32(row[1])
						};
					entities.Add(entity);
				}
			}
			return entities;
		}

		protected List<GenericCountsByRetention> GetWorkgroupCountsByRetention(string spName, IList<Analysis.RetentionValue> retentionCutoffs)
		{
			// Return a list of institutions
			List<GenericCountsByRetention> counts = new List<GenericCountsByRetention>();

			// Execute stored procedure
			DataTable dt = ExecuteProc(spName, retentionCutoffs);

			// Result set contains all grouping keys, a volume count, and list-index of retention Cutoff
			if (dt != null && dt.Rows.Count > 0)
			{
				foreach (DataRow row in dt.Rows)
				{
					GenericCountsByRetention entity = new GenericCountsByRetention(
						fiEntityID: Convert.ToInt32(row[0]),
						siteBankID: Convert.ToInt32(row[1]),
						entityID: Convert.ToInt32(row[2]),
						workgroupID: Convert.ToInt32(row[3]))
						{
							WorkgroupName = Convert.ToString(row[4]),
							BillingAccount = Convert.ToString(row[5]),
							BillingField1 = Convert.ToString(row[6]),
							BillingField2 = Convert.ToString(row[7]),
							Count = Convert.ToInt32(row[8]),
							RetentionCutoff = retentionCutoffs[Convert.ToInt32(row[9])],
						};
					counts.Add(entity);
				}
			}
			return counts;
		}

		#endregion

		public virtual List<GenericCounts> GetCustomers()
		{
			return GetWorkgroupList("usp_Billing_Extract_Customers");
		}

		public virtual List<GenericCounts> GetExceptionsEnabledCustomers()
		{
			return GetWorkgroupList("usp_Billing_Extract_ExceptionsEnabledCustomers");
		}

		public virtual List<GenericCounts> GetExceptionCounts()
		{
			return GetWorkgroupListWithMultipleCounts("usp_Billing_Extract_ExceptionCounts");
		}

		public virtual List<GenericCountsByRetention> GetItemCountsByRetention(IList<Analysis.RetentionValue> retentionCutoffs)
		{
			return GetWorkgroupCountsByRetention("usp_Billing_Extract_ItemCounts", retentionCutoffs);
		}

		public virtual List<GenericCountsByRetention> GetPaymentItemCountsByRetention(IList<Analysis.RetentionValue> retentionCutoffs)
		{
			return GetWorkgroupCountsByRetention("usp_Billing_Extract_PaymentItemCounts", retentionCutoffs);
		}

		public virtual List<GenericCountsByRetention> GetNonPaymentItemCountsByRetention(IList<Analysis.RetentionValue> retentionCutoffs)
		{
			return GetWorkgroupCountsByRetention("usp_Billing_Extract_NonPaymentItemCounts", retentionCutoffs);
		}

		public virtual List<GenericCounts> GetBulkDownloadZipCounts()
		{
			return GetWorkgroupListWithMultipleCounts("usp_Billing_Extract_BulkFileRecordCounts");
		}

		public virtual List<GenericCounts> GetBulkDownloadCsv1Counts()
		{
			return GetWorkgroupListWithMultipleCounts("usp_Billing_Extract_CsvRecordCounts");
		}

		public virtual List<GenericCounts> GetBulkDownloadCsv2Counts()
		{
			return GetWorkgroupListWithMultipleCounts("usp_Billing_Extract_CsvDownloadRecordCounts");
		}

		public virtual List<GenericCounts> GetBulkDownloadPrintViewCounts()
		{
			return GetWorkgroupListWithMultipleCounts("usp_Billing_Extract_PrintViewRecordCounts");
		}

		public virtual List<GenericCounts> GetBulkDownloadPdfViewCounts()
		{
			return GetWorkgroupListWithMultipleCounts("usp_Billing_Extract_PdfViewRecordCounts");
		}

		public virtual List<GenericCounts> GetBulkDownloadHtmlCounts()
		{
			return GetWorkgroupListWithMultipleCounts("usp_Billing_Extract_HtmlDownloadRecordCounts");
		}

		public virtual List<GenericCounts> GetBulkDownloadXmlCounts()
		{
			return GetWorkgroupListWithMultipleCounts("usp_Billing_Extract_XmlDownloadRecordCounts");
		}
	}
}
