﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.RecHub.BillingExtracts.Common;
using WFS.RecHub.BillingExtracts.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Ryan Schwantes
* Date: 2/4/2014
*
* Purpose: Billing Extracts Business Logic Class
*
* Modification History
*********************************************************************************/
namespace WFS.RecHub.BillingExtracts.API
{
	internal interface IBillingAPI : IFileGeneratorAPI
	{
		void Log(string message, string src, MessageType messageType, MessageImportance messageImportance);

		List<ICustomFileGenerator> GetAllGeneratorClasses();

		IBillingDAL GetDAL();

		bool RerunAllScheduledTasksOnStartup { get; }

		IRaamClient GetRaamClient();

		int GetRootEntityID();
	}

    public class BillingAPI : APIBase, IBillingAPI, IFileGeneratorAPI
    {
		internal const string CUSTOMFILEGENERATORFOLDER = "CustomFileGenerators";

		private IBillingDAL _BillingExtractsDAL = null;
		private bool? _rerunAllScheduledTasksOnStartup;
		private int? _rootEntityId = null;
		private static BillingAPI _anonymousAPI = null;
		private static readonly object _syncLock = new object();

		public static void Init(dlgLogEvent logger)
		{
			lock (_syncLock)
			{
				if (_anonymousAPI == null)
				{
					// Create a singleton instance of "this" with no service context
					_anonymousAPI = new BillingAPI(R360ServiceContext.Current);
					_anonymousAPI.LogEvent += logger;

					// One-time setup
					_anonymousAPI.InitAssemblyResolver();

					// Initialize scheduler background processes
					ScheduledTaskManager.Init(_anonymousAPI);
				}
			}
		}

		public BillingAPI(R360ServiceContext serviceContext) : base(serviceContext)
		{
			LogManager.Logger = new ConfigHelpers.Logger(o => OnLogEvent(o, this.GetType().Name, MessageType.Information, MessageImportance.Verbose), o => OnLogEvent(o, this.GetType().Name, MessageType.Warning, MessageImportance.Essential));
		}

		private IBillingDAL BillingExtractsDAL
		{
			get
			{
				if (_BillingExtractsDAL == null)
				{
					// TODO: Support Mocking the DAL
					_BillingExtractsDAL = BillingDALFactory.Create(_ServiceContext.SiteKey ?? string.Empty);
				}
				return (_BillingExtractsDAL);
			}
		}

		IBillingDAL IBillingAPI.GetDAL() { return BillingExtractsDAL; }

		public bool RerunAllScheduledTasksOnStartup 
		{ 
			get 
			{
				if (!_rerunAllScheduledTasksOnStartup.HasValue)
				{
					try
					{
						_rerunAllScheduledTasksOnStartup = bool.Parse(ConfigHelpers.GetAppSetting("RerunAllScheduledTasksOnStartup", "false"));
					}
					catch (Exception ex)
					{
						_rerunAllScheduledTasksOnStartup = false;
						OnLogEvent("Error loading AppSetting=RerunAllScheduledTasksOnStartup: " + ex.ToString(), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
					}
				}
				return _rerunAllScheduledTasksOnStartup.Value; 
			}
		}

		public int GetRootEntityID()
		{
			if (!_rootEntityId.HasValue)
			{
				try
				{
					_rootEntityId = int.Parse(ConfigHelpers.GetAppSetting("RootEntityID", "1"));
				}
				catch (Exception ex)
				{
					_rootEntityId = 1;
					OnLogEvent("Error loading AppSetting=RootEntityID: " + ex.ToString(), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				}
			}
			return _rootEntityId.Value;
		}

		IRaamClient IBillingAPI.GetRaamClient()
		{
			return new RaamClient(LogManager.Logger);
		}

		protected override WFS.RecHub.Common.ActivityCodes RequestType
		{
			get { throw new NotImplementedException(); }
		}

		protected override bool ValidateSID(out int userID)
		{
			// TODO: Not sure why this doesn't use a common DAL, other than the fact that sharing DALs adds complexity...
			// But if it did, then the APIBase could call it directly...
			bool bReturnValue = false;
			userID = -1;
			DataTable dtResults;

			if (BillingExtractsDAL.GetUserIDBySID(_ServiceContext.GetSID(), out dtResults))
			{
				if (dtResults.Rows.Count > 0)
				{
					userID = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
					bReturnValue = true;
				}
				else
				{
					OnLogEvent(string.Format("SID '{0}' could not be matched to a user", _ServiceContext.GetSID()),
						typeof(BillingAPI).Name, MessageType.Information, MessageImportance.Verbose);
				}
			}

			return bReturnValue;
		}

		#region Service Methods

		public void Ping(string logMessage)
		{
			OnLogEvent("Ping: " + logMessage, typeof(BillingAPI).Name, MessageType.Information, MessageImportance.Verbose);
		}

		public void GetBillingFormats(BillingFormatsResponse response)
		{
			OnLogEvent("Retrieving billing formats... ", typeof(BillingAPI).Name, MessageType.Information, MessageImportance.Debug);
			List<ICustomFileGenerator> fileGenerators = GetAllGeneratorClassesImpl();

			// TODO: Who validates permissions?  If it is the caller, we need to return the securably resource name...

			response.formatList = new List<BillingFormat>(fileGenerators.Select(o => new BillingFormat()
			{
				DisplayName = o.GetDisplayName(),
				FormatClass = o.GetType().FullName,
				BaseParameters = "<P GenClass='" + o.GetType().FullName + "' />",
			}));
		}

		public void GenerateFile(string xmlParameters, BaseResponse response)
		{
			OnLogEvent("Generating File for: " + xmlParameters, typeof(BillingAPI).Name, MessageType.Information, MessageImportance.Verbose);

			XDocument xdoc = XDocument.Load(new StringReader(xmlParameters));
			string generatorClass = xdoc.Root.Attribute("GenClass").Value;
			ICustomFileGenerator generator = GetGeneratorClass(generatorClass);

			List<string> errors;
			generator.GenerateFile(this, xmlParameters, out errors);

			// Return result
			if (errors != null && errors.Count > 0)
			{
				foreach (string error in errors)
				{
					OnLogEvent(error, generator.GetType().Name, MessageType.Error, MessageImportance.Essential);
					response.Errors.Add(error);
				}
				response.Status = StatusCode.FAIL;
			}
			else
			{
				response.Status = StatusCode.SUCCESS;
			}
		}

		#endregion

		#region Custom DLL Helper Methods

		List<ICustomFileGenerator> IBillingAPI.GetAllGeneratorClasses() { return GetAllGeneratorClassesImpl(); }
		private List<ICustomFileGenerator> GetAllGeneratorClassesImpl()
		{
			// Cache this list so we don't keep reloading it...?
			List<ICustomFileGenerator> result = new List<ICustomFileGenerator>();

			// Check for assemblies
			string basePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
			string fullPath = Path.Combine(basePath, CUSTOMFILEGENERATORFOLDER);
			string assemblyNameMask = "WFS.RecHub.BillingExtracts.*.dll";

			var assemblies = Directory.EnumerateFiles(fullPath, assemblyNameMask, SearchOption.TopDirectoryOnly);
			foreach (var assemblyName in assemblies)
			{
				try
				{
					var oAssembly = Assembly.LoadFrom(assemblyName);
					var types = oAssembly.GetTypes().Where(o => o.IsClass && !o.IsAbstract && o.GetInterfaces().Contains(typeof(ICustomFileGenerator)));

					foreach (var type in types)
					{
						try
						{
							var generator = Activator.CreateInstance(type);
							var generatorInterface = (ICustomFileGenerator)generator;

							result.Add(generatorInterface);
						}
						catch (Exception ex1)
						{
							OnErrorEvent(ex1, typeof(BillingAPI).Name, "GetAllGeneratorClasses.CreateInstance(" + type.FullName + ")");
							// continue, skipping this class
						}
					}
				}
				catch (ReflectionTypeLoadException exType)
				{
					OnErrorEvent(exType, typeof(BillingAPI).Name, "GetAllGeneratorClasses.LoadAssembly.GetTypes(" + assemblyName + ")");
					if (exType.LoaderExceptions != null)
					{
						try
						{
							foreach (var loadEx in exType.LoaderExceptions)
							{
								OnLogEvent("GetAllGeneratorClasses.LoadAssembly.GetTypes.LoaderException: " + loadEx.ToString(), typeof(BillingAPI).Name, MessageType.Error, MessageImportance.Essential);
							}
						}
						catch (Exception ex2)
						{
							OnErrorEvent(ex2, typeof(BillingAPI).Name, "GetAllGeneratorClasses.LoadAssembly.GetTypes.LogLoaderException");
						}
					}
					// continue, skipping this assembly
				}
				catch (Exception ex)
				{
					OnErrorEvent(ex, typeof(BillingAPI).Name, "GetAllGeneratorClasses.LoadAssembly(" + assemblyName + ")");
					// continue, skipping this assembly
				}
			}

			return result;
		}

		private ICustomFileGenerator GetGeneratorClass(string generatorClass)
		{
			// Split into namespace / class
			int dotPos = generatorClass.LastIndexOf('.');
			string genNamespace = generatorClass.Substring(0, dotPos);
			string genClassName = generatorClass.Substring(dotPos + 1);
			string genAssembly = genNamespace; // Assembly name must match the namespace...

			// Load the assembly, if necessary...
			string basePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
			string fullPath = Path.Combine(basePath, CUSTOMFILEGENERATORFOLDER, genAssembly + ".dll");
			var oAssembly = Assembly.LoadFrom(fullPath);
			
			// Load the class from the assembly
			var generatorType = oAssembly.GetType(generatorClass);
			if (generatorType == null)
				throw new Exception("Type " + generatorClass + " could not be loaded from the assembly.");

			var generator = Activator.CreateInstance(generatorType);
			var generatorInterface = (ICustomFileGenerator)generator;
			
			// Return it
			return generatorInterface;
		}

		private void InitAssemblyResolver()
		{
			AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
		}

		private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
		{
			// Only one version of the Billing API should be loaded; if the version doesn't resolve automatically, this method will return this instance
			// The custom file generators reference this DLL, so this method allows them to become "out-of-sync" with the version number without breaking...
			var assembly = Assembly.GetExecutingAssembly();
			if (args.Name.StartsWith(assembly.FullName.Split(',')[0]))
			{
				OnLogEvent(string.Format("Manually resolved assembly reference to '{0}' as this instance ('{1}')", args.Name, assembly.FullName), typeof(BillingAPI).Name, MessageType.Information, MessageImportance.Verbose);
				return assembly;
			}

			// Next - if "this" is the executable, then assemblies were ILMerged.
			// So return "this" for any call to the original assembly name ("BillingExtractsAPI, ...")
			var entryAssembly = Assembly.GetEntryAssembly();
			if (assembly == entryAssembly)
			{
				if (args.Name.StartsWith("BillingExtractsAPI,"))
				{
					OnLogEvent(string.Format("Manually resolved assembly reference to '{0}' as this instance ('{1}')", args.Name, assembly.FullName), typeof(BillingAPI).Name, MessageType.Information, MessageImportance.Verbose);
					return assembly;
				}
			}

			return null;
		}

		#endregion

		#region IFileGeneratorAPI

		void IFileGeneratorAPI.LogEvent(string message, string src, IFileGeneratorAPI_MessageType messageType, IFileGeneratorAPI_MessageImportance messageImportance)
		{
			OnLogEvent(message, src, (MessageType)(int)messageType, (MessageImportance)(int)messageImportance);
		}

		bool IFileGeneratorAPI.ExecuteBillingProc(string procName, IEnumerable<SqlParameter> parameters, out DataTable results)
		{
			return BillingExtractsDAL.ExecuteBillingProc(procName, parameters, out results);
		}

		string IFileGeneratorAPI.GetOptionalString(XElement xmlElement, string attribute, string defaultValue)
		{
			return ConfigUtilities.GetOptionalString(xmlElement, attribute, defaultValue);
		}

		Tuple<DateTime, DateTime> IFileGeneratorAPI.GetOptionalDates(XElement xmlElement, string date1, string date2, Func<Tuple<DateTime, DateTime>> defaultProvider)
		{
			return ConfigUtilities.GetOptionalDates(xmlElement, date1, date2, defaultProvider);
		}

		Tuple<DateTime, DateTime> IFileGeneratorAPI.GetLastMonth()
		{
			return ConfigUtilities.GetLastMonth();
		}

		string IFileGeneratorAPI.GetRequiredString(XDocument doc, string elementName, string configFileName, string moduleName)
		{
			return ConfigUtilities.GetRequiredString(doc, elementName, configFileName, moduleName, this);
		}

		string IFileGeneratorAPI.GetOptionalString(XDocument doc, string elementName, string defaultValue, string moduleName)
		{
			return ConfigUtilities.GetOptionalString(doc, elementName, defaultValue, moduleName, this);
		}

		bool IFileGeneratorAPI.GetOptionalBoolean(XDocument doc, string elementName, bool defaultValue, string moduleName)
		{
			return ConfigUtilities.GetOptionalBoolean(doc, elementName, defaultValue, moduleName, this);
		}

		void IFileGeneratorAPI.ConvertEntitiesToCustomers(IEnumerable<IEntityID> entities, ref object raamState)
		{
			RAAMUtilities.ConvertEntitiesToCustomers(entities, ref raamState, this);
		}

		IList<EntityWithFI> IFileGeneratorAPI.GetFIEntities(IEnumerable<IEntityID> entities, ref object raamState)
		{
			return RAAMUtilities.GetFIEntities(entities, ref raamState, this);
		}

		bool IFileGeneratorAPI.IsCorporate(int entityId, ref object raamState)
		{
			return RAAMUtilities.IsCorporate(entityId, ref raamState, this);
		}

		#endregion

		/// <summary>
		/// Helper Method to expose the service's logging provider...
		/// </summary>
		void IBillingAPI.Log(string message, string src, MessageType messageType, MessageImportance messageImportance)
		{
			OnLogEvent(message, src, messageType, messageImportance);
		}
	}
}
