﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;


namespace WFS.RecHub.BillingExtracts.API
{
	public enum IFileGeneratorAPI_MessageType
	{
		Error = 0,
		Information = 1,
		Warning = 2,
	}

	public enum IFileGeneratorAPI_MessageImportance
	{
		Essential = 0,
		Verbose = 1,
		Debug = 2,
	}

	public interface IEntityID
	{
		int EntityID { get; }
		int CustomerEntityID { get; set; }
	}


	public class EntitySimple
	{
		public int EntityID { get; set; }
	}

	public class EntityWithFI : EntitySimple
	{
		public int FIEntityID { get; set; }
	}

	public interface IFileGeneratorAPI
	{
		// Logging infrastructure
		void LogEvent(string message, string src, IFileGeneratorAPI_MessageType messageType, IFileGeneratorAPI_MessageImportance messageImportance);

		// DAL infrastructure
		bool ExecuteBillingProc(string procName, IEnumerable<SqlParameter> sqlParams, out DataTable results);

		// Configuration Utilities
		string GetOptionalString(XElement xmlElement, string attribute, string defaultValue);
		Tuple<DateTime, DateTime> GetOptionalDates(XElement xmlElement, string date1, string date2, Func<Tuple<DateTime, DateTime>> defaultProvider);
		Tuple<DateTime, DateTime> GetLastMonth();

		string GetRequiredString(XDocument doc, string elementName, string configFileName, string moduleName);
		string GetOptionalString(XDocument doc, string elementName, string defaultValue, string moduleName);
		bool GetOptionalBoolean(XDocument doc, string elementName, bool defaultValue, string moduleName);

		// RAAM Utilities
		void ConvertEntitiesToCustomers(IEnumerable<IEntityID> entities, ref object sharedState);
		IList<EntityWithFI> GetFIEntities(IEnumerable<IEntityID> entities, ref object sharedState);
		bool IsCorporate(int entityId, ref object sharedState);
	}

	public interface ICustomFileGenerator
	{
		string GetDisplayName();
		void GenerateFile(IFileGeneratorAPI api, string xmlParameters, out List<string> errors);
		void PerformScheduledWork(IFileGeneratorAPI api, IEnumerable<BillingBackgroundTaskTypes> scheduledTasks);
	}
}
