﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BillingExtracts.API
{
	public abstract class SharedGeneratorBase<LineItemType>
	{
		#region Properties / State

		protected abstract SharedConfigParametersBase CreateConfigParameters();
		private SharedConfigParametersBase _configParams;
		protected internal SharedConfigParametersBase _config
		{
			get
			{
				if (_configParams == null)
					_configParams = CreateConfigParameters();

				return _configParams;
			}
		}

		protected object _raamState = null;

		#endregion

		#region Main Driver; Activity Code / DAL Mapping

		/// <summary>
		/// The main driver function for the generator class - this processes the flat list of class-specific Activity Codes into groups relating to a specific DAL call.
		/// After grouping the activity codes, it calls the abstract "Handle" methods to validate the groups and assign the DAL method.
		/// </summary>
		/// <param name="activityCodeList"></param>
		/// <returns></returns>
		protected List<LineItemType> LoadLineItems(Analysis.ActivityInfo[] activityCodeList)
		{
			List<LineItemType> lineItems = new List<LineItemType>();

			// Go through the list of activity types, by aggregation mode, and separate into groups as necessary

			// 1. Counts of "things" (thing = Kind, Kind is typically "Exists" but might be some other property, distinct can be at multiple detail levels...)
			Dictionary<Analysis.Kind, List<Analysis.ActivityInfo>> countOfActivities = new Dictionary<Analysis.Kind, List<Analysis.ActivityInfo>>();

			// 2. Totals of "things" (thing = Kind, totals can be at multiple detail levels)
			Dictionary<Analysis.Kind, List<Analysis.ActivityInfo>> sumOfActivititesByKind = new Dictionary<Analysis.Kind, List<Analysis.ActivityInfo>>();

			// 3. Counts of "things" by Retention (thing = Detail Level, Kind is typically "Exists" but might be some other property, multiple retention levels for a single Detail level)
			Dictionary<Tuple<Analysis.Kind, Analysis.DetailLevel>, List<Analysis.ActivityInfo>> countOfActivitesByRetention = new Dictionary<Tuple<Analysis.Kind, Analysis.DetailLevel>, List<Analysis.ActivityInfo>>();

			for (int i = 0; i < activityCodeList.Length; i++)
			{
				var activityInfo = activityCodeList[i];
				if (activityInfo.Grouping == Analysis.GroupingLevel.NotSet)
					throw new Exception(string.Format("Grouping level was not set for activity {0}", activityInfo.ActivityCode));

				switch (activityInfo.Aggregation)
				{
					case Analysis.AggregationType.CountDistinct:
						{
							// There may be activities at different grouping levels -- generate the counts at all grouping levels at one time
							var key = activityInfo.AnalysisKind;
							if (!countOfActivities.ContainsKey(key))
								countOfActivities[key] = new List<Analysis.ActivityInfo>();
							countOfActivities[key].Add(activityInfo);
						}
						break;

					case Analysis.AggregationType.Total:
						{
							// There may be activities at different grouping levels -- generate the counts at all grouping levels at one time
							// Also, for some targets, we want to total multiple statistics (multiple detail levels)
							var key = activityInfo.AnalysisKind;
							if (!sumOfActivititesByKind.ContainsKey(key))
								sumOfActivititesByKind[key] = new List<Analysis.ActivityInfo>();
							sumOfActivititesByKind[activityInfo.AnalysisKind].Add(activityInfo);
						}
						break;

					case Analysis.AggregationType.TotalByRetention:
						{
							// There may be activities at different grouping levels -- generate the counts at all grouping levels at one time
							// Also, load all different retention periods at once
							var key = Tuple.Create(activityInfo.AnalysisKind, activityInfo.VolumeTarget);
							if (!countOfActivitesByRetention.ContainsKey(key))
								countOfActivitesByRetention[key] = new List<Analysis.ActivityInfo>();
							countOfActivitesByRetention[key].Add(activityInfo);
						}
						break;

					default:
						_config.API.LogEvent("Unexpected Aggregation Type: " + activityInfo.Aggregation, this.GetType().Name, IFileGeneratorAPI_MessageType.Error, IFileGeneratorAPI_MessageImportance.Essential);
						break;
				}
			}

			// Now, process the grouped "count distinct" activity codes...
			foreach (var entry in countOfActivities)
			{
				HandleSimpleCountLineItems(lineItems, entry.Value);
			}

			// Now, process the grouped "sum of" activity codes...
			foreach (var entry in sumOfActivititesByKind)
			{
				HandleSimpleTotalledLineItems(lineItems, entry.Value);
			}

			// Now, process the grouped "count of by retention" activity codes...
			foreach (var entry in countOfActivitesByRetention)
			{
				HandleRetentionTotalsLineItems(lineItems, entry.Value);
			}

			return lineItems;
		}

		// These methods are defined as abstract, because it is up to the derived class to do any validations and select the correct DAL method, before calling back to the aggregation methods
		// In reality, the implementations will be nearly identical...  But we don't want to have to modify base code if they do end up differing...
		protected abstract void HandleSimpleCountLineItems(List<LineItemType> lineItems, List<Analysis.ActivityInfo> activityInfos);
		protected abstract void HandleSimpleTotalledLineItems(List<LineItemType> lineItems, List<Analysis.ActivityInfo> activityInfos);
		protected abstract void HandleRetentionTotalsLineItems(List<LineItemType> lineItems, List<Analysis.ActivityInfo> activityInfos);

		#endregion

		#region Main Aggregation Logic; Get the Data, and Transform it for conversion to Line Items

		// This method is defined as abstract, because the derived class must correctly construct the line items (base class doesn't really know the type...)
		// The other "CreateLineItem" methods do all the work, though, of aggregating and keying the data, so it can just be passed in to this method to convert it to a line item...
		protected abstract void CreateLineItems(List<LineItemType> lineItems, Dictionary<GenericGroupingKeys, GenericCounts> data, IList<Analysis.ActivityInfo> activityInfos);

		// Call DAL and process distinct counts at multiple levels / groupings
		protected void CreateLineItemsWithCounts(List<LineItemType> lineItems, List<Analysis.ActivityInfo> activityInfos, Func<IEnumerable<GenericCounts>> getDALCounts)
		{
			// Create line items for all institutions returned
			var workgroups = getDALCounts();

			// Set FI if necessary
			AssignBankAndFI(ref workgroups);

			// Flatten entity list to appropriate level
			_config.API.ConvertEntitiesToCustomers(workgroups, ref _raamState);

			// Track at different grouping levels
			Dictionary<Analysis.DetailLevel, Analysis.GroupingLevel> groupDetailMap = new Dictionary<Analysis.DetailLevel, Analysis.GroupingLevel>();
			groupDetailMap[Analysis.DetailLevel.FI_Entity] = Analysis.GroupingLevel.FI_Entity;
			groupDetailMap[Analysis.DetailLevel.Customer_Entity] = Analysis.GroupingLevel.Customer_Entity;
			groupDetailMap[Analysis.DetailLevel.Workgroup] = Analysis.GroupingLevel.Workgroup;

			List<Analysis.GroupingLevel> groupingLevels = new List<Analysis.GroupingLevel> { Analysis.GroupingLevel.FI_Entity, Analysis.GroupingLevel.Bank, Analysis.GroupingLevel.Customer_Entity, Analysis.GroupingLevel.Workgroup };

			Dictionary<Analysis.DetailLevel, Dictionary<Analysis.GroupingLevel, Dictionary<GenericGroupingKeys, GenericCounts>>> groupedCounts = new Dictionary<Analysis.DetailLevel, Dictionary<Analysis.GroupingLevel, Dictionary<GenericGroupingKeys, GenericCounts>>>();
			foreach (var entry in groupDetailMap)
			{
				groupedCounts[entry.Key] = new Dictionary<Analysis.GroupingLevel, Dictionary<GenericGroupingKeys, GenericCounts>>();
				foreach (var level in groupingLevels)
				{
					if ((int)entry.Value >= (int)level)
					{
						groupedCounts[entry.Key][level] = new Dictionary<GenericGroupingKeys, GenericCounts>();
					}
				}
			}

			// Perform "distinct" operations
			foreach (var workgroup in workgroups)
			{
				var key = new GenericGroupingKeys(workgroup.FIEntityID, workgroup.SiteBankID, workgroup.CustomerEntityID, workgroup.WorkgroupID);
				workgroup.Count = 1; // Verify...

				groupedCounts[Analysis.DetailLevel.Workgroup][Analysis.GroupingLevel.Workgroup][new GenericGroupingKeys(key, Analysis.GroupingLevel.Workgroup)] = workgroup.Clone();
				groupedCounts[Analysis.DetailLevel.Customer_Entity][Analysis.GroupingLevel.Customer_Entity][new GenericGroupingKeys(key, Analysis.GroupingLevel.Customer_Entity)] = workgroup.Clone();
				groupedCounts[Analysis.DetailLevel.FI_Entity][Analysis.GroupingLevel.FI_Entity][new GenericGroupingKeys(key, Analysis.GroupingLevel.FI_Entity)] = workgroup.Clone();
			}

			// Summarize by all grouping levels (there is some "wasted" effort here if we aren't reporting on all levels -- but that should have minimal impact...)
			foreach (var entry in groupedCounts[Analysis.DetailLevel.Customer_Entity][Analysis.GroupingLevel.Customer_Entity])
			{
				Count(groupedCounts[Analysis.DetailLevel.Customer_Entity][Analysis.GroupingLevel.FI_Entity], new GenericGroupingKeys(entry.Key, Analysis.GroupingLevel.FI_Entity), entry.Value);
				Count(groupedCounts[Analysis.DetailLevel.Customer_Entity][Analysis.GroupingLevel.Bank], new GenericGroupingKeys(entry.Key, Analysis.GroupingLevel.Bank), entry.Value);
			}

			foreach (var entry in groupedCounts[Analysis.DetailLevel.Workgroup][Analysis.GroupingLevel.Workgroup])
			{
				Count(groupedCounts[Analysis.DetailLevel.Workgroup][Analysis.GroupingLevel.FI_Entity], new GenericGroupingKeys(entry.Key, Analysis.GroupingLevel.FI_Entity), entry.Value);
				Count(groupedCounts[Analysis.DetailLevel.Workgroup][Analysis.GroupingLevel.Bank], new GenericGroupingKeys(entry.Key, Analysis.GroupingLevel.Bank), entry.Value);
				Count(groupedCounts[Analysis.DetailLevel.Workgroup][Analysis.GroupingLevel.Customer_Entity], new GenericGroupingKeys(entry.Key, Analysis.GroupingLevel.Customer_Entity), entry.Value);
			}

			// Create the line items at the desired grouping levels
			foreach (var activityInfo in activityInfos)
			{
				CreateLineItems(lineItems, groupedCounts[activityInfo.VolumeTarget][activityInfo.Grouping], new Analysis.ActivityInfo[] { activityInfo });
			}
		}

		// Call DAL and process totals for multiple groupings, of multiple statistics simultaneously
		protected void CreateLineItemsWithTotals(List<LineItemType> lineItems, List<Analysis.ActivityInfo> activityInfos, Func<IEnumerable<GenericCounts>> getDALCounts)
		{
			// Create line items for all institutions returned
			var workgroups = getDALCounts();

			// Set FI if necessary
			AssignBankAndFI(ref workgroups);

			// Filter out FI-related activities
			FilterFIActivities(ref workgroups);

			// Flatten entity list to appropriate level
			_config.API.ConvertEntitiesToCustomers(workgroups, ref _raamState);

			// Split activities by grouping level
			Dictionary<Analysis.GroupingLevel, List<Analysis.ActivityInfo>> activityGroups = new Dictionary<Analysis.GroupingLevel, List<Analysis.ActivityInfo>>();
			foreach (var activityInfo in activityInfos)
			{
				if (!activityGroups.ContainsKey(activityInfo.Grouping))
					activityGroups[activityInfo.Grouping] = new List<Analysis.ActivityInfo>();
				activityGroups[activityInfo.Grouping].Add(activityInfo);
			}

			// Sort activities to match the order counts are returned in
			foreach (var group in activityGroups)
			{
				group.Value.Sort((o1, o2) => o1.VolumeTarget.CompareTo(o2.VolumeTarget));
			}

			// Track at different grouping levels
			Dictionary<Analysis.GroupingLevel, Dictionary<GenericGroupingKeys, GenericCounts>> groupedTotals = new Dictionary<Analysis.GroupingLevel, Dictionary<GenericGroupingKeys, GenericCounts>>();
			foreach (var grouping in new Analysis.GroupingLevel[] { Analysis.GroupingLevel.FI_Entity, Analysis.GroupingLevel.Bank, Analysis.GroupingLevel.Customer_Entity, Analysis.GroupingLevel.Workgroup })
			{
				groupedTotals[grouping] = new Dictionary<GenericGroupingKeys, GenericCounts>();
			}

			// Summarize by all grouping levels (there is some "wasted" effort here if we aren't reporting on all levels -- but that should have minimal impact...)
			foreach (var workgroup in workgroups)
			{
				var key = new GenericGroupingKeys(workgroup.FIEntityID, workgroup.SiteBankID, workgroup.CustomerEntityID, workgroup.WorkgroupID);
				Sum(groupedTotals[Analysis.GroupingLevel.FI_Entity], new GenericGroupingKeys(key, Analysis.GroupingLevel.FI_Entity), workgroup);
				Sum(groupedTotals[Analysis.GroupingLevel.Bank], new GenericGroupingKeys(key, Analysis.GroupingLevel.Bank), workgroup);
				Sum(groupedTotals[Analysis.GroupingLevel.Customer_Entity], new GenericGroupingKeys(key, Analysis.GroupingLevel.Customer_Entity), workgroup);
				Sum(groupedTotals[Analysis.GroupingLevel.Workgroup], new GenericGroupingKeys(key, Analysis.GroupingLevel.Workgroup), workgroup);
			}

			// Create the line items:
			//	By Grouping Level
			//		In order - ActivityInfos are sorted in the same order as the totals array...
			foreach (var activityList in activityGroups.Values)
			{
				CreateLineItems(lineItems, groupedTotals[activityList[0].Grouping], activityList);
			}
		}

		// Call DAL and process partitioned counts for multiple groupings
		protected void CreateSimpleRetentionCounts(List<LineItemType> lineItems, List<Analysis.ActivityInfo> activityInfos,
			Func<Analysis.ActivityInfo, bool> retentionValid, Func<IList<Analysis.RetentionValue>, IEnumerable<GenericCountsByRetention>> getDALCounts)
		{
			// Validate retention ranges / units
			if (retentionValid != null)
			{
				foreach (var info in activityInfos)
				{
					if (!retentionValid(info))
						throw new Exception("Unexpected volume tracking activity retention definition for " + getDALCounts.Method.Name);
				}
			}

			// Extract the retention cutoffs, and pass them into the DAL
			var retentionCutoffs = activityInfos.Select(o => o.Retention).Distinct().ToList();
			var workgroupCounts = getDALCounts(retentionCutoffs);

			// Flatten entity list to appropriate level
			_config.API.ConvertEntitiesToCustomers(workgroupCounts, ref _raamState);

			// Track at different grouping levels
			Dictionary<Analysis.GroupingLevel, Dictionary<Analysis.RetentionValue, Dictionary<GenericGroupingKeys, GenericCounts>>> groupedParitionedTotals = new Dictionary<Analysis.GroupingLevel, Dictionary<Analysis.RetentionValue, Dictionary<GenericGroupingKeys, GenericCounts>>>();
			foreach (var grouping in new Analysis.GroupingLevel[] { Analysis.GroupingLevel.FI_Entity, Analysis.GroupingLevel.Bank, Analysis.GroupingLevel.Customer_Entity, Analysis.GroupingLevel.Workgroup })
			{
				groupedParitionedTotals[grouping] = new Dictionary<Analysis.RetentionValue, Dictionary<GenericGroupingKeys, GenericCounts>>();
				foreach (var retention in retentionCutoffs)
				{
					groupedParitionedTotals[grouping][retention] = new Dictionary<GenericGroupingKeys, GenericCounts>();
				}
			}

			// Summarize by all grouping levels (there is some "wasted" effort here if we aren't reporting on all levels -- but that should have minimal impact...)
			foreach (var workgroup in workgroupCounts)
			{
				var key = new GenericGroupingKeys(workgroup.FIEntityID, workgroup.SiteBankID, workgroup.CustomerEntityID, workgroup.WorkgroupID);
				Sum(groupedParitionedTotals[Analysis.GroupingLevel.FI_Entity][workgroup.RetentionCutoff], new GenericGroupingKeys(key, Analysis.GroupingLevel.FI_Entity), workgroup);
				Sum(groupedParitionedTotals[Analysis.GroupingLevel.Bank][workgroup.RetentionCutoff], new GenericGroupingKeys(key, Analysis.GroupingLevel.Bank), workgroup);
				Sum(groupedParitionedTotals[Analysis.GroupingLevel.Customer_Entity][workgroup.RetentionCutoff], new GenericGroupingKeys(key, Analysis.GroupingLevel.Customer_Entity), workgroup);
				Sum(groupedParitionedTotals[Analysis.GroupingLevel.Workgroup][workgroup.RetentionCutoff], new GenericGroupingKeys(key, Analysis.GroupingLevel.Workgroup), workgroup);
			}

			// Split activities by grouping level
			Dictionary<Analysis.GroupingLevel, List<Analysis.ActivityInfo>> activityGroups = new Dictionary<Analysis.GroupingLevel, List<Analysis.ActivityInfo>>();
			foreach (var activityInfo in activityInfos)
			{
				if (!activityGroups.ContainsKey(activityInfo.Grouping))
					activityGroups[activityInfo.Grouping] = new List<Analysis.ActivityInfo>();
				activityGroups[activityInfo.Grouping].Add(activityInfo);
			}

			// Create the line items
			//	By Grouping
			//		targeting the correct retention level
			foreach (var activityList in activityGroups.Values)
			{
				foreach (var retention in retentionCutoffs)
				{
					CreateLineItems(lineItems, groupedParitionedTotals[activityList[0].Grouping][retention], activityList.Where(o => o.Retention == retention).ToList());
				}
			}
		}

		// Call DAL and process partitioned counts for multiple groupings, accumulating down partition hierarchy
		protected void CreateCumulativeTierRetentionCounts(List<LineItemType> lineItems, List<Analysis.ActivityInfo> activityInfos,
			Func<Analysis.ActivityInfo, bool> retentionValid, Func<IList<Analysis.RetentionValue>, IEnumerable<GenericCountsByRetention>> getDALCounts)
		{
			// Validate retention ranges / units
			if (retentionValid != null)
			{
				foreach (var info in activityInfos)
				{
					if (!retentionValid(info))
						throw new Exception("Unexpected volume tracking activity retention definition for " + getDALCounts.Method.Name);
				}
			}

			// Make sure this list is sorted for ease of processing the tiers
			activityInfos.Sort((o1, o2) => o1.Retention.CompareTo(o2.Retention));

			// Extract the retention cutoffs, and pass them into the DAL
			var retentionCutoffs = activityInfos.Select(o => o.Retention).Distinct().ToList();
			var workgroupCounts = getDALCounts(retentionCutoffs);

			// Flatten entity list to appropriate level
			_config.API.ConvertEntitiesToCustomers(workgroupCounts, ref _raamState);

			// Track at different grouping levels
			Dictionary<Analysis.GroupingLevel, Dictionary<Analysis.RetentionValue, Dictionary<GenericGroupingKeys, GenericCounts>>> groupedParitionedTotals = new Dictionary<Analysis.GroupingLevel, Dictionary<Analysis.RetentionValue, Dictionary<GenericGroupingKeys, GenericCounts>>>();
			foreach (var grouping in new Analysis.GroupingLevel[] { Analysis.GroupingLevel.FI_Entity, Analysis.GroupingLevel.Bank, Analysis.GroupingLevel.Customer_Entity, Analysis.GroupingLevel.Workgroup })
			{
				groupedParitionedTotals[grouping] = new Dictionary<Analysis.RetentionValue, Dictionary<GenericGroupingKeys, GenericCounts>>();
				foreach (var retention in retentionCutoffs)
				{
					groupedParitionedTotals[grouping][retention] = new Dictionary<GenericGroupingKeys, GenericCounts>();
				}
			}

			// Summarize by all grouping levels (there is some "wasted" effort here if we aren't reporting on all levels -- but that should have minimal impact...)
			foreach (var workgroup in workgroupCounts)
			{
				var key = new GenericGroupingKeys(workgroup.FIEntityID, workgroup.SiteBankID, workgroup.CustomerEntityID, workgroup.WorkgroupID);
				Sum(groupedParitionedTotals[Analysis.GroupingLevel.FI_Entity][workgroup.RetentionCutoff], new GenericGroupingKeys(key, Analysis.GroupingLevel.FI_Entity), workgroup);
				Sum(groupedParitionedTotals[Analysis.GroupingLevel.Bank][workgroup.RetentionCutoff], new GenericGroupingKeys(key, Analysis.GroupingLevel.Bank), workgroup);
				Sum(groupedParitionedTotals[Analysis.GroupingLevel.Customer_Entity][workgroup.RetentionCutoff], new GenericGroupingKeys(key, Analysis.GroupingLevel.Customer_Entity), workgroup);
				Sum(groupedParitionedTotals[Analysis.GroupingLevel.Workgroup][workgroup.RetentionCutoff], new GenericGroupingKeys(key, Analysis.GroupingLevel.Workgroup), workgroup);
			}

			// Accumulate for the retention partitions
			foreach (var grouping in groupedParitionedTotals.Values)
			{
				for (int i = 1; i < retentionCutoffs.Count; i++)		// Start from the beginning of the list, skipping the first entry
				{
					var sourceValues = grouping[retentionCutoffs[i]];
					for (int j = 0; j < i; j++)							// From that point, enumerate every entry before it ("lesser retention") in order
					{
						var targetValues = grouping[retentionCutoffs[j]];
						foreach (var entry in sourceValues)
						{
							if (!targetValues.ContainsKey(entry.Key))
							{
								targetValues[entry.Key] = entry.Value.Clone();
							}
							else
							{
								for (int k = 0; k < entry.Value.Counts.Length; k++)
								{
									targetValues[entry.Key].Counts[k] = targetValues[entry.Key].Counts[k] + entry.Value.Counts[k];
								}
							}
						}
					}
				}
			}

			// Split activities by grouping level
			Dictionary<Analysis.GroupingLevel, List<Analysis.ActivityInfo>> activityGroups = new Dictionary<Analysis.GroupingLevel, List<Analysis.ActivityInfo>>();
			foreach (var activityInfo in activityInfos)
			{
				if (!activityGroups.ContainsKey(activityInfo.Grouping))
					activityGroups[activityInfo.Grouping] = new List<Analysis.ActivityInfo>();
				activityGroups[activityInfo.Grouping].Add(activityInfo);
			}

			// Create the line items
			//	By Grouping
			//		targeting the (pre-accumulated) retention level
			foreach (var activityList in activityGroups.Values)
			{
				foreach (var retention in retentionCutoffs)
				{
					CreateLineItems(lineItems, groupedParitionedTotals[activityList[0].Grouping][retention], activityList.Where(o => o.Retention == retention).ToList());
				}
			}
		}

		// Sample Retention validation functions
		public static bool RetentionInDays(Analysis.ActivityInfo info) { return info.Retention.Units == Analysis.RetentionValue.Unit.Days; }
		public static bool RetentionZeroOrInYears(Analysis.ActivityInfo info) { return info.Retention.Value == 0 || info.Retention.Units == Analysis.RetentionValue.Unit.Years; }

		#endregion

		#region Data Cleanup / Filtering Utilities

		Dictionary<int, int> _bankCache = new Dictionary<int, int>();
		Dictionary<int, Tuple<string, string, string>> _billingFieldCache = new Dictionary<int, Tuple<string, string, string>>();

		protected void AssignBankAndFI(ref IEnumerable<GenericCounts> entities)
		{
			// FI Entity is usually determined from the database.  However, if there are some database rows that only contain an Entity ID (no Bank ID),
			//  we need to auto-assign a bank.
			// Note: Those records must have been sorted to the beginning of the list.
			if (entities.Any() && entities.First().FIEntityID <= 0)
			{
				// Do code-level filtering, if required, since the DB didn't know all the FI Entity IDs...
				HashSet<int> targets = null;
				if (_config.ExtractParams.TargetEntity != "*")
					targets = new HashSet<int>(_config.ExtractParams.TargetEntities.Select(o => int.Parse(o)));

				// Use the RAAM Utilities to select the FI Entity.
				// Then do a database lookup on all of the FI Entities to get the first bank associated with them
				// Only check the ones we don't know (beginning of list)
				var entitiesWithFIs = _config.API.GetFIEntities(entities.TakeWhile(o => o.FIEntityID <= 0), ref _raamState);
				List<GenericCounts> filteredEntities = new List<GenericCounts>();
				int index = 0;
				foreach (var entity in entities)
				{
					if (index < entitiesWithFIs.Count)
					{
						// Set IDs
						entity.FIEntityID = entitiesWithFIs[index].FIEntityID;
						if (entity.FIEntityID > 0)
						{
							if (!_bankCache.ContainsKey(entity.FIEntityID))
							{
								int siteBankId = _config.DAL.GetFirstBank(entity.FIEntityID);
								_bankCache[entity.FIEntityID] = siteBankId;
							}
							entity.SiteBankID = _bankCache[entity.FIEntityID];
						}

						// Check for filtering
						if (targets == null || targets.Contains(entity.FIEntityID))
							filteredEntities.Add(entity);
					}
					else
					{
						// These should have been pre-filtered by the database...
						filteredEntities.Add(entity);
					}
					index++;
				}

				// Replace with (potentially filtered) result list...
				entities = filteredEntities;
			}
		}

		protected void FilterFIActivities(ref IEnumerable<GenericCounts> entities)
		{
			// Activities that an FI / Operator do for their own trouble-shooting should not be passed on to their corporate customers.
			// So if the Logon Entity ID is provided, filter out those records
			if (entities.Any(o => o.LogonEntityID > 0))
			{
				// Use the RAAM Utilities to determine Entity Type and filter.
				List<GenericCounts> corporates = new List<GenericCounts>();
				foreach (var entity in entities)
				{
					if (entity.LogonEntityID <= 0 || _config.API.IsCorporate(entity.LogonEntityID, ref _raamState))
						corporates.Add(entity);
				}
				entities = corporates;
			}
		}

		protected void GetBillingFields(int entityID, out string billingAccount, out string billingField1, out string billingField2)
		{
			if (!_billingFieldCache.ContainsKey(entityID))
			{
				_config.DAL.GetBillingFields(entityID, out billingAccount, out billingField1, out billingField2);
				_billingFieldCache[entityID] = Tuple.Create(billingAccount, billingField1, billingField2);
			}

			var fields = _billingFieldCache[entityID];
			billingAccount = fields.Item1;
			billingField1 = fields.Item2;
			billingField2 = fields.Item3;
		}

		#endregion

		#region Simple Aggregation Helper Functions

		protected static void Count<T>(Dictionary<T, GenericCounts> results, T key, GenericCounts current)
		{
			// This is a count - so add one per entry (no pre-totalled values...  counts array only contains one value...)
			if (results.ContainsKey(key))
			{
				results[key].Count = results[key].Count + 1;
			}
			else
			{
				results[key] = current.Clone();
				results[key].Count = 1;
			}
		}

		protected static void Sum<T>(Dictionary<T, GenericCounts> results, T key, GenericCounts values)
		{
			// This is a sum - so add the values to the entry with this key
			if (!results.ContainsKey(key))
			{
				results[key] = values.Clone();
			}
			else
			{
				for (int i = 0; i < values.Counts.Length; i++)
				{
					results[key].Counts[i] = results[key].Counts[i] + values.Counts[i];
				}
			}
		}

		#endregion

		#region Standard Verification Methods with Meaningful Error Messages

		protected static void VerifyTarget(Analysis.ActivityInfo activityInfo, Analysis.DetailLevel requiredTarget, string context)
		{
			if (activityInfo.VolumeTarget != requiredTarget)
				throw new Exception("Invalid Activity configuration - Target = " + activityInfo.VolumeTarget + " (" + context + ")");
		}

		protected static void VerifyTargetIn(Analysis.ActivityInfo activityInfo, IList<Analysis.DetailLevel> requiredTargetSet, string context)
		{
			if (!requiredTargetSet.Contains(activityInfo.VolumeTarget))
				throw new Exception("Invalid Activity configuration - Target = " + activityInfo.VolumeTarget + " (" + context + ")");
		}

		protected static void VerifyTargets(IList<Analysis.ActivityInfo> activityInfos, List<Analysis.DetailLevel> requiredTargets, string context)
		{
			// Split activities by grouping level
			Dictionary<Analysis.GroupingLevel, List<Analysis.ActivityInfo>> activityGroups = new Dictionary<Analysis.GroupingLevel, List<Analysis.ActivityInfo>>();
			foreach (var activityInfo in activityInfos)
			{
				if (!activityGroups.ContainsKey(activityInfo.Grouping))
					activityGroups[activityInfo.Grouping] = new List<Analysis.ActivityInfo>();
				activityGroups[activityInfo.Grouping].Add(activityInfo);
			}

			// Now verify each group
			foreach (var activityList in activityGroups.Values)
			{
				if (activityList.Count != requiredTargets.Count)
					throw new Exception(string.Format("Invalid Activity configuration - Requires {0} targets, but has {1} ({2}; {3})", activityList.Count, requiredTargets.Count, context, activityList[0].Grouping));

				var availableTargets = new List<Analysis.DetailLevel>(requiredTargets);
				foreach (var activityInfo in activityList)
				{
					int index = availableTargets.IndexOf(activityInfo.VolumeTarget);
					if (index >= 0)
						availableTargets.RemoveAt(index); // Don't reuse match
					else
						throw new Exception("Invalid Activity configuration - Target Not Expected: " + activityInfo.VolumeTarget + " (" + context + ")");
				}
			}
		}
		#endregion
	}
}
