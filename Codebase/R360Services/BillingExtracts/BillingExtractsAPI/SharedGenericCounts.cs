﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BillingExtracts.API
{
	public class GenericGroupingKeys
	{
		// Key values
		public int FIEntityID { get; set; }
		public int SiteBankID { get; set; }
		public int EntityID { get; set; }
		public int WorkgroupID { get; private set; }	// Note: Workgroup is immutable; we use it as the hash code...

		public GenericGroupingKeys(int fiEntityID = 0, int siteBankID = 0, int entityID = 0, int workgroupID = 0)
		{
			FIEntityID = fiEntityID;
			SiteBankID = siteBankID;
			EntityID = entityID;
			WorkgroupID = workgroupID;
		}

		public GenericGroupingKeys(GenericGroupingKeys source, Analysis.GroupingLevel level)
			: this()
		{
			// Grouping levels are inclusive -- this level or greater implies them...
			if (level >= Analysis.GroupingLevel.FI_Entity)			FIEntityID = source.FIEntityID;
			if (level >= Analysis.GroupingLevel.Bank)				SiteBankID = source.SiteBankID;
			if (level >= Analysis.GroupingLevel.Customer_Entity)	EntityID = source.EntityID;
			if (level >= Analysis.GroupingLevel.Workgroup)			WorkgroupID = source.WorkgroupID;
		}

		public override bool Equals(object obj)
		{
			if (obj is GenericGroupingKeys)
			{
				var o = obj as GenericGroupingKeys;
				return FIEntityID.Equals(o.FIEntityID)
					&& SiteBankID.Equals(o.SiteBankID)
					&& EntityID.Equals(o.EntityID)
					&& WorkgroupID.Equals(o.WorkgroupID);
			}
			else
			{
				return base.Equals(obj);
			}
		}

		public override int GetHashCode()
		{
			return WorkgroupID;	// Fields more generic than Workgroup are mutable...  And if workgroup is populated it should be relatively unique...
		}
	}

	public class GenericCounts : GenericGroupingKeys, IEntityID
	{
		// "Customer"
		private int? _customerEntityID;
		public int CustomerEntityID
		{
			get
			{
				return _customerEntityID ?? EntityID;
			}
			set
			{
				_customerEntityID = value;
			}
		}

		// Event properties
		public int LogonEntityID { get; set; }

		// Workgroup properties
		public string WorkgroupName { get; set; }
		public string BillingAccount { get; set; }
		public string BillingField1 { get; set; }
		public string BillingField2 { get; set; }

		// The Count(s)
		public int Count { get { return Counts[0]; } set { Counts[0] = value; } }
		public int[] Counts { get; private set; }


		public GenericCounts(int fiEntityID = 0, int siteBankID = 0, int entityID = 0, int workgroupID = 0)
			: base(fiEntityID, siteBankID, entityID, workgroupID)
		{
			Counts = new int[1];
		}

		public GenericCounts(int[] counts, int fiEntityID = 0, int siteBankID = 0, int entityID = 0, int workgroupID = 0)
			: base(fiEntityID, siteBankID, entityID, workgroupID)
		{
			Counts = counts;
		}

		public GenericCounts Clone()
		{
			// Create a copy of counts - we don't want to share references...
			int[] newCounts = new int[this.Counts.Length];
			Array.Copy(this.Counts, newCounts, newCounts.Length);

			// Do a shallow copy - then replace the counts reference with our copy.
			var result = this.MemberwiseClone() as GenericCounts;
			result.Counts = newCounts;

			return result;
		}
	}

	public class GenericCountsByRetention : GenericCounts
	{
		public Analysis.RetentionValue RetentionCutoff { get; set; }

		public GenericCountsByRetention(int fiEntityID = 0, int siteBankID = 0, int entityID = 0, int workgroupID = 0)
			: base(fiEntityID, siteBankID, entityID, workgroupID)
		{
		}

		public GenericCountsByRetention(int[] counts, int fiEntityID = 0, int siteBankID = 0, int entityID = 0, int workgroupID = 0)
			: base(counts, fiEntityID, siteBankID, entityID, workgroupID)
		{
		}
	}
}
