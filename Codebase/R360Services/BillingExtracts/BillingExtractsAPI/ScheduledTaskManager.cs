﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using WFS.RecHub.Common;

namespace WFS.RecHub.BillingExtracts.API
{
	internal static class ScheduledTaskManager
	{
		private const string SCHEDULEDTASKSCONFIGFILENAME = "StandardScheduledTasks.config";
		private static object _syncLock = new object();
		private static Thread _schedulerThread = null;
		internal static IBillingAPI _api = null;
		internal static ManualResetEvent _cancelThreadEvent = new ManualResetEvent(false);	// Exposed for testing

		public static void Init(IBillingAPI api)
		{
			lock (_syncLock)
			{
				// Run a background thread to execute scheduled tasks
				if (_schedulerThread == null)
				{
					_api = api;
					_schedulerThread = new Thread(ScheduleManagerThread);
					_schedulerThread.IsBackground = true;
					_schedulerThread.Name = "BillingScheduler";
					_schedulerThread.Start();
				}
			}
		}

		private static void ScheduleManagerThread()
		{
			// This is the background thread that manages scheduled activities.
			// It must run "forever" while the service is up
			_api.Log(string.Format("Billing Background Scheduler: Started..."), "BillingSched", MessageType.Information, MessageImportance.Verbose);

			// Check for scheduled tasks every 5 minutes
			TimeSpan _processInterval = TimeSpan.FromMinutes(5);
			while (true)
			{
				try
				{
					CheckForScheduledWork();
				}
				catch (ThreadAbortException)
				{
					// This is a background thread; An "abort" is a normal activity during service shutdown
					break;
				}
				catch (Exception ex)
				{
					_api.Log(string.Format("Error in Billing Background Scheduler: {0}", ex.ToString()), "BillingSched", MessageType.Error, MessageImportance.Essential);
				}

				// Always pause five minutes between tasks (error, or not...)
				// That means the worst case is that ~300 messages will get logged per day if it fails on every loop...
				if (_cancelThreadEvent.WaitOne(_processInterval))
					break;
			}

			// Cleanup
			_schedulerThread = null;
			_api.Log(string.Format("Billing Background Scheduler: Stopped"), "BillingSched", MessageType.Information, MessageImportance.Verbose);
		}

		private static void CheckForScheduledWork()
		{
			// There is a standard set of "pre-defined" billing tasks that should be run on a near-daily basis - to "accumulate" data for other billing functions
			// In addition, we'd like to support the ability for "custom" tasks, in case there is a billing format that requires some sort of non-standard accumulation / analysis task.

			// So:
			//	- Load the local configuration, to see when standard tasks should run ("StandardScheduledTasks.config")
			//	- Load the Custom DLLs, and ask about scheduled tasks

			// 1. Standard Tasks
			List<BillingBackgroundTask> standardTasks = new List<BillingBackgroundTask>();
			string basePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
			string fullPath = Path.Combine(basePath, BillingAPI.CUSTOMFILEGENERATORFOLDER, SCHEDULEDTASKSCONFIGFILENAME);
			if (File.Exists(fullPath))
			{
				// <><Task .../><Task .../></>
				XDocument xmlConfig = XDocument.Load(fullPath);
				foreach (var element in xmlConfig.Root.Elements("Task"))
				{
					standardTasks.Add(BillingBackgroundTask.FromXML(element));
				}

				_api.Log("Checking " + standardTasks.Count.ToString() + " tasks for work...", "BillingSched", MessageType.Information, MessageImportance.Debug);
			}
			else
			{
				_api.Log("Standard Tasks Scheduler File does not exist: " + fullPath, "BillingSched", MessageType.Information, MessageImportance.Verbose);
			}

			foreach (var task in standardTasks)
			{
				task.DoWork(_api);
			}

			// 2. Custom DLLs
			var customGenerators = _api.GetAllGeneratorClasses();
			foreach (var customGenerator in customGenerators)
			{
				customGenerator.PerformScheduledWork(_api, standardTasks.Select(o => o.TaskType));
			}
		}
	}
}
