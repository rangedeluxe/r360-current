﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WFS.RecHub.BillingExtracts.API
{
	public abstract class SharedExtractParametersBase
	{
		public Tuple<DateTime, DateTime> DateRange { get; private set; }

		public string TargetEntity { get; private set; }
		public string TargetEntityDesc { get { return TargetEntity.Replace(';', '_').Replace("*", "ALL"); } }
		public string[] TargetEntities { get { return TargetEntity.Split(';'); } }

		protected readonly SharedConfigParametersBase _config;
		protected readonly XDocument _xml;
		protected SharedExtractParametersBase(SharedConfigParametersBase config, string xmlParameters)
		{
			_config = config;
			_xml = XDocument.Load(new StringReader(xmlParameters));
			DateRange = config.API.GetOptionalDates(_xml.Root, "StartDate", "EndDate", config.API.GetLastMonth);
			TargetEntity = config.API.GetOptionalString(_xml.Root, "Entity", "*");
		}
	}
}
