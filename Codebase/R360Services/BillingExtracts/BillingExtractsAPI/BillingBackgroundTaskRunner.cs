﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.BillingExtracts.API
{
	internal static class BillingBackgroundTaskRunner
	{
		private static readonly object _syncLock = new object();
		private static readonly Dictionary<string, DateTime> _runList = new Dictionary<string, DateTime>();

		public static bool CheckForWork(BillingBackgroundTask task, IBillingAPI api, DateTime now)
		{
			// Check local memory for last time this task was run
			DateTime? lastRunTime;
			lock (_syncLock)
			{
				lastRunTime = _runList.ContainsKey(task.Description) ? _runList[task.Description] : (DateTime?)null;
			}
			if (lastRunTime.HasValue)
			{
				DateTime nextRun = GetNextRun(lastRunTime.Value, task.RunTimeOfDay, task.RunDays);
				if (nextRun > now)
					return false;
			}
			else if (api.RerunAllScheduledTasksOnStartup)
			{
				// It is not in memory, which means we have not run this task since the service started...
				return true;
			}

			// Check database for last time this task was run
			api.GetDAL().GetLastRunTime(task.Description, out lastRunTime);
			if (lastRunTime.HasValue)
			{
				// Remember to convert from UTC times (returned from the database...)
				lastRunTime = lastRunTime.Value.ToLocalTime();

				// Update in-memory cache
				lock (_syncLock)
				{
					_runList[task.Description] = lastRunTime.Value;
				}

				// Check and see if we should run now
				DateTime nextRun = GetNextRun(lastRunTime.Value, task.RunTimeOfDay, task.RunDays);
				if (nextRun > now)
					return false;
			}

			// If it is time to run this task (i.e. we got here), return true
			return true;
		}

		public static void ExecuteTask(BillingBackgroundTask task, IBillingAPI api)
		{
			DateTime today = DateTime.Today; // TODO: Support mocking this for testing?
			bool executed = false;

			// Execute the task
			switch (task.TaskType)
			{
				case BillingBackgroundTaskTypes.SummarizeExceptionsEnabledCustomers:
					api.GetDAL().BeginTask(out executed, task.Description);
					IList<EntitySimple> customers = null;
					if (executed)
					{
						try
						{
							customers = RAAMUtilities.GetExceptionEnabledCustomers(api);
						}
						catch (Exception ex)
						{
							executed = false;
							api.Log("Error while retrieving Exception Enabled Customers:\r\n" + ex.ToString(), "BillingSched", MessageType.Error, MessageImportance.Essential);
							api.GetDAL().UnlockTask(task.Description);
						}
					}
					if (executed)
					{
						api.GetDAL().StoreExceptionCustomers(task.Description, customers.Select(o => o.EntityID));
					}
					break;

				default:
					api.GetDAL().SummarizeFacts(out executed, task.TaskType.ToString(), task.Description, today.AddDays(task.TargetDay));
					break;
			}

			// If a task was executed, store the current time in our local memory cache
			if (executed)
			{
				StoreTaskRunTime(task.Description, DateTime.Now);
				api.Log("Executed task: " + task.TaskType.ToString(), "BillingSched", MessageType.Information, MessageImportance.Verbose);
			}
			else
			{
				api.Log("Task (" + task.TaskType.ToString() + ") was skipped - database reported error or already locked", "BillingSched", MessageType.Information, MessageImportance.Verbose);
			}
		}

		#region Testing Framework

		/// <summary>
		/// This method is provided for testing
		/// </summary>
		internal static void ClearInMemoryRunHistory()
		{
			lock (_syncLock)
			{
				_runList.Clear();
			}
		}

		/// <summary>
		/// This method is exposed for test purposes only
		/// </summary>
		internal static void StoreTaskRunTime(string taskDescription, DateTime completionTime)
		{
			// Record completion in memory
			lock (_syncLock)
			{
				_runList[taskDescription] = completionTime;
			}
		}

		/// <summary>
		/// Calculate next run time based on last run time (this is only exposed for testing the logic)
		/// </summary>
		internal static DateTime GetNextRun(DateTime lastRunTime, TimeSpan runTimeOfDay, IEnumerable<DayOfWeek> runDaysOfWeek)
		{
			DateTime nextRun = lastRunTime;

			// Does the "time-of-day" match a run time?
			if (nextRun.TimeOfDay >= runTimeOfDay)
			{
				// Advance to next day (set to correct time-of-day)
				nextRun = nextRun.Date + TimeSpan.FromDays(1) + runTimeOfDay;
			}
			else
			{
				// Stay on same day (set to correct time-of-day)
				nextRun = nextRun.Date + runTimeOfDay;
			}

			// Are we currently on a day-of-week that we should run?
			while (!runDaysOfWeek.Contains(nextRun.DayOfWeek))
			{
				// Advance to next day 
				nextRun = nextRun + TimeSpan.FromDays(1);
			}

			// We should be at a valid date/time to run
			return nextRun;
		}

		#endregion
	}
}
