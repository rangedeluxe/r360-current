﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.R360RaamClient;

namespace WFS.RecHub.BillingExtracts.API
{
	internal static class RAAMUtilities
	{
		private class ProcessState
		{
			public IRaamClient Client { get; private set; }	
			public readonly Dictionary<int, SimpleEntity> Cache = new Dictionary<int, SimpleEntity>();
			public ProcessState(IBillingAPI api)
			{
				Client = api.GetRaamClient();
			}
		}

		private class SimpleEntity
		{
			public int ID { get; set; }
			public string EntityTypeCode { get; set; }
			public SimpleEntity Parent { get; set; }
		}

		internal static void ConvertEntitiesToCustomers(IEnumerable<IEntityID> entities, ref object sharedState, IBillingAPI api)
		{
			// Use the RAAM Entity Hierarchy to "flatten" this list by updating entities of type "LOB" with their parent "Customer" entity.
			// It will be up to the calling components to actually aggregate the results, based on their grouping needs...
			ProcessState state = sharedState as ProcessState;
			if (state == null) state = new ProcessState(api);
			foreach (var entity in entities)
			{
				// Get ancestry
				var ancestry = GetAncestry(state, entity.EntityID);
				if (ancestry != null)
				{
					// Flatten LOB types
					while (ancestry.EntityTypeCode == "LOB")
						ancestry = ancestry.Parent;

					entity.CustomerEntityID = ancestry.ID;
				}
				else
				{
					// If the entity did not exist, try to ignore / suppress it (no error...)
					api.Log(string.Format("Entity {0} was not found in RAAM - all associated workgroup data should be ignored.", entity.EntityID),
						"BillingRAAM", RecHub.Common.MessageType.Warning, RecHub.Common.MessageImportance.Essential);
					entity.CustomerEntityID = -1;
				}
			}
		}

		private static SimpleEntity GetAncestry(ProcessState state, int entityID)
		{
			// Check for cached value
			if (state.Cache.ContainsKey(entityID))
				return state.Cache[entityID];

			// Get ancestors from RAAM
			var hierarchy = state.Client.GetAncestorEntityHierarchy(entityID);

			// Reverse direction for easier lookup
			SimpleEntity entity = null;
			var current = hierarchy;
			while (current != null)
			{
				entity = new SimpleEntity { ID = current.ID, EntityTypeCode = current.EntityTypeCode, Parent = entity };
				state.Cache[entity.ID] = entity;

				if (current.ChildEntities == null || !current.ChildEntities.Any())
					break;

				current = current.ChildEntities.First();
			}

			// Return value
			return entity;
		}

		internal static IList<EntitySimple> GetExceptionEnabledCustomers(IBillingAPI api)
		{
			// Get all applicable entities from RAAM
			// Use the RAAM Entity Hierarchy to "flatten" this list by combining entities of type "LOB" with their parent "Customer" entity.
			ProcessState state = new ProcessState(api);
			int rootEntityId = api.GetRootEntityID();
			var hierarchy = state.Client.GetEntitiesWithPermission(rootEntityId, R360Shared.R360Permissions.Perm_ExceptionsSummary, R360Shared.R360Permissions.ActionType.View);

			List<EntitySimple> customers = new List<EntitySimple>();
			AddCustomers(customers, hierarchy);
			return customers;
		}

		private static bool AddCustomers(List<EntitySimple> customers, EntityDto entity)
		{
			// Do a depth first search
			foreach (var child in entity.ChildEntities)
			{
				if (AddCustomers(customers, child))
				{
					// Don't add "parents" of "customers" - just break out
					return true;
				}
			}

			// Now check this entity
			if (entity.EntityTypeCode == "LOB")
				return false; // This isn't customer level...

			// Otherwise - always count the leaf
			customers.Add(new EntitySimple { EntityID = entity.ID });
			return true;
		}

		internal static IList<EntityWithFI> GetFIEntities(IEnumerable<IEntityID> entities, ref object sharedState, IBillingAPI api)
		{
			ProcessState state = sharedState as ProcessState;
			if (state == null) state = new ProcessState(api);
			List<EntityWithFI> result = new List<EntityWithFI>();
			foreach (var entity in entities)
			{
				EntityWithFI withFI = new EntityWithFI() { EntityID = entity.EntityID };

				// Get ancestry
				var ancestry = GetAncestry(state, entity.EntityID);

				if (ancestry != null)
				{
					// Flatten LOB types
					while (ancestry != null && ancestry.EntityTypeCode != "FI")
						ancestry = ancestry.Parent;

					if (ancestry == null)
					{
						// If the entity did not exist, ignore / suppress it (no error...)
						api.Log(string.Format("Entity {0} did not have an FI ancestor in RAAM - cannot determine FI.", entity.EntityID),
							"BillingRAAM", RecHub.Common.MessageType.Warning, RecHub.Common.MessageImportance.Essential);
					}
					else
					{
						withFI.FIEntityID = ancestry.ID;
					}
				}
				else
				{
					// If the entity did not exist, ignore / suppress it (no error...)
					api.Log(string.Format("Entity {0} was not found in RAAM - cannot determine FI.", entity.EntityID),
						"BillingRAAM", RecHub.Common.MessageType.Warning, RecHub.Common.MessageImportance.Essential);
				}

				result.Add(withFI);
			}

			return result;
		}

		internal static bool IsCorporate(int entityId, ref object sharedState, IBillingAPI api)
		{
			ProcessState state = sharedState as ProcessState;
			if (state == null) state = new ProcessState(api);

			// Note: I don't actually care about the ancestry, but using this allows me to "opt-in" to the cached values...
			var logonEntity = GetAncestry(state, entityId);
			if (logonEntity != null)
			{
				switch (logonEntity.EntityTypeCode)
				{
					case "Corp":
					case "HC":
					case "LOB":
						return true;
				}
			}

			return false;
		}
	}
}
