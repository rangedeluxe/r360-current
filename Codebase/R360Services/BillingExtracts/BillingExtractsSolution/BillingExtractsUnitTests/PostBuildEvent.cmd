REM TargetPath = %1 
REM TargetDir = %2
REM ProjectDir = %3
REM OutDir = %4

if %4=="bin\Debug\." goto CreateDebuggingConfigAndCopy
if %4=="bin\Release\." goto CreateDebuggingConfigAndCopy
goto End

:CreateDebuggingConfigAndCopy
echo Info: Getting Debugging Configuration

IF NOT EXIST %3\..\..\BillingExtractsService\%4 GOTO NoServiceBuildDir
cd %3\..\..\BillingExtractsService\%4

IF NOT EXIST "CustomFileGenerators" GOTO NoServiceBuildDir
cd CustomFileGenerators

:HasCustomDir
copy *.config %2\

:NoServiceBuildDir

:End
echo Info: Post Build Event Complete