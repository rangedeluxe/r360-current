﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.WFSGP
{
	class WFSGPMockDAL : SharedDALBase
	{
		// WFSGP currently has no differences from the Generic CSV...
		WFS.RecHub.BillingExtracts.GenericCSV.GenericCSVMockDAL _genericDAL;

		public WFSGPMockDAL(SharedConfigParametersBase config) : base(config) 
		{
			_genericDAL = new GenericCSV.GenericCSVMockDAL(config);
		}

		public override List<GenericCounts> GetCustomers()
		{
			return _genericDAL.GetCustomers();
		}

		public override List<GenericCountsByRetention> GetItemCountsByRetention(IList<Analysis.RetentionValue> retentionCutoffs)
		{
			return _genericDAL.GetItemCountsByRetention(retentionCutoffs);
		}

		public override int GetFirstBank(int fiEntityID)
		{
			return _genericDAL.GetFirstBank(fiEntityID);
		}

		public override List<GenericCounts> GetExceptionsEnabledCustomers()
		{
			return _genericDAL.GetExceptionsEnabledCustomers();
		}

		public override List<GenericCounts> GetBulkDownloadZipCounts()
		{
			return _genericDAL.GetBulkDownloadZipCounts();
		}

		public override List<GenericCounts> GetExceptionCounts()
		{
			return _genericDAL.GetExceptionCounts();
		}

	}
}
