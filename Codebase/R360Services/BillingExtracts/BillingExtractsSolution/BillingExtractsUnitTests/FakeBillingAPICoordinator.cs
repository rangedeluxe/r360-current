﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.BillingExtracts.API;
using WFS.RecHub.BillingExtracts.API.Fakes;
using WFS.RecHub.BillingExtracts.DAL.Fakes;
using WFS.RecHub.R360RaamClient.Fakes;

namespace BillingExtractsUnitTests
{
	internal class FakeBillingAPICoordinator
	{
		public class FakeLogMessage
		{
			public string Message { get; private set; }
			public string Source { get; private set; }
			public IFileGeneratorAPI_MessageType Type { get; private set; }
			public IFileGeneratorAPI_MessageImportance Importance { get; private set; }
			public FakeLogMessage(string msg, string src, IFileGeneratorAPI_MessageType type, IFileGeneratorAPI_MessageImportance import)
			{
				Message = msg;
				Source = src;
				Type = type;
				Importance = import;
			}
		}

		private List<FakeLogMessage> _messageList = new List<FakeLogMessage>();
		public IReadOnlyList<FakeLogMessage> LogMessages
		{
			get { return _messageList; }
		}

		public IFileGeneratorAPI BuildFakeAPI_IFileGeneratorAPI()
		{
			var fakeBillingApi = BuildFakeAPI_IBillingAPI();

			var x = new StubIFileGeneratorAPI();
			x.LogEventStringStringIFileGeneratorAPI_MessageTypeIFileGeneratorAPI_MessageImportance =
					(msg, src, type, import) => _messageList.Add(new FakeLogMessage(msg, src, type, import));

			// Call through to Config Utility Methods
			x.GetOptionalBooleanXDocumentStringBooleanString = (doc, elm, def, mod) => ConfigUtilities.GetOptionalBoolean(doc, elm, def, mod, x);
			x.GetOptionalStringXDocumentStringStringString = (doc, elm, def, mod) => ConfigUtilities.GetOptionalString(doc, elm, def, mod, x);
			x.GetOptionalDatesXElementStringStringFuncOfTupleOfDateTimeDateTime = ConfigUtilities.GetOptionalDates;
			x.GetOptionalStringXElementStringString = ConfigUtilities.GetOptionalString;
			x.GetRequiredStringXDocumentStringStringString = (doc, elm, file, mod) => ConfigUtilities.GetRequiredString(doc, elm, file, mod, x);
			x.GetLastMonth = ConfigUtilities.GetLastMonth;

			// Call through to RAAM Utility Methods, using our fake RAAM client
			x.ConvertEntitiesToCustomersIEnumerableOfIEntityIDObjectRef = (IEnumerable<IEntityID> list, ref object state) => RAAMUtilities.ConvertEntitiesToCustomers(list, ref state, fakeBillingApi);
			x.GetFIEntitiesIEnumerableOfIEntityIDObjectRef = (IEnumerable<IEntityID> list, ref object state) => RAAMUtilities.GetFIEntities(list, ref state, fakeBillingApi);
			x.IsCorporateInt32ObjectRef = (int entityId, ref object state) => RAAMUtilities.IsCorporate(entityId, ref state, fakeBillingApi);

			return x;
		}

		public IBillingAPI BuildFakeAPI_IBillingAPI()
		{
			return new StubIBillingAPI()
			{
				LogStringStringMessageTypeMessageImportance = (msg, src, type, import) => _messageList.Add(new FakeLogMessage(msg, src, (IFileGeneratorAPI_MessageType)(int)type, (IFileGeneratorAPI_MessageImportance)(int)import)),
				GetDAL = () => new StubIBillingDAL()
				{
					GetLastRunTimeStringNullableOfDateTimeOut = (string desc, out DateTime? runTime) => { runTime = null; return true; }, // Just return false
				},
				GetRaamClient = () => new StubIRaamClient()
				{
					GetAncestorEntityHierarchyInt32 = (entityId) => FakeAncestry(entityId),
				},
			};
		}

		private Dictionary<int, EntityDto> _fakeEntityTree = null;

		private EntityDto FakeAncestry(int entityId)
		{
			if (_fakeEntityTree == null)
			{
				// Test Data
				_fakeEntityTree = new Dictionary<int,EntityDto>();
				EntityDto entity = new EntityDto { ID = 1, EntityTypeCode = "AdminEntity" }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 2, EntityTypeCode = "FI", ParentID = 1 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 3, EntityTypeCode = "FI", ParentID = 1 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 4, EntityTypeCode = "HC", ParentID = 2 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 5, EntityTypeCode = "Corp", ParentID = 4 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 6, EntityTypeCode = "Corp", ParentID = 4 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 7, EntityTypeCode = "Corp", ParentID = 2 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 8, EntityTypeCode = "Corp", ParentID = 3 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 9, EntityTypeCode = "LOB", ParentID = 5 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 10, EntityTypeCode = "LOB", ParentID = 5 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 11, EntityTypeCode = "LOB", ParentID = 8 }; _fakeEntityTree[entity.ID] = entity;
				entity = new EntityDto { ID = 12, EntityTypeCode = "LOB", ParentID = 8 }; _fakeEntityTree[entity.ID] = entity;
			}

			// Build the ancestry from the test data
			EntityDto result = null;
			int? target = entityId;
			while (target.HasValue && _fakeEntityTree.ContainsKey(target.Value))
			{
				var entity = _fakeEntityTree[target.Value];
				EntityDto clone = new EntityDto { ID = entity.ID, EntityTypeCode = entity.EntityTypeCode, ParentID = entity.ParentID, ChildEntities = new List<EntityDto>() };
				if (result != null)
					(clone.ChildEntities as List<EntityDto>).Add(result);
				result = clone;
				target = clone.ParentID;
			}

			return result;
		}
	}
}
