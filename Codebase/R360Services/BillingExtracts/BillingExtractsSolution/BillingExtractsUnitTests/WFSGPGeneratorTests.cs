﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BillingExtracts.API;
using WFS.RecHub.BillingExtracts.API.Fakes;
using WFS.RecHub.BillingExtracts.WFSGP;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace BillingExtractsUnitTests
{
	[TestClass]
	public class WFSGPGeneratorTests
	{
		[TestMethod]
		public void WFSGP_GenerateMockFile_CountCustomersNotLOB()
		{
			// Arrange - test class
			WFSGPGenerator generator = new WFSGPGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new WFSGPMockDAL(generator._config);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Arrange - expected results
			var list = generator._config.MockDAL.GetCustomers();
			string cust1 = GetWFSCustomerID(list[0].SiteBankID);
			string cust2 = GetWFSCustomerID(list[list.Count - 1].SiteBankID);
			int cust1Expected = 3;
			int cust2Expected = 1;
			// TODO: Should we add a 3rd customer to the mock data with a workgroup assigned to a holding company...?

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			XDocument xDoc = XDocument.Load(msInput);
			var root = xDoc.FirstNode as XElement;
			Assert.AreEqual("LineItems", root.Name);
			var lineItems = root.Elements();

			// Assert workgroups are correctly aggregated to customers
			FindLineItem(lineItems, cust1, "5220", cust1Expected);
			FindLineItem(lineItems, cust2, "5220", cust2Expected);
		}

		[TestMethod]
		public void WFSGP_GenerateMockFile_CountExceptionEnabledCustomers()
		{
			// Arrange - test class
			WFSGPGenerator generator = new WFSGPGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new WFSGPMockDAL(generator._config);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Arrange - expected results
			var list = generator._config.MockDAL.GetCustomers();
			string cust1 = GetWFSCustomerID(list[0].SiteBankID);
			string cust2 = GetWFSCustomerID(list[list.Count - 1].SiteBankID);
			int cust1Expected = 3;
			int cust2Expected = 1;

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			XDocument xDoc = XDocument.Load(msInput);
			var root = xDoc.FirstNode as XElement;
			Assert.AreEqual("LineItems", root.Name);
			var lineItems = root.Elements();

			// Assert workgroups are correctly aggregated to customers
			FindLineItem(lineItems, cust1, "5240", cust1Expected);
			FindLineItem(lineItems, cust2, "5240", cust2Expected);
		}

		[TestMethod]
		public void WFSGP_GenerateMockFile_VerifyRetentionBasedVolumes()
		{
			// Arrange - test class
			WFSGPGenerator generator = new WFSGPGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new WFSGPMockDAL(generator._config);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Arrange - expected
			var list = generator._config.MockDAL.GetCustomers();
			string fi1 = GetWFSCustomerID(list[0].SiteBankID);
			string fi2 = GetWFSCustomerID(list[list.Count - 1].SiteBankID);

			//		13	3	7
			//	5	299	133	561
			//	6	926	790	520
			//	7	425		122
			//	7	771	35	298
			//	9	385		819
			//	10	786	340	739
			//	8	846	843	
			//	11	113	194	459
			//	12	958	204	953

			int fi1expected7 = 561 + 520 + 122 + 298 + 819 + 739;
			int fi2expected7 = 459 + 953;
			int fi1expected3 = 133 + 790 + 35 + 340 + fi1expected7;
			int fi2expected3 = 843 + 194 + 204 + fi2expected7;
			int fi1expected13 = 299 + 926 + 425 + 771 + 385 + 786 + fi1expected3;
			int fi2expected13 = 846 + 113 + 958 + fi2expected3;

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			XDocument xDoc = XDocument.Load(msInput);
			var root = xDoc.FirstNode as XElement;
			Assert.AreEqual("LineItems", root.Name);
			var lineItems = root.Elements();

			// Assert exclusive activity codes for entity count calculated as expected
			//FindLineItem(lineItems, "A123", "5220", 299);
			//FindLineItem(lineItems, "A123", "5223", 133);
			//FindLineItem(lineItems, "A123", "5224", 561);
			//VerifyNoLineItem(lineItems, "C345", "5223");

			// Assert tiered activity codes for item count calculated as expected
			FindLineItem(lineItems, fi1, "5226", fi1expected13);
			FindLineItem(lineItems, fi1, "5227", fi1expected3);
			FindLineItem(lineItems, fi1, "5228", fi1expected7);
			FindLineItem(lineItems, fi2, "5226", fi2expected13);
			FindLineItem(lineItems, fi2, "5227", fi2expected3);
			FindLineItem(lineItems, fi2, "5228", fi2expected7);
		}

		[TestMethod]
		public void WFSGP_GenerateMockFile_CountBulkFileZipDownloads()
		{
			// Arrange - test class
			WFSGPGenerator generator = new WFSGPGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new WFSGPMockDAL(generator._config);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Arrange - expected results
			var list = generator._config.MockDAL.GetCustomers();
			string cust1 = GetWFSCustomerID(list[0].SiteBankID);
			string cust2 = GetWFSCustomerID(list[list.Count - 1].SiteBankID);

			//	new CustomerEntity(2, 5, 123, 5, new int[] { 3, 7 }),
			//	new CustomerEntity(2, 7, 123, 7, new int[] { 7, 17 }),
			//	new CustomerEntity(2, 1, 123, 7, new int[] { 5, 23 }),	Exclude: Logon Entity = FI
			//	new CustomerEntity(2, 9, 123, 9, new int[] { 29, 37 }),
			//	new CustomerEntity(2, 5, 123, 9, new int[] { 47, 67 }),
			//	new CustomerEntity(2, 2, 123, 9, new int[] { 103, 997 }), Exclude: Logon Entity = FI
			//	new CustomerEntity(3, 8, 456, 11, new int[] { 129, 402 }),
			int cust1ExpectedFiles = 3 + 7 + 29 + 47;
			int cust1ExpectedItems = 7 + 17 + 37 + 67;
			int cust2ExpectedFiles = 129;
			int cust2ExpectedItems = 402;

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			XDocument xDoc = XDocument.Load(msInput);
			var root = xDoc.FirstNode as XElement;
			Assert.AreEqual("LineItems", root.Name);
			var lineItems = root.Elements();

			// Assert workgroups are correctly aggregated to customers
			FindLineItem(lineItems, cust1, "5233", cust1ExpectedFiles);
			FindLineItem(lineItems, cust1, "5234", cust1ExpectedItems);
			FindLineItem(lineItems, cust2, "5233", cust2ExpectedFiles);
			FindLineItem(lineItems, cust2, "5234", cust2ExpectedItems);
		}

		[TestMethod]
		public void WFSGP_GenerateMockFile_CountExceptionTransactions()
		{
			// Arrange - test class
			WFSGPGenerator generator = new WFSGPGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new WFSGPMockDAL(generator._config);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Arrange - expected results
			var list = generator._config.MockDAL.GetCustomers();
			string cust1 = GetWFSCustomerID(list[0].SiteBankID);
			string cust2 = GetWFSCustomerID(list[list.Count - 1].SiteBankID);

			// All aggregation is done by the database - this should just be "pass through"
			//	new CustomerEntity(2, 0, 123, 0, new int[] { 100, 55, 15, 40, 1000, 104, 3, 7 }),
			//	new CustomerEntity(3, 0, 456, 0, new int[] { 10, 5, 1, 3, 20, 8, 0, 2 }),
			int cust1ExpectedTransactions = 100;
			int cust2ExpectedTransactions = 10;

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			XDocument xDoc = XDocument.Load(msInput);
			var root = xDoc.FirstNode as XElement;
			Assert.AreEqual("LineItems", root.Name);
			var lineItems = root.Elements();

			// Assert workgroups are correctly aggregated to customers
			FindLineItem(lineItems, cust1, "5241", cust1ExpectedTransactions);
			FindLineItem(lineItems, cust2, "5241", cust2ExpectedTransactions);
		}

		[TestMethod]
		public void WFSGP_GenerateMockFile_VerifyNoErrors()
		{
			// Arrange - test class
			WFSGPGenerator generator = new WFSGPGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new WFSGPMockDAL(generator._config);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);
		}

		#region Assert Utility Methods

		private void FindLineItem(IEnumerable<XElement> lineItems, string instID, string activityCode, int volume)
		{
			Assert.IsNotNull(lineItems.FirstOrDefault(
				o => o.Element("InstitutionID").Value == instID && o.Element("ActivityCode").Value == activityCode && o.Element("ActivityVolume").Value == volume.ToString()),
				"Could not find line item for InstitutionID={0}, Activity={1}, Volume={2}", instID, activityCode, volume);
		}

		private void VerifyNoLineItem(IEnumerable<XElement> lineItems, string instID, string activityCode)
		{
			Assert.IsNull(lineItems.FirstOrDefault(
				o => o.Element("InstitutionID").Value == instID && o.Element("ActivityCode").Value == activityCode),
				"Found line item for InstitutionID={0}, Activity={1}", instID, activityCode);
		}

		private string GetWFSCustomerID(int siteBankID)
		{
			// This must match the configuration file
			switch (siteBankID)
			{
				case 123:
					return "Y123";

				case 456:
					return "Y456";

				default:
					throw new Exception("Unexpected SiteBankId: " + siteBankID.ToString());
			}
		}

		#endregion
	}
}
