﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BillingExtracts.GenericCSV;
using System.IO;
using WFS.RecHub.BillingExtracts.API;
using System.Collections.Generic;

namespace BillingExtractsUnitTests
{
	[TestClass]
	public class GenericCSVGeneratorTests
	{
		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyFileBlankNoErrors()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P Entity=\"-1\" />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');
			Assert.AreEqual(2, lines.Length);	// Header line, EOF = 2 lines
		}

		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyNoDuplicateCodes()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();
			List<int> lobEntities = new List<int>(new int[] { 9, 10, 11, 12 });		// These are the entity IDs from the mock RAAM data

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');

			// Skip header, skip blank lines (footer) - compare all but last field 
			var activityCodes = lines.Skip(1).Where(o => o.Trim().Length > 0).Select(o => o.Substring(0, o.LastIndexOf(','))).ToList(); 
			CollectionAssert.AllItemsAreUnique(activityCodes);
		}

		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyPopulatedNoErrors()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');
			Assert.AreEqual(162, lines.Length);	// 162 lines in Mock Data
		}

		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyPopulatedSingleEntity_FI2_NoErrors()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P Entity=\"2\" />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');
			Assert.AreEqual(117, lines.Length);	// 117 lines in Mock Data for FI Entity 2 (I think...)
		}

		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyPopulatedSingleEntity_FI3_NoErrors()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P Entity=\"3\" />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');
			Assert.AreEqual(162 - 117 + 2, lines.Length);	// header / EOF are duplicated (2) - otherwise, should be the difference between complete file and FI 2
		}

		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyNoLOBEntities()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();
			List<int> lobEntities = new List<int>(new int[] { 9, 10, 11, 12 });		// These are the entity IDs from the mock RAAM data

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');

			var entityIDs = lines.Skip(1).Where(o => o.Trim().Length > 0).Select(o => int.Parse(o.Split(',')[4])); // Skip header, skip blank lines (footer) - entity ID is index 4 (5th field)
			foreach (var id in entityIDs)
			{
				Assert.IsFalse(lobEntities.Contains(id), "Entity ID " + id.ToString() + " found in result");
			}
		}

		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyCumulativeTotals()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();
			List<int> lobEntities = new List<int>(new int[] { 9, 10, 11, 12 });		// These are the entity IDs from the mock RAAM data

			// Arrange - expected results
			int fi2_2010 = 7949;
			int fi2_2011 = 4357;
			int fi2_2012 = 3059;
			int fi3_2010 = 4570;
			int fi3_2011 = 2653;
			int fi3_2012 = 1412;

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');

			var datalines = lines.Skip(1).Where(o => o.Trim().Length > 0);
			var fieldLines = datalines.Select(o => o.Split(','));
			Func<int, int, int> sumActivity = (int inst, int activity) => fieldLines.Where(o => int.Parse(o[2]) == inst && int.Parse(o[7]) == activity).Select(o => int.Parse(o[9])).Sum();

			Assert.AreEqual(fi2_2010, sumActivity(2, 2010));
			Assert.AreEqual(fi2_2011, sumActivity(2, 2011));
			Assert.AreEqual(fi2_2012, sumActivity(2, 2012));
			Assert.AreEqual(fi3_2010, sumActivity(3, 2010));
			Assert.AreEqual(fi3_2011, sumActivity(3, 2011));
			Assert.AreEqual(fi3_2012, sumActivity(3, 2012));

			Assert.AreEqual(fi2_2010, sumActivity(2, 2020) + sumActivity(2, 2030));
			Assert.AreEqual(fi2_2011, sumActivity(2, 2021) + sumActivity(2, 2031));
			Assert.AreEqual(fi2_2012, sumActivity(2, 2022) + sumActivity(2, 2032));
			Assert.AreEqual(fi3_2010, sumActivity(3, 2020) + sumActivity(3, 2030));
			Assert.AreEqual(fi3_2011, sumActivity(3, 2021) + sumActivity(3, 2031));
			Assert.AreEqual(fi3_2012, sumActivity(3, 2022) + sumActivity(3, 2032));
		}

		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifySimpleTotals()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();
			List<int> lobEntities = new List<int>(new int[] { 9, 10, 11, 12 });		// These are the entity IDs from the mock RAAM data

			// Arrange - expected results
			int fi2_3110 = 100;
			int fi3_3110 = 10;
			int fi2_3200 = 86;
			int fi3_3200 = 129;
			int fi2_3201 = 128;
			int fi3_3201 = 402;

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');

			var datalines = lines.Skip(1).Where(o => o.Trim().Length > 0);
			var fieldLines = datalines.Select(o => o.Split(','));
			Func<int, int, int> sumActivity = (int inst, int activity) => fieldLines.Where(o => int.Parse(o[2]) == inst && int.Parse(o[7]) == activity).Select(o => int.Parse(o[9])).Sum();

			Assert.AreEqual(fi2_3110, sumActivity(2, 3110));
			Assert.AreEqual(fi3_3110, sumActivity(3, 3110));
			Assert.AreEqual(fi2_3200, sumActivity(2, 3200));
			Assert.AreEqual(fi3_3200, sumActivity(3, 3200));
			Assert.AreEqual(fi2_3201, sumActivity(2, 3201));
			Assert.AreEqual(fi3_3201, sumActivity(3, 3201));
		}

		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyDistinctCounts()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();
			List<int> lobEntities = new List<int>(new int[] { 9, 10, 11, 12 });		// These are the entity IDs from the mock RAAM data

			// Arrange - expected results
			int fi2_1001 = 3;
			int fi3_1001 = 1;
			int fi2_1100 = 3;
			int fi3_1100 = 1;

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');

			var datalines = lines.Skip(1).Where(o => o.Trim().Length > 0);
			var fieldLines = datalines.Select(o => o.Split(','));
			Func<int, int, int> sumActivity = (int inst, int activity) => fieldLines.Where(o => int.Parse(o[2]) == inst && int.Parse(o[7]) == activity).Select(o => int.Parse(o[9])).Sum();

			Assert.AreEqual(fi2_1001, sumActivity(2, 1001));
			Assert.AreEqual(fi3_1001, sumActivity(3, 1001));
			Assert.AreEqual(fi2_1100, sumActivity(2, 1100));
			Assert.AreEqual(fi3_1100, sumActivity(3, 1100));
		}


		[TestMethod]
		public void GenericCSV_GenerateMockFile_VerifyGroupingTotals()
		{
			// Arrange - test class
			GenericCSVGenerator generator = new GenericCSVGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStream = new MemoryStream(output);
			generator._config.MockDAL = new GenericCSVMockDAL(generator._config as ConfigParameters);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();
			List<int> lobEntities = new List<int>(new int[] { 9, 10, 11, 12 });		// These are the entity IDs from the mock RAAM data

			// Arrange - expected results
			List<int[]> matchedCodes = new List<int[]> {
				new int[] { 1001, 2000 },
				new int[] { 1100, 2100 },
				new int[] { 2110, 3110 },
				new int[] { 2200, 3200 },
				new int[] { 2201, 3201 },
				new int[] { 2210, 3210 },
				new int[] { 2211, 3211 },
				new int[] { 2220, 3220 },
				new int[] { 2221, 3221 },
				new int[] { 2230, 3230 },
				new int[] { 2231, 3231 },
				new int[] { 2240, 3240 },
				new int[] { 2241, 3241 },
				new int[] { 2250, 3250 },
				new int[] { 2251, 3251 },
				new int[] { 2260, 3260 },
				new int[] { 2261, 3261 },
			};

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Split('\n');

			var datalines = lines.Skip(1).Where(o => o.Trim().Length > 0);
			var fieldLines = datalines.Select(o => o.Split(','));
			Func<int, int, int> sumActivity = (int inst, int activity) => fieldLines.Where(o => int.Parse(o[2]) == inst && int.Parse(o[7]) == activity).Select(o => int.Parse(o[9])).Sum();

			foreach (var entry in matchedCodes)
			{
				Assert.AreEqual(sumActivity(2, entry[0]), sumActivity(2, entry[1]));
				Assert.AreEqual(sumActivity(3, entry[0]), sumActivity(3, entry[1]));
			}
		}
	}
}
