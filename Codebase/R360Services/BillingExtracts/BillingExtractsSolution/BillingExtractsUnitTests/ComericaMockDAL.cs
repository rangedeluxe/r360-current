﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.Comerica
{
	class ComericaMockDAL : SharedDALBase
	{
		private WFS.RecHub.BillingExtracts.GenericCSV.GenericCSVMockDAL _genericDAL;
		public ComericaMockDAL(SharedConfigParametersBase config) : base(config) 
		{
			_genericDAL = new GenericCSV.GenericCSVMockDAL(config);
		}

		private List<T> ToComericaData<T>(List<T> counts) where T : GenericCounts
		{
			// Assign Billing accounts...
			for (int i = 0; i < counts.Count; i++)
			{
				int workgroupID = counts[i].WorkgroupID;
				if (workgroupID == 60) workgroupID = 50; // Give entity 6 the same account as entity 5...
				counts[i].BillingAccount = (workgroupID + (1000 * workgroupID) + (1000000 * workgroupID)).ToString();
				counts[i].BillingField1 = (counts[i].FIEntityID == 3 ? "501" : (counts[i].EntityID == 7 ? "042" : "002"));
			}

			return counts;
		}

		public override List<GenericCounts> GetCustomers()
		{
			var result = _genericDAL.GetCustomers();
			result = ToComericaData(result);
			return result;
		}

		public override List<GenericCounts> GetExceptionsEnabledCustomers()
		{
			var result = _genericDAL.GetExceptionsEnabledCustomers();
			result = ToComericaData(result);
			return result;
		}

		public override List<GenericCounts> GetExceptionCounts()
		{
			var result = _genericDAL.GetExceptionCounts();
			result = ToComericaData(result);
			return result;
		}

		public override List<GenericCounts> GetBulkDownloadZipCounts()
		{
			var result = _genericDAL.GetBulkDownloadZipCounts();
			result = ToComericaData(result);
			return result;
		}

		public override List<GenericCountsByRetention> GetItemCountsByRetention(IList<Analysis.RetentionValue> retentionCutoffs)
		{
			var result = _genericDAL.GetItemCountsByRetention(retentionCutoffs);
			result = ToComericaData(result);
			return result;
		}

		public override int GetFirstBank(int fiEntityID)
		{
			var list = GetCustomers();
			var match = list.Find(o => o.FIEntityID == fiEntityID);
			if (match != null)
				return match.SiteBankID;

			return 0;
		}

		public override void GetBillingFields(int entityID, out string billingAccount, out string billingField1, out string billingField2)
		{
			var list = GetCustomers();
			var match = list.Find(o => o.EntityID == entityID);
			if (match != null)
			{
				billingAccount = match.BillingAccount;
				billingField1 = match.BillingField1;
				billingField2 = match.BillingField2;
			}
			else
			{
				billingAccount = string.Empty;
				billingField1 = string.Empty;
				billingField2 = string.Empty;
			}
		}
	}
}
