﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;
using System.Xml.Linq;

namespace BillingExtractsUnitTests
{
	[TestClass]
	public class BillingBackgroundTaskTests
	{
		[TestMethod]
		public void ConvertDaysOfWeek_Blank_ReturnsAll()
		{
			// Arrange
			string testVal = string.Empty;

			// Act
			var result = BillingBackgroundTask.ConvertDaysOfWeek(testVal);

			// Assert
			Assert.AreEqual(7, result.Count);
			foreach (var day in Enum.GetValues(typeof(DayOfWeek)))
			{
				CollectionAssert.Contains(result, day);
			}
		}

		[TestMethod]
		public void ConvertDaysOfWeek_Star_ReturnsAll()
		{
			// Arrange
			string testVal = "*";

			// Act
			var result = BillingBackgroundTask.ConvertDaysOfWeek(testVal);

			// Assert
			Assert.AreEqual(7, result.Count);
			foreach (var day in Enum.GetValues(typeof(DayOfWeek)))
			{
				CollectionAssert.Contains(result, day);
			}
		}

		[TestMethod]
		public void ConvertDaysOfWeek_1_ReturnsSunday()
		{
			// Arrange
			string testVal = "1";

			// Act
			var result = BillingBackgroundTask.ConvertDaysOfWeek(testVal);

			// Assert
			Assert.AreEqual(1, result.Count);
			CollectionAssert.Contains(result, DayOfWeek.Sunday);
		}

		[TestMethod]
		public void ConvertDaysOfWeek_2_3_ReturnsMondayTuesday()
		{
			// Arrange
			string testVal = "2,3";

			// Act
			var result = BillingBackgroundTask.ConvertDaysOfWeek(testVal);

			// Assert
			Assert.AreEqual(2, result.Count);
			CollectionAssert.Contains(result, DayOfWeek.Monday);
			CollectionAssert.Contains(result, DayOfWeek.Tuesday);
		}

		internal static BillingBackgroundTask GetMockBackgroundTask(string daysOfWeek = "*", string description = "Test1", string type = "SummarizePaymentFacts")
		{
			return BillingBackgroundTask.FromXML(XElement.Parse("<Task Description='" + description + "' Type='" + type + "' TargetDay='-3' RunDaysOfWeek='" + daysOfWeek + "' RunTimeOfDay='13:00' />"));
		}

		[TestMethod]
		public void BillingBackgroundTask_FromXML_InitializesFromMock()
		{
			// Arrage and Act
			BillingBackgroundTask task = GetMockBackgroundTask();

			// Assert
			Assert.AreEqual("Test1", task.Description);
			Assert.AreEqual(BillingBackgroundTaskTypes.SummarizePaymentFacts, task.TaskType);
			Assert.AreEqual(-3, task.TargetDay);
			Assert.AreEqual(7, task.RunDays.Count, "Checking count of run days of week");
			Assert.AreEqual(TimeSpan.FromHours(13), task.RunTimeOfDay);
		}
	}
}
