﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BillingExtracts.API;
using System.Collections.Generic;

namespace BillingExtractsUnitTests
{
	[TestClass]
	public class RaamUtilityTests
	{
		private class Customer : IEntityID
		{
			public int TestID { get; set; }
			public int LogonEntityID { get; set; }
			public int EntityID { get; set; }
			//public object GroupingKey { get; set; }
			public int Count { get { return Counts[0]; } set { Counts = new int[] { value }; } }
			public int[] Counts { get; set; }

			private int? _customerEntityID;
			public int CustomerEntityID
			{
				get { return _customerEntityID ?? EntityID; }
				set { _customerEntityID = value; }
			}
		}

		private List<Customer> GetSampleCustomers()
		{
			// The Fake IRaamClient defines 12 entities (1 - 12)
			return new Customer[] {
				new Customer { TestID = 100, EntityID = 1, Count = 7 },
				new Customer { TestID = 300, EntityID = 2, Count = 13 },
				new Customer { TestID = 700, EntityID = 3, Count = 17 },
				new Customer { TestID = 1000, EntityID = 4, Count = 71 },
				new Customer { TestID = 1001, EntityID = 5, Count = 72 },
				new Customer { TestID = 1002, EntityID = 6, Count = 73 },
				new Customer { TestID = 1003, EntityID = 7, Count = 74 },
				new Customer { TestID = 1004, EntityID = 8, Count = 75 },
				new Customer { TestID = 1005, EntityID = 9, Count = 76 },
				new Customer { TestID = 1006, EntityID = 10, Count = 77 },
				new Customer { TestID = 1007, EntityID = 11, Count = 78 },
				new Customer { TestID = 1008, EntityID = 12, Count = 79 },
			}.ToList();
		}

		//private List<ICustomerEntitySummary> GetSampleCustomersWithGrouping()
		//{
		//	var list = GetSampleCustomers();

		//	// Add groupings
		//	var groups = new object[] { "Group 1", "Group 2" };
		//	for (int i = 0; i < list.Count; i++)
		//	{
		//		(list[i] as Customer).GroupingKey = groups[i % 2];
		//	}

		//	return list;
		//}

		[TestMethod]
		public void RAAMUtilities_FlattenFullList_Success()
		{
			// Arrange - test data
			List<Customer> entities = GetSampleCustomers();
			List<Customer> entitiesOrig = GetSampleCustomers();

			int corp5Expected = entities.First(o => o.EntityID == 5).Count
				+ entities.First(o => o.EntityID == 9).Count
				+ entities.First(o => o.EntityID == 10).Count;

			int corp8Expected = entities.First(o => o.EntityID == 8).Count
				+ entities.First(o => o.EntityID == 11).Count
				+ entities.First(o => o.EntityID == 12).Count;

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

			// Act
			object state = null;
			RAAMUtilities.ConvertEntitiesToCustomers(entities, ref state, fakeAPI);

			// Assert: 4 LOB elements should have been aggregated away
			Assert.AreEqual(8, entities.Select(o => o.CustomerEntityID).Distinct().Count());

			// Assert: Above Corp is unchanged
			for (int i = 0; i < 4; i++)
				Assert.AreEqual(entities[i].Count, entitiesOrig[i].Count);

			// Assert: Corp with no LOB are unchanged
			Assert.AreEqual(entities[5].Count, entitiesOrig[5].Count);
			Assert.AreEqual(entities[6].Count, entitiesOrig[6].Count);

			// Assert: Corp with LOB are summarized together
			Assert.AreEqual(corp5Expected, entities.Where(o => o.CustomerEntityID == 5).Sum(o => o.Count));
			Assert.AreEqual(corp8Expected, entities.Where(o => o.CustomerEntityID == 8).Sum(o => o.Count));
		}

		//[TestMethod]
		//public void RAAMUtilities_FlattenGroupedList_Success()
		//{
		//	// Arrange - test data
		//	List<ICustomerEntitySummary> entities = GetSampleCustomersWithGrouping();

		//	int corp51Expected = entities.Single(o => o.EntityID == 5).Count + entities.Single(o => o.EntityID == 9).Count;
		//	int corp52Expected = entities.Single(o => o.EntityID == 10).Count;

		//	int corp82Expected = entities.Single(o => o.EntityID == 8).Count + entities.Single(o => o.EntityID == 12).Count;
		//	int corp81Expected = entities.Single(o => o.EntityID == 11).Count;

		//	// Arrange - fake API
		//	FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
		//	IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

		//	// Act
		//	object state = null;
		//	var result = RAAMUtilities.ConvertEntitiesToCustomers(entities, ref state, fakeAPI);

		//	// Assert: 4 LOB elements should have been aggregated, but into 2 groups each
		//	Assert.AreEqual(8 + 2, result.Count);

		//	// Assert: Above Corp is unchanged
		//	for (int i = 0; i < 4; i++)
		//		Assert.AreEqual(entities[i].Count, result.Single(o => o.EntityID == entities[i].EntityID).Count);

		//	// Assert: Corp with no LOB are unchanged
		//	Assert.AreEqual(entities[5].Count, result.Single(o => o.EntityID == entities[5].EntityID).Count);
		//	Assert.AreEqual(entities[6].Count, result.Single(o => o.EntityID == entities[6].EntityID).Count);

		//	// Assert: Corp with LOB are summarized together, but in separate groups
		//	Assert.AreEqual(corp51Expected, result.Single(o => o.EntityID == 5 && o.FirstEntitySummary.GroupingKey.ToString() == "Group 1").Count);
		//	Assert.AreEqual(corp52Expected, result.Single(o => o.EntityID == 5 && o.FirstEntitySummary.GroupingKey.ToString() == "Group 2").Count);
		//	Assert.AreEqual(corp81Expected, result.Single(o => o.EntityID == 8 && o.FirstEntitySummary.GroupingKey.ToString() == "Group 1").Count);
		//	Assert.AreEqual(corp82Expected, result.Single(o => o.EntityID == 8 && o.FirstEntitySummary.GroupingKey.ToString() == "Group 2").Count);
		//}

		[TestMethod]
		public void RAAMUtilities_UnknownEntity_Ignored()
		{
			// Arrange - test data
			List<Customer> entities = GetSampleCustomers();
			entities.Add(new Customer { TestID = -1, EntityID = 10000, Count = 124 });	// This entity ID is not part of the test data

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

			// Act
			object state = null;
			RAAMUtilities.ConvertEntitiesToCustomers(entities, ref state, fakeAPI);

			// Assert: Entity ID should have been converted to -1
			Assert.IsNull(entities.FirstOrDefault(o => o.CustomerEntityID == 10000));
			Assert.IsNotNull(entities.FirstOrDefault(o => o.CustomerEntityID == -1));
		}

		[TestMethod]
		public void RAAMUtilities_Duplicate_Aggregated()
		{
			// Arrange - test data
			List<Customer> entities = GetSampleCustomers();
			List<Customer> doubleEntites = new List<Customer>();
			doubleEntites.AddRange(entities);
			doubleEntites.AddRange(entities);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

			// Act
			object state = null;
			RAAMUtilities.ConvertEntitiesToCustomers(doubleEntites, ref state, fakeAPI);

			// Assert: 4 LOB elements should have been aggregated away, and other duplicates should have been combined
			Assert.AreEqual(8, entities.Select(o => o.CustomerEntityID).Distinct().Count());
		}

		//[TestMethod]
		//public void RAAMUtilities_FlattenFullList_MultipleCounts_Success()
		//{
		//	// Arrange - test data
		//	List<ICustomerEntitySummary> entities = GetSampleCustomers();
		//	foreach (var entity in entities)
		//	{
		//		Customer cust = entity as Customer;
		//		cust.Counts = new int[] { cust.Count, cust.Count * 2 };
		//	}

		//	int corp5Expected1 = entities.First(o => o.EntityID == 5).Count
		//		+ entities.First(o => o.EntityID == 9).Count
		//		+ entities.First(o => o.EntityID == 10).Count;

		//	int corp5Expected2 = entities.First(o => o.EntityID == 5).Counts[1]
		//		+ entities.First(o => o.EntityID == 9).Counts[1]
		//		+ entities.First(o => o.EntityID == 10).Counts[1];

		//	int corp8Expected1 = entities.First(o => o.EntityID == 8).Count
		//		+ entities.First(o => o.EntityID == 11).Count
		//		+ entities.First(o => o.EntityID == 12).Count;

		//	int corp8Expected2 = entities.First(o => o.EntityID == 8).Counts[1]
		//		+ entities.First(o => o.EntityID == 11).Counts[1]
		//		+ entities.First(o => o.EntityID == 12).Counts[1];

		//	// Arrange - fake API
		//	FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
		//	IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

		//	// Act
		//	object state = null;
		//	var result = RAAMUtilities.ConvertEntitiesToCustomers(entities, ref state, fakeAPI);

		//	// Assert: 4 LOB elements should have been aggregated away
		//	Assert.AreEqual(8, result.Count);

		//	// Assert: Above Corp is unchanged
		//	for (int i = 0; i < 4; i++)
		//	{
		//		Assert.AreEqual(entities[i].Count, result.First(o => o.EntityID == entities[i].EntityID).Count);
		//		Assert.AreEqual(entities[i].Counts[1], result.First(o => o.EntityID == entities[i].EntityID).Counts[1]);
		//	}

		//	// Assert: Corp with no LOB are unchanged
		//	Assert.AreEqual(entities[5].Count, result.First(o => o.EntityID == entities[5].EntityID).Count);
		//	Assert.AreEqual(entities[6].Count, result.First(o => o.EntityID == entities[6].EntityID).Count);
		//	Assert.AreEqual(entities[5].Counts[1], result.First(o => o.EntityID == entities[5].EntityID).Counts[1]);
		//	Assert.AreEqual(entities[6].Counts[1], result.First(o => o.EntityID == entities[6].EntityID).Counts[1]);

		//	// Assert: Corp with LOB are summarized together
		//	Assert.AreEqual(corp5Expected1, result.First(o => o.EntityID == 5).Count);
		//	Assert.AreEqual(corp8Expected1, result.First(o => o.EntityID == 8).Count);
		//	Assert.AreEqual(corp5Expected2, result.First(o => o.EntityID == 5).Counts[1]);
		//	Assert.AreEqual(corp8Expected2, result.First(o => o.EntityID == 8).Counts[1]);
		//}

		[TestMethod]
		public void RAAMUtilities_FILogonEntity_Detected()
		{
			// Arrange - test data
			List<Customer> entities = GetSampleCustomers();
			entities[5].LogonEntityID = entities[5].EntityID; // Corp
			entities[6].LogonEntityID = 2; // FI

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

			// Act
			object state = null;
			List<Customer> corporates = new List<Customer>();
			foreach (var entity in entities)
			{
				if (entity.LogonEntityID <= 0 || RAAMUtilities.IsCorporate(entity.LogonEntityID, ref state, fakeAPI))
					corporates.Add(entity);
			}
			RAAMUtilities.ConvertEntitiesToCustomers(corporates, ref state, fakeAPI);

			// Assert: Entity ID = 7 should have been suppressed.
			Assert.AreEqual(entities.Count - 1, corporates.Count);

			// Assert: 4 LOB elements should have been aggregated away and Entity ID = 7 should have been suppressed.
			Assert.AreEqual(7, corporates.Select(o => o.CustomerEntityID).Distinct().Count());

			// Assert: Count of entities[6] should have been "lost" during aggregation...
			Assert.AreEqual(entities.Sum(o => o.Count) - entities[6].Count, corporates.Sum(o => o.Count));
		}
	}
}
