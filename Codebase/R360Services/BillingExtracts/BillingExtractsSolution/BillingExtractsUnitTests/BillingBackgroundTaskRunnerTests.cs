﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BillingExtracts.API;

namespace BillingExtractsUnitTests
{
	[TestClass]
	public class BillingBackgroundTaskRunnerTests
	{
		[TestMethod]
		public void GetNextRun_EarlierTime_ReturnSameDay()
		{
			// Arrange
			BillingBackgroundTask task = BillingBackgroundTaskTests.GetMockBackgroundTask();
			DateTime lastRun = new DateTime(2000, 1, 1, 3, 3, 3);
			DateTime expected = lastRun.Date + TimeSpan.FromHours(13);

			// Act
			DateTime result = BillingBackgroundTaskRunner.GetNextRun(lastRun, task.RunTimeOfDay, task.RunDays);

			// Assert
			Assert.AreEqual(expected, result);
		}

		[TestMethod]
		public void GetNextRun_EqualTime_ReturnNextDay()
		{
			// Arrange
			BillingBackgroundTask task = BillingBackgroundTaskTests.GetMockBackgroundTask();
			DateTime lastRun = new DateTime(2000, 1, 1, 13, 0, 0);
			DateTime expected = lastRun.Date + TimeSpan.FromDays(1) + TimeSpan.FromHours(13);

			// Act
			DateTime result = BillingBackgroundTaskRunner.GetNextRun(lastRun, task.RunTimeOfDay, task.RunDays);

			// Assert
			Assert.AreEqual(expected, result);
		}

		[TestMethod]
		public void GetNextRun_SkipDaysOfWeek_ReturnAppropriateDay()
		{
			// Arrange
			BillingBackgroundTask task = BillingBackgroundTaskTests.GetMockBackgroundTask(daysOfWeek: "3, 5");
			DateTime lastRun = new DateTime(2000, 1, 1, 13, 0, 0);
			DateTime expected = lastRun.Date + TimeSpan.FromDays(3) + TimeSpan.FromHours(13);

			// Act
			DateTime result = BillingBackgroundTaskRunner.GetNextRun(lastRun, task.RunTimeOfDay, task.RunDays);

			// Assert Setup
			Assert.AreEqual(2, task.RunDays.Count);
			CollectionAssert.Contains(task.RunDays, DayOfWeek.Tuesday);
			CollectionAssert.Contains(task.RunDays, DayOfWeek.Thursday);
			Assert.AreEqual(DayOfWeek.Saturday, lastRun.DayOfWeek);

			// Assert Result
			Assert.AreEqual(DayOfWeek.Tuesday, result.DayOfWeek);
			Assert.AreEqual(expected, result);
		}

		[TestMethod]
		public void CheckForWork_NoHistory_ReturnsTrue()
		{
			// Arrange
			BillingBackgroundTask task1 = BillingBackgroundTaskTests.GetMockBackgroundTask();
			DateTime now = new DateTime(2000, 1, 1, 3, 3, 3);
			bool expected = true;

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

			// Arrange - Task Runner internals
			BillingBackgroundTaskRunner.ClearInMemoryRunHistory();

			// Act
			bool result = BillingBackgroundTaskRunner.CheckForWork(task1, fakeAPI, now);

			// Assert
			Assert.AreEqual(expected, result);
		}

		[TestMethod]
		public void StoreRunTimeAndCheckForWork_ReturnsFalse()
		{
			// Arrange
			BillingBackgroundTask task1 = BillingBackgroundTaskTests.GetMockBackgroundTask();
			DateTime now = new DateTime(2000, 1, 1, 3, 3, 3);
			bool expectedResult = false;

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

			// Arrange - Task Runner internals
			BillingBackgroundTaskRunner.ClearInMemoryRunHistory();
			BillingBackgroundTaskRunner.StoreTaskRunTime(task1.Description, now);

			// Act
			bool result = BillingBackgroundTaskRunner.CheckForWork(task1, fakeAPI, now);

			// Assert
			Assert.AreEqual(expectedResult, result);
		}

		[TestMethod]
		public void StoreRunTimeAndCheckForWork_UniqueDescriptions_UniqueResults()
		{
			// Arrange
			BillingBackgroundTask task1 = BillingBackgroundTaskTests.GetMockBackgroundTask();
			BillingBackgroundTask task2 = BillingBackgroundTaskTests.GetMockBackgroundTask(description: "Test2");
			DateTime now = new DateTime(2000, 1, 1, 3, 3, 3);
			bool expectedFirstResult = false;
			bool expectedSecondResult = true;

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

			// Arrange - Task Runner internals
			BillingBackgroundTaskRunner.ClearInMemoryRunHistory();
			BillingBackgroundTaskRunner.StoreTaskRunTime(task1.Description, now);

			// Act
			bool result1 = BillingBackgroundTaskRunner.CheckForWork(task1, fakeAPI, now);
			bool result2 = BillingBackgroundTaskRunner.CheckForWork(task2, fakeAPI, now);

			// Assert
			Assert.AreEqual(expectedFirstResult, result1);
			Assert.AreEqual(expectedSecondResult, result2);
		}

		[TestMethod]
		public void StoreRunTimeAndCheckForWork_SameDescriptions_SameResults()
		{
			// Arrange
			BillingBackgroundTask task1 = BillingBackgroundTaskTests.GetMockBackgroundTask();
			BillingBackgroundTask task2 = BillingBackgroundTaskTests.GetMockBackgroundTask();
			DateTime now = new DateTime(2000, 1, 1, 3, 3, 3);
			bool expectedResult = false;

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IBillingAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IBillingAPI();

			// Arrange - Task Runner internals
			BillingBackgroundTaskRunner.ClearInMemoryRunHistory();
			BillingBackgroundTaskRunner.StoreTaskRunTime(task1.Description, now);

			// Act
			bool result1 = BillingBackgroundTaskRunner.CheckForWork(task1, fakeAPI, now);
			bool result2 = BillingBackgroundTaskRunner.CheckForWork(task2, fakeAPI, now);

			// Assert
			Assert.AreEqual(expectedResult, result1);
			Assert.AreEqual(expectedResult, result2);
		}
	}
}
