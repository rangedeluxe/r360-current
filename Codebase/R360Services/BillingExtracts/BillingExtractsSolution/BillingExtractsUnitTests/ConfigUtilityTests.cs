﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using System.IO;
using WFS.RecHub.BillingExtracts.API;

namespace BillingExtractsUnitTests
{
	[TestClass]
	public class ConfigUtilityTests
	{
		[TestMethod]
		public void ConfigUtilities_GetOptionalString_Success()
		{
			// Arrange
			var testXML = "<Root Test1='A value' />";
			XDocument xml = XDocument.Load(new StringReader(testXML));
			var expected = "A value";

			// Act
			var result = ConfigUtilities.GetOptionalString(xml.Root, "Test1", "Not found");

			// Assert
			Assert.AreEqual(expected, result);
		}

		[TestMethod]
		public void ConfigUtilities_GetOptionalString_ReturnsDefault()
		{
			// Arrange
			var testXML = "<Root Test1='A value' />";
			XDocument xml = XDocument.Load(new StringReader(testXML));
			var expected = "Not found";

			// Act
			var result = ConfigUtilities.GetOptionalString(xml.Root, "Test2", "Not found");

			// Assert
			Assert.AreEqual(expected, result);
		}

		[TestMethod]
		public void ConfigUtilities_GetOptionalDates_Success()
		{
			// Arrange
			var testXML = "<Root Date1='01/02/2003' Date2='02/03/2004' />";
			XDocument xml = XDocument.Load(new StringReader(testXML));
			var expected = Tuple.Create(new DateTime(2003, 01, 02), new DateTime(2004, 02, 03));

			// Act
			var result = ConfigUtilities.GetOptionalDates(xml.Root, "Date1", "Date2", ConfigUtilities.GetLastMonth);

			// Assert
			Assert.AreEqual(expected, result);
		}

		[TestMethod]
		public void ConfigUtilities_GetOptionalDates_ReturnsDefault()
		{
			// Arrange
			var testXML = "<Root Date1='01/02/2003' Date2='02/03/2004' />";
			XDocument xml = XDocument.Load(new StringReader(testXML));
			var expected = ConfigUtilities.GetLastMonth();

			// Act
			var result = ConfigUtilities.GetOptionalDates(xml.Root, "Date1X", "Date2", ConfigUtilities.GetLastMonth);

			// Assert
			Assert.AreEqual(expected, result);
		}

		[TestMethod]
		public void ConfigUtilities_GetRequiredString_Success()
		{
			// Arrange
			var testXML = "<Root><Test1>A value</Test1></Root>";
			XDocument xml = XDocument.Load(new StringReader(testXML));
			var expected = "A value";

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			var result = ConfigUtilities.GetRequiredString(xml, "Test1", "TestName", "UnitTests", fakeAPI);

			// Assert
			Assert.AreEqual(expected, result);
			Assert.IsFalse(fakeApiCoordinator.LogMessages.Any(o => o.Type == IFileGeneratorAPI_MessageType.Error));
		}

		[TestMethod]
		public void ConfigUtilities_GetRequiredString_Failed()
		{
			// Arrange
			var testXML = "<Root><Test1>A value</Test1></Root>";
			XDocument xml = XDocument.Load(new StringReader(testXML));
			string expected = null;

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			var result = ConfigUtilities.GetRequiredString(xml, "Test2", "TestName", "UnitTests", fakeAPI);

			// Assert
			Assert.AreEqual(expected, result);
			Assert.IsTrue(fakeApiCoordinator.LogMessages.Any(o => o.Type == IFileGeneratorAPI_MessageType.Error));
		}

		[TestMethod]
		public void ConfigUtilities_GetOptionalBoolean_Success()
		{
			// Arrange
			var testXML = "<Root><Test1>True</Test1></Root>";
			XDocument xml = XDocument.Load(new StringReader(testXML));
			var expected = true;

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			var result = ConfigUtilities.GetOptionalBoolean(xml, "Test1", false, "UnitTests", fakeAPI);

			// Assert
			Assert.AreEqual(expected, result);
			Assert.IsFalse(fakeApiCoordinator.LogMessages.Any(o => o.Type == IFileGeneratorAPI_MessageType.Error));
		}

		[TestMethod]
		public void ConfigUtilities_GetOptionalBoolean_ReturnDefault()
		{
			// Arrange
			var testXML = "<Root><Test1>True</Test1></Root>";
			XDocument xml = XDocument.Load(new StringReader(testXML));
			var expected = false;

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			var result = ConfigUtilities.GetOptionalBoolean(xml, "Test2", false, "UnitTests", fakeAPI);

			// Assert
			Assert.AreEqual(expected, result);
			Assert.IsFalse(fakeApiCoordinator.LogMessages.Any(o => o.Type == IFileGeneratorAPI_MessageType.Error));
		}
	}
}
