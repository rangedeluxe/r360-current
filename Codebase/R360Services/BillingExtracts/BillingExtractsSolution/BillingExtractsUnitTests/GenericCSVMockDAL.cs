﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BillingExtracts.API;

namespace WFS.RecHub.BillingExtracts.GenericCSV
{
	class GenericCSVMockDAL : SharedDALBase
	{
		public GenericCSVMockDAL(SharedConfigParametersBase config) : base(config) { }

		private void FilterList<T>(List<T> list) where T: GenericCounts
		{
			if (_config.ExtractParams != null &&
				_config.ExtractParams.TargetEntity != "*")
			{
				HashSet<int> entites = new HashSet<int>(_config.ExtractParams.TargetEntities.Select(o => int.Parse(o)));
				for (int i = list.Count - 1; i >= 0; i--)
				{
					if (list[i].FIEntityID > 0 && !entites.Contains(list[i].FIEntityID))
						list.RemoveAt(i);
				}
			}
		}

		public override List<GenericCounts> GetCustomers()
		{
			// Mock RAAM data uses entity IDs 5 - 12 for customer entities, where 8, 11, and 12 belong to the second FI
			List<GenericCounts> customers = new GenericCounts[] 
			{
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 5, workgroupID: 50) { WorkgroupName = "50", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 6, workgroupID: 60) { WorkgroupName = "60", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 7, workgroupID: 70) { WorkgroupName = "70", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 7, workgroupID: 71) { WorkgroupName = "71", LogonEntityID = 0, Count = 1 },	// Duplicate entity
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 9, workgroupID: 90) { WorkgroupName = "90", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 10, workgroupID: 100) { WorkgroupName = "100", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 3, siteBankID: 456, entityID: 8, workgroupID: 80) { WorkgroupName = "80", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 3, siteBankID: 456, entityID: 11, workgroupID: 110) { WorkgroupName = "110", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 3, siteBankID: 456, entityID: 12, workgroupID: 120) { WorkgroupName = "120", LogonEntityID = 0, Count = 1 },
			}.ToList();
			FilterList(customers);
			return customers;
		}

		/// <summary>
		/// This method generates random (but predictable) counts from 1 to maxReturned
		/// </summary>
		/// <param name="seed">Use different seeds for different calls to get different values</param>
		/// <param name="maxReturned">Maximum value for the count</param>
		/// <param name="maxGenerated">If greater than maxReturned, values that are greater will be interpretted as "NULL"</param>
		private List<GenericCountsByRetention> GetRandomCounts(int seed, int maxReturned, int maxGenerated, IList<Analysis.RetentionValue> retentionCutoffs)
		{
			List<GenericCountsByRetention> result = new List<GenericCountsByRetention>();

			Random r = new Random(seed); // Use seed for predictable results (not really random...)
			var custs = GetCustomers();
			foreach (var cust in custs)
			{
				foreach (var cutoff in retentionCutoffs)
				{
					int count = r.Next(maxGenerated) + 1;
					if (count <= maxReturned)
					{
						result.Add(new GenericCountsByRetention(
							fiEntityID: cust.FIEntityID,
							siteBankID: cust.SiteBankID,
							entityID: cust.EntityID,
							workgroupID: cust.WorkgroupID)
							{
								WorkgroupName = cust.WorkgroupName,
								LogonEntityID = cust.LogonEntityID,
								Count = count,
								RetentionCutoff = cutoff
							});
					}
				}
			}
			FilterList(result);
			return result;
		}

		public override List<GenericCountsByRetention> GetItemCountsByRetention(IList<Analysis.RetentionValue> retentionCutoffs)
		{
			var custList = GetRandomCounts(1, 1000, 1200, retentionCutoffs);
			return custList;
		}

		public override List<GenericCountsByRetention> GetPaymentItemCountsByRetention(IList<Analysis.RetentionValue> retentionCutoffs)
		{
			var custList = GetRandomCounts(1, 1000, 1200, retentionCutoffs);
			foreach (var entry in custList)
			{
				entry.Count -= entry.Count / 3;
			}
			return custList;
		}

		public override List<GenericCountsByRetention> GetNonPaymentItemCountsByRetention(IList<Analysis.RetentionValue> retentionCutoffs)
		{
			var custList = GetRandomCounts(1, 1000, 1200, retentionCutoffs);
			foreach (var entry in custList)
			{
				entry.Count = entry.Count / 3;
			}
			return custList;
		}

		public override int GetFirstBank(int fiEntityID)
		{
			var list = GetCustomers();
			var match = list.Find(o => o.FIEntityID == fiEntityID);
			if (match != null)
				return match.SiteBankID;

			return 0;
		}

		public override List<GenericCounts> GetExceptionsEnabledCustomers()
		{
			// This needs to match the RAAM Mock data...
			var result = new List<GenericCounts>(new GenericCounts[]
			{
				new GenericCounts(fiEntityID: 0, siteBankID: 0, entityID: 5, workgroupID: 50) { WorkgroupName = "50", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 0, siteBankID: 0, entityID: 6, workgroupID: 60) { WorkgroupName = "60", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 0, siteBankID: 0, entityID: 8, workgroupID: 80) { WorkgroupName = "80", LogonEntityID = 0, Count = 1 },
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 7, workgroupID: 70) { WorkgroupName = "70", LogonEntityID = 0, Count = 1 },	
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 7, workgroupID: 71) { WorkgroupName = "71", LogonEntityID = 0, Count = 1 },	// Duplicate entity
				new GenericCounts(fiEntityID: 2, siteBankID: 123, entityID: 9, workgroupID: 90) { WorkgroupName = "90", LogonEntityID = 0, Count = 1 }, // LOB entity
			});
			FilterList(result);
			return result;
		}

		public override List<GenericCounts> GetBulkDownloadZipCounts()
		{
			// This needs to match the RAAM Mock data...
			var result = new List<GenericCounts>(new GenericCounts[]
			{
				new GenericCounts(new int[] { 3, 7 }, fiEntityID: 2, siteBankID: 123, entityID: 5, workgroupID: 50) { WorkgroupName = "50", LogonEntityID = 5 },
				new GenericCounts(new int[] { 7, 17 }, fiEntityID: 2, siteBankID: 123, entityID: 7, workgroupID: 70) { WorkgroupName = "70", LogonEntityID = 7},
				new GenericCounts(new int[] { 5, 23 }, fiEntityID: 2, siteBankID: 123, entityID: 7, workgroupID: 71) { WorkgroupName = "71", LogonEntityID = 1},
				new GenericCounts(new int[] { 29, 37 }, fiEntityID: 2, siteBankID: 123, entityID: 9, workgroupID: 90) { WorkgroupName = "90", LogonEntityID = 9},
				new GenericCounts(new int[] { 47, 67 }, fiEntityID: 2, siteBankID: 123, entityID: 9, workgroupID: 90) { WorkgroupName = "90", LogonEntityID = 5},
				new GenericCounts(new int[] { 103, 997 }, fiEntityID: 2, siteBankID: 123, entityID: 9, workgroupID: 90) { WorkgroupName = "90", LogonEntityID = 2},
				new GenericCounts(new int[] { 129, 402 }, fiEntityID: 3, siteBankID: 456, entityID: 11, workgroupID: 110) { WorkgroupName = "110", LogonEntityID = 8},
			});
			FilterList(result);
			return result;
		}

		public override List<GenericCounts> GetExceptionCounts()
		{
			// This needs to match the RAAM Mock data...
			var result = new List<GenericCounts>(new GenericCounts[]
			{
				new GenericCounts(new int[] { 50, 30, 8, 22, 500, 52, 1, 4 }, fiEntityID: 2, siteBankID: 123, entityID: 5, workgroupID: 50) { WorkgroupName = "50", LogonEntityID = 0 },
				new GenericCounts(new int[] { 30, 15, 5, 12, 300, 34, 1, 2 }, fiEntityID: 2, siteBankID: 123, entityID: 6, workgroupID: 60) { WorkgroupName = "60", LogonEntityID = 0 },
				new GenericCounts(new int[] { 20, 10, 2, 6, 200, 18, 1, 1 }, fiEntityID: 2, siteBankID: 123, entityID: 7, workgroupID: 70) { WorkgroupName = "70", LogonEntityID = 0 },
				new GenericCounts(new int[] { 10, 5, 1, 3, 20, 8, 0, 2 }, fiEntityID: 3, siteBankID: 456, entityID: 8, workgroupID: 80) { WorkgroupName = "80", LogonEntityID = 0 },
			});
			FilterList(result);
			return result;
		}

		public override List<GenericCounts> GetBulkDownloadCsv1Counts()
		{
			var result = GetBulkDownloadZipCounts();
			foreach (var entry in result)
			{
				for (int i = 0; i < entry.Counts.Length; i++)
				{
					entry.Counts[i] += 1;
				}
			}
			return result;
		}

		public override List<GenericCounts> GetBulkDownloadCsv2Counts()
		{
			var result = GetBulkDownloadZipCounts();
			foreach (var entry in result)
			{
				for (int i = 0; i < entry.Counts.Length; i++)
				{
					entry.Counts[i] += 2;
				}
			}
			return result;
		}

		public override List<GenericCounts> GetBulkDownloadPrintViewCounts()
		{
			var result = GetBulkDownloadZipCounts();
			foreach (var entry in result)
			{
				for (int i = 0; i < entry.Counts.Length; i++)
				{
					entry.Counts[i] += 3;
				}
			}
			return result;
		}

		public override List<GenericCounts> GetBulkDownloadPdfViewCounts()
		{
			var result = GetBulkDownloadZipCounts();
			foreach (var entry in result)
			{
				for (int i = 0; i < entry.Counts.Length; i++)
				{
					entry.Counts[i] += 4;
				}
			}
			return result;
		}

		public override List<GenericCounts> GetBulkDownloadHtmlCounts()
		{
			var result = GetBulkDownloadZipCounts();
			foreach (var entry in result)
			{
				for (int i = 0; i < entry.Counts.Length; i++)
				{
					entry.Counts[i] += 5;
				}
			}
			return result;
		}

		public override List<GenericCounts> GetBulkDownloadXmlCounts()
		{
			var result = GetBulkDownloadZipCounts();
			foreach (var entry in result)
			{
				for (int i = 0; i < entry.Counts.Length; i++)
				{
					entry.Counts[i] += 6;
				}
			}
			return result;
		}

	}
}
