﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BillingExtracts.DAL;

namespace BillingExtractsUnitTests
{
	[TestClass]
	public class BillingDALTests
	{
		[TestMethod]
		public void Verify_ISO_8601_DateFormat_ToString()
		{
			// Arrange
			DateTime testDate = new DateTime(2000, 1, 2, 13, 4, 5, 6);
			string expected = "2000-01-02T13:04:05.006Z";

			// Act
			string result = testDate.ToString(BillingDAL.ISO8601DATEFORMAT_SQL127);

			// Assert
			Assert.AreEqual(expected, result);
		}
	}
}
