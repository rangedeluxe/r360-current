﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BillingExtracts.Comerica;
using WFS.RecHub.BillingExtracts.API;
using System.Collections.Generic;
using System.IO;

namespace BillingExtractsUnitTests
{
	[TestClass]
	public class ComericaGeneratorTests
	{
		[TestMethod]
		public void TestSort()
		{
			// Arrange
			ComericaGenerator gen = new ComericaGenerator();
			var sorted = new ComericaLineItem[] {
				new ComericaLineItem { BankNumber = 1, AccountNumber = 10, Activity = new Analysis.ActivityInfo { ActivityCode = 100 } },
				new ComericaLineItem { BankNumber = 1, AccountNumber = 10, Activity = new Analysis.ActivityInfo { ActivityCode = 200 } },
				new ComericaLineItem { BankNumber = 1, AccountNumber = 10, Activity = new Analysis.ActivityInfo { ActivityCode = 300 } },
				new ComericaLineItem { BankNumber = 1, AccountNumber = 20, Activity = new Analysis.ActivityInfo { ActivityCode = 100 } },
				new ComericaLineItem { BankNumber = 1, AccountNumber = 20, Activity = new Analysis.ActivityInfo { ActivityCode = 200 } },
				new ComericaLineItem { BankNumber = 1, AccountNumber = 20, Activity = new Analysis.ActivityInfo { ActivityCode = 300 } },
				new ComericaLineItem { BankNumber = 1, AccountNumber = 30, Activity = new Analysis.ActivityInfo { ActivityCode = 100 } },
				new ComericaLineItem { BankNumber = 1, AccountNumber = 30, Activity = new Analysis.ActivityInfo { ActivityCode = 200 } },
				new ComericaLineItem { BankNumber = 2, AccountNumber = 10, Activity = new Analysis.ActivityInfo { ActivityCode = 100 } },
				new ComericaLineItem { BankNumber = 2, AccountNumber = 10, Activity = new Analysis.ActivityInfo { ActivityCode = 200 } },
				new ComericaLineItem { BankNumber = 2, AccountNumber = 10, Activity = new Analysis.ActivityInfo { ActivityCode = 300 } },
				new ComericaLineItem { BankNumber = 2, AccountNumber = 20, Activity = new Analysis.ActivityInfo { ActivityCode = 100 } },
				new ComericaLineItem { BankNumber = 2, AccountNumber = 20, Activity = new Analysis.ActivityInfo { ActivityCode = 200 } },
				new ComericaLineItem { BankNumber = 2, AccountNumber = 20, Activity = new Analysis.ActivityInfo { ActivityCode = 300 } },
				new ComericaLineItem { BankNumber = 2, AccountNumber = 30, Activity = new Analysis.ActivityInfo { ActivityCode = 100 } },
				new ComericaLineItem { BankNumber = 2, AccountNumber = 30, Activity = new Analysis.ActivityInfo { ActivityCode = 200 } },
			}.ToList();

			Random r = new Random(1);
			var random = sorted.ToList();
			Shuffle(r, random);

			// Act
			gen.Sort(ref random);

			// Assert
			for (int i = 0; i < sorted.Count; i++)
				Assert.AreSame(sorted[i], random[i]);
		}

		private static void Shuffle<T>(Random r, List<T> array)
		{
			var random = r;
			for (int i = array.Count; i > 1; i--)
			{
				// Pick random element to swap.
				int j = random.Next(i); // 0 <= j <= i-1
				// Swap.
				T tmp = array[j];
				array[j] = array[i - 1];
				array[i - 1] = tmp;
			}
		}
		private static string GetLastMonthDate()
		{
			var date = DateTime.Now;
			date = new DateTime(date.Year, date.Month, 1);
			date = date - TimeSpan.FromDays(1);
			return date.ToString("MMddyyyy");
		}

		[TestMethod]
		public void Comerica_GenerateMockFile_XAT_VerifyNoErrors()
		{
			// Arrange - test class
			ComericaGenerator generator = new ComericaGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStreams = new Dictionary<string, System.IO.Stream>();
			generator._config.MockOutputStreams[ConfigParameters.c_XAT] = new MemoryStream(output);
			generator._config.MockDAL = new ComericaMockDAL(generator._config);
			string date = GetLastMonthDate();
			string lastMockLine = "042020" + date + "460INR TM  08263Monthly Maintenance Fee Per Customer    00000000000001 M000000000000000 00000000                              ";
			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P FileTypes=\"XAT\" Entity=\"2\" />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Replace("\r\n", "\n").Split('\n');
			Assert.AreEqual(17, lines.Length);			// Number of lines generated from mock data
			Assert.AreEqual(string.Empty, lines[16]);	// Last line is actually blank...
			Assert.AreEqual(lastMockLine, lines[15]);	// Last real line
		}

		[TestMethod]
		public void Comerica_GenerateMockFile_FB80_VerifyNoErrors()
		{
			// Arrange - test class
			ComericaGenerator generator = new ComericaGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStreams = new Dictionary<string, System.IO.Stream>();
			generator._config.MockOutputStreams[ConfigParameters.c_FB80] = new MemoryStream(output);
			generator._config.MockDAL = new ComericaMockDAL(generator._config);
			string date = GetLastMonthDate();
			string lastMockLine = "04200071071071" + date + date + "INR TM  082630000001 460                       ";

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P FileTypes=\"FB80\" Entity=\"2\" />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var lines = file.Replace("\r\n", "\n").Split('\n');
			Assert.AreEqual(28, lines.Length);			// Number of lines generated from mock data
			Assert.AreEqual(string.Empty, lines[27]);	// Last line is actually blank...
			Assert.AreEqual(lastMockLine, lines[26]);	// Last real line
		}

		[TestMethod]
		public void Comerica_GenerateMockFile_PRN_VerifyNoErrors()
		{
			// Arrange - test class
			ComericaGenerator generator = new ComericaGenerator();
			byte[] output = new byte[16 * 1024 * 1024];
			generator._config.MockOutputStreams = new Dictionary<string, System.IO.Stream>();
			generator._config.MockOutputStreams[ConfigParameters.c_PRN] = new MemoryStream(output);
			generator._config.MockDAL = new ComericaMockDAL(generator._config);

			// Arrange - fake API
			FakeBillingAPICoordinator fakeApiCoordinator = new FakeBillingAPICoordinator();
			IFileGeneratorAPI fakeAPI = fakeApiCoordinator.BuildFakeAPI_IFileGeneratorAPI();

			// Act
			List<string> errors;
			generator.GenerateFile(fakeAPI, "<P FileTypes=\"PRN\" Entity=\"2\" />", out errors);

			// Assert
			var firstError = fakeApiCoordinator.LogMessages.FirstOrDefault(o =>
				o.Importance == IFileGeneratorAPI_MessageImportance.Essential &&
				o.Type == IFileGeneratorAPI_MessageType.Error);
			if (firstError != null) // Explicitly check null, since we want to include properties in the message...
				Assert.Fail("Error found: <{0}> {1} ", firstError.Source, firstError.Message);

			// Prepare output for review
			int length = Array.IndexOf<byte>(output, 0);
			Array.Resize(ref output, length);
			MemoryStream msInput = new MemoryStream(output);
			StreamReader sr = new StreamReader(msInput);
			var file = sr.ReadToEnd();
			var pages = file.Split('\x0C');
			Assert.AreEqual(4, pages.Length);			// Number of pages generated from mock data
		}
	}
}
