﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common.Log;
using WFS.RecHub.R360Shared;
using WFS.RecHub.BillingExtracts.API;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Linq;
using WFS.RecHub.Common;
using System.Threading.Tasks;

namespace BillingExtractsIntegrationTesting
{
	[TestClass]
	public class DatabaseSchedulerTests
	{
		static readonly string _executionConfigDir = Path.Combine(Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath), "CustomFileGenerators");
		static readonly string _configFilePath = Path.Combine(_executionConfigDir, "StandardScheduledTasks.config");
		static readonly object _testGate = new object();

		static AppDomain _dom1;
		static AppDomain _dom2;

		[TestInitialize]
		public void Initialize()
		{
			// Prevent parallel testing - these tests operate on a singleton's current state
			Monitor.Enter(_testGate);
		}

		[TestCleanup]
		public void Cleanup()
		{
			// Test complete - unlock the gate
			Monitor.Exit(_testGate);
			
			if (_dom1 != null)
			{
				AppDomain.Unload(_dom1);
				_dom1 = null;
			}
			if (_dom2 != null)
			{
				AppDomain.Unload(_dom2);
				_dom2 = null;
			}
		}

		[TestMethod]
		[TestCategory("DB Integration Tests")]
		public void TestDBTaskLock()
		{
			// Step: Hook the log (Fake the standard log)
			cEventLog.UseFakeInMemEventLog = true;
			InMemoryEventLog.Instance.Clear();

			// Step: Select the config to use
			const string testConfig = "StandardScheduledTasksSingle.config";
			if (File.Exists(_configFilePath))
			{
				File.Delete(_configFilePath);
				Thread.Sleep(100);
			}
			File.Copy(Path.Combine(_executionConfigDir, testConfig), _configFilePath);

			// Construct / init the API
			R360ServiceContext.UseFakeServiceContext = true;
			var api = new BillingAPI(R360ServiceContext.Current);
			api.LogEvent += InMemoryEventLog.Instance.logEvent;

			// Start the Scheduled Task Manager
			BillingBackgroundTaskRunner.ClearInMemoryRunHistory();
			ScheduledTaskManager.Init(api);

			// Verify it started and we have the log
			Thread.Sleep(100);
			Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Billing Background Scheduler: Started..."), "Scheduler Thread not initialized\r\n" + InMemoryEventLog.Instance.ToString());

			// Verify "Delay" task has started executing
			Thread.Sleep(3000);
			Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message.StartsWith("Executing Procedure : EXEC RecHubBilling.usp_Billing_ExecuteScheduledTask @parmTaskDescription = 'Test Lock'")), "Task not started\r\n" + InMemoryEventLog.Instance.ToString());
			Assert.IsFalse(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Executed task: TestWithDelay"), "Task finished too quickly\r\n" + InMemoryEventLog.Instance.ToString());

			Thread.Sleep(3000);
			Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Executed task: TestWithDelay"), "Task did not complete\r\n" + InMemoryEventLog.Instance.ToString());

			// Shut down the scheduler thread to cleanup state
			ScheduledTaskManager._cancelThreadEvent.Set();
			Thread.Sleep(100);
			Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Billing Background Scheduler: Stopped"), "Scheduler Thread not shutdown\r\n" + InMemoryEventLog.Instance.ToString());
		}

		[TestMethod]
		[TestCategory("DB Integration Tests")]
		public void TestDBSingleTaskDualLocks()
		{
			// Use two app domains to test two singletons in parallel
			_dom1 = AppDomain.CreateDomain("TestDom1", null, AppDomain.CurrentDomain.SetupInformation);
			_dom2 = AppDomain.CreateDomain("TestDom2", null, AppDomain.CurrentDomain.SetupInformation);
			var proxy1 = (TestProxy)_dom1.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().FullName, "BillingExtractsIntegrationTesting.TestProxy");
			var proxy2 = (TestProxy)_dom2.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().FullName, "BillingExtractsIntegrationTesting.TestProxy");

			// Step: Select the config to use
			const string testConfig = "StandardScheduledTasksSingle.config";
			if (File.Exists(_configFilePath))
			{
				File.Delete(_configFilePath);
				Thread.Sleep(100);
			}
			File.Copy(Path.Combine(_executionConfigDir, testConfig), _configFilePath);

			// Start the delayed task in domain 1
			proxy1.TestLockAsync();
			Thread.Sleep(1000);

			// Start in domain 2 as well - should report already locked
			proxy2.TestAlreadyLockedAsync();

			// Wait for completion
			proxy1.Join();
			proxy2.Join();
		}

		[TestMethod]
		[TestCategory("DB Integration Tests")]
		public void TestDBDualTasksDualLocks()
		{
			// Use two app domains to test two singletons in parallel
			_dom1 = AppDomain.CreateDomain("TestDom1", null, AppDomain.CurrentDomain.SetupInformation);
			_dom2 = AppDomain.CreateDomain("TestDom2", null, AppDomain.CurrentDomain.SetupInformation);
			var proxy1 = (TestProxy)_dom1.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().FullName, "BillingExtractsIntegrationTesting.TestProxy");
			var proxy2 = (TestProxy)_dom2.CreateInstanceAndUnwrap(Assembly.GetExecutingAssembly().FullName, "BillingExtractsIntegrationTesting.TestProxy");

			// Step: Select the config to use
			const string testConfig = "StandardScheduledTasksSingle.config";
			if (File.Exists(_configFilePath))
			{
				File.Delete(_configFilePath);
				Thread.Sleep(100);
			}
			File.Copy(Path.Combine(_executionConfigDir, testConfig), _configFilePath);

			// Start the delayed task in domain 1
			proxy1.TestLockAsync();
			Thread.Sleep(1000);

			// Step: Select the config to use
			const string testConfig2 = "StandardScheduledTasksSingle2.config";
			if (File.Exists(_configFilePath))
			{
				File.Delete(_configFilePath);
				Thread.Sleep(100);
			}
			File.Copy(Path.Combine(_executionConfigDir, testConfig2), _configFilePath);

			// Start in domain 2 with different task - should run normally (in parallel)
			proxy2.TestLockAsync();

			// Wait for completion
			proxy1.Join();
			proxy2.Join();
		}
	}


	public class TestProxy : MarshalByRefObject
	{
		public void Fail()
		{
			Assert.Fail("Failure from: " + AppDomain.CurrentDomain.FriendlyName);
		}

		private Exception _lastException;
		private ManualResetEvent _asyncGate = new ManualResetEvent(false);
		public void Join()
		{
			if (!_asyncGate.WaitOne(30000))
				throw new Exception("Timed Out waiting for async operation");

			if (_lastException != null)
				throw _lastException;
		}

		private void VerifyNoErrors()
		{
			// Step: Check the log messages to see if any messages of type "Error" were reported...
			var errors = InMemoryEventLog.Instance.EventList.Where(o => o.Error != null || o.Importance == MessageImportance.Essential || o.Type == MessageType.Error).ToList();
			if (errors.Count > 0)
				Assert.Fail(AppDomain.CurrentDomain.FriendlyName + ": Errors: \r\n" + errors.Select(o => o.ToString()).Aggregate((s1, s2) => s1 + "\r\n" + s2));
		}

		private string GetStateMessage(string message)
		{
			return AppDomain.CurrentDomain.FriendlyName + ": " + message + "\r\n" + InMemoryEventLog.Instance.ToString() + "\r\n~";
		}

		public void TestLockAsync()
		{
			// Run test asynchronously
			_lastException = null;
			_asyncGate.Reset();
			Task.Run(() =>
			{
				try
				{
					// Step: Hook the log (Fake the standard log)
					cEventLog.UseFakeInMemEventLog = true;
					InMemoryEventLog.Instance.Clear();

					// Construct / init the API
					R360ServiceContext.UseFakeServiceContext = true;
					var api = new BillingAPI(R360ServiceContext.Current);
					api.LogEvent += InMemoryEventLog.Instance.logEvent;

					// Start the Scheduled Task Manager
					BillingBackgroundTaskRunner.ClearInMemoryRunHistory();
					ScheduledTaskManager.Init(api);

					// Verify it started and we have the log
					Thread.Sleep(100);
					Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Billing Background Scheduler: Started..."), GetStateMessage("Scheduler Thread not initialized"));

					// Verify "Delay" task has started executing
					Thread.Sleep(3000);
					VerifyNoErrors();
					Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message.StartsWith("Executing Procedure : EXEC RecHubBilling.usp_Billing_ExecuteScheduledTask @parmTaskDescription = 'Test Lock")), GetStateMessage("Task not started"));
					Assert.IsFalse(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Executed task: TestWithDelay"), GetStateMessage("Task finished too quickly"));

					Thread.Sleep(3000);
					VerifyNoErrors();
					Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Executed task: TestWithDelay"), GetStateMessage("Task did not complete"));

					// Shut down the scheduler thread to cleanup state
					ScheduledTaskManager._cancelThreadEvent.Set();
					Thread.Sleep(100);
					Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Billing Background Scheduler: Stopped"), GetStateMessage("Scheduler Thread not shutdown"));

					VerifyNoErrors();
				}
				catch (Exception ex)
				{
					_lastException = ex;
				}
				_asyncGate.Set();
			});
		}

		public void TestAlreadyLockedAsync()
		{
			// Run test asynchronously
			_lastException = null;
			_asyncGate.Reset();
			Task.Run(() =>
			{
				try
				{
					// Step: Hook the log (Fake the standard log)
					cEventLog.UseFakeInMemEventLog = true;
					InMemoryEventLog.Instance.Clear();

					// Construct / init the API
					R360ServiceContext.UseFakeServiceContext = true;
					var api = new BillingAPI(R360ServiceContext.Current);
					api.LogEvent += InMemoryEventLog.Instance.logEvent;

					// Start the Scheduled Task Manager
					BillingBackgroundTaskRunner.ClearInMemoryRunHistory();
					ScheduledTaskManager.Init(api);

					// Verify it started and we have the log
					Thread.Sleep(100);
					Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Billing Background Scheduler: Started..."), GetStateMessage("Scheduler Thread not initialized"));

					// Verify "Delay" task has started executing and was skipped
					Thread.Sleep(3000);
					VerifyNoErrors();
					Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message.StartsWith("Executing Procedure : EXEC RecHubBilling.usp_Billing_ExecuteScheduledTask @parmTaskDescription = 'Test Lock")), GetStateMessage("Task not started"));
					Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message.StartsWith("Task (TestWithDelay) was skipped - ")), GetStateMessage("Task was not skipped"));

					// Shut down the scheduler thread to cleanup state
					ScheduledTaskManager._cancelThreadEvent.Set();
					Thread.Sleep(100);
					Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message == "Billing Background Scheduler: Stopped"), GetStateMessage("Scheduler Thread not shutdown"));

					VerifyNoErrors();
				}
				catch (Exception ex)
				{
					_lastException = ex;
				}
				_asyncGate.Set();
			});
		}
	}
}
