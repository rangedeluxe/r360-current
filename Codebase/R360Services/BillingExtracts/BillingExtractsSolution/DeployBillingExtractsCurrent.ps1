param(
	[string]$iteration = "",
	[string]$area = "",
	[string]$buildNumber = ""
)

#Shell to Common R360Services Deployment Script, with correct parameters
$thisScript = Split-Path $MyInvocation.MyCommand.Path 
$commonScript = Join-Path $thisScript "..\..\ServiceShares\R360BuildDeployment.ps1"
$commonScript = [System.IO.Path]::GetFullPath($commonScript)

$commandWithParameters = "$commonScript" `
    + " -service RecHub.R360_Service.BillingExtracts" `
    + " -appConfig:BillingExtractsService.dll.config" `
    + " -folderName:BillingExtracts" `
	+ " -buildNumber:$buildNumber" `
	+ " -iteration:`"$iteration`"" `
	+ " -area:`"$area`"" `
    + " -dlls:`"" `
        + "BillingExtractsAPI.dll," `
	    + "BillingExtractsCommon.dll," `
	    + "BillingExtractsDAL.dll," `
	    + "BillingExtractsService.dll," `
	    + "DALBase.dll," `
	    + "ipoCrypto.dll," `
	    + "ipoDB.dll," `
	    + "ipoLib.dll," `
	    + "ipoLog.dll," `
	    + "ltaLog.dll," `
		+ "R360RaamClient.dll," `
	    + "R360Shared.dll," `
	    + "SessionDAL.dll," `
	    + "Wfs.Raam.Core.dll," `
	    + "Thinktecture.IdentityModel.dll" `
        + "`"" `
    + " -standalone" `
        + " -exeName:BillingExtractsService" `
		+ " -exeDLLs:`"" `
			+ "Wfs.Raam.Core.dll," `
			+ "Thinktecture.IdentityModel.dll" `
			+ "`"" `
    + " -pluginSubFolder:CustomFileGenerators" `
        + " -pluginNamePattern:`"WFS.RecHub.BillingExtracts.*.dll,StandardScheduledTasks`"" `
        + " -pluginExcludeNamePattern:WFS.RecHub.BillingExtracts.Fake.dll" `
        + " -pluginIncludeConfig" `
#    + " -web" `

Write-Host "Invoking Deployment Script with parameters: " $commandWithParameters

invoke-expression $commandWithParameters

Write-Host "Deployment Complete"
