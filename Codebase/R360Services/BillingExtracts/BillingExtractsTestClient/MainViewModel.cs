﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WFS.RecHub.BillingExtracts.Common;
using WFS.RecHub.BillingExtracts.ServicesClient;

namespace BillingExtractsTestClient
{
	class MainViewModel : ViewModelBase
	{
		private BillingExtractsManager _mgr;
		private BillingExtractsManager Mgr
		{
			get
			{
				if (_mgr == null)
				{
					_mgr = new BillingExtractsManager();
				}
				return _mgr;
			}
		}

		public MainViewModel()
		{
			_cmdPing = new SimpleCommand(Ping);
			_cmdGenFile = new SimpleCommand(GenFile);
			_cmdGetList = new SimpleCommand(GetList);

			InitLog();
		}

		#region Ping

		private async void Ping()
		{
			// Set UI Label
			PingText = "Pinging...";
			ErrorText = string.Empty;

			try
			{
				var mgr = Mgr;

				// Perform call asynchronously
				PingResponse response = await Task.Run<PingResponse>(() => mgr.Ping());

				// Display result
				string text = response.Status.ToString() + ": ";
				if (response.Errors != null && response.Errors.Count > 0)
					text += response.Errors.Aggregate((o1, o2) => o1 + "\r\n" + o2);
				else
					text += response.responseString;
				PingText = text;
			}
			catch (Exception ex)
			{
				// Display any error
				PingText = "(Failed)";
				ErrorText = ex.ToString();
			}

			// After any action, check for log updates
			CheckLog();
		}

		private readonly SimpleCommand _cmdPing;
		public ICommand CmdPing
		{
			get { return _cmdPing; }
		}

		private string _pingText = "(Not Run)";
		public string PingText
		{
			get { return _pingText; }
			set
			{
				_pingText = string.Format("{0}: {1}", DateTime.Now.ToString("HH:mm:ss"), value);
				OnPropertyChanged();
			}
		}

		#endregion

		#region Get Formats List

		private async void GetList()
		{
			// Set UI Label
			GetListText = "Getting List...";
			ErrorText = string.Empty;

			try
			{
				var mgr = Mgr;

				// Perform call asynchronously
				BillingFormatsResponse response = await Task.Run<BillingFormatsResponse>(() => mgr.GetBillingFormats());

				// Display result
				string text = response.Status.ToString() + ": ";
				if (response.Errors != null && response.Errors.Count > 0)
				{
					FormatsList = new List<BillingFormat>();
					text += response.Errors.Aggregate((o1, o2) => o1 + "\r\n" + o2);
				}
				else
				{
					FormatsList = response.formatList;
					text += "Count: " + response.formatList.Count.ToString();
				}
				
				GetListText = text;

				if (FormatsList.Count > 0)
					SelectedFormat = FormatsList[0];
				else
					SelectedFormat = null;
			}
			catch (Exception ex)
			{
				// Display any error
				GetListText = "(Failed)";
				ErrorText = ex.ToString();
			}

			// After any action, check for log updates
			CheckLog();
		}

		private readonly SimpleCommand _cmdGetList;
		public ICommand CmdGetList
		{
			get { return _cmdGetList; }
		}

		private string _getListText = "(Not Run)";
		public string GetListText
		{
			get { return _getListText; }
			set
			{
				_getListText = string.Format("{0}: {1}", DateTime.Now.ToString("HH:mm:ss"), value);
				OnPropertyChanged();
			}
		}

		private List<BillingFormat> _formatsList;
		public List<BillingFormat> FormatsList
		{
			get { return _formatsList; }
			set
			{
				_formatsList = value;
				OnPropertyChanged();
			}
		}

		private BillingFormat _selectedFormat;
		public BillingFormat SelectedFormat
		{
			get { return _selectedFormat; }
			set
			{
				_selectedFormat = value; 
				OnPropertyChanged();
			}
		}

		#endregion

		#region Generate File

		private async void GenFile()
		{
			// Set UI Label
			GenFileText = "Generating File...";
			ErrorText = string.Empty;

			try
			{
				var mgr = Mgr;

				// Perform call asynchronously
				string paramString = "<P GenClass='" + SelectedFormat.FormatClass + "' " + GenFileParams + "/>";
				GenerateFileResponse response = await Task.Run<GenerateFileResponse>(() => mgr.GenerateFile(paramString));

				// Display result
				string text = response.Status.ToString() + ": ";
				if (response.Errors != null && response.Errors.Count > 0)
				{
					text += response.Errors.Aggregate((o1, o2) => o1 + "\r\n" + o2);
				}
				else
				{
					text += "(Check filesystem)";
				}
				GenFileText = text;
			}
			catch (Exception ex)
			{
				// Display any error
				GenFileText = "(Failed)";
				ErrorText = ex.ToString();
			}

			// After any action, check for log updates
			CheckLog();
		}

		private readonly SimpleCommand _cmdGenFile;
		public ICommand CmdGenFile
		{
			get { return _cmdGenFile; }
		}

		private string _genFileText = "(Not Run)";
		public string GenFileText
		{
			get { return _genFileText; }
			set
			{
				_genFileText = string.Format("{0}: {1}", DateTime.Now.ToString("HH:mm:ss"), value);
				OnPropertyChanged();
			}
		}

		private string _genFileParams = "Example='Value' Example2='Value2'";
		public string GenFileParams
		{
			get { return _genFileParams; }
			set { _genFileParams = value; }
		}

		#endregion

		#region Generic Error Display

		private string _errorText;
		public string ErrorText
		{
			get { return _errorText; }
			set
			{
				_errorText = value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region Log File Monitoring

		private string _logText;
		public string LogText
		{
			get { return _logText; }
			set 
			{ 
				_logText = value;
				OnPropertyChanged();
			}
		}

		// Default location for the client test program's log is "..\ipo_log.txt"
		// TODO: Read IPOnline.ini path from appSettings, and read log name from IPONline.ini...
		private string _logPath = null;
		private long _startLogOffset = 0;
		private void InitLog()
		{
			try
			{
				// Detect log path
				string localIniPath = ConfigurationManager.AppSettings["localIni"];
				if (string.IsNullOrEmpty(localIniPath))
				{
					LogText = "(INI File Not Configured)";
				}
				else
				{
					string siteKey = "[" + ConfigurationManager.AppSettings["siteKey"] + "]";
					string[] iniSettings = File.ReadAllLines(localIniPath);
					int sectionIndex = Array.IndexOf(iniSettings, siteKey);
					if (sectionIndex < 0) sectionIndex = 0;
					string logFilePathSetting = iniSettings.Skip(sectionIndex).FirstOrDefault(o => o.StartsWith("LogFile="));
					if (logFilePathSetting != null)
						_logPath = logFilePathSetting.Split(new char[] { '=' }, 2)[1];

					if (string.IsNullOrEmpty(_logPath))
					{
						LogText = "(Log File Not Configured)";
					}
					else if (File.Exists(_logPath))
					{
						LogText = "(Log File Found)";
						using (var f = File.OpenRead(_logPath))
						{
							_startLogOffset = f.Length;
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogText = ex.ToString();
			}
		}

		private void CheckLog()
		{
			try
			{
				if (string.IsNullOrEmpty(_logPath))
				{
					LogText = "(Log File Not Configured)";
				}
				else if (File.Exists(_logPath))
				{
					using (var f = File.OpenRead(_logPath))
					{
						f.Seek(_startLogOffset, SeekOrigin.Begin);
						StreamReader sr = new StreamReader(f);
						LogText = sr.ReadToEnd();
					}
				}
				else
				{
					LogText = "(Log File Not Found: " + _logPath + ")";
				}
			}
			catch (Exception ex)
			{
				LogText = ex.ToString();
			}
		}

		#endregion
	}
}
