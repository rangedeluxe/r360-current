REM TargetPath = %1 
REM TargetDir = %2
REM ProjectDir = %3
REM OutDir = %4

if %4=="bin\Debug\." goto CreateDebuggingConfig
goto End

:CreateDebuggingConfig
echo Info: Setting Debugging Configuration
copy %2\..\..\..\..\..\..\Ini\WCFClients.web.config %2\WCFClients.config
REM %3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:localhost:9500/BillingExtracts;;;localhost:8733/Design_Time_Addresses/BillingExtracts
REM %3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:localhost:9500/BillingExtracts;;;localhost.fiddler:9500/BillingExtracts
REM %3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:localhost:9500/BillingExtracts;;;rechubdevapp02.qalabs.nwk:9500/BillingExtracts /replace:@@APP_SERVER@@;;;rechubdevapp02.qalabs.nwk /replace:@@RAAM_APP_SERVER@@;;;raamdevapp01.qalabs.nwk
%3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:@@APP_SERVER@@:9500/RecHubBilling;;;localhost:9500/RecHubBilling /replace:@@APP_SERVER@@;;;rechubdevapp02.qalabs.nwk /replace:@@RAAM_APP_SERVER@@;;;rechubdevapp02.qalabs.nwk "/replace:saveBootstrapContext=""true;;;saveBootstrapContext=""false" /replace:@@RAAM_THUMBPRINT@@;;;F500FE305F6037A593F88170D8687A66791FBBBB

REM Use Fiddler...
REM %3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:"binding name=\"wsHttp\";;;binding name=\"wsHttp\" proxyAddress=\"http://localhost:8888/\" useDefaultWebProxy=\"false\" "
REM %3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:"messageEncoding=\"Text\";;;messageEncoding=\"Text\" proxyAddress=\"http://localhost:8888/\" useDefaultWebProxy=\"false\" "
REM %3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:"messageEncoding=\"Mtom\";;;messageEncoding=\"Mtom\" proxyAddress=\"http://localhost:8888/\" useDefaultWebProxy=\"false\" "
REM %3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:"https://localhost;;;https://localhost.fiddler"

:End
echo Info: Post Build Event Complete