﻿using System;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.RecHub.AuditingServicesAPI;
using WFS.RecHub.AuditingServicesAPI.ApplicationConfiguration;
using WFS.RecHub.AuditingServicesCommon.Interfaces;
using WFS.RecHub.AuditingServicesCommon.Responses;
using WFS.RecHub.AuditingServicesDAL;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 01/12/2016
*
* Purpose:
*
* Modification History
* WI 255650 TWE  01/12/2016
*    Initial
* ******************************************************************************/

namespace WFS.RecHub.AuditingServices
{
    [PublicAPI]
    public class AuditingDitSvc : LTAService, IAuditingDit
    {
        private readonly IAuditLogger _logger;
        public AuditingServicesConfiguration Settings;

        public AuditingDitSvc()
        {
            Settings = new AuditingServicesConfiguration(new ConfigurationManagerProvider());
            _logger = new AuditLogger(Settings);
            Settings.ValidateSettings();
        }

        protected T FailOnError2<T>(Func<APIBase, T> operation, [CallerMemberName] string procName = null) where T : R360BaseResponse.BaseResponse, new()
        {
            try
            {
                APIBase api = NewAPIInst(ServiceContext);
                api.LogEvent += EventLog.logEvent;
                LogManager.Logger = EventLog.IPOtoILogger(this.GetType().Name);
                var response = operation(api);
                api.LogEvent -= EventLog.logEvent;
                return response;
            }
            catch (Exception ex)
            {
                LogGeneralException(ex, procName);
                return new T() { Status = R360BaseResponse.StatusCode.FAIL };
            }
        }

        private TResponse LogDitError<TResponse>(Func<IAuditingDit, TResponse> operation)
            where TResponse : R360BaseResponse.BaseResponse, new()
        {
            return FailOnError2(oapi =>
            {
                var response = operation((IAuditingDit) oapi);
                response.Errors.ForEach(err =>
                {
                    _logger.LogMessage("Auditing Dit Service", LTAMessageImportance.Debug);
                });
                return response;
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new AuditingDitApi(_logger, serviceContext, new AuditingDitDal("ipoServices"));
        }

        public PingResponse Ping()
        {
            _logger.LogMessage("starting Ping",LTAMessageImportance.Debug);
            return LogDitError(api => api.Ping());
        }

        public R360BaseResponse.BaseResponse DitStartProcess(int auditDateKey, Guid sourceTrackingId, DateTime processingStartTime)
        {
            _logger.LogMessage("starting Dit record", LTAMessageImportance.Debug);
            return LogDitError(api => api.DitStartProcess(auditDateKey, sourceTrackingId, processingStartTime));
        }

        public R360BaseResponse.BaseResponse DitEndProcess(int auditDateKey, Guid sourceTrackingId, DateTime processingEndTime, bool isDataSuccessful)
        {
            _logger.LogMessage("End Processing Dit record", LTAMessageImportance.Debug);
            return LogDitError(api => api.DitEndProcess(auditDateKey, sourceTrackingId, processingEndTime, isDataSuccessful));
        }

        public R360BaseResponse.BaseResponse DitEndProcessAll(int auditDateKey, Guid sourceTrackingId, DateTime processingEndTime,
            bool isDataSuccessful,
            int siteBankId, int siteClientAccountId, int immutableDateKey, int sourceBatchId,
            int paymentCount, int stubCount, int documentCount)
        {
            _logger.LogMessage("End Processing Dit record", LTAMessageImportance.Debug);
            return LogDitError(api => api.DitEndProcessAll(auditDateKey, sourceTrackingId, processingEndTime, isDataSuccessful,
                siteBankId, siteClientAccountId, immutableDateKey, sourceBatchId, paymentCount, stubCount, documentCount));
        }
    }
}
