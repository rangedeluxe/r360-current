﻿using System;
using System.Diagnostics;
using WFS.LTA.Common;
using WFS.RecHub.AuditingServicesAPI.ApplicationConfiguration;

namespace WFS.RecHub.AuditingServicesAPI
{
    public class AuditLogger : IAuditLogger
    {
        public AuditLogger(AuditingServicesConfiguration settings)
        {
            Settings = settings;
        }

        private AuditingServicesConfiguration Settings { get; set; }

        /// <summary>
        /// Returns a new ltaLog with the settings from the App.Config file.
        /// </summary>
        private ltaLog CreateLtaLog()
        {
            return new ltaLog(Settings.LogFilePath,
                Settings.LogFileMaxSize,
                Settings.LoggingDepth);
        }

        /// <summary>
        /// Logs an LTA message with the settings from the App.Config file.
        /// Also includes information about the calling stack.
        /// </summary>
        public void LogMessage(string message, LTAMessageImportance importance)
        {
            var log = CreateLtaLog();
            var previouscall = new StackTrace()
                .GetFrame(1);
            // Build source string from the call stack.
            var source = string.Format("{0}:{1}",
                previouscall.GetMethod().DeclaringType.Name,
                previouscall.GetMethod().Name);
            log.logEvent(message, source, importance);
        }

        /// <summary>
        /// Write an Exception's contents to the LTA log.
        /// Also provides ability to append a custom (friendly) message.
        /// </summary>
        public void LogError(Exception ex, LTAMessageImportance importance, string customMessage = "")
        {
            if (ex != null)
            {
                var log = CreateLtaLog();
                var msg = String.Empty;
                if (!String.IsNullOrWhiteSpace(customMessage))
                    msg = "CUSTOM MESSAGE: " + customMessage + Environment.NewLine + ex.ToString();
                else
                    msg = ex.ToString();
                msg = Environment.NewLine + new string('=', 80) + Environment.NewLine + msg + Environment.NewLine;
                log.logEvent(msg, ex.Source, importance);
            }
        }
    }
}
