﻿using System;
using WFS.RecHub.AuditingServicesCommon.Interfaces;
using WFS.RecHub.AuditingServicesCommon.Responses;
using WFS.RecHub.AuditingServicesDAL;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360BaseResponse;
using WFS.LTA.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 01/12/2016
*
* Purpose: 
*
* Modification History
* WI 255650 TWE  01/12/2016
*    Initial 
* ******************************************************************************/

namespace WFS.RecHub.AuditingServicesAPI
{
    public class AuditingDitApi : APIBase, IAuditingDit, IDisposable
    {
        // Track whether Dispose has been called.
        private bool _disposed;

        private readonly IAuditingDitDal _dal;
        private readonly IAuditLogger _logger;

        public AuditingDitApi(IAuditLogger logger, R360ServiceContext serviceContext, IAuditingDitDal dal)
            : base(serviceContext)
        {
            _logger = logger;
            _dal = dal;
        }

        public PingResponse Ping()
        {
            _logger.LogMessage("starting Dit record", LTAMessageImportance.Debug);
            OnLogEvent("Executing Ping", GetType().Name, MessageType.Information, MessageImportance.Debug);

            var output = new PingResponse()
            {
                ResponseString = "Pong"
            };
            return output;
        }

        public R360BaseResponse.BaseResponse DitStartProcess(int auditDateKey, Guid sourceTrackingId, DateTime processingStartTime)
        {
            var output = new R360BaseResponse.BaseResponse();

            OnLogEvent("Executing Dit Start",GetType().Name,MessageType.Information, MessageImportance.Debug);

            var bolRetVal = _dal.InsertDitStartTime(auditDateKey, sourceTrackingId, processingStartTime);

            output.Status = bolRetVal ? R360BaseResponse.StatusCode.SUCCESS : R360BaseResponse.StatusCode.FAIL;

            return output;
        }

        public R360BaseResponse.BaseResponse DitEndProcess(int auditDateKey, Guid sourceTrackingId, DateTime processingEndTime, bool isDataSuccessful)
        {
            var output = new R360BaseResponse.BaseResponse();

            OnLogEvent("Executing Dit Auditing End ", GetType().Name, MessageType.Information, MessageImportance.Debug);

            var bolRetVal = _dal.InsertDitEndTime(auditDateKey, sourceTrackingId, processingEndTime, isDataSuccessful);

            output.Status = bolRetVal ? R360BaseResponse.StatusCode.SUCCESS : R360BaseResponse.StatusCode.FAIL;

            return output;
        }

        public R360BaseResponse.BaseResponse DitEndProcessAll(int auditDateKey, Guid sourceTrackingId, DateTime processingEndTime, bool isDataSuccessful,
            int siteBankId, int siteClientAccountId, int immutableDateKey, int sourceBatchId,
            int paymentCount, int stubCount, int documentCount)
        {
            var output = new R360BaseResponse.BaseResponse();

            OnLogEvent("Executing Dit Auditing End ", GetType().Name, MessageType.Information, MessageImportance.Debug);

            var bolRetVal = _dal.InsertDitEndTime(auditDateKey, sourceTrackingId, processingEndTime, isDataSuccessful,
                 siteBankId,  siteClientAccountId,  immutableDateKey,  sourceBatchId,
                 paymentCount,  stubCount,  documentCount);

            output.Status = bolRetVal ? R360BaseResponse.StatusCode.SUCCESS : R360BaseResponse.StatusCode.FAIL;

            return output;
        }

        protected override bool ValidateSID(out int userId)
        {
            throw new NotImplementedException();
        }

        protected override ActivityCodes RequestType
        {
            get { throw new NotImplementedException(); }
        }

        #region IDisposable Implementation
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!_disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    //if (_sessiondal != null)
                    //{
                    //    _sessiondal.Dispose();
                    //}
                    // Dispose managed resources.
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                _disposed = true;

            }
        }
        #endregion
    }
}
