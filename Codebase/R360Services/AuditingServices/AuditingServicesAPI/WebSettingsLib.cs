﻿using System;
using System.Configuration;
using System.Linq;

namespace WFS.RecHub.AuditingServicesAPI
{
    /// <summary>
    /// Used to reference web.config appSettings keys
    /// </summary>
    public enum WebSettings
    {
        LogFileMaxSize,
        LogFilePath,
        SiteKey
    }
    public static class WebSettingsLib
    {
        /// <summary>
        /// retrieve appSettings by Enum key and provide default value (optional)
        /// </summary>
        public static string Get(WebSettings key, string defaultValue = "")
        {
            return Get(key.ToString(), defaultValue);
        }

        /// <summary>
        /// retrieve appSettings by String key and provide default value (optional)
        /// </summary>
        public static string Get(string key, string defaultValue = "")
        {
            return Get<string>(key, defaultValue);
        }

        /// <summary>
        /// Get a value cast from String to type 'T"
        /// </summary>
        /// <typeparam name="T">The desired return type.</typeparam>
        /// <returns></returns>
        public static T Get<T>(WebSettings key)
        {
            return Get<T>(key, default(T));

        }

        /// <summary>
        /// Get a value cast from String to type 'T" providing a default value.
        /// </summary>
        /// <typeparam name="T">The desired return type.</typeparam>
        public static T Get<T>(WebSettings key, T defaultValue)
        {
            return Get<T>(key.ToString(), defaultValue);
        }

        public static T Get<T>(string key, T defaultValue)
        {
            var result = defaultValue;
            key = key.Trim();

            try
            {
                if (!String.IsNullOrWhiteSpace(key))
                {
                    if (ConfigurationManager.AppSettings.AllKeys.Contains(key))
                    {
                        var s = ConfigurationManager.AppSettings[key];
                        if (!String.IsNullOrWhiteSpace(s))
                            result = (T)Convert.ChangeType(s, typeof(T));
                    }
                }
            }
            catch (Exception)
            {
                return default(T);
            }

            return result;
        }
    }
}
