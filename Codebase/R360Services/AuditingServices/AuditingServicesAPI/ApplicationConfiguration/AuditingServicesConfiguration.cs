﻿using WFS.RecHub.ApplicationBlocks.Common.Configuration;
using WFS.LTA.Common;
using System;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;

namespace WFS.RecHub.AuditingServicesAPI.ApplicationConfiguration
{
    public class AuditingServicesConfiguration : BaseConfigurationSettings
    {
        [ConfigurationSetting]
        public string LogFilePath { get; set; }

        [ConfigurationSetting]
        public string SiteKey { get; set; }

        public int LogFileMaxSize { get; set; }

        public LTAMessageImportance LoggingDepth { get; set; }

        public AuditingServicesConfiguration(IConfigurationProvider provider)
            : base(provider)
        {
            int tempint;

            // Log File Path
            if (string.IsNullOrWhiteSpace(LogFilePath))
                LogFilePath = Environment.CurrentDirectory;

            // Log File Max Size
            var logFileMaxSize = provider.GetSetting("LogFileMaxSize");
            var sizeint = logFileMaxSize.GetInteger();
            if (sizeint <= 0)
                throw new ConfigurationException("LogFileMaxSize");
            else
                LogFileMaxSize = sizeint;

            // SiteKey
            if (string.IsNullOrWhiteSpace(SiteKey))
                throw new ConfigurationException("SiteKey");

            // Logging Depth
            var loggingDepth = provider.GetSetting("LoggingDepth");
            if (string.IsNullOrWhiteSpace(loggingDepth)
                || !int.TryParse(loggingDepth, out tempint))
                throw new ConfigurationException("LoggingDepth");
            if (!Enum.IsDefined(typeof(LTAMessageImportance), tempint))
                throw new ConfigurationException("LoggingDepth");
            LoggingDepth = (LTAMessageImportance)Enum.Parse(typeof(LTAMessageImportance), loggingDepth);
        }

        public override void ValidateSettings()
        {
        }
    }
}
