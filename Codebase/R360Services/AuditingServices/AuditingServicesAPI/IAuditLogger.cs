﻿using System;
using WFS.LTA.Common;

namespace WFS.RecHub.AuditingServicesAPI
{
    public interface IAuditLogger
    {
        /// <summary>
        /// Logs an LTA message with the settings from the App.Config file.
        /// Also includes information about the calling stack.
        /// </summary>
        void LogMessage(string message, LTAMessageImportance importance);

        /// <summary>
        /// Write an Exception's contents to the LTA log.
        /// Also provides ability to append a custom (friendly) message.
        /// </summary>
        void LogError(Exception ex, LTAMessageImportance importance, string customMessage = "");
    }
}