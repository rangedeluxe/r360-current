﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 01/12/2016
*
* Purpose: 
*
* Modification History
* WI 255650 TWE  01/12/2016
*    Initial 
* ******************************************************************************/

namespace WFS.RecHub.AuditingServicesDAL
{
    public class AuditingDitDal : _DALBase, IAuditingDitDal
    {
        public AuditingDitDal(string vSiteKey)
            : base(vSiteKey, ConnectionType.RecHubData)
        { }

        public bool InsertDitStartTime(int auditDateKey, Guid sourceTrackingId, DateTime processingStartTime)
        {
            var bolRetVal = false;

            EventLog.logEvent("ready to insert record",GetType().Name,MessageType.Information,MessageImportance.Debug);

            try
            {
                var parms = new[]
                {
                    BuildParameter("@parmAuditDateKey", SqlDbType.Int, auditDateKey, ParameterDirection.Input),
                    BuildParameter("@parmSourceTrackingID", SqlDbType.UniqueIdentifier, sourceTrackingId,
                        ParameterDirection.Input),
                    BuildParameter("@parmProcessingStart", SqlDbType.DateTime2, processingStartTime,
                        ParameterDirection.Input)
                };
                bolRetVal = Database.executeProcedure("RecHubAudit.usp_factDITClientAudit_Ins", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name, "InsertDITStartTime(int auditDateKey, Guid sourceTrackingId, DateTime processingStartTime)");
            }

            return bolRetVal;
        }

        public bool InsertDitEndTime(int auditDateKey, Guid sourceTrackingId, DateTime processingEndTime, bool isDataSuccessful)
        {
            return InsertDitEndTime(auditDateKey, sourceTrackingId, processingEndTime, isDataSuccessful, -1,-1,-1,-1,-1,-1,-1);
        }


        public bool InsertDitEndTime(int auditDateKey, Guid sourceTrackingId, DateTime processingEndTime, bool isDataSuccessful,
            int siteBankId, int siteClientAccountId, int immutableDateKey, int sourceBatchId,
            int paymentCount, int stubCount, int documentCount)
        {
            var bolRetVal = false;

            EventLog.logEvent("ready to insert record", GetType().Name, MessageType.Information, MessageImportance.Debug);

            try
            {
                SqlParameter[] parms;
                List<SqlParameter> parmList;
                parmList = new List<SqlParameter>();

                // required parameters
                parmList.Add(BuildParameter("@parmAuditDateKey", SqlDbType.Int, auditDateKey, ParameterDirection.Input));
                parmList.Add(BuildParameter("@parmSourceTrackingID", SqlDbType.UniqueIdentifier, sourceTrackingId, ParameterDirection.Input));
                parmList.Add(BuildParameter("@parmProcessingEnd", SqlDbType.DateTime2, processingEndTime, ParameterDirection.Input));
                parmList.Add(BuildParameter("@parmIsDataSuccessful", SqlDbType.Bit, isDataSuccessful, ParameterDirection.Input));

                // optional parameters
                if (siteBankId > -1)
                    parmList.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId, ParameterDirection.Input));
                if (siteClientAccountId > -1)
                    parmList.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountId, ParameterDirection.Input));
                if (immutableDateKey > -1)
                    parmList.Add(BuildParameter("@parmImmutableDateKey", SqlDbType.Int, immutableDateKey, ParameterDirection.Input));
                if (sourceBatchId > -1)
                    parmList.Add(BuildParameter("@parmSourceBatchID", SqlDbType.Int, sourceBatchId, ParameterDirection.Input));
                if (paymentCount > -1)
                    parmList.Add(BuildParameter("@parmPaymentCount", SqlDbType.Int, paymentCount, ParameterDirection.Input));
                if (stubCount > -1)
                    parmList.Add(BuildParameter("@parmStubCount", SqlDbType.Int, stubCount, ParameterDirection.Input));
                if (documentCount > -1)
                    parmList.Add(BuildParameter("@parmDocumentCount", SqlDbType.Int, documentCount, ParameterDirection.Input));

                parms = parmList.ToArray();
                bolRetVal = Database.executeProcedure("RecHubAudit.usp_factDITClientAudit_Upd_ProcessingEnd", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name, "InsertDitEndTime(int auditDateKey, Guid sourceTrackingId, DateTime processingStartTime, bool isDataSuccessful)");
            }

            return bolRetVal;
        }
    }
}
