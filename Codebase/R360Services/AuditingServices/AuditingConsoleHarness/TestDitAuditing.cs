﻿

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 01/12/2016
*
* Purpose: 
*
* Modification History
* WI 255650 TWE  01/12/2016
*    Initial 
* ******************************************************************************/

using System;
using System.Threading;
using WFS.RecHub.AuditingServicesCommon.Responses;
using WFS.RecHub.R360BaseResponse;

namespace WFS.RecHub.AuditingConsoleHarness
{
    public class TestDitAuditing
    {
        private readonly AuditingServicesClient.AuditingServicesClient _client;
        private readonly int _auditDateKey;
        private readonly Guid _sourceTrackingId;
        
        public TestDitAuditing()
        {
            _client = new AuditingServicesClient.AuditingServicesClient();
            DateTime currentDateTime = DateTime.Now;

            _auditDateKey = int.Parse(currentDateTime.ToString("yyyyMMdd"));
            _sourceTrackingId = Guid.NewGuid();

        }

        public void HappyPathCallAllTests()
        {
            Console.WriteLine("  ");
            CallPing();

            Console.WriteLine("  ");
            CallStart();

            Console.WriteLine("  ");
            CallEndProcessing(true);
        }

        public void FailTest01Path()
        {
            Console.WriteLine("  ");
            CallPing();

            Console.WriteLine("  ");
            CallStart();

            Console.WriteLine("  ");
            CallEndProcessing(false);
        }

        public void Test02Path()
        {
            Console.WriteLine("  ");
            CallPing();

            Console.WriteLine("  ");
            CallStart();

            Console.WriteLine("  ");
            CallEndProcessing(true,-1,-1,-1,-1,-1,-1,-1);
        }

        public void Test03Path()
        {
            Console.WriteLine("  ");
            CallPing();

            Console.WriteLine("  ");
            CallStart();

            Console.WriteLine("  ");
            CallEndProcessing(false, 1, 2, 3, 4, 5, 6, 7);
        }

        private void CallPing()
        {
            Console.WriteLine("Ready to submit call Ping");
            //first ping
            PingResponse response = _client.Ping();

            if (response.Status == StatusCode.SUCCESS)
            {
                Console.WriteLine("Call was successful");
            }
            else
            {
                Console.WriteLine("Call failed");
            }
        }

        private void CallStart()
        {
            Console.WriteLine("Ready to submit call DitStartProcess");
            BaseResponse response = _client.DitStartProcess(_auditDateKey, _sourceTrackingId, DateTime.Now);

            if (response.Status == StatusCode.SUCCESS)
            {
                Console.WriteLine("Call was successful");
            }
            else
            {
                Console.WriteLine("Call failed");
            }
        }

        private void CallEndProcessing(bool isDataSuccessful)
        {
            //simulate the end process time stamp
            //sleep for 5 seconds
            Thread.Sleep(5000);
            
            Console.WriteLine("Ready to submit call CallEndProcessing");
            BaseResponse response = _client.DitEndProcess(_auditDateKey, _sourceTrackingId, DateTime.Now, isDataSuccessful);

            if (response.Status == StatusCode.SUCCESS)
            {
                Console.WriteLine("Call was successful");
            }
            else
            {
                Console.WriteLine("Call failed");
            }
        }

        private void CallEndProcessing(bool isDataSuccessful, 
            int siteBankId, int siteClientAccountId, int immutableDateKey, int sourceBatchId,
            int paymentCount, int stubCount, int documentCount)
        {
            //simulate the end process time stamp
            //sleep for 5 seconds
            Thread.Sleep(5000);

            Console.WriteLine("Ready to submit call CallEndProcessingAllFields");
            BaseResponse response = _client.DitEndProcessAll(_auditDateKey, _sourceTrackingId, DateTime.Now, isDataSuccessful,
                siteBankId,siteClientAccountId,immutableDateKey,sourceBatchId,
                paymentCount,stubCount,documentCount);

            if (response.Status == StatusCode.SUCCESS)
            {
                Console.WriteLine("Call was successful");
            }
            else
            {
                Console.WriteLine("Call failed");
            }
        }
    }
}
