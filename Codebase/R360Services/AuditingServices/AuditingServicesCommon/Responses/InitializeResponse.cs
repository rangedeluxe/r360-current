﻿using WFS.RecHub.R360BaseResponse;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 01/12/2016
*
* Purpose: 
*
* Modification History
* WI 255650 TWE  01/12/2016
*    Initial 
* ******************************************************************************/

namespace WFS.RecHub.AuditingServicesCommon.Responses
{
    public class InitializeResponse : BaseResponse
    {
    }
}
