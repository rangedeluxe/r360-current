﻿using System;
using System.Configuration;
using System.ServiceModel;
using JetBrains.Annotations;
using WFS.RecHub.AuditingServicesCommon.Interfaces;
using WFS.RecHub.AuditingServicesCommon.Responses;
using WFS.RecHub.R360BaseResponse;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 01/12/2016
*
* Purpose: 
*
* Modification History
* WI 255650 TWE  01/12/2016
*    Initial 
* ******************************************************************************/

namespace WFS.RecHub.AuditingServicesClient
{
    [PublicAPI]
    public class AuditingServicesClient : ServicesClientBase, IAuditingDit
    {
        private IAuditingDit _clientAuditingDit;
        private WS2007HttpBinding _binding = new WS2007HttpBinding("DITAuditingServiceConfig");

        public AuditingServicesClient()
        {
            EndpointAddress endpointAddress = new EndpointAddress( ConfigurationManager.AppSettings["AuditingService"]);
            _clientAuditingDit = new ChannelFactory<IAuditingDit>(_binding, endpointAddress).CreateChannel();
        }

        public PingResponse Ping()
        {
            return _clientAuditingDit.Ping();
        }

        public BaseResponse DitStartProcess(int auditDateKey, Guid sourceTrackingId, DateTime processingStartTime)
        {
            return _clientAuditingDit.DitStartProcess(auditDateKey,sourceTrackingId,processingStartTime);
        }

        public BaseResponse DitEndProcess(int auditDateKey, Guid sourceTrackingId, DateTime processingEndTime, bool isDataSuccessful)
        {
            return _clientAuditingDit.DitEndProcess(auditDateKey, sourceTrackingId, processingEndTime, isDataSuccessful);
        }

        public BaseResponse DitEndProcessAll(int auditDateKey, Guid sourceTrackingId, DateTime processingEndTime, bool isDataSuccessful,
            int siteBankId, int siteClientAccountId, int immutableDateKey, int sourceBatchId,
            int paymentCount, int stubCount, int documentCount)
        {
            return _clientAuditingDit.DitEndProcessAll(auditDateKey, sourceTrackingId, processingEndTime, isDataSuccessful,
                 siteBankId,  siteClientAccountId,  immutableDateKey,  sourceBatchId,
                 paymentCount,  stubCount,  documentCount);
        }
    }
}
