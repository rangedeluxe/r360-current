﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using WFS.RecHub.PostDepositBusinessRules;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace R360Tests
{
    /// <summary>
    /// Summary description for BusinessRulesEngineTests
    /// </summary>
    [TestClass]
    public class PostDepositBusinessRulesTests
    {
        private DataEntryField CreateDataEntryField(string name, string value)
        {
            return new DataEntryField
            {
                Name = name,
                Value = value
            };
        }

        private DataEntryFieldRule CreateDataEntryFieldRule(string fieldName, bool isCheck = true,
            bool isRequird = true)
        {
            return new DataEntryFieldRule
            {
                IsCheck = isCheck,
                FieldName = fieldName,
                IsRequired = isRequird
            };
        }

        private Payment CreatePayment(params string[] dataEntryFields)
        {
            var payment = new Payment();

            foreach (var dataEntryField in dataEntryFields)
            {
                payment.DataEntryFields.Add(CreateDataEntryField(dataEntryField.Split(',')[0],
                    dataEntryField.Split(',')[1]));
            }
            return payment;
        }

        private Stub CreateStub(params DataEntryField[] dataEntryFields)
        {
            var stub = new Stub();

            foreach (var dataEntryField in dataEntryFields)
            {
                stub.DataEntryFields.Add(dataEntryField);
            }
            return stub;
        }

        private void ValidateResponseErrors(IReadOnlyList<string> errors, string[] expectedErrors)
        {
            // First assert we get the expected number of errors returned.
            Assert.AreEqual(expectedErrors.Length, errors.Count);
            for (int i = 0; i < errors.Count; i++)
            {
                Assert.AreEqual(expectedErrors[i], errors[i], "ValidateResponseErrors");
            }
        }

        private ValidationResponse ValidateTransaction(Transaction transaction, Rules rules)
        {
            // Call the BRE to validate the transaction
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = new BusinessRulesEngine(ruleProvider);
            return businessRulesEngine.Validate(transaction);
        }

        private DetailedValidationResponse<Stub> DetailedTransactionValidation(Transaction transaction, Rules rules)
        {
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = new BusinessRulesEngine(ruleProvider);
            return businessRulesEngine.DetailedValidation(transaction);
        }

        [TestMethod]
        public void PostDepositBusinessRules_NoDataEntryRules()
        {
            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Payments.Add(CreatePayment("PaymentField1,"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "")));
            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, new Rules());

            Assert.AreEqual(true, response.Success, "No Rules");
        }

        [TestMethod]
        public void PostDepositBusinessRules_PaymentField_RequiredFieldMissing()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Payments.Add(CreatePayment("PaymentField1,"));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            ValidateResponseErrors(response.Errors, new[] {"Field is required"});
        }

        [TestMethod]
        public void PostDepositBusinessRules_PaymentField_RequiredFieldPresent()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Payments.Add(CreatePayment("PaymentField1,V1"));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            Assert.AreEqual(true, response.Success, "Payment noexception");
        }

        [TestMethod]
        public void PostDepositBusinessRules_StubField_RequiredFieldMissing()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "")));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            ValidateResponseErrors(response.Errors, new[] {"Field is required"});
        }

        [TestMethod]
        public void PostDepositBusinessRules_DetailedTransactionValidation_StubField_RequiredFieldMissing()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "")));

            // Call the BRE to validate the transaction
            var response = DetailedTransactionValidation(transaction, rules);

            Assert.IsTrue(ValidateDetailedResponseErrors(response.StubErrors, transaction.Stubs[0], "StubField1",
                "Field is required"));
        }

        private bool ValidateDetailedResponseErrors<T>(IReadOnlyDictionary<T, DataEntryError[]> dataEntryErrors,
            T dataEntryRow, string fieldName, string error)
        {
            Assert.IsTrue(dataEntryErrors.ContainsKey(dataEntryRow));
            Assert.IsTrue(dataEntryErrors[dataEntryRow].Any(errorItem =>
                errorItem.Field.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase)));
            return
                dataEntryErrors[dataEntryRow].Any(
                    errorItem => errorItem.Field.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase) &&
                                 errorItem.Error.Equals(error, StringComparison.CurrentCultureIgnoreCase));
        }

        [TestMethod]
        public void PostDepositBusinessRules_StubField_RequiredFieldMissingStub()
        {
            // Set up the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            ValidateResponseErrors(response.Errors, new[] {"Field is required"});
        }

        [TestMethod]
        public void PostDepositBusinessRules_StubField_NotRequiredFieldMissingStub()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false, false));

            // Setup the transaction for this test
            var transaction = new Transaction();

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            ValidateResponseErrors(response.Errors, new string[] { });
        }

        [TestMethod]
        public void PostDepositBusinessRules_StubField_RequiredFieldPresent()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V1")));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            Assert.AreEqual(true, response.Success, "Stub noexception");
        }

        [TestMethod]
        public void PostDepositBusinessRules_PaymentAndStubField_RequiredFieldMissing()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Payments.Add(CreatePayment("PaymentField1,"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "")));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            ValidateResponseErrors(response.Errors,
                new[] {"Field is required", "Field is required"});
        }

        [TestMethod]
        public void PostDepositBusinessRules_PaymentAndStubField_RequiredFieldWhitespaces()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Payments.Add(CreatePayment("PaymentField1,   "));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "   ")));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            ValidateResponseErrors(response.Errors,
                new[] {"Field is required", "Field is required"});
        }

        [TestMethod]
        public void PostDepositBusinessRules_PaymentAndStubField_RequiredFieldTabs()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Payments.Add(CreatePayment("PaymentField1,\t"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "\t\t")));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            ValidateResponseErrors(response.Errors,
                new[] {"Field is required", "Field is required"});
        }

        [TestMethod]
        public void PostDepositBusinessRules_PaymentAndStubField_RequiredFieldPresent()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            Assert.AreEqual(true, response.Success, "Payment and Stub noexception");
        }
        [TestMethod]
        public void PostDepositBusinessRules_PaymentAndStubField_IgnoreFieldNameCase()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));

            // Setup the transaction for this test
            var transaction = new Transaction();
            transaction.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response = ValidateTransaction(transaction, rules);

            Assert.AreEqual(true, response.Success, "Payment and Stub noexception");
        }

        [TestMethod]
        public void PostDepositBusinessRules_CacheRules_LookupOnce()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            //Setup the rule providers
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var cacheRuleProvider = new CachingRuleProvider(ruleProvider);
            var businessRulesEngine = new BusinessRulesEngine(cacheRuleProvider);

            // Setup the transaction for this test
            var transaction = new Transaction
            {
                SiteBankId = 1,
                SiteWorkgroupId = 123,
                PaymentSourceKey = 5
            };
            transaction.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response = businessRulesEngine.Validate(transaction);
            Assert.AreEqual(true, response.Success, "PostDepositBusinessRules_CacheRules");
            Assert.AreEqual(1, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules first transaction");

            var transaction2 = new Transaction
            {
                SiteBankId = 1,
                SiteWorkgroupId = 123,
                PaymentSourceKey = 5
            };
            transaction2.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction2.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction2.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction2.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response2 = businessRulesEngine.Validate(transaction2);
            Assert.AreEqual(1, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules second transaction");

            Assert.AreEqual(true, response2.Success, "PostDepositBusinessRules_CacheRules");
        }

        [TestMethod]
        public void PostDepositBusinessRules_CacheRules_LookupChangedBankID()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            //Setup the rule providers
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var cacheRuleProvider = new CachingRuleProvider(ruleProvider);
            var businessRulesEngine = new BusinessRulesEngine(cacheRuleProvider);

            // Setup the transaction for this test
            var transaction = new Transaction
            {
                SiteBankId = 1,
                SiteWorkgroupId = 123,
                PaymentSourceKey = 5
            };
            transaction.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response = businessRulesEngine.Validate(transaction);
            Assert.AreEqual(true, response.Success, "PostDepositBusinessRules_CacheRules_LookupChangedBankID");
            Assert.AreEqual(1, ruleProvider.LookupCount,
                "PostDepositBusinessRules_CacheRules_LookupChangedBankID first transaction");

            var transaction2 = new Transaction
            {
                SiteBankId = 2,
                SiteWorkgroupId = 123,
                PaymentSourceKey = 5
            };
            transaction2.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction2.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction2.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction2.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response2 = businessRulesEngine.Validate(transaction2);
            Assert.AreEqual(2, ruleProvider.LookupCount,
                "PostDepositBusinessRules_CacheRules_LookupChangedBankID second transaction");

            Assert.AreEqual(true, response2.Success, "PostDepositBusinessRules_CacheRules_LookupChangedBankID");
        }

        [TestMethod]
        public void PostDepositBusinessRules_CacheRules_LookupChangedClientAccountID()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            //Setup the rule providers
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var cacheRuleProvider = new CachingRuleProvider(ruleProvider);
            var businessRulesEngine = new BusinessRulesEngine(cacheRuleProvider);

            // Setup the transaction for this test
            var transaction = new Transaction
            {
                SiteBankId = 1,
                SiteWorkgroupId = 123,
                PaymentSourceKey = 5
            };
            transaction.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response = businessRulesEngine.Validate(transaction);
            Assert.AreEqual(true, response.Success, "PostDepositBusinessRules_CacheRules_LookupChangedClientAccountID");
            Assert.AreEqual(1, ruleProvider.LookupCount,
                "PostDepositBusinessRules_CacheRules_LookupChangedClientAccountID first transaction");

            var transaction2 = new Transaction
            {
                SiteBankId = 1,
                SiteWorkgroupId = 456,
                PaymentSourceKey = 5
            };
            transaction2.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction2.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction2.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction2.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response2 = businessRulesEngine.Validate(transaction2);
            Assert.AreEqual(2, ruleProvider.LookupCount,
                "PostDepositBusinessRules_CacheRules_LookupChangedClientAccountID second transaction");

            Assert.AreEqual(true, response2.Success,
                "PostDepositBusinessRules_CacheRules_LookupChangedClientAccountID");
        }

        [TestMethod]
        public void PostDepositBusinessRules_CacheRules_LookupChangedPaymentSource()
        {
            // Setup the rules for this test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            //Setup the rule providers
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var cacheRuleProvider = new CachingRuleProvider(ruleProvider);
            var businessRulesEngine = new BusinessRulesEngine(cacheRuleProvider);

            // Setup the transaction for this test
            var transaction = new Transaction
            {
                SiteBankId = 1,
                SiteWorkgroupId = 123,
                PaymentSourceKey = 5
            };
            transaction.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response = businessRulesEngine.Validate(transaction);
            Assert.AreEqual(true, response.Success, "PostDepositBusinessRules_CacheRules_LookupChangedPaymentSource");
            Assert.AreEqual(1, ruleProvider.LookupCount,
                "PostDepositBusinessRules_CacheRules_LookupChangedPaymentSource first transaction");

            var transaction2 = new Transaction
            {
                SiteBankId = 1,
                SiteWorkgroupId = 123,
                PaymentSourceKey = 116
            };
            transaction2.Payments.Add(CreatePayment("PaymentField1,V1"));
            transaction2.Payments.Add(CreatePayment("paymentfield1,V1"));
            transaction2.Payments.Add(CreatePayment("paymentField1,V1"));
            transaction2.Payments.Add(CreatePayment("Paymentfield1,V1"));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("StubField1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("stubfield1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("stubField1", "V2")));
            transaction2.Stubs.Add(CreateStub(CreateDataEntryField("Stubfield1", "V2")));

            // Call the BRE to validate the transaction
            var response2 = businessRulesEngine.Validate(transaction2);
            Assert.AreEqual(2, ruleProvider.LookupCount,
                "PostDepositBusinessRules_CacheRules_LookupChangedPaymentSource second transaction");

            Assert.AreEqual(true, response2.Success, "PostDepositBusinessRules_CacheRules_LookupChangedPaymentSource");
        }
    }
}
