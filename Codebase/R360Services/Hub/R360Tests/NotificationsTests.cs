﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;
using WFS.RecHub.R360Shared;

namespace R360Tests
{
    [TestClass]
    public class NotificationsTests
    {
        private NotificationsRepository _repo;
        private IServiceContext _serviceContext;
        private const string DISALLOWEDCHARACTERS = @"\t|\n|\r\\t|\\n|\\r";
        public NotificationsTests()
        {
            _serviceContext = new MockServiceContext();
            _repo = new NotificationsRepository(_serviceContext,
                new MockNotificationsDataProvider(), 
                new MockPreferencesProvider(new List<cOLPreference>
                {
                    new cOLPreference()
                    {
                        PreferenceName = "RecordsPerPage",
                        UserSetting = "50"
                    }
                }), 
                new NotificationsRequestXmlTransformer(), 
                new MockRaamPolicyProvider());
        }

        [TestMethod]
        public void Notification_Message_Should_Not_Contain_Special_Characters()
        {
            //arrange
            var response = _repo.Get(new NotificationRequestDto {MessageGroup = 1});
            //act
            var matches = Regex.Matches(response.Notification.Message, DISALLOWEDCHARACTERS);
            //assert
            Assert.IsTrue(matches.Count == 0);
        }

        [TestMethod]
        public void Notifications_Messages_Should_Not_Contain_Special_Characters()
        {
            //
            var response = _repo.GetAll(new NotificationsRequestDto {Workgroup = "someWorkgroup"});
            var foundMatch = response.Notifications.Any(x => Regex.IsMatch(x.Message, DISALLOWEDCHARACTERS));
            //assert
            Assert.IsFalse(foundMatch);
        }
    }
}
