﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;

namespace R360Tests
{
    /// <summary>
    /// <para>For purposes of these tests:</para>
    /// <para>"Blank" = a field that's left blank (and doesn't trigger an exception because of it).</para>
    /// <para>"Acceptable" = a value that triggers neither a bad-data-type error, nor an exception.</para>
    /// <para>"Exceptional" = a value that triggers a business-rule exception (and not a bad-data-type error).</para>
    /// <para>"Mistyped" = a value that triggers a bad-data-type error (and not a business-rule exception).</para>
    /// </summary>
    [TestClass]
    public class SaveTransactionHandlerTests_OneEditableStub : SaveTransactionHandlerTestCase
    {
        protected override void SetUpScenario()
        {
            SetUpDefaultWorkgroup();
            SetUpDefaultTransactionAndStub();
            LockToCurrentUser();
        }

        [TestMethod]
        public void BlankField_Numeric()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric);
            Execute(CreateFieldSaveRequest(field1, ""));
            Expect
                .SaveWasCalledForFields(SavedField(field1, ""))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void BlankField_Currency()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Currency);
            Execute(CreateFieldSaveRequest(field1, ""));
            Expect
                .SaveWasCalledForFields(SavedField(field1, ""))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void BlankField_Date()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Date);
            Execute(CreateFieldSaveRequest(field1, ""));
            Expect
                .SaveWasCalledForFields(SavedField(field1, ""))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void BlankField_Alphanumeric()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Alphanumeric);
            Execute(CreateFieldSaveRequest(field1, ""));
            Expect
                .SaveWasCalledForFields(SavedField(field1, ""))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void AcceptableField_Numeric()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric);
            Execute(CreateFieldSaveRequest(field1, "1234.5"));
            Expect
                .SaveWasCalledForFields(SavedField(field1, "1234.5", valueFloat: 1234.5f))
                .StatusWasSuccess()
                .NoDataTypeErrors();
        }
        [TestMethod]
        public void AcceptableField_Currency()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Currency);
            Execute(CreateFieldSaveRequest(field1, "1234.5"));
            Expect
                .SaveWasCalledForFields(SavedField(field1, "1234.5", valueMoney: 1234.5m))
                .StatusWasSuccess()
                .NoDataTypeErrors();
        }
        [TestMethod]
        public void AcceptableField_Date()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Date);
            Execute(CreateFieldSaveRequest(field1, "1/1/2001"));
            Expect
                .SaveWasCalledForFields(SavedField(field1, "1/1/2001", valueDateTime: new DateTime(2001, 1, 1)))
                .StatusWasSuccess()
                .NoDataTypeErrors();
        }
        [TestMethod]
        public void AcceptableField_Alphanumeric()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Alphanumeric);
            Execute(CreateFieldSaveRequest(field1, "1234"));
            Expect
                .SaveWasCalledForFields(SavedField(field1, "1234"))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void ExceptionalField()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Alphanumeric, isRequired: true);
            Execute(CreateFieldSaveRequest(field1, ""));
            Expect
                .SaveWasCalledForFields(SavedField(field1, ""))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .BusinessRuleExceptions(Tuple.Create(field1, Messages.BusinessRuleException_FieldIsRequired));
        }
        [TestMethod]
        public void MistypedField_Numeric()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric);
            Execute(CreateFieldSaveRequest(field1, "123x"));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(Tuple.Create(field1, Messages.StubError_BadNumericValue))
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void MistypedField_Date()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Date);
            Execute(CreateFieldSaveRequest(field1, "not a Date"));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(Tuple.Create(field1, Messages.StubError_BadDateValue))
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void MistypedField_Currency()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Currency);
            Execute(CreateFieldSaveRequest(field1, "not Currency"));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(Tuple.Create(field1, Messages.StubError_BadCurrencyValue))
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void MistypedField_CurrencyBeyondMax()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Currency);
            Execute(CreateFieldSaveRequest(field1, StringifyInvariant(MaxCurrencyValue + 1)));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(Tuple.Create(field1, Messages.StubError_BadCurrencyValue_BeyondMax))
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void MistypedField_CurrencyBeyondMin()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Currency);
            Execute(CreateFieldSaveRequest(field1, StringifyInvariant(-MaxCurrencyValue - 1)));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(Tuple.Create(field1, Messages.StubError_BadCurrencyValue_BeyondMin))
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void Acceptable_And_Acceptable()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric);
            var field2 = DefineDataEntryField(DataEntryDataType.Numeric);
            Execute(
                CreateFieldSaveRequest(field1, "1234"),
                CreateFieldSaveRequest(field2, "2345"));
            Expect
                .SaveWasCalledForFields(
                    SavedField(field1, "1234", valueFloat: 1234),
                    SavedField(field2, "2345", valueFloat: 2345))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void Acceptable_And_Exceptional()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric);
            var field2 = DefineDataEntryField(DataEntryDataType.Numeric, isRequired: true);
            Execute(
                CreateFieldSaveRequest(field1, "1234"),
                CreateFieldSaveRequest(field2, ""));
            Expect
                .SaveWasCalledForFields(
                    SavedField(field1, "1234", valueFloat: 1234),
                    SavedField(field2, ""))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .BusinessRuleExceptions(Tuple.Create(field2, Messages.BusinessRuleException_FieldIsRequired));
        }
        [TestMethod]
        public void Acceptable_And_Mistyped()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric);
            var field2 = DefineDataEntryField(DataEntryDataType.Numeric);
            Execute(
                CreateFieldSaveRequest(field1, "1234"),
                CreateFieldSaveRequest(field2, "234x"));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(Tuple.Create(field2, Messages.StubError_BadNumericValue))
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void Exceptional_And_Exceptional()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric, isRequired: true);
            var field2 = DefineDataEntryField(DataEntryDataType.Numeric, isRequired: true);
            Execute(
                CreateFieldSaveRequest(field1, ""),
                CreateFieldSaveRequest(field2, ""));
            Expect
                .SaveWasCalledForFields(
                    SavedField(field1, ""),
                    SavedField(field2, ""))
                .StatusWasSuccess()
                .NoDataTypeErrors()
                .BusinessRuleExceptions(
                    Tuple.Create(field1, Messages.BusinessRuleException_FieldIsRequired),
                    Tuple.Create(field2, Messages.BusinessRuleException_FieldIsRequired));
        }
        [TestMethod]
        public void Exceptional_And_Mistyped()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric, isRequired: true);
            var field2 = DefineDataEntryField(DataEntryDataType.Numeric);
            Execute(
                CreateFieldSaveRequest(field1, ""),
                CreateFieldSaveRequest(field2, "234x"));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(Tuple.Create(field2, Messages.StubError_BadNumericValue))
                .BusinessRuleExceptions(Tuple.Create(field1, Messages.BusinessRuleException_FieldIsRequired));
        }
        [TestMethod]
        public void Mistyped_And_Mistyped()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric);
            var field2 = DefineDataEntryField(DataEntryDataType.Numeric);
            Execute(
                CreateFieldSaveRequest(field1, "123x"),
                CreateFieldSaveRequest(field2, "234x"));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(
                    Tuple.Create(field1, Messages.StubError_BadNumericValue),
                    Tuple.Create(field2, Messages.StubError_BadNumericValue))
                .NoBusinessRuleExceptions();
        }
        [TestMethod]
        public void Acceptable_And_Exceptional_And_Mistyped()
        {
            var field1 = DefineDataEntryField(DataEntryDataType.Numeric);
            var field2 = DefineDataEntryField(DataEntryDataType.Numeric, isRequired: true);
            var field3 = DefineDataEntryField(DataEntryDataType.Numeric);
            Execute(
                CreateFieldSaveRequest(field1, "1234"),
                CreateFieldSaveRequest(field2, ""),
                CreateFieldSaveRequest(field3, "345x"));
            Expect
                .NoSave()
                .StatusWasFailureDueToTypeError()
                .DataTypeErrors(Tuple.Create(field3, Messages.StubError_BadNumericValue))
                .BusinessRuleExceptions(Tuple.Create(field2, Messages.BusinessRuleException_FieldIsRequired));
        }
    }
}
