﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;

namespace R360Tests
{
    [TestClass]
    public class TransactionImageIconsVisible
    {
        public TestContext TestContext { get; set; }
        private static TestContext _testContext;
        private static TransactionDetailRepository _transactionDetailRepository;
        private static ImageAPI _imageApi;

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _transactionDetailRepository = new TransactionDetailRepository(new MockR360ServiceContext());
            _imageApi = new ImageAPI(new MockR360ServiceContext());

        }

        [TestMethod]
        public void Check_ElectronicTransaction()
        {
            var iconVisible = _transactionDetailRepository.ImageIconVisible(new ImageRequestDto
            {
                BankId = 1,
                SourceBatchId = 1,
                BatchPaymentSource = "BatchSource",
                PicsDate = 20000101,
                IsCheck = true,
                BatchSequence = 0,
                WorkgroupId = 1,
                ImportTypeShortName = "ImportType"
            }, true, _imageApi);

            Assert.AreEqual(true, iconVisible);
        }

        [TestMethod]
        public void Document_ElectronicTransaction()
        {
            var iconVisible = _transactionDetailRepository.ImageIconVisible(new ImageRequestDto
            {
                BankId = 1,
                SourceBatchId = 1,
                BatchPaymentSource = "BatchSource",
                PicsDate = 20000101,
                IsCheck = false,
                BatchSequence = 0,
                WorkgroupId = 1,
                ImportTypeShortName = "ImportType"
            }, true, _imageApi);

            Assert.AreEqual(true, iconVisible);
        }

        [TestMethod]
        public void Stub_ElectronicTransaction_NotLinkedToDocument()
        {
            var iconVisible = _transactionDetailRepository.ImageIconVisible(new ImageRequestDto
            {
                BankId = 1,
                SourceBatchId = 1,
                BatchPaymentSource = "BatchSource",
                PicsDate = 20000101,
                IsCheck = false,
                BatchSequence = 0,
                WorkgroupId = 1,
                ImportTypeShortName = "ImportType"
            }, true, false, _imageApi);

            Assert.AreEqual(false,iconVisible);
        }

        [TestMethod]
        public void Stub_ElectronicTransaction_LinkedToDocument()
        {
            var iconVisible = _transactionDetailRepository.ImageIconVisible(new ImageRequestDto
            {
                BankId = 1,
                SourceBatchId = 1,
                BatchPaymentSource = "BatchSource",
                PicsDate = 20000101,
                IsCheck = false,
                BatchSequence = 0,
                WorkgroupId = 1,
                ImportTypeShortName = "ImportType"
            }, true, true, _imageApi);

            Assert.AreEqual(true, iconVisible);
        }

        [TestMethod]
        public void Stub_NonElectronicTransaction_NotLinkedToDocument()
        {
            var iconVisible = _transactionDetailRepository.ImageIconVisible(new ImageRequestDto
            {
                BankId = 1,
                SourceBatchId = 1,
                BatchPaymentSource = "BatchSource",
                PicsDate = 20000101,
                IsCheck = false,
                BatchSequence = 0,
                WorkgroupId = 1,
                ImportTypeShortName = "ImportType"
            }, false, false, _imageApi);

            Assert.AreEqual(false, iconVisible);
        }
    }
}
