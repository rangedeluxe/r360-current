﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using System.Xml;
using System.IO;

namespace R360Tests
{
    [TestClass]
    public class PaymentSearchXmlTests
    {
        private readonly IXmlTransformer<PaymentSearchXmlRequestDto> _requesttransformer;
        private readonly IXmlTransformer<PaymentSearchResponseDto> _responsetransformer;
        public PaymentSearchXmlTests()
        {
            _requesttransformer = new PaymentSearchRequestXmlTransformer();
            _responsetransformer = new PaymentSearchResponseXmlTransformer();
        }

        [TestMethod]
        public void PaymentSearch_Should_TransformToXml_BasicRequest()
        {
            // Arrange
            var today = DateTime.Now;
            var req = new PaymentSearchXmlRequestDto()
            {
                DateFrom = today,
                DateTo = today,
                Workgroups = new List<WorkgroupDto>()
                {
                    new WorkgroupDto()
                    {
                        BankId = 1, ClientAccountId = 10
                    }
                },
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection"
            };

            // Act
            var xml = _requesttransformer.ToXML(req);
            var xdoc = XDocument.Parse(xml.OuterXml);
            var ca = xdoc.Descendants("ClientAccounts").First().Descendants("ClientAccount").First();

            // Assert
            Assert.AreEqual("1", ca.Attribute("BankID").Value);
            Assert.AreEqual("10", ca.Attribute("ClientAccountID").Value);
            Assert.AreEqual(today.ToString("MM/dd/yyyy"), xdoc.Descendants("DepositDateFrom").First().Value);
            Assert.AreEqual(today.ToString("MM/dd/yyyy"), xdoc.Descendants("DepositDateTo").First().Value);
        }

        [TestMethod]
        public void PaymentSearch_Should_TransformFromXml_BasicResponse()
        {
            // Arrange
            var xml = new XmlDocument();
            xml.Load(Path.Combine(Environment.CurrentDirectory, "Resources", "SamplePaymentSearchResponse.xml"));

            // Act
            var dto = _responsetransformer.ToDTO(xml);

            // Assert
            Assert.AreEqual(222, dto.TotalRecords);
        }

        [TestMethod]
        public void PaymentSearch_Should_TransformToXml_RequestWithCriteria()
        {
            // Arrange
            var today = DateTime.Now;
            var req = new PaymentSearchXmlRequestDto()
            {
                DateFrom = today,
                DateTo = today,
                Workgroups = new List<WorkgroupDto>()
                {
                    new WorkgroupDto()
                    {
                        BankId = 1, ClientAccountId = 10
                    }
                },
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection",
                Criteria = new List<PaymentSearchCriteriaDto>()
                {
                    new PaymentSearchCriteriaDto()
                    {
                        XmlColumnName = "RemitterName",
                        Value = "James Bond"
                    }
                }
            };

            // Act
            var xml = _requesttransformer.ToXML(req);
            var xdoc = XDocument.Parse(xml.OuterXml);
            var node = xdoc.Descendants("RemitterName").First();

            // Assert
            Assert.IsNotNull(node);
            Assert.AreEqual("James Bond", node.Value);
        }

    }
}
