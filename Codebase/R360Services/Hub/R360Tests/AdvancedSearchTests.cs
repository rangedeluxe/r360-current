﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
//using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;
using WFS.RecHub.R360Shared;

namespace R360Tests
{
    [TestClass]
    public class AdvancedSearchTests
    {
        private static bool ValidateDataEntryFields(List<DataEntryDto> actualResults, List<DataEntryDto> expectedResults,
            string testMethod)
        {
            Assert.AreEqual(expectedResults.Count, actualResults.Count, $"{testMethod} data entry field count");
            for (var i = 0; i < actualResults.Count; i++)
            {
                Assert.AreEqual(expectedResults[i].FieldName, actualResults[i].FieldName,
                    $"{testMethod} FieldName does not match");
                Assert.AreEqual(expectedResults[i].Title, actualResults[i].Title, $"{testMethod} Title does not match");
                Assert.AreEqual(expectedResults[i].Value, actualResults[i].Value, $"{testMethod} Value does not match");
                Assert.AreEqual(expectedResults[i].Type, actualResults[i].Type, $"{testMethod} Type does not match");
            }
            return true;
        }

        private static XElement CreateSelectFieldElement(string tableName, string fieldName, int dataType,
            string reportTitle, string colId)
        {
            var selectField = new XElement("field",
                new XAttribute("tablename", tableName),
                new XAttribute("fieldname", fieldName),
                new XAttribute("datatype", dataType),
                new XAttribute("reporttitle", reportTitle),
                new XAttribute("ColID", colId));
            return selectField;
        }

        private static XmlDocument CreateResponseXmlDocument(string totalRecords = null, string documentCount = null,
            string checkCount = null, string checkTotal = null, XElement fieldXElement = null)
        {
            var xResponseDoc = new XDocument(
                new XElement("Page",
                    new XElement("RecordSet",
                        new XAttribute("Name", "Results"),
                        new XAttribute("TotalRecords", totalRecords ?? "0"),
                        new XAttribute("DocumentCount", documentCount ?? "0"),
                        new XAttribute("CheckCount", checkCount ?? "0"),
                        new XAttribute("CheckTotal", checkTotal ?? "0.00"),
                        fieldXElement)));


            //Convert XDocument to XmlDocument
            var xmlResponseDoc = new XmlDocument();
            using (var reader = xResponseDoc.CreateReader())
            {
                xmlResponseDoc.Load(reader);
            }

            return xmlResponseDoc;
        }

        private static AdvancedSearchRepository CreateAdvancedSearchRepository(string totalRecords = "2",
            string documentCount = "2", string checkCount = "2", string checkTotal = "20.00")
        {
            return new AdvancedSearchRepository(
                new MockServiceContext(),
                new MockAdvancedSearchDataProvider(CreateResponseXmlDocument(totalRecords: totalRecords,
                    checkCount: checkCount, documentCount: documentCount, checkTotal: checkTotal)),
                new MockImageService(),
                new MockPermissionProvider());
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_SuccessfulResults()
        {
            // Arrange
            var repository = CreateAdvancedSearchRepository();

            var dto = new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2
            };

            // Act
            var results = repository.Get(dto);

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            Assert.IsNotNull(results.Results);
            Assert.IsTrue(results.Results.Any());
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_ValidMetadata()
        {
            // Arrange
            var repository = CreateAdvancedSearchRepository();

            var dto = new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2
            };

            // Act
            var results = repository.Get(dto);

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            Assert.IsNotNull(results.Metadata);
            Assert.IsNotNull(results.Metadata.WorkgroupName);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldFail_DueToUnauthorizedWorkgroup()
        {
            // Arrange
            var repository = CreateAdvancedSearchRepository();

            var dto = new AdvancedSearchDto()
            {
                BankId = 3,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 3
            };

            // Act
            var results = repository.Get(dto);

            // Assert
            Assert.AreEqual(StatusCode.FAIL, results.Status);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldFail_DueToSearchDays()
        {
            // Arrange
            var repository = CreateAdvancedSearchRepository();

            var dto = new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now.AddDays(2),
                WorkgroupId = 2
            };

            // Act
            var results = repository.Get(dto);

            // Assert
            Assert.AreEqual(StatusCode.FAIL, results.Status);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldContain_MarkSenseData()
        {
            // Arrange
            var repository = CreateAdvancedSearchRepository();

            var dto = new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2
            };

            // Act
            var results = repository.Get(dto);

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            Assert.AreEqual(true, results.Results[0].IsMarkSense);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldFail_OnTooManyResults()
        {
            // Arrange
            var repository = CreateAdvancedSearchRepository(totalRecords: "-99");

            var dto = new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2
            };

            // Act
            var results = repository.Get(dto);
            //Assert
            Assert.AreEqual(StatusCode.FAIL, results.Status);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_Metadata()
        {
            // Arrange
            var repository = CreateAdvancedSearchRepository();

            var dto = new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2
            };

            // Act
            var results = repository.Get(dto);

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            Assert.IsNotNull(results.Metadata);
            Assert.IsTrue(results.Metadata.HasDownloadPermission);
        }

        [TestMethod]
        public void AdvancedSearch_ShouldReturn_MetaDataResultString()
        {
            // Arrange
            var repository = CreateAdvancedSearchRepository();

            var dto = new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2,
                WhereClauses = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        ReportTitle = "Apple",
                        Operator = AdvancedSearchWhereClauseOperator.IsGreaterThan,
                        Value = "Seeds",
                        IsStandard = true,
                        StandardXmlColumnName = "Apple"
                    }
                }
            };

            // Act
            var results = repository.Get(dto);

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            Assert.IsNotNull(results.Metadata);
            Assert.IsTrue(results.Metadata.CriteraString.Contains("Apple Is Greater Than or Equal to Seeds"));
        }

        [TestMethod]
        public void AdvancedSearch_OneCheckDataEntryField()
        {
            // Arrange response document
            var responseXmlDocument = CreateResponseXmlDocument(fieldXElement: new XElement("SelectFields",
                CreateSelectFieldElement("Checks", "CheckDate", 11, "[PMT].Check Date", "01")));

            // Arrange result set
            var searchResultsData = new MockAdvancedSearchResults();

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"ChecksCheckDate_01", "11/29/2015"}
            });

            // Arrange repository
            var repository = new AdvancedSearchRepository(
                new MockServiceContext(),
                new MockAdvancedSearchDataProvider(responseXmlDocument, searchResultsData),
                new MockImageService(),
                new MockPermissionProvider());

            // Act
            var results = repository.Get(new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2,
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "CheckDate",
                        DataType = AdvancedSearchWhereClauseDataType.DateTime,
                        ReportTitle = "[PMT].Check Date",
                        IsDataEntry = true
                    }
                }
            });

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            ValidateDataEntryFields(results.Results[0].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "CheckDate",
                        Title = "[PMT].Check Date",
                        Value = "11/29/2015",
                        Type = "11"
                    }
                },
                "AdvancedSearch_ShouldReturn_OneCheckDataEntryField");
        }

        [TestMethod]
        public void AdvancedSearch_OneStubDataEntryField()
        {
            // Arrange response document
            var responseXmlDocument =
                CreateResponseXmlDocument(fieldXElement: new XElement("SelectFields",
                    CreateSelectFieldElement("Stubs", "PolicyNumber", 1, "[INV].Policy Number", "01")));

            // Arrange result set
            var searchResultsData = new MockAdvancedSearchResults();

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsPolicyNumber_01", "123456789"}
            });

            // Arrange repository
            var repository = new AdvancedSearchRepository(
                new MockServiceContext(),
                new MockAdvancedSearchDataProvider(responseXmlDocument, searchResultsData),
                new MockImageService(),
                new MockPermissionProvider());

            // Act
            var results = repository.Get(new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2,
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Stubs,
                        FieldName = "PolicyNumber",
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[INV].Policy Number",
                        IsDataEntry = true
                    }
                }
            });

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            ValidateDataEntryFields(results.Results[0].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "PolicyNumber",
                        Title = "[INV].Policy Number",
                        Value = "123456789",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_OneStubDataEntryField");
        }

        [TestMethod]
        public void AdvancedSearch_OneDEFromOneUILabel()
        {
            // Arrange response document
            var responseXmlDocument = CreateResponseXmlDocument(fieldXElement: new XElement("SelectFields",
                CreateSelectFieldElement("Stubs", "AccountNum", 1, "[INV].Account Number", "01"),
                CreateSelectFieldElement("Stubs", "AcctNumber", 1, "[INV].Account Number", "02")));

            // Arrange result set
            var searchResultsData = new MockAdvancedSearchResults();

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"Deposit_Date", new DateTime(2017,07,24) },
                {"SourceBatchID", 564 },
                {"BatchNumber", 564 },
                {"TxnSequence", 1 },
                {"StubsAccountNum_01", "11111"},
                {"StubsAcctNumber_02", null}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"Deposit_Date", new DateTime(2017,07,24) },
                {"SourceBatchID", 564 },
                {"BatchNumber", 564 },
                {"TxnSequence", 2 },
                {"StubsAccountNum_01", "22222"},
                {"StubsAcctNumber_02", "33333"}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"Deposit_Date", new DateTime(2017,07,24) },
                {"SourceBatchID", 321 },
                {"BatchNumber", 321 },
                {"TxnSequence", 1 },
                {"StubsAccountNum_01", null},
                {"StubsAcctNumber_02", "44444"}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"Deposit_Date", new DateTime(2017,07,24) },
                {"SourceBatchID", 417 },
                {"BatchNumber", 417 },
                {"TxnSequence", 1 },
                {"StubsAccountNum_01", "55555"},
                {"StubsAcctNumber_02", null}
            });


            // Arrange repository
            var repository = new AdvancedSearchRepository(
                new MockServiceContext(),
                new MockAdvancedSearchDataProvider(responseXmlDocument, searchResultsData),
                new MockImageService(),
                new MockPermissionProvider());

            // Act
            var results = repository.Get(new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2,
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "Batch.Deposit_Date",
                        DataType = AdvancedSearchWhereClauseDataType.DateTime,
                        ReportTitle = "Deposit Date",
                        IsDataEntry = false,
                        IsStandard =  true
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "Batch.SourceBatchID",
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        ReportTitle = "Batch ID",
                        IsDataEntry = false,
                        IsStandard =  true
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "Batch.BatchNumber",
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        ReportTitle = "BatchNumber",
                        IsDataEntry = false,
                        IsStandard =  true
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "Transactions.TxnSequence",
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        ReportTitle = "Transaction",
                        IsDataEntry = false,
                        IsStandard =  true
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Stubs,
                        FieldName = "AccountNum",
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[INV].Account Number",
                        IsDataEntry = true
                    }
                }
            });

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            ValidateDataEntryFields(results.Results[0].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "Batch.Deposit_Date",
                        Title = "Deposit Date",
                        Value = "7/24/2017 12:00:00 AM",
                        Type = "11"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Batch.SourceBatchID",
                        Title = "Batch ID",
                        Value = "564",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Batch.BatchNumber",
                        Title = "BatchNumber",
                        Value = "564",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Transactions.TxnSequence",
                        Title = "Transaction",
                        Value = "1",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "AccountNum",
                        Title = "[INV].Account Number",
                        Value = "11111",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 1");

            ValidateDataEntryFields(results.Results[1].DataEntry.ToList(),
                new List<DataEntryDto>
                {
                    new DataEntryDto
                    {
                        FieldName = "Batch.Deposit_Date",
                        Title = "Deposit Date",
                        Value = "7/24/2017 12:00:00 AM",
                        Type = "11"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Batch.SourceBatchID",
                        Title = "Batch ID",
                        Value = "564",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Batch.BatchNumber",
                        Title = "BatchNumber",
                        Value = "564",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Transactions.TxnSequence",
                        Title = "Transaction",
                        Value = "2",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "AccountNum",
                        Title = "[INV].Account Number",
                        Value = "22222",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 2");

            ValidateDataEntryFields(results.Results[2].DataEntry.ToList(),
                new List<DataEntryDto>
                {
                    new DataEntryDto
                    {
                        FieldName = "Batch.Deposit_Date",
                        Title = "Deposit Date",
                        Value = "7/24/2017 12:00:00 AM",
                        Type = "11"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Batch.SourceBatchID",
                        Title = "Batch ID",
                        Value = "321",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Batch.BatchNumber",
                        Title = "BatchNumber",
                        Value = "321",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Transactions.TxnSequence",
                        Title = "Transaction",
                        Value = "1",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "AccountNum",
                        Title = "[INV].Account Number",
                        Value = "44444",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 3");

            ValidateDataEntryFields(results.Results[3].DataEntry.ToList(),
                new List<DataEntryDto>
                {
                    new DataEntryDto
                    {
                        FieldName = "Batch.Deposit_Date",
                        Title = "Deposit Date",
                        Value = "7/24/2017 12:00:00 AM",
                        Type = "11"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Batch.SourceBatchID",
                        Title = "Batch ID",
                        Value = "417",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Batch.BatchNumber",
                        Title = "BatchNumber",
                        Value = "417",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "Transactions.TxnSequence",
                        Title = "Transaction",
                        Value = "1",
                        Type = "6"
                    },
                    new DataEntryDto
                    {
                        FieldName = "AccountNum",
                        Title = "[INV].Account Number",
                        Value = "55555",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 4");

        }

        [TestMethod]
        public void AdvancedSearch_OneDEFromThreeFieldsOneUILabel()
        {
            // Arrange response document
            var responseXmlDocument = CreateResponseXmlDocument(fieldXElement: new XElement("SelectFields",
                CreateSelectFieldElement("Stubs", "AccountNumber", 1, "[INV].Account Number", "01"),
                CreateSelectFieldElement("Stubs", "AccountNumber", 1, "[INV].Account Number", "02"),
                CreateSelectFieldElement("Stubs", "AcctNumber", 1, "[INV].Account Number", "03")));

            // Arrange result set
            var searchResultsData = new MockAdvancedSearchResults();

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", "11111"},
                {"StubsAccountNumber_02", "1"},
                {"StubsAcctNumber_03", null}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", "22222"},
                {"StubsAccountNumber_02", "1"},
                {"StubsAcctNumber_03", null}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", "333333"}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", "333333"}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null}
            });

            // Arrange repository
            var repository = new AdvancedSearchRepository(
                new MockServiceContext(),
                new MockAdvancedSearchDataProvider(responseXmlDocument, searchResultsData),
                new MockImageService(),
                new MockPermissionProvider());

            // Act
            var results = repository.Get(new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2,
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Stubs,
                        FieldName = "AcctNumber",
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[INV].Account Number",
                        IsDataEntry = true

                    }
                }
            });

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            ValidateDataEntryFields(results.Results[0].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "11111",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 1");

            ValidateDataEntryFields(results.Results[1].DataEntry.ToList(),
                new List<DataEntryDto>
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "22222",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 2");

            ValidateDataEntryFields(results.Results[2].DataEntry.ToList(),
                new List<DataEntryDto>
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "333333",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 3");

            ValidateDataEntryFields(results.Results[3].DataEntry.ToList(),
                new List<DataEntryDto>
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "333333",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 4");

            ValidateDataEntryFields(results.Results[4].DataEntry.ToList(),
                new List<DataEntryDto>
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 4");

            ValidateDataEntryFields(results.Results[5].DataEntry.ToList(),
                new List<DataEntryDto>
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabel Row 5");

        }

        [TestMethod]
        public void AdvancedSearch_OneDEFromFiveFieldsOneUILabel()
        {
            // Arrange response document
            var responseXmlDocument = CreateResponseXmlDocument(fieldXElement: new XElement("SelectFields",
                CreateSelectFieldElement("Stubs", "AccountNumber", 1, "[INV].Account Number", "01"),
                CreateSelectFieldElement("Stubs", "AccountNumber", 1, "[INV].Account Number", "02"),
                CreateSelectFieldElement("Stubs", "AcctNumber", 1, "[INV].Account Number", "03"),
                CreateSelectFieldElement("Stubs", "AcctNum", 1, "[INV].Account Number", "04"),
                CreateSelectFieldElement("Stubs", "AccountNum", 1, "[INV].Account Number", "05")));

            // Arrange result set
            var searchResultsData = new MockAdvancedSearchResults();
            // Row 1
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", "11111"},
                {"StubsAccountNumber_02", "1"},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", null}
            });
            // Row 2
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", "1"},
                {"StubsAccountNumber_02", "11111"},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", "44444"},
                {"StubsAccountNum_05", null}
            });
            // Row 3
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", "11111"},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", null}
            });
            // Row 4
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", null}
            });
            // Row 5
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", "4444444444"},
                {"StubsAccountNum_05", null}
            });
            // Row 6
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", "444444"},
                {"StubsAccountNum_05", null}
            });
            // Row 7
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", null}
            });
            // Row 8
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", null}
            });
            // Row 9
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", null}
            });
            // Row 10
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", "55555555"}
            });
            // Row 11
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", "5"}
            });
            // Row 12
            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAccountNumber_02", null},
                {"StubsAcctNumber_03", null},
                {"StubsAcctNum_04", null},
                {"StubsAccountNum_05", null}
            });

            // Arrange repository
            var repository = new AdvancedSearchRepository(
                new MockServiceContext(),
                new MockAdvancedSearchDataProvider(responseXmlDocument, searchResultsData),
                new MockImageService(),
                new MockPermissionProvider());

            // Act
            var results = repository.Get(new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2,
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Stubs,
                        FieldName = "AccountNum",
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[INV].Account Number",
                        IsDataEntry = true

                    }
                }
            });

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            // Row 1
            ValidateDataEntryFields(results.Results[0].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "11111",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 1");
            // Row 2
            ValidateDataEntryFields(results.Results[1].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "1",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 2");
            // Row 3
            ValidateDataEntryFields(results.Results[2].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "11111",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 3");
            // Row 4
            ValidateDataEntryFields(results.Results[3].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 4");
            // Row 5
            ValidateDataEntryFields(results.Results[4].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "4444444444",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 5");
            // Row 6
            ValidateDataEntryFields(results.Results[5].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "444444",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 6");
            // Row 7
            ValidateDataEntryFields(results.Results[6].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 7");
            // Row 8
            ValidateDataEntryFields(results.Results[7].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 8");
            // Row 9
            ValidateDataEntryFields(results.Results[8].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 9");
            // Row 10
            ValidateDataEntryFields(results.Results[9].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "55555555",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 10");
            // Row 11
            ValidateDataEntryFields(results.Results[10].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "5",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 11");
            // Row 12
            ValidateDataEntryFields(results.Results[11].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_OneDEFromFiveFieldsOneUILabel Row 11");
        }


        [TestMethod]
        public void AdvancedSearch_TwoDEFromOneUILabelPlusOne()
        {
            // Arrange response document
            var responseXmlDocument = CreateResponseXmlDocument(fieldXElement: new XElement("SelectFields",
                CreateSelectFieldElement("Stubs", "AccountNumber", 1, "[INV].Account Number", "01"),
                CreateSelectFieldElement("Stubs", "AcctNumber", 1, "[INV].Account Number", "03"),
                CreateSelectFieldElement("Stubs", "PolicyNumber", 1, "[INV].Policy Number", "04")));

            // Arrange result set
            var searchResultsData = new MockAdvancedSearchResults();

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", "11111"},
                {"StubsAcctNumber_03", null},
                {"StubsPolicyNumber_04", "123456789"}
            });

            searchResultsData.AddResultRow(new Dictionary<string, dynamic>()
            {
                {"StubsAccountNumber_01", null},
                {"StubsAcctNumber_03", "22222"},
                {"StubsPolicyNumber_04", "987654321"}
            });

            // Arrange repository
            var repository = new AdvancedSearchRepository(
                new MockServiceContext(),
                new MockAdvancedSearchDataProvider(responseXmlDocument, searchResultsData),
                new MockImageService(),
                new MockPermissionProvider());


            // Act
            var results = repository.Get(new AdvancedSearchDto()
            {
                BankId = 1,
                DepositDateFrom = DateTime.Now,
                DepositDateTo = DateTime.Now,
                WorkgroupId = 2,
                SelectFields = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Stubs,
                        FieldName = "AccountNumber",
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[INV].Account Number",
                        IsDataEntry = true

                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        Table = AdvancedSearchWhereClauseTable.Stubs,
                        FieldName = "PolicyNumber",
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[INV].Policy Number",
                        IsDataEntry = true

                    }
                }
            });

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, results.Status);
            ValidateDataEntryFields(results.Results[0].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "11111",
                        Type = "1"
                    },
                    new DataEntryDto
                    {
                        FieldName = "PolicyNumber",
                        Title = "[INV].Policy Number",
                        Value = "123456789",
                        Type = "1"
                    }

                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabelPlusOne Row 1");

            ValidateDataEntryFields(results.Results[1].DataEntry.ToList(),
                new List<DataEntryDto>()
                {
                    new DataEntryDto
                    {
                        FieldName = "AccountNumber",
                        Title = "[INV].Account Number",
                        Value = "22222",
                        Type = "1"
                    },
                    new DataEntryDto
                    {
                        FieldName = "PolicyNumber",
                        Title = "[INV].Policy Number",
                        Value = "987654321",
                        Type = "1"
                    }
                }.ToList(),
                "AdvancedSearch_ShouldReturn_TwoDEFromOneUILabelPlusOne Row 2");

        }
    }
}

