﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.R360ServicesAPI;

namespace R360Tests
{
    [TestClass]
    public class DataTypeValidationErrorMessageHelperTests
    {
        [TestMethod]
        public void DataTypeValidationEnumMapTest()
        {
            //This test is to make sure that all enum values have an error message assigned in the DataTypeValidationErrorMessageHelper.
            foreach (DataEntryDataTypeValidationError enumError in Enum.GetValues(typeof(DataEntryDataTypeValidationError)))
            {
                var errorMessage = DataTypeValidationErrorMessageHelper.GetDataTypeValidationErrorMessage(enumError);
                Assert.IsFalse(string.IsNullOrWhiteSpace(errorMessage));
            }            
        }
    }
}
