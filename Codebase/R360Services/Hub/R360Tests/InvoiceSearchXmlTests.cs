﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using System.Xml;
using System.IO;

namespace R360Tests
{
    [TestClass]
    public class InvoiceSearchXmlTests
    {
        private readonly IXmlTransformer<InvoiceSearchXmlRequestDto> _requesttransformer;
        private readonly IXmlTransformer<InvoiceSearchResponseDto> _responsetransformer;
        public InvoiceSearchXmlTests()
        {
            _requesttransformer = new InvoiceSearchRequestXmlTransformer();
            _responsetransformer = new InvoiceSearchResponseXmlTransformer();
        }

        [TestMethod]
        public void InvoiceSearch_Should_TransformToXml_BasicRequest()
        {
            // Arrange
            var today = DateTime.Now;
            var req = new InvoiceSearchXmlRequestDto()
            {
                DateFrom = today,
                DateTo = today,
                Workgroups = new List<WorkgroupDto>()
                {
                    new WorkgroupDto()
                    {
                        BankId = 1, ClientAccountId = 10
                    }
                },
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection"
            };

            // Act
            var xml = _requesttransformer.ToXML(req);
            var xdoc = XDocument.Parse(xml.OuterXml);
            var ca = xdoc.Descendants("ClientAccounts").First().Descendants("ClientAccount").First();

            // Assert
            Assert.AreEqual("1", ca.Attribute("BankID").Value);
            Assert.AreEqual("10", ca.Attribute("ClientAccountID").Value);
            Assert.AreEqual(today.ToString("MM/dd/yyyy"), xdoc.Descendants("DepositDateFrom").First().Value);
            Assert.AreEqual(today.ToString("MM/dd/yyyy"), xdoc.Descendants("DepositDateTo").First().Value);
        }

        [TestMethod]
        public void InvoiceSearch_Should_TransformFromXml_BasicResponse()
        {
            // Arrange
            var xml = new XmlDocument();
            xml.Load(Path.Combine(Environment.CurrentDirectory, "Resources", "SampleInvoiceSearchResponse.xml"));

            // Act
            var dto = _responsetransformer.ToDTO(xml);

            // Assert
            Assert.AreEqual(222, dto.TotalRecords);
        }

        [TestMethod]
        public void InvoiceSearch_Should_TransformToXml_RequestWithCriteria()
        {
            // Arrange
            var today = DateTime.Now;
            var req = new InvoiceSearchXmlRequestDto()
            {
                DateFrom = today,
                DateTo = today,
                Workgroups = new List<WorkgroupDto>()
                {
                    new WorkgroupDto()
                    {
                        BankId = 1, ClientAccountId = 10
                    }
                },
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection",
                Criteria = new List<InvoiceSearchCriteriaDto>()
                {
                    new InvoiceSearchCriteriaDto()
                    {
                        XmlColumnName = "RemitterName",
                        Value = "James Bond"
                    }
                }
            };

            // Act
            var xml = _requesttransformer.ToXML(req);
            var xdoc = XDocument.Parse(xml.OuterXml);
            var node = xdoc.Descendants("RemitterName").First();

            // Assert
            Assert.IsNotNull(node);
            Assert.AreEqual("James Bond", node.Value);
        }

    }
}
