﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;

namespace R360Tests
{
    [TestClass]
    public class AdvancedSearchXmlTests
    {
        private readonly IXmlTransformer<AdvancedSearchRequestDto> _requesttransformer;
        private readonly IXmlTransformer<AdvancedSearchResponseDto> _responsetransformer;
        private readonly cSearchDAL _dal;
        public AdvancedSearchXmlTests()
        {
            _requesttransformer = new AdvancedSearchRequestXmlTransformer();
            _responsetransformer = new AdvancedSearchResponseXmlTransformer();
            _dal = new cSearchDAL("IPOnline");
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformFromXml_BasicRequest()
        {
            // Arrange
            var xml = new XmlDocument();
            xml.Load(Path.Combine(Environment.CurrentDirectory, "Resources", "SampleAdvancedSearchRequest.xml"));

            // Act
            var dto = _requesttransformer.ToDTO(xml);

            // Assert
            Assert.AreEqual(999, dto.BankId);
            Assert.AreEqual(303030, dto.WorkgroupId);
            Assert.AreEqual(new DateTime(2015, 8, 18), dto.DepositDateFrom);
            Assert.AreEqual(new DateTime(2015, 8, 18), dto.DepositDateTo);
            Assert.AreEqual(Guid.Parse("c1407ec6-317e-4456-9341-74550ec6d9fa"), dto.SessionId);
            Assert.AreEqual(1, dto.StartRecord);
            Assert.AreEqual(20, dto.RecordsPerPage);
            Assert.AreEqual(10, dto.PaymentSourceKey);
            Assert.AreEqual(20, dto.PaymentTypeKey);
            Assert.AreEqual(false, dto.COTSOnly);
            Assert.AreEqual(false, dto.MarkSenseOnly);
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformFromXml_FilledRequest()
        {
            // Arrange
            var xml = new XmlDocument();
            xml.Load(Path.Combine(Environment.CurrentDirectory, "Resources", "SampleAdvancedSearchFilledRequest.xml"));

            // Act
            var dto = _requesttransformer.ToDTO(xml);

            // Assert
            Assert.AreEqual(422, dto.BankId);
            Assert.AreEqual(510, dto.WorkgroupId);
            Assert.AreEqual(new DateTime(2016, 7, 13), dto.DepositDateFrom);
            Assert.AreEqual(new DateTime(2016, 7, 13), dto.DepositDateTo);
            Assert.AreEqual(Guid.Parse("e4e2bf6d-0eac-4431-b2ff-453f5a67986e"), dto.SessionId);
            Assert.AreEqual(1, dto.StartRecord);
            Assert.AreEqual(10, dto.RecordsPerPage);
            Assert.AreEqual(0, dto.PaymentSourceKey);
            Assert.AreEqual(0, dto.PaymentTypeKey);

            Assert.AreEqual(5, dto.WhereClauses.Count);
            Assert.AreEqual(AdvancedSearchWhereClauseTable.Checks, dto.WhereClauses[0].Table);
            Assert.AreEqual(2, dto.SelectFields.Count);
            Assert.AreEqual(AdvancedSearchWhereClauseTable.Checks, dto.SelectFields[0].Table);
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformFromXml_StandardFields()
        {
            // Arrange
            var xml = new XmlDocument();
            xml.Load(Path.Combine(Environment.CurrentDirectory, "Resources", "SampleAdvancedSearchFilledRequest.xml"));

            // Act
            var dto = _requesttransformer.ToDTO(xml);

            // Assert
            Assert.AreEqual(5, dto.WhereClauses.Count);
            Assert.AreEqual("10", dto.WhereClauses[1].Value);
            Assert.AreEqual("20", dto.WhereClauses[2].Value);
            Assert.AreEqual("10", dto.WhereClauses[3].Value);
            Assert.AreEqual("20", dto.WhereClauses[4].Value);
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformToXml_BasicRequest()
        {
            // Arrange
            var dto = new AdvancedSearchRequestDto()
            {
                BankId = 999,
                WorkgroupId = 303030,
                DepositDateFrom = new DateTime(2015, 08, 18),
                DepositDateTo = new DateTime(2015, 08, 19),
                SessionId = Guid.Parse("fa9a6f01-59f6-4fd2-a067-cce144886de7"),
                StartRecord = 0,
                RecordsPerPage = 50,
                COTSOnly = false,
                MarkSenseOnly = false,
                PaymentSourceKey = 10,
                PaymentTypeKey = 20
            };

            // Act
            var xml = _requesttransformer.ToXML(dto);

            // Assert
            Assert.AreEqual("999", xml.SelectSingleNode("/Root/BankID").InnerText);
            Assert.AreEqual("303030", xml.SelectSingleNode("/Root/ClientAccountID").InnerText);
            Assert.AreEqual("08/18/2015", xml.SelectSingleNode("/Root/DateFrom").InnerText);
            Assert.AreEqual("08/19/2015", xml.SelectSingleNode("/Root/DateTo").InnerText);
            Assert.AreEqual("fa9a6f01-59f6-4fd2-a067-cce144886de7", xml.SelectSingleNode("/Root/SessionID").InnerText);
            Assert.AreEqual("0", xml.SelectSingleNode("/Root/StartRecord").InnerText);
            Assert.AreEqual("50", xml.SelectSingleNode("/Root/RecordsPerPage").InnerText);
            Assert.AreEqual("False", xml.SelectSingleNode("/Root/COTSOnly").InnerText);
            Assert.AreEqual("False", xml.SelectSingleNode("/Root/MarkSenseOnly").InnerText);
            Assert.AreEqual("10", xml.SelectSingleNode("/Root/BatchSourceKey").InnerText);
            Assert.AreEqual("20", xml.SelectSingleNode("/Root/BatchPaymentTypeKey").InnerText);
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformFromXml_BasicResponse()
        {
            // Arrange
            var xml = new XmlDocument();
            xml.Load(Path.Combine(Environment.CurrentDirectory, "Resources", "SampleAdvancedSearchResponse.xml"));

            // Act
            var dto = _responsetransformer.ToDTO(xml);

            // Assert
            Assert.AreEqual(42, dto.TotalRecords);
            Assert.AreEqual(35, dto.CheckCount);
            Assert.AreEqual(35, dto.DocumentCount);
            Assert.AreEqual(2232.82d, dto.CheckTotal);
            Assert.IsNotNull(dto.SelectFields);
            Assert.AreEqual(AdvancedSearchWhereClauseTable.Checks, dto.SelectFields.First().Table);
            Assert.AreEqual("BAICustomerReference", dto.SelectFields.First().FieldName);
            Assert.AreEqual(AdvancedSearchWhereClauseDataType.String, dto.SelectFields.First().DataType);
            Assert.AreEqual("[PMT].BAI Customer Reference", dto.SelectFields.First().ReportTitle);
            Assert.AreEqual("04", dto.SelectFields.First().ColumnId);
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformToXml_WhereClauses_DataEntryFields()
        {
            // Arrange
            var dto = new AdvancedSearchRequestDto()
            {
                WhereClauses = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = 1,
                        DataType = AdvancedSearchWhereClauseDataType.DateTime,
                        FieldName = "TestFieldName",
                        IsDataEntry = true,
                        IsStandard = false,
                        Operator = AdvancedSearchWhereClauseOperator.BeginsWith,
                        ReportTitle = "TestReportTitle",
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        Value = "12"
                    }
                }
            };

            // Act
            var xml = _requesttransformer.ToXML(dto);
            var xmlfield = xml.SelectSingleNode("/Root/WhereClause/field[@fieldname='TestFieldName']");

            // Assert
            Assert.AreEqual("Checks", xmlfield.Attributes["tablename"].Value);
            Assert.AreEqual("TestFieldName", xmlfield.Attributes["fieldname"].Value);
            Assert.AreEqual("11", xmlfield.Attributes["datatype"].Value);
            Assert.AreEqual("TestReportTitle", xmlfield.Attributes["reporttitle"].Value);
            Assert.AreEqual("Begins With", xmlfield.Attributes["operator"].Value);
            Assert.AreEqual("1", xmlfield.Attributes["batchsourcekey"].Value);
            Assert.AreEqual("12", xmlfield.Attributes["value"].Value);
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformToXml_WhereClauses_StandardFields()
        {
            // Arrange
            var dto = new AdvancedSearchRequestDto()
            {
                WhereClauses = new List<AdvancedSearchWhereClauseDto>()
                {
                    new AdvancedSearchWhereClauseDto()
                    {
                        StandardXmlColumnName = "BatchIDFrom",
                        IsStandard = true,
                        Value = "10"
                    },
                    new AdvancedSearchWhereClauseDto()
                    {
                        StandardXmlColumnName = "BatchIDTo",
                        IsStandard = true,
                        Value = "20"
                    },
                }
            };

            // Act
            var xml = _requesttransformer.ToXML(dto);

            // Assert
            Assert.AreEqual("10", xml.SelectSingleNode("/Root/BatchIDFrom").InnerText);
            Assert.AreEqual("20", xml.SelectSingleNode("/Root/BatchIDTo").InnerText);
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformToXml_PaginationFields()
        {
            // Arrange
            var dto = new AdvancedSearchRequestDto()
            {
                StartRecord = 0,
                RecordsPerPage = 10
            };

            // Act
            var xml = _requesttransformer.ToXML(dto);

            // Assert
            Assert.AreEqual("0", xml.SelectSingleNode("/Root/StartRecord").InnerText);
            Assert.AreEqual("1", xml.SelectSingleNode("/Root/PaginateRS").InnerText);
            Assert.AreEqual("10", xml.SelectSingleNode("/Root/RecordsPerPage").InnerText);
        }

        [TestMethod]
        public void AdvancedSearch_Should_TransformToXml_SelectFields()
        {
            // Arrange
            var dto = new AdvancedSearchRequestDto();
            dto.SelectFields.AddRange(new List<AdvancedSearchWhereClauseDto>()
            {
                new AdvancedSearchWhereClauseDto() { BatchSourceKey = 255, DataType = AdvancedSearchWhereClauseDataType.String, FieldName = "CheckSequence", Table = AdvancedSearchWhereClauseTable.Checks, ReportTitle = "Payment Sequence" },
                new AdvancedSearchWhereClauseDto() { BatchSourceKey = 255, DataType = AdvancedSearchWhereClauseDataType.DateTime, FieldName = "DateOfSorts", Table = AdvancedSearchWhereClauseTable.Checks, ReportTitle = "DateOfSortsDisplayName" },
            });

            // Act
            var xml = _requesttransformer.ToXML(dto);
            var checkseq = xml.SelectSingleNode("/Root/SelectFields/field[@fieldname='CheckSequence']");
            var date = xml.SelectSingleNode("/Root/SelectFields/field[@fieldname='DateOfSorts']");

            // Assert
            Assert.AreEqual("Checks", checkseq.Attributes["tablename"].Value);
            Assert.AreEqual("CheckSequence", checkseq.Attributes["fieldname"].Value);
            Assert.AreEqual("1", checkseq.Attributes["datatype"].Value);
            Assert.AreEqual("Payment Sequence", checkseq.Attributes["reporttitle"].Value);
            Assert.AreEqual("255", checkseq.Attributes["batchsourcekey"].Value);

            Assert.AreEqual("Checks", date.Attributes["tablename"].Value);
            Assert.AreEqual("DateOfSorts", date.Attributes["fieldname"].Value);
            Assert.AreEqual("11", date.Attributes["datatype"].Value);
            Assert.AreEqual("DateOfSortsDisplayName", date.Attributes["reporttitle"].Value);
            Assert.AreEqual("255", date.Attributes["batchsourcekey"].Value);
        }






        /// <summary>
        /// Not a real test case, but can be used by a developer to hit the database.
        /// </summary>
        //[TestMethod]
        public void AdvancedSearch_Should_SucceedDatabaseCall_AfterTransformation()
        {
            // Transform DTO to XML.
            var dto = new AdvancedSearchRequestDto()
            {
                BankId = 999,
                WorkgroupId = 202020,
                DepositDateFrom = new DateTime(2015, 12, 03),
                DepositDateTo = new DateTime(2015, 12, 03),
                SessionId = Guid.Parse("fa9a6f01-59f6-4fd2-a067-cce144886de7"),
                StartRecord = 1,
                RecordsPerPage = 50,
                COTSOnly = false,
                MarkSenseOnly = false
            };
            var tables = new AdvancedSearchRequestDataTableTransformer().ToDataTable(dto);

            // Call DB with XML.
            XmlDocument outxmldoc;
            DataTable outdt;
            var res = _dal.LockboxSearch(tables, out outxmldoc, out outdt);

            // Verify results exist, stuff didn't break.
            Assert.IsTrue(res);
            Assert.IsTrue(outdt.Rows.Count > 0);
        }

    }
}
