﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;

namespace R360Tests
{
    [TestClass]
    public class BatchImageIconsVisible
    {
        public static BatchDetailRepository _batchDetailRepository;
        public static MockImageService _imageService;

        [ClassInitialize]
        public static void ClassSetUp(TestContext testContext)
        {
            _imageService = new MockImageService();
            _batchDetailRepository = new BatchDetailRepository(new MockR360ServiceContext(), _imageService, new MockBatchDataProvider());
        }

        [TestMethod]
        public void Check_ElectronicBatch()
        {
            var iconsVisible = _batchDetailRepository.ImageIconVisible(new ImageRequestDto
            {
                BankId = 1,
                SourceBatchId = 1,
                BatchPaymentSource = "BatchSource",
                PicsDate = 20000101,
                IsCheck = true,
                BatchSequence = 0,
                WorkgroupId = 1,
                ImportTypeShortName = "ImportType"
            }, true);

            Assert.AreEqual(true, iconsVisible);
        }

        [TestMethod]
        public void Document_ElectronicBatch()
        {
            var iconsVisible = _batchDetailRepository.ImageIconVisible(new ImageRequestDto
            {
                BankId = 1,
                SourceBatchId = 1,
                BatchPaymentSource = "BatchSource",
                PicsDate = 20000101,
                IsCheck = false,
                BatchSequence = 0,
                WorkgroupId = 1,
                ImportTypeShortName = "ImportType"
            }, true);

            Assert.AreEqual(true, iconsVisible);
        }
    }
}
