﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using System;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;
using WFS.RecHub.R360Shared;

namespace R360Tests
{
    [TestClass]
    public class AdvancedSearchStoredQueryTests
    {

        private readonly AdvancedSearchStoredQueryRepository _repository;

        public AdvancedSearchStoredQueryTests()
        {
            _repository = new AdvancedSearchStoredQueryRepository(
                new AdvancedSearchRequestXmlTransformer(),
                new MockAdvancedSearchStoredQueryDataProvider());
        }

        [TestMethod]
        public void AdvancedSearchStoredQueries_ShouldFail_WithoutQuery()
        {
            // Arrange
            var request = new AdvancedSearchStoredQueryDto()
            {
                Description = "Description",
                Name = "Name",
                IsDefault = true
            };

            // Act
            var result = _repository.Save(request);

            // Assert
            Assert.AreEqual(StatusCode.FAIL, result.Status);
        }

        [TestMethod]
        public void AdvancedSearchStoredQueries_ShouldSave_Successfully()
        {
            // Arrange
            var request = new AdvancedSearchStoredQueryDto()
            {
                Description = "Description",
                Name = "Name1",
                IsDefault = true,
                Query = new AdvancedSearchDto()
                {
                    BankId = 1,
                    WorkgroupId = 1,
                    DepositDateFrom = DateTime.Now,
                    DepositDateTo = DateTime.Now
                }
            };

            // Act
            var result = _repository.Save(request);

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, result.Status);
        }

        [TestMethod]
        public void AdvancedSearchStoredQueries_ShouldDelete_Successfully()
        {
            // Arrange
            var entity = _repository.GetAll().Data[0];

            // Act
            var result = _repository.Delete(entity.Id);

            // Assert
            Assert.AreEqual(StatusCode.SUCCESS, result.Status);
        }

        [TestMethod]
        public void AdvancedSearchStoredQueries_ShouldFailDeleting_DueToUnauthorizedId()
        {
            // Arrange
            var id = Guid.NewGuid();

            // Act
            var result = _repository.Delete(id);

            // Assert
            Assert.AreEqual(StatusCode.FAIL, result.Status);
        }
    }
}
