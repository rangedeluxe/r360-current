﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.R360ServicesAPI.Services;
using R360Tests.Mocks;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests
{
    [TestClass]
    public class BatchDetailTests
    {
        private static IServiceContext serviceContext;
        private static IImageService iconservice;
        private static IBatchDataProvider batchservice;

        [ClassInitialize]
        public static void TestStartUp(TestContext context)
        {
            serviceContext = new MockServiceContext();
            iconservice = new MockImageService();
            batchservice = new MockBatchDataProvider();
        }

        [TestMethod]
        public void TestGetBatchDetailDal()
        {
            var repo = new BatchDetailRepository(serviceContext, iconservice, batchservice);
            var request = new BatchDetailRequestDto
            {
                BankId = 999,
                BatchId = 26541,
                DepositDate = new DateTime(2015, 12, 1),
                WorkgroupId = 202020
            };

            // Act
            var response = repo.Get(request);
        }
    }
}