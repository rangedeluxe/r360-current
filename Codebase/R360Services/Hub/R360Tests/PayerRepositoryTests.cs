﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces.Fakes;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;

namespace R360Tests
{
    [TestClass]
    public class PayerRepositoryTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void When_TheR360ConnectionStringIsNotProvided_ThenThePayerRepositoryIsNotCreated()
        {
            //arrange
            var repository = new PayerRepository(null);

            //act
            
            //assert
        }

        [TestMethod]
        public void Will_ReturnNoResult_WhenNullLookupProvided()
        {

        }

        [TestMethod]
        public void Will_ReturnNoResult_WhenNoResultsAreFound()
        {
            //arrange
            var payerRepository = new PayerRepository(new R360DBConnectionSettings());

            //act
            var actual = payerRepository.TryGet(null);

            //assert
            Assert.IsTrue(actual is PossibleResult<IEnumerable<PayerDto>>);
        }

        [TestMethod]
        public void Can_Get_Payers()
        {
            //arrange
            var payerRepository = new PayerRepository(new R360DBConnectionSettings());
            var request = new PayerLookupRequestDto
            {
                BankId = 3966,
                WorkgroupId = 303,
                Account = "123456",
                RoutingNumber = "104000016"

            };

            //act
            var actual = payerRepository.TryGet(request);

            //assert
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.GetResult(new List<PayerDto>()));
            
        }
    }
}
