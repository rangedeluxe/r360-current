﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using Wfs.Logging.Performance;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.PostDepositExceptions;
using WFS.RecHub.R360Shared;

namespace R360Tests
{
    [TestClass]
    public class PostDepositExceptionsTests
    {
        private readonly MockPermissionProvider _permissions;

        public PostDepositExceptionsTests()
        {
            _permissions = new MockPermissionProvider();
        }

        [TestInitialize]
        public void SetUp()
        {
            ServiceContext = new MockServiceContext();
            ExceptionsDal = new MockPostDepositExceptionsDal();
            LockboxApi = new MockLockboxAPI();
            PostDepositDataProvider = new MockPostDepositDataProvider();
            StubDataProvider = new MockStubDataProvider();
            DocDataProvider = new MockDocumentDataProvider();
        }

        private MockPostDepositExceptionsDal ExceptionsDal { get; set; }
        private MockLockboxAPI LockboxApi { get; set; }
        private MockPostDepositDataProvider PostDepositDataProvider { get; set; }
        private MockStubDataProvider StubDataProvider { get; set; }
        private MockDocumentDataProvider DocDataProvider { get; set; }
        private MockServiceContext ServiceContext { get; set; }
        private ILookupRepository<PayerLookupRequestDto, PossibleResult<IEnumerable<PayerDto>>> PayerLookupRepository { get; set; }

        private PostDepositExceptionsApi GenerateAPI()
        {
            return new PostDepositExceptionsApi(
                ServiceContext,
                new MockR360ServicesDAL(),
                ExceptionsDal,
                new MockSessionDAL(),
                new MockRaamClient(),
                LockboxApi,
                new MockPaymentDataProvider(),
                StubDataProvider,
                new MockDataEntryDataProvider(),
                PostDepositDataProvider,
                new MockImageProvider(),
                new MockRuleProvider(),
                DocDataProvider,
                new NullPerformanceLogger(),
                _permissions,
                new MockRuleProvider(),
                PayerLookupRepository
            );
        }

        [TestMethod]
        public void GetPendingTransactions_ShouldReturn_ValidTransactions()
        {
            var api = GenerateAPI();

            var req = new PostDepositPendingTransactionsRequestDto();
            var results = api.GetPendingTransactions(req);

            Assert.IsTrue(results.Data.Transactions.Any());

        }

        [TestMethod]
        public void GetPendingTransactions_ShouldReturn_ValidTransactions_withNoUnlockOverride()
        {
            var api = GenerateAPI();
            _permissions.ProvidedPermissions.Add(new PermissionDto() { Permission = R360Permissions.Perm_PostDepositUnlockOverride, HasPermission = false });

            var req = new PostDepositPendingTransactionsRequestDto();
            var results = api.GetPendingTransactions(req);

            Assert.IsFalse(results.Data.Transactions[0].UserCanOverrideUnlock);
        }

        [TestMethod]
        public void GetPendingTransactions_ShouldReturn_ValidTransactions_withUnlockOverride()
        {
            var api = GenerateAPI();
            _permissions.ProvidedPermissions.Add(new PermissionDto() { Permission = R360Permissions.Perm_PostDepositUnlockOverride, HasPermission = true });

            var req = new PostDepositPendingTransactionsRequestDto();
            var results = api.GetPendingTransactions(req);

            Assert.IsTrue(results.Data.Transactions[0].UserCanOverrideUnlock);
        }

        [TestMethod]
        public void GetTransaction_ShouldReturn_ValidTransaction()
        {
            var api = GenerateAPI();
            var request = new PostDepositTransactionRequestDto()
            {
                BatchId = 1,
                DepositDate = DateTime.Now
            };

            PostDepositDataProvider.AddDataEntryValueDto(new DataEntryValueDto {FieldName = "Test1", Title = "TestTitle", Type = 1, Value = "TestValue",IsEditable = true});
            LockboxApi.SetOLLockboxes(new List<cOLLockbox> {new cOLLockbox {BankID = 1, LockboxID = 123}});
            ExceptionsDal.SetTransactions(new[]
            {
                new PostDepositTransactionResponseDto
                {
                    BankId = 1,
                    WorkgroupId = 123,
                    BatchId = 100,
                    DepositDate = DateTime.Now
                }
            });
            
            var response = api.GetTransaction(request);

            Assert.AreEqual(StatusCode.SUCCESS, response.Status);
        }

        [TestMethod]
        public void SavePayer()
        {
            var api = GenerateAPI();
            var request = new PostDepositTransactionRequestDto()
            {
                BatchId = 1,
                DepositDate = DateTime.Now
            };

            PostDepositDataProvider.AddDataEntryValueDto(new DataEntryValueDto { FieldName = "Test1", Title = "TestTitle", Type = 1, Value = "TestValue", IsEditable = true });
            LockboxApi.SetOLLockboxes(new List<cOLLockbox> { new cOLLockbox { BankID = 1, LockboxID = 123 } });
            ExceptionsDal.SetTransactions(new[]
            {
                new PostDepositTransactionResponseDto
                {
                    BankId = 1,
                    WorkgroupId = 123,
                    BatchId = 100,
                    DepositDate = DateTime.Now
                }
            });

            var resp = api.SavePayer(new PostDepositSavePayerDto
            {
                Account = "11",
                BankId = 11,
                ClientAccountId = 11,
                PayerName = "newPayer",
                RoutingNumber = "11",
                UserName = "tEmo"
            });

            Assert.AreEqual(StatusCode.SUCCESS, resp.Status);
        }

        [TestMethod]
        public void GetPostDepositDataEntryFieldsForStub_ShouldReturn_ValidDtoCollection()
        {
            var dataProvider = new MockPostDepositDataProvider();

            dataProvider.AddDataEntryValueDto(new DataEntryValueDto() { FieldName = "Test1", Title = "TestTitle", Type = 1, Value = "TestValue", IsEditable = true });
            dataProvider.AddDataEntryValueDto(new DataEntryValueDto() { FieldName = "Test2", Title = "TestTitle", Type = 1, Value = "TestValue", IsEditable = false });
            dataProvider.AddDataEntryValueDto(new DataEntryValueDto() { FieldName = "Test3", Title = "TestTitle", Type = 1, Value = "TestValue", IsEditable = true });

            var stubs = dataProvider.GetPostDepositDataEntryFieldsForStub(1, 34, 89, DateTime.Now, 45, 66);
            Assert.AreEqual(stubs.Count(), 3);
        }

        [TestMethod]
        public void LockTransaction_ShouldNotLock_LockedTransaction()
        {
            var api = GenerateAPI();

            PostDepositDataProvider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Locked;

            var response = api.LockTransaction(new LockTransactionRequestDto());

            Assert.AreEqual(StatusCode.FAIL, response.Status);
        }

        [TestMethod]
        public void LockTransaction_ShouldLock_UnlockedTransaction()
        {
            var api = GenerateAPI();

            PostDepositDataProvider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Unlocked;

            var response = api.LockTransaction(new LockTransactionRequestDto());

            Assert.AreEqual(StatusCode.SUCCESS, response.Status);
        }

        [TestMethod]
        public void UnlockTransaction_ShouldNotUnlock_UnlockedTransaction()
        {
            var api = GenerateAPI();

            PostDepositDataProvider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Unlocked;

            var response = api.UnlockTransactionWithoutOverride(new LockTransactionRequestDto());

            Assert.AreEqual(StatusCode.FAIL, response.Status);
        }

        [TestMethod]
        public void UnlockTransaction_ShouldNotUnlock_LockedTransactionByAnotherUser()
        {
            var dataprovider = new MockPostDepositDataProvider();
            var context = new MockR360ServiceContext();
            var api = new PostDepositExceptionsApi(
                context,
                new MockR360ServicesDAL(),
                new MockPostDepositExceptionsDal(),
                new MockSessionDAL(),
                new MockRaamClient(),
                new MockLockboxAPI(),
                new MockPaymentDataProvider(),
                new MockStubDataProvider(),
                new MockDataEntryDataProvider(),
                dataprovider,
                new MockImageProvider(),
                new MockRuleProvider(),
                DocDataProvider,
                new NullPerformanceLogger(),
                _permissions,
                new MockRuleProvider(),
                PayerLookupRepository);

            // Set current user and locked user to different IDs.
            context.SID = Guid.NewGuid();
            context.RAAMClaims = new SimpleClaim[]
            {
                new SimpleClaim() {Type = "Claim1", Value = "Val1"}
            };
            dataprovider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Locked;
            dataprovider.LockedByUser = Guid.NewGuid();
            _permissions.ProvidedPermissions.Add(new PermissionDto() { Permission = R360Permissions.Perm_PostDepositUnlockOverride, HasPermission = false });

            var response = api.UnlockTransactionWithoutOverride(new LockTransactionRequestDto());

            Assert.AreEqual(StatusCode.FAIL, response.Status);
        }

        [TestMethod]
        public void UnlockTransaction_ShouldUnlock_LockedTransaction()
        {
            var dataprovider = new MockPostDepositDataProvider();
            var context = new MockR360ServiceContext();
            var api = new PostDepositExceptionsApi(
                context,
                new MockR360ServicesDAL(),
                new MockPostDepositExceptionsDal(),
                new MockSessionDAL(),
                new MockRaamClient(),
                new MockLockboxAPI(),
                new MockPaymentDataProvider(),
                new MockStubDataProvider(),
                new MockDataEntryDataProvider(),
                dataprovider,
                new MockImageProvider(),
                new MockRuleProvider(),
                DocDataProvider,
                new NullPerformanceLogger(),
                new MockPermissionProvider(),
                new MockRuleProvider(),
                PayerLookupRepository);

            // Set current user and locked user to same IDs.
            context.SID = Guid.NewGuid();
            context.RAAMClaims = new SimpleClaim[]
            {
                new SimpleClaim() {Type = "Claim1", Value = "Val1"}
            };
            dataprovider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Locked;
            dataprovider.LockedByUser = context.SID;

            var response = api.UnlockTransactionWithoutOverride(new LockTransactionRequestDto());

            Assert.AreEqual(StatusCode.SUCCESS, response.Status);
        }

        [TestMethod]
        public void UnlockTransaction_ShouldUnlockWithOverride_LockedTransaction()
        {
            var dataprovider = new MockPostDepositDataProvider();
            var context = new MockR360ServiceContext();
            var api = new PostDepositExceptionsApi(
                context,
                new MockR360ServicesDAL(),
                new MockPostDepositExceptionsDal(),
                new MockSessionDAL(),
                new MockRaamClient(),
                new MockLockboxAPI(),
                new MockPaymentDataProvider(),
                new MockStubDataProvider(),
                new MockDataEntryDataProvider(),
                dataprovider,
                new MockImageProvider(),
                new MockRuleProvider(),
                DocDataProvider,
                new NullPerformanceLogger(),
                _permissions,
                new MockRuleProvider(),
                PayerLookupRepository);

            // Set current user and locked user to same IDs.
            context.SID = Guid.NewGuid();
            context.RAAMClaims = new SimpleClaim[]
            {
                new SimpleClaim() {Type = "Claim1", Value = "Val1"}
            };
            dataprovider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Locked;
            dataprovider.LockedByUser = Guid.NewGuid();

            dataprovider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Locked;
            _permissions.ProvidedPermissions.Add(new PermissionDto() { Permission = R360Permissions.Perm_PostDepositUnlockOverride, HasPermission = true });

            var response = api.UnlockTransaction(new LockTransactionRequestDto());

            Assert.AreEqual(StatusCode.SUCCESS, response.Status);
        }

        [TestMethod]
        public void UnlockTransaction_ShouldNotUnlockWithOverride_LockedTransaction()
        {
            var dataprovider = new MockPostDepositDataProvider();
            var context = new MockR360ServiceContext();
            var api = new PostDepositExceptionsApi(
                context,
                new MockR360ServicesDAL(),
                new MockPostDepositExceptionsDal(),
                new MockSessionDAL(),
                new MockRaamClient(),
                new MockLockboxAPI(),
                new MockPaymentDataProvider(),
                new MockStubDataProvider(),
                new MockDataEntryDataProvider(),
                dataprovider,
                new MockImageProvider(),
                new MockRuleProvider(),
                DocDataProvider,
                new NullPerformanceLogger(),
                _permissions,
                new MockRuleProvider(),
                PayerLookupRepository);

            // Set current user and locked user to same IDs.
            context.SID = Guid.NewGuid();
            context.RAAMClaims = new SimpleClaim[]
            {
                new SimpleClaim() {Type = "Claim1", Value = "Val1"}
            };
            dataprovider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Locked;
            dataprovider.LockedByUser = Guid.NewGuid();

            dataprovider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Locked;
            _permissions.ProvidedPermissions.Add(new PermissionDto() { Permission = R360Permissions.Perm_PostDepositUnlockOverride, HasPermission = false });

            var response = api.UnlockTransaction(new LockTransactionRequestDto());

            Assert.AreEqual(StatusCode.FAIL, response.Status);
        }
    }
}
