﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Xml;

namespace R360Tests
{
    public class BaseTestFixture
    {

        public const string RESOURCE_DIR = @"Resources";

        public XmlDocument LoadTest(string testname)
        {
            if (!testname.EndsWith("xml"))
                testname += ".xml";

            var path = Path.Combine(Environment.CurrentDirectory, RESOURCE_DIR, testname);
            if (!File.Exists(path))
                throw new InvalidOperationException(string.Format("The test at path '{0}' does not exist.", path));

            var doc = new XmlDocument();
            doc.LoadXml(File.ReadAllText(path));
            return doc;
        }

        /// <summary>
        /// Loads by the name of the previously called method.
        /// </summary>
        /// <returns></returns>
        public XmlDocument LoadTestByCallerName([CallerMemberName] string testname = null)
        {
            return LoadTest(testname);
        }
    }
}
