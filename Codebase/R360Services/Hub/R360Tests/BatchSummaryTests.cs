﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;
using WFS.RecHub.R360Shared;
using R360Tests.Mocks;

namespace R360Tests
{
    [TestClass]
    public class BatchSummaryTests
    {
        private static IServiceContext serviceContext;

        private BaseGenericResponse<cOLLockboxes> DefaultWorkgroups
        {
            get
            {
                var output = new BaseGenericResponse<cOLLockboxes>();
                output.Data = new cOLLockboxes();
                output.Data.Add(1, new cOLLockbox
                {
                    BankID = 1,
                    DisplayName = "Testing Workgroup",
                    EntityID = 2,
                    LongName = "Testing Workgroup",
                    LockboxID = 20,
                    MaximumSearchDays = 2
                });
                output.Status = StatusCode.SUCCESS;
                return output;
            }
        }

        [ClassInitialize]
        public static void TestStartUp(TestContext context)
        {
            serviceContext = new MockServiceContext();
        }

        [TestMethod, Ignore]
        public void BatchSummary_ShouldSucceed_RequestingBatches()
        {
            //// Arrange
            //using (ShimsContext.Create())
            //{
            //    ShimLockboxAPI.AllInstances.GetUserLockboxes = (api) => DefaultWorkgroups;
            //    ShimcItemProcDAL.AllInstances.GetBatchSummaryGuidInt32Int32DateTimeDateTimeNullableOfInt32NullableOfInt32BooleanInt32Int32StringStringStringInt32OutInt32OutDataTableOut =
            //        (cItemProcDAL api, Guid SessionID, int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, int? paymentTypeID, int? paymentSourceID, bool DisplayScannedCheckOnline
            //            , int start, int length, string orderby, string orderdirection, string search, out int recordstotal, out int recordsfiltered, out DataTable dt) =>
            //        {
            //            recordstotal = 1;
            //            recordsfiltered = 1;
            //            dt = DefaultBatches;
            //            return true;
            //        };
            //
            //    var repo = new BatchSummaryRepository(serviceContext);
            //    var request = new BatchSummaryRequestDto()
            //    {
            //        BankId = 1,
            //        ClientAccountId = 20,
            //        FromDate = new DateTime(2015, 01, 01),
            //        ToDate = new DateTime(2015, 01, 02)
            //    };
            //
            //    // Act
            //    var response = repo.Get(request);
            //
            //    // Assert
            //    Assert.AreEqual(StatusCode.SUCCESS, response.Status);
            //    Assert.AreEqual(1, response.TotalRecords);
            //    Assert.AreEqual(1, response.BatchSummaryList.Count());
            //}
        }

        [TestMethod]
        public void BatchSummary_ShouldDenyAccess_ToRequestedDateTimespan()
        {
            // Arrange
            var api = new MockLockboxAPI();
            var prefs = new MockPreferencesProvider();
            var batches = new MockBatchDataProvider();
            var repo = new BatchSummaryRepository(serviceContext, api, prefs, batches);
            var request = new BatchSummaryRequestDto
            {
                BankId = 1,
                ClientAccountId = 20,
                FromDate = new DateTime(2015, 01, 01),
                ToDate = new DateTime(2015, 01, 03)
            };

            // Act
            var response = repo.Get(request);

            // Assert
            Assert.AreEqual(StatusCode.FAIL, response.Status);

        }

        [TestMethod]
        public void BatchSummary_ShouldDenyAccess_ToRequestedWorkgroup()
        {
            // Arrange
            var api = new MockLockboxAPI();
            var prefs = new MockPreferencesProvider();
            var batches = new MockBatchDataProvider();
            var repo = new BatchSummaryRepository(serviceContext, api, prefs, batches);
            var request = new BatchSummaryRequestDto();

            // Act
            var response = repo.Get(request);

            // Assert
            Assert.AreEqual(StatusCode.FAIL, response.Status);
        }
    }
}