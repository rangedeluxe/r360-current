﻿using System;
using System.Collections.Generic;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockPostDepositDataProvider : IPostDepositDataProvider
    {
        private readonly List<DataEntrySetupDto> _dataEntrySetupDtos = new List<DataEntrySetupDto>();
        private readonly List<DataEntryValueDto> _dataEntryValueDtos = new List<DataEntryValueDto>();

        public MockPostDepositDataProvider()
        {
            LockStatus = LockReturnStatus.Unlocked;
            LockedByUser = Guid.NewGuid();
        }

        public enum LockReturnStatus
        {
            Locked,
            Unlocked
        }

        public void AddDataEntrySetupDto(DataEntrySetupDto dataEntrySetupDto)
        {
            _dataEntrySetupDtos.Add(dataEntrySetupDto);
        }
        public void AddDataEntryValueDto(DataEntryValueDto dataEntryValueDto)
        {
            _dataEntryValueDtos.Add(dataEntryValueDto);
        }

        public Guid LockedByUser { get; set; }

        public LockReturnStatus LockStatus { get; set; }

        public TransactionLockDto GetTransactionLock(long batchid, DateTime depositdate, int transactionid)
        {
            return LockStatus == LockReturnStatus.Locked
                ? new TransactionLockDto()
                {
                    User = "The Professor",
                    UserSID = this.LockedByUser,
                    LockedDate = DateTime.Now
                }
                : null;
        }
        public IEnumerable<DataEntryValueDto> GetPostDepositDataEntryFieldsForStub(int bankId, int workgroupId,
            long batchId, DateTime depositDate, int transactionId, int batchSequence)
        {
            return _dataEntryValueDtos;
        }
        public IEnumerable<DataEntrySetupDto> GetDataEntrySetupFields(int bankid, int workgroupid, long batchid, DateTime depositdate)
        {
            return _dataEntrySetupDtos;
        }
        public bool LockTransaction(long batchid, DateTime depositdate, int transactionid)
        {
            return true;
        }

        public bool UnlockTransaction(long batchid, DateTime depositdate, int transactionid)
        {
            return true;
        }
    }
}
