﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Shared;

namespace R360Tests.Mocks
{
    public class MockNotificationsDataProvider : INotificationsDataProvider
    {

        public NotificationsResponseDto GetUserNotifications(Guid SessionID, DateTime startDate, DateTime endDate,
            int notificationFileTypeKey, string orderBy, string orderDir, int startRecord, int maxRows, int timeZoneBias,
            string fileName, XmlDocument docReqXml, string search)
        {
            var response = new NotificationsResponseDto();
            response.Notifications = new List<NotificationDto>
            {
                new NotificationDto {Message = "Test \n \r \t \\n \\r \\t"},
                new NotificationDto {Message = "Test \n "},
                new NotificationDto {Message = "Test \r sadas "},
                new NotificationDto {Message = "Test \t sadas "},
                new NotificationDto {Message = "Test \n sadas \n"},
            };

            response.FilteredTotal = 1;
            response.TotalRecords = 1;
            response.Errors = new List<string>();
            response.Status = StatusCode.SUCCESS;
            return response;
        }

        public NotificationDto GetNotification(int messagegroup, int timezonebias, Guid sessionId)
        {
            return new NotificationDto {Message = "\n \r \t \\n \\r \\tTest \n \r \t \\n \\r \\t" };
        }

        public IEnumerable<NotificationFileDto> GetNotificationFiles(int messagegroup, Guid sessionId)
        {
            return new List<NotificationFileDto>();
        }

        public bool GetNotificationFileTypes(out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public List<WorkgroupDto> GetWorkgroups(string id)
        {
            var wgl = new List<WorkgroupDto>();
            wgl.Add(new WorkgroupDto {BankId = 1, ClientAccountId = 1});
            return wgl;
        }

        public long GetNotificationFileSize(string path)
        {
            throw new NotImplementedException();
        }
    }
}
