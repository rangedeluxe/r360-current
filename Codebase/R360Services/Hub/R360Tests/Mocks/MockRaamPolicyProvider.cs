﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Shared;

namespace R360Tests.Mocks
{
    public class MockRaamPolicyProvider : IPolicyProvider
    {
        public bool HasPermission(string asset, R360Permissions.ActionType type)
        {
            return asset == R360Permissions.Perm_AlertDownload;
        }
    }
}
