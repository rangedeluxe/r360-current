﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockImageProvider : IImageProvider
    {
        public IEnumerable<SingleImageRequestDto> GetAvailableDocumentImages(IEnumerable<ItemRequestDto> docs)
        {
            return new List<SingleImageRequestDto>()
            {

            };
        }

        public IEnumerable<SingleImageRequestDto> GetAvailablePaymentsImages(IEnumerable<ItemRequestDto> payments)
        {
            return new List<SingleImageRequestDto>()
            {

            };
        }

        public IEnumerable<SingleImageRequestDto> GetAvailableStubsImages(IEnumerable<ItemRequestDto> stubs)
        {
            return new List<SingleImageRequestDto>()
            {

            };
        }
    }
}
