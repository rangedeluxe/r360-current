﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockPaymentSearchDataProvider : IPaymentSearchDataProvider
    {
        private const string PS_RESULTS_TOTAL = "SamplePaymentSearchResponse.xml";
        private readonly XmlDocument _totals;
        public MockPaymentSearchDataProvider()
        {
            _totals = new XmlDocument();
            _totals.Load(Path.Combine(Environment.CurrentDirectory, "Resources", PS_RESULTS_TOTAL));
        }

        public cOLLockboxes GetAuthorizedWorkgroups()
        {
            var output = new cOLLockboxes();
            output.Add(1, new cOLLockbox()
            {
                BankID = 1,
                LockboxID = 2,
                MaximumSearchDays = 3,
                OnlineViewingDays = 3,
                LongName = "Workgroup Name"
            });
            output.Add(2, new cOLLockbox()
            {
                BankID = 3,
                LockboxID = 4,
                MaximumSearchDays = 3,
                OnlineViewingDays = 3,
                LongName = "Workgroup Name"
            });
            return output;
        }

        public bool GetPaymentSearch(XmlDocument xml, out XmlDocument resultsxml, out DataTable results)
        {
            resultsxml = _totals;
            results = new DataTable();
            results.Columns.AddRange(new DataColumn[]
            {
                new DataColumn("Account"),
                new DataColumn("Amount"),
                new DataColumn("BankID"),
                new DataColumn("BatchID"),
                new DataColumn("BatchNumber"),
                new DataColumn("BatchSequence"),
                new DataColumn("BatchSourceShortName"),
                new DataColumn("DDA"),
                new DataColumn("DepositDate"),
                new DataColumn("ImportTypeShortName"),
                new DataColumn("PaymentSource"),
                new DataColumn("PaymentType"),
                new DataColumn("PICSDate"),
                new DataColumn("RemitterName"),
                new DataColumn("RT"),
                new DataColumn("Serial"),
                new DataColumn("SourceBatchID"),
                new DataColumn("TransactionID"),
                new DataColumn("TxnSequence"),
                new DataColumn("ClientAccountID"),
                new DataColumn("ClientAccountKey")
            });

            var row = results.NewRow();
            row["Account"] = "1";
            row["Amount"] = 101.50;
            row["BankID"] = 1;
            row["BatchID"] = 2;
            row["BatchNumber"] = 3;
            row["BatchSequence"] = 4;
            row["BatchSourceShortName"] = "ACH";
            row["DDA"] = "121";
            row["DepositDate"] = "12/03/2015";
            row["ImportTypeShortName"] = "DIT";
            row["PaymentSource"] = "Source";
            row["PaymentType"] = "Type";
            row["PICSDate"] = 20151203;
            row["RemitterName"] = "Fred";
            row["RT"] = "00102";
            row["Serial"] = "002002";
            row["SourceBatchID"] = 10;
            row["TransactionID"] = 11;
            row["TxnSequence"] = 12;
            row["ClientAccountID"] = 13;
            row["ClientAccountKey"] = 14;
            results.Rows.Add(row);

            return true;
        }

        public List<WorkgroupDto> GetWorkgroups(string id)
        {
            return new List<WorkgroupDto>()
            {
                new WorkgroupDto()
                {
                    BankId = 1,
                    ClientAccountId = 2
                },
                new WorkgroupDto()
                {
                    BankId = 3,
                    ClientAccountId = 4
                }
            };
        }
    }
}
