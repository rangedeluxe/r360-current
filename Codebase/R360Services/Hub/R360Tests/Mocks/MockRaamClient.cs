﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;

namespace R360Tests.Mocks
{
    public class MockRaamClient : IRaamClient
    {
        public bool CreateOrUpdateWorkgroupResource(int entityID, int siteBankID, int workgroupID, string name)
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto GetAncestorEntityHierarchy(int entityID)
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto GetAncestorFIEntity(int entityID)
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto GetAuthorizedEntities()
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto GetAuthorizedEntitiesAndWorkgroups()
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto GetEntitiesWithPermission(int rootEntityId, string asset, R360Permissions.ActionType action)
        {
            throw new NotImplementedException();
        }

        public string GetEntityBreadcrumb(int entityID)
        {
            return "Entity";
        }

        public bool GetEntityBreadcrumbList(int entityId, List<EntityBreadcrumbDTO> entityBreadcrumbs, bool includeHierarchy)
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto GetEntityGroupHierarchy()
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto GetEntityHierarchy(int entityID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetEventTypes()
        {
            throw new NotImplementedException();
        }

        public IList<Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto> GetFIEntities()
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.EntityDto GetLoggedInEntity()
        {
            throw new NotImplementedException();
        }

        public string GetEntityName(int entityID)
        {
            return "MockRaamClientEntity";
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.UserDto GetUserByUserSID(Guid userSID)
        {
            return new Wfs.Raam.Service.Authorization.Contracts.DataContracts.UserDto()
            {
                Person = new Wfs.Raam.Service.Authorization.Contracts.DataContracts.PersonDto()
                {
                    FirstName = "banana",
                    LastName = "phone"
                }
            };
        }

        public IEnumerable<Wfs.Raam.Service.Authorization.Contracts.DataContracts.UserDto> GetUserHierarchy()
        {
            throw new NotImplementedException();
        }

        public IList<int> GetUsersFIEntitiesIds()
        {
            throw new NotImplementedException();
        }

        public Wfs.Raam.Service.Authorization.Contracts.DataContracts.ResourceDto GetWorkgroupResource(int siteBankID, int workgroupID)
        {
            return new Wfs.Raam.Service.Authorization.Contracts.DataContracts.ResourceDto()
            {
                ApplicationID = 1,
                EntityID = 1,
                ExternalReferenceID = "1",
                ID = 1,
                Name = "Ent",
                ResourceTypeCode = "Entity"
            };
        }

        public IEnumerable<Wfs.Raam.Service.Authorization.Contracts.DataContracts.ResourceDto> GetWorkgroupsForEntity(int entityid)
        {
            throw new NotImplementedException();
        }

        public void RemoveWorkgroupResource(int entityID, int siteBankID, int workgroupID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<EntityDto> GetAuthorizedEntityList()
        {
            return new List<EntityDto>()
            {
                new EntityDto()
                {
                    ExternalEntityID = "1",
                    HierarchyString = "WFS > Entity"
                }
            };
        }

        public string GetNameFromSid(Guid sid)
        {
            return "Bill Gates";
        }
    }
}
