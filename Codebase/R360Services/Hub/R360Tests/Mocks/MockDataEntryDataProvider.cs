﻿using System;
using System.Collections.Generic;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockDataEntryDataProvider : IDataEntryDataProvider
    {
        public IEnumerable<DataEntryValueDto> GetDataEntryFieldsForPayment(int bankid, int workgroupid, long batchid, DateTime depositdate, int transactionid, int batchsequence)
        {
            return new List<DataEntryValueDto>()
            {

            };
        }
    }
}
