﻿using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Services.R360ServicesAPI.Services;

namespace R360Tests.Mocks
{
    public class MockImageService : IImageService
    {
        public long GetImageSize(int PICSDate, int BankID, int LockboxID, long BatchID, int Item, int DepositDateKey, 
            string ImageType, short Page, long SourceBatchID, string BatchSourceShortName, string ImportTypeShortName)
        {
            return 1L;
        }

        public OlfResponseDto ImagesExist(ImageRequestDto request)
        {
            return new OlfResponseDto{ImagesAvailable = true,IsSuccessful = true};
        }

        public OlfResponseDto BatchSequenceImagesExist(ImageRequestDto request)
        {
            return new OlfResponseDto { ImagesAvailable = true, IsSuccessful = true };
        }
    }
}
