﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesAPI;

namespace R360Tests.Mocks
{
    public class MockLockboxAPI : ILockboxAPI
    {
        private cOLLockboxes _olLockboxes = new cOLLockboxes();

        public void SetOLLockboxes(List <cOLLockbox> olLockboxes)
        {
            int olLockboxCount = 1;

            foreach (var olLockbox in olLockboxes)
            {
                _olLockboxes.Add(olLockboxCount++, olLockbox);
            }
        }

        public ISessionInfo Session {
            set { throw new NotImplementedException(); }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<cLockbox> GetLockboxByID(Guid OLLockboxID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<Dictionary<int, string>> GetPaymentTypes()
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<cOLLockboxes> GetUserLockboxes()
        {
            return new BaseGenericResponse<cOLLockboxes>()
            {
                Data = _olLockboxes
            };
        }
    }
}
