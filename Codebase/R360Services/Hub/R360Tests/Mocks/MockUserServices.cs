﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace R360Tests.Mocks
{
    public class MockUserServices : IUserServices
    {
        public XmlDocumentResponse GetOLUserPreferences()
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetOnlineUserByID(int UserID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<cUserRAAM> GetUserByID(int UserID)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<cOLLockboxes> GetUserLockboxes()
        {
            var data = new cOLLockboxes();
            data.Add(0, new cOLLockbox()
            {
                BankID = 0,
                LockboxID = 0
            });
            data.Add(1, new cOLLockbox()
            {
                BankID = 0,
                LockboxID = 1
            });
            return new BaseGenericResponse<cOLLockboxes>()
            {
                Status = WFS.RecHub.R360Shared.StatusCode.SUCCESS,
                Data = data
            };
        }

        public BaseGenericResponse<UserPreferencesDTO> GetUserPreferences()
        {
            throw new NotImplementedException();
        }

        public PingResponse Ping()
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse RestoreDefaultOLPreferences()
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<UserPreferencesDTO> RestoreDefaultPreferences()
        {
            throw new NotImplementedException();
        }

        public XmlDocumentResponse SetOLUserPreferences(XmlDocument Parms)
        {
            throw new NotImplementedException();
        }

        public BaseGenericResponse<bool> SetUserPreferences(UserPreferencesDTO Preferences)
        {
            throw new NotImplementedException();
        }
    }
}
