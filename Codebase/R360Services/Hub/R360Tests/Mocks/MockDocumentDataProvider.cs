﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockDocumentDataProvider : IDocumentDataProvider
    {
        public IEnumerable<DocumentDto> GetTransactionDocuments(cTransaction transaction)
        {
            var docs = new List<DocumentDto>();
            docs.Add(new DocumentDto
            {
                BatchSequence = 1,
                BatchSourceShortName = "name",
                Description = "desc",
                DocumentSequence = 2,
                FileDescriptor = "png",
                ImageIsAvailable = true,
                ImportTypeShortName = "ach",
                PicsDate = 20170920,
                SourceProcessingDateKey = 20170920
            });
            return docs;
        }
    }
}
