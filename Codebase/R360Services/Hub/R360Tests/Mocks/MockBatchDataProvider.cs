﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockBatchDataProvider : IBatchDataProvider
    {
        public bool GetBatchDetail(long BatchID, DateTime DepositDate, bool DisplayScannedCheckOnline, int StartRecord, int RecordsPerPage, string OrderBy, string OrderDirection, string Search, out int TotalRecords, out int FilteredRecords, out DataTable dt)
        {
            var table = new DataTable();
            table.Columns.Add("SourceBatchID", typeof(long));
            table.Columns.Add("BatchSiteCode", typeof(int));
            table.Columns.Add("BatchId", typeof(long));
            table.Columns.Add("BatchNumber", typeof(string));
            table.Columns.Add("DepositDate", typeof(DateTime));
            table.Columns.Add("PICSDate", typeof(string));
            table.Columns.Add("ProcessingDate", typeof(DateTime));
            table.Columns.Add("TransactionID", typeof(int));
            table.Columns.Add("TxnSequence", typeof(int));
            table.Columns.Add("BatchSequence", typeof(int));
            table.Columns.Add("DocumentCount", typeof(int));
            table.Columns.Add("CheckCount", typeof(int));
            table.Columns.Add("StubCount", typeof(int));
            table.Columns.Add("PaymentSource", typeof(string));
            table.Columns.Add("PaymentType", typeof(string));
            table.Columns.Add("BatchTotal", typeof(decimal));
            table.Columns.Add("Amount", typeof(decimal));
            table.Columns.Add("TransactionCount", typeof(int));
            table.Columns.Add("Workgroup", typeof(string));
            table.Columns.Add("BankId", typeof(int));
            table.Columns.Add("WorkgroupId", typeof(int));
            table.Columns.Add("Serial", typeof(string));
            table.Columns.Add("RT", typeof(string));
            table.Columns.Add("Account", typeof(string));
            table.Columns.Add("RemitterName", typeof(string));
            table.Columns.Add("BatchCueID", typeof(int));
            table.Columns.Add("ImportTypeKey", typeof(int));
            table.Columns.Add("DDA", typeof(string));
            table.Columns.Add("BatchSourceShortName", typeof(string));
            table.Columns.Add("ImportTypeShortName", typeof(string));

            table.Rows.Add(1, 2, 3, "1234", DateTime.Now, "20151111", DateTime.Now, 1, 2, 3, 4, 5, 6, "s", "a", 200.22,
                200.22, 1, "s", 1, 2, "a", "s", "8708", "iii", 1, 2, "a", "b", "c");
            dt = table;
            TotalRecords = 1;
	        FilteredRecords = 0;
            return true;
        }

        public bool GetBatchSummary(int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, 
            int? paymentTypeID, int? paymentSourceID, bool DisplayScannedCheckOnline, int start, int length, 
            string orderby, string orderdirection, string search, out int recordstotal, out int recordsfiltered, out DataTable dt, out int transactionRecords, out int checkRecords,
            out int documentRecords, out decimal checkTotal)
        {
            var output = new DataTable();
            output.Columns.AddRange(new[]
            {
                    new DataColumn("BatchID", typeof (long)),
                    new DataColumn("SourceBatchID", typeof (long)),
                    new DataColumn("CheckAmount", typeof (long)),
                    new DataColumn("Deposit_Date", typeof (DateTime)),
                    new DataColumn("DepositDate", typeof (string)),
                    new DataColumn("TransactionCount", typeof (int)),
                    new DataColumn("CheckCount", typeof (int)),
                    new DataColumn("DocumentCount", typeof (int)),
                    new DataColumn("BatchSiteCode", typeof (int)),
                    new DataColumn("BatchCueID", typeof (int)),
                    new DataColumn("BatchNumber", typeof (string)),
                    new DataColumn("PaymentType", typeof (string)),
                    new DataColumn("PaymentSource", typeof (string))
                });
            var dr = output.NewRow();
            dr["BatchID"] = 1010L;
            dr["SourceBatchID"] = 1010L;
            dr["CheckAmount"] = 1010L;
            dr["Deposit_Date"] = new DateTime(2015, 01, 01);
            dr["DepositDate"] = "01/01/2015";
            dr["TransactionCount"] = 5;
            dr["CheckCount"] = 5;
            dr["DocumentCount"] = 5;
            dr["BatchSiteCode"] = 1;
            dr["BatchCueID"] = 1;
            dr["BatchNumber"] = "1010";
            dr["PaymentType"] = "ACH";
            dr["PaymentSource"] = "ACH";
            output.Rows.Add(dr);
            dt = output;
            recordstotal = 1;
            recordsfiltered = 1;
            transactionRecords = 5;
            checkRecords = 5;
            documentRecords = 5;
            checkTotal = 100.00M;
            return true;
        }
    }
}
