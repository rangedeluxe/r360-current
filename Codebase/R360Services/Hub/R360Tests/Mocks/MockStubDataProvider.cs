﻿using System;
using System.Collections.Generic;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI;

namespace R360Tests.Mocks
{
    public class MockStubDataProvider : IStubsDataProvider
    {
        private readonly List<PostDepositStubDetailDto> _stubDtos = new List<PostDepositStubDetailDto>();

        public void AddStub(PostDepositStubDetailDto stubDto)
        {
            _stubDtos.Add(stubDto);
        }

        public IEnumerable<PostDepositStubDetailDto> GetStubsForTransaction(int bankid, int workgroupid, long batchid,
            DateTime depositdate, int transactionid)
        {
            return _stubDtos;
        }
    }
}
