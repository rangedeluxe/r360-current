﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO.AdvancedSearch;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockAdvancedSearchDataProvider : IAdvancedSearchDataProvider
    {
        private const string AS_RESULTS_TOTAL = "AdvancedSearch_Results_Totals.xml";
        private XmlDocument _totals;
        private MockAdvancedSearchResults _searchResultsData;

        private void CreateSearchResults()
        {

            _searchResultsData = new MockAdvancedSearchResults();
            _searchResultsData.AddResultRow();
        }

        public MockAdvancedSearchDataProvider(XmlDocument reponseDocument,MockAdvancedSearchResults searchResultsData = null)
        {
            _totals = reponseDocument;
            if (searchResultsData == null)
                CreateSearchResults();
            else _searchResultsData = searchResultsData;
        }

        public cOLLockboxes GetAuthorizedWorkgroups()
        {
            var output = new cOLLockboxes();
            output.Add(1, new cOLLockbox()
            {
                BankID = 1,
                LockboxID = 2,
                MaximumSearchDays = 2,
                OnlineViewingDays = 3,
                LongName = "Workgroup Name"
            });
            return output;
        }

        public int GetMaxRowsForDownload()
        {
            return 10;
        }

        public string GetPaymentSourceName(int id)
        {
            return "PaymentSource";
        }

        public string GetPaymentTypeName(int id)
        {
            return "PaymentType";
        }
        public void LogActivityInBackground(int bankId, int workgroupId, ActivityCodes activityCode, int itemCount,
            string moduleName)
        {
            throw new NotImplementedException();
        }

        public bool LockboxSearch(TableParams tables, out XmlDocument docSearchResultsTotals, out DataTable dtSearchResults)
        {
            docSearchResultsTotals = _totals;
            dtSearchResults = _searchResultsData.SearchResults;
            return true;
        }

    }
}
