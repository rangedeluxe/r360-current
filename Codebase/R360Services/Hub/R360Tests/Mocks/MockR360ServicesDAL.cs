﻿using System;
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace R360Tests.Mocks
{
    public class MockR360ServicesDAL : IR360ServicesDAL
    {
        public bool DeleteOLUserPreference(int UserID, out int RowsReturned)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public bool GetActiveWorkgroups(Guid sessionID, EntityDTO entities, out List<WorkgroupNameDTO> workgroups)
        {
            throw new NotImplementedException();
        }

        public bool GetAuditUsers(out List<AuditUserDTO> users)
        {
            throw new NotImplementedException();
        }

        public bool GetBankName(int siteBankId, out string bankName)
        {
            bankName = "Real bank name for real people.";
            return true;
        }

        public bool GetClientAccounts(int UserID, Guid OLOrganizationID, out DataTable results)
        {
            throw new NotImplementedException();
        }

        public bool GetClientAccountSummary(Guid SessionID, int UserID, ReceivablesSummaryDBRequestDto request, out DataTable results)
        {
            throw new NotImplementedException();
        }

        public bool GetDDASummaryData(Guid sessionID, DateTime depositDate, out List<DDASummaryDataDto> summaryData)
        {
            throw new NotImplementedException();
        }

        public bool GetIntegraPayOnlyWorkgroups(Guid sessionID, EntityDTO entities, out List<WorkgroupNameDTO> workgroups)
        {
            throw new NotImplementedException();
        }

        public bool GetOLUserPreferenceCount(Guid OLPreferenceID, int UserID, out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public bool GetOLUserPreferenceIDByName(string PreferenceName, out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public bool GetOLUserPreferences(int UserID, out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public bool GetSourceBatchID(long batchID, out long result)
        {
            throw new NotImplementedException();
        }
        public bool GetUser(int UserID, out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public bool GetUserIDBySID(Guid SID, out DataTable results)
        {
            results = new DataTable();
            results.Columns.Add("UserID");
            var row = results.NewRow();
            row["UserID"] = 1;
            results.Rows.Add(row);
            return true;
        }

        public bool InsertOLUserPreference(Guid OLPreferenceID, int UserID, string UserSetting, out int RowsReturned)
        {
            throw new NotImplementedException();
        }

        public bool UpdateOLUserPreference(Guid OLPreferenceID, int UserID, string UserSetting, out int RowsReturned)
        {
            throw new NotImplementedException();
        }

        public bool WriteAuditEvent(int userID, string eventName, string eventType, string applicationName, string description)
        {
            return true;
        }
    }
}
