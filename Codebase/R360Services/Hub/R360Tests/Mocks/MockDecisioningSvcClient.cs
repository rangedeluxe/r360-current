﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.RecHub.OLDecisioningServicesClient;
using WFS.RecHub.R360Shared;

namespace R360Tests.Mocks
{
    public class MockDecisioningSvcClient : IDecisioningSvcClient
    {
        private readonly IServiceContext _context;
        public const int BATCHID_NONEXPIRED = 0;
        public const int BATCHID_EXPIRED = 1;

        public MockDecisioningSvcClient(IServiceContext context)
        {
            _context = context;
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            throw new NotImplementedException();
        }

        public PendingBatchesResponse GetAll()
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            var nonexpired = new DecisioningBatchDto()
            {
                BankId = 0,
                WorkgroupId = 0,
                MinutesUntilDeadLine = 1,
                SID = _context.GetSID(),
                GlobalBatchId = globalBatchId,
                Transactions = new List<DecisioningTransactionDto>()
                {
                    new DecisioningTransactionDto()
                    {
                        GlobalBatchId = globalBatchId
                    }
                }
            };

            var expired = new DecisioningBatchDto()
            {
                BankId = 0,
                WorkgroupId = 0,
                MinutesUntilDeadLine = -1,
                SID = _context.GetSID(),
                GlobalBatchId = globalBatchId,
                Transactions = new List<DecisioningTransactionDto>()
                {
                    new DecisioningTransactionDto()
                    {
                        GlobalBatchId = globalBatchId
                    }
                }
            };

            return new OLDBaseGenericResponse<DecisioningBatchDto>()
            {
                Status = WFS.RecHub.R360Shared.StatusCode.SUCCESS,
                Data = globalBatchId == BATCHID_NONEXPIRED ? nonexpired : expired
            };
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            throw new NotImplementedException();
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<WFS.RecHub.OlfRequestCommon.InProcessExceptionImageRequestDto> GetBatchImageDetails(int globalBatchId, int transactionId)
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<string> Ping()
        {
            throw new NotImplementedException();
        }

        public OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId)
        {
            throw new NotImplementedException();
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            return new UpdateTransactionResponse()
            {
                Status = StatusCode.SUCCESS,
                Data = true
            };
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            var nonexpired = new DecisioningBatchDto()
            {
                BankId = 0,
                WorkgroupId = 0,
                MinutesUntilDeadLine = 1,
                SID = _context.GetSID(),
                GlobalBatchId = globalBatchId,
                Transactions = new List<DecisioningTransactionDto>()
                {
                    new DecisioningTransactionDto()
                    {
                        GlobalBatchId = globalBatchId
                    }
                }
            };

            var expired = new DecisioningBatchDto()
            {
                BankId = 0,
                WorkgroupId = 0,
                MinutesUntilDeadLine = -1,
                SID = _context.GetSID(),
                GlobalBatchId = globalBatchId,
                Transactions = new List<DecisioningTransactionDto>()
                {
                    new DecisioningTransactionDto()
                    {
                        GlobalBatchId = globalBatchId
                    }
                }
            };

            return new OLDBaseGenericResponse<DecisioningBatchDto>()
            {
                Status = WFS.RecHub.R360Shared.StatusCode.SUCCESS,
                Data = globalBatchId == BATCHID_NONEXPIRED ? nonexpired : expired
            };
        }
    }
}
