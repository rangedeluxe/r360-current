﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockPreferencesProvider : IPreferencesProvider
    {
        public List<cOLPreference> Preferences { get; set; }

        public MockPreferencesProvider()
        {
            Preferences = new List<cOLPreference>();
        }

        public MockPreferencesProvider(List<cOLPreference> preferences)
        {
            Preferences = preferences;
        }
        public cOLPreference GetPreferenceByName(string name)
        {
            return Preferences.FirstOrDefault(x => x.PreferenceName == name);
        }

        public IEnumerable<cOLPreference> GetPreferences()
        {
            return Preferences;
        }
    }
}
