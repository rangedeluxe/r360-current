﻿using System;
using System.Collections.Generic;
using System.Xml;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using System.Linq;

namespace R360Tests.Mocks
{
    public class MockAdvancedSearchStoredQueryDataProvider : IAdvancedSearchStoredQueryDataProvider
    {
        private Guid id = Guid.Parse("13d35bd2-72a2-44ad-9bfd-b2890d0ca892");

        public bool Delete(Guid id)
        {
            return true;
        }

        public AdvancedSearchStoredQueryDto Get(Guid id)
        {
            return GetAll().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<AdvancedSearchStoredQueryDto> GetAll()
        {
            return new List<AdvancedSearchStoredQueryDto>()
            {
                new AdvancedSearchStoredQueryDto()
                {
                    Description = "Description",
                    Id = id,
                    IsDefault = true,
                    Name = "Name"
                }
            };
        }

        public int GetMaxQueries()
        {
            return 10;
        }

        public bool Save(AdvancedSearchStoredQueryDto item, XmlDocument itemxml)
        {
            return true;
        }
    }
}
