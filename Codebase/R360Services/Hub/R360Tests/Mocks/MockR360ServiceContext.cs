﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace R360Tests.Mocks
{
    public class MockR360ServiceContext : IServiceContext
    {
        public SimpleClaim[] RAAMClaims { get; set; }

        public string SiteKey { get; set; }

        public Guid SessionID { get; set; }

        public Guid SID { get; set; }

        public Guid GetSessionID()
        {
            return SessionID;
        }

        public Guid GetSID()
        {
            return SID;
        }

        public string GetLoginNameForAuditing()
        {
            return "TestName";
        }
    }
}
