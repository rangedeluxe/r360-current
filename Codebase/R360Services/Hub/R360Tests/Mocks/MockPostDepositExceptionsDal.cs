﻿using System;
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO;

namespace R360Tests.Mocks
{
    public class MockPostDepositExceptionsDal : IPostDepositExceptionsDal
    {
        private DataTable _transactionsTable;

        public PostDepositTransactionSaveSqlDto SavedTransaction { get; private set; }

        public void SetTransactions(IEnumerable<PostDepositTransactionResponseDto> dtos)
        {
            _transactionsTable = new DataTable();
            _transactionsTable.Columns.Add("EntityID", typeof(int));
            _transactionsTable.Columns.Add("SiteBankID", typeof(int));
            _transactionsTable.Columns.Add("Workgroup", typeof(string));
            _transactionsTable.Columns.Add("SiteClientAccountID", typeof(int));
            _transactionsTable.Columns.Add("BatchID", typeof(long));

            foreach (var dto in dtos)
            {
                var row = _transactionsTable.NewRow();
                row["EntityID"] = dto.EntityId;
                row["SiteBankID"] = dto.BankId;
                row["SiteClientAccountID"] = dto.WorkgroupId;
                row["Workgroup"] = $"{dto.WorkgroupId} - Mock Workgroup";
                row["BatchID"] = dto.BatchId;
                _transactionsTable.Rows.Add(row);
            }
        }

        public void Dispose()
        {
        }
        public bool DeleteTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, string userName, Guid SID)
        {
            throw new NotImplementedException();
        }
        public bool GetPendingTransactions(PostDepositPendingTransactionsRequestSqlDto request, out int totalrecords,
            out int filteredrecords, out DataTable dt)
        {
            dt = new DataTable();
            dt.Columns.Add("TransactionID", typeof(int));
            var row = dt.NewRow();
            row["TransactionID"] = 1;
            dt.Rows.Add(row);
            totalrecords = 1;
            filteredrecords = 1;
            return true;
        }
        public bool GetPostDepositDataEntryDetails(PostDepositDataEntryDetailsRequestSqlDto request, out DataTable dataTable)
        {
            throw new NotImplementedException();
        }
        public bool GetTransaction(PostDepositTransactionRequestSqlDto request, out DataTable dt)
        {
            dt = _transactionsTable;
            return dt != null;
        }
        public bool GetTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, out DataTable dt)
        {
            throw new NotImplementedException();
        }
        public bool InsertTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, string userName, Guid SID)
        {
            throw new NotImplementedException();
        }
        public bool SaveTransaction(PostDepositTransactionSaveSqlDto request)
        {
            SavedTransaction = request;
            return true;
        }
        public IEnumerable<DataEntrySetupDto> GetDataEntrySetupFields(Guid sessionId, int bankid,
            int workgroupid, long batchid, DateTime depositdate)
        {
            throw new NotImplementedException();
        }

        public bool DeleteStub(long batchid, DateTime depositdate, int transactionid, int batchsequence)
        {
            return true;
        }

        public bool CompleteTransaction(long batchId, DateTime depositDate, int transactionId, string userName)
        {
            return true;
        }

        public bool AuditTransactionDetailView(long batchId, DateTime depositDate, int transactionId, string userName, Guid SID)
        {
            throw new NotImplementedException();
        }

        public bool InsertStubs(long batchid, DateTime depositdate, int transactionid, Guid sessionid, IEnumerable<PostDepositStubSaveSqlDto> stubs)
        {
            throw new NotImplementedException();
        }

        public bool SavePayer(PostDepositSavePayerSqlDto request)
        {
            return true;
        }

        public bool SavePaymentsRemitters(PostDepositTransactionSaveSqlDto request)
        {
            throw new NotImplementedException();
        }
    }
}
