﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DAL;

namespace R360Tests.Mocks
{
    public class MockSessionDAL : ISessionDAL
    {
        public bool EndSession(Guid SessionID, out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public bool GetNextPageCounter(Guid SessionID, out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public bool GetSession(Guid SessionID, out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public bool GetSession(Guid sessionID, int userType, out DataTable dt)
        {
            throw new NotImplementedException();
        }

        public IReadOnlyList<BreadCrumb> GetSessionBreadCrumbs(Guid SessionID)
        {
            throw new NotImplementedException();
        }

        public bool InsertSessionActivityLog(Guid SessionID, string ModuleName, WFS.RecHub.Common.ActivityCodes ActivityCode, out int RowsAffected)
        {
            RowsAffected = 1;
            return true;
        }

        public bool InsertSessionActivityLog(Guid SessionID, string ModuleName, WFS.RecHub.Common.ActivityCodes ActivityCode, out int RowsAffected, out long ActivityID)
        {
            throw new NotImplementedException();
        }

        public bool InsertSessionActivityLog(Guid SessionID, string ModuleName, WFS.RecHub.Common.ActivityCodes ActivityCode, Guid OnlineImageQueueID, int itemCount, int BankID, int ClientAccountID, out int RowsReturned)
        {
            throw new NotImplementedException();
        }

        public bool InsertSessionLog(Guid SessionID, int NextPage, string PageTitle, string ScriptName, string QueryString, out int RowsAffected)
        {
            throw new NotImplementedException();
        }

        public bool SetWebDeliveredFlag(Guid OnlineImageQueueID, bool InternetDelivered)
        {
            throw new NotImplementedException();
        }

        public bool UpdateActivityLockbox(long ActivityLogID, int BankID, int ClientAccountID, out int RowsAffected)
        {
            throw new NotImplementedException();
        }

        public bool UpdateSession(int NextPage, Guid SessionID, out int RowsAffected)
        {
            throw new NotImplementedException();
        }
    }
}
