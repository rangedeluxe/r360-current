﻿using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace R360Tests.Mocks
{
    public class MockRuleProvider : IRuleProvider
    {
        private readonly Rules _rules = new Rules();

        public void Add(DataEntryFieldRule rule)
        {
            _rules.DataEntryFieldRules.Add(rule);
        }
        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey)
        {
            return _rules;
        }
    }
}