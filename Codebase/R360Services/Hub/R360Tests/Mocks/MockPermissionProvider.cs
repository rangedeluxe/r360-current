﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Shared;

namespace R360Tests.Mocks
{
    public class MockPermissionProvider : IPermissionProvider
    {
        public List<PermissionDto> ProvidedPermissions { get; set; }

        public MockPermissionProvider()
        {
            ProvidedPermissions = new List<PermissionDto>();
        }

        /// <summary>
        /// Returns true if no permissions are added to the provided list.
        /// </summary>
        /// <param name="permission"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool HasPermission(string permission, R360Permissions.ActionType type)
        {
            var perm = ProvidedPermissions.FirstOrDefault(x => x.Permission == permission);
            if (perm == null)
                return true;
            return perm.HasPermission;
        }
    }
}
