﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R360Tests.Mocks
{
    public class MockAdvancedSearchResults
    {
        public DataTable SearchResults { get; } = new DataTable();

        private readonly Dictionary<string, dynamic> _standardColumnValuesRow1 = new Dictionary<string, dynamic>()
        {
            { "BankID", 1 },
            { "ClientAccountID", 2},
            { "Deposit_Date", "12/03/2015"},
            { "PaymentSource", "WireSource"},
            { "PaymentType", "WireType"},
            { "BatchID", 3},
            { "SourceBatchID", 4},
            { "BatchNumber", 5},
            { "PICSDate", 20151203},
            { "TransactionID", 6},
            { "TxnSequence", 7},
            { "Amount", 10},
            { "SerialNumber", "001201"},
            { "ChecksTransactionCode", "001001"},
            { "numeric_serial", 1201},
            { "AccountNumber", "002002"},
            { "RT", "00102"},
            { "numeric_rt", 102},
            { "DDA", "123456711"},
            { "BatchSourceShortName", "Wire"},
            { "ImportTypeShortName", "DIT"},
            { "Payer", "FRED FLINTSTONE"},
            { "BatchSequence", 1},
            { "CheckCount", 1},
            { "DocumentCount", 1},
            { "StubCount", 0},
            { "MarkSenseDocumentCount", 1},
            { "ChecksCheckSequence", 1}
        };

        private readonly Dictionary<string, dynamic> _standardColumnValuesRow2 = new Dictionary<string, dynamic>()
        {
            { "BankID", 1 },
            { "ClientAccountID", 2},
            { "Deposit_Date", "12/03/2015"},
            { "PaymentSource", "WireSource"},
            { "PaymentType", "WireType"},
            { "BatchID", 3},
            { "SourceBatchID", 4},
            { "BatchNumber", 5},
            { "PICSDate", 20151203},
            { "TransactionID", 6},
            { "TxnSequence", 7},
            { "Amount", 10},
            { "SerialNumber", "001201"},
            { "ChecksTransactionCode", "001001"},
            { "numeric_serial", 1201},
            { "AccountNumber", "002002"},
            { "RT", "00102"},
            { "numeric_rt", 102},
            { "DDA", "123456711"},
            { "BatchSourceShortName", "Wire"},
            { "ImportTypeShortName", "DIT"},
            { "Payer", "FRANK UNDERWOOD"},
            { "BatchSequence", 2},
            { "CheckCount", 1},
            { "DocumentCount", 1},
            { "StubCount", 0},
            { "MarkSenseDocumentCount", 0},
            { "ChecksCheckSequence", 1}
        };

        private void AddColumn(DataRow row, KeyValuePair<string,dynamic> column)
        {
            //Make sure the column is not really there from some other process and add if not there
            if (!SearchResults.Columns.Contains(column.Key))
                SearchResults.Columns.Add(column.Key);

            //Add the value
            row[column.Key] = column.Value;
        }

        public void AddResultRow()
        {
            AddResultRow(_standardColumnValuesRow1);
            AddResultRow(_standardColumnValuesRow2);
        }

        public void AddResultRow(Dictionary<string, dynamic> rowData)
        {
            var row = SearchResults.NewRow();
            //first ensure all the standard fields are there with default data
            foreach (var column in _standardColumnValuesRow1)
            {
                AddColumn(row, column);
            }
            //now populate with requested values, replacing standard values as needed
            foreach (var column in rowData)
            {
                AddColumn(row, column);
            }
            SearchResults.Rows.Add(row);
        }
    }
}
