﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace R360Tests.Mocks
{
    public class MockPaymentDataProvider : IPaymentDataProvider
    {
        public IEnumerable<PaymentDto> GetPaymentsForTransaction(int bankid, int workgroupid, long batchid, DateTime depositdate, int transactionid)
        {
            return new List<PaymentDto>()
            {
                new PaymentDto()
            };
        }
    }
}
