﻿using WFS.RecHub.PostDepositBusinessRules.RuleProvider;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace R360Tests.Mocks
{
    class MockDatabaseRuleProvider : IRuleProvider
    {
        public int LookupCount => _lookupCount;

        private int _lookupCount = 0;
        private Rules _rules;
        public MockDatabaseRuleProvider(Rules rules)
        {
            _rules = rules;
        }
        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey)
        {
            _lookupCount++;
            return _rules;
        }
    }
}