﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;
using WFS.RecHub.R360Shared;

namespace R360Tests
{
    [TestClass]
    public class InvoiceSearchTests
    {
        private readonly InvoiceSearchRepository _repo;
        private readonly MockPermissionProvider _permissions;
        private readonly MockPreferencesProvider _preferences;

        public InvoiceSearchTests()
        {
            _permissions = new MockPermissionProvider();
            _preferences = new MockPreferencesProvider();
            _repo = new InvoiceSearchRepository(
                new InvoiceSearchRequestXmlTransformer(),
                new InvoiceSearchResponseXmlTransformer(),
                new MockInvoiceSearchDataProvider(),
                new MockServiceContext(),
                new MockDisplayBatchIdProvider(),
                new MockImageService(),
                _permissions,
                _preferences
            );
        }

        [TestInitialize]
        public void InitTests()
        {
            _preferences.Preferences.Clear();
            _preferences.Preferences.Add(new WFS.RecHub.Common.cOLPreference()
            {
                PreferenceName = "MaximumWorkgroups",
                DefaultSetting = "5"
            });
        }

        [TestMethod]
        public void InvoiceSearch_ShouldComplete_WithBasicData()
        {
            // Arrange
            var req = new InvoiceSearchRequestDto()
            {
                Workgroup = "2",
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now,
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection"
            };

            // Act
            var result = _repo.Get(req);

            // Assert
            Assert.AreEqual(WFS.RecHub.R360Shared.StatusCode.SUCCESS, result.Status);
        }

        [TestMethod]
        public void InvoiceSearch_ShouldReturn_BasicMetadata()
        {
            // Arrange
            var req = new InvoiceSearchRequestDto()
            {
                Workgroup = "2",
                DateFrom = DateTime.Now,
                DateTo = DateTime.Now,
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection"
            };

            // Act
            var result = _repo.Get(req);

            // Assert
            Assert.AreEqual(WFS.RecHub.R360Shared.StatusCode.SUCCESS, result.Status);
            Assert.IsNotNull(result.Metadata);
        }

        [TestMethod]
        public void InvoiceSearch_ShouldFail_WhenMaxSearchDaysAreOverLimit()
        {
            // Arrange
            var req = new InvoiceSearchRequestDto()
            {
                Workgroup = "1|2",
                DateFrom = DateTime.Now.AddDays(-3),
                DateTo = DateTime.Now,
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection"
            };
            _permissions.ProvidedPermissions.Add(new PermissionDto() { Permission = R360Permissions.Perm_DepositSearchSpan, HasPermission = false });

            // Act
            var result = _repo.Get(req);

            // Assert
            Assert.AreEqual(WFS.RecHub.R360Shared.StatusCode.FAIL, result.Status);
        }

        [TestMethod]
        public void InvoiceSearch_ShouldComplete_WhenMaxSearchDaysAreWithinLimit()
        {
            // Arrange
            var req = new InvoiceSearchRequestDto()
            {
                Workgroup = "2",
                DateFrom = DateTime.Now.AddDays(-2),
                DateTo = DateTime.Now,
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection"
            };
            _permissions.ProvidedPermissions.Add(new PermissionDto() { Permission = R360Permissions.Perm_DepositSearchSpan, HasPermission = false });

            // Act
            var result = _repo.Get(req);

            // Assert
            Assert.AreEqual(WFS.RecHub.R360Shared.StatusCode.SUCCESS, result.Status);
        }

        [TestMethod]
        public void InvoiceSearch_ShouldComplete_WhenMaxSearchDaysAreOverLimit_AndOverridePermissionIsEnabled()
        {
            // Arrange
            var req = new InvoiceSearchRequestDto()
            {
                Workgroup = "2",
                DateFrom = DateTime.Now.AddDays(-3),
                DateTo = DateTime.Now,
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection"
            };

            // Act
            var result = _repo.Get(req);

            // Assert
            Assert.AreEqual(WFS.RecHub.R360Shared.StatusCode.SUCCESS, result.Status);
        }

        [TestMethod]
        public void InvoiceSearch_ShouldFail_WhenMaxWorkgroupsIsExceeded()
        {
            // Arrange
            var req = new InvoiceSearchRequestDto()
            {
                Workgroup = "1|2",
                DateFrom = DateTime.Now.AddDays(-3),
                DateTo = DateTime.Now,
                OrderBy = "OrderBy",
                OrderDirection = "OrderDirection"
            };
            _preferences.Preferences.First().DefaultSetting = "1";

            // Act
            var result = _repo.Get(req);

            // Assert
            Assert.AreEqual(WFS.RecHub.R360Shared.StatusCode.FAIL, result.Status);
        }
    }
}
