﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;

namespace R360Tests
{
    [TestClass]
    public class ExceptionsTests
    {
        [TestMethod]
        public void UpdateTransaction_WillSucceed_ForNonExpiredDeadline()
        {
            // arrange
            var context = new MockServiceContext();
            var serv = new ExceptionsAPI(context, new MockDecisioningSvcClient(context), new MockR360ServicesDAL(), 
                new MockSessionDAL(), new MockPermissionProvider(), new MockUserServices(), new MockRaamClient());
            var batch = serv.GetBatchDetails(MockDecisioningSvcClient.BATCHID_NONEXPIRED);

            // act
            var res = serv.UpdateTransaction(batch.Data.Transactions.First());

            // assert
            Assert.AreEqual(WFS.RecHub.R360Shared.StatusCode.SUCCESS, res.Status);
        }

        [TestMethod]
        public void UpdateTransaction_WillFail_ForExpiredDeadline()
        {
            // arrange
            var context = new MockServiceContext();
            var serv = new ExceptionsAPI(context, new MockDecisioningSvcClient(context), new MockR360ServicesDAL(), 
                new MockSessionDAL(), new MockPermissionProvider(), new MockUserServices(), new MockRaamClient());
            var batch = serv.GetBatchDetails(MockDecisioningSvcClient.BATCHID_EXPIRED);

            // act
            var res = serv.UpdateTransaction(batch.Data.Transactions.First());

            // assert
            Assert.AreEqual(WFS.RecHub.R360Shared.StatusCode.FAIL, res.Status);
        }
    }
}
