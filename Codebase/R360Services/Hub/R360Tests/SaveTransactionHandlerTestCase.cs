﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.PostDepositBusinessRules;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.PostDepositExceptions.Handlers;
using WFS.RecHub.R360Shared;

namespace R360Tests
{
    [TestClass]
    public class SaveTransactionHandlerTestCase :
        SaveTransactionHandlerTestCase.IFluentSaveAssertion,
        SaveTransactionHandlerTestCase.IFluentStatusAssertion,
        SaveTransactionHandlerTestCase.IFluentDataTypeErrorAssertion,
        SaveTransactionHandlerTestCase.IFluentBusinessRuleExceptionAssertion
    {
        #region Fluent assertions

        protected interface IFluentSaveAssertion
        {
            IFluentStatusAssertion NoSave();
            IFluentStatusAssertion SaveWasCalledForFields(params PostDepositDataEntryFieldSaveSqlDto[] expectedFields);
        }

        IFluentStatusAssertion IFluentSaveAssertion.NoSave()
        {
            Assert.IsNull(ExceptionsDal.SavedTransaction, "SavedTransaction");

            return this;
        }
        IFluentStatusAssertion IFluentSaveAssertion.SaveWasCalledForFields(
            params PostDepositDataEntryFieldSaveSqlDto[] expectedFields)
        {
            Assert.IsNotNull(ExceptionsDal.SavedTransaction, "Expected Save to have been called");

            var expectedStrings = expectedFields.Select(SavedFieldToString);
            var actualStrings =
                from stub in ExceptionsDal.SavedTransaction.DataEntryFields
                from dataEntryField in stub.DataEntryFields
                select SavedFieldToString(dataEntryField);

            Assert.AreEqual(string.Join("\r\n", expectedStrings), string.Join("\r\n", actualStrings));

            return this;
        }

        protected interface IFluentStatusAssertion
        {
            IFluentDataTypeErrorAssertion StatusWasSuccess();
            IFluentDataTypeErrorAssertion StatusWasFailureDueToTypeError();
        }

        IFluentDataTypeErrorAssertion IFluentStatusAssertion.StatusWasSuccess()
        {
            Assert.AreEqual("", ResponseErrors, "Errors");
            Assert.AreEqual(StatusCode.SUCCESS, Response.Status, "Status");
            return this;
        }
        IFluentDataTypeErrorAssertion IFluentStatusAssertion.StatusWasFailureDueToTypeError()
        {
            Assert.AreEqual(Messages.TypeErrorPopUpMessage, ResponseErrors, "Errors");
            Assert.AreEqual(StatusCode.FAIL, Response.Status, "Status");
            return this;
        }

        protected interface IFluentDataTypeErrorAssertion
        {
            IFluentBusinessRuleExceptionAssertion NoDataTypeErrors();
            IFluentBusinessRuleExceptionAssertion DataTypeErrors(
                params Tuple<DataEntrySetupDto, string>[] fieldsAndExpectedErrors);
        }

        IFluentBusinessRuleExceptionAssertion IFluentDataTypeErrorAssertion.NoDataTypeErrors()
        {
            AssertDataTypeErrors(new Tuple<DataEntrySetupDto, string>[] { });
            return this;
        }
        IFluentBusinessRuleExceptionAssertion IFluentDataTypeErrorAssertion.DataTypeErrors(
            params Tuple<DataEntrySetupDto, string>[] fieldsAndExpectedErrors)
        {
            AssertDataTypeErrors(fieldsAndExpectedErrors);
            return this;
        }
        private void AssertDataTypeErrors(IEnumerable<Tuple<DataEntrySetupDto, string>> fieldsAndExpectedErrors)
        {
            var expectedFieldNames = string.Join(", ",
                fieldsAndExpectedErrors.Select(f => $"{f.Item1.FieldName}: '{f.Item2}'"));
            var actualFieldNames = string.Join(", ",
                Response.Data.DataTypeErrors.Select(e => $"{e.FieldName}: '{e.Error}'"));
            Assert.AreEqual(expectedFieldNames, actualFieldNames, "Fields with data-type errors");
        }

        protected interface IFluentBusinessRuleExceptionAssertion
        {
            void NoBusinessRuleExceptions();
            void BusinessRuleExceptions(params Tuple<DataEntrySetupDto, string>[] fieldsAndExpectedExceptionMessages);
        }

        void IFluentBusinessRuleExceptionAssertion.NoBusinessRuleExceptions()
        {
            AssertBusinessRuleExceptions(new Tuple<DataEntrySetupDto, string>[] { });
        }
        void IFluentBusinessRuleExceptionAssertion.BusinessRuleExceptions(
            params Tuple<DataEntrySetupDto, string>[] fieldsAndExpectedExceptionMessages)
        {
            AssertBusinessRuleExceptions(fieldsAndExpectedExceptionMessages);
        }
        private void AssertBusinessRuleExceptions(
            IEnumerable<Tuple<DataEntrySetupDto, string>> fieldsAndExpectedExceptionMessages)
        {
            var expected = string.Join(", ",
                fieldsAndExpectedExceptionMessages.Select(f => $"{f.Item1.FieldName}: '{f.Item2}'"));
            var actual = string.Join(", ",
                Response.Data.BusinessRuleExceptions.Select(e => $"{e.FieldName}: '{e.Error}'"));
            Assert.AreEqual(expected, actual, "Fields with business-rule exceptions");
        }

        #endregion

        /// <summary>
        /// These messages are part of their stories' acceptance criteria. So we won't use the same constants as the
        /// production code; instead we'll copy the constants here, so the tests can catch any unintended changes.
        /// </summary>
        protected static class Messages
        {
            public const string BusinessRuleException_FieldIsRequired = "Field is required";
            public const string StubError_BadNumericValue = "Numeric field";
            public const string StubError_BadDateValue = "Date field";
            public const string StubError_BadCurrencyValue = "Currency field";
            public const string StubError_BadCurrencyValue_BeyondMax = "Maximum currency value exceeded";
            public const string StubError_BadCurrencyValue_BeyondMin = "Minimum currency value exceeded";
            public const string TypeErrorPopUpMessage =
                "Please address the invalid data below before navigating to another transaction.";
        }

        private const int DefaultBankId = 1;
        private const int DefaultWorkgroupId = 123;
        protected const decimal MaxCurrencyValue = 900000000000000;

        [TestInitialize]
        public void SetUp()
        {
            Response = null;

            ServiceContext = new MockServiceContext();
            ExceptionsDal = new MockPostDepositExceptionsDal();
            LockboxApi = new MockLockboxAPI();
            PostDepositDataProvider = new MockPostDepositDataProvider();
            StubDataProvider = new MockStubDataProvider();
            DocDataProvider = new MockDocumentDataProvider();
            RuleProvider = new MockRuleProvider();
            SetUpScenario();
        }
        protected virtual void SetUpScenario()
        {
        }

        private MockPostDepositExceptionsDal ExceptionsDal { get; set; }
        private MockLockboxAPI LockboxApi { get; set; }
        private MockPostDepositDataProvider PostDepositDataProvider { get; set; }
        private MockRuleProvider RuleProvider { get; set; }
        private MockStubDataProvider StubDataProvider { get; set; }
        private MockDocumentDataProvider DocDataProvider { get; set; }
        private MockServiceContext ServiceContext { get; set; }
        private BaseGenericResponse<PostDepositTransactionSaveResultDto> Response { get; set; }
        private string ResponseErrors => string.Join("\r\n", Response.Errors);
        protected IFluentSaveAssertion Expect => this;

        protected static PostDepositDataEntryFieldSaveDto CreateFieldSaveRequest(DataEntrySetupDto field, string value)
        {
            return new PostDepositDataEntryFieldSaveDto {FieldName = field.FieldName, Value = value};
        }
        private static PostDepositTransactionSaveDto CreateSaveRequest(params PostDepositDataEntryFieldSaveDto[] fields)
        {
            return new PostDepositTransactionSaveDto
            {
                Stubs = new[]
                {
                    new PostDepositStubSaveDto
                    {
                        DataEntryFields = fields
                    }
                }
            };
        }
        private SaveTransactionHandler CreateHandler()
        {
            return new SaveTransactionHandler(
                ServiceContext,
                ExceptionsDal,
                new MockRaamClient(),
                LockboxApi,
                new MockPaymentDataProvider(),
                StubDataProvider,
                new MockDataEntryDataProvider(),
                PostDepositDataProvider,
                new MockImageProvider(),
                DocDataProvider,
                new BusinessRulesEngine(RuleProvider),
                (message, source) => Assert.Fail($"Received unexpected warning: {message}"),
                new MockR360ServicesDAL(),
                new BusinessRulesEngine(RuleProvider)
            );
        }
        protected DataEntrySetupDto DefineDataEntryField(DataEntryDataType dataType, string fieldName = null,
            bool isRequired = false)
        {
            var field = new DataEntrySetupDto
            {
                FieldName = fieldName ?? $"Field{Guid.NewGuid()}",
                DataType = (int) dataType,
                TableName = "Stubs",
            };
            PostDepositDataProvider.AddDataEntrySetupDto(field);

            if (isRequired)
            {
                RuleProvider.Add(new DataEntryFieldRule
                {
                    FieldName = field.FieldName,
                    IsCheck = false,
                    IsRequired = true,
                });
            }

            return field;
        }
        protected void Execute(params PostDepositDataEntryFieldSaveDto[] fieldSaveRequests)
        {
            var request = CreateSaveRequest(fieldSaveRequests);
            Response = CreateHandler().Execute(request);
        }
        protected void LockToCurrentUser()
        {
            PostDepositDataProvider.LockStatus = MockPostDepositDataProvider.LockReturnStatus.Locked;
            PostDepositDataProvider.LockedByUser = ServiceContext.GetSID();
        }
        protected PostDepositDataEntryFieldSaveSqlDto SavedField(DataEntrySetupDto field, string value,
            float? valueFloat = null, decimal? valueMoney = null, DateTime? valueDateTime = null)
        {
            return new PostDepositDataEntryFieldSaveSqlDto
            {
                FieldName = field.FieldName,
                Value = value,
                ValueFloat = valueFloat,
                ValueMoney = valueMoney,
                ValueDateTime = valueDateTime,
            };
        }
        private static string SavedFieldToString(PostDepositDataEntryFieldSaveSqlDto savedField)
        {
            var result = $"{savedField.FieldName}=";
            if (savedField.Value != null)
                result += $"'{savedField.Value}'";
            else
                result += "null";

            if (savedField.ValueFloat != null)
                result += $" float:{savedField.ValueFloat}";
            if (savedField.ValueMoney != null)
                result += $" money:{savedField.ValueMoney}";
            if (savedField.ValueDateTime != null)
                result += $" date:{savedField.ValueDateTime}";

            return result;
        }
        protected void SetUpDefaultTransactionAndStub()
        {
            // Our "get data-entry field values" logic would need to be more complicated if we wanted to
            // differentiate between multiple workgroups / transactions / stubs.

            ExceptionsDal.SetTransactions(new[]
            {
                new PostDepositTransactionResponseDto
                {
                    BankId = DefaultBankId,
                    WorkgroupId = DefaultWorkgroupId,
                }
            });

            StubDataProvider.AddStub(new PostDepositStubDetailDto());
        }
        protected void SetUpDefaultWorkgroup()
        {
            LockboxApi.SetOLLockboxes(new List<cOLLockbox>
            {
                new cOLLockbox {BankID = DefaultBankId, LockboxID = DefaultWorkgroupId}
            });
        }
        protected string StringifyInvariant(IFormattable obj)
        {
            return obj.ToString("", CultureInfo.InvariantCulture);
        }

        //TODO: Test what happens when the transaction isn't locked
        //TODO: Test what happens when the transaction is locked by a different user
    }
}
