﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360Tests.Mocks;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace R360Tests
{
    [TestClass]
    public class CachingRuleProviderTests
    {
        [TestMethod]
        public void PostDepositBusinessRules_CacheRules_DifferentBankID()
        {
            var rules = new Rules();
            //Setup the rule providers 
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var cacheRuleProvider = new CachingRuleProvider(ruleProvider);

            cacheRuleProvider.GetRules(1, 1, 1);
            Assert.AreEqual(1, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentBankID first lookup");
            cacheRuleProvider.GetRules(2, 1, 1);
            Assert.AreEqual(2, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentBankID second lookup");
            cacheRuleProvider.GetRules(2, 1, 1);
            Assert.AreEqual(2, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentBankID second lookup no change");
            cacheRuleProvider.GetRules(3, 1, 1);
            Assert.AreEqual(3, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentBankID third lookup");
        }

        [TestMethod]
        public void PostDepositBusinessRules_CacheRules_DifferentClientAccountID()
        {
            var rules = new Rules();
            //Setup the rule providers 
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var cacheRuleProvider = new CachingRuleProvider(ruleProvider);

            cacheRuleProvider.GetRules(1, 1, 1);
            Assert.AreEqual(1, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentClientAccountID first lookup");
            cacheRuleProvider.GetRules(1, 1, 1);
            Assert.AreEqual(1, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentClientAccountID first lookup no change");
            cacheRuleProvider.GetRules(1, 2, 1);
            Assert.AreEqual(2, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentClientAccountID second lookup");
            cacheRuleProvider.GetRules(1, 3, 1);
            Assert.AreEqual(3, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentClientAccountID third lookup");
        }

        [TestMethod]
        public void PostDepositBusinessRules_CacheRules_DifferentPaymentSource()
        {
            var rules = new Rules();
            //Setup the rule providers 
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var cacheRuleProvider = new CachingRuleProvider(ruleProvider);

            cacheRuleProvider.GetRules(1, 1, 1);
            Assert.AreEqual(1, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentPaymentSource first lookup");
            cacheRuleProvider.GetRules(1, 1, 2);
            Assert.AreEqual(2, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentPaymentSource second lookup");
            cacheRuleProvider.GetRules(1, 1, 3);
            Assert.AreEqual(3, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentPaymentSource third lookup");
            cacheRuleProvider.GetRules(1, 1, 3);
            Assert.AreEqual(3, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_DifferentPaymentSource third lookup no change");
        }

        [TestMethod]
        public void PostDepositBusinessRules_CacheRules_MultiChange()
        {
            var rules = new Rules();
            //Setup the rule providers 
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var cacheRuleProvider = new CachingRuleProvider(ruleProvider);

            cacheRuleProvider.GetRules(1, 1, 1);
            Assert.AreEqual(1, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_MultiChange first lookup");
            cacheRuleProvider.GetRules(2, 1, 2);
            Assert.AreEqual(2, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_MultiChange second lookup");
            cacheRuleProvider.GetRules(2, 2, 4);
            Assert.AreEqual(3, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_MultiChange third lookup");
            cacheRuleProvider.GetRules(2, 2, 4);
            Assert.AreEqual(3, ruleProvider.LookupCount, "PostDepositBusinessRules_CacheRules_MultiChange third lookup no change");
        }
    }
}
