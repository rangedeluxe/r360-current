﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/20/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/20/2014 Created
********************************************************************************/
namespace OLServicesClientTests.Services
{
    //[TestClass]
    public class RemitterClientTests
    {
        public readonly RemitterServiceManager _RemitterServiceManager = new RemitterServiceManager();

        public RemitterClientTests()
        {

        }

        [TestMethod]
        public void TestGetRemitterMaintenancePage()
        {
            //Arange
            Guid LockboxRemitterID = new Guid();

            //Act           
            XmlDocumentResponse response = _RemitterServiceManager.GetRemitterMaintenancePage(LockboxRemitterID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestGetAllRemitters()
        {
            //Arange
            int StartRecord = 0;
            bool ShowAll = true;

            //Act           
            XmlDocumentResponse response = _RemitterServiceManager.GetAllRemitters(StartRecord, ShowAll);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestGetLockboxRemitter()
        {
            //Arange
            string RoutingNumber = "";
            string AccountNumber = "";
            Guid OLLockboxID = new Guid();

            //Act           
            BaseGenericResponse<cRemitter> response = _RemitterServiceManager.GetLockboxRemitter(RoutingNumber, AccountNumber, OLLockboxID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestGetLockboxRemitterByID()
        {
            //Arange
            Guid LockboxRemitterID = new Guid();

            //Act           
            BaseGenericResponse<cRemitter> response = _RemitterServiceManager.GetLockboxRemitterByID(LockboxRemitterID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestLockboxRemitterExists()
        {
            //Arange
            string RoutingNumber = "";
            string AccountNumber = "";
            Guid OLLockboxID = new Guid();

            //Act           
            BaseResponse response = _RemitterServiceManager.LockboxRemitterExists(RoutingNumber, AccountNumber, OLLockboxID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestUpdateChecksRemitter()
        {
            //Arange
            string LockboxRemitterName = "";
            int BankID = 0;
            int LockboxID = 0;
            int BatchID = 0;
            DateTime DepositDate = new DateTime();
            int TransactionID = 0;
            int BatchSequence = 0;

            //Act           
            BaseResponse response = _RemitterServiceManager.UpdateChecksRemitter(LockboxRemitterName, BankID, LockboxID, BatchID, DepositDate, TransactionID, BatchSequence);

            //Assert   
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }
    }
}
