﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/20/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/20/2014 Created
********************************************************************************/
namespace OLServicesClientTests.Services
{
    //[TestClass]
    public class UserClientTests
    {
        public readonly UserServiceManager _UserServiceManager = new UserServiceManager();

        public UserClientTests()
        {

        }

        [TestMethod]
        public void TestGetUserByID()
        {
            //Arange
            int UserID = 0;

            //Act           
            BaseGenericResponse<cUserRAAM> response = _UserServiceManager.GetUserByID(UserID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestGetOnlineUserByID()
        {
            //Arange
            int UserID = 0;

            //Act           
            XmlDocumentResponse response = _UserServiceManager.GetOnlineUserByID(UserID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestGetOLUserPreferences()
        {
            //Arange

            //Act           
            XmlDocumentResponse response = _UserServiceManager.GetOLUserPreferences();

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestSetOLUserPreferences()
        {
            //Arange
            XmlDocument Parms = new XmlDocument();

            //Act           
            XmlDocumentResponse response = _UserServiceManager.SetOLUserPreferences(Parms);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestRestoreDefaultOLPreferences()
        {
            //Arange

            //Act           
            XmlDocumentResponse response = _UserServiceManager.RestoreDefaultOLPreferences();

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }
    }
}
