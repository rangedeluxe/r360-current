﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/20/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/20/2014 Created
 * * WI 154797 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
********************************************************************************/
namespace OLServicesClientTests.Services
{
    //[TestClass]
    public class SystemClientTests
    {
        public readonly SystemServiceManager _SystemServiceManager = new SystemServiceManager();

        public SystemClientTests()
        {

        }

        [TestMethod]
        public void TestInitPageActivity()
        {
            //Arange
            string ScriptName = "";
            string QueryString = "";

            //Act           
            PageActivityResponse response = _SystemServiceManager.InitPageActivity(ScriptName, QueryString);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestGetSessionUser()
        {
            //Arange

            //Act           
            BaseGenericResponse<cUserRAAM> response = _SystemServiceManager.GetSessionUser();

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

      
        [TestMethod]
        public void TestGetLockboxByID()
        {
            //Arange
            int OLWorkGroupsID = 0;

            //Act           
			BaseGenericResponse<cOLLockbox> response = _SystemServiceManager.GetLockboxByID(OLWorkGroupsID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

      
        [TestMethod]
        public void TestLogActivity()
        {
            //Arange
            ActivityCodes ActivityCode = new ActivityCodes();
            Guid OnlineImageQueueID = new Guid();
            int ItemCount = 0;
            int BankID = 0;
            int LockboxID = 0;

            //Act           
            BaseResponse response = _SystemServiceManager.LogActivity(ActivityCode, OnlineImageQueueID, ItemCount, BankID, LockboxID);

            //Assert      
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestSetWebDeliveredFlag()
        {
            //Arange
            Guid OnlineImageQueueID = new Guid();
            bool InternetDelivered = false;

            //Act           
            BaseResponse response = _SystemServiceManager.SetWebDeliveredFlag(OnlineImageQueueID, InternetDelivered);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestUpdateActivityLockbox()
        {
            //Arange
            long ActivityLogID = 0;
            int BankID = 0;
            int LockboxID = 0;

            //Act           
            BaseResponse response = _SystemServiceManager.UpdateActivityLockbox(ActivityLogID, BankID, LockboxID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

      
        public void TestGetUserLockboxes()
        {
            //Arange
            int UserID = 0;

            //Act           
            XmlDocumentResponse response = _SystemServiceManager.GetUserLockboxes(UserID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestGetBaseDocument()
        {
            //Arange

            //Act           
            XmlDocumentResponse response = _SystemServiceManager.GetBaseDocument();

            //Assert       
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }
    }
}
