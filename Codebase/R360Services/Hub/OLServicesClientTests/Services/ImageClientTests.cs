﻿using System;
using System.Data;
using System.IO;
using System.Xml;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/20/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/20/2014 Created
********************************************************************************/
namespace OLServicesClientTests.Services
{
    //[TestClass]
    public class ImageClientTests
    {
        public readonly ImageServiceManager _ImageServiceManager = new ImageServiceManager();

        public ImageClientTests()
        {

        }

        [TestMethod]
        public void TestGetImageJobFileAsAttachment()
        {
            //Arange
            Guid OnlineImageQueueID = new Guid();

            //Act           
            BaseGenericResponse<Stream> response = _ImageServiceManager.GetImageJobFileAsAttachment(OnlineImageQueueID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestCreateViewAllJobRequest()
        {
            //Arange
            XmlDocument Parms = new XmlDocument();

            //Act           
            BaseGenericResponse<DataSet> response = _ImageServiceManager.CreateViewAllJobRequest(Parms);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestCheckImageJobStatus()
        {
            //Arange
            Guid OnlineImageQueueID = new Guid();

            //Act           
            BaseGenericResponse<DataSet> response = _ImageServiceManager.CheckImageJobStatus(OnlineImageQueueID);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestCreateSearchResultsJobRequest()
        {
            //Arange
            XmlDocument SearchParms = new XmlDocument();
            XmlDocument JobParms = new XmlDocument();

            //Act           
            BaseGenericResponse<DataSet> response = _ImageServiceManager.CreateSearchResultsJobRequest(SearchParms, JobParms);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestCreateViewSpecifiedJobRequest()
        {
            //Arange
            XmlDocument Parms = new XmlDocument();

            //Act           
            BaseGenericResponse<DataSet> response = _ImageServiceManager.CreateViewSpecifiedJobRequest(Parms);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }

        [TestMethod]
        public void TestCreateViewSelectedJobRequest()
        {
            //Arange
            XmlDocument Parms = new XmlDocument();

            //Act           
            BaseGenericResponse<DataSet> response = _ImageServiceManager.CreateViewSelectedJobRequest(Parms);

            //Assert
            Assert.IsTrue(response.Status == StatusCode.SUCCESS);
        }
    }
}
