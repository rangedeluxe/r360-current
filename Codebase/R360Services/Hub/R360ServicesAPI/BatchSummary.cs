﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class BatchSummary
    {
        private readonly IReadOnlyRepository<BatchSummaryRequestDto, IEnumerable<BatchSummaryDto>, BatchSummaryRequestDto> repository;

        public BatchSummary(IReadOnlyRepository<BatchSummaryRequestDto, IEnumerable<BatchSummaryDto>, BatchSummaryRequestDto> repository)
        {
            this.repository = repository;
        }

        public IEnumerable<BatchSummaryDto> GetBatchSummary(BatchSummaryRequestDto requestContext)
        {
            //TODO: Add validations
            return repository.Get(requestContext);
        }
    }
}
