﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.OLDecisioningServicesClient;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360RaamClient;

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class ExceptionsAPI : APIBase, IDisposable
    {
        #region service fluff
        private readonly IServiceContext _serviceContext;
        private readonly IDecisioningSvcClient _client;
        private readonly IR360ServicesDAL _r360Dal;
        private readonly IPermissionProvider _permissions;
        private readonly IUserServices _userservice;
        private readonly IRaamClient _raamclient;
        private bool _disposed = false;

        public ExceptionsAPI(IServiceContext serviceContext, IDecisioningSvcClient client, 
            IR360ServicesDAL dal, ISessionDAL sessionDal, IPermissionProvider permissions,
            IUserServices usersvc, IRaamClient raam) 
            : base(serviceContext, sessionDal)
        {
            _serviceContext = serviceContext;
            _client = client;
            _r360Dal = dal;
            _permissions = permissions;
            _userservice = usersvc;
            _raamclient = raam;
        }

        protected override bool ValidateSID(out int userID)
        {
            bool bReturnValue = false;
            userID = -1;
            DataTable dtResults;

            OnLogEvent("Current User Claims: \r\n" + _ServiceContext.RAAMClaims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);

            if (_r360Dal.GetUserIDBySID(_ServiceContext.GetSID(), out dtResults))
            {
                userID = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
                bReturnValue = true;
            }

            return bReturnValue;
        }
        #endregion

        public OLDBaseGenericResponse<string> Ping()
        {
            return ValidateUserIdAndDo<OLDBaseGenericResponse<string>>((result, r360dal, userID, sessionID) => _client.Ping());
        }

        private void AppendR360Information(DecisioningBatchDto batch)
        {
            var workgroupResource = _raamclient.GetWorkgroupResource(batch.BankId, batch.WorkgroupId);
            batch.WorkgroupName = workgroupResource.Name;
            batch.Entity = _raamclient.GetEntityBreadcrumb(workgroupResource.EntityID);
            if (batch.SID.HasValue)
            {
                var person = _raamclient.GetUserByUserSID(batch.SID.Value).Person;
                batch.User = $"{person.FirstName} {person.LastName}";
            }
        }

        private void AppendR360Information(IEnumerable<DecisioningBatchDto> batches)
        {
            foreach (var b in batches)
                AppendR360Information(b);
        }

        public PendingBatchesResponse GetPendingBatches()
        {
            // todo:  need to add try catch to avoid reference error
            return ValidateUserIdAndDo<PendingBatchesResponse>((result, r360dal, userID, sessionID) =>
            {
                if (!_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View))
                {
                    var response = new PendingBatchesResponse();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var batches = _client.GetAll();
                var authorized = _userservice.GetUserLockboxes();
                batches.Batches = batches.Batches.Where(x => authorized.Data.Values.Any(y => y.BankID == x.BankId && y.LockboxID == x.WorkgroupId));

                AppendR360Information(batches.Batches);

                return batches;
            });

        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            return ValidateUserIdAndDo<PendingBatchesResponse>((result, r360dal, userID, sessionID) =>
            {
                if (!_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View))
                {
                    var response = new PendingBatchesResponse();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var batches = _client.GetBatches(bankId, workgroupId);
                var authorized = _userservice.GetUserLockboxes();
                batches.Batches = batches.Batches.Where(x => authorized.Data.Values.Any(y => y.BankID == x.BankId && y.LockboxID == x.WorkgroupId));

                AppendR360Information(batches.Batches);

                return batches;
            });
        }
        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            return ValidateUserIdAndDo<UpdateTransactionResponse>((result, r360dal, userID, sessionID) =>
            {
                var batch = _client.GetBatch(dto.GlobalBatchId).Data;

                // First check for permissions
                if ((!_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage))
                    || batch.SID.ToString() != _serviceContext.GetSID().ToString())
                {
                    var response = new UpdateTransactionResponse();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var authorized = _userservice.GetUserLockboxes();
                if (!authorized.Data.Values.Any(y => y.BankID == batch.BankId && y.LockboxID == batch.WorkgroupId))
                {
                    var response = new UpdateTransactionResponse();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                // Now check for deadline.
                if (batch.MinutesUntilDeadLine.HasValue && batch.MinutesUntilDeadLine < 0)
                {
                    var response = new UpdateTransactionResponse();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Exception Deadline has passed. You can no longer process this batch." };
                    return response;
                }

                return _client.UpdateTransaction(dto);
            });
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            return ValidateUserIdAndDo<OLDBaseGenericResponse<DecisioningBatchDto>>((result, r360dal, userID, sessionID) => 
            {
                if (!_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View))
                {
                    var response = new OLDBaseGenericResponse<DecisioningBatchDto>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var batch = _client.GetBatchDetails(globalBatchId);
                var authorized = _userservice.GetUserLockboxes();
                if (!authorized.Data.Values.Any(y => y.BankID == batch.Data.BankId && y.LockboxID == batch.Data.WorkgroupId))
                {
                    var response = new OLDBaseGenericResponse<DecisioningBatchDto>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                AppendR360Information(batch.Data);
      
                return batch;
            });
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            return ValidateUserIdAndDo<OLDBaseGenericResponse<DecisioningBatchDto>>((result, r360dal, userID, sessionID) =>
            {
                if (!_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View))
                {
                    var response = new OLDBaseGenericResponse<DecisioningBatchDto>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var batch = _client.GetBatch(globalBatchId);
                var authorized = _userservice.GetUserLockboxes();
                if (!authorized.Data.Values.Any(y => y.BankID == batch.Data.BankId && y.LockboxID == batch.Data.WorkgroupId))
                {
                    var response = new OLDBaseGenericResponse<DecisioningBatchDto>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                AppendR360Information(batch.Data);

                return batch;
            });
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            return ValidateUserIdAndDo<OLDBaseGenericResponse<DecisioningTransactionDto>>((result, r360dal, userID, sessionID) =>
            {
                if (!_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.View))
                {
                    var response = new OLDBaseGenericResponse<DecisioningTransactionDto>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var trans = _client.GetTransactionDetails(globalBatchId, transactionid);
                var authorized = _userservice.GetUserLockboxes();
                if (!authorized.Data.Values.Any(y => y.BankID == trans.Data.ParentBatch.BankId && y.LockboxID == trans.Data.ParentBatch.WorkgroupId))
                {
                    var response = new OLDBaseGenericResponse<DecisioningTransactionDto>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var workgroupResource = _raamclient.GetWorkgroupResource(trans.Data.ParentBatch.BankId, trans.Data.ParentBatch.WorkgroupId);
                AppendR360Information(trans.Data.ParentBatch);

                return trans;
            });
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            return ValidateUserIdAndDo<OLDBaseGenericResponse<bool>>((result, r360dal, userID, sessionID) =>
            {
                var batch = _client.GetBatch(globalBatchId).Data;
                if (!_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage)
                    || batch.SID.HasValue)
                {
                    var response = new OLDBaseGenericResponse<bool>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var authorized = _userservice.GetUserLockboxes();
                if (!authorized.Data.Values.Any(y => y.BankID == batch.BankId && y.LockboxID == batch.WorkgroupId))
                {
                    var response = new OLDBaseGenericResponse<bool>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                return _client.CheckOutBatch(globalBatchId, sid, firstName, lastName, login);
            });
        }

        public OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId)
        {
            return ValidateUserIdAndDo<OLDBaseGenericResponse<bool>>((result, r360dal, userID, sessionID) =>
            {
                var batch = _client.GetBatch(globalBatchId).Data;
                // Cases when we can't unlock the batch.
                //  - when it's not locked.
                //  - when it's locked and we don't have manage permission.
                //  - when it's locked and we're not the owner and we don't have override permission.
                //  - and finally, when we don't have access to the workgroup.
                // Broke out in seperate cases for ease-of-readability.
                var cannotreset = false;
                if (!batch.SID.HasValue)
                    cannotreset = true;
                else if (
                    !_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions,R360Permissions.ActionType.Manage)
                    && !_permissions.HasPermission(R360Permissions.Perm_ExceptionsUnlockOverride,R360Permissions.ActionType.Manage))
                {
                    cannotreset = true;
                }
                else if (batch.SID.Value != _serviceContext.GetSID()
                    && !_permissions.HasPermission(R360Permissions.Perm_ExceptionsUnlockOverride, R360Permissions.ActionType.Manage))
                {
                    cannotreset = true;
                }

                if (cannotreset)
                {
                    var response = new OLDBaseGenericResponse<bool>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var authorized = _userservice.GetUserLockboxes();
                if (!authorized.Data.Values.Any(y => y.BankID == batch.BankId && y.LockboxID == batch.WorkgroupId))
                {
                    var response = new OLDBaseGenericResponse<bool>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                return _client.ResetBatch(globalBatchId);
            });
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            return ValidateUserIdAndDo<OLDBaseGenericResponse<bool>>((result, r360dal, userID, sessionID) =>
            {
                var batch = _client.GetBatch(dto.globalBatchId).Data;
                if (!_permissions.HasPermission(R360Permissions.Perm_PreDepositExceptions, R360Permissions.ActionType.Manage)
                    || !batch.SID.HasValue
                    || batch.SID.Value != _serviceContext.GetSID())
                {
                    var response = new OLDBaseGenericResponse<bool>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                var authorized = _userservice.GetUserLockboxes();
                if (!authorized.Data.Values.Any(y => y.BankID == batch.BankId && y.LockboxID == batch.WorkgroupId))
                {
                    var response = new OLDBaseGenericResponse<bool>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Unauthorized request." };
                    return response;
                }

                if (batch.MinutesUntilDeadLine.HasValue && batch.MinutesUntilDeadLine.Value < 0)
                {
                    var response = new OLDBaseGenericResponse<bool>();
                    response.Status = StatusCode.FAIL;
                    response.Errors = new List<string>() { "Deadline has passed, this work can't be submitted." };
                    return response;
                }

                return _client.CompleteBatch(dto);
            });
        }

        public BaseResponse WriteAuditEvent(string eventName, string eventType, string applicationName, string description)
        {
            return ValidateUserIdAndDo<BaseResponse>((result, r360dal, userID, sessionID) =>
            {
                if (r360dal.WriteAuditEvent(userID, eventName, eventType, applicationName, description))
                {
                    result.Status = StatusCode.SUCCESS;
                }
                return result;
            });
        }

        #region Wrapper Method
        protected override ActivityCodes RequestType { get; }
        private T ValidateUserIdAndDo<T>(Func<T, IR360ServicesDAL, int, Guid, T> operation, [CallerMemberName] string procName = null)
          where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T { Status = StatusCode.FAIL };

            try
            {
                int userID;
                // Validate / get the user ID
                if (ValidateSIDAndLog(procName, out userID))
                {
                    // Perform the requested operation
                    //return operation(result, R360ServicesDAL, userID, _ServiceContext.GetSessionID());
                    return operation(result, _r360Dal, userID, _serviceContext.GetSessionID());
                }
                else
                {
                    result.Errors.Add("Invalid SID");
                }
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }

        #endregion
        #region Dispose
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_r360Dal != null)
                    {
                        _r360Dal.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                _disposed = true;

            }
        }
        #endregion
    }
}
