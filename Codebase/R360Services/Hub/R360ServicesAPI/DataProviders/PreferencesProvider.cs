﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BusinessCommon;
using WFS.RecHub.Common;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class PreferencesProvider : IPreferencesProvider
    {
        private IEnumerable<cOLPreference> preferences;
        public PreferencesProvider(string siteKey)
        {
            var pref = new OLPreferences(siteKey);
            pref.LoadOLPreferences();
            preferences = pref.GetPreferences;
        }

        public IEnumerable<cOLPreference> GetPreferences()
        {
            return preferences;
        }

        public cOLPreference GetPreferenceByName(string name)
        {
            return preferences.FirstOrDefault(x => x.PreferenceName == name);
        }
    }
}
