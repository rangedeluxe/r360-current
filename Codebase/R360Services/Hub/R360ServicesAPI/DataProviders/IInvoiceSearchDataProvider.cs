﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IInvoiceSearchDataProvider
    {
        List<WorkgroupDto> GetWorkgroups(string id);
        cOLLockboxes GetAuthorizedWorkgroups();
        bool GetInvoiceSearch(XmlDocument xml, out XmlDocument resultsxml, out DataTable results);
        string GetEntityName(int entityID);
    }
}
