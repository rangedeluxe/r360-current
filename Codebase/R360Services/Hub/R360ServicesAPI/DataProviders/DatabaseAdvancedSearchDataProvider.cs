﻿using System;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.BusinessCommon;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO.AdvancedSearch;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabaseAdvancedSearchDataProvider : IAdvancedSearchDataProvider
    {
        private readonly cSearchDAL _searchdal;
        private readonly LockboxAPI _lockboxapi;
        private readonly OLPreferences _prefs;
        private cSessionDAL _sessionDal;
        private Guid _sessionId;

        public DatabaseAdvancedSearchDataProvider(IServiceContext serviceContext)
        {
            _sessionId = serviceContext.GetSessionID();
            _searchdal = new cSearchDAL(serviceContext.SiteKey);
            _lockboxapi = new LockboxAPI(serviceContext);
            _prefs = new OLPreferences(serviceContext.SiteKey);
            _sessionDal = new cSessionDAL(serviceContext.SiteKey, _DMPObjectRoot.ConnectionType.RecHubData);
        }

        public cOLLockboxes GetAuthorizedWorkgroups()
        {
            var result = _lockboxapi.GetUserLockboxes();
            return (result != null) ? result.Data : null;
        }

        public bool LockboxSearch(TableParams tables, out XmlDocument docSearchResultsTotals, out DataTable dtSearchResults)
        {
            return _searchdal.LockboxSearch(tables, out docSearchResultsTotals, out dtSearchResults);
        }

        public int GetMaxRowsForDownload()
        {
            _prefs.LoadOLPreferences();
            var p = _prefs.GetPreferences.FirstOrDefault(x => x.PreferenceName == "MaxPrintableRows");
            return int.Parse(p.DefaultSetting);
        }

        public string GetPaymentSourceName(int id)
        {
            return _lockboxapi.GetPaymentSources(false)
                .Data
                .First(x => x.Key == id)
                .Value;
        }

        public string GetPaymentTypeName(int id)
        {
            return _lockboxapi.GetPaymentTypes()
                .Data
                .First(x => x.Key == id)
                .Value;
        }
        public void LogActivityInBackground(int bankId, int workgroupId, ActivityCodes activityCode, int itemCount,
            string moduleName)
        {
            Task.Run(() =>
            {
                int rowsReturned;
                _sessionDal.InsertSessionActivityLog(_sessionId, moduleName, activityCode, Guid.Empty,
                    itemCount, bankId, workgroupId, out rowsReturned);
            });
        }
    }
}