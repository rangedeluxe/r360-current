﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using static WFS.RecHub.R360Shared.ConfigHelpers;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabasePaymentSearchDataProvider : IPaymentSearchDataProvider
    {
        private readonly IRaamClient _raamclient;
        private readonly cSearchDAL _dal;
        private readonly LockboxAPI _lockboxapi;
        public DatabasePaymentSearchDataProvider(IServiceContext context)
        {
            _raamclient = new RaamClient(new Logger((s) => { }, (s) => { }));
            _dal = new cSearchDAL(context.SiteKey);
            _lockboxapi = new LockboxAPI(context);
        }

        public bool GetPaymentSearch(XmlDocument xml, out XmlDocument resultsxml, out DataTable results)
        {
            return _dal.RemittanceSearch(xml, out resultsxml, out results);
        }

        public List<WorkgroupDto> GetWorkgroups(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            var output = new List<WorkgroupDto>();

            // If it's a workgroup.
            if (id.Contains("|"))
            {
                var split = id.Trim().Split('|');
                output.Add(new WorkgroupDto()
                {
                    BankId = int.Parse(split[0]),
                    ClientAccountId = int.Parse(split[1])
                });
                return output;
            }

            // It's an entity. So we need to compile a list of children workgroups.
            var entityid = int.Parse(id);
            var resources = _raamclient.GetWorkgroupsForEntity(entityid);
            foreach (var r in resources)
            {
                var split = r.ExternalReferenceID.Trim().Split('|');
                output.Add(new WorkgroupDto()
                {
                    BankId = int.Parse(split[0]),
                    ClientAccountId = int.Parse(split[1])
                });
            }

            return output;
        }

        public cOLLockboxes GetAuthorizedWorkgroups()
        {
            var result = _lockboxapi.GetUserLockboxes();
            return (result != null) ? result.Data : null;
        }
    }
}
