﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IAdvancedSearchStoredQueryDataProvider
    {
        AdvancedSearchStoredQueryDto Get(Guid id);
        IEnumerable<AdvancedSearchStoredQueryDto> GetAll();
        bool Delete(Guid id);
        bool Save(AdvancedSearchStoredQueryDto item, XmlDocument itemxml);
        int GetMaxQueries();
    }
}
