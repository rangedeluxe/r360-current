﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IBatchDataProvider
    {
        bool GetBatchSummary(int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate, 
            int? paymentTypeID, int? paymentSourceID, bool DisplayScannedCheckOnline, 
            int start, int length, string orderby, string orderdirection, string search, 
            out int recordstotal, out int recordsfiltered, out DataTable dt,
            out int transactionRecords, out int checkRecords, out int documentRecords, out decimal checkTotal);
        bool GetBatchDetail(long BatchID, DateTime DepositDate, bool DisplayScannedCheckOnline, int StartRecord, 
            int RecordsPerPage, string OrderBy, string OrderDirection, string Search, out int TotalRecords, out int FilteredRecords, out DataTable dt);
    }
}
