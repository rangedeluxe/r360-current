﻿using System.Data;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO.AdvancedSearch;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    /// <summary>
    /// DAL functionality that's needed for Advanced Search
    /// </summary>
    public interface IAdvancedSearchDataProvider
    {
        bool LockboxSearch(TableParams tables, out XmlDocument docSearchResultsTotals, out DataTable dtSearchResults);
        cOLLockboxes GetAuthorizedWorkgroups();
        int GetMaxRowsForDownload();
        string GetPaymentSourceName(int id);
        string GetPaymentTypeName(int id);
        void LogActivityInBackground(int bankId, int workgroupId, ActivityCodes activityCode, int itemCount,
            string moduleName);
    }
}
