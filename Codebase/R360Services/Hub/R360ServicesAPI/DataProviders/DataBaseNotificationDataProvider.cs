﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Xml;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.DAL;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using static WFS.RecHub.R360Shared.ConfigHelpers;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DataBaseNotificationDataProvider : INotificationsDataProvider
    {
        private const string CENDS_SITEKEY = "iponline";
        private readonly cNotificationDAL _dal;
        private readonly IRaamClient _raamclient;
        private readonly OLFOnlineClient _olf;
        private readonly IServiceContext _context;
        

        public DataBaseNotificationDataProvider(IServiceContext servicecontext)
        {
            _context = servicecontext;
            _dal = new cNotificationDAL(servicecontext.SiteKey);
            _raamclient = new RaamClient(new Logger((s) => { }, (s) => { }));
            _olf = new OLFOnlineClient(servicecontext.SiteKey);
        }
  
        public NotificationsResponseDto GetUserNotifications(Guid SessionID, DateTime startDate, DateTime endDate, int notificationFileTypeKey,
            string orderBy, string orderDir, int startRecord, int maxRows, int timeZoneBias, string fileName,
            XmlDocument docReqXml, string search)
        {
            var response = new NotificationsResponseDto();
            int totalRecords = 0;
            DataTable dt = null;
            int filteredTotal = 0;
            var notifications = new List<NotificationDto>();
            var resp = _dal.GetUserNotifications(SessionID, startDate, endDate, notificationFileTypeKey, orderBy, orderDir,
                 startRecord, maxRows, timeZoneBias, fileName, docReqXml, out totalRecords, out dt, out filteredTotal, search);
            if (resp)
            {
                response.FilteredTotal = filteredTotal;
                notifications.AddRange(dt.Rows.Cast<DataRow>().Select(row => new NotificationDto
                {
                    Id = row.GetString("LockboxID") + " - " + row.GetString("LockboxName"),
                    MessageGroup = row.GetInteger("NotificationMessageGroup"),
                    Date = row.GetDateTime("NotificationDate"),
                    DateString = row.GetDateTime("NotificationDate").ToString(CultureInfo.CurrentCulture),
                    Message = row.GetString("MessageText"),
                    AttachmentCount = row.GetInteger("NotificationFileCount"),
                    ViewNotificationIcon = row.GetInteger("NotificationFileCount") > 0
                }));
                response.TotalRecords = totalRecords;
                response.Notifications = notifications;
                response.FilteredTotal = filteredTotal;
            }
            return response;
        }

        public bool GetNotificationFileTypes(out DataTable dt)
        {
            return _dal.GetNotificationFileTypes(out dt);
        }

        public NotificationDto GetNotification(int messagegroup, int timezonebias, Guid SessionID)
        {
            DataTable dt = null;
            var result = _dal.GetNotification(messagegroup, timezonebias, SessionID, out dt);
            if (!result) return null;
            var row = dt.Rows[0];
            var notification = new NotificationDto()
            {
                Id = row.GetString("LockboxID") + " - " + row.GetString("LockboxName"),
                MessageGroup = row.GetInteger("NotificationMessageGroup"),
                Date = row.GetDateTime("NotificationDate"),
                DateString = row.GetDateTime("NotificationDate").ToString(CultureInfo.CurrentCulture),
                Message =row.GetString("MessageText"),
                AttachmentCount = row.GetInteger("NotificationFileCount"),
                ViewNotificationIcon = row.GetInteger("NotificationFileCount") > 0,
                Files = new List<NotificationFileDto>()
            };
            return notification;
        }

        public IEnumerable<NotificationFileDto> GetNotificationFiles(int messagegroup, Guid sessionId)
        {
            DataTable dt = null;
            var response = new List<NotificationFileDto>();
            var resp = _dal.GetNotificationFiles(messagegroup, sessionId, out dt);
            if (!resp)
            {
                return null;
            }
            foreach (DataRow r in dt.Rows)
            {
                var file = new NotificationFileDto()
                {
                    FileExtension = r.GetString("FileExtension"),
                    FileId = r.GetGuid("FileIdentifier"),
                    FilePath = r.GetString("FilePath"),
                    FileTypeDescription = r.GetString("FileTypeDescription"),
                    Id = r.GetInteger("factNotificationFileKey"),
                    MessageGroup = r.GetInteger("NotificationMessageGroup"),
                    UserFileName = r.GetString("UserFileName")
                };
                file.FileSize = GetNotificationFileSize(file.FilePath);
                response.Add(file);
            }
            return response;
        }

        public List<WorkgroupDto> GetWorkgroups(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return null;

            var output = new List<WorkgroupDto>();

            // If it's a workgroup.
            if (id.Contains("|"))
            {
                var split = id.Trim().Split('|');
                output.Add(new WorkgroupDto()
                {
                    BankId = int.Parse(split[0]),
                    ClientAccountId = int.Parse(split[1])
                });
                return output;
            }

            // It's an entity. So we need to compile a list of children workgroups.
            var entityid = int.Parse(id);
            var resources = _raamclient.GetWorkgroupsForEntity(entityid);
            foreach (var r in resources)
            {
                var split = r.ExternalReferenceID.Trim().Split('|');
                output.Add(new WorkgroupDto()
                {
                    BankId = int.Parse(split[0]),
                    ClientAccountId = int.Parse(split[1])
                });
            }

            return output;
        }

        public long GetNotificationFileSize(string path)
        {
            try
            {
                return _olf.GetCENDSFileSize(new OLFServices.FileMetaData() { SiteKey = CENDS_SITEKEY }, path);
            }
            catch
            {
                return 0;
            }
        }
    }
}
