﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.BusinessCommon;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabaseAdvancedSearchStoredQueryDataProvider : IAdvancedSearchStoredQueryDataProvider
    {
        private readonly cSearchDAL _dal;
        private readonly R360ServicesDAL _userdal;
        private readonly OLPreferences _prefs;
        private readonly int _userid;

        public DatabaseAdvancedSearchStoredQueryDataProvider()
        {
            _dal = new cSearchDAL(R360ServiceContext.Current.SiteKey);
            _userdal = new R360ServicesDAL(R360ServiceContext.Current.SiteKey);
            _prefs = new OLPreferences(R360ServiceContext.Current.SiteKey);

            // Grab our current UserID based on the SID.
            DataTable dt;
            if (_userdal.GetUserIDBySID(R360ServiceContext.Current.GetSID(), out dt))
                _userid = dt.Rows[0].GetInteger("UserID");

        }

        public bool Delete(Guid id)
        {
            int rows;
            return _dal.DeleteSavedQuery(_userid, id, out rows);
        }

        public AdvancedSearchStoredQueryDto Get(Guid id)
        {
            DataTable dt;
            var success = _dal.GetPreDefSearchQueryForUser(id, _userid, out dt);
            if (!success || !dt.HasData())
                return null;

            var row = dt.Rows[0];

            var output = new AdvancedSearchStoredQueryDto()
            {
                Description = row.GetString("Description"),
                Id = row.GetGuid("PredefSearchID"),
                IsDefault = row.GetBoolean("IsDefault"),
                Name = row.GetString("Name"),
                XMLParams = row.GetString("SearchParmsXML")
            };

            return output;
        }

        public IEnumerable<AdvancedSearchStoredQueryDto> GetAll()
        {
            DataTable dt;

            var success = _dal.GetPreDefSearchQueriesForUser(_userid, out dt);
            if (!success)
                return null;

            var output = new List<AdvancedSearchStoredQueryDto>();

            foreach (DataRow row in dt.Rows)
            {
                output.Add(new AdvancedSearchStoredQueryDto()
                {
                    Description = row.GetString("Description"),
                    Id = row.GetGuid("PredefSearchID"),
                    IsDefault = row.GetBoolean("IsDefault"),
                    Name = row.GetString("Name"),
                });
            }

            return output;
        }

        public int GetMaxQueries()
        {
            _prefs.LoadOLPreferences();
            var p = _prefs.GetPreferences.FirstOrDefault(x => x.PreferenceName == "MaximumQueriesSaved");
            return int.Parse(p.DefaultSetting);
        }

        public bool Save(AdvancedSearchStoredQueryDto item, XmlDocument itemxml)
        {
            // Get the count of the current user's saved queries. We need to set the default if it's the only one.
            int count = 0;
            Guid id = item.Id;
            int rows;
            int error;
            bool success = false;

            var queries = GetAll();
            if (queries == null)
                return false;
            count = queries.Count();

            var def = item.IsDefault || count == 0;
            if (item.Description == null)
                item.Description = string.Empty;
            if (item.Id == null || item.Id == Guid.Empty)
                success = _dal.InsertUserPreDefSavedQuery(_userid, def ? 1 : 0, item.Name, item.Description, itemxml, out id, out rows, out error);
            else
            {
                // One to update the XML, the next to update the default.
                success = _dal.UpdatePreDefSearchQuery(item.Id, item.Name, item.Description, itemxml, out rows);
                success = success && _dal.UpdateSavedQuery(_userid, item.Id, item.IsDefault ? 1 : 0, out rows);
            }

            item.Id = id;
            item.IsDefault = def;
            return true;
        }
    }
}