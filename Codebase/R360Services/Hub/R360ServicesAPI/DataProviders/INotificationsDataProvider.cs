﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    //TODO: These should all be refactored to return dto's instead of dataTables to make mocking easier
    public interface INotificationsDataProvider
    {
        NotificationsResponseDto GetUserNotifications(Guid SessionID, DateTime startDate, DateTime endDate, int notificationFileTypeKey, 
                                string orderBy, string orderDir, int startRecord, int maxRows, int timeZoneBias, 
                                string fileName, XmlDocument docReqXml,string search);

        NotificationDto GetNotification(int messagegroup, int timezonebias, Guid sessionId);

        IEnumerable<NotificationFileDto> GetNotificationFiles(int messagegroup, Guid sessionId);

        bool GetNotificationFileTypes(out DataTable dt);

        List<WorkgroupDto> GetWorkgroups(string id);
        long GetNotificationFileSize(string path);
    }
}
