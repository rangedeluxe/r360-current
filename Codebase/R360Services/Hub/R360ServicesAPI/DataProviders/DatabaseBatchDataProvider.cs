﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabaseBatchDataProvider : IBatchDataProvider
    {
        private readonly IServiceContext _context;
        private readonly cItemProcDAL _dal;
        public DatabaseBatchDataProvider(IServiceContext context)
        {
            _context = context;
            _dal = new cItemProcDAL(context.SiteKey);
        }

        public bool GetBatchDetail(long BatchID, DateTime DepositDate, bool DisplayScannedCheckOnline, int StartRecord, int RecordsPerPage, string OrderBy, string OrderDirection, string Search, out int TotalRecords, out int FilteredRecords,  out DataTable dt)
        {
            return _dal.GetBatchDetail(_context.GetSessionID(), BatchID, DepositDate, DisplayScannedCheckOnline, StartRecord, RecordsPerPage, OrderBy, OrderDirection, Search, out TotalRecords, out FilteredRecords, out dt);
        }

        public bool GetBatchSummary(int BankID, int ClientAccountID, DateTime StartDate, DateTime EndDate,
            int? paymentTypeID, int? paymentSourceID, bool DisplayScannedCheckOnline,
            int start, int length, string orderby, string orderdirection, string search, out int recordstotal,
            out int recordsfiltered, out DataTable dt, out int transactionRecords, out int checkRecords,
            out int documentRecords, out decimal checkTotal)
        {
            return _dal.GetBatchSummary(_context.GetSessionID(), BankID, ClientAccountID, StartDate, EndDate, paymentTypeID, paymentSourceID, 
                DisplayScannedCheckOnline, start, length, orderby, orderdirection, search, out recordstotal, out recordsfiltered, out dt, out transactionRecords,
                out checkRecords, out documentRecords, out checkTotal);
        }
    }
}
