﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.BusinessCommon;
using WFS.RecHub.DAL;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using static WFS.RecHub.R360Shared.ConfigHelpers;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabaseDisplayBatchIdProvider : IDisplayBatchIdProvider
    {
        private readonly LockboxAPI _lockboxapi;

        public DatabaseDisplayBatchIdProvider(IServiceContext servicecontext)
        {
            _lockboxapi = new LockboxAPI(servicecontext);
        }

        public bool GetDisplayBatchIdProperty(List<WorkgroupDto> workgroups)
        {
            // The lockbox API already takes into account workgroup AND parent entity setting.
            foreach(var w in workgroups)
            {
                var box = _lockboxapi.GetLockBoxByAccountIDBankID(w.BankId, w.ClientAccountId);
                if (box.Data.DisplayBatchID)
                    return true;
            }
            return false;
        }
    }
}
