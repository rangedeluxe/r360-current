﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IDisplayBatchIdProvider
    {
        /// <summary>
        /// Returns true or false if we're meant to display the batch ID.
        /// Cascades based on workgroup, and parent entities.
        /// </summary>
        /// <param name="workgrouporentity"></param>
        /// <returns></returns>
        bool GetDisplayBatchIdProperty(List<WorkgroupDto> workgroups);
    }
}
