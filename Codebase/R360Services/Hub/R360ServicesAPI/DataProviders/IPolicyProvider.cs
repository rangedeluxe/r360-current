﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IPolicyProvider
    {
        bool HasPermission(string asset, R360Permissions.ActionType type);
    }
}
