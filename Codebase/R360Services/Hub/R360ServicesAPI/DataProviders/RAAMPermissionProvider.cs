﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class RAAMPermissionProvider : IPermissionProvider
    {
        public bool HasPermission(string permission, R360Permissions.ActionType type)
        {
            return R360Permissions.Current.Allowed(permission, type);
        }
    }
}
