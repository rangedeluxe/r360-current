﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Services
{
    public class OLFImageService : IImageService
    {
        private readonly ImageAPI _api;
        public OLFImageService()
        {
            _api = new ImageAPI(R360ServiceContext.Current);
        }

        public long GetImageSize(int PICSDate, int BankID, int LockboxID, long BatchID, int Item, int DepositDateKey, string ImageType, short Page, long SourceBatchID, string BatchSourceShortName, string ImportTypeShortName)
        {
            return _api.GetImageSize(PICSDate, BankID, LockboxID, BatchID, Item, DepositDateKey, ImageType, Page, SourceBatchID, BatchSourceShortName, ImportTypeShortName).Data;
        }

        public OlfResponseDto ImagesExist(ImageRequestDto request)
        {
            return _api.ImagesExist(request);
        }

        public OlfResponseDto BatchSequenceImagesExist(ImageRequestDto request)
        {
            return _api.BatchSequenceImagesExist(request);
        }
    }
}
