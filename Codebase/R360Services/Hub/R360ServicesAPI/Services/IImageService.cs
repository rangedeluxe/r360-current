﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OLFServices.DataTransferObjects;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Services
{
    public interface IImageService
    {
        long GetImageSize(int PICSDate, int BankID, int LockboxID, long BatchID, int Item, int DepositDateKey, string ImageType, short Page, long SourceBatchID, string BatchSourceShortName, string ImportTypeShortName);
        OlfResponseDto ImagesExist(ImageRequestDto request);
        OlfResponseDto BatchSequenceImagesExist(ImageRequestDto request);
    }
}
