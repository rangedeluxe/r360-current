﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.itemProcessing.DAL;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class RemitterAPI : BaseOLAPI
    {
        private CBXAPI _CBXSrv;
        private LockboxAPI _LockboxSrv;
        private cIPRemitterDAL _RemitterDAL = null;
        private SystemAPI _SystemSrv;

        public RemitterAPI(R360ServiceContext serviceContext)
            : base(serviceContext)
        {
        }

        /// <summary>
        /// Retrieves reference to CBXSrv service
        /// </summary>
        private CBXAPI CBXSrv
        {
            get
            {
                if (_CBXSrv == null)
                {
                    _CBXSrv = new CBXAPI(_serviceContext);
                    _CBXSrv.Session = Session;
                }

                return (_CBXSrv);
            }
        }
        /// <summary>
        /// Retrieves reference to LockboxSrv service
        /// </summary>
        private LockboxAPI LockboxSrv
        {
            get
            {
                if (_LockboxSrv == null)
                {
                    _LockboxSrv = new LockboxAPI(_serviceContext);
                    _LockboxSrv.Session = Session;
                }

                return (_LockboxSrv);
            }
        }
        private cIPRemitterDAL RemitterDAL
        {
            get
            {
                if (_RemitterDAL == null)
                {
                    _RemitterDAL = new cIPRemitterDAL(this.SiteKey);
                }
                return (_RemitterDAL);
            }
        }
        protected override RecHub.Common.ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acR360WebServiceCall;
            }
        }
        /// <summary>
        /// Retrieves reference to SystemSrv service
        /// </summary>
        private SystemAPI SystemSrv
        {
            get
            {
                if (_SystemSrv == null)
                {
                    _SystemSrv = new SystemAPI(_serviceContext);
                    _SystemSrv.Session = Session;
                }

                return (_SystemSrv);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !Disposed)
            {
                if (_CBXSrv != null)
                {
                    _CBXSrv.Dispose();
                }
                if (_LockboxSrv != null)
                {
                    _LockboxSrv.Dispose();
                }
                if (_RemitterDAL != null)
                {
                    _RemitterDAL.Dispose();
                }
                if (_SystemSrv != null)
                {
                    _SystemSrv.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        protected override bool ValidateSID(out int userID)
        {
            // TODO: Implement this; it will be called from the standard wrapper methods...
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetRemitterMaintenancePage(Guid LockboxRemitterID)
        {
            XmlDocument objXML = null;
            cRemitter objRemitter;
            XmlNode nodeUserLockboxes;
            cOLLockboxes objLockboxes;
            XmlDocumentResponse response = null;

            try
            {
                // Retrieve base XML document
                objXML = SystemSrv.GetBaseDocument().Data;

                if (LockboxRemitterID != Guid.Empty && GetLockboxRemitterByID(LockboxRemitterID, out objRemitter))
                {
                    objXML.DocumentElement.AppendChild(objXML.ImportNode(objRemitter.AsXMLNode("Remitter"), true));
                }

                nodeUserLockboxes = ipoXmlLib.addElement(objXML.DocumentElement, "UserLockboxes");

                objLockboxes = LockboxSrv.GetUserLockboxes().Data;

                if (objLockboxes != null && objLockboxes.Count > 0)
                {
                    foreach (cOLLockbox lockbox in objLockboxes.Values)
                    {
                        nodeUserLockboxes.AppendChild(objXML.ImportNode(lockbox.AsXMLNode("Lockbox"), true));
                    }
                }

                response = new XmlDocumentResponse()
                {
                    Data = objXML,
                    Status = StatusCode.SUCCESS
                };

                return (response);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetRemitterMaintenancePage");

                // Add error message to return document.
                if (objXML == null)
                {
                    objXML = SystemSrv.GetBaseDocument().Data;
                }

                ipoXmlLib.AddMessageToDoc(objXML,
                                          xmlMessageType.xmlMsgError,
                                          "An error occurred while retrieving Remitter Maintenance Page values.");

                response = new XmlDocumentResponse()
                {
                    Data = objXML,
                    Status = StatusCode.FAIL
                };

                return (response);
            }
        }

        public XmlDocumentResponse GetAllRemitters(int StartRecord, bool ShowAll)
        {
            bool bolRetVal = false;
            cOLLockboxes objLockboxes;
            XmlDocument objXML = null;
            int lngStartRecord;
            XmlDocument objRetVal = null;
            XmlDocumentResponse response = null;

            try
            {
                //retrieve the base xml return document
                objXML = SystemSrv.GetBaseDocument().Data;

                if (StartRecord <= 0)
                {
                    lngStartRecord = 1;
                }
                else
                {
                    lngStartRecord = StartRecord;
                }

                objLockboxes = LockboxSrv.GetUserLockboxes().Data;

                //return xml document as SOAP attachment
                objRetVal = objXML;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetAllRemitters");

                if (objXML == null)
                {
                    objXML = SystemSrv.GetBaseDocument().Data;
                }

                //add generic error message to return document
                ipoXmlLib.AddMessageToDoc(objXML,
                                          xmlMessageType.xmlMsgError,
                                          "An error occurred while retrieving the remitters. Please try your request again.");
            }

            response = new XmlDocumentResponse()
            {
                Data = objRetVal,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<cRemitter> GetLockboxRemitter(Guid LockboxRemitterID)
        {
            cRemitter remitter;
            BaseGenericResponse<cRemitter> response = null;
            bool bolRetVal;

            bolRetVal = GetLockboxRemitterByID(LockboxRemitterID, out remitter);

            response = new BaseGenericResponse<cRemitter>()
            {
                Data = remitter,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<cRemitter> GetLockboxRemitter(string RoutingNumber, string AccountNumber, Guid OLLockboxID)
        {
            bool bolRetVal = false;
            DataTable dt = null;
            cRemitter objRemitter = null;
            cLockbox objOLLockbox = null;
            cRemitter objRetVal = null;
            BaseGenericResponse<cRemitter> response = null;

            objOLLockbox = LockboxSrv.GetLockboxByID(OLLockboxID).Data;

            if (objOLLockbox != null)
            {
                if (RemitterDAL.GetLockboxRemitter(objOLLockbox.BankID,
                                                    objOLLockbox.CustomerID,
                                                    objOLLockbox.LockboxID,
                                                    RoutingNumber,
                                                    AccountNumber, out dt)&&dt!=null)
                {
                    bolRetVal = true;

                    if (dt.HasData() && dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        objRemitter = new cRemitter();
                        objRemitter.LoadDataRow(ref dr);

                        if (objRemitter != null)
                        {
                            objRetVal = objRemitter;
                        }

                        dt.Dispose();
                    }
                    else
                    {
                        objRemitter = GetGlobalRemitter(RoutingNumber, AccountNumber);

                        if (objRemitter != null)
                        {
                            objRetVal = objRemitter;
                        }
                    }
                }
                else
                {
                    EventLog.logEvent("Unable to retrieve Lockbox Remitter.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                }
            }

            if (objRemitter != null)
            {
                objRemitter.BankID = objOLLockbox.BankID;
                objRemitter.CustomerID = objOLLockbox.CustomerID;
                objRemitter.LockboxID = objOLLockbox.LockboxID;
                objRemitter.RoutingNumber = RoutingNumber;
                objRemitter.Account = AccountNumber;
            }

            response = new BaseGenericResponse<cRemitter>()
            {
                Data = objRetVal,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        private bool GetLockboxRemitterByID(Guid LockboxRemitterID, out cRemitter Remitter)
        {
            cRemitter objRemitter = null;
            DataTable dt = null;
            cOLLockbox objLockbox;
            bool bolRetVal;

            try
            {
                if (RemitterDAL.GetLockboxRemitter(LockboxRemitterID, out dt) && dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    objRemitter = new cRemitter();
                    objRemitter.LoadDataRow(ref dr);

                    // Retrieve the Lockbox so we can validate the current session user has permission.
                    objLockbox = LockboxSrv.GetLockboxByLockboxID(objRemitter.BankID, objRemitter.LockboxID).Data;

                    if (objLockbox != null)
                    {
                        if (CBXSrv.CheckLockboxPermission(objLockbox.OLLockboxID).Status == StatusCode.FAIL)
                        {
                            objRemitter = null;
                            bolRetVal = false;
                        }
                        else
                        {
                            bolRetVal = true;
                        }
                    }
                    else
                    {
                        objRemitter = null;
                        bolRetVal = false;
                    }

                    dt.Dispose();
                }
                else
                {
                    objRemitter = null;
                    bolRetVal = false;
                }
            }
            catch
            {
                objRemitter = null;
                bolRetVal = false;
                throw;
            }

            Remitter = objRemitter;

            return (bolRetVal);
        }

        private cRemitter GetGlobalRemitter(string RoutingNumber, string AccountNumber)
        {
            cRemitter objRetVal = null;
            DataTable dt = null;
            cRemitter objRemitter;

            RemitterDAL.GetGlobalRemitter(RoutingNumber, AccountNumber, out dt);

            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                objRemitter = new cRemitter();
                objRemitter.LoadDataRow(ref dr);

                if ((objRemitter != null))
                {
                    objRetVal = objRemitter;
                }

                dt.Dispose();
            }

            return objRetVal;
        }

        public BaseResponse LockboxRemitterExists(string RoutingNumber, string AccountNumber, Guid OLLockboxID)
        {
            bool bolRetVal = false;
            cRemitter objRemitter;
            BaseResponse response = null;

            objRemitter = GetLockboxRemitter(RoutingNumber,
                                             AccountNumber,
                                             OLLockboxID).Data;

            if (objRemitter == null)
            {
                bolRetVal = false;
            }
            else
            {
                bolRetVal = true;
            }

            response = new BaseResponse()
            {
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseResponse UpdateChecksRemitter(string LockboxRemitterName, int BankID, int LockboxID, int BatchID, DateTime DepositDate, int TransactionID, int BatchSequence)
        {
            bool bolRetVal = false;
            int intRowsAffected;
            BaseResponse response = null;

            try
            {
                //execute sql
                RemitterDAL.UpdateChecksRemitter(LockboxRemitterName,
                                                 BankID,
                                                 LockboxID,
                                                 BatchID,
                                                 DepositDate,
                                                 TransactionID,
                                                 BatchSequence,
                                                 out intRowsAffected);

                //check for success
                if (intRowsAffected > 0)
                {
                    //record updated
                    bolRetVal = true;
                }
                else if (intRowsAffected == 0)
                {
                    //check record not found
                    throw (new Exception("Unable to add the remitter. The check record was not valid."));
                }
                else
                {
                    //error
                    throw (new Exception(RemitterDAL.GetLastError.Description));
                }
            }
            catch (Exception ex)
            {
                //log Error
                EventLog.logError(ex, this.GetType().Name, "UpdateChecksRemitter");

                //throw error back to calling application
                throw ;
            }

            response = new BaseResponse()
            {
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }
    }
}
