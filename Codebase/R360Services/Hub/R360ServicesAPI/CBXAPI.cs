﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brian Holmes
* Date: 06/12/2014
*
* Purpose:
*
* Modification History
* WI 146250 BDH 06/12/2014
*   -Created
* WI 154797 TWE 07/21/2014
*   -Remove OLservices and start using the new WCF version
* WI 157331 TWE 08/06/2014
*   -Add permissions node to base document - fix stored procedure call
* WI 156685 JMC 08/18/2014
*   -Corrected bug to populate BatchSummary Xml with OLWorkgroupID instead of
*    OLLockboxID
* WI 157331 SAS 09/29/2014
*   Change done for retrieving image relative path.
* WI 175214 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName
* WI 176360 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName
* WI 191689 JSF 4/17/2015
*    Batch Summary - Search Workgroup - when max viewing days exceeded, referring to an "...account"
********************************************************************************/

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class CBXAPI : BaseOLAPI
    {
        private ImageAPI _ImageSrv;
        private cItemProcDAL _ItemProcDAL = null;
        private ILockboxAPI _LockboxSrv;
        private ISystemAPI _SystemSrv;

        public CBXAPI(IServiceContext serviceContext)
            : base(serviceContext)
        {
        }

        /// <summary>
        /// Retrieves reference to ImageSrv service
        /// </summary>
        private ImageAPI ImageSrv
        {
            get
            {
                if (_ImageSrv == null)
                {
                    _ImageSrv = CreateImageSrv();
                    _ImageSrv.Session = Session;
                }

                return (_ImageSrv);
            }
        }

        private cItemProcDAL ItemProcDAL
        {
            get
            {
                if (_ItemProcDAL == null)
                {
                    _ItemProcDAL = CreateItemProcDAL();
                }

                return (_ItemProcDAL);
            }
        }

        /// <summary>
        /// Retrieves reference to LockboxSrv service
        /// </summary>
        private ILockboxAPI LockboxSrv
        {
            get
            {
                if (_LockboxSrv == null)
                {
                    _LockboxSrv = CreateLockboxSrv();
                    _LockboxSrv.Session = Session;
                }

                return (_LockboxSrv);
            }
        }

        protected override ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acR360WebServiceCall;
            }
        }
        /// <summary>
        /// Retrieves reference to SystemSrv service
        /// </summary>
        private ISystemAPI SystemSrv
        {
            get
            {
                if (_SystemSrv == null)
                {
                    _SystemSrv = CreateSystemSrv();
                    _SystemSrv.Session = Session;
                }

                return (_SystemSrv);
            }
        }

        protected virtual ImageAPI CreateImageSrv()
        {
            return new ImageAPI(_serviceContext);
        }
        protected virtual cItemProcDAL CreateItemProcDAL()
        {
            return new cItemProcDAL(this.SiteKey);
        }
        protected virtual ILockboxAPI CreateLockboxSrv()
        {
            return new LockboxAPI(_serviceContext);
        }
        protected virtual SystemAPI CreateSystemSrv()
        {
            return new SystemAPI(_serviceContext);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing && !Disposed)
            {
                if (_ImageSrv != null)
                {
                    _ImageSrv.Dispose();
                }
                if (_ItemProcDAL != null)
                {
                    _ItemProcDAL.Dispose();
                }
                if (_LockboxSrv != null)
                {
                    _LockboxSrv.Dispose();
                }
                if (_SystemSrv != null)
                {
                    _SystemSrv.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        protected override bool ValidateSID(out int userID)
        {
            // TODO: Implement this; it will be called from the standard wrapper methods...
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetDepositTotals(XmlDocument docReqXml)
        {
            DataTable dt = null;
            DateTime dteDepositDate;
            int intTemp;
            bool bolPaginateRS;
            int lngStartRecord;
            DateTime dteTemp;
            XmlDocumentResponse response = null;

            SaveXmlLog(docReqXml, GetRequestIDFromXml(docReqXml), "ReqGetDepositTotals");

            try
            {
                //Retrieve Deposit Total parameters from the parameter Xml document.
                if (docReqXml.DocumentElement.SelectSingleNode("DepositDate") == null)
                {
                    dteDepositDate = DateTime.Now.Date;
                }
                else
                {
                    if (DateTime.TryParse(docReqXml.DocumentElement.SelectSingleNode("DepositDate").InnerText, out dteTemp))
                    {
                        dteDepositDate = dteTemp;
                    }
                    else
                    {
                        dteDepositDate = DateTime.Now.Date;
                    }
                }

                if (docReqXml.DocumentElement.SelectSingleNode("PaginateRS") == null)
                {
                    bolPaginateRS = true;
                }
                else
                {
                    if (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("PaginateRS").InnerText, out intTemp))
                    {
                        bolPaginateRS = (intTemp == 1);
                    }
                    else
                    {
                        bolPaginateRS = true;
                    }
                }

                if (docReqXml.DocumentElement.SelectSingleNode("StartRecord") == null)
                {
                    lngStartRecord = 1;
                }
                else
                {
                    if (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("StartRecord").InnerText, out intTemp))
                    {
                        lngStartRecord = intTemp;
                    }
                    else
                    {
                        lngStartRecord = 1;
                    }
                }

                // Retrieve base XML document
                response = SystemSrv.GetBaseDocument();

                // Validate input data
                if (dteDepositDate < DateTime.Parse("1/1/1900"))
                {
                    ipoXmlLib.AddMessageToDoc(response.Data, xmlMessageType.xmlMsgError, "You must enter a valid Deposit Date in mm/dd/yyyy format greater than 1/1/1900.");
                }

                if (ItemProcDAL.GetLockboxSummary(_ServiceContext.GetSessionID(), Session.SessionUser.UserID, int.Parse(dteDepositDate.ToString("yyyyMMdd")), Session.OLSiteOptions.Preferences.UseCutoff, out dt))
                {
                    // Add recordset to return XML document
                    if (dt.Rows.Count > 0)
                    {
                        ipoXmlLib.AddRecordsetToDoc(response.Data, dt, "DailyDeposits", null, null, lngStartRecord, (bolPaginateRS ? Session.ServerOptions.Preferences.LockboxSummaryRecordsPerPage : 0), false);
                    }
                    else
                    {
                        ipoXmlLib.AddMessageToDoc(response.Data, xmlMessageType.xmlMsgInformation, "There are no Lockboxes in the system for you to view. Please try again.");
                    }

                    dt.Dispose();
                }
                else
                {
                    EventLog.logEvent("Unable to retrieve Lockbox Summary information from the database", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                }

                //Calculate paging information and add PageInfo node to nodeRS
                ipoXmlLib.AddPageInfo(response.Data.DocumentElement, lngStartRecord, Session.ServerOptions.Preferences.LockboxSummaryRecordsPerPage, null, ".");

                response.Status = StatusCode.SUCCESS;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetDepositTotals");

                // Add error message to return document.
                if (response.Data == null)
                {
                    response = SystemSrv.GetBaseDocument();
                }

                ipoXmlLib.AddMessageToDoc(response.Data, xmlMessageType.xmlMsgError, "An error occurred while retrieving the deposit totals. Please verify the Deposit Date and try again.");

                response.Status = StatusCode.FAIL;
            }

            SaveXmlLog(response.Data, GetRequestIDFromXml(docReqXml), "RespGetDepositTotals");

            return (response);
        }

        private Decimal GetLockboxTotal(int BankID, int LockboxID, DateTime StartDate)
        {
            return (GetLockboxTotal(BankID, LockboxID, StartDate, StartDate, null, null));
        }

        private Decimal GetLockboxTotal(int BankID, int LockboxID, DateTime StartDate, DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID)
        {
            DataTable dt = null;
            Decimal decRetVal = (Decimal)0;

            try
            {
				if (ItemProcDAL.GetLockboxTotal(_ServiceContext.GetSessionID(), BankID, LockboxID, StartDate, EndDate, PaymentTypeID, PaymentSourceID, out dt)) decRetVal = (Decimal)dt.Rows[0]["DepositTotal"];
                {
                    dt.Dispose();
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetLockboxTotal");
                decRetVal = (Decimal)0;
            }

            return (decRetVal);
        }

        private int GetTransactionCount(int BankID, int LockboxID, DateTime StartDate, DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID)
        {
            DataTable dt = null;
            int intRetVal = 0;

            try
            {
                if (ItemProcDAL.GetTransactionCount(_ServiceContext.GetSessionID(), BankID, LockboxID, StartDate, EndDate, PaymentTypeID, PaymentSourceID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        intRetVal = (int)dt.Rows[0]["TransactionCount"];
                    }
                    else
                    {
                        intRetVal = 0;
                    }
                }

                dt.Dispose();
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetTransactionCount");
                intRetVal = 0;
            }

            return (intRetVal);
        }

        public XmlDocumentResponse GetBatchTransactions(XmlDocument docReqXml)
        {
            bool bolRetVal = false;
            XmlDocument docRespXml = null;
            XmlDocument objDoc = null;
            XmlNode node;
            XmlNode nodeRS;
            XmlNode nodeRecord;
            int intBankID;
            int intLockboxID = 0;
            int intStartRecord;
            int intRecordCount;
	        int intFilteredCount;
            long lngImageSize;
            bool bolPaginateRS;
            int intCurrentBankID = 0;
            int intCurrentLockboxID = 0;
            long longCurrentBatchID = 0;
            DateTime dteCurrentDepositDate = DateTime.MinValue;
            int intNumDisplayRecords;
            int intBatchCount;
            DateTime dteDepositDate = DateTime.MinValue;
            cOLLockboxes objOLLockboxes;
            DataTable dt = null;
            int y = 0;
            int z = 0;
            long longBatchID;
            int intTemp;
            long longTemp;
            DateTime dteTemp;
            Guid gidTemp;
            cOLLockbox objOLLockbox;
            DataRow dr2;
            XmlDocumentResponse response = null;

            SaveXmlLog(docReqXml, GetRequestIDFromXml(docReqXml), "ReqGetBatchTransactions");

            try
            {
                objDoc = SystemSrv.GetBaseDocument().Data;

                if (docReqXml.DocumentElement.SelectSingleNode("StartRecord") != null && int.TryParse(docReqXml.DocumentElement.SelectSingleNode("StartRecord").InnerText, out intTemp))
                {
                    intStartRecord = intTemp;
                }
                else
                {
                    intStartRecord = 1;
                }

                if (docReqXml.DocumentElement.SelectSingleNode("OLLockboxID") != null && ipoLib.TryParse(docReqXml.DocumentElement.SelectSingleNode("OLLockboxID").InnerText, out gidTemp))
                {
                    objOLLockboxes = LockboxSrv.GetUserLockboxes().Data;
                    objOLLockbox = objOLLockboxes.Values.FirstOrDefault(ollockbox => ollockbox.OLLockboxID == gidTemp);
                }
                else
                {
                    if (docReqXml.DocumentElement.SelectSingleNode("BankID") != null && int.TryParse(docReqXml.DocumentElement.SelectSingleNode("BankID").InnerText, out intTemp))
                    {
                        intBankID = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid BankID"));
                    }

                    if (docReqXml.DocumentElement.SelectSingleNode("LockboxID") != null && int.TryParse(docReqXml.DocumentElement.SelectSingleNode("LockboxID").InnerText, out intTemp))
                    {
                        intLockboxID = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid LockboxID"));
                    }

                    objOLLockboxes = LockboxSrv.GetUserLockboxes().Data;
                    objOLLockbox = objOLLockboxes.Values.FirstOrDefault(ollockbox => ollockbox.BankID == intBankID &&
                                                                                     ollockbox.LockboxID == intLockboxID);
                }

                if (docReqXml.DocumentElement.SelectSingleNode("BatchID") != null && long.TryParse(docReqXml.DocumentElement.SelectSingleNode("BatchID").InnerText, out longTemp))
                {
                    longBatchID = longTemp;
                }
                else
                {
                    longBatchID = -1;
                }

                if (docReqXml.DocumentElement.SelectSingleNode("DepositDate") != null && DateTime.TryParse(docReqXml.DocumentElement.SelectSingleNode("DepositDate").InnerText, out dteTemp))
                {
                    dteDepositDate = dteTemp;
                }
                else
                {
                    dteDepositDate = DateTime.MinValue;
                }

                if (docReqXml.DocumentElement.SelectSingleNode("PaginateRS") == null)
                {
                    bolPaginateRS = true;
                }
                else
                {
                    if (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("PaginateRS").InnerText, out intTemp))
                    {
                        bolPaginateRS = (intTemp == 1);
                    }
                    else
                    {
                        bolPaginateRS = true;
                    }
                }
                
                // if paging is set to false pass in 0 so we get all records. 
                // this is for the printer friendly version...
                if (bolPaginateRS)
                {
                    intNumDisplayRecords = Session.ServerOptions.Preferences.RecordsPerPage;
                }
                else
                {
                    intNumDisplayRecords = 0;
                }

                SystemSrv.UpdateActivityLockbox(ipoXmlLib.GetActivityIDFromXml(docReqXml), objOLLockbox.BankID, objOLLockbox.LockboxID);

                if (dteDepositDate > DateTime.MinValue)
                {
                    if (objOLLockbox != null)
                    {
                        if (ipoDateTimeLib.DateDiff(DateInterval.Day, dteDepositDate.Date, DateTime.Today) > objOLLockbox.OnlineViewingDays)
                        {
                            //check if difference between depositdate and currentdate is greater than lockbox.onlineviewingdays
                            ipoXmlLib.AddMessageToDoc(objDoc, xmlMessageType.xmlMsgInformation, "The information for " + dteDepositDate.ToString("MM/dd/yyyy") + " is not available.");
                        }
                        else
                        {
                            if (ItemProcDAL.GetBatchDetail(_ServiceContext.GetSessionID(), longBatchID, dteDepositDate, Session.ServerOptions.Preferences.DisplayScannedCheckOnline, intStartRecord, intNumDisplayRecords,"", "", "", out intRecordCount, out intFilteredCount, out dt))
                            {

                                bolRetVal = true;

                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    //Create the main Recordset node
                                    nodeRS = ipoXmlLib.addElement(objDoc.DocumentElement, "Recordset");
                                    ipoXmlLib.addAttribute(nodeRS, "Name", "BatchDetail");

                                    //Create a Record node for the lockboxid and depositdate
                                    nodeRecord = ipoXmlLib.addElement(nodeRS, "Record");
                                    ipoXmlLib.addAttribute(nodeRecord, "OLLockboxID", objOLLockbox.OLLockboxID.ToString());
                                    ipoXmlLib.addAttribute(nodeRecord, "LockboxID", objOLLockbox.LockboxID.ToString());
                                    ipoXmlLib.addAttribute(nodeRecord, "LongName", objOLLockbox.LongName);
                                    ipoXmlLib.addAttribute(nodeRecord, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
                                    ipoXmlLib.addAttribute(nodeRecord, "SiteCode", objOLLockbox.SiteCode.ToString());
                                    ipoXmlLib.addAttribute(nodeRecord, "DisplayBatchID", objOLLockbox.DisplayBatchID.ToString());
                                    ipoXmlLib.addAttribute(nodeRecord, "ImportTypeKey", dt.Rows[0]["ImportTypeKey"].ToString());


                                    if (bolPaginateRS)
                                    {
                                        intNumDisplayRecords = Session.ServerOptions.Preferences.RecordsPerPage;
                                    }
                                    else
                                    {
                                        intNumDisplayRecords = dt.Rows.Count;
                                    }

                                    if (dt.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dt.Rows.Count; ++i)
                                        {
                                            dr2 = dt.Rows[i];

                                            if (intCurrentBankID != (int)dr2["BankID"] ||
                                               intCurrentLockboxID != (int)dr2["LockboxID"] ||
                                               longCurrentBatchID != (long)dr2["BatchID"] ||
                                               dteCurrentDepositDate != ((DateTime)dr2["DepositDate"]).Date)
                                            {

                                                //Add totals to previous globalbatchid recordset node
                                                if (nodeRS != null)
                                                {
                                                    ipoXmlLib.addAttribute(nodeRS, "EndRecord", (intStartRecord + dt.Rows.Count - 1).ToString());
                                                    ipoXmlLib.addAttribute(nodeRS, "TotalRecords", intRecordCount.ToString());
                                                    ipoXmlLib.addAttribute(nodeRS, "ImageCount", z.ToString());
                                                    ipoXmlLib.addAttribute(nodeRS, "DisplayBatchTotals", "Y");

                                                    z = 0;
                                                }

                                                //Create Recordset node
                                                nodeRS = ipoXmlLib.addElement(nodeRecord, "Recordset");

                                                ipoXmlLib.addAttribute(nodeRS, "Name", "BatchDetails");
                                                ipoXmlLib.addAttribute(nodeRS, "BankID", dr2["BankID"].ToString());
                                                ipoXmlLib.addAttribute(nodeRS, "LockboxID", dr2["LockboxID"].ToString());
                                                ipoXmlLib.addAttribute(nodeRS, "BatchID", dr2["BatchID"].ToString());
                                                ipoXmlLib.addAttribute(nodeRS, "BatchNumber", dr2["BatchNumber"].ToString());
                                                ipoXmlLib.addAttribute(nodeRS, "DepositDate", ((DateTime)dr2["DepositDate"]).ToString("MM/dd/yyyy"));
                                                ipoXmlLib.addAttribute(nodeRS, "BatchSiteCode", dr2["BatchSiteCode"] != null ? dr2["BatchSiteCode"].ToString() : string.Empty);
                                                ipoXmlLib.addAttribute(nodeRS, "PaymentType", dr2["PaymentType"].ToString());
                                                ipoXmlLib.addAttribute(nodeRS, "PaymentSource", dr2["PaymentSource"].ToString());
                                                ipoXmlLib.addAttribute(nodeRS, "BatchTotal", dr2["BatchTotal"].ToString());
												ipoXmlLib.addAttribute(nodeRS, "BatchSourceShortName", dr2["BatchSourceShortName"].ToString());
												ipoXmlLib.addAttribute(nodeRS, "ImportTypeShortName", dr2["ImportTypeShortName"].ToString());
                                            }

                                            //Add record to nodeRS
                                            node = ipoXmlLib.AddRecordAsNode(nodeRS, dr2, false);

                                            //check if image and add attribute appropriately
                                            if (!dr2.IsNull("BatchSequence"))
                                            {
                                                //TODO: PULL IMAGE SIZE FROM IMAGE XML COLUMN!
                                                lngImageSize = ImageSrv.GetImageSize(int.Parse(dr2["PICSDate"].ToString()), objOLLockbox.BankID, objOLLockbox.LockboxID,
                                                    (long)dr2["BatchID"], (int)dr2["BatchSequence"],
                                                    Convert.ToInt32(dteDepositDate.ToString("yyyyMMdd")), "c", 0, long.Parse(dr2["SourceBatchID"].ToString()), dr2["BatchSourceShortName"].ToString(), dr2["ImportTypeShortName"].ToString()).Data;

                                                ipoXmlLib.addAttribute(node, "ImageSize", lngImageSize.ToString());

                                                if (lngImageSize > 0)
                                                {
                                                    z += 1;
                                                }

                                                lngImageSize = 0;
                                            }
                                            else
                                            {
                                                ipoXmlLib.addAttribute(node, "ImageSize", "0");
                                            }

                                            //Increment documentcount
                                            if ((int)dr2["DocumentCount"] > 0)
                                            {
                                                z += (int)dr2["DocumentCount"];
                                            }

                                            y += 1;

                                            intCurrentBankID = (int)dr2["BankID"];
                                            intCurrentLockboxID = (int)dr2["LockboxID"];
                                            longCurrentBatchID = (long)dr2["BatchID"];
                                            dteCurrentDepositDate = ((DateTime)dr2["DepositDate"]).Date;

                                            if (y == intNumDisplayRecords || i == dt.Rows.Count - 1)
                                            {
                                                if (i < dt.Rows.Count - 1)
                                                {
                                                    //Add appropriate attribute whether or not to display batch totals
                                                    if ((int)dt.Rows[i]["BankID"] == (int)dt.Rows[i + 1]["BankID"] &&
                                                       (int)dt.Rows[i]["LockboxID"] == (int)dt.Rows[i + 1]["LockboxID"] &&
                                                       (long)dt.Rows[i]["BatchID"] == (long)dt.Rows[i + 1]["BatchID"] &&
                                                       (DateTime)dt.Rows[i]["DepositDate"] == (DateTime)dt.Rows[i]["DepositDate"])
                                                    {
                                                        ipoXmlLib.addAttribute(nodeRS, "DisplayBatchTotals", "N");
                                                    }
                                                    else
                                                    {
                                                        ipoXmlLib.addAttribute(nodeRS, "DisplayBatchTotals", "Y");
                                                    }
                                                }
                                                else
                                                {
                                                    ipoXmlLib.addAttribute(nodeRS, "DisplayBatchTotals", "Y");
                                                }

                                                //Always add the Endrecord, TotalRecords, and ImageCount Attributes
                                                ipoXmlLib.addAttribute(nodeRS, "EndRecord", (intStartRecord + dt.Rows.Count - 1).ToString());
                                                ipoXmlLib.addAttribute(nodeRS, "TotalRecords", intRecordCount.ToString());
                                                ipoXmlLib.addAttribute(nodeRS, "ImageCount", z.ToString());

                                                break;
                                            }
                                        }
                                    }

                                    node = ipoXmlLib.AddPageInfo(objDoc.DocumentElement.SelectSingleNode("Recordset"), intStartRecord, intNumDisplayRecords);
                                    ipoXmlLib.addAttribute(node, "MaxPrintableRows", Session.ServerOptions.Preferences.maxPrintableRows.ToString());

                                    if (bolPaginateRS)
                                    {
                                        //Need to update the PageInfo node Attributes with the appropriate Rows.Count and previous/next/last start records
                                        //because we are manually creating the structure and paginating the ADO Recordset
                                        node.SelectSingleNode("@TotalRecords").InnerText = intRecordCount.ToString();
                                        node.SelectSingleNode("@NextStartRecord").InnerText = Pagination.GetNextStartRecord(intStartRecord, Session.ServerOptions.Preferences.RecordsPerPage, intRecordCount).ToString();
                                        node.SelectSingleNode("@PreviousStartRecord").InnerText = Pagination.GetPreviousStartRecord(intStartRecord, Session.ServerOptions.Preferences.RecordsPerPage).ToString();
                                        node.SelectSingleNode("@LastStartRecord").InnerText = Pagination.GetLastStartRecord(Session.ServerOptions.Preferences.RecordsPerPage, intRecordCount).ToString();
                                    }

                                    //Add QueryString values for pagination of the recordset
                                    ipoXmlLib.addAttribute(node, "OLLockboxID", objOLLockbox.OLLockboxID.ToString());
                                    ipoXmlLib.addAttribute(node, "BankID", objOLLockbox.BankID.ToString());
                                    ipoXmlLib.addAttribute(node, "LockboxID", objOLLockbox.LockboxID.ToString());
                                    ipoXmlLib.addAttribute(node, "BatchID", longBatchID.ToString());
                                    ipoXmlLib.addAttribute(node, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));

                                    //Add grand totals if last page displayed, batch count is greater than 1, and a single not a batch request
                                    intBatchCount = GetBatchCount(objOLLockbox.BankID, objOLLockbox.LockboxID, dteDepositDate);

                                    if (intBatchCount > 1 && longBatchID < 0)
                                    {
                                        if (!bolPaginateRS)
                                        {
                                            ipoXmlLib.addAttribute(node, "DisplayPageTotals", "Y");
                                            ipoXmlLib.addAttribute(node, "BatchGrandTotal", GetLockboxTotal(objOLLockbox.BankID, objOLLockbox.LockboxID, dteDepositDate).ToString());
                                        }
                                        else if (((intStartRecord + Session.ServerOptions.Preferences.RecordsPerPage - 1) >= intRecordCount))
                                        {
                                            ipoXmlLib.addAttribute(node, "DisplayPageTotals", "Y");
                                            ipoXmlLib.addAttribute(node, "BatchGrandTotal", GetLockboxTotal(objOLLockbox.BankID, objOLLockbox.LockboxID, dteDepositDate).ToString());
                                        }
                                    }
                                }
                                else
                                {
                                    ipoXmlLib.AddMessageToDoc(objDoc, xmlMessageType.xmlMsgInformation, "No records were found for batch.");
                                }
                            }
                            else
                            {
                                EventLog.logEvent("Request Failed, unable to retrieve BatchDetail information from the database", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                                ipoXmlLib.AddMessageToDoc(objDoc, xmlMessageType.xmlMsgError, "Request Failed Please Retry.");
                            }
                        }
                    }
                    else
                    {
                        //User does not have access to this lockbox
                        ipoXmlLib.AddMessageToDoc(objDoc, xmlMessageType.xmlMsgInformation, "You do not have access to this Lockbox.");
                    }
                }

                docRespXml = objDoc;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchTransactions");

                if (objDoc == null)
                {
                    objDoc = SystemSrv.GetBaseDocument().Data;
                }

                ipoXmlLib.AddMessageToDoc(objDoc, xmlMessageType.xmlMsgError, "An error occurred while retrieving the batch detail.");
                docRespXml = objDoc;
            }
            finally
            {
                if (dt != null)
                {
                    dt.Dispose();
                }
            }

            SaveXmlLog(docRespXml, GetRequestIDFromXml(docReqXml), "RespGetBatchTransactions");


            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        internal bool AuthorizeBatchToUser(int BankID, int LockboxID, long BatchID, DateTime DepositDate, out cOLLockbox objOLLockbox)
        {
            cOLLockboxes objLockboxes;
            bool bolRetVal = false;

            objOLLockbox = null;
            objLockboxes = LockboxSrv.GetUserLockboxes().Data;

            foreach (int id in objLockboxes.Keys)
            {
                objOLLockbox = objLockboxes[id];

                if (objOLLockbox.BankID == BankID && objOLLockbox.LockboxID == LockboxID)
                {
                    bolRetVal = true;
                    break;
                }
            }

            return (bolRetVal);
        }

        private int GetBatchCount(int BankID, int LockboxID, System.DateTime StartDate)
        {
            return (GetBatchCount(BankID, LockboxID, StartDate, StartDate, null, null));
        }

        private int GetBatchCount(int BankID, int LockboxID, System.DateTime StartDate, System.DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID)
        {
            DataTable dt = null;
            int intRetVal = 0;

            try
            {
                if (ItemProcDAL.GetBatchCount(_ServiceContext.GetSessionID(), BankID, LockboxID, StartDate, EndDate, PaymentTypeID, PaymentSourceID, out dt))
                {
                    intRetVal = (int)dt.Rows[0]["BatchCount"];
                }

                dt.Dispose();
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchCount");
                intRetVal = 0;
            }

            return (intRetVal);
        }

        public BaseResponse CheckLockboxPermission(Guid OLLockboxID)
        {
            bool bolRetVal = false;
            BaseResponse response = null;

            if (LockboxSrv.GetLockboxByID(OLLockboxID) != null)
            {
                bolRetVal = true;
            }

            response = new BaseResponse()
            {
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        /// <summary>
        /// Deprecated - Use BatchSummaryRepository instead.
        /// </summary>
        /// <param name="docReqXml"></param>
        /// <returns></returns>
        public XmlDocumentResponse GetBatchSummary(XmlDocument docReqXml)
        {
            //BDH 6/6/2014
            Guid iPOnlineSessionId = Guid.Empty;

            DataTable dtTotals;
            DataTable dt = null;
            DataRow dr;
            XmlDocument docRespXml = null;
            XmlNode nodeRS;
            XmlNode nodeRecord;
            XmlNode nodeTemp;
            int bankID = -1;
            int clientAccountID = -1;
            DateTime dteStartDate = DateTime.MinValue;
            DateTime dteEndDate = DateTime.MinValue;
            int lngStartRecord = 0;
            int? intPaymentTypeID = null;
            int? intPaymentSourceID = null;
            bool bolPaginateRS;
            bool blnError = false;
            cOLLockboxes objOLWorkgroups;
            cOLLockbox objOLLockbox = null;
            DateTime dteProcessingDate;
            int intTemp;
            XmlDocumentResponse response = null;
            bool excludeValidation = false;

            SaveXmlLog(docReqXml, GetRequestIDFromXml(docReqXml), "ReqGetBatchSummary");

            try
            {

                // Extract parameters from input document

                // CR 11213 EJG 01/07/2005 - Made changes for CR 9347 that were not checked in.
                //Get the base XML document
                docRespXml = SystemSrv.GetBaseDocument().Data;

                nodeTemp = docReqXml.DocumentElement.SelectSingleNode("ExcludeValidation");

                if (nodeTemp != null)
                {
                    if (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("ExcludeValidation").InnerText, out intTemp))
                    {
                        excludeValidation = (intTemp == 1);
                    }
                }

                nodeTemp = docReqXml.DocumentElement.SelectSingleNode("BankID");

                if (nodeTemp != null)
                {
                    bankID = int.Parse(nodeTemp.InnerText);
                }

                nodeTemp = docReqXml.DocumentElement.SelectSingleNode("ClientAccountID");

                if (nodeTemp != null)
                {
                    clientAccountID = int.Parse(nodeTemp.InnerText);
                }

                if ((bankID == -1  || clientAccountID == -1 ) && (!excludeValidation))
                {
                    ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "Invalid Workgroup");
                    blnError = true;
                }

                nodeTemp = docReqXml.DocumentElement.SelectSingleNode("StartDate");

                if (nodeTemp != null)
                {
                    dteStartDate = DateTime.Parse(nodeTemp.InnerText);

                    if (dteStartDate < DateTime.Parse("01/01/1900"))
                    {
                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "Date From must be greater than or equal to 1/1/1900.");
                        blnError = true;
                    }
                }

                nodeTemp = docReqXml.DocumentElement.SelectSingleNode("EndDate");

                if ((nodeTemp != null))
                {
                    dteEndDate = DateTime.Parse(nodeTemp.InnerText);

                    if (dteEndDate < DateTime.Parse("01/01/1900"))
                    {
                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "Date To must be greater than or equal to 1/1/1900.");
                        blnError = true;
                    }
                }

                if (ipoDateTimeLib.DateDiff(DateInterval.Day, dteStartDate, dteEndDate) < 0)
                {
                    ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "When specifying a Deposit Date range, the Date From cannot be greater than the Date To.");
                    blnError = true;
                }

                nodeTemp = docReqXml.DocumentElement.SelectSingleNode("StartRecord");

                if ((nodeTemp != null))
                {
                    lngStartRecord = int.Parse(nodeTemp.InnerText);
                }

                if (lngStartRecord <= 0)
                {
                    lngStartRecord = 1;
                }

                nodeTemp = docReqXml.DocumentElement.SelectSingleNode("PaymentTypeID");

                if (nodeTemp != null)
                {
                    if (int.Parse(nodeTemp.InnerText) != -1)
                    {
                        intPaymentTypeID = int.Parse(nodeTemp.InnerText);
                    }
                }

                nodeTemp = docReqXml.DocumentElement.SelectSingleNode("PaymentSourceID");

                if (nodeTemp != null)
                {
                    if (int.Parse(nodeTemp.InnerText) != -1)
                    {
                        intPaymentSourceID = int.Parse(nodeTemp.InnerText);
                    }
                }

                if (docReqXml.DocumentElement.SelectSingleNode("PaginateRS") == null)
                {
                    bolPaginateRS = true;
                }
                else
                {
                    if (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("PaginateRS").InnerText, out intTemp))
                    {
                        bolPaginateRS = (intTemp == 1);
                    }
                    else
                    {
                        bolPaginateRS = true;
                    }
                }

                // Add user lockbox recordset to return document
                objOLWorkgroups = LockboxSrv.GetUserLockboxes().Data;

                var sortedDic = from entry in objOLWorkgroups
                                orderby entry.Value.LockboxID
                                select entry;

                foreach (KeyValuePair<int, cOLLockbox> objTempLockbox in sortedDic)
                {
                    nodeTemp = ipoXmlLib.AddFormFieldAsNode(docRespXml.DocumentElement, "frmMain", "cboOLLockbox", objTempLockbox.Value.OLWorkGroupsID.ToString(), "OPTION");
                    ipoXmlLib.addAttribute(nodeTemp, "BankID", objTempLockbox.Value.BankID.ToString());
                    ipoXmlLib.addAttribute(nodeTemp, "LockboxID", objTempLockbox.Value.LockboxID.ToString());
                    ipoXmlLib.addAttribute(nodeTemp, "ShortName", objTempLockbox.Value.ShortName);
                    ipoXmlLib.addAttribute(nodeTemp, "LongName", objTempLockbox.Value.LongName);
                }

                Dictionary<int, string> paymentTypes = LockboxSrv.GetPaymentTypes().Data;

                foreach (var paymentType in paymentTypes)
                {
                    nodeTemp = ipoXmlLib.AddFormFieldAsNode(docRespXml.DocumentElement, "frmMain", "cboPaymentTypes", paymentType.Key.ToString(), "OPTION");
                    ipoXmlLib.addAttribute(nodeTemp, "PaymentTypeID", paymentType.Key.ToString());
                    ipoXmlLib.addAttribute(nodeTemp, "PaymentType", paymentType.Value);
                }

                if (!blnError && !excludeValidation)
                {
                    objOLLockbox = objOLWorkgroups.FirstOrDefault(x => (x.Value is cOLLockbox)  && ((x.Value).BankID == bankID && (x.Value).LockboxID == clientAccountID)).Value;

                    //retrieve specified user lockbox
                    //objOLLockbox = objOLWorkgroups[Convert.ToInt32(strOLWorkGroupID)];

                    //Validate that user has access to lockbox
                    if (objOLLockbox != null)
                    {
                        SystemSrv.UpdateActivityLockbox(ipoXmlLib.GetActivityIDFromXml(docReqXml), objOLLockbox.BankID, objOLLockbox.LockboxID);

                        // cr 21133 jcs 09/25/2007 - check that deposit date range doesn't exceed maximum search days
                        if (objOLLockbox.MaximumSearchDays > 0 && ipoDateTimeLib.DateDiff(DateInterval.Day, dteStartDate.Date, dteEndDate.Date) >= objOLLockbox.MaximumSearchDays)
                        {
                            blnError = true;

                            ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation,
                                string.Format("The Deposit Date range is limited to {0} day(s) when searching this workgroup.",
                                    objOLLockbox.OnlineViewingDays > objOLLockbox.MaximumSearchDays
                                        ? objOLLockbox.MaximumSearchDays
                                        : objOLLockbox.OnlineViewingDays));
                        }

                        // Check that date range values do not exceed lockbox viewing days
                        if (ipoDateTimeLib.DateDiff(DateInterval.Day, dteStartDate.Date, DateTime.Today) > objOLLockbox.OnlineViewingDays)
                        {
                            dteStartDate = DateTime.Today.AddDays(objOLLockbox.OnlineViewingDays * -1);
                        }

                        // Check that date range values do not exceed processing date
                        dteProcessingDate = objOLLockbox.CurrentProcessingDate;


                        //CR 11062 EJG 01/07/2005
                        //Exit gracefully if using cutoff
                        if (Session.ServerOptions.Preferences.UseCutoff )
                        {
                            //The start date is greater than or equal to the processing date and the lockbox is not cutoff and
                            //the customer is enforcing cutoff logic. No results should be returned so do not execute search.
                            blnError = true;

                            ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation,
                                                      "No records were found matching the criteria specified.");
                        }

                        if (!blnError)
                        {
                            //Create the base node
                            dtTotals = new DataTable();

                            dtTotals.Columns.Add("OLLockboxID", Type.GetType("System.String"));
                            dtTotals.Columns.Add("BankID", Type.GetType("System.Int32"));
                            dtTotals.Columns.Add("LockboxID", Type.GetType("System.Int32"));
                            dtTotals.Columns.Add("LongName", Type.GetType("System.String"));
                            dtTotals.Columns.Add("SiteCode", Type.GetType("System.Int32"));
                            dtTotals.Columns.Add("BatchCount", Type.GetType("System.Int32"));
                            dtTotals.Columns.Add("TransactionCount", Type.GetType("System.Int32"));
                            dtTotals.Columns.Add("CheckCount", Type.GetType("System.Int32"));
                            dtTotals.Columns.Add("DocumentCount", Type.GetType("System.Int32"));
                            dtTotals.Columns.Add("BatchTotal", Type.GetType("System.Double"));
                            dtTotals.Columns.Add("StartDate", Type.GetType("System.DateTime"));
                            dtTotals.Columns.Add("EndDate", Type.GetType("System.DateTime"));
                            dtTotals.Columns.Add("DisplayBatchID", Type.GetType("System.Boolean"));

                            dr = dtTotals.NewRow();

                            dr["OLLockboxID"] = objOLLockbox.OLLockboxID;
                            dr["BankID"] = objOLLockbox.BankID;
                            dr["LockboxID"] = objOLLockbox.LockboxID;
                            dr["LongName"] = objOLLockbox.LongName;
                            dr["SiteCode"] = objOLLockbox.SiteCode;
                            dr["BatchCount"] = GetBatchCount(objOLLockbox.BankID, objOLLockbox.LockboxID, dteStartDate, dteEndDate, intPaymentTypeID, intPaymentSourceID);
                            dr["TransactionCount"] = GetTransactionCount(objOLLockbox.BankID, objOLLockbox.LockboxID, dteStartDate, dteEndDate, intPaymentTypeID, intPaymentSourceID);
                            dr["CheckCount"] = GetCheckCount(objOLLockbox.BankID, objOLLockbox.LockboxID, dteStartDate, dteEndDate, intPaymentTypeID, intPaymentSourceID);
                            dr["DocumentCount"] = GetDocumentCount(objOLLockbox.BankID, objOLLockbox.LockboxID, dteStartDate, dteEndDate, intPaymentTypeID, intPaymentSourceID);
                            dr["BatchTotal"] = GetLockboxTotal(objOLLockbox.BankID, objOLLockbox.LockboxID, dteStartDate, dteEndDate, intPaymentTypeID, intPaymentSourceID);
                            dr["StartDate"] = dteStartDate;
                            dr["EndDate"] = dteEndDate;
                            dr["DisplayBatchID"] = objOLLockbox.DisplayBatchID;

                            dtTotals.Rows.Add(dr);

                            try
                            {
                                var totalrecords = 0;
                                var filteredrecords = 0;
                                if (ItemProcDAL.GetBatchSummary(Session.SessionID, objOLLockbox.BankID,
                                                               objOLLockbox.LockboxID,
                                                               dteStartDate,
                                                               dteEndDate,
                                                               intPaymentTypeID,
                                                               intPaymentSourceID,
                                                               Session.ServerOptions.Preferences.DisplayScannedCheckOnline,
                                                               lngStartRecord,
                                                               100,             //the max number of lines to bring back
                                                               string.Empty,
                                                               string.Empty,
                                                               string.Empty,
                                                               out totalrecords,
                                                               out filteredrecords,
                                                               out dt,
                                                               out int transactionRecords,
                                                               out int checkRecords,
                                                               out int documentRecords,
                                                               out decimal checkTotal))
                                {

                                    if (dt.Rows.Count > 0)
                                    {
                                        // Add totals recordset to document
                                        nodeRS = ipoXmlLib.AddRecordsetToDoc(docRespXml, dtTotals, "BatchSummary", null, null, 1);
                                        dtTotals.Dispose();

                                        nodeRecord = ipoXmlLib.AddRecordsetToNode(nodeRS.SelectSingleNode("Record"),
                                                                                  dt,
                                                                                  "BatchDetails",
                                                                                  null,
                                                                                  null,
                                                                                  0,
                                                                                  0,
                                                                                  false);
                                        dt.Dispose();

                                        //Determine if totals are displayed
                                        if (!bolPaginateRS)
                                        {
                                            ipoXmlLib.addAttribute(nodeRS.SelectSingleNode("Record"), "DisplayPageTotals", "Y");
                                        }
                                        else if (int.Parse(nodeRecord.SelectSingleNode("@EndRecord").InnerText) == int.Parse(nodeRecord.SelectSingleNode("@TotalRecords").InnerText))
                                        {
                                            ipoXmlLib.addAttribute(nodeRS.SelectSingleNode("Record"), "DisplayPageTotals", "Y");
                                        }
                                        else
                                        {
                                            ipoXmlLib.addAttribute(nodeRS.SelectSingleNode("Record"), "DisplayPageTotals", "N");
                                        }

                                        //Calculate paging information and add PageInfo node to nodeRS
                                        XmlNode xmnPageInfo = ipoXmlLib.AddPageInfo(nodeRS, lngStartRecord, Session.ServerOptions.Preferences.RecordsPerPage);

                                        ipoXmlLib.addAttribute(xmnPageInfo, "MaxPrintableRows", Session.ServerOptions.Preferences.maxPrintableRows.ToString());
                                    }
                                    else
                                    {
                                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation,
                                                                  "No records were found matching the criteria specified.");
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                throw;
                            }
                            finally
                            {
                                if (dtTotals != null)
                                {
                                    dtTotals.Dispose();
                                }
                            }
                        }
                    }
                    else
                    {
                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation,
                                                  "You do not have access to this Lockbox.");
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetBatchSummary");

                if (docRespXml == null)
                {
                    docRespXml = SystemSrv.GetBaseDocument().Data;
                }

                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation,
                                          "An error occurred while retrieving the batch summary information. Please verify the Deposit Date range and Lockbox and try again.");
            }

            SaveXmlLog(docRespXml, GetRequestIDFromXml(docReqXml), "RespGetBatchSummary");

            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = StatusCode.SUCCESS
            };

            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.AppendChild(xmlDoc.ImportNode(docRespXml, true));

            return (response);
        }

        private int GetCheckCount(int BankID, int LockboxID, DateTime StartDate, DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID)
        {
            DataTable dt;
            DataRow dr;
            int intRetVal = 0;

            try
            {
                if (ItemProcDAL.GetCheckCount(_ServiceContext.GetSessionID(), BankID, LockboxID, StartDate, EndDate, PaymentTypeID, PaymentSourceID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        dr = dt.Rows[0];
                        intRetVal = ipoLib.NVL(ref dr, "CheckCount", 0);
                    }
                }

                dt.Dispose();
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetCheckCount");
                intRetVal = 0;
            }

            return (intRetVal);
        }

        private int GetDocumentCount(int BankID, int LockboxID, System.DateTime StartDate, System.DateTime EndDate, int? PaymentTypeID, int? PaymentSourceID)
        {
            DataTable dt;
            DataRow dr;
            int intRetVal = 0;

            try
            {
                if (ItemProcDAL.GetDocumentCount(_ServiceContext.GetSessionID(), BankID, LockboxID, StartDate, EndDate, Session.ServerOptions.Preferences.DisplayScannedCheckOnline, PaymentTypeID, PaymentSourceID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        dr = dt.Rows[0];
                        intRetVal = ipoLib.NVL(ref dr, "DocumentCount", 0);
                    }
                }

                dt.Dispose();
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetDocumentCount");
            }

            return (intRetVal);
        }
    }
}
