﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.OLFServicesClient;


/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
 * * WI 165464 SAS 09/15/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class ReportAPI : BaseOLAPI
    {
        private CBXAPI _CBXSrv;
        private cItemProcDAL _ItemProcDAL = null;
        private cReportDAL _ReportDAL = null;
        private SystemAPI _SystemSrv;

        public ReportAPI(R360ServiceContext serviceContext)
            : base(serviceContext)
        {
        }

        /// <summary>
        /// Retrieves reference to CBXSrv service
        /// </summary>
        private CBXAPI CBXSrv
        {
            get
            {
                if (_CBXSrv == null)
                {
                    _CBXSrv = new CBXAPI(_serviceContext);
                    _CBXSrv.Session = Session;
                }

                return (_CBXSrv);
            }
        }
        private cItemProcDAL ItemProcDAL
        {
            get
            {
                if (_ItemProcDAL == null)
                {
                    _ItemProcDAL = new cItemProcDAL(this.SiteKey);
                }

                return (_ItemProcDAL);
            }
        }
        private cReportDAL ReportDAL
        {
            get
            {
                if (_ReportDAL == null)
                {
                    _ReportDAL = new cReportDAL(this.SiteKey);
                }
                return (_ReportDAL);
            }
        }
        protected override RecHub.Common.ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acR360WebServiceCall;
            }
        }
        /// <summary>
        /// Retrieves reference to SystemSrv service
        /// </summary>
        private SystemAPI SystemSrv
        {
            get
            {
                if (_SystemSrv == null)
                {
                    _SystemSrv = new SystemAPI(_serviceContext);
                    _SystemSrv.Session = Session;
                }

                return (_SystemSrv);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !Disposed)
            {
                if (_CBXSrv != null)
                {
                    _CBXSrv.Dispose();
                }
                if (_ItemProcDAL != null)
                {
                    _ItemProcDAL.Dispose();
                }
                if (_ReportDAL != null)
                {
                    _ReportDAL.Dispose();
                }
                if (_SystemSrv != null)
                {
                    _SystemSrv.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        protected override bool ValidateSID(out int userID)
        {
            // TODO: Implement this; it will be called from the standard wrapper methods...
            throw new NotImplementedException();
        }

        public XmlDocumentResponse GetHOAParameters(XmlDocument docReqXml)
        {
            bool bolRetVal = false;
            XmlDocument docRespXml = null;
            string strReportName = "";
            int intCustomerID = -1;
            int intLockboxID = -1;
            int intTemp;
            DateTime dteDepositDate;
            DateTime dteTemp;
            DataTable dt = null;
            XmlDocumentResponse response = null;

            SaveXmlLog(docReqXml, GetRequestIDFromXml(docReqXml), "ReqGetHOAParameters");

            try
            {
                //Retrieve report name
                if ((docReqXml.DocumentElement.SelectSingleNode("ReportName") != null))
                {
                    strReportName = docReqXml.DocumentElement.SelectSingleNode("ReportName").InnerText;
                }
                else
                {
                    throw (new Exception("Invalid Report Name"));
                }

                //Retrieve Deposit Date parameter from the parameter Xml document.
                if (docReqXml.DocumentElement.SelectSingleNode("DepositDate") == null)
                {
                    dteDepositDate = DateTime.Now.Date;
                }
                else
                {
                    if (DateTime.TryParse(docReqXml.DocumentElement.SelectSingleNode("DepositDate").InnerText, out dteTemp))
                    {
                        dteDepositDate = dteTemp;
                    }
                    else
                    {
                        dteDepositDate = DateTime.Now.Date;
                    }
                }

                //Retrieve customerid
                if ((docReqXml.DocumentElement.SelectSingleNode("CustomerID") != null) && (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("CustomerID").InnerText, out intTemp)))
                {
                    intCustomerID = intTemp;
                }

                //Retrieve lockboxid
                if ((docReqXml.DocumentElement.SelectSingleNode("LockboxID") != null) && (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("LockboxID").InnerText, out intTemp)))
                {
                    intLockboxID = intTemp;
                }

                docRespXml = SystemSrv.GetBaseDocument().Data;

                switch (strReportName)
                {
                    case "HOAAllPMASummary":

                        // get customerID
                        if (ReportDAL.GetHOAPMACustomers(int.Parse(dteDepositDate.ToString("yyyyMMdd")), Session.SessionUser.UserID, out dt))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                ipoXmlLib.AddRecordsetToDoc(docRespXml, dt, "HOAPMACustomers");
                                bolRetVal = true;
                            }
                            else
                            {
                                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "There are no records for this Property Manager");
                            }
                        }
                        else
                        {
                            EventLog.logEvent("Unable to retrieve information from the database", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                            ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while retrieving customerId data for the All Poperty Management Summary Report.");
                        }
                        break;
                    case "HOAPMADetail":

                        // get HOA Name
                        if (ReportDAL.GetHOAPMAHOANames(int.Parse(dteDepositDate.ToString("yyyyMMdd")), Session.SessionUser.UserID, intCustomerID, intLockboxID, out dt))
                        {
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                ipoXmlLib.AddRecordsetToDoc(docRespXml, dt, "HOAPMAHOANames");
                                bolRetVal = true;
                            }
                            else
                            {
                                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "There are no records for this Property Manager");
                            }
                        }
                        else
                        {
                            EventLog.logEvent("Unable to retrieve information from the database", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                            ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while retrieving HOA Names for the Poperty Management Detail Report.");
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetHOAParameters");

                // Add error message to return document.
                if (docRespXml == null)
                {
                    docRespXml = SystemSrv.GetBaseDocument().Data;
                }

                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while retrieving Poperty Management Report Parameters. ");
            }

            SaveXmlLog(docRespXml, GetRequestIDFromXml(docReqXml), "RespGetHOAPMASummary");

            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public XmlDocumentResponse GetHOAPMASummary(XmlDocument docReqXml)
        {
            bool bolRetVal = false;
            DataTable dt = null;
            DataTable dtDetail = null;
            XmlDocument docRespXml = null;
            XmlDocument objRetVal = null;
            DateTime dteDepositDate;
            DateTime dteTemp;
            int intLockboxID = -1;
            int intCustomerID;
            int intTemp;
            decimal decTemp;
            decimal decTotalAmount = 0;
            int intTotalCount = 0;
            XmlNode node;
            XmlNode subnode;
            bool bAllCompanies = true; // Default to run report for all Management Companies
            XmlDocumentResponse response = null;

            SaveXmlLog(docReqXml, GetRequestIDFromXml(docReqXml), "ReqGetHOAPMASummary");

            try
            {
                //Retrieve Deposit Date parameter from the parameter Xml document.
                if (docReqXml.DocumentElement.SelectSingleNode("DepositDate") == null)
                {
                    dteDepositDate = DateTime.Now.Date;
                }
                else
                {
                    if (DateTime.TryParse(docReqXml.DocumentElement.SelectSingleNode("DepositDate").InnerText, out dteTemp))
                    {
                        dteDepositDate = dteTemp;
                    }
                    else
                    {
                        dteDepositDate = DateTime.Now.Date;
                    }
                }

                //Retrieve CustomerID parameter from the parameter XML document.
                if ((docReqXml.DocumentElement.SelectSingleNode("CustomerID") != null) && (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("CustomerID").InnerText, out intTemp)))
                {
                    intCustomerID = intTemp;
                }
                else
                {
                    throw (new Exception("Invalid CustomerID"));
                }

                //Retrieve LockboxID parameter from the parameter XML document.
                if ((docReqXml.DocumentElement.SelectSingleNode("LockboxID") != null) && (docReqXml.DocumentElement.SelectSingleNode("LockboxID").InnerText != ""))
                {
                    bAllCompanies = false; // Run report for specific lockbox (Management Company)

                    if (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("LockboxID").InnerText, out intTemp))
                    {
                        intLockboxID = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid LockboxID"));
                    }
                }

                docRespXml = SystemSrv.GetBaseDocument().Data;

                // Validate input data
                if (dteDepositDate < DateTime.Parse("1/1/1900"))
                {
                    ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "InValid Processing Date.");
                }

                // Retrieve data
                // Get Header information
                if (ReportDAL.GetHOAPMASummaryHeader(intCustomerID, intLockboxID, int.Parse(dteDepositDate.ToString("yyyyMMdd")), Session.SessionUser.UserID, out dt))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        // Add Input Information
                        // Store inputs into xml doc.
                        node = ipoXmlLib.addElement((docRespXml.DocumentElement), "Inputs", string.Empty);

                        ipoXmlLib.addAttribute(node, "CustomerID", intCustomerID.ToString());
                        ipoXmlLib.addAttribute(node, "LockboxID", intLockboxID.ToString());
                        ipoXmlLib.addAttribute(node, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));

                        // Add header info
                        node = ipoXmlLib.addElement((docRespXml.DocumentElement), "Recordset", string.Empty);

                        ipoXmlLib.addAttribute(node, "Name", "HOAPMAReport");

                        foreach (DataRow dr in dt.Rows)
                        {
                            subnode = ipoXmlLib.addElement(node, "Lockbox", string.Empty);

                            ipoXmlLib.addAttribute(subnode, "CustomerName", dr["Customer_Name"].ToString());
                            ipoXmlLib.addAttribute(subnode, "LockboxName", dr["Lockbox_Name"].ToString());
                            ipoXmlLib.addAttribute(subnode, "LockboxNumber", dr["Lockbox_Number"].ToString());
                            ipoXmlLib.addAttribute(subnode, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));

                            // Call detail information
                            if (ReportDAL.GetHOAPMASummaryData(intCustomerID, int.Parse(dr["Lockbox_Number"].ToString()), int.Parse(dteDepositDate.ToString("yyyyMMdd")), Session.SessionUser.UserID, out dtDetail))
                            {
                                if (dtDetail != null && dtDetail.Rows.Count > 0)
                                {
                                    if (int.TryParse(dtDetail.Compute("Sum(StubCount)", string.Empty).ToString(), out intTemp))
                                    {
                                        intTotalCount += intTemp;
                                    }
                                    else
                                    {
                                        intTemp = 0;
                                    }
                                    if (decimal.TryParse(dtDetail.Compute("Sum(StubAmount)", string.Empty).ToString(), out decTemp))
                                    {
                                        decTotalAmount += decTemp;
                                    }
                                    else
                                    {
                                        decTemp = 0;
                                    }

                                    ipoXmlLib.addAttribute(subnode, "TotalTransactionCount", intTemp.ToString());
                                    ipoXmlLib.addAttribute(subnode, "TotalTransactionAmount", decTemp.ToString());
                                    ipoXmlLib.AddRecordsetToNode(subnode, dtDetail, bAllCompanies == true ? "HOAPMAAllSummaryData" : "HOAPMASummaryData", "", "", 1, 0, false);
                                    bolRetVal = true;
                                }
                                else
                                {
                                    ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "There are no records for this Property Manager");
                                }
                            }
                            else
                            {
                                EventLog.logEvent("An error has occurred in usp_GetHOAPMASummaryData.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);

                                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while retrieving detail data for the Poperty Management Summary Report.");
                            }
                        }

                        ipoXmlLib.addAttribute(node, "GrandTotalCount", intTotalCount.ToString());
                        ipoXmlLib.addAttribute(node, "GrandTotalAmount", decTotalAmount.ToString());

                    }
                    else
                    {
                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "There are no records for this Property Manager");
                    }
                    if (dt != null)
                    {
                        dt.Dispose();
                    }
                }
                else
                {
                    EventLog.logEvent("Unable to retrieve information from the database", this.GetType().Name, MessageType.Error, MessageImportance.Essential);

                    ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while retrieving detail data for the Poperty Management Summary Report.");
                }

                objRetVal = docRespXml;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetHOAPMASummary");
                // Add error message to return document.

                if (docRespXml == null)
                {
                    docRespXml = SystemSrv.GetBaseDocument().Data;
                }

                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while retrieving Poperty Management Summary Report. Please verify the parameters and try again.");
            }

            SaveXmlLog(docRespXml, GetRequestIDFromXml(docReqXml), "RespGetHOAPMASummary");

            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public XmlDocumentResponse GetHOAPMADetail(XmlDocument docReqXml)
        {
            bool bolRetVal = false;
            DataTable dt = null;
            XmlDocument docRespXml = null;
            XmlDocument objRetVal = null;
            XmlNode node = null;
            DateTime dteDepositDate;
            DateTime dteTemp;
            int intLockboxID = -1;
            int intCustomerID;
            int intTemp;
            string strHOA_Number = "";
            bool bContinue = true;
            XmlDocumentResponse response = null;

            SaveXmlLog(docReqXml, GetRequestIDFromXml(docReqXml), "ReqGetHOAPMADetail");

            try
            {
                //Retrieve Processing Date parameter from the parameter Xml document.
                if (docReqXml.DocumentElement.SelectSingleNode("DepositDate") == null)
                {
                    dteDepositDate = DateTime.Now.Date;
                }
                else
                {
                    if (DateTime.TryParse(docReqXml.DocumentElement.SelectSingleNode("DepositDate").InnerText, out dteTemp))
                    {
                        dteDepositDate = dteTemp;
                    }
                    else
                    {
                        dteDepositDate = DateTime.Now.Date;
                    }
                }

                //Retrieve CustomerID parameter from the parameter XML document.
                if ((docReqXml.DocumentElement.SelectSingleNode("CustomerID") != null) && (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("CustomerID").InnerText, out intTemp)))
                {
                    intCustomerID = intTemp;
                }
                else
                {
                    throw (new Exception("Invalid CustomerID"));
                }

                //Retrieve LockboxID parameter from the parameter XML document.
                if (docReqXml.DocumentElement.SelectSingleNode("LockboxID") != null)
                {
                    if (int.TryParse(docReqXml.DocumentElement.SelectSingleNode("LockboxID").InnerText, out intTemp))
                    {
                        intLockboxID = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid LockboxID"));
                    }
                }

                //Retrieve Stub Account - ie HOA_Number
                if ((docReqXml.DocumentElement.SelectSingleNode("HOA_Number") != null))
                {
                    strHOA_Number = docReqXml.DocumentElement.SelectSingleNode("HOA_Number").InnerText;
                }
                else
                {
                    throw (new Exception("Invalid HOA_Number"));
                }

                docRespXml = SystemSrv.GetBaseDocument().Data;

                // Validate input data
                if (dteDepositDate < DateTime.Parse("1/1/1900"))
                {
                    ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "InValid Processing Date.");
                }

                // Store inputs into xml doc.
                node = ipoXmlLib.addElement((docRespXml.DocumentElement), "Inputs", string.Empty);

                ipoXmlLib.addAttribute(node, "CustomerID", intCustomerID.ToString());
                ipoXmlLib.addAttribute(node, "LockboxID", intLockboxID.ToString());
                ipoXmlLib.addAttribute(node, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
                ipoXmlLib.addAttribute(node, "HOA_Number", strHOA_Number);

                // Retrieve data
                // Get Header information
                if (ReportDAL.GetHOAPMADetailHeader(intCustomerID, intLockboxID, int.Parse(dteDepositDate.ToString("yyyyMMdd")), Session.SessionUser.UserID, strHOA_Number, out dt))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        // Add recordset to return XML document
                        ipoXmlLib.AddRecordsetToDoc(docRespXml, dt, "HOAPMADetailHeader");

                        bolRetVal = true;
                    }
                    else
                    {
                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "There are no records for this HOA Account");

                        bContinue = false;
                    }
                    if (dt != null)
                    {
                        dt.Dispose();
                    }
                }
                else
                {
                    EventLog.logEvent("Unable to retrieve header information from the database", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    bContinue = false;
                }

                // Get Data information
                if ((bContinue) && (ReportDAL.GetHOAPMADetailData(intCustomerID, intLockboxID, int.Parse(dteDepositDate.ToString("yyyyMMdd")), Session.SessionUser.UserID, strHOA_Number, out dt)))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        // Add recordset to return XML document
                        ipoXmlLib.AddRecordsetToDoc(docRespXml, dt, "HOAPMADetailData");
                    }
                    else
                    {
                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "There are no records for this HOA Account");

                        bContinue = false;
                    }
                    if (dt != null)
                    {
                        dt.Dispose();
                    }
                }
                else
                {
                    EventLog.logEvent("Unable to retrieve detail information from the database", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    bContinue = false;
                }

                // Get footer information
                if ((bContinue) && (ReportDAL.GetHOAPMADetailFooter(intCustomerID, intLockboxID, int.Parse(dteDepositDate.ToString("yyyyMMdd")), Session.SessionUser.UserID, strHOA_Number, out dt)))
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        // Add recordset to return XML document
                        ipoXmlLib.AddRecordsetToDoc(docRespXml, dt, "HOAPMADetailFooter");
                    }
                    else
                    {
                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "There are no records for this Property Manager HOA Account");

                        bContinue = false;
                    }
                    if (dt != null)
                    {
                        dt.Dispose();
                    }
                }
                else
                {
                    EventLog.logEvent("Unable to retrieve footer information from the database", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    bContinue = false;
                }

                objRetVal = docRespXml;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetHOAPMADetail");

                // Add error message to return document.
                if (docRespXml == null)
                {
                    docRespXml = SystemSrv.GetBaseDocument().Data;
                }

                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while retrieving Property Management Detail Report. Please verify the parameters and try again.");
            }

            SaveXmlLog(docRespXml, GetRequestIDFromXml(docReqXml), "RespGetHOAPMADetail");

            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<DataSet> CreateReportJobRequest(XmlDocument docParms)
        {
            int intBankID = -1;
            int intBatchID = -1;
            int intBatchNumber = -1;
            int intLockboxID = -1;
            int intRowsAffected;
            int intTemp;
            int intTransactionID = -1;
            int intBatchSequence = -1;
            cOLLockbox objOLLockbox = null;
            DataSet dsRetVal = null;

            string strFileDisplayName = "";
            string strPICSDate = "";
            string strReportName = "";
            DateTime dteDepositDate;
            DateTime dteProcessingDate;
            DateTime dteTemp;
            Guid gidOnlineImageQueueID;
            XmlDocument objXMLOut;
            XmlNode nodejobRequest;
            XmlNode nodeReport;
            XmlNode nodeReports;
            XmlNode nodeRequestReports;
            BaseGenericResponse<DataSet> response = null;
            try
            {
                gidOnlineImageQueueID = Guid.NewGuid();
                objXMLOut = ipoXmlLib.GetXMLDoc("ipoServiceRequest");
                nodejobRequest = ipoXmlLib.addElement(objXMLOut.DocumentElement, "jobRequest", string.Empty);
                ipoXmlLib.addAttribute(nodejobRequest, "jobID", gidOnlineImageQueueID.ToString());
                ipoXmlLib.addAttribute(nodejobRequest, "jobRequestType", "REPORT");
                ipoXmlLib.addAttribute(nodejobRequest, "siteKey", base.SiteKey);
                ipoXmlLib.addAttribute(nodejobRequest, "sessionID", base.SessionID.ToString());

                nodeReports = ipoXmlLib.addElement(nodejobRequest, "Reports", string.Empty);
                nodeReport = ipoXmlLib.addElement(nodeReports, "Report", string.Empty);
                nodeRequestReports = docParms.DocumentElement.SelectSingleNode("Reports");

                if (nodeRequestReports != null && nodeRequestReports.ChildNodes.Count>0)
                {
                    XmlNode nodeTemp=nodeRequestReports.ChildNodes[0];

                        if (nodeTemp.SelectSingleNode("ReportName") != null)
                        {
                            strReportName = nodeTemp.SelectSingleNode("ReportName").InnerText;
                        }
                        else
                        {
                            throw (new Exception("Invalid Report Name"));
                        }
                        if (nodeTemp.Attributes["BankID"] != null && int.TryParse(nodeTemp.Attributes["BankID"].InnerText, out intTemp))
                        {
                            intBankID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BankID"));
                        }

                        if (nodeTemp.Attributes["LockboxID"] != null && int.TryParse(nodeTemp.Attributes["LockboxID"].InnerText, out intTemp))
                        {
                            intLockboxID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid LockboxID"));
                        }

                        if (nodeTemp.Attributes["BatchID"] != null && int.TryParse(nodeTemp.Attributes["BatchID"].InnerText, out intTemp))
                        {
                            intBatchID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchID"));
                        }

                        if (nodeTemp.Attributes["BatchNumber"] != null && int.TryParse(nodeTemp.Attributes["BatchNumber"].InnerText, out intTemp))
                        {
                            intBatchNumber = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchNumber"));
                        }

                        if (nodeTemp.Attributes["DepositDate"] != null && DateTime.TryParse(nodeTemp.Attributes["DepositDate"].InnerText, out dteTemp))
                        {
                            dteDepositDate = dteTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid DepositDate"));
                        }
                        if (nodeTemp.Attributes["ProcessingDate"] != null && DateTime.TryParse(nodeTemp.Attributes["ProcessingDate"].InnerText, out dteTemp))
                        {
                            dteProcessingDate = dteTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid ProcessingDate"));
                        }
                        if (nodeTemp.Attributes["PICSDate"] != null)
                        {
                            strPICSDate = nodeTemp.Attributes["PICSDate"].InnerText;
                        }
                        else
                        {
                            throw (new Exception("Invalid PICSDate"));
                        }

                        if (nodeTemp.Attributes["TransactionID"] != null && int.TryParse(nodeTemp.Attributes["TransactionID"].InnerText, out intTemp))
                        {
                            intTransactionID = intTemp;
                        }
                        else
                        {
                            intTransactionID = -1;
                        }
                        if (nodeTemp.Attributes["BatchSequence"] != null && int.TryParse(nodeTemp.Attributes["BatchSequence"].InnerText, out intTemp))
                        {
                            intBatchSequence = intTemp;
                        }
                        else
                        {
                            intBatchSequence = -1;
                        }

                        strFileDisplayName = strReportName + "_" + intBankID.ToString() + "_" +
                                                     intLockboxID.ToString() + "_" +
                                                     intBatchID.ToString() + "_" +
                                                     strPICSDate + "_" +
                                                     intTransactionID.ToString();
                        ipoXmlLib.addAttribute(nodeReport, "ReportName", strReportName);
                        ipoXmlLib.addAttribute(nodeReport, "BankID", intBankID.ToString());
                        ipoXmlLib.addAttribute(nodeReport, "LockboxID", intLockboxID.ToString());
                        ipoXmlLib.addAttribute(nodeReport, "BatchID", intBatchID.ToString());
                        ipoXmlLib.addAttribute(nodeReport, "BatchNumber", intBatchNumber.ToString());
                        ipoXmlLib.addAttribute(nodeReport, "BatchSequence", intBatchSequence.ToString());
                        ipoXmlLib.addAttribute(nodeReport, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
                        ipoXmlLib.addAttribute(nodeReport, "ProcessingDate", dteProcessingDate.ToString("MM/dd/yyyy"));
                        ipoXmlLib.addAttribute(nodeReport, "PICSDate", strPICSDate);
                        ipoXmlLib.addAttribute(nodeReport, "TransactionID", intTransactionID.ToString());
                        ipoXmlLib.addAttribute(nodeReport, "DisplayScannedCheck", (Session.ServerOptions.Preferences.DisplayScannedCheckOnline.ToString()));

                    if (CBXSrv.AuthorizeBatchToUser(intBankID, intLockboxID, intBatchID, dteDepositDate, out objOLLockbox))
                    {
                        //Create the request in the DB
                        //Note:  CreateOnlineImageQueue only accepts 1 fileDisplayName.
                        ItemProcDAL.CreateOnlineImageQueue(gidOnlineImageQueueID,
                                                           Session.SessionUser.UserID,
                                                           strFileDisplayName,
                                                           Session.ServerOptions.Preferences.DisplayScannedCheckOnline,
                                                           out intRowsAffected);
                        if (intRowsAffected <= 0)
                        {
                            throw (new Exception("Unable to create the report request in OnlineImageQueue."));
                        }
                        //Get Request
                        dsRetVal = CheckImageJobStatus(gidOnlineImageQueueID).Data;
                        if (dsRetVal != null)
                        {
                            if (dsRetVal.Tables[0].Rows.Count > 0)
                            {
                                if (!SubmitImageJob(gidOnlineImageQueueID, objXMLOut))
                                {
                                    dsRetVal = null;
                                    throw (new Exception("Unable to submit the request to the IPO Image Service for processing."));
                                }
                            }
                        }
                        else
                        {
                            throw (new Exception("Unable to submit the request to the IPO Image Service for processing."));
                        }
                    }
                    else
                    {
                        throw (new Exception("User does not have access to the report requested."));
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "CreateReportJobReport");
                throw ;
            }
            response = new BaseGenericResponse<DataSet>()
            {
                Data = dsRetVal,
                Status = (dsRetVal != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };
            return response;
        }

        public bool SubmitImageJob(Guid OLImageQueueID, XmlDocument doc)
        {
            string strFileName;
            bool bolRetVal = false;
            OLFOnlineClient OLFClient = new OLFOnlineClient(this.SiteKey);
            try
            {
                strFileName = OLImageQueueID.ToString() + ".xml";
                MemoryStream ReadFileStream = new MemoryStream();
                doc.Save(ReadFileStream);
                ReadFileStream.Position = 0;
                OLFClient.PutStream(strFileName, ReadFileStream, Constants.OLF_IMAGE_SVC_JOB);
                ReadFileStream.Flush();
                ReadFileStream.Close();
                bolRetVal = true;
            }
            catch (Exception)
            {
                bolRetVal = false;
            }
            return (bolRetVal);
        }


        /// <summary>
        /// Check the status of a View All images request
        /// </summary>
        /// <param name="OnlineImageQueueID"></param>
        /// <returns>ADODB Record set with the request information.</returns>
        public BaseGenericResponse<DataSet> CheckImageJobStatus(Guid OnlineImageQueueID)
        {
            DataTable dtRetVal = null;
            DataTable dt = null;
            BaseGenericResponse<DataSet> response = null;
            bool bolRetVal = false;
            try
            {
                if ( (ItemProcDAL.GetOnlineImageQueue(OnlineImageQueueID, out dt)) &&
                     (dt.Rows.Count > 0) &&
                     (Session.SessionUser.UserID.ToString().Trim().ToLower() == dt.Rows[0]["UserID"].ToString().Trim().ToLower())
                   )
                {
                    dtRetVal = dt;
                    bolRetVal = true;
                }
                else
                {
                    throw (new Exception("The user does not have access to view report request."));
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "CheckImageJobStatus");
                throw ;
            }
            response = new BaseGenericResponse<DataSet>()
            {
                Data = dtRetVal.DataSet,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }
    }
}
