﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;
using System.Runtime.CompilerServices;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.R360ServicesAPI.Services;
using WFS.RecHub.R360RaamClient;
using WFS.LTA.Common;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;

/******************************************************************************
* ** WAUSAU Financial Systems (WFS)
* ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
* *******************************************************************************
* *******************************************************************************
* ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
* *******************************************************************************
* * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* * other trademarks cited herein are property of their respective owners.
* * These materials are unpublished confidential and proprietary information
* * of WFS and contain WFS trade secrets.  These materials may not be used,
* * copied, modified or disclosed except as expressly permitted in writing by
* * WFS (see the WFS license agreement for details).  All copies, modifications
* * and derivative works of these materials are property of WFS.
* *
* * Author: Charlie Johnson
* * Date: 05/23/2013
* *
* * Purpose: Adapts the data returned from R360ServicesDAL into the R360ServicesDTO objects or object arrays
* *            within Response objects to be transmitted via WCF.  This inherits and conforms to the
* *            iR360Serives interface.
* *
* * Modification History
* WI 100422 CEJ 05/23/2013 Created
* WI 101855 WJS 07/05/2013  Added CheckUserPermissionsForFunction
* WI 113475 WJS 09/06/2013
*        Online Displays an Error When the User Tries to Open the
*        Framework with Available Common Exceptions
* WI 114230 TWE 09/17/2013
*        Modify GetExceptionSummary to handle the new decisioning status 0,1,2,3
* WI 107711 CEJ 10/17/2013
*   Create the ability log userActivity in the sessionLog from the R360 Services
* WI  146612 DJW 6/27/2014
*   Changed "Client Account" to "Workgroup"
* ******************************************************************************/

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class R360ServicesManager : APIBase, IDisposable
    {
        // Track whether Dispose has been called.
        private bool disposed = false;

        R360ServicesDAL _dsdDAL = null;

        public R360ServicesManager(IServiceContext serviceContext)
            : base(serviceContext)
        {
        }

        #region Session Validation

        protected override bool ValidateSID(out int userID)
        {
            bool bReturnValue = false;
            userID = -1;
            DataTable dtResults;

            OnLogEvent("Current User Claims: \r\n" + _ServiceContext.RAAMClaims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);

            if (R360ServicesDAL.GetUserIDBySID(_ServiceContext.GetSID(), out dtResults))
            {
                userID = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
                bReturnValue = true;
            }

            return bReturnValue;
        }

        #endregion

        #region Wrapper Method

        private T ValidateUserIdAndDo<T>(Action<T, IR360ServicesDAL, int, Guid> operation, [CallerMemberName] string procName = null)
          where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T();
            result.Status = StatusCode.FAIL;

            try
            {
                int userID;
                // Validate / get the user ID
                if (ValidateSIDAndLog(procName, out userID))
                {
                    // Perform the requested operation
                    operation(result, R360ServicesDAL, userID, _ServiceContext.GetSessionID());
                }
                else
                {
                    result.Errors.Add("Invalid SID");
                }
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }


        private T ValidateUserIdAndDo<T>(Action<T, R360ServicesDAL, R360ServicesExceptionSummaryDAL, int, Guid> operation, [CallerMemberName] string procName = null)
        where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T();
            result.Status = StatusCode.FAIL;

            try
            {
                int userID;
                using (R360ServicesDAL dsdDAL = new R360ServicesDAL(SiteKey))
                {
                    using (R360ServicesExceptionSummaryDAL dsExceptDal = new R360ServicesExceptionSummaryDAL(SiteKey))
                    {
                        // Validate / get the user ID
                        if (ValidateSIDAndLog(procName, out userID))
                        {
                            // Perform the requested operation
                            operation(result, dsdDAL, dsExceptDal, userID, _ServiceContext.GetSessionID());
                        }
                        else
                        {
                            result.Errors.Add("Invalid SID");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }

        #endregion

        public DataEntryResponseDTO GetDataEntryFieldsForWorkgroup(DataEntrySearchDTO search)
        {
            return ValidateUserIdAndDo<DataEntryResponseDTO>((srsResults, dsdDAL, userID, sessionID) =>
            {
                var dal = new cSearchDAL(SiteKey);
                DataTable dt;
                srsResults.DataEntryFields = new List<DataEntryResponse>();
                if (dal.GetLockboxDataEntryFields(search.SiteBankID, search.SiteClientAccountID, sessionID, out dt))
                {
                    // Convert to model type
                    foreach (DataRow row in dt.Rows)
                        srsResults.DataEntryFields.Add(DataEntryResponse.FromDataRow(row));

                    // Return Success
                    srsResults.Status = StatusCode.SUCCESS;
                }
                else
                {
                    srsResults.Errors.Add("Error querying workgroup data entry fields");
                }
            });
        }

        public SummariesResponse GetClientAccountSummary(ReceivablesSummaryRequestDto request)
        {
            return ValidateUserIdAndDo<SummariesResponse>((srsResults, dsdDAL, userID, sessionID) =>
            {
                // Grab the workgroups for our user, validate it against the request.
                var workgroups = new List<WorkgroupDto>();
                var workgroupapi = new LockboxAPI(this._ServiceContext);
                var raamclient = new RaamClient(new ConfigHelpers.Logger(
                    x => OnLogEvent(x, "", MessageType.Information, MessageImportance.Verbose),
                    x => OnLogEvent(x, "", MessageType.Error, MessageImportance.Essential)));
                if (request.EntityId.HasValue)
                {
                    // Populate the workgroup list from RAAM's tree.
                    var wgs = raamclient.GetWorkgroupsForEntity(request.EntityId.Value);
                    if (wgs == null)
                    {
                        srsResults = new SummariesResponse()
                        {
                            Status = StatusCode.FAIL,
                            Errors = new List<string>() { "User does not have access to requested entity." }
                        };
                        return;
                    }

                    foreach (var r in wgs)
                    {
                        var split = r.ExternalReferenceID.Trim().Split('|');
                        workgroups.Add(new WorkgroupDto()
                        {
                            BankId = int.Parse(split[0]),
                            ClientAccountId = int.Parse(split[1])
                        });
                    }
                }
                else
                {
                    // We only have 1 workgroup to deal with.
                    var ourworkgroups = workgroupapi.GetUserLockboxes();
                    if (ourworkgroups.Status != StatusCode.SUCCESS || ourworkgroups.Data == null)
                    {
                        srsResults = new SummariesResponse()
                        {
                            Status = StatusCode.FAIL,
                            Errors = ourworkgroups.Errors
                        };
                        return;
                    }
                    if (!ourworkgroups.Data.Any(w => w.Value.BankID == request.BankId && w.Value.LockboxID == request.WorkgroupId))
                    {
                        srsResults = new SummariesResponse()
                        {
                            Status = StatusCode.FAIL,
                            Errors = new List<string>() { "User does not have access to requested workgroup." }
                        };
                        return;
                    }
                    workgroups.Add(new WorkgroupDto() { BankId = request.BankId.Value, ClientAccountId = request.WorkgroupId.Value });
                }

                var req = new ReceivablesSummaryDBRequestDto()
                {
                    DepositDate = request.DepositDate,
                    Workgroups = workgroups
                };

                // Call DAL to get summary info
                DataTable dtResults;
                if (dsdDAL.GetClientAccountSummary(sessionID, userID, req, out dtResults))
                {
                    // Convert to model type
                    List<SummaryModel> lstPayModels = new List<SummaryModel>();
                    foreach (DataRow drCurRow in dtResults.Rows)
                    {
                        lstPayModels.Add(new SummaryModel(drCurRow));
                    }
                    srsResults.Summaries = lstPayModels.ToArray();

                    // Return Success
                    srsResults.Status = StatusCode.SUCCESS;
                }
                else
                {
                    srsResults.Errors.Add("Error querying workgroup summary data");
                }
            });
        }

        public ClientAccountsResponse GetClientAccounts()
        {
            return GetClientAccountsForOrganization(Guid.Empty);
        }

        public ClientAccountsResponse GetClientAccountsForOrganization(Guid OLOrganizationID)
        {
            return ValidateUserIdAndDo<ClientAccountsResponse>((carResults, dsdDAL, userID, sessionID) =>
            {
                // Call DAL to get summary info
                DataTable dtResults;
                if (dsdDAL.GetClientAccounts(userID, OLOrganizationID, out dtResults))
                {
                    carResults.ClientAccounts = dtResults.ToList<ClientAccountModel>(ClientAccountModel.FieldMapping).ToArray();

                    // Return Success
                    carResults.Status = StatusCode.SUCCESS;
                }
                else
                {
                    carResults.Errors.Add("Error querying client accounts");
                }
            });
        }

        public ExceptionTypeSummaryResponse GetExceptionTypeSummary(EntityDTO entity, WorkgroupIDsDTO workgroup, DateTime datefrom, DateTime dateto)
        {
            return ValidateUserIdAndDo<ExceptionTypeSummaryResponse>((carResults, dsdDAL, dsExceptDAL, userID, sessionID) =>
            {
                // Call DAL to get summary info
                DataTable dtResults;

                if (dsExceptDAL.GetExceptionSummaryByWorkgroup(sessionID, entity, datefrom, dateto, workgroup, out dtResults))
                {
                    List<ExceptionTypeSummary> resultContract = new List<ExceptionTypeSummary>();

                    if (dtResults == null)
                    {
                        carResults.Errors.Add("Error querying GetExceptionSummaryByWorkgroup");
                        carResults.Status = StatusCode.FAIL;
                    }
                    else
                    {
                        // Pre-fill the container with 2 rows (post-process and in-process).
                        resultContract.Add(new ExceptionTypeSummary() { PaymentType = "Post-Process" });
                        resultContract.Add(new ExceptionTypeSummary() { PaymentType = "In-Process" });

                        //returned table assumed to be in LongName order
                        foreach (DataRow row in dtResults.Rows)
                        {
                            // Data is grouped by In-Process or Post-Process,
                            // then by the DecisioningStatus field, which is the TransactionStatus.
                            // 0: NoException, 1: Unresolved, 2: Accepted, 3: Rejected, 99: Complete.
                            // Goal: First group by In-Process or Post-Process, then get the sum and count
                            // of the transactions in each.

                            // Parse data first.
                            var amount = 0m;
                            if (!Decimal.TryParse(row["Amount"].ToString(), out amount))
                                OnLogEvent(string.Format("GetExceptionTypeSummary: Could not convert {0}, to a decimal. (Amount)", row["Amount"].ToString()), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                            var count = 0;
                            if (!int.TryParse(row["ItemCount"].ToString(), out count))
                                OnLogEvent(string.Format("GetExceptionTypeSummary: Could not convert {0}, to an int. (ItemCount)", row["ItemCount"].ToString()), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                            var decisioningstatus = -1;
                            if (!int.TryParse(row["DecisioningStatus"].ToString(), out decisioningstatus))
                                OnLogEvent(string.Format("GetExceptionTypeSummary: Could not convert {0}, to an int. (DecisioningStatus)", row["DecisioningStatus"].ToString()), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                            var paymenttype = row["ProcessType"].ToString();

                            // Grab the result row.
                            var resultrow = resultContract.FirstOrDefault(x => x.PaymentType == paymenttype);
                            if (resultrow == null)
                                OnLogEvent(string.Format("GetExceptionTypeSummary: Invalid Process Type: {0}", row["ProcessType"].ToString()), this.GetType().Name, MessageType.Error, MessageImportance.Essential);

                            // Append the values to the row.
                            if (decisioningstatus == 1)
                            {
                                // Means Unresolved.
                                resultrow.ExceptionCount += count;
                                resultrow.ExceptionAmount += amount;
                            }
                            else
                            {
                                // Means Resolved.
                                resultrow.ResolvedCount += count;
                                resultrow.ResolvedAmount += amount;
                            }

                        }
                        carResults.SummaryByType = resultContract.ToArray();
                        carResults.Status = StatusCode.SUCCESS;
                    }
                }
            });
        }

        public PermissionsResponse CheckUserPermissionsForFunction(string function, string mode)
        {
            return ValidateUserIdAndDo<PermissionsResponse>((permissResults, dsdDAL, sdtUserId, sessionID) =>
            {
                permissResults.HasPermission = true;
                permissResults.Status = StatusCode.SUCCESS;
            });
        }

        protected R360ServicesDAL R360ServicesDAL
        {
            get
            {
                if (_dsdDAL == null)
                {
                    if (!string.IsNullOrEmpty(SiteKey))
                    {
                        _dsdDAL = new R360ServicesDAL(SiteKey);
                    }
                }
                return (_dsdDAL);
            }
        }

        protected override ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acR360WebServiceCall;
            }
        }

        public BaseResponse WriteAuditEvent(string eventName, string eventType, string applicationName, string description)
        {
            return ValidateUserIdAndDo<BaseResponse>((result, dsdDAL, userID, sessionID) =>
            {
                if (dsdDAL.WriteAuditEvent(userID, eventName, eventType, applicationName, description))
                {
                    result.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseGenericResponse<List<WorkgroupNameDTO>> GetActiveWorkgroups(EntityDTO entity)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<List<WorkgroupNameDTO>>>((result, dsdDAL, userID, sessionID) =>
            {
                List<WorkgroupNameDTO> workgroups;
                if (dsdDAL.GetActiveWorkgroups(sessionID, entity, out workgroups))
                {
                    result.Data = workgroups;
                    result.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseGenericResponse<List<WorkgroupNameDTO>> GetIntegraPayOnlyWorkgroups(EntityDTO entity)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<List<WorkgroupNameDTO>>>((result, dsdDAL, userID, sessionID) =>
            {
                List<WorkgroupNameDTO> workgroups;
                if (dsdDAL.GetIntegraPayOnlyWorkgroups(sessionID, entity, out workgroups))
                {
                    result.Data = workgroups;
                    result.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseGenericResponse<long> GetSourceBatchID(long batchID)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<long>>((result, dsdDAL, userID, sessionID) =>
            {
                long sourceBatchID = 0;
                if (dsdDAL.GetSourceBatchID(batchID, out sourceBatchID))
                {
                    result.Data = sourceBatchID;
                    result.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseGenericResponse<List<AuditUserDTO>> GetAuditUsers()
        {
            return ValidateUserIdAndDo<BaseGenericResponse<List<AuditUserDTO>>>((result, dsdDAL, userID, sessionID) =>
            {
                List<AuditUserDTO> users;
                if (dsdDAL.GetAuditUsers(out users))
                {
                    result.Data = users;
                    result.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseGenericResponse<string> GetBankName(int siteBankId) {
            return ValidateUserIdAndDo<BaseGenericResponse<string>>((result, r360Dal, userId, sessionId) => {
                var bankName = "";
                if (r360Dal.GetBankName(siteBankId, out bankName)) {
                    result.Data = bankName;
                    result.Status = StatusCode.SUCCESS;
                }
                else {
                    result.Data = "";
                    result.Status = StatusCode.FAIL;
                }
            });
        }

        public BaseGenericResponse<List<DDASummaryDataDto>> GetDDASummary(DateTime depositDate)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<List<DDASummaryDataDto>>>((result, dsdDAL, userID, sessionID) =>
            {
                List<DDASummaryDataDto> ddaSummaryData;
                if (dsdDAL.GetDDASummaryData(sessionID, depositDate, out ddaSummaryData))
                {
                    result.Data = ddaSummaryData;
                    result.Status = StatusCode.SUCCESS;
                }
            });
        }

        public AdvancedSearchResultsDto GetAdvancedSearch(AdvancedSearchDto request)
        {
            return ValidateUserIdAndDo<AdvancedSearchResultsDto>((result, dal, userId, sessionId) =>
            {
                var advancedsearch = new AdvancedSearchRepository(
                    _ServiceContext,
                    new DatabaseAdvancedSearchDataProvider(_ServiceContext),
                    new OLFImageService(),
                    new RAAMPermissionProvider());
                var data = advancedsearch.Get(request);
                result.Errors = data.Errors;
                result.Results = data.Results;
                result.Status = data.Status;
                result.Totals = data.Totals;
                result.Metadata = data.Metadata;
            });
        }

        public BatchSummaryResponseDto GetBatchSummary(BatchSummaryRequestDto requestContext)
        {
            return ValidateUserIdAndDo<BatchSummaryResponseDto>((result, dsdDAL, userID, sessionID) =>
            {
                var batchSummary = new BatchSummaryRepository(this._ServiceContext, 
                    new LockboxAPI(this._ServiceContext),
                    new PreferencesProvider(this._ServiceContext.SiteKey), 
                    new DatabaseBatchDataProvider(this._ServiceContext));
                var data = batchSummary.Get(requestContext);
                result.TotalRecords = data.TotalRecords;
                result.FilteredRecords = data.FilteredRecords;
                result.CheckCount = data.CheckCount;
                result.CheckTotal = data.CheckTotal;
                result.DocumentCount = data.DocumentCount;
                result.TransactionCount = data.TransactionCount;
                result.Preferences = data.Preferences;
                result.SiteCode = data.SiteCode;
                result.Status = data.Status;
                result.Errors = data.Errors;
                result.BatchSummaryList = data.BatchSummaryList;
            });
        }

        public BatchDetailResponseDto GetBatchDetail(BatchDetailRequestDto requestContext)
        {
            return ValidateUserIdAndDo<BatchDetailResponseDto>((result, dsdDAL, userID, sessionID) =>
            {
                var batchDetail = new BatchDetailRepository(this._ServiceContext, new OLFImageService(), new DatabaseBatchDataProvider(this._ServiceContext));
                var data = batchDetail.Get(requestContext);
                result.TotalRecords = data.TotalRecords;
                result.FilteredRecords = data.FilteredRecords;
                result.Status = data.Status;
                result.Errors = data.Errors;
                result.BatchDetailList = data.BatchDetailList;
            });
        }

        public TransactionDetailResponseDto GetTransactionDetail(TransactionDetailRequestDto requestContext)
        {
            return ValidateUserIdAndDo<TransactionDetailResponseDto>((result, dsDal, userID, sessionID) =>
            {
                var repo = new TransactionDetailRepository(this._ServiceContext);
                var data = repo.Get(requestContext);
                result.AccountSiteCode = data.AccountSiteCode;
                result.BankID = data.BankID;
                result.BatchCueID = data.BatchCueID;
                result.BatchID = data.BatchID;
                result.DepositDateKey = data.DepositDateKey;
                result.DepositDate = data.DepositDate;
                result.BatchNumber = data.BatchNumber;
                result.BatchSiteCode = data.BatchSiteCode;
                result.DisplayBatchID = data.DisplayBatchID;
                result.PaymentSource = data.PaymentSource;
                result.SourceBatchID = data.SourceBatchID;
                result.WorkgroupID = data.WorkgroupID;
                result.TransactionNumber = data.TransactionNumber;
                result.TransactionId = data.TransactionId;
                result.WorkgroupDisplayName = data.WorkgroupDisplayName;
                result.PaymentType = data.PaymentType;
                result.PICSDate = data.PICSDate;
                result.ImportTypeShortName = data.ImportTypeShortName;
                result.BatchSourceShortName = data.BatchSourceShortName;
                result.PaymentTypeShortName = data.PaymentTypeShortName;
                result.TransactionId = data.TransactionId;
                result.Payments = data.Payments;
                result.Stubs = data.Stubs;
                result.Documents = data.Documents;
                result.Preferences = data.Preferences;
                result.BatchTransactions = data.BatchTransactions;
                result.ViewAllImagesIconVisible = data.ViewAllImagesIconVisible;
                result.Status = data.Status;
                result.Errors = data.Errors;
            });
        }

        public NotificationsResponseDto GetNotificationsResponse(NotificationsRequestDto request)
        {
            
            return ValidateUserIdAndDo<NotificationsResponseDto>((result, dal, userId, sessionId) =>
            {
                var repo = new NotificationsRepository(_ServiceContext, new DataBaseNotificationDataProvider(_ServiceContext),
                                                        new PreferencesProvider(_ServiceContext.SiteKey), 
                                                        new NotificationsRequestXmlTransformer(),
                                                        new RAAMPolicyProvider());
                var data = repo.GetAll(request);
                result.TotalRecords = data.TotalRecords;
                result.FilteredTotal = data.FilteredTotal;
                result.Errors = data.Errors;
                result.Notifications = data.Notifications;
                result.Status = data.Status;
            });
        }

        public NotificationResponseDto GetNotification(NotificationRequestDto request)
        {

            return ValidateUserIdAndDo<NotificationResponseDto>((result, dal, userId, sessionId) =>
            {
                var repo = new NotificationsRepository(_ServiceContext, new DataBaseNotificationDataProvider(_ServiceContext),
                                                        new PreferencesProvider(_ServiceContext.SiteKey),
                                                        new NotificationsRequestXmlTransformer(),
                                                        new RAAMPolicyProvider());
                var data = repo.Get(request);
                result.Notification = data.Notification;
                result.MetaData = data.MetaData;
                result.Errors = data.Errors;
                result.Status = data.Status;
            });
        }

        public NotificationsFileTypesResponseDto  GetNotificationFileTypes()
        {
            return ValidateUserIdAndDo<NotificationsFileTypesResponseDto>((result, dal, userId, sessionId) =>
            {
                var dbDal = new DataBaseNotificationDataProvider(_ServiceContext);
                var dt = new DataTable();
                var types = new List<NotificationFileTypeDto>();
                if (dbDal.GetNotificationFileTypes(out dt))
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        var fileType = new NotificationFileTypeDto
                        {
                            NotificationFileTypeKey = row.GetInteger("NotificationFileTypeKey"),
                            FileTypeDescription = row.GetString("FileTypeDescription")
                        };
                        types.Add(fileType);
                    }
                    result.FileTypes = types;
                    result.Status = StatusCode.SUCCESS;
                }
            });
        }


        public PaymentTypesResponseDto GetPaymentTypes()
        {
            return ValidateUserIdAndDo<PaymentTypesResponseDto>((result, dsdDAL, userID, sessionID) =>
            {
                var dal = new cSearchDAL(this.SiteKey);
                var dt = new DataTable();
                var paymentTypes = new List<PaymentTypeDto>();
                if (dal.GetPaymentTypes(this.GetSession(), out dt))
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        paymentTypes.Add(new PaymentTypeDto
                        {
                            LongName = row.GetString("LongName"),
                            ShortName = row.GetString("ShortName"),
                            PaymentTypeKey = row.GetInteger("BatchPaymentTypeKey")
                        });
                    }
                    result.PaymentTypes = paymentTypes;
                    result.Status = StatusCode.SUCCESS;
                }

            });
        }

        public PaymentSourcesResponseDto GetPaymentSources()
        {
            return ValidateUserIdAndDo<PaymentSourcesResponseDto>((result, dsdDAL, userID, sessionID) =>
            {
                var dal = new cSearchDAL(this.SiteKey);
                var dt = new DataTable();
                var sources = new List<PaymentSourceDto>();
                if (dal.GetPaymentSources(this.GetSession(), true, out dt) && dt != null)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        sources.Add(new PaymentSourceDto
                        {
                            LongName = row.GetString("LongName"),
                            ShortName = row.GetString("ShortName"),
                            PaymentSourceKey = row.GetInteger("BatchSourceKey")
                        });
                    }
                    result.PaymentSources = sources;
                    result.Status = StatusCode.SUCCESS;
                }
            });
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> SaveStoredQuery(AdvancedSearchStoredQueryDto entity)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<AdvancedSearchStoredQueryDto>>((result, dsdDAL, userID, sessionID) =>
            {
                var transformer = new AdvancedSearchRequestXmlTransformer();
                var repo = new AdvancedSearchStoredQueryRepository(
                    transformer, 
                    new DatabaseAdvancedSearchStoredQueryDataProvider());
                var res = repo.Save(entity);
                result.Data = res.Data;
                result.Errors = res.Errors;
                result.Status = res.Status;
            });
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> DeleteStoredQuery(Guid entity)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<AdvancedSearchStoredQueryDto>>((result, dsdDAL, userID, sessionID) =>
            {
                var transformer = new AdvancedSearchRequestXmlTransformer();
                var repo = new AdvancedSearchStoredQueryRepository(
                    transformer,
                    new DatabaseAdvancedSearchStoredQueryDataProvider());
                var res = repo.Delete(entity);
                result.Data = res.Data;
                result.Errors = res.Errors;
                result.Status = res.Status;
            });
        }

        public BaseGenericResponse<List<AdvancedSearchStoredQueryDto>> GetStoredQueries()
        {
            return ValidateUserIdAndDo<BaseGenericResponse<List<AdvancedSearchStoredQueryDto>>>((result, dsdDAL, userID, sessionID) =>
            {
                var repo = new AdvancedSearchStoredQueryRepository(
                    new AdvancedSearchRequestXmlTransformer(),
                    new DatabaseAdvancedSearchStoredQueryDataProvider());
                var res = repo.GetAll();
                result.Data = res.Data;
                result.Errors = res.Errors;
                result.Status = res.Status;
            });
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> GetStoredQuery(Guid id)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<AdvancedSearchStoredQueryDto>>((result, dsdDAL, userID, sessionID) =>
            {
                var repo = new AdvancedSearchStoredQueryRepository(
                    new AdvancedSearchRequestXmlTransformer(),
                    new DatabaseAdvancedSearchStoredQueryDataProvider());
                var res = repo.Get(id);
                result.Data = res.Data;
                result.Errors = res.Errors;
                result.Status = res.Status;
            });
        }

        public PaymentSearchResponseDto GetPaymentSearch(PaymentSearchRequestDto request)
        {
            return ValidateUserIdAndDo<PaymentSearchResponseDto>((result, dsdDAL, userID, sessionID) =>
            {
                var repo = new PaymentSearchRepository(
                    new PaymentSearchRequestXmlTransformer(),
                    new PaymentSearchResponseXmlTransformer(),
                    new DatabasePaymentSearchDataProvider(R360ServiceContext.Current),
                    R360ServiceContext.Current,
                    new DatabaseDisplayBatchIdProvider(R360ServiceContext.Current),
                    new OLFImageService(),
                    new RAAMPermissionProvider(),
                    new PreferencesProvider(R360ServiceContext.Current.SiteKey));
                var res = repo.Get(request);
                result.TotalRecords = res.TotalRecords;
                result.Results = res.Results;
                result.Errors = res.Errors;
                result.Status = res.Status;
                result.Metadata = res.Metadata;
            });
        }

        public InvoiceSearchResponseDto GetInvoiceSearch(InvoiceSearchRequestDto request)
        {
            return ValidateUserIdAndDo<InvoiceSearchResponseDto>((result, dsdDAL, userID, sessionID) =>
            {
                var repo = new InvoiceSearchRepository(
                    new InvoiceSearchRequestXmlTransformer(), 
                    new InvoiceSearchResponseXmlTransformer(), 
                    new DatabaseInvoiceSearchDataProvider(R360ServiceContext.Current),
                    R360ServiceContext.Current,
                    new DatabaseDisplayBatchIdProvider(R360ServiceContext.Current),
                    new OLFImageService(), 
                    new RAAMPermissionProvider(), 
                    new PreferencesProvider(R360ServiceContext.Current.SiteKey));
                var res = repo.Get(request);
                result.TotalRecords = res.TotalRecords;
                result.Results = res.Results;
                result.Errors = res.Errors;
                result.Status = res.Status;
                result.Metadata = res.Metadata;
            });
        }
        

        #region Dispose
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_dsdDAL != null)
                    {
                        _dsdDAL.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;

            }
        }
        #endregion
    }
}
