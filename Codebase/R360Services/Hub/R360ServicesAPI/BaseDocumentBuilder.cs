﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class BaseDocumentBuilder
    {
        private readonly IReadOnlyList<BreadCrumb> _breadCrumbs;
        private readonly DateTime _currentDateTime;
        private readonly cEventLog _eventLog;
        private readonly Func<string, R360Permissions.ActionType, bool> _isPermissionAllowed;
        private readonly LogonMethodType _logonMethod;
        private readonly int _sessionExpirationInMinutes;
        private readonly IList<cOLPreference> _userOLPreferences;

        public BaseDocumentBuilder(cEventLog eventLog, IList<cOLPreference> userOLPreferences,
            IReadOnlyList<BreadCrumb> breadCrumbs, int sessionExpirationInMinutes,
            LogonMethodType logonMethod, DateTime currentDateTime,
            Func<string, R360Permissions.ActionType, bool> isPermissionAllowed)
        {
            _eventLog = eventLog;
            _userOLPreferences = userOLPreferences;
            _breadCrumbs = breadCrumbs;
            _sessionExpirationInMinutes = sessionExpirationInMinutes;
            _logonMethod = logonMethod;
            _currentDateTime = currentDateTime;
            _isPermissionAllowed = isPermissionAllowed;
        }

        private void AddLogonMethodAsNode(XmlNode nodeParent, int LogonMethod, string NodeName)
        {
            XmlNode node;

            node = ipoXmlLib.addElement(nodeParent, NodeName);
            ipoXmlLib.addAttribute(node, "Value", LogonMethod.ToString());
        }
        private bool AddSessionBreadCrumbsAsNode(XmlNode nodeParent, IReadOnlyList<BreadCrumb> breadCrumbs)
        {
            XmlNode node;
            XmlElement nodeTemp;
            string strScriptName;
            bool bolRetVal;

            node = ipoXmlLib.addElement(nodeParent, "SessionBreadCrumbs");

            if (breadCrumbs != null)
            {
                // Skip the first item because it contains data for the current page.
                foreach (var breadCrumb in breadCrumbs.Skip(1))
                {
                    strScriptName = breadCrumb.ScriptName;
                    nodeTemp = ipoXmlLib.addElement(node, "SessionBreadCrumb");
                    nodeTemp.IsEmpty = true;
                    ipoXmlLib.addAttribute(nodeTemp, "ScriptName", strScriptName.ToLower().Trim());
                    ipoXmlLib.addAttribute(nodeTemp, "PageCounter", breadCrumb.PageCounter.ToString());
                }

                bolRetVal = true;
            }
            else
            {
                _eventLog.logEvent("Unable to retrieve Session BreadCrumbs",
                                  this.GetType().Name,
                                  MessageType.Error,
                                  MessageImportance.Essential);
                bolRetVal = false;
            }

            return (bolRetVal);
        }
        private void AddSessionExpirationAsNode(XmlNode nodeParent, int sessionExpirationInMinutes, string NodeName)
        {
            XmlNode node;

            node = ipoXmlLib.addElement(nodeParent, NodeName);
            ipoXmlLib.addAttribute(node, "ExpirationSeconds", (sessionExpirationInMinutes * 60).ToString());
        }
        private void AddUserOLPreferencesAsNode(XmlNode nodeParent, IList<cOLPreference> lstPreferences)
        {
            XmlNode node;
            XmlElement nodeTemp;
            node = ipoXmlLib.addElement(nodeParent, "OLPreferences");

            if (lstPreferences != null)
            {
                foreach (cOLPreference objOLPreference in lstPreferences)
                {
                    if (objOLPreference.PreferenceGroup == "Display Options")
                    {
                        nodeTemp = ipoXmlLib.addElement(node, "OLPreference");
                        nodeTemp.IsEmpty = true;

                        ipoXmlLib.addAttribute(nodeTemp, "PreferenceName", objOLPreference.PreferenceName);
                        ipoXmlLib.addAttribute(nodeTemp, "AppType", objOLPreference.AppType.ToString());
                        ipoXmlLib.addAttribute(nodeTemp, "UserSettings", objOLPreference.UserSetting);
                        ipoXmlLib.addAttribute(nodeTemp, "DefaultSettings", objOLPreference.DefaultSetting);

                    }
                }
            }
            else
            {
                _eventLog.logEvent("Unable to retrieve Preferences",
                                  this.GetType().Name,
                                  MessageType.Error,
                                  MessageImportance.Essential);
            }
        }
        private void AddUserPermissionsAsNode(XmlNode nodeParent)
        {
            XmlNode node;
            XmlElement nodeTemp;

            node = ipoXmlLib.addElement(nodeParent, "Permissions");

            if (_isPermissionAllowed(R360Permissions.Perm_AdvanceSearchQueries, R360Permissions.ActionType.View))
            {
                nodeTemp = ipoXmlLib.addElement(node, "Permission");
                nodeTemp.IsEmpty = true;
                ipoXmlLib.addAttribute(nodeTemp, "Name", R360Permissions.Perm_AdvanceSearchQueries);
                ipoXmlLib.addAttribute(nodeTemp, "Script", "maintainquery.aspx");
                ipoXmlLib.addAttribute(nodeTemp, "Mode", "Manage");
            }

            if (_isPermissionAllowed(R360Permissions.Perm_AlertDownload, R360Permissions.ActionType.View))
            {
                nodeTemp = ipoXmlLib.addElement(node, "Permission");
                nodeTemp.IsEmpty = true;
                ipoXmlLib.addAttribute(nodeTemp, "Name", R360Permissions.Perm_AlertDownload);
                ipoXmlLib.addAttribute(nodeTemp, "Script", "AlertDownload");
                ipoXmlLib.addAttribute(nodeTemp, "Mode", "View");
            }

            if (_isPermissionAllowed(R360Permissions.Perm_AdvanceSearchDownload, R360Permissions.ActionType.View))
            {
                nodeTemp = ipoXmlLib.addElement(node, "Permission");
                nodeTemp.IsEmpty = true;
                ipoXmlLib.addAttribute(nodeTemp, "Name", R360Permissions.Perm_AdvanceSearchDownload);
                ipoXmlLib.addAttribute(nodeTemp, "Script", "AdvanceSearchDownload");
                ipoXmlLib.addAttribute(nodeTemp, "Mode", "View");
            }

            if (_isPermissionAllowed(R360Permissions.Perm_ReportImageRPSAudit, R360Permissions.ActionType.View))
            {
                nodeTemp = ipoXmlLib.addElement(node, "Permission");
                nodeTemp.IsEmpty = true;
                ipoXmlLib.addAttribute(nodeTemp, "Name", R360Permissions.Perm_ReportImageRPSAudit);
                ipoXmlLib.addAttribute(nodeTemp, "Script", "ReportImageRPSAudit");
                ipoXmlLib.addAttribute(nodeTemp, "Mode", "View");
            }

            if (_isPermissionAllowed(R360Permissions.Perm_AssignPayer, R360Permissions.ActionType.Manage))
            {
                nodeTemp = ipoXmlLib.addElement(node, "Permission");
                nodeTemp.IsEmpty = true;
                ipoXmlLib.addAttribute(nodeTemp, "Name", R360Permissions.Perm_AssignPayer);
                ipoXmlLib.addAttribute(nodeTemp, "Mode", "Manage");
            }
        }
        public XmlDocumentResponse BuildBaseDocument()
        {
            XmlDocument xmlDoc = null;
            XmlComment nodeComment;
            XmlNode nodeSystemInfo;
            XmlNode nodeUserInfo;
            XmlDocumentResponse response = null;

            try
            {
                xmlDoc = ipoXmlLib.GetXMLDoc("Page");

                nodeComment = xmlDoc.CreateComment("Generated " + _currentDateTime + " by " +
                                                   FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileDescription + ", Version " +
                                                   FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileMajorPart +
                                                   FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileMinorPart +
                                                   FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileBuildPart);

                xmlDoc.InsertBefore(nodeComment, xmlDoc.DocumentElement);

                nodeComment = xmlDoc.CreateComment(FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).LegalCopyright);
                xmlDoc.InsertBefore(nodeComment, xmlDoc.DocumentElement);

                nodeSystemInfo = ipoXmlLib.addElement(xmlDoc.DocumentElement, "SystemInfo");

                ipoXmlLib.AddDateAsNode(nodeSystemInfo, _currentDateTime, "CurrentDate");
                AddSessionExpirationAsNode(nodeSystemInfo, _sessionExpirationInMinutes, "SessionExpiration");
                AddLogonMethodAsNode(nodeSystemInfo, (int)_logonMethod, "LogonMethod");

                // Add User information node
                nodeUserInfo = ipoXmlLib.addElement(xmlDoc.DocumentElement, "UserInfo");
                AddUserPermissionsAsNode(nodeUserInfo);
                AddUserOLPreferencesAsNode(nodeUserInfo, _userOLPreferences);
                AddSessionBreadCrumbsAsNode(nodeUserInfo, _breadCrumbs);
            }
            catch (Exception ex)
            {
                _eventLog.logError(ex, this.GetType().Name, "GetBaseDocument");
            }

            response = new XmlDocumentResponse()
            {
                Data = xmlDoc,
                Status = StatusCode.SUCCESS   //always will contain data???
            };

            return (response);
        }
    }
}
