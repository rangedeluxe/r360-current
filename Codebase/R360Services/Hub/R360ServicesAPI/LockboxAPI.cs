﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.itemProcessing.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
* ** WAUSAU Financial Systems (WFS)
* ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
* *******************************************************************************
* *******************************************************************************
* ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
* *******************************************************************************
* * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* * other trademarks cited herein are property of their respective owners.
* * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
* *
* * Author: Brian Holmes
* * Date: 06/16/2014
* *
* * Purpose:
 * *
* * Modification History
* * WI 146250 BDH 06/16/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class LockboxAPI : BaseOLAPI, ILockboxAPI
    {
        private cItemProcDAL _ItemProcDAL = null;
        private cIPItemProcDAL _ItemProcessingDAL = null;
        private cSearchDAL _SearchDAL = null;

        public LockboxAPI(IServiceContext serviceContext)
            : base(serviceContext)
        {
        }

        private cItemProcDAL ItemProcDAL
        {
            get
            {
                if (_ItemProcDAL == null)
                {
                    _ItemProcDAL = new cItemProcDAL(this.SiteKey);
                }

                return (_ItemProcDAL);
            }
        }
        private cIPItemProcDAL ItemProcessingDAL
        {
            get
            {
                if (_ItemProcessingDAL == null)
                {
                    _ItemProcessingDAL = new cIPItemProcDAL(this.SiteKey);
                }

                return (_ItemProcessingDAL);
            }
        }
        protected override RecHub.Common.ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acR360WebServiceCall;
            }
        }
        private cSearchDAL SearchDAL
        {
            get
            {
                if (_SearchDAL == null)
                {
                    _SearchDAL = new cSearchDAL(this.SiteKey);
                }

                return (_SearchDAL);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !Disposed)
            {
                if (_ItemProcDAL != null)
                {
                    _ItemProcDAL.Dispose();
                }
                if (_ItemProcessingDAL != null)
                {
                    _ItemProcessingDAL.Dispose();
                }
                if (_SearchDAL != null)
                {
                    _SearchDAL.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        protected override bool ValidateSID(out int userID)
        {
            // TODO: Implement this; it will be called from the standard wrapper methods...
            throw new NotImplementedException();
        }

        public BaseGenericResponse<cOLLockboxes> GetUserLockboxes()
        {
            bool bolRetVal = false;
            cOLLockboxes objRetVal = null;
            DataTable dt = null;
            BaseGenericResponse<cOLLockboxes> response = null;

            objRetVal = new cOLLockboxes();

            dt = GetUserWorkGroups(base.Session.SessionID);

            //Get a collection of the User Lockboxes
            if (dt != null)
            {
                objRetVal.LoadDataTable(dt);
                dt.Dispose();
                bolRetVal = true;
            }

            response = new BaseGenericResponse<cOLLockboxes>()
            {
                Data = objRetVal,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<List<cCommingledLockbox>> GetDestinationLockboxes(int BankID, int LockboxID)
        {
            bool bolRetVal = false;
            cCommingledLockbox objLockbox = null;
            List<cCommingledLockbox> objLockboxes = null;
            DataTable dt = null;
            BaseGenericResponse<List<cCommingledLockbox>> response = null;

            objLockboxes = new List<cCommingledLockbox>();

            if (ItemProcessingDAL.GetDestLockboxes(BankID, LockboxID, out dt))
            {
                foreach (DataRow dr in dt.Rows)
                {
                    objLockbox = new cCommingledLockbox();
                    objLockbox.LoadDataRow(dr);
                    objLockboxes.Add(objLockbox);
                }

                bolRetVal = true;
            }

            response = new BaseGenericResponse<List<cCommingledLockbox>>()
            {
                Data = objLockboxes,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        private DataTable GetUserWorkGroups(Guid sessionID)
        {

            DataTable dtRetVal = null;
            DataTable dt = null;

            // create recordset of workgroups available to current user
            if (sessionID != null && sessionID != Guid.Empty)
            {
                if (SearchDAL.UserWorkgroupEntitlements(sessionID, out dt))
                {
                    dtRetVal = dt;
                }

                dt = null;
            }

            return dtRetVal;
        }

        public BaseGenericResponse<cLockbox> GetLockboxByID(Guid OLLockboxID)
        {
            bool bolRetVal = false;
            cLockbox objRetVal = null;
            DataTable dt = null;
            DataRow dr;
            cLockbox tmpLockbox;
            BaseGenericResponse<cLockbox> response = null;

            if (ItemProcDAL.GetLockboxByID(Session.SessionUser, OLLockboxID, out dt))
            {
                if (dt.Rows.Count > 0)
                {
                    dr = dt.Rows[0];
                    tmpLockbox = new cOLLockbox();

                    if (tmpLockbox.LoadDataRow(dr))
                    {
                        objRetVal = tmpLockbox;
                        bolRetVal = true;
                    }

                    tmpLockbox = null;
                }

                dt.Dispose();
            }

            response = new BaseGenericResponse<cLockbox>()
            {
                Data = objRetVal,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<cOLLockbox> GetLockboxByLockboxID(int BankID, int LockboxID)
        {
            bool bolRetVal = false;
            cOLLockbox objRetVal = null;
            DataTable dt = null;
            DataRow dr;
            cOLLockbox tmpLockbox;
            BaseGenericResponse<cOLLockbox> response = null;

            if (ItemProcDAL.GetLockboxByLockboxID(Session.SessionUser, BankID, LockboxID, out dt))
            {
                if (dt.Rows.Count > 0)
                {
                    dr = dt.Rows[0];
                    tmpLockbox = new cOLLockbox();

                    if (tmpLockbox.LoadDataRow(dr))
                    {
                        objRetVal = tmpLockbox;
                        bolRetVal = true;
                    }

                    tmpLockbox = null;
                }

                dt.Dispose();
            }

            response = new BaseGenericResponse<cOLLockbox>()
            {
                Data = objRetVal,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<cOLLockbox> GetLockBoxByAccountIDBankID(int BankID, int LockboxID)
        {
            bool bolRetVal = false;
            cOLLockbox objRetVal = null;
            DataTable dt = null;
            DataRow dr;
            cOLLockbox tmpLockbox;
            BaseGenericResponse<cOLLockbox> response = null;

            if (ItemProcDAL.GetLockBoxByAccountIDBankID(Session.GetSession(), BankID, LockboxID, out dt))
            {
                if (dt.Rows.Count > 0)
                {
                    dr = dt.Rows[0];
                    tmpLockbox = new cOLLockbox();

                    if (tmpLockbox.LoadDataRow(dr))
                    {
                        objRetVal = tmpLockbox;
                        bolRetVal = true;
                    }

                    tmpLockbox = null;
                }

                dt.Dispose();
            }

            response = new BaseGenericResponse<cOLLockbox>()
            {
                Data = objRetVal,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<Dictionary<int, string>> GetPaymentSources(bool ActiveOnly)
        {
            bool bolRetVal = false;
            Dictionary<int, string> results = new Dictionary<int, string>();
            DataTable dt;
            BaseGenericResponse<Dictionary<int, string>> response = null;

            if (SearchDAL.GetPaymentSources(base.Session.SessionID, ActiveOnly, out dt))
            {
                foreach (DataRow row in dt.Rows)
                {
                    int key = (short)row["BatchSourceKey"];
                    results.Add(key, (string)row["LongName"]);
                }

                bolRetVal = true;
            }

            response = new BaseGenericResponse<Dictionary<int, string>>()
            {
                Data = results,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<Dictionary<int, string>> GetPaymentTypes()
        {
            bool bolRetVal = false;
            Dictionary<int, string> results = new Dictionary<int, string>();
            DataTable dt;
            BaseGenericResponse<Dictionary<int, string>> response = null;

            if (SearchDAL.GetPaymentTypes(base.Session.SessionID, out dt))
            {
                foreach (DataRow row in dt.Rows)
                {
                    int key = (byte)row["BatchPaymentTypeKey"];
                    results.Add(key, (string)row["LongName"]);
                }

                bolRetVal = true;
            }

            response = new BaseGenericResponse<Dictionary<int, string>>()
            {
                Data = results,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }
    }
}