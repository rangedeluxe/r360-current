﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     1/14/2009
*
* Purpose:  _DMPObjectRoot.cs.
*
* Modification History
* 01/14/2009 CR 99999 XXX
*     - Initial release.
* CR 31831 JMC 11/11/2010
*     -Added 'Warning' to MsgType enumeration.
* CR 54181 JMC 09/04/2012
*   -Added NotificationDAL object.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96309 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for OLServicesAPI
******************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public static class Constants
    {
        public const string OLF_IMAGE_SVC_JOB = "OnlineArchive";
        public const string OLF_CENDS_JOB = "CENDS";
    }

    [Serializable]
    internal class NonLoggableException : Exception
    {
        public NonLoggableException(string message)
            : base(message)
        {
        }
    }
}
