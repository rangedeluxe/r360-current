﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Repositories
{
    public class NotificationsRepository : IReadOnlyListRepository<NotificationRequestDto, NotificationResponseDto, NotificationsRequestDto, NotificationsResponseDto, NotificationRequestDto>
    {
        private readonly IServiceContext _context;
        private readonly INotificationsDataProvider _notificationsDataProvider;
        private readonly IPreferencesProvider _preferences;
        private readonly IPolicyProvider _policyprovider;
        private readonly IXmlTransformer<NotificationsRequestDto> _requesTransformer;
        private const string DISALLOWEDCHARACTERS = @"\t|\n|\r|\\t|\\n|\\r";
        //TODO: refactor this into a interface
        private readonly cOLSiteOptions _olSiteOptions;
        private const int TIME_ZONE_BIAS = 60;
        private const string RECORDS_PER_PAGE_PREF = "RecordsPerPage";
        public NotificationsRepository(IServiceContext context, INotificationsDataProvider notificationsDataProvider,
                                       IPreferencesProvider preferences, IXmlTransformer<NotificationsRequestDto> requestTransformer,
                                       IPolicyProvider policyprovider)
        {
            _context = context;
            _notificationsDataProvider = notificationsDataProvider;
            _preferences = preferences;
            _requesTransformer = requestTransformer;
            _policyprovider = policyprovider;
            _olSiteOptions = new cOLSiteOptions(_context.SiteKey);
        }
        public NotificationResponseDto Get(NotificationRequestDto request)
        {
            var response = new NotificationResponseDto();

            try
            {
                // Grabbing the notification.
                var resp = _notificationsDataProvider.GetNotification(request.MessageGroup, request.UtcOffset, _context.GetSessionID());
                if (resp == null)
                {
                    response.Status = StatusCode.FAIL;
                    response.Errors = new string[] { "Could not find requested notification." }.ToList();
                    return response;
                }
                //removes carriage return, new lines
                resp.Message = Regex.Replace(resp.Message, DISALLOWEDCHARACTERS, string.Empty);
                response.Notification = resp;
                // Grabbing the notification's files.
                var files = _notificationsDataProvider.GetNotificationFiles(request.MessageGroup, _context.GetSessionID());
                if (files == null)
                {
                    response.Status = StatusCode.FAIL;
                    response.Errors = new string[] { "Could not find requested notification." }.ToList();
                    return response;
                }
                response.Notification.Files = files.ToList();

                // Setting the result metadata.
                response.MetaData = new NotificationMetaData()
                {
                    HasDownloadPermission = _policyprovider.HasPermission(R360Permissions.Perm_AlertDownload, R360Permissions.ActionType.View)
                };
                
            }
            catch(Exception ex)
            {
                response.Status = StatusCode.FAIL;
                response.Errors = new string[] { ex.Message }.ToList();
            }

            return response;
        }

        public NotificationsResponseDto GetAll(NotificationsRequestDto request)
        {
            var response = new NotificationsResponseDto();
            try
            {
                int recordsPerPage = int.TryParse(_preferences.GetPreferenceByName(RECORDS_PER_PAGE_PREF).UserSetting,
                                             out recordsPerPage) ? recordsPerPage : 50;
                var notifications = new List<NotificationDto>();
                
                request.ClientAccounts = _notificationsDataProvider.GetWorkgroups(request.Workgroup);
                var xml = _requesTransformer.ToXML(request);
                var resp = _notificationsDataProvider.GetUserNotifications(_context.GetSessionID(), request.StartDate,
                    request.EndDate, request.FileTypeKey, request.SortBy,
                    request.SortByDir, request.StartRecord, request.RecordsPerPage, request.UtcOffset, request.FileName, xml,
                    request.Search);

                foreach (var notification in resp.Notifications)
                {
                    notification.Message = Regex.Replace(notification.Message, DISALLOWEDCHARACTERS, string.Empty);
                }
              
                resp.Status = StatusCode.SUCCESS;
                response = resp;
            }
            catch (Exception ex)
            {
                response.Status = StatusCode.FAIL;
                response.Errors = new List<string>(new string[] { ex.Message });
            }
            return response;
        }
    }
}
