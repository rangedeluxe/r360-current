﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Services.R360ServicesAPI.Services;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Repositories
{
    public class PaymentSearchRepository : IReadOnlyRepository<PaymentSearchResponseDto, PaymentSearchResponseDto, PaymentSearchRequestDto>
    {
        private const string MAXIMUM_WORKGROUPS_PREF = "MaximumWorkgroups";
        private const string LINE_BREAK_CHAR = "<br/>";
        private readonly IXmlTransformer<PaymentSearchXmlRequestDto> _requesttransformer;
        private readonly IXmlTransformer<PaymentSearchResponseDto> _responsetransformer;
        private readonly IPaymentSearchDataProvider _dataprovider;
        private readonly IServiceContext _servicecontext;
        private readonly IDisplayBatchIdProvider _displaybatchidprovider;
        private readonly IImageService _iconservice;
        private readonly IPermissionProvider _permissionprovider;
        private readonly IPreferencesProvider _preferencesprovider;

        public PaymentSearchRepository(
            IXmlTransformer<PaymentSearchXmlRequestDto> requesttransformer,
            IXmlTransformer<PaymentSearchResponseDto> responsetransformer, 
            IPaymentSearchDataProvider dataprovider,
            IServiceContext servicecontext,
            IDisplayBatchIdProvider batchidprovider,
            IImageService imageiconservice,
            IPermissionProvider permissionprovider,
            IPreferencesProvider preferencesprovider
            )
        {
            _requesttransformer = requesttransformer;
            _responsetransformer = responsetransformer;
            _dataprovider = dataprovider;
            _servicecontext = servicecontext;
            _displaybatchidprovider = batchidprovider;
            _iconservice = imageiconservice;
            _permissionprovider = permissionprovider;
            _preferencesprovider = preferencesprovider;
        }

        public PaymentSearchResponseDto Get(PaymentSearchRequestDto request)
        {
            var output = new PaymentSearchResponseDto();

            // Get the list of workgroups from our ID.
            var workgroups = _dataprovider.GetWorkgroups(request.Workgroup);
            var authorizedworkgroups = _dataprovider.GetAuthorizedWorkgroups();

            // Filter the list based on permissions.  
            var workgroupscopy = new List<WorkgroupDto>();
            workgroupscopy.AddRange(workgroups);
            foreach (var w in workgroupscopy)
            {
                if (!authorizedworkgroups.Any(x => x.Value.BankID == w.BankId && x.Value.LockboxID == w.ClientAccountId))
                    workgroups.Remove(w);
            }

            // Check to make sure the list of workgroups isn't exceeding the system setting's maximum.
            var max = int.Parse(_preferencesprovider.GetPreferenceByName(MAXIMUM_WORKGROUPS_PREF)
                .DefaultSetting);
            if (workgroups.Count > max)
            {
                output.Status = StatusCode.FAIL;
                output.Errors.Add($"The number of workgroups for the selected entity hierarchy is {workgroups.Count}.{LINE_BREAK_CHAR}The system will only allow {max} workgroups in a search.{LINE_BREAK_CHAR}Please select a lower level entity or contact your administrator.");
                return output;
            }

            // Check the viewing days to make sure we're not over our date span if the user doesn't have the override permission.
            if (!_permissionprovider.HasPermission(R360Permissions.Perm_DepositSearchSpan, R360Permissions.ActionType.View))
            {
                var failingboxes = new List<cOLLockbox>();
                foreach (var w in workgroups)
                {
                    var box = authorizedworkgroups.First(x => x.Value.BankID == w.BankId && x.Value.LockboxID == w.ClientAccountId)
                        .Value;

                    if (box.MaximumSearchDays > 0
                        && ipoDateTimeLib.DateDiff(DateInterval.Day, request.DateFrom, request.DateTo) >= box.MaximumSearchDays)
                        failingboxes.Add(box);
                }
                if (failingboxes.Any())
                {
                    var lowestbox = failingboxes
                        .OrderBy(x => x.MaximumSearchDays < x.OnlineViewingDays 
                            ? x.MaximumSearchDays 
                            : x.OnlineViewingDays)
                        .First();

                    output.Status = StatusCode.FAIL;
                    output.Errors.Add(string.Format("The Deposit Date range is limited to {0} day(s) when searching workgroup '{1}'.",
                            lowestbox.OnlineViewingDays > lowestbox.MaximumSearchDays
                                ? lowestbox.MaximumSearchDays
                                : lowestbox.OnlineViewingDays,
                            lowestbox.LongName));
                    return output;
                }
            }

            foreach (var w in workgroups)
            {
                var box = authorizedworkgroups.First(x => x.Value.BankID == w.BankId && x.Value.LockboxID == w.ClientAccountId)
                    .Value;

                // Check that date range values do not exceed lockbox viewing days
                if (ipoDateTimeLib.DateDiff(DateInterval.Day, request.DateFrom, DateTime.Today) > box.OnlineViewingDays)
                {
                    request.DateFrom = DateTime.Today.AddDays(box.OnlineViewingDays * -1);
                }
            }

            // Populate the request before sending it off to the transformer.
            var dto = new PaymentSearchXmlRequestDto()
            {
                DateFrom = request.DateFrom,
                DateTo = request.DateTo,
                Workgroups = workgroups,
                Session = _servicecontext.GetSessionID(),
                Start = request.Start,
                Length = request.Length,
                OrderBy = request.OrderBy,
                OrderDirection = request.OrderDirection,
                Criteria = request.Criteria,
                PaymentType = request.PaymentType,
                PaymentSource = request.PaymentSource
            };

            // Convert it to an XML object.
            var xml = _requesttransformer.ToXML(dto);

            // Fire it off to the SP.
            XmlDocument resultsxml;
            DataTable results;
            var success = _dataprovider.GetPaymentSearch(xml, out resultsxml, out results);

            // Kick out early if we failed the DB call.
            if (!success || results == null)
            {
                output.Errors.Add("Error retrieving the data from the database.");
                output.Status = R360Shared.StatusCode.FAIL;
                return output;
            }

            // Process the XML results.
            var totals = _responsetransformer.ToDTO(resultsxml);
            output.TotalRecords = totals.TotalRecords;

            // Process the DB results.
            foreach (DataRow r in results.Rows)
            {
                var res = new PaymentSearchResultDto()
                {
                    Account = r.GetString("Account"),
                    Amount = r.GetDouble("Amount"),
                    BankId = r.GetInteger("BankID"),
                    BatchId = r.GetBigInteger("BatchID"),
                    BatchNumber = r.GetInteger("BatchNumber"),
                    BatchSequence = r.GetInteger("BatchSequence"),
                    BatchSourceShortName = r.GetString("BatchSourceShortName"),
                    DDA = r.GetString("DDA"),
                    DepositDate = r.GetDateTime("DepositDate"),
                    DepositDateString = r.GetDateTime("DepositDate").ToString("MM/dd/yyyy"),
                    ImportTypeShortName = r.GetString("ImportTypeShortName"),
                    PaymentSource = r.GetString("PaymentSource"),
                    PaymentType = r.GetString("PaymentType"),
                    PICSDate = r.GetInteger("PICSDate"),
                    RemitterName = r.GetString("RemitterName"),
                    RT = r.GetString("RT"),
                    Serial = r.GetString("Serial"),
                    SourceBatchId = r.GetBigInteger("SourceBatchID"),
                    TransactionId = r.GetInteger("TransactionID"),
                    TransactionSequence = r.GetInteger("TxnSequence"),
                    WorkgroupId = r.GetInteger("ClientAccountID"),
                    WorkgroupKey = r.GetInteger("ClientAccountKey"),
                };
                res.ShowPaymentIcon = _iconservice.GetImageSize(res.PICSDate, res.BankId, res.WorkgroupId, res.BatchId, res.BatchSequence,
                    int.Parse(res.DepositDate.ToString("yyyyMMdd")), "c", 0, res.SourceBatchId, res.BatchSourceShortName, res.ImportTypeShortName) > 0;
                output.Results.Add(res);
            }

            // Process the metadata.
            output.Metadata = new PaymentSearchMetadata()
            {
                DisplayBatchId = _displaybatchidprovider.GetDisplayBatchIdProperty(workgroups)
            };

            // Finally donezo.
            return output;
        }
    }
}
