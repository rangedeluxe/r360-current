﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common;
using System.Data;
using WFS.RecHub.BusinessCommon;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Repositories
{
    public class BatchSummaryRepository : IReadOnlyRepository<BatchSummaryRequestDto, BatchSummaryResponseDto, BatchSummaryRequestDto>
    {
        private readonly IServiceContext serviceContext;
        private readonly ILockboxAPI lockboxApi;
        private readonly IPreferencesProvider preferences;
        private readonly IBatchDataProvider batchapi;

        public BatchSummaryRepository(IServiceContext serviceContext, ILockboxAPI lockboxapi, 
            IPreferencesProvider prefs, IBatchDataProvider batches)
        {
            this.serviceContext = serviceContext;
            this.lockboxApi = lockboxapi;
            this.preferences = prefs;
            this.batchapi = batches;
        }

        public BatchSummaryResponseDto Get(BatchSummaryRequestDto request)
        {
            var response = new BatchSummaryResponseDto();

            var authorizedworkgroups = lockboxApi.GetUserLockboxes();
            
            // Grab the workgroup, check to make sure we have access to it.
            var requestedworkgroup = authorizedworkgroups.Data
                .FirstOrDefault(x => x.Value.BankID == request.BankId && x.Value.LockboxID == request.ClientAccountId);
            if (requestedworkgroup.Equals(default(KeyValuePair<int, cOLLockbox>)))
            {
                response.Status = StatusCode.FAIL;
                response.Errors.Add("User does not have access to the requested workgroup.");
                return response;
            }
            var requestedworkgroupval = requestedworkgroup.Value;

            // Check the viewing days to make sure we're not over our date span.
            if (requestedworkgroupval.MaximumSearchDays > 0
                && ipoDateTimeLib.DateDiff(DateInterval.Day, request.FromDate, request.ToDate) >= requestedworkgroupval.MaximumSearchDays)
            {
                response.Status = StatusCode.FAIL;
                response.Errors.Add(string.Format("The Deposit Date range is limited to {0} day(s) when searching this workgroup.",
                        requestedworkgroupval.OnlineViewingDays > requestedworkgroupval.MaximumSearchDays
                            ? requestedworkgroupval.MaximumSearchDays
                            : requestedworkgroupval.OnlineViewingDays));
                return response;
            }

            // Check that date range values do not exceed lockbox viewing days
            if (ipoDateTimeLib.DateDiff(DateInterval.Day, request.FromDate, DateTime.Today) > requestedworkgroupval.OnlineViewingDays)
            {
                request.FromDate = DateTime.Today.AddDays(requestedworkgroupval.OnlineViewingDays * -1);
            }
            // Get the preferences for hiding/displaying data.
            var prefs = preferences.GetPreferences();
            response.AddPreferences(prefs);
            response.SiteCode = requestedworkgroup.Value.SiteCode;
            
            // Finally ready to grab our batches.
            var datatable = new DataTable();
            var recordstotal = 0;
            var recordsfiltered = 0;
            var transactionCount = 0;
            var checkCount = 0;
            var documentCount = 0;
            var checkTotal = 0.0M;

            if (!batchapi.GetBatchSummary(request.BankId, request.ClientAccountId, request.FromDate, request.ToDate,
                request.PaymentTypeId, request.PaymentSourceId, true, request.Start, request.Length, request.OrderBy, request.OrderDirection,
                request.Search, out recordstotal, out recordsfiltered, out datatable, out transactionCount, out checkCount, out documentCount, out checkTotal) || datatable == null)
            {
                response.Status = StatusCode.FAIL;
                response.Errors.Add("Error receiving the batch summary.");
                return response;
            }

            // Map the DTOs and finish up.
            var batches = new List<BatchSummaryDto>();
            foreach (DataRow row in datatable.Rows)
            {
                batches.Add(new BatchSummaryDto()
                {
                    BankId = request.BankId,
                    BatchID = row.GetBigInteger("BatchID"),
                    BatchNumber = row.GetString("BatchNumber"),
                    ClientAccountId = request.ClientAccountId,
                    DepositDate = row.GetDateTime("DepositDate"),
                    DocumentCount = row.GetInteger("DocumentCount"),
                    PaymentCount = row.GetInteger("CheckCount"),
                    PaymentSource = row.GetString("PaymentSource"),
                    PaymentType = row.GetString("PaymentType"),
                    SourceBatchID = row.GetBigInteger("SourceBatchID"),
                    TotalAmount = row.GetDecimal("CheckAmount"),
                    TransactionCount = row.GetInteger("TransactionCount"),
                    BatchSiteCode = row.GetInteger("BatchSiteCode"),
                    DDA = row.GetString("DDA")
                });
            }

            response.BatchSummaryList = batches;
            response.TotalRecords = recordstotal;
            response.FilteredRecords = recordsfiltered;
            response.TransactionCount = transactionCount;
            response.CheckCount = checkCount;
            response.DocumentCount = documentCount;
            response.CheckTotal = checkTotal;

            return response;
        } 
    }

}
