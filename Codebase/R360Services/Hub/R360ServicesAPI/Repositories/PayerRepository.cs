﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.ApplicationBlocks.DataAccess;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Repositories
{
    public class PayerRepository : ILookupRepository<PayerLookupRequestDto, PossibleResult<IEnumerable<PayerDto>>>
    {
        private ConnectionStringSettings _connectionStringSettings { get; set; }

        public PayerRepository(R360DBConnectionSettings connectionStringSettings)
        {
            if (connectionStringSettings == null)//defend here, don't create the object
                throw new ArgumentNullException("Must provide R360DBConnectionStringSettings");

            _connectionStringSettings = connectionStringSettings.Value;
        }
       
        public PossibleResult<IEnumerable<PayerDto>> TryGet(PayerLookupRequestDto request)
        {
            if (request == null)
                return Result.None<IEnumerable<PayerDto>>();

            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmSiteBankID", dbProviderFactory, DbType.Int32, ParameterDirection.Input, request.BankId),
                Database.CreateParm("@parmSiteClientAccountID", dbProviderFactory, DbType.Int32, ParameterDirection.Input, request.WorkgroupId),
                Database.CreateParm("@parmRoutingNumber", dbProviderFactory, DbType.String, ParameterDirection.Input, request.RoutingNumber),
                Database.CreateParm("@parmAccount", dbProviderFactory, DbType.String, ParameterDirection.Input, request.Account)
            };

            var dataSetResult = 
                Database.GetDataSet(_connectionStringSettings, CommandType.StoredProcedure, "RecHubData.usp_WorkgroupPayers_Get", parms);

            if (!dataSetResult.HasData())
                return Result.None<IEnumerable<PayerDto>>();

            return Result.Real(dataSetResult.Tables[0].ToObjectList<PayerDto>() as IEnumerable<PayerDto>);
        }
    }
}
