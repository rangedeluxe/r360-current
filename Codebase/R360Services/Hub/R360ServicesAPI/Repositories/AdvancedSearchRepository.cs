﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using System.Globalization;
using WFS.RecHub.Common;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Services.R360ServicesAPI.Services;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Repositories
{
    public class AdvancedSearchRepository : IReadOnlyRepository<AdvancedSearchDto, AdvancedSearchResultsDto, AdvancedSearchDto>
    {
        private const int TOOMANYRESULTS = -99;
        private readonly IServiceContext _servicecontext;
        private readonly IAdvancedSearchDataProvider _dataprovider;
        private readonly IImageService _iconservice;
        private readonly IPermissionProvider _permissionservice;

        // Not services.
        private readonly AdvancedSearchRequestDataTableTransformer _requesttransformer;
        private readonly AdvancedSearchResponseXmlTransformer _responsetransformer;

        public AdvancedSearchRepository(
            IServiceContext context,
            IAdvancedSearchDataProvider dataprovider,
            IImageService imageiconservice,
            IPermissionProvider permissionservice)
        {
            _servicecontext = context;
            _dataprovider = dataprovider;
            _requesttransformer = new AdvancedSearchRequestDataTableTransformer();
            _responsetransformer = new AdvancedSearchResponseXmlTransformer();
            _iconservice = imageiconservice;
            _permissionservice = permissionservice;
        }

        public AdvancedSearchResultsDto Get(AdvancedSearchDto req)
        {
            var output = new AdvancedSearchResultsDto();
            output.Results = new List<AdvancedSearchResultDto>();

            var authorizedworkgroups = _dataprovider.GetAuthorizedWorkgroups();

            // Grab the workgroup, check to make sure we have access to it.
            var requestedworkgroup = authorizedworkgroups
                .FirstOrDefault(x => x.Value.BankID == req.BankId && x.Value.LockboxID == req.WorkgroupId);
            if (requestedworkgroup.Equals(default(KeyValuePair<int, cOLLockbox>)))
            {
                output.Status = StatusCode.FAIL;
                output.Errors.Add("User does not have access to the requested workgroup.");
                return output;
            }
            var requestedworkgroupval = requestedworkgroup.Value;

            // Check the viewing days to make sure we're not over our date span.
            if (requestedworkgroupval.MaximumSearchDays > 0
                && ipoDateTimeLib.DateDiff(DateInterval.Day, req.DepositDateFrom, req.DepositDateTo) >= requestedworkgroupval.MaximumSearchDays)
            {
                output.Status = StatusCode.FAIL;
                output.Errors.Add(string.Format("The Deposit Date range is limited to {0} day(s) when searching this workgroup.",
                        requestedworkgroupval.OnlineViewingDays > requestedworkgroupval.MaximumSearchDays
                            ? requestedworkgroupval.MaximumSearchDays
                            : requestedworkgroupval.OnlineViewingDays));
                return output;
            }

            // Check that date range values do not exceed lockbox viewing days
            if (ipoDateTimeLib.DateDiff(DateInterval.Day, req.DepositDateFrom, DateTime.Today) > requestedworkgroupval.OnlineViewingDays)
            {
                req.DepositDateFrom = DateTime.Today.AddDays(requestedworkgroupval.OnlineViewingDays * -1);
            }

            // Arrange our data.
            var request = new AdvancedSearchRequestDto()
            {
                BankId = req.BankId,
                WorkgroupId = req.WorkgroupId,
                DepositDateFrom = req.DepositDateFrom,
                DepositDateTo = req.DepositDateTo,
                RecordsPerPage = req.Length,
                StartRecord = req.Start,
                SessionId = _servicecontext.GetSessionID(),
                MarkSenseOnly = false,
                COTSOnly = false,
                WhereClauses = req.WhereClauses,
                SortBy = req.OrderBy,
                SortByDir = req.OrderDirection,
                SortByDisplayName = req.OrderByDisplayName,
                SelectFields = req.SelectFields,
                PaymentSourceKey = req.PaymentSourceKey,
                PaymentTypeKey = req.PaymentTypeKey,
                Paginate = req.PaginateResults
            };

            // Find the default columns that the SP does not expect to see, and remove them.
            var defaults = new List<DataEntryDto>()
            {
                new DataEntryDto() { FieldName="Batch.Deposit_Date", Title="Deposit Date", Type="11" },
                new DataEntryDto() { FieldName="Batch.BatchNumber", Title="Batch", Type="6" },
                new DataEntryDto() { FieldName="Batch.PaymentSource", Title="Payment Source", Type="1" },
                new DataEntryDto() { FieldName="Batch.PaymentType", Title="Payment Type", Type="1" },
                new DataEntryDto() { FieldName="Transactions.TxnSequence", Title="Transaction", Type="6" },
                new DataEntryDto() { FieldName="Batch.SourceBatchID", Title="Batch ID", Type="6" },
                new DataEntryDto() { FieldName="Batch.BatchNumber", Title="Batch Number", Type="6" },
            };
            var selectedfields = new List<AdvancedSearchWhereClauseDto>();
            foreach (var s in request.SelectFields)
                selectedfields.Add(s);
            var toremove = request.SelectFields
                .Where(x => defaults.Any(y => y.FieldName == x.FieldName && y.Title == x.ReportTitle && y.Type == ((int)x.DataType).ToString()))
                .ToList();
            foreach (var r in toremove)
                request.SelectFields.Remove(r);

            // Convert to datatable for the proc;
            var tables = _requesttransformer.ToDataTable(request);

            // Call the proc.
            DataTable dtresults;
            XmlDocument xmlresults;
            var success = _dataprovider.LockboxSearch(tables, out xmlresults, out dtresults);
            if (dtresults == null)
                dtresults = new DataTable();

            // Restore the fields.
            request.SelectFields = selectedfields;

            // Transform the output.
            output.Totals = _responsetransformer.ToDTO(xmlresults);

            // Kick out if we have too many results to process.
            if (output.Totals.TotalRecords == TOOMANYRESULTS)
            {
                output.Status = StatusCode.FAIL;
                output.Errors.Add(
                    "The search you submitted results in a response too large to return.  " +
                    "Please narrow your criteria and resubmit.");
                return output;
            }

            // Process data
            foreach (DataRow row in dtresults.Rows)
            {
                // Standard fields.
                var r = new AdvancedSearchResultDto()
                {
                    BankId = row.GetInteger("BankID"),
                    BatchNumber = row.GetInteger("BatchNumber"),
                    BatchId = row.GetBigInteger("BatchID"),
                    BatchSequence = row.GetInteger("BatchSequence"),
                    DepositDate = row.GetDateTime("Deposit_Date"),
                    DepositDateString = row.GetDateTime("Deposit_Date").ToString("MM/dd/yyyy"),
                    PaymentAmount = row.GetNullableDouble("Amount"),
                    PaymentSource = row.GetString("PaymentSource"),
                    PaymentType = row.GetString("PaymentType"),
                    PICSDate = DateTime.ParseExact(row.GetInteger("PICSDate").ToString(), "yyyyMMdd", CultureInfo.InvariantCulture),
                    PICSDateInt = row.GetInteger("PICSDate"),
                    SourceBatchId = row.GetBigInteger("SourceBatchID"),
                    TransactionId = row.GetInteger("TransactionID"),
                    TransactionSequence = row.GetInteger("TxnSequence"),
                    WorkgroupId = row.GetInteger("ClientAccountID"),
                    BatchSourceShortName = row.GetString("BatchSourceShortName"),
                    ImportTypeShortName = row.GetString("ImportTypeShortName"),
                    DocumentCount = row.GetInteger("DocumentCount"),
                    CheckCount = row.GetInteger("CheckCount"),
                    MarkSenseDocumentCount = row.GetInteger("MarkSenseDocumentCount")
                };


                r.ShowPaymentIcon = (r.PaymentType.ToUpper() == "ACH" || r.PaymentType.ToUpper() == "WIRE") || _iconservice.BatchSequenceImagesExist(
                                          new ImageRequestDto
                                          {
                                              BankId = r.BankId,
                                              SourceBatchId = r.SourceBatchId,
                                              BatchPaymentSource = r.BatchSourceShortName,
                                              DepositDate = r.DepositDate,
                                              PicsDate = r.PICSDateInt,
                                              IsCheck = true,
                                              BatchSequence = row.GetInteger("ChecksBatchSequence"),
                                              WorkgroupId = r.WorkgroupId,
                                              ImportTypeShortName =  r.ImportTypeShortName
                                          }).ImagesAvailable;

                //This is temporary beginning put back in until we can update the AdvSearch SP to return the document batch sequence
                r.ShowDocumentIcon = r.DocumentCount > 0;
                //r.ShowDocumentIcon = (r.PaymentType.ToUpper() == "ACH" || r.PaymentType.ToUpper() == "WIRE") || _iconservice.BatchSequenceImagesExist(
                //                           new ImageRequestDto
                //                           {
                //                               BankId = r.BankId,
                //                               SourceBatchId = r.SourceBatchId,
                //                               BatchPaymentSource = r.BatchSourceShortName,
                //                               DepositDate = r.DepositDate,
                //                               PicsDate = r.PICSDateInt,
                //                               IsCheck = false,
                //                               BatchSequence = ,
                //                               WorkgroupId = r.WorkgroupId
                //                           }).ImagesAvailable;

                r.ShowAllIcon = r.ShowPaymentIcon && r.ShowDocumentIcon;

                r.IsMarkSense = r.MarkSenseDocumentCount > 0;

                // Handling select fields.

                var de = new List<DataEntryDto>();

                foreach (var f in request.SelectFields)
                {
                    // We selected some fields. Need to populate them with data.
                    // There can be multiple columns returned for each Report Title (RecHubData.dimWorkgroupDataEntryColumns.UILabel) requested.
                    var responsecolumn =
                        output.Totals.SelectFields.FindAll(
                            x => x.Table == f.Table && x.ReportTitle == f.ReportTitle && x.DataType == f.DataType);

                    // It could be a standard column, so we just mock one up.
                    if (responsecolumn.Count == 0)
                        responsecolumn = new List<AdvancedSearchSelectFieldResponseDto>()
                        {
                            new AdvancedSearchSelectFieldResponseDto
                            {
                                ColumnId = null,
                                DataType = f.DataType,
                                Table = f.Table,
                                FieldName = f.FieldName,
                                ReportTitle = f.ReportTitle
                            }
                        };

                    //Walk the list of columns returned and add each one.
                    foreach (var rColumn in responsecolumn)
                    {
                        var columnname = rColumn.Table == AdvancedSearchWhereClauseTable.None
                            ? rColumn.FieldName
                            : rColumn.FieldName.Contains(".")
                                ? rColumn.FieldName.Split('.')[1]
                                : rColumn.Table.ToString() + rColumn.FieldName;
                        if (!string.IsNullOrEmpty(rColumn.ColumnId))
                            columnname += "_" + rColumn.ColumnId;
                        var deLookup = de.Find(d => d.Title == rColumn.ReportTitle);
                        if (deLookup == null)
                        {
                            de.Add(new DataEntryDto()
                            {
                                FieldName = rColumn.FieldName,
                                Title = rColumn.ReportTitle,
                                Type = ((int)rColumn.DataType).ToString(),
                                Value = row.GetString(columnname)
                            });
                            if (!string.IsNullOrEmpty(row.GetString(columnname)))
                                break;
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(row.GetString(columnname)))
                                continue;
                            de.Find(d => d.Title == rColumn.ReportTitle).Value = row.GetString(columnname);
                            break;
                        }
                    }
                }

                r.DataEntry = de;

                output.Results.Add(r);
            }

            // Set the metadata for the results.
            output.Metadata = new AdvancedSearchMetadata()
            {
                WorkgroupId = requestedworkgroupval.LockboxID,
                WorkgroupName = string.IsNullOrWhiteSpace(requestedworkgroupval.LongName)
                    ? string.Format("{0} - {1}", requestedworkgroupval.LockboxID, requestedworkgroupval.ShortName)
                    : requestedworkgroupval.LongName,
                DisplayBatchId = requestedworkgroupval.DisplayBatchID,
                MaxRowsForDownload = _dataprovider.GetMaxRowsForDownload(),
                HasDownloadPermission = _permissionservice.HasPermission(R360Permissions.Perm_AdvanceSearchDownload, R360Permissions.ActionType.View),
                HasPayments = output.Results.Any(x => x.ShowPaymentIcon),
                HasDocuments = output.Results.Any(x => x.ShowDocumentIcon)
            };

            // Process the totals into the metadata string.
            var criteriastring = "Workgroup = " +  requestedworkgroupval.LongName + "; ";
            criteriastring += "Deposit Date Is Greater Than or Equal to " + request.DepositDateFrom.ToString("MM/dd/yyyy") + " and Less Than or Equal to " + request.DepositDateTo.ToString("MM/dd/yyyy") + "; ";

            if (request.PaymentSourceKey > -1)
                criteriastring += "Payment Source equals " + _dataprovider.GetPaymentSourceName(request.PaymentSourceKey) + "; ";

            if (request.PaymentTypeKey > -1)
                criteriastring += "Payment Type equals " + _dataprovider.GetPaymentTypeName(request.PaymentTypeKey) + "; ";

            if (request.WhereClauses != null)
            {
                var added = new List<AdvancedSearchWhereClauseDto>();
                foreach (var c in request.WhereClauses)
                {
                    // for standard elements, we might have duplicate entries with the 'equals' operator.
                    if (added.Any(x => x.FieldName == c.FieldName && x.Operator == c.Operator && x.Table == c.Table))
                        continue;

                    var op = c.Operator == AdvancedSearchWhereClauseOperator.BeginsWith ? "Begins With"
                        : c.Operator == AdvancedSearchWhereClauseOperator.Contains ? "Contains"
                        : c.Operator == AdvancedSearchWhereClauseOperator.EndsWith ? "Ends With"
                        : c.Operator == AdvancedSearchWhereClauseOperator.Equals ? "Equals"
                        : c.Operator == AdvancedSearchWhereClauseOperator.IsGreaterThan ? "Is Greater Than"
                        : c.Operator == AdvancedSearchWhereClauseOperator.IsLessThan ? "Is Less Than"
                        : "No Operator";
                    if ((c.Operator == AdvancedSearchWhereClauseOperator.IsLessThan
                        || c.Operator == AdvancedSearchWhereClauseOperator.IsGreaterThan)
                        && c.IsStandard)
                        op += " or Equal to";
                    criteriastring += c.ReportTitle + " " + op + " " + c.Value + "; ";
                    added.Add(c);
                }
            }

            output.Metadata.CriteraString = criteriastring;

            if (req.ActivityCode.HasValue)
            {
                // Let the logging happen in the background - the client doesn't need to wait
                _dataprovider.LogActivityInBackground(
                    bankId: req.BankId,
                    workgroupId: req.WorkgroupId,
                    activityCode: req.ActivityCode.Value,
                    itemCount: output.Results.Count,
                    moduleName: "Advanced Search CSV");
            }

            return output;
        }

    }
}
