﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
using WFS.RecHub.DAL;
using System.Data;
using WFS.RecHub.BusinessCommon;
using WFS.RecHub.OLFServices.DataTransferObjects;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Repositories
{
    public class TransactionDetailRepository : IReadOnlyRepository<TransactionDetailRequestDto, TransactionDetailResponseDto, TransactionDetailRequestDto>
    {
        private readonly IServiceContext serviceContext;

        //public so it can be unit tested
        public bool ImageIconVisible(ImageRequestDto imageRequest, bool isElectronic, bool isLinkedToDocument, ImageAPI imgApi)
        {
            // set to true, this is the state if it is an electronic batch
            var iconVisible = true;

            if (!isLinkedToDocument)
            {
                iconVisible = false;
            }
            else
            {
                if (!isElectronic)
                    iconVisible = imgApi.BatchSequenceImagesExist(imageRequest).ImagesAvailable;
            }

            return iconVisible;
        }

        //public so it can be unit tested
        public bool ImageIconVisible(ImageRequestDto imageRequest, bool isElectronic, ImageAPI imgApi)
        {
            // set to true, this is the state if it is an electronic batch
            var iconVisible = true;

            if (!isElectronic)
                iconVisible = imgApi.BatchSequenceImagesExist(imageRequest).ImagesAvailable;

            return iconVisible;
        }

        public TransactionDetailRepository(IServiceContext serviceContext)
        {
            this.serviceContext = serviceContext;
        }
        public TransactionDetailResponseDto Get(TransactionDetailRequestDto request)
        {

            var response = new TransactionDetailResponseDto();
            var lockboxservice = new LockboxAPI(serviceContext);
            var authorizedworkgroups = lockboxservice.GetUserLockboxes();

            //authorize batch/transaction to user
            var requestedworkgroup = authorizedworkgroups.Data
                .FirstOrDefault(x => x.Value.BankID == request.BankId && x.Value.LockboxID == request.WorkgroupID);
            if (requestedworkgroup.Equals(default(KeyValuePair<int, cOLLockbox>)))
            {
                response.Status = StatusCode.FAIL;
                response.Errors.Add("The transaction requested is not valid..");
                return response;
            }
            response.WorkgroupDisplayName = requestedworkgroup.Value.LongName;
            response.DisplayBatchID = requestedworkgroup.Value.DisplayBatchID;

            // Get the preferences for hiding/displaying data.
            var pref = new OLPreferences(serviceContext.SiteKey);
            pref.LoadOLPreferences();
            response.AddPreferences(pref.GetPreferences);

            var dal = new cItemProcDAL(serviceContext.SiteKey);
            var imgApi = new ImageAPI(serviceContext);
            //Get Transaction Summary 
            var retVal = false;
            DataTable dtTxn;
            if (request.TransactionId >= 0)
                retVal = dal.GetTransactionDetails(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID,
                  request.BatchID, request.DepositDate, request.TransactionId, false, out dtTxn);
            else
                retVal = dal.GetTransactionDetailsBySequence(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID,
                    request.BatchID, request.DepositDate, request.TransactionSequence, false, out dtTxn);

            if (!retVal)
            {
                response.Status = StatusCode.FAIL;
                response.Errors.Add("Unable to retrieve the transaction requested.");
                return response;
            }
            response.AddTransactionDetails(dtTxn);

            //Get all the transactions in batch
            DataTable batchTransactions;
            if (dal.GetBatchTransactions(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID,
                request.DepositDate,
                request.BatchID, out batchTransactions))
            {
                response.AddBatchTransactions(batchTransactions);
            }
            //Gets Payment/Checks Transactions
            DataTable dtChecks = null;

            if (request.TransactionId >= 0)
            {
                retVal = dal.GetTransactionChecks(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID,
                    request.DepositDate, request.BatchID, request.TransactionId, out dtChecks);
            }
            else
            {
                retVal = dal.GetTransactionChecksBySequence(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID,
                    request.DepositDate, request.BatchID, request.TransactionSequence, out dtChecks);
            }
            response.AddPayments(dtChecks);
            //Add Images
            foreach (var payment in response.Payments)
            {
                payment.CheckIconVisible = ImageIconVisible(new ImageRequestDto
                {
                    BankId = response.BankID,
                    SourceBatchId = response.SourceBatchID,
                    BatchPaymentSource = payment.BatchSourceShortName,
                    PicsDate = payment.PICSDate,
                    IsCheck = true,
                    BatchSequence = payment.BatchSequence,
                    WorkgroupId = response.WorkgroupID,
                    ImportTypeShortName = response.ImportTypeShortName
                }, response.IsElectronic, imgApi);
            }
            //R360-9330 - Image Icons Aren't Appearing when ImageStorageTyp is set to 3 on rechub.ini file FileGroup in dto
            //Get DE fields for checks
            DataTable deColumns = null;
            bool dalSuccess = dal.GetDataEntryFields(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID, request.BatchID,
                                                     request.DepositDate, out deColumns);

            if ((deColumns == null) || (deColumns.Rows == null))
            {
                response.Status = StatusCode.FAIL;
                response.Errors.Add("Unable to retrieve data entry fields.");
                return response;
            }

            if(dalSuccess && deColumns.Rows.Count > 0)
            {
                //checks if there is a deCol that corresponds to checks and turns it into an ienumerable
                var checksDeCols =
                    deColumns.Rows.Cast<DataRow>()
                        .Where(
                            row =>
                                string.Equals(row["TableName"].ToString(), "checks", StringComparison.OrdinalIgnoreCase))
                        .ToList();

                if (checksDeCols.Any())
                {
                    foreach (var payment in response.Payments)
                    {
                        //build the DE fields collection   
                        DataTable deChecks = null;
                        dal.GetTransactionChecksDE(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID, request.BatchID,
                                                   request.DepositDate, payment.TransactionId, payment.BatchSequence, out deChecks);

                        if ((deChecks == null) || (deChecks.Rows == null))
                        {
                            response.Status = StatusCode.FAIL;
                            response.Errors.Add("Unable to retrieve transaction checks.");
                            return response;
                        }

                        var dataEntryFields = new List<DataEntryDto>();
                        //converts it to ienumerable
                        var dataEntryChecks = deChecks.Rows.Cast<DataRow>().Select(row => row).ToList();
                        foreach (var col in checksDeCols)
                        {
                            var deField = new DataEntryDto
                            {
                                FieldName = col["FldName"].ToString(),
                                Title = col["FldTitle"].ToString(),
                                Type = col["FldDataTypeEnum"].ToString()
                            };

                            var field = dataEntryChecks.FirstOrDefault(
                                x =>
                                    string.Equals(x["TableName"].ToString(), col["TableName"].ToString(),
                                        StringComparison.OrdinalIgnoreCase)
                                    &&
                                    string.Equals(x["FldName"].ToString(), col["FldName"].ToString(),
                                        StringComparison.OrdinalIgnoreCase));

                            deField.Value = field != null ? field["DataEntryValue"].ToString() : string.Empty;
                            dataEntryFields.Add(deField);

                        }
                        payment.DataEntryFields = dataEntryFields;
                    }
                }
            }

            //Get Stubs
            var objTransaction = new cTransaction(request.BankId , request.WorkgroupID, request.DepositDate, request.BatchID,
                    -1, request.TransactionId , request.TransactionSequence);
            DataTable dtStubs;
            if (request.TransactionId > 0)
            {
                dal.GetTransactionStubs(serviceContext.GetSessionID(), objTransaction, out dtStubs);
            }
            else
            {
                dal.GetTransactionStubsBySequence(serviceContext.GetSessionID(), objTransaction, out dtStubs);
            }

            response.AddStubs(dtStubs);
            foreach (var stub in response.Stubs)
            {
                DataTable dtStubsImg = null;
                stub.StubIconVisible = false;

                if (dal.GetDocument(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID,
                                    request.DepositDate,
                                    request.BatchID, stub.TransactionId, stub.DocumentBatchSequence,
                                    response.Preferences.Any(x =>  x.Name.ToLower() == "displayscannedcheckonline" && x.Value == "Y") ,
                                    out dtStubsImg)
                    && (dtStubsImg != null)
                    && (dtStubsImg.Rows != null))
                {
                    stub.StubIconVisible = ImageIconVisible(new ImageRequestDto
                    {
                        BankId = response.BankID,
                        SourceBatchId = response.SourceBatchID,
                        BatchPaymentSource = response.BatchSourceShortName,
                        PicsDate = response.PICSDate,
                        IsCheck = false,
                        BatchSequence = stub.DocumentBatchSequence,
                        WorkgroupId = response.WorkgroupID,
                        ImportTypeShortName = response.ImportTypeShortName
                    }, response.IsElectronic, stub.IsLinkedToDocument, imgApi);
                }
            }

            //Get Stubs DE Fields
            var stubsDeCols =
                deColumns.Rows.Cast<DataRow>()
                    .Where(
                        row => string.Equals(row["TableName"].ToString(), "stubs", StringComparison.OrdinalIgnoreCase))
                    .ToList();
            if (stubsDeCols.Any())
            {
                foreach (var transactionDetailStub in response.Stubs)
                {
                    DataTable deStubs = null;
                    dal.GetTransactionStubsDE(serviceContext.GetSessionID(), request.BankId, request.WorkgroupID,
                        request.BatchID,
                        request.DepositDate, transactionDetailStub.TransactionId, transactionDetailStub.BatchSequence, out deStubs);

                    if ((deStubs == null) || (deStubs.Rows == null))
                    {
                        response.Status = StatusCode.FAIL;
                        response.Errors.Add("Unable to retrieve transaction stubs.");
                        return response;
                    }

                    var dataEntryFields = new List<DataEntryDto>();

                    var dataEntryStubs = deStubs.Rows.Cast<DataRow>().Select(row => row).ToList();
                    foreach (DataRow col in stubsDeCols)
                    {
                        var DEField = new DataEntryDto
                        {
                            FieldName = col["FldName"].ToString(),
                            Title = col["FldTitle"].ToString(),
                            Type = col["FldDataTypeEnum"].ToString()

                        };
                        var field =
                            dataEntryStubs.FirstOrDefault(
                                x =>
                                    string.Equals(x["TableName"].ToString(), col["TableName"].ToString(),
                                        StringComparison.OrdinalIgnoreCase) &&
                                    string.Equals(x["FldName"].ToString(), col["FldName"].ToString(),
                                        StringComparison.OrdinalIgnoreCase));
                        DEField.Value = field != null ? field["DataEntryValue"].ToString() : string.Empty;
                        dataEntryFields.Add(DEField);
                    }
                    transactionDetailStub.DataEntryFields = dataEntryFields;
                }
            }
            //Get Documents
            objTransaction = new cTransaction(request.BankId, request.WorkgroupID, request.DepositDate , request.BatchID,
                                               -1 , request.TransactionId, request.TransactionSequence);
            DataTable dtDocuments = null;
            if (request.TransactionId > -1)
            {
                retVal = dal.GetTransactionDocuments(serviceContext.GetSessionID(), objTransaction, false,
                    out dtDocuments);
            }
            else
            {
                retVal = dal.GetTransactionDocumentsBySequence(serviceContext.GetSessionID(), objTransaction, false,
                    out dtDocuments);
            }
            if (retVal)
            {
                response.AddDocuments(dtDocuments);
            }
            foreach (var doc in response.Documents)
            {
                //get document image sizes
                int picsDate = int.TryParse(doc.PicsDate, out picsDate) ? picsDate : 0;
                doc.DocumentIconVisible = ImageIconVisible(new ImageRequestDto
                {
                    BankId = response.BankID,
                    SourceBatchId = response.SourceBatchID,
                    BatchPaymentSource = doc.BatchSourceShortName,
                    PicsDate = picsDate,
                    IsCheck = false,
                    BatchSequence = doc.BatchSequence,
                    WorkgroupId = response.WorkgroupID,
                    ImportTypeShortName = response.ImportTypeShortName
                }, response.IsElectronic, imgApi);
            }
            //R360-9330 - Image Icons Aren't Appearing when ImageStorageTyp is set to 3 on rechub.ini file FileGroup in dto
            response.ViewAllImagesIconVisible = response.Payments.Any(p => p.CheckIconVisible) ||
                                                response.Documents.Any(d => d.DocumentIconVisible);
            return response;
        }


    }
}
