﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Repositories
{
    public class AdvancedSearchStoredQueryRepository : 
        IListRepository<AdvancedSearchStoredQueryDto, 
            Guid, 
            Guid, 
            BaseGenericResponse<AdvancedSearchStoredQueryDto>, 
            BaseGenericResponse<List<AdvancedSearchStoredQueryDto>>, 
            Guid>
    {

        private readonly IXmlTransformer<AdvancedSearchRequestDto> _xmltransformer;
        private readonly IAdvancedSearchStoredQueryDataProvider _dataprovider;

        public AdvancedSearchStoredQueryRepository(
            IXmlTransformer<AdvancedSearchRequestDto> xmltransformer,
            IAdvancedSearchStoredQueryDataProvider dataprovider)
        {
            _xmltransformer = xmltransformer;
            _dataprovider = dataprovider;
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> Delete(Guid entity)
        {
            var output = new BaseGenericResponse<AdvancedSearchStoredQueryDto>();
            var query = _dataprovider.Get(entity);
            if (query == null)
            {
                output.Status = StatusCode.FAIL;
                output.Errors.Add("Could not find that query.");
                return output;
            }

            output.Status = _dataprovider.Delete(query.Id)
                ? StatusCode.SUCCESS
                : StatusCode.FAIL;
            return output;
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> Get(Guid id)
        {
            var output = new BaseGenericResponse<AdvancedSearchStoredQueryDto>();

            var query = _dataprovider.Get(id);
            output.Data = query;

            // Load up the data.
            var doc = new XmlDocument();
            doc.LoadXml(query.XMLParams);
            query.XMLParams = string.Empty;

            // Transform it into the DTO.
            var dto = _xmltransformer.ToDTO(doc);
            query.Query = new AdvancedSearchDto()
            {
                BankId = dto.BankId,
                WorkgroupId = dto.WorkgroupId,
                DepositDateFrom = dto.DepositDateFrom,
                DepositDateTo = dto.DepositDateTo,
                Length = dto.RecordsPerPage,
                Start = dto.StartRecord,
                WhereClauses = dto.WhereClauses,
                OrderBy = dto.SortBy,
                OrderDirection = dto.SortByDir,
                OrderByDisplayName = dto.SortByDisplayName,
                SelectFields = dto.SelectFields,
                PaymentSourceKey = dto.PaymentSourceKey,
                PaymentTypeKey = dto.PaymentTypeKey
            };

            return output;
        }

        public BaseGenericResponse<List<AdvancedSearchStoredQueryDto>> GetAll()
        {
            var output = new BaseGenericResponse<List<AdvancedSearchStoredQueryDto>>();

            var queries = _dataprovider.GetAll();
            output.Data = queries.ToList();

            return output;
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> Save(AdvancedSearchStoredQueryDto entity)
        {
            var output = new BaseGenericResponse<AdvancedSearchStoredQueryDto>();

            if (entity == null || entity.Query == null)
            {
                output.Status = StatusCode.FAIL;
                output.Errors.Add("Query required for save.");
                return output;
            }

            // Validation - Checking for duplicate query names.
            var queries = _dataprovider.GetAll();
            if (queries == null)
            {
                output.Status = R360Shared.StatusCode.FAIL;
                output.Errors.Add("An unexpected error occurred while retreiving saved queries.");
                return output;
            }
            var count = queries.Count();

            // Make sure there's not currently a query with the same name.
            var q = queries.FirstOrDefault(x => x.Name.ToUpper() == entity.Name.ToUpper());
            // Case: we're inserting with a duplicate name.
            if (entity.Id == Guid.Empty && q != null)
            {
                output.Status = R360Shared.StatusCode.FAIL;
                output.Errors.Add("A saved query with that name already exists.");
                return output;
            }
            // Case: we're re-naming a query to a duplicate name.
            if (entity.Id != Guid.Empty && q != null && q.Id != entity.Id)
            {
                output.Status = R360Shared.StatusCode.FAIL;
                output.Errors.Add("A saved query with that name already exists.");
                return output;
            }

            // Validation - Make sure we didn't exceed the max queries system setting.
            var max = _dataprovider.GetMaxQueries();
            if (entity.Id == Guid.Empty && count >= max)
            {
                output.Status = R360Shared.StatusCode.FAIL;
                output.Errors.Add("Maximum number of saved queries reached.");
                return output;
            }

            // Arrange our data.
            var request = new AdvancedSearchRequestDto()
            {
                BankId = entity.Query.BankId,
                WorkgroupId = entity.Query.WorkgroupId,
                DepositDateFrom = entity.Query.DepositDateFrom,
                DepositDateTo = entity.Query.DepositDateTo,
                RecordsPerPage = entity.Query.Length,
                StartRecord = entity.Query.Start,
                MarkSenseOnly = false,
                COTSOnly = false,
                WhereClauses = entity.Query.WhereClauses,
                SortBy = entity.Query.OrderBy,
                SortByDir = entity.Query.OrderDirection,
                SortByDisplayName = entity.Query.OrderByDisplayName,
                SelectFields = entity.Query.SelectFields,
                PaymentSourceKey = entity.Query.PaymentSourceKey,
                PaymentTypeKey = entity.Query.PaymentTypeKey
            };
            if (request.WhereClauses == null)
                request.WhereClauses = new List<AdvancedSearchWhereClauseDto>();
            if (request.SelectFields == null)
                request.SelectFields = new List<AdvancedSearchWhereClauseDto>();

            // Grab our XML and save it.
            var xml = _xmltransformer.ToXML(request);
            var result = _dataprovider.Save(entity, xml);
            if (result)
                output.Data = entity;
            else
            {
                output.Status = R360Shared.StatusCode.FAIL;
                output.Errors.Add("An error occurred while saving the saved query.");
                return output;
            }
            return output;
        }
    }
}