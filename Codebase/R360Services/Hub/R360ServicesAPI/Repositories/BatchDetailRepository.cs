﻿using System.Collections.Generic;
using System.Data;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Services.R360ServicesAPI.Services;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.Repositories
{
    public class BatchDetailRepository :
        IReadOnlyRepository<BatchDetailRequestDto, BatchDetailResponseDto, BatchDetailRequestDto>
    {
        private readonly IServiceContext serviceContext;
        private readonly IImageService _iconservice;
        private readonly IBatchDataProvider _batchservice;

        public BatchDetailRepository(IServiceContext serviceContext,
            IImageService imageservice, IBatchDataProvider batchservice)
        {
            this.serviceContext = serviceContext;
            this._iconservice = imageservice;
            this._batchservice = batchservice;
        }

        public BatchDetailResponseDto Get(BatchDetailRequestDto request)
        {
            var response = new BatchDetailResponseDto();

            var datatable = new DataTable();
            var recordstotal = 0;
            var recordsfiltered = 0;
            if (!_batchservice.GetBatchDetail(request.BatchId, request.DepositDate, true,
                request.Start, request.Length, request.OrderBy, request.OrderDirection, request.Search, out recordstotal, out recordsfiltered, out datatable))
            {
                response.Status = StatusCode.FAIL;
                response.Errors.Add("Error receiving the batch summary.");
                return response;
            }

            // Map the DTOs and finish up.
            var batches = new List<BatchDetailDto>();
	        if (datatable != null)
	        {
		        foreach (DataRow row in datatable.Rows)
		        {
			        var dto = new BatchDetailDto
			        {
				        SourceBatchId = row.GetBigInteger("SourceBatchID"),
				        BatchSiteCode = row.GetInteger("BatchSiteCode"),
				        BatchId = request.BatchId,
				        BatchNumber = row.GetString("BatchNumber"),
				        DepositDate = row.GetDateTime("DepositDate"),
				        PICSDate = row.GetString("PICSDate"),
				        ProcessingDate = row.GetDateTime("ProcessingDate"),
				        TransactionID = row.GetInteger("TransactionID"),
				        TxnSequence = row.GetInteger("TxnSequence"),
				        BatchSequence = row.GetInteger("BatchSequence"),
				        DocumentBatchSequence = row.GetInteger("DocumentBatchSequence"),
				        DocumentCount = row.GetInteger("DocumentCount"),
				        CheckCount = row.GetInteger("CheckCount"),
				        StubCount = row.GetInteger("StubCount"),
				        PaymentSource = row.GetString("PaymentSource"),
				        PaymentType = row.GetString("PaymentType"),
				        TotalAmount = row.GetDecimal("BatchTotal"),
				        Amount = row.GetDecimal("Amount"),
				        TransactionCount = row.GetInteger("TransactionCount"),
				        Workgroup = row.GetString("LongName"),
				        BankId = row.GetInteger("BankId"),
				        WorkgroupId = request.WorkgroupId,
				        Serial = row.GetString("Serial"),
				        RT = row.GetString("RT"),
				        Account = row.GetString("Account"),
				        RemitterName = row.GetString("RemitterName"),
				        BatchCueID = row.GetInteger("BatchCueID"),
				        ImportTypeKey = row.GetInteger("ImportTypeKey"),
				        DDA = row.GetString("DDA"),
				        BatchSourceShortName = row.GetString("BatchSourceShortName"),
				        ImportTypeShortName = row.GetString("ImportTypeShortName"),
				        AccountSiteCode = row.GetInteger("AccountSiteCode"),
						FileGroup = row.GetString("FileGroup"),
                        IsElectronic = row.GetString("PaymentType").ToUpper() == "ACH" || row.GetString("PaymentType").ToUpper() == "WIRE"
                    };

                    dto.ShowPaymentIcon = ImageIconVisible(new ImageRequestDto
                    {
                        BankId = dto.BankId,
                        SourceBatchId = dto.SourceBatchId,
                        BatchPaymentSource = dto.BatchSourceShortName,
                        DepositDate = dto.DepositDate,
                        PicsDate = int.Parse(dto.PICSDate),
                        IsCheck = true,
                        BatchSequence = dto.BatchSequence,
                        WorkgroupId = dto.WorkgroupId,
                        ImportTypeShortName = dto.ImportTypeShortName
                    }, dto.IsElectronic);

                    dto.ShowDocumentIcon = ImageIconVisible(new ImageRequestDto
                    {
                        BankId = dto.BankId,
                        SourceBatchId = dto.SourceBatchId,
                        BatchPaymentSource = dto.BatchSourceShortName,
                        DepositDate = dto.DepositDate,
                        PicsDate = int.Parse(dto.PICSDate),
                        IsCheck = false,
                        BatchSequence = dto.DocumentBatchSequence,
                        WorkgroupId = dto.WorkgroupId,
                        ImportTypeShortName = dto.ImportTypeShortName
                    }, dto.IsElectronic);

			        dto.ShowAllIcon = dto.ShowPaymentIcon && dto.ShowDocumentIcon;

			        batches.Add(dto);
		        }
	        }

	        response.BatchDetailList = batches;
            response.TotalRecords = recordstotal;
            response.FilteredRecords = recordsfiltered;

            return response;
        }

        public bool ImageIconVisible(ImageRequestDto imageRequest, bool isElectronic)
        {
            // set to true, this is the state if it is an electronic batch
            var iconVisible = true;

            if (!isElectronic)
                iconVisible = _iconservice.BatchSequenceImagesExist(imageRequest).ImagesAvailable;

            return iconVisible;
        }

    }
}