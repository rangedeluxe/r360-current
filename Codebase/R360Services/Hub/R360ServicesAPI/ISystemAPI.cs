﻿using System;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public interface ISystemAPI : IDisposable
    {
        ISessionInfo Session { set; }

        XmlDocumentResponse GetBaseDocument();
        BaseResponse UpdateActivityLockbox(long ActivityLogID, int BankID, int LockboxID);
    }
}
