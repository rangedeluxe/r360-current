﻿using System;
using System.Data;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
 *   WI 156038 TWE 07/28/2014
 *       add back code dropped during conversion to WCF
 * * WI 156704 TWE 08/01/2014
 *      Adjust the calls for user preferences
********************************************************************************/

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class UserAPI : BaseOLAPI
    {
        private IUserDal _userDal;
        private LockboxAPI _LockboxSrv;
        private SystemAPI _SystemSrv;

        public UserAPI(IServiceContext serviceContext)
            : base(serviceContext)
        {
        }

        /// <summary>
        /// Retrieves reference to LockboxSrv service
        /// </summary>
        private LockboxAPI LockboxSrv
        {
            get
            {
                if (_LockboxSrv == null)
                {
                    _LockboxSrv = new LockboxAPI(_serviceContext);
                    _LockboxSrv.Session = Session;
                }

                return (_LockboxSrv);
            }
        }
        private IUserDal UserDal
        {
            get
            {
                if (_userDal == null)
                {
                    if (!string.IsNullOrEmpty(SiteKey))
                    {
                        _userDal = new UserDal(SiteKey);
                    }
                }
                return _userDal;
            }
        }
        protected override ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acR360WebServiceCall;
            }
        }
        /// <summary>
        /// Retrieves reference to SystemSrv service
        /// </summary>
        private SystemAPI SystemSrv
        {
            get
            {
                if (_SystemSrv == null)
                {
                    _SystemSrv = new SystemAPI(_serviceContext);
                    _SystemSrv.Session = Session;
                }

                return (_SystemSrv);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !Disposed)
            {
                if (_userDal != null)
                {
                    _userDal.Dispose();
                }
                if (_LockboxSrv != null)
                {
                    _LockboxSrv.Dispose();
                }
                if (_SystemSrv != null)
                {
                    _SystemSrv.Dispose();
                }
            }
            base.Dispose(disposing);
        }        
        protected override bool ValidateSID(out int userID)
        {
            // TODO: Implement this; it will be called from the standard wrapper methods...
            throw new NotImplementedException();
        }


        public BaseGenericResponse<cUserRAAM> GetUserByID(int UserID)
        {
            cUserRAAM objRetVal = null;
            DataTable dt = null;
            cUserRAAM objUser = null;
            BaseGenericResponse<cUserRAAM> response = null;

            try
            {
                // TODO: Check for required permissions.

                if (UserDal.GetUser(UserID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        objUser = new cUserRAAM();

                        if (!objUser.LoadDataRow(ref dr))
                        {
                            throw (new Exception("Unable to retrieve user."));
                        }
                    }
                }

                objRetVal = objUser;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetUserByID");
            }
            finally
            {
                if (dt != null)
                {
                    dt.Dispose();
                    dt = null;
                }
            }

            response = new BaseGenericResponse<cUserRAAM>()
            {
                Data = objRetVal,
                Status = (objRetVal != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }


        public XmlDocumentResponse GetOnlineUserByID(int UserID)
        {
            XmlDocument docRespXml = null;
            XmlDocument objXML = null;
            XmlNode node;
            cUserRAAM objUser;
            cOLLockboxes objSessionUserLockboxes;
            XmlDocumentResponse response = null;

            try
            {
                objXML = SystemSrv.GetBaseDocument().Data;
                objSessionUserLockboxes = LockboxSrv.GetUserLockboxes().Data;

                //only retrieve the user for a valid user.
                //UserID of -1 indicates new user being created and to only
                //return user properties, available lockboxes, and available
                //permissions to assign to the the new user
                if (UserID != -1)
                {
                    //retireve the user requested
                    objUser = GetUserByID(UserID).Data;

                    //convert the user object to xml and add to the document
                    node = ipoXmlLib.addElement((objXML.DocumentElement), "Recordset", string.Empty);
                    ipoXmlLib.addAttribute(node, "Name", "User");
                    objUser.ToXml(node, "Record");

                    //add the user's lockboxes
                    objXML.DocumentElement.AppendChild(objXML.ImportNode(GetOnlineUserLockboxesByID(UserID, objSessionUserLockboxes).Data.AsXMLNode("UserLockboxes"), true));
                }
                else
                {
                    objUser = new cUserRAAM();

                    //convert the user object to xml and add to the document
                    node = ipoXmlLib.addElement((objXML.DocumentElement), "Recordset", string.Empty);
                    ipoXmlLib.addAttribute(node, "Name", "User");
                    objUser.ToXml(node, "User");
                }

                //add the available lockboxes (session user lockboxes)
                objXML.DocumentElement.AppendChild(objXML.ImportNode(objSessionUserLockboxes.AsXMLNode("AvailableLockboxes"), true));

                //return the xml document as an attachment
                docRespXml = objXML;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOnlineUserByID");

                if (objXML == null)
                {
                    objXML = SystemSrv.GetBaseDocument().Data;
                }

                ipoXmlLib.AddMessageToDoc(objXML, xmlMessageType.xmlMsgError, "Unable to retrieve the requested user.");

                docRespXml = objXML;
            }

            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = (docRespXml != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<cOLLockboxes> GetOnlineUserLockboxesByID(int UserID, cOLLockboxes SessionUserLockboxes)
        {
            cUserRAAM objUser;
            cOLLockboxes objRetVal = null;
            cOLLockboxes objUserLockboxes;
            BaseGenericResponse<cOLLockboxes> response = null;

            try
            {
                objUser = GetUserByID(UserID).Data;

                if (objUser != null)
                {
                    //Get the requested user's lockboxes
                    objUserLockboxes = LockboxSrv.GetUserLockboxes().Data;

                    objRetVal = new cOLLockboxes();

                    foreach (cOLLockbox Lockbox in objUserLockboxes.Values)
                    {
                        if (SessionUserLockboxes.ContainsKey(Lockbox.OLWorkGroupsID))
                        {
                            //Session has access to view this lockbox
                            objRetVal.Add(Lockbox.OLWorkGroupsID, Lockbox);
                        }
                    }
                }
                else
                {
                    objRetVal = null;
                }
            }
            catch (Exception ex)
            {
                EventLog.logEvent(ex.Message, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            }

            response = new BaseGenericResponse<cOLLockboxes>()
            {
                Data = objRetVal,
                Status = (objRetVal != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<UserPreferencesDTO> GetUserPreference()
        {
            bool retVal = false;
            DataTable dt = null;
            UserPreferencesDTO prefs = new UserPreferencesDTO();
            try
            {
                if (UserDal.GetOLUserPreferences(Session.SessionUser.UserID, out dt))
                {
                    retVal = true;
                    foreach (DataRow row in dt.Rows)
                    {
                        prefs.LoadDataRow(row);
                    }
                }
                else
                {
                    throw new Exception(UserDal.GetLastError.Description);
                }

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLUserPreferences");
            }
            finally
            {
                if (dt != null)
                {
                    dt.Dispose();
                    dt = null;
                }
            }

            var response = new BaseGenericResponse<UserPreferencesDTO>()
            {
                Data = prefs,
                Status = retVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }
        public XmlDocumentResponse GetOLUserPreferences()
        {
            bool bolRetVal = false;
            DataTable dt = null;
            XmlDocument docRespXml = null;
            XmlDocumentResponse response = null;

            try
            {
                docRespXml = SystemSrv.GetBaseDocument().Data;

                //get the OLUserPreferences
                if (UserDal.GetOLUserPreferences(Session.SessionUser.UserID, out dt))
                {
                    if (dt.Rows.Count == 0)
                    {
                        ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "There are no user preferences available at this time.");
                        bolRetVal = true;
                    }
                    else
                    {
                        ipoXmlLib.AddRecordsetToDoc(docRespXml, dt, "OLUserPreferences");
                    }
                }
                else
                {
                    throw new Exception(UserDal.GetLastError.Description);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLUserPreferences");

                if (docRespXml == null)
                {
                    docRespXml = SystemSrv.GetBaseDocument().Data;
                }

                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while retrieving your user preferences.");
            }
            finally
            {
                if (dt != null)
                {
                    dt.Dispose();
                    dt = null;
                }
            }

            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public XmlDocumentResponse RestoreDefaultOLPreferences()
        {
            bool bolRetVal = false;
            XmlDocument docRespXml = null;
            int intRowsAffected;
            XmlDocumentResponse response = null;

            try
            {
                //set return to existing user preferences
                docRespXml = GetOLUserPreferences().Data;

                UserDal.DeleteOLUserPreference(Session.SessionUser.UserID, out intRowsAffected);

                if (intRowsAffected >= 0)
                {
                    docRespXml = GetOLUserPreferences().Data;
                    ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "All user preferences have been restored to the default settings.");

                    bolRetVal = true;
                }
                else
                {
                    throw new Exception(UserDal.GetLastError.Description);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "RestoreDefaultOLPreferences");

                if (docRespXml == null)
                {
                    docRespXml = GetOLUserPreferences().Data;
                }

                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while restoring default preference settings.");
            }

            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }


        public BaseGenericResponse<UserPreferencesDTO> RestoreDefaultPreferences()
        {
            bool bolRetVal = false;
            UserPreferencesDTO prefs = new UserPreferencesDTO();
            DataTable dt = null;

            int intRowsAffected;
            try
            {
                UserDal.DeleteOLUserPreference(Session.SessionUser.UserID, out intRowsAffected);

                if (intRowsAffected >= 0)
                {
                    bolRetVal = true;

                    if (UserDal.GetOLUserPreferences(Session.SessionUser.UserID, out dt))
                    {
                        bolRetVal = true;
                        foreach (DataRow row in dt.Rows)
                        {
                            prefs.LoadDataRow(row);
                        }
                    }
                    else
                    {
                        throw new Exception(UserDal.GetLastError.Description);
                    }

                }
                else
                {
                    throw new Exception(UserDal.GetLastError.Description);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "RestoreDefaultOLPreferences");
                bolRetVal = false;
            }

            var response = new BaseGenericResponse<UserPreferencesDTO>()
            {
                Data = prefs,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<bool> SetUserPreferences(UserPreferencesDTO pref)
        {
            bool retValue = false;            

            try
            {
                string recordsPreferenceName = "RecordsPerPage";
                string remittePreferencesName = "DisplayRemitterNameInPDF";
                Guid recordsPreferenceID = GetOLPreferenceID(recordsPreferenceName);
                Guid DisplayRemitterNameInPDFID = GetOLPreferenceID(remittePreferencesName);
                int intRowsAffected;
                int records;
                bool display;
                if (recordsPreferenceID != Guid.Empty && DisplayRemitterNameInPDFID != Guid.Empty)
                {
                    UserDal.BeginTrans();
                    int.TryParse(pref.recordsPerPage.ToString(), out records);
                    if (IsOLUserPreference(recordsPreferenceID, Session.SessionUser.UserID))
                    {
                        UserDal.UpdateOLUserPreference(recordsPreferenceID, Session.SessionUser.UserID, records.ToString(), out intRowsAffected);
                    }
                    else
                    {
                        UserDal.InsertOLUserPreference(recordsPreferenceID, Session.SessionUser.UserID, records.ToString(), out intRowsAffected);
                    }
                    display = pref.displayRemitterNameinPdf.HasValue ? pref.displayRemitterNameinPdf.Value : false;
                    if ( IsOLUserPreference(DisplayRemitterNameInPDFID, Session.SessionUser.UserID))
                    {
                        UserDal.UpdateOLUserPreference(DisplayRemitterNameInPDFID, Session.SessionUser.UserID, display  ? "Y" : "N", out intRowsAffected);
                    }
                    else
                    {
                        UserDal.InsertOLUserPreference(DisplayRemitterNameInPDFID, Session.SessionUser.UserID, display ? "Y" : "N" , out intRowsAffected);
                    }
                    //commit the transaction
                    UserDal.CommitTrans();
                    retValue = true;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "SetOLUserPreferences");                
                retValue = false;
            }

            var response = new BaseGenericResponse<bool>
            {
                Data = retValue,
                Status = retValue ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public XmlDocumentResponse SetOLUserPreferences(XmlDocument docReqXml)
        {
            XmlDocument docRespXml = null;
            Guid OLPreferenceID;
            int intRowsAffected;
            XmlDocumentResponse response = null;

            try
            {
                //check to see if childnodes exist
                if (docReqXml.DocumentElement.HasChildNodes)
                {
                    UserDal.BeginTrans();

                    foreach (XmlNode n in docReqXml.DocumentElement.ChildNodes)
                    {
                        //check if valid OLPreferences record
                        OLPreferenceID = GetOLPreferenceID(n.Name);

                        if (OLPreferenceID != Guid.Empty)
                        {
                            //check if assigned to user
                            if (IsOLUserPreference(OLPreferenceID, Session.SessionUser.UserID))
                            {
                                //update user preference
                                UserDal.UpdateOLUserPreference(OLPreferenceID, Session.SessionUser.UserID, n.InnerText, out intRowsAffected);
                            }
                            else
                            {
                                //insert user preference
                                UserDal.InsertOLUserPreference(OLPreferenceID, Session.SessionUser.UserID, n.InnerText, out intRowsAffected);
                            }

                            if (intRowsAffected < 0)
                            {
                                //rollback the transaction and quit
                                UserDal.RollbackTrans();

                                throw (new Exception("An error occurred while executing the SQL command."));
                            }
                        }

                        //reset olpreferenceid
                        OLPreferenceID = Guid.Empty;
                    }

                    //commit the transaction
                    UserDal.CommitTrans();

                    //get the new oluserpreferences
                    docRespXml = GetOLUserPreferences().Data;

                    //add success message
                    ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgInformation, "Your user preferences have been updated.");
                }
                else
                {
                    //get the new oluserpreferences
                    docRespXml = GetOLUserPreferences().Data;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "SetOLUserPreferences");

                if (docRespXml == null)
                {
                    //get the existing oluserpreferences
                    docRespXml = GetOLUserPreferences().Data;
                }

                ipoXmlLib.AddMessageToDoc(docRespXml, xmlMessageType.xmlMsgError, "An error occurred while setting your user preferences.");
            }

            response = new XmlDocumentResponse()
            {
                Data = docRespXml,
                Status = (docRespXml != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        private Guid GetOLPreferenceID(string PreferenceName)
        {
            DataTable dt = null;
            Guid gidRetVal;

            try
            {
                if (UserDal.GetOLUserPreferenceIDByName(PreferenceName, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        gidRetVal = (Guid)dt.Rows[0]["OLPreferenceID"];
                    }
                    else
                    {
                        gidRetVal = Guid.Empty;
                    }
                }
                else
                {
                    throw new Exception(UserDal.GetLastError.Description);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLPreferenceID");
                gidRetVal = Guid.Empty;
            }
            finally
            {
                if (dt != null)
                {
                    dt.Dispose();
                    dt = null;
                }
            }

            return (gidRetVal);
        }

        private bool IsOLUserPreference(Guid OLPreferenceID, int UserID)
        {
            DataTable dt = null;
            bool bolRetVal = false;

            try
            {
                if (UserDal.GetOLUserPreferenceCount(OLPreferenceID, UserID, out dt))
                {
                    if ((int)dt.Rows[0]["OLUserPreference"] > 0)
                    {
                        bolRetVal = true;
                    }
                }
                else
                {
                    throw new Exception(UserDal.GetLastError.Description);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "IsOLUserPrefernce");
                bolRetVal = false;
            }
            finally
            {
                if (dt != null)
                {
                    dt.Dispose();
                    dt = null;
                }
            }

            return (bolRetVal);
        }
    }
}
