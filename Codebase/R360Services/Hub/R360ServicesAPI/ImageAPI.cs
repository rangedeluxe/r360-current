﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.Common;
using WFS.LTA.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.OLDecisioningServicesClient;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.OLFServices;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.OLFServices.DataTransferObjects;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
 * * WI 154797 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 157331 TWE 08/06/2014
*    Add permissions node to base document - fix stored procedure call
* WI 157331 SAS 09/29/2014
*   Change done for retrieving image relative path.
* * WI 175214 SAS 10/30/2014
*   Update ImportTypeShortName field to BatchSourceName
* * WI 176360 SAS 11/07/2014
*    Changes done to add ImportTypeShortName and BatchSourceShortName
********************************************************************************/

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class ImageAPI : BaseOLAPI
    {
        private CBXAPI _CBXSrv;
        private cItemProcDAL _ItemProcDAL = null;
        OLFOnlineClient _OLFClient;
        private DecisioningSvcClient _decisioningServicesClient;

        private const string NORMALFONT = "Times-Roman";
        private const string BOLDFONT = "Times-Bold";
        private const short FONTSIZE = 12;
        
        private static string _siteKey;

        private SortedList _LockboxColorModes = null;

        public ImageAPI(IServiceContext serviceContext)
            : base(serviceContext)
        {
        }

        /// <summary>
        /// Retrieves reference to CBXSrv service
        /// </summary>
        private CBXAPI CBXSrv
        {
            get
            {
                if (_CBXSrv == null)
                {
                    _CBXSrv = new CBXAPI(_serviceContext);
                    _CBXSrv.Session = Session;
                }

                return (_CBXSrv);
            }
        }

        private cItemProcDAL ItemProcDAL
        {
            get
            {
                if (_ItemProcDAL == null)
                {
                    _ItemProcDAL = new cItemProcDAL(this.SiteKey);
                }

                return (_ItemProcDAL);
            }
        }

        protected override RecHub.Common.ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acR360WebServiceCall;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !Disposed)
            {
                if (_CBXSrv != null)
                {
                    _CBXSrv.Dispose();
                }
                if (_ItemProcDAL != null)
                {
                    _ItemProcDAL.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        protected override bool ValidateSID(out int userID)
        {
            // TODO: Implement this; it will be called from the standard wrapper methods...
            throw new NotImplementedException();
        }

        private OLFOnlineClient OLFClient
        {
            get
            {
                if (_OLFClient == null)
                {
                    _OLFClient = new OLFOnlineClient(this.SiteKey);
                }
                return (_OLFClient);
            }
        }

        private DecisioningSvcClient OlDecisioningSvcClient
        {
            get
            {
                if (_decisioningServicesClient == null)
                {
                    _decisioningServicesClient = new DecisioningSvcClient(this.SiteKey, LtaEventLog);
                }
                return _decisioningServicesClient;
            }
        }

        private FileMetaData BuildMetaData()
        {
            FileMetaData fmd = new FileMetaData();
            fmd.SiteKey = this.SiteKey;
            fmd.SessionID = this.SessionID.ToString();
            fmd.EmulationID = this.EmulationID.ToString();

            return (fmd);
        }

        public OlfResponseDto ImagesExist(ImageRequestDto request)
        {
            request.SiteKey = SiteKey;
            return OLFClient.ImagesExist(request);
        }

        public OlfResponseDto BatchSequenceImagesExist(ImageRequestDto request)
        {
            request.SiteKey = SiteKey;
            return OLFClient.BatchSequenceImagesExist(request);
        }

        public BaseGenericResponse<long> GetImageSize(int PICSDate, int BankID, int LockboxID, long BatchID, int Item, int DepositDateKey, string ImageType, short Page, long SourceBatchID, string BatchSourceShortName, string ImportTypeShortName)
        {
            long lngRetVal = 0;
            DataTable dt;
            byte bytOnlineColorMode;
            BaseGenericResponse<long> response = null;

            try
            {
                bytOnlineColorMode = GetOnlineColorMode(BankID, LockboxID);

                if (!(OLSiteOptions.CheckImageExists))
                { //CR 45994 JNE 9/15/2011
                    lngRetVal = 1;
                }
                else if ((OLSiteOptions.imageStorageMode == ImageStorageMode.PICS) || (OLSiteOptions.imageStorageMode == ImageStorageMode.FILEGROUP))
                {
                    lngRetVal = OLFClient.GetImageSize(BuildMetaData(), bytOnlineColorMode, PICSDate.ToString(), BankID, LockboxID, BatchID, Item, ImageType, Page, SourceBatchID, BatchSourceShortName, ImportTypeShortName);
                }
                else
                {
                    if (ItemProcDAL.GetImageInfo(_ServiceContext.GetSessionID(),
												BankID,
                                                LockboxID,
                                                PICSDate,
                                                BatchID,
                                                Item,
                                                DepositDateKey,
                                                (ImageType.ToUpper() == "C" ? TableTypes.Checks : TableTypes.Documents),
                                                out dt))
                    {
                        if (dt.Rows.Count > 0 && (!dt.Rows[0].IsNull("FileSize")))
                        {
                            lngRetVal = (int)dt.Rows[0]["FileSize"];
                        }
                    }
                    else
                    {
                        lngRetVal = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                LtaEventLog.logEvent(ex.Message, this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                lngRetVal = 0;
            }

            response = new BaseGenericResponse<long>()
            {
                Data = lngRetVal
            };

            return (response);
        }

        public BaseGenericResponse<long> GetCENDSFileSize(string filePath)
        {
            long lngRetVal = 0;
            BaseGenericResponse<long> response = null;

            try
            {
                lngRetVal = OLFClient.GetCENDSFileSize(BuildMetaData(), filePath);
            }
            catch (Exception ex)
            {
                LtaEventLog.logEvent(ex.Message, this.GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                lngRetVal = 0;
            }

            response = new BaseGenericResponse<long>()
            {
                Data = lngRetVal
            };

            return (response);
        }

        public BaseGenericResponse<Stream> GetImageJobFileAsAttachment(Guid OnlineImageQueueID)
        {
            Stream objRetVal = null;
            DataTable dt;
            DataRow dr;
            BaseGenericResponse<Stream> response = null;

            try
            {
                if (ItemProcDAL.GetOnlineImageQueue(OnlineImageQueueID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        dr = dt.Rows[0];

                        //Verify requested file belongs to the session user
                        if (Session.SessionUser.UserID.ToString().Trim().ToLower() == dr["UserID"].ToString().Trim().ToLower())
                        {
                            if (OLFClient.GetImageJobSize(BuildMetaData(), OnlineImageQueueID) > 0)
                            {
                                objRetVal = OLFClient.GetImageJobFileAsAttachment(OnlineImageQueueID);

                                if (objRetVal == null)
                                {
                                    throw (new Exception("Unable to open file"));
                                }
                            }
                            else
                            {
                                throw (new Exception("File Not Found."));
                            }
                        }
                        else
                        {
                            throw (new Exception("Access Denied."));
                        }
                    }
                    else
                    {
                        throw (new Exception("The view all request does not exist."));
                    }

                    dt.Dispose();
                }
                else
                {
                    throw (new Exception("Request Not Valid."));
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetImageJobFileAsAttachment");

                throw ;
            }

            response = new BaseGenericResponse<Stream>()
            {
                Data = objRetVal,
                Status = (objRetVal != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<DataSet> CreateViewAllJobRequest(XmlDocument docParms)
        {
            DataSet dsRetVal = null;
            string strUserFileName;
            Guid gidOnlineImageQueueID;
            short intPage;
            cOLLockbox objOLLockbox = null;
            XmlDocument objXMLOut;
            XmlNode nodejobRequest;
            XmlNode nodeJobRequestOptions;
            XmlNode nodeTransactions;
            XmlNode nodeTransaction;
            XmlNode nodeBatches;
            XmlNode nodeBatch;
            int intBankID;
            int intLockboxID;
            DateTime dteDepositDate;
            int intBatchID;
            int intTransactionID;
            int intTxnSequence;
            int intBatchNumber;
            bool bDisplayBatchID = false;
            int intTemp;
            DateTime dteTemp;
            int intRowsAffected;
            BaseGenericResponse<DataSet> response = null;

            try
            {
                if (docParms.DocumentElement.SelectSingleNode("Page") == null)
                {
                    intPage = -1;
                }
                else
                {
                    try
                    {
                        intPage = short.Parse(docParms.DocumentElement.SelectSingleNode("Page").InnerText);
                    }
                    catch (Exception)
                    {
                        intPage = -1;
                    }
                }

                if (docParms.DocumentElement.SelectSingleNode("BankID") != null && int.TryParse(docParms.DocumentElement.SelectSingleNode("BankID").InnerText, out intTemp))
                {
                    intBankID = intTemp;
                }
                else
                {
                    throw (new Exception("Invalid BankID"));
                }

                if (docParms.DocumentElement.SelectSingleNode("LockboxID") != null && int.TryParse(docParms.DocumentElement.SelectSingleNode("LockboxID").InnerText, out intTemp))
                {
                    intLockboxID = intTemp;
                }
                else
                {
                    throw (new Exception("Invalid LockboxID"));
                }

                if (docParms.DocumentElement.SelectSingleNode("BatchID") != null && int.TryParse(docParms.DocumentElement.SelectSingleNode("BatchID").InnerText, out intTemp))
                {
                    intBatchID = intTemp;
                }
                else
                {
                    throw (new Exception("Invalid BatchID"));
                }

                if (docParms.DocumentElement.SelectSingleNode("DepositDate") != null && DateTime.TryParse(docParms.DocumentElement.SelectSingleNode("DepositDate").InnerText, out dteTemp))
                {
                    dteDepositDate = dteTemp;
                }
                else
                {
                    throw (new Exception("Invalid DepositDate"));
                }

                if (docParms.DocumentElement.SelectSingleNode("TransactionID") != null && int.TryParse(docParms.DocumentElement.SelectSingleNode("TransactionID").InnerText, out intTemp))
                {
                    intTransactionID = intTemp;
                }
                else
                {
                    intTransactionID = -1;
                }

                if (docParms.DocumentElement.SelectSingleNode("TxnSequence") != null && int.TryParse(docParms.DocumentElement.SelectSingleNode("TxnSequence").InnerText, out intTemp))
                {
                    intTxnSequence = intTemp;
                }
                else
                {
                    intTxnSequence = intTransactionID;
                }

                if (docParms.DocumentElement.SelectSingleNode("BatchNumber") != null && int.TryParse(docParms.DocumentElement.SelectSingleNode("BatchNumber").InnerText, out intTemp))
                {
                    intBatchNumber = intTemp;
                }
                else
                {
                    intBatchNumber = intBatchID;
                }

                if (CBXSrv.AuthorizeBatchToUser(intBankID, intLockboxID, intBatchID, dteDepositDate, out objOLLockbox))
                {
                    if (objOLLockbox != null)
                    {
                        bDisplayBatchID = objOLLockbox.DisplayBatchID;
                    }

                    gidOnlineImageQueueID = Guid.NewGuid();
                    objXMLOut = ipoXmlLib.GetXMLDoc("ipoServiceRequest");
                    nodejobRequest = ipoXmlLib.addElement(objXMLOut.DocumentElement, "jobRequest", string.Empty);

                    ipoXmlLib.addAttribute(nodejobRequest, "jobID", gidOnlineImageQueueID.ToString());
                    ipoXmlLib.addAttribute(nodejobRequest, "siteKey", base.SiteKey);

                    if (intTransactionID < 0)
                    {
                        //Batch Request
                        strUserFileName = "Batch " + intBatchID.ToString();

                        ipoXmlLib.addAttribute(nodejobRequest, "jobRequestType", "BATCH");

                        nodeJobRequestOptions = ipoXmlLib.addElement(nodejobRequest, "jobRequestOptions", string.Empty);

                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayScannedCheck", Session.ServerOptions.Preferences.DisplayScannedCheckOnline.ToString());
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "returnType", "PDF");
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "includeImages", "True");
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayRemitterNameInPDF", (Session.ServerOptions.Preferences.DisplayRemitterNameInPDF ? "1" : "0"));
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "checkImageDisplayMode", ((int)GetImageDisplayModeForBatch(intBankID, intLockboxID, "check")).ToString());
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "documentImageDisplayMode", ((int)GetImageDisplayModeForBatch(intBankID, intLockboxID, "document")).ToString());

                        nodeBatches = ipoXmlLib.addElement(nodejobRequest, "Batches", string.Empty);
                        nodeBatch = ipoXmlLib.addElement(nodeBatches, "Batch", string.Empty);

                        ipoXmlLib.addAttribute(nodeBatch, "BankID", intBankID.ToString());
                        ipoXmlLib.addAttribute(nodeBatch, "LockboxID", intLockboxID.ToString());
                        ipoXmlLib.addAttribute(nodeBatch, "BatchID", intBatchID.ToString());
                        ipoXmlLib.addAttribute(nodeBatch, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
                        ipoXmlLib.addAttribute(nodeBatch, "BatchNumber", intBatchNumber.ToString());
                        ipoXmlLib.addAttribute(nodeBatch, "DisplayBatchID", bDisplayBatchID.ToString());
                    }
                    else
                    {

                        //Transaction Request
                        strUserFileName = "Batch " + intBatchID.ToString() + " Transaction " + intTxnSequence.ToString();

                        ipoXmlLib.addAttribute(nodejobRequest, "jobRequestType", "TRANSACTION");

                        nodeJobRequestOptions = ipoXmlLib.addElement(nodejobRequest, "jobRequestOptions", string.Empty);

                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayScannedCheck", Session.ServerOptions.Preferences.DisplayScannedCheckOnline.ToString());
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "returnType", "PDF");
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "includeImages", "True");
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayRemitterNameInPDF", (Session.ServerOptions.Preferences.DisplayRemitterNameInPDF ? "1" : "0"));
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "checkImageDisplayMode", ((int)GetImageDisplayModeForBatch(intBankID, intLockboxID, "check")).ToString());
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "documentImageDisplayMode", ((int)GetImageDisplayModeForBatch(intBankID, intLockboxID, "document")).ToString());

                        nodeTransactions = ipoXmlLib.addElement(nodejobRequest, "transactions", string.Empty);
                        nodeTransaction = ipoXmlLib.addElement(nodeTransactions, "transaction", string.Empty);

                        ipoXmlLib.addAttribute(nodeTransaction, "BankID", intBankID.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "LockboxID", intLockboxID.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "BatchID", intBatchID.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
                        ipoXmlLib.addAttribute(nodeTransaction, "TransactionID", intTransactionID.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "BatchNumber", intBatchNumber.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "DisplayBatchID", bDisplayBatchID.ToString());
                    }

                    ipoXmlLib.addAttribute(nodejobRequest, "sessionID", Session.SessionID.ToString());

                    if (docParms.DocumentElement.SelectSingleNode("ImageFilterOption") != null)
                    {
                        switch (docParms.DocumentElement.SelectSingleNode("ImageFilterOption").InnerText.Trim().ToLower())
                        {
                            case "checksonly":
                                ipoXmlLib.addAttribute(nodeJobRequestOptions, "imageFilterOption", "ChecksOnly");
                                break;
                            case "documentsonly":
                                ipoXmlLib.addAttribute(nodeJobRequestOptions, "imageFilterOption", "DocumentsOnly");
                                break;
                            default:
                                ipoXmlLib.addAttribute(nodeJobRequestOptions, "imageFilterOption", "All");
                                break;
                        }
                    }
                    else
                    {
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "imageFilterOption", "All");
                    }

                    // Use the new "Display" format including DDA for returned image
                    ipoXmlLib.addAttribute(nodeJobRequestOptions, "olClientAccountDisplayOverride", objOLLockbox.LongName);

                    //Create the request in the DB
                    ItemProcDAL.CreateOnlineImageQueue(gidOnlineImageQueueID,
                                                       Session.SessionUser.UserID,
                                                       strUserFileName,
                                                       Session.ServerOptions.Preferences.DisplayScannedCheckOnline,
                                                       out intRowsAffected);

                    if (intRowsAffected <= 0)
                    {
                        throw (new Exception("Unable to create the view all request in OnlineImageQueue."));
                    }


                    //Get Request
                    dsRetVal = CheckImageJobStatus(gidOnlineImageQueueID).Data;

                    if (dsRetVal != null)
                    {
                        if (dsRetVal.Tables[0].Rows.Count > 0)
                        {
                            //Submit request to CBXServer
                            if (!SubmitImageJob(gidOnlineImageQueueID, objXMLOut))
                            {
                                dsRetVal = null;

                                throw (new Exception("Unable to submit the request to the IPO Image Service for processing."));
                            }
                        }
                    }
                    else
                    {
                        throw (new Exception("Unable to retrieve the new view all request."));
                    }
                }
                else
                {
                    throw (new Exception("User does not have access to the batch requested."));
                }

            }
            catch (Exception ex)
            {

                EventLog.logError(ex, this.GetType().Name, "CreateViewAllImagesRequest");

                throw ;
            }

            response = new BaseGenericResponse<DataSet>()
            {
                Data = dsRetVal,
                Status = (dsRetVal != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<DataSet> CheckImageJobStatus(Guid OnlineImageQueueID)
        {
            bool bolRetVal = false;
            DataTable dtRetVal = null;
            DataTable dt;
            BaseGenericResponse<DataSet> response = null;

            try
            {
                if (ItemProcDAL.GetOnlineImageQueue(OnlineImageQueueID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        //Verify User has access to this request
                        if (Session.SessionUser.UserID.ToString().Trim().ToLower() == dt.Rows[0]["UserID"].ToString().Trim().ToLower())
                        {
                            dtRetVal = dt;
                            bolRetVal = true;
                        }
                        else
                        {
                            throw (new Exception("The user does not have access to the view all request."));
                        }
                    }
                    else
                    {
                        throw (new Exception("The view all request does not exist."));
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "CheckImageJobStatus");
                throw;
            }

            response = new BaseGenericResponse<DataSet>()
            {
                Data = dtRetVal.DataSet,
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<DataSet> CreateSearchResultsJobRequest(XmlDocument SearchParms, XmlDocument JobParms)
        {
            DataSet dsRetVal = null;
            Guid gidOnlineImageQueueID;
            int intRowsAffected;
            BaseGenericResponse<DataSet> response = null;

            try
            {
                gidOnlineImageQueueID = Guid.NewGuid();

                //Create the request in the DB
                ItemProcDAL.CreateOnlineImageQueue(gidOnlineImageQueueID, Session.SessionUser.UserID, Session.ServerOptions.Preferences.DisplayScannedCheckOnline, out intRowsAffected);

                if (intRowsAffected <= 0)
                {
                    throw (new Exception("Unable to create the job request in OnlineImageQueue."));
                }

                //Get Request
                dsRetVal = CheckImageJobStatus(gidOnlineImageQueueID).Data;

                if (dsRetVal != null)
                {
                    if (dsRetVal.Tables[0].Rows.Count > 0)
                    {
                        //Submit request to CBXServer
                        if (!SubmitImageJob(gidOnlineImageQueueID, AppendJobInfo(gidOnlineImageQueueID.ToString(), SearchParms, JobParms)))
                        {
                            dsRetVal = null;
                            throw (new Exception("Unable to submit the request to the IPO Image Service for processing."));
                        }
                    }
                }
                else
                {
                    throw (new Exception("Unable to retrieve the new job request."));
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "CreateSearchResultsImagesRequest");
                throw ;
            }

            response = new BaseGenericResponse<DataSet>()
            {
                Data = dsRetVal,
                Status = (dsRetVal != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        /// <summary>
        /// Fills in the request info to be passed to OLF services
        /// </summary>
        /// <param name="jobId">Job ID</param>
        /// <param name="SearchParms">xml for search parameters</param>
        /// <param name="JobParms">Current Job parameters</param>
        /// <returns></returns>
        private XmlDocument AppendJobInfo(string jobId, XmlDocument SearchParms, XmlDocument JobParms)
        {
            XmlDocument objNewXml;
            XmlDocument objJobParmXml;
            XmlNode nodejobRequest;
            XmlNode nodeOldJobRequestOptions;
            XmlNode nodeSearchParms;
            objJobParmXml = JobParms;

            // Create New XmlDocument
            objNewXml = ipoXmlLib.GetXMLDoc("ipoServiceRequest");

            // Create Job Request Node
            nodejobRequest = ipoXmlLib.addElement((objNewXml.DocumentElement), "jobRequest", string.Empty);

            ipoXmlLib.addAttribute(nodejobRequest, "jobID", jobId);
            ipoXmlLib.addAttribute(nodejobRequest, "siteKey", base.SiteKey);
            ipoXmlLib.addAttribute(nodejobRequest, "jobRequestType", "SEARCH_RESULTS");
            ipoXmlLib.addAttribute(nodejobRequest, "sessionID", Session.SessionID.ToString());

            // Append Search Parameters
            foreach (XmlNode node in SearchParms.FirstChild.ChildNodes)
            {
                nodeSearchParms = nodejobRequest.OwnerDocument.ImportNode(node, true);
                nodejobRequest.AppendChild(nodeSearchParms);
            }

            // Request Options
            nodeOldJobRequestOptions = objJobParmXml.DocumentElement.SelectSingleNode("jobRequestOptions");

            if (nodeOldJobRequestOptions != null)
            {
                string clientAccID = SearchParms.DocumentElement.SelectSingleNode("ClientAccountID").InnerText;
                string workgroupName = SearchParms.DocumentElement.SelectSingleNode("WorkgroupSelectionLabel").InnerText;
                ipoXmlLib.addAttribute(nodeOldJobRequestOptions, "displayScannedCheck", Session.ServerOptions.Preferences.DisplayScannedCheckOnline.ToString());
                //element olf image services looks for to display in the report.
                ipoXmlLib.addAttribute(nodeOldJobRequestOptions, "olClientAccountDisplayOverride", clientAccID + " - " + workgroupName);
                nodejobRequest.AppendChild(nodejobRequest.OwnerDocument.ImportNode(objJobParmXml.DocumentElement.SelectSingleNode("jobRequestOptions"), true));
            }
            else
            {
                throw (new Exception("Job Request Options must be included with the Xml Job request."));
            }

            return objNewXml;
        }

        private void SetColumnAttributes(XmlNode parentNode, string FieldName, string dataTypeEnum, string displayName)
        {
            ipoXmlLib.addAttribute(parentNode, "fieldName", FieldName);
            ipoXmlLib.addAttribute(parentNode, "dataTypeEnum", dataTypeEnum);
            ipoXmlLib.addAttribute(parentNode, "displayName", displayName);
        }

        public BaseGenericResponse<DataSet> CreateViewSpecifiedJobRequest(XmlDocument Parms)
        {
            DataSet dsRetVal = null;
            Guid GUID;
            XmlDocument objXML;
            XmlNode objNode;
            cOLLockbox objOLLockbox = null;
            short intScannedCheck = 0;
            short intPage;
            XmlDocument objXMLOut;
            XmlNode nodejobRequest;
            XmlNode nodeJobRequestOptions;
            XmlNode nodeImage;
            XmlNode nodeImages;
            XmlNode nodeRequestImages;
            string strFileDisplayName = string.Empty;
            int intBankID;
            int intLockboxID;
            int intBatchID;
            int intBatchNumber;
            DateTime dteDepositDate;
            string strPICSDate;
            int intBatchSequence;
            int intTransactionID;
            bool bDisplayBatchID = false;
            DateTime dteTemp;
            int intTemp;
            int intRowsAffected;
            BaseGenericResponse<DataSet> response = null;

            try
            {
                objXML = Parms;
                objNode = objXML.DocumentElement;

                try
                {
                    if (objNode.SelectSingleNode("Page") == null)
                    {
                        intPage = -1;
                    }
                    else
                    {
                        intPage = short.Parse(objNode.SelectSingleNode("Page").InnerText);
                    }
                }
                catch (Exception)
                {
                    // nodeTypedValue was probably NULL.
                    intPage = -1;
                }

                if (Session.ServerOptions.Preferences.DisplayScannedCheckOnline)
                {
                    intScannedCheck = 1;
                }

                GUID = Guid.NewGuid();
                objXMLOut = ipoXmlLib.GetXMLDoc("ipoServiceRequest");
                nodejobRequest = ipoXmlLib.addElement(objXMLOut.DocumentElement, "jobRequest", string.Empty);

                ipoXmlLib.addAttribute(nodejobRequest, "jobID", GUID.ToString());
                ipoXmlLib.addAttribute(nodejobRequest, "siteKey", base.SiteKey);
                ipoXmlLib.addAttribute(nodejobRequest, "jobRequestType", "VIEW_SPECIFIED");
                ipoXmlLib.addAttribute(nodejobRequest, "sessionID", Session.SessionID.ToString());

                nodeJobRequestOptions = ipoXmlLib.addElement(nodejobRequest, "jobRequestOptions", string.Empty);

                ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayScannedCheck", Session.ServerOptions.Preferences.DisplayScannedCheckOnline.ToString());
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "page", intPage.ToString());
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "returnType", "PDF");
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "includeImages", "True");
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayRemitterNameInPDF", Session.ServerOptions.Preferences.DisplayRemitterNameInPDF.ToString());

                nodeImages = ipoXmlLib.addElement(nodejobRequest, "images", string.Empty);
                nodeRequestImages = objXML.DocumentElement.SelectSingleNode("Images");

                if (nodeRequestImages != null)
                {
                    foreach (XmlNode nodeTemp in nodeRequestImages.ChildNodes)
                    {
                        if (nodeTemp.Attributes["BankID"] != null && int.TryParse(nodeTemp.Attributes["BankID"].InnerText, out intTemp))
                        {
                            intBankID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BankID"));
                        }

                        if (nodeTemp.Attributes["LockboxID"] != null && int.TryParse(nodeTemp.Attributes["LockboxID"].InnerText, out intTemp))
                        {
                            intLockboxID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid LockboxID"));
                        }

                        if (nodeTemp.Attributes["BatchID"] != null && int.TryParse(nodeTemp.Attributes["BatchID"].InnerText, out intTemp))
                        {
                            intBatchID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchID"));
                        }

                        if (nodeTemp.Attributes["BatchNumber"] != null && int.TryParse(nodeTemp.Attributes["BatchNumber"].InnerText, out intTemp))
                        {
                            intBatchNumber = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchNumber"));
                        }

                        if (nodeTemp.Attributes["DepositDate"] != null && DateTime.TryParse(nodeTemp.Attributes["DepositDate"].InnerText, out dteTemp))
                        {
                            dteDepositDate = dteTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid DepositDate"));
                        }

                        if (nodeTemp.Attributes["PICSDate"] != null)
                        {
                            strPICSDate = nodeTemp.Attributes["PICSDate"].InnerText;
                        }
                        else
                        {
                            throw (new Exception("Invalid PICS Date"));
                        }

                        if (nodeTemp.Attributes["TransactionID"] != null && int.TryParse(nodeTemp.Attributes["TransactionID"].InnerText, out intTemp))
                        {
                            intTransactionID = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid TransactionID"));
                        }

                        if (nodeTemp.Attributes["BatchSequence"] != null && int.TryParse(nodeTemp.Attributes["BatchSequence"].InnerText, out intTemp))
                        {
                            intBatchSequence = intTemp;
                        }
                        else
                        {
                            throw (new Exception("Invalid BatchSequence"));
                        }

                        switch (nodeTemp.Name.ToLower())
                        {
                            case "check":
                                if (!CBXSrv.AuthorizeBatchToUser(intBankID, intLockboxID, intBatchID, dteDepositDate, out objOLLockbox))
                                {
                                    throw (new Exception("User does not have access to the requested Check"));
                                }

                                bDisplayBatchID = objOLLockbox.DisplayBatchID;
                                nodeImage = ipoXmlLib.addElement(nodeImages, "Check", string.Empty);

                                strFileDisplayName = intBankID.ToString() + "_" +
                                                     intLockboxID.ToString() + "_" +
                                                     intBatchID.ToString() + "_" +
                                                     strPICSDate + "_" +
                                                     intTransactionID.ToString() + "_" +
                                                     intBatchSequence.ToString() + "_" +
                                                     "C";

                                break;
                            case "document":
                                if (!CBXSrv.AuthorizeBatchToUser(intBankID, intLockboxID, intBatchID, dteDepositDate, out objOLLockbox))
                                {
                                    throw (new Exception("User does not have access to the requested Document"));
                                }

                                bDisplayBatchID = objOLLockbox.DisplayBatchID;
                                nodeImage = ipoXmlLib.addElement(nodeImages, "Document", string.Empty);

                                strFileDisplayName = intBankID.ToString() + "_" +
                                                     intLockboxID.ToString() + "_" +
                                                     intBatchID.ToString() + "_" +
                                                     strPICSDate + "_" +
                                                     intTransactionID.ToString() + "_" +
                                                     intBatchSequence.ToString();
                                break;
                            default:
                                nodeImage = null;
                                break;
                        }

                        if (nodeImage != null)
                        {
                            ipoXmlLib.addAttribute(nodeImage, "BankID", intBankID.ToString());
                            ipoXmlLib.addAttribute(nodeImage, "LockboxID", intLockboxID.ToString());
                            ipoXmlLib.addAttribute(nodeImage, "BatchID", intBatchID.ToString());
                            ipoXmlLib.addAttribute(nodeImage, "BatchNumber", intBatchNumber.ToString());
                            ipoXmlLib.addAttribute(nodeImage, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
                            ipoXmlLib.addAttribute(nodeImage, "PICSDate", strPICSDate);
                            ipoXmlLib.addAttribute(nodeImage, "TransactionID", intTransactionID.ToString());
                            ipoXmlLib.addAttribute(nodeImage, "BatchSequence", intBatchSequence.ToString());
                            ipoXmlLib.addAttribute(nodeImage, "DisplayScannedCheck", (Session.ServerOptions.Preferences.DisplayScannedCheckOnline ? "1" : "0"));
                            ipoXmlLib.addAttribute(nodeImage, "DisplayBatchID", bDisplayBatchID.ToString());
                        }

                        // can only set one display mode if multiple images were to appear in request
                        // shouldn't be a problem since all items would be from same lockbox.
                        if (nodeJobRequestOptions.Attributes["checkImageDisplayMode"] == null)
                        {
                            ipoXmlLib.addAttribute(nodeJobRequestOptions, "checkImageDisplayMode", ((int)GetImageDisplayModeForBatch(intBankID, intLockboxID, "check")).ToString());
                        }

                        if (nodeJobRequestOptions.Attributes["documentImageDisplayMode"] == null)
                        {
                            ipoXmlLib.addAttribute(nodeJobRequestOptions, "documentImageDisplayMode", ((int)GetImageDisplayModeForBatch(intBankID, intLockboxID, "document")).ToString());
                        }
                    }

                    // Use the new "Display" format including DDA for returned image
                    ipoXmlLib.addAttribute(nodeJobRequestOptions, "olClientAccountDisplayOverride", objOLLockbox.LongName);

                    //Create the request in the DB
                    if (ItemProcDAL.CreateOnlineImageQueue(GUID, Session.SessionUser.UserID, strFileDisplayName, (intScannedCheck > 0 ? true : false), out intRowsAffected))
                    {
                        if (intRowsAffected <= 0)
                        {
                            throw (new Exception("Unable to create the view specified request in OnlineImageQueue."));
                        }
                    }
                    else
                    {
                        throw (new Exception("Unable to create the view specified request in OnlineImageQueue."));
                    }

                    // Get Request
                    dsRetVal = CheckImageJobStatus(GUID).Data;

                    if (dsRetVal != null)
                    {
                        if (dsRetVal.Tables[0].Rows.Count > 0)
                        {
                            // Submit request to CBXServer
                            if (!SubmitImageJob(GUID, objXMLOut))
                            {
                                dsRetVal = null;
                                throw (new Exception("Unable to submit the request to the IPO Image Service for processing."));
                            }
                        }
                    }
                    else
                    {
                        throw (new Exception("Unable to retrieve the new view specified request."));
                    }

                }
                else
                {
                    throw (new Exception("Unable to generate a new database GUID."));
                }

            }
            catch (Exception ex)
            {

                EventLog.logError(ex, this.GetType().Name, "CreateViewSpecifiedJobRequest");

                throw ;
            }

            response = new BaseGenericResponse<DataSet>()
            {
                Data = dsRetVal,
                Status = (dsRetVal != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<DataSet> CreateViewSelectedJobRequest(XmlDocument Parms)
        {
            Guid gidOnlineImageQueueId = Guid.NewGuid();
            return CreateJobRequest(gidOnlineImageQueueId, ReformatViewSelectedXml(gidOnlineImageQueueId.ToString(), Parms));
        }

        private BaseGenericResponse<DataSet> CreateJobRequest(Guid gidOnlineImageQueueID, XmlDocument requestFileXml)
        {
            DataSet dsRetVal = null;
            int intRowsAffected;
            BaseGenericResponse<DataSet> response = null;

            try
            {
                //Create the request in the DB
                if (ItemProcDAL.CreateOnlineImageQueue(gidOnlineImageQueueID, Session.SessionUser.UserID, Session.ServerOptions.Preferences.DisplayScannedCheckOnline, out intRowsAffected))
                {
                    if (intRowsAffected <= 0)
                    {
                        throw (new Exception("Unable to create the job request in OnlineImageQueue."));
                    }
                }

                //Get Request
                dsRetVal = CheckImageJobStatus(gidOnlineImageQueueID).Data;

                if (dsRetVal != null)
                {
                    if (dsRetVal.Tables[0].Rows.Count > 0)
                    {
                        //Submit request to CBXServer
                        if (!SubmitImageJob(gidOnlineImageQueueID, requestFileXml))
                        {
                            throw (new Exception("Unable to submit the request to the IPO Image Service for processing."));
                        }
                    }
                }
                else
                {
                    throw (new Exception("Unable to retrieve the new job request."));
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "CreateViewSelectedJobRequest");
                throw ;
            }

            response = new BaseGenericResponse<DataSet>()
            {
                Data = dsRetVal,
                Status = (dsRetVal != null) ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseGenericResponse<DataSet> CreateInProcessExceptionJobRequest(InProcessExceptionImageRequestDto request)
        {
            var jobId = Guid.NewGuid();

            var root = new XElement("ipoServiceRequest");

            var nodeJobRequest = new XElement("jobRequest",
                new XAttribute("jobID", jobId),
                new XAttribute("siteKey", SiteKey),
                new XAttribute("jobRequestType", "IN_PROCESS_EXCEPTION"),
                new XAttribute("sessionID", Session.SessionID.ToString()));
            root.Add(nodeJobRequest);

            var theResponse = OlDecisioningSvcClient.GetBatchImageDetails(request.GlobalBatchId, request.TransactionId);

            // Add the transaction data
            theResponse.Data.GlobalBatchId = request.GlobalBatchId;
            theResponse.Data.TransactionId = request.TransactionId;
            theResponse.Data.Amount = request.Amount;
            theResponse.Data.BankId = request.BankId;
            theResponse.Data.BatchId = request.BatchId;
            theResponse.Data.BatchNumber = request.BatchNumber;
            theResponse.Data.DepositDate = request.DepositDate;
            theResponse.Data.PaymentSource = request.PaymentSource;
            theResponse.Data.ProcessingDate = request.ProcessingDate;
            theResponse.Data.TransactionSequence = request.TransactionSequence;
            theResponse.Data.Workgroup = request.Workgroup;
            theResponse.Data.WorkgroupId = request.WorkgroupId;

            // add the display options to the DTO
            theResponse.Data.DisplayRemitterNameInPdf = Session.ServerOptions.Preferences.DisplayRemitterNameInPDF;
            theResponse.Data.DisplayModeForCheck = (int) GetImageDisplayModeForBatch(request.BankId, request.WorkgroupId, "check");
            theResponse.Data.DisplayModeForDocument = (int)GetImageDisplayModeForBatch(request.BankId, request.WorkgroupId, "document");

            var contractRequest = new XElement("contract");
            nodeJobRequest.Add(contractRequest);

            using (var writer = contractRequest.CreateWriter())
            {
                var serializer = new DataContractSerializer(typeof(InProcessExceptionImageRequestDto));
                serializer.WriteObject(writer, theResponse.Data);
            }

            var xmlDocument = new XmlDocument();
            xmlDocument.Load(root.CreateReader());
            return CreateJobRequest(jobId, xmlDocument);
        }

        private XmlDocument ReformatViewSelectedXml(string jobId, XmlDocument OldXml)
        {
            XmlDocument objNewXml;
            XmlNode nodejobRequest;
            XmlNode nodeJobRequestOptions;
            XmlNode nodeTransaction;
            XmlNode nodeTransactions;
            XmlNode nodeOldJobRequestOptions;
            XmlNode nodeSelectedItems;
            XmlNodeList nlSelectedItems;
            Hashtable dicBatches;
            int intBankID;
            int intLockboxID;
            int intBatchID;
            DateTime dteDepositDate;
            int intTransactionID;
            int intBatchNumber;
            int intTemp;
            DateTime dteTemp;
            bool blnUserAuthorized = false;
            cOLLockbox objOLLockbox = null;

            // Create New XmlDocument
            objNewXml = ipoXmlLib.GetXMLDoc("ipoServiceRequest");

            // Create Job Request Node
            nodejobRequest = ipoXmlLib.addElement((objNewXml.DocumentElement), "jobRequest", string.Empty);

            ipoXmlLib.addAttribute(nodejobRequest, "jobID", jobId);
            ipoXmlLib.addAttribute(nodejobRequest, "siteKey", base.SiteKey);
            ipoXmlLib.addAttribute(nodejobRequest, "jobRequestType", "TRANSACTION");
            ipoXmlLib.addAttribute(nodejobRequest, "sessionID", Session.SessionID.ToString());

            nodeOldJobRequestOptions = OldXml.DocumentElement.SelectSingleNode("jobRequestOptions");

            if (nodeOldJobRequestOptions != null)
            {
                nodeJobRequestOptions = nodejobRequest.AppendChild(nodejobRequest.OwnerDocument.ImportNode(OldXml.DocumentElement.SelectSingleNode("jobRequestOptions"), true));

                ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayScannedCheck", Session.ServerOptions.Preferences.DisplayScannedCheckOnline.ToString());
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "returnType", "PDF");
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "includeImages", "True");
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayRemitterNameInPDF", Session.ServerOptions.Preferences.DisplayRemitterNameInPDF.ToString());
                ipoXmlLib.addAttribute(nodeJobRequestOptions, "displayBatchID", Session.ServerOptions.Preferences.DisplayBatchNumberAndBatchIDResearch.ToString());

                //Append Image Filter Option.  Expects: "ChecksOnly", "DocsOnly", or "All"
                nodeSelectedItems = OldXml.DocumentElement.SelectSingleNode("SelectedItems");

                if ((nodeSelectedItems.Attributes.GetNamedItem("ImageFilterOption") != null))
                {
                    switch (nodeSelectedItems.Attributes.GetNamedItem("ImageFilterOption").InnerText)
                    {
                        case "ChecksOnly":
                            ipoXmlLib.addAttribute(nodeJobRequestOptions, "imageFilterOption", "ChecksOnly");
                            break;
                        case "DocumentsOnly":
                            ipoXmlLib.addAttribute(nodeJobRequestOptions, "imageFilterOption", "DocumentsOnly");
                            break;
                        default:
                            ipoXmlLib.addAttribute(nodeJobRequestOptions, "imageFilterOption", "All");
                            break;
                    }
                }
                else
                {
                    ipoXmlLib.addAttribute(nodeJobRequestOptions, "imageFilterOption", "All");
                }

                //UPGRADE_NOTE: Object nodeSelectedItems may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
                nodeSelectedItems = null;
            }
            else
            {
                throw (new Exception("Job Request Options must be included with the Xml Job request."));
            }

            //*Create Transactions Node
            nodeTransactions = ipoXmlLib.addElement(nodejobRequest, "transactions", string.Empty);
            nlSelectedItems = OldXml.DocumentElement.SelectNodes("SelectedItems/SelectedItem");

            if (nlSelectedItems != null)
            {
                dicBatches = new Hashtable();

                foreach (XmlNode nodeSelectedItem in nlSelectedItems)
                {
                    if (nodeSelectedItem.Attributes["BankID"] != null && int.TryParse(nodeSelectedItem.Attributes["BankID"].Value, out intTemp))
                    {
                        intBankID = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid BankID specified for View Specified request."));
                    }

                    if (nodeSelectedItem.Attributes["ClientAccountID"] != null && int.TryParse(nodeSelectedItem.Attributes["ClientAccountID"].Value, out intTemp))
                    {
                        intLockboxID = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid ClientAccountID specified for View Specified request."));
                    }

                    if (nodeSelectedItem.Attributes["BatchID"] != null && int.TryParse(nodeSelectedItem.Attributes["BatchID"].Value, out intTemp))
                    {
                        intBatchID = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid BatchID specified for View Specified request."));
                    }

                    if (nodeSelectedItem.Attributes["DepositDate"] != null && DateTime.TryParse(nodeSelectedItem.Attributes["DepositDate"].Value, out dteTemp))
                    {
                        dteDepositDate = dteTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid DepositDate specified for View Specified request."));
                    }

                    if (nodeSelectedItem.Attributes["TransactionID"] != null && int.TryParse(nodeSelectedItem.Attributes["TransactionID"].Value, out intTemp))
                    {
                        intTransactionID = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid TransactionID specified for View Specified request."));
                    }

                    if (nodeSelectedItem.Attributes["BatchNumber"] != null && int.TryParse(nodeSelectedItem.Attributes["BatchNumber"].Value, out intTemp))
                    {
                        intBatchNumber = intTemp;
                    }
                    else
                    {
                        throw (new Exception("Invalid BatchNumber specified for View Specified request."));
                    }

                    // Only Authorize using the first item, they are all from the same lockbox.  The DAL isn't using the deposit date
                    // to restrict viewing days.  Once the DAL uses Viewing days, i.e., different deposit dates in this group of selected items,
                    // then this needs to change.
                    if (!blnUserAuthorized)
                    {
                        if (!CBXSrv.AuthorizeBatchToUser(intBankID, intLockboxID, intBatchID, dteDepositDate, out objOLLockbox))
                        {
                            throw (new Exception("User does not have access to view the selected items."));
                        }
                        else
                        {
                            blnUserAuthorized = true;
                        }
                    }

                    string strViewSelectedKey = intBankID.ToString() + "~" +
                                                intLockboxID.ToString() + "~" +
                                                intBatchID.ToString() + "~" +
                                                dteDepositDate.ToString("yyyyMMdd") + "~" +
                                                intTransactionID.ToString() + "~" +
                                                intBatchNumber.ToString();

                    if (!dicBatches.Contains(strViewSelectedKey))
                    {
                        dicBatches.Add(strViewSelectedKey, string.Empty);

                        nodeTransaction = ipoXmlLib.addElement(nodeTransactions, "transaction", string.Empty);

                        ipoXmlLib.addAttribute(nodeTransaction, "BankID", intBankID.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "LockboxID", intLockboxID.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "BatchID", intBatchID.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
                        ipoXmlLib.addAttribute(nodeTransaction, "TransactionID", intTransactionID.ToString());
                        ipoXmlLib.addAttribute(nodeTransaction, "BatchNumber", intBatchNumber.ToString());
                    }

                    // can only set one display mode if multiple images were to appear in request.
                    // shouldn't be a problem since all items would be from same lockbox.
                    if (nodeJobRequestOptions.Attributes["checkImageDisplayMode"] == null)
                    {
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "checkImageDisplayMode", ((int)GetImageDisplayModeForBatch(intBankID, intLockboxID, "check")).ToString());
                    }

                    if (nodeJobRequestOptions.Attributes["documentImageDisplayMode"] == null)
                    {
                        ipoXmlLib.addAttribute(nodeJobRequestOptions, "documentImageDisplayMode", ((int)GetImageDisplayModeForBatch(intBankID, intLockboxID, "document")).ToString());
                    }
                }       // end foreach nlSelectedItems

                // Use the new ClientDisplay Label w/DDA
                if (objOLLockbox != null)
                {
                    ipoXmlLib.addAttribute(nodeJobRequestOptions, "olClientAccountDisplayOverride", objOLLockbox.LongName);
                }
            }

            return objNewXml;
        }

        private bool SubmitImageJob(Guid OLImageQueueID, XmlDocument doc)
        {
            string strFileName;
            bool bolRetVal = false;

            try
            {
                strFileName = OLImageQueueID.ToString() + ".xml";

                MemoryStream ReadFileStream = new MemoryStream();
                doc.Save(ReadFileStream);
                ReadFileStream.Position = 0;
                OLFClient.PutStream(strFileName, ReadFileStream, Constants.OLF_IMAGE_SVC_JOB);
                ReadFileStream.Flush();
                ReadFileStream.Close();

                bolRetVal = true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex);
                bolRetVal = false;
            }

            return (bolRetVal);
        }

        private OLFImageDisplayMode GetImageDisplayModeForBatch(int BankID, int LockboxID, string ImageType)
        {
            DataTable dt;
            OLFImageDisplayMode enmRetVal;

            try
            {
                enmRetVal = OLFImageDisplayMode.Undefined;

                if (ItemProcDAL.GetImageDisplayModeForBatch(Session.SessionID, BankID, LockboxID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        switch (ImageType.ToLower())
                        {
                            case "check":
                                if (dt.Rows[0].IsNull("CheckImageDisplayMode"))
                                {
                                    enmRetVal = Session.ServerOptions.Preferences.CheckImageDisplayMode;
                                }
                                else
                                {
                                    try
                                    {
                                        enmRetVal = ((OLFImageDisplayMode)(byte)(dt.Rows[0]["CheckImageDisplayMode"]));

                                        if (enmRetVal == OLFImageDisplayMode.Undefined)
                                        {
                                            enmRetVal = Session.ServerOptions.Preferences.CheckImageDisplayMode;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        EventLog.logError(e, this.GetType().Name, "GetImageDisplayModeForBatch");
                                        enmRetVal = Session.ServerOptions.Preferences.CheckImageDisplayMode;
                                    }
                                }

                                break;
                            case "document":
                                if (dt.Rows[0].IsNull("DocumentImageDisplayMode"))
                                {
                                    enmRetVal = Session.ServerOptions.Preferences.DocumentImageDisplayMode;
                                }
                                else
                                {
                                    try
                                    {
                                        enmRetVal = ((OLFImageDisplayMode)(byte)(dt.Rows[0]["DocumentImageDisplayMode"]));

                                        if (enmRetVal == OLFImageDisplayMode.Undefined)
                                        {
                                            enmRetVal = Session.ServerOptions.Preferences.DocumentImageDisplayMode;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        EventLog.logError(e, this.GetType().Name, "GetImageDisplayModeForBatch");
                                        enmRetVal = Session.ServerOptions.Preferences.DocumentImageDisplayMode;
                                    }
                                }

                                break;
                        }
                    }

                    dt.Dispose();
                }
            }
            catch (Exception)
            {
                enmRetVal = OLFImageDisplayMode.Undefined;
            }

            return (enmRetVal);
        }

        private OLFImageDisplayMode GetImageDisplayModeForLockbox(Guid OLLockboxID, string ImageType)
        {
            DataTable dt;
            OLFImageDisplayMode enmRetVal;

            try
            {
                enmRetVal = OLFImageDisplayMode.Undefined;

                if (ItemProcDAL.GetImageDisplayModeForLockbox(OLLockboxID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        switch (ImageType.ToLower())
                        {
                            case "check":
                                if (dt.Rows[0].IsNull("CheckImageDisplayMode"))
                                {
                                    enmRetVal = Session.ServerOptions.Preferences.CheckImageDisplayMode;
                                }
                                else
                                {
                                    try
                                    {
                                        enmRetVal = ((OLFImageDisplayMode)(byte)dt.Rows[0]["CheckImageDisplayMode"]);
                                    }
                                    catch (Exception e)
                                    {
                                        EventLog.logError(e, this.GetType().Name, "GetImageDisplayModeForLockbox");
                                        enmRetVal = Session.ServerOptions.Preferences.CheckImageDisplayMode;
                                    }
                                }

                                break;
                            case "document":
                                if (dt.Rows[0].IsNull("DocumentImageDisplayMode"))
                                {
                                    enmRetVal = Session.ServerOptions.Preferences.DocumentImageDisplayMode;
                                }
                                else
                                {
                                    try
                                    {
                                        enmRetVal = ((OLFImageDisplayMode)(byte)dt.Rows[0]["DocumentImageDisplayMode"]);
                                    }
                                    catch (Exception e)
                                    {
                                        enmRetVal = Session.ServerOptions.Preferences.DocumentImageDisplayMode;
                                        EventLog.logError(e, this.GetType().Name, "GetImageDisplayModeForBatch");
                                    }
                                }

                                break;
                        }
                    }

                    dt.Dispose();
                }
            }
            catch (Exception)
            {
                enmRetVal = OLFImageDisplayMode.Undefined;
            }

            return (enmRetVal);
        }

        private byte GetOnlineColorMode(int bankID, int lockboxID)
        {
            DataTable dt;
            byte bytRetVal = 0;
            bool bolRetVal;
            string strCollectionKey = bankID.ToString() + "~" + lockboxID.ToString();

            if (_LockboxColorModes == null)
            {
                _LockboxColorModes = new SortedList();
            }

            if (_LockboxColorModes.ContainsKey(strCollectionKey))
            {
                bytRetVal = (byte)_LockboxColorModes[strCollectionKey];
            }
            else
            {
                bolRetVal = ItemProcDAL.GetLockboxOnlineColorMode(bankID, lockboxID, out dt);

                if (bolRetVal && dt.Rows.Count > 0)
                {
                    System.Data.DataRow dr = dt.Rows[0];

                    try
                    {
                        bytRetVal = ipoLib.NVL(ref dr, "OnlineColorMode", LTAConstants.COLOR_MODE_BLACK_AND_WHITE);
                    }
                    catch (InvalidCastException)
                    {
                        bytRetVal = (Byte)ipoLib.NVL(ref dr, "OnlineColorMode", (int)LTAConstants.COLOR_MODE_BLACK_AND_WHITE);
                    }

                    dt.Dispose();
                    _LockboxColorModes.Add(strCollectionKey, bytRetVal);
                }
                else
                {
                    dt.Dispose();
                    bytRetVal = LTAConstants.COLOR_MODE_BLACK_AND_WHITE;
                    throw (new Exception("Specified Lockbox does not exist; Lockbox : "
                                    + lockboxID.ToString()));
                }
            }

            return (bytRetVal);
        }
    }
}
