﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Runtime.CompilerServices;

using WFS.RecHub.BusinessCommon;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
 * * WI 154797 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 156704 TWE 08/01/2014
*      Adjust the calls for user preferences
* WI 157331 TWE 08/06/2014
*    Add permissions node to base document - fix stored procedure call
* WI 200563 TWE 04/09/2015
*    Add AlertDownload permission to permission node
********************************************************************************/

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class SystemAPI : BaseOLAPI, ISystemAPI
    {
        private R360ServicesDAL _dsdDAL = null;
        private LockboxAPI _LockboxSrv;
        private cSessionDAL _SessionDAL = null;
        private UserAPI _UserSrv;

        public SystemAPI(IServiceContext serviceContext)
            : base(serviceContext)
        {
        }

        #region Session Validation

        protected override bool ValidateSID(out int userID)
        {
            bool bReturnValue = false;
            userID = -1;
            DataTable dtResults;

            OnLogEvent("Current User Claims: \r\n" + _ServiceContext.RAAMClaims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);

            if (R360ServicesDAL.GetUserIDBySID(_ServiceContext.GetSID(), out dtResults))
            {
                userID = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
                bReturnValue = true;
            }

            return bReturnValue;
        }

        #endregion

        #region Wrapper Method

        private T ValidateUserIdAndDo<T>(Action<T, R360ServicesDAL, int, Guid> operation, [CallerMemberName] string procName = null)
          where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T();
            result.Status = StatusCode.FAIL;

            try
            {
                int userID;
                // Validate / get the user ID
                if (ValidateSIDAndLog(procName, out userID))
                {
                    // Perform the requested operation
                    operation(result, R360ServicesDAL, userID, _ServiceContext.GetSessionID());
                }
                else
                {
                    result.Errors.Add("Invalid SID");
                }
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }


        private T ValidateUserIdAndDo<T>(Action<T, R360ServicesDAL, R360ServicesExceptionSummaryDAL, int, Guid> operation, [CallerMemberName] string procName = null)
        where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T();
            result.Status = StatusCode.FAIL;

            try
            {
                int userID;
                using (R360ServicesDAL dsdDAL = new R360ServicesDAL(SiteKey))
                {
                    using (R360ServicesExceptionSummaryDAL dsExceptDal = new R360ServicesExceptionSummaryDAL(SiteKey))
                    {
                        // Validate / get the user ID
                        if (ValidateSIDAndLog(procName, out userID))
                        {
                            // Perform the requested operation
                            operation(result, dsdDAL, dsExceptDal, userID, _ServiceContext.GetSessionID());
                        }
                        else
                        {
                            result.Errors.Add("Invalid SID");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }

        #endregion

        /// <summary>
        /// Retrieves reference to LockboxSrv service
        /// </summary>
        private LockboxAPI LockboxSrv
        {
            get
            {
                if (_LockboxSrv == null)
                {
                    _LockboxSrv = new LockboxAPI(_serviceContext);
                    _LockboxSrv.Session = Session;
                }

                return (_LockboxSrv);
            }
        }
        private R360ServicesDAL R360ServicesDAL
        {
            get
            {
                if (_dsdDAL == null)
                {
                    if (!string.IsNullOrEmpty(SiteKey))
                    {
                        _dsdDAL = new R360ServicesDAL(SiteKey);
                    }
                }
                return (_dsdDAL);
            }
        }
        protected override RecHub.Common.ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acR360WebServiceCall;
            }
        }
        private cSessionDAL SessionDAL
        {
            get
            {
                if (_SessionDAL == null)
                {
                    _SessionDAL = new cSessionDAL(this.SiteKey);
                }

                return (_SessionDAL);
            }
        }
        /// <summary>
        /// Retrieves reference to UserSrv service
        /// </summary>
        private UserAPI UserSrv
        {
            get
            {
                if (_UserSrv == null)
                {
                    _UserSrv = new UserAPI(_serviceContext);
                    _UserSrv.Session = Session;
                }
                return (_UserSrv);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !Disposed)
            {
                if (_dsdDAL != null)
                {
                    _dsdDAL.Dispose();
                }
                if (_LockboxSrv != null)
                {
                    _LockboxSrv.Dispose();
                }
                if (_SessionDAL != null)
                {
                    _SessionDAL.Dispose();
                }
                if (_UserSrv != null)
                {
                    _UserSrv.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        public PageActivityResponse InitPageActivity(string ScriptName, string QueryString)
        {
            PageActivityResponse response = null;

            try
            {
                response = new PageActivityResponse()
                {
                    Data = new PageActivity()
                    {
                        ActivityLogID = LogPageAccess(ScriptName, QueryString)
                    },
                    Status = StatusCode.SUCCESS
                };

                return (response);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex);

                response = new PageActivityResponse()
                {
                    Data = new PageActivity()
                    {
                        ActivityLogID = -1,
                        UserHasPagePermission = false
                    },
                    Status = StatusCode.FAIL
                };

                return (response);
            }
        }

        private long LogPageAccess(string vScriptName, string vQueryString)
        {
            long longRetVal = -1;
            int intNextPage;
            bool blnError = false;
            DataTable dt;
            int lngRecords;
            long lActivityLogID;
            string strModuleName;

            try
            {
                try
                {
                    // Determine next page counter number
                    if (SessionDAL.GetNextPageCounter(SessionID, out dt) && dt.Rows.Count > 0)
                    {
                        intNextPage = (int)dt.Rows[0]["NextPage"];
                        dt.Dispose();
                        SessionDAL.InsertSessionLog(SessionID, intNextPage, string.Empty, vScriptName, vQueryString, out lngRecords);
                        SessionDAL.UpdateSession(intNextPage, SessionID, out lngRecords);
                    }
                    else
                    {
                        //TODO: Log Error!!!
                        intNextPage = 0;
                    }
                }
                catch (Exception e)
                {
                    EventLog.logError(e, this.GetType().Name, "LogPageAccess");
                    intNextPage = 0;
                }

                lActivityLogID = 0;

                if (vScriptName.IndexOf(".") > -1)
                {
                    strModuleName = vScriptName.Substring(0, vScriptName.IndexOf(".")).ToUpper();
                }
                else
                {
                    strModuleName = vScriptName.ToUpper();
                }

                switch (strModuleName)
                {
                    case "DAILYDEPOSITS":
                        strModuleName = "LOCKBOXSUMMARY";
                        break;
                    case "REMITDISPLAY":
                        strModuleName = "BATCHDETAIL";
                        break;
                    case "TRANSACTIONDISPLAY":
                        strModuleName = "TRANSACTIONDETAIL";
                        break;
                    case "VIEWREMITTER":
                        strModuleName = "VIEWREMITTERS";
                        break;
                    default:
                        break;
                    //Everything else gets the actual name of the page.
                }

                // Log page access to SessionLog table
                SessionDAL.InsertSessionActivityLog(SessionID, strModuleName, ActivityCodes.acPageAccess, out lngRecords, out lActivityLogID);

                if (lngRecords != 1)
                {
                    blnError = true;
                }
                else
                {
                    longRetVal = lActivityLogID;
                }

                SessionDAL.UpdateSession(intNextPage, SessionID, out lngRecords);

                if (lngRecords != 1)
                {
                    blnError = true;
                }

                if (blnError)
                {
                    longRetVal = -1;
                }
            }
            catch (Exception ex)
            {
                longRetVal = -1;
                OnErrorEvent(ex, this.GetType().Name, "LogPageAccess");
                throw (new Exception("Unable to log page access."));
            }

            return longRetVal;
        }


        public BaseGenericResponse<cUserRAAM> GetSessionUser()
        {
            BaseGenericResponse<cUserRAAM> response = null;

            try
            {
                response = new BaseGenericResponse<cUserRAAM>()
                {
                    Data = Session.SessionUser,
                    Status = (Session.SessionUser != null) ? StatusCode.SUCCESS : StatusCode.FAIL
                };
            }
            catch (Exception ex)
            {

                // Write failure to log file and return error
                EventLog.logError(ex, this.GetType().Name, "GetSessionUser");
                throw (new Exception("Unable to retrieve session user object."));
            }

            return (response);
        }


        public BaseGenericResponse<cOLLockbox> GetLockboxByID(int OLWorkgroupsID)
        {
            cOLLockboxes objLockboxes;
            BaseGenericResponse<cOLLockbox> response = null;

            objLockboxes = LockboxSrv.GetUserLockboxes().Data;

			if (objLockboxes != null && objLockboxes.ContainsKey(OLWorkgroupsID))
            {
                response = new BaseGenericResponse<cOLLockbox>()
                {
					Data = objLockboxes[OLWorkgroupsID],
                    Status = StatusCode.SUCCESS
                };
            }
            else
            {
                response = new BaseGenericResponse<cOLLockbox>()
                {
                    Data = null,
                    Status = StatusCode.FAIL
                };
            }

            return (response);
        }

        public XmlDocumentResponse GetBaseDocument()
        {
            var userOLPreferences = GetUserOLPreferences();
            var breadCrumbs = SessionDAL.GetSessionBreadCrumbs(Session.SessionID);
            var sessionExpirationInMinutes = Session.ServerOptions.Preferences.SessionExpiration;
            var logonMethod = Session.OLSiteOptions.LogonMethod;
            var currentDateTime = DateTime.Now;

            var builder = new BaseDocumentBuilder(EventLog, userOLPreferences,
                breadCrumbs, sessionExpirationInMinutes, logonMethod,
                currentDateTime, IsPermissionAllowed);
            return builder.BuildBaseDocument();
        }
        private bool IsPermissionAllowed(string asset, R360Permissions.ActionType actionType)
        {
            return R360Permissions.Current.Allowed(asset, actionType);
        }
        private IList<cOLPreference> GetUserOLPreferences()
        {
            OLPreferences UserOLPreferences = new OLPreferences(base.SiteKey);
            UserOLPreferences.LoadOLPreferences(Session.SessionUser.UserID);
            return UserOLPreferences.GetPreferences;
        }
        public BaseResponse LogActivity(ActivityCodes ActivityCode, Guid OnlineImageQueueID, int itemCount, int BankID, int LockboxID)
        {
            bool bolRetVal = false;
            int intRecordsAffected;
            BaseResponse response = null;

            SessionDAL.InsertSessionActivityLog(Session.SessionID, "CBOServer", ActivityCode, OnlineImageQueueID, itemCount, BankID, LockboxID, out intRecordsAffected);

            if (intRecordsAffected >= 0)
            {
                bolRetVal = true;
            }
            else
            {
                bolRetVal = false;
            }

            response = new BaseResponse()
            {
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseResponse SetWebDeliveredFlag(Guid OnlineImageQueueID, bool InternetDelivered)
        {
            bool bolRetVal = false;
            BaseResponse response = null;

            if (SessionDAL.SetWebDeliveredFlag(OnlineImageQueueID, InternetDelivered))
            {
                bolRetVal = true;
            }
            else
            {
                bolRetVal = false;
            }

            response = new BaseResponse()
            {
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }

        public BaseResponse UpdateActivityLockbox(long ActivityLogID, int BankID, int LockboxID)
        {
            bool bolRetVal = false;
            int intRowsAffected;
            BaseResponse response = null;

            if (ActivityLogID > 0)
            {

                if (SessionDAL.UpdateActivityLockbox(ActivityLogID, BankID, LockboxID, out intRowsAffected))
                {
                    bolRetVal = true;
                }
                else
                {
                    bolRetVal = false;
                }
            }

            response = new BaseResponse()
            {
                Status = bolRetVal ? StatusCode.SUCCESS : StatusCode.FAIL
            };

            return (response);
        }


    }
}
