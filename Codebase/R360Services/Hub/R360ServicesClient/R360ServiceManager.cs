﻿using System;
using System.Collections.Generic;
using System.Configuration;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.Common;
using WFS.LTA.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.Common.DTO;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Ryan Schwantes 
 * * Date: 06/23/2013
 * *
 * * Purpose:  Service Client
 * *
 * * Modification History
 * WI 100422 RS 06/23/2013 Created
 * WI 101855 WJS 7/5/2013   Added CheckUserPermissionsForFunction
 * WI 143459 RDS 05/22/2014 Move binding to config file
* ******************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class R360ServiceManager : IR360Services
    {
        #region Construction / Initialization

        // Initialization parameters
        private const string LogSource = "R360ServicesClient";
        private static ltaLog _eventLog;
        private static string _siteKey;
        private static cSiteOptions _SiteOptions = null;
        private readonly R360ServiceClient _client;

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected static cSiteOptions SiteOptions
        {
            get
            {
                if (_SiteOptions == null)
                {
                    _SiteOptions = new cSiteOptions(_siteKey);
                }
                return _SiteOptions;
            }
        }
        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected static ltaLog EventLog
        {
            get
            {
                if (_eventLog == null)
                {
                    _eventLog = new ltaLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (LTAMessageImportance)SiteOptions.loggingDepth);
                }

                return _eventLog;
            }
        }

        protected static string SiteKey
        {
            get
            {
                return (_siteKey);
            }
            set
            {
                _siteKey = value;
            }
        }

        static R360ServiceManager()
        {
            // Make sure the site key is initialized before any other properties are used
            GetSiteKey();
        }

        public R360ServiceManager()
        {
            // Create connection context
            try
            {
                _client = new R360ServiceClient(_siteKey, EventLog);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, LogSource, ".ctor");

                // Consructors should not throw exceptions -- just initialize to null and report errors later...
                _client = null;
            }
        }

        // Specialied method for site key that properly initializes the EventLog after the site key has been set
        private static void GetSiteKey()
        {
            string defaultValue = "IPOnline";
            try
            {
                string value = ConfigurationManager.AppSettings["siteKey"];
                if (string.IsNullOrEmpty(value))
                    value = defaultValue;

                _siteKey = value;

                EventLog.logEvent(string.Format("Using SiteKey = '{0}'", value), LogSource, LTA.Common.LTAMessageImportance.Verbose);
            }
            catch (Exception ex)
            {
                _siteKey = defaultValue;

                EventLog.logWarning(string.Format("Error reading configuration setting 'siteKey' - {0}", ex.Message), LogSource, LTA.Common.LTAMessageImportance.Verbose);
            }
        }

        private void ValidateConnection()
        {
            if (_client == null)
                throw new Exception("Configuration Error - check log for details");
        }

        #endregion

        public PingResponse Ping()
        {
            ValidateConnection();
            return _client.Ping();
        }

        public SummariesResponse GetClientAccountSummary(ReceivablesSummaryRequestDto request)
        {
            ValidateConnection();
            return _client.GetClientAccountSummary(request);
        }

        public ClientAccountsResponse GetClientAccounts()
        {
            ValidateConnection();
            return _client.GetClientAccounts();
        }

        public ClientAccountsResponse GetClientAccountsForOrganization(Guid OLOrganizationID)
        {
            ValidateConnection();
            return _client.GetClientAccountsForOrganization(OLOrganizationID);
        }

        public ExceptionTypeSummaryResponse GetExceptionTypeSummary(EntityDTO entity, WorkgroupIDsDTO workgroup, DateTime datefrom, DateTime dateto)
        {
            ValidateConnection();
            return _client.GetExceptionTypeSummary(entity, workgroup, datefrom, dateto);
        }

        public PermissionsResponse CheckUserPermissionsForFunction(string function, string mode)
        {
            ValidateConnection();
            return _client.CheckUserPermissionsForFunction(function, mode);
        }

        public BaseResponse WriteAuditEvent(string eventName, string applicationName, string description)
        {
            return WriteAuditEvent(eventName, "Uncategorized", applicationName, description);
        }

        public BaseResponse WriteAuditEvent(string eventName, string eventType, string applicationName, string description)
        {
            ValidateConnection();
            return _client.WriteAuditEvent(eventName, eventType, applicationName, description);
        }

        public BaseGenericResponse<List<WorkgroupNameDTO>> GetActiveWorkgroups(EntityDTO entity)
        {
            ValidateConnection();
            return _client.GetActiveWorkgroups(entity);
        }

        public BaseGenericResponse<List<WorkgroupNameDTO>> GetIntegraPayOnlyWorkgroups(EntityDTO entity)
        {
            ValidateConnection();
            return _client.GetIntegraPayOnlyWorkgroups(entity);
        }

        public BaseGenericResponse<List<AuditUserDTO>> GetAuditUsers()
        {
            ValidateConnection();
            return _client.GetAuditUsers();
        }

        public BaseGenericResponse<string> GetBankName(int siteBankId) {
            ValidateConnection();
            return _client.GetBankName(siteBankId);
        }

        public BaseGenericResponse<List<DDASummaryDataDto>> GetDDASummary(DateTime depositDate)
        {
            ValidateConnection();
            return _client.GetDDASummary(depositDate);
        }

        public DataEntryResponseDTO GetDataEntryFieldsForWorkgroup(DataEntrySearchDTO search)
        {
            ValidateConnection();
            return _client.GetDataEntryFieldsForWorkgroup(search);
        }

        public BaseGenericResponse<long> GetSourceBatchID(long batchID)
        {
            ValidateConnection();
            return _client.GetSourceBatchID(batchID);
        }

        public AdvancedSearchResultsDto GetAdvancedSearch(AdvancedSearchDto req)
        {
            ValidateConnection();
            return _client.GetAdvancedSearch(req);
        }

        public BatchSummaryResponseDto GetBatchSummary(BatchSummaryRequestDto requestContext)
        {
            ValidateConnection();
            return _client.GetBatchSummary(requestContext);
        }

        public BatchDetailResponseDto GetBatchDetail(BatchDetailRequestDto requestContext)
        {
            ValidateConnection();
            return _client.GetBatchDetail(requestContext);
        }

        public TransactionDetailResponseDto GetTransactionDetail(TransactionDetailRequestDto requestContext)
        {
            ValidateConnection();
            return _client.GetTransactionDetail(requestContext);
        }

        public NotificationsResponseDto GetNotifications(NotificationsRequestDto request)
        {
            ValidateConnection();
            return _client.GetNotificationsResponse(request);
        }

        public NotificationResponseDto GetNotification(NotificationRequestDto request)
        {
            ValidateConnection();
            return _client.GetNotification(request);
        }

        public NotificationsFileTypesResponseDto GetNotificationsFileTypesResponse()
        {
            ValidateConnection();
            return _client.GetNotificationsFileTypesResponse();
        }

        public PaymentTypesResponseDto GetPaymentTypes()
        {
            ValidateConnection();
            return _client.GetPaymentTypes();
        }
        public PaymentSourcesResponseDto GetPaymentSources()
        {
            ValidateConnection();
            return _client.GetPaymentSources();
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> SaveStoredQuery(AdvancedSearchStoredQueryDto entity)
        {
            ValidateConnection();
            return _client.SaveStoredQuery(entity);
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> DeleteStoredQuery(Guid entity)
        {
            ValidateConnection();
            return _client.DeleteStoredQuery(entity);
        }

        public BaseGenericResponse<List<AdvancedSearchStoredQueryDto>> GetStoredQueries()
        {
            ValidateConnection();
            return _client.GetStoredQueries();
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> GetStoredQuery(Guid id)
        {
            ValidateConnection();
            return _client.GetStoredQuery(id);
        }

        public PaymentSearchResponseDto GetPaymentSearch(PaymentSearchRequestDto request)
        {
            ValidateConnection();
            return _client.GetPaymentSearch(request);
        }

        public InvoiceSearchResponseDto GetInvoiceSearch(InvoiceSearchRequestDto request)
        {
            ValidateConnection();
            return _client.GetInvoiceSearch(request);
        }
    }
}
