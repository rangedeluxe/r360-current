﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{


	internal class HttpHeaderBehavior : IEndpointBehavior
	{
		private readonly ISessionHeaderValueProvider _headerProvider;
		public HttpHeaderBehavior(ISessionHeaderValueProvider headerProvider)
		{
			_headerProvider = headerProvider;
		}

		public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
		{
		}

		public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
		{
			clientRuntime.ClientMessageInspectors.Add(new HttpHeaderMessageInspector(_headerProvider));
		}

		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
		{
		}

		public void Validate(ServiceEndpoint endpoint)
		{
		}
	}

	internal class HttpHeaderMessageInspector : IClientMessageInspector
	{
		private readonly ISessionHeaderValueProvider _headerProvider;
		public HttpHeaderMessageInspector(ISessionHeaderValueProvider headerProvider)
		{
			_headerProvider = headerProvider;
		}

		public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
		{
			// No action
		}

		public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
		{
			var headers = _headerProvider.SessionHeaderValue;

			// Apply HTTP headers to the request
			object objHttpRequestMessage;
			HttpRequestMessageProperty httpRequestMessage;
			if (request.Properties.TryGetValue(HttpRequestMessageProperty.Name, out objHttpRequestMessage))
			{
				httpRequestMessage = objHttpRequestMessage as HttpRequestMessageProperty;
			}
			else
			{
				httpRequestMessage = new HttpRequestMessageProperty();
				request.Properties.Add(HttpRequestMessageProperty.Name, httpRequestMessage);
			}

			// Add each header value
			httpRequestMessage.Headers["SiteKey"] = headers.SiteKey;


            if (headers.SessionID != Guid.Empty)
				httpRequestMessage.Headers["SessionID"] = headers.SessionID.ToString();

			if (headers.RAAMClaims != null )
				httpRequestMessage.Headers["RAAMClaims"] = headers.RAAMClaims.ToString();

			return null;
		}
	}
}
