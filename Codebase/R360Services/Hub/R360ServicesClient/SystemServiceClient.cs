﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
 * * WI 154797 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class SystemServiceClient : ServicesClientBase, ISystemServices
    {
        private readonly ISystemServices _SystemService = null;

        public SystemServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
			_SystemService = R360ServiceFactory.Create<ISystemServices>();
        }

        private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
        {
            // All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
            try
            {
                using (new OperationContextScope((IContextChannel)_SystemService))
                {
                    // Set context inside of operation scope
                    R360ServiceContext.Init(this.SiteKey);

                    return operation();
                }
            }
            catch (FaultException<InvalidSiteKeyFault> ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw;
            }
        }

        public PingResponse Ping()
        {
            return Do(_SystemService.Ping);
        }

        public PageActivityResponse InitPageActivity(string ScriptName, string QueryString)
        {
            return Do(() => _SystemService.InitPageActivity(ScriptName, QueryString));
        }

        public BaseGenericResponse<cUserRAAM> GetSessionUser()
        {
            return Do(() => _SystemService.GetSessionUser());
        }

        public BaseGenericResponse<cOLLockbox> GetLockboxByID(int OLWorkGroupsID)
        {
			return Do(() => _SystemService.GetLockboxByID(OLWorkGroupsID));
        }


        public BaseGenericResponse<cOLLockbox> GetLockBoxByAccountIDBankID(int BankID, int LockboxID)
        {
            return Do(() => _SystemService.GetLockBoxByAccountIDBankID(BankID, LockboxID));
        }

        public BaseResponse LogActivity(ActivityCodes ActivityCode, Guid OnlineImageQueueID, int ItemCount, int BankID, int LockboxID)
        {
            return Do(() => _SystemService.LogActivity(ActivityCode, OnlineImageQueueID, ItemCount, BankID, LockboxID));
        }

        public BaseResponse SetWebDeliveredFlag(Guid OnlineImageQueueID, bool InternetDelivered)
        {
            return Do(() => _SystemService.SetWebDeliveredFlag(OnlineImageQueueID, InternetDelivered));
        }

        public BaseResponse UpdateActivityLockbox(long ActivityLogID, int BankID, int LockboxID)
        {
            return Do(() => _SystemService.UpdateActivityLockbox(ActivityLogID, BankID, LockboxID));
        }

        public XmlDocumentResponse GetUserLockboxes(int UserID)
        {
            return Do(() => _SystemService.GetUserLockboxes(UserID));
        }

        public XmlDocumentResponse GetBaseDocument()
        {
            return Do(() => _SystemService.GetBaseDocument());
        }
    }
}
