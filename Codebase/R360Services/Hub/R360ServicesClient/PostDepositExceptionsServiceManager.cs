﻿using System;
using System.Collections.Generic;
using System.Configuration;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class PostDepositExceptionsServiceManager : IPostDepositExceptionServices
    {
        private readonly PostDepositExceptionsServiceClient _client;
        private static ltaLog _eventLog;
        private static string _siteKey;
        private const string LogSource = "PostDepositExceptionsServicesClient";
        private static cSiteOptions _siteOptions = null;

        static PostDepositExceptionsServiceManager()
        {
            // Make sure the site key is initialized before any other properties are used
            GetSiteKey();
        }
        public PostDepositExceptionsServiceManager() : this(
            new PostDepositExceptionsServiceClient(_siteKey, EventLog))
        {

        }

        public PostDepositExceptionsServiceManager(PostDepositExceptionsServiceClient decisioningSvcClient)
        {
            _client = decisioningSvcClient;
        }

        private void ValidateConnection()
        {
            if (_client == null)
                throw new Exception("Configuration Error - check log for details");
        }
        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        private static ltaLog EventLog => _eventLog ?? (_eventLog = new ltaLog(SiteOptions.logFilePath,
            SiteOptions.logFileMaxSize,
            (LTAMessageImportance)SiteOptions.loggingDepth));

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        private static cSiteOptions SiteOptions => _siteOptions ?? (_siteOptions = new cSiteOptions(_siteKey));

        private static void GetSiteKey()
        {
            const string defaultValue = "IPOnline";
            try
            {
                string value = ConfigurationManager.AppSettings["siteKey"];
                if (string.IsNullOrEmpty(value))
                    value = defaultValue;

                _siteKey = value;

                EventLog.logEvent($"Using SiteKey = '{value}'", LogSource, LTA.Common.LTAMessageImportance.Verbose);
            }
            catch (Exception ex)
            {
                _siteKey = defaultValue;
                EventLog.logWarning($"Error reading configuration setting 'siteKey' - {ex.Message}", LogSource, LTA.Common.LTAMessageImportance.Verbose);
            }
        }

        public BaseGenericResponse<PostDepositTransactionsResponseDto> GetPendingTransactions(PostDepositPendingTransactionsRequestDto request)
        {
            try
            {
                ValidateConnection();
                return _client.GetPendingTransactions(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error getting pending transactions : " + e.Message, "GetPendingTransactions", LTAMessageType.Error);
                return new BaseGenericResponse<PostDepositTransactionsResponseDto>
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                    Data = null
                };
            }
        }

        public BaseGenericResponse<PostDepositTransactionDetailResponseDto> GetTransaction(
            PostDepositTransactionRequestDto request)
        {
            try
            {
                ValidateConnection();
                return _client.GetTransaction(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error getting Transaction : " + e.Message, "GetTransaction", LTAMessageType.Error);
                return new BaseGenericResponse<PostDepositTransactionDetailResponseDto>
                {
                    Errors = new List<string> {e.Message},
                    Status = StatusCode.FAIL,
                    Data = null
                };
            }
        }
        public BaseGenericResponse<PostDepositTransactionValidationResponseDto> GetTransactionValidationResults(
            PostDepositTransactionDetailResponseDto request)
        {
            try
            {
                ValidateConnection();
                return _client.GetTransactionValidationResults(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error validating Transaction : " + e.Message, "GetTransactionValidationResults",
                    LTAMessageType.Error);
                return new BaseGenericResponse<PostDepositTransactionValidationResponseDto>
                {
                    Errors = new List<string> {e.Message},
                    Status = StatusCode.FAIL,
                    Data = null
                };
            }
        }

        public BaseResponse LockTransaction(LockTransactionRequestDto request)
        {
            try
            {
                ValidateConnection();
                return _client.LockTransaction(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error locking Transaction : " + e.Message, "LockTransaction", LTAMessageType.Error);
                return new BaseResponse
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                };
            }
        }

        public BaseResponse UnlockTransaction(LockTransactionRequestDto request)
        {
            try
            {
                ValidateConnection();
                return _client.UnlockTransaction(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error unlocking Transaction : " + e.Message, "UnlockTransaction", LTAMessageType.Error);
                return new BaseResponse
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                };
            }
        }

        public BaseResponse UnlockTransactionWithoutOverride(LockTransactionRequestDto request)
        {
            try
            {
                ValidateConnection();
                return _client.UnlockTransactionWithoutOverride(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error unlocking Transaction : " + e.Message, "UnlockTransactionWithoutOverride", LTAMessageType.Error);
                return new BaseResponse
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                };
            }
        }

        public BaseGenericResponse<PostDepositSavePayerResultDto> SavePayer(PostDepositSavePayerDto request)
        {
            try
            {
                ValidateConnection();
                return _client.SavePayer(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error saving payer : " + e.Message, nameof(SavePayer),
                    LTAMessageType.Error);
                return new BaseGenericResponse<PostDepositSavePayerResultDto>
                {
                    Status = StatusCode.FAIL,
                    Errors = new List<string> { e.Message },
                };

            }
        }

        public BaseGenericResponse<PostDepositTransactionSaveResultDto> SaveTransaction(PostDepositTransactionSaveDto request)
        {
            try
            {
                ValidateConnection();
                return _client.SaveTransaction(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error saving transaction : " + e.Message, nameof(SaveTransaction),
                    LTAMessageType.Error);
                return new BaseGenericResponse<PostDepositTransactionSaveResultDto>
                {
                    Status = StatusCode.FAIL,
                    Errors = new List<string> {e.Message},
                };
            }
        }

        public BaseResponse AuditTransactionDetailView(PostDepositExceptionTransactionDto request)
        {
            try
            {
                ValidateConnection();
                return _client.AuditTransactionDetailView(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error Auditing Transaction Detail View : " + e.Message, "AuditTransactionDetailView", LTAMessageType.Error);
                return new BaseResponse
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                };
            }
        }

        public BaseGenericResponse<bool> CompleteTransaction(PostDepositTransactionRequestDto request)
        {
            try
            {
                ValidateConnection();
                return _client.CompleteTransaction(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error completing transaction : " + e.Message, nameof(CompleteTransaction),
                    LTAMessageType.Error);
                return new BaseGenericResponse<bool>
                {
                    Status = StatusCode.FAIL,
                    Errors = new List<string> { e.Message },
                };
            }
        }

        public BaseGenericResponse<PayerResponseDto> GetPayerList(PayerLookupRequestDto request)
        {
            try
            {
                ValidateConnection();
                return _client.GetPayerList(request);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error completing transaction : " + e.Message, nameof(CompleteTransaction),
                    LTAMessageType.Error);
                return new BaseGenericResponse<PayerResponseDto>
                {
                    Status = StatusCode.FAIL,
                    Errors = new List<string> { e.Message },
                };
            }
        }
    }
}
