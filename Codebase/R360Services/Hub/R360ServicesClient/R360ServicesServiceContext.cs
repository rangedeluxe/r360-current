﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    [DataContract(Namespace = "urn:spike.R360ServicesServices:v1", Name = "R360ServicesServiceContext")]
    
    class R360ServicesServiceContext
    {

         /// <summary>
        /// Unique name of object stored in headers
        /// </summary>
        internal const string HeaderServiceContext = "R360ServicesServiceContext";

        /// <summary>
        /// Unique name of object stored in headers
        /// </summary>
        internal const string HeaderSiteKey = "SiteKey";
        internal const string HeaderSessionID = "SessionID";

        /// <summary>
        /// namespace of object stored in headers
        /// </summary>
        internal const string HeaderNamespace = "urn:spike.R360ServicesServices:v1";
        
        private string _SessionID = string.Empty;
        private string _SiteKey = string.Empty;

        public R360ServicesServiceContext() { 
        }


        public R360ServicesServiceContext(string vSiteKey, string vSessionID) {
            _SiteKey = vSiteKey;
            _SessionID = vSessionID;
        }

        public R360ServicesServiceContext(string vSiteKey) {
            _SiteKey = vSiteKey;
            _SessionID = string.Empty;
        }
        [DataMember(IsRequired = true, Name = "SessionID")]
        public string SessionID
        {
            get { return _SessionID; }
            set { _SessionID = value; }
        }


        [DataMember(IsRequired = true, Name = "SiteKey")]
        public string SiteKey
        {
            get { return _SiteKey; }
            set { _SiteKey = value; }
        }
         /// <summary>
        /// Gets or sets the current context a service call is running on
        /// </summary>
        /// <remarks>This is set on the client and retrieved on the host</remarks>
        /// <value>The current.</value>
        public static R360ServicesServiceContext Current
        {

            set
            {
                MessageHeader<R360ServicesServiceContext> header = new MessageHeader<R360ServicesServiceContext>(value);
                OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader(R360ServicesServiceContext.HeaderServiceContext, R360ServicesServiceContext.HeaderNamespace));
            }

            get
            {

                string strSiteKey;
                string strSessionID;

                int intIndexOfHeader;

                strSiteKey = string.Empty;
                // determine if servicecontext was sent in headers
                intIndexOfHeader = OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderServiceContext, HeaderNamespace);

                if (intIndexOfHeader == -1)
                {

                    // determine if the name of the source application was sent in the headers
                    intIndexOfHeader = OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderSiteKey, HeaderNamespace);

                    if (intIndexOfHeader == -1)
                    {
                        ////throw new ApplicationException("Site Key not found!"); 
                        strSiteKey = string.Empty;
                    }
                    else
                    {
                        strSiteKey = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(HeaderSiteKey, HeaderNamespace);
                    }

                    // determine if the name of the source application was sent in the headers
                    intIndexOfHeader = OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderSessionID, HeaderNamespace);

                    if (intIndexOfHeader == -1)
                    {
                        ////throw new ApplicationException("SessionID not found!"); 
                        strSessionID = string.Empty;
                    }
                    else
                    {
                        strSessionID = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>(HeaderSessionID, HeaderNamespace);
                    }
                }
                return new R360ServicesServiceContext(strSiteKey);
            }
                          
        }
    }
}
