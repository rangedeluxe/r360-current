﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Ryan Schwantes 
 * * Date: 06/23/2013
 * *
 * * Purpose:  Service Client
 * *
 * * Modification History
 * WI 100422 RS 06/23/2013 Created
 * WI 101855 WJS 7/5/2013   Added CheckUserPermissionsForFunction
 * WI 143459 RDS 05/22/2014	Move binding to config file
* ******************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    internal class R360ServiceClient : ServicesClientBase
    {
        private readonly IR360Services _R360Service = null;

        public R360ServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
            _R360Service = R360ServiceFactory.Create<IR360Services>();
        }

		private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
		{
			// All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
			try
			{
				using (new OperationContextScope((IContextChannel)_R360Service))
				{
					// Set context inside of operation scope
					R360ServiceContext.Init(this.SiteKey);

					return operation();
				}
			}
			catch (FaultException<InvalidSiteKeyFault> ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw (new Exception(ex.Message, ex));
			}
			catch (FaultException<ServerFaultException> ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw (new Exception(ex.Message, ex));
			}
			catch (FaultException ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw (new Exception(ex.Message, ex));
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw;
			}
		}

        public PingResponse Ping()
        {
			return Do(_R360Service.Ping);
        }

        public SummariesResponse GetClientAccountSummary(ReceivablesSummaryRequestDto request)
        {
			return Do(() => _R360Service.GetClientAccountSummary(request));
        }


        public ClientAccountsResponse GetClientAccounts()
        {
			return Do(() => _R360Service.GetClientAccounts());
        }

        public ClientAccountsResponse GetClientAccountsForOrganization(Guid OLOrganizationID)
        {
			return Do(() => _R360Service.GetClientAccountsForOrganization(OLOrganizationID));
        }

        public DataEntryResponseDTO GetDataEntryFieldsForWorkgroup(DataEntrySearchDTO search)
        {
            return Do(() => _R360Service.GetDataEntryFieldsForWorkgroup(search));
        }

        public BaseGenericResponse<long> GetSourceBatchID(long batchID)
        {
            return Do<BaseGenericResponse<long>>(() => _R360Service.GetSourceBatchID(batchID));
        }

        public AdvancedSearchResultsDto GetAdvancedSearch(AdvancedSearchDto req)
        {
            return Do<AdvancedSearchResultsDto>(() => _R360Service.GetAdvancedSearch(req));
        }

        public BatchSummaryResponseDto GetBatchSummary(BatchSummaryRequestDto requestContext)
        {
            return Do<BatchSummaryResponseDto>(() => _R360Service.GetBatchSummary(requestContext));
        }

        public BatchDetailResponseDto GetBatchDetail(BatchDetailRequestDto requestContext)
        {
            return Do<BatchDetailResponseDto>(() => _R360Service.GetBatchDetail(requestContext));
        }

        public TransactionDetailResponseDto GetTransactionDetail(TransactionDetailRequestDto requestContext)
        {
            return Do<TransactionDetailResponseDto>(() => _R360Service.GetTransactionDetail(requestContext));
        }

        public NotificationsResponseDto GetNotificationsResponse(NotificationsRequestDto request)
        {
            return Do<NotificationsResponseDto>(() => _R360Service.GetNotifications(request));
        }

        public NotificationResponseDto GetNotification(NotificationRequestDto request)
        {
            return Do<NotificationResponseDto>(() => _R360Service.GetNotification(request));
        }

        public NotificationsFileTypesResponseDto GetNotificationsFileTypesResponse()
        {
            return Do<NotificationsFileTypesResponseDto>(()=> _R360Service.GetNotificationsFileTypesResponse());
        }

        public PaymentTypesResponseDto GetPaymentTypes()
        {
            return Do<PaymentTypesResponseDto>(() => _R360Service.GetPaymentTypes());
        }

        public PaymentSourcesResponseDto GetPaymentSources()
        {
            return Do<PaymentSourcesResponseDto>(() => _R360Service.GetPaymentSources());
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> SaveStoredQuery(AdvancedSearchStoredQueryDto entity)
        {
            return Do<BaseGenericResponse<AdvancedSearchStoredQueryDto>>(() => _R360Service.SaveStoredQuery(entity));
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> DeleteStoredQuery(Guid entity)
        {
            return Do<BaseGenericResponse<AdvancedSearchStoredQueryDto>>(() => _R360Service.DeleteStoredQuery(entity));
        }

        public BaseGenericResponse<List<AdvancedSearchStoredQueryDto>> GetStoredQueries()
        {
            return Do<BaseGenericResponse<List<AdvancedSearchStoredQueryDto>>>(() => _R360Service.GetStoredQueries());
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> GetStoredQuery(Guid id)
        {
            return Do<BaseGenericResponse<AdvancedSearchStoredQueryDto>>(() => _R360Service.GetStoredQuery(id));
        }

        public PaymentSearchResponseDto GetPaymentSearch(PaymentSearchRequestDto request)
        {
            return Do<PaymentSearchResponseDto>(() => _R360Service.GetPaymentSearch(request));
        }

        public InvoiceSearchResponseDto GetInvoiceSearch(InvoiceSearchRequestDto request)
        {
            return Do<InvoiceSearchResponseDto>(() => _R360Service.GetInvoiceSearch(request));
        }

        internal ExceptionTypeSummaryResponse GetExceptionTypeSummary(EntityDTO entity, WorkgroupIDsDTO workgroup, DateTime datefrom, DateTime dateto)
        {
			return Do(() => _R360Service.GetExceptionTypeSummary(entity, workgroup, datefrom, dateto));
        }

        internal PermissionsResponse CheckUserPermissionsForFunction(string function, string mode)
        {
            return Do(() => _R360Service.CheckUserPermissionsForFunction(function, mode));
        }

		internal BaseResponse WriteAuditEvent(string eventName, string eventType, string applicationName, string description)
		{
			return Do(() => _R360Service.WriteAuditEvent(eventName, eventType, applicationName, description));
		}

		internal BaseGenericResponse<List<WorkgroupNameDTO>> GetActiveWorkgroups(EntityDTO entity)
		{
			return Do<BaseGenericResponse<List<WorkgroupNameDTO>>>(() => _R360Service.GetActiveWorkgroups(entity));
		}

        internal BaseGenericResponse<List<WorkgroupNameDTO>> GetIntegraPayOnlyWorkgroups(EntityDTO entity)
        {
            return Do<BaseGenericResponse<List<WorkgroupNameDTO>>>(() => _R360Service.GetIntegraPayOnlyWorkgroups(entity));
        }

        internal BaseGenericResponse<List<AuditUserDTO>> GetAuditUsers()
        {
            return Do<BaseGenericResponse<List<AuditUserDTO>>>(() => _R360Service.GetAuditUsers());
        }

        internal BaseGenericResponse<string> GetBankName(int siteBankId) {
            return Do<BaseGenericResponse<string>>(() => _R360Service.GetBankName(siteBankId));
        }

        internal BaseGenericResponse<List<DDASummaryDataDto>> GetDDASummary(DateTime depositDate)
        {
            return Do<BaseGenericResponse<List<DDASummaryDataDto>>>(() => _R360Service.GetDDASummary(depositDate));
        }

    }
}
