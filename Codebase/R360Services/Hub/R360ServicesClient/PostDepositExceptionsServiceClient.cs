﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using WFS.LTA.Common;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class PostDepositExceptionsServiceClient : ServicesClientBase, IPostDepositExceptionServices
    {
        private readonly IPostDepositExceptionServices _exceptionServices;

        public PostDepositExceptionsServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
            if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(GetType().Name);
            _exceptionServices = R360ServiceFactory.Create<IPostDepositExceptionServices>();
        }

        public BaseGenericResponse<PostDepositTransactionsResponseDto> GetPendingTransactions(PostDepositPendingTransactionsRequestDto request)
        {
            return Do(() => _exceptionServices.GetPendingTransactions(request));
        }

        public BaseGenericResponse<PostDepositTransactionDetailResponseDto> GetTransaction(
            PostDepositTransactionRequestDto request)
        {
            return Do(() => _exceptionServices.GetTransaction(request));
        }

        public BaseGenericResponse<PostDepositTransactionValidationResponseDto> GetTransactionValidationResults(
            PostDepositTransactionDetailResponseDto request)
        {
            return Do(() => _exceptionServices.GetTransactionValidationResults(request));
        }

        public BaseResponse LockTransaction(LockTransactionRequestDto request)
        {
            return Do(() => _exceptionServices.LockTransaction(request));
        }

        public BaseResponse UnlockTransaction(LockTransactionRequestDto request)
        {
            return Do(() => _exceptionServices.UnlockTransaction(request));
        }

        public BaseResponse UnlockTransactionWithoutOverride(LockTransactionRequestDto request)
        {
            return Do(() => _exceptionServices.UnlockTransactionWithoutOverride(request));
        }

        public BaseGenericResponse<PostDepositSavePayerResultDto> SavePayer(PostDepositSavePayerDto request)
        {
            return Do(() => _exceptionServices.SavePayer(request));
        }

        public BaseGenericResponse<PostDepositTransactionSaveResultDto> SaveTransaction(PostDepositTransactionSaveDto request)
        {
            return Do(() => _exceptionServices.SaveTransaction(request));
        }

        public BaseResponse AuditTransactionDetailView(PostDepositExceptionTransactionDto request)
        {
            return Do(() => _exceptionServices.AuditTransactionDetailView(request));
        }

        public BaseGenericResponse<bool> CompleteTransaction(PostDepositTransactionRequestDto request)
        {
            return Do(() => _exceptionServices.CompleteTransaction(request));
        }

        private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
        {
            // All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
            try
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                using (new OperationContextScope((IContextChannel) _exceptionServices))
                {
                    // Set context inside of operation scope
                    R360ServiceContext.Init(SiteKey);
                    return operation();
                }
            }
            catch (FaultException<InvalidSiteKeyFault> ex)
            {
                EventLog.logError(ex, GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name, methodName);
                throw;
            }
        }

        public BaseGenericResponse<PayerResponseDto> GetPayerList(PayerLookupRequestDto request)
        {
            return Do(() => _exceptionServices.GetPayerList(request));
        }
    }
}
