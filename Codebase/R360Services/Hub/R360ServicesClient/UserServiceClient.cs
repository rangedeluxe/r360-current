﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class UserServiceClient : ServicesClientBase, IUserServices
    {
        private readonly IUserServices _UserService = null;

        public UserServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
			_UserService = R360ServiceFactory.Create<IUserServices>();
        }

        private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
        {
            // All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
            try
            {
                using (new OperationContextScope((IContextChannel)_UserService))
                {
                    // Set context inside of operation scope
                    R360ServiceContext.Init(this.SiteKey);

                    return operation();
                }
            }
            catch (FaultException<InvalidSiteKeyFault> ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw;
            }
        }

        public PingResponse Ping()
        {
            return Do(_UserService.Ping);
        }

        public BaseGenericResponse<cUserRAAM> GetUserByID(int UserID)
        {
            return Do(() => _UserService.GetUserByID(UserID));
        }

        public XmlDocumentResponse GetOnlineUserByID(int UserID)
        {
            return Do(() => _UserService.GetOnlineUserByID(UserID));
        }

        public XmlDocumentResponse GetOLUserPreferences()
        {
            return Do(() => _UserService.GetOLUserPreferences());
        }

        public XmlDocumentResponse SetOLUserPreferences(XmlDocument Parms)
        {
            return Do(() => _UserService.SetOLUserPreferences(Parms));
        }

        public XmlDocumentResponse RestoreDefaultOLPreferences()
        {
            return Do(() => _UserService.RestoreDefaultOLPreferences());
        }

        public BaseGenericResponse<UserPreferencesDTO> GetUserPreferences()
        {
            return Do(()=> _UserService.GetUserPreferences());
        }
        
        public BaseGenericResponse<bool> SetUserPreferences(UserPreferencesDTO Preferences)
        {
            return Do(() => _UserService.SetUserPreferences(Preferences));
        }

        public BaseGenericResponse<UserPreferencesDTO> RestoreDefaultPreferences()
        {
            return Do(() => _UserService.RestoreDefaultPreferences());
        }

        public BaseGenericResponse<cOLLockboxes> GetUserLockboxes()
        {
            return Do(() => _UserService.GetUserLockboxes());
        }
    }
}
