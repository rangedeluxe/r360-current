﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using WFS.LTA.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class ExceptionsServiceClient : ServicesClientBase, IExceptionServices
    {
        private readonly IExceptionServices _exceptionServices = null;

        public ExceptionsServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
            if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(GetType().Name);
            _exceptionServices = R360ServiceFactory.Create<IExceptionServices>();
        }

        public OLDBaseGenericResponse<string> Ping()
        {
            return Do(() => _exceptionServices.Ping());
        }

        public PendingBatchesResponse GetPendingBatches()
        {
            return Do(() => _exceptionServices.GetPendingBatches());
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            return Do(() => _exceptionServices.GetBatches(bankId, workgroupId));
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            return Do(()=> _exceptionServices.GetBatchDetails(globalBatchId));
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            return Do(() => _exceptionServices.GetBatch(globalBatchId));
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            return Do(() => _exceptionServices.GetTransactionDetails(globalBatchId, transactionid));
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            return Do(() => _exceptionServices.CheckOutBatch(globalBatchId, sid, firstName, lastName, login));
        }

        public OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId)
        {
            return Do(() => _exceptionServices.ResetBatch(globalBatchId));
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            return Do(() => _exceptionServices.UpdateTransaction(dto));
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            return Do(() => _exceptionServices.CompleteBatch(dto));
        }

        private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
        {
            // All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
            try
            {
                using (new OperationContextScope((IContextChannel)_exceptionServices))
                {
                    // Set context inside of operation scope
                    R360ServiceContext.Init(SiteKey);
                    return operation();
                }
            }
            catch (FaultException<InvalidSiteKeyFault> ex)
            {
                EventLog.logError(ex, GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name, methodName);
                throw;
            }
        }
    }
}
