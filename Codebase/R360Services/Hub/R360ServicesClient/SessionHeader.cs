﻿using System;
using System.ServiceModel.Channels;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
	public class SessionHeader
	{
		/// <remarks/>
		public System.Guid SessionID { get; set; }

		/// <remarks/>
		public string SiteKey { get; set; }

		/// <remarks/>
		public string[] RAAMClaims  { get; set; }
	}

	public class CustomSoapHeader : MessageHeader
	{
		private readonly string _headerName;
		public string HeaderValue { get; private set; }

		public CustomSoapHeader(string headerName, string headerValue)
		{
			HeaderValue = headerValue;
			_headerName = headerName;
		}

		public override string Name
		{
			get { return _headerName; }
		}

		public override string Namespace
		{
			get { return "http://tempuri.org/"; }
		}

		protected override void OnWriteHeaderContents(System.Xml.XmlDictionaryWriter writer, MessageVersion messageVersion)
		{
			// Write the content of the header directly using the XmlDictionaryWriter
			writer.WriteElementString("Key", HeaderValue);
		}
	}
}
