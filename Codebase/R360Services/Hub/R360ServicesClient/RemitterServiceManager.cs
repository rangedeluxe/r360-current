﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class RemitterServiceManager : IRemitterServices
    {
        #region Construction / Initialization

        // Initialization parameters
        private const string LogSource = "RemitterServiceClient";
        private static ltaLog _eventLog;
        private static string _siteKey;
        private static cSiteOptions _SiteOptions = null;
        private readonly RemitterServiceClient _client;

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected static cSiteOptions SiteOptions
        {
            get
            {
                if (_SiteOptions == null)
                {
                    _SiteOptions = new cSiteOptions(_siteKey);
                }
                return _SiteOptions;
            }
        }
        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected static ltaLog EventLog
        {
            get
            {
                if (_eventLog == null)
                {
                    _eventLog = new ltaLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (LTAMessageImportance)SiteOptions.loggingDepth);
                }

                return _eventLog;
            }
        }

        protected static string SiteKey
        {
            get
            {
                return (_siteKey);
            }
            set
            {
                _siteKey = value;
            }
        }

        static RemitterServiceManager()
        {
            // Make sure the site key is initialized before any other properties are used
            GetSiteKey();
        }

        public RemitterServiceManager()
        {
            // Create connection context
            try
            {
                _client = new RemitterServiceClient(_siteKey, EventLog);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, LogSource, ".ctor");

                // Consructors should not throw exceptions -- just initialize to null and report errors later...
                _client = null;
            }
        }

        // Specialied method for site key that properly initializes the EventLog after the site key has been set
        private static void GetSiteKey()
        {
            string defaultValue = "IPOnline";
            try
            {
                string value = ConfigurationManager.AppSettings["siteKey"];
                if (string.IsNullOrEmpty(value))
                    value = defaultValue;

                _siteKey = value;

                EventLog.logEvent(string.Format("Using SiteKey = '{0}'", value), LogSource, LTA.Common.LTAMessageImportance.Verbose);
            }
            catch (Exception ex)
            {
                _siteKey = defaultValue;

                EventLog.logWarning(string.Format("Error reading configuration setting 'siteKey' - {0}", ex.Message), LogSource, LTA.Common.LTAMessageImportance.Verbose);
            }
        }

        private void ValidateConnection()
        {
            if (_client == null)
                throw new Exception("Configuration Error - check log for details");
        }

        #endregion

        public PingResponse Ping()
        {
            ValidateConnection();
            return _client.Ping();
        }

        public XmlDocumentResponse GetRemitterMaintenancePage(Guid LockboxRemitterID)
        {
            ValidateConnection();
            return _client.GetRemitterMaintenancePage(LockboxRemitterID);
        }

        public XmlDocumentResponse GetAllRemitters(int StartRecord, bool ShowAll)
        {
            ValidateConnection();
            return _client.GetAllRemitters(StartRecord, ShowAll);
        }

        public BaseGenericResponse<cRemitter> GetLockboxRemitter(string RoutingNumber, string AccountNumber, Guid OLLockboxID)
        {
            ValidateConnection();
            return _client.GetLockboxRemitter(RoutingNumber, AccountNumber, OLLockboxID);
        }

        public BaseGenericResponse<cRemitter> GetLockboxRemitterByID(Guid LockboxRemitterID)
        {
            ValidateConnection();
            return _client.GetLockboxRemitterByID(LockboxRemitterID);
        }

        public BaseResponse LockboxRemitterExists(string RoutingNumber, string AccountNumber, Guid OLLockboxID)
        {
            ValidateConnection();
            return _client.LockboxRemitterExists(RoutingNumber, AccountNumber, OLLockboxID);
        }

        public BaseResponse UpdateChecksRemitter(string LockboxRemitterName, int BankID, int LockboxID, int BatchID, DateTime DepositDate, int TransactionID, int BatchSequence)
        {
            ValidateConnection();
            return _client.UpdateChecksRemitter(LockboxRemitterName, BankID, LockboxID, BatchID, DepositDate, TransactionID, BatchSequence);
        }
    }
}
