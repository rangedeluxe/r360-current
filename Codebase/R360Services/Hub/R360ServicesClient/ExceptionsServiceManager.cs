﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using OLDecisioningShared.DTO;
using OLDecisioningShared.Responses;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class ExceptionsServiceManager : IExceptionServices
    {
        private readonly ExceptionsServiceClient _client;
        private static ltaLog _eventLog;
        private static string _siteKey;
        private const string LogSource = "R360ServicesClient";
        private static cSiteOptions _siteOptions = null;
        private readonly IRaamClient _raamClient;
        private readonly IUserServices _userServiceClient;

        static ExceptionsServiceManager()
        {
            // Make sure the site key is initialized before any other properties are used
            GetSiteKey();
        }
        public ExceptionsServiceManager() : this(
            new ExceptionsServiceClient(_siteKey, EventLog),
            new UserServiceClient(_siteKey, _eventLog),
            new RaamClient(new ConfigHelpers.Logger(x => 
            EventLog.logEvent(x, "", LTAMessageType.Information, LTAMessageImportance.Verbose), 
                x => EventLog.logEvent(x, "", LTAMessageType.Warning, LTAMessageImportance.Essential))))
        {

        }

        public ExceptionsServiceManager(ExceptionsServiceClient decisioningSvcClient, 
            IUserServices userServiceClient, IRaamClient raamClient)
        {
            _client = decisioningSvcClient;
            _userServiceClient = userServiceClient;
            _raamClient = raamClient;
        }

        public OLDBaseGenericResponse<string> Ping()
        {
            ValidateConnection();
            return _client.Ping();
        }

        public PendingBatchesResponse GetPendingBatches()
        {
            try
            {
                ValidateConnection();
                return _client.GetPendingBatches();
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error getting batches" + e.Message, "GetPendingBatches", LTAMessageType.Error);
                return new PendingBatchesResponse { Errors = new List<string> { e.Message },
                    Batches = null, Status = StatusCode.FAIL };
            }
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            try
            {
                ValidateConnection();
                return _client.GetBatches(bankId, workgroupId);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error getting batches" + e.Message, "GetBatchDetails", LTAMessageType.Error);
                return new PendingBatchesResponse
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                    Batches = null
                };
            }
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            try
            {
                ValidateConnection();
                return _client.GetBatchDetails(globalBatchId);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error getting batches" + e.Message, "GetBatchDetails", LTAMessageType.Error);
                return new OLDBaseGenericResponse<DecisioningBatchDto>
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                    Data = null
                };
            }
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            try
            {
                ValidateConnection();
                return _client.GetBatch(globalBatchId);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error getting batch " + e.Message, "GetBatch", LTAMessageType.Error);
                return new OLDBaseGenericResponse<DecisioningBatchDto>
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                    Data = null
                };
            }
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            try
            {
                ValidateConnection();
                return _client.GetTransactionDetails(globalBatchId, transactionid);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error getting batches" + e.Message, "GetBatchDetails", LTAMessageType.Error);
                return new OLDBaseGenericResponse<DecisioningTransactionDto>
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                    Data = null
                };
            }
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            try
            {
                ValidateConnection();
                return _client.CheckOutBatch(globalBatchId, sid, firstName, lastName, login);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error locking a batch." + e.Message, "CheckOutBatch", LTAMessageType.Error);
                return new OLDBaseGenericResponse<bool>
                {
                    Errors = new List<string> { e.Message },
                    Status = StatusCode.FAIL,
                    Data = false
                };
            }
        }

        public OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId)
        {
            ValidateConnection();
            return _client.ResetBatch(globalBatchId);
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            try
            {
                ValidateConnection();
                return _client.UpdateTransaction(dto);
            }
            catch (Exception e)
            {
                EventLog.logEvent("Error updating transaction." + e.Message, "GetPendingBatches", LTAMessageType.Error);
                return new UpdateTransactionResponse { Errors = new List<string> { e.Message }, Status = StatusCode.FAIL, Data = false };
            }
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            ValidateConnection();
            return _client.CompleteBatch(dto);
        }

        private void ValidateConnection()
        {
            if (_client == null)
                throw new Exception("Configuration Error - check log for details");
        }
        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        private static ltaLog EventLog => _eventLog ?? (_eventLog = new ltaLog(SiteOptions.logFilePath,
            SiteOptions.logFileMaxSize,
            (LTAMessageImportance)SiteOptions.loggingDepth));

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        private static cSiteOptions SiteOptions => _siteOptions ?? (_siteOptions = new cSiteOptions(_siteKey));

        private static void GetSiteKey()
        {
            const string defaultValue = "IPOnline";
            try
            {
                string value = ConfigurationManager.AppSettings["siteKey"];
                if (string.IsNullOrEmpty(value))
                    value = defaultValue;

                _siteKey = value;

                EventLog.logEvent($"Using SiteKey = '{value}'", LogSource, LTA.Common.LTAMessageImportance.Verbose);
            }
            catch (Exception ex)
            {
                _siteKey = defaultValue;
                EventLog.logWarning($"Error reading configuration setting 'siteKey' - {ex.Message}", LogSource, LTA.Common.LTAMessageImportance.Verbose);
            }
        }


    }
}
