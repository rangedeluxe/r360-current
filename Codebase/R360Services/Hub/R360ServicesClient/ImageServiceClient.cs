﻿using System;
using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Xml;

using WFS.LTA.Common;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class ImageServiceClient : ServicesClientBase, IImageServices
    {
        private readonly IImageServices _ImageService = null;

        public ImageServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
			_ImageService = R360ServiceFactory.Create<IImageServices>();
        }

        private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
        {
            // All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
            try
            {
                using (new OperationContextScope((IContextChannel)_ImageService))
                {
                    // Set context inside of operation scope
                    R360ServiceContext.Init(this.SiteKey);

                    return operation();
                }
            }
            catch (FaultException<InvalidSiteKeyFault> ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw;
            }
        }

        public PingResponse Ping()
        {
            return Do(_ImageService.Ping);
        }

        public BaseGenericResponse<Stream> GetImageJobFileAsAttachment(Guid OnlineImageQueueID)
        {
            return Do(() => _ImageService.GetImageJobFileAsAttachment(OnlineImageQueueID));
        }

        public BaseGenericResponse<byte[]> GetImageJobFileAsBuffered(Guid OnlineImageQueueID)
        {
            return Do(() => _ImageService.GetImageJobFileAsBuffered(OnlineImageQueueID));
        }

        public BaseGenericResponse<DataSet> CreateViewAllJobRequest(XmlDocument Parms)
        {
            return Do(() => _ImageService.CreateViewAllJobRequest(Parms));
        }

        public BaseGenericResponse<DataSet> CheckImageJobStatus(Guid OnlineImageQueueID)
        {
            return Do(() => _ImageService.CheckImageJobStatus(OnlineImageQueueID));
        }

        public BaseGenericResponse<DataSet> CreateSearchResultsJobRequest(XmlDocument SearchParms, XmlDocument JobParms)
        {
            return Do(() => _ImageService.CreateSearchResultsJobRequest(SearchParms, JobParms));
        }

        public BaseGenericResponse<DataSet> CreateViewSpecifiedJobRequest(XmlDocument Parms)
        {
            return Do(() => _ImageService.CreateViewSpecifiedJobRequest(Parms));
        }

        public BaseGenericResponse<DataSet> CreateViewSelectedJobRequest(XmlDocument Parms)
        {
            return Do(() => _ImageService.CreateViewSelectedJobRequest(Parms));
        }
        public BaseGenericResponse<DataSet> CreateInProcessExceptionJobRequest(InProcessExceptionImageRequestDto request)
        {
            return Do(() => _ImageService.CreateInProcessExceptionJobRequest(request));
        }
    }
}
