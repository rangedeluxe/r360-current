﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class RemitterServiceClient : ServicesClientBase, IRemitterServices
    {
        private readonly IRemitterServices _RemitterService = null;

        public RemitterServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
			_RemitterService = R360ServiceFactory.Create<IRemitterServices>();
        }

        private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
        {
            // All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
            try
            {
                using (new OperationContextScope((IContextChannel)_RemitterService))
                {
                    // Set context inside of operation scope
                    R360ServiceContext.Init(this.SiteKey);

                    return operation();
                }
            }
            catch (FaultException<InvalidSiteKeyFault> ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException<ServerFaultException> ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (FaultException ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw (new Exception(ex.Message, ex));
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw;
            }
        }

        public PingResponse Ping()
        {
            return Do(_RemitterService.Ping);
        }

        public XmlDocumentResponse GetRemitterMaintenancePage(Guid LockboxRemitterID)
        {
            return Do(() => _RemitterService.GetRemitterMaintenancePage(LockboxRemitterID));
        }

        public XmlDocumentResponse GetAllRemitters(int StartRecord, bool ShowAll)
        {
            return Do(() => _RemitterService.GetAllRemitters(StartRecord, ShowAll));
        }

        public BaseGenericResponse<cRemitter> GetLockboxRemitter(string RoutingNumber, string AccountNumber, Guid OLLockboxID)
        {
            return Do(() => _RemitterService.GetLockboxRemitter(RoutingNumber, AccountNumber, OLLockboxID));
        }

        public BaseGenericResponse<cRemitter> GetLockboxRemitterByID(Guid LockboxRemitterID)
        {
            return Do(() => _RemitterService.GetLockboxRemitterByID(LockboxRemitterID));
        }

        public BaseResponse LockboxRemitterExists(string RoutingNumber, string AccountNumber, Guid OLLockboxID)
        {
            return Do(() => _RemitterService.LockboxRemitterExists(RoutingNumber, AccountNumber, OLLockboxID));
        }

        public BaseResponse UpdateChecksRemitter(string LockboxRemitterName, int BankID, int LockboxID, int BatchID, DateTime DepositDate, int TransactionID, int BatchSequence)
        {
            return Do(() => _RemitterService.UpdateChecksRemitter(LockboxRemitterName, BankID, LockboxID, BatchID, DepositDate, TransactionID, BatchSequence));
        }
    }
}
