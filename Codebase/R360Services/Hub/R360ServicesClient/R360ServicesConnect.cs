﻿using System;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Xml;
using WFS.LTA.Common;
using WFS.RecHub.R360Services.Common;
namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    public class InvalidSiteKeyException : Exception
    {

        /// <summary></summary>
        public override string Message
        {
            get
            {
                return "Invalid Site Key";
            }
        }
    }
    public class R360ServicesConnect : _ServicesClientBase
    {
        [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
        private WS2007HttpBinding _Binding = new WS2007HttpBinding();
        private EndpointAddress _Endpointaddress = null;
        private iR360Services _R360ServicesService = null;
        private Guid _gidSessionID = Guid.Empty;
        private const long MAXBUFFERSIZE = 32000000;
        // Define a property to associate with all method calls as a SOAP Header value
        public SessionHeader SessionHeaderValue { get; private set; }

        public R360ServicesConnect(string vSiteKey, string strServerNameLocation, ltaLog log) : base(vSiteKey, log) {

             SessionHeaderValue = new SessionHeader();
             SessionHeaderValue.SiteKey = vSiteKey;
            _Binding.MaxReceivedMessageSize = 67108864;
            _Binding.MessageEncoding = WSMessageEncoding.Mtom;
            _Binding.ReaderQuotas.MaxArrayLength = Int32.MaxValue;
            _Binding.ReaderQuotas.MaxBytesPerRead = Int32.MaxValue;
            _Binding.ReaderQuotas.MaxDepth = Int32.MaxValue;
            _Binding.ReaderQuotas.MaxNameTableCharCount = Int32.MaxValue;
            _Binding.ReaderQuotas.MaxStringContentLength = Int32.MaxValue;

            _Binding.Security.Mode = SecurityMode.None;

            SetEndPointAddress(strServerNameLocation);
            CreateChannel();
        }
        private void SetEndPointAddress(string strServerNameLocation)
        {
            if (strServerNameLocation.StartsWith("https", StringComparison.CurrentCultureIgnoreCase))
                _Binding.Security.Mode = SecurityMode.Transport;
            else
                _Binding.Security.Mode = SecurityMode.None;
            
            _Endpointaddress = new EndpointAddress(strServerNameLocation);
        }
        private void CreateChannel()
        {
            _R360ServicesService = new ChannelFactory<iR360Services>(_Binding, _Endpointaddress).CreateChannel();
        }
        public Guid GIDSessionID
        {
            get
            {
                return _gidSessionID;
            }
            set
            {
                _gidSessionID = value;
            }
        }

      public cPingResponse Ping()
      {

            try
            {
                using (new OperationContextScope((IContextChannel)_R360ServicesService))
                {
                    return _R360ServicesService.Ping();
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "Ping");
                throw;
            }
        }
      public GetSessionResponse GetSession(string[] RAAMUserClaims)
      {
          GetSessionResponse response = new GetSessionResponse();
          try
          {
              using (new OperationContextScope((IContextChannel)_R360ServicesService))
              {
                  GetSessionRequest request = new GetSessionRequest();
                  request.RAAMUserClaims = RAAMUserClaims;
                  response =  _R360ServicesService.GetSession(request);
                  int j = 5;
              }
          }
          catch (Exception ex)
          {
              EventLog.logError(ex, this.GetType().Name, "GetSession");
              throw;
          }
          return response;
      }
      public cSummariesResponse GetClientAccountSummary(DateTime summaryDate)
      {

          try
          {
              Guid securityID = new Guid();
              securityID = Guid.NewGuid();
              using (new OperationContextScope((IContextChannel)_R360ServicesService))
              {
                  //cR360ServiceContext.Current = new cR360ServiceContext(securityID);

                  return _R360ServicesService.GetClientAccountSummary(summaryDate);
              }
          }
          catch (Exception ex)
          {
              EventLog.logError(ex, this.GetType().Name, "GetClientAccountSummary");
              throw;
          }
      }

    }
}
