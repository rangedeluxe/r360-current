﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
	internal interface ISessionHeaderValueProvider
	{
		SessionHeader SessionHeaderValue { get; }
	}

	internal class SoapHeaderBehavior : IEndpointBehavior
	{
		private readonly ISessionHeaderValueProvider _headerProvider;
		public SoapHeaderBehavior(ISessionHeaderValueProvider headerProvider)
		{
			_headerProvider = headerProvider;
		}

		public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
		{
		}

		public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
		{
			clientRuntime.ClientMessageInspectors.Add(new SoapHeaderMessageInspector(_headerProvider));
		}

		public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
		{
		}

		public void Validate(ServiceEndpoint endpoint)
		{
		}
	}

	internal class SoapHeaderMessageInspector : IClientMessageInspector
	{
		private readonly ISessionHeaderValueProvider _headerProvider;
		public SoapHeaderMessageInspector(ISessionHeaderValueProvider headerProvider)
		{
			_headerProvider = headerProvider;
		}

		public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
		{
			// No action
		}

		public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
		{
			// Apply SOAP headers to the request

			// Prepare the request message copy to be modified
			MessageBuffer buffer = request.CreateBufferedCopy(Int32.MaxValue);
			request = buffer.CreateMessage();

			// Add each header value
			var headers = _headerProvider.SessionHeaderValue;
			request.Headers.Add(new CustomSoapHeader("SiteKey", headers.SiteKey));
			if (headers.SessionID != Guid.Empty)
				request.Headers.Add(new CustomSoapHeader("SessionID", headers.SessionID.ToString()));
			if (headers.RAAMClaims != null )
                request.Headers.Add(new CustomSoapHeader("RAAMClaims", headers.RAAMClaims.ToString()));

			return null;
		}
	}
}
