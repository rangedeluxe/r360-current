﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.R360ServicesServicesClient
{
    internal class R360ServiceClientEx : iR360ServicesClient, ISessionHeaderValueProvider
	{
		// Define a property to associate with all method calls as a SOAP Header value
		public SessionHeader SessionHeaderValue { get; private set; }

		/// <summary>
		/// Use this constructor to properly intialize the Header information for the session, and setup the connection based on the SiteId configuration
		/// </summary>
		/// <param name="sessionHeader"></param>
		public R360ServiceClientEx(SessionHeader sessionHeader)
			: this(GetAddressForSession(sessionHeader))
		{
			SessionHeaderValue = sessionHeader;
		}

		// Helper constructor to delegate to generated code
		private R360ServiceClientEx(EndpointAddress address)
			: base(GetBindingForSession(address), address)
		{
			this.Endpoint.EndpointBehaviors.Add(new HttpHeaderBehavior(this));
		}

		// Create the Binding in code; no configuration file
		private static BasicHttpBinding GetBindingForSession(EndpointAddress address)
		{
			BasicHttpBinding bhbBinding = new BasicHttpBinding();

			bhbBinding.MaxReceivedMessageSize = 2147483647;
			bhbBinding.MessageEncoding = WSMessageEncoding.Mtom;
			bhbBinding.TransferMode = TransferMode.Streamed;
			bhbBinding.ReaderQuotas.MaxArrayLength = 2147483647;
			bhbBinding.ReaderQuotas.MaxBytesPerRead = 2147483647;
			bhbBinding.ReaderQuotas.MaxDepth = 2147483647;
			bhbBinding.ReaderQuotas.MaxNameTableCharCount = 2147483647;
			bhbBinding.ReaderQuotas.MaxStringContentLength = 2147483647;
			if (string.Compare(address.Uri.Scheme, Uri.UriSchemeHttps, StringComparison.OrdinalIgnoreCase) == 0)
			{
				bhbBinding.Security.Mode = BasicHttpSecurityMode.Transport;
			}
			else
			{
				bhbBinding.Security.Mode = BasicHttpSecurityMode.None;
			}

			return bhbBinding;
		}
        /*
        private void SetEndPointAddress(string strServerNameLocation)
        {
            if (strServerNameLocation.StartsWith("https", StringComparison.CurrentCultureIgnoreCase))
                _Binding.Security.Mode = SecurityMode.Transport;
            else
                _Binding.Security.Mode = SecurityMode.None;

            _Endpointaddress = new EndpointAddress(strServerNameLocation);
        }*/
        private static EndpointAddress GetAddressForSession(SessionHeader endPointAddress)
		{
			// TODO: load this address based on the sessionHeader SiteIdKey...
            var address = new EndpointAddress("http://localhost:8080/R360Services/R360Services.svc");
			return address;

			/*
						string strServerNameLocation;

						try{
							strServerNameLocation = ipoINILib.IniReadValue("OnlineFileServices", "ServerLocation");
						}catch(Exception ex){
							EventLog.logError(ex,this.GetType().Name,"OLFOnlineClient");
							strServerNameLocation = string.Empty;
						}
            
						endpointaddress = new EndpointAddress(strServerNameLocation);  
			 */
		}
	}
}
