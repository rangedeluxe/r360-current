﻿using System;
using System.Data;

namespace WFS.RecHub.DAL
{
    public interface IUserDal : IDisposable
    {
        cError GetLastError { get; }

        void BeginTrans();
        void CommitTrans();
        void RollbackTrans();

        bool GetOLUserPreferences(int UserID, out DataTable dt);
        bool GetOLUserPreferenceIDByName(string PreferenceName, out DataTable dt);
        bool DeleteOLUserPreference(int UserID, out int RowsReturned);
        bool UpdateOLUserPreference(Guid OLPreferenceID, int UserID, string UserSetting, out int RowsReturned);
        bool InsertOLUserPreference(Guid OLPreferenceID, int UserID, string UserSetting, out int RowsReturned);
        bool GetOLUserPreferenceCount(Guid OLPreferenceID, int UserID, out DataTable dt);
        bool GetUser(int UserID, out DataTable dt);
    }
}
