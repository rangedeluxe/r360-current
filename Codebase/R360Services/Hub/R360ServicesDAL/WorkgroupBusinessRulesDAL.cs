﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace WFS.RecHub.DAL
{
    public class WorkgroupBusinessRulesDAL : _DALBase, IRuleProvider
    {
        public WorkgroupBusinessRulesDAL(string siteKey) : base(siteKey)
        {
        }
        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey)
        {
            var rules = new Rules();
            DataTable dt;
            try
            {
                var parms = new[] {
                    BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId, ParameterDirection.Input),
                    BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteWorkgroupId, ParameterDirection.Input)
                };
                if (Database.executeProcedure("RecHubData.usp_dimWorkgroupBusinessRules_Get", parms, out dt))
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        bool required = !string.IsNullOrEmpty(row["PostDepositPayerRequired"].ToString()) && row["PostDepositPayerRequired"].ToString().ToLower() == "true";
                        rules.DataEntryFieldRules.Add(new DataEntryFieldRule()
                        {
                            IsCheck =true,
                            FieldName = row["FieldName"].ToString(),
                            IsRequired = required
                        });
                    }
                }


            }
            catch (Exception e)
            {
                EventLog.logEvent($"An error occurred while executing RecHubData.usp_dimWorkgroupBusinessRules_Get: {e.Message}",
                    this.GetType().Name,
                    MessageType.Error,
                    MessageImportance.Essential);
            }
            return rules;
        }
    }
}
