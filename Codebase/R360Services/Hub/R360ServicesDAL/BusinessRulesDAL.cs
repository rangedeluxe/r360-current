﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders {
    public class BusinessRulesDal : _DALBase, IRuleProvider {
        public BusinessRulesDal(string siteKey) : base(siteKey) {
        }
        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey) {
            var rules = new Rules();
            DataTable dt;
            try {
                var parms = new[] {
                    BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId, ParameterDirection.Input),
                    BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteWorkgroupId, ParameterDirection.Input),
                    BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batchSourceKey, ParameterDirection.Input)
                };
                if (Database.executeProcedure("RecHubData.usp_dimWorkgroupDataEntryColumns_Get_BusinessRules", parms, out dt)) {
                    foreach (DataRow row in dt.Rows) {
                        rules.DataEntryFieldRules.Add(new DataEntryFieldRule() {
                            IsCheck = (bool)row["IsCheck"],
                            FieldName = row["FieldName"].ToString(),
                            IsRequired = (bool)row["IsRequired"]
                        });
                    }
                }
            }
            catch (Exception ex) {
                EventLog.logEvent($"An error occurred while executing RecHubData.usp_dimWorkgroupDataEntryColumns_Get_BusinessRules: {ex.Message}", 
                    this.GetType().Name, 
                    MessageType.Error, 
                    MessageImportance.Essential);
            }
            return rules;
        }
    }
}
