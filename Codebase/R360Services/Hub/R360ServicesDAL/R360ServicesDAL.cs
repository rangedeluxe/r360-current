﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/23/2013
 * *
 * * Purpose: Provides the connection from the R360Services to the database
 * *
 * * Modification History
 * WI 100422 CEJ 05/23/2013 Created
 * WI 101786 DRP 06/24/2013 
 * -  Added GetExceptionSummariesByAccount
 * WI 110739 TWE 08/07/2013
 *    Return description column for display
 * WI 121918 CMC 11/08/2013
 *    Call new RecHubData.usp_GetPaymentTypeTotals stored proc
 * WI 156704 TWE 08/01/2014
 *    Adjust the calls for user preferences
 * ******************************************************************************/

namespace WFS.RecHub.DAL
{
    /// <summary>
    /// Used to access data for R360Servcies
    /// </summary>
    public class R360ServicesDAL : _DALBase, IR360ServicesDAL
    {
        public R360ServicesDAL(string SiteKey) : base(SiteKey)
        {
        }

        public bool GetClientAccountSummary(Guid SessionID, int UserID, ReceivablesSummaryDBRequestDto request, out DataTable results)
        {
            bool bReturnValue = false;

            results = null;
            SqlParameter[] parms;

            try
            {
                // Populate a table for the workgroup list.
                var workgroups = (request.Workgroups as IList<WorkgroupDto>).ToDataTableParams();

                // Call the SP.
                string sFormatedDepositDate = request.DepositDate.ToString("yyyyMMdd");
                parms = new SqlParameter[] {
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, SessionID, ParameterDirection.Input),
                    BuildParameter("@parmDepositDateKey", SqlDbType.Int, Convert.ToInt32(sFormatedDepositDate), ParameterDirection.Input),
                    BuildParameter("@parmWorkgroups", SqlDbType.Structured, workgroups, ParameterDirection.Input),
                };

                bReturnValue = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_ClientAccountSummary_by_SP",
                        parms, out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name,
                        "GetClientAccountSummary(int UserID, Guid OLClientID, bool UseCutoff, DateTime DepositDate, out DataTable results)");
                bReturnValue = false;
            }
            return bReturnValue;
        }

        public bool GetSourceBatchID(long batchID, out long result)
        {
            bool returnValue = false;
            result = 0;
            DataTable res = null;
            SqlParameter[] parms;
            try
            {
                parms = new SqlParameter[] { BuildParameter("@parmBatchID", SqlDbType.BigInt, batchID, ParameterDirection.Input) };
                returnValue = Database.executeProcedure("RecHubData.usp_factBatchSummary_Get_SourceBatchID", parms, out res);
                if (returnValue && res != null && res.Rows.Count > 0)
                {
                    long.TryParse(res.Rows[0]["SourceBatchID"].ToString(), out result);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetSourceBatchID(long batchID, out long results)");
                returnValue = false;
            }
            return returnValue;
        }

        public bool GetUserIDBySID(Guid SID, out DataTable results)
        {
            bool bReturnValue = false;

            results = null;
            SqlParameter[] parms;
            try
            {
                parms = new SqlParameter[] {
                    BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input)
                };
                bReturnValue = Database.executeProcedure("RecHubUser.usp_Users_UserID_Get_BySID",
                        parms, out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetUserIDBySID(Guid SID, out DataTable results)");
                bReturnValue = false;
            }

            return bReturnValue;
        }

        public bool GetAuditUsers(out List<AuditUserDTO> users)
        {
            DataTable dt = null;
            users = null;

            try
            {
                if (Database.executeProcedure("RecHubUser.usp_Users_Get_All", out dt))
                {
                    users = new List<AuditUserDTO>();
                    if (dt != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            users.Add(new AuditUserDTO()
                            {
                                UserID = (int)dt.Rows[i]["UserId"],
                                SID = (Guid)dt.Rows[i]["RA3MSID"]
                            });
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetAuditUsers");
            }

            return false;
        }

        public bool GetBankName(int siteBankId, out string bankName)
        {
            DataTable dt = null;
            var result = false;

            bankName = "";
            try
            {
                var parms = new SqlParameter[] {
                    BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId, ParameterDirection.Input)
                };

                if (Database.executeProcedure("RecHubData.usp_dimBanks_Get_BySiteBankID", parms, out dt))
                {
                    if (dt == null)
                    {
                        EventLog.logEvent($"Bank Name not found for Site Bank ID: {siteBankId}", $"{this.GetType().ToString()}GetBankName", MessageType.Error, MessageImportance.Essential);
                        throw new Exception();
                    }
                    else
                    {
                        bankName = dt.Rows[0]["BankName"].ToString();
                        result = true;
                    }
                }
                else
                {
                    EventLog.logEvent($"Failed to query Bank Name by Site Bank ID: {siteBankId}", $"{this.GetType().ToString()}GetBankName", MessageType.Error, MessageImportance.Essential);
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetBankName");
            }
            return result;
        }

        public bool GetClientAccounts(int UserID, Guid OLOrganizationID, out DataTable results)
        {
            bool bReturnValue = false;

            results = null;
            List<SqlParameter> lstParms = new List<SqlParameter>();

            try
            {
                lstParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
                lstParms.Add(BuildParameter("@parmOLOrganizationID", SqlDbType.UniqueIdentifier, OLOrganizationID, ParameterDirection.Input));
                bReturnValue = Database.executeProcedure("RecHubUser.usp_OLClientAccounts_Get_ByUserIDOLOrganizationID", lstParms.ToArray(), out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name,
                    "GetClientAccounts(int UserID, Guid OLOrganizationID, out DataTable results)");
            }
            return bReturnValue;
        }


        public bool WriteAuditEvent(int userID, string eventName, string eventType, string applicationName, string description)
        {
            bool bRtnval = false;
            const string PROCNAME = "RecHubCommon.usp_WFS_EventAudit_Ins";
            try
            {
                SqlParameter[] parms = new SqlParameter[] {
                    BuildParameter("@parmApplicationName", SqlDbType.VarChar, 256, applicationName),
                    BuildParameter("@parmEventName", SqlDbType.VarChar, 64, eventName),
                    BuildParameter("@parmEventType", SqlDbType.VarChar, 64, eventType),
                    BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input),
                    BuildParameter("@parmAuditMessage", SqlDbType.VarChar, description, ParameterDirection.Input),
                };
                bRtnval = Database.executeProcedure(PROCNAME, parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "WriteAuditEvent");
                bRtnval = false;
            }
            return bRtnval;
        }

        public bool GetActiveWorkgroups(Guid sessionID, EntityDTO entities, out List<WorkgroupNameDTO> workgroups)
        {
            workgroups = null;
            try
            {
                SqlParameter[] parms = new SqlParameter[]
                {
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input),
                    BuildParameter("@parmEntities", SqlDbType.Xml, entities == null ? (object)DBNull.Value : entities.ToFlatDBXml(), ParameterDirection.Input),
                };

                DataTable dt = null;
                if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_ByEntities_Active", parms, out dt))
                {
                    workgroups = new List<WorkgroupNameDTO>();
                    if (dt != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            workgroups.Add(new WorkgroupNameDTO()
                            {
                                EntityId = (int)dt.Rows[i]["EntityID"],
                                SiteBankID = (int)dt.Rows[i]["SiteBankID"],
                                SiteWorkgroupID = (int)dt.Rows[i]["SiteClientAccountID"],
                                DisplayLabel = (string)dt.Rows[i]["DisplayLabel"]
                            });
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetActiveWorkgroups");
            }
            return false;
        }

        public bool GetIntegraPayOnlyWorkgroups(Guid sessionID, EntityDTO entities, out List<WorkgroupNameDTO> workgroups)
        {
            workgroups = null;
            try
            {
                SqlParameter[] parms = new SqlParameter[]
                {
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input),
                    BuildParameter("@parmEntities", SqlDbType.Xml, entities == null ? (object)DBNull.Value : entities.ToFlatDBXml(), ParameterDirection.Input),
                    BuildParameter("@parmBatchSource", SqlDbType.VarChar, "integrapay", ParameterDirection.Input),
                };

                DataTable dt = null;
                if (Database.executeProcedure("RecHubData.usp_dimClientAccounts_Get_ByEntities_Active", parms, out dt))
                {
                    workgroups = new List<WorkgroupNameDTO>();
                    if (dt != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            workgroups.Add(new WorkgroupNameDTO()
                            {
                                EntityId = (int)dt.Rows[i]["EntityID"],
                                SiteBankID = (int)dt.Rows[i]["SiteBankID"],
                                SiteWorkgroupID = (int)dt.Rows[i]["SiteClientAccountID"],
                                DisplayLabel = (string)dt.Rows[i]["DisplayLabel"]
                            });
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetActiveWorkgroups");
            }
            return false;
        }

        public bool GetDDASummaryData(Guid sessionID, DateTime depositDate, out List<DDASummaryDataDto> summaryData)
        {
            bool bReturnValue = false;
            DataTable results = null;
            SqlParameter[] parms;

            summaryData = new List<DDASummaryDataDto>();

            try
            {
                int depositDateParm = Int32.Parse(depositDate.ToString("yyyyMMdd"));

                parms = new SqlParameter[] {
                    BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateParm, ParameterDirection.Input),
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input)
                };

                bReturnValue = Database.executeProcedure("RecHubData.usp_DDASummary", parms, out results);

                if (bReturnValue && results != null)
                {
                    for (int i = 0; i < results.Rows.Count; i++)
                    {
                        summaryData.Add(new DDASummaryDataDto()
                        {
                            EntityID = (int)results.Rows[i]["EntityID"],
                            SiteBankID = (int)results.Rows[i]["SiteBankID"],
                            SiteClientAccountID = (int)results.Rows[i]["SiteClientAccountID"],
                            DDA = results.Rows[i]["DDA"].ToString(),
                            PaymentCount = (int)results.Rows[i]["PaymentCount"],
                            PaymentSource = results.Rows[i]["PaymentSource"].ToString(),
                            PaymentTotal = (decimal)results.Rows[i]["PaymentTotal"],
                            PaymentType = results.Rows[i]["PaymentType"].ToString(),
                            WorkgroupName = results.Rows[i]["DisplayLabel"].ToString()
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetDDASummaryData(Guid sessionID, DateTime depositDate, out DataTable results)");
                bReturnValue = false;
            }

            return bReturnValue;
        }
    }
}
