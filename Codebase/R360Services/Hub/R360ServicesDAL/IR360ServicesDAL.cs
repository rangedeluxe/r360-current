﻿using System;
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.DAL
{
    public interface IR360ServicesDAL : IDisposable
    {
        bool GetClientAccountSummary(Guid SessionID, int UserID, ReceivablesSummaryDBRequestDto request, out DataTable results);

        bool GetSourceBatchID(long batchID, out long result);

        bool GetUserIDBySID(Guid SID, out DataTable results);

        bool GetAuditUsers(out List<AuditUserDTO> users);

        bool GetBankName(int siteBankId, out string bankName);

        bool GetClientAccounts(int UserID, Guid OLOrganizationID, out DataTable results);

        bool WriteAuditEvent(int userID, string eventName, string eventType, string applicationName, string description);

        bool GetActiveWorkgroups(Guid sessionID, EntityDTO entities, out List<WorkgroupNameDTO> workgroups);

        bool GetIntegraPayOnlyWorkgroups(Guid sessionID, EntityDTO entities, out List<WorkgroupNameDTO> workgroups);

        bool GetDDASummaryData(Guid sessionID, DateTime depositDate, out List<DDASummaryDataDto> summaryData);
    }
}
