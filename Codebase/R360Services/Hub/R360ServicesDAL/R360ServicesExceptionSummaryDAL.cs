﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/23/2013
 * *
 * * Purpose: Provides the connection from the R360Services to the database
 * *
 * * Modification History
 * WI 101786 DRP 06/24/2013 
 * -  Added GetExceptionSummariesByAccount (moved to this class_
 * ******************************************************************************/
namespace WFS.RecHub.DAL
{
    public class R360ServicesExceptionSummaryDAL: _DALBase, IDisposable {
        public R360ServicesExceptionSummaryDAL(string SiteKey):base(SiteKey, ConnectionType.CommonException) {
            }


        public bool GetExceptionSummaryByWorkgroup(Guid sessionID, EntityDTO entity, DateTime datefrom, DateTime dateto, WorkgroupIDsDTO workgroup, out DataTable results)
        {
            bool bReturnValue = false;
            // The Ds make sure we have values of "02" instead of "2" for months and days.
            var datefromint = int.Parse(datefrom.Year.ToString("D4") + datefrom.Month.ToString("D2") + datefrom.Day.ToString("D2"));
            var datetoint = int.Parse(dateto.Year.ToString("D4") + dateto.Month.ToString("D2") + dateto.Day.ToString("D2"));
            results = null;
            SqlParameter[] parms;
            try
            {
                parms = new SqlParameter[] {
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input),
					BuildParameter("@parmEntities", SqlDbType.Xml, entity == null ? (object)DBNull.Value : entity.ToFlatDBXml(), ParameterDirection.Input),
					BuildParameter("@parmSiteBankID", SqlDbType.Int, workgroup == null ? (object)DBNull.Value : workgroup.SiteBankID, ParameterDirection.Input),
					BuildParameter("@parmWorkgroupID", SqlDbType.Int, workgroup == null ? (object)DBNull.Value : workgroup.SiteWorkgroupID, ParameterDirection.Input),
                    BuildParameter("@parmDateFrom", SqlDbType.Int, datefromint, ParameterDirection.Input),
                    BuildParameter("@parmDateTo", SqlDbType.Int, datetoint, ParameterDirection.Input)
                };
                bReturnValue = Database.executeProcedure("RecHubException.usp_CommonExceptions_Get_Summary_ByAccount",
                        parms, out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetExceptionSummaryByWorkgroup(int Entity, int Account, int UserID, out DataTable results)");
                bReturnValue = false;
            }

            return bReturnValue;
        }
    }
}
