using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace WFS.RecHub.DAL
{
    public class UserDal : _DALBase, IUserDal
    {
        public UserDal(string siteKey) : base(siteKey)
        {
        }

        /// <summary>
        /// Gets the OL user preferences.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetOLUserPreferences(int UserID, out DataTable dt)
        {
            bool bRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            arParms = new List<SqlParameter>();
            try
            {
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRetVal = Database.executeProcedure("RecHubUser.usp_OLUserPreferences_Get_ByUserID", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLUserPreferences(int UserID, out DataTable dt)");
            }

            return bRetVal;
        }

        /// <summary>
        /// Gets the name of the OL user preference ID by.
        /// </summary>
        /// <param name="PreferenceName">Name of the preference.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetOLUserPreferenceIDByName(string PreferenceName, out DataTable dt)
        {
            bool bRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPreferenceName", SqlDbType.Char, PreferenceName, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRetVal = Database.executeProcedure("RecHubUser.usp_OLPreferences_Get_OLPreferenceIDByName", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLUserPreferenceIDByName(string PreferenceName, out DataTable dt)");
            }

            return bRetVal;
        }

        /// <summary>
        /// Deletes the OL user preference.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <param name="RowsReturned">The rows affected.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool DeleteOLUserPreference(int UserID, out int RowsReturned)
        {
            bool bRetVal = false;
            RowsReturned = 0;
            SqlParameter[] parms;
            SqlParameter parmRowsReturned;
            List<SqlParameter> arParms;
            arParms = new List<SqlParameter>();
            arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
            parmRowsReturned = BuildParameter("@parmRowsReturned", SqlDbType.Int, RowsReturned, ParameterDirection.Output);
            arParms.Add(parmRowsReturned);
            parms = arParms.ToArray();
            bRetVal = Database.executeProcedure("RecHubUser.usp_OLUserPreferences_Del", parms);
            if (bRetVal)
            {
                RowsReturned = (int)parmRowsReturned.Value;
            }
            else
            {
                RowsReturned = 0;
            }

            return bRetVal;
        }


        /// <summary>
        /// Updates the OL user preference.
        /// </summary>
        /// <param name="OLPreferenceID">The OL preference ID.</param>
        /// <param name="UserID">The user ID.</param>
        /// <param name="UserSetting">The user setting.</param>
        /// <param name="RowsReturned">The rows affected.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool UpdateOLUserPreference(Guid OLPreferenceID, int UserID, string UserSetting, out int RowsReturned)
        {
            bool bRetVal = false;
            RowsReturned = 0;
            SqlParameter[] parms;
            SqlParameter parmRowsReturned;
            List<SqlParameter> arParms;
            arParms = new List<SqlParameter>();
            arParms.Add(BuildParameter("@parmOLPreferenceID", SqlDbType.UniqueIdentifier, OLPreferenceID, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmUserSettings", SqlDbType.VarChar, UserSetting, ParameterDirection.Input));
            parmRowsReturned = BuildParameter("@parmRowsReturned", SqlDbType.Int, RowsReturned, ParameterDirection.Output);
            arParms.Add(parmRowsReturned);
            parms = arParms.ToArray();
            bRetVal = Database.executeProcedure("RecHubUser.usp_OLUserPreferences_Upd_UserSetting", parms);
            if (bRetVal)
            {
                RowsReturned = (int)parmRowsReturned.Value;
            }
            else
            {
                RowsReturned = 0;
            }

            return bRetVal;
        }

        /// <summary>
        /// Inserts the OL user preference.
        /// </summary>
        /// <param name="OLPreferenceID">The OL preference ID.</param>
        /// <param name="UserID">The user ID.</param>
        /// <param name="UserSetting">The user setting.</param>
        /// <param name="RowsReturned">The rows affected.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool InsertOLUserPreference(Guid OLPreferenceID, int UserID, string UserSetting, out int RowsReturned)
        {
            bool bRetVal = false;
            RowsReturned = 0;
            SqlParameter[] parms;
            SqlParameter parmRowsReturned;
            List<SqlParameter> arParms;
            arParms = new List<SqlParameter>();
            arParms.Add(BuildParameter("@parmOLPreferenceID", SqlDbType.UniqueIdentifier, OLPreferenceID, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmUserSetting", SqlDbType.VarChar, UserSetting, ParameterDirection.Input));
            parmRowsReturned = BuildParameter("@parmRowsReturned", SqlDbType.Int, RowsReturned, ParameterDirection.Output);
            arParms.Add(parmRowsReturned);
            parms = arParms.ToArray();
            bRetVal = Database.executeProcedure("RecHubUser.usp_OLUserPreferences_Ins", parms);
            if (bRetVal)
            {
                RowsReturned = (int)parmRowsReturned.Value;
            }
            else
            {
                RowsReturned = 0;
            }

            return bRetVal;
        }

        /// <summary>
        /// Gets the OL user preference count.
        /// </summary>
        /// <param name="OLPreferenceID">The OL preference ID.</param>
        /// <param name="UserID">The user ID.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetOLUserPreferenceCount(Guid OLPreferenceID, int UserID, out DataTable dt)
        {
            bool bRetVal = false;
            dt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmOLPreferenceID", SqlDbType.UniqueIdentifier, OLPreferenceID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRetVal = Database.executeProcedure("RecHubUser.usp_OLUserPreferences_Get_Count", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetOLUserPreferenceCount(Guid OLPreferenceID, int UserID, out DataTable dt)");
            }

            return bRetVal;
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <param name="dt">The data table.</param>
        /// <returns>
        /// Result of store procedure call
        /// </returns>
        public bool GetUser(int UserID, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, UserID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubUser.usp_Users_Get_ByUserID", parms, out tempdt);

                // remove after business layer is updated
                dt = DALLib.ConvertDataTable(ref tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetUser(int UserID, out DataTable dt)");
            }

            return (bolRetVal);
        }
    }
}