﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class PostDepositEntityListXmlTransformer : IXmlTransformer<List<PostDepositEntityDto>>
    {
        public List<PostDepositEntityDto> ToDTO(XmlDocument input)
        {
            throw new NotImplementedException();
        }

        public XmlDocument ToXML(List<PostDepositEntityDto> input)
        {
            var doc = new XDocument(
                new XElement("Entities",
                    input.Select(entity => new XElement("Entity",
                        new XAttribute("EntityID", entity.EntityId),
                        new XAttribute("EntityHierarchy", entity.EntityHierarchy)
                    ))
                )
            );

            var xdoc = new XmlDocument();
            using (var reader = doc.CreateReader())
                xdoc.Load(reader);

            return xdoc;
        }
    }
}
