﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class InvoiceSearchRequestXmlTransformer : IXmlTransformer<InvoiceSearchXmlRequestDto>
    {
        public InvoiceSearchXmlRequestDto ToDTO(XmlDocument input)
        {
            throw new NotImplementedException();
        }

        public XmlDocument ToXML(InvoiceSearchXmlRequestDto input)
        {

            // First we sanitize our criteria.
            var sanitizedCriteria = new List<InvoiceSearchCriteriaDto>();
            foreach (var c in input.Criteria)
            {
                // Convert Equals to combinations of < and >, as the XML in the SP doesn't actually support 'equals'.
                if (c.Operator == InvoiceSearchOperator.Equals)
                {
                    sanitizedCriteria.Add( new InvoiceSearchCriteriaDto()
                    {
                        DataType = c.DataType,
                        Operator = InvoiceSearchOperator.IsLessThanOrEqualTo,
                        ReportTitle = c.ReportTitle,
                        Value = c.Value,
                        XmlColumnName = c.XmlColumnName
                    });
                    sanitizedCriteria.Add( new InvoiceSearchCriteriaDto()
                    {
                        DataType = c.DataType,
                        Operator = InvoiceSearchOperator.IsGreaterThanOrEqualTo,
                        ReportTitle = c.ReportTitle,
                        Value = c.Value,
                        XmlColumnName = c.XmlColumnName
                    });
                }
                else
                {
                    sanitizedCriteria.Add(c);
                }
            }
            input.Criteria = sanitizedCriteria;

            var doc = new XDocument(
                new XElement("Root",
                    new XElement("DepositDateFrom", input.DateFrom.ToString("MM/dd/yyyy")),
                    new XElement("DepositDateTo", input.DateTo.ToString("MM/dd/yyyy")),
                    new XElement("BatchPaymentTypeKey", input.PaymentType > -1 ? input.PaymentType.ToString() : ""),
                    new XElement("BatchSourceKey", input.PaymentSource > -1 ? input.PaymentSource.ToString() : ""),
                    new XElement("SessionID", input.Session.ToString()),
                    input.EntityName != null ? new XElement("EntityName", input.EntityName) : null,
                    new XElement("FullUserName", input.FullUserName),
                    new XElement("RecordsPerPage", input.Length.ToString()),
                    new XElement("StartRecord", (input.Start + 1).ToString()),
                    new XElement("SortBy", string.Format("{0}~{1}", input.OrderBy, input.OrderDirection.ToLower() == "asc" ? "ASCENDING" : "DESCENDING")),
                    new XElement("ClientAccounts",
                        input.Workgroups.Select(x => new XElement("ClientAccount", new XAttribute("BankID", x.BankId),
                                                     new XAttribute("ClientAccountID", x.ClientAccountId)))
                    ),
                    input.Criteria.Select(x =>
                    {
                        var name = x.XmlColumnName;
                        if (x.Operator == InvoiceSearchOperator.IsLessThanOrEqualTo)
                            name += "To";
                        else if (x.Operator == InvoiceSearchOperator.IsGreaterThanOrEqualTo)
                            name += "From";
                        return new XElement(name, x.Value.Replace("'", "''"));
                    })
                )
            );

            // Convert the xdoc to an xmldoc and return.
            var xmldoc = new XmlDocument();
            using (var reader = doc.CreateReader())
            {
                xmldoc.Load(reader);
            }
            return xmldoc;
        }
    }
}
