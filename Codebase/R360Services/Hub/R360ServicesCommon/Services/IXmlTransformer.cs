﻿using System;
using System.Xml;

namespace WFS.RecHub.R360Services.Common.Services
{
    public interface IXmlTransformer<DtoType>
    {
        DtoType ToDTO(XmlDocument input);
        XmlDocument ToXML(DtoType input);
    }
}
