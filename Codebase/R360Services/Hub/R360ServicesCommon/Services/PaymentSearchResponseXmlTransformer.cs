﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class PaymentSearchResponseXmlTransformer : IXmlTransformer<PaymentSearchResponseDto>
    {
        public PaymentSearchResponseDto ToDTO(XmlDocument input)
        {
            var output = new PaymentSearchResponseDto();
            var doc = XDocument.Parse(input.OuterXml);

            var recordset = doc.Descendants("RecordSet").First();
            var totalrecords = recordset.Attribute("TotalRecords")?.Value;

            output.TotalRecords = string.IsNullOrWhiteSpace(totalrecords) 
                ? 0 
                : long.Parse(totalrecords);

            return output;
        }

        public XmlDocument ToXML(PaymentSearchResponseDto input)
        {
            throw new NotImplementedException();
        }
    }
}
