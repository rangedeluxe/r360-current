﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class NotificationsRequestXmlTransformer : IXmlTransformer<NotificationsRequestDto>
    {
        public XmlDocument ToXML(NotificationsRequestDto input)
        {
            var doc = new XmlDocument();
            var root = doc.CreateElement("Root");

            var node = doc.CreateElement("StartDate");
            node.InnerText = input.StartDate.ToString("MM/dd/yyyy");
            root.AppendChild(node);

            node = doc.CreateElement("EndDate");
            node.InnerText = input.EndDate.ToString("MM/dd/yyyy");
            root.AppendChild(node);

            node = doc.CreateElement("StartRecord");
            node.InnerText = input.StartRecord.ToString();
            root.AppendChild(node);

            node = doc.CreateElement("OrderBy");
            node.InnerText = input.SortBy;
            root.AppendChild(node);

            node = doc.CreateElement("OrderDir");
            node.InnerText = input.SortByDir;
            root.AppendChild(node);

            if (input.ClientAccounts != null && input.ClientAccounts.Any())
            {
                node = doc.CreateElement("ClientAccounts");
            }

            foreach (var acct in input.ClientAccounts)
            {
                var acctNode = doc.CreateElement("ClientAccount");
                acctNode.SetAttribute("BankID", acct.BankId.ToString());
                acctNode.SetAttribute("ClientAccountID", acct.ClientAccountId.ToString());
                node.AppendChild(acctNode);
            }
            root.AppendChild(node);
            doc.AppendChild(root);
            return doc;
        }

        //not needed
        public NotificationsRequestDto ToDTO(XmlDocument input)
        {
            throw new NotImplementedException();
        }
    }
}
