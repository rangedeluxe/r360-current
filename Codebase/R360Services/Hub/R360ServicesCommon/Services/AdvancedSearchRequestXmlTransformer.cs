﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class AdvancedSearchRequestXmlTransformer : IXmlTransformer<AdvancedSearchRequestDto>
    {
        public AdvancedSearchRequestDto ToDTO(XmlDocument input)
        {
            var dto = new AdvancedSearchRequestDto();

            // Basic data.
            var node = input.SelectSingleNode("/Root/BankID");
            dto.BankId = int.Parse(node.InnerText);

            node = input.SelectSingleNode("/Root/ClientAccountID");
            dto.WorkgroupId = int.Parse(node.InnerText);

            // DateFrom and DateTo are not included in stored queries.
            node = input.SelectSingleNode("/Root/DateFrom");
            if (node != null && !string.IsNullOrWhiteSpace(node.InnerText))
                dto.DepositDateFrom = DateTime.Parse(node.InnerText);

            node = input.SelectSingleNode("/Root/DateTo");
            if (node != null && !string.IsNullOrWhiteSpace(node.InnerText))
                dto.DepositDateTo = DateTime.Parse(node.InnerText);

            node = input.SelectSingleNode("/Root/SessionID");
            dto.SessionId = Guid.Parse(node.InnerText);

            node = input.SelectSingleNode("/Root/StartRecord");
            dto.StartRecord = int.Parse(node.InnerText);

            node = input.SelectSingleNode("/Root/RecordsPerPage");
            if (node != null && !string.IsNullOrWhiteSpace(node.InnerText))
                dto.RecordsPerPage = int.Parse(node.InnerText);

            node = input.SelectSingleNode("/Root/COTSOnly");
            dto.COTSOnly = bool.Parse(node.InnerText);

            node = input.SelectSingleNode("/Root/MarkSenseOnly");
            dto.MarkSenseOnly = bool.Parse(node.InnerText);

            node = input.SelectSingleNode("/Root/BatchPaymentTypeKey");
            int tempint = 0;
            int.TryParse(node.InnerText, out tempint);
            dto.PaymentTypeKey = tempint;

            node = input.SelectSingleNode("/Root/BatchSourceKey");
            tempint = 0;
            int.TryParse(node.InnerText, out tempint);
            dto.PaymentSourceKey = tempint;

            node = input.SelectSingleNode("/Root/SortBy");
            dto.SortBy = node.InnerText;

            node = input.SelectSingleNode("/Root/SortByDir");
            dto.SortByDir = node.InnerText;

            node = input.SelectSingleNode("/Root/SortByDisplayName");
            dto.SortByDisplayName = node.InnerText;

            // Where Clauses
            var wheres = input.SelectNodes("/Root/WhereClause/field");
            if (wheres != null)
            {
                foreach (XmlNode wherenode in wheres)
                {
                    // The old page still uses 5 where clauses no matter what. So we don't want to be deserializing them.
                    if (string.IsNullOrWhiteSpace(wherenode.Attributes["fieldname"].Value))
                        continue;

                    var w = new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = int.Parse(wherenode.Attributes["batchsourcekey"].Value),
                        DataType = (AdvancedSearchWhereClauseDataType)(int.Parse(wherenode.Attributes["datatype"].Value)),
                        FieldName = wherenode.Attributes["fieldname"].Value,
                        IsDataEntry = true,
                        IsStandard = false,
                        ReportTitle = wherenode.Attributes["reporttitle"].Value,
                        Value = wherenode.Attributes["value"].Value
                    };

                    var opval = wherenode.Attributes["operator"].Value;
                    w.Operator = opval == "Begins With"
                        ? AdvancedSearchWhereClauseOperator.BeginsWith
                        : opval == "Contains"
                        ? AdvancedSearchWhereClauseOperator.Contains
                        : opval == "Ends With"
                        ? AdvancedSearchWhereClauseOperator.EndsWith
                        : opval == "Equals"
                        ? AdvancedSearchWhereClauseOperator.Equals
                        : opval == "Is Greater Than"
                        ? AdvancedSearchWhereClauseOperator.IsGreaterThan
                        : AdvancedSearchWhereClauseOperator.IsLessThan;

                    var tableval = wherenode.Attributes["tablename"].Value;
                    w.Table = tableval == "Checks"
                        ? AdvancedSearchWhereClauseTable.Checks
                        : tableval == "Stubs"
                        ? AdvancedSearchWhereClauseTable.Stubs
                        : AdvancedSearchWhereClauseTable.None;

                    dto.WhereClauses.Add(w);
                }
            }

            // SelectFields
            var selects = input.SelectNodes("/Root/SelectFields/field");
            if (selects != null)
            {
                foreach (XmlNode selectnode in selects)
                {
                    var w = new AdvancedSearchWhereClauseDto()
                    {
                        BatchSourceKey = int.Parse(selectnode.Attributes["batchsourcekey"].Value),
                        DataType = (AdvancedSearchWhereClauseDataType)(int.Parse(selectnode.Attributes["datatype"].Value)),
                        FieldName = selectnode.Attributes["fieldname"].Value,
                        IsDataEntry = true,
                        IsStandard = false,
                        ReportTitle = selectnode.Attributes["reporttitle"].Value,
                    };

                    var tableval = selectnode.Attributes["tablename"].Value;
                    w.Table = tableval == "Checks"
                        ? AdvancedSearchWhereClauseTable.Checks
                        : tableval == "Stubs"
                        ? AdvancedSearchWhereClauseTable.Stubs
                        : AdvancedSearchWhereClauseTable.None;

                    dto.SelectFields.Add(w);
                }
            }

            // Standard Fields
            // Logic: If we have both 'From' and 'To' fields with equal values, then we need 1 single where clause of 'equals'.
            //        otherwise use the greaterthan/lessthan.
            var batchnumberfrom = input.SelectSingleNode("/Root/BatchNumberFrom");
            var batchnumberto = input.SelectSingleNode("/Root/BatchNumberTo");
            if (batchnumberfrom != null && !string.IsNullOrWhiteSpace(batchnumberfrom.InnerText)
                && batchnumberto != null && !string.IsNullOrWhiteSpace(batchnumberto.InnerText)
                && batchnumberfrom.InnerText == batchnumberto.InnerText)
            {
                dto.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                {
                    IsStandard = true,
                    StandardXmlColumnName = "BatchNumber",
                    Operator = AdvancedSearchWhereClauseOperator.Equals,
                    DataType = AdvancedSearchWhereClauseDataType.Float,
                    ReportTitle = "Batch Number",
                    OrderByName = "BatchNumber",
                    Value = batchnumberfrom.InnerText
                });
            }
            else
            {
                // They don't equal, so add them as greaterthan or lessthan as we need.
                if (batchnumberfrom != null && !string.IsNullOrWhiteSpace(batchnumberfrom.InnerText))
                {
                    dto.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                    {
                        IsStandard = true,
                        StandardXmlColumnName = "BatchNumber",
                        Operator = AdvancedSearchWhereClauseOperator.IsGreaterThan,
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        ReportTitle = "Batch Number",
                        OrderByName = "BatchNumber",
                        Value = batchnumberfrom.InnerText
                    });
                }

                if (batchnumberto != null && !string.IsNullOrWhiteSpace(batchnumberto.InnerText))
                {
                    dto.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                    {
                        IsStandard = true,
                        StandardXmlColumnName = "BatchNumber",
                        Operator = AdvancedSearchWhereClauseOperator.IsLessThan,
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        ReportTitle = "Batch Number",
                        OrderByName = "BatchNumber",
                        Value = batchnumberto.InnerText
                    });
                }
            }

            var batchidfrom = input.SelectSingleNode("/Root/BatchIDFrom");
            var batchidto = input.SelectSingleNode("/Root/BatchIDTo");
            if (batchidfrom != null && !string.IsNullOrWhiteSpace(batchidfrom.InnerText)
                && batchidto != null && !string.IsNullOrWhiteSpace(batchidto.InnerText)
                && batchidfrom.InnerText == batchidto.InnerText)
            {
                dto.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                {
                    IsStandard = true,
                    StandardXmlColumnName = "BatchID",
                    Operator = AdvancedSearchWhereClauseOperator.Equals,
                    DataType = AdvancedSearchWhereClauseDataType.Float,
                    ReportTitle = "Batch ID",
                    OrderByName = "BatchID",
                    Value = batchidfrom.InnerText
                });
            }
            else
            {
                // They don't equal, so add them as greaterthan or lessthan as we need.
                if (batchidfrom != null && !string.IsNullOrWhiteSpace(batchidfrom.InnerText))
                {
                    dto.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                    {
                        IsStandard = true,
                        StandardXmlColumnName = "BatchID",
                        Operator = AdvancedSearchWhereClauseOperator.IsGreaterThan,
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        ReportTitle = "Batch ID",
                        OrderByName = "BatchID",
                        Value = batchidfrom.InnerText
                    });
                }

                if (batchidto != null && !string.IsNullOrWhiteSpace(batchidto.InnerText))
                {
                    dto.WhereClauses.Add(new AdvancedSearchWhereClauseDto()
                    {
                        IsStandard = true,
                        StandardXmlColumnName = "BatchID",
                        Operator = AdvancedSearchWhereClauseOperator.IsLessThan,
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        ReportTitle = "Batch ID",
                        OrderByName = "BatchID",
                        Value = batchidto.InnerText
                    });
                }
            }

            return dto;
        }

        public XmlDocument ToXML(AdvancedSearchRequestDto input)
        {
            var doc = new XmlDocument();
            var root = doc.CreateElement("Root");

            // Basic data.
            var node = doc.CreateElement("Action");
            node.InnerText = "Refine";
            root.AppendChild(node);

            node = doc.CreateElement("PageAction");
            node.InnerText = "search";
            root.AppendChild(node);

            node = doc.CreateElement("BankID");
            node.InnerText = input.BankId.ToString();
            root.AppendChild(node);

            node = doc.CreateElement("ClientAccountID");
            node.InnerText = input.WorkgroupId.ToString();
            root.AppendChild(node);

            node = doc.CreateElement("WorkgroupSelection");
            node.InnerText = string.Format("{0}|{1}", input.BankId, input.WorkgroupId);
            root.AppendChild(node);

            node = doc.CreateElement("DateFrom");
            node.InnerText = input.DepositDateFrom.ToString("MM/dd/yyyy");
            root.AppendChild(node);

            node = doc.CreateElement("DateTo");
            node.InnerText = input.DepositDateTo.ToString("MM/dd/yyyy");
            root.AppendChild(node);

            node = doc.CreateElement("SessionID");
            node.InnerText = input.SessionId.ToString();
            root.AppendChild(node);

            node = doc.CreateElement("COTSOnly");
            node.InnerText = input.COTSOnly.ToString();
            root.AppendChild(node);

            node = doc.CreateElement("MarkSenseOnly");
            node.InnerText = input.MarkSenseOnly.ToString();
            root.AppendChild(node);

            node = doc.CreateElement("BatchPaymentTypeKey");
            node.InnerText = input.PaymentTypeKey.ToString();
            root.AppendChild(node);

            node = doc.CreateElement("BatchSourceKey");
            node.InnerText = input.PaymentSourceKey.ToString();
            root.AppendChild(node);

            node = doc.CreateElement("displaybatchid");
            node.InnerText = input.DisplayBatchID.ToString();
            root.AppendChild(node);

            // Whereclauses - DataEntry.
            node = doc.CreateElement("WhereClause");
            foreach (var wc in input.WhereClauses.Where(x => x.IsDataEntry))
            {
                var datatype = (int)wc.DataType;
                var op = wc.Operator == AdvancedSearchWhereClauseOperator.BeginsWith
                    ? "Begins With"
                    : wc.Operator == AdvancedSearchWhereClauseOperator.Contains
                    ? "Contains"
                    : wc.Operator == AdvancedSearchWhereClauseOperator.EndsWith
                    ? "Ends With"
                    : wc.Operator == AdvancedSearchWhereClauseOperator.Equals
                    ? "Equals"
                    : wc.Operator == AdvancedSearchWhereClauseOperator.IsGreaterThan
                    ? "Is Greater Than"
                    : "Is Less Than";
                var table = wc.Table == AdvancedSearchWhereClauseTable.Checks
                    ? "Checks"
                    : wc.Table == AdvancedSearchWhereClauseTable.Stubs
                    ? "Stubs"
                    : string.Empty;
                var wherenode = doc.CreateElement("field");
                wherenode.SetAttribute("tablename", table);
                wherenode.SetAttribute("fieldname", wc.FieldName);
                wherenode.SetAttribute("datatype", datatype.ToString());
                wherenode.SetAttribute("reporttitle", wc.ReportTitle);
                wherenode.SetAttribute("operator", op);
                wherenode.SetAttribute("batchsourcekey", wc.BatchSourceKey.ToString());
                wherenode.SetAttribute("value", wc.Value);

                node.AppendChild(wherenode);
            }
            root.AppendChild(node);

            // Whereclauses - Standard fields. (Like 'BatchIDFrom').
            foreach(var wc in input.WhereClauses.Where(x => x.IsStandard))
            {
                node = doc.CreateElement(wc.StandardXmlColumnName);
                node.InnerText = wc.Value;
                root.AppendChild(node);
            }

            // Pagination Fields
            node = doc.CreateElement("PaginateRS");
            node.InnerText = input.Paginate ? "1" : "0";
            root.AppendChild(node);

            // The SP is 1-based, and the datatable is 0-based. So, we add 1.
            //*** This was causing an issue when viewing advanced search results as pdf's.  It was cutting 
            //*** off the first record of the search.  Removing the add 1. 
            node = doc.CreateElement("StartRecord");
            node.InnerText = (input.StartRecord).ToString();
            root.AppendChild(node);

            node = doc.CreateElement("RecordsPerPage");
            node.InnerText = input.RecordsPerPage.ToString();
            root.AppendChild(node);

            // Sorting fields.
            node = doc.CreateElement("SortBy");
            node.InnerText = input.SortBy;
            root.AppendChild(node);

            node = doc.CreateElement("SortByDir");
            node.InnerText = input.SortByDir;
            root.AppendChild(node);

            node = doc.CreateElement("SortByDisplayName");
            node.InnerText = input.SortByDisplayName;
            root.AppendChild(node);

            // Select fields.
            node = doc.CreateElement("SelectFields");
            foreach (var sf in input.SelectFields.Where(x => x != null))
            {
                var datatype = (int)sf.DataType;
                var table = sf.Table == AdvancedSearchWhereClauseTable.Checks
                    ? "Checks"
                    : sf.Table == AdvancedSearchWhereClauseTable.Stubs
                    ? "Stubs"
                    : string.Empty;
                var wherenode = doc.CreateElement("field");
                wherenode.SetAttribute("tablename", table);
                wherenode.SetAttribute("fieldname", sf.FieldName);
                wherenode.SetAttribute("datatype", datatype.ToString());
                wherenode.SetAttribute("reporttitle", sf.ReportTitle);
                wherenode.SetAttribute("batchsourcekey", sf.BatchSourceKey.ToString());

                node.AppendChild(wherenode);
            }

            root.AppendChild(node);


            // Append default (empty) nodes for the current stored proc.
            // Make sure to edit this as we continue to work.
            root.AppendChild(doc.CreateElement("WorkgroupSelectionLabel"));
            root.AppendChild(doc.CreateElement("ActionPreDefSearchID"));
            root.AppendChild(doc.CreateElement("OLWorkGroupsID"));
            root.AppendChild(doc.CreateElement("Serial"));
            root.AppendChild(doc.CreateElement("SelectivePrintMode"));
            root.AppendChild(doc.CreateElement("SelectivePrintImageOption"));
            root.AppendChild(doc.CreateElement("SavedQueryName"));
            root.AppendChild(doc.CreateElement("SavedQueryDescription"));
            root.AppendChild(doc.CreateElement("DefaultQuery"));
            root.AppendChild(doc.CreateElement("AppendImageSizes"));
            root.AppendChild(doc.CreateElement("UserId"));
            root.AppendChild(doc.CreateElement("DisplayScannedCheck"));
            
            doc.AppendChild(root);
            return doc;
        }
    }
}
