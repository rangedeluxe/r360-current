﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.Common.DTO.AdvancedSearch;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class AdvancedSearchRequestDataTableTransformer
    {
        public TableParams ToDataTable(AdvancedSearchRequestDto request)
        {
            var result = new TableParams();

            result.BaseRequest = new List<BaseRequestDto>()
                { BaseRequestDto.FromRequest(request) }
                .ToDataTableParams();

            result.WhereClause = request.WhereClauses
                .Where(w => w.IsDataEntry)
                .Select(w => WhereClauseDto.FromRequest(w))
                .ToList()
                .ToDataTableParams();

            result.SelectFields = request.SelectFields
                .Select(s => SelectFieldDto.FromRequest(s))
                .ToList()
                .ToDataTableParams();

            return result;
        }
    }
}
