﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class PaymentSearchRequestXmlTransformer : IXmlTransformer<PaymentSearchXmlRequestDto>
    {
        public PaymentSearchXmlRequestDto ToDTO(XmlDocument input)
        {
            throw new NotImplementedException();
        }

        public XmlDocument ToXML(PaymentSearchXmlRequestDto input)
        {
            // First we sanitize our criteria.
            var sanitizedCriteria = new List<PaymentSearchCriteriaDto>();
            foreach (var c in input.Criteria)
            {
                // Convert Equals to combinations of < and >, as the XML in the SP doesn't actually support 'equals'.
                if (c.Operator == PaymentSearchOperator.Equals)
                {
                    sanitizedCriteria.Add(new PaymentSearchCriteriaDto()
                    {
                        DataType = c.DataType,
                        Operator = PaymentSearchOperator.IsLessThanOrEqualTo,
                        ReportTitle = c.ReportTitle,
                        Value = c.Value,
                        XmlColumnName = c.XmlColumnName
                    });
                    sanitizedCriteria.Add(new PaymentSearchCriteriaDto()
                    {
                        DataType = c.DataType,
                        Operator = PaymentSearchOperator.IsGreaterThanOrEqualTo,
                        ReportTitle = c.ReportTitle,
                        Value = c.Value,
                        XmlColumnName = c.XmlColumnName
                    });
                }
                else
                {
                    sanitizedCriteria.Add(c);
                }
            }
            input.Criteria = sanitizedCriteria;

            var doc = new XDocument(
                new XElement("Root",
                    new XElement("DepositDateFrom", input.DateFrom.ToString("MM/dd/yyyy")),
                    new XElement("DepositDateTo", input.DateTo.ToString("MM/dd/yyyy")),
                    new XElement("BatchPaymentTypeKey", input.PaymentType > -1 ? input.PaymentType.ToString() : ""),
                    new XElement("BatchSourceKey", input.PaymentSource > -1 ? input.PaymentSource.ToString() : ""),
                    new XElement("SessionID", input.Session.ToString()),
                    new XElement("RecordsPerPage", input.Length.ToString()),
                    new XElement("StartRecord", (input.Start + 1).ToString()),
                    new XElement("SortBy", string.Format("{0}~{1}", input.OrderBy, input.OrderDirection.ToLower() == "asc" ? "ASCENDING" : "DESCENDING")),
                    new XElement("ClientAccounts",
                        input.Workgroups.Select(x => new XElement("ClientAccount", new XAttribute("BankID", x.BankId),
                                                     new XAttribute("ClientAccountID", x.ClientAccountId)))
                    ),
                    input.Criteria.Select(x =>
                    {
                        var name = x.XmlColumnName;
                        if (x.Operator == PaymentSearchOperator.IsLessThanOrEqualTo)
                            name += "To";
                        else if (x.Operator == PaymentSearchOperator.IsGreaterThanOrEqualTo)
                            name += "From";
                        return new XElement(name, x.Value.Replace("'", "''"));
                    })
                )
            );

            // Convert the xdoc to an xmldoc and return.
            var xmldoc = new XmlDocument();
            using (var reader = doc.CreateReader())
            {
                xmldoc.Load(reader);
            }
            return xmldoc;
        }
    }
}
