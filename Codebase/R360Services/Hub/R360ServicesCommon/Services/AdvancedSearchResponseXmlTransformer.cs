﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class AdvancedSearchResponseXmlTransformer : IXmlTransformer<AdvancedSearchResponseDto>
    {
        public AdvancedSearchResponseDto ToDTO(XmlDocument input)
        {
            var dto = new AdvancedSearchResponseDto();

            // Basic data.
            var node = input.SelectSingleNode("/Page/RecordSet/@TotalRecords");
            dto.TotalRecords = int.Parse(node.Value);

            node = input.SelectSingleNode("/Page/RecordSet/@DocumentCount");
            dto.DocumentCount = int.Parse(node.Value);

            node = input.SelectSingleNode("/Page/RecordSet/@CheckCount");
            dto.CheckCount = int.Parse(node.Value);

            node = input.SelectSingleNode("/Page/RecordSet/@CheckTotal");
            dto.CheckTotal = double.Parse(node.Value);

            // Select Fields
            var nodes = input.SelectNodes("/Page/RecordSet/SelectFields/field");
            if (nodes != null)
            {
                foreach (XmlNode field in nodes)
                {
                    dto.SelectFields.Add(new AdvancedSearchSelectFieldResponseDto()
                    {
                        Table = field.Attributes["tablename"].Value == "Checks"
                            ? AdvancedSearchWhereClauseTable.Checks
                            : AdvancedSearchWhereClauseTable.Stubs,
                        FieldName = field.Attributes["fieldname"].Value,
                        DataType = (AdvancedSearchWhereClauseDataType)(int.Parse(field.Attributes["datatype"].Value)),
                        ReportTitle = field.Attributes["reporttitle"].Value,
                        ColumnId = field.Attributes["ColID"].Value
                    });
                }
            }

            return dto;
        }

        public XmlDocument ToXML(AdvancedSearchResponseDto input)
        {
            // Not sure why we would ever need this.
            throw new NotImplementedException();
        }
    }
}
