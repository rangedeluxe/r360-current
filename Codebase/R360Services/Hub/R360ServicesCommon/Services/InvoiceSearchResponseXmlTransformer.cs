﻿using System;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.Common.Services
{
    public class InvoiceSearchResponseXmlTransformer : IXmlTransformer<InvoiceSearchResponseDto>
    {
        public InvoiceSearchResponseDto ToDTO(XmlDocument input)
        {
            var output = new InvoiceSearchResponseDto();
            var doc = XDocument.Parse(input.OuterXml);

            var recordset = doc.Descendants("RecordSet").First();
            var totalrecords = recordset.Attribute("TotalRecords")?.Value;

            output.TotalRecords = string.IsNullOrWhiteSpace(totalrecords) 
                ? 0 
                : long.Parse(totalrecords);

            return output;
        }

        public XmlDocument ToXML(InvoiceSearchResponseDto input)
        {
            throw new NotImplementedException();
        }
    }
}
