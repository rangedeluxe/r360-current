﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class AdvancedSearchWhereClauseDto
    {
        /// <summary>
        /// This means the column will go into the 'whereclause' portion of the XML.
        /// </summary>
        public bool IsDataEntry { get; set; }

        /// <summary>
        /// This means the column will go not into the 'whereclause' portion of the XML.
        /// </summary>
        public bool IsStandard { get; set; }

        /// <summary>
        /// If the column is standard, this is where the data will be housed.
        /// </summary>
        public string StandardXmlColumnName { get; set; }

        public AdvancedSearchWhereClauseTable Table { get; set; }
        public string FieldName { get; set; }
        public string ReportTitle { get; set; }
        public AdvancedSearchWhereClauseOperator Operator { get; set; }

        public AdvancedSearchWhereClauseDataType DataType { get; set; }
        public string Value { get; set; }
        public int BatchSourceKey { get; set; }
        public string OrderByName { get; set; }
        public bool IsDisplayFieldOnly { get; set; }
        public bool IsDefault { get; set; }
    }

    public enum AdvancedSearchWhereClauseTable
    {
        Checks,
        Stubs,
        None
    }

    /// <summary>
    /// NOTE: These need to be in-sync with the advanced search's where clauses as well. (Javascript)
    /// If these are changed, we'll need to update the JS.
    /// </summary>
    public enum AdvancedSearchWhereClauseOperator
    {
        None = 0,
        IsGreaterThan = 1,
        IsLessThan = 2,
        Equals = 3,
        BeginsWith = 4,
        EndsWith = 5,
        Contains = 6
    }

    public enum AdvancedSearchWhereClauseDataType
    {
        String = 1,
        Decimal = 7,
        Float = 6,
        DateTime = 11
    }
}
