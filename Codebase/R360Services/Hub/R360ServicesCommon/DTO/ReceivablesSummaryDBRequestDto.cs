﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class ReceivablesSummaryDBRequestDto
    {
        public IEnumerable<WorkgroupDto> Workgroups { get; set; }
        public DateTime DepositDate { get; set; }
    }
}
