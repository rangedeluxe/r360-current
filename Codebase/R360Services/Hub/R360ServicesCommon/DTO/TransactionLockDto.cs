﻿using System;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class TransactionLockDto
    {
        [MapFrom("RA3MSID")]
        public Guid UserSID { get; set; }
        public string User { get; set; }
        public DateTime LockedDate { get; set; }
        public string UserEntity { get; set; }
        public string LockedDateString { get; set; }
    }
}
