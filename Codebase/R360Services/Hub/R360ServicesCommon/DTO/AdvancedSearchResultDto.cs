﻿using System;
using System.Collections.Generic;

namespace WFS.RecHub.R360Services.Common.DTO
{
    /// <summary>
    /// Houses information for a single row of an advanced search.
    /// </summary>
    public class AdvancedSearchResultDto
    {
        public DateTime DepositDate { get; set; }

        public long BatchId { get; set; }
        public long SourceBatchId { get; set; }
        public int BatchNumber { get; set; }
        public int PaymentSourceId { get; set; }
        public string PaymentSource { get; set; }
        public int PaymentTypeId { get; set; }
        public string PaymentType { get; set; }
        public int TransactionId { get; set; }
        public int TransactionSequence { get; set; }

        public double? PaymentAmount { get; set; }
        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public DateTime PICSDate { get; set; }
        public int BatchSequence { get; set; }

        public IEnumerable<DataEntryDto> DataEntry{ get; set; }
        public string DepositDateString { get; set; }
        public string BatchSourceShortName { get; set; }
        public string ImportTypeShortName { get; set; }
        public bool ShowPaymentIcon { get; set; }
        public int DocumentCount { get; set; }
        public bool ShowDocumentIcon { get; set; }
        public int PICSDateInt { get; set; }
        public int CheckCount { get; set; }
        public bool ShowAllIcon { get; set; }
        public bool IsMarkSense { get; set; }
        public int MarkSenseDocumentCount { get; set; }
    }
}
