﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class OLPreference
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
