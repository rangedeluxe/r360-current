﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositSavePayerSqlDto
    {
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public string RoutingNumber { get; set; }
        public string Account { get; set; }
        public string PayerName { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public bool IsDefault { get; set; } = false;

    }
}
