﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositPendingTransactionsRequestDto
    {
        public int Start { get; set; }
        public int Length { get; set; }
        public string OrderBy { get; set; }
        public string OrderByDirection { get; set; }
        public string Search { get; set; }
    }
}
