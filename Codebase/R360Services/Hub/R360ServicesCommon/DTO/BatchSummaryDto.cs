﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class BatchSummaryDto
    {

        [DataMember]
        public int BankId { get; set; }
        [DataMember]
        public int ClientAccountId { get; set; }
        [DataMember]
        public long BatchID { get; set; }
        [DataMember]
        public long SourceBatchID { get; set; }
        [DataMember]
        public DateTime DepositDate { get; set; }
        [DataMember]
        public string BatchNumber { get; set; }
        [DataMember]
        public string PaymentSource { get; set; }
        [DataMember]
        public string PaymentType { get; set; }
        
        [DataMember]
        public int TransactionCount { get; set; }
        [DataMember]
        public int PaymentCount { get; set; }
        [DataMember]
        public int DocumentCount { get; set; }
        [DataMember]
        public decimal TotalAmount { get; set; }
        [DataMember]
        public int BatchSiteCode { get; set; }
        [DataMember]
        public string DDA { get; set; }
    }
}
