﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PaymentSearchXmlRequestDto
    {

        public PaymentSearchXmlRequestDto()
        {
            Criteria = new List<PaymentSearchCriteriaDto>();
        }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int Length { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public Guid Session { get; set; }
        public int Start { get; set; }
        public List<WorkgroupDto> Workgroups { get; set; }
        public List<PaymentSearchCriteriaDto> Criteria { get; set; }
        public int PaymentType { get; set; }
        public int PaymentSource { get; set; }
    }
}
