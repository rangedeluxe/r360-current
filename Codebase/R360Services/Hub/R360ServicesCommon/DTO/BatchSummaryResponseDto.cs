﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class BatchSummaryResponseDto : BaseResponse
    {
        [DataMember]
        public IEnumerable<BatchSummaryDto> BatchSummaryList { get; set; }
        [DataMember]
        public int TotalRecords { get; set; }
        [DataMember]
        public int FilteredRecords { get; set; }
        [DataMember]
        public int TransactionCount { get; set; }
        [DataMember]
        public int CheckCount { get; set; }
        [DataMember]
        public int DocumentCount { get; set; }
        [DataMember]
        public decimal CheckTotal { get; set; }
        [DataMember]
        public IEnumerable<OLPreference> Preferences { get; set; }
        [DataMember]
        public int SiteCode { get; set; }
        public void AddPreferences(IEnumerable<cOLPreference> prefs)
        {
            var prefoutput = (prefs.Where(
                pref => pref.PreferenceName == "ShowBatchSiteCodeOnline" || 
                        pref.PreferenceName == "ShowBankIDOnline")
                .Select(pref => new OLPreference()
                {
                    Name = pref.PreferenceName,
                    Value = pref.DefaultSetting
                })).ToList();
            Preferences = prefoutput;
        }

    }
}
