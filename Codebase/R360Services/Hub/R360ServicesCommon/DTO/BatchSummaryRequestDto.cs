﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class BatchSummaryRequestDto
    {
        [DataMember]
        public DateTime FromDate { get; set; }
        [DataMember]
        public DateTime ToDate { get; set; }
        [DataMember]
        public int BankId { get; set; }
        [DataMember]
        public int ClientAccountId { get; set; }
        [DataMember]
        public Nullable<int> PaymentTypeId { get; set; }
        [DataMember]
        public Nullable<int> PaymentSourceId { get; set; }
        [DataMember]
        public int Start { get; set; }
        [DataMember]
        public int Length { get; set; }
        [DataMember]
        public string OrderBy { get; set; }
        [DataMember]
        public string OrderDirection { get; set; }
        [DataMember]
        public string Search { get; set; }
    }
}
