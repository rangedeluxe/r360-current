﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class ReceivablesSummaryRequestDto
    {
        public DateTime DepositDate { get; set; }
        public int? BankId { get; set; }
        public int? WorkgroupId { get; set; }
        public int? EntityId { get; set; }
    }
}
