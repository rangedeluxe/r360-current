﻿

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class InvoiceSearchCriteriaDto
    {
        public string XmlColumnName { get; set; }
        public string ReportTitle { get; set; }
        public string Value { get; set; }
        public int DataType { get; set; }
        public InvoiceSearchOperator Operator { get; set; }
    }
}
