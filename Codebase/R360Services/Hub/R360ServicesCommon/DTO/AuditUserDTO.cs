﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common
{
    [DataContract]
    public class AuditUserDTO
    {
        [DataMember(IsRequired = true)]
        public int UserID;

        [DataMember(IsRequired = true)]
        public string LogonName;

        [DataMember(IsRequired = true)]
        public string FirstName;

        [DataMember(IsRequired = true)]
        public string LastName;

        [DataMember(IsRequired = true)]
        public Guid SID;
    }
}
