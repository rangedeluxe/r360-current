﻿

namespace WFS.RecHub.R360Services.Common.DTO
{
    public enum InvoiceSearchOperator
    {
        Contains = 1,
        IsGreaterThanOrEqualTo = 2,
        IsLessThanOrEqualTo = 3,
        Equals = 4
    }
}
