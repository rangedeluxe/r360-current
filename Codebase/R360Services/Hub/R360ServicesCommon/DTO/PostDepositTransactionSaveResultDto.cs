using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PostDepositTransactionSaveResultDto
    {
        [DataMember]
        public IList<PostDepositCheckSaveErrorDto> PaymentExceptions { get; set; } = 
            new List<PostDepositCheckSaveErrorDto>();
        [DataMember]
        public IList<PostDepositStubSaveErrorDto> BusinessRuleExceptions { get; set; } =
            new List<PostDepositStubSaveErrorDto>();

        [DataMember]
        public IList<PostDepositStubSaveErrorDto> DataTypeErrors { get; set; } =
            new List<PostDepositStubSaveErrorDto>();
    }
}