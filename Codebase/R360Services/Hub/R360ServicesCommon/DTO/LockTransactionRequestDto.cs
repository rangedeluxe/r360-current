﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class LockTransactionRequestDto
    {
        public int TransactionId { get; set; }
        public long BatchId { get; set; }
        public DateTime DepositDate { get; set; }
    }
}
