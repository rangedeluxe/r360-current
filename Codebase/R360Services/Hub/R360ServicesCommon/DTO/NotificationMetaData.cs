﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class NotificationMetaData
    {
        public bool HasDownloadPermission { get; set; }
    }
}
