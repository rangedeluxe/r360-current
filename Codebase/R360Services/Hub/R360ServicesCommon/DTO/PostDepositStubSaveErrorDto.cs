﻿using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PostDepositStubSaveErrorDto
    {
        [DataMember]
        public int BatchSequence { get; set; }
        [DataMember]
        public string FieldName { get; set; }
        [DataMember]
        public string Error { get; set; }
    }
}