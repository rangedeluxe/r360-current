﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/23/2013
 * *
 * * Purpose: Defines the client account object containing OLClientAccountID and the 
 * *            DisplayName
 * *        
 * * Modification History
 * WI 100422 CEJ 05/23/2013 Created
 * WI 110739 TWE 08/07/2013
 *    Return description column for display
 * WI 111515 TWE 08/12/2013
 *    Return ShortName column for display Name
* ******************************************************************************/

namespace WFS.RecHub.Common
{
  [DataContract]
  public class ClientAccountModel
  {
    public ClientAccountModel()
    {
    }

    public ClientAccountModel(DataRow drData)
      : this()
    {
      ID = new Guid(drData["OLClientAccountID"].ToString());
      Name = drData["ClientAccountName"].ToString();
    }

    public static Dictionary<string, string> FieldMapping
    {
      get
      {
        return new Dictionary<string, string>() {
                    {"ID", "OLClientAccountID"},
                    {"Name", "ClientAccountName"}
                };
      }
    }

    [DataMember]
    public Guid ID
    {
      get;
      set;
    }

    [DataMember]
    public string Name
    {
      get;
      set;
    }

    [DataMember]
    public int AccountID { get; set; }
  }
}

