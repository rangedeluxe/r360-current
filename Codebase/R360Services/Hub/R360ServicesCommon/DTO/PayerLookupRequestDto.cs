﻿namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PayerLookupRequestDto
    {
        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public string RoutingNumber { get; set; }
        public string Account { get; set; }
        public string PayerName { get; set; }
    }
}
