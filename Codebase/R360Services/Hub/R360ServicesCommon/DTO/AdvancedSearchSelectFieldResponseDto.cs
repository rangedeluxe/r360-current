﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class AdvancedSearchSelectFieldResponseDto
    {

        public AdvancedSearchWhereClauseTable Table { get; set; }
        public string FieldName { get; set; }
        public AdvancedSearchWhereClauseDataType DataType { get; set; }
        public string ReportTitle { get; set; }
        public string ColumnId { get; set; }

    }
}
