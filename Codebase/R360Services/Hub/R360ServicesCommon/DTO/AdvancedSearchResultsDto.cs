﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class AdvancedSearchResultsDto : BaseResponse
    {
        public AdvancedSearchResponseDto Totals { get; set; }
        public List<AdvancedSearchResultDto> Results { get; set; }
        public AdvancedSearchMetadata Metadata { get; set; }
    }
}
