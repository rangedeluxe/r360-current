﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PostDepositCheckSaveErrorDto
    {
        [DataMember]
        public int CheckSequence { get; set; }
        [DataMember]
        public string FieldName { get; set; }
        [DataMember]
        public string Error { get; set; }
    }
}
