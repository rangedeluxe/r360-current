﻿namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositStubDataEntryExceptionDto
    {
        public int BatchSequence { get; set; }
        public int StubSequence { get; set; }
        public string FieldName { get; set; }
        public string Error { get; set; }
    }
}