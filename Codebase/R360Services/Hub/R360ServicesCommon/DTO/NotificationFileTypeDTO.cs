﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class NotificationFileTypeDto
    {
        [DataMember]
        public int NotificationFileTypeKey { get; set; }
        [DataMember]
        public string FileTypeDescription { get; set; }
    }
}
