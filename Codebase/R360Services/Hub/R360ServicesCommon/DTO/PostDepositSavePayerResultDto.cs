﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PostDepositSavePayerResultDto
    {
        [DataMember]
        public bool Success { get; set; }
        [DataMember]
        public List<string> ErrorList { get; set; } = new List<string>();
    }
}
