﻿using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PostDepositDataEntryFieldSaveDto
    {
        [DataMember]
        public string FieldName { get; set; }
        [DataMember]
        public string Value { get; set; }
    }
}