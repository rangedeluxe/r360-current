﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositTransactionsResponseDto
    {
        public List<PostDepositTransactionResponseDto> Transactions { get; set; }
        public int TotalRecords { get; set; }
        public int TotalFilteredRecords { get; set; }
    }
}
