﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class NotificationsRequestDto
    {
        [DataMember]
        public DateTime StartDate { get; set; }
        [DataMember]
        public  DateTime EndDate { get; set; }
        [DataMember]
        public  string SortBy { get; set; }
        [DataMember]
        public string SortByDir { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public int FileTypeKey { get; set; }
        [DataMember]
        public  int StartRecord { get; set; }
        [DataMember]
        public int RecordsPerPage { get; set; }
        [DataMember]
        public string Search { get; set; }
        [DataMember]
        public string Workgroup { get; set; }
        [DataMember]
        public int UtcOffset { get; set; }
        public List<WorkgroupDto> ClientAccounts { get; set; }

    }
}
