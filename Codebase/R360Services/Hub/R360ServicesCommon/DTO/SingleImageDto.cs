﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class SingleImageDto
    {
        public long BatchId { get; set; }
        public int BatchSequence { get; set; }
        public DateTime DepositDate { get; set; }
        public bool IsCheck { get; set; }
        public string SiteKey { get; set; }
        public Guid SessionId { get; set; }
        public OLFImageDisplayMode DisplayMode { get; set; }
    }
}
