﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class NotificationDto
    {
        [DataMember]
        public DateTime Date { get; set; }  
        [DataMember]
        public string DateString { get; set; }
        [DataMember]
        public  string Id { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public  int AttachmentCount { get; set; }
        [DataMember]
        public bool ViewNotificationIcon { get; set; }
        [DataMember]
        public int MessageGroup { get; set; }
        [DataMember]
        public List<NotificationFileDto> Files { get; set; }
    }
}