﻿using System;
using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class BatchDetailRequestDto
    {
        [DataMember]
        public int WorkgroupId { get; set; }

        [DataMember]
        public long BatchId { get; set; }

        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public DateTime DepositDate { get; set; }

        [DataMember]
        public int Start { get; set; }

        [DataMember]
        public int Length { get; set; }

        [DataMember]
        public string OrderBy { get; set; }

        [DataMember]
        public string OrderDirection { get; set; }

        [DataMember]
        public string Search { get; set; }
    }
}