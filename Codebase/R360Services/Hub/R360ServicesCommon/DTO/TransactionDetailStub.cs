﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class TransactionDetailStubs
    {
        [DataMember]
        public int StubSequence { get; set; }
        [DataMember]
        public double Amount { get; set; }
        [DataMember]
        public bool StubIconVisible { get; set; }
        [DataMember]
        public int BatchSequence { get; set; }
        [DataMember]
        public bool ItemReportIconVisible { get; set; }
        [DataMember]
        public string SourceProcessingDate { get; set; }
        [DataMember]
        public int DocumentBatchSequence { get; set; }
        [DataMember]
        public bool IsLinkedToDocument { get; set; }
        [DataMember]
        public IEnumerable<DataEntryDto> DataEntryFields { get; set; }
        public int TransactionId { get; set; }
    }
}
