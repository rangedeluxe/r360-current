﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class AdvancedSearchMetadata
    {
        public string WorkgroupName { get; set; }
        public int WorkgroupId { get; set; }
        public bool DisplayBatchId { get; set; }
        public int MaxRowsForDownload { get; set; }
        public bool HasDownloadPermission { get; set; }
        public string CriteraString { get; set; }
        public bool HasPayments { get; set; }
        public bool HasDocuments { get; set; }
    }
}
