﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class NotificationRequestDto
    {
        public int MessageGroup { get; set; }
        public int UtcOffset { get; set; }
    }
}
