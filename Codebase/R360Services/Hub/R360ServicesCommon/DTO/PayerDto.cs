﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PayerDto
    {
        public string RoutingNumber { get; set; }
        public string Account { get; set; }
        public string PayerName { get; set; }
        public string CreatedBy { get; set; }
        public bool IsDefault { get; set; }
    }
}
