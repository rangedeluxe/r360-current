﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class AdvancedSearchResponseDto
    {
        public AdvancedSearchResponseDto()
        {
            SelectFields = new List<AdvancedSearchSelectFieldResponseDto>();
        }
        public int CheckCount { get; set; }
        public double CheckTotal { get; set; }
        public int DocumentCount { get; set; }
        public int TotalRecords { get; set; }
        public List<AdvancedSearchSelectFieldResponseDto> SelectFields { get; set; }
    }
}
