﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class NotificationsFileTypesResponseDto : BaseResponse
    {
        [DataMember]
        public IEnumerable<NotificationFileTypeDto> FileTypes { get; set; }
    }
}
