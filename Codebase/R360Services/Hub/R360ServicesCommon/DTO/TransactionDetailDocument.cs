﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class TransactionDetailDocument
    {
        [DataMember]
        public int DocumentSequence { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string PicsDate { get; set; }
        [DataMember]
        public bool DocumentIconVisible { get; set; }
        [DataMember]
        public string SourceProcessingDate { get; set; }
        [DataMember]
        public int BatchSequence { get; set; }
        [DataMember]
        public string FileDescriptor { get; set; }
        [DataMember]
        public string BatchSourceShortName { get; set; }
        [DataMember]
        public string ImportTypeShortName { get; set; }
        [DataMember]
        public int SequenceWithinTransaction { get; set; }
    }
}