﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositTransactionRequestDto
    {
        public long BatchId { get; set; }
        public int TransactionId { get; set; }
        public DateTime DepositDate { get; set; }
        public int RecordsTotal { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
    }
}
