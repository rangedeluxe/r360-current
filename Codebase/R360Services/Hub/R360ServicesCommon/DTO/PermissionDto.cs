﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PermissionDto
    {
        public bool HasPermission { get; set; }
        public string Permission { get; set; }
    }
}
