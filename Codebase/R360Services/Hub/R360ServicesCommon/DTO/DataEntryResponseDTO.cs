﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class DataEntryResponseDTO : BaseResponse
    {
        public List<DataEntryResponse> DataEntryFields { get; set; }
    }
}
