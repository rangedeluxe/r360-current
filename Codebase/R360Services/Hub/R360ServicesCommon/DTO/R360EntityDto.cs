﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class R360EntityDto
    {
        public int EntityId { get; set; }
        public bool DisplayBatchId { get; set; }
        public int PaymentImageDisplayMode { get; set; }
        public int DocumentImageDisplayMode { get; set; }
        public int ViewingDays { get; set; }
        public int MaximumSearchDays { get; set; }
    }
}
