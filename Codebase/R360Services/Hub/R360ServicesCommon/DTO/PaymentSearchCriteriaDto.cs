﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PaymentSearchCriteriaDto
    {
        public string XmlColumnName { get; set; }
        public string ReportTitle { get; set; }
        public string Value { get; set; }
        public int DataType { get; set; }
        public PaymentSearchOperator Operator { get; set; }
    }
}
