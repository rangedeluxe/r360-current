﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PostDepositStubSaveDto
    {
        [DataMember]
        public int BatchSequence { get; set; }
        [DataMember]
        public IList<PostDepositDataEntryFieldSaveDto> DataEntryFields { get; set; } =
            new List<PostDepositDataEntryFieldSaveDto>();
    }
}