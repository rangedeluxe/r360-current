﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/23/2013
 * *
 * * Purpose: Defines the payment type total object containing the total deposited for a
 * *            given payment type
 * *
 * * Modification History
 * WI 100422 CEJ 05/23/2013 Created
* ******************************************************************************/

namespace WFS.RecHub.Common {
    [DataContract]
    public class PieSliceModel {
        public PieSliceModel() {
        }

        public PieSliceModel(string sPaymentType) {
            PaymentType = sPaymentType;
        }

        public PieSliceModel(DataRow drData)
        {
            LoadData(
                drData["PaymentType"].ToString(),
                (int)(drData["TransactionCount"] ?? 0),
                Convert.ToDouble(drData["DepositTotal"] ?? 0.0),
                Convert.ToInt32(drData["PaymentTypeKey"] ?? 0));
        }

        public PieSliceModel(XElement xmeData) {
            LoadData(
                xmeData.Attribute("PaymentType").Value,
                Convert.ToInt32(xmeData.Attribute("ItemCount").Value),
                Convert.ToInt32(xmeData.Attribute("ItemTotal").Value));
        }

        public static Dictionary<string, string> FieldMapping {
            get {
                return new Dictionary<string, string>() {
                    {"PaymentType", "PaymentType"},
                    {"ItemCount", "TransactionCount"},
                    {"ItemTotal", "DepositTotal"}
                };
            }
        }

        public XElement ToXml() {
            return new XElement("PaymentPieModel",
                    new XAttribute("PaymentType", PaymentType),
                    new XAttribute("ItemCount", ItemCount),
                    new XAttribute("ItemTotal", ItemTotal));
        }

        public void LoadData(
            string sPaymentType,
            int iItemCount,
            double dTotal,
            int paymentTypeKey = 0)
        {
            PaymentType = sPaymentType;
            ItemCount = iItemCount;
            ItemTotal = dTotal;
            PaymentTypeKey = paymentTypeKey;
        }

        [DataMember]
        public string PaymentType {
            get;
            set;
        }

        [DataMember]
        public int PaymentTypeKey { get; set; }

        [DataMember]
        public int ItemCount {
            get;
            set;
        }

        [DataMember]
        public double ItemTotal {
            get;
            set;
        }
    }
}
