﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class TransactionDetailResponseDto : BaseResponse
    {
        [DataMember]
        public int BankID { get; set; }
        [DataMember]
        public int WorkgroupID { get; set; }
        [DataMember]
        public string WorkgroupDisplayName { get; set; }
        [DataMember]
        public DateTime DepositDate { get; set; }

        [DataMember]
        public int DepositDateKey { get; set; } 
        [DataMember]
        public int AccountSiteCode { get; set; }
        [DataMember]
        public int BatchNumber { get; set; }
        [DataMember]
        public int SourceBatchID { get; set; }
        [DataMember]
        public int BatchID { get; set; }
        [DataMember]
        public int BatchSiteCode { get; set; }
        [DataMember]
        public int BatchCueID { get; set; }
        [DataMember]
        public int TransactionId { get; set; }
        [DataMember]
        public int TransactionNumber { get; set; }
        [DataMember]
        public bool DisplayBatchID { get; set; }
        [DataMember]
        public bool ViewAllImagesIconVisible { get; set; }
        [DataMember]
        public int PaymentType { get; set; }
        [DataMember]
        public string BatchSourceShortName { get; set; }
        [DataMember]
        public string PaymentTypeShortName { get; set; }
        [DataMember]
        public int PaymentSource { get; set; }
        [DataMember]
        public double BatchTotal { get; set; }
        [DataMember]
        public int PICSDate { get; set; }
        [DataMember]
        public string ImportTypeShortName { get; set; }
        [DataMember]
        public bool IsElectronic { get; set; }
        [DataMember]
        public IEnumerable<TransactionDetailPayment> Payments { get; set; }
        [DataMember]
        public IEnumerable<TransactionDetailStubs> Stubs { get; set; }
        [DataMember]
        public IEnumerable<TransactionDetailDocument> Documents { get; set; }

        [DataMember]
        public IEnumerable<OLPreference> Preferences { get; set; }
        [DataMember]
        public IEnumerable<Tuple<int, int>> BatchTransactions { get; set; }

        [DataMember]
        public string FileGroup { get; set; }

        public void AddPreferences(IEnumerable<cOLPreference> prefs)
        {
            var prefoutput = new List<OLPreference>();
            foreach (var pref in prefs)
            {
                prefoutput.Add(new OLPreference()
                {
                    Name = pref.PreferenceName,
                    Value = pref.DefaultSetting
                });
            }
            Preferences = prefoutput;
        }

        public void AddTransactionDetails(DataTable dtDeails)
        {
            if (dtDeails.Rows.Count > 0)
            {
                int bankId = int.TryParse(dtDeails.Rows[0]["BankID"].ToString(), out bankId) ? bankId : 0;
                int workGroupId = int.TryParse(dtDeails.Rows[0]["LockBoxID"].ToString(), out workGroupId) ? workGroupId : 0;
                DateTime deposit = DateTime.TryParse(dtDeails.Rows[0]["DepositDate"].ToString(), out deposit) ? deposit : DateTime.MinValue;
                int depDateKey = int.TryParse(deposit.ToString("yyyyMMdd"), out depDateKey) ? depDateKey : 0;
                int batchNum = int.TryParse(dtDeails.Rows[0]["BatchNumber"].ToString(), out batchNum) ? batchNum : 0;
                int batchSiteCode = int.TryParse(dtDeails.Rows[0]["BatchSiteCode"].ToString(), out batchSiteCode) ? batchSiteCode : 0;
                int batchCue = int.TryParse(dtDeails.Rows[0]["BatchCueID"].ToString(), out batchCue) ? batchCue : 0;
                int transactionID = int.TryParse(dtDeails.Rows[0]["TransactionID"].ToString(), out transactionID) ? transactionID : 0;
                int transactionSequence = int.TryParse(dtDeails.Rows[0]["TxnSequence"].ToString(), out transactionSequence) ? transactionSequence: 0;
                int sourceBatchID = int.TryParse(dtDeails.Rows[0]["SourceBatchID"].ToString(), out sourceBatchID) ? sourceBatchID : 0;
                int batchID = int.TryParse(dtDeails.Rows[0]["BatchID"].ToString(), out batchID) ? batchID : 0;
                int siteCode = int.TryParse(dtDeails.Rows[0]["SiteCodeID"].ToString(), out siteCode) ? siteCode : 0;
                DateTime picsDt = DateTime.TryParse(dtDeails.Rows[0]["PICSDate"].ToString(), out picsDt) ? picsDt : DateTime.MinValue;
                int picsNum = int.TryParse(picsDt.ToString("yyyyMMdd"), out picsNum) ? picsNum : 0;

                BankID = bankId;
                WorkgroupID = workGroupId;
                WorkgroupDisplayName = dtDeails.Rows[0]["LongName"].ToString();
                DepositDate = deposit;
                DepositDateKey = depDateKey;
                AccountSiteCode = siteCode;
                BatchNumber = batchNum;
                BatchSiteCode = batchSiteCode;
                BatchCueID = batchCue;
                TransactionId = transactionID;
                TransactionNumber = transactionSequence;
                SourceBatchID = sourceBatchID;
                BatchID = batchID;
                PICSDate = picsNum;
                ImportTypeShortName = dtDeails.Rows[0]["ImportTypeShortName"].ToString();
                BatchSourceShortName = dtDeails.Rows[0]["BatchSourceShortName"].ToString();
                PaymentTypeShortName = dtDeails.Rows[0]["PaymentTypeShortName"].ToString();
                //Electronic batches are defined by ACH or Wire
                IsElectronic = PaymentTypeShortName.ToUpper() == "ACH" || PaymentTypeShortName.ToUpper() == "WIRE";
            }
        }

        public void AddPayments(DataTable dtPayments)
        {
            var checks = new List<TransactionDetailPayment>();
            foreach (DataRow drTransaction in dtPayments.Rows)
            {
                double amount = double.TryParse(drTransaction["Amount"].ToString(), out amount) ? amount : 0;
                int picsDate = int.TryParse(drTransaction["PICSDate"].ToString(), out picsDate) ? picsDate : 0;
                int batchSequence = int.TryParse(drTransaction["BatchSequence"].ToString(), out batchSequence) ? batchSequence : 0;
                int checkSeq = int.TryParse(drTransaction["CheckSequence"].ToString(), out checkSeq) ? checkSeq : 0;
                int transactionid = int.TryParse(drTransaction["TransactionID"].ToString(), out transactionid) ? transactionid : 0;
                DateTime procDate;
                DateTime.TryParseExact(drTransaction["SourceProcessingDateKey"].ToString(), "yyyyMMdd",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out procDate);

                checks.Add(new TransactionDetailPayment
                {
                    PaymentSequence = checkSeq,
                    RT = drTransaction["RT"].ToString(),
                    AccountNumber = drTransaction["Account"].ToString(),
                    CheckTraceRefNumber = drTransaction["Serial"].ToString(),
                    Payer = drTransaction["RemitterName"].ToString(),
                    DDA = drTransaction["DDA"].ToString(),
                    PaymentAmount = amount,
                    PICSDate = picsDate,
                    BatchSequence = batchSequence,
                    BatchSourceShortName =  drTransaction["BatchSourceShortName"].ToString(),
                    SourceProcessingDate = procDate.ToShortDateString(),
                    TransactionId = transactionid
                });
            }
            Payments = checks;
        }
        public void AddStubs(DataTable dtStubs)
        {
            var stubs = new List<TransactionDetailStubs>();
            foreach (DataRow row in dtStubs.Rows)
            {
                double amount = double.TryParse(row["Amount"].ToString(), out amount) ? amount : 0;
                int stubSeq = int.TryParse(row["StubSequence"].ToString(), out stubSeq) ? stubSeq : 0;
                int bSeq = int.TryParse(row["BatchSequence"].ToString(), out bSeq) ? bSeq : 0;
                int docbatchseq = int.TryParse(row["DocumentBatchSequence"].ToString(), out docbatchseq) ? docbatchseq : 0;
                //A basic rule about seq numbers is they must be great then 0 to be valid. Rather then having that logic everywhere, set a flag here.
                var isLinkedToDocument = docbatchseq > 0;
                int transactionid = int.TryParse(row["TransactionID"].ToString(), out transactionid) ? transactionid : 0;
                DateTime procDate;
                DateTime.TryParseExact(row["SourceProcessingDateKey"].ToString(), "yyyyMMdd",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out procDate);
                stubs.Add(
                        new TransactionDetailStubs
                        {
                            Amount = amount,
                            StubSequence = stubSeq,
                            BatchSequence = bSeq,
                            StubIconVisible = true,
                            SourceProcessingDate = procDate.ToShortDateString(),
                            DocumentBatchSequence = docbatchseq,
                            IsLinkedToDocument = isLinkedToDocument,
                            TransactionId = transactionid
                        }
                    );
            }
            Stubs = stubs;
        }

        public void AddDocuments(DataTable dtDocs)
        {
            var documents = new List<TransactionDetailDocument>();
            foreach (DataRow document in dtDocs.Rows)
            {
                int docSequence = int.TryParse(document["DocumentSequence"].ToString(), out docSequence) ? docSequence : 0;
                int batchSequence = int.TryParse(document["BatchSequence"].ToString(), out batchSequence) ? batchSequence : 0;
                int sequenceWithinTransaction =
                    int.TryParse(document["SequenceWithinTransaction"].ToString(), out sequenceWithinTransaction)
                        ? sequenceWithinTransaction
                        : 0;

                DateTime procDate;
                DateTime.TryParseExact(document["SourceProcessingDateKey"].ToString(), "yyyyMMdd",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out procDate);
                documents.Add(
                    new TransactionDetailDocument
                    {
                        DocumentSequence = docSequence,
                        Description = document["Description"].ToString(),
                        PicsDate = document["PICSDate"].ToString(),
                        BatchSequence = batchSequence,
                        DocumentIconVisible = true,
                        SourceProcessingDate = procDate.ToShortDateString(),
                        FileDescriptor = document["FileDescriptor"].ToString(),
                        BatchSourceShortName = document["BatchSourceShortName"].ToString(),
                        ImportTypeShortName = document["ImportTypeShortName"].ToString(),
                        SequenceWithinTransaction = sequenceWithinTransaction
                    });
            }
            Documents = documents;
        }

        public void AddBatchTransactions(DataTable dtTrans)
        {
            var trans = new List<Tuple<int, int>>();
            foreach (DataRow row in dtTrans.Rows)
            {
                int id = int.TryParse(row["TransactionID"].ToString(), out id) ? id : 0;
                int seq = int.TryParse(row["TxnSequence"].ToString(), out seq) ? seq : 0;
                trans.Add(new Tuple<int, int>(id, seq));
            }
            BatchTransactions = trans;
        }

    }
}
