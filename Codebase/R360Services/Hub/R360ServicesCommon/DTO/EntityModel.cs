﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFS.RecHub.Common 
{
  public class EntityModel
  {
    public string id { get; set; }

    public string label { get; set; }

    public string icon { get; set; }

    public long value { get; set; }
    
    public bool isNode { get; set; }
    
    public IList<EntityModel> items { get; set; }

    public EntityModel()
    {
      items = new List<EntityModel>();
    }
  }
}