﻿

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class InvoiceSearchMetadata
    {
        public bool DisplayBatchId { get; set; }
    }
}
