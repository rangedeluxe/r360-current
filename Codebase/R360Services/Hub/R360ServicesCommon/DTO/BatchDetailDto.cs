﻿using System;
using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class BatchDetailDto
    {
        [DataMember]
        public string Workgroup { get; set; }

        [DataMember]
        public int WorkgroupId { get; set; }

        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public string SiteCode { get; set; }

        [DataMember]
        public long SourceBatchId { get; set; }

        [DataMember]
        public long BatchId { get; set; }

        [DataMember]
        public string BatchNumber { get; set; }

        [DataMember]
        public string PaymentSource { get; set; }

        [DataMember]
        public string PaymentType { get; set; }

        [DataMember]
        public DateTime DepositDate { get; set; }

        [DataMember]
        public int TransactionCount { get; set; }

        [DataMember]
        public int PaymentCount { get; set; }

        [DataMember]
        public int DocumentCount { get; set; }

        [DataMember]
        public decimal TotalAmount { get; set; }

        [DataMember]
        public string PICSDate { get; set; }

        [DataMember]
        public DateTime ProcessingDate { get; set; }

        [DataMember]
        public int TransactionID { get; set; }

        [DataMember]
        public int TxnSequence { get; set; }

        [DataMember]
        public decimal BatchTotal { get; set; }

        [DataMember]
        public int BatchSequence { get; set; }

        [DataMember]
        public string Serial { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public string RT { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string RemitterName { get; set; }

        [DataMember]
        public int CheckCount { get; set; }

        [DataMember]
        public int StubCount { get; set; }


        [DataMember]
        public int BatchSiteCode { get; set; }

        [DataMember]
        public int BatchCueID { get; set; }

        [DataMember]
        public string DDA { get; set; }

        [DataMember]
        public int ImportTypeKey { get; set; }

        [DataMember]
        public string BatchSourceShortName { get; set; }

        [DataMember]
        public string ImportTypeShortName { get; set; }

        [DataMember]
        public int AccountSiteCode { get; set; }

        [DataMember]
        public bool IsElectronic { get; set; }

        [DataMember]
        public bool ShowPaymentIcon { get; set; }

        [DataMember]
        public bool ShowDocumentIcon { get; set; }

        [DataMember]
        public bool ShowAllIcon { get; set; }

        [DataMember]
        public int DocumentBatchSequence { get; set; }

	    [DataMember]
	    public string FileGroup { get; set; }
    }
}