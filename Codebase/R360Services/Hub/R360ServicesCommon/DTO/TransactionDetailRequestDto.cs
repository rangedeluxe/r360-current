﻿using System;
using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class TransactionDetailRequestDto
    {
        [DataMember]
        public int BankId { get; set; }
        [DataMember]
        public int WorkgroupID { get; set; }
        [DataMember]
        public DateTime DepositDate { get; set; }
        [DataMember]
        public long BatchID { get; set; }
        [DataMember]
        public int TransactionId { get; set; }
        [DataMember]
        public int TransactionSequence { get; set; }
    }
}
