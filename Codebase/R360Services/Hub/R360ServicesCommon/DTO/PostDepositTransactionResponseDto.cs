﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositTransactionResponseDto
    {
        [MapFrom("SiteBankID, BankID")]
        public int BankId { get; set; }
        [MapFrom("SiteClientAccountID, ClientAccountID")]
        public int WorkgroupId { get; set; }
        public string Workgroup { get; set; }
        [MapFrom("BatchID")]
        public long BatchId { get; set; }
        [MapFrom("SourceBatchID")]
        public long SourceBatchId { get; set; }
        [MapFrom("TransactionID")]
        public int TransactionId { get; set; }
        [MapFrom("TxnSequence")]
        public int TransactionSequence { get; set; }
        [MapFrom("TransactionExceptionStatusKey")]
        public int TransactionExceptionStatus { get; set; }
        [MapFrom("CheckTotal")]
        public decimal PaymentTotal { get; set; }
        [MapFrom("Deposit_Date")]
        public DateTime DepositDate { get; set; }
        [MapFrom("CheckCount")]
        public int PaymentCount { get; set; }
        public int StubCount { get; set; }
        public int DocumentCount { get; set; }
        public int BatchNumber { get; set; }
        public string PaymentType { get; set; }
        public string PaymentSource { get; set; }
        public int PaymentSourceKey { get; set; }
        [MapFrom("EntityID")]
        public int EntityId { get; set; }
        public string EntityBreadcrumb { get; set; }
        public IEnumerable<PostDepositExceptionTransactionDto> Transactions { get; set; }
        public IEnumerable<PaymentDto> Payments { get; set; }
        public IEnumerable<StubDto> Stubs { get; set; }
        public IEnumerable<DocumentDto> Documents { get; set; }
        public IEnumerable<DataEntrySetupDto> DataEntrySetupList { get; set; }
        public TransactionLockDto Lock { get; set; }
        public bool UserCanUnlock { get; set; }
        public bool UserCanOverrideUnlock { get; set; }
    }
}
