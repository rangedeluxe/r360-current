﻿using System.Collections.Generic;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositStubSaveSqlDto
    {
        public int BatchSequence { get; set; }
        public IList<PostDepositDataEntryFieldSaveSqlDto> DataEntryFields { get; set; } =
            new List<PostDepositDataEntryFieldSaveSqlDto>();
    }
}