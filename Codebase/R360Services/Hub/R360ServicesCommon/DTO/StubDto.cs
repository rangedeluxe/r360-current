﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class StubDto
    {
        public int StubSequence { get; set; }
        public decimal Amount { get; set; }
        public string AccountNumber { get; set; }
        public bool StubIconVisible { get; set; }
        public int BatchSequence { get; set; }
        public bool ItemReportIconVisible { get; set; }
        public string SourceProcessingDate
        {
            get
            {
                return DateTime
                    .ParseExact(SourceProcessingDateKey.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture)
                    .ToString("MM/dd/yyyy");
            }
        }
        public int SourceProcessingDateKey { get; set; }
        public int DocumentBatchSequence { get; set; }
        public IEnumerable<DataEntryValueDto> DataEntryFields { get; set; }
        public int TransactionId { get; set; }
        
    }
}
