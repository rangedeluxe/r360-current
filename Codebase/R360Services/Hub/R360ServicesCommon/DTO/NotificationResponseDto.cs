﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class NotificationResponseDto : BaseResponse
    {
        public NotificationDto Notification { get; set; }
        public NotificationMetaData MetaData { get; set; }
    }
}
