using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class TransactionDetailPayment
    {

        [DataMember]
        public int PaymentSequence { get; set; }
        [DataMember]
        public string RT { get; set; }
        [DataMember]
        public string AccountNumber { get; set; }
        [DataMember]
        public string CheckTraceRefNumber { get; set; }
        [DataMember]
        public string Payer { get; set; }
        [DataMember]
        public string DDA { get; set; }
        [DataMember]
        public double PaymentAmount { get; set; }
        [DataMember]
        public bool CheckIconVisible { get; set; }
        [DataMember]
        public bool ItemReportIconVisible { get; set; }
        [DataMember]
        public int PICSDate { get; set; }
        [DataMember]
        public int BatchSequence { get; set; }
        [DataMember]
        public string BatchSourceShortName { get; set; }
        [DataMember]
        public  string SourceProcessingDate { get; set; }
        [DataMember]
        public IEnumerable<DataEntryDto> DataEntryFields { get; set; }
        public int TransactionId { get; set; }
    }
}