﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
	public class PostDepositCheckSaveDto
	{
		[DataMember]
		public int CheckSequence { get; set; }
		[DataMember]
		public string Payer { get; set; }
		
	}
}
