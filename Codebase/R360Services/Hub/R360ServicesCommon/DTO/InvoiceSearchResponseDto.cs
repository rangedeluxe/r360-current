﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class InvoiceSearchResponseDto : BaseResponse
    {

        public InvoiceSearchResponseDto()
        {
            Results = new List<InvoiceSearchResultDto>();
        }

        [DataMember]
        public List<InvoiceSearchResultDto> Results { get; set; }
        [DataMember]
        public long TotalRecords { get; set; }
        [DataMember]
        public InvoiceSearchMetadata Metadata { get; set; }

    }
}
