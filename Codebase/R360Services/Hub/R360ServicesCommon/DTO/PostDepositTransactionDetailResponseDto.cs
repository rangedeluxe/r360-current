﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositTransactionDetailResponseDto
    {
        [MapFrom("SiteBankID, BankID")]
        public int BankId { get; set; }
        [MapFrom("SiteClientAccountID, ClientAccountID")]
        public int WorkgroupId { get; set; }
        [CanBeNull]
        public string Workgroup { get; set; }
        [MapFrom("BatchID")]
        public long BatchId { get; set; }
        [MapFrom("SourceBatchID")]
        public long SourceBatchId { get; set; }
        [MapFrom("TransactionID")]
        public int TransactionId { get; set; }
        [MapFrom("TxnSequence")]
        public int TransactionSequence { get; set; }
        [MapFrom("Deposit_Date")]
        public DateTime DepositDate { get; set; }
        [CanBeNull]
        public string PaymentType { get; set; }
        [CanBeNull]
        public string PaymentSource { get; set; }
        public int PaymentSourceKey { get; set; }
        [MapFrom("EntityID")]
        public int EntityId { get; set; }
        public bool PostDepositPayerRequired { get; set; }
        [CanBeNull]
        public string EntityBreadcrumb { get; set; }
        [CanBeNull]
        public IEnumerable<PostDepositExceptionTransactionDto> Transactions { get; set; }
        [CanBeNull]
        public IEnumerable<PaymentDto> Payments { get; set; }
        [CanBeNull]
        public IEnumerable<PostDepositStubDetailDto> Stubs { get; set; }
        [CanBeNull]
        public IEnumerable<DocumentDto> Documents { get; set; }
        [CanBeNull]
        public IEnumerable<DataEntrySetupDto> DataEntrySetupList { get; set; }
        [CanBeNull]
        public TransactionLockDto Lock { get; set; }
    }
}
