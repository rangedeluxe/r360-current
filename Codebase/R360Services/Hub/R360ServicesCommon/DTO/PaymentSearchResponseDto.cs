﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PaymentSearchResponseDto : BaseResponse
    {

        public PaymentSearchResponseDto()
        {
            Results = new List<PaymentSearchResultDto>();
        }

        [DataMember]
        public List<PaymentSearchResultDto> Results { get; set; }
        [DataMember]
        public long TotalRecords { get; set; }
        [DataMember]
        public PaymentSearchMetadata Metadata { get; set; }

    }
}
