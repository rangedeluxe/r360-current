﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PostDepositTransactionSaveDto
    {
        [DataMember]
        public DateTime DepositDate { get; set; }
        [DataMember]
        public long BatchId { get; set; }
        [DataMember]
        public long SourceBatchId { get; set; }
        [DataMember]
        public int TransactionId { get; set; }
        [DataMember, CanBeNull]
        public IList<PostDepositStubSaveDto> Stubs { get; set; } = new List<PostDepositStubSaveDto>();
        [DataMember]
        public IList<PostDepositCheckSaveDto> Checks { get; set; } = new List<PostDepositCheckSaveDto>();
        [DataMember]
        public IList<PostDepositSavePayerDto> Payers { get; set; } = new List<PostDepositSavePayerDto>();
    }
}
