﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/23/2013
 * *
 * * Purpose: Defines the organization object containing the OLOrganizationID and the 
 * *            Organization Code
 * *
 * * Modification History
 * WI 100422 CEJ 05/23/2013 Code & Unit Test Get Account Summary Method
 *           RDS 06/23/2014 Organization concept has been removed - use RAAM Entities
* ******************************************************************************/
/*
namespace WFS.RecHub.Common
{
    [DataContract]
    public class OrganizationModel
    {
        public OrganizationModel()
        {
        }

        public OrganizationModel(DataRow drData)
            : this()
        {
            ID = new Guid(drData["OLOrganizationID"].ToString());
            Code = drData["OrganizationCode"].ToString();
        }

        public OrganizationModel(XElement xmeData)
            : this()
        {
            ID = new Guid(xmeData.Attribute("OrganizationID").Value);
            Code = xmeData.Attribute("OrganizationCode").Value;
        }

        public static Dictionary<string, string> FieldMapping
        {
            get
            {
                return new Dictionary<string, string>() {
                    {"ID", "OLOrganizationID"},
                    {"Code", "OrganizationCode"}
                };
            }
        }

        public XElement ToXML()
        {
            return new XElement("OrganizationModel",
                    new XAttribute("OrganizationID", ID.ToString()),
                    new XAttribute("OrganizationCode", Code));
        }

        [DataMember]
        public Guid ID
        {
            get;
            set;
        }

        [DataMember]
        public string Code
        {
            get;
            set;
        }
    }
}
*/