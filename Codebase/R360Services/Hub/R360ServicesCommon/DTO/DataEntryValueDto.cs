﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class DataEntryValueDto
    {
        [MapFrom("FldName")]
        public string FieldName { get; set; }
        [MapFrom("FldTitle")]
        public string Title { get; set; }
        [MapFrom("FldDataTypeEnum")]
        public int Type { get; set; }
        [MapFrom("DataEntryValue")]
        public string Value { get; set; }

        public bool IsEditable { get; set; }
        public bool IsUIAddedField { get; set; }
    }
}
