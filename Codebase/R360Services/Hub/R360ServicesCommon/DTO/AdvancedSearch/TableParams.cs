﻿using System.Data;

namespace WFS.RecHub.R360Services.Common.DTO.AdvancedSearch
{
    public class TableParams
    {
        public DataTable BaseRequest { get; set; }
        public DataTable WhereClause { get; set; }
        public DataTable SelectFields { get; set; }
    }
}
