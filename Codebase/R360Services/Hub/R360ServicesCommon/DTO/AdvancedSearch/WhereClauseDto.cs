﻿using WFS.RecHub.ApplicationBlocks.DataAccess.Interfaces;

namespace WFS.RecHub.R360Services.Common.DTO.AdvancedSearch
{
    public class WhereClauseDto : ITableParam
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int DataType { get; set; }
        public string ReportTitle { get; set; }
        public string Operator { get; set; }
        public int BatchSourceKey { get; set; }
        public string DataValue { get; set; }

        public static WhereClauseDto FromRequest(AdvancedSearchWhereClauseDto dto)
        {
            return new WhereClauseDto()
            {
                TableName = dto.Table == AdvancedSearchWhereClauseTable.Checks
                    ? "Checks"
                    : dto.Table == AdvancedSearchWhereClauseTable.Stubs
                    ? "Stubs"
                    : string.Empty,
                FieldName = dto.FieldName,
                DataType = ((int)dto.DataType),
                ReportTitle = dto.ReportTitle,
                Operator = dto.Operator == AdvancedSearchWhereClauseOperator.BeginsWith
                    ? "Begins With"
                    : dto.Operator == AdvancedSearchWhereClauseOperator.Contains
                    ? "Contains"
                    : dto.Operator == AdvancedSearchWhereClauseOperator.EndsWith
                    ? "Ends With"
                    : dto.Operator == AdvancedSearchWhereClauseOperator.Equals
                    ? "Equals"
                    : dto.Operator == AdvancedSearchWhereClauseOperator.IsGreaterThan
                    ? "Is Greater Than"
                    : "Is Less Than",
                BatchSourceKey = dto.BatchSourceKey,
                DataValue = dto.Value
            };
        }
    }
}
