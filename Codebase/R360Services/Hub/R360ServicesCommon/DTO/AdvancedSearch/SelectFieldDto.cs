﻿using WFS.RecHub.ApplicationBlocks.DataAccess.Interfaces;

namespace WFS.RecHub.R360Services.Common.DTO.AdvancedSearch
{
    public class SelectFieldDto : ITableParam
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int DataType { get; set; }
        public string ReportTitle { get; set; }
        public int BatchSourceKey { get; set; }

        public static SelectFieldDto FromRequest(AdvancedSearchWhereClauseDto dto)
        {
            return new SelectFieldDto()
            {
                TableName = dto.Table == AdvancedSearchWhereClauseTable.Checks
                    ? "Checks"
                    : dto.Table == AdvancedSearchWhereClauseTable.Stubs
                    ? "Stubs"
                    : string.Empty,
                FieldName = dto.FieldName,
                DataType = (int)dto.DataType,
                ReportTitle = dto.ReportTitle,
                BatchSourceKey = dto.BatchSourceKey
            };
        }
    }
}
