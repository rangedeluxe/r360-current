﻿using System;
using System.Linq;
using WFS.RecHub.ApplicationBlocks.DataAccess.Interfaces;

namespace WFS.RecHub.R360Services.Common.DTO.AdvancedSearch
{
    public class BaseRequestDto : ITableParam
    {
        public int BankID { get; set; }
        public int ClientAccountID { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public long? BatchIDFrom { get; set; }
        public long? BatchIDTo { get; set; }
        public long? BatchNumberFrom { get; set; }
        public long? BatchNumberTo { get; set; }
        public decimal? AmountFrom { get; set; }
        public decimal? AmountTo { get; set; }
        public Guid SessionID { get; set; }
        public string COTSOnly { get; set; }
        public string MarkSenseOnly { get; set; }
        public int? BatchPaymentTypeKey { get; set; }
        public int? BatchSourceKey { get; set; }
        public int PaginateRS { get; set; }
        public int StartRecord { get; set; }
        public int RecordsPerPage { get; set; }
        public string SortBy { get; set; }
        public string SortByDir { get; set; }
        public string SortByDisplayName { get; set; }
        public string Serial { get; set; }
        public string DisplayScannedCheck { get; set; }

        public static BaseRequestDto FromRequest(AdvancedSearchRequestDto dto)
        {
            var response = new BaseRequestDto()
            {
                BankID = dto.BankId,
                ClientAccountID = dto.WorkgroupId,
                DateFrom = dto.DepositDateFrom,
                DateTo = dto.DepositDateTo,
                SessionID = dto.SessionId,
                COTSOnly = dto.COTSOnly.ToString(),
                MarkSenseOnly = dto.MarkSenseOnly.ToString(),
                BatchPaymentTypeKey = dto.PaymentTypeKey >= 0 ? dto.PaymentTypeKey : (int?)null,
                BatchSourceKey = dto.PaymentSourceKey >= 0 ? dto.PaymentSourceKey : (int?)null,
                PaginateRS = dto.Paginate ? 1 : 0,
                StartRecord = (dto.StartRecord + 1),
                RecordsPerPage = dto.RecordsPerPage,
                SortBy = dto.SortBy,
                SortByDir = dto.SortByDir,
                SortByDisplayName = dto.SortByDisplayName
            };

            // I seriously tried to parse these in the most elegant way I could.
            // I apologize for nothing.
            long templong;
            decimal tempdecimal;
            response.BatchIDFrom = 
                long.TryParse(dto.WhereClauses?.FirstOrDefault(x => x.IsStandard && x.StandardXmlColumnName == "BatchIDFrom")?.Value, out templong)
                ? templong : (long?)null;
            response.BatchIDTo =
                long.TryParse(dto.WhereClauses?.FirstOrDefault(x => x.IsStandard && x.StandardXmlColumnName == "BatchIDTo")?.Value, out templong)
                ? templong : (long?)null;
            response.BatchNumberFrom =
                long.TryParse(dto.WhereClauses?.FirstOrDefault(x => x.IsStandard && x.StandardXmlColumnName == "BatchNumberFrom")?.Value, out templong)
                ? templong : (long?)null;
            response.BatchNumberTo =
                long.TryParse(dto.WhereClauses?.FirstOrDefault(x => x.IsStandard && x.StandardXmlColumnName == "BatchNumberTo")?.Value, out templong)
                ? templong : (long?)null;
            response.AmountFrom =
                decimal.TryParse(dto.WhereClauses?.FirstOrDefault(x => x.IsStandard && x.StandardXmlColumnName == "AmountFrom")?.Value, out tempdecimal)
                ? tempdecimal : (decimal?)null;
            response.AmountTo =
                decimal.TryParse(dto.WhereClauses?.FirstOrDefault(x => x.IsStandard && x.StandardXmlColumnName == "AmountTo")?.Value, out tempdecimal)
                ? tempdecimal : (decimal?)null;
            return response;
        }
    }
}