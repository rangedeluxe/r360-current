﻿using System.Collections.Generic;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositTransactionValidationResponseDto
    {
        public IEnumerable<PostDepositStubDataEntryExceptionDto> StubDataEntryExceptions { get; set; }
    }
}
