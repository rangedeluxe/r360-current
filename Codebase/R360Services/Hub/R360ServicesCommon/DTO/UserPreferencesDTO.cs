﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class UserPreferencesDTO
    {
        //if these are null the user doesn't have permission to override them
        public int? recordsPerPage { get; set; }
        public bool? displayRemitterNameinPdf { get; set; }

        public bool LoadDataRow(DataRow row)
        {
            if (row.Table.Columns.Count > 0)
            {
                if (row["PreferenceName"].ToString().ToLower() == "recordsperpage")
                {
                    int defaultRecords;
                    int.TryParse(row["DefaultSetting"].ToString(), out defaultRecords);
                    this.recordsPerPage = ipoLib.NVL(ref row, "UserSetting", defaultRecords);
                }
                else if (row["PreferenceName"].ToString().ToLower() == "displayremitternameinpdf")
                {
                    displayRemitterNameinPdf = row["UserSetting"].ToString() == "Y" ? true : false;
                }
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
