﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositEntityDto
    {
        public string EntityId { get; set; }
        public string EntityHierarchy { get; set; }
    }
}
