using System.Runtime.Serialization;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class DataEntryDto
    {
        [DataMember]
        public string FieldName { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Type { get; set; }
        [DataMember]
        public string Value { get; set; }
    }
}