﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class NotificationsResponseDto : BaseResponse
    {
        [DataMember]
        public int TotalRecords { get; set; }
        [DataMember]
        public int FilteredTotal { get; set; }
        [DataMember]
        public IEnumerable<NotificationDto> Notifications { get; set; }
    }
}
