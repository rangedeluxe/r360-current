﻿using System;
using System.Collections.Generic;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class InvoiceSearchXmlRequestDto
    {

        public InvoiceSearchXmlRequestDto()
        {
            Criteria = new List<InvoiceSearchCriteriaDto>();
        }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public int Length { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public Guid Session { get; set; }
        public int Start { get; set; }
        public List<WorkgroupDto> Workgroups { get; set; }
        public List<InvoiceSearchCriteriaDto> Criteria { get; set; }
        public int PaymentType { get; set; }
        public int PaymentSource { get; set; }
        public string FullUserName { get; set; }
        public string EntityName { get; set; }
    }
}
