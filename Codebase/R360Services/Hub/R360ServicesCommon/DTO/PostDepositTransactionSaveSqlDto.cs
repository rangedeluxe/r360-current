using System;
using System.Collections.Generic;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositTransactionSaveSqlDto
    {
        public DateTime DepositDate { get; set; }
        public long BatchId { get; set; }
        public int TransactionId { get; set; }
        public IList<PostDepositCheckSaveDto> Checks{get; set; }
        public IList<PostDepositStubSaveSqlDto> DataEntryFields { get; set; }
    }
}