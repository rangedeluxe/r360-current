﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.R360Services.Common.DTO
{
    
    public class DocumentDto
    {
        [MapFrom("DocumentSequence")]        
        public int DocumentSequence { get; set; }

        
        public string Description { get; set; }

        [MapFrom("PICSDate")]
        public int PicsDate { get; set; }

        [MapFrom("SourceProcessingDateKey")]         
        public int SourceProcessingDateKey { get; set; }

        
        public string SourceProcessingDate => DateTime
            .ParseExact(SourceProcessingDateKey.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture)
            .ToString("MM/dd/yyyy");

        [MapFrom("BatchSequence")]        
        public int BatchSequence { get; set; }        
        
        public string FileDescriptor { get; set; }
        
        public string BatchSourceShortName { get; set; }
        
        public string ImportTypeShortName { get; set; }
        
        public bool ImageIsAvailable { get; set; }
    }
}
