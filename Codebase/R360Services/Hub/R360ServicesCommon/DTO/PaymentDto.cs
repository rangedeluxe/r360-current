﻿using System;
using System.Collections.Generic;
using System.Globalization;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PaymentDto
    {
        [MapFrom("CheckSequence")]
        public int PaymentSequence { get; set; }
        public string RT { get; set; }
        [MapFrom("Account")]
        public string AccountNumber { get; set; }
        [MapFrom("Serial")]
        public string CheckTraceRefNumber { get; set; }
        [MapFrom("RemitterName")]
        public string Payer { get; set; }
        public string DDA { get; set; }
        public decimal Amount { get; set; }
        public bool CheckIconVisible { get; set; }
        public bool ItemReportIconVisible { get; set; }
        public int PICSDate { get; set; }
        public int BatchSequence { get; set; }
        public string BatchSourceShortName { get; set; }
        public string SourceProcessingDate
        {
            get
            {
                return DateTime
                    .ParseExact(SourceProcessingDateKey.ToString(), "yyyyMMdd", CultureInfo.InvariantCulture)
                    .ToString("MM/dd/yyyy");
            }
        }
        public int SourceProcessingDateKey { get; set; }
        public IEnumerable<DataEntryValueDto> DataEntryFields { get; set; }
        [MapFrom("TransactionID")]
        public int TransactionId { get; set; }
        public bool ImageIsAvailable { get; set; }
        public IEnumerable<PostDepositRequestPayerResponseDto> PayerList { get; set; }

    }
}
