﻿using System;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class InvoiceSearchResultDto
    {
        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public int WorkgroupKey { get; set; }
        public long BatchId { get; set; }
        public long SourceBatchId { get; set; }
        public int BatchNumber { get; set; }
        public DateTime DepositDate { get; set; }
        public int PICSDate { get; set; }
        public int TransactionId { get; set; }
        public int TransactionSequence { get; set; }
        public double Amount { get; set; }
        public string DDA { get; set; }
        public string RT { get; set; }
        public string Account { get; set; }
        public int BatchSequence { get; set; }
        public int DocumentBatchSequence { get; set; }
        public string Serial { get; set; }
        public string RemitterName { get; set; }
        public string PaymentType { get; set; }
        public string PaymentSource { get; set; }
        public string BatchSourceLongName { get; set; }
        public string BatchSourceShortName { get; set; }
        public string ImportTypeLongName { get; set; }
        public string ImportTypeShortName { get; set; }
        public string DepositDateString { get; set; }

        public bool ShowDocumentIcon { get; set; }
    }
}
