﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositStubDto
    {
        public int StubSequence { get; set; }
        public double Amount { get; set; }
        public bool StubIconVisible { get; set; }
        public int BatchSequence { get; set; }
        public bool ItemReportIconVisible { get; set; }
        public string SourceProcessingDate { get; set; }
        public int DocumentBatchSequence { get; set; }
        public IEnumerable<DataEntryDto> DataEntryFields { get; set; }
        public int TransactionId { get; set; }
    }
}
