﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using System.Xml;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Kyle C.
 * * Date: 08/04/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * 08/04/2014 Created
********************************************************************************/
namespace WFS.RecHub.Common
{
	[DataContract]
	public class WorkgroupNameDTO
	{
		[DataMember]
		public int EntityId { get; set; }

		[DataMember]
		public int SiteBankID { get; set; }

		[DataMember]
		public int SiteWorkgroupID { get; set; }

		[DataMember]
		public string DisplayLabel { get; set; }
	}
}