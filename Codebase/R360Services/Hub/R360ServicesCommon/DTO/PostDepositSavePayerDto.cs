﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PostDepositSavePayerDto
    {
        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public int ClientAccountId { get; set; }

        [DataMember]
        public string RoutingNumber { get; set; }

        [DataMember]
        public string Account { get; set; }

        [DataMember]
        public string PayerName { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        // ReSharper disable once IdentifierTypo
        public bool Persistable { get; set; }
    }
}
