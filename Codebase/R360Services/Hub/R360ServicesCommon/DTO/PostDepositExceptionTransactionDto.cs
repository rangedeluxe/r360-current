﻿using System;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositExceptionTransactionDto
    {
        [MapFrom("BatchID")]
        public long BatchId { get; set; }
        [MapFrom("TransactionID")]
        public int TransactionId { get; set; }
        [MapFrom("Deposit_Date")]
        public DateTime DepositDate { get; set; }
        public string ShortDepositDate { get { return DepositDate.ToShortDateString(); } }
    }
}
