﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/23/2013
 * *
 * * Purpose: Defines the session data object containing the user id and list of organizations to
 * *            which the session provides access.
 * *
 * * Modification History
 * WI 100422 CEJ 05/23/2013 Created
 *           RDS 06/23/2014 Organization concept has been removed - use RAAM Entities
* ******************************************************************************/
/*
namespace WFS.RecHub.Common {
   public class SessionData {
        public SessionData() {
        }

        public SessionData(XElement xmeData) {
            UserID = Convert.ToInt32(xmeData.Attribute("UserID").Value);
            List<OrganizationModel> lstTemp = new List<OrganizationModel>();
            foreach(XElement xmaCurOrganization in xmeData.Elements("OrganizationModel")) {
                lstTemp.Add(new OrganizationModel(xmaCurOrganization));
            }
            OrganizationsData = lstTemp.ToArray();
        }

        public SessionData(DataRow drData)
            :this((int)drData["UserID"], new OrganizationModel(drData)) {
        }

        public SessionData(int iUserID, OrganizationModel ormOrganizationData):this(iUserID, new OrganizationModel[] {ormOrganizationData}) {
        }

        public SessionData(int iUserID, OrganizationModel[] ormOrganizationsData) {
            UserID = iUserID;
            OrganizationsData = ormOrganizationsData;
        }

        public XElement ToXML() {
            return new XElement("SessionData",
                    new XAttribute("UserID", UserID.ToString()), 
                    (from OrganizationModel ormCurOrganizationData in OrganizationsData
                         select ormCurOrganizationData.ToXML()));
        }

        public int UserID {
            get;
            set;
        }

        public OrganizationModel[] OrganizationsData {
            get;
            set;
        }

        public static Dictionary<string, string> FieldMapping {
            get {
                return new Dictionary<string, string>() {
                    {"OrganizationID", "OLOrganizationID"},
                    {"UserID", "UserID"}};
            }
        }
    }
}
*/