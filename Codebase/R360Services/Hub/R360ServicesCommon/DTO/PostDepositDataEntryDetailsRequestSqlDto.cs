﻿using System;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositDataEntryDetailsRequestSqlDto
    {
        public Guid SessionId { get; set; }
        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public long BatchId { get; set; }
        public DateTime DepositDate { get; set; }
        public int TransactionId { get; set; }
        public int BatchSequence { get; set; }
    }
}
