﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class DataEntrySetupDto
    {
        [MapFrom("FldTitle")]
        public string Title { get; set; }
        public string TableName { get; set; }
        [MapFrom("FldName")]
        public string FieldName { get; set; }
        [MapFrom("FldDataTypeEnum")]
        public int DataType { get; set; }
        public bool IsRequired { get; set;}
    }
}