﻿using System;
using System.Collections.Generic;
using WFS.RecHub.Common;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace WFS.RecHub.R360Services.Common.DTO
{
    /// <summary>
    /// This is to hold everything that comes in from the R360Manager class.
    /// </summary>
    public class AdvancedSearchDto
    {

        public AdvancedSearchDto()
        {
            WhereClauses = new List<AdvancedSearchWhereClauseDto>();
            SelectFields = new List<AdvancedSearchWhereClauseDto>();
            PaginateResults = true;
        }

        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public DateTime DepositDateFrom { get; set; }
        public DateTime DepositDateTo { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public string OrderBy { get; set; }
        public string OrderDirection { get; set; }
        public string OrderByDisplayName { get; set; }
        public int PaymentSourceKey { get; set; }
        public int PaymentTypeKey { get; set; }
        public bool PaginateResults { get; set; }
        /// <summary>
        /// Optional. If set, log an additional record to SessionActivityLog
        /// with this code and the record count.
        /// </summary>
        public ActivityCodes? ActivityCode { get; set; }

        public List<AdvancedSearchWhereClauseDto> WhereClauses { get; set; }
        public List<AdvancedSearchWhereClauseDto> SelectFields { get; set; }
    }
}
