﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public enum PaymentSearchOperator
    {
        Contains = 1,
        IsGreaterThanOrEqualTo = 2,
        IsLessThanOrEqualTo = 3,
        Equals = 4
    }
}
