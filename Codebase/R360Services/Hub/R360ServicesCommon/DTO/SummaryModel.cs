﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

/******************************************************************************
 ** WAUSAU Financial Systems (WFS)
 ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 *******************************************************************************
 *******************************************************************************
 ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 *******************************************************************************
 * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * other trademarks cited herein are property of their respective owners.
 * These materials are unpublished confidential and proprietary information 
 * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * copied, modified or disclosed except as expressly permitted in writing by 
 * WFS (see the WFS license agreement for details).  All copies, modifications 
 * and derivative works of these materials are property of WFS.
 *
 * Author: Charlie Johnson
 * Date: 05/23/2013
 *
 * Purpose: Defines the payment type total object containing the batch count, transaction count and 
 *            total deposited for a given payment type/ payment source within the given organization
 *            and client account
 *
 * Modification History
 * WI 100422 CEJ 05/23/2013 Created
 * WI 111826 TWE 08/14/2013 
 *    change URL to contain formating for start and end date.
 * WI 155797 CEJ 07/24/2014
 *     Alter HubViews SummaryModel ClientSummaryURL to use the OLWorkgroupID instead of the lockbox id
 ******************************************************************************/

namespace WFS.RecHub.Common
{
    [DataContract]
    public class SummaryModel
    {
        private static int payID = 0;
        public SummaryModel(
                string sOrganization,
                string sAccount,
                string sPaymentSource,
                string sPaymentType,
                int iPaymentSourceId,
                int iPaymentTypeId,
                int iBatchCount,
                int iTransactionCount,
                int iRecHubBankId,
                int iRecHubWorkgroupId,
                double dTotal,
                string sClientSummaryURL,
                int entityid,
                string dda,
                int paymentcount)
        {
            LoadData(
                sOrganization,
                sAccount,
                sPaymentSource,
                sPaymentType,
                iPaymentSourceId,
                iPaymentTypeId,
                iBatchCount,
                iTransactionCount,
                iRecHubBankId,
                iRecHubWorkgroupId,
                dTotal,
                sClientSummaryURL,
                entityid,
                dda,
                paymentcount);
        }

        public SummaryModel()
        {
        }

        public SummaryModel(DataRow drData)
        {
            LoadData(
                drData["EntityName"].ToString(),
                drData["WorkgroupName"].ToString(),
                drData["BatchSource"].ToString(),
                drData["PaymentType"].ToString(),
                (Int16)(drData["BatchSourceID"] ?? 0),
                (byte)(drData["PaymentTypeID"] ?? 0),
                (int)(drData["BatchCount"] ?? 0),
                (int)(drData["TransactionCount"] ?? 0),
                (int)(drData["SiteBankID"] ?? 0),
                (int)(drData["SiteClientAccountID"] ?? 0),
                Convert.ToDouble(drData["PaymentTotal"]),
                string.Empty,
                (int)(drData["EntityID"] ?? 0),
                drData["DDA"].ToString(),
                (int)(drData["PaymentCount"] ?? 0));
        }

        public void LoadData(
                string sOrganization,
                string sAccount,
                string sPaymentSource,
                string sPaymentType,
                int iPaymentSourceId,
                int iPaymentTypeId,
                int iBatchCount,
                int iTransactionCount,
                int ibankid,
                int iRecHubWorkgroupId,
                double dTotal,
                string sClientSummaryURL,
                int entityid,
                string dda,
                int paymentcount)
        {
            PaymentID = payID++;
            Organization = sOrganization;
            Account = sAccount;
            PaymentSource = sPaymentSource;
            PaymentType = sPaymentType;
            PaymentSourceId = iPaymentSourceId;
            PaymentTypeId = iPaymentTypeId;
            BatchCount = iBatchCount;
            RecHubBankId = ibankid;
            RecHubWorkgroupId = iRecHubWorkgroupId;
            TransactionCount = iTransactionCount;
            Total = dTotal;
            ClientSummaryURL = sClientSummaryURL;
            EntityID = entityid;
            DDA = dda;
            PaymentCount = paymentcount;
        }

        [DataMember]
        public int PaymentCount { get; set; }

        [DataMember]
        public int EntityID
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentID
        {
            get;
            set;
        }

        [DataMember]
        public string Organization
        {
            get;
            set;
        }

        [DataMember]
        public string Account
        {
            get;
            set;
        }

        [DataMember]
        public string PaymentSource
        {
            get;
            set;
        }

        [DataMember]
        public string PaymentType
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentSourceId
        {
            get;
            set;
        }

        [DataMember]
        public int PaymentTypeId
        {
            get;
            set;
        }

        [DataMember]
        public int BatchCount
        {
            get;
            set;
        }

        [DataMember]
        public int TransactionCount
        {
            get;
            set;
        }

        [DataMember]
        public int RecHubBankId { get; set; }

        [DataMember]
        public int RecHubWorkgroupId
        {
            get;
            set;
        }

        [DataMember]
        public double Total
        {
            get;
            set;
        }


        [DataMember]
        public string ClientSummaryURL
        {
            get;
            set;
        }

        [DataMember]
        public string DDA { get; set; }
    }
}
