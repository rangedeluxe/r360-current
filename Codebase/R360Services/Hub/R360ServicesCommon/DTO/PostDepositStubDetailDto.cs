﻿using System.Collections.Generic;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositStubDetailDto
    {
        public int StubSequence { get; set; }
        public int BatchSequence { get; set; }
        public IEnumerable<DataEntryValueDto> DataEntryFields { get; set; }
    }
}