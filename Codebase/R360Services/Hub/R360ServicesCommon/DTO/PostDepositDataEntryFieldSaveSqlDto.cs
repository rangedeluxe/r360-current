﻿using System;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositDataEntryFieldSaveSqlDto
    {
        public string FieldName { get; set; }
        public DateTime? ValueDateTime { get; set; }
        public float? ValueFloat { get; set; }
        public decimal? ValueMoney { get; set; }
        public string Value { get; set; }
    }
}