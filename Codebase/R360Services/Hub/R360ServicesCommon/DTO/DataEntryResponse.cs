﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class DataEntryResponse
    {
        public int DataType { get; set; }
        public string TableName { get; set; }
        public string FldName { get; set; }
        public string DisplayName { get; set; }
        public int BatchSourceKey { get; set; }

        public static DataEntryResponse FromDataRow(DataRow row)
        {
            try
            {
                return new DataEntryResponse()
                {
                    DataType = int.Parse(row["DataType"].ToString()),
                    TableName = (string)row["TableName"],
                    FldName = (string)row["FldName"],
                    DisplayName = row["DisplayName"] == null || row["DisplayName"] == DBNull.Value
                        ? string.Empty
                        : (string)row["DisplayName"],
                    BatchSourceKey = int.Parse(row["BatchSourceKey"].ToString())
                };
            }
            catch
            {
                return null;
            }
        }
    }
}
