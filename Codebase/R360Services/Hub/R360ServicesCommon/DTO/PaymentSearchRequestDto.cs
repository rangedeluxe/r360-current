﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class PaymentSearchRequestDto
    {
        public PaymentSearchRequestDto()
        {
            Criteria = new List<PaymentSearchCriteriaDto>();
        }

        [DataMember]
        public DateTime DateFrom { get; set; }
        [DataMember]
        public DateTime DateTo { get; set; }
        [DataMember]
        public string Workgroup { get; set; }
        [DataMember]
        public int PaymentType { get; set; }
        [DataMember]
        public int PaymentSource { get; set; }
        [DataMember]
        public int Start { get; set; }
        [DataMember]
        public int Length { get; set; }
        [DataMember]
        public string OrderBy { get; set; }
        [DataMember]
        public string OrderDirection { get; set; }
        [DataMember]
        public List<PaymentSearchCriteriaDto> Criteria { get; set; }
    }
}
