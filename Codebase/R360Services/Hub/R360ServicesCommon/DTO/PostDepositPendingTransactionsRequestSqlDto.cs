﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class PostDepositPendingTransactionsRequestSqlDto
    {
        public Guid SessionId { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public string OrderBy { get; set; }
        public string OrderByDirection { get; set; }
        public string Search { get; set; }
        public XmlDocument Entities { get; set; }
    }
}
