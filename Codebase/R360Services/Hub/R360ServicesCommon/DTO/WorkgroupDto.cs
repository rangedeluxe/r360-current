﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Attributes;
using WFS.RecHub.ApplicationBlocks.DataAccess.Interfaces;

namespace WFS.RecHub.R360Services.Common.DTO
{
    [DataContract]
    public class WorkgroupDto : ITableParam
    {
        [MapFrom("BankId")]
        [DataMember]
        public int BankId { get; set; }
        [MapFrom("WorkgroupId")]
        [DataMember]
        public int ClientAccountId { get; set;}
    }
}
