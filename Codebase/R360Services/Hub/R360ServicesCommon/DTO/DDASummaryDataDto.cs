﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common
{
    [DataContract]
    public class DDASummaryDataDto
    {
        [DataMember]
        public int EntityID { get; set; }

		[DataMember]
		public int SiteBankID { get; set; }

		[DataMember]
		public int SiteClientAccountID { get; set; }

        [DataMember]
        public string WorkgroupName { get; set; }

        [DataMember]
        public string PaymentSource { get; set; }

        [DataMember]
        public string PaymentType { get; set; }

        [DataMember]
        public string DDA { get; set; }

        [DataMember]
        public int PaymentCount { get; set; }

        [DataMember]
        public decimal PaymentTotal { get; set; }

    }
}
