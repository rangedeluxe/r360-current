﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    /// <summary>
    /// This object is to hold everything we need to send off to the stored proc.
    /// </summary>
    public class AdvancedSearchRequestDto
    {
        public AdvancedSearchRequestDto()
        {
            WhereClauses = new List<AdvancedSearchWhereClauseDto>();
            SelectFields = new List<AdvancedSearchWhereClauseDto>();
            Paginate = true;
        }

        public int BankId { get; set; }
        public int WorkgroupId { get; set; }
        public DateTime DepositDateFrom { get; set; }
        public DateTime DepositDateTo { get; set; }
        public Guid SessionId { get; set; }

        public int RecordsPerPage { get; set; }
        public int StartRecord { get; set; }

        // These next values affect a filter on how many rows are displayed.
        public bool COTSOnly { get; set; }
        public bool MarkSenseOnly { get; set; }
        public List<AdvancedSearchWhereClauseDto> WhereClauses { get; set; }
        public List<AdvancedSearchWhereClauseDto> SelectFields { get; set; }
        public string SortBy { get; set; }
        public string SortByDir { get; set; }
        public string SortByDisplayName { get; set; }
        public  int PaymentSourceKey { get; set; }
        public  int PaymentTypeKey { get; set; }
        public bool Paginate { get; set; }
        public bool DisplayBatchID { get; set; }
    }
}
