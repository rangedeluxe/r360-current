﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services.Common.DTO
{
    public class NotificationFileDto
    {
        public int Id { get; set; }
        public int MessageGroup { get; set; }
        public Guid FileId { get; set; }
        public string UserFileName { get; set; }
        public string FileExtension { get; set; }
        public string FileTypeDescription { get; set; }
        public string FilePath { get; set; }
        public long FileSize { get; set; }
    }
}