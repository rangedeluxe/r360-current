﻿using System;
using System.ServiceModel;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.Common
{
    [ServiceContract(Namespace = "urn:wausaufs.com:services:HubService")]
    public interface IPostDepositExceptionServices 
    {
        [OperationContract]
        BaseGenericResponse<PostDepositTransactionsResponseDto> GetPendingTransactions(PostDepositPendingTransactionsRequestDto request);

        [OperationContract]
        BaseGenericResponse<PostDepositTransactionDetailResponseDto> GetTransaction(
            PostDepositTransactionRequestDto request);

        [OperationContract]
        BaseGenericResponse<PostDepositTransactionValidationResponseDto> GetTransactionValidationResults(
            PostDepositTransactionDetailResponseDto request);

        [OperationContract]
        BaseResponse LockTransaction(LockTransactionRequestDto request);

        [OperationContract]
        BaseResponse UnlockTransaction(LockTransactionRequestDto request);

        [OperationContract]
        BaseResponse UnlockTransactionWithoutOverride(LockTransactionRequestDto request);

        [OperationContract]
        BaseGenericResponse<PostDepositSavePayerResultDto> SavePayer(PostDepositSavePayerDto request);

        [OperationContract]
        BaseGenericResponse<PostDepositTransactionSaveResultDto> SaveTransaction(PostDepositTransactionSaveDto request);
        [OperationContract]
        BaseGenericResponse<bool>  CompleteTransaction(PostDepositTransactionRequestDto request);

        [OperationContract]
        BaseResponse AuditTransactionDetailView(PostDepositExceptionTransactionDto request);

        [OperationContract]
        BaseGenericResponse<PayerResponseDto> GetPayerList(PayerLookupRequestDto request);
    }
}
