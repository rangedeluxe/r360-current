﻿using System;
using OLDecisioningShared.Responses;
using System.ServiceModel;
using OLDecisioningShared.DTO;

namespace WFS.RecHub.R360Services.Common
{
    [ServiceContract(Namespace = "urn:wausaufs.com:services:HubService")]
    public interface IExceptionServices
    {
        [OperationContract]
        OLDBaseGenericResponse<string> Ping();

        [OperationContract]
        PendingBatchesResponse GetPendingBatches();

        [OperationContract]
        PendingBatchesResponse GetBatches(long bankId, long workgroupId);

        [OperationContract]
        OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId);

        [OperationContract]
        OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId);

        [OperationContract]
        OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid);

        [OperationContract]
        OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login);

        [OperationContract]
        OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId);

        [OperationContract]
        UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto);

        [OperationContract]
        OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto);
    }
}
