﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
 * * WI 154797 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
********************************************************************************/
namespace WFS.RecHub.R360Services.Common
{
    [ServiceContract(Namespace = "urn:wausaufs.com:services:HubService")]
    public interface IRemitterServices
    {
        [OperationContract]
        PingResponse Ping();

        [OperationContract]
        [XmlSerializerFormat]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ServerFaultException))]
        XmlDocumentResponse GetRemitterMaintenancePage(Guid LockboxRemitterID);

        [OperationContract]
        [XmlSerializerFormat]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ServerFaultException))]
        XmlDocumentResponse GetAllRemitters(int StartRecord, bool ShowAll);

        [OperationContract]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ServerFaultException))]
        BaseGenericResponse<cRemitter> GetLockboxRemitter(string RoutingNumber, string AccountNumber, Guid OLLockboxID);

        [OperationContract]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ServerFaultException))]
        BaseGenericResponse<cRemitter> GetLockboxRemitterByID(Guid LockboxRemitterID);

        [OperationContract]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ServerFaultException))]
        BaseResponse LockboxRemitterExists(string RoutingNumber, string AccountNumber, Guid OLLockboxID);

        [OperationContract]
        [FaultContract(typeof(InvalidSiteKeyFault))]
        [FaultContract(typeof(InvalidSessionIDFault))]
        [FaultContract(typeof(SessionExpiredFault))]
        [FaultContract(typeof(InvalidSessionFault))]
        [FaultContract(typeof(ServerFaultException))]
        BaseResponse UpdateChecksRemitter(string LockboxRemitterName, int BankID, int LockboxID, int BatchID, DateTime DepositDate, int TransactionID, int BatchSequence);
    }
}
