﻿using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Wayne Schwarz
 * * Date: 07/03/2013
 * *
 * * Purpose: Houses the permissions Response object
 * *
 * * Modification History
 * WI 101855 WJS 07/03/2013 Created
 * ******************************************************************************/

namespace WFS.RecHub.R360Services.Common
{
    public class PermissionsResponse : BaseResponse
    {
        public PermissionsResponse()
            : base()
        {
        }

        public bool HasPermission { get; set; }
    }
}
