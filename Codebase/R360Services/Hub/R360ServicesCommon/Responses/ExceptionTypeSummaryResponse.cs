﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: David Parker
 * * Date: 06/24/2013
 * *
 * * Purpose: Defines the list of <exception summary by payment type> 
 * *
 * * Modification History
 * WI 101786 DRP 06/24/2013 Created
* ******************************************************************************/

namespace WFS.RecHub.R360Services.Common 
{
    public class ExceptionTypeSummaryResponse : BaseResponse
    {
        public ExceptionTypeSummaryResponse()
            : base()
        {
            SummaryByType = new ExceptionTypeSummary[] {};
        }

        public ExceptionTypeSummary[] SummaryByType
        { get; set; }
    }
}
