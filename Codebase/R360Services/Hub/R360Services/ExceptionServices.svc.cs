﻿using OLDecisioningShared.Responses;
using System;
using OLDecisioningShared.DTO;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;
using WFS.RecHub.OLDecisioningServicesClient;
using WFS.RecHub.Common;
using WFS.LTA.Common;
using WFS.RecHub.OLDecisioningShared;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360RaamClient;
using static WFS.RecHub.R360Shared.ConfigHelpers;

namespace WFS.RecHub.R360Services
{
    public class ExceptionServices: LTAService, IExceptionServices
    {
        private cSiteOptions _siteOptions;
        private ltaLog _eventLog;

        public ExceptionServices()
        {
            _siteOptions = new cSiteOptions("IPOnline");
            _eventLog = new ltaLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (LTAMessageImportance)SiteOptions.loggingDepth);
        }

        public OLDBaseGenericResponse<string> Ping()
        {
            return FailOnError(excepApi => ((ExceptionsAPI) excepApi).Ping());
        }

        public PendingBatchesResponse GetPendingBatches()
        {
            return FailOnError(excepApi =>
                    ((ExceptionsAPI)excepApi).GetPendingBatches());
        }

        public PendingBatchesResponse GetBatches(long bankId, long workgroupId)
        {
            return FailOnError(excepApi => ((ExceptionsAPI) excepApi).GetPendingBatches());
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatchDetails(int globalBatchId)
        {
            return FailOnError(excepApi => ((ExceptionsAPI) excepApi).GetBatchDetails(globalBatchId));
        }

        public OLDBaseGenericResponse<DecisioningBatchDto> GetBatch(int globalBatchId)
        {
            return FailOnError(excepApi => ((ExceptionsAPI)excepApi).GetBatch(globalBatchId));
        }

        public OLDBaseGenericResponse<DecisioningTransactionDto> GetTransactionDetails(int globalBatchId, int transactionid)
        {
            return FailOnError(excepApi => ((ExceptionsAPI)excepApi).GetTransactionDetails(globalBatchId, transactionid));
        }

        public OLDBaseGenericResponse<bool> CheckOutBatch(int globalBatchId, Guid sid, string firstName, string lastName, string login)
        {
            return FailOnError(excepApi => ((ExceptionsAPI)excepApi).CheckOutBatch(globalBatchId,sid, firstName,lastName,login));
        }

        public OLDBaseGenericResponse<bool> ResetBatch(int globalBatchId)
        {
            return FailOnError(excepApi => ((ExceptionsAPI)excepApi).ResetBatch(globalBatchId));
        }

        public UpdateTransactionResponse UpdateTransaction(DecisioningTransactionDto dto)
        {
            return FailOnError(excepApi => ((ExceptionsAPI)excepApi).UpdateTransaction(dto));
        }

        public OLDBaseGenericResponse<bool> CompleteBatch(DecisioningCompleteBatchDto dto)
        {
            return FailOnError(excepApi => ((ExceptionsAPI)excepApi).CompleteBatch(dto));
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            var api = new ExceptionsAPI(serviceContext,
                new DecisioningSvcClient("IPOnline", _eventLog, R360ServiceFactory.Create<IDecisioning>()),
                new R360ServicesDAL("IPOnline"),
                new cSessionDAL("IPOnline"),
                new RAAMPermissionProvider(),
                new UserServices(),
                new RaamClient(new Logger(x => Api_LogEvent(x, string.Empty, MessageType.Information, MessageImportance.Verbose),
                    x => Api_LogEvent(x, string.Empty, MessageType.Warning, MessageImportance.Essential))));
            api.LogEvent += Api_LogEvent;
            return api;
        }

        private void Api_LogEvent(string message, string src, MessageType messageType, MessageImportance messageImportance)
        {
            // MAYBE WE SHOULD MAKE ONE MORE LOGGING LIBRARY.
            var type = messageType == MessageType.Error ? LTAMessageType.Error
                : messageType == MessageType.Information ? LTAMessageType.Information
                : LTAMessageType.Warning;
            var import = messageImportance == MessageImportance.Debug ? LTAMessageImportance.Debug
                : messageImportance == MessageImportance.Essential ? LTAMessageImportance.Essential
                : LTAMessageImportance.Verbose;
            _eventLog.logEvent(message, src, type, import);
        }
    }
}