﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/12/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/12/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services
{
    public class CBXServices : BaseOLService, ICBXServices
    {
        public XmlDocumentResponse GetDepositTotals(XmlDocument Parms)
        {
            return FailOnError((cbxAPI) =>
            {
                return ((CBXAPI)cbxAPI).GetDepositTotals(Parms);
            });
        }

        public XmlDocumentResponse GetBatchSummary(XmlDocument Parms)
        {
            return FailOnError((cbxAPI) =>
            {
                return ((CBXAPI)cbxAPI).GetBatchSummary(Parms);
            });
        }

        public XmlDocumentResponse GetBatchTransactions(XmlDocument Parms)
        {
            return FailOnError((cbxAPI) =>
            {
                return ((CBXAPI)cbxAPI).GetBatchTransactions(Parms);
            });
        }

        public BaseResponse CheckLockboxPermission(Guid OLLockboxID)
        {
            return FailOnError((cbxAPI) =>
            {
                return ((CBXAPI)cbxAPI).CheckLockboxPermission(OLLockboxID);
            });
        }

        public XmlDocumentResponse GetAllRemitters(int StartRecord, bool ShowAll)
        {
            return FailOnError((remitterAPI) =>
            {
                return ((RemitterAPI)remitterAPI).GetAllRemitters(StartRecord, ShowAll);
            });
        }

        public BaseGenericResponse<cRemitter> GetLockboxRemitterByID(Guid LockboxRemitterID)
        {
            return FailOnError((remitterAPI) =>
            {
                return ((RemitterAPI)remitterAPI).GetLockboxRemitter(LockboxRemitterID);
            });
        }

        public BaseGenericResponse<cRemitter> GetLockboxRemitter(string RoutingNumber, string AccountNumber, Guid OLLockboxID)
        {
            return FailOnError((remitterAPI) =>
            {
                return ((RemitterAPI)remitterAPI).GetLockboxRemitter(RoutingNumber, AccountNumber, OLLockboxID);
            });
        }

        public BaseResponse LockboxRemitterExists(string RoutingNumber, string AccountNumber, Guid OLLockboxID)
        {
            return FailOnError((remitterAPI) =>
            {
                return ((RemitterAPI)remitterAPI).LockboxRemitterExists(RoutingNumber, AccountNumber, OLLockboxID);
            });
        }

        public BaseResponse UpdateChecksRemitter(string LockboxRemitterName, int BankID, int LockboxID, int BatchID, DateTime DepositDate, int TransactionID, int BatchSequence)
        {
            return FailOnError((remitterAPI) =>
            {
                return ((RemitterAPI)remitterAPI).UpdateChecksRemitter(LockboxRemitterName, BankID, LockboxID, BatchID, DepositDate, TransactionID, BatchSequence);
            });
        }

        public XmlDocumentResponse GetRemitterMaintenancePage(Guid LockboxRemitterID)
        {
            return FailOnError((remitterAPI) =>
            {
                return ((RemitterAPI)remitterAPI).GetRemitterMaintenancePage(LockboxRemitterID);
            });
        }

        public PingResponse Ping()
        {
            return FailOnError((ignored) =>
            {
                EventLog.logEvent("Ping Received for " + OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString(), this.GetType().Name, RecHub.Common.MessageType.Information, RecHub.Common.MessageImportance.Debug);
                PingResponse response = new PingResponse();
                response.responseString = "Pong";
                return response;
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new CBXAPI(serviceContext);
        }
    }
}
