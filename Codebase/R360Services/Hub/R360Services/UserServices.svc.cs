﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services
{
    public class UserServices : BaseOLService, IUserServices
    {
        public PingResponse Ping()
        {
            return FailOnError((ignored) =>
            {
                EventLog.logEvent("Ping Received for " + OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString(), this.GetType().Name, RecHub.Common.MessageType.Information, RecHub.Common.MessageImportance.Debug);
                PingResponse response = new PingResponse();
                response.responseString = "Pong";
                return response;
            });
        }

        public BaseGenericResponse<cUserRAAM> GetUserByID(int UserID)
        {
            return FailOnError((userAPI) =>
            {
                return ((UserAPI)userAPI).GetUserByID(UserID);
            });
        }

        public XmlDocumentResponse GetOnlineUserByID(int UserID)
        {
            return FailOnError((userAPI) =>
            {
                return ((UserAPI)userAPI).GetOnlineUserByID(UserID);
            });
        }

        public XmlDocumentResponse GetOLUserPreferences()
        {
            return FailOnError((userAPI) =>
            {
                return ((UserAPI)userAPI).GetOLUserPreferences();
            });
        }

        public XmlDocumentResponse SetOLUserPreferences(XmlDocument Parms)
        {
            return FailOnError((userAPI) =>
            {
                return ((UserAPI)userAPI).SetOLUserPreferences(Parms);
            });
        }

        public XmlDocumentResponse RestoreDefaultOLPreferences()
        {
            return FailOnError((userAPI) =>
            {
                return ((UserAPI)userAPI).RestoreDefaultOLPreferences();
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new UserAPI(serviceContext);
        }

        public BaseGenericResponse<UserPreferencesDTO> GetUserPreferences()
        {
            return FailOnError((userAPI) =>
            {
                return ((UserAPI)userAPI).GetUserPreference();
            });
        }

        public BaseGenericResponse<bool> SetUserPreferences(UserPreferencesDTO Preferences)
        {
            return FailOnError((userAPI) => 
            {
                return ((UserAPI)userAPI).SetUserPreferences(Preferences);
            });
        }

        public BaseGenericResponse<UserPreferencesDTO> RestoreDefaultPreferences()
        {
            return FailOnError((userAPI) =>
            {
                return ((UserAPI)userAPI).RestoreDefaultPreferences();
            });
        }

        public BaseGenericResponse<cOLLockboxes> GetUserLockboxes()
        {
            return FailOnError((locboxApi) =>
            {
                LockboxAPI lockbox = new LockboxAPI(R360ServiceContext.Current);
                return lockbox.GetUserLockboxes();
            });
        }
    }
}
