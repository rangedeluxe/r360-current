﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/23/2013
 * *
 * * Purpose: Adapts the data returned from R360ServicesDAL into the R360ServicesDTO objects or object arrays
 * *            within Response objects to be transmitted via WCF.  This inherits and conforms to the
 * *            iR360Serives interface.
 * *
 * * Modification History
 * WI 100422 CEJ 05/23/2013 Created
 * WI 101786 DRP 06/24/2013 Added GetExceptionTypeSummary
 * WI 101855 WJS 7/5/2013   Added CheckUserPermissionsForFunction
 * WI 107711 CEJ 10/16/2013
 *    - Create the ability log userActivity in the sessionLog from the R360 Services
 ******************************************************************************/
namespace WFS.RecHub.R360Services
{
    public class R360Services : LTAService, IR360Services
    {
        public PingResponse Ping()
        {
            return FailOnError((ignored) =>
            {
                EventLog.logEvent("Ping Received for " + OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString(), this.GetType().Name, RecHub.Common.MessageType.Information, RecHub.Common.MessageImportance.Debug);
                PingResponse response = new PingResponse();
                response.responseString = "Pong";
                return response;
            });
        }

        public DataEntryResponseDTO GetDataEntryFieldsForWorkgroup(DataEntrySearchDTO search)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetDataEntryFieldsForWorkgroup(search);
            });
        }

        public SummariesResponse GetClientAccountSummary(ReceivablesSummaryRequestDto request)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetClientAccountSummary(request);
            });
        }


        public ClientAccountsResponse GetClientAccounts()
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetClientAccounts();
            });
        }

        public ClientAccountsResponse GetClientAccountsForOrganization(Guid OLOrganizationID)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetClientAccountsForOrganization(OLOrganizationID);
            });
        }

        public ExceptionTypeSummaryResponse GetExceptionTypeSummary(EntityDTO entity, WorkgroupIDsDTO workgroup, DateTime datefrom, DateTime dateto)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetExceptionTypeSummary(entity, workgroup, datefrom, dateto);
            });
        }

        public PermissionsResponse CheckUserPermissionsForFunction(string function, string mode)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).CheckUserPermissionsForFunction(function, mode);
            });
        }

        public BaseResponse WriteAuditEvent(string eventName, string eventType, string applicationName, string description)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).WriteAuditEvent(eventName, eventType, applicationName, description);
            });
        }

        public BaseGenericResponse<List<WorkgroupNameDTO>> GetActiveWorkgroups(EntityDTO entity)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetActiveWorkgroups(entity);
            });
        }

        public BaseGenericResponse<List<WorkgroupNameDTO>> GetIntegraPayOnlyWorkgroups(EntityDTO entity)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetIntegraPayOnlyWorkgroups(entity);
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            var r360ServicesManager = new R360ServicesManager(serviceContext);
            r360ServicesManager.LogEvent += EventLog.logEvent;
            return r360ServicesManager;
        }

        public BaseGenericResponse<long> GetSourceBatchID(long batchID)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetSourceBatchID(batchID);
            });
        }
        public BaseGenericResponse<List<AuditUserDTO>> GetAuditUsers()
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetAuditUsers();
            });
        }

        public BaseGenericResponse<string> GetBankName(int siteBankId) {
            return FailOnError((r360Api) => {
                return ((R360ServicesManager) r360Api).GetBankName(siteBankId);
            });
        }

        public BaseGenericResponse<List<DDASummaryDataDto>> GetDDASummary(DateTime depositDate)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetDDASummary(depositDate);
            });
        }

        public AdvancedSearchResultsDto GetAdvancedSearch(AdvancedSearchDto requestContext)
        {
            return FailOnError(r360Api => ((R360ServicesManager) r360Api).GetAdvancedSearch(requestContext));
        }

        public BatchSummaryResponseDto GetBatchSummary(BatchSummaryRequestDto requestContext)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetBatchSummary(requestContext);
            });
        }

        public BatchDetailResponseDto GetBatchDetail(BatchDetailRequestDto requestContext)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetBatchDetail(requestContext);
            });
        }

        public TransactionDetailResponseDto GetTransactionDetail(TransactionDetailRequestDto request)
        {
            return FailOnError((r360API) => ((R360ServicesManager) r360API).GetTransactionDetail(request));
        }

        public NotificationsResponseDto GetNotifications(NotificationsRequestDto request)
        {
            return FailOnError((r360API) => ((R360ServicesManager)r360API).GetNotificationsResponse(request));
        }

        public NotificationResponseDto GetNotification(NotificationRequestDto request)
        {
            return FailOnError((r360API) => ((R360ServicesManager)r360API).GetNotification(request));
        }

        public NotificationsFileTypesResponseDto GetNotificationsFileTypesResponse()
        {
            return FailOnError((r360API) => ((R360ServicesManager) r360API).GetNotificationFileTypes());
        }

        public PaymentTypesResponseDto GetPaymentTypes()
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetPaymentTypes();
            });
        }

        public PaymentSourcesResponseDto GetPaymentSources()
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetPaymentSources();
            }); 
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> SaveStoredQuery(AdvancedSearchStoredQueryDto entity)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).SaveStoredQuery(entity);
            });
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> DeleteStoredQuery(Guid entity)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).DeleteStoredQuery(entity);
            });
        }

        public BaseGenericResponse<List<AdvancedSearchStoredQueryDto>> GetStoredQueries()
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetStoredQueries();
            });
        }

        public BaseGenericResponse<AdvancedSearchStoredQueryDto> GetStoredQuery(Guid id)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetStoredQuery(id);
            });
        }

        public PaymentSearchResponseDto GetPaymentSearch(PaymentSearchRequestDto request)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetPaymentSearch(request);
            });
        }

        public InvoiceSearchResponseDto GetInvoiceSearch(InvoiceSearchRequestDto request)
        {
            return FailOnError((r360API) =>
            {
                return ((R360ServicesManager)r360API).GetInvoiceSearch(request);
            });
        }
    }
}
