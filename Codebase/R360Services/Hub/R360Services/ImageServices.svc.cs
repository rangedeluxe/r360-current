﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.OlfRequestCommon;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
********************************************************************************/
namespace WFS.RecHub.R360Services
{
    public class ImageServices : BaseOLService, IImageServices
    {
        public PingResponse Ping()
        {
            return FailOnError((ignored) =>
            {
                EventLog.logEvent("Ping Received for " + OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString(), this.GetType().Name, RecHub.Common.MessageType.Information, RecHub.Common.MessageImportance.Debug);
                PingResponse response = new PingResponse();
                response.responseString = "Pong";
                return response;
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new ImageAPI(serviceContext);
        }

        public BaseGenericResponse<Stream> GetImageJobFileAsAttachment(Guid OnlineImageQueueID)
        {
            return FailOnError((imageAPI) =>
            {
                return ((ImageAPI)imageAPI).GetImageJobFileAsAttachment(OnlineImageQueueID);
            });
        }

        public BaseGenericResponse<byte[]> GetImageJobFileAsBuffered(Guid OnlineImageQueueID)
        {
            return FailOnError((imageAPI) =>
            {
                var response = ((ImageAPI)imageAPI).GetImageJobFileAsAttachment(OnlineImageQueueID);
                var bufferedResponse = new BaseGenericResponse<byte[]>();
                using (var memoryStream = new MemoryStream())
                {
                    response.Data.CopyTo(memoryStream);
                    bufferedResponse.Data =  memoryStream.ToArray();
                }
                return bufferedResponse;
            });
        }

        public BaseGenericResponse<DataSet> CreateViewAllJobRequest(XmlDocument Parms)
        {
            return FailOnError((imageAPI) =>
            {
                return ((ImageAPI)imageAPI).CreateViewAllJobRequest(Parms);
            });
        }

        public BaseGenericResponse<DataSet> CheckImageJobStatus(Guid OnlineImageQueueID)
        {
            return FailOnError((imageAPI) =>
            {
                return ((ImageAPI)imageAPI).CheckImageJobStatus(OnlineImageQueueID);
            });
        }

        public BaseGenericResponse<DataSet> CreateSearchResultsJobRequest(XmlDocument SearchParms, XmlDocument JobParms)
        {
            return FailOnError((imageAPI) =>
            {
                return ((ImageAPI)imageAPI).CreateSearchResultsJobRequest(SearchParms, JobParms);
            });
        }

        public BaseGenericResponse<DataSet> CreateViewSpecifiedJobRequest(XmlDocument Parms)
        {
            return FailOnError((imageAPI) =>
            {
                return ((ImageAPI)imageAPI).CreateViewSpecifiedJobRequest(Parms);
            });
        }

        public BaseGenericResponse<DataSet> CreateViewSelectedJobRequest(XmlDocument Parms)
        {
            return FailOnError((imageAPI) =>
            {
                return ((ImageAPI)imageAPI).CreateViewSelectedJobRequest(Parms);
            });
        }
        public BaseGenericResponse<DataSet> CreateInProcessExceptionJobRequest(InProcessExceptionImageRequestDto request)
        {
            return FailOnError(imageApi => ((ImageAPI) imageApi).CreateInProcessExceptionJobRequest(request));
        }
    }
}
