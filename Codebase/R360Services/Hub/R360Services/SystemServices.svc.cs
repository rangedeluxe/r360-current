﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
 * * WI 154797 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
********************************************************************************/
namespace WFS.RecHub.R360Services
{
    public class SystemServices : BaseOLService, ISystemServices
    {
        public PingResponse Ping()
        {
            return FailOnError((ignored) =>
            {
                EventLog.logEvent("Ping Received for " + OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString(), this.GetType().Name, RecHub.Common.MessageType.Information, RecHub.Common.MessageImportance.Debug);
                PingResponse response = new PingResponse();
                response.responseString = "Pong";
                return response;
            });
        }

        public PageActivityResponse InitPageActivity(string ScriptName, string QueryString)
        {
            return FailOnError((systemAPI) =>
            {
                return ((SystemAPI)systemAPI).InitPageActivity(ScriptName, QueryString);
            });
        }

        public BaseGenericResponse<cUserRAAM> GetSessionUser()
        {
            return FailOnError((systemAPI) =>
            {
                return ((SystemAPI)systemAPI).GetSessionUser();
            });
        }

        public BaseGenericResponse<cOLLockbox> GetLockboxByID(int OLWorkgroupsID)
        {
            return FailOnError((systemAPI) =>
            {
				return ((SystemAPI)systemAPI).GetLockboxByID(OLWorkgroupsID);
            });
        }

        public BaseGenericResponse<cOLLockbox> GetLockBoxByAccountIDBankID(int BankID, int LockboxID)
        {
            return FailOnError((systemAPI) =>
            {
                return new LockboxAPI(this.ServiceContext).GetLockBoxByAccountIDBankID(BankID, LockboxID);
            });
        }

        public BaseResponse LogActivity(ActivityCodes ActivityCode, Guid OnlineImageQueueID, int ItemCount, int BankID, int LockboxID)
        {
            return FailOnError((systemAPI) =>
            {
                return ((SystemAPI)systemAPI).LogActivity(ActivityCode, OnlineImageQueueID, ItemCount, BankID, LockboxID);
            });
        }

        public BaseResponse SetWebDeliveredFlag(Guid OnlineImageQueueID, bool InternetDelivered)
        {
            return FailOnError((systemAPI) =>
            {
                return ((SystemAPI)systemAPI).SetWebDeliveredFlag(OnlineImageQueueID, InternetDelivered);
            });
        }

        public BaseResponse UpdateActivityLockbox(long ActivityLogID, int BankID, int LockboxID)
        {
            return FailOnError((systemAPI) =>
            {
                return ((SystemAPI)systemAPI).UpdateActivityLockbox(ActivityLogID, BankID, LockboxID);
            });
        }

        public XmlDocumentResponse GetUserLockboxes(int UserID)
        {
            return FailOnError((lockboxAPI) =>
            {
                return CreateLockboxResponse(UserID, (LockboxAPI)lockboxAPI);
            });
        }

        private XmlDocumentResponse CreateLockboxResponse(int UserID, LockboxAPI lockboxAPI)
        {
            XmlDocument docLockboxes = null;
            XmlNode nodeLockboxes = null;
            XmlDocumentResponse response = null;

            try
            {
                nodeLockboxes = lockboxAPI.GetUserLockboxes().Data.AsXMLNode();
                docLockboxes = new XmlDocument();
                docLockboxes.AppendChild(docLockboxes.ImportNode(nodeLockboxes, true));

                response = new XmlDocumentResponse()
                {
                    Data = docLockboxes,
                    Status = StatusCode.SUCCESS
                };
            }
            catch (Exception)
            {
                response = new XmlDocumentResponse()
                {
                    Data = docLockboxes,
                    Status = StatusCode.FAIL
                };
            }

            return response;
        }

        public XmlDocumentResponse GetBaseDocument()
        {
            return FailOnError((systemAPI) =>
            {
                return ((SystemAPI)systemAPI).GetBaseDocument();
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new SystemAPI(serviceContext);
        }
    }
}
