﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.Data;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/15/2014
 * *
 * * Purpose:  
 * *
 * * Modification History
 * * WI 146250 BDH 06/15/2014 Created
 * * WI 165464 SAS 09/15/2014 Changes done for generating Report request 
********************************************************************************/
namespace WFS.RecHub.R360Services
{
    public class ReportServices : BaseOLService, IReportServices
    {
        public PingResponse Ping()
        {
            return FailOnError((ignored) =>
            {
                EventLog.logEvent("Ping Received for " + OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString(), this.GetType().Name, RecHub.Common.MessageType.Information, RecHub.Common.MessageImportance.Debug);
                PingResponse response = new PingResponse();
                response.responseString = "Pong";
                return response;
            });
        }

        public XmlDocumentResponse GetHOAPMASummary(XmlDocument Parms)
        {
            return FailOnError((reportAPI) =>
            {
                return ((ReportAPI)reportAPI).GetHOAPMASummary(Parms);
            });
        }

        public XmlDocumentResponse GetHOAPMADetail(XmlDocument Parms)
        {
            return FailOnError((reportAPI) =>
            {
                return ((ReportAPI)reportAPI).GetHOAPMADetail(Parms);
            });
        }

        public XmlDocumentResponse GetHOAParameters(XmlDocument Parms)
        {
            return FailOnError((reportAPI) =>
            {
                return ((ReportAPI)reportAPI).GetHOAParameters(Parms);
            });
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            return new ReportAPI(serviceContext);
        }

        public BaseGenericResponse<DataSet> CreateReportJobRequest(XmlDocument Parms)
        {
            return FailOnError((reportAPI) =>
            {
                return ((ReportAPI)reportAPI).CreateReportJobRequest(Parms);
            });
        }
    }
}
