﻿using Wfs.Logging.Performance;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Shared;
using WFS.RecHub.Common;
using WFS.LTA.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.PostDepositExceptions;
using WFS.RecHub.R360Services.R360ServicesAPI.Repositories;
using WFS.RecHub.ApplicationBlocks.Common;

namespace WFS.RecHub.R360Services
{
    public class PostDepositExceptionServices : LTAService, IPostDepositExceptionServices
    {
        private const string SITEKEY = "IPOnline";
        private cSiteOptions _siteOptions;
        private ltaLog _eventLog;

        public PostDepositExceptionServices()
        {
            _siteOptions = new cSiteOptions(SITEKEY);
            _eventLog = new ltaLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (LTAMessageImportance)SiteOptions.loggingDepth);
        }

        BaseGenericResponse<PostDepositTransactionsResponseDto> IPostDepositExceptionServices.GetPendingTransactions(PostDepositPendingTransactionsRequestDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi) api).GetPendingTransactions(request));
        }

        public BaseGenericResponse<PostDepositTransactionDetailResponseDto> GetTransaction(
            PostDepositTransactionRequestDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi) api).GetTransaction(request));
        }

        public BaseGenericResponse<PostDepositSavePayerResultDto> SavePayer(PostDepositSavePayerDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi) api).SavePayer(request));
        }

        public BaseGenericResponse<PostDepositTransactionSaveResultDto> SaveTransaction(PostDepositTransactionSaveDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi) api).SaveTransaction(request));
        }

        public BaseGenericResponse<bool> CompleteTransaction(PostDepositTransactionRequestDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi) api).CompleteTransaction(request));
        }

        public BaseResponse AuditTransactionDetailView(PostDepositExceptionTransactionDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi)api).AuditTransactionDetailView(request));
        }

        public BaseGenericResponse<PostDepositTransactionValidationResponseDto> GetTransactionValidationResults(
            PostDepositTransactionDetailResponseDto transactionResponse)
        {
            var result = FailOnError(api =>
                ((PostDepositExceptionsApi) api).GetTransactionValidationResults(transactionResponse));
            return result;
        }

        protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
        {
            var api = new PostDepositExceptionsApi(serviceContext,
                new R360ServicesDAL(SITEKEY),
                new PostDepositExceptionsDal(SITEKEY),
                new cSessionDAL(SITEKEY),
                new RaamClient(new ConfigHelpers.Logger(x => Api_LogEvent(x, string.Empty, MessageType.Information, MessageImportance.Verbose),
                    x => Api_LogEvent(x, string.Empty, MessageType.Warning, MessageImportance.Essential))),
                new LockboxAPI(serviceContext),
                new DatabasePaymentDataProvider(serviceContext),
                new DatabaseStubDataProvider(serviceContext),
                new DatabaseDataEntryDataProvider(serviceContext),
                new DatabasePostDepositDataProvider(serviceContext),
                new OLFImageProvider(serviceContext.GetSessionID(), SITEKEY, _eventLog),
                new BusinessRulesDal(SITEKEY),
                new DatabaseDocumentDataProvider(serviceContext),
                new PerformanceLogger(new StopwatchHighResolutionTimerProvider()),
                new RAAMPermissionProvider(),
                new WorkgroupBusinessRulesDAL(SITEKEY), 
                new PayerRepository(new R360DBConnectionSettings())
                );
            return api;
        }

        private void Api_LogEvent(string message, string src, MessageType messageType, MessageImportance messageImportance)
        {
            // MAYBE WE SHOULD MAKE ONE MORE LOGGING LIBRARY.
            var type = messageType == MessageType.Error ? LTAMessageType.Error
                : messageType == MessageType.Information ? LTAMessageType.Information
                : LTAMessageType.Warning;
            var import = messageImportance == MessageImportance.Debug ? LTAMessageImportance.Debug
                : messageImportance == MessageImportance.Essential ? LTAMessageImportance.Essential
                : LTAMessageImportance.Verbose;
            _eventLog.logEvent(message, src, type, import);
        }

        public BaseResponse LockTransaction(LockTransactionRequestDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi) api).LockTransaction(request));
        }

        public BaseResponse UnlockTransaction(LockTransactionRequestDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi) api).UnlockTransaction(request));
        }

        public BaseResponse UnlockTransactionWithoutOverride(LockTransactionRequestDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi)api).UnlockTransactionWithoutOverride(request));
        }

        public BaseGenericResponse<PayerResponseDto> GetPayerList(PayerLookupRequestDto request)
        {
            return FailOnError(api => ((PostDepositExceptionsApi)api).GetPayerList(request));
        }
    }
}