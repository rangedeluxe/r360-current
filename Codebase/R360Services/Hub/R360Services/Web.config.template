﻿<?xml version="1.0"?>
<configuration>
  <configSections>
    <section name="system.identityModel" type="System.IdentityModel.Configuration.SystemIdentityModelSection, System.IdentityModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
    <section name="wfs.raam.core" type="Wfs.Raam.Core.Configuration.CoreSettingsSection, Wfs.Raam.Core"/>
  </configSections>
  <system.identityModel configSource="identity.app.config"/>

  <wfs.raam.core applicationcontextswitchenabled="true"/>

  <connectionStrings>
    @@R360_DB_CONNECTIONSTRING@@
  </connectionStrings>

  <appSettings>
    <add key="localIni" value="@@APPDRIVE@@\WFSApps\RecHub\bin2\RecHub.ini"/>
    <add key="WCFConfigLocation" value="@@APPDRIVE@@\WFSApps\RecHub\bin2\WCFClients.app.config"/>
    <add key="siteKey" value="ipoServices"/>
  </appSettings>
  <system.web>
    <compilation debug="false" targetFramework="4.6"/>
    <httpRuntime targetFramework="4.6"/>
  </system.web>
  <system.serviceModel>
    <serviceHostingEnvironment multipleSiteBindingsEnabled="true">
      <serviceActivations>
        <add relativeAddress="R360Services.svc" service="WFS.RecHub.R360Services.R360Services"/>
        <add relativeAddress="PostDepositExceptionServices.svc" service="WFS.RecHub.R360Services.PostDepositExceptionServices"/>
        <add relativeAddress="CBXServices.svc" service="WFS.RecHub.R360Services.CBXServices"/>
        <add relativeAddress="ImageServices.svc" service="WFS.RecHub.R360Services.ImageServices"/>
        <add relativeAddress="RemitterServices.svc" service="WFS.RecHub.R360Services.RemitterServices"/>
        <add relativeAddress="ReportServices.svc" service="WFS.RecHub.R360Services.ReportServices"/>
        <add relativeAddress="SystemServices.svc" service="WFS.RecHub.R360Services.SystemServices"/>
        <add relativeAddress="UserServices.svc" service="WFS.RecHub.R360Services.UserServices"/>
        <add relativeAddress="ExceptionServices.svc" service="WFS.RecHub.R360Services.ExceptionServices"/>
      </serviceActivations>
    </serviceHostingEnvironment>
    <bindings>
      <ws2007FederationHttpBinding>
        <!-- Standard RAAM-Enabled HTTPS Binding -->
        <binding name="WfsRaamBinding" maxReceivedMessageSize="67108864" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647"/>
          <security mode="TransportWithMessageCredential">
            <message establishSecurityContext="false" issuedKeyType="BearerKey"/>
          </security>
        </binding>
      </ws2007FederationHttpBinding>
    </bindings>
    <services>
      <service name="WFS.RecHub.R360Services.R360Services" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.IR360Services"/>
      </service>
      <service name="WFS.RecHub.R360Services.PostDepositExceptionServices" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.IPostDepositExceptionServices"/>
      </service>
      <service name="WFS.RecHub.R360Services.CBXServices" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.ICBXServices"/>
      </service>
      <service name="WFS.RecHub.R360Services.ImageServices" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.IImageServices"/>
      </service>
      <service name="WFS.RecHub.R360Services.RemitterServices" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.IRemitterServices"/>
      </service>
      <service name="WFS.RecHub.R360Services.ReportServices" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.IReportServices"/>
      </service>
      <service name="WFS.RecHub.R360Services.SystemServices" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.ISystemServices"/>
      </service>
      <service name="WFS.RecHub.R360Services.UserServices" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.IUserServices"/>
      </service>
      <service name="WFS.RecHub.R360Services.ExceptionServices" behaviorConfiguration="Secure.ServiceBehavior">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.R360Services.Common.IExceptionServices"/>
      </service>
    </services>
    <behaviors>
      <serviceBehaviors>
        <behavior name="Secure.ServiceBehavior">
          <serviceDebug includeExceptionDetailInFaults="false"/>
          <serviceAuthorization principalPermissionMode="Always"/>
          <serviceCredentials useIdentityConfiguration="true"/>
        </behavior>
        <behavior name="">
          <serviceMetadata httpGetEnabled="true" httpsGetEnabled="true"/>
          <serviceDebug includeExceptionDetailInFaults="false"/>
        </behavior>
      </serviceBehaviors>
    </behaviors>
  </system.serviceModel>
  <system.webServer>
    <modules runAllManagedModulesForAllRequests="true"/>
  </system.webServer>
  <system.diagnostics>
    <trace autoflush="true"/>
    <sources>
      <!--  switchValue indicates level of logging service should record
        Critical        - 	  Logs the following exceptions: OutOfMemoryException,ThreadAbortException,StackOverflowException,
                              ConfigurationErrorsException, SEHException,Application start errors,Failfast events,System hangs,
                              Poison messages: message traces that cause the application to fail
        Error           -     All exceptions are logged.
        Warning         -     Events logged: The application is receiving more requests than its throttling settings allow, 
                              the receiving queue is near its maximum configured capacity, timeout has exceeded.Credentials are rejected
        Information     -     Messages helpful for monitoring and diagnosing system status, measuring performance or profiling are generated.
        Verbose         -     Positive events are logged. Events that mark successful milestones.Low level events for both user code and servicing are emitted.
        ActivityTracing -     Flow events between processing activities and components.
        All             -     All events are logged.
        Off             -     No logs.
      -->
      <source propagateActivity="true" name="System.ServiceModel" switchValue="Error,ActivityTracing">
        <listeners>
          <clear/>
          <add type="System.Diagnostics.DefaultTraceListener" name="Default">
            <filter type=""/>
          </add>
          <add name="CircularTraceListener" />
        </listeners>
      </source>
      <source name="Wfs.Raam.Core" switchName="Wfs.Raam.Core.Switch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <clear/>
          <add name="WfsRaamCoreTraceListener" initializeData="@@APPDRIVE@@\WFSApps\Logs\RecHubServices_wfs.raam.core.svclog" type="System.Diagnostics.XmlWriterTraceListener" traceOutputOptions="Timestamp"/>
        </listeners>
      </source>
    </sources>
    <switches>
      <add name="Wfs.Raam.Core.Switch" value="Verbose"/>
    </switches>
    <sharedListeners>
      <add name="CircularTraceListener" type="WFS.System.Tracing.CircularTraceListener, CircularTraceListener, Version=1.0, Culture=neutral"
     initializeData="@@APPDRIVE@@\WFSApps\Logs\RecHubServices_Trace.svclog" maxFileSizeKB="5000" />
    </sharedListeners>
  </system.diagnostics>
</configuration>