﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.DAL;
using DatabaseIntegrationTestHelpers;
using R360DatabaseIntegrationTestHelpers;

namespace R360IntegrationTests
{
    [TestClass]
    public class PostDepositExceptionsDal_IntegrationTests
    {
        private static IntegrationDatabase Database { get; set; }
        private static RecHubAudit RecHubAudit { get; set; }
        private static RecHubCommon RecHubCommon { get; set; }
        private static RecHubData RecHubData { get; set; }
        private static RecHubException RecHubException { get; set; }
        private static RecHubUser RecHubUser { get; set; }
        private static DateTime RunDate { get; set; }

        private const int SiteBankId = 1;
        private const byte TransactionExceptionAcceptedStatusKey = 1;

        private static void SetUpRecHubData()
        {
            RecHubData = new RecHubData(Database, RunDate);
            RecHubData.MakeTable("dimTransactionExceptionStatuses");
            RecHubData.MakeTable("factTransactionSummary");
        }

        private static void PopulateRecHubData()
        {
            RecHubData.InsertDimBank(1, SiteBankId, bankName: "Test Bank");
            RecHubData.InsertDimClientAccount(1, siteBankId: SiteBankId, siteClientAccountId: 123,
                shortName: "Test Client", longName: "Test Client");
            RecHubData.InsertDimBatchPaymentTypes(0, "TestingType",
                "Test Payment Type");
            RecHubData.InsertDimBatchSources(1, shortName: "TestingSource",
                longName: "Test Batch Source");
            RecHubData.InsertDimDepositStatuses(2, 850,
                "Deposit Complete");

            RecHubData.InsertDimTransactionExceptionStatuses(
                TransactionExceptionAcceptedStatusKey, "Accepted",
                "Exception addressed and transaction Accepted");
        }

        private static void SetUpAuditing()
        {
            RecHubAudit = new RecHubAudit(Database, RunDate);
            RecHubAudit.MakeTable("dimAuditApplications");
            RecHubAudit.MakeTable("dimAuditColumns");
            RecHubAudit.MakeTable("dimAuditEvents");
            RecHubAudit.MakeTable("factDataAuditMessages");
            RecHubAudit.MakeTable("factDataAuditDetails");
            RecHubAudit.MakeTable("factDataAuditSummary");
            RecHubAudit.MakeTable("factEventAuditMessages");
            RecHubAudit.MakeTable("factEventAuditSummary");
            RecHubAudit.MakeStoredProcedure("usp_dimAuditApplications_Get");
            RecHubAudit.MakeStoredProcedure("usp_dimAuditColumns_Get");
            RecHubAudit.MakeStoredProcedure("usp_factDataAuditSummary_Ins");
            RecHubAudit.MakeStoredProcedure("usp_factDataAuditMessages_Ins");
            RecHubAudit.MakeStoredProcedure("usp_factEventAuditSummary_Ins");
            RecHubAudit.MakeStoredProcedure("usp_factEventAuditMessages_Ins");
            RecHubAudit.MakeStoredProcedure("usp_dimAuditEvents_Get");
        }

        private static void SetUpCommon()
        {
            RecHubCommon = new RecHubCommon(Database, RunDate);
            RecHubCommon.MakeStoredProcedure("usp_WFS_EventAudit_Ins");
            RecHubCommon.MakeStoredProcedure("usp_WFS_DataAudit_Ins");
        }

        private static void SetUpExceptions()
        {
            RecHubException = new RecHubException(Database, RunDate);
            RecHubException.MakeTable("PostDepositBatchExceptions");
            RecHubException.MakeTable("PostDepositTransactionExceptions");
            RecHubException.MakeTable("PostDepositDataEntryDetails");
            RecHubException.MakeStoredProcedure("usp_PostDepositTransactionExceptions_AuditTransactionDetailView");
            RecHubException.MakeStoredProcedure("usp_PostDepositTransactionExceptions_Del");
            RecHubException.MakeStoredProcedure("usp_PostDepositTransactionExceptions_Ins");
            RecHubException.MakeStoredProcedure("usp_PostDepositTransactionExceptions_Complete");
        }

        private static void SetUpUser()
        {
            RecHubUser = new RecHubUser(Database, RunDate);
            RecHubUser.MakeTable("Users");
            RecHubUser.MakeStoredProcedure("usp_Users_UserID_Get_BySID");
        }
        private static IPostDepositExceptionsDal CreateDal()
        {
            var pdeDal = new PostDepositExceptionsDal("DummySiteKey");
            pdeDal.UseDatabase(Database.CDatabase);
            return pdeDal;
        }
        private static void AssertSuccess(Func<bool> action)
        {
            // If action throws an exception, great! The test will have a good stack trace.
            var success = action();

            // But if an exception happens in cDatabase, it will stop the exception's propagation and save it
            // for later. It's not clear why it does this, but we need to detect the condition and fail the test.
            Assert.AreEqual(
                GetSuccessString(true, null),
                GetSuccessString(success, Database.CDatabase.lastException));
        }
        private static string GetSuccessString(bool success, Exception exception)
        {
            return exception == null
                ? $"No exception, success = {success}"
                : $"Exception: \"{exception.Message}\"; success = {success}";
        }

        private static List<string> GetEventAuditMessages(int userId, string eventName, string applicationName=null)
        {
            //Get the audit message for validation
            Database.CDatabase.CreateDataTable(
                "DECLARE @ApplicationName VARCHAR(256) = " + (applicationName == null ? "NULL;" : "'" + applicationName + "';") +
                $"SELECT AuditMessage FROM RecHubAudit.factEventAuditMessages INNER JOIN RecHubAudit.dimAuditEvents ON " +
                "RecHubAudit.dimAuditEvents.AuditEventKey = RecHubAudit.factEventAuditMessages.AuditEventKey " +
                "INNER JOIN RecHubAudit.dimAuditApplications ON RecHubAudit.dimAuditApplications.AuditApplicationKey = RecHubAudit.factEventAuditMessages.AuditApplicationKey " +
                $"WHERE UserID = {userId} " +
                $"AND EventName = '{eventName}' AND ApplicationName = CASE WHEN @ApplicationName IS NULL THEN ApplicationName ELSE '{applicationName}' END ORDER BY AuditKey,AuditMessagePart",
                out var results);

            var auditMessages = new List<string>();
            foreach (DataRow row in results.Rows)
            {
                auditMessages.Add(row["AuditMessage"].ToString());
            }
            return auditMessages;
        }

        [ClassInitialize]
        public static void ClassSetUp(TestContext context)
        {
            RunDate = DateTime.Today;
            Database = IntegrationDatabase.CreateNew();
            SetUpCommon();
            SetUpUser();
            SetUpRecHubData();
            PopulateRecHubData();
            SetUpAuditing();
            SetUpExceptions();
        }

        [TestMethod]
        public void DeleteTransactionLock_DeleteExistingLock()
        {
            const int batchId = 121;
            const int transactionId = 32;
            const int userId = 559;
            string userName = $"Test User-{userId}";

            var userSid = Guid.NewGuid();

            RecHubUser.InsertUser(userId: userId, logonName: userName, raamSid: userSid);
            RecHubException.InsertPostDepositTransactionException(batchId: batchId, transactionId: transactionId,
                lockedByUserId: userId);

            AssertSuccess(() => CreateDal().DeleteTransactionLock(batchId, RunDate, transactionId, userName, userSid));

            const string sql = "SELECT COUNT(*) FROM RecHubException.PostDepositTransactionExceptions";
            Assert.AreEqual(0, Database.Connection.Query<int>(sql).Single(), "Record count after delete");
        }
        [TestMethod, Ignore("BUG: Stored procedure tries to COMMIT twice in this case")]
        public void DeleteTransactionLock_LockDoesNotExist()
        {
            const int batchId = 121;
            const int existingTransactionId = 32;
            const int transactionIdToUnlock = 33;
            const int userId = 559;
            string userName = $"Test User-{userId}";

            var userSid = Guid.NewGuid();

            RecHubUser.InsertUser(userId: userId, logonName: userName, raamSid: userSid);
            RecHubException.InsertPostDepositTransactionException(batchId: batchId,
                transactionId: existingTransactionId, lockedByUserId: userId);

            AssertSuccess(
                () => CreateDal().DeleteTransactionLock(batchId, RunDate, transactionIdToUnlock, userName, userSid));

            const string sql = "SELECT COUNT(*) FROM RecHubException.PostDepositTransactionExceptions";
            Assert.AreEqual(1, Database.Connection.Query<int>(sql).Single(), "Record count after delete");
        }

        [TestMethod]
        public void DeleteTransactionLock_AuditMessage()
        {
            const int batchId = 321;
            const int transactionId = 31;
            const int sourceBatchId = 199;
            const int userId = 600;
            string userName = $"Test User-{userId}";

            var userSid = Guid.NewGuid();

            RecHubUser.InsertUser(userId: userId, logonName: userName, raamSid: userSid);
            RecHubData.InsertFactTransactionSummary(bankKey: 1, clientAccountKey: 1, batchId: batchId, sourceBatchId: sourceBatchId,
                batchSourceKey: 1, batchPaymentTypeKey: 0, transactionExceptionStatusKey: TransactionExceptionAcceptedStatusKey,
                transactionId: transactionId, txnSequence: 1);

            var pdeDal = CreateDal();
            // first lock the record
            Assert.IsTrue(pdeDal.InsertTransactionLock(batchId, RunDate, transactionId, userName, userSid),
                "Add Lock Record");

            AssertSuccess(
                () => CreateDal().DeleteTransactionLock(batchId, RunDate, transactionId, userName, userSid));

            //Get the audit message for validation
            var auditMessages = GetEventAuditMessages(userId, "Post-Deposit Exceptions", "RecHubException.usp_PostDepositTransactionExceptions_Del");


            var expectedMessage =
                $"{userName} unlocked Post-Deposit Exceptions Transaction: {transactionId}, Transaction Sequence: 1 " +
                $"in Batch: {sourceBatchId}" +
                $" for Deposit Date: {RunDate:MM-dd-yyyy}, Bank: 1 - Test Bank, Workgroup: 123 - Test Client, " +
                "Payment Source: Test Batch Source, Payment Type: Test Payment Type";

            Assert.AreEqual(expectedMessage, string.Join("", auditMessages.ToArray()), "Audit Message");
        }

        [TestMethod]
        public void DeleteTransactionLock_Override_AuditMessage()
        {
            const int batchId = 322;
            const int transactionId = 32;
            const int sourceBatchId = 200;
            const int lockUserId = 610;
            const int unLockUserId = 611;
            string lockUserName = $"Test User-{lockUserId}";
            string unLockUserName = $"Test User-{unLockUserId}";

            var lockUserSid = Guid.NewGuid();
            var unLockUserSid = Guid.NewGuid();

            RecHubUser.InsertUser(userId: lockUserId, logonName: lockUserName, raamSid: lockUserSid);
            RecHubUser.InsertUser(userId: unLockUserId, logonName: lockUserName, raamSid: unLockUserSid);
            RecHubData.InsertFactTransactionSummary(bankKey: 1, clientAccountKey: 1, batchId: batchId, sourceBatchId: sourceBatchId,
                batchSourceKey: 1, batchPaymentTypeKey: 0, transactionExceptionStatusKey: TransactionExceptionAcceptedStatusKey,
                transactionId: transactionId, txnSequence: 1);

            var pdeDal = CreateDal();
            // first lock the record
            Assert.IsTrue(pdeDal.InsertTransactionLock(batchId, RunDate, transactionId, lockUserName, lockUserSid),
                "Add Lock Record");

            AssertSuccess(
                () => CreateDal().DeleteTransactionLock(batchId, RunDate, transactionId, unLockUserName, unLockUserSid));

            //Get the audit message for validation
            var unlockAuditMessages = GetEventAuditMessages(unLockUserId, "Post-Deposit Exceptions", "RecHubException.usp_PostDepositTransactionExceptions_Del");
            var lockAuditMessages = GetEventAuditMessages(lockUserId, "Post-Deposit Exceptions", "RecHubException.usp_PostDepositTransactionExceptions_Ins");

            var lockExpectedMessage =
                $"{lockUserName} locked Post-Deposit Exceptions Transaction: {transactionId}, Transaction Sequence: 1 " +
                $"in Batch: {sourceBatchId}" +
                $" for Deposit Date: {RunDate:MM-dd-yyyy}, Bank: 1 - Test Bank, Workgroup: 123 - Test Client, " +
                "Payment Source: Test Batch Source, Payment Type: Test Payment Type";

            var unlockExpectedMessage =
                $"{unLockUserName} unlocked Post-Deposit Exceptions Transaction: {transactionId}, Transaction Sequence: 1 " +
                $"in Batch: {sourceBatchId}" +
                $" for Deposit Date: {RunDate:MM-dd-yyyy}, Bank: 1 - Test Bank, Workgroup: 123 - Test Client, " +
                "Payment Source: Test Batch Source, Payment Type: Test Payment Type";

            Assert.AreEqual(lockExpectedMessage, string.Join("", lockAuditMessages.ToArray()), "Lock Audit Message");
            Assert.AreEqual(unlockExpectedMessage, string.Join("", unlockAuditMessages.ToArray()), "Unlock Audit Message");
        }

        [TestMethod]
        public void TransactionComplete_AuditMessage()
        {
            const long batchId = 100L;
            const int transactionId = 1;
            const int sourceBatchId = 99;
            const int userId = 1;
            string userName = $"Test User-{userId}";

            var userSid = Guid.NewGuid();

            //Set up the data
            RecHubUser.InsertUser(userId, logonName: userName, raamSid: userSid);
            RecHubData.InsertFactTransactionSummary(bankKey: 1, clientAccountKey: 1, batchId: batchId, sourceBatchId: sourceBatchId,
                batchSourceKey: 1, batchPaymentTypeKey: 0, transactionExceptionStatusKey: TransactionExceptionAcceptedStatusKey,
                transactionId: transactionId, txnSequence: 1);

            var pdeDal = CreateDal();
            // first lock the record
            Assert.IsTrue(pdeDal.InsertTransactionLock(batchId, RunDate, transactionId, userName, userSid),
                "Add Lock Record");
            //Complete the transaction using the DAL
            Assert.IsTrue(pdeDal.CompleteTransaction(batchId, RunDate, transactionId, userName),
                "Complete Transaction");
            //Get the audit message for validation
            var auditMessages = GetEventAuditMessages(userId, "Post-Deposit Exceptions", "RecHubException.usp_PostDepositTransactionExceptions_Complete");


            var expectedMessage =
                $"{userName} accepted the Post Deposit Exception(s) for Batch: {sourceBatchId}, Transaction: {transactionId}, Transaction Sequence: 1 " +
                $"for Deposit Date: {RunDate:MM-dd-yyyy}, Bank: 1 - Test Bank, Workgroup: 123 - Test Client, " +
                "Payment Source: Test Batch Source, Payment Type: Test Payment Type";

            Assert.AreEqual(expectedMessage, string.Join("", auditMessages.ToArray()), "Audit Message");
        }

        [TestMethod]
        public void TransactionComplete_ExceptionStatusKey()
        {
            const long batchId = 101L;
            const int transactionId = 1;
            const int sourceBatchId = 1;
            const int userId = 2;
            string userName = $"Test User-{userId}";

            var userSid = Guid.NewGuid();

            //Set up the data
            RecHubUser.InsertUser(userId, logonName: userName, raamSid: userSid);
            RecHubData.InsertFactTransactionSummary(bankKey: 1, clientAccountKey: 1, batchId: batchId, sourceBatchId: sourceBatchId,
                batchSourceKey: 1, batchPaymentTypeKey: 0, transactionExceptionStatusKey: TransactionExceptionAcceptedStatusKey,
                transactionId: transactionId, txnSequence: 1);

            var pdeDal = CreateDal();
            //First lock the record
            Assert.IsTrue(pdeDal.InsertTransactionLock(batchId, RunDate, transactionId, userName, userSid),
                "Add Lock Record");
            //Complete the transaction using the DAL
            Assert.IsTrue(pdeDal.CompleteTransaction(batchId, RunDate, transactionId, "Test User"),
                "Complete Transaction");
            //Get the status key for validation
            Database.CDatabase.CreateDataTable(
                $"SELECT TransactionExceptionStatusKey FROM RecHubData.factTransactionSummary WHERE BatchID = {batchId} AND TransactionID = {transactionId}",
                out var results);

            Assert.AreEqual(TransactionExceptionAcceptedStatusKey,results?.Rows[0]["TransactionExceptionStatusKey"], "TransactionComplete_ExceptionStatusKey");
        }

        [TestMethod]
        public void TransactionDetailsView_ViewedPage_AuditMessage()
        {
            const long batchId = 102L;
            const int transactionId = 1;
            const int sourceBatchId = 99;
            const int userId = 5;
            string userName = $"Test User-{userId}";

            var userSid = Guid.NewGuid();

            //Set up the data
            RecHubUser.InsertUser(userId, logonName: userName, raamSid: userSid);
            RecHubData.InsertFactTransactionSummary(bankKey: 1, clientAccountKey: 1, batchId: batchId, sourceBatchId: sourceBatchId,
                batchSourceKey: 1, batchPaymentTypeKey: 0, transactionExceptionStatusKey: TransactionExceptionAcceptedStatusKey,
                transactionId: transactionId, txnSequence: 1);

            var pdeDal = CreateDal();
            //Add the audit record
            Assert.IsTrue(pdeDal.AuditTransactionDetailView(batchId, RunDate, transactionId, userName, userSid));

            //Get the audit message for validation
            var auditMessages = GetEventAuditMessages(userId, "Viewed Page");

            var expectedMessage =
                $"Post-Deposit Exceptions Transaction Details page was viewed by {userName} for Batch: {sourceBatchId}, Transaction: {transactionId}, Transaction Sequence: 1 " +
                $"for Deposit Date: {RunDate:MM-dd-yyyy}, Bank: 1 - Test Bank, Workgroup: 123 - Test Client, " +
                "Payment Source: Test Batch Source, Payment Type: Test Payment Type";

            Assert.AreEqual(expectedMessage, string.Join("", auditMessages.ToArray()), "Audit Message");
        }

        [TestMethod]
        public void TransactionDetailsView_PostDepositExceptions_AuditMessage()
        {
            const long batchId = 101L;
            const int transactionId = 1;
            const int sourceBatchId = 99;
            const int userId = 15;
            string userName = $"Test User-{userId}";

            var userSid = Guid.NewGuid();

            //Set up the data
            RecHubUser.InsertUser(userId, logonName: userName, raamSid: userSid);
            RecHubData.InsertFactTransactionSummary(bankKey: 1, clientAccountKey: 1, batchId: batchId, sourceBatchId: sourceBatchId,
                batchSourceKey: 1, batchPaymentTypeKey: 0, transactionExceptionStatusKey: TransactionExceptionAcceptedStatusKey,
                transactionId: transactionId, txnSequence: 1);

            var pdeDal = CreateDal();
            //Add the audit record
            Assert.IsTrue(pdeDal.AuditTransactionDetailView(batchId, RunDate, transactionId, userName, userSid));

            //Get the audit message for validation
            var auditMessages = GetEventAuditMessages(userId, "Post-Deposit Exceptions", "RecHubException.usp_PostDepositTransactionExceptions_AuditTransactionDetailView");

            var expectedMessage =
                $"{userName} viewed the Post-Deposit Exceptions Transaction Details for Batch: {sourceBatchId}, Transaction: {transactionId}, Transaction Sequence: 1 " +
                $"for Deposit Date: {RunDate:MM-dd-yyyy}, Bank: 1 - Test Bank, Workgroup: 123 - Test Client, " +
                "Payment Source: Test Batch Source, Payment Type: Test Payment Type";

            Assert.AreEqual(expectedMessage, string.Join("", auditMessages.ToArray()), "Audit Message");
        }

        [TestMethod]
        public void TransactionLock_AuditMessage()
        {
            const long batchId = 105L;
            const int transactionId = 1;
            const int sourceBatchId = 199;
            const int userId = 25;
            string userName = $"Test User-{userId}";

            var userSid = Guid.NewGuid();

            //Set up the data
            RecHubUser.InsertUser(userId, logonName: userName, raamSid: userSid);
            RecHubData.InsertFactTransactionSummary(bankKey: 1, clientAccountKey: 1, batchId: batchId, sourceBatchId: sourceBatchId,
                batchSourceKey: 1, batchPaymentTypeKey: 0, transactionExceptionStatusKey: TransactionExceptionAcceptedStatusKey,
                transactionId: transactionId, txnSequence: 1);

            var pdeDal = CreateDal();
            //First lock the record
            Assert.IsTrue(pdeDal.InsertTransactionLock(batchId, RunDate, transactionId, userName, userSid),
                "Add Lock Record");

            //Get the audit message for validation
            var auditMessages = GetEventAuditMessages(userId, "Post-Deposit Exceptions", "RecHubException.usp_PostDepositTransactionExceptions_Ins");

            var expectedMessage =
                $"{userName} locked Post-Deposit Exceptions Transaction: {transactionId}, Transaction Sequence: 1 " +
                $"in Batch: {sourceBatchId}" +
                $" for Deposit Date: {RunDate:MM-dd-yyyy}, Bank: 1 - Test Bank, Workgroup: 123 - Test Client, " +
                "Payment Source: Test Batch Source, Payment Type: Test Payment Type";

            Assert.AreEqual(expectedMessage, string.Join("", auditMessages.ToArray()), "Audit Message");
        }
    }
}
