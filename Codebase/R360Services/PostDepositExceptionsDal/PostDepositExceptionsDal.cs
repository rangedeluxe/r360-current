using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.DAL
{
    public class PostDepositExceptionsDal : _DALBase, IPostDepositExceptionsDal
    {
        public PostDepositExceptionsDal(string vSiteKey) : base(vSiteKey)
        {
        }

        public bool DeleteTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, string userName, Guid SID)
        {
            bool bRtnval = false;
            SqlParameter[] parms;

            var depositdate = int.Parse(DepositDate.ToString("yyyyMMdd"));
            DataTable dt;

            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositdate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserName", SqlDbType.VarChar, userName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmErrorCode", SqlDbType.Int, 0, ParameterDirection.Output));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubException.usp_PostDepositTransactionExceptions_Del", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, int UserID)");
            }

            return bRtnval;
        }

        public bool InsertStubs(long batchid, DateTime depositdate, int transactionid, Guid sessionid, IEnumerable<PostDepositStubSaveSqlDto> stubs)
        {
            var bolRetVal = false;
            try
            {
                var depositDateKey = GetDepositDateKeyFromDate(depositdate);
                var fields = stubs.Select(x => new PostDepositStubSaveSqlDto()
                {
                    BatchSequence = x.BatchSequence,
                    DataEntryFields = x.DataEntryFields
                }).ToList();

                var dataEntryValues = CreateDataEntryValuesTable(fields);
                var parms = new[]
                {
                    BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input),
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, batchid, ParameterDirection.Input),
                    BuildParameter("@parmTransactionID", SqlDbType.Int, transactionid, ParameterDirection.Input),
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionid, ParameterDirection.Input),
                    BuildParameter("@parmDataEntryValues", SqlDbType.Structured, dataEntryValues, ParameterDirection.Input)
                };
                bolRetVal = Database.executeProcedure("RecHubException.usp_Insert_RelatedItems", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name,
                    "InsertStubs(long batchid, DateTime depositdate, int transactionid, IEnumerable<PostDepositStubSaveSqlDto> stubs)");
                bolRetVal = false;
            }
            return (bolRetVal);
        }

        public bool GetPendingTransactions(PostDepositPendingTransactionsRequestSqlDto request, out int totalrecords, out int filteredrecords, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            filteredrecords = 0;
            totalrecords = 0;
            SqlParameter recordstotal;
            SqlParameter recordsfiltered;
            try
            {
                var parms = new List<SqlParameter>()
                {
                    BuildParameter("@parmSessionId", SqlDbType.UniqueIdentifier, request.SessionId, ParameterDirection.Input),
                    BuildParameter("@parmEntityBreadcrumbs", SqlDbType.Xml, request.Entities.OuterXml, ParameterDirection.Input),
                    BuildParameter("@parmStart", SqlDbType.Int, request.Start, ParameterDirection.Input),
                    BuildParameter("@parmLength", SqlDbType.Int, request.Length, ParameterDirection.Input),
                    BuildParameter("@parmOrderBy", SqlDbType.VarChar, request.OrderBy, ParameterDirection.Input),
                    BuildParameter("@parmOrderDirection", SqlDbType.VarChar, request.OrderByDirection, ParameterDirection.Input),
                    BuildParameter("@parmSearch", SqlDbType.VarChar, request.Search ?? string.Empty, ParameterDirection.Input)
                };
                recordsfiltered = BuildParameter("@parmRecordsFiltered", SqlDbType.Int, filteredrecords, ParameterDirection.Output);
                recordstotal = BuildParameter("@parmRecordsTotal", SqlDbType.Int, totalrecords, ParameterDirection.Output);
                parms.Add(recordsfiltered);
                parms.Add(recordstotal);
                bolRetVal = Database.executeProcedure("RecHubData.usp_factExceptionTransactions_Get", parms.ToArray(), out tempdt);
                if (bolRetVal)
                {
                    filteredrecords = (int)recordsfiltered.Value;
                    totalrecords = (int)recordstotal.Value;
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetPendingTransactions(out DataTable dt)");
            }

            dt = tempdt;
            return (bolRetVal);
        }
        public bool GetPostDepositDataEntryDetails(PostDepositDataEntryDetailsRequestSqlDto request,
            out DataTable dataTable)
        {
            var result = false;
            dataTable = null;
            try
            {
                var parameters = new[]
                {
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, request.SessionId, ParameterDirection.Input),
                    BuildParameter("@parmSiteBankID", SqlDbType.Int, request.BankId, ParameterDirection.Input),
                    BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, request.WorkgroupId, ParameterDirection.Input),
                    BuildParameter("@parmDepositDate", SqlDbType.DateTime, request.DepositDate, ParameterDirection.Input),
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, request.BatchId, ParameterDirection.Input),
                    BuildParameter("@parmTransactionID", SqlDbType.Int, request.TransactionId, ParameterDirection.Input),
                    BuildParameter("@parmBatchSequence", SqlDbType.Int, request.BatchSequence, ParameterDirection.Input)
                };

                DataTable tempDataTable;
                result = Database.executeProcedure(
                    "RecHubException.usp_PostDepositTransactionExceptions_DataEntryDetails_Get", parameters,
                    out tempDataTable);
                dataTable = DALLib.ConvertDataTable(ref tempDataTable);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name, nameof(GetPostDepositDataEntryDetails));
            }

            return result;
        }
        public bool GetTransaction(PostDepositTransactionRequestSqlDto request, out DataTable dt)
        {
            bool bolRetVal = false;
            dt = null;
            DataTable tempdt = null;
            SqlParameter[] parms;
            var depositdatekey = GetDepositDateKeyFromDate(request.DepositDate);
            try
            {
                parms = new SqlParameter[] {
                    BuildParameter("@parmSessionId", SqlDbType.UniqueIdentifier, request.SessionId, ParameterDirection.Input),
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, request.BatchId, ParameterDirection.Input),
                    BuildParameter("@parmTransactionID", SqlDbType.Int, request.TransactionId, ParameterDirection.Input),
                    BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositdatekey, ParameterDirection.Input)
                };
                bolRetVal = Database.executeProcedure("RecHubData.usp_factExceptionTransactionDetail_Get", parms, out tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetTransaction(PostDepositTransactionRequestDto request, out DataTable dt)");
            }

            dt = tempdt;
            return (bolRetVal);
        }
        public bool GetTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, out DataTable dt)
        {
            bool bRtnval = false;
            SqlParameter[] parms;

            var depositdate = GetDepositDateKeyFromDate(DepositDate);
            DataTable tempdt = null;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositdate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubException.usp_PostDepositTransactionExceptions_Get", parms, out tempdt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, int UserID, out DataTable dt)");
            }
            dt = tempdt;
            return bRtnval;
        }
        public bool InsertTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, string userName, Guid SID)
        {
            bool bRtnval = false;
            SqlParameter[] parms;

            DataTable dt;
            var depositdate = int.Parse(DepositDate.ToString("yyyyMMdd"));

            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.BigInt, BatchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositdate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTransactionID", SqlDbType.Int, TransactionID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserName", SqlDbType.VarChar, userName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmErrorCode", SqlDbType.Int, 0, ParameterDirection.Output));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure("RecHubException.usp_PostDepositTransactionExceptions_Ins", parms, out dt);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, int UserID)");
            }

            return bRtnval;
        }

        public bool SavePayer(PostDepositSavePayerSqlDto request)
        {
            var retval = true;
            //return values from SP
            int errorCode = 0;
            int workgroupPayerKey = 0;

            try
            {
                var parms = new[]
                {
                    BuildParameter("@parmSiteBankID", SqlDbType.Int, request.SiteBankId, ParameterDirection.Input),
                    BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, request.SiteClientAccountId, ParameterDirection.Input),
                    BuildParameter("@parmRoutingNumber", SqlDbType.VarChar, request.RoutingNumber, ParameterDirection.Input),
                    BuildParameter("@parmAccount", SqlDbType.VarChar, request.Account, ParameterDirection.Input),
                    BuildParameter("@parmPayerName", SqlDbType.VarChar, request.PayerName, ParameterDirection.Input),
                    BuildParameter("@parmUserId", SqlDbType.Int, request.UserId, ParameterDirection.Input),
                    BuildParameter("@parmUserName", SqlDbType.VarChar, request.UserName, ParameterDirection.Input),
                    BuildParameter("@parmIsDefault", SqlDbType.Bit, request.IsDefault, ParameterDirection.Input),
                    BuildParameter("@parmWorkgroupPayerKey", SqlDbType.Int, workgroupPayerKey, ParameterDirection.Output)
                };
                retval = Database.executeProcedure("RecHubData.usp_WorkgroupPayers_Upsert",parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "SavePayer(PostDepositTransactionSavePayerSqlDto request)");
                retval = false;
            }

            return retval;
        }

        public bool SavePaymentsRemitters(PostDepositTransactionSaveSqlDto request)
        {
            var retval = true;
            try
            {
                var depositDateKey = GetDepositDateKeyFromDate(request.DepositDate);
                var checks = CreateCheckPayerValuesTable(request.Checks);

                var parms = new[]
                {
                        BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input),
                        BuildParameter("@parmBatchID", SqlDbType.BigInt, request.BatchId, ParameterDirection.Input),
                        BuildParameter("@parmTransactionID", SqlDbType.Int, request.TransactionId,
                            ParameterDirection.Input),
                        BuildParameter("@parmRemitterNameValues", SqlDbType.Structured, checks,
                            ParameterDirection.Input),
                    };

                retval &= Database.executeProcedure("RecHubException.usp_factChecksRemitter_Upd", parms);


            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name,
                    "SavePaymentsRemitters(PostDepositTransactionSaveSqlDto request)");
                retval = false;
            }
            return retval;
        }

        public bool SaveTransaction(PostDepositTransactionSaveSqlDto request)
        {
            var bolRetVal = false;
            try
            {
                var depositDateKey = GetDepositDateKeyFromDate(request.DepositDate);
                var dataEntryValues = CreateDataEntryValuesTable(request.DataEntryFields);
                var parms = new[]
                {
                    BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input),
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, request.BatchId, ParameterDirection.Input),
                    BuildParameter("@parmTransactionID", SqlDbType.Int, request.TransactionId,
                        ParameterDirection.Input),
                    BuildParameter("@parmDataEntryValues", SqlDbType.Structured, dataEntryValues,
                        ParameterDirection.Input)
                };
                WriteDataTableToLog(dataEntryValues);
                bolRetVal = Database.executeProcedure("RecHubException.usp_UpSert_RelatedItems", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name,
                    "SaveTransaction(PostDepositTransactionSaveSqlDto request, out DataTable dt)");
                bolRetVal = false;
            }
            return (bolRetVal);
        }

        public bool CompleteTransaction(long batchId, DateTime depositDate, int transactionId, string userName)
        {
            try
            {
                var depositDateKey = GetDepositDateKeyFromDate(depositDate);
                var parms = new[]
                {
                    BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input),
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, batchId, ParameterDirection.Input),
                    BuildParameter("@parmTransactionID", SqlDbType.Int, transactionId,
                        ParameterDirection.Input),
                    BuildParameter("@parmUserName", SqlDbType.VarChar, userName,ParameterDirection.Input)
                };
                return Database.executeProcedure("RecHubException.usp_PostDepositTransactionExceptions_Complete", parms);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool AuditTransactionDetailView(long batchId, DateTime depositDate, int transactionId, string userName, Guid SID)
        {
            try
            {
                var depositDateKey = GetDepositDateKeyFromDate(depositDate);
                var parms = new[]
                {
                    BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input),
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, batchId, ParameterDirection.Input),
                    BuildParameter("@parmTransactionID", SqlDbType.Int, transactionId,ParameterDirection.Input),
                    BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input),
                    BuildParameter("@parmUserName", SqlDbType.VarChar, userName,ParameterDirection.Input)
                };
                return Database.executeProcedure("RecHubException.usp_PostDepositTransactionExceptions_AuditTransactionDetailView", parms);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public bool DeleteStub(long batchid, DateTime depositdate, int transactionid, int batchsequence)
        {
            var bolRetVal = false;
            try
            {
                var depositDateKey = GetDepositDateKeyFromDate(depositdate);
                var parms = new[]
                {
                    BuildParameter("@parmDepositDateKey", SqlDbType.Int, depositDateKey, ParameterDirection.Input),
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, batchid, ParameterDirection.Input),
                    BuildParameter("@parmTransactionID", SqlDbType.Int, transactionid, ParameterDirection.Input),
                    BuildParameter("@parmBatchSequence", SqlDbType.Int, batchsequence, ParameterDirection.Input)
                };
                bolRetVal = Database.executeProcedure("RecHubException.usp_del_RelatedItem", parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name, "DeleteStub(long batchid, DateTime depositdate, int transactionid, int batchsequence)");
                bolRetVal = false;
            }
            return (bolRetVal);
        }

        private void WriteDataTableToLog(DataTable dataEntryValues)
        {
            var recordNum = 1;
            EventLog.logEvent("usp_UpSert_RelatedItems", "DataTable @parmDataEntryValues", MessageImportance.Debug);
            foreach (DataRow row in dataEntryValues.Rows)
            {
                EventLog.logEvent("", $"DataTableRecord:{recordNum++}", MessageImportance.Debug);
                foreach (DataColumn tableColumn in row.Table.Columns)
                {
                    EventLog.logEvent(row[tableColumn].ToString(), tableColumn.ColumnName + ":",
                        MessageImportance.Debug);
                }
            }
        }

        private int GetDepositDateKeyFromDate(DateTime depositDate)
        {
            return Convert.ToInt32(depositDate.ToString("yyyyMMdd", CultureInfo.InvariantCulture));
        }

        private static DataTable CreateCheckPayerValuesTable(IList<PostDepositCheckSaveDto> checks)
        {
            const string columnCheckSequence = "CheckSequence";
            const string ColumnPayer = "Payer";
            var table = new DataTable();

            table.Columns.Add(columnCheckSequence, typeof(int));
            table.Columns.Add(ColumnPayer, typeof(string));
            foreach (var check in checks)
            {
                var row = table.NewRow();
                row[columnCheckSequence] = check.CheckSequence;
                row[ColumnPayer] = check.Payer;
                table.Rows.Add(row);
            }
            return table;
        }

        private static DataTable CreateDataEntryValuesTable(IList<PostDepositStubSaveSqlDto> dataEntryFields)
        {
            const string columnBatchSequence = "BatchSequence";
            const string columnIsCheck = "IsCheck";
            const string columnValueDateTime = "DataEntryValueDateTime";
            const string columnValueFloat = "DataEntryValueFloat";
            const string columnValueMoney = "DataEntryValueMoney";
            const string columnValue = "DataEntryValue";
            const string columnFieldName = "FieldName";

            var table = new DataTable();

            table.Columns.Add(columnBatchSequence, typeof(int));
            table.Columns.Add(columnIsCheck, typeof(bool));
            table.Columns.Add(columnValueDateTime, typeof(DateTime));
            table.Columns.Add(columnValueFloat, typeof(float));
            table.Columns.Add(columnValueMoney, typeof(decimal));
            table.Columns.Add(columnValue, typeof(string));
            table.Columns.Add(columnFieldName, typeof(string));

            foreach (var postDepositStubSaveDto in dataEntryFields)
            {
                foreach (var field in postDepositStubSaveDto.DataEntryFields)
                {
                    var row = table.NewRow();
                    row[columnBatchSequence] = postDepositStubSaveDto.BatchSequence;
                    row[columnIsCheck] = false;
                    row[columnValueDateTime] = field.ValueDateTime ?? (object)DBNull.Value;
                    row[columnValueFloat] = field.ValueFloat ?? (object)DBNull.Value;
                    row[columnValueMoney] = field.ValueMoney ?? (object)DBNull.Value;
                    row[columnFieldName] = field.FieldName;
                    row[columnValue] = field.Value ?? "";
                    table.Rows.Add(row);
                }
            }
            return table;
        }

        public IEnumerable<DataEntrySetupDto> GetDataEntrySetupFields(Guid sessionId, int bankid, int workgroupid,
            long batchid, DateTime depositdate)
        {
            try
            {
                var parms = new[]
                {
                    BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionId, ParameterDirection.Input),
                    BuildParameter("@parmSiteBankID", SqlDbType.Int, bankid, ParameterDirection.Input),
                    BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, workgroupid, ParameterDirection.Input),
                    BuildParameter("@parmDepositDate", SqlDbType.DateTime, depositdate, ParameterDirection.Input),
                    BuildParameter("@parmBatchID", SqlDbType.BigInt, batchid, ParameterDirection.Input)
                };
                DataTable rawDataTable;
                var bolRetVal = Database.executeProcedure(
                    "RecHubException.usp_PostDepositTransactionExceptions_Get_DEColumns", parms, out rawDataTable);
                if (!bolRetVal)
                    throw new InvalidOperationException("Error running database query");

                var normalizedDataTable = DALLib.ConvertDataTable(ref rawDataTable);
                return normalizedDataTable.ToObjectList<DataEntrySetupDto>();
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, GetType().Name, nameof(GetDataEntrySetupFields));
                throw;
            }
        }
    }
}