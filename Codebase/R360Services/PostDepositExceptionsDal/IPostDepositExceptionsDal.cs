﻿using System;
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.DAL
{
    public interface IPostDepositExceptionsDal : IDisposable
    {
        bool DeleteTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, string userName, Guid SID);
        bool GetPendingTransactions(PostDepositPendingTransactionsRequestSqlDto request,
            out int totalrecords, out int filteredrecords, out DataTable dt);
        bool GetPostDepositDataEntryDetails(PostDepositDataEntryDetailsRequestSqlDto request, out DataTable dataTable);
        bool GetTransaction(PostDepositTransactionRequestSqlDto request, out DataTable dt);
        bool GetTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, out DataTable dt);
        bool InsertTransactionLock(long BatchID, DateTime DepositDate, int TransactionID, string userName, Guid SID);
        bool SaveTransaction(PostDepositTransactionSaveSqlDto request);
        IEnumerable<DataEntrySetupDto> GetDataEntrySetupFields(Guid sessionId, int bankid, int workgroupid,
            long batchid, DateTime depositdate);
        bool DeleteStub(long batchid, DateTime depositdate, int transactionid, int batchsequence);
        bool CompleteTransaction(long batchId, DateTime depositDate, int transactionId, string userName);
        bool AuditTransactionDetailView(long batchId, DateTime depositDate, int transactionId, string userName, Guid SID);
        bool InsertStubs(long batchid, DateTime depositdate, int transactionid, Guid sessionid, IEnumerable<PostDepositStubSaveSqlDto> stubs);
        bool SavePayer(PostDepositSavePayerSqlDto request);
        bool SavePaymentsRemitters(PostDepositTransactionSaveSqlDto request);
    }
}