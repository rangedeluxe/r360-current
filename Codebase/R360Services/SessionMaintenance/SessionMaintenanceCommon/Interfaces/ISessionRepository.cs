﻿using WFS.RecHub.ApplicationBlocks.Common;

namespace WFS.RecHub.SessionMaintenance.Common.Interfaces
{
    public interface ISessionRepository
    {
        void EstablishSession(string xmlDocument, bool refreshEntitlementsOnly);
    }
}
