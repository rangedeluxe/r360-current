﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.Common;
using WFS.RecHub.ApplicationBlocks.DataAccess;
using WFS.RecHub.SessionMaintenance.Common;
using WFS.RecHub.SessionMaintenance.Common.Interfaces;
using WFS.RecHub.ApplicationBlocks.Common.Extensions;

namespace WFS.RecHub.SessionMaintenance.API.Repositories
{
    public class SessionRepository : ISessionRepository
    {
        private ConnectionStringSettings _connectionStringSettings;

        public SessionRepository(R360DBConnectionSettings r360DbConnectionSettings)
        {
            _connectionStringSettings = r360DbConnectionSettings.ToConnectionStringSettings();
        }

        public void EstablishSession(string xmlDocument, bool refreshEntitlementsOnly)
        {
            if (string.IsNullOrEmpty(xmlDocument)) return;
            
            var dbProviderFactory = Database.GetFactory(_connectionStringSettings);

            var parms = new List<DbParameter>
            {
                Database.CreateParm("@parmSession", dbProviderFactory, DbType.Xml, ParameterDirection.Input, xmlDocument),
                Database.CreateParm("@parmRefreshEntitlements", dbProviderFactory, DbType.Boolean, ParameterDirection.Input, refreshEntitlementsOnly),
            };

            Database.ExecuteNonQuery(_connectionStringSettings, CommandType.StoredProcedure, "RecHubUser.usp_SessionRegistration", parms);
        }
    }
}
