﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.SessionMaintenance.API
{
	internal class WorkgroupInfo
	{
		public string ID;
		public int EntityID;
		public string EntityName;
	}
}
