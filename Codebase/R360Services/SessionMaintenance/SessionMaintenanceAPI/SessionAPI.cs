﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;

using WFS.RecHub.SessionMaintenance.Common;
using WFS.RecHub.SessionMaintenance.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Shared;

using Wfs.Raam.Service;
using Wfs.Raam.Service.Authorization.Contracts.ServiceContracts;
using Wfs.Raam.Core.WCF;
using Wfs.Raam.Core;
using Wfs.Raam.Core.Authorization;
using Wfs.Raam.Core.Services;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using WFS.RecHub.SessionMaintenance.Common.Interfaces;
using WFS.RecHub.SessionMaintenance.API.Repositories;
using WFS.RecHub.ApplicationBlocks.Common;

////using Wfs.Raam.Service.IdentityServer.Contracts;
////using Wfs.Raam.Service.IdentityServer.Contracts.ServiceContracts;
//using Wfs.Raam.Core.Helper;
////using Wfs.Raam.RelyingParty.ServiceContracts.Test;
////using Wfs.Raam.RelyingParty.DataContracts.Test;
//using Wfs.Raam.Service.Authorization.Contracts.DataContracts;



/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 05/24/2014
*
* Purpose: 
*
* Modification History
* WI 143046 TWE 05/24/2014 Created
* WI 187312 TWE 01/30/2015 log message when user has no workgroups
* ******************************************************************************/

namespace WFS.RecHub.SessionMaintenance.API
{
    public class SessionAPI : APIBase
    {
        #region API Wrappers
        
        protected virtual T Do<T>(Action<T> operation, [CallerMemberName] string procName = null)
           where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T();
            result.Status = StatusCode.FAIL;

            try
            {
				// Perform operation (no pre-conditions / security)
                operation(result);
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }

        #endregion

        private ISessionRepository _sessionRepository;

        public SessionAPI(R360ServiceContext serviceContext) : 
            this(serviceContext, new SessionRepository(new R360DBConnectionSettings()))
        { }

        public SessionAPI(R360ServiceContext serviceContext, ISessionRepository sessionRepositoryContext): base(serviceContext)
        {
            _sessionRepository = sessionRepositoryContext;
        }

        protected override bool ValidateSID(out int userID)
        {
			throw new NotImplementedException("The Session Maintenance Service does not validate SIDs");
        }

        protected override WFS.RecHub.Common.ActivityCodes RequestType
        {
            get { throw new NotImplementedException(); }
        }

        public void Ping(string logMessage)
        {
            OnLogEvent("Ping: " + logMessage, typeof(SessionAPI).Name, MessageType.Information, MessageImportance.Verbose);
        }

		public EstablishSessionResponse EstablishSession()
		{
			return EstablishSession(false);
		}

		public EstablishSessionResponse RefreshEntitlements()
		{
			return EstablishSession(true);
		}

        private EstablishSessionResponse EstablishSession(bool refreshEntitlementsOnly)
        {
            return Do<EstablishSessionResponse>((response) =>
            {
                try
                {
					OnLogEvent("Current User Claims: \r\n" + _ServiceContext.RAAMClaims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);
                    
                    var claims = WSFederatedAuthentication.ClaimsAccess.GetClaims();

                    string entityIDstr = string.Empty;
                    string entityName = string.Empty;
                    string userIDstr = string.Empty;
                    string sessionIDstr = string.Empty;
                    string sidstr = string.Empty;
					string userName = string.Empty;
					List<WorkgroupInfo> workgroups = new List<WorkgroupInfo>();
                    if (claims != null)
                    {
                        entityIDstr = WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.EntityId);
                        entityName = WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.EntityName);
                        userIDstr = WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.UserId);
                        sessionIDstr = WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.SessionId);
                        sidstr = WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.Sid);
						userName = WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.UserName);
                        //output the information
                        OnLogEvent("Entity ID:    >>" + entityIDstr + "<<", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
                        OnLogEvent("Entity Name:  >>" + entityName + "<<", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
                        OnLogEvent("User ID:      >>" + userIDstr + "<<", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
                        OnLogEvent("Session ID:   >>" + sessionIDstr + "<<", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
                        OnLogEvent("sidstr:       >>" + sidstr + "<<", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
						OnLogEvent("User Name:    >>" + userName + "<<", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
                    }
                    else
                    {
                        OnLogEvent("No claims found", this.GetType().Name, MessageType.Information, MessageImportance.Debug);
                    }

					// Get the workgroup authrorizations via the RAAM services
					if (GetAuthorizedWorkgroups(out workgroups))
					{
						string docXML;

						if (buildXML(sessionIDstr, sidstr, entityIDstr, entityName, userName, workgroups, out docXML))
						{
                            _sessionRepository.EstablishSession(docXML, refreshEntitlementsOnly);
						}

						response.Data = new EstablishSessionDTO {Session = new Guid(sessionIDstr), User = 0 };
						response.Status = StatusCode.SUCCESS;
					}
                }
                catch (Exception ex)
                {
					OnErrorEvent(ex, this.GetType().Name, "EstablishSession");
                }

            });
        }

        /// <summary>
        /// Helper class to write Start / End Elements with "using" syntax...
        /// </summary>
        internal class DisposableElement : IDisposable
		{
			private readonly XmlWriter _xmlWriter;
			public DisposableElement(XmlWriter xmlWriter, string elementName)
			{
				_xmlWriter = xmlWriter;
				xmlWriter.WriteStartElement(elementName);
			}

			public void Dispose()
			{
				_xmlWriter.WriteEndElement();
			}
		}

        //
        //buildXML(sessionIDstr, sidstr, entityIDstr, userIDstr out docXML))
        private bool buildXML(string sessionID, string RA3MSID, string entityID, string entityName, string userName, List<WorkgroupInfo> workgroups, out string xml)
        {
            bool bReturnValue = false;
			xml = null;
            bool viewAhead = R360Permissions.Current.Allowed(R360Permissions.Perm_ViewAhead, R360Permissions.ActionType.View);
			//Example XML:
            //<SessionEntitlements>
			//    <Session>
			//        <SessionID>{A3F664E3-9B66-48C3-A156-BECB51BAA2CF}</SessionID>
			//        <RA3MSID>{0EEA91B3-3810-4A1B-8C57-A17A8189EC18}</RA3MSID>
			//        <LogonEntityID>2</LogonEntityID>
			//        <LogonEntityName>TheName</LogonEntityName>
			//        <LogonName>administrator</LogonName>
			//    </Session>
            //    <ClientAccounts>
            //        <ClientAccount ID="1|123" EntityID="4" EntityName="Spacely Sprockets"/>
			//        <ClientAccount ID="2|123" EntityID="10" EntityName="Imperial Stormtroopers"/>
            //    </ClientAccounts>
            //</SessionEntitlements>
            try
            {
				StringBuilder sbXml = new StringBuilder();
				XmlWriter xmlWriter = XmlWriter.Create(sbXml, new XmlWriterSettings() { OmitXmlDeclaration = true });
				using (new DisposableElement(xmlWriter, "SessionEntitlements"))
				{
					using (new DisposableElement(xmlWriter, "Session"))
					{
						xmlWriter.WriteElementString("SessionID", sessionID);
						xmlWriter.WriteElementString("RA3MSID", RA3MSID);
						xmlWriter.WriteElementString("LogonEntityID", entityID);		
						xmlWriter.WriteElementString("LogonEntityName", entityName);
						xmlWriter.WriteElementString("LogonName", userName);
                        xmlWriter.WriteElementString("ViewBatchDataAheadOfDeposit", viewAhead ? "1" : "0");
					}
                    
					using (new DisposableElement(xmlWriter, "ClientAccounts"))
					{
						foreach (var workgroup in workgroups)
						{
							using (new DisposableElement(xmlWriter, "ClientAccount"))
							{
								xmlWriter.WriteAttributeString("ID", workgroup.ID);
								xmlWriter.WriteAttributeString("EntityID", workgroup.EntityID.ToString());
								xmlWriter.WriteAttributeString("EntityName", workgroup.EntityName);
							}
						}
					}
                    OnLogEvent("WorkGroupCount = " + workgroups.Count.ToString(), this.GetType().Name, MessageType.Information, MessageImportance.Debug);

                    if (workgroups.Count == 0)
                    {
                        OnLogEvent("Current user " + userName + " has no workgroups assigned for viewing purposes, Work Group Count = " + workgroups.Count.ToString(), 
                            this.GetType().Name, MessageType.Warning, MessageImportance.Essential);
                    }
				}

				// Return the XML string
				xmlWriter.Flush();
				xml = sbXml.ToString();
                bReturnValue = true;

            }
            catch (Exception ex)
            {
				OnErrorEvent(ex, this.GetType().Name, "buildXML");
			}
            return bReturnValue;
        }

		private bool GetAuthorizedWorkgroups(out List<WorkgroupInfo> workgroups)
		{
			workgroups = null;
			try
			{
				var client = new RaamClient(new ConfigHelpers.Logger(
						o => OnLogEvent(o, this.GetType().Name, MessageType.Information, MessageImportance.Verbose),
						o => OnLogEvent(o, this.GetType().Name, MessageType.Warning, MessageImportance.Essential)));
				var entity = client.GetAuthorizedEntitiesAndWorkgroups();

				workgroups = new List<WorkgroupInfo>();
				GetWorkgroups(entity, workgroups, client);

				return true;
			}
			catch (Exception ex)
			{
				OnErrorEvent(ex, this.GetType().Name, "GetAuthorizedWorkgroups");

				return false;
			}
		}

		private static void GetWorkgroups(EntityDto entity, List<WorkgroupInfo> workgroups, RaamClient client)
		{
			var resources = entity != null ? entity.Resources : null;
			if (entity != null)
			{
				if (resources != null)
					workgroups.AddRange(resources.Select(o => new WorkgroupInfo()
					{
						ID = o.ExternalReferenceID,
						EntityID = entity.ID,
						EntityName = entity.Name
					}));

				//check the child entities for workgroups
				if (entity.ChildEntities != null && entity.ChildEntities.Count() > 0)
				{
					foreach (var child in entity.ChildEntities)
					{
						GetWorkgroups(child, workgroups, client);
					}
				}
			}
		}
    }
}
