#Shell to Common R360Services Deployment Script, with correct parameters
$thisScript = Split-Path $MyInvocation.MyCommand.Path 
$commonScript = Join-Path $thisScript "..\..\ServiceShares\R360BuildDeployment.ps1"
$commonScript = [System.IO.Path]::GetFullPath($commonScript)

$commandWithParameters = "$commonScript" `
    + " -service RecHub.R360_Service.SessionMaintenanceService" `
    + " -appConfig:web.config" `
		+ " -configSubFolder:_PublishedWebsites\SessionMaintenanceService" `
    + " -folderName:RecHubSessionMaintenanceService" `
    + " -dlls:`"" `
		+ "DALBase.dll," `
	    + "Framework.SharedContracts.dll," `
	    + "ipoCrypto.dll," `
	    + "ipoDB.dll," `
	    + "ipoLib.dll," `
	    + "ipoLog.dll," `
	    + "ltaCommon.dll," `
	    + "ltaLog.dll," `
		+ "R360RaamClient.dll," `
	    + "R360Shared.dll," `
	    + "SessionDAL.dll," `
	    + "SessionMainteanceCommon.dll," `
	    + "SessionMaintenance.dll," `
	    + "SessionMaintenanceAPI.dll," `
	    + "SessionMaintenanceDAL.dll," `
	    + "Wfs.Raam.Core.dll," `
		+ "Wfs.Raam.Service.Authorization.Contracts.dll," `
	    + "Thinktecture.IdentityModel.dll," `
		+ "WFS.System.Logging.dll," `
		+ "Newtonsoft.Json.dll" `
        + "`"" `
    + " -web" `

Write-Host "Invoking Deployment Script with parameters: " $commandWithParameters

invoke-expression $commandWithParameters

Write-Host "Deployment Complete"

