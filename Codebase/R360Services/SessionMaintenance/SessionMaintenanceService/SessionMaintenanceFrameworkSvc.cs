﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WFS.RecHub.SessionMaintenance.Common;
using WFS.RecHub.SessionMaintenance.API;
using WFS.RecHub.R360Shared;
using Framework.SharedContracts;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 05/24/2014
*
* Purpose: 
*
* Modification History
* WI 143046 TWE 05/24/2014 Created
* ******************************************************************************/

namespace WFS.RecHub.SessionMaintenance
{
    public class SessionMaintenanceFrameworkSvc : LTAService, ISessionNotification
    {
		public SessionMaintenanceFrameworkSvc()
        {
        }

        //Establish User
		public void NewSession()
        {
            try
            {
                var results = FailOnError((oapi) =>
                {
                    var api = (SessionAPI)oapi;
                    return api.EstablishSession();
                });

                if (results.Status == StatusCode.FAIL)
                {
                    throw new Exception(String.Join(";", results.Errors));
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        #region LTAService Overrides

        protected override APIBase NewAPIInst(R360Shared.R360ServiceContext serviceContext)
        {
            return new SessionAPI(serviceContext);
        }

        #endregion
	}
}
