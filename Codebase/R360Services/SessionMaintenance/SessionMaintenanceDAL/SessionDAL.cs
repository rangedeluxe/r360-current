﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.DAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:  Tom Emery
* Date: 05/24/2014
*
* Purpose: 
*
* Modification History
* WI 143046 TWE 05/24/2014 Created
* ******************************************************************************/

namespace WFS.RecHub.SessionMaintenance.DAL
{
    internal class SessionDAL : _DALBase, ISessionDAL
    {
        public SessionDAL(string vSiteKey) : base(vSiteKey, ConnectionType.RecHubData)
        {
        }

        public bool EstablishSession(string xml, bool refreshEntitlementsOnly)
        {
            bool bReturnValue = false;

            // results = null;
            SqlParameter[] parms;
            try
            {
                parms = new SqlParameter[] {
                    BuildParameter("@parmSession", SqlDbType.Xml, xml, ParameterDirection.Input),
					BuildParameter("@parmRefreshEntitlements", SqlDbType.Bit, refreshEntitlementsOnly, ParameterDirection.Input)
                };
                bReturnValue = Database.executeProcedure("RecHubUser.usp_SessionRegistration",
                        parms);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "EstablishSession(string xml)");
                bReturnValue = false;
            }

            return bReturnValue;
        }
    }
}
