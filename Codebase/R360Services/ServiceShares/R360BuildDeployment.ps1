param(
    [string]$service,         #The name of the output folder
    [string]$folderName,      #Service Folder Name (web virtual directory or standalone)
    [string]$appConfig,       #The name of the app.config file
    [string]$dlls,            #Comma-delimited list of DLL Names to deploy
	[string]$configSubFolder, #e.g. _PublishedWebsites\MyWebService; leave blank if configs are in the "Binaries" folder...
    
    [switch]$web,        #Deploy in web service format? 
    [switch]$standalone, #Deploy as a stand-alone EXE? 
    [string]$exeName,    #Name (without extension) of the EXE to create
	[string]$exeDLLs,	 #DLLs to deploy with the EXE (instead of merging)
    
    [string]$pluginSubFolder,            #If provided, creates a subfolder for holding plugin DLLs
    [string]$pluginNamePattern,          #The comma-delimited name pattern(s) to use for plugin DLLs
    [string]$pluginExcludeNamePattern,   #The comma-delimited name pattern(s) to exclude for plugin DLLs
    [switch]$pluginIncludeConfig,        #If set, automatically include ".config" files with the same name pattern(s)

    [string]$sourcePath, #Used for testing only - leave blank in final scripts
    [string]$ilmergeExe,  #Used for testing only - leave blank in final scripts
	[string]$buildNumber = "",
	[string]$iteration = "",
	[string]$area = ""
    )

function Get-TFS 
(
    [string] $ServerName = $(Throw 'serverName is required')
)
{
    # load the required dll
    [void][System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.Client");

    $propertiesToAdd = (
        ('VCS', 'Microsoft.TeamFoundation.VersionControl.Client', 'Microsoft.TeamFoundation.VersionControl.Client.VersionControlServer'),
        ('WIT', 'Microsoft.TeamFoundation.WorkItemTracking.Client', 'Microsoft.TeamFoundation.WorkItemTracking.Client.WorkItemStore'),
        ('BS', 'Microsoft.TeamFoundation.Build.Common', 'Microsoft.TeamFoundation.Build.Proxy.BuildStore'),
        ('CSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.ICommonStructureService'),
        ('GSS', 'Microsoft.TeamFoundation', 'Microsoft.TeamFoundation.Server.IGroupSecurityService')
    );

    # fetch the TFS instance, but add some useful properties to make life easier
    # Make sure to "promote" it to a psobject now to make later modification easier
    [psobject] $tfs = [Microsoft.TeamFoundation.Client.TeamFoundationServerFactory]::GetServer($ServerName);
    foreach ($entry in $propertiesToAdd) 
	{
        $scriptBlock = '
            [System.Reflection.Assembly]::LoadWithPartialName("{0}") > $null
            $this.GetService([{1}])
        ' -f $entry[1],$entry[2];
        $tfs | add-member scriptproperty $entry[0] $ExecutionContext.InvokeCommand.NewScriptBlock($scriptBlock);
    }
    return $tfs;
}

function Has-WorkItems (
	[string] $iterPath,
	[string] $areaPath
) {
	$TFSServer = "http://TFSPROD:8080/tfs/wfs"
	$TFSTemp = Get-TFS $TFSServer;
	$WorkItemCollection = $TFSTemp.WIT.Query(
        "SELECT [System.Id] 
            FROM WorkItems 
            WHERE [System.AreaPath] under 'Receivables_Hub'
                AND [Microsoft.VSTS.CMMI.TaskType] != 'DB Change' 
                AND [System.IterationPath] under '" + $iterPath + "' 
                AND [System.AreaPath] under '" + $areaPath + "'
                AND [System.State] = 'Resolved'  
                AND [WFS.WorkItemStatus] = 'Ready For Build' 
                ORDER BY [System.Id]");
	return $WorkItemCollection.Count -ne 0;
}	
	
$doDeploy = $true;
if($iteration -ne "") {
	if($area -ne "") {
		$doDeploy = Has-WorkItems $iteration $area;
	}
}

if($doDeploy) {
	#This Script performs standard deployment of the Build for an R360 Service
	Write-Host "Deploying R360 2.1 Service Build To QA Pending 2.1\<Current> Folder"
	Write-Host "Script: " $MyInvocation.MyCommand.Path
	$scriptPath = Split-Path -Parent -Path $MyInvocation.MyCommand.Path

	#Source path is determined relative to where the script runs from... Which means this only works from a standard TFS Build setup...
	$sourcePathRootDir = Join-Path $scriptPath "..\..\..\..\Binaries\"
	$sourcePathRootDir = [System.IO.Path]::GetFullPath($sourcePathRootDir)

	IF ($sourcePath -ne "")
	{
		# Source Path has been overridden... Probably for Testing...
		$sourcePathRootDir = $sourcePath
	}

	Write-Host "Using Source Path Folder: " $sourcePathRootDir
	$sourceFiles = Join-Path $sourcePathRootDir "*"

	#Target path is hardcoded to the version (2.01)
	$targetPathRootDir = "\\10.27.2.172\ProductDevelopment\Legacy\QA Pending\RecHub_Build_Location\2015\2.01\"
	Write-Host "Copying To:          " $targetPathRootDir

	#Current Build is a standard sub-folder
	$targetPathCurrentDir = Join-Path $targetPathRootDir "Current_Build"
	Write-Host "Target Current:      " $targetPathCurrentDir

	#Also create a folder to hold this build - for now, just generate a unique build number (in the future, build number should be passed in...)
	if($buildNumber -eq "") {
		$today = Get-Date -format "yy.MM.dd"
		$targetPathBuildNbrDir = Join-Path $targetPathRootDir ("Bld_" + $today + ".")
		$bldNum = 1
		While($true) {
			$targetPathBldNbrTest = $targetPathBuildNbrDir + $bldNum
			If (Test-Path $targetPathBldNbrTest) {
				$bldNum++; 
			}
			else {
				break;
			}
		}
		$targetPathBuildNbrDir = $targetPathBldNbrTest
	}
	else {
		$targetPathBuildNbrDir = Join-Path $targetPathRootDir $buildNumber
	}

	Write-Host "Target Build Number: " $targetPathBuildNbrDir

	$outputDirs = @($targetPathBuildNbrDir, $targetPathCurrentDir)

	#Verify the caller specified the service label to use for the output
	IF ($service -eq "")
	{
		throw New-Object System.ArgumentException("[service] label was not specified", "service")
	}
	Write-Host "Service: " $service

	#Verify the caller specified the app.config file name
	IF ($appConfig -eq "")
	{
		throw New-Object System.ArgumentException("[appConfig] file name was not specified", "appConfig")
	}

	#Verify the caller specified a list of modules to copy to the output
	IF ($dlls -eq "")
	{
		throw New-Object System.ArgumentException("[dlls] module list was not specified", "dlls")
	}
	$aDlls = $dlls -split ",";
	Write-Host "DLL Count: " $aDlls.Count.ToString()

	#Verify the caller specified a folder name to put the output directly in
	IF ($folderName -eq "")
	{
		throw New-Object System.ArgumentException("[folderName] folder name was not specified", "folderName")
	}

	#Setup names / paths for configuration files
	$configSourceDir = Join-Path $sourcePathRootDir $configSubFolder
	$configSourceFiles = Join-Path $configSourceDir "*"

	# Perform web application deployment?
	IF ($web)
	{
		Write-Host "Deploying as Web Application..."
		foreach ($outputDir in $outputDirs)
		{
			# Use wwwroot\R360Services\<Folder Name> for web application output
			$targetDir = Join-Path (Join-Path (Join-Path $outputDir $service) "wwwroot") $folderName
			Write-Host "Copying To:          " $targetDir
			New-Item -ItemType Directory -Force -Path $targetDir > $null  #Create directory before trying to copy files in...
			
			# web.config
			$configTarget = Join-Path $targetDir "web.config.oem"
			Copy-Item -path $configSourceFiles -destination $configTarget -include $appConfig

			# bin
			$targetDir = Join-Path $targetDir "bin\"        
			New-Item -ItemType Directory -Force -Path $targetDir > $null  #Create directory before trying to copy files in...
			Copy-Item -path $sourceFiles -destination $targetDir -include $aDlls
			
			# Check for Plugin DLL subdirectory
			IF (!($pluginSubFolder -eq ""))
			{
				$targetDir = Join-Path $targetDir $pluginSubFolder
				New-Item -ItemType Directory -Force -Path $targetDir > $null  #Create directory before trying to copy files in...
				
				$includePatterns = $pluginNamePattern -split ","
				$excludePatterns = $pluginExcludeNamePattern -split ","
				
				Copy-Item -path $sourceFiles -destination $targetDir -include $includePatterns -exclude $excludePatterns
				
				IF ($pluginIncludeConfig)
				{
					$includePatterns = $includePatterns | ForEach-Object { $_ + ".config" } 
					$excludePatterns = $excludePatterns | ForEach-Object { $_ + ".config" } 

					Copy-Item -path $sourceFiles -destination $targetDir -include $includePatterns -exclude $excludePatterns

					#Rename configs to ".oem"
					$includePatterns | %{ Get-ChildItem -Path $targetDir -Filter $_ |  %{ 
						$newName = $_.name + ".oem"
						$newNamePath = $_.fullname + ".oem"
						If (Test-Path $newNamePath) { Remove-Item $newNamePath }
						Rename-Item -Path $_.fullname -NewName $newName 
					} } 
				}
			}
		}
	}

	# Perform EXE deployment?
	IF ($standalone)
	{
		Write-Host "Deploying as Standalone EXE..."
		
		#Setup paths for IL Merge
		$targetDir = Join-Path (Join-Path $outputDirs[0] $service) "bin2"
		$logOutputDir = Join-Path $outputDirs[0] "BuildLogs\"
		$logOutput = Join-Path $logOutputDir "ILMergeResult.log"
		New-Item -ItemType Directory -Force -Path $targetDir > $null  #Create directory before trying to copy files in...
		New-Item -ItemType Directory -Force -Path $logOutputDir > $null  #Create directory before trying to copy files in...

		#Verify the caller specified an EXE name to use
		IF ($exeName -eq "")
		{
			throw New-Object System.ArgumentException("[exeName] EXE name was not specified", "exeName")
		} 
		$exeOutput = Join-Path $targetDir ($exeName + ".exe")
		
		#Check for DLLs to exclude from merging
		IF ($exeDLLs -ne "")
		{
			$noMergeDlls = $exeDLLs -split ",";
			Write-Host "Exclude DLL Count: " $noMergeDlls.Count.ToString()
			#$libRefs = " /lib:`"" + $sourcePathRootDir + "`""  #Don't need this, since all DLLs are in the same folder...
		}

		#Compile list of DLLs to merge
		$mergeFiles = Join-Path $sourcePathRootDir "GenericWcfServiceHost.exe"
		Foreach ($file in $aDlls)
		{
			$temp = Join-Path $sourcePathRootDir $file
			IF (!($noMergeDlls -contains $file))
			{
				$mergeFiles = $mergeFiles + " " + $temp
			}
		}
		
		# Execute IL Merge Process
		IF ($ilmergeExe -eq "") { $ILMergeExe = "C:\Tools\ILMerge_2.12.0803\ILMerge.exe" }
		$args = "/targetplatform:`"v4,C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0`" /ndebug $mergeFiles /out:`"$exeOutput`" /log:`"$logOutput`" /allowDup"
		Write-Host $args
		$oProc = Start-Process $ilmergeExe -Args $args  -PassThru
		Wait-Process -inputObject $oProc
		Write-Host "Merge Complete"
		IF (!(Test-Path $exeOutput))
		{
			throw New-Object System.Exception("IL Merge Failed - EXE was not created (see logs at: " + $logOutput + ")")
		}
		
		foreach ($outputDir in $outputDirs)
		{
			# Use bin2 for EXE output
			$targetDir = Join-Path (Join-Path $outputDir $service) "bin2"
			Write-Host "Copying To:          " $targetDir
			New-Item -ItemType Directory -Force -Path $targetDir > $null  #Create directory before trying to copy files in...
			
			# app.config
			$configTarget = Join-Path $targetDir ($exeName + ".exe.config.oem")
			Copy-Item -path $configSourceFiles -destination $configTarget -include $appConfig
			
			# EXE
			IF ($outputDir -ne $outputDirs[0])
			{
				Copy-Item -path $exeOutput -destination $targetDir
			}

			# DLLs
			IF ($noMergeDlls.Count -gt 0)
			{
				Copy-Item -path $sourceFiles -destination $targetDir -include $noMergeDlls
			}
			
			# Check for Plugin DLL subdirectory
			IF (!($pluginSubFolder -eq ""))
			{
				$targetDir = Join-Path $targetDir $pluginSubFolder
				New-Item -ItemType Directory -Force -Path $targetDir > $null  #Create directory before trying to copy files in...
				
				$includePatterns = $pluginNamePattern -split ","
				$excludePatterns = $pluginExcludeNamePattern -split ","
				
				Copy-Item -path $sourceFiles -destination $targetDir -include $includePatterns -exclude $excludePatterns
				
				IF ($pluginIncludeConfig)
				{
					$includePatterns = $includePatterns | ForEach-Object { $_ + ".config" } 
					$excludePatterns = $excludePatterns | ForEach-Object { $_ + ".config" } 

					Copy-Item -path $sourceFiles -destination $targetDir -include $includePatterns -exclude $excludePatterns

					#Rename configs to ".oem"
					$includePatterns | %{ Get-ChildItem -Path $targetDir -Filter $_ |  %{ 
						$newName = $_.name + ".oem"
						$newNamePath = $_.fullname + ".oem"
						If (Test-Path $newNamePath) { Remove-Item $newNamePath }
						Rename-Item -Path $_.fullname -NewName $newName 
					} } 
				}
			}
		}
	}
}