﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Services {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main() {
            ServiceBase[] ServicesToRun;
            //for(int i = 30; i > 0; i--) {
            //    System.Threading.Thread.Sleep(1000);
            //}
            ServicesToRun = new ServiceBase[] 
            { 
                new R360WCFHost() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
