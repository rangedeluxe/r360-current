﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OLFServices;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/28/2013
 * *
 * * Purpose: This hosts the R360 Service as a windows service
 * *
 * * Modification History
 * WI 100422 CEJ 05/28/2013 Created
* ******************************************************************************/

namespace WFS.RecHub.R360Services {
    public partial class R360WCFHost : ServiceBase {
        cWCFHostBase _whbWebHost = null;
        cEventLog _EventLog = new cEventLog(System.Configuration.ConfigurationManager.AppSettings["BaseAddress"], 10000, MessageImportance.Debug);

        private string BaseAddress {
            get {
                return System.Configuration.ConfigurationManager.AppSettings["BaseAddress"];
            }
        }

      /*  public R360WCFHost() {
            try {
                _whbWebHost = new cWCFHostBase(BaseAddress, new cR360Services());
                _whbWebHost.LogEvent += _whbWebHost_LogEvent;
            }
            catch(Exception ex) {
                _EventLog.logError(ex, this.GetType().Name);
            }
        }*/

        void _whbWebHost_LogEvent(string sMessage, string sSource, MessageType mstEventType, MessageImportance msiEventImportance) {
            _EventLog.logEvent(sMessage, sSource, mstEventType, msiEventImportance);
        }

        protected override void OnPause() {
            base.OnPause();
            try {
                if(_whbWebHost != null) {
                    _whbWebHost.Close();
                }
            }
            catch(Exception ex) {
                _EventLog.logError(ex, this.GetType().Name);
            }
        }

        protected override void OnContinue() {
            base.OnContinue();
            try {
                if(_whbWebHost != null) {
                    _whbWebHost.Open();
                }
            }
            catch(Exception ex) {
                _EventLog.logError(ex, this.GetType().Name);
            }
        }

        protected override void OnStart(string[] args) {
            try {
                if(_whbWebHost != null) {
                    _whbWebHost.Open();
                }
            }
            catch(Exception ex) {
                _EventLog.logError(ex, this.GetType().Name);
            }
        }

        protected override void OnStop() {
            try {
                if(_whbWebHost != null) {
                    _whbWebHost.Close();
                    _whbWebHost.Dispose();
                    _whbWebHost = null;
                }
            }
            catch(Exception ex) {
                _EventLog.logError(ex, this.GetType().Name);
            }
        }
    }
}
