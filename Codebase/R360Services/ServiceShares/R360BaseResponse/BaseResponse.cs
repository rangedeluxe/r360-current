﻿
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WFS.RecHub.R360BaseResponse
{
    public enum StatusCode
    {
        SUCCESS,
        FAIL
    }

    [DataContract]
    public class BaseResponse
    {
        public BaseResponse()
        {
            Errors = new List<string>();
            Status = StatusCode.SUCCESS;
        }

        [DataMember]
        public StatusCode Status
        {
            get;
            set;
        }

        [DataMember]
        public List<string> Errors
        {
            get;
            set;
        }
    }
}
