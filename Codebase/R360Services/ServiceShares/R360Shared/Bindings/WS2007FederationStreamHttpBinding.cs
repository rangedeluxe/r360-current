﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Shared.Bindings
{
    public class WS2007FederationStreamHttpBindingElement : StandardBindingCollectionElement<WS2007FederationStreamHttpBinding, WS2007FederationStreamHttpBindingElementCollection>
    {


    }

    public class WS2007FederationStreamHttpBinding : WS2007FederationHttpBinding
    {

        public WS2007FederationStreamHttpBinding()
        {

        }


        protected override TransportBindingElement GetTransport()
        {
            return new HttpsTransportBindingElement()
            {
                TransferMode = TransferMode.Streamed,
                // Setting the max size to 4GB, as some 5k rows downloads can be pretty big.
                MaxReceivedMessageSize = 4294967296L
            };
            
        }

    }

    public class WS2007FederationStreamHttpBindingElementCollection : WS2007FederationHttpBindingElement
    {

        protected override Type BindingElementType
        {
            get { return typeof(WS2007FederationStreamHttpBinding); }
        }

    }
}
