﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Shared
{
    public class R360ContextContainer : IContextContainer
    {
        public IServiceContext Current
        {
            get
            {
                return R360ServiceContext.Current;
            }
        }
    }
}
