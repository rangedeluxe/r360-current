﻿using System;

namespace WFS.RecHub.R360Shared
{
	public static class LogManager
	{
		[ThreadStatic] private static ConfigHelpers.ILogger _logger;

		public static ConfigHelpers.ILogger Logger
		{
			get
			{
				if (_logger != null)
					return _logger;

				return new ConfigHelpers.Logger(o => Console.WriteLine("INFO: " + o), o => Console.WriteLine("WARN: " + o));
			}
			set
			{
				_logger = value;
			}
		}

		public static bool IsDefault
		{
			get
			{
				return _logger == null;
			}
		}
	}
}
