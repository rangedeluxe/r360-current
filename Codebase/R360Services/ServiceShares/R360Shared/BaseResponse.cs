﻿using System.Collections.Generic;
using System.Runtime.Serialization;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Charlie Johnson
 * * Date: 05/24/2013
 * *
 * * Purpose: Defines the base class to contain the call status for the return values of the 
 * *          WCF calls.
 * *
 * * Modification History
 * WI 100422 CEJ 05/24/2013  Created
 * ******************************************************************************/

namespace WFS.RecHub.R360Shared
{
    public enum StatusCode {
        SUCCESS,
        FAIL
    }

	[DataContract]
    public class BaseResponse {
        public BaseResponse() {
            Errors = new List<string>();
            Status = StatusCode.SUCCESS;
        }

		[DataMember]
        public StatusCode Status {
            get;
            set;
        }

		[DataMember]
        public List<string> Errors {
            get;
            set;
        }
    }

}
