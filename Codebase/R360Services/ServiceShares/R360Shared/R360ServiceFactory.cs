﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;

namespace WFS.RecHub.R360Shared
{
    /// <summary>
    /// This class loads WCF Client Channel information from the app.config.
    /// It was cloned from Wfs.Raam.Core.WCF.ServiceFactory -- in the near future, it will be modified to delegate to that implementation in most cases, but also support "Faking" to run disconnected from RAAM.
    /// </summary>
    public static class R360ServiceFactory
    {
        /// <summary>
        /// ClientSection from a configuration file
        /// </summary>
        private static readonly ClientSection _clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;

        /// <summary>
        /// BindingsSection from a configuration file
        /// </summary>
        private static readonly BindingsSection _bindingsSection = ConfigurationManager.GetSection("system.serviceModel/bindings") as BindingsSection;

        private static readonly bool _useRAAM;
        static R360ServiceFactory()
        {
            try
            {
                bool fakeRaam;
                bool.TryParse(ConfigHelpers.GetAppSetting("FakeRAAM", "false"), out fakeRaam);
                _useRAAM = !fakeRaam;
            }
            catch (Exception ex)
            {
                LogManager.Logger.LogWarning(ex.ToString());
            }
        }

        public static T Create<T>(WFS.RecHub.R360Shared.ConfigHelpers.ILogger log)
        {
            if (LogManager.IsDefault) LogManager.Logger = log;
            return Create<T>();
        }

        /// <summary>
        /// Creates a communicate channel to a service and returns service contract bound to the service for application consumption
        /// </summary>
        /// <typeparam name="T">Service Contract</typeparam>
        /// <returns>Service Contract</returns>
        public static T Create<T>()
        {
            T context = default(T); //initialize type to null

            string configLocation = ConfigHelpers.GetAppSetting("WCFConfigLocation", null);

            if (string.IsNullOrWhiteSpace(configLocation))
                configLocation = null;

            if (_useRAAM)
            {
                context = Wfs.Raam.Core.WCF.ServiceFactory.CreateUniversal<T>(configLocation);
            }
            else
            {
                var clientSection = _clientSection;	// Default to the app.config / web.config
                var bindingsSection = _bindingsSection; // Default to the app.config / web.config

                // Did the caller request a different configuration file?
                if (!string.IsNullOrWhiteSpace(configLocation))
                {
                    // Verify path
                    if (!Path.IsPathRooted(configLocation))
                    {
                        // Root it to the current location...
                        string basePath = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath);
                        configLocation = Path.Combine(basePath, configLocation);
                    }

                    // Load sections
                    var configuration = ConfigurationManager.OpenMappedExeConfiguration(new ExeConfigurationFileMap() { ExeConfigFilename = configLocation }, ConfigurationUserLevel.None);
                    clientSection = configuration.GetSection("system.serviceModel/client") as ClientSection;
                    bindingsSection = configuration.GetSection("system.serviceModel/bindings") as BindingsSection;
                }

                if (clientSection != null && bindingsSection != null)
                {
                    foreach (ChannelEndpointElement endpoint in clientSection.Endpoints)
                    {
                        if (endpoint.Contract.Equals(typeof(T).FullName, StringComparison.OrdinalIgnoreCase))
                        {
                            Binding binding = GetBinding(bindingsSection, endpoint.BindingConfiguration);

                            context = ChannelFactory<T>.CreateChannel(binding, new EndpointAddress(endpoint.Address));

                            break;
                        }
                    }
                }
            }

            if (context == null)
                throw new Exception(typeof(T).ToString() + " client connection information was not found (Configuration: " + (configLocation ?? "<local app.config>") + ")");

            return context;
        }

        /// <summary>
        /// Get the Binding defined for the service
        /// </summary>
        /// <param name="bindingsSection">The bindings section from the config file to search</param>
        /// <param name="bindingConfigurationName">Name of binding section</param>
        /// <returns>Binding object</returns>
        private static Binding GetBinding(BindingsSection bindingsSection, string bindingConfigurationName)
        {
            const int NO_BINDINGS = 0;

            var bindingTypeElement = (from currentBinding in bindingsSection.BindingCollections
                                      where currentBinding.ConfiguredBindings.Count > NO_BINDINGS
                                        && currentBinding.ContainsKey(bindingConfigurationName)
                                      select currentBinding).FirstOrDefault();

            if (bindingTypeElement == null)
                return null;

            var bindingElement = (from currentBinding in bindingTypeElement.ConfiguredBindings
                                  where currentBinding.Name == bindingConfigurationName
                                  select currentBinding).FirstOrDefault();

            if (bindingElement == null)
                return null;

            var binding = (Binding)Activator.CreateInstance(bindingTypeElement.BindingType);
            bindingElement.ApplyConfiguration(binding);

            return binding;
        }

        private static object GetServiceChannel(object proxy)
        {
            // Get Real Proxy from Transparent Proxy
            var serviceProxy = RemotingServices.GetRealProxy(proxy);
            if (serviceProxy == null) return null;

            // Assume Real Proxy is of type ServiceChannelProxy, with field "serviceChannel"
            var derivedType = serviceProxy.GetType();
            var fieldInfo = derivedType.GetField("serviceChannel", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fieldInfo == null) return null;

            // Get serviceChannel value
            return fieldInfo.GetValue(serviceProxy);
        }

        public static string GetRemoteAddress(object proxy)
        {
            var channel = GetServiceChannel(proxy) as IOutputChannel;
            if (channel == null)
                return "N/A";

            return channel.RemoteAddress.ToString();
        }

        public static IContextChannel GetContextChannel(object proxy)
        {
            return GetServiceChannel(proxy) as IContextChannel;
        }
    }
}
