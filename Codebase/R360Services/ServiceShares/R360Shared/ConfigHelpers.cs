﻿using System;
using System.Configuration;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

namespace WFS.RecHub.R360Shared
{
	public static class ConfigHelpers
	{
		public interface ILogger
		{
			void LogEvent(string message);
			void LogWarning(string message);
		}

		public class Logger : ILogger
		{
			public Action<string> _logEvent;
			public Action<string> _logWarning;

			public Logger(Action<string> logEvent, Action<string> logWarning)
			{
				_logEvent = logEvent;
				_logWarning = logWarning;
			}

			public void LogEvent(string message)
			{
				_logEvent(message);
			}

			public void LogWarning(string message)
			{
				_logWarning(message);
			}
		}

		// Generic app setting access
		public static string GetAppSetting(string settingName, string defaultValue)
		{
			try
			{
				string value = ConfigurationManager.AppSettings[settingName];
				if (string.IsNullOrEmpty(value))
					value = defaultValue;

				if (value != defaultValue)
					LogManager.Logger.LogEvent(string.Format("Using '{0}' = '{1}'", settingName, value));

				return value;
			}
			catch (Exception ex)
			{
				LogManager.Logger.LogWarning(string.Format("Error reading configuration setting {0} - {1}", settingName, ex.Message));
				return defaultValue;
			}
		}

		public static ILogger LTAtoILogger(this ltaLog eventLog, string logSource)
		{
			return new Logger(eventLog.Verbose(logSource), eventLog.VerboseWarning(logSource));
		}

		public static ILogger IPOtoILogger(this cEventLog eventLog, string logSource)
		{
			return new Logger(eventLog.Verbose(logSource), eventLog.VerboseWarning(logSource));
		}

		private static Action<string> Verbose(this ltaLog eventLog, string logSource)
		{
			return o => eventLog.logEvent(o, logSource, LTAMessageImportance.Verbose);
		}

		private static Action<string> Verbose(this cEventLog eventLog, string logSource)
		{
			return o => eventLog.logEvent(o, logSource, MessageImportance.Verbose);
		}

		private static Action<string> VerboseWarning(this ltaLog eventLog, string logSource)
		{
			return o => eventLog.logWarning(o, logSource, LTAMessageImportance.Verbose);
		}

		private static Action<string> VerboseWarning(this cEventLog eventLog, string logSource)
		{
			return o => eventLog.logWarning(o, logSource, MessageImportance.Verbose);
		}
	}
}
