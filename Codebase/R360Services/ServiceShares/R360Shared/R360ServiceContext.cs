﻿using System;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.Linq;
using WFS.RecHub.Common;
using Wfs.Raam.Core;
using System.Collections.Generic;
using Wfs.Raam.Core.Services;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:Ryan Schwantes
* Date: 6/22/2013
*
* Purpose: R360 Shared Class
*
* Modification History
*             RS 6/22/2013 - Initial Creation
*  WI 107711  WJS  7/5/2013 - Add SessionID 
* WI 146351 DJW 6/23/2014
*  Added Logging/Auditing support 
*  (these are part of Ryan's changes from the feature01 branch).
*********************************************************************************/
namespace WFS.RecHub.R360Shared
{
	[DataContract(Namespace = ServiceConstants.ServiceNamespaces.Root)]
	public class SimpleClaim
	{
		[DataMember]
		public string Type { get; set; }

		[DataMember]
		public string Value { get; set; }
	}

	[DataContract(Namespace = ServiceConstants.ServiceNamespaces.Root)]
	public class R360ServiceContext : IServiceContext
	{
		/// <summary>
		/// Unique name of object stored in headers
		/// </summary>
		internal const string HeaderServiceContext = "R360ServiceContext";

		[DataMember]
		public string SiteKey { get; set; }

		private SimpleClaim[] _RAAMClaims = null;
		public SimpleClaim[] RAAMClaims 
		{
			get
			{
				if (_RAAMClaims == null)
				{
					// Load ambient claims as simple claims for service to use...
                    CurrentPrincipal principal = WSFederatedAuthentication.CurrentPrincipal;
					_RAAMClaims = principal.Identity.Claims.Select(cv => new SimpleClaim() { Type = cv.Type, Value = cv.Value }).ToArray();
		        }
				return _RAAMClaims;
			}
		}

		private R360ServiceContext(string siteKey)
		{
			SiteKey = siteKey;
		}

		public static void Init()
		{
			Init(string.Empty);
		}

		public static void Init(string siteKey)
		{
			var context = new R360ServiceContext(siteKey);

			MessageHeader<R360ServiceContext> header = new MessageHeader<R360ServiceContext>(context);
            if (OperationContext.Current != null)
			    OperationContext.Current.OutgoingMessageHeaders.Add(header.GetUntypedHeader(HeaderServiceContext, ServiceConstants.ServiceNamespaces.Root));
		}

		/// <summary>
		/// Used for Testing; applies a "fake" context without requiring actual OperationContext values...
		/// </summary>
		internal static bool UseFakeServiceContext { get; set; }

		/// <summary>
		/// Gets or sets the current context a service call is running on
		/// </summary>
		/// <remarks>This is set on the client and retrieved on the host</remarks>
		/// <value>The current.</value>
		public static R360ServiceContext Current
		{
			get
			{
				if (UseFakeServiceContext)
				{
					PerformImpersonation();
					var context = new R360ServiceContext("MockSiteKey");
					return context;
				}
				else
				{
					// determine if servicecontext was sent in service headers
					if (OperationContext.Current != null)
					{
						int intIndexOfHeader = OperationContext.Current.IncomingMessageHeaders.FindHeader(HeaderServiceContext, ServiceConstants.ServiceNamespaces.Root);

						if (intIndexOfHeader >= 0)
						{
							// Return the header value
							return OperationContext.Current.IncomingMessageHeaders.GetHeader<R360ServiceContext>(intIndexOfHeader);
						}
					}

					// Not found in header - return a blank context
					return new R360ServiceContext(string.Empty);
				}
			}
		}

		public Guid GetSID()
		{
			if (RAAMClaims != null)
			{
				var sidClaim = RAAMClaims.FirstOrDefault(o => o.Type == ClaimTypes.Sid);
				Guid sid;
				if (sidClaim != null && Guid.TryParse(sidClaim.Value, out sid))
					return sid;
			}

			throw new InvalidSessionException();
		}

        public Guid GetSessionID()
        {
            if (RAAMClaims != null)
            {
                var sessionIDClaim = RAAMClaims.FirstOrDefault(o => o.Type == WfsClaimTypes.SessionId);
                Guid sessionID;
                if (sessionIDClaim != null && Guid.TryParse(sessionIDClaim.Value, out sessionID))
                    return sessionID;
            }

            throw new InvalidSessionException();
        }

	    public string GetLoginNameForAuditing()
	    {
	        if (RAAMClaims != null)
	        {
	            return RAAMClaims.FirstOrDefault(o => o.Type == ClaimTypes.Name)?.Value ?? "Name Not Defined";
	        }

	        throw new InvalidSessionException();
        }

        private static bool? _impersonateSid;
		private static Guid _fakeSid;
		private static Guid _fakeSession;
		private static bool ImpersonateSid()
		{
			if (!_impersonateSid.HasValue)
			{
				string sid = ConfigHelpers.GetAppSetting("ImpersonateSID", null);
				if (sid != null)
				{
					_impersonateSid = true;
					try
					{
						_fakeSid = new Guid(sid);
					}
					catch (Exception ex)
					{
						LogManager.Logger.LogWarning(ex.ToString());
					}

					try
					{
						_fakeSession = new Guid(ConfigHelpers.GetAppSetting("ImpersonateSessionID", null));
					}
					catch (Exception ex)
					{
						LogManager.Logger.LogWarning(ex.ToString());
					}
				}
				else
				{
					_impersonateSid = false;
				}
			}

			return _impersonateSid.Value;
		}

		public static void PerformImpersonation()
		{
			try
			{
				if (ImpersonateSid())
				{
					ClaimsPrincipal principal = ClaimsPrincipal.Current;
					if (principal != null)
					{
						System.Security.Claims.ClaimsIdentity identity = principal.Identities.ElementAt(0);

						var sidClaim = new Claim(System.Security.Claims.ClaimTypes.Sid, _fakeSid.ToString());
						if (!identity.HasClaim(sidClaim.Type, sidClaim.Value))
							identity.AddClaim(sidClaim);
						
						if (_fakeSession != Guid.Empty)
						{
							var sessionClaim = new Claim(WfsClaimTypes.SessionId, _fakeSession.ToString());
							if (!identity.HasClaim(sessionClaim.Type, sessionClaim.Value))
								identity.AddClaim(sessionClaim);
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogManager.Logger.LogWarning(ex.ToString());
			}
		}
	}
}
