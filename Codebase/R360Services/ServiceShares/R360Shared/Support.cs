﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:  
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New File
******************************************************************************/
namespace WFS.RecHub.R360Shared
{

  /// <summary></summary>
  public enum R360ServicesErrorCodes
  {
    /// <summary></summary>
    Success = 200,
    /// <summary></summary>
    UndefinedError = 700,
    /// <summary></summary>
    InvalidSiteKey = 701,
    /// <summary></summary>
    InvalidSessionID = 702,
    /// <summary></summary>
    InvalidRAAMClaim = 703,
    /// <summary></summary>
    SessionExpired = 704,
    /// <summary></summary>
    InvalidSession = 705,
    /// <summary></summary>
    CommunicationError = 731,
    /// <summary></summary>
    ConfigurationError = 732
  }

 
}