﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:  
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New file
* WI 107711 CEJ 10/16/2013
*   Create the ability log userActivity in the sessionLog from the R360 Services
******************************************************************************/
namespace WFS.RecHub.R360Shared
{
	public abstract class LTAService
	{
		private R360ServiceContext _ServiceContext = null;

		private cSiteOptions _SiteOptions = null;
		private cEventLog _EventLog = null;

		private const string INISECTION_R360SERVICES = "R360Services";
		private const string INIKEY_INTERMEDIATESERVER = "IntermediateServer";
		private const string INIKEY_SERVERLOCATION = "ServerLocation";

		protected R360ServiceContext ServiceContext
		{
			get
			{
				if (_ServiceContext == null)
					_ServiceContext = R360ServiceContext.Current;

				return _ServiceContext;
			}
		}

        protected abstract APIBase NewAPIInst(R360ServiceContext serviceContext);

        protected T FailOnError<T>(Func<APIBase, T> operation, [CallerMemberName] string procName = null)
            where T : BaseResponse, new()
        {
            try
            {
                APIBase api = NewAPIInst(ServiceContext);
                api.LogEvent += EventLog.logEvent;
                LogManager.Logger = EventLog.IPOtoILogger(this.GetType().Name);
                var response = operation(api);
                api.LogEvent -= EventLog.logEvent;
                return response;
            }
            catch (Exception ex)
            {
                LogGeneralException(ex, procName);
                return new T {Status = StatusCode.FAIL, Errors = new List<string> {"A system error occurred."}};
            }
        }

		/// <summary>
		/// Uses SiteKey to retrieve site options from the local .ini file.
		/// </summary>
		protected cSiteOptions SiteOptions
		{
			get
			{
				if (_SiteOptions == null)
					_SiteOptions = new cSiteOptions(ServiceContext.SiteKey);

				return _SiteOptions;
			}
		}

		/// <summary>
		/// Logging component using settings from the local siteOptions object.
		/// </summary>
		protected cEventLog EventLog
		{
			get
			{
				if (_EventLog == null)
				{
					_EventLog = new cEventLog(SiteOptions.logFilePath,
											  SiteOptions.logFileMaxSize,
											  (MessageImportance)SiteOptions.loggingDepth);
				}

				return _EventLog;
			}
		}

		protected string SiteKey
		{
			get { return ServiceContext.SiteKey; }
		}

        #region Operation Wrappers for Error Handling

		protected void LogGeneralException(Exception ex, string procName)
		{
			EventLog.logEvent("A general exception occurred", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
			EventLog.logError(ex, this.GetType().Name, procName);
		}

		protected ServerFaultException BuildGeneralException(Exception ex, string procName)
		{
			EventLog.logEvent("A general exception occurred", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
			EventLog.logError(ex, this.GetType().Name, procName);
			string msg = string.Format("A general exception occurred while executing {0}() method", procName);
			return (BuildServerFaultException(R360ServicesErrorCodes.UndefinedError, msg, msg));
		}

		protected ServerFaultException BuildIniReadException(Exception ex, string procName, string section, string key)
		{
			EventLog.logEvent("Unable to read ini setting: [" + section + "]." + key, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
			EventLog.logError(ex, this.GetType().Name, procName);
			return (BuildServerFaultException(R360ServicesErrorCodes.ConfigurationError,
											 "A configuration error occurred at the server",
											 "A configuration error occurred at the server"));
		}

		protected ServerFaultException BuildCommunicationsChannelException(Exception ex, string procName, string Msg)
		{
			EventLog.logEvent("Unable to establish communications channel - " + Msg, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
			EventLog.logError(ex, this.GetType().Name, procName);
			return (BuildServerFaultException(R360ServicesErrorCodes.CommunicationError,
											 "A communications channel occurred at the server",
											 "A communications channel occurred at the server"));
		}

		private static ServerFaultException BuildServerFaultException(R360ServicesErrorCodes ErrorCode, string Msg, string Details)
		{
			ServerFaultException objError;

			objError = new ServerFaultException();
			objError.errorcode = "Error";
			objError.message = ((int)ErrorCode).ToString() + " - " + Msg;
			objError.details = Details;

			return (objError);
		}
		#endregion

    //**************************************
    // Common Data Contracts
    //**************************************
    [DataContract]
    public class ServerFaultException
    {
      [DataMember]
      public string errorcode;

      [DataMember]
      public string message;

      [DataMember]
      public string details;
    }
	}
}