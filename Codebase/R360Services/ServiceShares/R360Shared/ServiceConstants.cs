﻿/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 5/22/2013
*
* Purpose: Common Exception  Class
*
* Modification History
* WI 101774 WJS  5/22/2013
*   - Initial Creation
*
*********************************************************************************/
namespace WFS.RecHub.R360Shared
{
  public static class ServiceConstants
  {
    internal static class ServiceNamespaces
    {
      public const string Root = "urn:wausaufs.com:services:RecHubServices";
    }

    public static class DateTimeFormats
    {
      public const string DefaultDateFormat = "M/d/yyyy";
      public const string InvariantDateFormat = "yyyyMMdd";
      public const string DateTimeFormat = "u";
    }

  }

}
