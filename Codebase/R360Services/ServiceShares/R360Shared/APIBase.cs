﻿using System;
using System.Runtime.CompilerServices;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Charlie Johnson
* Date: 10/17/2013
*
* Purpose: Provide the base functionality for all R360 Services
*
* Modification History
* WI 107711 CEJ 10/17/2013
*   Create the ability log userActivity in the sessionLog from the R360 Services
* WI 118355 CEJ 10/22/2013
*   Alter _R360APIBase to use the RecHubData connection to log activity
******************************************************************************/

namespace WFS.RecHub.R360Shared
{
    public abstract class APIBase
    {
        public delegate void dlgLogEvent(string message, string src, MessageType messageType, MessageImportance messageImportance);

        protected readonly IServiceContext _ServiceContext;
        private readonly ISessionDAL _sessionDal;

        public event dlgLogEvent LogEvent;

        protected APIBase(IServiceContext serviceContext)
        {
            _ServiceContext = serviceContext;
        }

        protected APIBase(IServiceContext serviceContext, ISessionDAL dal)
            : this(serviceContext)
        {
            _sessionDal = dal;
        }

        protected void OnErrorEvent(Exception ex, string src, string procName)
        {
            var sMessage = procName != "" ? $"Error occurred in {procName}.  {ex}" : "{ex}.";
            OnLogEvent($"{sMessage}  {ex.TargetSite}",
                src, MessageType.Error, MessageImportance.Essential);
        }

        protected void OnLogEvent(string message, string src, MessageType messageType, MessageImportance messageImportance)
        {
            LogEvent?.Invoke(message, src, messageType, messageImportance);
        }

        #region Session Validation

        protected abstract bool ValidateSID(out int userID);

        protected bool ValidateSIDAndLog(string procName, out int userID)
        {
            bool bAns = ValidateSID(out userID);

            if (bAns)
            {
                LogActivity(procName);
            }
            return bAns;
        }

        public bool LogThisActivity([CallerMemberName] string procName = null)
        {
            return LogActivity(procName);
        }

        public bool LogActivity(string procName)
        {
            var success = false;
            try
            {
                var dal = _sessionDal ?? new cSessionDAL(SiteKey, _DMPObjectRoot.ConnectionType.RecHubData);
                var sessionId = _ServiceContext.GetSessionID();
                int rowCount;
                if (dal.InsertSessionActivityLog(sessionId, procName, RequestType, out rowCount))
                {
                    if (rowCount == 0)
                        OnErrorEvent(new Exception("Session Activity Log was not updated."), "", procName);
                    else
                        success = true;
                }
                if (dal.UpdateSession(0, sessionId, out rowCount))
                    success &= rowCount > 0;
            }
            catch (Exception ex)
            {
                success = false;
                OnErrorEvent(ex, "", procName);
            }
            return success;
        }

        #endregion

        protected string SiteKey => "IPOnline";

        public Guid GetSession()
        {
            return _ServiceContext.GetSessionID();
        }

        protected abstract ActivityCodes RequestType
        {
            get;
        }
    }
}
