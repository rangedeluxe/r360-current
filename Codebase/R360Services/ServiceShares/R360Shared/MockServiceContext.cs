﻿using System;

namespace WFS.RecHub.R360Shared
{
    public class MockServiceContext : IServiceContext
    {
        public Guid GetSessionID()
        {
            return Guid.Parse(ConfigHelpers.GetAppSetting("ImpersonateSessionID", Guid.Empty.ToString()));
        }

        public Guid GetSID()
        {
            return Guid.Parse(ConfigHelpers.GetAppSetting("ImpersonateSID", Guid.Empty.ToString()));
        }

        public string SiteKey
        {
            get
            {
                return (string)ConfigHelpers.GetAppSetting("siteKey", string.Empty);
            }
            
        }

        public string GetLoginNameForAuditing()
        {
            return "MockUser";
        }

        public SimpleClaim[] RAAMClaims => new SimpleClaim[] { new SimpleClaim { Type = "xyz", Value = "123" },
            new SimpleClaim{Type="http://schemas.wfs.com/2014/01/identity/claims/userid", Value="1"} };
    }
}
