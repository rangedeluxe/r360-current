using System;
using System.Collections.Generic;

namespace WFS.RecHub.R360Shared {
    public class NullableKeyCache<TKey, TValue> {
        private Dictionary<TKey, TValue> _store = new Dictionary<TKey, TValue>();
        private Func<TKey, TValue> _getValue;
        private object _valueWhenKeyIsNull = null;

        public NullableKeyCache(Func<TKey, TValue> getValue, TValue valueWhenKeyIsNull):this(getValue) {
            _valueWhenKeyIsNull = valueWhenKeyIsNull;
        }

        public NullableKeyCache(Func<TKey, TValue> getValue) {
            _valueWhenKeyIsNull = null;
            _getValue = getValue;
        }

        public TValue GetValue(TKey key) {
            return GetValue(key, _valueWhenKeyIsNull);
        }

        public TValue GetValue(TKey key, object valueWhenKeyIsNull) {
            TValue result = (TValue) valueWhenKeyIsNull;

            if (key != null) {
                if (!_store.ContainsKey(key)) {
                    _store.Add(key, _getValue(key));
                }
                result = _store[key];
            }
            return result;
        }
    }
}