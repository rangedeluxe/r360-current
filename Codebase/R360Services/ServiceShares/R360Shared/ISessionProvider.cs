﻿using System;

namespace WFS.RecHub.R360Shared
{
    public interface IServiceContext
    {
        Guid GetSessionID();
        Guid GetSID();
        string GetLoginNameForAuditing();
        string SiteKey { get; }
        SimpleClaim[] RAAMClaims { get; }
    }
}
