﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Wfs.Raam.Core;
using Wfs.Raam.Core.Authorization;
using Wfs.Raam.Core.Services;

namespace WFS.RecHub.R360Shared
{
    public class R360Permissions
    {
        public const string R360ApplicationName = "Receivables360Online";

        #region Action Types

        // Action Type - must be one of the following
        public enum ActionType
        {
            View,
            Add,
            Update,
            Delete,
            Manage,
        }

        #endregion

        #region Asset names - define constants here

        // Receivables Exceptions
        public const string Perm_ExceptionsSummary = "ExceptionSummary";
        public const string Perm_ExceptionsDetail = "ExceptionDetail";
        public const string Perm_ExceptionsBreakLock = "ExceptionBreak";
        public const string Perm_ExceptionsAccept = "ExceptionAccept";
        public const string Perm_ExceptionsReject = "ExceptionsReject";
        public const string Perm_ExceptionsOverride = "ExceptionAcceptOverride";
        public const string Perm_ExceptionsUnlockOverride = "ExceptionsUnlockOverride";
        public const string Perm_PreDepositExceptions = "PreDepositExceptions";

        // Dashboard
        public const string Perm_Dashboard = "Dashboard";
        public const string Perm_ReceivablesSummary = "ReceivablesSummary";
        public const string Perm_DashboardPaymentChart = "PaymentTypePieChart";
        public const string Perm_DashboardExceptionsChart = "ExceptionsPieChart";
        public const string Perm_DashboardExceptionPostProcessChart = "ExceptionsPostProcessPieChart";
        public const string Perm_DashboardExceptionInProcessChart = "ExceptionsInProcessPieChart";
        public const string Perm_DashboardReport = "DashboardReport";

        // DDA Summary
        public const string Perm_DDASummary = "DDASummary";

        // Reports
        public const string Perm_ReportConfigurationAudit = "ConfigurationAuditReport";
        public const string Perm_ReportExtractAudit = "ExtractAuditReport";
        public const string Perm_ReportDITAuditAcknowlegement = "DITAuditAcknowledgementReport";
        public const string Perm_ReportImageRPSAudit = "ImageRPSAuditReport";
        public const string Perm_ReportUserActivity = "UserActivityReport";
        public const string Perm_ReportExceptionsAuditReport = "ExceptionsAudit";
        public const string Perm_ReportImportReconciliation = "ImportReconciliationReport";
        public const string Perm_ReportSecurityGroupProfile = "SecurityGroupProfileReport";
        public const string Perm_ReportSecurityUserActivity = "SecurityUserActivityReport";

        // Workgroups
        public const string Perm_WorkgroupMaintenance = "WorkgroupMaintenance";
        public const string Perm_WorkgroupAssociation = "WorkgroupEntity";
        public const string Perm_WorkgroupVirtual = "Workgroup";

        // Alerts
        public const string Perm_Alerts = "Alerts";
        public const string Perm_AlertDownload = "AlertDownload";   //Function - Notifications - Download files - View
        public const string Perm_AlertSystemException = "AlertFatal";  //Requirement 146481
        public const string Perm_AlertHighDollar = "AlertHighDollar";
        public const string Perm_AlertPaymentFromPayer = "AlertPaymentFrom";
        public const string Perm_AlertExtractComplete = "AlertExtractComplete";
        public const string Perm_AlertDecisionPending = "AlertDecisionPending";
        public const string Perm_AlertFileRecieved = "AlertFileReceived";
        public const string Perm_Notifications = "Notifications";

        // Payer Maintenance
        public const string Perm_Payer = "Payer";
        public const string Perm_AssignPayer = "AssignPayer";

        // Payment Source Maintenance
        public const string Perm_PaymentSource = "PaymentSource";

        public const string Perm_DocumentTypes = "DocumentType";

        public const string Perm_NotificationTypes = "NotificationsTypes";

        // Post Deposit Exceptions
        public const string Perm_PostDepositExceptions = "PostDepositExceptions";
        public const string Perm_PostDepositUnlockOverride = "PostDepositUnlockOverride";

        // Other
        //public const string Perm_Items = "Items";
        public const string Perm_AdvanceSearchDownload = "SearchDownload";
        public const string Perm_AdvanceSearchQueries = "Queries";
        public const string Perm_AdvanceSearch = "AdvancedSearch";
        public const string Perm_PaymentSearch = "PaymentSearch";
        public const string Perm_UserPreferences = "UserPreferences";
        public const string Perm_BatchSummary = "BatchSummary";
        public const string Perm_SystemSettings = "SystemSettings";
        public const string Perm_Bank = "Bank";
        public const string Perm_ViewAhead = "ViewBatchDataAheadOfDeposit";
        public const string Perm_DepositSearchSpan = "SearchSpan";
        public const string Perm_ExtractDesignManager = "ExtractDesignManager";

        // Maintenance
        public const string Perm_ExtractConfigurationManager = "ExtractPortlet";
        public const string Perm_UploadExceptions = "UploadExceptions";

        //Payer
        public const string Perm_PayerMaintenance = "PayerMaintenance";


        #endregion

        #region Public Interface

        // Expose the Permissions Helper through a static "singleton"
        public static R360Permissions Current
        {
            get
            {
                // Determine Session ID
                var sessionIdClaim = ClaimsPrincipal.Current.FindFirst(WfsClaimTypes.SessionId);
                Guid sessionId = sessionIdClaim == null ? Guid.Empty : new Guid(sessionIdClaim.Value);

                // Check for cached thread-local value
                if (_permissionsHelper == null || sessionId != _sessionID)
                {
                    // Set thread-local variable and return it
                    _sessionID = sessionId;
                    _permissionsHelper = new R360Permissions();
                }

                // Initiate context switch (if necessary...)
                CurrentPrincipal principal = WSFederatedAuthentication.CurrentPrincipal;
                var currentAppClaim = principal.Identity.Claims.FirstOrDefault(o => o.Type == WfsClaimTypes.ApplicationName);
                LogManager.Logger.LogEvent("R360Permissions.Current called");
                if (currentAppClaim == null || currentAppClaim.Value != R360ApplicationName)
                {
                    int claimCountBefore = WSFederatedAuthentication.CurrentPrincipal.Identity.Claims.Count();
                    LogManager.Logger.LogEvent("Before call to principal.ApplicationContextSwitch");
                    LogManager.Logger.LogEvent(string.Format("Session: {0} - Identity Claims Count= {1}",
                        sessionId, WSFederatedAuthentication.CurrentPrincipal.Identity.Claims.Count()));
                    LogManager.Logger.LogEvent(string.Format("Session: {0} - PrimaryIdentity Claims Count= {1}",
                        sessionId, WSFederatedAuthentication.CurrentPrincipal.PrimaryIdentity.Claims.Count()));
                    LogManager.Logger.LogEvent(string.Format("Session: {0} - Principal Identities Count= {1}",
                        sessionId, WSFederatedAuthentication.CurrentPrincipal.Principal.Identities.Count()));
                    LogManager.Logger.LogEvent(string.Format("Session: {0} - Application Name '{1}' ",
                        sessionId, R360ApplicationName));
                    
                    principal.ApplicationContextSwitch(R360ApplicationName);

                    LogManager.Logger.LogEvent("After call to principal.ApplicationContextSwitch");
                    LogManager.Logger.LogEvent(string.Format("Session: {0} - Identity Claims Count= {1}",
                        sessionId, WSFederatedAuthentication.CurrentPrincipal.Identity.Claims.Count()));
                    LogManager.Logger.LogEvent(string.Format("Session: {0} - PrimaryIdentity Claims Count= {1}",
                        sessionId, WSFederatedAuthentication.CurrentPrincipal.PrimaryIdentity.Claims.Count()));
                    LogManager.Logger.LogEvent(string.Format("Session: {0} - Principal Identities Count= {1}",
                        sessionId, WSFederatedAuthentication.CurrentPrincipal.Principal.Identities.Count()));
                    LogManager.Logger.LogEvent(string.Format("Session: {0} - Application Name '{1}' ",
                        sessionId, R360ApplicationName));

                    if (claimCountBefore >= WSFederatedAuthentication.CurrentPrincipal.Identity.Claims.Count())
                    {
                        LogManager.Logger.LogWarning(
                            string.Format(
                                "Session: {0} - Claim Counts Found: Count Before call = {1}, Count After call = {2}",
                                sessionId, claimCountBefore,
                                WSFederatedAuthentication.CurrentPrincipal.Identity.Claims.Count()));
                        LogManager.Logger.LogWarning("Current User Claims: \r\n" +
                                                     WSFederatedAuthentication.CurrentPrincipal.Identity.Claims.Select(
                                                             o => "  => " + o.Type + ": " + o.Value)
                                                         .Aggregate((o1, o2) => o1 + "\r\n" + o2));
                    }
                }
                return _permissionsHelper;
            }
        }

        public Guid SessionID
        {
            get
            {
                return _sessionID;
            }
        }

        public bool Allowed(string asset, ActionType action)
        {
            // Ask RAAM to evaluate the policy
            var result = _authManager.CheckPolicy(asset, ToRaamAction(action));

            // For testing / validation - log the result
            LogManager.Logger.LogEvent(string.Format("Session: {3} - Permission to '{0}' asset '{1}' = {2}", action, asset, result, SessionID));

            // And return it...
            return result;
        }

        public IList<string> GetAllowedPermissions(ActionType action, params string[] assets)
        {
            List<string> result = new List<string>();
            foreach (var asset in assets)
            {
                if (Allowed(asset, action))
                    result.Add(asset);
            }
            return result;
        }

        public static string ToRaamAction(ActionType action)
        {
            switch (action)
            {
                case ActionType.Add:
                case ActionType.Update:
                case ActionType.Delete:
                case ActionType.Manage:
                    return "Manage";

                default:
                case ActionType.View:
                    return action.ToString();
            }
        }

        #endregion

        #region Private Implementation
        
        // Thread-local caching
        [ThreadStatic]
        private static Guid _sessionID;
        [ThreadStatic]
        private static R360Permissions _permissionsHelper;

        private AuthorizationManager _authManager = new AuthorizationManager();

        private R360Permissions()
        {
        }

        #endregion
    }
}
