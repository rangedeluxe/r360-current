﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360Shared
{
    public interface IContextContainer
    {
        IServiceContext Current { get; }
    }
}
