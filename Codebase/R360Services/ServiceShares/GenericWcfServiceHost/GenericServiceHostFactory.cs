﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using WFS.RecHub.Common;

namespace GenericWcfServiceHost
{
	/// <summary>
	/// This class implements the boiler-plate overrides for a service host factory that uses a customized WfsServiceHost.
	/// Note: To use from a .svc file, you'd still need to create your own class deriving from this with a no-parameter constructor
	/// </summary>
	public class GenericServiceHostFactory : ServiceHostFactory
	{
		private readonly Type _serviceType;

		/// <summary>
		/// Instantiate the factory specifying the service type
		/// </summary>
		/// <param name="serviceType">This should be the type of a WCF Service</param>
		public GenericServiceHostFactory(Type serviceType)
			: base()
		{
			_serviceType = serviceType;
		}

		/// <summary>
		/// Create the Service Host (Service type was specified in the constructor)
		/// </summary>
		/// <param name="constructorString">The constructor string is ignored</param>
		/// <param name="baseAddresses">Use "new Uri[0]" by default</param>
		/// <returns></returns>
		public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
		{
			return CreateServiceHost(_serviceType, baseAddresses);
		}

		protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
		{
			ServiceHost host = new GenericServiceHost(serviceType, baseAddresses);
			return host;
		}
	}

	/// <summary>
	/// This class implements the boiler-plate overrides for a service host factory that uses a customized WfsServiceHost
	/// Note: To use from a .svc file, you'd still need to create your own class deriving from this without a templated type parameter
	/// </summary>
	public class GenericServiceHostFactory<MyService> : ServiceHostFactory
		where MyService : new()
	{
		/// <summary>
		/// Create the Service Host (Service type was specified in the constructor)
		/// </summary>
		/// <param name="constructorString">The constructor string is ignored</param>
		/// <param name="baseAddresses">Use "new Uri[0]" by default</param>
		/// <returns></returns>
		public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
		{
			return CreateServiceHost(typeof(MyService), baseAddresses);
		}

		protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
		{
			ServiceHost host = new GenericServiceHost(serviceType, baseAddresses);
			return host;
		}
	}

	/// <summary>
	/// This class acts as a custom host for a WFS WCF Service, making use of the IWfsService interface on the service if implemented.
	/// </summary>
	public class GenericServiceHost : ServiceHost
	{
		// TODO: Define a shared service interface that we can use for notifying a WFS WCF service of controller events...
		private interface IWfsService
		{
			// TODO: This is a stub...  It doesn't belong here...
		}

		private readonly Type _serviceType;
		private object _controllerInstance;
		private IWfsService ControllerInstance
		{
			get
			{
				if (_controllerInstance == null)
					_controllerInstance = Activator.CreateInstance(_serviceType);
				return _controllerInstance as IWfsService;
			}
		}

		public GenericServiceHost(Type serviceType)
			: this(serviceType, new Uri[0])
		{
		}

		public GenericServiceHost(Type serviceType, Uri[] baseAddresses)
			: base(serviceType, baseAddresses)
		{
			_serviceType = serviceType;
			Program.EventLog.logEvent("GenericServiceHost created with service type " + serviceType.FullName, Program.LogSource, MessageImportance.Verbose);
		}

		protected override void InitializeRuntime()
		{
			base.InitializeRuntime();
		}

		protected override void OnFaulted()
		{
			//TODO Handle fault event
			Program.EventLog.logEvent("GenericServiceHost OnFaulted for service type " + _serviceType.FullName, Program.LogSource, MessageImportance.Essential);
			base.OnFaulted();
		}

		protected override void OnOpening()
		{
			Program.EventLog.logEvent("GenericServiceHost OnOpening for service type " + _serviceType.FullName + " - initializing...\r\nEndpoint(s): " 
				+ string.Join("\r\n", this.Description.Endpoints.Select(o => "  " + o.ListenUri.ToString())), 
				Program.LogSource, MessageImportance.Verbose);
			var service = ControllerInstance;
			if (service != null)
			{
				// TODO: Implement generic interface methods...
				//service.Initialize();
			}
			base.OnOpening();
		}

		protected override void OnOpened()
		{
			base.OnOpened();
		}

		protected override void OnClose(TimeSpan timeout)
		{
			base.OnClose(timeout);
		}

		protected override void OnClosing()
		{
			base.OnClosing();
		}

		protected override void OnClosed()
		{
			Program.EventLog.logEvent("GenericServiceHost OnClosed for service type " + _serviceType.FullName + " - cleaning up...", Program.LogSource, MessageImportance.Verbose);
			var service = ControllerInstance;
			if (service != null)
			{
				// TODO: implement generic interface methods...
				//service.Cleanup();
			}
			base.OnClosed();
		}
	}
}
