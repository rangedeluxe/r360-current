﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;
using System.Diagnostics;
using System.ServiceModel;
using System.Configuration.Install;
using System.ComponentModel;
using System.ServiceModel.Activation;
using WFS.RecHub.Common;

namespace GenericWcfServiceHost
{
	/// <summary>
	/// This class functions as the boiler plate for a typical Windows Service that hosts one WCF service
	/// </summary>
	public class WindowsServiceBase : ServiceBase
	{
		internal class ServiceHostCreator
		{
			public Func<string, ServiceHostBase> Create;
			public bool IsRequired;
		}
		internal static List<ServiceHostCreator> _serviceHostCreators = new List<ServiceHostCreator>();
		internal static List<ServiceHostBase> _serviceHosts = new List<ServiceHostBase>();

		/// <summary>
		/// Use this method to host multiple services within the same service host
		/// </summary>
		public void AddService<MyFactory>()
			where MyFactory : ServiceHostFactory, new()
		{
			AddService<MyFactory>(true);
		}

		/// <summary>
		/// Use this method to host multiple services within the same service host
		/// </summary>
		public void AddService<MyFactory>(bool required)
			where MyFactory : ServiceHostFactory, new()
		{
			Func<string, ServiceHostBase> delayedCreationFunc = delegate(string args) { return new MyFactory().CreateServiceHost(args, new Uri[0]); };
			_serviceHostCreators.Add(new ServiceHostCreator() { Create = delayedCreationFunc, IsRequired = required });
		}

		/// <summary>
		/// Use this method host multiple services within the same service host, when the factory is created by the calling code (instead of specified by type)
		/// </summary>
		/// <param name="serviceFactory"></param>
		public void AddService(ServiceHostFactory serviceFactory)
		{
			Func<string, ServiceHostBase> delayedCreationFunc = delegate(string args) { return serviceFactory.CreateServiceHost(args, new Uri[0]); };
			_serviceHostCreators.Add(new ServiceHostCreator() { Create = delayedCreationFunc, IsRequired = true });
		}

		/// <summary>
		/// Constructor for the singleton service instance
		/// </summary>
		/// <param name="serviceName">The name of the Windows Service</param>
		public WindowsServiceBase(string serviceName)
		{
			// Set up parameters...
			if (serviceName != null)
			{
				this.ServiceName = serviceName;
				this.AutoLog = true;
			}
		}

		/// <summary>
		/// The OnStart method is called when the Windows Service is starting, and handles the WCF service startup 
		/// </summary>
		/// <param name="args"></param>
		protected override void OnStart(string[] args)
		{
			try
			{
				string argString = string.Join(" ", args);
				foreach (var serviceCreatorInfo in _serviceHostCreators)
				{
					try
					{
						ServiceHostBase service = serviceCreatorInfo.Create(argString);
						service.Open();
						_serviceHosts.Add(service);
					}
					catch (Exception ex)
					{
						if (serviceCreatorInfo.IsRequired)
							throw;
						else
							Program.EventLog.logEvent("Error starting optional service:\r\nException: " + ex.ToString(), Program.LogSource, MessageImportance.Verbose);
					}
				}
			}
			catch (Exception ex)
			{
				Program.EventLog.logEvent("Error during service startup:\r\nException: " + ex.ToString(), Program.LogSource, MessageImportance.Essential);
				throw;
			}
		}

		/// <summary>
		/// The OnStop method is called when the Windows Service is stopping, and handles the WCF service shutdown
		/// </summary>
		protected override void OnStop()
		{
			try
			{
				foreach (ServiceHostBase service in _serviceHosts)
				{
					if (service.State == CommunicationState.Opened)
						service.Close();
			    }
			}
			catch (Exception ex)
			{
				Program.EventLog.logEvent("Error during service stop:\r\nException: " + ex.ToString(), Program.LogSource, MessageImportance.Essential);
			}
		}

		#region Helpers

		#endregion

		/// <summary>
		/// Use the RunConsoleOrService method to automatically detect the Windows Service installation state, and optionally start it in Console mode instead of as a service
		/// </summary>
		/// <param name="args">arguments (e.g. command-line arguments) that could affect how the service starts.  e.g. "/Console"</param>
		public void RunConsoleOrService(string[] args)
		{
			// Check for Service status
			bool bServiceExists = false;
			bool bServiceRunning = false;
			ServiceController comp = null;
			Exception exThrown = null;
			if (!string.IsNullOrWhiteSpace(this.ServiceName))
			{
				try
				{
					// First, find out if this service is already running...
					comp = new ServiceController(this.ServiceName);
					if (comp.Status != ServiceControllerStatus.Stopped)
						bServiceRunning = true;
					bServiceExists = true;
				}
				catch (Exception ex)
				{
					exThrown = ex;
				}
			}

			if ((args.Length == 0 && !bServiceExists) ||
				(args.Length > 0 && (string.Compare(args[0], "/Console", true) == 0)))
			{
				// Run in Console mode...
				if (bServiceRunning)
				{
					/* Optional - we could stop the service...
					Console.WriteLine("Stopping Service...");
					comp.Stop();
					comp.WaitForStatus(ServiceControllerStatus.Stopped);
					Console.WriteLine("Service Stopped");
					*/

					Console.WriteLine("Service is currently running.  Please stop the service before running in Console mode.");
					Console.WriteLine("Press ENTER to continue...");
					Console.ReadLine();
				}
				else
				{
					// Run in Console mode...
					OnStart(args);
					Console.WriteLine("Press ENTER to terminate Console Service...");
					Console.ReadLine();
					OnStop();
				}

				/* Optional: Restart service, but launch a separate process to do it in case not all ports have been released (e.g. remoting)
				if (bServiceRunning)
				{
					Process me = Process.GetCurrentProcess();
					Process.Start(me.MainModule.FileName, "/Service");
				}
				*/
			}
			else if (bServiceExists && args.Length > 0 && string.Compare(args[0], "/Service", true) == 0)
			{
				// Start the service, if necessary
				if (!bServiceRunning)
				{
					Console.WriteLine("Starting Service...");
					comp.Start();
					comp.WaitForStatus(ServiceControllerStatus.Running);
				}
				if (comp.Status == ServiceControllerStatus.Running)
					Console.WriteLine("Service Running");
			}
			else if (!bServiceExists || args.Length > 0)
			{
				// Incompatible parameters...
				Console.WriteLine("Service does not exist on this system.");
				Console.WriteLine("To install the service, use the InstallUtil.exe utility program.");
				Console.WriteLine("To run in Console mode, use a /Console parameter on the command line.");
				Console.WriteLine();
				Console.WriteLine();
				if (exThrown != null)
				{
					Console.WriteLine("Service Detection Error:");
					Console.WriteLine(exThrown.ToString());
					Console.WriteLine();
					Console.WriteLine();
				}
				Console.WriteLine("Press Enter to Quit...");
				Console.ReadLine();
			}
			else
			{
				// Start the Windows Service
				ServiceBase.Run(this);
			}
		}
	}
}
