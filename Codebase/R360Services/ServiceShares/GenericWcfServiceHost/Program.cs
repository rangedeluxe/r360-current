﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;

namespace GenericWcfServiceHost
{
	class Program
	{
		internal const string LogSource = "GnrcSvcHost";
		internal static readonly cEventLog EventLog;

		static Program()
		{
			// Start the Trace Log immediately
			cSiteOptions siteOptions = new cSiteOptions("ipoServices");
			EventLog = new cEventLog(siteOptions.logFilePath, siteOptions.logFileMaxSize, (MessageImportance)siteOptions.loggingDepth);
		}

		static void Main(string[] args)
		{
			// Load the service(s) configuration
			List<ServiceActivationInfo> serviceDefs = AutoDetectServices();
			if (serviceDefs.Count == 0)
			{
				EventLog.logWarning("No service types detected -- please check the configuration", LogSource, MessageImportance.Essential);
				throw new Exception("No service types detected -- please check the configuration");
			}

			string serviceName = ConfigurationManager.AppSettings["WindowsServiceName"];
			if (string.IsNullOrEmpty(serviceName) || serviceName.Contains('*'))
			{
				EventLog.logWarning("Service Name not specified -- running as console", LogSource, MessageImportance.Verbose);
				serviceName = null;
			}

			// Setup the Windows Service base class to handle the Service controller events
			WindowsServiceBase service = new WindowsServiceBase(serviceName);
			foreach (var info in serviceDefs)
			{
				// Right now we are only using the service type from the service definition -- warn if factory is defined.
				if (!string.IsNullOrEmpty(info.FactoryType))
				{
					EventLog.logWarning("Custom Factory types are not currently supported - service will be launched using the generic factory", LogSource, MessageImportance.Verbose);
				}

				// In order for endpoints to configure correctly, the "relative path" must actually be included in the service's host BaseAddress
				// We can't easilty modify or verify that here, so we'll just ignore the relative path that was specified...
				// If we wanted to verify it, we could pass it to the ServiceHost, and it could check the BaseAddress (loaded from the config) to see if it ends in the RelativeAddress, as part of its "OnOpening" event...

				service.AddService(new GenericServiceHostFactory(info.ServiceType));
			}

			// Then start
			service.RunConsoleOrService(args);
		}

		private class ServiceActivationInfo
		{
			public Type ServiceType { get; set; }
			public string RelativePath { get; set; }
			public string FactoryType { get; set; }
		}

		private static List<ServiceActivationInfo> AutoDetectServices()
		{
			var localAssemblies = LoadLocalDLLs();

			List<ServiceActivationInfo> serviceTypes = new List<ServiceActivationInfo>();

			// Load Configuration section
			ServiceModelSectionGroup section = ServiceModelSectionGroup.GetSectionGroup(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None));
			if (section != null)
			{
				var hostingSection = section.ServiceHostingEnvironment;
				if (hostingSection != null)
				{
					foreach (ServiceActivationElement serviceEntry in hostingSection.ServiceActivations)
					{
						try
						{
							ServiceActivationInfo info = new ServiceActivationInfo();

							// Check factory - we'll delay verification of the type until later...
							if (!string.IsNullOrWhiteSpace(serviceEntry.Factory))
								info.FactoryType = serviceEntry.Factory;

							// Try to locate which assembly contains the desired type; there are currently no naming standards to use as hints, so we have to check them all...
							Type serviceType = Type.GetType(serviceEntry.Service); // Check current EXE; useful if service was ILMerged in...
							if (serviceType == null)
							{
								foreach (var assembly in localAssemblies)
								{
									serviceType = assembly.GetType(serviceEntry.Service);
									if (serviceType != null)
										break;
								}
							}
							if (serviceType == null)
								throw new Exception("Type could not be loaded: " + serviceEntry.Service);
							info.ServiceType = serviceType;

							// Check relative path - this is required by IIS, so it won't be blank
							info.RelativePath = serviceEntry.RelativeAddress;

							serviceTypes.Add(info);
						}
						catch (Exception ex)
						{
							EventLog.logWarning(string.Format("Error loading service type: {0}\r\nException: {1}", serviceEntry.Service, ex.ToString()), LogSource, MessageImportance.Essential);
						}
					}
				}
			}

			return serviceTypes;
		}

		internal static List<Assembly> LoadLocalDLLs()
		{
			List<Assembly> localDLLs = new List<Assembly>();
			try
			{
				string localPath = Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ApplicationBase);
				var dlls = Directory.GetFiles(localPath, "*.dll");
				foreach (string dllName in dlls)
				{
					try
					{
						localDLLs.Add(Assembly.Load(Path.GetFileNameWithoutExtension(dllName)));
					}
					catch (Exception exInner)
					{
						EventLog.logWarning(string.Format("Error loading DLL: {0}.\r\nException: {1}", dllName, exInner.ToString()), LogSource, MessageImportance.Debug);
					}
				}
			}
			catch (Exception ex)
			{
				EventLog.logWarning(string.Format("Error loading local DLLs.\r\nException: {0}", ex.ToString()), LogSource, MessageImportance.Essential);
			}
			return localDLLs;
		}
	}
}
