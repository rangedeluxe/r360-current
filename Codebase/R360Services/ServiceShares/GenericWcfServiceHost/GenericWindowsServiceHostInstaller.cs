﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace GenericWcfServiceHost
{
	/// <summary>
	/// This class provides the boiler plate for installing a Windows Service. 
	/// To run correctly, the config file must include a WfsServices section referencing your service
	/// </summary>
	[RunInstaller(true)]
	public class GenericWindowsServiceHostInstaller : Installer
	{
		private ServiceInstaller serviceInstaller;
		private ServiceProcessInstaller processInstaller;


		/// <summary>
		/// Call this constructor from your derived class's constuctor, to pass in the service name, etc., and it will take care of doing the installation
		/// </summary>
		/// <param name="serviceName">The (short) name of the windows service</param>
		/// <param name="displayName">The longer display name that displays in the Windows Services console</param>
		/// <param name="description">The service description text that can be viewed in the Windows Services console</param>
		public GenericWindowsServiceHostInstaller()
		{
			string serviceName, displayName, description;
			GetServiceNames(out serviceName, out displayName, out description);

			processInstaller = new ServiceProcessInstaller();
			serviceInstaller = new ServiceInstaller();

			processInstaller.Account = ServiceAccount.LocalSystem;
			serviceInstaller.StartType = ServiceStartMode.Automatic;
			serviceInstaller.ServiceName = serviceName;
			if (displayName != null)
				serviceInstaller.DisplayName = displayName;
			if (description != null)
				serviceInstaller.Description = description;

			Installers.Add(serviceInstaller);
			Installers.Add(processInstaller);
		}

		private void GetServiceNames(out string serviceName, out string displayName, out string description)
		{
			string configLocation = Assembly.GetExecutingAssembly().Location;
			var appSettings = ConfigurationManager.OpenExeConfiguration(configLocation).AppSettings;
			serviceName = appSettings.Settings["WindowsServiceName"].Value;
			if (string.IsNullOrWhiteSpace(serviceName) || serviceName.Contains('*'))
				throw new Exception("'WindowsServiceName' was not set in the config file (" + configLocation + ")");

			if (appSettings.Settings.AllKeys.Contains("WindowsServiceDisplayName"))
			{
				displayName = appSettings.Settings["WindowsServiceDisplayName"].Value;
				if (string.IsNullOrWhiteSpace(displayName))
					displayName = serviceName;
			}
			else
			{
				displayName = serviceName;
			}

			description = string.Empty;
			if (appSettings.Settings.AllKeys.Contains("WindowsServiceDescription"))
			{
				description = appSettings.Settings["WindowsServiceDescription"].Value;
			}
		}
	}
}
