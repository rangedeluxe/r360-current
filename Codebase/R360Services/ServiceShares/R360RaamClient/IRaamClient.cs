﻿using System;
using System.Collections.Generic;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;

namespace WFS.RecHub.R360RaamClient
{
	/// <summary>
	/// This is a subset of the public API of the RAAM client, used to simplify unit testing.
	/// No need to keep this in sync with the actual public API; only need to update if you change one of the method signatures or add another method that a unit test needs to Fake...
	/// </summary>
	public interface IRaamClient
	{
		EntityDto GetAncestorEntityHierarchy(int entityID);
		EntityDto GetAncestorFIEntity(int entityID);
		EntityDto GetAuthorizedEntitiesAndWorkgroups();
		IList<EntityDto> GetFIEntities();
		EntityDto GetLoggedInEntity();
		IList<int> GetUsersFIEntitiesIds();
		ResourceDto GetWorkgroupResource(int siteBankID, int workgroupID);
		EntityDto GetEntitiesWithPermission(int rootEntityId, string asset, R360Shared.R360Permissions.ActionType action);
        string GetEntityBreadcrumb(int entityID);
        bool GetEntityBreadcrumbList(int entityId, List<EntityBreadcrumbDTO> entityBreadcrumbs, bool includeHierarchy);
        IEnumerable<UserDto> GetUserHierarchy();
        EntityDto GetAuthorizedEntities();
        EntityDto GetEntityGroupHierarchy();
        IEnumerable<string> GetEventTypes();
        UserDto GetUserByUserSID(Guid userSID);
        IEnumerable<ResourceDto> GetWorkgroupsForEntity(int entityid);
        bool CreateOrUpdateWorkgroupResource(int entityID, int siteBankID, int workgroupID, string name);
        void RemoveWorkgroupResource(int entityID, int siteBankID, int workgroupID);
        EntityDto GetEntityHierarchy(int entityID);
        IEnumerable<EntityDto> GetAuthorizedEntityList();
        string GetEntityName(int entityID);
        string GetNameFromSid(Guid sid);
    }
}
