﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360RaamClient
{
    public class EntityUsersDTO
    {
        public string id { get; set; }
		public string label { get; set; }
		public string icon { get; set; }
		public long value { get; set; }
		public bool isNode { get; set; }
		public string entityTypeCode { get; set; }
		public IList<EntityUsersDTO> items { get; set; }

        public EntityUsersDTO()
		{
            items = new List<EntityUsersDTO>();
		}

       
    }
}
