﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Security.Claims;
using System.Web;
using Wfs.Logging;
using Wfs.Raam.Core;
using Wfs.Raam.Core.Helper.Enums;
using Wfs.Raam.Core.Services;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using Wfs.Raam.Service.Authorization.Contracts.ServiceContracts;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360RaamClient
{

	public class RaamClient : IRaamClient
	{
		private WFS.RecHub.R360Shared.ConfigHelpers.ILogger _logger = null;

		private static readonly CacheItemPolicy _cachePolicy = new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromMinutes(10) };


		public RaamClient(WFS.RecHub.R360Shared.ConfigHelpers.ILogger log)
		{
			if (LogManager.IsDefault) LogManager.Logger = log;
			_logger = log;
		}

		public static int LoggedInUserID
		{
			get
			{
				return int.Parse(ClaimsPrincipal.Current.Claims.First(o => o.Type == WfsClaimTypes.UserId).Value);
			}
		}

		public static int LoggedInEntityID
		{
			get
			{
				return int.Parse(ClaimsPrincipal.Current.Claims.First(o => o.Type == WfsClaimTypes.EntityId).Value);
			}
		}

		private IResourceContract _RAAMResourceService = null;
		private IResourceContract RAAMResourceService
		{
			get
			{
				if (_RAAMResourceService == null)
					_RAAMResourceService = R360ServiceFactory.Create<IResourceContract>();
				return _RAAMResourceService;
			}
		}

		private IApplicationContract _RAAMApplicationService = null;
		private IApplicationContract RAAMApplicationService
		{
			get
			{
				if (_RAAMApplicationService == null)
					_RAAMApplicationService = R360ServiceFactory.Create<IApplicationContract>();
				return _RAAMApplicationService;
			}
		}

		private IEntityContract _RAAMEntityService = null;
		private IEntityContract RAAMEntityService
		{
			get
			{
				if (_RAAMEntityService == null)
					_RAAMEntityService = R360ServiceFactory.Create<IEntityContract>();
				return _RAAMEntityService;
			}
		}

        private IUserContract _RAAMUserService = null;
        private IUserContract RAAMUserService
        {
            get
            {
                if (_RAAMUserService == null)
                    _RAAMUserService = R360ServiceFactory.Create<IUserContract>();
                return _RAAMUserService;
            }
        }

		private IRoleContract _RAAMRoleService = null;
		private IRoleContract RAAMRoleService
		{
			get
			{
				if (_RAAMRoleService == null)
					_RAAMRoleService = R360ServiceFactory.Create<IRoleContract>();
				return _RAAMRoleService;
			}
		}

		private IGroupContract _RAAMGroupService = null;
		private IGroupContract RAAMGroupService
		{
			get
			{
				if (_RAAMGroupService == null)
					_RAAMGroupService = R360ServiceFactory.Create<IGroupContract>();
				return _RAAMGroupService;
			}
		}

		private IAuthorizationPolicyContract _RAAMAuthorizationPolicyService = null;
		private IAuthorizationPolicyContract RAAMAuthorizationPolicyService
		{
			get
			{
				if (_RAAMAuthorizationPolicyService == null)
					_RAAMAuthorizationPolicyService = R360ServiceFactory.Create<IAuthorizationPolicyContract>();
				return _RAAMAuthorizationPolicyService;
			}
        }

        private IUserActivityContract _RAAMUserActivityService = null;
        private IUserActivityContract RAAMUserActivityService
        {
            get
            {
                if (_RAAMUserActivityService == null)
                    _RAAMUserActivityService = R360ServiceFactory.Create<IUserActivityContract>();
                return _RAAMUserActivityService;
            }
        }

		private ApplicationDto _recHubApplication = null;
		private int RecHubApplicationID
		{
			get
			{
				if (_recHubApplication == null)
				{
					_recHubApplication = RAAMApplicationService.GetApplicationByName("Receivables360Online");
					if (_recHubApplication == null)
						throw new Exception("R360 application is not configured in RAAM or retrieval by name failed.");
				}
				return _recHubApplication.ID;
			}
		}

        public EntityDto GetEntityHierarchy(int entityID)
        {
            return RAAMEntityService.GetEntityHierarchy(entityID);
        }

		public EntityDto GetAuthorizedEntitiesAndWorkgroups()
		{
			var claims = ClaimsPrincipal.Current.Claims;

			// Get IDs
			Guid userSID = Guid.Parse(claims.First(o => o.Type == WfsClaimTypes.Sid).Value);

			// Call the Raam Service
			var raamEntities = RAAMEntityService.GetEntitiesWithResources(ResourceType.Workgroup.ToString(), userSID, R360Shared.R360Permissions.ActionType.View.ToString());

			return raamEntities;
		}

        public EntityDto GetLoggedInEntity()
        {
            var claims = ClaimsPrincipal.Current.Claims;

            int entityID = int.Parse(claims.First(o => o.Type == WfsClaimTypes.EntityId).Value);

            var entity = RAAMEntityService.GetEntity(entityID);

            return entity;
        }

	    public string GetEntityName(int entityID)
	    {
	        var entity = RAAMEntityService.GetAncestorEntityHierarchy(entityID);
            //This is done to search for Corps. GetAncestor always returns the root entity when searching a corp
	        var result = Find(entity, entityID);
	        return result != null ? result.Name : string.Empty;
	    }

		public EntityDto GetAncestorEntityHierarchy(int entityID)
		{
			return RAAMEntityService.GetAncestorEntityHierarchy(entityID);
		}

		public EntityDto GetAncestorFIEntity(int entityID)
		{
			var raamEntities = GetAncestorEntityHierarchy(entityID);
			EntityDto fiEntity;

			FindFIForEntity(entityID, raamEntities, out fiEntity);

			return fiEntity;
		}

		private bool FindFIForEntity(int entityID, EntityDto node, out EntityDto fiEntity)
		{
			bool found = false;
			fiEntity = null;

			if (node.ID == entityID)
				found = true;
			else if (node.ChildEntities != null)
			{
				foreach (var child in node.ChildEntities)
				{
					if (FindFIForEntity(entityID, child, out fiEntity))
					{
						found = true;
						break;
					}
				}
			}

			if (found && fiEntity == null)//if we are tracking out of the recursion and we did not find a lower FI, check this entity
			{
				if (node.EntityTypeCode == "FI")
					fiEntity = node;
			}
			return found;
		}

		public IList<int> GetUsersFIEntitiesIds()
		{
			return GetFIEntities().Select(o => o.ID).ToList();
		}

		public IList<EntityDto> GetFIEntities()
		{
			var claims = ClaimsPrincipal.Current.Claims;
			var sessionId = claims.FirstOrDefault(o => o.Type == WfsClaimTypes.SessionId);
			if (sessionId == null) throw new Exception("No SessionId Claim");
			string cacheKey = sessionId.Value + ";FIEntities";

			var cache = MemoryCache.Default;
			var entities = cache[cacheKey] as List<EntityDto>;
			if (entities == null)
			{

				int entityId = int.Parse(claims.First(o => o.Type == WfsClaimTypes.EntityId).Value);

				// Call the RAAM Service
				var raamEntities = RAAMEntityService.GetEntityHierarchy(entityId);
                var raamAncetorEntities = RAAMEntityService.GetAncestorEntityHierarchy(entityId);

				entities = new List<EntityDto>();
				GetFIEntityList(raamEntities, ref entities);
                GetFIEntityList(raamAncetorEntities, ref entities);

				// Cache this response so we don't have to call RAAM again...
				cache.Set(cacheKey, entities, _cachePolicy);
			}
			else
			{
				_logger.LogEvent("Entity List retrieved from local cache");
			}
			
			return entities;
		}

		private void GetFIEntityList(EntityDto node, ref List<EntityDto> fiEntities)
		{
            bool alreadyExists = fiEntities.Any(p => p.ID == node.ID);
            if (node.EntityTypeCode == "FI" && !alreadyExists) // dont add duplicates
		    {
	                fiEntities.Add(node);
		    }

		    if (node.ChildEntities != null)
			{
				foreach (var child in node.ChildEntities)
				{
					GetFIEntityList(child, ref fiEntities);
				}
			}
		}

		public ResourceDto GetWorkgroupResource(int siteBankID, int workgroupID)
		{
			return RAAMResourceService.GetResource(RecHubApplicationID, ResourceType.Workgroup.ToString(), RaamHelper.FormatWorkgroupRefID(siteBankID, workgroupID));
		}

		public bool CreateOrUpdateWorkgroupResource(int entityID, int siteBankID, int workgroupID, string name)
		{
			var resource = GetWorkgroupResource(siteBankID, workgroupID);

			if (resource == null)
			{
				resource = new ResourceDto()
				{
					ApplicationID = RecHubApplicationID,
					EntityID = entityID,
					ExternalReferenceID = RaamHelper.FormatWorkgroupRefID(siteBankID, workgroupID),
					Name = RaamHelper.FormatWorkgroupResourceName(siteBankID, workgroupID, name),
					ResourceTypeCode = ResourceType.Workgroup.ToString()
				};

				resource = RAAMResourceService.CreateResource(resource);
			}
			else if (entityID != resource.EntityID)
			{
				throw new Exception("Workgroup is already assigned to a different Entity in RAAM.  Configuration must be manually corrected.");
			}
			else
			{
				resource.Name = RaamHelper.FormatWorkgroupResourceName(siteBankID, workgroupID, name);
				RAAMResourceService.UpdateResource(resource);
			}

			return resource.ID != default(int);
		}

		public void RemoveWorkgroupResource(int entityID, int siteBankID, int workgroupID)
		{
			_logger.LogEvent(string.Format("RemoveWorkgroupResource called for entity {0}, bank {1}, workgroup {2}", entityID, siteBankID, workgroupID));
			var resource = GetWorkgroupResource(siteBankID, workgroupID);
			if (resource != null)
			{
				if (entityID != resource.EntityID)
					throw new Exception("Unable to remove workgroup resource.  Resource has been assigned to a different Entity in RAAM.  Configuration must be manually corrected.");

				_logger.LogEvent(string.Format("Removing workgroup resource {0} from RAAM", resource.ID));
				RAAMResourceService.DeleteResource(resource.ID);
			}
			else
			{
				_logger.LogEvent(string.Format("No resource found for entity {0}, bank id {1}, workgroup id {2}, unable to remove", entityID, siteBankID, workgroupID));
			}
		}

		public string GetEntityBreadcrumb(int entityID)
		{
			var claims = ClaimsPrincipal.Current.Claims;
			var sessionId = claims.FirstOrDefault(o => o.Type == WfsClaimTypes.SessionId);
			if (sessionId == null) throw new Exception("No SessionId Claim");
			string cacheKey = sessionId.Value + ";EntityBreadcrumb-" + entityID.ToString();

			var cache = MemoryCache.Default;

			var breadcrumb = cache[cacheKey] as string;
			if (breadcrumb == null)
			{
				int loginEntityId = int.Parse(claims.First(o => o.Type == WfsClaimTypes.EntityId).Value);

				// Call the RAAM Service
				var raamEntities = RAAMEntityService.GetAncestorEntityHierarchy(entityID);
				breadcrumb = BuildEntityBreadcrumb(raamEntities, loginEntityId, false);
				
				// Cache this response so we don't have to call RAAM again...
				cache.Set(cacheKey, breadcrumb, _cachePolicy);
			}
			else
			{
				_logger.LogEvent("Entity Breadcrumb retrieved from local cache");
			}

			return breadcrumb;
		}

		private string BuildEntityBreadcrumb(EntityDto entity, int loggedInEntityId, bool isAtOrBelowLoggedInEntity)
		{
			if (entity == null)
				return string.Empty;

			var child = entity.ChildEntities.FirstOrDefault();

			string crumb = string.Empty;

            if (!isAtOrBelowLoggedInEntity && entity.ID == loggedInEntityId)
                isAtOrBelowLoggedInEntity = true;

            if (isAtOrBelowLoggedInEntity)
				crumb = entity.Name + (child != null ? "\\" : string.Empty);

            return crumb + BuildEntityBreadcrumb(child, loggedInEntityId, isAtOrBelowLoggedInEntity);
		}

        public bool GetEntityBreadcrumbList(int entityId,  List<EntityBreadcrumbDTO> entityBreadcrumbs, bool includeHierarchy)
        {

            if (entityBreadcrumbs == null)
            {
                entityBreadcrumbs = new List<EntityBreadcrumbDTO>();
            }

            var entity = GetEntityHierarchy(entityId);
            entityBreadcrumbs.Add(new EntityBreadcrumbDTO { EntityId = entity.ID, Breadcrumb = GetEntityBreadcrumb(entity.ID) });

            if (includeHierarchy)
            {
                foreach (var child in entity.ChildEntities)
                {
                    GetEntityBreadcrumbList(child.ID, entityBreadcrumbs, includeHierarchy);
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the user hierarchy.
        /// </summary>
        /// <param name="entityID">The entity ID.</param>
        /// <returns></returns>
        public IEnumerable<UserDto> GetUserHierarchy()
        {
            var claims = ClaimsPrincipal.Current.Claims;

            // Get IDs
            int userID = Int32.Parse(claims.First(o => o.Type == WfsClaimTypes.UserId).Value);
            return RAAMUserService.GetUsersByEntityHierarchy(userID);
        }

        /// <summary>
        /// Gets the root entity ID
        /// </summary>
        /// <returns></returns>
        public int GetRootEntityID()
        {
            return RAAMEntityService.GetRootEntityID();
        }

        /// <summary>
        /// Gets the Authorized entities for userID
        /// </summary>
        /// <returns>EntityDto</returns>
        public EntityDto GetAuthorizedEntities()
        {
            var claims = ClaimsPrincipal.Current.Claims;

            // Get IDs
            int entityID; 
            int.TryParse(claims.First(o => o.Type == WfsClaimTypes.EntityId).Value, out entityID);

            // Call the Raam Service
            var raamEntities = RAAMEntityService.GetEntityHierarchy(entityID);

            return raamEntities;
        }

        public EntityDto GetEntityGroupHierarchy()
        {
            var claims = ClaimsPrincipal.Current.Claims;
            int userID;
            int.TryParse(claims.First(o => o.Type == WfsClaimTypes.UserId).Value, out userID);
            return RAAMEntityService.GetAuthorizedEntitiesWithGroupResources(userID);            
        }

        public UserDto GetUserByUserSID(Guid userSID)
        {
            var users = RAAMUserService.GetUsersBySids(new List<Guid>{ userSID });
            
            return users != null ? users.FirstOrDefault() : null;
        }

		public EntityDto GetEntitiesWithPermission(int rootEntityId, string asset, R360Shared.R360Permissions.ActionType action)
		{
			// Call the Raam Service to get all entities
			var raamEntities = RAAMEntityService.GetEntityHierarchy(rootEntityId);

			// Recursively "trim" entities that don't match the permission
			if (CheckForPermission(raamEntities, asset, R360Shared.R360Permissions.ToRaamAction(action)))
				return raamEntities;

			return null;
		}

		private bool CheckForPermission(EntityDto entity, string asset, string action)
		{
			// Depth-first search - children first
			List<EntityDto> keepChildEntities = new List<EntityDto>();
			if (entity.ChildEntities != null)
			{
				foreach (var child in entity.ChildEntities)
				{
					if (CheckForPermission(child, asset, action))
						keepChildEntities.Add(child);
				}
			}
			entity.ChildEntities = keepChildEntities;

			// Short-circuit - if a child entity has permission, we don't trim this one
			if (keepChildEntities.Count > 0)
				return true;
       
			// No children - check whether this entity has a user with the designated permission

			// Find the policy ID for this entity
			var policies = RAAMAuthorizationPolicyService.GetAuthorizationPoliciesByEntityAndApplication(entity.ID, RecHubApplicationID);
			var policy = policies.SingleOrDefault(o => o.Resource.Name == asset && o.Permission.Name == action);
			if (policy == null)
			{
				// No matching authorization policy found - this entity does not have this permissions
				return false;
			}

			// Check for a role with the permission
			var roles = RAAMRoleService.GetRoles(entity.ID, RecHubApplicationID);
			foreach (var roleSummary in roles)
			{
				var role = RAAMRoleService.GetRole(roleSummary.ID);
				if (role.AuthorizationPolicies.Any(o => o.ID == policy.ID))
				{
					// Found role - check for a group with users for this role
					var groupList = RAAMGroupService.GetGroupSummaryByEntity(entity.ID);
					foreach (var group in groupList)
					{
						if (groupList.First(o => o.ID == group.ID).UserCount > 0)
						{
							// User found
							return true;
						}
					}
				}
			}

			// Role-in-use not found - this entity does not have this permission
			return false;
		}

        public IEnumerable<string> GetEventTypes()
        {
            return RAAMUserActivityService.GetEventNames();
        }

        public IEnumerable<EntityDto> GetAuthorizedEntityList()
        {
            var list = new List<EntityDto>();

            var root = GetAuthorizedEntities();
            if (root == null)
                return list;

            GetAuthorizedEntityListRecursion(list, root);

            return list;
        }

        private void GetAuthorizedEntityListRecursion(IList<EntityDto> currentList, EntityDto currentEntity)
        {
            if (currentEntity.ChildEntities != null)
                foreach (var e in currentEntity.ChildEntities)
                    GetAuthorizedEntityListRecursion(currentList, e);
            currentList.Add(currentEntity);
        }

        public IEnumerable<ResourceDto> GetWorkgroupsForEntity(int entityid)
        {
            var list = new List<ResourceDto>();
            var ent = GetAuthorizedEntitiesAndWorkgroups();

            // First, we need to find the root entity by the ID.  Kick out early if we don't have access to it.
            var root = Find(ent, entityid);
            if (root == null)
                return list;

            // Now, we list up all of the workgroups associated.
            GetWorkgroupsForEntityRecursion(list, root);

            return list;
        }

        private void GetWorkgroupsForEntityRecursion(IList<ResourceDto> currentList, EntityDto currentEntity)
        {
            // Depth first
            foreach (var e in currentEntity.ChildEntities)
                GetWorkgroupsForEntityRecursion(currentList, e);

            currentEntity.Resources
                .Where(x => x.ResourceTypeCode == "Workgroup")
                .ToList()
                .ForEach(r =>
                {
                    currentList.Add(r);
                });
        }

        private EntityDto Find(EntityDto root, int entid)
        {
            // If we found, kick out early.
            if (root.ID == entid)
                return root;

            // Traverse the tree.
            foreach (var c in root.ChildEntities)
            {
                var e = Find(c, entid);
                if (e != null)
                    return e;
            }
            return null;
        }

        public string GetNameFromSid(Guid sid)
        {
            var user = GetUserByUserSID(sid);
            return user?.Person == null
                ? string.Empty
                : $"{user.Person.FirstName} {user.Person.LastName}".Trim();
        }
    }
}
