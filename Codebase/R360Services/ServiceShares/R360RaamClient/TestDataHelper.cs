﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360RaamClient
{
    public class TestDataHelper
    {
        public static IList<EntityUsersDTO> Entities(bool entitiesOnly)
        {
            IList<EntityUsersDTO> rvalue = new List<EntityUsersDTO>();

            var root = new EntityUsersDTO { id = "100", label = "Corporate ABC", entityTypeCode = "Corp" };
            rvalue.Add(root);

            // fi
            for (int i = 1; i < 15; i++)
            {
                root.items.Add(new EntityUsersDTO { id = i.ToString(), isNode = true, label = "User-" + i, icon = "/Assets/wfs/shim.gif" });
            }

            return rvalue;
        }
    }
}
