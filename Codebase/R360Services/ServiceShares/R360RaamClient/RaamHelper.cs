﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;

namespace WFS.RecHub.R360RaamClient
{
	public class RaamHelper
	{
		public static string FormatWorkgroupRefID(int siteBankID, int workgroupID)
		{
			return siteBankID.ToString() + "|" + workgroupID.ToString();
		}

		public static string FormatWorkgroupResourceName(int siteBankID, int workgroupID, string name)
		{
			if (string.IsNullOrWhiteSpace(name))
				return workgroupID.ToString();
			return workgroupID.ToString() + " - " + name;
		}

		public static int ParseWorkgroupID(ResourceDto workgroup)
		{
			int workgroupID = 0;
			if (workgroup != null && workgroup.ExternalReferenceID != null)
			{
				var split = workgroup.ExternalReferenceID.Split(new char[] { '|' });
				if (split.Length == 2)
					int.TryParse(split[1], out workgroupID);
			}
			return workgroupID;
		}

		//public static int ParseBankID(ResourceDto workgroup)
		//{
		//	int bankID = 0;
		//	if (workgroup != null && workgroup.ExternalReferenceID != null)
		//	{
		//		var split = workgroup.ExternalReferenceID.Split(new char[] { '|' });
		//		if (split.Length == 2)
		//			int.TryParse(split[0], out bankID);
		//	}
		//	return bankID;
		//}

		//public static void ParseWorkgroupRefID(ResourceDto workgroup, out int bankID, out int workgroupID)
		//{
		//	workgroupID = 0;
		//	bankID = 0;
		//	if (workgroup != null && workgroup.ExternalReferenceID != null)
		//	{
		//		var split = workgroup.ExternalReferenceID.Split(new char[] { '|' });
		//		if (split.Length == 2)
		//		{
		//			int.TryParse(split[0], out bankID);
		//			int.TryParse(split[1], out workgroupID);
		//		}
		//	}
		//}
	}
}
