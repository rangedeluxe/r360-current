﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.R360RaamClient
{
    public class EntityBreadcrumbDTO
    {
        public int EntityId { get; set; }
        public string Breadcrumb { get; set; }

        public string ToXML()
        {
            return string.Format("<Entity EntityID=\"{0}\" BreadCrumb=\"{1}\" />", EntityId, SecurityElement.Escape(Breadcrumb));
        }
    }
}
