﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigFileModifier
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				// Arg1 = config file
				if (args.Length < 2)
				{
					Console.WriteLine("Error: ConfigFileModifier: Invalid Argument list: " + string.Join(" ", args));
					return;
				}

				string fileName = args[0];
				string configText = File.ReadAllText(fileName);
				bool bChanged = false;

				for (int i = 1; i < args.Length; i++)
				{
					string arg = args[i];
					if (arg.StartsWith("/replace:", StringComparison.OrdinalIgnoreCase))
					{
						// Replace text
						string[] pieces = arg.Split(new char[] {':'}, 2)[1].Split(new string[] { ";;;" }, 2, StringSplitOptions.None);
						Console.WriteLine("Info: Replacing <" + pieces[0] + "> with <" + pieces[1] + ">");
						configText = configText.Replace(pieces[0], pieces[1]);
						bChanged = true;
					}
					else
					{
						Console.WriteLine("Info: ConfigFileModifier: Unrecognized argument: " + arg);
					}
				}

				if (bChanged)
				{
					File.WriteAllText(fileName, configText);
					Console.WriteLine("Info: ConfigFileModifier: Updated " + fileName);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error: " + ex.ToString());
			}
		}
	}
}
