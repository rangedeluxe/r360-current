﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CommonExceptionTestClient
{
	public class MockDataParms
	{
		public string Server { get; set; }
		public string Database { get; set; }
		public string User { get; set; }
		[XmlIgnore]
		public string Password { get; set; }
	}

	public static class MockData
	{
		internal static void Synchronize(MockDataParms p)
		{
			// Build connection string
			SqlConnectionStringBuilder bldr = new SqlConnectionStringBuilder();
			bldr.ApplicationName = "CommonExceptionTestClient";
			bldr.DataSource = p.Server;
			bldr.InitialCatalog = p.Database;
			if (string.IsNullOrWhiteSpace(p.User))
				bldr.IntegratedSecurity = true;
			else
			{
				bldr.UserID = p.User;
				bldr.Password = p.Password;
			}

			using (SqlConnection conn = new SqlConnection(bldr.ToString()))
			{
				conn.Open();

				SqlCommand cmd = new SqlCommand() { Connection = conn };

				// Mock Workgroup
				cmd.CommandText = @"if not exists (select * from RecHubUser.OLOrganizations where OLOrganizationID = '{00000000-0042-0042-0042-000000000000}')
insert into RecHubUser.OLOrganizations (
       OLOrganizationID
      ,OrganizationCode
      ,Description
      ,IsActive
      ,IsParent
      ,CheckImageDisplayMode
      ,DocumentImageDisplayMode
      ,DisplayBatchID
      ,BrandingSchemeID)
select '{00000000-0042-0042-0042-000000000000}', 'MockCode', 'Mock Org', 1, 0, 0, 0, 0, 
	(select top 1 BrandingSchemeID from RecHubuser.BrandingSchemes)

if not exists (select * from RecHubUser.OLClientAccounts where OLClientAccountID = '{00000042-0000-0000-0000-000000000042}')
insert into RecHubUser.OLClientAccounts (
       OLClientAccountID
      ,OLOrganizationID
      ,SiteBankID
      ,SiteClientAccountID
      ,DisplayName
      ,IsActive
      ,CheckImageDisplayMode
      ,DocumentImageDisplayMode
      ,InvoiceBalancingOption
      ,HOA
      ,DisplayBatchID)
select '{00000042-0000-0000-0000-000000000042}', '{00000000-0042-0042-0042-000000000000}', 42, 42, 'Mock Workgroup', 1, 0, 0, 0, 0, 0

if not exists (select * from RecHubData.dimClientAccounts where SiteBankID = 42 and SiteClientAccountID = 42)
insert into RecHubData.dimClientAccounts (
	SiteCodeID, SiteBankID, SiteOrganizationID, SiteClientAccountID, MostRecent, IsActive, CutOff, OnlineColorMode, IsCommingled, DataRetentionDays, ImageRetentionDays, CreationDate, ModificationDate, SiteClientAccountKey, ShortName)
select top 1 -1, 42, 42, 42, 1, 1, CutOff, OnlineColorMode, IsCommingled, DataRetentionDays, ImageRetentionDays, GETDATE(), GETDATE(), SiteClientAccountKey, 'Mock Work' from RecHubData.dimClientAccounts where ClientAccountKey = -1";
				cmd.ExecuteNonQuery();

				// Mock User
				cmd.CommandText = @"if not exists (select * from RecHubUser.Users where RA3MSID = '{00000042-0042-0042-0042-000000000042}')
insert into RecHubuser.Users (
       RA3MSID
      ,UserType
      ,LogonName
      ,FirstName
      ,LastName
      ,SuperUser
      ,Password
      ,PasswordSet
      ,IsFirstTime
      ,FailedLogonAttempts
      ,FailedSecurityQuestionAttempts
      ,IsActive
      ,OLOrganizationID)
select	'{00000042-0042-0042-0042-000000000042}', 0, 'MockUser', 'MockUser', 'MockUser', 1, 'Undefined', '1900-01-01', 0, 0, 0, 0, '{00000000-0042-0042-0042-000000000000}'";
				cmd.ExecuteNonQuery();

				// Mock User permissions
				cmd.CommandText = @"declare @userId int = (select top 1 UserId from RecHubUser.Users where RA3MSID = '{00000042-0042-0042-0042-000000000042}')
declare @permissionId int = (select top 1 PermissionId from RecHubUser.Permissions where PermissionName = 'view exception summary' and PermissionMode = 'view')
if not exists (select * from RecHubuser.UserPermissions where UserID = @userId and PermissionID = @permissionId)
insert into RecHubUser.UserPermissions (UserID, PermissionID) values (@userId, @permissionId)

set @permissionId = (select top 1 PermissionId from RecHubUser.Permissions where PermissionName = 'view exception detail' and PermissionMode = 'view')
if not exists (select * from RecHubuser.UserPermissions where UserID = @userId and PermissionID = @permissionId)
insert into RecHubUser.UserPermissions (UserID, PermissionID) values (@userId, @permissionId)

set @permissionId = (select top 1 PermissionId from RecHubUser.Permissions where PermissionName = 'break exception lock' and PermissionMode = 'modify')
if not exists (select * from RecHubuser.UserPermissions where UserID = @userId and PermissionID = @permissionId)
insert into RecHubUser.UserPermissions (UserID, PermissionID) values (@userId, @permissionId)";
				cmd.ExecuteNonQuery();

				// Mock User Session
				cmd.CommandText = @"if not exists (select * from RecHubUser.Session where SessionID = '{42000000-4200-4200-4200-420000000000}')
insert into RecHubUser.Session (
       SessionID
      ,UserID
      ,IPAddress
      ,BrowserType
      ,LogonDateTime
      ,PageCounter
      ,IsSuccess
      ,IsSessionEnded
      ,LogonName
      ,IsRegistered)
select '{42000000-4200-4200-4200-420000000000}', 
	(select top 1 UserId from RecHubUser.Users where RA3MSID = '{00000042-0042-0042-0042-000000000042}'),
	'0.0.0.0',
	'Mock',
	GETDATE(),
	0,
	1,
	0,
	'Mock Session',
	0";
				cmd.ExecuteNonQuery();

				// Delete old data
				cmd.CommandText = @"delete RecHubException.DecisioningTransactions where IntegraPAYExceptionKey = (select IntegraPAYExceptionKey from RecHubException.IntegraPAYExceptions where GlobalBatchId = 42 and BatchId = 420 and CheckAmount = 42.42)
delete RecHubException.IntegraPAYExceptions where GlobalBatchId = 42 and BatchId = 420 and CheckAmount = 42.42";
				cmd.ExecuteNonQuery();

				cmd.CommandText = @"delete DT from RecHubException.DecisioningTransactions DT left join RecHubException.Transactions T on DT.TransactionKey = T.TransactionKey where DT.TransactionKey is not null and T.TransactionKey is null";
				cmd.ExecuteNonQuery();

				// Mock IntegraPAY Transaction
				cmd.CommandText = @"
  insert into RecHubException.IntegraPAYExceptions (
       DepositDateKey
      ,DecisioningStatus
      ,InProcessException
      ,BatchSourceKey
      ,Deadline
      ,BatchPaymentTypeKey
      ,CheckAmount
      ,GlobalBatchID
      ,SiteBankID
      ,SiteClientAccountID
      ,ImmutableDateKey
      ,BatchID
      ,TransactionID
      ,CreationDate
      ,ModificationDate)
select	cast(convert(varchar(8), GETDATE(), 112) as int),
		0, -- unresolved
		1,
		(select top 1 BatchSourceKey from RecHubData.dimBatchSources),
		dateadd(hour, 23, cast(CAST(getdate() as date) as datetime)),
		(select top 1 BatchPaymentTypeKey from RecHubdata.dimBatchPaymentTypes),
		42.42,
		42,
		42,
		42,
		cast(convert(varchar(8), GETDATE(), 112) as int),
		420,
		421,
		GETDATE(),
		GETDATE()";
				cmd.ExecuteNonQuery();

				// Mock Post Process Batches
				cmd.CommandText = @"-- Post Process Mock Exceptions
declare @todayKey int = cast(convert(varchar(8), getdate(), 112) as int)

-- Data Entry Setup for Mock Workgroup
if not exists (select * from RecHubException.DataEntrySetups where SiteBankID = 42 and SiteWorkGroupID = 42)
insert into RecHubException.DataEntrySetups (SiteBankID, SiteWorkGroupID)
select 42, 42

if not exists (select * from RecHubData.dimDataEntryColumns where TableType=3 and FldName='MockPaymentDate' and DataEntryColumnKey = 422)
begin
	set identity_insert RecHubData.dimDataEntryColumns ON
	insert into RecHubData.dimDataEntryColumns (DataEntryColumnKey, TableType, TableName, DataType, FldName, DisplayName, FldLength, ScreenOrder, MarkSense, CreationDate)
	select 422, 3, 'StubsDataEntry', 11, 'MockPaymentDate', 'Mock Payment Date', 10, 2, 0, getdate()
	set identity_insert RecHubData.dimDataEntryColumns OFF
end
if not exists (select * from RecHubData.dimDataEntryColumns where TableType=2 and FldName='Amount' and DataEntryColumnKey = 421)
begin
	set identity_insert RecHubData.dimDataEntryColumns ON
	insert into RecHubData.dimDataEntryColumns (DataEntryColumnKey, TableType, TableName, DataType, FldName, DisplayName, FldLength, ScreenOrder, MarkSense, CreationDate)
	select 421, 2, 'Stubs', 7, 'Amount', 'Amount', 20, 1, 0, getdate()
	set identity_insert RecHubData.dimDataEntryColumns OFF
end

declare @dataEntrySetupKey bigint = (select DataEntrySetupKey from RecHubException.DataEntrySetups where SiteBankID = 42 and SiteWorkGroupID = 42)
declare @batchSourceKey tinyint = (select top 1 BatchSourceKey from RecHubData.dimBatchSources)
declare @paymentTypeKey tinyint = (select top 1 BatchPaymentTypeKey from RecHubdata.dimBatchPaymentTypes)

if not exists (select * from RecHubException.DataEntrySetupFields where DataEntrySetupKey = @dataEntrySetupKey and FldName = 'MockPaymentDate')
insert into RecHubException.DataEntrySetupFields (FldName, TableType, DataEntrySetupKey)
select 'MockPaymentDate', 3, @dataEntrySetupKey union all
select 'Amount', 2, @dataEntrySetupKey

-- Remove any old data
delete DT
from RecHubException.DecisioningTransactions DT
inner join RecHubException.Transactions T on DT.TransactionKey = T.TransactionKey
inner join RecHubException.ExceptionBatches B on T.ExceptionBatchKey = B.ExceptionBatchKey
where B.DataEntrySetupKey = @dataEntrySetupKey and B.SiteBankID = 42 and B.SiteWorkGroupID = 42

delete SDE
from RecHubException.StubDataEntry SDE
inner join RecHubException.Stubs S on S.StubKey = SDE.StubKey
inner join RecHubException.Transactions T on T.TransactionKey = S.TransactionKey
inner join RecHubException.ExceptionBatches B on T.ExceptionBatchKey = B.ExceptionBatchKey
where B.DataEntrySetupKey = @dataEntrySetupKey and B.SiteBankID = 42 and B.SiteWorkGroupID = 42


delete S
from RecHubException.Stubs S
inner join RecHubException.Transactions T on T.TransactionKey = S.TransactionKey
inner join RecHubException.ExceptionBatches B on T.ExceptionBatchKey = B.ExceptionBatchKey
where B.DataEntrySetupKey = @dataEntrySetupKey and B.SiteBankID = 42 and B.SiteWorkGroupID = 42

delete P
from RecHubException.Payments P
inner join RecHubException.Transactions T on T.TransactionKey = P.TransactionKey
inner join RecHubException.ExceptionBatches B on T.ExceptionBatchKey = B.ExceptionBatchKey
where B.DataEntrySetupKey = @dataEntrySetupKey and B.SiteBankID = 42 and B.SiteWorkGroupID = 42

delete T
from RecHubException.Transactions T
inner join RecHubException.ExceptionBatches B on T.ExceptionBatchKey = B.ExceptionBatchKey
where B.DataEntrySetupKey = @dataEntrySetupKey and B.SiteBankID = 42 and B.SiteWorkGroupID = 42

delete RecHubException.ExceptionBatches where DataEntrySetupKey = @dataEntrySetupKey and SiteBankID = 42 and SiteWorkGroupID = 42

-- Create Mock Batch(es)
insert into RecHubException.ExceptionBatches (DataEntrySetupKey, SiteBankID, SiteWorkGroupID, BatchID,
	DepositDateKey, ImmutableDateKey, SourceProcessingDateKey, BatchSourceKey, BatchPaymentTypeKey, BatchStatusKey)
select @dataEntrySetupKey, 42, 42, 42, @todayKey, @todayKey, @todayKey, @batchSourceKey, @paymentTypeKey, 1 union all
select @dataEntrySetupKey, 42, 42, 420, @todayKey, @todayKey, @todayKey, @batchSourceKey, @paymentTypeKey, 1

declare @batchKey1 bigint = (select ExceptionBatchKey from RecHubException.ExceptionBatches where DataEntrySetupKey = @dataEntrySetupKey and SiteBankID = 42 and SiteWorkGroupID = 42 and BatchID = 42)
declare @batchKey2 bigint = (select ExceptionBatchKey from RecHubException.ExceptionBatches where DataEntrySetupKey = @dataEntrySetupKey and SiteBankID = 42 and SiteWorkGroupID = 42 and BatchID = 420)

-- Create Mock Transacation(s)
insert into RecHubException.Transactions (ExceptionBatchKey, TransactionID, TxnSequence)
select @batchKey1, 421, 1 union all
select @batchKey1, 423, 3 union all
select @batchKey2, 4205, 5

declare @tranKey1 bigint = (select TransactionKey from RecHubException.Transactions where ExceptionBatchKey = @batchKey1 and TransactionId = 421)
declare @tranKey2 bigint = (select TransactionKey from RecHubException.Transactions where ExceptionBatchKey = @batchKey1 and TransactionId = 423)
declare @tranKey3 bigint = (select TransactionKey from RecHubException.Transactions where ExceptionBatchKey = @batchKey2 and TransactionId = 4205)

-- Create Mock Stubs(s)
insert into RecHubException.Stubs (TransactionKey, TxnSequence, SequenceWithinTransaction, BatchSequence, StubSequence, Amount)
select @tranKey1, 1, 1, 2, 1, 50.01 union all
select @tranKey1, 1, 2, 3, 2, 50.02 union all
select @tranKey1, 1, 3, 4, 3, 50.03 union all
select @tranKey2, 3, 1, 8, 6, 120.07 union all
select @tranKey3, 5, 1, 101, 9, 0.00

-- Create Mock Payment(s)
insert into RecHubException.Payments (TransactionKey, TxnSequence, SequenceWithinTransaction, BatchSequence, PaymentSequence, Amount)
select @tranKey1, 1, 5, 5, 1, 50.01 union all
select @tranKey1, 1, 6, 6, 2, 100.05 union all
select @tranKey2, 3, 4, 9, 4, 120.07 union all
select @tranKey3, 5, 2, 102, 6, 0.00
";
				cmd.ExecuteNonQuery();

				// Generate the DecisioningTransaction rows
				cmd.CommandText = @"
	/* Generate Common Exception ID (= DecisioningTransactionKey) for IntegraPAYExceptions */
	INSERT INTO RecHubException.DecisioningTransactions (IntegraPAYExceptionKey, TransactionStatusKey, TransactionBeginWork, TransactionEndWork, TransactionReset, ModifiedBy)
	SELECT RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey, 1, 0, 0, 0, 'MockProvisioner'
	FROM RecHubException.IntegraPAYExceptions 
	LEFT JOIN RecHubException.DecisioningTransactions ON
		RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey = RecHubException.DecisioningTransactions.IntegraPAYExceptionKey
	WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey IS NULL

	/* Generate Common Exception ID (= DecisioningTransactionKey) for PostProcess (DIT) Exceptions */
	INSERT INTO RecHubException.DecisioningTransactions (TransactionKey, TransactionStatusKey, TransactionBeginWork, TransactionEndWork, TransactionReset, ModifiedBy)
	SELECT RecHubException.Transactions.TransactionKey, 1, 0, 0, 0, 'MockProvisioner'
	FROM RecHubException.Transactions 
	LEFT JOIN RecHubException.DecisioningTransactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey IS NULL";
				cmd.ExecuteNonQuery();
			}
		}

		internal static void SetTransactionStatus(MockDataParms p, long commonExceptionId, int status)
		{
			// Build connection string
			SqlConnectionStringBuilder bldr = new SqlConnectionStringBuilder();
			bldr.ApplicationName = "CommonExceptionTestClient";
			bldr.DataSource = p.Server;
			bldr.InitialCatalog = p.Database;
			if (string.IsNullOrWhiteSpace(p.User))
				bldr.IntegratedSecurity = true;
			else
			{
				bldr.UserID = p.User;
				bldr.Password = p.Password;
			}

			using (SqlConnection conn = new SqlConnection(bldr.ToString()))
			{
				conn.Open();

				SqlCommand cmd = new SqlCommand() { Connection = conn };
				cmd.CommandText = @"UPDATE RecHubException.DecisioningTransactions SET TransactionStatusKey = " + status.ToString() + @" WHERE DecisioningTransactionKey = " + commonExceptionId.ToString();
				cmd.ExecuteNonQuery();
			}
		}

		private static void VerifyCount(int expected, SqlCommand cmd)
		{
			object actual = cmd.ExecuteScalar();
			if (expected != (int)actual)
				throw new Exception(string.Format("Verify Failed - Expected: {0}, Actual: {1} - From: {2}", expected, actual, cmd.CommandText));
		}

		internal static void VerifyComplete(MockDataParms p, IEnumerable<int> commonExceptionIds)
		{
			// Build connection string
			SqlConnectionStringBuilder bldr = new SqlConnectionStringBuilder();
			bldr.ApplicationName = "CommonExceptionTestClient";
			bldr.DataSource = p.Server;
			bldr.InitialCatalog = p.Database;
			if (string.IsNullOrWhiteSpace(p.User))
				bldr.IntegratedSecurity = true;
			else
			{
				bldr.UserID = p.User;
				bldr.Password = p.Password;
			}

			using (SqlConnection conn = new SqlConnection(bldr.ToString()))
			{
				conn.Open();
				string idClause = " DecisioningTransactionKey in(" + commonExceptionIds.Select(o => o.ToString()).Aggregate((s1, s2) => s1 + "," + s2) + ")";

				SqlCommand cmd = new SqlCommand() { Connection = conn };
				cmd.CommandText = "select count(*) from RecHubException.DecisioningTransactions WHERE TransactionStatusKey >= 2 AND" + idClause;
				VerifyCount(4, cmd);

				cmd.CommandText = @"select count(*) from RecHubException.DecisioningTransactions DT INNER JOIN RecHubException.Transactions T ON DT.TransactionKey = T.TransactionKey
INNER JOIN RecHubException.ExceptionBatches B on T.ExceptionBatchKey = B.ExceptionBatchKey WHERE B.BatchStatusKey >= 2 AND" + idClause;
				VerifyCount(3, cmd);

				cmd.CommandText = @"select count(*) from RecHubException.DecisioningTransactions DT INNER JOIN RecHubException.IntegraPAYExceptions I ON DT.IntegraPAYExceptionKey = I.IntegraPAYExceptionKey
WHERE I.DecisioningStatus = 3 AND" + idClause;
				VerifyCount(1, cmd);
			}
		}
	}
}
