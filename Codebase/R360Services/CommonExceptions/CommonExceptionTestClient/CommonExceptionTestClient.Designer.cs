﻿namespace CommonExceptionTestClient
{
    partial class CommonExceptionTestClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this._btnRequestImage = new System.Windows.Forms.Button();
			this._btnRejectTransaction = new System.Windows.Forms.Button();
			this._btnAcceptTransaction = new System.Windows.Forms.Button();
			this._txtLog = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this._txtCommonExceptionID = new System.Windows.Forms.TextBox();
			this.UnlockTransaction = new System.Windows.Forms.Button();
			this.CheckOutTransaction = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.Ping = new System.Windows.Forms.Button();
			this.GetTransactionsGetTransactionPendingDecision = new System.Windows.Forms.Button();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.splitContainer3 = new System.Windows.Forms.SplitContainer();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.tvImageList = new System.Windows.Forms.TreeView();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.label8 = new System.Windows.Forms.Label();
			this.txtLockboxId = new System.Windows.Forms.TextBox();
			this.cmbFront = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.cmbMode = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtSeq = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.txtBatchId = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtBankId = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.txtDate = new System.Windows.Forms.TextBox();
			this.bntRefresh = new System.Windows.Forms.Button();
			this.txtFileDescriptor = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this._tabMockData = new System.Windows.Forms.TabPage();
			this.label13 = new System.Windows.Forms.Label();
			this._txtMockDbDatabase = new System.Windows.Forms.TextBox();
			this._btnMockVerifyData = new System.Windows.Forms.Button();
			this.label12 = new System.Windows.Forms.Label();
			this._txtMockDbPassword = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this._txtMockDbUser = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this._txtMockDbServer = new System.Windows.Forms.TextBox();
			this._btnHitDIT = new System.Windows.Forms.Button();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
			this.splitContainer3.Panel1.SuspendLayout();
			this.splitContainer3.Panel2.SuspendLayout();
			this.splitContainer3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.statusStrip1.SuspendLayout();
			this._tabMockData.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Controls.Add(this._tabMockData);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(927, 416);
			this.tabControl1.TabIndex = 9;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this._btnHitDIT);
			this.tabPage1.Controls.Add(this._btnRequestImage);
			this.tabPage1.Controls.Add(this._btnRejectTransaction);
			this.tabPage1.Controls.Add(this._btnAcceptTransaction);
			this.tabPage1.Controls.Add(this._txtLog);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this._txtCommonExceptionID);
			this.tabPage1.Controls.Add(this.UnlockTransaction);
			this.tabPage1.Controls.Add(this.CheckOutTransaction);
			this.tabPage1.Controls.Add(this.textBox1);
			this.tabPage1.Controls.Add(this.Ping);
			this.tabPage1.Controls.Add(this.GetTransactionsGetTransactionPendingDecision);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(919, 390);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Common Ex Tests";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// _btnRequestImage
			// 
			this._btnRequestImage.Location = new System.Drawing.Point(732, 37);
			this._btnRequestImage.Margin = new System.Windows.Forms.Padding(2);
			this._btnRequestImage.Name = "_btnRequestImage";
			this._btnRequestImage.Size = new System.Drawing.Size(136, 24);
			this._btnRequestImage.TabIndex = 20;
			this._btnRequestImage.Text = "Request Image";
			this._btnRequestImage.UseVisualStyleBackColor = true;
			this._btnRequestImage.Click += new System.EventHandler(this._btnRequestImage_Click);
			// 
			// _btnRejectTransaction
			// 
			this._btnRejectTransaction.Location = new System.Drawing.Point(592, 37);
			this._btnRejectTransaction.Margin = new System.Windows.Forms.Padding(2);
			this._btnRejectTransaction.Name = "_btnRejectTransaction";
			this._btnRejectTransaction.Size = new System.Drawing.Size(136, 24);
			this._btnRejectTransaction.TabIndex = 19;
			this._btnRejectTransaction.Text = "Reject Transaction";
			this._btnRejectTransaction.UseVisualStyleBackColor = true;
			this._btnRejectTransaction.Click += new System.EventHandler(this._btnRejectTransaction_Click);
			// 
			// _btnAcceptTransaction
			// 
			this._btnAcceptTransaction.Location = new System.Drawing.Point(452, 37);
			this._btnAcceptTransaction.Margin = new System.Windows.Forms.Padding(2);
			this._btnAcceptTransaction.Name = "_btnAcceptTransaction";
			this._btnAcceptTransaction.Size = new System.Drawing.Size(136, 24);
			this._btnAcceptTransaction.TabIndex = 18;
			this._btnAcceptTransaction.Text = "Accept Transaction";
			this._btnAcceptTransaction.UseVisualStyleBackColor = true;
			this._btnAcceptTransaction.Click += new System.EventHandler(this._btnAcceptTransaction_Click);
			// 
			// _txtLog
			// 
			this._txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this._txtLog.Location = new System.Drawing.Point(10, 234);
			this._txtLog.Margin = new System.Windows.Forms.Padding(2);
			this._txtLog.Multiline = true;
			this._txtLog.Name = "_txtLog";
			this._txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this._txtLog.Size = new System.Drawing.Size(902, 151);
			this._txtLog.TabIndex = 17;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 13);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(106, 13);
			this.label1.TabIndex = 16;
			this.label1.Text = "CommonExceptionID";
			// 
			// _txtCommonExceptionID
			// 
			this._txtCommonExceptionID.Location = new System.Drawing.Point(143, 10);
			this._txtCommonExceptionID.Margin = new System.Windows.Forms.Padding(2);
			this._txtCommonExceptionID.Name = "_txtCommonExceptionID";
			this._txtCommonExceptionID.Size = new System.Drawing.Size(76, 20);
			this._txtCommonExceptionID.TabIndex = 9;
			// 
			// UnlockTransaction
			// 
			this.UnlockTransaction.Location = new System.Drawing.Point(318, 37);
			this.UnlockTransaction.Margin = new System.Windows.Forms.Padding(2);
			this.UnlockTransaction.Name = "UnlockTransaction";
			this.UnlockTransaction.Size = new System.Drawing.Size(130, 24);
			this.UnlockTransaction.TabIndex = 14;
			this.UnlockTransaction.Text = "Unlock Transaction";
			this.UnlockTransaction.UseVisualStyleBackColor = true;
			this.UnlockTransaction.Click += new System.EventHandler(this.UnlockTransaction_Click);
			// 
			// CheckOutTransaction
			// 
			this.CheckOutTransaction.Location = new System.Drawing.Point(178, 37);
			this.CheckOutTransaction.Margin = new System.Windows.Forms.Padding(2);
			this.CheckOutTransaction.Name = "CheckOutTransaction";
			this.CheckOutTransaction.Size = new System.Drawing.Size(136, 24);
			this.CheckOutTransaction.TabIndex = 13;
			this.CheckOutTransaction.Text = "CheckOutTransaction";
			this.CheckOutTransaction.UseVisualStyleBackColor = true;
			this.CheckOutTransaction.Click += new System.EventHandler(this.CheckOutTransaction_Click);
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(10, 65);
			this.textBox1.Margin = new System.Windows.Forms.Padding(2);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(902, 160);
			this.textBox1.TabIndex = 15;
			// 
			// Ping
			// 
			this.Ping.Location = new System.Drawing.Point(10, 37);
			this.Ping.Margin = new System.Windows.Forms.Padding(2);
			this.Ping.Name = "Ping";
			this.Ping.Size = new System.Drawing.Size(56, 24);
			this.Ping.TabIndex = 11;
			this.Ping.Text = "Ping";
			this.Ping.UseVisualStyleBackColor = true;
			this.Ping.Click += new System.EventHandler(this.Ping_Click);
			// 
			// GetTransactionsGetTransactionPendingDecision
			// 
			this.GetTransactionsGetTransactionPendingDecision.Location = new System.Drawing.Point(70, 37);
			this.GetTransactionsGetTransactionPendingDecision.Margin = new System.Windows.Forms.Padding(2);
			this.GetTransactionsGetTransactionPendingDecision.Name = "GetTransactionsGetTransactionPendingDecision";
			this.GetTransactionsGetTransactionPendingDecision.Size = new System.Drawing.Size(104, 24);
			this.GetTransactionsGetTransactionPendingDecision.TabIndex = 12;
			this.GetTransactionsGetTransactionPendingDecision.Text = "GetTransactions Pending Decision";
			this.GetTransactionsGetTransactionPendingDecision.UseVisualStyleBackColor = true;
			this.GetTransactionsGetTransactionPendingDecision.Click += new System.EventHandler(this.GetTransactions_Click);
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.splitContainer3);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(919, 390);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Image Test";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// splitContainer3
			// 
			this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.splitContainer3.IsSplitterFixed = true;
			this.splitContainer3.Location = new System.Drawing.Point(3, 3);
			this.splitContainer3.Name = "splitContainer3";
			this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer3.Panel1
			// 
			this.splitContainer3.Panel1.Controls.Add(this.splitContainer2);
			// 
			// splitContainer3.Panel2
			// 
			this.splitContainer3.Panel2.Controls.Add(this.statusStrip1);
			this.splitContainer3.Panel2MinSize = 22;
			this.splitContainer3.Size = new System.Drawing.Size(913, 384);
			this.splitContainer3.SplitterDistance = 355;
			this.splitContainer3.TabIndex = 7;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.tvImageList);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.splitContainer1);
			this.splitContainer2.Size = new System.Drawing.Size(913, 355);
			this.splitContainer2.SplitterDistance = 167;
			this.splitContainer2.TabIndex = 6;
			// 
			// tvImageList
			// 
			this.tvImageList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tvImageList.HideSelection = false;
			this.tvImageList.Location = new System.Drawing.Point(0, 0);
			this.tvImageList.Name = "tvImageList";
			this.tvImageList.Size = new System.Drawing.Size(167, 355);
			this.tvImageList.TabIndex = 0;
			this.tvImageList.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.tvImageList_BeforeExpand);
			this.tvImageList.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvImageList_AfterSelect);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.label8);
			this.splitContainer1.Panel1.Controls.Add(this.txtLockboxId);
			this.splitContainer1.Panel1.Controls.Add(this.cmbFront);
			this.splitContainer1.Panel1.Controls.Add(this.label7);
			this.splitContainer1.Panel1.Controls.Add(this.cmbMode);
			this.splitContainer1.Panel1.Controls.Add(this.label6);
			this.splitContainer1.Panel1.Controls.Add(this.txtSeq);
			this.splitContainer1.Panel1.Controls.Add(this.label5);
			this.splitContainer1.Panel1.Controls.Add(this.txtBatchId);
			this.splitContainer1.Panel1.Controls.Add(this.label4);
			this.splitContainer1.Panel1.Controls.Add(this.txtBankId);
			this.splitContainer1.Panel1.Controls.Add(this.label3);
			this.splitContainer1.Panel1.Controls.Add(this.label9);
			this.splitContainer1.Panel1.Controls.Add(this.txtDate);
			this.splitContainer1.Panel1.Controls.Add(this.bntRefresh);
			this.splitContainer1.Panel1.Controls.Add(this.txtFileDescriptor);
			this.splitContainer1.Panel1.Controls.Add(this.label10);
			this.splitContainer1.Panel1MinSize = 66;
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
			this.splitContainer1.Size = new System.Drawing.Size(742, 355);
			this.splitContainer1.SplitterDistance = 66;
			this.splitContainer1.TabIndex = 2;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(318, 15);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(51, 13);
			this.label8.TabIndex = 16;
			this.label8.Text = "Lockbox:";
			// 
			// txtLockboxId
			// 
			this.txtLockboxId.Location = new System.Drawing.Point(375, 8);
			this.txtLockboxId.Name = "txtLockboxId";
			this.txtLockboxId.Size = new System.Drawing.Size(79, 20);
			this.txtLockboxId.TabIndex = 3;
			// 
			// cmbFront
			// 
			this.cmbFront.FormattingEnabled = true;
			this.cmbFront.ItemHeight = 13;
			this.cmbFront.Items.AddRange(new object[] {
            "Front",
            "Back"});
			this.cmbFront.Location = new System.Drawing.Point(653, 35);
			this.cmbFront.Name = "cmbFront";
			this.cmbFront.Size = new System.Drawing.Size(83, 21);
			this.cmbFront.TabIndex = 8;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(586, 41);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(64, 13);
			this.label7.TabIndex = 13;
			this.label7.Text = "Front/Back:";
			// 
			// cmbMode
			// 
			this.cmbMode.FormattingEnabled = true;
			this.cmbMode.ItemHeight = 13;
			this.cmbMode.Items.AddRange(new object[] {
            "Bitonal",
            "Color",
            "Gray"});
			this.cmbMode.Location = new System.Drawing.Point(487, 35);
			this.cmbMode.Name = "cmbMode";
			this.cmbMode.Size = new System.Drawing.Size(83, 21);
			this.cmbMode.TabIndex = 7;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(420, 41);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(61, 13);
			this.label6.TabIndex = 11;
			this.label6.Text = "ColorMode:";
			// 
			// txtSeq
			// 
			this.txtSeq.Location = new System.Drawing.Point(636, 12);
			this.txtSeq.Name = "txtSeq";
			this.txtSeq.Size = new System.Drawing.Size(100, 20);
			this.txtSeq.TabIndex = 5;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(586, 15);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(29, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "Seq:";
			// 
			// txtBatchId
			// 
			this.txtBatchId.Location = new System.Drawing.Point(510, 8);
			this.txtBatchId.Name = "txtBatchId";
			this.txtBatchId.Size = new System.Drawing.Size(70, 20);
			this.txtBatchId.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(460, 11);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(47, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "BatchId:";
			// 
			// txtBankId
			// 
			this.txtBankId.Location = new System.Drawing.Point(222, 8);
			this.txtBankId.Name = "txtBankId";
			this.txtBankId.Size = new System.Drawing.Size(90, 20);
			this.txtBankId.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(172, 11);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(44, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "BankId:";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(9, 11);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(51, 13);
			this.label9.TabIndex = 4;
			this.label9.Text = "DateKey:";
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(66, 4);
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(100, 20);
			this.txtDate.TabIndex = 1;
			// 
			// bntRefresh
			// 
			this.bntRefresh.Location = new System.Drawing.Point(12, 35);
			this.bntRefresh.Name = "bntRefresh";
			this.bntRefresh.Size = new System.Drawing.Size(75, 23);
			this.bntRefresh.TabIndex = 9;
			this.bntRefresh.Text = "Refresh";
			this.bntRefresh.UseVisualStyleBackColor = true;
			this.bntRefresh.Click += new System.EventHandler(this.bntRefresh_Click);
			// 
			// txtFileDescriptor
			// 
			this.txtFileDescriptor.Location = new System.Drawing.Point(375, 38);
			this.txtFileDescriptor.Name = "txtFileDescriptor";
			this.txtFileDescriptor.Size = new System.Drawing.Size(34, 20);
			this.txtFileDescriptor.TabIndex = 6;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(294, 38);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(74, 13);
			this.label10.TabIndex = 0;
			this.label10.Text = "FileDescriptor:";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox1.Location = new System.Drawing.Point(0, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(742, 285);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// statusStrip1
			// 
			this.statusStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 0);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(913, 25);
			this.statusStrip1.TabIndex = 5;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 20);
			this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
			// 
			// _tabMockData
			// 
			this._tabMockData.Controls.Add(this.label13);
			this._tabMockData.Controls.Add(this._txtMockDbDatabase);
			this._tabMockData.Controls.Add(this._btnMockVerifyData);
			this._tabMockData.Controls.Add(this.label12);
			this._tabMockData.Controls.Add(this._txtMockDbPassword);
			this._tabMockData.Controls.Add(this.label11);
			this._tabMockData.Controls.Add(this._txtMockDbUser);
			this._tabMockData.Controls.Add(this.label2);
			this._tabMockData.Controls.Add(this._txtMockDbServer);
			this._tabMockData.Location = new System.Drawing.Point(4, 22);
			this._tabMockData.Name = "_tabMockData";
			this._tabMockData.Padding = new System.Windows.Forms.Padding(3);
			this._tabMockData.Size = new System.Drawing.Size(919, 390);
			this._tabMockData.TabIndex = 2;
			this._tabMockData.Text = "Setup Mock Data";
			this._tabMockData.UseVisualStyleBackColor = true;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(7, 39);
			this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(74, 13);
			this.label13.TabIndex = 25;
			this.label13.Text = "DB Database:";
			// 
			// _txtMockDbDatabase
			// 
			this._txtMockDbDatabase.Location = new System.Drawing.Point(84, 36);
			this._txtMockDbDatabase.Margin = new System.Windows.Forms.Padding(2);
			this._txtMockDbDatabase.Name = "_txtMockDbDatabase";
			this._txtMockDbDatabase.Size = new System.Drawing.Size(176, 20);
			this._txtMockDbDatabase.TabIndex = 24;
			// 
			// _btnMockVerifyData
			// 
			this._btnMockVerifyData.Location = new System.Drawing.Point(84, 118);
			this._btnMockVerifyData.Name = "_btnMockVerifyData";
			this._btnMockVerifyData.Size = new System.Drawing.Size(133, 23);
			this._btnMockVerifyData.TabIndex = 23;
			this._btnMockVerifyData.Text = "Verify Mock Data";
			this._btnMockVerifyData.UseVisualStyleBackColor = true;
			this._btnMockVerifyData.Click += new System.EventHandler(this._btnMockVerifyData_Click);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(7, 87);
			this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(74, 13);
			this.label12.TabIndex = 22;
			this.label12.Text = "DB Password:";
			// 
			// _txtMockDbPassword
			// 
			this._txtMockDbPassword.Location = new System.Drawing.Point(84, 84);
			this._txtMockDbPassword.Margin = new System.Windows.Forms.Padding(2);
			this._txtMockDbPassword.Name = "_txtMockDbPassword";
			this._txtMockDbPassword.Size = new System.Drawing.Size(176, 20);
			this._txtMockDbPassword.TabIndex = 21;
			this._txtMockDbPassword.UseSystemPasswordChar = true;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(7, 63);
			this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(50, 13);
			this.label11.TabIndex = 20;
			this.label11.Text = "DB User:";
			// 
			// _txtMockDbUser
			// 
			this._txtMockDbUser.Location = new System.Drawing.Point(84, 60);
			this._txtMockDbUser.Margin = new System.Windows.Forms.Padding(2);
			this._txtMockDbUser.Name = "_txtMockDbUser";
			this._txtMockDbUser.Size = new System.Drawing.Size(176, 20);
			this._txtMockDbUser.TabIndex = 19;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(7, 15);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(59, 13);
			this.label2.TabIndex = 18;
			this.label2.Text = "DB Server:";
			// 
			// _txtMockDbServer
			// 
			this._txtMockDbServer.Location = new System.Drawing.Point(84, 12);
			this._txtMockDbServer.Margin = new System.Windows.Forms.Padding(2);
			this._txtMockDbServer.Name = "_txtMockDbServer";
			this._txtMockDbServer.Size = new System.Drawing.Size(176, 20);
			this._txtMockDbServer.TabIndex = 17;
			// 
			// _btnHitDIT
			// 
			this._btnHitDIT.Location = new System.Drawing.Point(732, 5);
			this._btnHitDIT.Margin = new System.Windows.Forms.Padding(2);
			this._btnHitDIT.Name = "_btnHitDIT";
			this._btnHitDIT.Size = new System.Drawing.Size(136, 24);
			this._btnHitDIT.TabIndex = 21;
			this._btnHitDIT.Text = "Hit DIT";
			this._btnHitDIT.UseVisualStyleBackColor = true;
			this._btnHitDIT.Click += new System.EventHandler(this._btnHitDIT_Click);
			// 
			// CommonExceptionTestClient
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(927, 416);
			this.Controls.Add(this.tabControl1);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CommonExceptionTestClient";
			this.Text = "CommonExceptionTestClient";
			this.Load += new System.EventHandler(this.CommonExceptionTestClient_Load);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.splitContainer3.Panel1.ResumeLayout(false);
			this.splitContainer3.Panel2.ResumeLayout(false);
			this.splitContainer3.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
			this.splitContainer3.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel1.PerformLayout();
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this._tabMockData.ResumeLayout(false);
			this._tabMockData.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        internal System.Windows.Forms.TreeView tvImageList;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label8;
		internal System.Windows.Forms.TextBox txtLockboxId;
		internal System.Windows.Forms.ComboBox cmbFront;
        private System.Windows.Forms.Label label7;
		internal System.Windows.Forms.ComboBox cmbMode;
        private System.Windows.Forms.Label label6;
		internal System.Windows.Forms.TextBox txtSeq;
        private System.Windows.Forms.Label label5;
		internal System.Windows.Forms.TextBox txtBatchId;
        private System.Windows.Forms.Label label4;
		internal System.Windows.Forms.TextBox txtBankId;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        internal System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Button bntRefresh;
		internal System.Windows.Forms.TextBox txtFileDescriptor;
        private System.Windows.Forms.Label label10;
		internal System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _txtCommonExceptionID;
        private System.Windows.Forms.Button UnlockTransaction;
        private System.Windows.Forms.Button CheckOutTransaction;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Ping;
        private System.Windows.Forms.Button GetTransactionsGetTransactionPendingDecision;
        private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.TextBox _txtLog;
		private System.Windows.Forms.TabPage _tabMockData;
		private System.Windows.Forms.Button _btnMockVerifyData;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox _txtMockDbPassword;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox _txtMockDbUser;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox _txtMockDbServer;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox _txtMockDbDatabase;
		private System.Windows.Forms.Button _btnAcceptTransaction;
		private System.Windows.Forms.Button _btnRejectTransaction;
		private System.Windows.Forms.Button _btnRequestImage;
		private System.Windows.Forms.Button _btnHitDIT;

    }
}

