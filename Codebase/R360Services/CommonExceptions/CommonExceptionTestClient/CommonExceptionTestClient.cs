﻿using System;
using System.Windows.Forms;
using WFS.RecHub.CommonExceptionServicesClient;
using System.Security.Claims;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace CommonExceptionTestClient
{
	public partial class CommonExceptionTestClient : Form
	{
		// The Common Exception Manager object is used to call the service and retrieve / modify data for common exceptions
		public readonly CommonExceptionManager _commonExceptionManager;

		public CommonExceptionTestClient()
		{
			InitializeComponent();
			toolStripStatusLabel1.Text = "";

			// Test app uses a hard-coded identity
			ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { 
				//new Claim(ClaimTypes.Sid, "{4a183c6f-13d6-4303-bf7a-f8b32deda643}") 
				new Claim(ClaimTypes.Sid, "{00000042-0042-0042-0042-000000000042}"),
				new Claim("http://www.wausaufs.com/ram/2012/01/identity/claims/sessionid", "{42000000-4200-4200-4200-420000000000}")
			}));

			InitLog();

			_commonExceptionManager = new CommonExceptionManager();
		}

		private void OutputText(string Text)
		{
			this.textBox1.Text = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "    " + Text + "\r\n" + this.textBox1.Text;
		}

		private void OutputErrors(string method, BaseResponse response)
		{
			OutputText(method + " Failed:  " + String.Join("; ", response.Errors));
		}

		#region Common Exception Tests

		private void Ping_Click(object sender, EventArgs e)
		{
			try
			{
				PingResponse response = _commonExceptionManager.Ping();

				if (response.Status == StatusCode.SUCCESS)
				{
					OutputText("Ping Response: " + response.responseString);
				}
				else
				{
					OutputErrors("Ping", response);
				}
			}
			catch (Exception ex)
			{
				OutputText("Exception: " + ex.Message);
			}

			// After any action, check for log updates
			CheckLog();
		}

		private void GetTransactions_Click(object sender, EventArgs e)
		{
			try
			{
				// Request Transactions
				TransactionsResponse response = _commonExceptionManager.GetTransactionsPendingDecision();

				// Process response
				if (response.Status == StatusCode.SUCCESS)
				{
					int rowCount = response.Transactions.Count;
					string ids = string.Empty;
					if (rowCount > 0)
					{
						_txtCommonExceptionID.Text = response.Transactions[0].CommonExceptionId.ToString();
						ids = response.Transactions.Select(o => o.CommonExceptionId.ToString()).Aggregate((s1, s2) => s1 + ", " + s2);
					}
					OutputText("Get Transactions successed. Returned " + rowCount.ToString() + " number of rows - " + ids);
				}
				else
				{
					OutputErrors("GetTransactionsPendingDecision", response);
				}
			}
			catch (Exception ex)
			{
				OutputText("Exception: " + ex.Message);
			}

			// After any action, check for log updates
			CheckLog();
		}

		private void UnlockTransaction_Click(object sender, EventArgs e)
		{
			int commonExceptionID;
			if (!Int32.TryParse(this._txtCommonExceptionID.Text, out commonExceptionID))
			{
				MessageBox.Show("Could not convert common exception ID");
				return;
			}

			try
			{
				BaseResponse response = _commonExceptionManager.UnlockTransaction(commonExceptionID);
				if (response.Status == StatusCode.SUCCESS)
				{
					OutputText("Unlock Transaction Success");
				}
				else
				{
					OutputErrors("UnlockTransaction", response);
				}
			}
			catch (Exception ex)
			{
				OutputText("Exception: " + ex.Message);
			}

			// After any action, check for log updates
			CheckLog();
		}

		private void CheckOutTransaction_Click(object sender, EventArgs e)
		{
			int commonExceptionID;
			if (!Int32.TryParse(this._txtCommonExceptionID.Text, out commonExceptionID))
			{
				MessageBox.Show("Could not convert common exception ID");
				return;
			}

			try
			{
				TransactionDetailsResponse response = _commonExceptionManager.CheckoutTransaction(commonExceptionID);
				if (response.Status == StatusCode.SUCCESS)
				{
					OutputText("Checkout Transaction Success - Status = " + response.Transaction.Status.ToString());
				}
				else
				{
					OutputErrors("Checkout Transaction", response);
				}
			}
			catch (Exception ex)
			{
				OutputText("Exception: " + ex.Message);
			}

			// After any action, check for log updates
			CheckLog();
		}

		private void _btnAcceptTransaction_Click(object sender, EventArgs e)
		{
			int commonExceptionID;
			if (!Int32.TryParse(this._txtCommonExceptionID.Text, out commonExceptionID))
			{
				MessageBox.Show("Could not convert common exception ID");
				return;
			}

			try
			{
				CheckinResponse response = _commonExceptionManager.CheckinTransaction(commonExceptionID, ExceptionAction.Accept, false, false, null, null);
				if (response.Status == StatusCode.SUCCESS)
				{
					OutputText("Accept Transaction Success");
				}
				else
				{
					OutputErrors("Accept Transaction", response);
				}
			}
			catch (Exception ex)
			{
				OutputText("Exception: " + ex.Message);
			}

			// After any action, check for log updates
			CheckLog();
		}

		private void _btnRejectTransaction_Click(object sender, EventArgs e)
		{
			int commonExceptionID;
			if (!Int32.TryParse(this._txtCommonExceptionID.Text, out commonExceptionID))
			{
				MessageBox.Show("Could not convert common exception ID");
				return;
			}

			try
			{
				CheckinResponse response = _commonExceptionManager.CheckinTransaction(commonExceptionID, ExceptionAction.Reject, false, false, null, null);
				if (response.Status == StatusCode.SUCCESS)
				{
					OutputText("Reject Transaction Success");
				}
				else
				{
					OutputErrors("Reject Transaction", response);
				}
			}
			catch (Exception ex)
			{
				OutputText("Exception: " + ex.Message);
			}

			// After any action, check for log updates
			CheckLog();
		}

		#endregion

		#region Image tests
		private void bntRefresh_Click(object sender, EventArgs e)
		{
			ImageRetrievalAPITester.LoadImage(this);
		}

		private void CommonExceptionTestClient_Load(object sender, EventArgs e)
		{
			ImageRetrievalAPITester.Init(this);

			try
			{
				string configPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CommonExceptionTestClient");
				using (FileStream fs = File.OpenRead(Path.Combine(configPath, "MockDataLastValues.txt")))
				{
					XmlSerializer ser = new XmlSerializer(typeof(MockDataParms));
					MockDataParms p = ser.Deserialize(fs) as MockDataParms;
					if (p != null)
					{
						_txtMockDbServer.Text = p.Server;
						_txtMockDbDatabase.Text = p.Database;
						_txtMockDbUser.Text = p.User;
					}
				}
			}
			catch { }
		}

		private void tvImageList_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			ImageRetrievalAPITester.OnBeforeExpand(this, e);
		}

		private void tvImageList_AfterSelect(object sender, TreeViewEventArgs e)
		{
			ImageRetrievalAPITester.OnAfterSelect(this, e);
		}
		#endregion

		#region Log File Monitoring

		// Default location for the client test program's log is "..\ipo_log.txt"
		// Read IPOnline.ini path from appSettings, and read log name from IPONline.ini...
		private string _logPath = null;
		private long _startLogOffset = 0;
		private void InitLog()
		{
			try
			{
				// Detect log path
				string localIniPath = ConfigurationManager.AppSettings["localIni"];
				if (string.IsNullOrEmpty(localIniPath))
				{
					_txtLog.Text = "(INI File Not Configured)";
				}
				else
				{
					string siteKey = "[" + ConfigurationManager.AppSettings["siteKey"] + "]";
					string[] iniSettings = File.ReadAllLines(localIniPath);
					int sectionIndex = Array.IndexOf(iniSettings, siteKey);
					if (sectionIndex < 0) sectionIndex = 0;
					string logFilePathSetting = iniSettings.Skip(sectionIndex).FirstOrDefault(o => o.StartsWith("LogFile="));
					if (logFilePathSetting != null)
						_logPath = logFilePathSetting.Split(new char[] { '=' }, 2)[1];

					if (string.IsNullOrEmpty(_logPath))
					{
						_txtLog.Text = "(Log File Not Configured)";
					}
					else if (File.Exists(_logPath))
					{
						_txtLog.Text = "(Log File Found)";
						using (var f = File.OpenRead(_logPath))
						{
							_startLogOffset = f.Length;
						}
					}
				}
			}
			catch (Exception ex)
			{
				_txtLog.Text = ex.ToString();
			}
		}

		private void CheckLog()
		{
			try
			{
				if (string.IsNullOrEmpty(_logPath))
				{
					_txtLog.Text = "(Log File Not Configured)";
				}
				else if (File.Exists(_logPath))
				{
					using (var f = File.OpenRead(_logPath))
					{
						f.Seek(_startLogOffset, SeekOrigin.Begin);
						StreamReader sr = new StreamReader(f);
						_txtLog.Text = sr.ReadToEnd();
					}
				}
				else
				{
					_txtLog.Text = "(Log File Not Found: " + _logPath + ")";
				}
			}
			catch (Exception ex)
			{
				_txtLog.Text = ex.ToString();
			}
		}

		#endregion

		private void _btnMockVerifyData_Click(object sender, EventArgs e)
		{
			// Capture parameters
			MockDataParms p = new MockDataParms();
			try
			{
				p.Server = _txtMockDbServer.Text;
				p.Database = _txtMockDbDatabase.Text;
				p.User = _txtMockDbUser.Text;
				p.Password = _txtMockDbPassword.Text;

				string configPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CommonExceptionTestClient");
				Directory.CreateDirectory(configPath);
				using (FileStream fs = File.Create(Path.Combine(configPath, "MockDataLastValues.txt")))
				{
					XmlSerializer ser = new XmlSerializer(typeof(MockDataParms));
					ser.Serialize(fs, p);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			try
			{
				MockData.Synchronize(p);
				MessageBox.Show("Done");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void _btnRequestImage_Click(object sender, EventArgs e)
		{
			int commonExceptionID;
			if (!Int32.TryParse(this._txtCommonExceptionID.Text, out commonExceptionID))
			{
				MessageBox.Show("Could not convert common exception ID");
				return;
			}

			try
			{
				CEImageRequest req = new CEImageRequest() { CommonExceptionID = commonExceptionID, BatchSequence = 1, FileDescriptor = "C", IsBack = false, ColorMode = ImageColorMode.Bitonal };
				ReturnItem response = _commonExceptionManager.ServeImage(req);
				OutputText("Retrieve Image: " + response.bRetVal.ToString());
			}
			catch (Exception ex)
			{
				OutputText("Exception: " + ex.Message);
			}

			// After any action, check for log updates
			CheckLog();
		}

		private void _btnHitDIT_Click(object sender, EventArgs e)
		{
			try
			{
				var response = _commonExceptionManager.ProcessStagedTransactions();
				OutputText("ProcessStagedTransactions: " + response.Status.ToString());
			}
			catch (Exception ex)
			{
				OutputText("Exception: " + ex.Message);
			}

			// After any action, check for log updates
			CheckLog();
		}
	}
}
