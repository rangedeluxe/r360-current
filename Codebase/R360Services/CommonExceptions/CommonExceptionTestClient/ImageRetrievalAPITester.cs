﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.CommonException.CommonExceptionAPI;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 10/05/2013
*
* Purpose: Supplies the a test interface for the CommonException functionality
*
* Modification History
* WI 115373 CEJ 10/05/2013
*   Change the CommonExceptionAPI to use the OLFImageService to retrieve images
******************************************************************************/

namespace CommonExceptionTestClient
{
	internal static class ImageRetrievalAPITester
	{
		public static void Init(CommonExceptionTestClient form)
		{
			string baseImagepath = WFS.RecHub.Common.ipoINILib.IniReadValue("IPOnline", "ImagePath");
			if (string.IsNullOrWhiteSpace(baseImagepath))
			{
				var node = form.tvImageList.Nodes.Add("No Image path set");
				node.Tag = string.Empty;
			}
			else
			{
				foreach (string dir in Directory.GetDirectories(baseImagepath))
				{
					string filename = Path.GetFileName(dir);
					string shorter = filename.Substring(0, 6);
					TreeNode parent;
					if (form.tvImageList.Nodes.ContainsKey(shorter))
						parent = form.tvImageList.Nodes[shorter];
					else
					{
						parent = form.tvImageList.Nodes.Add(shorter, shorter);
						parent.Tag = "";
					}
					TreeNode node = new TreeNode("Date:" + filename);
					node.Tag = dir;
					parent.Nodes.Add(node);
					node.Nodes.Add(new TreeNode { Text = "Dummy" });
				}
			}
		}

		public static void OnBeforeExpand(CommonExceptionTestClient form, TreeViewCancelEventArgs e)
		{
			TreeNode current = e.Node;

			if (current.Nodes.Count == 0)
				return;
			if (current.Nodes[0].Text != "Dummy")
				return;
			current.Nodes.Clear();

			string prefix = "";
			switch (current.Text.Substring(0, 4))
			{
				case "Date":
					prefix = "BankId:";
					break;
				case "Bank":
					prefix = "LockboxId:";
					break;
				case "Lock":
					prefix = "File:";
					break;
			}
			if (prefix != "File:")
			{
				foreach (string dir in Directory.GetDirectories((string)current.Tag))
				{
					TreeNode node = new TreeNode(prefix + Path.GetFileName(dir));
					node.Tag = dir;
					current.Nodes.Add(node);
					node.Nodes.Add(new TreeNode { Text = "Dummy" });
				}
			}
			else
			{
				foreach (string dir in Directory.GetFiles((string)current.Tag))
				{
					if (dir.ToLower().EndsWith(".tif"))
					{
						string fn = Path.GetFileName(dir);
						string[] parts = fn.Split('_');
						TreeNode node = new TreeNode(prefix + Path.GetFileName(dir) + "(" + +new FileInfo(dir).Length + ")");
						LTAImageInfoContract contract = new LTAImageInfoContract();
						contract.SourceBatchID = int.Parse(parts[0]);
						contract.BatchSequence = int.Parse(parts[1]);
						contract.FileDescriptor = parts[2];
						if (parts[3].StartsWith("F"))
							contract.IsBack = false;
						else
							contract.IsBack = true;
						contract.ColorMode = 1;
						if (parts[3].Substring(1, 1) == "G")
							contract.ColorMode = 4;
						else
							if (parts[3].Substring(1, 1) == "C")
								contract.ColorMode = 2;

						contract.LockboxID = int.Parse(current.Text.Substring(10));
						contract.BankID = int.Parse(current.Parent.Text.Substring(7));
						contract.ProcessingDateKey = int.Parse(current.Parent.Parent.Text.Substring(5));

						node.Tag = contract;
						current.Nodes.Add(node);
					}
				}
			}
			form.tvImageList.Invalidate();
		}

		public static void OnAfterSelect(CommonExceptionTestClient form, TreeViewEventArgs e)
		{
			TreeNode current = e.Node;
			if (current.Tag.GetType() == typeof(string))
			{
				return;
			}

			LTAImageInfoContract contract = (LTAImageInfoContract)current.Tag;

			form.txtBatchId.Text = contract.SourceBatchID.ToString();
			form.txtSeq.Text = contract.BatchSequence.ToString();
			form.txtFileDescriptor.Text = contract.FileDescriptor;
			if (contract.IsBack)
				form.cmbFront.SelectedIndex = 1;
			else
				form.cmbFront.SelectedIndex = 0;
			if (contract.ColorMode == 1)
				form.cmbMode.SelectedIndex = 0;
			else
				if (contract.ColorMode == 2)
					form.cmbMode.SelectedIndex = 1;
				else
					form.cmbMode.SelectedIndex = 2;
			form.txtLockboxId.Text = contract.LockboxID.ToString();
			form.txtBankId.Text = contract.BankID.ToString();
			form.txtDate.Text = contract.ProcessingDateKey.ToString();
		}

		public static void LoadImage(CommonExceptionTestClient form)
		{
			string errorContext = "";

			LTAImageInfoContract imageRequest = new LTAImageInfoContract();

			try
			{
				errorContext = "Error converting ProcessingDateKey";
				imageRequest.ProcessingDateKey = int.Parse(form.txtDate.Text);

				errorContext = "Error converting BankID";
				imageRequest.BankID = int.Parse(form.txtBankId.Text);

				errorContext = "Error converting LockboxID";
				imageRequest.LockboxID = int.Parse(form.txtLockboxId.Text);

				errorContext = "Error converting BatchID";
				imageRequest.SourceBatchID = int.Parse(form.txtBatchId.Text);

				errorContext = "Error converting BatchSequence";
				imageRequest.BatchSequence = int.Parse(form.txtSeq.Text);

				errorContext = "Error converting ColorMode";
				imageRequest.ColorMode = 1;
				if (form.cmbMode.Text == "Color")
					imageRequest.ColorMode = 2;
				else if (form.cmbMode.Text == "Gray")
					imageRequest.ColorMode = 4;

				errorContext = "Error converting FileDescriptor";
				imageRequest.FileDescriptor = form.txtFileDescriptor.Text;

				imageRequest.IsBack = form.cmbFront.Text == "Back";
				errorContext = "";
			}
			catch (Exception ex)
			{
				if (errorContext.Length > 0)
					MessageBox.Show(errorContext + ": " + ex.Message);
				else
					MessageBox.Show("exception:" + ex.Message);
			}

			try
			{
				form.pictureBox1.Image = null;
				form.Cursor = Cursors.WaitCursor;
				DateTime starter = DateTime.Now;

				ImageRetrieval ir = new ImageRetrieval("IPOnline");
                ReturnItem item = ir.ServeImage(imageRequest, new Guid(ConfigurationManager.AppSettings["Session ID"]));

				if (item.bRetVal)
				{
					Bitmap bm = new Bitmap(item.data);
					form.pictureBox1.Image = bm;
				}
				else
				{
					MessageBox.Show(item.strrespXml);
				}
				form.toolStripStatusLabel1.Text = "milliseconds=" + (DateTime.Now - starter).TotalMilliseconds;
			}
			catch (Exception ex)
			{
				MessageBox.Show("exception:" + ex.Message);
			}
			finally
			{
				form.Cursor = Cursors.Default;
			}
		}
	}
}
