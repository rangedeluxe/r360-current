﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using WFS.RecHub.R360Shared;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 5/22/2013
*
* Purpose: Common Exception WCF Interface
*
* Modification History
* WI 101774 WJS  5/22/2013
*   - Initial Creation
* WI 101852 DRP 6/5/2013
*   - Adding Image Retrieval
*
*********************************************************************************/
namespace WFS.RecHub.CommonException.Common
{
  [ServiceContract(Namespace = "urn:wausaufs.com:services:CommonExceptionService")]
  public interface ICommonException
  {
    [OperationContract]
    PingResponse Ping();

    [OperationContract]
    TransactionsResponse GetTransactionsPendingDecision();

    [OperationContract]
    BaseResponse UnlockTransaction(int commonExceptionID);

    [OperationContract]
    TransactionDetailsResponse ViewTransaction(int commonExceptionID);

    [OperationContract]
    TransactionDetailsResponse CheckoutTransaction(int commonExceptionID);

    [OperationContract]
    CheckinResponse CheckinTransaction(int commonExceptionID, ExceptionAction transactionDecision, bool overrideRules, bool forceBalance, IEnumerable<PaymentItemUpdate> stubUpdates, IEnumerable<PaymentItemUpdate> paymentUpdates);

    [OperationContract]
    ReturnItem ServeImage(CEImageRequest imageRequest);

	[OperationContract]
	BaseResponse ProcessStagedTransactions();

	[OperationContract]
	GenericResponse<DefaultBRErrorMessagesDto> GetDefaultBRErrorMessages();
  }



  [DataContract]
  public enum ImageColorMode
  {
    [EnumMember]
    Unknown = 0,

    [EnumMember]
    Bitonal = 1,

    [EnumMember]
    Color = 2,

    [EnumMember]
    Grayscale = 4,
  }
}
