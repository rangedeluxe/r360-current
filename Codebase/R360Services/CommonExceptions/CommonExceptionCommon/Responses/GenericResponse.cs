﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Kyle Colden
 * * Date: 08/21/2014
 * *
 * * Purpose: Houses the generic data with the call status for return to WCF Client
 * *
 * * Modification History
 * WI ?????? KLC 08/21/2014 Created
* ******************************************************************************/
namespace WFS.RecHub.CommonException.Common
{
	public class GenericResponse<T> : BaseResponse
	{
		public GenericResponse()
			: base()
		{
		}

		public T Data { get; set; }
	}
}
