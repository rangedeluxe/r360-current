﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 6/4/2013
*
* Purpose: Common Exception Common Class
*
* Modification History
* WI 101774 WJS  6/4/2013
*   - Initial Creation
*
*********************************************************************************/
namespace WFS.RecHub.CommonException.Common
{
	public class TransactionResponse : BaseResponse
	{
		public TransactionResponse() : base() { }

		public TransactionModel Transaction { get; set; }
	}

	public class TransactionsResponse : BaseResponse
	{
		public TransactionsResponse() : base() { }

		public List<TransactionModel> Transactions { get; set; }
	}
}
