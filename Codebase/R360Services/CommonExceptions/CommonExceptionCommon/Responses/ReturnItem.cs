﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: David Parker
* Date: 6/06/2013
*
* Purpose: Common Exception Image Stream Wrapper
*
* Modification History
* WI 101852 DRP 6/5/2013
*   - Adding Image Retrieval
*********************************************************************************/
namespace WFS.RecHub.CommonException.Common
{
    [MessageContract]
    public class ReturnItem : IDisposable
    {
        [MessageHeader(MustUnderstand = true)]
        public bool bRetVal
        { get; set; }
        [MessageHeader(MustUnderstand = true)]
        public string strrespXml
        { get; set; }
        [MessageBodyMember]
        public Stream data
        { get; set; }

		// TODO: What is the point of a Dispose method on a MessageContract?  No one is ever going to call it...
        public void Dispose()
        {
            if (data != null)
            {
                data.Close();
                data = null;
            }
        }
    }
}
