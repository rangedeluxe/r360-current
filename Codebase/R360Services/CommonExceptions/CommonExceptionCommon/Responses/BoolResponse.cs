﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.CommonException.Common
{
	public class BoolResponse : BaseResponse
	{
		public bool Value;
	}
}
