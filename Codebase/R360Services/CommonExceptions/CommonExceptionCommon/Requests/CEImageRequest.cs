﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: David Parker
* Date: 6/06/2013
*
* Purpose: Common Exception Image Stream Request contract
*
* Modification History
* WI 101852 DRP 6/5/2013
*   - Adding Image Retrieval
*********************************************************************************/
namespace WFS.RecHub.CommonException.Common
{
    [MessageContract]
    public class CEImageRequest
    {
        [MessageBodyMember(Order = 1)]
        public int CommonExceptionID
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 5)]
        public int BatchSequence
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 6)]
        public string FileDescriptor
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 7)]
        public bool IsBack
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 8)]
        public ImageColorMode ColorMode
        {
            get;
            set;
        }
    }
}
