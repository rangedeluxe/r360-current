﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public class PaymentItem
	{
		[DataMember]
		public Guid Id { get; set; }

		[DataMember]
		public bool IsException { get; set; }

		[DataMember]
		public IList<FieldDataAndInfo> FieldValues { get; set; }

		[DataMember]
		public IList<string> Messages { get; set; }
	}

	[DataContract]
	public class ItemList
	{
		[DataMember]
		public IList<PaymentItem> Items { get; set; }

		[DataMember]
		public IList<ColumnDefinition> FieldDefinitions { get; set; }
	}
}
