﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public enum FieldDataTypes
	{
		[EnumMember]
		Unknown = 0,
		[EnumMember]
		Text = 1,
		[EnumMember]
		FloatingPoint = 6,
		[EnumMember]
		Currency = 7,
		[EnumMember]
		Date = 11,
	}

	[DataContract]
	public class ColumnDefinition
	{
		[DataMember]
		public bool Editable { get; set; }

		[DataMember]
		public string Tooltip { get; set; }

		[DataMember]
		public string ColumnFieldName { get; set; }

		[DataMember]
		public string ColumnTitle { get; set; }

		[DataMember]
		public int Sequence { get; set; }

		[DataMember]
		public int MinLength { get; set; }

		[DataMember]
		public int MaxLength { get; set; }

		[DataMember]
		public bool Required { get; set; }

		[DataMember]
		public string RequiredMessage { get; set; }

		[DataMember]
		public FormatTypes FormatType { get; set; }

		[DataMember]
		public string Mask { get; set; }

		[DataMember]
		public Guid Id { get; set; }

		[DataMember]
		public bool IsItemAmount { get; set; }

		[DataMember]
		public bool HasBusinessRules { get; set; }

		[DataMember]
		public FieldDataTypes DataType { get; set; }
	}

	[DataContract]
	public class FieldDataAndInfo
	{
		[DataMember]
		public Guid ColumnDefId { get; set; }

		[DataMember]
		public string FieldValue { get; set; }

		[DataMember]
		public bool AwaitingDecision { get; set; }

		[DataMember]
		public string DecisionMessage { get; set; }
	}

	[DataContract]
	public class FieldData
	{
		[DataMember]
		public Guid ColumnDefId { get; set; }

		[DataMember]
		public string FieldValue { get; set; }
	}

}
