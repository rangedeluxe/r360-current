﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.CommonException.Common
{
	public class FieldDto
	{
		public int? DataEntryColumnKey;
		public string FieldName;
		public string Value;
		public DateTime DateTimeValue;
		public bool IsException = false;
		public string ExceptionMessage = null;
	}
}
