﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public class TransactionImageRef
	{
		[DataMember]
		public int BatchSequence { get; set; }

		[DataMember]
		public string FileDescriptor { get; set; }
	}
}
