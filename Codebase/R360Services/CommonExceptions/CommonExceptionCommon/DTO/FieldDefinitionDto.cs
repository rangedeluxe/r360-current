﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public enum FieldTableTypes
	{
		[EnumMember]
		Payments = 0,
		[EnumMember]
		PaymentsDataEntry = 1,
		[EnumMember]
		Stubs = 2,
		[EnumMember]
		StubsDataEntry = 3,
	}

	[DataContract]
	public class FieldDefinitionDto
	{
		[DataMember]
		public int DataEntryColumnKey;

		[DataMember]
		public FieldTableTypes TableType;

		[DataMember]
		public FieldDataTypes DataType;

		[DataMember]
		public int FieldLength;

		[DataMember]
		public int ScreenOrder;

		[DataMember]
		public string FldName;

		[DataMember]
		public string DisplayName;

		[DataMember]
		public List<BusinessRulesDto> BusinessRules;
	}
}
