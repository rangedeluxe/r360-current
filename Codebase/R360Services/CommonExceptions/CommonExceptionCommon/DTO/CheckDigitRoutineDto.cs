﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public enum CheckDigitMethod
	{
		[EnumMember]
		SumOfDigits = 0,
		[EnumMember]
		OnesDigit = 1,
		[EnumMember]
		ProductAddition = 2,
		//TODO: Custom?
	}

	[DataContract]
	public enum WeightPatternDirection
	{
		[EnumMember]
		LeftToRight = 0,
		[EnumMember]
		RightToLeft = 1,
	}

	[DataContract]
	public class CheckDigitRoutineDto
	{
		public long Key;

		[DataMember]
		public int Offset;

		[DataMember]
		public CheckDigitMethod Method;

		[DataMember]
		public int Modulus;

		[DataMember]
		public int Compliment;

		[DataMember]
		public WeightPatternDirection WeightsDirection;

		[DataMember]
		public int[] Weights;

		[DataMember]
		public bool IgnoreSpaces;

		[DataMember]
		public int Rem10Replacement;

		[DataMember]
		public int Rem11Replacement;

		[DataMember]
		public string FailureMessage;

		[DataMember]
		public Dictionary<char, int?> ReplacementValues;
	}
}
