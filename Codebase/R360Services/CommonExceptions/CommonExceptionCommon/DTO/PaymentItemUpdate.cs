﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public enum ExceptionAction
	{
		[EnumMember]
		NoAction,
		[EnumMember]
		Accept,
		[EnumMember]
		Reject,
	}

	[DataContract]
	public enum ExceptionItemAction
	{
		[EnumMember]
		NoChange = 0,

		[EnumMember]
		UpdateItem = 1,

		[EnumMember]
		DeleteItem = 2,

		[EnumMember]
		InsertItem = 3,
	}

	[DataContract]
	public class PaymentItemUpdate
	{
		[DataMember]
		public Guid Id { get; set; }

		[DataMember]
		public ExceptionItemAction Action { get; set; }

		[DataMember]
		public IList<FieldData> FieldValues { get; set; }
	}
}
