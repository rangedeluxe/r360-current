﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public class PaymentSubTypeDto
	{
		[DataMember]
		public int PaymentSubTypeKey;

		[DataMember]
		public PaymentTypes PaymentType;

		[DataMember]
		public string SubTypeDescription;
	}
}
