﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 5/22/2013
*
* Purpose: Transaction Model Class
*
* Modification History
* WI 101774 WJS  6/4/2013
*   - Initial Creation
* WI 143442 DJW 05/30/2014
*     Updated to handle batch collisions (BatchID to long, and added BatchSourceID)
*********************************************************************************/
namespace WFS.RecHub.CommonException.Common
{
    [DataContract]
    public class TransactionModel
    {
        /*  public enum BatchSourceKeys
          {
              Unknown = 0,
              integraPAY = 1,
              WebDDL = 2,
              ImageRPS = 3
          }

          public enum PaymentTypeNum
          {
              CHECK = 1,
              ACH = 2,
              UNKNOWN = 3
          }
        */

        public enum ExceptionTypes
        {
            InProcess = 0,
            Posting = 1
        }

        public enum DecisioningStatus
        {
            Unresolved = 0,
            Pending = 1,
        }

        [DataMember]
        public int CommonExceptionId { get; set; }

        [DataMember]
        public string PaymentSource { get; set; }

        [DataMember]
        public string Workgroup { get; set; }

        [DataMember]
        public string PaymentType { get; set; }

        [DataMember]
        public int PaymentTypeId { get; set; }

        [DataMember]
        public string DepositDate { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public DecisioningStatus Status { get; set; }

        [DataMember]
        public string Deadline { get; set; }

        [DataMember]
        public ExceptionTypes ExceptionType { get; set; }

        [DataMember]
        public string ProcessingDate { get; set; }

        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public int ClientAccountID { get; set; }

        [DataMember]
        public long BatchId { get; set; }

        [DataMember]
        public long SourceBatchID { get; set; }

        [DataMember]
        public int TransactionID { get; set; }

        [DataMember]
        public bool IsLocked { get; set; }

        [DataMember]
        public int LockOwner { get; set; }

        [DataMember]
        public string LockOwnerName { get; set; }

        [DataMember]
        public string LockTime { get; set; }

        [DataMember]
        public int EntityID { get; set; }

        [DataMember]
        public int? LockOwnerEntityID { get; set; }

        [DataMember]
        public string BatchSourceShortName { get; set; }

        [DataMember]
        public string ImportTypeShortName { get; set; }

        [DataMember]
        public bool DisplayBatchID { get; set; }
    }
}
