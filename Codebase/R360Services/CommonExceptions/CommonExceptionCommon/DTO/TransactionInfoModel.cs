﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public class TransactionInfoModel
	{
		public enum TransactionDecision
		{
			Unresolved = 0,
			Accepted = 1,
			Rejected = 2,
			Unknown = 3,
		}

		public enum BalancingOptions
		{
			NotRequired = 0,
			Desired = 1,
			Required = 2,
		}

		[DataMember]
		public int CommonExceptionId { get; set; }

		[DataMember]
		public ItemList StubItems { get; set; }

		[DataMember]
		public ItemList PaymentItems { get; set; }

		[DataMember]
		public IList<TransactionImageRef> TransactionImages { get; set; }

		[DataMember]
		public TransactionDecision Decision { get; set; }

		[DataMember]
		public BalancingOptions BalancingOption { get; set; }

		[DataMember]
		public bool TransactionRequiresInvoice { get; set; }

		[DataMember]
		public bool IsTransactionException { get; set; }

		[DataMember]
		public string Message { get; set; }
	}
}
