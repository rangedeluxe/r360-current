﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public enum PaymentTypes
	{
		[EnumMember]
		Check = 0,
		[EnumMember]
		ACH = 1,
		[EnumMember]
		Wire = 2,
		[EnumMember]
		CreditCard = 3,
		[EnumMember]
		SWIFT = 4,
		[EnumMember]
		Cash = 5,
		[EnumMember]
		CorrespondenceOnly = 6,
	}
}
