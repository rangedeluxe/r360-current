﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public class DefaultBRErrorMessagesDto
	{
		[DataMember] public string InvoiceIsRequired;
		[DataMember] public string FieldIsRequired;
		[DataMember] public string FieldLengthInvalid;
		[DataMember] public string InvalidControlChars;
		[DataMember] public string InvalidAmountFormat;
		[DataMember] public string InvalidNumberFormat;
		[DataMember] public string InvalidDateFormat;
		[DataMember] public string InvalidCharacters;
	}
}
