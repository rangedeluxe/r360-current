﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	public class ItemDto
	{
		public long? BatchID { get; set; }
		public int? TransactionID { get; set; }
		public int BatchSequence { get; set; }
		public bool IsPayment { get; set; }
		public bool IsException { get; set; }
		public ItemDataDto ItemData { get; set; }
		public IList<FieldDto> Fields { get; set; }
		public IList<string> Messages { get; set; }
	}
}
