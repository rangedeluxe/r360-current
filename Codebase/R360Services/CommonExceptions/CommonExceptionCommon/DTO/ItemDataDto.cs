﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	public class ItemDataDto
	{
		public string Amount;
		public string RT;
		public string Account;
		public string TransactionCode;
		public string Serial;
		public string RemitterName;
	}
}
