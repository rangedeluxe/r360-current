﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public class RuleDefinitionsDto
	{
		[DataMember]
		public bool TransactionRequiresInvoice;

		[DataMember]
		public InvoiceBalancingOptions BalancingOption;

		[DataMember]
		public List<FieldDefinitionDto> FieldDefinitions;
	}
}
