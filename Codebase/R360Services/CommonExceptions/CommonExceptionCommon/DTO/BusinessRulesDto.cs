﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.Common
{
	[DataContract]
	public enum FormatTypes
	{
		[EnumMember]
		Any = 1,
		[EnumMember]
		Number = 2,
		[EnumMember]
		NumberDashes = 3,
		[EnumMember]
		Amount = 4,
		//[EnumMember]
		//OtherAmount = 5,
		[EnumMember]
		Alphanumeric = 6,
		[EnumMember]
		NumberAndSpecialChar = 7,
		[EnumMember]
		AlphanumericAndSpecialChar = 8,
		[EnumMember]
		Date = 9,
		[EnumMember]
		CustomDate = 10,
		[EnumMember]
		Custom = 11,
	}

	[DataContract]
	public class BusinessRulesDto
	{
		[DataMember]
		public int? BatchSourceKey;

		[DataMember]
		public int? BatchPaymentTypeKey;

		[DataMember]
		public int? BatchPaymentSubTypeKey;

		[DataMember]
		public int MinLength;

		[DataMember]
		public int MaxLength;

		[DataMember]
		public bool Required;

		[DataMember]
		public bool UserCanEdit;

		[DataMember]
		public string RequiredMessage;

		[DataMember]
		public FormatTypes FormatType;

		[DataMember]
		public string Mask;

		[DataMember]
		public CheckDigitRoutineDto CheckDigitRoutine;

		[DataMember]
		public long? FieldValidationKey;

		[DataMember]
		public string FieldValidationFailureMessage;

		[DataMember]
		public int FieldValidationTypeKey;
	}
}
