﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.CommonException.Common
{
	public class TransactionDto
	{
		public long? BatchID;
		public int? TransactionID;
		public int BatchSourceKey;
		public PaymentTypes BatchPaymentTypeKey;
		public PaymentSubTypeDto BatchPaymentSubType;
		public long DataEntrySetupKey;
		public bool IsException;
		public bool IsTransactionException;
		public string Message;

		public IList<ItemDto> Items;
	}
}
