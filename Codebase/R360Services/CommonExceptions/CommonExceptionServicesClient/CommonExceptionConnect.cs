﻿using System;
using System.ServiceModel;
using WFS.LTA.Common;
using System.Runtime.CompilerServices;
using WFS.RecHub.R360Shared;
using System.Collections.Generic;
using WFS.RecHub.CommonException.Common;
namespace WFS.RecHub.CommonExceptionServicesClient
{
	internal class CommonExceptionConnect : _ServicesClientBase, ICommonException
	{
		private readonly ICommonException _CommonExceptionService = null;

		public CommonExceptionConnect(string vSiteKey, ltaLog log)
			: base(vSiteKey, log)
		{
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
			_CommonExceptionService = R360ServiceFactory.Create<ICommonException>();
		}

		private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
		{
			// All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
			try
			{
				using (new OperationContextScope((IContextChannel)_CommonExceptionService))
				{
					// Set context inside of operation scope
					R360ServiceContext.Init(this.SiteKey);

					return operation();
				}
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw;
			}
		}

		public PingResponse Ping()
		{
			return Do(_CommonExceptionService.Ping);
		}

		public TransactionsResponse GetTransactionsPendingDecision()
		{
			return Do(_CommonExceptionService.GetTransactionsPendingDecision);
		}

        public BaseResponse UnlockTransaction(int commonExceptionID)
		{
			return Do(() => _CommonExceptionService.UnlockTransaction(commonExceptionID));
		}

		public TransactionDetailsResponse ViewTransaction(int commonExceptionID)
		{
			return Do(() => _CommonExceptionService.ViewTransaction(commonExceptionID));
		}

		public TransactionDetailsResponse CheckoutTransaction(int commonExceptionID)
		{
			return Do(() => _CommonExceptionService.CheckoutTransaction(commonExceptionID));
		}

		public CheckinResponse CheckinTransaction(int commonExceptionID, ExceptionAction transactionDecision, bool overrideRules, bool forceBalance, IEnumerable<PaymentItemUpdate> stubUpdates, IEnumerable<PaymentItemUpdate> paymentUpdates)
		{
			return Do(() => _CommonExceptionService.CheckinTransaction(commonExceptionID, transactionDecision, overrideRules, forceBalance, stubUpdates, paymentUpdates));
		}

		public ReturnItem ServeImage(CEImageRequest imageRequest)
		{
			return Do(() => _CommonExceptionService.ServeImage(imageRequest));
		}

		public BaseResponse ProcessStagedTransactions()
		{
			return Do(() => _CommonExceptionService.ProcessStagedTransactions());
		}

		public GenericResponse<DefaultBRErrorMessagesDto> GetDefaultBRErrorMessages()
		{
			return Do(() => _CommonExceptionService.GetDefaultBRErrorMessages());
		}
	}
}
