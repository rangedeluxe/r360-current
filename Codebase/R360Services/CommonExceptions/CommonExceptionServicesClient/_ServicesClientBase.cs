﻿using System;
using WFS.LTA.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* WI ***** WJS 5/23/2013

******************************************************************************/
namespace WFS.RecHub.CommonExceptionServicesClient {

    public abstract class _ServicesClientBase { 

        private ltaLog _EventLog = null;
        private string _SiteKey;

        public _ServicesClientBase(string vSiteKey, ltaLog log) {
            _SiteKey = vSiteKey;
            _EventLog = log;
        }
        
     

        /// <summary>
        /// Determines the section of the local .ini file to be used for local 
        /// options.
        /// </summary>
        protected string SiteKey{
           get { return _SiteKey; }
        }
       
        protected ltaLog EventLog{
            get{

                return _EventLog;
            }
        }

    }
}
