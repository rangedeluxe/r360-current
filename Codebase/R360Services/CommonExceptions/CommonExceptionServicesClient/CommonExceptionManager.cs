﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Claims;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.CommonExceptionServicesClient
{
  public class CommonExceptionManager : ICommonException
  {
    #region Construction / Initialization

    // Initialization parameters
    private const string LogSource = "CommonExceptionMngr";
    private static ltaLog _eventLog;
    private static string _siteKey;
    private static cSiteOptions _SiteOptions = null;
    private readonly CommonExceptionConnect _connection;

    /// <summary>
    /// Uses SiteKey to retrieve site options from the local .ini file.
    /// </summary>
    protected static cSiteOptions SiteOptions
    {
      get
      {
        if (_SiteOptions == null)
        {
          _SiteOptions = new cSiteOptions(_siteKey);
        }
        return _SiteOptions;
      }
    }
    /// <summary>
    /// Logging component using settings from the local siteOptions object.
    /// </summary>
    protected static ltaLog EventLog
    {
      get
      {
        if (_eventLog == null)
        {
          _eventLog = new ltaLog(SiteOptions.logFilePath,
                                    SiteOptions.logFileMaxSize,
                                    (LTAMessageImportance)SiteOptions.loggingDepth);
        }

        return _eventLog;
      }
    }

    protected static string SiteKey
    {
      get
      {
        return (_siteKey);
      }
      set
      {
        _siteKey = value;
      }
    }

    static CommonExceptionManager()
    {
      // Make sure the site key is initialized before any other properties are used
      GetSiteKey();
    }

	public CommonExceptionManager()
	{
		// Create connection context
		try
		{
			_connection = new CommonExceptionConnect(_siteKey, _eventLog);
		}
		catch (Exception ex)
		{
			EventLog.logError(ex, LogSource, ".ctor");

			// Consructors should not throw exceptions -- just initialize to null and report errors later...
			_connection = null;
		}
	}

    // Specialied method for site key that properly initializes the EventLog after the site key has been set
    private static void GetSiteKey()
    {
      string defaultValue = "IPOnline";
      try
      {
        string value = ConfigurationManager.AppSettings["siteKey"];
        if (string.IsNullOrEmpty(value))
          value = defaultValue;

        _siteKey = value;

        EventLog.logEvent(string.Format("Using SiteKey = '{0}'", value), LogSource, LTA.Common.LTAMessageImportance.Verbose);
      }
      catch (Exception ex)
      {
        _siteKey = defaultValue;

        EventLog.logWarning(string.Format("Error reading configuration setting 'siteKey' - {0}", ex.Message), LogSource, LTA.Common.LTAMessageImportance.Verbose);
      }
    }

	private bool FlagConfigured(string flag)
	{
		try
		{
			string val = ConfigurationManager.AppSettings[flag];
			if (!string.IsNullOrWhiteSpace(val))
				return bool.Parse(val);

			return false;
		}
		catch
		{
			return false;
		}
	}

    private void ValidateConnection()
    {
      if (_connection == null)
        throw new Exception("Configuration Error - check log for details");

	  if (FlagConfigured("UseMockIdentity"))
	  {
		  ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { 
				new Claim(ClaimTypes.Sid, "{00000042-0042-0042-0042-000000000042}"),
				new Claim("http://www.wausaufs.com/ram/2012/01/identity/claims/sessionid", "{42000000-4200-4200-4200-420000000000}")
			}));
	  }
    }

    #endregion

    public PingResponse Ping()
    {
      ValidateConnection();
      return _connection.Ping();
    }

    public TransactionsResponse GetTransactionsPendingDecision()
    {
      ValidateConnection();
      return _connection.GetTransactionsPendingDecision();
    }

    public TransactionDetailsResponse ViewTransaction(int commonExceptionId)
    {
      ValidateConnection();
      return _connection.ViewTransaction(commonExceptionId);
    }

    public TransactionDetailsResponse CheckoutTransaction(int commonExceptionId)
    {
      ValidateConnection();
      return _connection.CheckoutTransaction(commonExceptionId);
    }

	public CheckinResponse CheckinTransaction(int commonExceptionID, ExceptionAction transactionDecision, bool overrideRules, bool forceBalance, IEnumerable<PaymentItemUpdate> stubUpdates, IEnumerable<PaymentItemUpdate> paymentUpdates)
	{
		ValidateConnection();
		return _connection.CheckinTransaction(commonExceptionID, transactionDecision, overrideRules, forceBalance, stubUpdates, paymentUpdates);
	}

    public BaseResponse UnlockTransaction(int commonExceptionId)
    {
      ValidateConnection();
      return _connection.UnlockTransaction(commonExceptionId);
    }

    public ReturnItem ServeImage(CEImageRequest imageRequest)
    {
      ValidateConnection();
      return _connection.ServeImage(imageRequest);
    }

	public GenericResponse<DefaultBRErrorMessagesDto> GetDefaultBRErrorMessages()
	{
		ValidateConnection();
		return _connection.GetDefaultBRErrorMessages();
	}


	public BaseResponse ProcessStagedTransactions()
	{
		ValidateConnection();
		return _connection.ProcessStagedTransactions();
	}
  }
}
