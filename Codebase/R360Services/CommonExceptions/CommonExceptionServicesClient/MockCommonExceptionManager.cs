﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.CommonExceptionServicesClient
{
  public class MockCommonExceptionManager : ICommonException
  {
    public PingResponse Ping()
    {
      PingResponse response = new PingResponse() { responseString = "MockPong" };
      return response;
    }

    public TransactionsResponse GetTransactionsPendingDecision()
    {
      TransactionsResponse response = new TransactionsResponse()
      {
        Transactions = new List<TransactionModel>(new TransactionModel[]
				{
					new TransactionModel()
					{
						Amount = 12345.67m,
						BankId = 1,
						BatchId = 12,
						SourceBatchID = 112,
						ClientAccountID = 123,
						CommonExceptionId = 1234,
						Deadline = (DateTime.Now + TimeSpan.FromHours(3)).ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat),
						DepositDate = DateTime.Today.ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						ExceptionType = TransactionModel.ExceptionTypes.InProcess,
						IsLocked = false,
						LockOwner = -1,
						LockTime = null,
						PaymentSource = "Mock Source",
						PaymentType = "Mock ACH",
						ProcessingDate = (DateTime.Today - TimeSpan.FromDays(1)).ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						Status = TransactionModel.DecisioningStatus.Unresolved
					},
					new TransactionModel()
					{
						Amount = 12355.67m,
						BankId = 1,
						BatchId = 12,
						SourceBatchID = 112,
						ClientAccountID = 123,
						CommonExceptionId = 1235,
						Deadline = (DateTime.Now + TimeSpan.FromHours(3)).ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat),
						DepositDate = DateTime.Today.ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						ExceptionType = TransactionModel.ExceptionTypes.InProcess,
						IsLocked = false,
						LockOwner = -1,
						LockTime = null,
						PaymentSource = "Mock Source",
						PaymentType = "Mock ACH",
						ProcessingDate = (DateTime.Today - TimeSpan.FromDays(1)).ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						Status = TransactionModel.DecisioningStatus.Unresolved
					},
					new TransactionModel()
					{
						Amount = 12365.67m,
						BankId = 1,
						BatchId = 12,
						SourceBatchID = 112,
						ClientAccountID = 123,
						CommonExceptionId = 1236,
						Deadline = (DateTime.Now + TimeSpan.FromHours(3)).ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat),
						DepositDate = DateTime.Today.ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						ExceptionType = TransactionModel.ExceptionTypes.InProcess,
						IsLocked = false,
						LockOwner = 42,
						LockTime = (DateTime.Now - TimeSpan.FromMinutes(1)).ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat),
						PaymentSource = "Mock Source",
						PaymentType = "Mock ACH",
						ProcessingDate = (DateTime.Today - TimeSpan.FromDays(1)).ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						Status = TransactionModel.DecisioningStatus.Pending,
					},	
					new TransactionModel()
					{
						Amount = 98765.43m,
						BankId = 1,
						BatchId = 22,
						SourceBatchID = 122,
						ClientAccountID = 321,
						CommonExceptionId = 4321,
						Deadline = (DateTime.Now + TimeSpan.FromHours(4)).ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat),
						DepositDate = (DateTime.Today - TimeSpan.FromDays(1)).ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						ExceptionType = TransactionModel.ExceptionTypes.InProcess,
						IsLocked = true,
						LockOwner = -1,
						LockTime = null,
						PaymentSource = "Mock Source",
						PaymentType = "Mock Check",
						ProcessingDate = DateTime.Today.ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						Status = TransactionModel.DecisioningStatus.Unresolved
					},
					new TransactionModel()
					{
						Amount = 98764.43m,
						BankId = 1,
						BatchId = 22,
						SourceBatchID = 122,
						ClientAccountID = 321,
						CommonExceptionId = 4322,
						Deadline = (DateTime.Now + TimeSpan.FromHours(4)).ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat),
						DepositDate = (DateTime.Today - TimeSpan.FromDays(1)).ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						ExceptionType = TransactionModel.ExceptionTypes.InProcess,
						IsLocked = true,
						LockOwner = 43,
						LockTime = (DateTime.Now - TimeSpan.FromMinutes(2)).ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat),
						PaymentSource = "Mock Source",
						PaymentType = "Mock Check",
						ProcessingDate = DateTime.Today.ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat),
						Status = TransactionModel.DecisioningStatus.Unresolved
					},
				})
      };
      return response;
    }

    public TransactionDetailsResponse CheckoutTransaction(int commonExceptionId)
    {
      byte[] filler = new byte[8];

      // Mock Locking == only succeed on some
      TransactionDetailsResponse response = new TransactionDetailsResponse()
      {
        LockCreated = (commonExceptionId == 1234 || commonExceptionId == 4321),
        TransactionInfo = new TransactionInfoModel()
        {
          Decision = (commonExceptionId == 1236 ? TransactionInfoModel.TransactionDecision.Accepted : TransactionInfoModel.TransactionDecision.Unresolved),
        },
        Transaction = GetTransactionsPendingDecision().Transactions.Find(o => o.CommonExceptionId == commonExceptionId),
      };

      // Mock Fields (depends on batch)
      switch (commonExceptionId)
      {
        case 1234:
        case 1235:
        case 1236:
			response.TransactionInfo.StubItems = new ItemList()
			{
				FieldDefinitions = new ColumnDefinition[] 
					{
						new ColumnDefinition()
						{
							ColumnFieldName = "CustCode",
							ColumnTitle = "Customer Code",
							FormatType = FormatTypes.Any,
							Editable = true,
							Id = new Guid(43, 42, 1, filler),
							MaxLength = 20,
							Required = true,
							Sequence = 0,
							Tooltip = "Customer Code",
							IsItemAmount = false,
							HasBusinessRules = false
						},
						new ColumnDefinition()
						{
							ColumnFieldName = "Amount",
							ColumnTitle = "Amount",
							FormatType = FormatTypes.Amount,
							Editable = true,
							Id = new Guid(42, 42, 2, filler),
							MaxLength = 20,
							Required = true,
							Sequence = 1,
							Tooltip = "Amount",
							IsItemAmount = true,
							HasBusinessRules = false
						},
					}
			};
          break;

        case 4321:
        case 4322:
		  response.TransactionInfo.StubItems = new ItemList()
		  {
			  FieldDefinitions = new ColumnDefinition[] 
					{
						new ColumnDefinition()
						{
							ColumnFieldName = "InvoiceNbr",
							ColumnTitle = "Invoice Number",
							FormatType = FormatTypes.Any,
							Editable = true,
							Id = new Guid(42, 42, 1, filler),
							MaxLength = 20,
							Required = true,
							Sequence = 0,
							Tooltip = "Invoice Number",
							IsItemAmount = false,
							HasBusinessRules = false
						},
						new ColumnDefinition()
						{
							ColumnFieldName = "InvoiceAmt",
							ColumnTitle = "Invoice Amount",
							FormatType = FormatTypes.Amount,
							Editable = true,
							Id = new Guid(42, 42, 2, filler),
							MaxLength = 20,
							Required = true,
							Sequence = 1,
							Tooltip = "Invoice Amount",
							IsItemAmount = true,
							HasBusinessRules = false
						},
						new ColumnDefinition()
						{
							ColumnFieldName = "PostmarkDate",
							ColumnTitle = "Postmark Date",
							FormatType = FormatTypes.Date,
							Mask = "MM/dd/yyyy",
							Editable = true,
							Id = new Guid(42, 42, 3, filler),
							MaxLength = 10,
							Required = false,
							Sequence = 2,
							Tooltip = "Postmark Date",
							IsItemAmount = false,
							HasBusinessRules = false
						},
					}
		  };
          break;
      }

      // Mock Transactions
      switch (commonExceptionId)
      {
        case 1234:
          response.TransactionInfo.StubItems.Items = new PaymentItem[]
					{
						new PaymentItem()
						{
							Id = new Guid(1234, 1, 1, filler),
							IsException = true,
							FieldValues = new FieldDataAndInfo[0],
							Messages = new string[0],
						},
					};

          response.TransactionInfo.TransactionImages = new TransactionImageRef[]
					{
						new TransactionImageRef()
						{
							BatchSequence = 1
						},
					};
          break;

        case 1235:
		  response.TransactionInfo.StubItems.Items = new PaymentItem[]
					{
						new PaymentItem()
						{
							Id = new Guid(1235, 1, 1, filler),
							IsException = true,
							FieldValues = new FieldDataAndInfo[0],
							Messages = new string[0],
						},
					};

          response.TransactionInfo.TransactionImages = new TransactionImageRef[]
					{
						new TransactionImageRef()
						{
							BatchSequence = 2
						},
					};
          break;

        case 1236:
		  response.TransactionInfo.StubItems.Items = new PaymentItem[]
					{
						new PaymentItem()
						{
							Id = new Guid(1236, 1, 1, filler),
							IsException = true,
							FieldValues = new FieldDataAndInfo[0],
							Messages = new string[0],
						},
					};

          response.TransactionInfo.TransactionImages = new TransactionImageRef[]
					{
						new TransactionImageRef()
						{
							BatchSequence = 3
						},
					};
          break;

        case 4321:
		  response.TransactionInfo.StubItems.Items = new PaymentItem[]
					{
						new PaymentItem()
						{
							Id = new Guid(4321, 1, 1, filler),
							IsException = true,
							FieldValues = new FieldDataAndInfo[]
							{
								new FieldDataAndInfo()
								{
									ColumnDefId = new Guid(42, 42, 2, filler),
									FieldValue = "98765.43",
									AwaitingDecision = true,
								},
							},
							Messages = new List<string>(),
						},
					};

          response.TransactionInfo.TransactionImages = new TransactionImageRef[]
					{
						new TransactionImageRef()
						{
							BatchSequence = 1
						},
						new TransactionImageRef()
						{
							BatchSequence = 3
						},
						new TransactionImageRef()
						{
							BatchSequence = 2
						},
					};
          break;

        case 4322:
		  response.TransactionInfo.StubItems.Items = new PaymentItem[]
					{
						new PaymentItem()
						{
							Id = new Guid(4322, 1, 1, filler),
							IsException = true,
							FieldValues = new FieldDataAndInfo[]
							{
								new FieldDataAndInfo()
								{
									ColumnDefId = new Guid(42, 42, 2, filler),
									FieldValue = "100.42",
									AwaitingDecision = true,
								},
							},
							Messages = new List<string>(),
						},
						new PaymentItem()
						{
							Id = new Guid(4322, 2, 1, filler),
							IsException = false,
							FieldValues = new FieldDataAndInfo[]
							{
								new FieldDataAndInfo()
								{
									ColumnDefId = new Guid(42, 42, 1, filler),
									FieldValue = "5555-555",
									AwaitingDecision = false,
								},
								new FieldDataAndInfo()
								{
									ColumnDefId = new Guid(42, 42, 2, filler),
									FieldValue = "101.42",
									AwaitingDecision = false,
								},
								new FieldDataAndInfo()
								{
									ColumnDefId = new Guid(42, 42, 3, filler),
									FieldValue = "03/21/2007",
									AwaitingDecision = false,
								},
							},
							Messages = new List<string>(),
						},
					};

		  response.TransactionInfo.TransactionImages = new TransactionImageRef[]
					{
						new TransactionImageRef()
						{
							BatchSequence = 4
						},
						new TransactionImageRef()
						{
							BatchSequence = 5
						},
						new TransactionImageRef()
						{
							BatchSequence = 6
						},
					};
          break;
      }

	  foreach (var item in response.TransactionInfo.StubItems.Items)
      {
        // Generate messages to display in the UI...
        foreach (var field in item.FieldValues.Where(o => o.AwaitingDecision))
        {
          item.Messages.Add("Please review: " + response.TransactionInfo.StubItems.FieldDefinitions.First(o => o.Id == field.ColumnDefId).ColumnTitle);
        }
      }
      return response;
    }

    public TransactionDetailsResponse ViewTransaction(int commonExceptionId)
    {
      var checkout = CheckoutTransaction(commonExceptionId);
      return new TransactionDetailsResponse()
      {
        Errors = checkout.Errors,
        Status = checkout.Status,
        Transaction = checkout.Transaction,
        TransactionInfo = checkout.TransactionInfo,
      };
    }

	public CheckinResponse CheckinTransaction(int commonExceptionID, ExceptionAction transactionDecision, bool overrideRules, bool forceBalance, IEnumerable<PaymentItemUpdate> stubUpdates, IEnumerable<PaymentItemUpdate> paymentUpdates)
	{
      throw new NotImplementedException();
    }

    public BaseResponse UnlockTransaction(int commonExceptionId)
    {
      throw new NotImplementedException();
    }

    public ReturnItem ServeImage(CEImageRequest imageRequest)
    {
      ReturnItem retVal = new ReturnItem()
      {
        bRetVal = true,
        strrespXml = null,
      };
      try
      {
        var transaction = GetTransactionsPendingDecision().Transactions.First(o => o.CommonExceptionId == imageRequest.CommonExceptionID);

        Bitmap bmp = new Bitmap(1100, 450, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
        bmp.SetResolution(200, 200);
        bool noImage = false;
        using (Graphics g = Graphics.FromImage(bmp))
        {
          g.FillRectangle(Brushes.White, new Rectangle(0, 0, 1100, 450));
          switch (transaction.BatchId)
          {
            case 12:
              // Mock ACH
              g.DrawString(string.Format("Mock ACH Seq {0}", imageRequest.BatchSequence), SystemFonts.DialogFont, Brushes.Black, 50, 50);
              g.DrawString(imageRequest.IsBack ? "(Back)" : "(Front)", SystemFonts.DialogFont, Brushes.Black, 50, 250);
              break;

            case 22:
              if (imageRequest.BatchSequence == 1 || imageRequest.BatchSequence == 2 || imageRequest.BatchSequence == 4)
              {
                // Mock Check
                g.DrawString(string.Format("Mock Check Seq {0}", imageRequest.BatchSequence), SystemFonts.DialogFont, Brushes.Black, 50, 50);
                g.DrawString(imageRequest.IsBack ? "(Back)" : "(Front)", SystemFonts.DialogFont, Brushes.Black, 50, 250);
              }
              else
              {
                // Mock Stub
                noImage = true;
              }

              break;
          }
        }

        if (!noImage)
        {
          // Save mock image to return value stream
          MemoryStream ms = new MemoryStream();
          bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
          ms.Seek(0, SeekOrigin.Begin);
          retVal.data = ms;
        }
      }
      catch (Exception)
      {
        retVal.bRetVal = false;
      }
      return retVal;
    }

	public GenericResponse<DefaultBRErrorMessagesDto> GetDefaultBRErrorMessages()
	{
		throw new NotImplementedException();
	}

	public BaseResponse ProcessStagedTransactions()
	{
		throw new NotImplementedException();
	}

	private void Verify(bool valid)
    {
      if (!valid)
        throw new Exception("Data was not valid");
    }
  }
}
