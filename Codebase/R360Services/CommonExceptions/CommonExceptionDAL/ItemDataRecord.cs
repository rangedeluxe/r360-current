﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DAL
{
	public class ItemDataRecord
	{
		public string Amount;
		public string RT;
		public string Account;
		public string TransactionCode;
		public string Serial;
		public string RemitterName;

		internal static ItemDataRecord FromData(DataRow row)
		{
			return new ItemDataRecord()
			{
				Amount = ((decimal)row["Amount"]).ToString("0.00"),
				RT = row["RT"] == DBNull.Value ? null : (string)row["RT"],
				Account = row["Account"] == DBNull.Value ? null : (string)row["Account"],
				TransactionCode = row["TransactionCode"] == DBNull.Value ? null : (string)row["TransactionCode"],
				Serial = row["Serial"] == DBNull.Value ? null : (string)row["Serial"],
				RemitterName = row["RemitterName"] == DBNull.Value ? null : (string)row["RemitterName"]
			};
		}
	}
}
