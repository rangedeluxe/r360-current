﻿using System;
using System.Collections.Generic;
using WFS.RecHub.CommonException.Common;
namespace WFS.RecHub.DAL
{
	public interface ICommonExceptionDAL
	{
		//void BeginTrans();
		//void CommitTrans();
		//void RollbackTrans();

		//bool CheckoutTransaction(int commonExceptionID, int userID, out System.Data.DataTable dt);
		//bool GetCommonExceptions(int userId, out System.Data.DataTable dt);
		//bool GetUserIDBySID(Guid SID, out System.Data.DataTable results);
		//bool CheckinTransaction(int commonExceptionID, int userId, sbyte status, bool overrideAllLocks, out System.Data.DataTable dt);
		//bool VerifyTransactionCheckedOut(int commonExceptionID, int userID, out System.Data.DataTable dt);

		//bool GetBatchesPendingCompletion(out System.Data.DataTable dt);
		//bool MarkBatchComplete(int globalBatchId);

		bool GetRules(long dataEntrySetupKey, out RuleDefinitionsDto rulesDto);
		//bool GetCheckDigitReplacementValues(long cdKey, out Dictionary<char, int?> replacementValues);
		bool GetPaymentSubTypes(out List<PaymentSubTypeDto> paymentSubTypes);
		bool CheckForValidationValuesExistence(long fieldValidationKey, string fieldValue, out bool exists);
	}
}
