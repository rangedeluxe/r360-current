﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DAL
{
	internal class DataRecord
	{
		public long BatchID;
		public int TransactionID;
		public int BatchSequence;
		public object Data;

		internal int CompareKeys(DataRecord compareRec)
		{
			if (compareRec == null)
				return -1;
			else if (BatchID < compareRec.BatchID)
				return -1;
			else if (BatchID == compareRec.BatchID && TransactionID < compareRec.TransactionID)
				return -1;
			else if (BatchID == compareRec.BatchID && TransactionID == compareRec.TransactionID && BatchSequence < compareRec.BatchSequence)
				return -1;
			else if (BatchID == compareRec.BatchID && TransactionID == compareRec.TransactionID && BatchSequence == compareRec.BatchSequence)
				return 0;

			return 1;
		}

		internal static DataRecord LoadFromCheckRow(DataRow row)
		{
			var rec = LoadFromRow(row);
			rec.Data = row.ConvertToCheckItemDto();
			return rec;
		}

		internal static DataRecord LoadFromStubRow(DataRow row)
		{
			var rec = LoadFromRow(row);
			rec.Data = row.ConvertToStubItemDto();
			return rec;
		}

		internal static DataRecord LoadFromFieldRow(DataRow row)
		{
			var rec = LoadFromRow(row);
			rec.Data = row.ConvertToFieldDto();
			return rec;
		}

		private static DataRecord LoadFromRow(DataRow row)
		{
			return new DataRecord()
			{
				BatchID = (long)row["BatchID"],
				TransactionID = (int)row["TransactionID"],
				BatchSequence = (int)row["BatchSequence"]
			};
		}
	}
}
