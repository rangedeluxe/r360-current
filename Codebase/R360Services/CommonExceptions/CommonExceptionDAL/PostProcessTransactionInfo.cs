﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.DAL.CommonExceptions
{
	public class PostProcessTransactionInfo
	{
		public TransactionStatus CurrentStatus;
		public long DataEntrySetupKey;
		public int BatchSourceKey;
		public int BatchPaymentTypeKey;
		public List<ItemRecord> Payments;
		public List<ItemRecord> Stubs;
		public List<TransactionImageRef> ImageRefs;
		public bool IsTransactionException;
		public string Message;
	}
}
