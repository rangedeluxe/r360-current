﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.DAL.CommonExceptions;

namespace WFS.RecHub.DAL
{
	public static class CommonExceptionDALExtensions
	{
		public static FieldDefinitionDto ConvertToFieldDefinitionDto(this DataRow dr)
		{
			FieldDefinitionDto fieldDef = null;

			if (dr.Table.Columns.Count > 0)
			{
				fieldDef = new FieldDefinitionDto();
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "dataentrycolumnkey":
							fieldDef.DataEntryColumnKey = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "tabletype":
							fieldDef.TableType = (FieldTableTypes)ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "datatype":
							fieldDef.DataType = (FieldDataTypes)ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "fldlength":
							fieldDef.FieldLength = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "screenorder":
							fieldDef.ScreenOrder = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "fldname":
							fieldDef.FldName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "displayname":
							fieldDef.DisplayName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
					}
				}
			}
			return fieldDef;
		}

		public static BusinessRulesDto ConvertToBusinessRulesDto(this DataRow dr)
		{
			BusinessRulesDto rules = null;
			CheckDigitRoutineDto cdr = null;

			if (dr.Table.Columns.Count > 0)
			{
				rules = new BusinessRulesDto();
				cdr = new CheckDigitRoutineDto();
				cdr.Key = 0;
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "batchsourcekey":
							if (dr[dc.ColumnName] != DBNull.Value)
								rules.BatchSourceKey = (short)dr[dc.ColumnName];
							break;
						case "batchpaymenttypekey":
							if (dr[dc.ColumnName] != DBNull.Value)
								rules.BatchPaymentTypeKey = (byte)dr[dc.ColumnName];
							break;
						case "batchpaymentsubtypekey":
							if (dr[dc.ColumnName] != DBNull.Value)
								rules.BatchPaymentSubTypeKey = (byte)dr[dc.ColumnName];
							break;
						case "minlength":
							rules.MinLength = ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "maxlength":
							rules.MaxLength = ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "required":
							rules.Required = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "usercanedit":
							rules.UserCanEdit = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "formattype":
							rules.FormatType = (FormatTypes)ipoLib.NVL(ref dr, dc.ColumnName, (byte)1);
							break;
						case "mask":
							rules.Mask = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "requiredmessage":
							rules.RequiredMessage = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "checkdigitroutinekey":
							cdr.Key = dr.IsNull(dc.ColumnName) ? 0L : (long)dr[dc.ColumnName];
							break;
						case "offset":
							cdr.Offset = ipoLib.NVL(ref dr, dc.ColumnName, (short)0);
							break;
						case "method":
							cdr.Method = (CheckDigitMethod)ipoLib.NVL(ref dr, dc.ColumnName, (int)CheckDigitMethod.SumOfDigits);
							break;
						case "modulus":
							cdr.Modulus = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "compliment":
							cdr.Compliment = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "weightpatterndirection":
							cdr.WeightsDirection = (WeightPatternDirection)ipoLib.NVL(ref dr, dc.ColumnName, (int)WeightPatternDirection.LeftToRight);
							break;
						case "weightpatternvalue":
							cdr.Weights = ParseCommaSeparatedIntegers(ipoLib.NVL(ref dr, dc.ColumnName, string.Empty));
							break;
						case "ignorespaces":
							cdr.IgnoreSpaces = ipoLib.NVL(ref dr, dc.ColumnName, false);
							break;
						case "remainder10replacement":
							cdr.Rem10Replacement = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "remainder11replacement":
							cdr.Rem11Replacement = ipoLib.NVL(ref dr, dc.ColumnName, 0);
							break;
						case "checkdigitfailuremessage":
							cdr.FailureMessage = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;


						case "fieldvalidationkey":
							rules.FieldValidationKey = dr.IsNull(dc.ColumnName) ? 0L : (long)dr[dc.ColumnName];
							break;
						case "fieldvalidationfailuremessage":
							rules.FieldValidationFailureMessage = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
						case "fieldvalidationtypekey":
							rules.FieldValidationTypeKey = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
					}
				}

				if (cdr.Key != 0)
					rules.CheckDigitRoutine = cdr;

				if (rules.FormatType == FormatTypes.Date || rules.FormatType == FormatTypes.CustomDate)
				{
					//convert the custom date mask to have the proper .NET date components
					rules.Mask = rules.Mask.Replace('m', 'M').Replace('Y', 'y').Replace('D', 'd');
				}

			}
			return rules;
		}

		public static KeyValuePair<char, int?> ConvertToCDReplacementValue(this DataRow dr)
		{
			char key = '\0';
			int? value = null;

			if (dr.Table.Columns.Count > 0)
			{
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "originalvalue":
							key = ((string)dr[dc.ColumnName])[0];//not allowed to be null and better not be an empty string, that's not valid either
							break;
						case "replacementvalue":
							if (dr[dc.ColumnName] != DBNull.Value)
								value = (byte)dr[dc.ColumnName];
							break;
					}
				}
			}
			return new KeyValuePair<char, int?>(key, value);
		}

		public static PaymentSubTypeDto ConvertToPaymentSubTypeDto(this DataRow dr)
		{
			PaymentSubTypeDto subType = null;

			if (dr.Table.Columns.Count > 0)
			{
				subType = new PaymentSubTypeDto();
				foreach (DataColumn dc in dr.Table.Columns)
				{
					switch (dc.ColumnName.ToLower())
					{
						case "batchpaymentsubtypekey":
							subType.PaymentSubTypeKey = ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "batchpaymenttypekey":
							subType.PaymentType = (PaymentTypes)ipoLib.NVL(ref dr, dc.ColumnName, (byte)0);
							break;
						case "subtypedescription":
							subType.SubTypeDescription = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
							break;
					}
				}
			}
			return subType;
		}

		public static TransactionDto ConvertToTransactionDto(this DataRow dr)
		{
			TransactionDto transaction = new TransactionDto();
			transaction.BatchID = (long)dr["BatchID"];
			transaction.TransactionID = (int)dr["TransactionID"];
			transaction.BatchSourceKey = (Int16)dr["BatchSourceKey"];
			transaction.BatchPaymentTypeKey = (PaymentTypes)(byte)dr["BatchPaymentTypeKey"];
			transaction.DataEntrySetupKey = (long)dr["DataEntrySetupKey"];
			transaction.Items = new List<ItemDto>();
			return transaction;
		}

		public static ItemDto ConvertToCheckItemDto(this DataRow dr)
		{
			ItemDto item = new ItemDto();
			item.BatchID = (long)dr["BatchID"];
			item.TransactionID = (int)dr["TransactionID"];
			item.BatchSequence = (int)dr["BatchSequence"];
			item.IsPayment = true;
			item.IsException = false;
			item.ItemData = new ItemDataDto();
			item.ItemData.Amount = Convert.ToString(dr["Amount"]);
			item.ItemData.RT = Convert.ToString(dr["RoutingNumber"]);
			item.ItemData.Account = Convert.ToString(dr["Account"]);
			if (dr["Serial"] != DBNull.Value)
				item.ItemData.Serial = Convert.ToString(dr["Serial"]);
			if (dr["RemitterName"] != DBNull.Value)
				item.ItemData.RemitterName = Convert.ToString(dr["RemitterName"]);
			
			item.Fields = new List<FieldDto>();

			item.Messages = null;
			return item;
		}

		public static ItemDto ConvertToStubItemDto(this DataRow dr)
		{
			ItemDto item = new ItemDto();
			item.BatchID = (long)dr["BatchID"];
			item.TransactionID = (int)dr["TransactionID"];
			item.BatchSequence = (int)dr["BatchSequence"];
			item.IsPayment = false;
			item.IsException = false;
			item.Fields = new List<FieldDto>();
			item.Fields.Add(new FieldDto() { DataEntryColumnKey = null, FieldName = "Amount", Value = Convert.ToString(dr["Amount"]) });
			if (dr["AccountNumber"] != DBNull.Value)
				item.Fields.Add(new FieldDto() { DataEntryColumnKey = null, FieldName = "AccountNumber", Value = Convert.ToString(dr["AccountNumber"]) });

			item.Messages = null;
			return item;
		}

		public static FieldDto ConvertToFieldDto(this DataRow dr)
		{
			FieldDto field = new FieldDto();
			field.DataEntryColumnKey = (int)dr["DataEntryColumnKey"];
			field.FieldName = null;
			if (dr["DataEntryValue"] != DBNull.Value)
				field.Value = Convert.ToString(dr["DataEntryValue"]);
			else if (dr["DataEntryValueMoney"] != DBNull.Value)
				field.Value = Convert.ToString(dr["DataEntryValueMoney"]);
			if (dr["DataEntryValueFloat"] != DBNull.Value)
				field.Value = Convert.ToString(dr["DataEntryValueFloat"]);
			else if (dr["DataEntryValueDateTime"] != DBNull.Value)
			{
				field.DateTimeValue = (DateTime)dr["DataEntryValueDateTime"];
				field.Value = null;//we'll convert later
			}
			return field;
		}

		public static PostProcessTransactionInfo ConvertToPostProcessTransactionInfo(this DataRow dr, DataRowCollection payments, DataRowCollection paymentDataEntry, DataRowCollection stubDataEntry)
		{
			var ppt = new PostProcessTransactionInfo();

			ppt.CurrentStatus = (TransactionStatus)(short)dr["TransactionStatus"];
			ppt.DataEntrySetupKey = (long)dr["DataEntrySetupKey"];
			ppt.BatchSourceKey = (short)dr["BatchSourceKey"];
			ppt.BatchPaymentTypeKey = (int)(byte)dr["BatchPaymentTypeKey"];
			ppt.Payments = paymentDataEntry == null ? null : ItemRecord.FromData(payments, paymentDataEntry);
			ppt.Stubs = stubDataEntry == null ? null : ItemRecord.FromData(null/*standard fields are translated to DE already*/, stubDataEntry);
			
			return ppt;
		}

		public static TransactionImageRef ConvertToTransactionImageRef(this DataRow dr)
		{
			TransactionImageRef imageRef = new TransactionImageRef();
			imageRef.FileDescriptor = (string)dr["FileDescriptor"];
			imageRef.BatchSequence = (int)dr["BatchSequence"];
			return imageRef;
		}

		#region CSV Helper
		private static int[] ParseCommaSeparatedIntegers(string values)
		{
			if (values == null || values.Length == 0)
				return null;

			return values.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(o => int.Parse(o)).ToArray();
		}
		#endregion
	}
}
