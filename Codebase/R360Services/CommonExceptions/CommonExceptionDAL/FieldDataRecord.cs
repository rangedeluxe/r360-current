﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DAL
{
	public class FieldDataRecord
	{
		public long DataEntryColumnKey { get; set; }
		public DateTime DateTimeValue { get; set; }
		public string Value { get; set; }

		internal static FieldDataRecord FromData(DataRow row)
		{
			DateTime dtValue = DateTime.MinValue;
			string value = null;
			object rowVal = row["FieldValue"];
			if (rowVal is DateTime)//DateTime gets treated special, leaving the value null, value will be set by BRE.
				dtValue = (DateTime)row["FieldValue"];
			else if (rowVal is decimal)//money translates to decimal
				value = ((decimal)rowVal).ToString("0.00");
			else if (rowVal is double)//float translates to double
				value = ((double)rowVal).ToString();
			else//everything else
				value = Convert.ToString(rowVal);

			return new FieldDataRecord()
				{
					DataEntryColumnKey = Convert.ToInt64(row["DataEntryColumnKey"]),
					DateTimeValue = dtValue,
					Value = value,
				};
		}
	}
}
