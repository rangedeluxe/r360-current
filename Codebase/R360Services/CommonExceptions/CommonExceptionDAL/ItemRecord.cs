﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.DAL.CommonExceptions
{
	public class ItemRecord
	{
		public int BatchSequence { get; set; }
		public ExceptionItemAction Action { get; set; }
		public ItemDataRecord ItemInfo { get; set; }

		private List<FieldDataRecord> _fields = new List<FieldDataRecord>();
		public List<FieldDataRecord> Fields
		{
			get { return _fields; }
			set { _fields = value; }
		}

		internal static List<ItemRecord> FromData(DataRowCollection itemRows, DataRowCollection fieldRows)
		{
			List<ItemRecord> result = new List<ItemRecord>();

			if (itemRows != null)
			{
				foreach (DataRow itemRow in itemRows)
				{
					result.Add(new ItemRecord()
					{
						BatchSequence = Convert.ToInt32(itemRow["BatchSequence"]),
						ItemInfo = ItemDataRecord.FromData(itemRow)
					});
				}
			}

			ItemRecord curItem = null;
			int curItemIndex = 0;
			foreach (DataRow fieldRow in fieldRows)
			{
				var fieldBatchSequence = Convert.ToInt32(fieldRow["BatchSequence"]);

				//two scenarios
				// 1 - we have item data and have already created the ItemRecords (if condition)
				// 2 - we have no item data and must create the ItemRecords (else if condition)

				if (itemRows != null)
				{
					if (curItem == null || curItem.BatchSequence != fieldBatchSequence)
					{
						//for efficiency, we'll forward read assuming things are in order coming from the database
						while (curItemIndex < result.Count && result[curItemIndex].BatchSequence != fieldBatchSequence)
							curItemIndex++;

						if (curItemIndex >= result.Count)
						{
							//likely something is out of order, but that's not possible unless someone messed up the stored procedure
							// which would not surprise me, so protect against it
							curItemIndex = result.FindIndex(o => o.BatchSequence == fieldBatchSequence);
							if (curItemIndex < 0)
								throw new Exception("PaymentDataEntry record(s) encountered that do not have a corresponding Payment record, batch sequence " + fieldBatchSequence.ToString());
						}

						curItem = result[curItemIndex];
					}
				}
				else if (curItem == null || curItem.BatchSequence != fieldBatchSequence)
				{
					curItem = new ItemRecord()
					{
						BatchSequence = fieldBatchSequence,
						ItemInfo = null
					};
					result.Add(curItem);
				}

				curItem.Fields.Add(FieldDataRecord.FromData(fieldRow));
			}

			return result;
		}
	}

	public static class ItemUpdate
	{
		public static string ToXml(IList<ItemRecord> updates)
		{
			bool hasChanges = false;
			StringBuilder xmlUpdates = new StringBuilder();
			using (XmlWriter xw = XmlWriter.Create(xmlUpdates, new XmlWriterSettings() { OmitXmlDeclaration = true }))
			{
				xw.WriteStartElement("Updates");
				for (int i = 0; i < updates.Count; i++)
				{
					var update = updates[i];
					//for now we only support updated items, no deletes or adds
					if (update.Action != ExceptionItemAction.NoChange)
					{
						// Item
						xw.WriteStartElement("I");
						xw.WriteAttributeString("TmpId", i.ToString());
						xw.WriteAttributeString("Seq", update.BatchSequence.ToString());
						xw.WriteAttributeString("Act", ((int)update.Action).ToString());

						// Dynamic Fields
						if (update.Fields != null && update.Fields.Count > 0 && update.Action != ExceptionItemAction.DeleteItem)
						{
							foreach (var field in update.Fields)
							{
								xw.WriteStartElement("F");
								xw.WriteAttributeString("ID", field.DataEntryColumnKey.ToString());
								if (field.DateTimeValue != DateTime.MinValue)
									xw.WriteAttributeString("DateVal", field.DateTimeValue.ToString(ServiceConstants.DateTimeFormats.InvariantDateFormat));
								else
									xw.WriteAttributeString("Val", field.Value);
								xw.WriteEndElement();	// </F>
							}
						}
						hasChanges = true;

						xw.WriteEndElement();	// </I>
					}
				}
				xw.WriteEndElement();	// </Updates>
				xw.Flush();
			}
			return hasChanges ? xmlUpdates.ToString() : null;
		}
	}
}
