﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DAL
{
	/// <summary>
	/// This class deals with "predictable" mock data for use in testing
	/// </summary>
	public class MockCommonExceptionDAL : ICommonExceptionDAL
	{
		public void BeginTrans() { }
		public void CommitTrans() { }
		public void RollbackTrans() { }

		public bool GetUserIDBySID(Guid SID, out DataTable results)
		{
			results = new DataTable();

			results.Columns.Add("UserID");

			var row = results.NewRow();
			row["UserID"] = 42;

			results.Rows.Add(row);
			return true;
		}

		public bool GetCommonExceptions(int userId, out DataTable dt)
		{
			dt = new DataTable();

			dt.Columns.Add("CommonExceptionID");
			dt.Columns.Add("SiteBankID");
			dt.Columns.Add("SiteClientAccountID");
			dt.Columns.Add("DepositDateKey");
			dt.Columns.Add("BatchID");
			dt.Columns.Add("ImmutableDateKey");
			dt.Columns.Add("CheckAmount");
			dt.Columns.Add("Deadline");
			dt.Columns.Add("InProcessException");
			dt.Columns.Add("DecisioningStatus");
			dt.Columns.Add("BatchPaymentTypeKey");
			dt.Columns.Add("LongName"); // Batch Payment Type
			dt.Columns.Add("Locked");
			dt.Columns.Add("LockTime");

			var row = dt.NewRow();
			row["CommonExceptionID"] = 1234;
			row["SiteBankID"] = 1;
			row["SiteClientAccountID"] = 123;
			row["BatchID"] = 12;
			row["DepositDateKey"] = int.Parse(DateTime.Today.ToString("yyyyMMdd"));
			row["ImmutableDateKey"] = int.Parse(DateTime.Today.ToString("yyyyMMdd"));
			row["CheckAmount"] = 0m;
			row["Deadline"] = DateTime.Now;
			row["InProcessException"] = (byte)1;
			row["DecisioningStatus"] = (byte)1;
			row["BatchPaymentTypeKey"] = 1;
			row["Locked"] = 0;
			row["LockTime"] = DateTime.MinValue;
			dt.Rows.Add(row);

			row = dt.NewRow();
			row["CommonExceptionID"] = 1236;
			row["SiteBankID"] = 1;
			row["SiteClientAccountID"] = 123;
			row["BatchID"] = 12;
			row["DepositDateKey"] = int.Parse(DateTime.Today.ToString("yyyyMMdd"));
			row["ImmutableDateKey"] = int.Parse(DateTime.Today.ToString("yyyyMMdd"));
			row["CheckAmount"] = 0m;
			row["Deadline"] = DateTime.Now;
			row["InProcessException"] = (byte)1;
			row["DecisioningStatus"] = (byte)1;
			row["BatchPaymentTypeKey"] = 1;
			row["Locked"] = 0;
			row["LockTime"] = DateTime.MinValue;
			dt.Rows.Add(row);

			return true;
		}

		public bool CheckoutTransaction(int commonExceptionID, int userID, out DataTable dt)
		{
			return MockOLBatchId("LockCreated", commonExceptionID, out dt);
		}

		public bool VerifyTransactionCheckedOut(int commonExceptionID, int userID, out DataTable dt)
		{
			return MockOLBatchId("Locked", commonExceptionID, out dt);
		}

		private bool MockOLBatchId(string lockFieldName, int commonExceptionID, out DataTable dt)
		{
			dt = new DataTable();

			dt.Columns.Add("GlobalBatchID");
			dt.Columns.Add("TransactionID");
			dt.Columns.Add(lockFieldName);

			var row = dt.NewRow();
			row["GlobalBatchID"] = 1217016778;	// Picked this to match the data in OLDecisioning...
			row["TransactionID"] = commonExceptionID - 1233;
			row[lockFieldName] = true;

			dt.Rows.Add(row);
			return true;
		}

		public bool CheckinTransaction(int commonExceptionID, int userId, sbyte status, bool overrideLocks, out System.Data.DataTable dt)
		{
			throw new NotImplementedException();
		}

		public bool GetBatchesPendingCompletion(out DataTable dt)
		{
			throw new NotImplementedException();
		}

		public bool MarkBatchComplete(int globalBatchId)
		{
			throw new NotImplementedException();
		}
	}
}
