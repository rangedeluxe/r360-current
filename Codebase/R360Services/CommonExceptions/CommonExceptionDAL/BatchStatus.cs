﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DAL.CommonExceptions
{
	public enum BatchStatus
	{
		Unresolved = 1,
		Pending,
		Resolved = 3,
	}
}
