﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.DAL.CommonExceptions;
using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Wayne Schwarz
* Date:     06/04/2013
*
* Purpose:  
*
* WI 114937 TWE 09/25/2013
*   - pass Invoice Balancing Option to OLDecisioning service.
******************************************************************************/
namespace WFS.RecHub.DAL
{
	public class CommonExceptionDAL : _DALBase, ICommonExceptionDAL
	{
		public CommonExceptionDAL(string vSiteKey)
			: base(vSiteKey, ConnectionType.CommonException)
		{
		}

		public bool GetUserIDBySID(Guid SID, out DataTable results)
		{
			bool bReturnValue = false;

			results = null;
			SqlParameter[] parms;
			try
			{
				parms = new SqlParameter[] {
                    BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input)
                };
				bReturnValue = Database.executeProcedure("RecHubUser.usp_Users_UserID_Get_BySID",
						parms, out results);
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "GetUserIDBySID(Guid SID, out DataTable results)");
				bReturnValue = false;
			}

			return bReturnValue;
		}

		public bool GetCommonExceptions(int userId, Guid sessionID, out DataTable dt)
		{
			bool bRtnval = false;
			List<SqlParameter> lstParms = new List<SqlParameter>();
			const string PROCNAME = "RecHubException.usp_CommonExceptions_Get";
			dt = null;
			try
			{
				lstParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input));
				bRtnval = Database.executeProcedure(PROCNAME, lstParms.ToArray(), out dt);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		public bool GetCommonException(int userId, Guid sessionID, int commonExceptionId, out DataTable dt)
		{
			bool bRtnval = false;
			List<SqlParameter> lstParms = new List<SqlParameter>();
			const string PROCNAME = "RecHubException.usp_CommonExceptions_Get_ByCommonExceptionID";
			dt = null;
			try
			{
				lstParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input));
				lstParms.Add(BuildParameter("@parmCommonExceptionID", SqlDbType.Int, commonExceptionId, ParameterDirection.Input));

				bRtnval = Database.executeProcedure(PROCNAME, lstParms.ToArray(), out dt);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}
		
		public bool CheckoutTransaction(int commonExceptionID, int userID, Guid sessionId, out DataTable dt)
		{
			bool bRtnval = false;
			SqlParameter[] parms;
			const string PROCNAME = "RecHubException.usp_CommonExceptions_CheckoutTransaction";
			dt = null;
			try
			{
				List<SqlParameter> arParms;
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmCommonExceptionID", SqlDbType.Int, commonExceptionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionId, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		public bool CheckinTransaction(int commonExceptionID, int userID, byte status, bool overrideAllLocks, bool overrideRules, bool forceBalance, out DataTable dt)
		{
			bool bRtnval = false;
			SqlParameter[] parms;
			const string PROCNAME = "RecHubException.usp_CommonExceptions_CheckinTransaction";
			dt = null;
			try
			{
				List<SqlParameter> arParms;
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmCommonExceptionID", SqlDbType.Int, commonExceptionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmStatus", SqlDbType.TinyInt, status, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmOverrideAllLocks", SqlDbType.Bit, overrideAllLocks, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmOverrideRules", SqlDbType.Bit, overrideRules, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmForceBalance", SqlDbType.Bit, forceBalance, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		public bool VerifyTransactionCheckedOut(int commonExceptionID, int userID, out DataTable dt)
		{
			bool bRtnval = false;
			SqlParameter[] parms;
			const string PROCNAME = "RecHubException.usp_CommonExceptions_VerifyTransactionCheckout";
			dt = null;
			try
			{
				List<SqlParameter> arParms;
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmCommonExceptionID", SqlDbType.Int, commonExceptionID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		public bool GetBatchesPendingCompletion(out DataTable dt)
		{
			bool bRtnval = false;
			const string PROCNAME = "RecHubException.usp_CommonExceptions_Get_BatchesPendingCompletion";
			dt = null;
			try
			{
				bRtnval = Database.executeProcedure(PROCNAME, out dt);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		public bool MarkIntegraPAYBatchComplete(int globalBatchId)
		{
			bool bRtnval = false;
			const string PROCNAME = "RecHubException.usp_IntegraPAYExceptions_Upd_BatchComplete";
			try
			{
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmGlobalBatchID", SqlDbType.Int, globalBatchId, ParameterDirection.Input));
				SqlParameter[] parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		public bool MarkPostProcessBatchComplete(long batchKey)
		{
			bool bRtnval = false;
			const string PROCNAME = "RecHubException.usp_ExceptionBatches_Upd_BatchStatus";
			try
			{
				SqlParameter[] parms = new SqlParameter[] 
				{
					BuildParameter("@parmExceptionBatchKey", SqlDbType.BigInt, batchKey, ParameterDirection.Input),
					BuildParameter("@parmBatchStatusKey", SqlDbType.SmallInt, (int)BatchStatus.Pending, ParameterDirection.Input)
				};
				bRtnval = Database.executeProcedure(PROCNAME, parms);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		public bool WriteAuditEvent(string auditEvent, int userID, string description)
		{
			bool bRtnval = false;
			const string PROCNAME = "RecHubCommon.usp_WFS_EventAudit_Ins";
			try
			{
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmApplicationName", SqlDbType.VarChar, "CommonExceptions", ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, auditEvent, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmAuditMessage", SqlDbType.VarChar, description, ParameterDirection.Input));
				SqlParameter[] parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms);
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		private bool ExecuteProc(string procName, int commonExceptionID, int userID, out DataTable data)
		{
			try
			{
				// Parameter is the common exeption ID
				SqlParameter[] arParms = new SqlParameter[] { 
					BuildParameter("@parmCommonExceptionID", SqlDbType.Int, commonExceptionID, ParameterDirection.Input),
					BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input),
				};

				// Execute procedure, and throw excpetion (for logging) if it failed...
				if (!Database.executeProcedure(procName, arParms, out data))
					throw new Exception("(See Previous Error Report)");
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + procName, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				data = null;
				return false;
			}

			return true;
		}

		public bool GetTransactionInformation(int commonExceptionID, int userID, out PostProcessTransactionInfo transactionInfo)
		{
			// Default to no result
			transactionInfo = null;

			// Get Transaction row
			DataTable dtTransactions;
			if (!ExecuteProc("RecHubException.usp_Transactions_Get_ByCommonExceptionID", commonExceptionID, userID, out dtTransactions)) 
				return false;

			if (dtTransactions.Rows.Count < 1)
				throw new Exception(string.Format("Common Exception ID {0} did not contain post process transaction information", commonExceptionID));

			// Get Payment Items
			DataTable dtPayments;
			if (!ExecuteProc("RecHubException.usp_Payments_Get_ByCommonExceptionID", commonExceptionID, userID, out dtPayments))
				return false;

			DataTable dtPaymentDataEntry;
			if (!ExecuteProc("RecHubException.usp_PaymentDataEntry_Get_ByCommonExceptionID", commonExceptionID, userID, out dtPaymentDataEntry))
				return false;

			// Get Stub Items
			DataTable dtStubs;
			if (!ExecuteProc("RecHubException.usp_Stubs_Get_ByCommonExceptionID", commonExceptionID, userID, out dtStubs))
				return false;

			// Convert to object
			transactionInfo = dtTransactions.Rows[0].ConvertToPostProcessTransactionInfo(dtPayments.Rows, dtPaymentDataEntry.Rows, dtStubs.Rows);

			return true;
		}

		private bool ExecuteUpdateProc(string procName, int commonExceptionID, int userID, string xmlUpdates)
		{
			try
			{
				// Parameters are the common exeption ID, and the updates (in XML)
				SqlParameter[] arParms = new SqlParameter[] 
				{
					BuildParameter("@parmCommonExceptionID", SqlDbType.Int, commonExceptionID, ParameterDirection.Input),
					BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input),
					BuildParameter("@parmXmlUpdates", SqlDbType.Xml, xmlUpdates, ParameterDirection.Input)
				};

				// Execute procedure, and throw excpetion (for logging) if it failed...
				if (!Database.executeProcedure(procName, arParms))
					throw new Exception("(See Previous Error Report)");
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: " + procName, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				return false;
			}

			return true;
		}

		public bool UpdateTransaction(int userID, int commonExceptionID, List<ItemRecord> paymentUpdates, List<ItemRecord> stubUpdates)
		{
			if (paymentUpdates != null && paymentUpdates.Count > 0)
			{
				string updates = ItemUpdate.ToXml(paymentUpdates);
				if (updates != null && !ExecuteUpdateProc("RecHubException.usp_Payments_InsUpd_ByCommonExceptionID", commonExceptionID, userID, updates))
					return false;
			}

			if (stubUpdates != null && stubUpdates.Count > 0)
			{
				string updates = ItemUpdate.ToXml(stubUpdates);
				if (updates != null && !ExecuteUpdateProc("RecHubException.usp_Stubs_InsUpd_ByCommonExceptionID", commonExceptionID, userID, updates))
					return false;
			}

			return true;
		}

		public bool GetRules(long dataEntrySetupKey, out RuleDefinitionsDto rulesDto)
		{
			rulesDto = null;

			DataTable dt;

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmDataEntrySetupKey", SqlDbType.BigInt, dataEntrySetupKey, ParameterDirection.Input));

				var parmRequiresInvoice = BuildParameter("@parmRequiresInvoice", SqlDbType.Bit, DBNull.Value, ParameterDirection.Output);
				var parmBalancingOption = BuildParameter("@parmBalancingOption", SqlDbType.TinyInt, DBNull.Value, ParameterDirection.Output);

				parms.Add(parmRequiresInvoice);
				parms.Add(parmBalancingOption);

				if (Database.executeProcedure("RecHubException.usp_BusinessRules_Get_ByDataEntrySetupKey", parms.ToArray(), out dt))
				{
					rulesDto = new RuleDefinitionsDto();
					rulesDto.TransactionRequiresInvoice = parmRequiresInvoice.Value != DBNull.Value && (bool)parmRequiresInvoice.Value;
					rulesDto.BalancingOption = parmBalancingOption.Value == DBNull.Value ? InvoiceBalancingOptions.BalancingNotRequired : (InvoiceBalancingOptions)(byte)parmBalancingOption.Value;

					if (dt != null)
					{
						//Convert to DTO Type
						rulesDto.FieldDefinitions = new List<FieldDefinitionDto>();
						FieldDefinitionDto fieldDef = null;
						foreach (DataRow row in dt.Rows)
						{
							int dataEntryColumnKey = (int)row["DataEntryColumnKey"];
							if (fieldDef == null || fieldDef.DataEntryColumnKey != dataEntryColumnKey)
							{
								fieldDef = row.ConvertToFieldDefinitionDto();
								rulesDto.FieldDefinitions.Add(fieldDef);
							}

							if (row["BusinessRuleKey"] != DBNull.Value)
							{
								BusinessRulesDto br = row.ConvertToBusinessRulesDto();
								if (br.CheckDigitRoutine != null)
								{
									//get the check digit replacement values
									Dictionary<char, int?> replacementValues;
									if (!GetCheckDigitReplacementValues(br.CheckDigitRoutine.Key, out replacementValues))
										return false;
									br.CheckDigitRoutine.ReplacementValues = replacementValues;
								}

                            if (fieldDef != null)
                              {
                                if (fieldDef.BusinessRules == null)
                                  {
                                    fieldDef.BusinessRules = new List<BusinessRulesDto>();
                                  }                            
                               fieldDef.BusinessRules.Add(br);
                              }
							}
						}
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().ToString(), "GetDataEntrySetup");
			}
			return false;
		}

		public bool GetCheckDigitReplacementValues(long cdKey, out Dictionary<char, int?> replacementValues)
		{
			replacementValues = null;

			DataTable dt;	

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmCheckDigitRoutineKey", SqlDbType.BigInt, cdKey, ParameterDirection.Input));

				if (Database.executeProcedure("RecHubException.usp_CheckDigitReplacementValues_Get", parms.ToArray(), out dt))
				{
					replacementValues = new Dictionary<char, int?>();

					//Convert to DTO Type
					foreach (DataRow row in dt.Rows)
					{
						KeyValuePair<char, int?> pair = row.ConvertToCDReplacementValue();
						replacementValues.Add(pair.Key, pair.Value);
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().ToString(), "GetCheckDigitReplacementValues");
			}
			return false;
		}

		public bool GetPaymentSubTypes(out List<PaymentSubTypeDto> paymentSubTypes)
		{
			paymentSubTypes = null;

			DataTable dt;

			try
			{
				if (Database.executeProcedure("RecHubData.usp_dimBatchPaymentSubTypes_Get", out dt))
				{
					//Convert to DTO Type
					paymentSubTypes = new List<PaymentSubTypeDto>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows)
					{
						paymentSubTypes.Add(row.ConvertToPaymentSubTypeDto());
					}
					return true;
				}
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().ToString(), "GetPaymentSubTypes");
			}
			return false;
		}

		public bool CheckForValidationValuesExistence(long fieldValidationKey, string fieldValue, out bool exists)
		{
			exists = false;

			try
			{
				List<SqlParameter> parms = new List<SqlParameter>();
				parms.Add(BuildParameter("@parmFieldValidationKey", SqlDbType.BigInt, fieldValidationKey, ParameterDirection.Input));
				parms.Add(BuildParameter("@parmFieldValue", SqlDbType.VarChar, fieldValue, ParameterDirection.Input));

				var parmValueExists = BuildParameter("@parmValueExists", SqlDbType.Bit, DBNull.Value, ParameterDirection.Output);

				parms.Add(parmValueExists);

				if (Database.executeProcedure("RecHubException.usp_FieldValidationValues_ValidateValue", parms.ToArray()))
				{
					exists = parmValueExists.Value != DBNull.Value && (bool)parmValueExists.Value;
					return true;
				}
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().ToString(), "ValidateFieldValue");
			}
			return false;
		}

		public bool GetStagedTransactions(out List<TransactionDto> transactions)
		{
			transactions = null;

			DataTable transDT;
			DataTable checksDT;
			DataTable stubsDT;
			DataTable columnsDT;

			try
			{
				if (!Database.executeProcedure("DITStaging.usp_factTransactionSummary_UpGet_ForBusinessRules", out transDT))
					return false;

				//short circuit if we have no transactions, no need to check for items or fields
				if (transDT.Rows.Count == 0)
					return true;

				if (!Database.executeProcedure("DITStaging.usp_factChecks_Get_ForValidation", out checksDT))
					return false;

				if (!Database.executeProcedure("DITStaging.usp_factStubs_Get_ForValidation", out stubsDT))
					return false;

				if (!Database.executeProcedure("DITStaging.usp_factDataEntryDetails_Get_ForValidation", out columnsDT))
					return false;

				DataRecord curCheck = null;
				DataRecord curStub = null; 
				DataRecord curField = null;
				DataRecord nextItem = null;
				int checkIndex = 0;
				int stubIndex = 0;
				int fieldIndex = 0;

				//Convert to DTO Type
				transactions = new List<TransactionDto>(transDT.Rows.Count);
				for (int tranIndex = 0; tranIndex < transDT.Rows.Count; tranIndex++)
				{
					//setup the transaction
					var tran = transDT.Rows[tranIndex].ConvertToTransactionDto();
					transactions.Add(tran);

					//keep adding the next item in order until we have every one for this transaction
					bool checkForItems = true;
					while (checkForItems)
					{
						//find the next item in the transaction
						if (nextItem == null)
						{
							//figure out the next item
							if (curCheck == null && checkIndex < checksDT.Rows.Count)
							{
								curCheck = DataRecord.LoadFromCheckRow(checksDT.Rows[checkIndex]);
								checkIndex++;
							}

							if (curStub == null && stubIndex < stubsDT.Rows.Count)
							{
								curStub = DataRecord.LoadFromStubRow(stubsDT.Rows[stubIndex]);
								stubIndex++;
							}

							//pick which item is next
							if (curStub == null || (curCheck != null && curCheck.CompareKeys(curStub) < 0))
							{
								nextItem = curCheck;
								curCheck = null;
							}
							else
							{
								nextItem = curStub;
								curStub = null;
							}
						}

						if (nextItem == null)
							checkForItems = false;
						else if (nextItem.BatchID == tran.BatchID && nextItem.TransactionID == tran.TransactionID)
						{
							var item = (ItemDto)nextItem.Data;
							tran.Items.Add(item);

							//get any fields for the item
							bool checkForFields = true;
							while (checkForFields)
							{
								if (curField == null && fieldIndex < columnsDT.Rows.Count)
								{
									curField = DataRecord.LoadFromFieldRow(columnsDT.Rows[fieldIndex]);
									fieldIndex++;
								}

								if (curField == null)
									checkForFields = false;
								else
								{
									int compare = curField.CompareKeys(nextItem);
									if (compare == 0)
									{
										item.Fields.Add((FieldDto)curField.Data);
										curField = null;
									}
									else if (compare < 0)
										throw new Exception("Data entry detail record encountered with no matching Check/Stub record or data is out of order");
									else
										checkForFields = false;//we are past fields for this item
								}
							}

							nextItem = null;
						}
						else if (nextItem.BatchID < tran.BatchID || (nextItem.BatchID == tran.BatchID && nextItem.TransactionID < tran.TransactionID))
							throw new Exception("Payment or Stubs record encountered with no matching Transaction or data is out of order");
						else
							checkForItems = false;//we are past items for this transaction
					}
				}

				return true;
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().ToString(), "GetStagedTransactions");
			}
			return false;
		}

		public bool SetStagedTransactionsExceptionState(TransactionDto transaction, bool isException)
		{
			SqlParameter[] parms;
			try
			{
				parms = new SqlParameter[]
				{
					BuildParameter("@parmBatchID", SqlDbType.BigInt, transaction.BatchID, ParameterDirection.Input),
					BuildParameter("@parmTransactionID", SqlDbType.Int, transaction.TransactionID, ParameterDirection.Input),
					BuildParameter("@parmException", SqlDbType.Bit, isException, ParameterDirection.Input)
				};

				if (Database.executeProcedure("DITStaging.usp_factTransactionSummary_Upd_Exception", parms))
					return true;
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().ToString(), "SetStagedTransactionsExceptionState");
			}
			return false;
		}

		public bool GetTransactionDocuments(Guid sessionID, TransactionModel tranInfo, out List<TransactionImageRef> documentImageRefs)
		{
			DataTable dt;
			documentImageRefs = null;
			try
			{
				// Parameters are the common exeption ID, and the updates (in XML)
				SqlParameter[] arParms = new SqlParameter[] 
				{
					BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionID, ParameterDirection.Input),
					BuildParameter("@parmSiteBankID", SqlDbType.Int, tranInfo.BankId, ParameterDirection.Input),
					BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, tranInfo.ClientAccountID, ParameterDirection.Input),
					BuildParameter("@parmDepositDate", SqlDbType.DateTime, DateTime.ParseExact(tranInfo.DepositDate, ServiceConstants.DateTimeFormats.DefaultDateFormat, System.Globalization.CultureInfo.InvariantCulture), ParameterDirection.Input),
					BuildParameter("@parmBatchID", SqlDbType.BigInt, tranInfo.BatchId, ParameterDirection.Input),
					BuildParameter("@parmTransactionID", SqlDbType.Int, tranInfo.TransactionID, ParameterDirection.Input),
					BuildParameter("@parmDisplayScannedCheck", SqlDbType.Int, 0/*best guess for now*/, ParameterDirection.Input)
				};

				// Execute procedure, and throw excpetion (for logging) if it failed...
				if (!Database.executeProcedure("RecHubData.usp_factDocuments_Get_ByTransactionID", arParms, out dt))
					throw new Exception("(See Previous Error Report)");

				documentImageRefs = new List<TransactionImageRef>(dt.Rows.Count);
				foreach (DataRow row in dt.Rows)
				{
					documentImageRefs.Add(row.ConvertToTransactionImageRef());
				}
			}
			catch (Exception ex)
			{
				EventLog.logEvent("Unable to execute procedure: RecHubData.usp_factDocuments_Get_ByTransactionID", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				return false;
			}
			return true;
		}
	}
}
