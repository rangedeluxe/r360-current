﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.DAL.CommonExceptions
{
	public enum TransactionStatus
	{
		NoException = 0,
		Unresolved = 1,
		Accepted = 2,
		Rejected = 3,
	}
}
