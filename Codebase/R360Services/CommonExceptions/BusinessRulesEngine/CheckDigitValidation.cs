﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.BusinessRules
{
	internal static class CheckDigitValidation
	{
		internal static void ValidateCheckDigit(CheckDigitRoutineDto routine, FieldDto field)
		{
			int i;
			int w;

			if (field == null || string.IsNullOrEmpty(field.Value))
				return;//It is not the check digit routine's job to ensure something was entered

			if (routine == null)
				return;

			if (routine.Offset == 0)
			{
				field.IsException = true;
				field.ExceptionMessage = ErrorMessages.CDInvalidOffset;
				return;
			}

			int cdPos = routine.Offset > 0 ? routine.Offset - 1 : (field.Value.Length + routine.Offset);
			if (cdPos < 0 || cdPos >= field.Value.Length)
			{
				field.IsException = true;
				field.ExceptionMessage = ErrorMessages.CDInvalidLength;
				return;
			}

			if (routine.Modulus == 0)
			{
				field.IsException = true;
				field.ExceptionMessage = ErrorMessages.CDInvalidModulus;
				return;
			}

			//Apply replacement values
			int?[] valueList = new int?[field.Value.Length];
			char value;
			for (i = 0; i < field.Value.Length; i++)
			{
				value = field.Value[i];
				if (routine.ReplacementValues != null && routine.ReplacementValues.ContainsKey(value))
					valueList[i] = routine.ReplacementValues[value];
				else if (char.IsDigit(value))
					valueList[i] = value - '0';
				else if (value != ' ' || (value == ' ' && !routine.IgnoreSpaces))
				{
					field.ExceptionMessage = ErrorMessages.CDInvalidNumber;
					field.IsException = true;
					return;
				}
			}

			//ensure we have a valid digit for the check digit
			if (!valueList[cdPos].HasValue)
			{
				field.ExceptionMessage = ErrorMessages.CDNotNumeric;
				field.IsException = true;
				return;
			}

			int[] weights = routine.Weights;

			//apply the weights...
			w = 0;
			int nTotal = 0;
			i = routine.WeightsDirection == WeightPatternDirection.RightToLeft ? valueList.Length - 1 : 0;
			int increment = routine.WeightsDirection == WeightPatternDirection.RightToLeft ? -1 : 1;
			for (; i >= 0 && i < valueList.Length; i += increment)
			{
				if (i == cdPos || !valueList[i].HasValue)
					continue;

				int nVal = valueList[i].Value * weights[w];

				if (routine.Method == CheckDigitMethod.SumOfDigits)
				{
					//little math trick, I shall not prove
					if (nVal > 0)
					{
						int rem = nVal % 9;
						if (rem == 0)
							rem = 9;
						nTotal += rem;
					}
				}
				else if (routine.Method == CheckDigitMethod.OnesDigit)
					nTotal += nVal % 10;
				else //product addition
					nTotal += nVal;

				w++;
				if (w == weights.Length)
					w = 0;
			}

			nTotal = nTotal % routine.Modulus;

			if (routine.Compliment > 0)
				nTotal = routine.Compliment - nTotal;

			if (nTotal == 10)
				nTotal = routine.Rem10Replacement;
			else if (nTotal == 11)
				nTotal = routine.Rem11Replacement;
			else
				nTotal %= 10;

			if (nTotal != valueList[cdPos])
			{
				field.ExceptionMessage = string.IsNullOrEmpty(routine.FailureMessage) ? ErrorMessages.CDFailed : routine.FailureMessage;
				field.IsException = true;
			}
		}

	}
}
