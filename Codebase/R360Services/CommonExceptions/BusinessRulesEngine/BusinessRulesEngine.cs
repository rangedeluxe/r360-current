﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WFS.RecHub.R360Shared;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.Common;
using System.Globalization;

namespace WFS.RecHub.BusinessRules
{
    public class BusinessRulesEngine : APIBase
    {
		internal ICommonExceptionDAL _ceDAL = null;
		private const string c_sSpecialCharacters = "&/*?@";

		public BusinessRulesEngine(IServiceContext serviceContext)
			: base(serviceContext)
		{
		}

		#region APIBase Methods

		protected override bool ValidateSID(out int userID)
		{
			throw new NotImplementedException();
		}

		protected override ActivityCodes RequestType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		#endregion

		#region IBusinessRulesEngine Methods

		private ICommonExceptionDAL ExceptionDAL
		{
			get
			{
				if (_ceDAL == null)
					_ceDAL = new CommonExceptionDAL(_ServiceContext.SiteKey ?? string.Empty);
				return _ceDAL;
			}
		}

		/// <summary>
		/// Helper Method to expose the service's logging provider...
		/// </summary>
		void Log(string message, string src, MessageType messageType, MessageImportance messageImportance)
		{
			OnLogEvent(message, src, messageType, messageImportance);
		}

		#endregion

		public bool ValidateTransaction(TransactionDto tranInfo, bool stopOnException, bool desiredBalancingTriggersException)
		{
			RuleDefinitionsDto rulesDto;

			if (tranInfo == null)
				throw new ArgumentNullException("tranInfo");

			try
			{
				if (tranInfo.Items == null || tranInfo.Items.Count == 0)
					return true;//no items to validate

				GetRules(tranInfo.DataEntrySetupKey, out rulesDto);
				tranInfo.IsException = !ValidateItems(tranInfo, rulesDto, stopOnException, desiredBalancingTriggersException);
			}
			catch (Exception ex)
			{
				Log(string.Format("Error applying business rules: {0}", ex.ToString()), "BusinessRulesEngine", MessageType.Error, MessageImportance.Essential);
				return false;
			}

			return true;
		}

		private bool ValidateItems(TransactionDto tranInfo, RuleDefinitionsDto rulesDto, bool stopOnException, bool desiredBalancingTriggersException)
		{
			bool isValid = true;

			//first check if we fail the transaction rule of requiring at least one document/stub/invoice
			if (rulesDto.TransactionRequiresInvoice && tranInfo.Items.FirstOrDefault(o => !o.IsPayment) == null)
			{
				isValid = false;
				tranInfo.IsTransactionException = true;
				tranInfo.Message = ErrorMessages.InvoiceIsRequired;
			}

			if (!stopOnException || isValid)
			{
				if (tranInfo.BatchPaymentSubType == null)
					tranInfo.BatchPaymentSubType = GetTransactionsPaymentSubType(tranInfo, rulesDto.FieldDefinitions);

				decimal paymentTotal = 0;
				decimal invoiceTotal = 0;

				for (int i = 0; i < tranInfo.Items.Count; i++)
				{
					ItemDto item = tranInfo.Items[i];
					decimal itemAmount;
					ValidateItem(tranInfo, rulesDto.FieldDefinitions, item, stopOnException, out itemAmount);
					if (item.IsException)
					{
						isValid = false;
						if (stopOnException)
							break;
					}

					if (item.IsPayment)
						paymentTotal += itemAmount;
					else
						invoiceTotal += itemAmount;
				}

				//if we are valid or we are hitting every item and we haven't already flagged the transaction as in error, we should apply balancing
				if ((isValid || !stopOnException) && !tranInfo.IsTransactionException)
				{
					if (rulesDto.BalancingOption != InvoiceBalancingOptions.BalancingNotRequired && paymentTotal != invoiceTotal)
					{
						tranInfo.IsTransactionException = rulesDto.BalancingOption == InvoiceBalancingOptions.BalancingRequired || desiredBalancingTriggersException;
						tranInfo.Message = ErrorMessages.TransactionOutOfBalance;
					}
				}
			}
			return isValid;
		}

		private void ValidateItem(TransactionDto tranInfo, IList<FieldDefinitionDto> fieldDefs, ItemDto item, bool stopOnException, out decimal itemAmount)
		{
			itemAmount = 0;
			if (item.IsPayment)
			{
				try
				{
					itemAmount = decimal.Parse(item.ItemData.Amount);
				}
				catch
				{
					//nothing to do, amount value is invalid and cannot be counted towards the total
				}
			}

			//do a merge of the field defs and fields to ensure all definitions are present and validate all fields
			//assume the items fields are ordered by ScreenOrder, but don't rely on it and reorder if necessary, pushing unknown fields to the end
			int fieldIndex = 0;
			foreach (var fieldDef in fieldDefs)
			{
				//verify this field def applies to this item
				var tableType = fieldDef.TableType;
				if ((item.IsPayment && (tableType == FieldTableTypes.Payments || tableType == FieldTableTypes.PaymentsDataEntry))
					|| (!item.IsPayment && (tableType == FieldTableTypes.Stubs || tableType == FieldTableTypes.StubsDataEntry)))
				{
					int checkIndex = fieldIndex;
					int matchIndex = -1;
					//check all field at and after the current fieldIndex for a matching key
					while (checkIndex < item.Fields.Count)
					{
						if (!item.Fields[checkIndex].DataEntryColumnKey.HasValue)
						{
							if (item.Fields[checkIndex].FieldName == fieldDef.FldName)
							{
								matchIndex = checkIndex;
								break;
							}
						}
						else if (item.Fields[checkIndex].DataEntryColumnKey == fieldDef.DataEntryColumnKey)
						{
							matchIndex = checkIndex;
							break;
						}
						checkIndex++;
					}

					if (matchIndex == -1)
					{
						//we did not find the field, insert a new field there so we can validate its empty value
						var newField = new FieldDto()
						{
							DataEntryColumnKey = fieldDef.DataEntryColumnKey,
							Value = string.Empty,
							ExceptionMessage = null
						};
						item.Fields.Insert(fieldIndex, newField);
					}
					else if (matchIndex != fieldIndex)
					{
						//we found the field at a different position
						item.Fields.Insert(fieldIndex, item.Fields[matchIndex]);
						item.Fields.RemoveAt(matchIndex + 1);//remove, adjusting for the insert we just did
					}

					//we know the field is there, we can now validate it
					BusinessRulesDto rules = GetApplicableRules((int)tranInfo.BatchSourceKey, (int)tranInfo.BatchPaymentTypeKey, tranInfo.BatchPaymentSubType, fieldDef);
					FieldDto field = item.Fields[fieldIndex];
					ValidateField(rules, field);//must call even if no rules as we need to convert DateTime objects (and other types in the future?) to the string values.

					//if the field has an exception that wants the field name, then format it
					if (field.ExceptionMessage != null && field.ExceptionMessage.Contains("{0}"))
						field.ExceptionMessage = string.Format(field.ExceptionMessage, fieldDef.DisplayName);

					if (field.IsException)
					{
						item.IsException = true;
						if (stopOnException)
							return;
					}

					if (fieldDef.FldName == "Amount" && !string.IsNullOrEmpty(field.Value))
					{
						try
						{
							itemAmount = decimal.Parse(field.Value);
						}
						catch
						{
							//nothing to do, amount value is invalid and cannot be counted towards the total
						}
					}

					fieldIndex++;
				}
			}
		}

		public void ValidateField(BusinessRulesDto rules, FieldDto field)
		{
			field.IsException = false;
			field.ExceptionMessage = string.Empty;

			if (string.IsNullOrEmpty(field.Value) && field.DateTimeValue != DateTime.MinValue)
			{
				if (rules != null)
					field.Value = field.DateTimeValue.ToString(rules.Mask, CultureInfo.InvariantCulture);
				else
					field.Value = field.DateTimeValue.ToString("MM/dd/yyyy");
			}

			if (rules != null)
			{
				//perform all the validations....
				if (!field.IsException)
					ValidateRequired(rules, field);

				if (!field.IsException)
					ValidateLength(rules, field);

				if (!field.IsException)
					ValidateFormatType(rules, field);

				if (!field.IsException)
					CheckDigitValidation.ValidateCheckDigit(rules.CheckDigitRoutine, field);

				if (!field.IsException)
					ValidateFieldValue(rules, field);
			}
		}

		internal void ValidateRequired(BusinessRulesDto rules, FieldDto field)
		{
			if (rules.Required && string.IsNullOrEmpty(field.Value))
			{
				field.ExceptionMessage = string.IsNullOrEmpty(rules.RequiredMessage) ? ErrorMessages.FieldIsRequired : rules.RequiredMessage;
				field.IsException = true;
			}
		}

		internal void ValidateLength(BusinessRulesDto rules, FieldDto field)
		{
			//only applies if we have a value
			if (!string.IsNullOrEmpty(field.Value))
			{
				if (rules.MinLength > field.Value.Length || (rules.MaxLength > 0 && rules.MaxLength < field.Value.Length))
				{
					field.ExceptionMessage = ErrorMessages.FieldLengthInvalid;
					field.IsException = true;
				}
			}
		}

		internal void ValidateFormatType(BusinessRulesDto rules, FieldDto field)
		{
			if (!string.IsNullOrEmpty(field.Value))
			{
				if (field.Value.Any(o => char.IsControl(o)))
				{
					field.ExceptionMessage = ErrorMessages.InvalidControlChars;
					field.IsException = true;
					return;
				}

				switch (rules.FormatType)
				{
					case FormatTypes.Amount:
						decimal val;
						if ( !decimal.TryParse(field.Value, out val) || val < 0 || val.ToString("F8") != val.ToString("F2") + "000000")
						{
							field.ExceptionMessage = ErrorMessages.InvalidAmountFormat;
							field.IsException = true;
						}
						break;
					case FormatTypes.Number:
						if (field.Value.Any(o => !char.IsDigit(o)))
						{
							field.ExceptionMessage = ErrorMessages.InvalidNumberFormat;
							field.IsException = true;
						}
						break;
					case FormatTypes.NumberAndSpecialChar:
						if (field.Value.Any(o => !char.IsDigit(o) && !c_sSpecialCharacters.Contains(o)))
						{
							field.ExceptionMessage = ErrorMessages.InvalidCharacters;
							field.IsException = true;
						}
						break;
					case FormatTypes.Alphanumeric:
						if (field.Value.Any(o => !char.IsLetterOrDigit(o)))
						{
							field.ExceptionMessage = ErrorMessages.InvalidCharacters;
							field.IsException = true;
						}
						break;
					case FormatTypes.AlphanumericAndSpecialChar:
						if (field.Value.Any(o => !char.IsLetterOrDigit(o) && !c_sSpecialCharacters.Contains(o)))
						{
							field.ExceptionMessage = ErrorMessages.InvalidCharacters;
							field.IsException = true;
						}
						break;
					case FormatTypes.NumberDashes:
						if (field.Value.Any(o => !char.IsNumber(o) && o != '-'))
						{
							field.ExceptionMessage = ErrorMessages.InvalidCharacters;
							field.IsException = true;
						}
						break;
					case FormatTypes.Date:
					case FormatTypes.CustomDate:
						DateTime dateTime;
						if (!DateTime.TryParseExact(field.Value, rules.Mask ?? "MM/dd/yyyy", null, System.Globalization.DateTimeStyles.None, out dateTime))
						{
							field.ExceptionMessage = ErrorMessages.InvalidDateFormat;
							field.IsException = true;
						}
						break;
					case FormatTypes.Custom:
						string validCharacters = (rules.Mask ?? string.Empty).ToLower();
						if (field.Value.Any(o => !validCharacters.Contains(char.ToLower(o))))
						{
							field.ExceptionMessage = ErrorMessages.InvalidCharacters;
							field.IsException = true;
						}
						break;
					case FormatTypes.Any:
					default:
						break;
				}
			}
		}

		internal void ValidateFieldValue(BusinessRulesDto rules, FieldDto field)
		{
			if ((rules.FieldValidationKey ?? 0) <= 0)
				return;

			if (field == null || string.IsNullOrEmpty(field.Value))
				return;//It is not the field validation's job to ensure something was entered

			bool exists = false;
			if (!ExceptionDAL.CheckForValidationValuesExistence(rules.FieldValidationKey.Value, field.Value, out exists))
				throw new Exception("Error encountered checking validation table.");

			if ((rules.FieldValidationTypeKey == 1 && exists) //exists in stop file
				|| (rules.FieldValidationTypeKey == 2 && !exists)) //doesn't exist in accept file)
			{
				field.IsException = true;
				field.ExceptionMessage = string.IsNullOrEmpty(rules.FieldValidationFailureMessage) ? ErrorMessages.FVFailed : rules.FieldValidationFailureMessage;
			}
		}

		private PaymentSubTypeDto GetTransactionsPaymentSubType(TransactionDto tranInfo, IList<FieldDefinitionDto> fieldDefs)
		{
			IList<PaymentSubTypeDto> subTypes = GetPaymentSubTypes();
			PaymentSubTypeDto subType = null;

			if (subTypes != null && subTypes.Count > 0)
			{
				//currently only support sub types on ACH payments
				if (tranInfo.BatchPaymentTypeKey == PaymentTypes.ACH && tranInfo.Items != null)
				{
					//get the ServiceEntryClass DataEntryColumnKey
					FieldDefinitionDto secFieldDef = fieldDefs.FirstOrDefault(o => o.TableType == FieldTableTypes.PaymentsDataEntry && string.Compare(o.FldName, "StandardEntryClass", true) == 0);
					if (secFieldDef != null)
					{
						FieldDto field = null;
						foreach (var item in tranInfo.Items)
						{
							if (item.IsPayment && item.Fields != null)
							{
								field = item.Fields.FirstOrDefault(o => o.DataEntryColumnKey == secFieldDef.DataEntryColumnKey && !string.IsNullOrEmpty(o.Value));
								if (field != null)
									break;
							}
						}

						if (field != null)
							subType = subTypes.FirstOrDefault(o => o.PaymentType == tranInfo.BatchPaymentTypeKey && o.SubTypeDescription == field.Value);
					}
				}
			}
			return subType;
		}

		public BusinessRulesDto GetApplicableRules(int batchSourceKey, int batchPaymentTypeKey, PaymentSubTypeDto paymentSubType, FieldDefinitionDto fieldDef)
		{
			BusinessRulesDto rules = null;
			int highestWeight = -1;
			if (fieldDef.BusinessRules != null)
			{
				int currentWeight;
				foreach (var checkRules in fieldDef.BusinessRules)
				{
					//weighting goes as, Source matches are most important, worth 100 points
					//	payment sub types is next most important, worth 10 points (match here implies paymen type match)
					//	payment type is least important, worth 1 point
					//		if we decide to switch and make payment type more important than source than change source = 1, sub type = 100, type = 10.
					currentWeight = 0;
					//assume the rule applies until something does not match
					if (paymentSubType == null)
					{
						if (checkRules.BatchPaymentSubTypeKey.HasValue)
							continue;//not a match, rule expects a sub-type
					}
					else if ((checkRules.BatchPaymentSubTypeKey ?? paymentSubType.PaymentSubTypeKey) == paymentSubType.PaymentSubTypeKey)
						currentWeight += checkRules.BatchPaymentSubTypeKey.HasValue ? 10 : 0;
					else
						continue;//doesn't match

					if ((checkRules.BatchPaymentTypeKey ?? (int)batchPaymentTypeKey) == batchPaymentTypeKey)
						currentWeight += checkRules.BatchPaymentTypeKey.HasValue ? 1 : 0;
					else
						continue;//doesn't match

					if ((checkRules.BatchSourceKey ?? batchSourceKey) == batchSourceKey)
						currentWeight += checkRules.BatchSourceKey.HasValue ? 100 : 0;
					else
						continue;//doesn't match

					if (highestWeight < currentWeight)
					{
						rules = checkRules;
						highestWeight = currentWeight;
					}
				}
			}

			return rules;
		}

		private Dictionary<long, RuleDefinitionsDto> _ruleDefinitionsSet = new Dictionary<long, RuleDefinitionsDto>();
		public void GetRules(long dataEntrySetupKey, out RuleDefinitionsDto rulesDto)
		{	
			if (!_ruleDefinitionsSet.TryGetValue(dataEntrySetupKey, out rulesDto))
			{
				if (!ExceptionDAL.GetRules(dataEntrySetupKey, out rulesDto))
					throw new ApplicationException("Failed to get field setup.");

				_ruleDefinitionsSet.Add(dataEntrySetupKey, rulesDto);
			}
		}

		private IList<PaymentSubTypeDto> _subTypes = null;
		public IList<PaymentSubTypeDto> GetPaymentSubTypes()
		{
			if (_subTypes == null)
			{
				List<PaymentSubTypeDto> subTypes;
				if (!ExceptionDAL.GetPaymentSubTypes(out subTypes))
					throw new ApplicationException("Failed to retreive payment sub types, unable to apply business rules.");

				_subTypes = subTypes;
			}

			return _subTypes;
		}
    }
}
