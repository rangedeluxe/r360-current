﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.BusinessRules
{
	public static class ErrorMessages
	{
		internal static string PleaseReview = "Please Review.";

		public static string TransactionOutOfBalance = "Amounts for this Transaction are out of balance." + "  " + PleaseReview;
		public static string InvoiceIsRequired = "Every transaction must contain at least one related item." + "  " + PleaseReview;

		public static string FieldIsRequired = "{0} is required." + "  " + PleaseReview;
		public static string FieldLengthInvalid = "The length of the {0} is invalid." + "  " + PleaseReview;
		public static string InvalidControlChars = "{0} contins an invalid control character." + "  " + PleaseReview;
		public static string InvalidAmountFormat = "{0} is not a valid amount." + "  " + PleaseReview;
		public static string InvalidNumberFormat = "{0} is not a valid number." + "  " + PleaseReview;
		public static string InvalidDateFormat = "{0} is not a valid date." + "  " + PleaseReview;
		public static string InvalidCharacters = "{0} contains invalid characters." + "  " + PleaseReview;

		public static string CDInvalidLength = "Unable to apply the check digit routine for the {0} field, the field is not long enough." + "  " + PleaseReview;
		public static string CDInvalidOffset = "Unable to apply the check digit routine for the {0} field, the offset configuration is invalid.";
		public static string CDInvalidModulus = "Unable to apply the check digit routine for the {0} field, the modulus configuration is invalid.";
		public static string CDInvalidNumber = "Check digit routine may not be applied to a non-numeric value." + "  " + PleaseReview;
		public static string CDNotNumeric = "Unable to apply check digit routine, the value in the check digit position is not numeric." + "  " + PleaseReview;
		public static string CDFailed = "Check digit validation failed." + "  " + PleaseReview;

		public static string FVFailed = "The value entered is not valid according to table validation." + "  " + PleaseReview;
	}
}
