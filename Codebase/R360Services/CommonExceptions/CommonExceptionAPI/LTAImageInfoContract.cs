﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: David Parker
* Date: 6/06/2013
*
* Purpose: Common Exception Image Stream Request contract
*
* Modification History
* WI 101852 DRP 6/5/2013
*   - Adding Image Retrieval
* WI 143442 DJW 05/30/2014
*     Updated to handle batch collisions (BatchID to long, and added BatchSourceID)
* WI 175549 SAS 10/31/2014
*   Update ImportTypeShortName field to BatchSourceName  
*********************************************************************************/
namespace WFS.RecHub.CommonException.CommonExceptionAPI
{
    [MessageContract]
    public class LTAImageInfoContract
    {

        [MessageBodyMember(Order = 1)]
        public int ProcessingDateKey
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 2)]
        public int BankID
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 3)]
        public int LockboxID
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 4)]
        public long BatchID
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 5)]
        public int BatchSequence
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 6)]
        public string FileDescriptor
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 7)]
        public bool IsBack
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 8)]
        public short ColorMode
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 9)]
        public bool AcceptTiffFormat 
        { 
            get; 
            set; 
        }

        [MessageBodyMember(Order = 10)]
        public long SourceBatchID
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 11)]
        public string BatchSourceName
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 12)]
        public string ImportTypeShortName
        {
            get;
            set;
        }

        [MessageBodyMember(Order = 13)]
        public int PaymentType
        {
            get;
            set;
        }
    }
}
