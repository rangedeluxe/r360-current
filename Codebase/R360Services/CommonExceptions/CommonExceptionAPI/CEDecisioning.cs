﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WFS.RecHub.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
using System.Runtime.CompilerServices;
using OLSvc = WFS.RecHub.OLDecisioningServicesClient;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.DAL.CommonExceptions;
using WFS.RecHub.BusinessRules;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 5/22/2013
*
* Purpose: Common Exception API Class
*
* Modification History
* WI 101774 WJS  5/22/2013
*   - Initial Creation
* WI 114661 TWE  9/20/2013
*   - Make sure all columns of data have a field of data to display
* WI 114799 TWE  9/23/2013
*   - Correct the Editable flag
* WI 114937 TWE 09/25/2013
*   - pass Invoice Balancing Option to OLDecisioning service.
* WI 115373 CEJ 09/28/2013
*   Change the CommonExceptionAPI to use the OLFImageService to retrieve images
* WI 107711 CEJ 10/17/2013
*   Create the ability log userActivity in the sessionLog from the R360 Services
* WI 143442 DJW 05/30/2014
*     Updated to handle batch collisions (BatchID to long, and added BatchSourceID)
* WI 155461 CMC 08/12/2014
*     Fixed message text
*********************************************************************************/
namespace WFS.RecHub.CommonException.CommonExceptionAPI
{
	public class CEDecisioning : APIBase
	{
		private CommonExceptionDAL _CommonExceptionDAL = null;

		public CEDecisioning(R360ServiceContext serviceContext)
			: base(serviceContext)
		{
			CompleteBatchManager.Init();
		}

		#region Session Validation

		private enum PermissionMode
		{
			unknown,
			view,
			modify
		}

		private class ValidateUserId
		{
			private readonly CEDecisioning _api;
			public ValidateUserId(CEDecisioning api)
			{
				_api = api;
			}

			public string Permission { get; set; }
			public PermissionMode PermissionMode { get; set; }

			private bool ValidateSID(out int userID)
			{
				return _api.ValidateSID(out userID);
			}

			private void ValidatePermission(int userID, BaseResponse response)
			{
				if (Permission != null)
				{
					//DataTable dtResults;
					//using (cPermissionsDAL dsPermissionsDAL = new cPermissionsDAL(_api._ServiceContext.SiteKey))
					//{
					//  if (!dsPermissionsDAL.CheckUserPermissionsForFunction(Permission, PermissionMode.ToString(), userID, out dtResults) || dtResults == null)
					//  {
					//	_api.OnLogEvent("Error querying CheckUserPermissionsForFunction", typeof(CEDecisioning).Name, MessageType.Error, MessageImportance.Verbose);
					//	response.Errors.Add("Error occurred verifying permissions");
					//  }
					//  else if (dtResults.Rows.Count == 0)
					//  {
					//	_api.OnLogEvent(string.Format("User {0} does not have '{1}' permission for '{2}'", userID, PermissionMode, Permission), typeof(CEDecisioning).Name, MessageType.Information, MessageImportance.Verbose);
					//	response.Errors.Add("Action not permitted for this user");
					//  }
					//}
				}
			}

			public T Do<T>(Action<T, int, Guid> operation, [CallerMemberName] string procName = null)
			  where T : BaseResponse, new()
			{
				// Initialize result
				T result = new T();
				result.Status = StatusCode.FAIL;

				try
				{
					// Validate / get the user ID
					int userID;
					if (ValidateSID(out userID))
					{
						// Check required permission
						ValidatePermission(userID, result);

						// Perform the requested operation
						if (result.Errors.Count == 0)
						{
							_api.LogActivity(procName);
							operation(result, userID, _api._ServiceContext.GetSessionID());
						}
					}
					else
					{
						result.Errors.Add("Invalid Session");
					}
				}
				catch (Exception ex)
				{
					// Log exception and return failure
					result.Status = StatusCode.FAIL;
					_api.OnErrorEvent(ex, typeof(CEDecisioning).Name, procName);
				}

				return result;
			}
		}

		#endregion

		/// <summary>
		/// Returns a list of transaction that are waiting to be decisioned
		/// </summary>
		public TransactionsResponse GetTransactionsPendingDecision()
		{
			return new ValidateUserId(this)
			{
				PermissionMode = PermissionMode.view,
				Permission = "View Exception Summary"
			}
			.Do<TransactionsResponse>((response, userId, sessionId) =>
			{
				// Call DAL to get the exceptions
				DataTable dt = null;
				if (CommonExceptDAL.GetCommonExceptions(userId, GetSession(), out dt))
				{
					// Convert to Model type
					response.Transactions = new List<TransactionModel>(dt.Rows.Count);
					foreach (DataRow row in dt.Rows) // Loop over the rows.
					{
						TransactionModel item = AddItemToTransactionModel(row);
						response.Transactions.Add(item);
					}

					// Return success
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public BaseResponse UnlockTransaction(int commonExceptionID)
		{
			return new ValidateUserId(this)
			{
				PermissionMode = PermissionMode.modify,
				Permission = "Break Exception Lock"
			}
			.Do<BaseResponse>((response, userId, sessionId) =>
			{
				bool userCanOverrideLocks = true; // This was checked during user validation above

				// Call DAL to unlock the transaction
				DataTable dt;
				if (CommonExceptDAL.CheckinTransaction(commonExceptionID, userId, (byte)CEDecisioningStatus.Unresolved, userCanOverrideLocks, false, false, out dt))
				{
					// Return Success / Failure
					if (dt.Rows.Count == 0)
						response.Errors.Add("Invalid ID");
					else if (Convert.ToBoolean(dt.Rows[0]["StillLocked"]))
						response.Errors.Add("Could not unlock transaction");
					else
						response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public TransactionDetailsResponse CheckoutTransaction(int commonExceptionID)
		{
			return new ValidateUserId(this)
			{
				PermissionMode = PermissionMode.view,
				Permission = "View Exception Detail"	// Checkout doesn't require "modify" rights...  Modify rights will be checked separately before "locking"...
			}
			.Do<TransactionDetailsResponse>((response, userId, sessionId) =>
			{
				// Call DAL to lock the transaction
				DataTable dtResults;
				if (CommonExceptDAL.CheckoutTransaction(commonExceptionID, userId, sessionId, out dtResults))
				{
					var ceProperties = CommonExceptionProperties.FromDataRow(dtResults.Rows[0]);
					response.LockCreated = ceProperties.LockCreated;

					// Return transaction details (whether we locked it or not...)
					CEDecisioningStatus currentStatus;
					try
					{
						response.TransactionInfo = GetTransactionDetails(userId, commonExceptionID, ceProperties, out currentStatus);
					}
					catch
					{
						// If we couldn't get the batch information, do not hold the transaction lock
						// It would be nice to do this as a database transaction rollback, but the potential time spent in GetBatchInformation is too high to justify the rare case of a rollback...
						if (response.LockCreated)
						{
							try
							{
								CommonExceptDAL.CheckinTransaction(commonExceptionID, userId, (byte)CEDecisioningStatus.Unresolved, false, false, false, out dtResults);
								response.LockCreated = false;
							}
							catch { }
						}

						// Throw the error on
						throw;
					}

					// Automatically resynchronize if our status is out of sync
					if (currentStatus != CEDecisioningStatus.Unresolved && response.LockCreated)
					{
						// This transaction is complete...  Not sure why our database doesn't match, but update it to match...
						if (CheckinTransactionImpl(commonExceptionID, userId, currentStatus, false, false))
						{
							// Mark success
							response.LockCreated = false;
							response.Status = StatusCode.SUCCESS;
						}
						else
						{
							response.Errors.Add("Error clearing lock status flag");
						}
					}
					else
					{
						// Mark success
						response.Status = StatusCode.SUCCESS;
					}

					// Return the transaction summary fields regardless of status (Waited until now to verify we are synchronized)
					string error;
					response.Transaction = GetTransactionSummary(userId, sessionId, commonExceptionID, out error);
					if (error == null)
						GetTransactionDocuments(sessionId, response.Transaction, response.TransactionInfo, out error);

					if (error != null)
					{
						response.Errors.Add(error);
						response.Status = StatusCode.FAIL;
					}
				}
			});
		}

		public TransactionDetailsResponse ViewTransaction(int commonExceptionID)
		{
			return new ValidateUserId(this)
			{
				PermissionMode = PermissionMode.view,
				Permission = "View Exception Summary"
			}
			.Do<TransactionDetailsResponse>((response, userId, sessionId) =>
			{
				// Call DAL to get the IDs (ignore the lock status)
				DataTable dtResults;
				if (CommonExceptDAL.VerifyTransactionCheckedOut(commonExceptionID, userId, out dtResults))
				{
					var ceProperties = CommonExceptionProperties.FromDataRow(dtResults.Rows[0]);

					// Return transaction details
					CEDecisioningStatus currentStatus;

					//DataTable permResults;
					//using (cPermissionsDAL dsPermissionsDAL = new cPermissionsDAL(_ServiceContext.SiteKey))
					//{
					//	var permission = "View Exception Detail";
					//	if (!dsPermissionsDAL.CheckUserPermissionsForFunction(permission, PermissionMode.view.ToString(), userId, out permResults) || permResults == null)
					//	{
					//		OnLogEvent("Error querying CheckUserPermissionsForFunction", typeof(CEDecisioning).Name, MessageType.Error, MessageImportance.Verbose);
					//	}
					//	else if (permResults.Rows.Count == 0)
					//	{
					//		OnLogEvent(string.Format("User {0} does not have '{1}' permission for '{2}'", userId, PermissionMode.view, permission), typeof(CEDecisioning).Name, MessageType.Information, MessageImportance.Verbose);
					//	}
					//	else
					//	{
					response.TransactionInfo = GetTransactionDetails(userId, commonExceptionID, ceProperties, out currentStatus);
					//  }
					//}

					// Return the transaction summary fields regardless of status (Waited until now to verify we are synchronized)
					string error;
					response.Transaction = GetTransactionSummary(userId, sessionId, commonExceptionID, out error);
					if (error == null)
						GetTransactionDocuments(sessionId, response.Transaction, response.TransactionInfo, out error);

					if (error != null)
					{
						response.Errors.Add(error);
						response.Status = StatusCode.FAIL;
					}
					else
					{
						response.Status = StatusCode.SUCCESS;
					}
				}
			});
		}

		private TransactionInfoModel GetTransactionDetails(int userId, int commonExceptionID, CommonExceptionProperties additionalData, out CEDecisioningStatus currentStatus)
		{
			decimal checkAmount, stubAmount;
			IList<ColumnDefinition> stubFieldDefinitions;
			IList<PaymentItem> stubItems;
			IList<ColumnDefinition> paymentFieldDefinitions;
			IList<PaymentItem> paymentItems;
			IList<TransactionImageRef> images;
			InvoiceBalancingOptions balancingOption;
			bool tranRequiresInvoice = false;
			bool isTransactionException = false;
			string transactionMessage = string.Empty;

			// Check exception type, to know where to load the batch from
			if (additionalData.ExceptionType == CEExceptionType.IntegraPAYInProcess)
			{
				// Load batch information from separate IntegraPAY database
				GetIntegraPAYBatchInformation(additionalData.GlobalBatchID, additionalData.TransactionID, out stubFieldDefinitions, out stubItems, out paymentFieldDefinitions, out paymentItems, out images, out currentStatus, out checkAmount, out stubAmount, out balancingOption);
			}
			else
			{
				// Load batch information from local RecHubException database schema
				GetPostProcessTransactionInformation(commonExceptionID, userId, out stubFieldDefinitions, out stubItems, out paymentFieldDefinitions, out paymentItems, out currentStatus, out checkAmount, out stubAmount, out tranRequiresInvoice, out isTransactionException, out transactionMessage, out balancingOption);

				images = null;//we'll get these later as we need more information, specifically the bank and workgroup id
			}

			return new TransactionInfoModel()
			{
				CommonExceptionId = commonExceptionID,
				StubItems = new ItemList() { Items = stubItems, FieldDefinitions = stubFieldDefinitions },
				PaymentItems = (paymentFieldDefinitions == null ? null : new ItemList() { Items = paymentItems, FieldDefinitions = paymentFieldDefinitions }),
				TransactionImages = images,
				Decision = currentStatus.ToTransactionDecision(),
				BalancingOption = (TransactionInfoModel.BalancingOptions)(int)balancingOption,
				TransactionRequiresInvoice = tranRequiresInvoice,
				IsTransactionException = isTransactionException,
				Message = transactionMessage
			};
		}

		public TransactionResponse GetTransaction(int commonExceptionID)
		{
			return new ValidateUserId(this)
			{
				PermissionMode = PermissionMode.view,
				Permission = "View Exception Detail"
			}
			.Do<TransactionResponse>((response, userId, sessionId) =>
			{
				string error;

				response.Transaction = GetTransactionSummary(userId, sessionId, commonExceptionID, out error);
				if (error != null)
				{
					response.Errors.Add(error);
					response.Status = StatusCode.FAIL;
				}
				else
				{
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		private TransactionModel GetTransactionSummary(int userId, Guid sessionId, int commonExceptionID, out string error)
		{
			// Call DAL to get the exceptions
			DataTable dt = null;
			if (CommonExceptDAL.GetCommonException(userId, sessionId, commonExceptionID, out dt) && dt.Rows.Count > 0)
			{
				error = null;

				// Convert to Model type
				return AddItemToTransactionModel(dt.Rows[0]);
			}
			else
			{
				error = "Error retrieving transaction summary";
				return null;
			}
		}

		private TransactionModel AddItemToTransactionModel(DataRow row)
		{
			DateTime tmpDateTime;
			decimal tmpDecimal;
			int tmpInt;
			long tmpLong;
			//byte tempByte;
			bool tempBit;
			TransactionModel item = new TransactionModel();
			if (Int32.TryParse(row["CommonExceptionID"].ToString(), out tmpInt))
			{
				item.CommonExceptionId = tmpInt;
			}
			if (Int32.TryParse(row["SiteBankID"].ToString(), out tmpInt))
			{
				item.BankId = tmpInt;
			}
			if (Int32.TryParse(row["SiteClientAccountID"].ToString(), out tmpInt))
			{
				item.ClientAccountID = tmpInt;
			}
			if (long.TryParse(row["BatchID"].ToString(), out tmpLong))
			{
				item.BatchId = tmpLong;
			}
			if (long.TryParse(row["SourceBatchID"].ToString(), out tmpLong))
			{
				item.SourceBatchID = tmpLong;
			} 
			if (decimal.TryParse(row["CheckAmount"].ToString(), out tmpDecimal))
			{
				item.Amount = tmpDecimal;
			}
            if (Int32.TryParse(row["BatchPaymentTypeKey"].ToString(), out tmpInt))
            {
                item.PaymentTypeId = tmpInt;
            }

			item.PaymentType = row["BatchPaymentTypeName"].ToString();
			if (Int32.TryParse(row["DecisioningStatus"].ToString(), out tmpInt))
			{
				item.Status = ((CEDecisioningStatus)tmpInt).ToTransactionStatus();
			}
			if (Int32.TryParse(row["DepositDateKey"].ToString(), out tmpInt))
			{
				item.DepositDate = DateTime.ParseExact(tmpInt.ToString(), ServiceConstants.DateTimeFormats.InvariantDateFormat, System.Globalization.CultureInfo.InvariantCulture)
		  .ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat);
			}
			if (Int32.TryParse(row["ImmutableDateKey"].ToString(), out tmpInt))
			{
				item.ProcessingDate = DateTime.ParseExact(tmpInt.ToString(), ServiceConstants.DateTimeFormats.InvariantDateFormat, System.Globalization.CultureInfo.InvariantCulture)
			  .ToString(ServiceConstants.DateTimeFormats.DefaultDateFormat);
			}
			if (Boolean.TryParse(row["InProcessException"].ToString(), out tempBit))
			{
				item.ExceptionType = tempBit ? TransactionModel.ExceptionTypes.InProcess : TransactionModel.ExceptionTypes.Posting;
			}
			if (DateTime.TryParse(row["Deadline"].ToString(), out tmpDateTime))
			{
				item.Deadline = tmpDateTime.ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat);
			}
			item.PaymentSource = row["BatchSourceName"].ToString();
			item.Workgroup = row["LongName"].ToString();
			if (Int32.TryParse(row["Locked"].ToString(), out tmpInt))
			{
				if (tmpInt > 0)
				{
					//it is locked;
					item.IsLocked = true;
					item.LockOwner = tmpInt;
					if (DateTime.TryParse(row["LockTime"].ToString(), out tmpDateTime))
					{
						item.LockTime = tmpDateTime.ToUniversalTime().ToString(ServiceConstants.DateTimeFormats.DateTimeFormat);
					}
					item.LockOwnerName = row["LockOwnerName"].ToString();
				}
				else
				{
					item.IsLocked = false;
					item.LockOwner = -1;
					item.LockTime = null;
				}
			}
			item.TransactionID = (int)row["TransactionID"];
			item.EntityID = (int)row["EntityID"];
			item.LockOwnerEntityID = row["LockOwnerEntityID"] == DBNull.Value ? null : (int?)row["LockOwnerEntityID"];
			item.BatchSourceShortName = row["BatchSourceShortName"] == DBNull.Value ? null : (string)row["BatchSourceShortName"];
			item.ImportTypeShortName = row["ImportTypeShortName"] == DBNull.Value ? null : (string)row["ImportTypeShortName"];
			item.DisplayBatchID = row["DisplayBatchID"] == DBNull.Value ? false : (bool)row["DisplayBatchID"];
			return item;
		}

		private void GetTransactionDocuments(Guid sessionId, TransactionModel tran, TransactionInfoModel tranInfo, out string error)
		{
			error = null;

			List<TransactionImageRef> imageRefs = null;
			if (tranInfo.PaymentItems != null && tranInfo.PaymentItems.Items != null)
				imageRefs = tranInfo.PaymentItems.Items.Select(o => new TransactionImageRef() { BatchSequence = IntFromGuid(o.Id), FileDescriptor = _ipoPICSImages.FILE_DESCRIPTOR_CHECK }).ToList();
			else
				imageRefs = new List<TransactionImageRef>();//blank list for documents
			
			// Call DAL to get the exceptions
			List<TransactionImageRef> docImageRefs = null;
			if (CommonExceptDAL.GetTransactionDocuments(sessionId, tran, out docImageRefs))
			{
				if (docImageRefs != null)
					imageRefs.AddRange(docImageRefs);

				tranInfo.TransactionImages = imageRefs;
			}
			else
			{
				error = "Error retrieving transaction's documents";
			}
		}

		private void GetPostProcessTransactionInformation(int commonExceptionID, int userID,
		  out IList<ColumnDefinition> stubFieldDefinitions, out IList<PaymentItem> stubItems, out IList<ColumnDefinition> paymentFieldDefinitions, out IList<PaymentItem> paymentItems,
		  out CEDecisioningStatus currentStatus, out decimal checkAmount, out decimal stubAmount,
			out bool tranRequiresInvoice, out bool isTransactionException, out string transactionMessage,
			out InvoiceBalancingOptions balancingOption)
		{
			// Get the transaction information from the database
			PostProcessTransactionInfo tran;
			if (!CommonExceptDAL.GetTransactionInformation(commonExceptionID, userID, out tran))
				throw new Exception("Unexpected failure retrieving transaction information from the database");

			currentStatus = tran.CurrentStatus.ToCEDecisioningStatus();

			ValidateTransaction(tran, out tranRequiresInvoice, out balancingOption, out stubFieldDefinitions, out paymentFieldDefinitions, out stubItems, out paymentItems, out checkAmount, out stubAmount);

			isTransactionException = tran.IsTransactionException;
			transactionMessage = tran.Message;
		}

		private bool ValidateTransaction(PostProcessTransactionInfo tran, out bool tranRequiresInvoice, out InvoiceBalancingOptions balancingOption,
											out IList<ColumnDefinition> stubFieldDefinitions, out IList<ColumnDefinition> paymentFieldDefinitions,
											out IList<PaymentItem> stubItems, out IList<PaymentItem> paymentItems,
											out decimal checkAmount, out decimal stubAmount)
		{
			// Copy field definitions for the data entry fields
			// This will come from the Business Rules Engine
			var bre = new BusinessRulesEngineManager(_ServiceContext);

			IList<PaymentItem> validatedStubs, validatedPayments;
			bre.ValidateTransaction(tran, out tranRequiresInvoice, out balancingOption, out stubFieldDefinitions, out paymentFieldDefinitions, out validatedStubs, out validatedPayments);

			bool hasException = tran.IsTransactionException;
			ConvertValidatedItems(tran.Payments, validatedPayments, paymentFieldDefinitions, out paymentItems, out checkAmount, ref hasException);
			ConvertValidatedItems(tran.Stubs, validatedStubs, stubFieldDefinitions, out stubItems, out stubAmount, ref hasException);

			return !hasException;
		}

		private void ConvertDateValues(PostProcessTransactionInfo tran, IList<ColumnDefinition> stubFieldDefinitions, IList<ColumnDefinition> paymentFieldDefinitions)
		{
			if (tran != null)
			{
				ConvertDateValues(tran.Stubs, stubFieldDefinitions);
				ConvertDateValues(tran.Payments, paymentFieldDefinitions);
			}
		}

		private void ConvertDateValues(IList<ItemRecord> items, IList<ColumnDefinition> definitions)
		{
			if (items == null || definitions == null)
				return;

			foreach (var item in items)
			{
				if (item.Fields == null)
					continue;

				foreach (var field in item.Fields)
				{
					if (!string.IsNullOrEmpty(field.Value))
					{
						var fieldKey = GuidFromLong(field.DataEntryColumnKey);
						var def = definitions.FirstOrDefault(o => o.Id == fieldKey);

						if (def != null && def.DataType == FieldDataTypes.Date)
						{
							string format = string.IsNullOrEmpty(def.Mask) ? "MM/dd/yyyy" : def.Mask;
							DateTime dateTime;
							if (DateTime.TryParseExact(field.Value, format, null, System.Globalization.DateTimeStyles.None, out dateTime))
								field.DateTimeValue = dateTime;
						}
					}
				}
			}
		}

		private void ConvertValidatedItems(IList<ItemRecord> originalItems, IList<PaymentItem> validatedItems, IList<ColumnDefinition> columnDefs, out IList<PaymentItem> items, out decimal totalAmount, ref bool hasException)
		{
			items = new List<PaymentItem>();

			totalAmount = 0;

			if (originalItems == null)
				return;

			var amountFieldDef = columnDefs.FirstOrDefault(o => o.IsItemAmount);
			foreach (var origItem in originalItems)
			{
				//don't return deleted items, the UI is already tracking them
				if (origItem.Action == ExceptionItemAction.DeleteItem)
					continue;

				//first create a list of field values returned
				List<FieldDataAndInfo> fldValues = validatedItems.First(o => o.Id == GuidFromLong(origItem.BatchSequence)).FieldValues.Select(o => new FieldDataAndInfo()
				{
					ColumnDefId = o.ColumnDefId,
					FieldValue = o.FieldValue,
					AwaitingDecision = o.AwaitingDecision,
					DecisionMessage = o.DecisionMessage,
				}).ToList();

				var item = new PaymentItem()
				{
					Id = GuidFromInt(origItem.BatchSequence),
					IsException = fldValues.Exists(o => o.AwaitingDecision),
					FieldValues = fldValues,
					Messages = new List<string>()
				};

				// Track payment amounts
				if (amountFieldDef != null)
				{
					var amountField = item.FieldValues.FirstOrDefault(o => o.ColumnDefId == amountFieldDef.Id);
					if (amountField != null && amountField.FieldValue != null)
					{
						decimal tmpAmount;
						if (decimal.TryParse(amountField.FieldValue, out tmpAmount))
							totalAmount += tmpAmount;
					}
				}

				// Generate messages to display in the UI...
				foreach (var field in item.FieldValues.Where(o => o.AwaitingDecision))
				{
					item.Messages.Add(!string.IsNullOrEmpty(field.DecisionMessage) ? field.DecisionMessage
										: "Please review: " + columnDefs.First(o => o.Id == field.ColumnDefId).ColumnTitle);
				}

				if (item.IsException)
					hasException = true;

				items.Add(item);

				OnLogEvent("Item DTO created:  " + item.Id.ToString(),
							this.GetType().Name,
							MessageType.Information,
							MessageImportance.Debug);
			}
		}

		public GenericResponse<DefaultBRErrorMessagesDto> GetDefaultBRErrorMessages()
		{
			return new GenericResponse<DefaultBRErrorMessagesDto>()
			{
				Status = StatusCode.SUCCESS,
				Data = new DefaultBRErrorMessagesDto()
				{
					InvoiceIsRequired = ErrorMessages.InvoiceIsRequired,
					FieldIsRequired = ErrorMessages.FieldIsRequired,
					FieldLengthInvalid = ErrorMessages.FieldLengthInvalid,
					InvalidControlChars = ErrorMessages.InvalidControlChars,
					InvalidAmountFormat = ErrorMessages.InvalidAmountFormat,
					InvalidNumberFormat = ErrorMessages.InvalidNumberFormat,
					InvalidDateFormat = ErrorMessages.InvalidDateFormat,
					InvalidCharacters = ErrorMessages.InvalidCharacters
				}
			};
		}

		private void GetIntegraPAYBatchInformation(int globalBatchID, int transactionID,
			out IList<ColumnDefinition> stubFieldDefinitions, out IList<PaymentItem> stubItems, out IList<ColumnDefinition> paymentFieldDefinitions, out IList<PaymentItem> paymentItems,
			out IList<TransactionImageRef> images, out CEDecisioningStatus currentStatus, out decimal checkAmount, out decimal stubAmount, out InvoiceBalancingOptions balancingOption)
		{
			// This method is used to retrieve exception details from OLDecisioning
			OLSvc.IOLDecisioningSvcClient olDecisioningClient = OLSvc.OLDecisioningSvcClient.CreateClient(_ServiceContext.SiteKey, APIConstants.OLDecisioning.ServiceUserId);
			var batch = olDecisioningClient.GetBatch(globalBatchID); // TODO: Can we optimize the OLDecisioningService to return a single transaction?
			if (batch == null) throw new Exception(string.Format("Global Batch ID {0} returned no data from OLDecisioning", globalBatchID));

			// Check for incorrect checkout...
			if (batch.DepositStatus == DepositStatusValues.DepositInDecision &&
				batch.LogonName != APIConstants.OLDecisioning.ServiceUserId)
			{
				OnLogEvent(string.Format("Invalid batch checkout status -- batch reports it is checked out by '{0}'; will re-attempt checkout...", batch.LogonName),
					  this.GetType().Name, MessageType.Error, MessageImportance.Essential);

				var response = olDecisioningClient.ResetBatch(globalBatchID);
				if (!response.Success)
					throw new Exception("Reset Batch failed with the following messages:\r\n" + string.Join("\r\n", response.Messages));

				batch.DepositStatus = DepositStatusValues.DepositPendingDecision;
			}

			// Auto check-out the batch if necessary
			if (batch.DepositStatus == DepositStatusValues.DepositPendingDecision)
			{
				var response = olDecisioningClient.CheckOutBatch(globalBatchID);
				if (!response.Success)
					throw new Exception("Checkout Batch failed with the following messages:\r\n" + string.Join("\r\n", response.Messages));

				batch.DepositStatus = DepositStatusValues.DepositInDecision;
			}

			if (batch.Transactions == null) throw new Exception(string.Format("Global Batch ID {0} returned no transactions from OLDecisioning", globalBatchID));
			var tran = batch.Transactions.Find(o => o.TransactionID == transactionID);
			if (tran == null) throw new Exception(string.Format("Global Batch ID {0} did not contain transaction {1} from OLDecisioning", globalBatchID, transactionID));

			switch (tran.DecisionSourceStatus)
			{
				case DecisionSrcStatusTypes.PendingDecisionBatchLogon:
				case DecisionSrcStatusTypes.PendingDecisionBatchLogonTDE:
					currentStatus = CEDecisioningStatus.Unresolved;
					break;

				default:
				case DecisionSrcStatusTypes.NoDecisionRequired:
				case DecisionSrcStatusTypes.Accept:
					currentStatus = CEDecisioningStatus.AcceptedPending;
					// Note: We default to "Accept" because if it is not being rejected, then mostly likely it will still be processed...
					break;

				case DecisionSrcStatusTypes.Reject:
					currentStatus = CEDecisioningStatus.RejectedPending;
					break;
			}

			balancingOption = batch.BalancingOption;

			// Create lists for the return values
			stubFieldDefinitions = new List<ColumnDefinition>();
			stubItems = new List<PaymentItem>();
			paymentFieldDefinitions = new List<ColumnDefinition>();
			paymentItems = new List<PaymentItem>();
			images = new List<TransactionImageRef>();

			//create the standard check data entry fields...
			var checkAmountFieldDef = new ColumnDefinition() { ColumnFieldName = "Amount", ColumnTitle = "Payment Amount", Editable = false, Id = Guid.NewGuid(), Sequence = 0, Tooltip = "Payment Amount", MaxLength = 0, FormatType = FormatTypes.Amount, IsItemAmount = true, HasBusinessRules = false, DataType = FieldDataTypes.Currency };
			var checkRTFieldDef = new ColumnDefinition() { ColumnFieldName = "RT", ColumnTitle = "R/T", Editable = false, Id = Guid.NewGuid(), Sequence = 0, Tooltip = "R/T", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text };
			var checkAccountFieldDef = new ColumnDefinition() { ColumnFieldName = "Account", ColumnTitle = "Account Number", Editable = false, Id = Guid.NewGuid(), Sequence = 0, Tooltip = "Account Number", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text };
			var checkSerialFieldDef = new ColumnDefinition() { ColumnFieldName = "Serial", ColumnTitle = "Check/Trace/Ref Number", Editable = false, Id = Guid.NewGuid(), Sequence = 0, Tooltip = "Check/Trace/Ref Number", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text };
			var checkPayeeFieldDef = new ColumnDefinition() { ColumnFieldName = "Payee", ColumnTitle = "Payee", Editable = false, Id = Guid.NewGuid(), Sequence = 0, Tooltip = "Payee", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text };
			var checkRemitterFieldDef = new ColumnDefinition() { ColumnFieldName = "Remitter", ColumnTitle = "Payer", Editable = false, Id = Guid.NewGuid(), Sequence = 0, Tooltip = "Payer", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text };
			var checkDeisioningReasonFieldDef = new ColumnDefinition() { ColumnFieldName = "DecisioningReason", ColumnTitle = "Decisioning Reason", Editable = false, Id = Guid.NewGuid(), Sequence = 0, Tooltip = "Decisioning Reason", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text };

			//first comes the Decisioning Reason
			paymentFieldDefinitions.Add(checkDeisioningReasonFieldDef);

			//add the standard check field definitions
			paymentFieldDefinitions.Add(checkAmountFieldDef);
			paymentFieldDefinitions.Add(checkRTFieldDef);
			paymentFieldDefinitions.Add(checkAccountFieldDef);
			paymentFieldDefinitions.Add(checkSerialFieldDef);
			paymentFieldDefinitions.Add(checkPayeeFieldDef);
			paymentFieldDefinitions.Add(checkRemitterFieldDef);

			// Copy field definitions for the batch data entry fields
			foreach (var deField in batch.DESetupFields)
			{
				var fieldDef = new ColumnDefinition()
				{
					ColumnFieldName = deField.FldName,
					ColumnTitle = deField.FldPrompt,
					Editable = deField.DocumentType == DocumentTypes.Stub && SetEditableFlag(deField.TypeReadOnly), //Stubs are the only keyable exceptions.
					Id = deField.DESetupFieldID,
					Sequence = deField.ScreenOrder, // Looks like ScreenOrder is by doc type... If we return more than one DocumentType, this won't work...
					Tooltip = deField.FldPrompt,
					MaxLength = deField.FldLength,
					FormatType = BusinessRulesEngineManager.GetFormatType((FieldDataTypes)(int)deField.FldDataTypeEnum, null),
					Required = deField.TypeRequired,
					IsItemAmount = (string.Compare(deField.FldName, "Amount", StringComparison.InvariantCultureIgnoreCase) == 0),
					HasBusinessRules = deField.AllowDecisioning//best guess for now...
				};
				if (deField.DocumentType == DocumentTypes.Stub)
					stubFieldDefinitions.Add(fieldDef);
				else
					paymentFieldDefinitions.Add(fieldDef);
			}

			// Copy Checks, Documents, and Stubs to the image reference list
			// Copy checks to the payment item list
			checkAmount = 0;
			foreach (var check in tran.Checks)
			{
				var imageRef = new TransactionImageRef()
				{
					BatchSequence = check.BatchSequence,
					FileDescriptor = _ipoPICSImages.FILE_DESCRIPTOR_CHECK,	// All checks have a file descriptor of "C"...?
				};
				images.Add(imageRef);

				checkAmount += check.Amount;
				List<FieldDataAndInfo> fldValues = new List<FieldDataAndInfo>();
				fldValues.Add(new FieldDataAndInfo() { ColumnDefId = checkAmountFieldDef.Id, FieldValue = check.Amount.ToString() });
				fldValues.Add(new FieldDataAndInfo() { ColumnDefId = checkRTFieldDef.Id, FieldValue = check.RT });
				fldValues.Add(new FieldDataAndInfo() { ColumnDefId = checkAccountFieldDef.Id, FieldValue = check.Account });
				fldValues.Add(new FieldDataAndInfo() { ColumnDefId = checkSerialFieldDef.Id, FieldValue = check.Serial });
				fldValues.Add(new FieldDataAndInfo() { ColumnDefId = checkPayeeFieldDef.Id, FieldValue = check.PayeeName });
				fldValues.Add(new FieldDataAndInfo() { ColumnDefId = checkRemitterFieldDef.Id, FieldValue = check.RemitterName });

				fldValues.AddRange(check.Fields.ConvertAll(o => new FieldDataAndInfo()
				{
					ColumnDefId = o.DESetupFieldID,
					FieldValue = o.FldData,
					AwaitingDecision = false,
					DecisionMessage = string.Empty,
				}));

				fldValues.Add(new FieldDataAndInfo() { ColumnDefId = checkDeisioningReasonFieldDef.Id, FieldValue = check.DecisioningReasonDesc, AwaitingDecision = check.DecisioningReasonID != Guid.Empty, DecisionMessage = check.DecisioningReasonDesc });

				if (check.DecisioningReasonID != Guid.Empty)
					checkDeisioningReasonFieldDef.HasBusinessRules = true;

				var item = new PaymentItem()
				{
					Id = GuidFromInt(check.GlobalCheckID),
					IsException = check.DecisioningReasonID != Guid.Empty,
					FieldValues = fldValues,
					Messages = new List<string>(),
				};

				// Generate messages to display in the UI...
				foreach (var field in item.FieldValues.Where(o => o.AwaitingDecision))
				{
					item.Messages.Add(!string.IsNullOrEmpty(field.DecisionMessage) ? field.DecisionMessage
					  : "Please review: " + paymentFieldDefinitions.First(o => o.Id == field.ColumnDefId).ColumnTitle);
				}

				paymentItems.Add(item);
				OnLogEvent("Check Item Created:  ",
							this.GetType().Name,
							MessageType.Information,
							MessageImportance.Debug);

			}

			foreach (var doc in tran.Documents)
			{
				var imageRef = new TransactionImageRef()
				{
					BatchSequence = doc.BatchSequence,
					FileDescriptor = doc.FileDescriptor
				};
				images.Add(imageRef);
			}

			// Copy stubs to the stub item list
			stubAmount = 0;
			var stubAmountFieldDef = stubFieldDefinitions.FirstOrDefault(o => o.IsItemAmount);
			if (tran.Stubs != null)
			{
				foreach (var stub in tran.Stubs)
				{
					//first create a list of field values returned
					List<FieldDataAndInfo> fldValues = stub.Fields.ConvertAll(o => new FieldDataAndInfo()
					{
						ColumnDefId = o.DESetupFieldID,
						FieldValue = o.FldData,
						AwaitingDecision = o.FieldDecisionStatus.HasValue && o.FieldDecisionStatus.Value == FieldDecisionStatusTypes.PendingDecision,
						DecisionMessage = o.DecisioningReasonDesc,
					});

					//now add the dummy list of columns to the list for fields not returned
					foreach (var fieldDef in stubFieldDefinitions)
					{
						var fieldObjInList = fldValues.SingleOrDefault(r => r.ColumnDefId == fieldDef.Id);
						if (fieldObjInList == null)
						{
							var nullItem = new FieldDataAndInfo()
							{
								ColumnDefId = fieldDef.Id,
								FieldValue = string.Empty,
								AwaitingDecision = false,
								DecisionMessage = string.Empty
							};
							fldValues.Add(nullItem);
						}
					}

					OnLogEvent("Field Count:  " + fldValues.Count.ToString(),
								this.GetType().Name,
								MessageType.Information,
								MessageImportance.Debug);


					var item = new PaymentItem()
					{
						Id = stub.DEItemRowDataID,
						IsException = stub.Fields.Any(o => o.FieldDecisionStatus.HasValue && o.FieldDecisionStatus.Value == FieldDecisionStatusTypes.PendingDecision),
						FieldValues = fldValues,
						Messages = new List<string>(),
					};

					// Track stub amounts
					if (stubAmountFieldDef != null)
					{
						var amountField = item.FieldValues.FirstOrDefault(o => o.ColumnDefId == stubAmountFieldDef.Id);
						if (amountField != null && amountField.FieldValue != null)
						{
							decimal tmpAmount;
							if (decimal.TryParse(amountField.FieldValue, out tmpAmount))
								stubAmount += tmpAmount;
						}
					}

					// Generate messages to display in the UI...
					foreach (var field in item.FieldValues.Where(o => o.AwaitingDecision))
					{
						item.Messages.Add(!string.IsNullOrEmpty(field.DecisionMessage) ? field.DecisionMessage
						  : "Please review: " + stubFieldDefinitions.First(o => o.Id == field.ColumnDefId).ColumnTitle);
					}
					stubItems.Add(item);
					OnLogEvent("Stub Item Created:  ",
								this.GetType().Name,
								MessageType.Information,
								MessageImportance.Debug);

					/* Stubs don't have images...?
					var imageRef = new TransactionImageRef()
					{
					  BatchSequence = stub.BatchSequence,
					  FileDescriptor = ?
					};
					images.Add(imageRef);
					*/
				}
			}
		}

		private bool SetEditableFlag(bool readOnlyFlag)
		{
			//the flag on the Integrapay database is called ReadOnly
			// if the ReadOnly flag is true then set editable flag to false
			if (readOnlyFlag)
			{
				return false;
			}
			else
			{
				// if the readOnly flag is false then set editable flag to true
				return true;
			}
		}

		public CheckinResponse CheckinTransaction(int commonExceptionID, ExceptionAction transactionDecision, bool overrideRules, bool forceBalance,
		  IEnumerable<PaymentItemUpdate> stubUpdates, IEnumerable<PaymentItemUpdate> paymentUpdates)
		{
			return new ValidateUserId(this)
				// No Permission check here -- if they have it locked, then they have permission to check it back in
			.Do<CheckinResponse>((response, userId, sessionId) =>
			{
				// Call DAL to verify the transaction is locked, and get general information about it
				DataTable dtResults = null;
				int globalBatchID, transactionID;
				bool allowCheckin = true;
				TransactionInfoModel reevaluatedTran = null;
				if (CommonExceptDAL.VerifyTransactionCheckedOut(commonExceptionID, userId, out dtResults))
				{
					// Check lock status
					bool locked = Convert.ToBoolean(dtResults.Rows[0]["Locked"]);
					if (!locked)
					{
						response.NotLockedByCurrentUser = true;
						response.Errors.Add("Transaction was not locked");
						return;
					}

					IList<string> errors = null;
					var exceptionType = (CEExceptionType)Convert.ToByte(dtResults.Rows[0]["ExceptionType"]);
					if (exceptionType == CEExceptionType.IntegraPAYInProcess)
					{
						// Make the updates
						globalBatchID = Convert.ToInt32(dtResults.Rows[0]["GlobalBatchID"]);
						transactionID = Convert.ToInt32(dtResults.Rows[0]["TransactionID"]);
						DecisionSrcStatusTypes tranDescision =
						  transactionDecision == ExceptionAction.Accept ? DecisionSrcStatusTypes.Accept
						  : transactionDecision == ExceptionAction.Reject ? DecisionSrcStatusTypes.Reject
						  : DecisionSrcStatusTypes.Undefined;

						errors = UpdateIntegraPAYTransaction(userId, commonExceptionID, globalBatchID, transactionID, tranDescision, stubUpdates, forceBalance);
					}
					else if (exceptionType == CEExceptionType.PostProcess)
					{
						var tran = dtResults.Rows[0].ConvertToPostProcessTransactionInfo(null, null, null);
						UpdatePostProcessTransaction(userId, commonExceptionID, transactionDecision, tran, stubUpdates, paymentUpdates, overrideRules, out allowCheckin, out reevaluatedTran);
					}
					else
					{
						throw new Exception("Unrecognized Exception Type: " + exceptionType.ToString());
					}

					if (errors != null && errors.Count > 0)
					{
						// Report validation errors
						response.Errors.AddRange(errors);
					}
					else if (allowCheckin)
					{
						// Call DAL to unlock the transaction
						CEDecisioningStatus tranDecisionB =
						  transactionDecision == ExceptionAction.Accept ? CEDecisioningStatus.AcceptedPending
						  : transactionDecision == ExceptionAction.Reject ? CEDecisioningStatus.RejectedPending
						  : CEDecisioningStatus.Unresolved;

						if (CheckinTransactionImpl(commonExceptionID, userId, tranDecisionB, overrideRules, forceBalance))
						{
							// Mark success
							response.Status = StatusCode.SUCCESS;
						}
						else
						{
							response.Errors.Add("Failed to update transaction lock status");
						}
					}
					else
					{
						response.TransactionStillInError = true;
						response.Status = StatusCode.SUCCESS;
						response.ReevaluatedTran = reevaluatedTran;
					}
				}
			});
		}

		private bool CheckinTransactionImpl(int commonExceptionID, int userId, CEDecisioningStatus status, bool overrideRules, bool forceBalance)
		{
			DataTable dtResults;
			if (CommonExceptDAL.CheckinTransaction(commonExceptionID, userId, (byte)status, false, overrideRules, forceBalance, out dtResults))
			{
				if (dtResults.Rows.Count == 0)
				{
					OnLogEvent(string.Format("Invalid ID ({0}) for CheckinTransaction - no status returned", commonExceptionID),
					  this.GetType().Name, MessageType.Error, MessageImportance.Essential);
					return false;
				}

				if (Convert.ToBoolean(dtResults.Rows[0]["StillLocked"]))
				{
					OnLogEvent(string.Format("CheckinTransaction for ID: {0}, User: {1} failed", commonExceptionID, userId),
					  this.GetType().Name, MessageType.Error, MessageImportance.Essential);
					return false;
				}

				// Check for batch completion
				var exceptionType = (CEExceptionType)Convert.ToByte(dtResults.Rows[0]["ExceptionType"]);
				if (exceptionType == CEExceptionType.IntegraPAYInProcess)
				{
					CEDecisioningStatus minStatus = (CEDecisioningStatus)Convert.ToInt32(dtResults.Rows[0]["MinDecisioningStatus"]);
					if (minStatus > CEDecisioningStatus.Unresolved)
					{
						// Call CompleteBatch
						int globalBatchID = Convert.ToInt32(dtResults.Rows[0]["GlobalBatchID"]);
						CompleteBatchManager.CompleteBatch(globalBatchID);
					}
				}
				else if (exceptionType == CEExceptionType.PostProcess)
				{
					TransactionStatus minStatus = (TransactionStatus)Convert.ToInt32(dtResults.Rows[0]["MinTransactionStatus"]);
					if (minStatus > TransactionStatus.Unresolved)
					{
						// Call CompleteBatch
						CommonExceptDAL.MarkPostProcessBatchComplete(Convert.ToInt64(dtResults.Rows[0]["ExceptionBatchKey"]));
					}
				}
				return true;
			}

			return false;
		}

		private void UpdatePostProcessTransaction(int userId, int commonExceptionID, ExceptionAction transactionDecision,
													PostProcessTransactionInfo tran, IEnumerable<PaymentItemUpdate> clientStubUpdates,
													IEnumerable<PaymentItemUpdate> clientPaymentUpdates, bool overrideRules,
													out bool allowCheckin, out TransactionInfoModel reevaluatedTran)
		{
			allowCheckin = true;
			reevaluatedTran = null;

			if (transactionDecision == ExceptionAction.Accept)
			{
				tran.Stubs = GetItemRecords(clientStubUpdates);
				tran.Payments = GetItemRecords(clientPaymentUpdates);

				IList<ColumnDefinition> stubFieldDefinitions, paymentFieldDefinitions;
				IList<PaymentItem> stubItems, paymentItems;
				IList<TransactionImageRef> images;
				decimal checkAmount, stubAmount;
				bool tranRequiresInvoice;
				InvoiceBalancingOptions balancingOption;

				//we have to always revalidate to get the correct rules for our columnns so that we can then convert the date types to the correct format for SQL to parse and store
				allowCheckin = ValidateTransaction(tran, out tranRequiresInvoice, out balancingOption, out stubFieldDefinitions, out paymentFieldDefinitions, out stubItems, out paymentItems, out checkAmount, out stubAmount);
				reevaluatedTran = new TransactionInfoModel()
				{
					CommonExceptionId = commonExceptionID,
					StubItems = new ItemList() { Items = stubItems, FieldDefinitions = stubFieldDefinitions },
					PaymentItems = (paymentFieldDefinitions == null ? null : new ItemList() { Items = paymentItems, FieldDefinitions = paymentFieldDefinitions }),
					TransactionImages = null,//no need to reload images when returning the revalidated transaction
					Decision = TransactionInfoModel.TransactionDecision.Unresolved,
					BalancingOption = (TransactionInfoModel.BalancingOptions)(int)balancingOption,
					TransactionRequiresInvoice = tranRequiresInvoice,
					IsTransactionException = tran.IsTransactionException,
					Message = tran.Message
				};

				if (overrideRules && allowCheckin == false)
					allowCheckin = true;

				ConvertDateValues(tran, stubFieldDefinitions, paymentFieldDefinitions);
			}

			if (allowCheckin || overrideRules)
			{

				if (!CommonExceptDAL.UpdateTransaction(userId, commonExceptionID, tran.Payments, tran.Stubs))
					throw new Exception("Unexpected failure updating transaction information in the database");

				// Note: The decision will actually get stored during the checkin that follows...
			}
		}

		private List<ItemRecord> GetItemRecords(IEnumerable<PaymentItemUpdate> updates)
		{
			// ... Do we have any updates to make...?
			if (updates != null)
			{
				return updates.Select(item => new ItemRecord()
					{
						BatchSequence = IntFromGuid(item.Id),
						Action = item.Action,
						ItemInfo = new ItemDataRecord()
						{
							RT = FindValue(BusinessRulesEngineManager.PaymentRTGuid, item.FieldValues),
							Account = FindValue(BusinessRulesEngineManager.PaymentAccountGuid, item.FieldValues),
							Serial = FindValue(BusinessRulesEngineManager.PaymentSerialGuid, item.FieldValues),
							RemitterName = FindValue(BusinessRulesEngineManager.PaymentRemitterGuid, item.FieldValues),
							Amount = FindValue(BusinessRulesEngineManager.PaymentAmountGuid, item.FieldValues)
						},
						Fields = item.FieldValues == null ?
							new List<FieldDataRecord>()
							: item.FieldValues
								.Where(o => !BusinessRulesEngineManager.StandardFieldGuids.Contains(o.ColumnDefId))
								.Select(fieldVal => new FieldDataRecord()
								{
									DataEntryColumnKey = LongFromGuid(fieldVal.ColumnDefId),
									Value = fieldVal.FieldValue,
								}).ToList()
					}).ToList();	// TODO: Verify UI won't send "null" entries that we are supposed to exclude from this list...
			}
			return null;
		}

		private string FindValue(Guid id, IList<FieldData> fields)
		{
			var field = fields == null ? null : fields.FirstOrDefault(o => o.ColumnDefId == id);
			return field == null ? null : field.FieldValue;
		}

		private IList<string> UpdateIntegraPAYTransaction(int userID, int commonExceptionID, int globalBatchID, int transactionID,
		  DecisionSrcStatusTypes transactionDecision, IEnumerable<PaymentItemUpdate> updates, bool forceBalance)
		{
			// ... Do we have any updates to make...?
			if (updates == null && transactionDecision == DecisionSrcStatusTypes.Undefined && !forceBalance)
				return null;

			// This method is used to update exception details in OLDecisioning
			OLSvc.IOLDecisioningSvcClient olDecisioningClient = OLSvc.OLDecisioningSvcClient.CreateClient(_ServiceContext.SiteKey, APIConstants.OLDecisioning.ServiceUserId);
			OLSvc.BatchTransaction transaction = new OLSvc.BatchTransaction()
			{
				TransactionID = transactionID,
				DecisionSourceStatus = transactionDecision,
				DestinationLockboxKey = Guid.Empty,
				BalancingAction = forceBalance ? InvoiceBalancingActions.ForceBalance : InvoiceBalancingActions.SaveUnbalanced,
				Stubs = new List<OLSvc.TransactionStubItem>()
			};

			int updateCount = 0, insertCount = 0, deleteCount = 0;
			if (updates != null || transactionDecision == DecisionSrcStatusTypes.Accept)
			{
				// Load the field definitions...  TODO: Could we cache these somehow?
				var batch = olDecisioningClient.GetBatch(globalBatchID); // TODO: Can we cache this somehow during the "Get"?
				if (batch == null) throw new Exception(string.Format("Global Batch ID {0} returned no data from OLDecisioning", globalBatchID));
				if (batch.Transactions == null) throw new Exception(string.Format("Global Batch ID {0} returned no transactions from OLDecisioning", globalBatchID));
				if (batch.DepositStatus != DepositStatusValues.DepositInDecision)
				{
					throw new Exception(string.Format("Invalid state for batch {0} ({1}) - cannot perform updates", globalBatchID, batch.DepositStatus));
				}

				var tran = batch.Transactions.Find(o => o.TransactionID == transactionID);
				if (tran == null) throw new Exception(string.Format("Global Batch ID {0} did not contain transaction {1} from OLDecisioning", globalBatchID, transactionID));

				IEnumerable<OLSvc.DESetupField> fieldDefs = batch.DESetupFields.Where(o => o.DocumentType == DocumentTypes.Stub);

				if (updates != null)
				{
					transaction.Stubs = updates.Where(o => o.Action != ExceptionItemAction.NoChange).Select(stub => new OLSvc.TransactionStubItem()
					{
						Action = AsItemAction(stub.Action),
						DEItemRowDataID = stub.Id,
						Fields = fieldDefs.Select(fieldDef =>
						{
							var fieldValue = stub.FieldValues == null ? null : stub.FieldValues.FirstOrDefault(val => val.ColumnDefId == fieldDef.DESetupFieldID);
							var origValue = (stub.Action == ExceptionItemAction.InsertItem || stub.Id == Guid.Empty) ? null :
							  tran.Stubs.Find(origStub => origStub.DEItemRowDataID == stub.Id).Fields.Find(origField => origField.DESetupFieldID == fieldDef.DESetupFieldID);
							return new OLSvc.DEField()
							{
								Action = fieldValue == null ? FieldActions.NoAction : fieldValue.FieldValue == null ? FieldActions.Delete : FieldActions.Update,
								TableName = fieldDef.TableName,
								FldName = fieldDef.FldName,
								FldValue = (fieldValue == null ? (origValue == null ? null : origValue.FldData) : fieldValue.FieldValue) ?? string.Empty,
								FieldDecisionStatus = transactionDecision == DecisionSrcStatusTypes.Accept && // Common Exceptions does not support field-level rejection, so auto-accept any field that is pending acceptance
								  origValue != null && origValue.FieldDecisionStatus.HasValue && origValue.FieldDecisionStatus.Value == FieldDecisionStatusTypes.PendingDecision ?
								  FieldDecisionStatusTypes.Accept : FieldDecisionStatusTypes.Undefined,
							};
						})
							// The above created a field for all defined fields; filter that list to only include fields that are being updated or accepted...
						.Where(field => field.Action != FieldActions.NoAction || field.FieldDecisionStatus != FieldDecisionStatusTypes.Undefined).ToList(),
					}).ToList();

					// Audit the number of updates, inserts, and deletes
					foreach (var stub in transaction.Stubs)
					{
						switch (stub.Action)
						{
							case OLSvc.ItemActions.Insert:
								insertCount++;
								break;
							case OLSvc.ItemActions.Update:
								updateCount++;
								break;
							case OLSvc.ItemActions.Delete:
								deleteCount++;
								break;
						}
					}
				}

				// On Acceptance, we must validate all fields on all stubs have been accepted...
				if (transactionDecision == DecisionSrcStatusTypes.Accept)
				{
					transaction.Stubs.AddRange(
					  tran.Stubs
						.Where(stub => !transaction.Stubs.Exists(update => update.DEItemRowDataID == stub.DEItemRowDataID))
						.Select(stub => new OLSvc.TransactionStubItem()
						{
							Action = OLSvc.ItemActions.Update,
							DEItemRowDataID = stub.DEItemRowDataID,
							Fields = stub.Fields
							  .Where(field => field.FieldDecisionStatus == FieldDecisionStatusTypes.PendingDecision)
							  .Select(field => new OLSvc.DEField()
							  {
								  Action = FieldActions.NoAction,
								  TableName = field.TableName,
								  FldName = field.FldName,
								  FldValue = field.FldData ?? string.Empty,
								  FieldDecisionStatus = FieldDecisionStatusTypes.Accept
							  }).ToList()
						})
						  .Where(stub => stub.Fields.Any()));

					// Note: We don't audit "accept field" as updates...
				}
			}

			// Call through to the OLDecisioning service
			var response = olDecisioningClient.UpdateBatch(globalBatchID, new OLSvc.BatchTransaction[] { transaction });
			if (response.Success && updates != null)
			{
				// Audit the changes that we just requested, so that there is a user ID associated with those changes
				foreach (var itemUpdate in transaction.Stubs)
				{
					AuditItemUpdate(userID, commonExceptionID, globalBatchID, transactionID, itemUpdate);
				}
				AuditItemCount(updateCount, "Update", "updated", userID, commonExceptionID, globalBatchID, transactionID);
				AuditItemCount(insertCount, "Insert", "inserted", userID, commonExceptionID, globalBatchID, transactionID);
				AuditItemCount(deleteCount, "Delete", "deleted", userID, commonExceptionID, globalBatchID, transactionID);

				return null;
			}
			else
				return response.Messages;
		}

		private void AuditItemCount(int itemCount, string eventName, string pastTense, int userID, int commonExceptionID, int globalBatchID, int transactionID)
		{
			if (itemCount > 0)
			{
				CommonExceptDAL.WriteAuditEvent(string.Format("E {0} Item", eventName), userID,
				  string.Format("Exception ID: {0}, GlobalBatchID: {1}, TransactionID: {2} - {3} item(s) {4}",
					commonExceptionID, globalBatchID, transactionID, itemCount, pastTense));
			}
		}

		private void AuditItemUpdate(int userID, int commonExceptionID, int globalBatchID, int transactionID, OLSvc.TransactionStubItem update)
		{
			string message = string.Format("Exception ID: {0}, GlobalBatchID: {1}, TransactionID: {2} - Row: {3}",
					commonExceptionID, globalBatchID, transactionID, update.DEItemRowDataID);
			foreach (var fieldDef in update.Fields)
			{
				message += string.Format(";  [Col={0}] Value={1}", fieldDef.FldName, fieldDef.FldValue);
			}
			CommonExceptDAL.WriteAuditEvent(string.Format("E IP {0}", update.Action), userID, message);
		}

		private OLSvc.ItemActions AsItemAction(ExceptionItemAction action)
		{
			switch (action)
			{
				case ExceptionItemAction.DeleteItem:
					return OLSvc.ItemActions.Delete;
				case ExceptionItemAction.UpdateItem:
					return OLSvc.ItemActions.Update;
				case ExceptionItemAction.InsertItem:
					return OLSvc.ItemActions.Insert;
				default:
					throw new Exception("Unexpected value: " + action.ToString());
			}
		}

		#region Business Rules Support

		public RuleDefinitionsResponse GetRules(long dataEntrySetupKey)
		{
			return new ValidateUserId(this)
				//No permissions here
			.Do<RuleDefinitionsResponse>((response, userId, sessionId) =>
			{
				// Call DAL to get the exceptions
				RuleDefinitionsDto rulesDto;
				if (CommonExceptDAL.GetRules(dataEntrySetupKey, out rulesDto))
				{
					response.RuleDefinitions = rulesDto;
					// Return success
					response.Status = StatusCode.SUCCESS;
				}
			});
		}

		public BaseResponse ProcessStagedTransactions()
		{
			//TODO: Wrap this in a Do?  We don't have an actual user here, it should be the SSIS package calling us

			BaseResponse response = new BaseResponse();
			response.Status = StatusCode.FAIL;

			List<TransactionDto> transactions;
			//no validating of user here, it is an authenticated service user
			if (CommonExceptDAL.GetStagedTransactions(out transactions))
			{
				if (transactions != null)
				{
					OnLogEvent(string.Format("Beginning transaction validation, {0} transactions to process.", transactions.Count), typeof(CEDecisioning).Name, MessageType.Information, MessageImportance.Debug);

					BusinessRulesEngine bre = new BusinessRulesEngine(_ServiceContext);
					foreach (var transaction in transactions)
					{
						if (bre.ValidateTransaction(transaction, true, true))
						{
							OnLogEvent(string.Format("Transaction validation complete, batch {0}, transaction {1}, exception state {2}.", transaction.BatchID, transaction.TransactionID, transaction.IsException), typeof(CEDecisioning).Name, MessageType.Information, MessageImportance.Debug);

							if (!CommonExceptDAL.SetStagedTransactionsExceptionState(transaction, transaction.IsException))
							{
								OnLogEvent(string.Format("Error updating staged transaction's exception state, batch {0}, transaction {1}, exception state {2}.", transaction.BatchID, transaction.TransactionID, transaction.IsException), typeof(CEDecisioning).Name, MessageType.Error, MessageImportance.Essential);
								return response;
							}
						}
						else
						{
							OnLogEvent("Transaction validations failed, aborting validation of remaining transactions.", typeof(CEDecisioning).Name, MessageType.Warning, MessageImportance.Essential);
							return response;
						}
					}
					OnLogEvent(string.Format("Transaction validations complete, {0} transactions processed.", transactions.Count), typeof(CEDecisioning).Name, MessageType.Information, MessageImportance.Debug);
				}
				else
					OnLogEvent("No transactions staged for validation.", typeof(CEDecisioning).Name, MessageType.Information, MessageImportance.Debug);

				response.Status = StatusCode.SUCCESS;
			}

			return response;
		}

		#endregion

		#region ID Type Conversion Helpers

		private Guid GuidFromLong(long id)
		{
			byte[] rawId = BitConverter.GetBytes(id).Reverse().ToArray();										// Convert to bytes
			byte[] rawGuid = new byte[16];													// Initializes to all zeros...
			Array.Copy(rawId, 0, rawGuid, rawGuid.Length - rawId.Length, rawId.Length);		// Copy to end of byte array
			return new Guid(rawGuid);														// Convert to 128-bit GUID
		}

		private Guid GuidFromInt(int id)
		{
			byte[] rawId = BitConverter.GetBytes(id).Reverse().ToArray();										// Convert to bytes
			byte[] rawGuid = new byte[16];													// Initializes to all zeros...
			Array.Copy(rawId, 0, rawGuid, rawGuid.Length - rawId.Length, rawId.Length);		// Copy to end of byte array
			return new Guid(rawGuid);														// Convert to 128-bit GUID
		}

		private int IntFromGuid(Guid guid)
		{
			return BitConverter.ToInt32(guid.ToByteArray().Reverse().ToArray(), 0);
		}

		private long LongFromGuid(Guid guid)
		{
			return BitConverter.ToInt64(guid.ToByteArray().Reverse().ToArray(), 0);
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		protected CommonExceptionDAL CommonExceptDAL
		{
			get
			{
				if (_CommonExceptionDAL == null)
				{
					//if (_ServiceContext.SiteKey.Contains("[MockCommonExceptionDAL]"))
					//{
					//	_CommonExceptionDAL = new MockCommonExceptionDAL();
					//}
					//else
					//{
					// retrieve setting from SiteKey section of INI, if provided. else
					// default to [ipoDecisioningAPI] section.
					_CommonExceptionDAL = new CommonExceptionDAL(_ServiceContext.SiteKey ?? string.Empty);
					//}
				}
				return (_CommonExceptionDAL);
			}
		}

		protected override bool ValidateSID(out int userID)
		{
			bool bReturnValue = false;
			userID = -1;
			DataTable dtResults;

			if (CommonExceptDAL.GetUserIDBySID(_ServiceContext.GetSID(), out dtResults))
			{
				if (dtResults.Rows.Count > 0)
				{
					userID = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
					bReturnValue = true;
				}
				else
				{
					OnLogEvent(string.Format("SID '{0}' could not be matched to a user", _ServiceContext.GetSID()),
						typeof(CEDecisioning).Name, MessageType.Information, MessageImportance.Verbose);
				}
			}

			return bReturnValue;
		}

		protected override ActivityCodes RequestType
		{
			get
			{
				return ActivityCodes.acCommonExceptionsWebServiceCall;
			}
		}
	}
}
