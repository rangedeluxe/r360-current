﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Xml;
using System.Configuration;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.OLFServicesClient;
using System.Drawing.Imaging;
using WFS.RecHub.CommonException.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: David Parker
* Date: 6/06/2013
*
* Purpose: Common Exception Image Stream Implementation
*
* Modification History
* WI 101852 DRP 6/5/2013
*   - Adding Image Retrieval
* WI 115373 CEJ 09/28/2013
*   Change the CommonExceptionAPI to use the OLFImageService to retrieve images
* WI 143442 DJW 05/30/2014
*     Updated to handle batch collisions (BatchID to long, and added BatchSourceID)
* WI 175549 SAS 10/31/2014
*   Update ImportTypeShortName field to BatchSourceName   
*********************************************************************************/
namespace WFS.RecHub.CommonException.CommonExceptionAPI
{
    public class ImageRetrieval
    {
        private string _siteKey;
        private cSiteOptions options;
        private cEventLog _eventLog;

        public ImageRetrieval(string siteKeyValue)
        {
            this._siteKey = siteKeyValue;
            options = new cSiteOptions(siteKeyValue);
            _eventLog = new cEventLog(options.logFilePath
                                    , options.logFileMaxSize
                                    , (MessageImportance)options.loggingDepth);
        }
        public ReturnItem ServeImage(LTAImageInfoContract item, Guid SessionID)
        {
            MemoryStream imageData = new MemoryStream();

            //Bitmap bm = null;

            try
            {
                _eventLog.logEvent("ImageRetrieval.ServeImage, DateKey=" + item.ProcessingDateKey +
                    ", BankID=" + item.BankID + ", lockboxid=" + item.LockboxID + ", SourceBatchID=" + item.SourceBatchID +
                    ", BatchSourceName=" + item.BatchSourceName + ", ImportTypeShortName=" + item.ImportTypeShortName +
                    ", BatchSequence=" + item.BatchSequence + ", FileDescriptor=" + item.FileDescriptor,
                    "ImageRetrievalSvc",
                    MessageImportance.Debug);
                _eventLog.logEvent("ImageRetrieval.ServeImage, SiteKey=" + options.siteKey +
                    ", imagePath=" + options.imagePath + ", imageStorageMode=" + options.imageStorageMode.ToString() +
                    ", IsBack=" + item.IsBack.ToString() + ", AcceptTiffFormat=" + item.AcceptTiffFormat.ToString(),
                    "ImageRetrievalSvc",
                    MessageImportance.Debug);
                OLFServiceClient cOLFClient = new OLFServiceClient(_siteKey);
                byte[][] bytaImages;
                byte[] ar;
                string sFileDescriptor;
                string sFileExt;

                cOLFClient.SetSession(SessionID);

                if (cOLFClient.GetImage(
                        item.BankID,
                        item.LockboxID,
                        item.ProcessingDateKey,
                        item.ProcessingDateKey,
                        item.BatchID,
                        item.BatchSequence,
                        item.FileDescriptor == _ipoPICSImages.FILE_DESCRIPTOR_CHECK,
                        0,
                        item.SourceBatchID,
                        item.BatchSourceName,
                        item.ImportTypeShortName,
                        item.PaymentType,
                        out bytaImages,
                        out sFileDescriptor,
                        out sFileExt))
                {
                    ar = bytaImages[item.IsBack ? 1 : 0];
                    _eventLog.logEvent("ImageRetrieval.ServeImage, GetImage Success",
                        "ImageRetrievalSvc",
                        MessageImportance.Debug);

                    imageData = new MemoryStream(ar);

                    // Convert to non-TIFF format if requested and applicable
                    if (!item.AcceptTiffFormat)
                    {
                        _eventLog.logEvent("ImageRetrieval.ServeImage, AcceptTiffFormat Success",
                            "ImageRetrievalSvc",
                            MessageImportance.Debug);

                        if (ar != null && ar.Length > 3 && ar[0] == 73 && ar[1] == 73 && ar[2] == 42) // Does it start with "II*"?
                        {
                            _eventLog.logEvent("ImageRetrieval.ServeImage, Ready to convert",
                                "ImageRetrievalSvc",
                                MessageImportance.Debug);
                            // Convert images to client format...
                            Bitmap bmp = new Bitmap(imageData);
                            // Note: Right now, only handling bitonal TIFFs...  Not sure what to do if we encounter a grayscale or color TIFF...
                            // Updated (LA , 12/2/2014) - Tested to Handle PixelFormat.Format1bppIndexed and PixelFormat.Format32bppRgb
                            MemoryStream msPng = new MemoryStream();
                            EncoderParameters prms = new EncoderParameters(1);
                            prms.Param[0] = new EncoderParameter(Encoder.ColorDepth, 1);
                            bmp.Save(msPng, ImageFormat.Png);
                            msPng.Position = 0;
                            imageData = msPng;

                        }
                    }

                    //if (HasPropertyItem(bm, 274))
                    //    eventLog.logEvent("found orientation tag of " + bm.GetPropertyItem(274).Value[0],"ImageRetrievalSvc", LTAMessageImportance.Debug);
                    //else
                    //    if (HasPropertyItem(bm, 20624))
                    //    {
                    //        eventLog.logEvent("found jpeg tags, no orientation ", "ImageRetrievalSvc", LTAMessageImportance.Debug);
                    //        //bm.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    //        //imageData = new MemoryStream();
                    //       // bm.Save(imageData, System.Drawing.Imaging.ImageFormat.Tiff);
                    //        //imageData.Position = 0;
                    //    }
                    //bm.Dispose();
                    //bm = null;
                    //imageData.Position = 0; 

                    return new ReturnItem
                    {
                        bRetVal = true,
                        strrespXml = "no rsp:" + item.FileDescriptor,
                        data = imageData
                    };
                }
                else
                {
                    _eventLog.logWarning("ImageRetrieval.ServeImage, GetImage failed, consult logs for the reason", "ImageRetrievalSvc", MessageImportance.Essential);
                    return new ReturnItem
                    {
                        bRetVal = false,
                        strrespXml = BuildMsgDoc("Error", new string[] { "GetImage failed, consult logs for the reason" }).OuterXml,
                        data = imageData
                    };
                }


            }
            catch (Exception ex)
            {
                _eventLog.logError(ex, "ImageRetrieval", "ServeImage");
                ReturnItem error = new ReturnItem
                {
                    bRetVal = false,
                    strrespXml = BuildErrorDoc(ex, "ImageRetrieval.ServeImage").OuterXml,
                    data = imageData
                };
                return error;
            }
            //finally
            //{
            //    if (bm != null)
            //    {
            //        bm.Dispose();
            //        bm = null;
            //    }

            //}

        }
        private XmlDocument BuildErrorDoc(Exception ex, string context = null)
        {
            List<string> ErrorMessages = new List<string>();
            if (context != null)
                ErrorMessages.Add("context=" + context);
            ErrorMessages.Add(ex.Message);
            if (ex.InnerException != null)
                ErrorMessages.Add("Inner Exception:" + ex.InnerException.Message);
            return (BuildMsgDoc("Error", ErrorMessages.ToArray()));
        }

        private XmlDocument BuildMsgDoc(string MsgType, string[] ErrorMessages)
        {

            XmlDocument docXml;
            XmlNode nodeRoot;
            XmlNode nodeMessages;
            XmlAttribute attType;
            XmlNode nodeMsg;


            docXml = new XmlDocument();
            nodeRoot = docXml.CreateElement("Root", docXml.NamespaceURI);
            nodeMessages = docXml.CreateElement("Messages", docXml.NamespaceURI);
            foreach (string msg in ErrorMessages)
            {
                nodeMsg = docXml.CreateElement("Message");
                attType = docXml.CreateAttribute("Type", docXml.NamespaceURI);
                attType.Value = MsgType;
                nodeMsg.Attributes.Append(attType);
                nodeMsg.InnerText = msg;
                nodeMessages.AppendChild(nodeMsg);
            }
            nodeRoot.AppendChild(nodeMessages);
            docXml.AppendChild(nodeRoot);

            return (docXml);
        }

        private bool HasPropertyItem(Bitmap bm, int index)
        {

            for (int i = 0; i < bm.PropertyIdList.Length; i++)
                if (bm.PropertyIdList[i] == index)
                    return true;
            return false;
        }
    }
}
