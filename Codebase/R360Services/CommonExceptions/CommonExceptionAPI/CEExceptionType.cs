﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.CommonExceptionAPI
{
	public enum CEExceptionType
	{
		Unknown = 0,
		PostProcess = 1,
		IntegraPAYInProcess = 2,
	}
}
