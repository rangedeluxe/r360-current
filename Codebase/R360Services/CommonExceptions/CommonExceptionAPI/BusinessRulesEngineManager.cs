﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.BusinessRules;
using WFS.RecHub.Common;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.DAL.CommonExceptions;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.CommonException.CommonExceptionAPI
{
	internal class BusinessRulesEngineManager
	{
		BusinessRulesEngine _bre = null;
		private readonly bool _faked;
		public BusinessRulesEngineManager(IServiceContext serviceContext)
		{
			// TODO: Handle mocking
			_faked = false;
			_bre = new BusinessRulesEngine(serviceContext);
		}

		public static readonly Guid PaymentRTGuid = new Guid("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFF0");
		public static readonly Guid PaymentAccountGuid = new Guid("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFF1");
		public static readonly Guid PaymentSerialGuid = new Guid("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFF2");
		public static readonly Guid PaymentRemitterGuid = new Guid("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFF3");
		public static readonly Guid PaymentAmountGuid = new Guid("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFF4");
		public static readonly Guid[] StandardFieldGuids = { PaymentRTGuid, PaymentAccountGuid, PaymentSerialGuid, PaymentRemitterGuid, PaymentAmountGuid };

		public void ValidateBatch(object data)
		{
			throw new NotImplementedException();
		}

		public void ValidateTransaction(PostProcessTransactionInfo tran, out bool tranRequiresInvoice, out InvoiceBalancingOptions balancingOption, out IList<ColumnDefinition> stubFieldDefs, out IList<ColumnDefinition> paymentFieldDefs, out IList<PaymentItem> stubs, out IList<PaymentItem> payments)
		{
			tranRequiresInvoice = false;
			stubs = new List<PaymentItem>();
			payments = new List<PaymentItem>();

			if (_faked)
			{
				GetRules(tran.DataEntrySetupKey, tran.BatchSourceKey, tran.BatchPaymentTypeKey, null, out tranRequiresInvoice, out balancingOption, out stubFieldDefs, out paymentFieldDefs);

				FieldDataRecord nullField = new FieldDataRecord() { Value = string.Empty };

				foreach (var stub in tran.Stubs)
				{
					var item = new PaymentItem()
					{
						Id = GuidFromLong(stub.BatchSequence),
						FieldValues = new List<FieldDataAndInfo>(),
					};

					Random r = new Random((int)stub.BatchSequence);
					foreach (var fld in stubFieldDefs)
					{
						var fldInfo = new FieldDataAndInfo()
						{
							ColumnDefId = fld.Id,
							FieldValue = (stub.Fields.Find(o => o.DataEntryColumnKey == LongFromGuid(fld.Id)) ?? nullField).Value,
							AwaitingDecision = r.Next(2) == 1,
						};

						if (fldInfo.AwaitingDecision)
							fldInfo.DecisionMessage = "Mock Error for: " + fld.ColumnTitle;

						item.FieldValues.Add(fldInfo);
					}

					item.IsException = item.FieldValues.Any(o => o.AwaitingDecision);
					stubs.Add(item);
				}

				foreach (var payment in tran.Payments)
				{
					var item = new PaymentItem()
					{
						Id = GuidFromLong(payment.BatchSequence),
						FieldValues = new List<FieldDataAndInfo>(),
					};

					Random r = new Random((int)payment.BatchSequence);
					foreach (var fld in paymentFieldDefs)
					{
						var fldInfo = new FieldDataAndInfo()
						{
							ColumnDefId = fld.Id,
							FieldValue = (payment.Fields.Find(o => o.DataEntryColumnKey == LongFromGuid(fld.Id)) ?? nullField).Value,
							AwaitingDecision = r.Next(2) == 1,
						};

						if (fldInfo.AwaitingDecision)
							fldInfo.DecisionMessage = "Mock Error for: " + fld.ColumnTitle;

						item.FieldValues.Add(fldInfo);
					}

					item.IsException = item.FieldValues.Any(o => o.AwaitingDecision);
					payments.Add(item);
				}
			}
			else
			{
				TransactionDto tranInfo = new TransactionDto()
				{
					BatchSourceKey = (int)tran.BatchSourceKey,
					BatchPaymentTypeKey = (PaymentTypes)(int)tran.BatchPaymentTypeKey,
					BatchPaymentSubType = null,//this will get set as part of validating the transaction
					DataEntrySetupKey = tran.DataEntrySetupKey,
					IsTransactionException = false,
					Message = string.Empty,
					Items = new List<ItemDto>()
				};

				if (tran.Stubs != null)
				{
					foreach (var stub in tran.Stubs)
					{
						if (stub.Action != ExceptionItemAction.DeleteItem)
						{
							tranInfo.Items.Add(new ItemDto()
							{
								BatchSequence = stub.BatchSequence,
								IsPayment = false,
								Fields = stub.Fields == null ? null : stub.Fields.Select(o => new FieldDto()
								{
									DataEntryColumnKey = (int)o.DataEntryColumnKey,
									DateTimeValue = o.DateTimeValue,
									Value = o.Value
								}).ToList()
							});
						}
					}
				}

				if (tran.Payments != null)
				{
					foreach (var payment in tran.Payments)
					{
						if (payment.Action != ExceptionItemAction.DeleteItem)
						{
							tranInfo.Items.Add(new ItemDto()
							{
								BatchSequence = payment.BatchSequence,
								IsPayment = true,
								ItemData = new ItemDataDto()
								{
									RT = payment.ItemInfo.RT,
									Account = payment.ItemInfo.Account,
									Serial = payment.ItemInfo.Serial,
									RemitterName = payment.ItemInfo.RemitterName,
									Amount = payment.ItemInfo.Amount
								},
								Fields = payment.Fields == null ? null : payment.Fields.Select(o => new FieldDto()
								{
									DataEntryColumnKey = (int)o.DataEntryColumnKey,
									DateTimeValue = o.DateTimeValue,
									Value = o.Value
								}).ToList()
							});
						}
					}
				}

				if (!_bre.ValidateTransaction(tranInfo, false, false))
					throw new Exception("Failed to validate transaction.");

				tran.IsTransactionException = tranInfo.IsTransactionException;
				tran.Message = tranInfo.Message;

				foreach (var item in tranInfo.Items)
				{
					List<FieldDataAndInfo> fields = null;
					if (item.IsPayment && item.ItemData != null)
					{
						fields = new List<FieldDataAndInfo>
						{
							new	FieldDataAndInfo() { ColumnDefId = PaymentRTGuid, FieldValue = item.ItemData.RT },
							new	FieldDataAndInfo() { ColumnDefId = PaymentAccountGuid, FieldValue = item.ItemData.Account },
							new	FieldDataAndInfo() { ColumnDefId = PaymentSerialGuid, FieldValue = item.ItemData.Serial },
							new	FieldDataAndInfo() { ColumnDefId = PaymentRemitterGuid, FieldValue = item.ItemData.RemitterName },
							new	FieldDataAndInfo() { ColumnDefId = PaymentAmountGuid, FieldValue = item.ItemData.Amount }
						};
					}

					if (item.Fields != null)
					{
						if (fields == null)
							fields = new List<FieldDataAndInfo>();

						fields.AddRange(item.Fields.Select(o => new FieldDataAndInfo()
						{
							ColumnDefId = GuidFromLong(o.DataEntryColumnKey??0),
							AwaitingDecision = o.IsException,
							DecisionMessage = o.ExceptionMessage,
							FieldValue = o.Value
						}));
					}

					PaymentItem ceItem = new PaymentItem()
					{
						Id = GuidFromLong(item.BatchSequence),
						IsException = item.IsException,
						Messages = item.Messages,
						FieldValues = fields
					};

					if (item.IsPayment)
						payments.Add(ceItem);
					else
						stubs.Add(ceItem);
				}

				GetRules(tran.DataEntrySetupKey, tran.BatchSourceKey, tran.BatchPaymentTypeKey, tranInfo.BatchPaymentSubType, out tranRequiresInvoice, out balancingOption, out stubFieldDefs, out paymentFieldDefs);
			}
		}

		public void GetRules(long dataEntrySetupKey, int batchSourceKey, int batchPaymentTypeKey, PaymentSubTypeDto paymentSubType, out bool tranRequiresInvoice, out InvoiceBalancingOptions balancingOption, out IList<ColumnDefinition> stubFieldDefs, out IList<ColumnDefinition> paymentFieldDefs)
		{
			tranRequiresInvoice = false;
			balancingOption = InvoiceBalancingOptions.BalancingNotRequired;
			stubFieldDefs = new List<ColumnDefinition>();
			paymentFieldDefs = new List<ColumnDefinition>();

			if (_faked)
			{
				var fieldDefDto = new ColumnDefinition()
				{
					ColumnFieldName = "Amount",
					ColumnTitle = "Mock Amount",
					Editable = true,
					Id = GuidFromLong(421),
					Sequence = 1,
					Tooltip = "Amount",
					MaxLength = 20,
					FormatType = FormatTypes.Amount,
					Required = true,
					IsItemAmount = (string.Compare("Amount", "Amount", StringComparison.InvariantCultureIgnoreCase) == 0),
					HasBusinessRules = false,
					DataType = FieldDataTypes.Currency
				};
				stubFieldDefs.Add(fieldDefDto);

				fieldDefDto = new ColumnDefinition()
				{
					ColumnFieldName = "MockPaymentDate",
					ColumnTitle = "Mock Payment Date",
					Editable = true,
					Id = GuidFromLong(422),
					Sequence = 2,
					Tooltip = "Mock Payment Date",
					MaxLength = 10,
					FormatType = FormatTypes.Date,
					Mask = "MM/dd/yyyy",
					Required = true,
					IsItemAmount = (string.Compare("MockPaymentDate", "Amount", StringComparison.InvariantCultureIgnoreCase) == 0),
					HasBusinessRules = false,
					DataType = FieldDataTypes.Date
				};
				stubFieldDefs.Add(fieldDefDto);

				fieldDefDto = new ColumnDefinition()
				{
					ColumnFieldName = "MockData",
					ColumnTitle = "Mock Data",
					Editable = false,
					Id = GuidFromLong(423),
					Sequence = 3,
					Tooltip = "Mock Data",
					MaxLength = 100,
					FormatType = FormatTypes.Any,
					Required = false,
					IsItemAmount = (string.Compare("MockData", "Amount", StringComparison.InvariantCultureIgnoreCase) == 0),
					HasBusinessRules = false,
					DataType = FieldDataTypes.Text
				};
				stubFieldDefs.Add(fieldDefDto);
			}
			else
			{
				RuleDefinitionsDto rulesDto;
				_bre.GetRules(dataEntrySetupKey, out rulesDto);

				//hard code the standard payment fields to always display and never be editable
				paymentFieldDefs.Add(new ColumnDefinition() { ColumnFieldName = "RT", ColumnTitle = "R/T", Editable = false, Id = PaymentRTGuid, Sequence = 0, Tooltip = "R/T", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text });
				paymentFieldDefs.Add(new ColumnDefinition() { ColumnFieldName = "Account", ColumnTitle = "Account Number", Editable = false, Id = PaymentAccountGuid, Sequence = 0, Tooltip = "Account Number", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text });
				paymentFieldDefs.Add(new ColumnDefinition() { ColumnFieldName = "Serial", ColumnTitle = "Check/Trace/Ref Number", Editable = false, Id = PaymentSerialGuid, Sequence = 0, Tooltip = "Check/Trace/Ref Number", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text });
				paymentFieldDefs.Add(new ColumnDefinition() { ColumnFieldName = "Remitter", ColumnTitle = "Payer", Editable = false, Id = PaymentRemitterGuid, Sequence = 0, Tooltip = "Payer", MaxLength = 0, FormatType = FormatTypes.Any, IsItemAmount = false, HasBusinessRules = false, DataType = FieldDataTypes.Text });
				paymentFieldDefs.Add(new ColumnDefinition() { ColumnFieldName = "Amount", ColumnTitle = "Payment Amount", Editable = false, Id = PaymentAmountGuid, Sequence = 0, Tooltip = "Payment Amount", MaxLength = 0, FormatType = FormatTypes.Amount, IsItemAmount = true, HasBusinessRules = false, DataType = FieldDataTypes.Currency });

				if (rulesDto != null)
				{
					tranRequiresInvoice = rulesDto.TransactionRequiresInvoice;
					balancingOption = rulesDto.BalancingOption;
					if (rulesDto.FieldDefinitions != null)
					{
						foreach (var fieldDef in rulesDto.FieldDefinitions)
						{
							var rules = _bre.GetApplicableRules(batchSourceKey, batchPaymentTypeKey, paymentSubType, fieldDef);
							var colDef = new ColumnDefinition()
							{
								ColumnFieldName = fieldDef.FldName,
								ColumnTitle = fieldDef.DisplayName,
								Editable = rules != null ? rules.UserCanEdit : false,
								Id = GuidFromLong(fieldDef.DataEntryColumnKey),
								Sequence = fieldDef.ScreenOrder,
								Tooltip = fieldDef.DisplayName,
								MinLength = rules != null ? rules.MinLength : 0,
								MaxLength = rules != null && rules.MaxLength > 0 ? rules.MaxLength : fieldDef.FieldLength,
								FormatType = GetFormatType(fieldDef.DataType, rules != null ? (FormatTypes?)rules.FormatType : null),
								Mask = rules != null ? rules.Mask : null,
								Required = rules != null ? rules.Required : false,
								RequiredMessage = rules != null ? rules.RequiredMessage : null,
								IsItemAmount = (string.Compare(fieldDef.FldName, "Amount", StringComparison.InvariantCultureIgnoreCase) == 0),
								HasBusinessRules = rules != null,
								DataType = fieldDef.DataType
							};

							if (fieldDef.TableType == FieldTableTypes.Payments || fieldDef.TableType == FieldTableTypes.PaymentsDataEntry)
								paymentFieldDefs.Add(colDef);
							else
								stubFieldDefs.Add(colDef);
						}
					}
				}
			}
		}

		internal static FormatTypes GetFormatType(FieldDataTypes dataType, FormatTypes? formatType)
		{
			//if no format type was specified, translate the data type as that becomes more important
			if (formatType == null || formatType.Value == FormatTypes.Any)
			{
				if (dataType == FieldDataTypes.Currency)
					return FormatTypes.Amount;
				else if (dataType == FieldDataTypes.Date)
					return FormatTypes.Date;
				else if (dataType == FieldDataTypes.FloatingPoint)
					return FormatTypes.Number;
				else
					return FormatTypes.Any;
			}
			return formatType.Value;
		}

		private Guid GuidFromLong(long id)
		{
			byte[] rawId = BitConverter.GetBytes(id).Reverse().ToArray();					// Convert to bytes
			byte[] rawGuid = new byte[16];													// Initializes to all zeros...
			Array.Copy(rawId, 0, rawGuid, rawGuid.Length - rawId.Length, rawId.Length);		// Copy to end of byte array
			return new Guid(rawGuid);														// Convert to 128-bit GUID
		}

		private long LongFromGuid(Guid guid)
		{
			return BitConverter.ToInt64(guid.ToByteArray().Reverse().ToArray(), 0);
		}
	}
}
