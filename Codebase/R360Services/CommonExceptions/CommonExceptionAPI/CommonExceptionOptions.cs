using System;
using System.Collections;
using System.Collections.Specialized;
using WFS.RecHub.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 5/22/2013
*
* Purpose: Common Exception Option Class
*
* Modification History
* WI 101774 WJS  5/22/2013
*   - Initial Creation
*
*********************************************************************************/
namespace WFS.RecHub.CommonException.CommonExceptionAPI
{

	/// <summary>
	/// Summary description for CommonExceptionOptions.
	/// </summary>
	public class CommonExceptionOptions : cSiteOptions
	{
		private readonly TimeSpan _completeBatchMonitorInterval;
		private readonly bool _completeBatchMonitorDisabled;

		public CommonExceptionOptions(string vSiteKey)
			: base(vSiteKey)
		{
			// Apply appropriate default values
			_completeBatchMonitorInterval = TimeSpan.FromMinutes(5);
			_completeBatchMonitorDisabled = false;

			// Read Configured values
			StringCollection colSiteOptions = ipoINILib.GetINISection(siteKey);
			string strKey;
			string strValue;
			const string INIKEY_COMPLETEBATCHMONITORDISABLED = "CompleteBatchMonitorDisabled";
			const string INIKEY_COMPLETEBATCHMONITORMINUTES = "CompleteBatchMonitorMinutes";
			foreach (var entry in colSiteOptions)
			{
				var split = entry.Split(new char[] { '=' }, 2);
				if (split.Length == 2)
				{
					strKey = split[0];
					strValue = split[1];

					if (string.Compare(strKey, INIKEY_COMPLETEBATCHMONITORMINUTES, StringComparison.OrdinalIgnoreCase) == 0)
					{
						double minutes;
						if (double.TryParse(strValue, out minutes))
						{
							if (minutes > 0)
								_completeBatchMonitorInterval = TimeSpan.FromMinutes(minutes);
						}
					}

					if (string.Compare(strKey, INIKEY_COMPLETEBATCHMONITORDISABLED, StringComparison.OrdinalIgnoreCase) == 0)
					{
						bool disabled;
						if (bool.TryParse(strValue, out disabled))
						{
							_completeBatchMonitorDisabled = disabled;
						}
					}

				}
			}
		}

		public TimeSpan CompleteBatchMonitorInterval
		{
			get { return _completeBatchMonitorInterval; }
		}

		public bool CompleteBatchMonitorDisabled
		{
			get { return _completeBatchMonitorDisabled; }
		}
	}

}
