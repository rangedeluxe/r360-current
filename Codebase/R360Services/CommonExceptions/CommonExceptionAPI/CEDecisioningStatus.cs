﻿using System;
using WFS.RecHub.CommonException.Common;
using CE_DAL = WFS.RecHub.DAL.CommonExceptions;

namespace WFS.RecHub.CommonException.CommonExceptionAPI
{
	public enum CEDecisioningStatus
	{
		//See ToTransactionStatus for explanation of statuses and what's going on
		NoException = -1,
		Unresolved = 0,
		AcceptedPending = 1,
		RejectedPending = 2,
		Resolved = 3,
	}

	public static class DecisionStatusConversions
	{
		public static TransactionModel.DecisioningStatus ToTransactionStatus(this CEDecisioningStatus status)
		{
			//Originally the In-Process exceptions from integraPAY were the only thing in the 2.00 system.  Those came to us with a
			// DecisioningStatus which had a values of
			//		DecisioningStatus
			//			Unresolved = 0
			//			AcceptedPending = 1
			//			RejectedPending = 2
			//			Resolved = 3
			// Those values were not displayed as is on the screen, they were converted to be simply Unresolved or Pending, hence this method.
			//
			// With the addition of post process exceptions in 2.01 a new status type was added.  That status was
			// TransactionStatus which had values of 
			//		TransactionStatus (from RecHubException.TransactionStatuses)
			//			NoException = 0   (same thing as Resolved)
			//			Unresolved = 1
			//			Accepted = 2
			//			Rejected = 3
			//Once again, we will not display those values, but they must be converted.  They are converted two times.
			//The first conversion happens in the stored procedures which select both types of records at once.  To line up the statuses
			// in the returned dataset the stored procedures subtract one from the TransacationStatus.  This works to translate the
			// TransactionStatus to the DecisioningStatus, except for the NoException status.  For that we simply have to add
			// a new -1 value to our enumeration.  We can do that since all statuses are simply becoming Unresolved or Pending in
			// the end.

			switch (status)
			{
				case CEDecisioningStatus.Unresolved:
					return TransactionModel.DecisioningStatus.Unresolved;

				case CEDecisioningStatus.AcceptedPending:
				case CEDecisioningStatus.RejectedPending:
				default:
					return TransactionModel.DecisioningStatus.Pending;
			}
		}

		public static TransactionInfoModel.TransactionDecision ToTransactionDecision(this CEDecisioningStatus status)
		{
			switch (status)
			{
				case CEDecisioningStatus.Unresolved:
					return TransactionInfoModel.TransactionDecision.Unresolved;

				case CEDecisioningStatus.AcceptedPending:
					return TransactionInfoModel.TransactionDecision.Accepted;

				case CEDecisioningStatus.RejectedPending:
					return TransactionInfoModel.TransactionDecision.Rejected;

				default:
					return TransactionInfoModel.TransactionDecision.Unknown;
			}
		}

		public static CEDecisioningStatus ToCEDecisioningStatus(this CE_DAL.TransactionStatus status)
		{
			switch (status)
			{
				default:
				case CE_DAL.TransactionStatus.Unresolved:
					return CEDecisioningStatus.Unresolved;

				case CE_DAL.TransactionStatus.Accepted:
					return CEDecisioningStatus.AcceptedPending;

				case CE_DAL.TransactionStatus.Rejected:
					return CEDecisioningStatus.RejectedPending;

				case CE_DAL.TransactionStatus.NoException:
					return CEDecisioningStatus.Resolved;
			}
		}

		public static CE_DAL.TransactionStatus ToTransactionStatus(this ExceptionAction action)
		{
			switch (action)
			{
				case ExceptionAction.Accept:
					return CE_DAL.TransactionStatus.Accepted;

				case ExceptionAction.Reject:
					return CE_DAL.TransactionStatus.Rejected;

				default:
				case ExceptionAction.NoAction:
					return CE_DAL.TransactionStatus.Unresolved;
			}
		}
	}
}
