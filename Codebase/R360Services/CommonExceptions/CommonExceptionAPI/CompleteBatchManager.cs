﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.DAL;
using OLSvc = WFS.RecHub.OLDecisioningServicesClient;

namespace WFS.RecHub.CommonException.CommonExceptionAPI
{
	internal static class CompleteBatchManager
	{
		private const string LogSource = "CECmpltBatchMngrThrd";

		private static readonly object _completeBatchLock = new object();
		private static AutoResetEvent _batchQueuedForCompletion = new AutoResetEvent(false);
		private static List<int> _completionBatchIds = new List<int>();

		static CompleteBatchManager()
		{
			// Start the background monitoring thread...
			StartBatchCompletionThread();
		}

		public static void Init()
		{
			// Initialization is actually handled by the static constructor -- but this gives other classes something to call that will invoke the static constructor...
		}

		public static void CompleteBatch(int globalBatchId)
		{
			// Add the batch ID to the list (thread-safe)
			lock (_completeBatchLock)
			{
				_completionBatchIds.Add(globalBatchId);
			}

			// Signal background thread to process it.
			_batchQueuedForCompletion.Set();
		}

		private static void StartBatchCompletionThread()
		{
			Thread t = new Thread(BatchCompletionThread);
			t.IsBackground = true; // Allow this thread to be shut down automatically when the service shuts down
			t.Name = "CompleteBatch";
			t.Start();
		}

		private static void BatchCompletionThread()
		{
			// In order to allow for auto-recovery, we will monitor the Common Exceptions table for "outstanding" batches
			// To simplify multi-threading - all batch completions will be done from this background thread to reduce the chance of duplicate actions.

			// Note: This is not really a "monitor continuously" thread, since the service is hosted in IIS (i.e. it can shutdown due to idle timeouts...)
			//  - But we'd hope that all outstanding batches could be completed prior to idle timeout 
			//  - And restarting this thread would be as simple as a user checking the status / dashboard for Exceptions...

			TimeSpan roundingInterval = TimeSpan.FromSeconds(1);
			TimeSpan monitorInterval;
			try
			{
				monitorInterval = SiteOptions.CompleteBatchMonitorInterval;
			}
			catch (Exception)
			{
				monitorInterval = TimeSpan.FromMinutes(5);
			}

			bool monitorDisabled = false;
			DateTime nextScan = DateTime.Now + TimeSpan.FromSeconds(10); // Perform first scan 10 seconds after service startup (i.e. delayed start)
			try
			{
				if (SiteOptions.CompleteBatchMonitorDisabled)
				{
					monitorDisabled = true;
					EventLog.logEvent("Monitor Loop is disabled", LogSource, MessageType.Information, MessageImportance.Verbose);
				}
				else
				{
					EventLog.logEvent(string.Format("Starting Monitor Loop at {0} with checks every {1}", nextScan.ToShortTimeString(), monitorInterval.ToString()),
						LogSource, MessageType.Information, MessageImportance.Verbose);
				}
			}
			catch
			{
			}

			while (true)
			{
				try
				{
					// Wait for a signal / timeout
					if (monitorDisabled)
						_batchQueuedForCompletion.WaitOne();
					else
					{
						TimeSpan waitInterval = nextScan - DateTime.Now + roundingInterval;
						if (waitInterval > TimeSpan.Zero)
							_batchQueuedForCompletion.WaitOne(waitInterval);
					}

					// Get list of pending batches
					int[] batchIds;
					lock (_completeBatchLock)
					{
						batchIds = _completionBatchIds.ToArray();
						_completionBatchIds.Clear();
					}

					// Process them
					CompleteIntegraPAYBatches(batchIds, false);

					// Check for outstanding batches
					if (!monitorDisabled && DateTime.Now >= nextScan)
					{
						DataTable dt;
						if (CommonExceptDAL.GetBatchesPendingCompletion(out dt))
						{
							if (dt.Rows.Count > 0)
							{
								// Separate based on batch type
								List<int> integraPayBatchIds = new List<int>();
								List<long> postProcessBatchKeys = new List<long>();
								for (int i = 0; i < dt.Rows.Count; i++)
								{
									var exceptionType = (CEExceptionType)Convert.ToByte(dt.Rows[i]["ExceptionType"]);
									if (exceptionType == CEExceptionType.IntegraPAYInProcess)
										integraPayBatchIds.Add(Convert.ToInt32(dt.Rows[i]["GlobalBatchID"]));
									else if (exceptionType == CEExceptionType.PostProcess)
										postProcessBatchKeys.Add(Convert.ToInt64(dt.Rows[i]["ExceptionBatchKey"]));
								}

								if (integraPayBatchIds.Count > 0)
									CompleteIntegraPAYBatches(integraPayBatchIds.ToArray(), true);

								if (postProcessBatchKeys.Count > 0)
									CompletePostProcessBatches(postProcessBatchKeys.ToArray());
							}
						}
						else
						{
							EventLog.logEvent("Error in call to GetBatchesPendingCompletion", LogSource, MessageType.Error, MessageImportance.Essential);
						}

						nextScan = nextScan + monitorInterval;
					}
				}
				catch (ThreadAbortException)
				{
					EventLog.logEvent("Shutting Down...", LogSource, MessageType.Information, MessageImportance.Verbose);

					// Thread abort is not processed
					throw;
				}
				catch (Exception ex)
				{
					// Log the error, but continue the loop (forever)
					EventLog.logEvent("A general exception occurred", LogSource, MessageType.Error, MessageImportance.Essential);
					EventLog.logError(ex, LogSource, "MonitorLoop");

					if (!monitorDisabled && nextScan < DateTime.Now)
					{
						// Delay the next processing for at least one minute, so we don't flood the log / service with a repeated error...
						nextScan = DateTime.Now + TimeSpan.FromMinutes(1);
					}
				}
			}
		}

		private static void CompletePostProcessBatches(long[] batchKeys)
		{
			foreach (long key in batchKeys)
			{
				if (!CommonExceptDAL.MarkPostProcessBatchComplete(key))
				{
					EventLog.logEvent("Error in call to MarkPostProcessBatchComplete", LogSource, MessageType.Error, MessageImportance.Essential);
				}
			}
		}

		private static void CompleteIntegraPAYBatches(int[] batchIds, bool verifyState)
		{
			OLSvc.IOLDecisioningSvcClient olDecisioningClient = OLSvc.OLDecisioningSvcClient.CreateClient(SiteKey, APIConstants.OLDecisioning.ServiceUserId);

			foreach (int batchId in batchIds)
			{
				try
				{
					bool batchComplete = false;

					// If this is an auto-recovery attempt, verify state first
					if (verifyState)
					{
						var batch = olDecisioningClient.GetBatch(batchId);
						if (batch.DepositStatus == DepositStatusValues.DepositPendingDecision)
						{
							// Why don't we have the batch locked?  Try to lock it and continue...
							var response = olDecisioningClient.CheckOutBatch(batchId);
							if (!response.Success)
								throw new Exception("Checkout Batch for batch ID " + batchId.ToString() + " failed with the following messages:\r\n" + string.Join("\r\n", response.Messages));
						}
						else if (batch.DepositStatus != DepositStatusValues.DepositInDecision)
						{
							// Batch is not in a state that needs completing, so skip the "completion" and just update our database to "promoted"
							EventLog.logEvent(string.Format("Batch ID {0} had an unexpected status of {1} - it will be marked as Resolved / Promoted", batchId, batch.DepositStatus),
								LogSource, MessageType.Warning, MessageImportance.Essential);
							batchComplete = true;
						}
					}

					// Call the OL Decisioning Service to verify / promote the batch
					if (!batchComplete)
					{
						var response = olDecisioningClient.CompleteBatch(batchId, null);
						if (response.Success)
						{
							batchComplete = true;
						}
						else
						{
							// Report the error messages...
							throw new Exception("Complete Batch for batch ID " + batchId.ToString() + " failed with the following messages:\r\n" + string.Join("\r\n", response.Messages));
						}
					}

					// Update our state to reflect the current status of the batch
					if (batchComplete)
					{
						if (!CommonExceptDAL.MarkIntegraPAYBatchComplete(batchId))
						{
							EventLog.logEvent("Error in call to MarkBatchComplete", LogSource, MessageType.Error, MessageImportance.Essential);
						}
					}
				}
				catch (Exception ex)
				{
					EventLog.logEvent("Complete Batch failed for batch ID: " + batchId.ToString(), LogSource, MessageType.Error, MessageImportance.Essential);
					EventLog.logError(ex, LogSource, "CompleteBatches");
				}
			}
		}

		#region Error Handling / Logging / Site Options

		private static cEventLog _eventLog = null;
		private static CommonExceptionOptions _siteOptions = null;
		private static string _siteKey = null;

		/// <summary>
		/// This manager class processes work for "all" sites -- so it uses a site key as defined in the configuration file.  
		/// This means it is effectively impossible to (generically) support multiple site keys for Common Exceptions, since there is no way to differentiate between sites during auto-recovery of batch completion...
		/// </summary>
		private static string SiteKey
		{
			get
			{
				if (_siteKey == null)
				{
					try
					{
						_siteKey = ConfigurationManager.AppSettings["siteKey"] ?? string.Empty;
					}
					catch
					{
						_siteKey = string.Empty;
					}
					EventLog.logEvent("Using SiteKey = " + _siteKey, LogSource, MessageType.Information, MessageImportance.Debug);
				}
				return _siteKey;
			}
		}

		/// <summary>
		/// Retrieve site options from the local .ini file.
		/// </summary>
		private static CommonExceptionOptions SiteOptions
		{
			get
			{
				if (_siteOptions == null)
					_siteOptions = new CommonExceptionOptions(SiteKey);

				return _siteOptions;
			}
		}

		/// <summary>
		/// Logging component using settings from the local siteOptions object.
		/// </summary>
		private static cEventLog EventLog
		{
			get
			{
				if (_eventLog == null)
				{
					var options = SiteOptions;
					_eventLog = new cEventLog(options.logFilePath,
											  options.logFileMaxSize,
											  (MessageImportance)options.loggingDepth);
				}

				return _eventLog;
			}
		}

		#endregion

		#region DAL

		private static CommonExceptionDAL _CommonExceptionDAL = null;

		/// <summary>
		/// 
		/// </summary>
		private static CommonExceptionDAL CommonExceptDAL
		{
			get
			{
				if (_CommonExceptionDAL == null)
				{
					//if (SiteKey.Contains("[MockCommonExceptionDAL]"))
					//{
					//	_CommonExceptionDAL = new MockCommonExceptionDAL();
					//}
					//else
					//{
					_CommonExceptionDAL = new CommonExceptionDAL(SiteKey);
					//}
				}
				return (_CommonExceptionDAL);
			}
		}

		#endregion
	}
}
