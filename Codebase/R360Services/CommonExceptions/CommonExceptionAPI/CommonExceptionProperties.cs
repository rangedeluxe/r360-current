﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.CommonException.CommonExceptionAPI
{
	internal class CommonExceptionProperties
	{
		public static CommonExceptionProperties FromDataRow(DataRow row)
		{
			return new CommonExceptionProperties(row);
		}

		private readonly DataRow _sourceData;
		private CommonExceptionProperties(DataRow row)
		{
			_sourceData = row;
		}

		public bool LockCreated { get { return Convert.ToBoolean(_sourceData["LockCreated"]); } }
		public CEExceptionType ExceptionType { get { return (CEExceptionType)Convert.ToByte(_sourceData["ExceptionType"]); } }
		
		// IntegraPAY In Process Exception IDs
		public int GlobalBatchID { get { return Convert.ToInt32(_sourceData["GlobalBatchID"]); } }
		public int TransactionID { get { return Convert.ToInt32(_sourceData["TransactionID"]); } }

	}
}
