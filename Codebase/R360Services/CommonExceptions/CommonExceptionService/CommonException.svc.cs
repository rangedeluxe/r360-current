﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.CommonException.CommonExceptionAPI;
using WFS.RecHub.R360Shared;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 5/22/2013
*
* Purpose: Common Exception WCF Interface
*
* Modification History
* WI 101774 WJS  5/22/2013
*   - Initial Creation*
* WI 101852 DRP 6/5/2013
*   - Adding Image Retrieval
* WI 107711 CEJ 10/16/2013
*   Create the ability log userActivity in the sessionLog from the R360 Services
* WI 143442 DJW 05/30/2014
*     Updated to handle batch collisions (BatchID to long, and added BatchSourceID)
* WI 175558 SAS 10/31/2014
*  Update ImportTypeShortName field to BatchSourceName   
*********************************************************************************/
namespace WFS.RecHub.CommonException
{
	public class CommonException : LTAService, ICommonException
	{
		public CommonException()
		{
		}

		// Ping
		public PingResponse Ping()
		{
            return FailOnError((ignored) =>
			{
                PingResponse response = new PingResponse();
				response.responseString = "Pong";
				return response;
			});
		}

		public TransactionsResponse GetTransactionsPendingDecision()
		{
            return FailOnError((objBatchDecision) =>
			{
				return ((CEDecisioning)objBatchDecision).GetTransactionsPendingDecision();
			});
		}

        public BaseResponse UnlockTransaction(int commonExceptionID)
		{
            return FailOnError((objBatchDecision) =>
			{
                return ((CEDecisioning)objBatchDecision).UnlockTransaction(commonExceptionID);
			});
		}

		public TransactionDetailsResponse ViewTransaction(int commonExceptionID)
		{
			return FailOnError((objBatchDecision) =>
			{
                return ((CEDecisioning)objBatchDecision).ViewTransaction(commonExceptionID);
			});
		}

		public TransactionDetailsResponse CheckoutTransaction(int commonExceptionID)
		{
            return FailOnError((objBatchDecision) =>
			{
                return ((CEDecisioning)objBatchDecision).CheckoutTransaction(commonExceptionID);
			});
		}

		public CheckinResponse CheckinTransaction(int commonExceptionID, ExceptionAction transactionDecision, bool overrideRules, bool forceBalance, IEnumerable<PaymentItemUpdate> stubUpdates, IEnumerable<PaymentItemUpdate> paymentUpdates)
		{
			return FailOnError((objBatchDecision) =>
			{
                return ((CEDecisioning)objBatchDecision).CheckinTransaction(commonExceptionID, transactionDecision, overrideRules, forceBalance, stubUpdates, paymentUpdates);
			});
		}

		public ReturnItem ServeImage(CEImageRequest imageRequest)
		{
			try
			{
				CEDecisioning api = new CEDecisioning(ServiceContext);
				api.LogEvent += EventLog.logEvent;
				LogManager.Logger = EventLog.IPOtoILogger(this.GetType().Name);

				var transactionResponse = api.GetTransaction(imageRequest.CommonExceptionID);
				if (transactionResponse.Status == StatusCode.SUCCESS)
				{
					LTAImageInfoContract imgRequest = new LTAImageInfoContract()
					{
						BankID = transactionResponse.Transaction.BankId,
						SourceBatchID = transactionResponse.Transaction.SourceBatchID,
						BatchSequence = imageRequest.BatchSequence,
						ColorMode = (short)imageRequest.ColorMode,
						IsBack = imageRequest.IsBack,
						FileDescriptor = imageRequest.FileDescriptor,
						LockboxID = transactionResponse.Transaction.ClientAccountID,
						ProcessingDateKey = int.Parse(DateTime.ParseExact(transactionResponse.Transaction.ProcessingDate, ServiceConstants.DateTimeFormats.DefaultDateFormat, System.Globalization.CultureInfo.InvariantCulture)
							.ToString(ServiceConstants.DateTimeFormats.InvariantDateFormat)),
						AcceptTiffFormat = false,
						BatchSourceName = transactionResponse.Transaction.BatchSourceShortName,
						ImportTypeShortName = transactionResponse.Transaction.ImportTypeShortName,
						BatchID = transactionResponse.Transaction.BatchId,
                        PaymentType = transactionResponse.Transaction.PaymentTypeId

					};

					ImageRetrieval irServer = new ImageRetrieval(ServiceContext.SiteKey);
					return irServer.ServeImage(imgRequest, api.GetSession());
				}
				else
				{
					throw new Exception("Could not retrieve transaction info: " + string.Join(", ", transactionResponse.Errors));
				}
			}
			catch (Exception ex)
			{
				LogGeneralException(ex, "ServeImage");
				return new ReturnItem() { bRetVal = false };
			}
		}

		#region Business Rules Support

		public GenericResponse<DefaultBRErrorMessagesDto> GetDefaultBRErrorMessages()
		{
			return FailOnError((objBatchDecision) =>
			{
				return ((CEDecisioning)objBatchDecision).GetDefaultBRErrorMessages();
			});
		}

		#endregion

		public BaseResponse ProcessStagedTransactions()
		{
			return FailOnError((objBatchDecision) =>
			{
				return ((CEDecisioning)objBatchDecision).ProcessStagedTransactions();
			});
		}

		protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
		{
            return new CEDecisioning(serviceContext);
        }
    }
}