﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.CommonException;
using WFS.RecHub.R360Shared;
using System.Threading;
using System.Xml.Linq;
using System.Reflection;
using System.IO;
using CommonExceptionTestClient;
using System.Security.Claims;
using System.Linq;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.Common;
using System.Collections.Generic;

namespace CommonExceptionIntegrationTesting
{
	[TestClass]
	public class DatabaseStatusTest
	{
		[TestCategory("Integration")]
		[TestMethod]
		public void Integration_VerifyDatabaseStatusUpdates_NoErrors()
		{
			// Step: Populate the database to a known Fake state
			XDocument dbConnParams = XDocument.Load(Path.Combine(Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath), "DatabaseConnection.xml"));

			// Database connection parameters are loaded from a configuration file; as necessary, update the configuration file to point to a valid database...
			MockDataParms dbParms = new MockDataParms() { Server = dbConnParams.Root.Element("ServerName").Value, Database = dbConnParams.Root.Element("DatabaseName").Value };
			var dbUser = dbConnParams.Root.Element("UserName");
			if (dbUser != null && !string.IsNullOrWhiteSpace(dbUser.Value))
			{
				dbParms.User = dbUser.Value;
				dbParms.Password = dbConnParams.Root.Element("Password").Value;
			}
			MockData.Synchronize(dbParms);


			// Step: Fake the ambient context to represent the Mock User / Session defined in the Mock Data
			R360ServiceContext.UseFakeServiceContext = true;


			// Step: Hook the log (Fake the standard log)
			cEventLog.UseFakeInMemEventLog = true;
			InMemoryEventLog.Instance.Clear();


			// Step: TODO: Fake the BRE


			// Step: Issue Ping - verify we can create the service instance
			var service = new CommonException();
			var response = service.Ping();
			Assert.AreEqual("Pong", response.responseString, InMemoryEventLog.Instance.ToString());

			// Step: Verify Background Monitor thread starts when service instance is created
			Thread.Sleep(100); // Give enough time to start the background thread...
			Assert.IsTrue(InMemoryEventLog.Instance.EventList.Exists(o => o.Message.StartsWith("Starting Monitor Loop")), "Background thread not initialized\r\n" + InMemoryEventLog.Instance.ToString());


			// Step: Issue GetTransactions; should return 4 transactions for the Fake User with the Fake data
			var tranResponse = service.GetTransactionsPendingDecision();
			Assert.AreEqual(StatusCode.SUCCESS, tranResponse.Status, InMemoryEventLog.Instance.ToString());
			Assert.AreEqual(4, tranResponse.Transactions.Count, InMemoryEventLog.Instance.ToString());
			Assert.AreEqual(1, tranResponse.Transactions.Where(o => o.ExceptionType == TransactionModel.ExceptionTypes.InProcess).Count(), InMemoryEventLog.Instance.ToString());
			Assert.AreEqual(3, tranResponse.Transactions.Where(o => o.ExceptionType == TransactionModel.ExceptionTypes.Posting).Count(), InMemoryEventLog.Instance.ToString());

			// Step: Issue Checkout / Unlock / Checkin operations
			{
				// Checkout first transaction - should succeed
				var checkoutResponse = service.CheckoutTransaction(tranResponse.Transactions[0].CommonExceptionId);
				Assert.AreEqual(StatusCode.SUCCESS, checkoutResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsTrue(checkoutResponse.LockCreated, InMemoryEventLog.Instance.ToString());

				// Verify fields
				Assert.AreEqual(1, checkoutResponse.TransactionInfo.StubItems.FieldDefinitions.Count);
				Assert.AreEqual("MockAcctNum", checkoutResponse.TransactionInfo.StubItems.FieldDefinitions[0].ColumnFieldName);
				Assert.AreEqual(1, checkoutResponse.TransactionInfo.StubItems.Items.Count);
				Assert.AreEqual(1, checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues.Count);
				Assert.IsTrue(checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues[0].AwaitingDecision);
			}
			{
				// Unlock first transaction - should succeed
				var checkinResponse = service.CheckinTransaction(tranResponse.Transactions[0].CommonExceptionId, ExceptionAction.NoAction, false, false, null, null);
				Assert.AreEqual(StatusCode.SUCCESS, checkinResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsFalse(checkinResponse.NotLockedByCurrentUser, InMemoryEventLog.Instance.ToString());
			}
			{
				// Repeat unlock - should fail
				var checkinResponse = service.CheckinTransaction(tranResponse.Transactions[0].CommonExceptionId, ExceptionAction.NoAction, false, false, null, null);
				Assert.AreEqual(StatusCode.FAIL, checkinResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsTrue(checkinResponse.NotLockedByCurrentUser, InMemoryEventLog.Instance.ToString());
			}
			{
				// Checkout again - should succeed
				var checkoutResponse = service.CheckoutTransaction(tranResponse.Transactions[0].CommonExceptionId);
				Assert.AreEqual(StatusCode.SUCCESS, checkoutResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsTrue(checkoutResponse.LockCreated, InMemoryEventLog.Instance.ToString());
			}
			{
				// Accept first transaction - should succeed
				var checkinResponse = service.CheckinTransaction(tranResponse.Transactions[0].CommonExceptionId, ExceptionAction.Accept, false, false, null, null);
				Assert.AreEqual(StatusCode.SUCCESS, checkinResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsFalse(checkinResponse.NotLockedByCurrentUser, InMemoryEventLog.Instance.ToString());
			}
			{
				// Checkout again - should fail to lock (no longer unresolved)
				var checkoutResponse = service.CheckoutTransaction(tranResponse.Transactions[0].CommonExceptionId);
				Assert.AreEqual(StatusCode.SUCCESS, checkoutResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsFalse(checkoutResponse.LockCreated, InMemoryEventLog.Instance.ToString());
			}
			{
				// Reject 2nd transaction
				var checkoutResponse = service.CheckoutTransaction(tranResponse.Transactions[1].CommonExceptionId);
				Assert.AreEqual(StatusCode.SUCCESS, checkoutResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsTrue(checkoutResponse.LockCreated, InMemoryEventLog.Instance.ToString());

				// Verify fields
				Assert.AreEqual(3, checkoutResponse.TransactionInfo.StubItems.FieldDefinitions.Count);
				Assert.AreEqual("Amount", checkoutResponse.TransactionInfo.StubItems.FieldDefinitions[0].ColumnFieldName);
				Assert.AreEqual("MockPaymentDate", checkoutResponse.TransactionInfo.StubItems.FieldDefinitions[1].ColumnFieldName);
				Assert.AreEqual("MockData", checkoutResponse.TransactionInfo.StubItems.FieldDefinitions[2].ColumnFieldName);
				Assert.AreEqual(3, checkoutResponse.TransactionInfo.StubItems.Items.Count);
				Assert.AreEqual(3, checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues.Count);
				Assert.IsTrue(checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues[0].AwaitingDecision);
				Assert.AreEqual(checkoutResponse.TransactionInfo.StubItems.FieldDefinitions[0].Id, checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues[0].ColumnDefId);
				Assert.AreEqual(50.01m, decimal.Parse(checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues[0].FieldValue));

				var checkinResponse = service.CheckinTransaction(tranResponse.Transactions[1].CommonExceptionId, ExceptionAction.Reject, false, false, null, null);
				Assert.AreEqual(StatusCode.SUCCESS, checkinResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsFalse(checkinResponse.NotLockedByCurrentUser, InMemoryEventLog.Instance.ToString());
			}

			{
				// Accept 3rd transaction, with updates
				var checkoutResponse = service.CheckoutTransaction(tranResponse.Transactions[2].CommonExceptionId);
				Assert.AreEqual(StatusCode.SUCCESS, checkoutResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsTrue(checkoutResponse.LockCreated, InMemoryEventLog.Instance.ToString());
				Assert.AreEqual(1, checkoutResponse.TransactionInfo.StubItems.Items.Count);
				Assert.AreEqual(3, checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues.Count);
				Assert.IsTrue(checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues[0].AwaitingDecision);
				Assert.AreEqual(checkoutResponse.TransactionInfo.StubItems.FieldDefinitions[0].Id, checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues[0].ColumnDefId);
				Assert.AreEqual(120.07m, decimal.Parse(checkoutResponse.TransactionInfo.StubItems.Items[0].FieldValues[0].FieldValue));

				PaymentItemUpdate update = new PaymentItemUpdate()
				{
					Id = checkoutResponse.TransactionInfo.StubItems.Items[0].Id,
					Action = ExceptionItemAction.UpdateItem,
					FieldValues = new List<FieldData>( new FieldData[] {
						new FieldData() { ColumnDefId = checkoutResponse.TransactionInfo.StubItems.FieldDefinitions[0].Id, FieldValue = 127.77m.ToString() },
						new FieldData() { ColumnDefId = checkoutResponse.TransactionInfo.StubItems.FieldDefinitions[1].Id, FieldValue = "04/01/2000" } }),
				};

				var checkinResponse = service.CheckinTransaction(tranResponse.Transactions[2].CommonExceptionId, ExceptionAction.Accept, false, false, new PaymentItemUpdate[] { update }, null);
				Assert.AreEqual(StatusCode.SUCCESS, checkinResponse.Status, InMemoryEventLog.Instance.ToString());
				Assert.IsFalse(checkinResponse.NotLockedByCurrentUser, InMemoryEventLog.Instance.ToString());
			}


			// Step: Manually update database, and check result of background thread on batch recovery
			int minIndex = InMemoryEventLog.Instance.EventList.Count;
			string expected = "Executing Procedure : EXEC RecHubException.usp_ExceptionBatches_Upd_BatchStatus @parmExceptionBatchKey = ";
			MockData.SetTransactionStatus(dbParms, tranResponse.Transactions[3].CommonExceptionId, 2);
			DateTime end = DateTime.UtcNow + TimeSpan.FromSeconds(20);
			while (DateTime.UtcNow < end)
			{
				Thread.Sleep(500);
				if (InMemoryEventLog.Instance.EventList.Skip(minIndex).Any(o => o.Message.StartsWith(expected)))
					break;
			}
			Assert.IsTrue(InMemoryEventLog.Instance.EventList.Skip(minIndex).Any(o => o.Message.StartsWith(expected)), InMemoryEventLog.Instance.ToString());

			// Step: Check the log messages to see if any messages of type "Error" were reported...
			var errors = InMemoryEventLog.Instance.EventList.Where(o => o.Error != null || o.Importance == MessageImportance.Essential || o.Type == MessageType.Error).ToList();
			if (errors.Count > 0)
				Assert.Fail("Errors: \r\n" + errors.Select(o => o.ToString()).Aggregate((s1, s2) => s1 + "\r\n" + s2));

			// Step: Manually query database, and verify the Fake transactions / batches all have their correct statuses
			MockData.VerifyComplete(dbParms, tranResponse.Transactions.Select(o => o.CommonExceptionId));
		}
	}
}
