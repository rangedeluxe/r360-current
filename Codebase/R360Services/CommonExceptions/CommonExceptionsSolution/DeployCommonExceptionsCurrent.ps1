param(
	[string]$iteration = "",
	[string]$area = "",
	[string]$buildNumber = ""
)

#Shell to Common R360Services Deployment Script, with correct parameters
$thisScript = Split-Path $MyInvocation.MyCommand.Path 
$commonScript = Join-Path $thisScript "..\..\ServiceShares\R360BuildDeployment.ps1"
$commonScript = [System.IO.Path]::GetFullPath($commonScript)

$commandWithParameters = "$commonScript" `
    + " -service RecHub.R360_Service.CommonExceptionsService" `
    + " -appConfig:web.config" `
		+ " -configSubFolder:_PublishedWebsites\CommonExceptionService" `
    + " -folderName:RecHubExceptionsService" `
    + " -buildNumber:$buildNumber" `
	+ " -iteration:`"$iteration`"" `
	+ " -area:`"$area`"" `
    + " -dlls:`"" `
		+ "BusinessRulesEngine.dll," `
	    + "CommonExceptionAPI.dll," `
	    + "CommonExceptionCommon.dll," `
	    + "CommonExceptionDAL.dll," `
	    + "CommonExceptionService.dll," `
		+ "DALBase.dll," `
	    + "ipoCrypto.dll," `
	    + "ipoDB.dll," `
	    + "ipoImages.dll," `
	    + "ipoLib.dll," `
	    + "ipoLog.dll," `
	    + "ipoPICS.dll," `
	    + "ltaCommon.dll," `
	    + "ltaLog.dll," `
	    + "OLDecisioningServicesClient.dll," `
	    + "OLFServicesClient.dll," `
	    + "OLFServicesCommon.dll," `
	    + "OLFServicesDAL.dll," `
	    + "PermissionDAL.dll," `
		+ "R360RaamClient.dll," `
	    + "R360Shared.dll," `
	    + "SessionDAL.dll," `
	    + "Wfs.Raam.Core.dll," `
		+ "Wfs.Raam.Service.Authorization.Contracts.dll," `
	    + "Thinktecture.IdentityModel.dll" `
        + "`"" `
    + " -web" `

Write-Host "Invoking Deployment Script with parameters: " $commandWithParameters

invoke-expression $commandWithParameters

Write-Host "Deployment Complete"

