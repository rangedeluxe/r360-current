﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.CommonException.CommonExceptionDTO;
using WFS.RecHub.CommonExceptionServicesClient;
using WFS.RecHub.R360Services.Common;

namespace CommonExceptionUnitTests
{
	[TestClass]
	public class MockClientTests
	{
		[TestMethod]
		public void MockTest()
		{
			MockCommonExceptionManager mgr = new MockCommonExceptionManager();
			var response = mgr.GetTransactionsPendingDecision();
			Assert.AreEqual(StatusCode.SUCCESS, response.Status);
			Assert.AreEqual(5, response.Transactions.Count);

			var response2 = mgr.CheckoutTransaction(response.Transactions[3].CommonExceptionId);
			Assert.AreEqual(StatusCode.SUCCESS, response2.Status);

			var response3 = mgr.ServeImage(new LTAImageInfoContract()
			{
				BankID = response.Transactions[3].BankId,
				BatchID = response.Transactions[3].BatchId,
				BatchSequence = response2.TransactionImages[0].BatchSequence,
				IsBack = false
			});

			response3.data.CopyTo(new FileStream(@"C:\Temp\MockImage.png", FileMode.Create));
		}
	}
}
