﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using WFS.RecHub.CommonException.common;
using WFS.RecHub.CommonException.CommonExceptionDTO;
using WFS.RecHub.CommonExceptionDTO;
using WFS.RecHub.CommonExceptionServicesClient;
using WFS.RecHub.R360Services.Common;

namespace CommonExceptionUnitTests
{
	[TestClass]
	public class ClientTests
	{
		CommonExceptionManager _mgr = new CommonExceptionManager();
		private TransactionsResponse _pendingTransactions;
		private int _commonExceptionId;
		private LockTransactionResponse _checkedOutTransaction;

		[TestMethod]
		public void GetPendingTransactions()
		{
			// Test app uses a hard-coded identity
			ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.Sid, "{4a183c6f-13d6-4303-bf7a-f8b32deda643}") }));
				// DOT Database = userId 10 (Wayne)
				// Wayne Database = userId 2
 
			var response = _mgr.GetTransactionsPendingDecision();
			Assert.AreEqual(StatusCode.SUCCESS, response.Status);

			// Store for following calls
			_pendingTransactions = response;
		}

		[TestMethod]
		public void Checkout()
		{
			// Use higher-level test method
			GetPendingTransactions();
			_commonExceptionId = _pendingTransactions.Transactions.First(o => o.Status == TransactionModel.DecisioningStatus.Unresolved 
				&& o.ClientAccountID <= 105 // Right now the data only seems to be valid for lockboxes 103, 104, and 105...
			).CommonExceptionId;

			var response = _mgr.CheckoutTransaction(_commonExceptionId);
			Assert.AreEqual(StatusCode.SUCCESS, response.Status);

			// Store for following calls
			_checkedOutTransaction = response;
		}

		[TestMethod]
		public void UpdateItemFieldsInvalidDataType()
		{
			// Use higher-level test method
			Checkout();

			// Enumerate the exception items and fields...
			var item = _checkedOutTransaction.TransactionItems.First(o => o.IsException);
			PaymentItemUpdate update = new PaymentItemUpdate()
			{
				Action = ExceptionItemAction.UpdateItem,
				Id = item.Id,
				FieldValues = new FieldData[] { new FieldData() { ColumnDefId = _checkedOutTransaction.FieldDefinitions.First(o => o.DataType == FieldDataTypes.Currency).Id, FieldValue = "TestValue" } }
			};

			// Call the update method
			var response = _mgr.CheckinTransaction(_commonExceptionId, ExceptionAction.NoAction, new PaymentItemUpdate[] { update });
			Assert.AreEqual(StatusCode.FAIL, response.Status);
			Assert.AreEqual(1, response.Errors.Count);
			Assert.AreEqual("Error: (15008) Invalid Decimal value [TestValue] specified for Data Entry Field [Amount].", response.Errors[0]);
		}

		[TestMethod]
		public void UpdateItemFieldsValidDataType()
		{
			// Use higher-level test method
			Checkout();

			// Enumerate the exception items and fields...
			var item = _checkedOutTransaction.TransactionItems.First(o => o.IsException);
			PaymentItemUpdate update = new PaymentItemUpdate()
			{
				Action = ExceptionItemAction.UpdateItem,
				Id = item.Id,
				FieldValues = new FieldData[] { new FieldData() { ColumnDefId = _checkedOutTransaction.FieldDefinitions.First(o => o.DataType == FieldDataTypes.Currency).Id, FieldValue = "42.42" } }
			};

			// Call the update method
			var response = _mgr.CheckinTransaction(_commonExceptionId, ExceptionAction.NoAction, new PaymentItemUpdate[] { update });
			Assert.AreEqual(StatusCode.SUCCESS, response.Status);
		}

		[TestMethod]
		public void RejectTransaction()
		{
			// Use higher-level test method
			Checkout();

			// Call the update method
			var response3 = _mgr.CheckinTransaction(_commonExceptionId, ExceptionAction.Reject, null);
			Assert.AreEqual(StatusCode.SUCCESS, response3.Status);
		}

		[TestMethod]
		public void DeleteStub()
		{
			// Use higher-level test method
			Checkout();

			// Enumerate the exception items and fields...
			var item = _checkedOutTransaction.TransactionItems.Last();
			PaymentItemUpdate update = new PaymentItemUpdate()
			{
				Action = ExceptionItemAction.DeleteItem,
				Id = item.Id,
				FieldValues = null,
			};

			// Call the update method
			var response = _mgr.CheckinTransaction(_commonExceptionId, ExceptionAction.NoAction, new PaymentItemUpdate[] { update });
			Assert.AreEqual(StatusCode.SUCCESS, response.Status);
		}

		[TestMethod]
		public void InsertStub()
		{
			// Use higher-level test method
			Checkout();

			// Create a new item
			PaymentItemUpdate update = new PaymentItemUpdate()
			{
				Action = ExceptionItemAction.InsertItem,
				Id = Guid.Empty,
				FieldValues = new FieldData[] { new FieldData() { ColumnDefId = _checkedOutTransaction.FieldDefinitions.First(o => o.IsItemAmount).Id, FieldValue = "-42.42" } },
			};

			// Call the update method
			var response = _mgr.CheckinTransaction(_commonExceptionId, ExceptionAction.NoAction, new PaymentItemUpdate[] { update });
			Assert.AreEqual(StatusCode.SUCCESS, response.Status);
		}

		[TestMethod]
		public void AcceptTransaction()
		{
			// Use higher-level test method
			Checkout();

			// Call the update method
			var response3 = _mgr.CheckinTransaction(_commonExceptionId, ExceptionAction.Accept, null);
			Assert.AreEqual(StatusCode.SUCCESS, response3.Status);
		}

	}
}
