﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BusinessRules;
using System.Collections.Generic;
using WFS.RecHub.R360Shared;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.DAL.Fakes;

namespace WFS.RecHub.BusinessRulesUnitTests.BatchXMLTests
{
	[TestClass]
	public class ACHBatchTests
	{
		public static BusinessRulesEngine _bre;
		[ClassInitialize()]
		public static void ClassInitialize(TestContext context)
		{
			R360ServiceContext.UseFakeServiceContext = true;
			_bre = new BusinessRulesEngine(R360ServiceContext.Current);
			_bre._ceDAL = new StubICommonExceptionDAL()
				{
					GetRulesInt64RuleDefinitionsDtoOut = MockFieldSetups.GetACHFieldSetup,
					GetPaymentSubTypesListOfPaymentSubTypeDtoOut = MockPaymentSubTypes.GetPaymentSubTypes,
					CheckForValidationValuesExistenceInt64StringBooleanOut =
						delegate(long fieldValidationKey, string fieldValue, out bool valid)
						{
							valid = fieldValidationKey != 1 || fieldValue == "CCD" || fieldValue == "CIE" || fieldValue == "CTX" || fieldValue == "IAT" || fieldValue == "PPD";
							return true;
						}
				};
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void TestNullTransactionInfo()
		{
			_bre.ValidateTransaction(null, false, false);
		}

		[TestMethod]
		public void TestNullItemInfo()
		{
			TransactionDto ti = new TransactionDto()
			{
				DataEntrySetupKey = 1,
				BatchSourceKey = 1,
				BatchPaymentTypeKey = PaymentTypes.ACH,
				IsException = false,
				Items = null
			};
			Assert.AreEqual(true, _bre.ValidateTransaction(ti, false, false));
			Assert.AreEqual(false, ti.IsException);
		}

		[TestMethod]
		public void TestZeroItemInfos()
		{
			TransactionDto ti = new TransactionDto()
			{
				DataEntrySetupKey = 1,
				BatchSourceKey = 1,
				BatchPaymentTypeKey = PaymentTypes.ACH,
				IsException = false,
				Items = new List<ItemDto>()
			};
			Assert.AreEqual(true, _bre.ValidateTransaction(ti, false, false));
			Assert.AreEqual(false, ti.IsException);
		}

		[TestMethod]
		public void TestExceptionsXML()
		{
			//no longer doing the xml stuff, look at changing to loading data from the fake DAL....
			// but then why, if I can insert it into the real DB and actually test from there...
		}
	}
}
