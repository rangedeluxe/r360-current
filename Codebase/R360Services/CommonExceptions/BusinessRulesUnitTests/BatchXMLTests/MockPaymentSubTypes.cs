﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	internal class MockPaymentSubTypes
	{
		internal static bool GetPaymentSubTypes(out List<PaymentSubTypeDto> subTypes)
		{
			subTypes = new List<PaymentSubTypeDto>()
			{
				new PaymentSubTypeDto() { PaymentSubTypeKey = 1, PaymentType = PaymentTypes.ACH, SubTypeDescription = "CCD" },
				new PaymentSubTypeDto() { PaymentSubTypeKey = 2, PaymentType = PaymentTypes.ACH, SubTypeDescription = "CIE" },
				new PaymentSubTypeDto() { PaymentSubTypeKey = 3, PaymentType = PaymentTypes.ACH, SubTypeDescription = "CTX" },
				new PaymentSubTypeDto() { PaymentSubTypeKey = 4, PaymentType = PaymentTypes.ACH, SubTypeDescription = "IAT" },
				new PaymentSubTypeDto() { PaymentSubTypeKey = 5, PaymentType = PaymentTypes.ACH, SubTypeDescription = "PPD" }
			};
			return true;
		}
	}
}
