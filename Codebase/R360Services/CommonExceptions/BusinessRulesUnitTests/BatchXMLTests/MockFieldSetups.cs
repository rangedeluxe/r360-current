﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	internal class MockFieldSetups
	{
		internal static bool InvalidDataEntryKey(long dataEntrySetupKey, out List<FieldDefinitionDto> fieldDefs)
		{
			fieldDefs = null;
			return false;
		}

		internal static bool NoFieldDefinitions(long dataEntrySetupKey, out List<FieldDefinitionDto> fieldDefs)
		{
			fieldDefs = null;
			return true;
		}


		internal static bool GetACHFieldSetup(long dataEntrySetupKey, out RuleDefinitionsDto rulesDto)
		{
			int key = 100;
			int screenOrder = 1;

			rulesDto = new RuleDefinitionsDto()
			{
				TransactionRequiresInvoice = false,
				FieldDefinitions = new List<FieldDefinitionDto>()
				{
					CreateFieldDefinition(key++, FieldTableTypes.Payments, FieldDataTypes.Text, 80, screenOrder++, "Amount", "Amount", null),
					CreateFieldDefinition(key++, FieldTableTypes.Payments, FieldDataTypes.Text, 80, screenOrder++, "Account", "Account", null),
					CreateFieldDefinition(key++, FieldTableTypes.Payments, FieldDataTypes.Text, 80, screenOrder++, "Serial", "Serial", null),
					CreateFieldDefinition(key++, FieldTableTypes.Payments, FieldDataTypes.Text, 80, screenOrder++, "RemitterName", "RemitterName", CreateRequiredBusinessRule()),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 3, screenOrder++, "ServiceClassCode", "ServiceClassCode", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 16, screenOrder++, "CompanyName", "CompanyName", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 20, screenOrder++, "CompanyData", "CompanyData", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 10, screenOrder++, "CompanyID", "CompanyID", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 3, screenOrder++, "StandardEntryClass", "StandardEntryClass", CreateStandardEntryClassBusinessRule()),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 10, screenOrder++, "EntryDescription", "EntryDescription", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 6, screenOrder++, "DescriptiveDate", "DescriptiveDate", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Date, 80, screenOrder++, "EffectiveDate", "EffectiveDate", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Date, 80, screenOrder++, "SettlementDate", "SettlementDate", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 8, screenOrder++, "OriginatingDFI", "OriginatingDFI", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 80, screenOrder++, "ACHBatchNumber", "ACHBatchNumber", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 80, screenOrder++, "ElectronicTransactionCode", "ElectronicTransactionCode", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 80, screenOrder++, "ReceivingCompany", "ReceivingCompany", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 80, screenOrder++, "TraceNumber", "TraceNumber", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 80, screenOrder++, "IndividualName", "IndividualName", CreatePPDAndCIERequiredBusinessRule()),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 80, screenOrder++, "OFACIndicator1", "OFACIndicator1", null),
					CreateFieldDefinition(key++, FieldTableTypes.PaymentsDataEntry, FieldDataTypes.Text, 80, screenOrder++, "OFACIndicator2", "OFACIndicator2", null),

					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Text, 80, screenOrder++, "Amount", "Amount", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Text, 80, screenOrder++, "AccountNumber", "AccountNumber", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Text, 80, screenOrder++, "InvoiceNumber", "InvoiceNumber", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Currency, 80, screenOrder++, "BPRMonetaryAmount", "BPRMonetaryAmount", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Text, 80, screenOrder++, "BPRAccountNumber", "BPRAccountNumber", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Text, 80, screenOrder++, "RMRReferenceNumber", "RMRReferenceNumber", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Currency, 80, screenOrder++, "RMRMonetaryAmount", "RMRMonetaryAmount", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Currency, 80, screenOrder++, "RMRTotalInvoiceAmount", "RMRTotalInvoiceAmount", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Currency, 80, screenOrder++, "RMRDiscountAmount", "RMRDiscountAmount", null),
					CreateFieldDefinition(key++, FieldTableTypes.Stubs, FieldDataTypes.Text, 80, screenOrder++, "ReassociationTraceNumber", "ReassociationTraceNumber", null),
				}
			};
			return true;
		}

		private static FieldDefinitionDto CreateFieldDefinition(int key, FieldTableTypes tableType, FieldDataTypes dataType, int length, int screenOrder, string fldName, string displayName, List<BusinessRulesDto> businessRules)
		{
			return new FieldDefinitionDto()
			{
				DataEntryColumnKey = key,
				TableType = tableType,
				DataType = dataType,
				FieldLength = length,
				ScreenOrder = screenOrder,
				FldName = fldName,
				DisplayName = displayName,
				BusinessRules = businessRules
			};
		}

		private static List<BusinessRulesDto> CreateRequiredBusinessRule()
		{
			return new List<BusinessRulesDto>()
		{
			new BusinessRulesDto()
			{
				Required = true
			}
		};
		}

		private static List<BusinessRulesDto> CreateStandardEntryClassBusinessRule()
		{
			return new List<BusinessRulesDto>()
		{
			new BusinessRulesDto()
			{
				Required = true,
				RequiredMessage = "Standard entry class is required for all ACH entries.",
				FieldValidationKey = 1
			}
		};
		}

		private static List<BusinessRulesDto> CreatePPDAndCIERequiredBusinessRule()
		{
			return new List<BusinessRulesDto>()
		{
			new BusinessRulesDto()
			{
				BatchPaymentTypeKey = (int)PaymentTypes.ACH,
				BatchPaymentSubTypeKey = 5/*PPD*/,
				Required = true,
				RequiredMessage = "This field is requied for PPD"
			},
			new BusinessRulesDto()
			{
				BatchPaymentTypeKey = (int)PaymentTypes.ACH,
				BatchPaymentSubTypeKey = 5/*CIE*/,
				Required = true,
				RequiredMessage = "This field is requied for CIE"
			}
		};
		}

	}
}
