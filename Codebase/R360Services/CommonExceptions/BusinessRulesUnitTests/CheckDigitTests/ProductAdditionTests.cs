﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BusinessRules;
using System.Collections.Generic;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	[TestClass]
	public class ProductAdditionTests
	{
		private CheckDigitRoutineDto upcNegOffset = new CheckDigitRoutineDto()
			{
				Offset = -1,
				Method = CheckDigitMethod.ProductAddition,
				Modulus = 10,
				Compliment = 10,
				WeightsDirection = WeightPatternDirection.LeftToRight,
				Weights = new int[] { 3, 1 },
				Rem10Replacement = 0,
				Rem11Replacement = 1,
				ReplacementValues = null
			};

		private CheckDigitRoutineDto upcPosOffset = new CheckDigitRoutineDto()
			{
				Offset = 12,
				Method = CheckDigitMethod.ProductAddition,
				Modulus = 10,
				Compliment = 10,
				WeightsDirection = WeightPatternDirection.LeftToRight,
				Weights = new int[] { 3, 1 },
				Rem10Replacement = 0,
				Rem11Replacement = 1,
				ReplacementValues = null
			};


		[TestMethod]
		public void TestUPCNegOffsetPasses()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 1,
				Value = "036000241457",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(upcNegOffset, field);

			Assert.AreEqual(field.DataEntryColumnKey, 1);
			Assert.AreEqual(field.Value, "036000241457");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}

		[TestMethod]
		public void TestUPCNegOffsetFails()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 2,
				Value = "036000241451",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(upcNegOffset, field);

			Assert.AreEqual(field.DataEntryColumnKey, 2);
			Assert.AreEqual(field.Value, "036000241451");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDFailed);
		}

		[TestMethod]
		public void TestUPCPosOffsetPasses()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 3,
				Value = "036000241457",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(upcNegOffset, field);

			Assert.AreEqual(field.DataEntryColumnKey, 3);
			Assert.AreEqual(field.Value, "036000241457");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}

		[TestMethod]
		public void TestUPCPosOffsetFails()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 4,
				Value = "036000241451",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(upcNegOffset, field);

			Assert.AreEqual(field.DataEntryColumnKey, 4);
			Assert.AreEqual(field.Value, "036000241451");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDFailed);
		}

		[TestMethod]
		public void TestIgnoreSpacesByReplacements()
		{
			Dictionary<char, int?> originalReplacements;
			Dictionary<char, int?> ignoreSpaceReplacements = new Dictionary<char, int?>() { { ' ', null } };
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 5,
				Value = "036   241457",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			originalReplacements = upcNegOffset.ReplacementValues;

			upcNegOffset.ReplacementValues = null;
			CheckDigitValidation.ValidateCheckDigit(upcNegOffset, field);
			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "036   241457");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidNumber);

			field.IsException = false;
			field.ExceptionMessage = string.Empty;

			upcNegOffset.ReplacementValues = ignoreSpaceReplacements;//set the replacement values to replace spaces
			CheckDigitValidation.ValidateCheckDigit(upcNegOffset, field);
			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "036   241457");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);

			upcNegOffset.ReplacementValues = originalReplacements;//set the replacement values back to the original set


			//perform the same work for the positive offset
			originalReplacements = upcPosOffset.ReplacementValues;

			upcPosOffset.ReplacementValues = null;
			CheckDigitValidation.ValidateCheckDigit(upcPosOffset, field);
			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "036   241457");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidNumber);

			field.IsException = false;
			field.ExceptionMessage = string.Empty;

			upcPosOffset.ReplacementValues = ignoreSpaceReplacements;//set the replacement values to replace spaces
			CheckDigitValidation.ValidateCheckDigit(upcPosOffset, field);
			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "036   241457");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);

			upcPosOffset.ReplacementValues = originalReplacements;//set the replacement values back to the original set
		}

		[TestMethod]
		public void TestIgnoreSpacesBySetting()
		{
			bool originalIgnoreSpaces;
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 5,
				Value = "036   241457",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			originalIgnoreSpaces = upcNegOffset.IgnoreSpaces;

			upcNegOffset.IgnoreSpaces = false;
			CheckDigitValidation.ValidateCheckDigit(upcNegOffset, field);
			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "036   241457");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidNumber);

			field.IsException = false;
			field.ExceptionMessage = string.Empty;

			upcNegOffset.IgnoreSpaces = true;
			CheckDigitValidation.ValidateCheckDigit(upcNegOffset, field);
			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "036   241457");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);

			upcNegOffset.IgnoreSpaces = originalIgnoreSpaces;//set the ignore spaces back to the original set


			//perform the same work for the positive offset
			originalIgnoreSpaces = upcPosOffset.IgnoreSpaces;

			upcPosOffset.IgnoreSpaces = false;
			CheckDigitValidation.ValidateCheckDigit(upcPosOffset, field);
			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "036   241457");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidNumber);

			field.IsException = false;
			field.ExceptionMessage = string.Empty;

			upcPosOffset.IgnoreSpaces = true;
			CheckDigitValidation.ValidateCheckDigit(upcPosOffset, field);
			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "036   241457");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);

			upcPosOffset.IgnoreSpaces = originalIgnoreSpaces;//set the ignore spaces back to the original set
		}
	}
}
