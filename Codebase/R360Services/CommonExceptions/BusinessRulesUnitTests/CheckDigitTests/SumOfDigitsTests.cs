﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BusinessRules;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	[TestClass]
	public class SumOfDigitsTests
	{
		public CheckDigitRoutineDto bnymRoutine = new CheckDigitRoutineDto()
		{
			Offset = 10,
			Method = CheckDigitMethod.SumOfDigits,
			Modulus = 10,
			Compliment = 10,
			WeightsDirection = WeightPatternDirection.LeftToRight,
			Weights = new int[] { 2, 1 },
			Rem10Replacement = 0,
			Rem11Replacement = 0,
			ReplacementValues = new System.Collections.Generic.Dictionary<char, int?>()
			{
				{'A', 10},
				{'B', 11},
				{'C', 12},
				{'D', 13},
				{'E', 14}
				//This is not their full set of the alphabet
			}
		};

		[TestMethod]
		public void TestBNYMCheckDigitPasses()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 1,
				Value = "D015013354",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(bnymRoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 1);
			Assert.AreEqual(field.Value, "D015013354");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}

		[TestMethod]
		public void TestBNYMCheckDigitFails()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 2,
				Value = "D015013355",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(bnymRoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 2);
			Assert.AreEqual(field.Value, "D015013355");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDFailed);
		}

	}
}
