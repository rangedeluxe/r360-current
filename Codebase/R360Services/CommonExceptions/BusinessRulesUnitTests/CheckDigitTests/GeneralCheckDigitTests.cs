﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BusinessRules;
using System.Collections.Generic;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	[TestClass]
	public class GeneralCheckDigitTests
	{
		[TestMethod]
		public void TestFieldIsNull()
		{
			try
			{
				CheckDigitValidation.ValidateCheckDigit(ABACheckDigitTests.ABARoutine, null);
			}
			catch (Exception ex)
			{
				Assert.Fail("Field value null test failed.\r\n" + ex.ToString());
			}
		}

		[TestMethod]
		public void TestFieldIsEmpty()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 1,
				Value = string.Empty,
				IsException = false,
				ExceptionMessage = null
			};

			//test that no exception is raised
			CheckDigitValidation.ValidateCheckDigit(ABACheckDigitTests.ABARoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 1);
			Assert.AreEqual(field.Value, string.Empty);
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFieldIsBlank()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 2,
				Value = "            ",
				IsException = false,
				ExceptionMessage = null
			};

			//test that no exception is raised
			CheckDigitValidation.ValidateCheckDigit(ABACheckDigitTests.ABARoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 2);
			Assert.AreEqual(field.Value, "            ");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidNumber);
		}

		[TestMethod]
		public void TestRoutineIsNull()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 3,
				Value = "123456789",
				IsException = false,
				ExceptionMessage = null
			};

			CheckDigitValidation.ValidateCheckDigit(null, field);

			Assert.AreEqual(field.DataEntryColumnKey, 3);
			Assert.AreEqual(field.Value, "123456789");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestModulusZero()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 4,
				Value = "101110116",
				IsException = false,
				ExceptionMessage = null
			};

			CheckDigitRoutineDto badMod = new CheckDigitRoutineDto()
			{
				Offset = 9,
				Method = CheckDigitMethod.ProductAddition,
				Modulus = 0,
				Compliment = 10,
				WeightsDirection = WeightPatternDirection.LeftToRight,
				Weights = new int[] { 3, 7, 1 },
				Rem10Replacement = 0,
				Rem11Replacement = 0,
				ReplacementValues = new Dictionary<char, int?>() { { '-', null } }
			};

			CheckDigitValidation.ValidateCheckDigit(badMod, field);

			Assert.AreEqual(field.DataEntryColumnKey, 4);
			Assert.AreEqual(field.Value, "101110116");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidModulus);
		}

		[TestMethod]
		public void TestOffsetZero()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 5,
				Value = "101110116",
				IsException = false,
				ExceptionMessage = null
			};

			CheckDigitRoutineDto badOffset = new CheckDigitRoutineDto()
			{
				Offset = 0,
				Method = CheckDigitMethod.ProductAddition,
				Modulus = 10,
				Compliment = 10,
				WeightsDirection = WeightPatternDirection.LeftToRight,
				Weights = new int[] { 3, 7, 1 },
				Rem10Replacement = 0,
				Rem11Replacement = 0,
				ReplacementValues = new Dictionary<char, int?>() { { '-', null } }
			};

			CheckDigitValidation.ValidateCheckDigit(badOffset, field);

			Assert.AreEqual(field.DataEntryColumnKey, 5);
			Assert.AreEqual(field.Value, "101110116");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidOffset);
		}

		[TestMethod]
		public void TestPositiveCDPosition()
		{
			CheckDigitRoutineDto offsetTest = new CheckDigitRoutineDto()
			{
				Offset = 9,
				Method = CheckDigitMethod.ProductAddition,
				Modulus = 10,
				Compliment = 10,
				WeightsDirection = WeightPatternDirection.LeftToRight,
				Weights = new int[] { 3, 7, 1 },
				Rem10Replacement = 0,
				Rem11Replacement = 0,
				ReplacementValues = new Dictionary<char, int?>() { { '-', null } }
			};

			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 6,
				Value = "123456780",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			//using the positive offset of 9 this number will pass
			CheckDigitValidation.ValidateCheckDigit(offsetTest, field);
			Assert.AreEqual(field.DataEntryColumnKey, 6);
			Assert.AreEqual(field.Value, "123456780");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);

			//using the positive offset of 10 this number will not pass
			offsetTest.Offset = 10;
			CheckDigitValidation.ValidateCheckDigit(offsetTest, field);
			Assert.AreEqual(field.DataEntryColumnKey, 6);
			Assert.AreEqual(field.Value, "123456780");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidLength);
		}

		[TestMethod]
		public void TestNegativeCDPosition()
		{
			CheckDigitRoutineDto offsetTest = new CheckDigitRoutineDto()
			{
				Offset = -1,
				Method = CheckDigitMethod.ProductAddition,
				Modulus = 10,
				Compliment = 10,
				WeightsDirection = WeightPatternDirection.LeftToRight,
				Weights = new int[] { 3, 7, 1 },
				Rem10Replacement = 0,
				Rem11Replacement = 0,
				ReplacementValues = new Dictionary<char, int?>() { { '-', null } }
			};

			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 6,
				Value = "123456780",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			//using the negative offset of 1 this number will pass
			CheckDigitValidation.ValidateCheckDigit(offsetTest, field);
			Assert.AreEqual(field.DataEntryColumnKey, 6);
			Assert.AreEqual(field.Value, "123456780");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);

			//using the positive offset of 10 this number will not pass
			offsetTest.Offset = -10;
			CheckDigitValidation.ValidateCheckDigit(offsetTest, field);
			Assert.AreEqual(field.DataEntryColumnKey, 6);
			Assert.AreEqual(field.Value, "123456780");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDInvalidLength);
		}

		[TestMethod]
		public void TestPriorExceptionNotOverwrittenOnPass()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 7,
				Value = "123456780",
				IsException = true,
				ExceptionMessage = "Already set exception message."
			};
			CheckDigitValidation.ValidateCheckDigit(ABACheckDigitTests.ABARoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 7);
			Assert.AreEqual(field.Value, "123456780");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, "Already set exception message.");
		}


		[TestMethod]
		public void TestPriorExceptionOverwrittenOnFail()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 8,
				Value = "123456789",
				IsException = true,
				ExceptionMessage = "Already set exception message."
			};
			CheckDigitValidation.ValidateCheckDigit(ABACheckDigitTests.ABARoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 8);
			Assert.AreEqual(field.Value, "123456789");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ABACheckDigitTests.ABARoutine.FailureMessage);
		}
	}
}
