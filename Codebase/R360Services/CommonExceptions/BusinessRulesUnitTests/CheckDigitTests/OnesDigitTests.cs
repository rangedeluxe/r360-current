﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WFS.RecHub.BusinessRules;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	[TestClass]
	public class OnesDigitTests
	{
		CheckDigitRoutineDto routine = new CheckDigitRoutineDto()
		{
			Offset = -1,
			Method = CheckDigitMethod.OnesDigit,
			Modulus = 12,
			Compliment = 0,
			WeightsDirection = WeightPatternDirection.RightToLeft,
			Weights = new int[] { 1, 3 },
			Rem10Replacement = 15,
			Rem11Replacement = 16,
			ReplacementValues = new Dictionary<char, int?>()
			{
				{ 'A', 15 },
				{ 'B', 16 }
			}
		};

		[TestMethod]
		public void TestOnesDigitsPasses()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 1,
				Value = "642984436361",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(routine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 1);
			Assert.AreEqual(field.Value, "642984436361");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}

		[TestMethod]
		public void TestOnesDigitFails()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 2,
				Value = "642984436366",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(routine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 2);
			Assert.AreEqual(field.Value, "642984436366");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.CDFailed);
		}

		[TestMethod]
		public void TestRemainder10Replacement()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 3,
				Value = "64298443633A",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(routine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 3);
			Assert.AreEqual(field.Value, "64298443633A");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}

		[TestMethod]
		public void TestRemainder11Replacement()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 4,
				Value = "64298443634B",
				IsException = false,
				ExceptionMessage = string.Empty
			};

			CheckDigitValidation.ValidateCheckDigit(routine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 4);
			Assert.AreEqual(field.Value, "64298443634B");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}
	}
}
