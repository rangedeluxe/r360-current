﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BusinessRules;
using System.Collections.Generic;
using WFS.RecHub.CommonException.Common;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	[TestClass]
	public class ABACheckDigitTests
	{
		internal static CheckDigitRoutineDto ABARoutine = new CheckDigitRoutineDto()
		{
			Offset = 9,
			Method = CheckDigitMethod.ProductAddition,
			Modulus = 10,
			Compliment = 10,
			WeightsDirection = WeightPatternDirection.LeftToRight,
			Weights = new int[] { 3, 7, 1 },
			Rem10Replacement = 0,
			Rem11Replacement = 0,
			ReplacementValues = new Dictionary<char, int?>() { { '-', null } },
			FailureMessage = "The check digit is invalid for the ABA routine."
		};

		[TestMethod]
		public void TestTightwadBankABACheckDigit()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 1,
				Value = "101110116",
				IsException = false,
				ExceptionMessage = string.Empty
			};
			CheckDigitValidation.ValidateCheckDigit(ABARoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 1);
			Assert.AreEqual(field.Value, "101110116");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}

		[TestMethod]
		public void TestPassingBogusABACheckDigit()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 2,
				Value = "123456780",
				IsException = false,
				ExceptionMessage = string.Empty
			};
			CheckDigitValidation.ValidateCheckDigit(ABARoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 2);
			Assert.AreEqual(field.Value, "123456780");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}

		[TestMethod]
		public void TestFailingABACheckDigit()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 3,
				Value = "123456789",
				IsException = false,
				ExceptionMessage = string.Empty
			};
			CheckDigitValidation.ValidateCheckDigit(ABARoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 3);
			Assert.AreEqual(field.Value, "123456789");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, "The check digit is invalid for the ABA routine.");
		}

		[TestMethod]
		public void TestPassingABAWithDash()
		{
			FieldDto field = new FieldDto()
			{
				DataEntryColumnKey = 4,
				Value = "1234-0244",
				IsException = false,
				ExceptionMessage = string.Empty
			};
			CheckDigitValidation.ValidateCheckDigit(ABARoutine, field);

			Assert.AreEqual(field.DataEntryColumnKey, 4);
			Assert.AreEqual(field.Value, "1234-0244");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, string.Empty);
		}
	}
}
