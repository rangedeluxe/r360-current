﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BusinessRules;
using System.Collections.Generic;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;
using WFS.RecHub.DAL.Fakes;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	[TestClass]
	public class FieldLengthTests
	{
		public static BusinessRulesEngine _bre;
		[ClassInitialize()]
		public static void ClassInitialize(TestContext context)
		{
			_bre = new BusinessRulesEngine(null);
		}

		[TestMethod]
		public void TestEmptyFieldPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 10,
				MaxLength = 20
			};

			FieldDto field = new FieldDto();

			field.Value = string.Empty;
			_bre.ValidateLength(rules, field);

			Assert.AreEqual(string.Empty, field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);
		}

		[TestMethod]
		public void TestNullFieldPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 10,
				MaxLength = 20
			};

			FieldDto field = new FieldDto();

			field.Value = null;
			_bre.ValidateLength(rules, field);

			Assert.AreEqual(null, field.Value);
			Assert.AreEqual(false,  field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);
		}


		[TestMethod]
		public void TestPriorExceptionNotOverwrittenOnPass()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 10,
				MaxLength = 20
			};

			FieldDto field = new FieldDto();

			//first assert value passes
			field.Value = "123456789012345";
			_bre.ValidateLength(rules, field);
			Assert.AreEqual("123456789012345", field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);

			//now set field is in error and run passing test again
			field.IsException = true;
			field.ExceptionMessage = "Already set exception message.";
			_bre.ValidateLength(rules, field);
			Assert.AreEqual("123456789012345", field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual("Already set exception message.", field.ExceptionMessage);
		}

		[TestMethod]
		public void TestNoLengthRestrictionPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 0,
				MaxLength = 0
			};

			FieldDto field = new FieldDto();

			field.Value = "123";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("123", field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);

			field.Value = "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);
		}

		[TestMethod]
		public void TestMinLengthOnlyPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 10,
				MaxLength = 0
			};

			FieldDto field = new FieldDto();

			field.Value = "1234567890";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("1234567890", field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);

			field.Value = "12345678901234567890";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("12345678901234567890", field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);
		}

		[TestMethod]
		public void TestMinLengthOnlyFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 10,
				MaxLength = 0
			};

			FieldDto field = new FieldDto();

			field.Value = "1";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("1", field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual(ErrorMessages.FieldLengthInvalid, field.ExceptionMessage);
		}

		[TestMethod]
		public void TestMinAndMaxLengthPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 10,
				MaxLength = 20
			};

			FieldDto field = new FieldDto();

			field.Value = "1234567890";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("1234567890", field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);

			field.Value = "12345678901234567890";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("12345678901234567890", field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);
		}

		[TestMethod]
		public void TestMinAndMaxLengthMinFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 10,
				MaxLength = 20
			};

			FieldDto field = new FieldDto();

			field.Value = "123456789";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("123456789", field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual(ErrorMessages.FieldLengthInvalid, field.ExceptionMessage);
		}

		[TestMethod]
		public void TestMinAndMaxLengthMaxFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				MinLength = 10,
				MaxLength = 20
			};

			FieldDto field = new FieldDto();

			field.Value = "123456789012345678901";
			_bre.ValidateLength(rules, field);

			Assert.AreEqual("123456789012345678901", field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual(ErrorMessages.FieldLengthInvalid, field.ExceptionMessage);
		}
	}
}
