﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BusinessRules;
using System.Collections.Generic;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	[TestClass]
	public class FormatTypeTests
	{
		public static BusinessRulesEngine _bre;
		[ClassInitialize()]
		public static void ClassInitialize(TestContext context)
		{
			_bre = new BusinessRulesEngine(null);
		}

		[TestMethod]
		public void TestFormatTypeEmptyField()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Amount
			};

			FieldDto field = new FieldDto()
			{
				Value = string.Empty
			};

			_bre.ValidateFormatType(rules, field);

			Assert.AreEqual(field.Value, string.Empty);
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeNullField()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Amount
			};

			FieldDto field = new FieldDto()
			{
				Value = null
			};

			_bre.ValidateFormatType(rules, field);

			Assert.AreEqual(field.Value, null);
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeRetainsExistingException()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Amount
			};

			FieldDto field = new FieldDto()
			{
				Value = string.Empty,
				IsException = true,
				ExceptionMessage = "Existing exception message."
			};

			_bre.ValidateFormatType(rules, field);

			Assert.AreEqual(field.Value, string.Empty);
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, "Existing exception message.");
		}

		[TestMethod]
		public void TestFormatTypeControlCharactersFail()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Amount
			};

			FieldDto field = new FieldDto()
			{
				Value = "Testing\rControl\nCharacters"
			};

			_bre.ValidateFormatType(rules, field);

			Assert.AreEqual(field.Value, "Testing\rControl\nCharacters");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidControlChars);
		}

		[TestMethod]
		public void TestFormatTypeAny()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Any
			};

			FieldDto field = new FieldDto()
			{
				Value = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()_+=-`[]{}\\|;:'\"/?<>,."
			};

			_bre.ValidateFormatType(rules, field);

			Assert.AreEqual(field.Value, "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()_+=-`[]{}\\|;:'\"/?<>,.");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeAmountPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Amount
			};

			FieldDto field = new FieldDto();

			field.Value = "1.23";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.23");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "99999";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "99999");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "999.99";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "999.99");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "12345678901234567890.12";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12345678901234567890.12");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "123.00000000";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123.00000000");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = ".12";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, ".12");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeAmountFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Amount
			};

			FieldDto field = new FieldDto();

			field.Value = "1.234";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.234");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidAmountFormat);

			field.Value = "1.23000004";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.23000004");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidAmountFormat);

			field.Value = "ABC";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidAmountFormat);

			field.Value = "1.23.44";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.23.44");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidAmountFormat);

			field.Value = "$2.30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "$2.30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidAmountFormat);

			field.Value = "2 30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "2 30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidAmountFormat);
		}

		[TestMethod]
		public void TestFormatTypeNumberPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Number
			};

			FieldDto field = new FieldDto();

			field.Value = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "1";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeNumberFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Number
			};

			FieldDto field = new FieldDto();

			field.Value = "1.234";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.234");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidNumberFormat);

			field.Value = "1A";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1A");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidNumberFormat);

			field.Value = "1 3";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1 3");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidNumberFormat);

			field.Value = "$2.30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "$2.30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidNumberFormat);

			field.Value = "230&";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "230&");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidNumberFormat);

			field.Value = "1-3";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1-3");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidNumberFormat);
		}

		[TestMethod]
		public void TestFormatTypeNumberAndSpecialCharPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.NumberAndSpecialChar
			};

			FieldDto field = new FieldDto();

			field.Value = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890&/*?@";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890&/*?@");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "1234567890";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1234567890");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "&/*?@";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "&/*?@");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeNumberAndSpecialCharFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.NumberAndSpecialChar
			};

			FieldDto field = new FieldDto();

			field.Value = "1.234";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.234");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "1A";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1A");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "1 3";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1 3");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "$2.30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "$2.30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);
		}

		[TestMethod]
		public void TestFormatTypeAlphanumericPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Alphanumeric
			};

			FieldDto field = new FieldDto();

			field.Value = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "ABC";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "123";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "ABC123";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC123");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeAlphanumericFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Alphanumeric
			};

			FieldDto field = new FieldDto();

			field.Value = "ABC 123";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC 123");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "(none)";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "(none)");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "1.30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "$2.30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "$2.30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "ABC123&";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC123&");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);
		}

		[TestMethod]
		public void TestFormatTypeAlphanumericAndSpecialCharPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.AlphanumericAndSpecialChar
			};

			FieldDto field = new FieldDto();

			field.Value = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789&/*?@";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789&/*?@");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "ABC";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "123";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "ABC123";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC123");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "ABC&123";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC&123");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "&/*?@";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "&/*?@");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeAlphanumericAndSpecialCharFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.AlphanumericAndSpecialChar
			};

			FieldDto field = new FieldDto();

			field.Value = "ABC 123";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC 123");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "(none)";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "(none)");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "1.30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "$2.30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "$2.30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "ABC & 123";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC & 123");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);
		}

		[TestMethod]
		public void TestFormatTypeNumberDashesPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.NumberDashes
			};

			FieldDto field = new FieldDto();

			field.Value = "1234567890-";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1234567890-");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "1";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "----";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "----");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "1-3";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1-3");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeNumberDashesFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.NumberDashes
			};

			FieldDto field = new FieldDto();

			field.Value = "1.234";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1.234");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "1A";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1A");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "1 3";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1 3");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "$2.30";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "$2.30");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "230&";
			field.IsException = false;
			field.ExceptionMessage = string.Empty;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "230&");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

		}

		[TestMethod]
		public void TestFormatTypeDatePasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Date
			};

			FieldDto field = new FieldDto();

			rules.Mask = "MM/dd/yyyy";
			field.Value = "12/31/2014";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12/31/2014");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			rules.Mask = "MM/dd/yy";
			field.Value = "12/31/14";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12/31/14");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
			

			rules.Mask = "MMddyyyy";
			field.Value = "12312014";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12312014");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			rules.Mask = "MMddyy";
			field.Value = "123114";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123114");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			
			rules.Mask = "yyyyMMdd";
			field.Value = "20141231";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "20141231");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeDateFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Date
			};

			FieldDto field = new FieldDto();

			rules.Mask = "MM/dd/yyyy";
			field.Value = "12-31-2014";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12-31-2014");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "MM/dd/yy";
			field.Value = "123114";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123114");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "MMddyyyy";
			field.Value = "ABC";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "MMddyy";
			field.Value = "311214";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "311214");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);


			rules.Mask = "yyyyMMdd";
			field.Value = "00001231";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "00001231");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);
		}

		[TestMethod]
		public void TestFormatTypeCustomDatePasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.CustomDate
			};

			FieldDto field = new FieldDto();

			rules.Mask = "MM/dd/yyyy";
			field.Value = "12/31/2014";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12/31/2014");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			rules.Mask = "MM/dd/yy";
			field.Value = "12/31/14";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12/31/14");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);


			rules.Mask = "MMddyyyy";
			field.Value = "12312014";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12312014");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			rules.Mask = "MMddyy";
			field.Value = "123114";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123114");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			rules.Mask = "yyyyMMdd";
			field.Value = "20141231";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "20141231");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			rules.Mask = "yyyy.MM.dd";
			field.Value = "2014.12.31";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "2014.12.31");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			rules.Mask = "MM_dd_yyyy";
			field.Value = "12_31_2014";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12_31_2014");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			rules.Mask = "MM-dd-yyyy";
			field.Value = "12-31-2014";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12-31-2014");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeCustomDateFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.CustomDate
			};

			FieldDto field = new FieldDto();

			rules.Mask = "MM/dd/yyyy";
			field.Value = "12-31-2014";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12-31-2014");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "MM/dd/yy";
			field.Value = "123114";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "123114");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "MMddyyyy";
			field.Value = "ABC";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "ABC");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "MMddyy";
			field.Value = "311214";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "311214");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);


			rules.Mask = "yyyyMMdd";
			field.Value = "00001231";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "00001231");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "yyyy.MM.dd";
			field.Value = "2014*12*31";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "2014*12*31");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "MM_dd_yyyy";
			field.Value = "12 31_2014";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12 31_2014");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);

			rules.Mask = "MM-dd-yyyy";
			field.Value = "12-31-14";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "12-31-14");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidDateFormat);
		}

		[TestMethod]
		public void TestFormatTypeCustomPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Custom,
				Mask = "ABC 123_&"
			};

			FieldDto field = new FieldDto();

			field.Value = "A1 B2 C3 & A_B_C";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "A1 B2 C3 & A_B_C");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "abc";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "abc");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "1";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "1");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);

			field.Value = "A & B";
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "A & B");
			Assert.AreEqual(field.IsException, false);
			Assert.AreEqual(field.ExceptionMessage, null);
		}

		[TestMethod]
		public void TestFormatTypeCustomFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				FormatType = FormatTypes.Custom,
				Mask = "ABC 123_&"
			};

			FieldDto field = new FieldDto();

			field.Value = "First*Star";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "First*Star");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "XYZ";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "XYZ");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);

			field.Value = "456";
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateFormatType(rules, field);
			Assert.AreEqual(field.Value, "456");
			Assert.AreEqual(field.IsException, true);
			Assert.AreEqual(field.ExceptionMessage, ErrorMessages.InvalidCharacters);
		}
	}
}
