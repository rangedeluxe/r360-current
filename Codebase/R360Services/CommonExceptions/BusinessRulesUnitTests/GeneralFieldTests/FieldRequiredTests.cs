﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFS.RecHub.BusinessRules;
using System.Collections.Generic;
using WFS.RecHub.CommonException.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.BusinessRulesUnitTests
{
	[TestClass]
	public class FieldRequiredTests
	{
		public static BusinessRulesEngine _bre;
		[ClassInitialize()]
		public static void ClassInitialize(TestContext context)
		{
			_bre = new BusinessRulesEngine(null);
		}

		[TestMethod]
		public void TestEmptyFieldNotRequiredPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				Required = false
			};

			FieldDto field = new FieldDto();

			field.Value = string.Empty;

			_bre.ValidateRequired(rules, field);

			Assert.AreEqual(string.Empty, field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);
		}

		[TestMethod]
		public void TestEmptyFieldRequiredFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				Required = true,
				RequiredMessage = "Field is required."
			};

			FieldDto field = new FieldDto();

			field.Value = string.Empty;
			_bre.ValidateRequired(rules, field);

			Assert.AreEqual(string.Empty, field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual("Field is required.", field.ExceptionMessage);
		}

		[TestMethod]
		public void TestNullFieldNotRequiredPasses()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				Required = false
			};

			FieldDto field = new FieldDto();

			field.Value = null;
			_bre.ValidateRequired(rules, field);

			Assert.AreEqual(null, field.Value);
			Assert.AreEqual(false,  field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);
		}

		[TestMethod]
		public void TestNullFieldRequiredFails()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				Required = true,
				RequiredMessage = "Field is required."
			};

			FieldDto field = new FieldDto();

			field.Value = null;
			_bre.ValidateRequired(rules, field);

			Assert.AreEqual(null, field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual("Field is required.", field.ExceptionMessage);
		}

		[TestMethod]
		public void TestPriorExceptionNotOverwrittenOnPass()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				Required = true
			};

			FieldDto field = new FieldDto();

			//first assert value passes
			field.Value = "123456789012345";
			_bre.ValidateRequired(rules, field);
			Assert.AreEqual("123456789012345", field.Value);
			Assert.AreEqual(false, field.IsException);
			Assert.AreEqual(null, field.ExceptionMessage);

			//now set field is in error and run passing test again
			field.IsException = true;
			field.ExceptionMessage = "Already set exception message.";
			_bre.ValidateRequired(rules, field);
			Assert.AreEqual("123456789012345", field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual("Already set exception message.", field.ExceptionMessage);
		}

		[TestMethod]
		public void TestRequiredFailsDefaultMessage()
		{
			BusinessRulesDto rules = new BusinessRulesDto()
			{
				Required = true,
				RequiredMessage = null
			};

			FieldDto field = new FieldDto();

			field.Value = string.Empty;
			_bre.ValidateRequired(rules, field);

			Assert.AreEqual(string.Empty, field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual(ErrorMessages.FieldIsRequired, field.ExceptionMessage);

			field.Value = null;
			field.IsException = false;
			field.ExceptionMessage = null;
			_bre.ValidateRequired(rules, field);

			Assert.AreEqual(null, field.Value);
			Assert.AreEqual(true, field.IsException);
			Assert.AreEqual(ErrorMessages.FieldIsRequired, field.ExceptionMessage);
		}
	}
}
