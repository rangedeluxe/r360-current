﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.R360Services.Common.Services;
using System.Globalization;
using Wfs.Logging.Performance;
using WFS.RecHub.PostDepositBusinessRules;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;
using WFS.RecHub.R360Services.PostDepositExceptions.Handlers;
using WFS.RecHub.R360Services.R360ServicesAPI;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.ApplicationBlocks.Common.Interfaces;
using WFS.RecHub.ApplicationBlocks.Common;

namespace WFS.RecHub.R360Services.PostDepositExceptions
{
    public class PostDepositExceptionsApi : APIBase, IDisposable
    {
        private class StubWithKeys : Stub
        {
            public int BatchSequence { get; set; }
            public int StubSequence { get; set; }
        }

        private readonly IServiceContext _serviceContext;
        private readonly IR360ServicesDAL _r360Dal;
        private readonly IPostDepositExceptionsDal _postDepositExceptionsDal;
        private readonly IRaamClient _raamClient;
        private readonly ILockboxAPI _lockboxApi;
        private readonly IPaymentDataProvider _payments;
        private readonly IStubsDataProvider _stubs;
        private readonly IDataEntryDataProvider _dataEntry;
        private readonly IXmlTransformer<List<PostDepositEntityDto>> _entitiesTransformer;
        private readonly IPostDepositDataProvider _postDeposit;
        private readonly IImageProvider _images;
        private readonly IDocumentDataProvider _documents;
        private readonly IPerformanceLoggerWithStatistics _performanceLogger;
        private readonly BusinessRulesEngine _businessRulesEngine;
        private readonly BusinessRulesEngine _workgroupBuinessRuleEngine;
        private readonly IPermissionProvider _permissionprovider;
        private readonly ILookupRepository<PayerLookupRequestDto, PossibleResult<IEnumerable<PayerDto>>> _payerLookupRepository;
        private bool _disposed;

        public PostDepositExceptionsApi(IServiceContext serviceContext, IR360ServicesDAL r360ServicesDal,
            IPostDepositExceptionsDal postDepositExceptionsDal, ISessionDAL sessionDal, IRaamClient raam, ILockboxAPI lockboxApi,
            IPaymentDataProvider payments, IStubsDataProvider stubs, IDataEntryDataProvider dataEntry,
            IPostDepositDataProvider postDeposit, IImageProvider images, IRuleProvider deRuleProvider,
            IDocumentDataProvider documents, IPerformanceLoggerWithStatistics performanceLogger,
            IPermissionProvider permissionprovider, IRuleProvider wgRuleProvider,
            ILookupRepository<PayerLookupRequestDto, PossibleResult<IEnumerable<PayerDto>>> payerLookupRepository)
            : base(serviceContext, sessionDal)
        {
            _serviceContext = serviceContext;
            _r360Dal = r360ServicesDal;
            _postDepositExceptionsDal = postDepositExceptionsDal;
            _raamClient = raam;
            _lockboxApi = lockboxApi;
            _payments = payments;
            _stubs = stubs;
            _dataEntry = dataEntry;
            _postDeposit = postDeposit;
            _images = images;
            _entitiesTransformer = new PostDepositEntityListXmlTransformer();
            _businessRulesEngine = new BusinessRulesEngine(deRuleProvider);
            _documents = documents;
            _performanceLogger = performanceLogger;
            _permissionprovider = permissionprovider;
            _workgroupBuinessRuleEngine = new BusinessRulesEngine(wgRuleProvider);
            _payerLookupRepository = payerLookupRepository;
        }

        protected override bool ValidateSID(out int userID)
        {
            bool bReturnValue = false;
            userID = -1;
            DataTable dtResults;

            OnLogEvent("Current User Claims: \r\n" + _ServiceContext.RAAMClaims.Select(o => "  => " + o.Type + ": " + o.Value).Aggregate((o1, o2) => o1 + "\r\n" + o2), this.GetType().Name, MessageType.Information, MessageImportance.Verbose);

            if (_r360Dal.GetUserIDBySID(_ServiceContext.GetSID(), out dtResults))
            {
                userID = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
                bReturnValue = true;
            }

            return bReturnValue;
        }

        public BaseGenericResponse<PostDepositTransactionsResponseDto> GetPendingTransactions(PostDepositPendingTransactionsRequestDto request)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<PostDepositTransactionsResponseDto>>((result, userID, sessionID) =>
            {
                var response = new BaseGenericResponse<PostDepositTransactionsResponseDto>();

                // Gather our entity tree first.
                var ents = _raamClient.GetAuthorizedEntityList();


                var entlist = ents
                    .Select(e => new PostDepositEntityDto()
                    {
                        EntityHierarchy = _raamClient.GetEntityBreadcrumb(e.ID),
                        EntityId = e.ID.ToString()
                    })
                    .ToList();

                var req = new PostDepositPendingTransactionsRequestSqlDto()
                {
                    Length = request.Length,
                    OrderBy = request.OrderBy,
                    OrderByDirection = request.OrderByDirection,
                    Search = request.Search,
                    SessionId = _serviceContext.GetSessionID(),
                    Start = request.Start,
                    Entities = _entitiesTransformer.ToXML(entlist)
                };

                // Now push it to the SP.
                DataTable dt;
                int recordstotal;
                int recordsfiltered;
                if (!_postDepositExceptionsDal.GetPendingTransactions(req, out recordstotal, out recordsfiltered, out dt))
                    return new BaseGenericResponse<PostDepositTransactionsResponseDto>() { Status = StatusCode.FAIL, Errors = new List<string>() { "Error retrieving data from database." } };

                // Convert to list.
                var list = dt.ToObjectList<PostDepositTransactionResponseDto>();
                bool operatorCanOverRideLock = _permissionprovider.HasPermission(
                    R360Permissions.Perm_PostDepositUnlockOverride,
                    R360Permissions.ActionType.Manage);

                // Populate lock status.
                foreach (var t in list)
                {
                    t.Lock = _postDeposit.GetTransactionLock(t.BatchId, t.DepositDate, t.TransactionId);
                    if (t.Lock != null)
                    {
                        var user = _raamClient.GetUserByUserSID(t.Lock.UserSID);
                        t.Lock.User = $"{user.Person.FirstName} {user.Person.LastName}";
                        t.Lock.UserEntity = _raamClient.GetEntityName(user.EntityId);
                        t.Lock.LockedDateString = t.Lock.LockedDate.ToString(CultureInfo.InvariantCulture);
                    }
                    t.UserCanOverrideUnlock = operatorCanOverRideLock;
                    t.UserCanUnlock = t.Lock != null && t.Lock.UserSID == _serviceContext.GetSID();
                }


                response.Data = new PostDepositTransactionsResponseDto();
                response.Data.Transactions = list;
                response.Data.TotalFilteredRecords = recordsfiltered;
                response.Data.TotalRecords = recordstotal;
                return response;
            });

        }

        private BaseGenericResponse<PostDepositTransactionDetailResponseDto> GetTransactionInternal(
            PostDepositTransactionRequestDto request)
        {
            return LogPerformance("Post-Deposit Exceptions > GetTransaction", () =>
            {
                var handler = new GetTransactionHandler(_serviceContext, _postDepositExceptionsDal, _raamClient,
                    _lockboxApi, _payments, _stubs, _dataEntry, _postDeposit, _images, _documents);
                return handler.Execute(request);
            });
        }
        private T LogPerformance<T>(string actionName, Func<T> action)
        {
            try
            {
                using (_performanceLogger.StartPerformanceTimer(actionName))
                {
                    return action();
                }
            }
            finally
            {
                var actions = _performanceLogger.GetMeasuredActions();
                var reportLines = PerformanceLogReporter.GetReport(actions);
                var report =
                    new[] {$"Performance timings for {actionName}:"}
                    .Concat(reportLines.Select(line => $"\t{line}"));
                var message = string.Join("\r\n", report);
                OnLogEvent(message, GetType().Name, MessageType.Information, MessageImportance.Debug);
            }
        }

        public BaseGenericResponse<PostDepositTransactionDetailResponseDto> GetTransaction(
            PostDepositTransactionRequestDto request)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<PostDepositTransactionDetailResponseDto>>(
                (result, userId, sessionId) => GetTransactionInternal(request));
        }

        private BaseGenericResponse<PostDepositTransactionSaveResultDto> SaveTransactionInternal(
            PostDepositTransactionSaveDto request)
        {
            return LogPerformance("Post-Deposit Exceptions > SaveTransaction", () =>
            {
                var handler = new SaveTransactionHandler(_serviceContext, _postDepositExceptionsDal, _raamClient,
                    _lockboxApi, _payments, _stubs, _dataEntry, _postDeposit, _images, _documents,
                    _businessRulesEngine, LogWarning, _r360Dal, _workgroupBuinessRuleEngine);
                
                return handler.Execute(request);
            });
        }

        private void LogWarning(string message, string source)
        {
            OnLogEvent(message, source, MessageType.Warning, MessageImportance.Essential);
        }

        public BaseGenericResponse<PostDepositSavePayerResultDto> SavePayer(PostDepositSavePayerDto request)
        {
            var user = _raamClient.GetUserByUserSID(_serviceContext.GetSID());
            var username = $"{user.Person.FirstName} {user.Person.LastName}";
            return ValidateUserIdAndDo<BaseGenericResponse<PostDepositSavePayerResultDto>>(
                (defaultResult, userId, sessionId) =>
                {
                    if ( _postDepositExceptionsDal.SavePayer(new PostDepositSavePayerSqlDto
                    {
                        SiteBankId = request.BankId,
                        SiteClientAccountId = request.ClientAccountId,
                        RoutingNumber = request.RoutingNumber,
                        Account = request.Account,
                        PayerName = request.PayerName,
                        UserName = username,
                        UserId = userId
                    } ) )
                        return new BaseGenericResponse<PostDepositSavePayerResultDto> { Status = StatusCode.SUCCESS};
                    else
                    {
                        return new BaseGenericResponse<PostDepositSavePayerResultDto>
                            {Status = StatusCode.FAIL};
                    }
                });
        }

        public BaseGenericResponse<PayerResponseDto> GetPayerList(PayerLookupRequestDto request)
        {
            return new BaseGenericResponse<PayerResponseDto>
            {
                Data = new PayerResponseDto
                {
                    PayerList = _payerLookupRepository.TryGet(request).GetResult(new List<PayerDto>())
                }

            };
        }

        public BaseGenericResponse<PostDepositTransactionSaveResultDto> SaveTransaction(
            PostDepositTransactionSaveDto request)
        {
            return ValidateUserIdAndDo<BaseGenericResponse<PostDepositTransactionSaveResultDto>>(
                (defaultResult, userId, sessionId) =>
                {
                    // Make sure it's locked.
                    var status =
                        _postDeposit.GetTransactionLock(request.BatchId, request.DepositDate, request.TransactionId);
                    if (status == null)
                    {
                        OnLogEvent($"Unable to save, Batch {request.BatchId} Transaction {request.TransactionId} Not locked", GetType().Name,
                            MessageType.Information, MessageImportance.Essential);
                        return new BaseGenericResponse<PostDepositTransactionSaveResultDto>
                        {
                            Status = StatusCode.FAIL,
                            Errors = new List<string>()
                            {
                                $"Unable to save, Transaction {request.TransactionId} is no longer locked."
                            }
                        };
                    }
                    // Make sure it's locked by the current user.
                    if (status.UserSID != _serviceContext.GetSID())
                    {
                        OnLogEvent($"Unable to save, Batch {request.BatchId} Transaction {request.TransactionId}  Not locked by current user", GetType().Name,
                            MessageType.Information, MessageImportance.Essential);

                        return new BaseGenericResponse<PostDepositTransactionSaveResultDto>
                        {
                            Status = StatusCode.FAIL,
                            Errors = new List<string>()
                            {
                                $"Unable to save, Transaction {request.TransactionId} is no longer locked by you."
                            }
                        };
                    }
                    return SaveTransactionInternal(request);
                });
        }

        public BaseGenericResponse<bool> CompleteTransaction(PostDepositTransactionRequestDto request)
        {
            var result = _postDepositExceptionsDal.CompleteTransaction(request.BatchId, request.DepositDate,
                request.TransactionId, _ServiceContext.GetLoginNameForAuditing());
            return new BaseGenericResponse<bool>
            {
                Status = result ?  StatusCode.SUCCESS : StatusCode.FAIL,
                Data = result,
                Errors = result ? new List<string>() : new List<string> { "There was an error completing the transaction" }
            };

        }

        public BaseResponse AuditTransactionDetailView(PostDepositExceptionTransactionDto request)
        {
            var result = _postDepositExceptionsDal.AuditTransactionDetailView(request.BatchId, request.DepositDate,
                request.TransactionId, _ServiceContext.GetLoginNameForAuditing(), _ServiceContext.GetSID());
            if (!result)
                return new BaseResponse()
                {
                    Status = StatusCode.FAIL,
                    Errors = new List<string>() { "Error writting audit transaction detail view message." }
                };
            return new BaseResponse()
            {
                Status = StatusCode.SUCCESS
            };
        }

        public BaseGenericResponse<PostDepositTransactionValidationResponseDto> GetTransactionValidationResults(
            PostDepositTransactionDetailResponseDto transactionDto)
        {
            var transaction = ConvertTransactionResponseDtoToTransaction(transactionDto);
            var transactionErrors = _businessRulesEngine.DetailedValidation(transaction);
            return new BaseGenericResponse<PostDepositTransactionValidationResponseDto>()
            {
                Data = ConvertDetailedValidationResponseToValidationResponseDto(transactionErrors)
            };
        }

        private Transaction<StubWithKeys> ConvertTransactionResponseDtoToTransaction(
            PostDepositTransactionDetailResponseDto transactionDto)
        {
            return transactionDto == null
                ? null
                : new Transaction<StubWithKeys>
                {
                    SiteBankId = transactionDto.BankId,
                    SiteWorkgroupId = transactionDto.WorkgroupId,
                    PaymentSourceKey = transactionDto.PaymentSourceKey,
                    Payments = transactionDto.Payments.Select(ConvertPaymentDTOToPayment).ToList(),
                    Stubs = transactionDto.Stubs.Select(ConvertStubDTOToStub).ToList()
                };
        }

        private Payment ConvertPaymentDTOToPayment(PaymentDto payment)
        {
            return new Payment()
            {
                Payer = payment.Payer,
                DataEntryFields = payment.DataEntryFields.Select(
                    dataEntryField => new DataEntryField()
                    {
                        Name = dataEntryField.FieldName,
                        Value = dataEntryField.Value
                    }).ToList()
            };
        }

        private StubWithKeys ConvertStubDTOToStub(PostDepositStubDetailDto stub)
        {
            return new StubWithKeys
            {
                BatchSequence = stub.BatchSequence,
                StubSequence = stub.StubSequence,
                DataEntryFields = stub.DataEntryFields.Select(
                    dataEntryField => new DataEntryField()
                    {
                        Name = dataEntryField.FieldName,
                        Value = dataEntryField.Value
                    }).ToList()
            };
        }

        private PostDepositTransactionValidationResponseDto ConvertDetailedValidationResponseToValidationResponseDto(
            DetailedValidationResponse<StubWithKeys> validationResponse)
        {
            return new PostDepositTransactionValidationResponseDto()
            {
                StubDataEntryExceptions =
                    from pair in validationResponse.StubErrors
                    let stub = pair.Key
                    let errors = pair.Value
                    from error in errors
                    select new PostDepositStubDataEntryExceptionDto
                    {
                        BatchSequence = stub.BatchSequence,
                        StubSequence = stub.StubSequence,
                        FieldName = error.Field,
                        Error = error.Error,
                    }
            };
        }

        public BaseResponse UnlockTransactionWithoutOverride(LockTransactionRequestDto dto)
        {
            return ValidateUserIdAndDo<BaseResponse>((result, userid, sessionid) =>
            {
                // Make sure it's locked.
                var status = _postDeposit.GetTransactionLock(dto.BatchId, dto.DepositDate, dto.TransactionId);
                if (status == null)
                    return new BaseResponse()
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string>() { "Transaction already unlocked." }
                    };
                // Make sure it's locked by the current user.
                if (status.UserSID != _serviceContext.GetSID())
                {
                    return new BaseResponse()
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string>() {$"Transaction {dto.TransactionId} is locked by another user."}
                    };
                }
                // Attempt to unlock.
                if (!_postDeposit.UnlockTransaction(dto.BatchId, dto.DepositDate, dto.TransactionId))
                    return new BaseResponse()
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string>() { $"Could not unlock transaction {dto.TransactionId}." }
                    };
                return new BaseResponse()
                {
                    Status = StatusCode.SUCCESS
                };
            });
        }

        public BaseResponse UnlockTransaction(LockTransactionRequestDto dto)
        {
            return ValidateUserIdAndDo<BaseResponse>((result, userid, sessionid) =>
            {
                // Make sure it's locked.
                var status = _postDeposit.GetTransactionLock(dto.BatchId, dto.DepositDate, dto.TransactionId);
                if (status == null)
                    return new BaseResponse()
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string>() { "Transaction already unlocked." }
                    };
                // Make sure it's locked by the current user.
                if (status.UserSID != _serviceContext.GetSID())
                {
                    //check for unlock override
                    if (_permissionprovider.HasPermission(R360Permissions.Perm_PostDepositUnlockOverride,
                        R360Permissions.ActionType.Manage))
                    {
                        OnLogEvent("Batch " + dto.BatchId + " unlocked via override", GetType().Name, MessageType.Information, MessageImportance.Essential);
                    }
                    else
                        return new BaseResponse()
                        {
                            Status = StatusCode.FAIL,
                            Errors = new List<string>() { $"Transaction {dto.TransactionId} is locked by another user." }
                        };
                }
                // Attempt to unlock.
                if (!_postDeposit.UnlockTransaction(dto.BatchId, dto.DepositDate, dto.TransactionId))
                    return new BaseResponse()
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string>() { $"Could not unlock transaction {dto.TransactionId}." }
                    };
                return new BaseResponse()
                {
                    Status = StatusCode.SUCCESS
                };
            });
        }

        public BaseResponse LockTransaction(LockTransactionRequestDto dto)
        {
            return ValidateUserIdAndDo<BaseResponse>((result, userid, sessionid) =>
            {
                // Make sure it's not already locked.
                var status = _postDeposit.GetTransactionLock(dto.BatchId, dto.DepositDate, dto.TransactionId);
                if (status != null)
                    return new BaseResponse()
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string>() { "Transaction already locked." }
                    };
                if (!_postDeposit.LockTransaction(dto.BatchId, dto.DepositDate, dto.TransactionId))
                    return new BaseResponse()
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string>() { $"Could not lock transaction {dto.TransactionId}." }
                    };
                return new BaseResponse()
                {
                    Status = StatusCode.SUCCESS
                };
            });
        }

        public BaseResponse WriteAuditEvent(string eventName, string eventType, string applicationName, string description)
        {
            return ValidateUserIdAndDo<BaseResponse>((result, userID, sessionID) =>
            {
                if (_r360Dal.WriteAuditEvent(userID, eventName, eventType, applicationName, description))
                {
                    result.Status = StatusCode.SUCCESS;
                }
                return result;
            });
        }

        #region Wrapper Method
        protected override ActivityCodes RequestType { get; }
        private T ValidateUserIdAndDo<T>(Func<T, int, Guid, T> operation, [CallerMemberName] string procName = null)
          where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T { Status = StatusCode.FAIL };

            try
            {
                int userID;
                // Validate / get the user ID
                if (ValidateSIDAndLog(procName, out userID))
                {
                    // Perform the requested operation
                    //return operation(result, R360ServicesDAL, userID, _ServiceContext.GetSessionID());
                    return operation(result, userID, _serviceContext.GetSessionID());
                }
                else
                {
                    result.Errors.Add("Invalid SID");
                }
            }
            catch (UserFacingException ex)
            {
                // UserFacingException is meant to be displayed directly to the user; no need to log
                result.Errors.Add(ex.Message);
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }

        #endregion
        #region Dispose
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    _r360Dal?.Dispose();
                    _postDepositExceptionsDal?.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                _disposed = true;

            }
        }
        #endregion
    }
}
