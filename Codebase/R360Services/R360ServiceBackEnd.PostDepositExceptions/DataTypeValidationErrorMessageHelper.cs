using System;
using System.Collections.Generic;
using WFS.RecHub.Common;

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public static class DataTypeValidationErrorMessageHelper
    {
        private static readonly Dictionary<DataEntryDataTypeValidationError, string> DataTypeValidationErrorMessages =
            new Dictionary<DataEntryDataTypeValidationError, string>
            {
                {DataEntryDataTypeValidationError.InvalidCurrencyValue, "Currency field"},
                {DataEntryDataTypeValidationError.InvalidNumericValue, "Numeric field"},
                {DataEntryDataTypeValidationError.InvalidDateTimeValue, "Date field"},
                {DataEntryDataTypeValidationError.MaxCurrencyValueExceeded, "Maximum currency value exceeded"},
                {DataEntryDataTypeValidationError.MinCurrencyValueExceeded, "Minimum currency value exceeded"}
            };

        public static string GetDataTypeValidationErrorMessage(DataEntryDataTypeValidationError dataTypeError)
        {
            if (!DataTypeValidationErrorMessages.TryGetValue(dataTypeError, out string message))
            {
                //This should not happen, just in case if the enum is changed and corresponding string value is not added to the dictionary.
                throw new Exception(
                    $"Message entry is missing for the DataEntryDataTypeValidationError enum value '{dataTypeError}'");
            }
            return message;
        }
    }
}