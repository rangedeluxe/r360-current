﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using JetBrains.Annotations;
using WFS.RecHub.ApplicationBlocks.DataAccess;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.PostDepositBusinessRules;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesAPI;

namespace WFS.RecHub.R360Services.PostDepositExceptions.Handlers
{
    public class SaveTransactionHandler
    {
        private class StubWithBatchSequence : Stub
        {
            public int BatchSequence { get; set; }
        }

        private const string BadDataTypeMessage =
            "Please address the invalid data below before navigating to another transaction.";

        private const string EVENTTYPE = "Post-Deposit Exceptions";
        private readonly IServiceContext _serviceContext;
        private readonly IPostDepositExceptionsDal _postDepositExceptionsDal;
        private readonly IRaamClient _raamClient;
        private readonly ILockboxAPI _lockboxApi;
        private readonly IPaymentDataProvider _payments;
        private readonly IStubsDataProvider _stubs;
        private readonly IDataEntryDataProvider _dataEntry;
        private readonly IPostDepositDataProvider _postDeposit;
        private readonly IImageProvider _images;
        private readonly IDocumentDataProvider _documents;
        private readonly BusinessRulesEngine _businessRulesEngine;
        private readonly Action<string, string> _logWarning;
        private readonly IR360ServicesDAL _r360Dal;
        private readonly BusinessRulesEngine _workgroupBusinessRuleEngine;
        public SaveTransactionHandler(IServiceContext serviceContext,
            IPostDepositExceptionsDal postDepositExceptionsDal, IRaamClient raam, ILockboxAPI lockboxApi,
            IPaymentDataProvider payments, IStubsDataProvider stubs, IDataEntryDataProvider dataEntry,
            IPostDepositDataProvider postDeposit, IImageProvider images, IDocumentDataProvider documents,
            BusinessRulesEngine businessRulesEngine, Action<string, string> logWarning, IR360ServicesDAL r360Dal, 
            BusinessRulesEngine workgroupBusinessRuleEngine)
        {
            _serviceContext = serviceContext;
            _postDepositExceptionsDal = postDepositExceptionsDal;
            _raamClient = raam;
            _lockboxApi = lockboxApi;
            _payments = payments;
            _stubs = stubs;
            _dataEntry = dataEntry;
            _postDeposit = postDeposit;
            _images = images;
            _documents = documents;
            _businessRulesEngine = businessRulesEngine;
            _logWarning = logWarning;
            _r360Dal = r360Dal;
            _workgroupBusinessRuleEngine = workgroupBusinessRuleEngine;
        }

        private GetTransactionHandler CreateHandlerForGetTransaction()
        {
            return new GetTransactionHandler(_serviceContext, _postDepositExceptionsDal, _raamClient,
                _lockboxApi, _payments, _stubs, _dataEntry, _postDeposit, _images, _documents);
        }
        private LoadedTransaction GetTransaction(long batchId, DateTime depositDate, int transactionId)
        {
            var request = new PostDepositTransactionRequestDto
            {
                BatchId = batchId,
                DepositDate = depositDate,
                TransactionId = transactionId
            };
            var handler = CreateHandlerForGetTransaction();
            var response = handler.Execute(request);
            
            if (response == null || response.Status != StatusCode.SUCCESS || response.Data == null)
                throw new Exception("Failed to retrieve Transaction.");
            
            return new LoadedTransaction(
                siteBankId: response.Data.BankId,
                siteWorkgroupId: response.Data.WorkgroupId,
                paymentSourceKey: response.Data.PaymentSourceKey,
                dataEntrySetupList: response.Data.DataEntrySetupList,
                stubs: response.Data.Stubs,
                workgroup: response.Data.Workgroup,
                paymentSource: response.Data.PaymentSource, payments:response.Data.Payments);
        }
        public BaseGenericResponse<PostDepositTransactionSaveResultDto> Execute(PostDepositTransactionSaveDto request)
        {
            EnsureUserHasTransactionLocked(request.BatchId, request.DepositDate, request.TransactionId);

            var loadedTransaction = GetTransaction(request.BatchId, request.DepositDate, request.TransactionId);
            var processedRequest = ProcessRequest(request.Checks, request.Stubs, loadedTransaction);
            
            var businessRuleExceptions = GetBusinessRuleExceptions(processedRequest);
            var checkRulesExceptions = GetChecksExceptions(processedRequest);
            
            return processedRequest.CanSave
                ? Save(request, loadedTransaction, processedRequest, businessRuleExceptions, checkRulesExceptions)
                : GetFailedSaveResponse(processedRequest.StubDataTypeErrors, businessRuleExceptions, checkRulesExceptions);
        }

        private IEnumerable<PostDepositCheckSaveErrorDto> GetChecksExceptions(ProcessedRequest request)
        {
            var businessRulesTransaction = request.CreateBusinessRulesTransaction();
            var validationResults = _workgroupBusinessRuleEngine.PaymentDetailedValidation(businessRulesTransaction);
            return
              from pair in validationResults.PaymentErrors
              let pay = pair.Key
              let errors = pair.Value
              from error in errors
              select new PostDepositCheckSaveErrorDto
              {
                  CheckSequence = pay.CheckSequence,
                  FieldName = error.Field,
                  Error = error.Error,
              };
        }

        private IEnumerable<PostDepositStubSaveErrorDto> GetBusinessRuleExceptions(ProcessedRequest processedRequest )
        {
            var businessRulesTransaction = processedRequest.CreateBusinessRulesTransaction();

            var validationResults = _businessRulesEngine.DetailedValidation(businessRulesTransaction);
            if (validationResults.StubErrors.Count > 0)
            {
                return
                    from pair in validationResults.StubErrors
                    let stub = pair.Key
                    let errors = pair.Value
                    from error in errors
                    select new PostDepositStubSaveErrorDto
                    {
                        BatchSequence = stub.BatchSequence,
                        FieldName = error.Field,
                        Error = error.Error,
                    };
            }
            return
                from pair in validationResults.NonStubErrors
                select new PostDepositStubSaveErrorDto
                {
                    BatchSequence = -1,
                    FieldName = pair.Field,
                    Error = pair.Error
                };
        }

        private static BaseGenericResponse<PostDepositTransactionSaveResultDto> GetFailedSaveResponse(
            IEnumerable<PostDepositStubSaveErrorDto> stubDataTypeErrors,
            IEnumerable<PostDepositStubSaveErrorDto> businessRuleExceptions,
            IEnumerable<PostDepositCheckSaveErrorDto> checksExceptions)
        {
            return new BaseGenericResponse<PostDepositTransactionSaveResultDto>
            {
                Status = StatusCode.FAIL,
                Errors = new List<string> {BadDataTypeMessage},
                Data = new PostDepositTransactionSaveResultDto
                {
                    DataTypeErrors = stubDataTypeErrors.ToList(),
                    BusinessRuleExceptions = businessRuleExceptions.ToList(),
                    PaymentExceptions = checksExceptions.ToList()
                },
            };
        }
        private BaseGenericResponse<PostDepositTransactionSaveResultDto> Save(PostDepositTransactionSaveDto request,
            LoadedTransaction loadedTransaction, ProcessedRequest processedRequest,
            IEnumerable<PostDepositStubSaveErrorDto> businessRuleExceptions,
            IEnumerable<PostDepositCheckSaveErrorDto> checksExceptions)
        {
            var dataEntryFieldsForDal = processedRequest.StubsToSave;
            UpdateStubRows(request, loadedTransaction, dataEntryFieldsForDal);
            InsertStubRows(request, loadedTransaction, dataEntryFieldsForDal);
            DeleteStubRows(request, loadedTransaction, request.Stubs);
            UpdatePayments(request, loadedTransaction, request.Checks);
            LogUpdateAuditDetailTransaction(request, loadedTransaction);

            var user = _raamClient.GetUserByUserSID(_serviceContext.GetSID());
            var userId = getCurrentUserId();
            var username = $"{user.Person.FirstName} {user.Person.LastName}";
            
            foreach (var payer in request.Payers)
            {
                if (string.IsNullOrEmpty(payer.PayerName)) continue;
                PostDepositSavePayerSqlDto payerToSave = new PostDepositSavePayerSqlDto
                {
                    SiteBankId = payer.BankId,
                    SiteClientAccountId = payer.ClientAccountId,
                    RoutingNumber = payer.RoutingNumber,
                    Account = payer.Account,
                    PayerName = payer.PayerName,
                    UserId = userId,
                    UserName = username
                };

                if (!_postDepositExceptionsDal.SavePayer(payerToSave))
                {
                    throw new Exception($"Database error attempting to save payer {payerToSave.PayerName}");
                }
            }
            
            return new BaseGenericResponse<PostDepositTransactionSaveResultDto>
            {
                Status = StatusCode.SUCCESS,
                Data = new PostDepositTransactionSaveResultDto
                {
                    BusinessRuleExceptions = businessRuleExceptions.ToList(),
                    PaymentExceptions = checksExceptions.ToList()
                },
            };
        }

        private void UpdatePayments(PostDepositTransactionSaveDto request, LoadedTransaction loadedTransaction, 
            IList<PostDepositCheckSaveDto> requestChecks)
        {
            foreach (var check in request.Checks) { if (string.IsNullOrEmpty(check.Payer)) { check.Payer = ""; } }

            var checksToUpdate = requestChecks
                .Where(sent => loadedTransaction.Payments.Any(s => s.PaymentSequence== sent.CheckSequence))
                .ToList();
            if (checksToUpdate.Any())
            {
                var saveRequest = new PostDepositTransactionSaveSqlDto
                {
                    BatchId = request.BatchId,
                    DepositDate = request.DepositDate,
                    TransactionId = request.TransactionId,
                    Checks = checksToUpdate
                };
                if (!_postDepositExceptionsDal.SavePaymentsRemitters(saveRequest))
                {
                    throw new Exception("Database error attempting to update payment rows");
                }
            }
        }

        private void EnsureUserHasTransactionLocked(long batchId, DateTime depositDate, int transactionId)
        {
            var status = _postDeposit.GetTransactionLock(batchId, depositDate, transactionId);
            if (status == null || status.UserSID != _serviceContext.GetSID())
            {
                LogWarning("User attempted to save a transaction that was not locked by the user.");
                throw new UserFacingException("Access denied.");
            }
        }
        private void UpdateStubRows(PostDepositTransactionSaveDto request, LoadedTransaction loadedTransaction,
            [NotNull] IEnumerable<PostDepositStubSaveSqlDto> dataEntryFieldsForDal)
        {
            if (dataEntryFieldsForDal == null)
                throw new ArgumentNullException(nameof(dataEntryFieldsForDal));

            var stubstoupdate = dataEntryFieldsForDal
                .Where(sent => loadedTransaction.Stubs.Any(s => s.BatchSequence == sent.BatchSequence))
                .ToList();
            if (stubstoupdate.Any())
            {
                // This is just updates, so we'll filter based on rows that already exist in the transaction.
                var saverequest = new PostDepositTransactionSaveSqlDto()
                {
                    BatchId = request.BatchId,
                    DepositDate = request.DepositDate,
                    TransactionId = request.TransactionId,
                    DataEntryFields = stubstoupdate
                };
                if (!_postDepositExceptionsDal.SaveTransaction(saverequest))
                {
                    throw new Exception("Database error attempting to update stub rows");
                }
            }
        }

        private void LogUpdateAuditDetailTransaction(PostDepositTransactionSaveDto request, LoadedTransaction oldTransaction)
        {
            int userId = getCurrentUserId();
            _r360Dal.GetBankName(oldTransaction.SiteBankId, out var bankName);
            var userNameFromSid =_raamClient.GetNameFromSid(_serviceContext.GetSID());
            var currentChecks = request.Checks?.ToList();
            var currentStubs = request.Stubs?.ToList();
            var oldChecks = oldTransaction.Payments?.ToArray();
            var oldStubs = oldTransaction.Stubs?.ToList();

            try
            {
                var auditMessage =
                    $"{userNameFromSid} keyed an exception for Bank: {oldTransaction.SiteBankId}-{bankName}, " +
                    $"Workgroup: {oldTransaction.Workgroup}, Batch: {request.SourceBatchId} , Transaction: {request.TransactionId}" +
                    $", for Deposit Date: {request.DepositDate.ToShortDateString()}" +
                    $", Payment Source: {oldTransaction.PaymentSource} and ";

                if (currentChecks != null)
                {
                    foreach (var currentCheck in currentChecks)
                    {
                        var auditDetail = new StringBuilder();
                        var oldCheck = oldChecks?.FirstOrDefault(x => x.PaymentSequence == currentCheck.CheckSequence);

                        if (oldCheck != null && currentCheck.Payer != oldCheck?.Payer && currentCheck.CheckSequence > 0)
                        {
                            if (auditDetail.Length > 0)
                                auditDetail.Append(",");
                            auditDetail.Append($" Payer was changed from '{oldCheck?.Payer}' to '{currentCheck.Payer}'");
                        }

                        if (auditDetail.Length > 0)
                        {
                            auditDetail.Append(".");
                            _r360Dal.WriteAuditEvent(userId, "Post-Deposit Exceptions",
                                "Post-Deposit Exceptions",
                                "Exceptions Summary/Detail/Transaction Detail",
                                $"{auditMessage}Check Sequence: {oldCheck?.PaymentSequence}. " +
                                auditDetail);
                        }
                    }
                }

                if (currentStubs != null)
                    foreach (var currentStub in currentStubs)
                    {
                        StringBuilder auditDetail = new StringBuilder();
                        var oldStub = oldStubs.FirstOrDefault(x => x.BatchSequence == currentStub.BatchSequence);

                        if (currentStub != null &&
                            oldStub != null &&
                            currentStub.BatchSequence > 0)
                        {
                            foreach (var currentDeField in currentStub.DataEntryFields)
                            {
                                var oldDeField =
                                    oldStub.DataEntryFields.FirstOrDefault(x =>
                                        x.FieldName == currentDeField.FieldName);

                                if ((currentDeField.Value ?? "") != (oldDeField?.Value ?? ""))
                                {
                                    if (auditDetail.Length > 0)
                                        auditDetail.Append(",");
                                    auditDetail.Append(
                                        $" {currentDeField.FieldName} was changed from '{oldDeField?.Value}' to '{currentDeField.Value}'");
                                }
                            }
                        }

                        if (auditDetail.Length > 0)
                        {
                            auditDetail.Append(".");
                            _r360Dal.WriteAuditEvent(userId, "Post-Deposit Exceptions",
                                "Post-Deposit Exceptions",
                                "Exceptions Summary/Detail/Transaction Detail",
                                $"{auditMessage}Stub Sequence: {oldStub?.StubSequence}. " +
                                auditDetail);
                        }
                    }
            }
            catch (Exception e)
            {
                LogWarning($"Incomplete auditing for this exception update. Exception:  {e.Message}");
            }
        }

        private void InsertStubRows(PostDepositTransactionSaveDto request, LoadedTransaction loadedTransaction,
            IEnumerable<PostDepositStubSaveSqlDto> dataEntryFieldsForDal)
        {
            var stubstoadd = dataEntryFieldsForDal
                .Where(sent => !loadedTransaction.Stubs.Any(stub => stub.BatchSequence == sent.BatchSequence))
                .ToList();
            if (stubstoadd.Any())
            {
                LogInsertAuditDetailTransaction(request,loadedTransaction);
                if (!_postDepositExceptionsDal.InsertStubs(request.BatchId, request.DepositDate,
                    request.TransactionId, _serviceContext.GetSessionID(), stubstoadd))
                {
                    throw new Exception("Database error attempting to insert stub rows");
                }
            }
        }

        private void LogInsertAuditDetailTransaction(PostDepositTransactionSaveDto request, LoadedTransaction oldTransaction)
        {
            int userId = getCurrentUserId();
            _r360Dal.GetBankName(oldTransaction.SiteBankId, out var bankName);
            var userNameFromSid = _raamClient.GetNameFromSid(_serviceContext.GetSID());
            var currentStubs = request.Stubs?.ToList();

            if (currentStubs == null) return;

            foreach (var currentStub in currentStubs)
            {
                if (currentStub != null &&
                    currentStub.BatchSequence < 0)
                {
                    StringBuilder auditDetail = new StringBuilder();
                    foreach (var currentDeField in currentStub.DataEntryFields)
                    {
                        if (auditDetail.Length > 0)
                            auditDetail.Append(",");
                        auditDetail.Append($" {currentDeField.FieldName} was inserted with value '{currentDeField.Value}'");
                    }

                    if (auditDetail.Length > 0)
                    {
                        auditDetail.Append(".");
                        _r360Dal.WriteAuditEvent(userId, "Post-Deposit Exceptions",
                            "Post-Deposit Exceptions",
                            "Exceptions Summary/Detail/Transaction Detail",
                            $"{userNameFromSid} added a new related item for Bank: {oldTransaction.SiteBankId}-{bankName}, " +
                            $"Workgroup: {oldTransaction.Workgroup}, Batch: {request.SourceBatchId} , Transaction: {request.TransactionId}" +
                            $", for Deposit Date: {request.DepositDate.ToShortDateString()}" +
                            $", Payment Source: {oldTransaction.PaymentSource} and Stub Sequence: 'Unassigned'. " +
                            auditDetail);
                    }
                }
            }
        }

        private void DeleteStubRows(PostDepositTransactionSaveDto request, LoadedTransaction loadedTransaction,
            IList<PostDepositStubSaveDto> sentstubs)
        {
            var stubstodelete = loadedTransaction.Stubs
                .Where(stub => !sentstubs.Any(sent =>
                    sent.BatchSequence == stub.BatchSequence && stub.DataEntryFields.Any(de => de.IsEditable)));

            
            foreach (var stub in stubstodelete)
            {
                // Ensure that: transaction is locked AND current user locked it AND
                // all data entry columns in this stub have been added by the UI.
                if (stub.DataEntryFields.Any(field => !field.IsUIAddedField))
                {
                    LogWarning("User attempted to delete row incorrectly. Skipping row.");
                    continue;
                    
                }
                _r360Dal.GetBankName(loadedTransaction.SiteBankId, out var bankName);;
                _r360Dal.WriteAuditEvent(getCurrentUserId(),
                    EVENTTYPE , EVENTTYPE, "PostDepositExceptions/Save",
                    $"{_raamClient.GetNameFromSid(_serviceContext.GetSID())}" +
                    $" deleted a related item from Batch: {request.SourceBatchId}, Transaction: {request.TransactionId}" +
                    $", for Deposit Date: {request.DepositDate.ToShortDateString()}" +
                    $", Bank: {loadedTransaction.SiteBankId} - {bankName}, " +
                    $"Workgroup: {loadedTransaction.SiteWorkgroupId} - {loadedTransaction.Workgroup} " +
                    $", Payment Source: {loadedTransaction.PaymentSource} and Stub Sequence: {stub.StubSequence}");
                _postDepositExceptionsDal.DeleteStub(request.BatchId, request.DepositDate,
                    request.TransactionId, stub.BatchSequence);
            }
        }

        private int getCurrentUserId()
        {
            var userId = -1;

            if (_r360Dal.GetUserIDBySID(_serviceContext.GetSID(), out var dtResults))
            {
                userId = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
            }

            return userId;
        }

        private void LogWarning(string message)
        {
            _logWarning?.Invoke(message, GetType().Name);
        }

        private ProcessedRequest ProcessRequest(IList<PostDepositCheckSaveDto> checks,
            IEnumerable<PostDepositStubSaveDto> clientStubs, LoadedTransaction loadedTransaction)
        {
            var processedRequest = new ProcessedRequest(
                siteBankId: loadedTransaction.SiteBankId,
                siteWorkgroupId: loadedTransaction.SiteWorkgroupId,
                paymentSourceKey: loadedTransaction.PaymentSourceKey);

            var fieldsToSave = GetFieldsToSave(clientStubs, loadedTransaction.StubDataEntrySetup);
            processedRequest.AddRange(fieldsToSave);

            processedRequest.ChecksToSave = checks;
            return processedRequest;
        }

        private static IEnumerable<FieldToSave> GetFieldsToSave(
            IEnumerable<PostDepositStubSaveDto> clientStubs, StubDataEntrySetup stubDataEntrySetup)
        {
            return
                from clientStub in clientStubs
                from field in clientStub.DataEntryFields
                let dataEntryFieldSetup = stubDataEntrySetup.TryGetByFieldName(field.FieldName)
                where dataEntryFieldSetup != null
                select new FieldToSave(
                    batchSequence: clientStub.BatchSequence,
                    fieldName: field.FieldName,
                    dataType: (DataEntryDataType) dataEntryFieldSetup.DataType,
                    value: field.Value);
        }

        private class FieldToSave
        {
            public FieldToSave(int batchSequence, string fieldName, DataEntryDataType dataType, string value)
            {
                BatchSequence = batchSequence;
                FieldName = fieldName;
                DataType = dataType;
                Value = value;
            }

            public int BatchSequence { get; }
            public string FieldName { get; }
            public DataEntryDataType DataType { get; }
            public string Value { get; }
        }

        private class ProcessedRequest
        {
            private readonly List<PostDepositStubSaveSqlDto> _stubsToSave = new List<PostDepositStubSaveSqlDto>();
            private readonly List<PostDepositStubSaveErrorDto> _stubDataTypeErrors = new List<PostDepositStubSaveErrorDto>();
            private IList<PostDepositCheckSaveDto> _checksToSave = new List<PostDepositCheckSaveDto>();
            public ProcessedRequest(int siteBankId, int siteWorkgroupId, int paymentSourceKey)
            {
                SiteBankId = siteBankId;
                SiteWorkgroupId = siteWorkgroupId;
                PaymentSourceKey = paymentSourceKey;
            }

            public int SiteBankId { get; }
            public int SiteWorkgroupId { get; }
            public int PaymentSourceKey { get; }
            public bool CanSave => !_stubDataTypeErrors.Any();
            public IEnumerable<PostDepositStubSaveErrorDto> StubDataTypeErrors => _stubDataTypeErrors;
            public IReadOnlyList<PostDepositStubSaveSqlDto> StubsToSave => _stubsToSave;
            public IList<PostDepositCheckSaveDto> ChecksToSave
            {
                get { return _checksToSave; }
                set { _checksToSave = value; }
            }

            private void Add(FieldToSave field)
            {
                var dataTypeError = GetDataTypeError(field);

                if (dataTypeError == null)
                    AddDalField(field);
                else
                    AddDataTypeError(field, dataTypeError.Value);
            }
            private void AddDataTypeError(FieldToSave field, DataEntryDataTypeValidationError dataTypeError)
            {
                _stubDataTypeErrors.Add(new PostDepositStubSaveErrorDto
                {
                    BatchSequence = field.BatchSequence,
                    FieldName = field.FieldName,
                    Error = DataTypeValidationErrorMessageHelper.GetDataTypeValidationErrorMessage(dataTypeError)
                });
            }
            public void AddRange(IEnumerable<FieldToSave> fields)
            {
                foreach (var field in fields)
                    Add(field);
            }
            private void AddDalField(FieldToSave fieldToSave)
            {
                var dalField = ToDalField(fieldToSave);
                var dalStub = FindOrCreateDalStub(fieldToSave.BatchSequence);
                dalStub.DataEntryFields.Add(dalField);
            }
            public Transaction<StubWithBatchSequence> CreateBusinessRulesTransaction()
            {
                return new Transaction<StubWithBatchSequence>
                {
                    SiteBankId = SiteBankId,
                    SiteWorkgroupId = SiteWorkgroupId,
                    PaymentSourceKey = PaymentSourceKey,
                    Stubs = StubsToSave.Select(CreateBusinessRulesStub).ToList(),
                    Payments = ChecksToSave.Select(CreateBusinessRulesPayment).ToList()
                };
            }
            private Payment CreateBusinessRulesPayment(PostDepositCheckSaveDto check)
            {
                
                return new Payment
                {
                    CheckSequence = check.CheckSequence,
                    Payer = check.Payer                
                };
            }
            private StubWithBatchSequence CreateBusinessRulesStub(PostDepositStubSaveSqlDto dalStub)
            {
                return new StubWithBatchSequence
                {
                    BatchSequence = dalStub.BatchSequence,
                    DataEntryFields = dalStub.DataEntryFields.Select(CreateBusinessRulesDataEntryField).ToList(),
                };
            }
            private DataEntryField CreateBusinessRulesDataEntryField(PostDepositDataEntryFieldSaveSqlDto dalField)
            {
                return new DataEntryField
                {
                    Name = dalField.FieldName,
                    Value = dalField.Value,
                };
            }
            private PostDepositStubSaveSqlDto FindOrCreateDalStub(int batchSequence)
            {
                var dalStub = _stubsToSave.FirstOrDefault(e => e.BatchSequence == batchSequence);
                if (dalStub == null)
                {
                    dalStub = new PostDepositStubSaveSqlDto
                    {
                        BatchSequence = batchSequence,
                        DataEntryFields = new List<PostDepositDataEntryFieldSaveSqlDto>()
                    };
                    _stubsToSave.Add(dalStub);
                }
                return dalStub;
            }
            private static DataEntryDataTypeValidationError? GetDataTypeError(FieldToSave field)
            {
                // As far as data-type validation is concerned, blanks are always considered OK.
                // (It's up to the user, via business rules, to decide when and whether blanks are exceptions.)
                if (IsFieldBlank(field))
                    return null;

                return DataEntryDataTypeValidation.ValidateInputValue(field.DataType, field.Value);
            }
            private static bool IsFieldBlank(FieldToSave fieldToSave)
            {
                return string.IsNullOrEmpty(fieldToSave.Value);
            }
            private static PostDepositDataEntryFieldSaveSqlDto ToDalField(FieldToSave fieldToSave)
            {
                var dalField = new PostDepositDataEntryFieldSaveSqlDto
                {
                    FieldName = fieldToSave.FieldName,
                    Value = fieldToSave.Value,
                };
                if (!IsFieldBlank(fieldToSave))
                {
                    switch (fieldToSave.DataType)
                    {
                        case DataEntryDataType.Currency:
                            dalField.ValueMoney = Convert.ToDecimal(fieldToSave.Value);
                            break;
                        case DataEntryDataType.Date:
                            dalField.ValueDateTime = DateTime.Parse(fieldToSave.Value);
                            break;
                        case DataEntryDataType.Numeric:
                            dalField.ValueFloat = float.Parse(fieldToSave.Value);
                            break;
                    }
                }
                return dalField;
            }
        }

        private class StubDataEntrySetup
        {
            private readonly Dictionary<string, DataEntrySetupDto> _dataEntrySetupByFieldName;

            private StubDataEntrySetup([NotNull] Dictionary<string, DataEntrySetupDto> dataEntrySetupByFieldName)
            {
                _dataEntrySetupByFieldName = dataEntrySetupByFieldName;
            }

            [NotNull]
            public static StubDataEntrySetup FromDataEntrySetupFields(
                [CanBeNull, ItemCanBeNull] IEnumerable<DataEntrySetupDto> list)
            {
                var stubFieldSetup = list
                    .ToNonNullListOfNonNullItems()
                    .Where(item => item.TableName == "Stubs");

                // There's no database index to prevent duplicate field names, so we can't use standard ToDictionary.
                var dictionary = stubFieldSetup.ToDictionaryIgnoringDuplicates(item => item.FieldName);

                return new StubDataEntrySetup(dictionary);
            }
            [CanBeNull]
            public DataEntrySetupDto TryGetByFieldName(string fieldName)
            {
                _dataEntrySetupByFieldName.TryGetValue(fieldName, out var result);
                return result;
            }
        }

        private class LoadedTransaction
        {
            public LoadedTransaction(int siteBankId, int siteWorkgroupId, int paymentSourceKey,
                [CanBeNull, ItemCanBeNull] IEnumerable<DataEntrySetupDto> dataEntrySetupList,
                [CanBeNull, ItemCanBeNull] IEnumerable<PostDepositStubDetailDto> stubs,
                string workgroup, string paymentSource, IEnumerable<PaymentDto> payments)
            {
                SiteBankId = siteBankId;
                SiteWorkgroupId = siteWorkgroupId;
                PaymentSourceKey = paymentSourceKey;
                StubDataEntrySetup = StubDataEntrySetup.FromDataEntrySetupFields(dataEntrySetupList);
                Stubs = stubs.ToNonNullListOfNonNullItems();
                Workgroup = workgroup;
                PaymentSource = paymentSource;
                Payments = payments;
            }

            public int PaymentSourceKey { get; }
            public int SiteBankId { get; }
            public int SiteWorkgroupId { get; }
            [NotNull]
            public StubDataEntrySetup StubDataEntrySetup { get; }
            [NotNull, ItemNotNull]
            public IEnumerable<PostDepositStubDetailDto> Stubs { get; }
            public string Workgroup { get; }
            public string PaymentSource { get; }
            public IEnumerable<PaymentDto> Payments { get; }

        }
    }
}
