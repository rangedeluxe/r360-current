﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Shared;
using WFS.RecHub.R360Services.R360ServicesAPI.DataProviders;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360RaamClient;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Services.R360ServicesAPI;

namespace WFS.RecHub.R360Services.PostDepositExceptions.Handlers
{
    public class GetTransactionHandler
    {
        private readonly IServiceContext _serviceContext;
        private readonly IPostDepositExceptionsDal _postDepositExceptionsDal;
        private readonly IRaamClient _raamClient;
        private readonly ILockboxAPI _lockboxApi;
        private readonly IPaymentDataProvider _payments;
        private readonly IStubsDataProvider _stubs;
        private readonly IDataEntryDataProvider _dataEntry;
        private readonly IXmlTransformer<List<PostDepositEntityDto>> _entitiesTransformer;
        private readonly IPostDepositDataProvider _postDeposit;
        private readonly IImageProvider _images;
        private readonly IDocumentDataProvider _documents;

        public GetTransactionHandler(IServiceContext serviceContext,
            IPostDepositExceptionsDal postDepositExceptionsDal, IRaamClient raam, ILockboxAPI lockboxApi,
            IPaymentDataProvider payments, IStubsDataProvider stubs, IDataEntryDataProvider dataEntry,
            IPostDepositDataProvider postDeposit, IImageProvider images, IDocumentDataProvider documents)
        {
            _serviceContext = serviceContext;
            _postDepositExceptionsDal = postDepositExceptionsDal;
            _raamClient = raam;
            _lockboxApi = lockboxApi;
            _payments = payments;
            _stubs = stubs;
            _dataEntry = dataEntry;
            _postDeposit = postDeposit;
            _images = images;
            _entitiesTransformer = new PostDepositEntityListXmlTransformer();
            _documents = documents;
        }

        public BaseGenericResponse<PostDepositTransactionDetailResponseDto> Execute(
            PostDepositTransactionRequestDto request)
        {
            var response = new BaseGenericResponse<PostDepositTransactionDetailResponseDto>();

            var req = new PostDepositTransactionRequestSqlDto()
            {
                BatchId = request.BatchId,
                DepositDate = request.DepositDate,
                SessionId = _serviceContext.GetSessionID(),
                TransactionId = request.TransactionId
            };
            DataTable dt;
            if (!_postDepositExceptionsDal.GetTransaction(req, out dt))
            {
                return new BaseGenericResponse<PostDepositTransactionDetailResponseDto>
                {
                    Status = StatusCode.FAIL,
                    Errors = new List<string> {"Error retrieving data from database."}
                };
            }

            response.Data = dt
                .ToObjectList<PostDepositTransactionDetailResponseDto>()
                .FirstOrDefault();

            // Gather our entity tree first.
            var ents = _raamClient.GetAuthorizedEntityList();
            var entlist = ents
                .Select(e => new PostDepositEntityDto()
                {
                    EntityHierarchy = _raamClient.GetEntityBreadcrumb(e.ID),
                    EntityId = e.ID.ToString()
                })
                .ToList();

            // Authorize transaction to this user.
            var authorizedworkgroups = _lockboxApi.GetUserLockboxes();
            var requestedworkgroup = authorizedworkgroups.Data
                .FirstOrDefault(x => x.Value.BankID == response.Data.BankId && x.Value.LockboxID == response.Data.WorkgroupId);
            if (requestedworkgroup.Equals(default(KeyValuePair<int, cOLLockbox>)))
            {
                response.Status = StatusCode.FAIL;
                response.Errors.Add("Unauthorized Request.");
                return response;
            }

            // Get the data entry fields associated with this batch.
            response.Data.DataEntrySetupList = _postDeposit.GetDataEntrySetupFields(response.Data.BankId,
                response.Data.WorkgroupId, response.Data.BatchId, response.Data.DepositDate);

            // Get the payments associated with the transaction.
            response.Data.Payments = _payments.GetPaymentsForTransaction(response.Data.BankId, response.Data.WorkgroupId,
                response.Data.BatchId, response.Data.DepositDate, response.Data.TransactionId);

            // Get the stubs associated with the transaction.
            response.Data.Stubs = _stubs.GetStubsForTransaction(response.Data.BankId, response.Data.WorkgroupId,
                response.Data.BatchId, response.Data.DepositDate, response.Data.TransactionId);

            // Get the documents associated with the transaction
            response.Data.Documents = _documents.GetTransactionDocuments(
                new cTransaction(response.Data.BankId, response.Data.WorkgroupId,
                response.Data.DepositDate, response.Data.BatchId, -1, response.Data.TransactionId,
                response.Data.TransactionSequence));

            // Get the locking status.
            response.Data.Lock = _postDeposit.GetTransactionLock(response.Data.BatchId, response.Data.DepositDate, response.Data.TransactionId);

            // Get the username for the lock.
            if (response.Data.Lock != null)
            {
                var user = _raamClient.GetUserByUserSID(response.Data.Lock.UserSID);
                response.Data.Lock.User = $"{user.Person.FirstName} {user.Person.LastName}";
            }

            // Get the data entry details for each of the stubs and payments.
            foreach (var p in response.Data.Payments)
            {
                p.DataEntryFields = _dataEntry.GetDataEntryFieldsForPayment(response.Data.BankId, response.Data.WorkgroupId, response.Data.BatchId,
                    response.Data.DepositDate, response.Data.TransactionId, p.BatchSequence);
                p.DataEntryFields = response.Data.DataEntrySetupList
                    .Join(
                        p.DataEntryFields,
                        x => x.FieldName + x.TableName + x.DataType,
                        x => x.FieldName + "Checks" + x.Type,
                        (x, y) => y)
                    .GroupBy(x => x.FieldName + "Checks" + x.Type)
                    .Select(x => x.First());
            }
            foreach (var s in response.Data.Stubs)
            {
                s.DataEntryFields = _postDeposit.GetPostDepositDataEntryFieldsForStub(
                    response.Data.BankId, response.Data.WorkgroupId, response.Data.BatchId,
                    response.Data.DepositDate, response.Data.TransactionId, s.BatchSequence);
                s.DataEntryFields = response.Data.DataEntrySetupList
                    .Join(
                        s.DataEntryFields,
                        x => x.FieldName + x.TableName + x.DataType,
                        x => x.FieldName + "Stubs" + x.Type,
                        (x, y) => y)
                    .GroupBy(x => x.FieldName + "Stubs" + x.Type)
                    .Select(x => x.First());
            }

            // This gets all of the available image request objects in the transaction,
            // so we know which images exist and which ones don't.
            var paymentitems = response.Data.Payments
                .Select(x => new ItemRequestDto()
                {
                    BatchId = response.Data.BatchId,
                    BatchSequence = x.BatchSequence,
                    DepositDate = response.Data.DepositDate
                });
            var paymentimages = _images.GetAvailablePaymentsImages(paymentitems);
            foreach (var p in response.Data.Payments)
                p.ImageIsAvailable = paymentimages
                    .FirstOrDefault(x => x.BatchSequence == p.BatchSequence) != null;


            var docitems = response.Data.Documents.Select(x => new ItemRequestDto
            {
                BatchId = response.Data.BatchId,
                BatchSequence = x.BatchSequence,
                DepositDate = response.Data.DepositDate
            });
            var docImages = _images.GetAvailableDocumentImages(docitems);
            foreach (var d in response.Data.Documents)
                d.ImageIsAvailable = docImages
                    .FirstOrDefault(x => x.BatchSequence == d.BatchSequence) != null;

            // Get all transactions only if we have RecordsTotal count, otherwise the call fails!
            if (request.RecordsTotal > 0)
            {
                // This just gets the whole list of transactions so we can navigate back and forth.
                var dbReq = new PostDepositPendingTransactionsRequestSqlDto()
                {
                    Length = request.RecordsTotal,
                    OrderBy = request.OrderBy,
                    OrderByDirection = request.OrderDirection,
                    Search = string.Empty,
                    SessionId = _serviceContext.GetSessionID(),
                    Start = 0,
                    Entities = _entitiesTransformer.ToXML(entlist)
                };

                int recordstotal;
                int recordsfiltered;
                if (!_postDepositExceptionsDal.GetPendingTransactions(dbReq, out recordstotal, out recordsfiltered, out dt))
                {
                    return new BaseGenericResponse<PostDepositTransactionDetailResponseDto>
                    {
                        Status = StatusCode.FAIL,
                        Errors = new List<string>() { "Error retrieving data from database." }
                    };
                }

                var transactionList = dt.ToObjectList<PostDepositExceptionTransactionDto>();
                if (response.Data != null)
                {
                    response.Data.Transactions = transactionList;
                    var entitybc = entlist.FirstOrDefault(x => int.Parse(x.EntityId) == response.Data.EntityId);
                    response.Data.EntityBreadcrumb = entitybc?.EntityHierarchy;
                }
            }
            return response;
        }
    }
}
