﻿using System;
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabaseDataEntryDataProvider : IDataEntryDataProvider
    {
        private readonly cItemProcDAL _dal;
        private readonly IServiceContext _context;
        public DatabaseDataEntryDataProvider(IServiceContext context)
        {
            _context = context;
            _dal = new cItemProcDAL(context.SiteKey);
        }

        public IEnumerable<DataEntryValueDto> GetDataEntryFieldsForPayment(int bankid, int workgroupid, long batchid, DateTime depositdate, int transactionid, int batchsequence)
        {
            DataTable dt;
            if (!_dal.GetTransactionChecksDE(_context.GetSessionID(), bankid, workgroupid, batchid, depositdate, transactionid, batchsequence, out dt))
            {
                return null;
            }
            return dt.ToObjectList<DataEntryValueDto>();
        }
    }
}
