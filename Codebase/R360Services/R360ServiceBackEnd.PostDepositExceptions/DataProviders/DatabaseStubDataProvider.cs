﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabaseStubDataProvider : IStubsDataProvider
    {
        private readonly cItemProcDAL _dal;
        private readonly IServiceContext _context;
        public DatabaseStubDataProvider(IServiceContext context)
        {
            _context = context;
            _dal = new cItemProcDAL(context.SiteKey);
        }
        public IEnumerable<PostDepositStubDetailDto> GetStubsForTransaction(int bankid, int workgroupid, long batchid,
            DateTime depositdate, int transactionid)
        {
            DataTable dt;
            var transaction = new cTransaction(bankid, workgroupid, depositdate, batchid, 0, transactionid, 0);
            if (!_dal.GetTransactionStubs(_context.GetSessionID(), transaction, out dt))
            {
                return null;
            }
            return dt.ToObjectList<PostDepositStubDetailDto>();
        }
    }
}