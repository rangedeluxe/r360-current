﻿using System;
using System.Collections.Generic;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public interface IStubsDataProvider
    {
        IEnumerable<PostDepositStubDetailDto> GetStubsForTransaction(int bankid, int workgroupid, long batchid,
            DateTime depositdate, int transactionid);
    }
}
