﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IPaymentDataProvider
    {
        IEnumerable<PaymentDto> GetPaymentsForTransaction(int bankid, int workgroupid, long batchid, DateTime depositdate, int transactionid);
    }
}
