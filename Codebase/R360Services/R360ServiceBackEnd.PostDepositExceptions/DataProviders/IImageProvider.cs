﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.OLFServices.DataTransferObjects;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IImageProvider
    {
        IEnumerable<SingleImageRequestDto> GetAvailablePaymentsImages(IEnumerable<ItemRequestDto> payments);
        IEnumerable<SingleImageRequestDto> GetAvailableStubsImages(IEnumerable<ItemRequestDto> stubs);
        IEnumerable<SingleImageRequestDto> GetAvailableDocumentImages(IEnumerable<ItemRequestDto> docs);
    }
}