﻿using System.Collections.Generic;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IDocumentDataProvider
    {
        IEnumerable<DocumentDto> GetTransactionDocuments(cTransaction transaction);
    }
}
