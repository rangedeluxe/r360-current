﻿using System;
using System.Collections.Generic;
using WFS.LTA.Common;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class OLFImageProvider : IImageProvider
    {
        private readonly ltaLog _ltaLog;
        private readonly OLFOnlineClient _olfClient;
        private readonly Guid _sessionId;
        private readonly string _siteKey;

        public OLFImageProvider(Guid sessionId, string siteKey, ltaLog ltaLog)
        {
            _sessionId = sessionId;
            _siteKey = siteKey;
            _ltaLog = ltaLog;
            _olfClient = new OLFOnlineClient(siteKey);
        }

        public IEnumerable<SingleImageRequestDto> GetAvailablePaymentsImages(IEnumerable<ItemRequestDto> payments)
        {
            try
            {
                return _olfClient.GetAvailablePaymentsImages(payments, _siteKey, _sessionId);
            }
            catch (Exception ex)
            {
                _ltaLog.logEvent("Error Occurred while requesting payments images from OLF", GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                _ltaLog.logEvent(ex.Message, GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                throw;
            }
        }

        public IEnumerable<SingleImageRequestDto> GetAvailableStubsImages(IEnumerable<ItemRequestDto> stubs)
        {
            try
            {
                return _olfClient.GetAvailableStubsImages(stubs, _siteKey, _sessionId);
            }
            catch (Exception ex)
            {
                _ltaLog.logEvent("Error Occurred while requesting stubs images from OLF", GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                _ltaLog.logEvent(ex.Message, GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                throw;
            }
        }

        public IEnumerable<SingleImageRequestDto> GetAvailableDocumentImages(IEnumerable<ItemRequestDto> docs)
        {
            try
            {
                return _olfClient.GetAvailableDocumentImages(docs, _siteKey, _sessionId);
            }
            catch (Exception ex)
            {
                _ltaLog.logEvent("Error Occurred while requesting document images from OLF", GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                _ltaLog.logEvent(ex.Message, GetType().Name, LTAMessageType.Error, LTAMessageImportance.Essential);
                throw;
            }
        }
    }
}
