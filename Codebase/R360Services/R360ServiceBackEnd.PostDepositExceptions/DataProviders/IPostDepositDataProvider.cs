﻿using System;
using System.Collections.Generic;
using WFS.RecHub.R360Services.Common.DTO;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IPostDepositDataProvider
    {
        bool LockTransaction(long batchid, DateTime depositdate, int transactionid);
        bool UnlockTransaction(long batchid, DateTime depositdate, int transactionid);
        TransactionLockDto GetTransactionLock(long batchid, DateTime depositdate, int transactionid);
        IEnumerable<DataEntryValueDto> GetPostDepositDataEntryFieldsForStub(int bankId, int workgroupId, long batchId,
            DateTime depositDate, int transactionId, int batchSequence);
        IEnumerable<DataEntrySetupDto> GetDataEntrySetupFields(int bankid, int workgroupid,
            long batchid, DateTime depositdate);
    }
}
