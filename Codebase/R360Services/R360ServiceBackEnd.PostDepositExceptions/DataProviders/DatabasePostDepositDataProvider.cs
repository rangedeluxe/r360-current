﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabasePostDepositDataProvider : IPostDepositDataProvider
    {
        private readonly IR360ServicesDAL _r360ServicesDal;
        private readonly IPostDepositExceptionsDal _postDepositExceptionsDal;
        private readonly IServiceContext _context;

        public DatabasePostDepositDataProvider(IServiceContext context)
        {
            _context = context;
            _r360ServicesDal = new R360ServicesDAL(_context.SiteKey);
            _postDepositExceptionsDal = new PostDepositExceptionsDal(_context.SiteKey);
        }

        public TransactionLockDto GetTransactionLock(long batchid, DateTime depositdate, int transactionid)
        {
            DataTable dt;
            if (!_postDepositExceptionsDal.GetTransactionLock(batchid, depositdate, transactionid, out dt))
                return null;
            return dt.ToObjectList<TransactionLockDto>().FirstOrDefault();
        }
        public IEnumerable<DataEntryValueDto> GetPostDepositDataEntryFieldsForStub(int bankId, int workgroupId,
            long batchId, DateTime depositDate, int transactionId, int batchSequence)
        {
            var request = new PostDepositDataEntryDetailsRequestSqlDto
            {
                SessionId = _context.GetSessionID(),
                BankId = bankId,
                WorkgroupId = workgroupId,
                BatchId = batchId,
                DepositDate = depositDate,
                TransactionId = transactionId,
                BatchSequence = batchSequence,
            };

            DataTable dt;
            if (!_postDepositExceptionsDal.GetPostDepositDataEntryDetails(request, out dt))
                return null;

            return dt.ToObjectList<DataEntryValueDto>();
        }
        public bool LockTransaction(long batchid, DateTime depositdate, int transactionid)
        {
            return _postDepositExceptionsDal.InsertTransactionLock(batchid, depositdate, transactionid, _context.GetLoginNameForAuditing(), _context.GetSID());
        }

        public bool UnlockTransaction(long batchid, DateTime depositdate, int transactionid)
        {
            return _postDepositExceptionsDal.DeleteTransactionLock(batchid, depositdate, transactionid, _context.GetLoginNameForAuditing(), _context.GetSID());
        }
        public IEnumerable<DataEntrySetupDto> GetDataEntrySetupFields(int bankid, int workgroupid,
            long batchid, DateTime depositdate)
        {
            var sessionId = _context.GetSessionID();
            return _postDepositExceptionsDal.GetDataEntrySetupFields(sessionId, bankid, workgroupid,
                batchid, depositdate);
        }
    }
}
