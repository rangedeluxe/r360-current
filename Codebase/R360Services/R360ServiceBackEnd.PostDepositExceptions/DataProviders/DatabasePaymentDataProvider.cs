﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabasePaymentDataProvider : IPaymentDataProvider
    {
        private readonly cItemProcDAL _dal;
        private readonly IServiceContext _context;
        public DatabasePaymentDataProvider(IServiceContext context)
        {
            _context = context;
            _dal = new cItemProcDAL(context.SiteKey);
        }
        public IEnumerable<PaymentDto> GetPaymentsForTransaction(int bankid, int workgroupid, long batchid, DateTime depositdate, int transactionid)
        {
            DataTable dt;
            if (!_dal.GetTransactionChecks(_context.GetSessionID(), bankid, workgroupid,
                    depositdate, batchid, transactionid, out dt))
            {
                return null;
            }
            return dt.ToObjectList<PaymentDto>();
        }
    }
}
