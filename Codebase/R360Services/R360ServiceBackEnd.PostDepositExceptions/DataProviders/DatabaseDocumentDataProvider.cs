﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public class DatabaseDocumentDataProvider : IDocumentDataProvider
    {
        private readonly IServiceContext _context;
        private readonly cItemProcDAL _dal;
        public DatabaseDocumentDataProvider(IServiceContext context)
        {
            _context = context;
            _dal = new cItemProcDAL(context.SiteKey);
        }
        public IEnumerable<DocumentDto> GetTransactionDocuments(cTransaction transaction)
        {
            DataTable dt;
            var res = transaction.TransactionID > 0
                ? _dal.GetTransactionDocuments(_context.GetSessionID(), transaction, false, out dt)
                : _dal.GetTransactionDocumentsBySequence(_context.GetSessionID(), transaction, false, out dt);
            return dt.ToObjectList<DocumentDto>();
        }
          
        
    }
}
