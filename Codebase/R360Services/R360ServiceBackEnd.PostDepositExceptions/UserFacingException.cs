﻿using System;

namespace WFS.RecHub.R360Services.PostDepositExceptions
{
    public class UserFacingException : Exception
    {
        public UserFacingException(string message) : base(message)
        {
        }
    }
}
