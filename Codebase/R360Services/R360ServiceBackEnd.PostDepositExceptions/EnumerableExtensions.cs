﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace WFS.RecHub.R360Services.PostDepositExceptions
{
    public static class EnumerableExtensions
    {
        [NotNull]
        public static Dictionary<TKey, TItem> ToDictionaryIgnoringDuplicates<TItem, TKey>(
            [NotNull] this IEnumerable<TItem> items, [NotNull] Func<TItem, TKey> getKey)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));
            if (getKey == null)
                throw new ArgumentNullException(nameof(getKey));

            var dictionary = new Dictionary<TKey, TItem>();
            foreach (var item in items)
            {
                var key = getKey(item);
                if (!dictionary.ContainsKey(key))
                    dictionary[key] = item;
            }

            return dictionary;
        }
        [NotNull, ItemNotNull]
        public static IReadOnlyList<T> ToNonNullListOfNonNullItems<T>(
            [CanBeNull, ItemCanBeNull] this IEnumerable<T> self)
            where T : class
        {
            if (self == null)
                return new T[] { };

            return self.Where(item => item != null).ToList();
        }
    }
}
