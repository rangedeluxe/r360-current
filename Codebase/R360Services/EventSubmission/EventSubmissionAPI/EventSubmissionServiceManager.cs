﻿using System;
using System.Data;
using System.Runtime.CompilerServices;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.EventSubmission.Common;
using WFS.RecHub.R360Shared;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Eric Smasal
 * * Date:   07/24/2013
 * *
 * * Purpose: Adapts the data returned from EventSubmissionDAL into the EventSubmissionDTO objects or object arrays
 * *            within Response objects to be transmitted via WCF.  This inherits and conforms to the 
 * *            IEventSubissionService interface.
 * *
 * * Modification History
 * WI 99880 EAS 07/24/2013 Initial Creation
* WI 107711 CEJ 10/17/2013
*   Create the ability log userActivity in the sessionLog from the R360 Services
* ******************************************************************************/

namespace WFS.RecHub.EventSubmission.EventSubmissionAPI
{
	public class EventSubmissionServiceManager: APIBase
	{
		#region Member Variables

		private EventSubmissionDAL _eventSubmissionDAL = null;

		#endregion

		#region Properties

		protected EventSubmissionDAL EventSubmissionDAL
		{
			get
			{
				if (_eventSubmissionDAL == null)
				{
					if (!string.IsNullOrEmpty(SiteKey))
					{
						_eventSubmissionDAL = new EventSubmissionDAL(SiteKey);
					}
				}
				return (_eventSubmissionDAL);
			}
		}

		#endregion

    public EventSubmissionServiceManager(R360ServiceContext serviceContext)
      : base(serviceContext)
    {
		}

		#region Session Validation

		protected override bool ValidateSID(out int userID)
		{
            bool bReturnValue = false;
            userID = -1;
            DataTable dtResults;

            if(EventSubmissionDAL.GetUserIDBySID(_ServiceContext.GetSID(), out dtResults)) {
                userID = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
                bReturnValue = true;
            }

            return bReturnValue;
        }

		private T ValidateUserIdAndDo<T>(Action<T, int> operation, [CallerMemberName] string procName = null)
			where T : BaseResponse, new()
		{
			// Initialize result
			T result = new T();
			result.Status = StatusCode.FAIL;

			try
			{
				// Validate / get the user ID
				int userID;
				if (ValidateSIDAndLog(procName, out userID))
				{
					// Perform the requested operation
					operation(result, userID);
				}
				else
				{
					result.Errors.Add("Invalid Session");
				}
			}
			catch (Exception ex)
			{
				// Log exception, and return a generic message since we don't know what it says
				result.Errors.Add("Unexpected error in " + procName);
				OnErrorEvent(ex, this.GetType().Name, procName);
			}

			return result;
		}

		#endregion

		public EventSubmissionResponse RaiseAlert(EventLogSubmission eventSubmission)
		{
            return ValidateUserIdAndDo<EventSubmissionResponse>((response, userId) =>
            {
                //Call DAL to Insert to the Alert Event Log
                if (EventSubmissionDAL.InsertEventLog(eventSubmission.EventName, eventSubmission.SiteCodeID, eventSubmission.SiteBankID, eventSubmission.SiteClientAccountID, eventSubmission.Message))
                {
                    //Nothing to tie into for permissions here, the permissions are applied by report

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
		}

        protected override ActivityCodes RequestType {
            get {
                return ActivityCodes.acR360WebServiceCall;
            }
        }
    }
}
