﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Eric Smasal
* Date:     07/23/2013
*
* Purpose:  Execute a stored procedure, that will end up writing a row 
*           to the event log IF an appropriate EventRule already exists. 
* Modification History
* WI 99881  EAS 07/23/2013
*       Inital Creation
******************************************************************************/

namespace WFS.RecHub.DAL
{
	public class EventSubmissionDAL : _DALBase
    {
        #region Member Variables

        #region Constants

        const string procEventLogIns = "RecHubAlert.usp_EventLog_Ins";
        const string procEventAuditIns = "RecHubCommon.usp_WFS_EventAudit_Ins";

        #endregion

        #endregion

        #region Constructors

        public EventSubmissionDAL(string vSiteKey)
			: base(vSiteKey)
		{
		}

		public EventSubmissionDAL(string vSiteKey, ConnectionType enmConnectionType)
			: base(vSiteKey, enmConnectionType)
		{
        }

        #endregion

        #region Methods

        #region Public

        public bool InsertEventLog(string eventName, int? siteCodeID, int? siteBankID, int? siteClientAccountID, string message)
        {
            bool bRetVal = false;
            
            SqlParameter[] parms;
            List<SqlParameter> arParms = new List<SqlParameter>();

            try
            {
                arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, eventName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID == null ? null : siteCodeID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID == null ? null : siteBankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteClientAccountID == null ? null : siteClientAccountID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmMessage", SqlDbType.VarChar, message == String.Empty ? null : message, ParameterDirection.Input));

                parms = arParms.ToArray();

                bRetVal = Database.executeProcedure(procEventLogIns, parms);

            }
            catch(Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "InsertEventLog(string eventName, int? siteCodeID, int? siteBankID, int? siteClienAccountID, string message)");
                bRetVal = false;
            }

            return bRetVal;
        }

        public bool WriteAuditEvent(string auditEvent, int userID, string description)
        {
            bool bRetVal = false;
            List<SqlParameter> arParms = new List<SqlParameter>();

            try
            {
                arParms.Add(BuildParameter("@parmApplicationName", SqlDbType.VarChar, "EventSubmission", ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, auditEvent, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmAuditMessage", SqlDbType.VarChar, description, ParameterDirection.Input));

                SqlParameter[] parms = arParms.ToArray();

                bRetVal = Database.executeProcedure(procEventAuditIns, parms);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + procEventAuditIns, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bRetVal = false;
            }
            return bRetVal;
        }
        public bool GetUserIDBySID(Guid SID, out DataTable results)
        {
            bool bReturnValue = false;

            results = null;
            SqlParameter[] parms;
            try
            {
                parms = new SqlParameter[] {
                    BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input)
                };
                bReturnValue = Database.executeProcedure("RecHubUser.usp_Users_UserID_Get_BySID",
                        parms, out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetUserIDBySID(Guid SID, out DataTable results)");
                bReturnValue = false;
            }

            return bReturnValue;
        }

        #endregion

        #endregion
    }
}
