﻿namespace EventSubmissionTestClient
{
    partial class EventSubmissionTestClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbActions = new System.Windows.Forms.GroupBox();
            this.btnRaiseAlert = new System.Windows.Forms.Button();
            this.btnPing = new System.Windows.Forms.Button();
            this.gbResults = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.gbFields = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEventName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtClientAcctID = new System.Windows.Forms.TextBox();
            this.txtBankID = new System.Windows.Forms.TextBox();
            this.txtSiteCodeID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.gbActions.SuspendLayout();
            this.gbResults.SuspendLayout();
            this.gbFields.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbActions
            // 
            this.gbActions.Controls.Add(this.btnRaiseAlert);
            this.gbActions.Controls.Add(this.btnPing);
            this.gbActions.Location = new System.Drawing.Point(7, 176);
            this.gbActions.Name = "gbActions";
            this.gbActions.Size = new System.Drawing.Size(261, 202);
            this.gbActions.TabIndex = 0;
            this.gbActions.TabStop = false;
            this.gbActions.Text = "Actions";
            // 
            // btnRaiseAlert
            // 
            this.btnRaiseAlert.Location = new System.Drawing.Point(20, 100);
            this.btnRaiseAlert.Name = "btnRaiseAlert";
            this.btnRaiseAlert.Size = new System.Drawing.Size(96, 42);
            this.btnRaiseAlert.TabIndex = 7;
            this.btnRaiseAlert.Text = "Raise Alert";
            this.btnRaiseAlert.UseVisualStyleBackColor = true;
            this.btnRaiseAlert.Click += new System.EventHandler(this.btnRaiseAlert_Click);
            // 
            // btnPing
            // 
            this.btnPing.Location = new System.Drawing.Point(20, 35);
            this.btnPing.Name = "btnPing";
            this.btnPing.Size = new System.Drawing.Size(96, 42);
            this.btnPing.TabIndex = 6;
            this.btnPing.Text = "Ping";
            this.btnPing.UseVisualStyleBackColor = true;
            this.btnPing.Click += new System.EventHandler(this.btnPing_Click);
            // 
            // gbResults
            // 
            this.gbResults.Controls.Add(this.richTextBox1);
            this.gbResults.Location = new System.Drawing.Point(293, 176);
            this.gbResults.Name = "gbResults";
            this.gbResults.Size = new System.Drawing.Size(479, 202);
            this.gbResults.TabIndex = 1;
            this.gbResults.TabStop = false;
            this.gbResults.Text = "Results";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(7, 21);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(466, 175);
            this.richTextBox1.TabIndex = 8;
            this.richTextBox1.Text = "";
            // 
            // gbFields
            // 
            this.gbFields.Controls.Add(this.txtMessage);
            this.gbFields.Controls.Add(this.label5);
            this.gbFields.Controls.Add(this.txtSiteCodeID);
            this.gbFields.Controls.Add(this.txtBankID);
            this.gbFields.Controls.Add(this.txtClientAcctID);
            this.gbFields.Controls.Add(this.label4);
            this.gbFields.Controls.Add(this.label3);
            this.gbFields.Controls.Add(this.label2);
            this.gbFields.Controls.Add(this.txtEventName);
            this.gbFields.Controls.Add(this.label1);
            this.gbFields.Location = new System.Drawing.Point(7, 13);
            this.gbFields.Name = "gbFields";
            this.gbFields.Size = new System.Drawing.Size(765, 157);
            this.gbFields.TabIndex = 2;
            this.gbFields.TabStop = false;
            this.gbFields.Text = "Event Submission Data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Client Account Site ID:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Site Bank ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Site Code ID:";
            // 
            // txtEventName
            // 
            this.txtEventName.Location = new System.Drawing.Point(169, 21);
            this.txtEventName.Name = "txtEventName";
            this.txtEventName.Size = new System.Drawing.Size(172, 22);
            this.txtEventName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Event Name:";
            // 
            // txtClientAcctID
            // 
            this.txtClientAcctID.Location = new System.Drawing.Point(169, 122);
            this.txtClientAcctID.Name = "txtClientAcctID";
            this.txtClientAcctID.Size = new System.Drawing.Size(172, 22);
            this.txtClientAcctID.TabIndex = 4;
            // 
            // txtBankID
            // 
            this.txtBankID.Location = new System.Drawing.Point(169, 92);
            this.txtBankID.Name = "txtBankID";
            this.txtBankID.Size = new System.Drawing.Size(172, 22);
            this.txtBankID.TabIndex = 3;
            // 
            // txtSiteCodeID
            // 
            this.txtSiteCodeID.Location = new System.Drawing.Point(169, 57);
            this.txtSiteCodeID.Name = "txtSiteCodeID";
            this.txtSiteCodeID.Size = new System.Drawing.Size(172, 22);
            this.txtSiteCodeID.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(424, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Message:";
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(508, 21);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(251, 123);
            this.txtMessage.TabIndex = 5;
            // 
            // EventSubmissionTestClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 390);
            this.Controls.Add(this.gbFields);
            this.Controls.Add(this.gbResults);
            this.Controls.Add(this.gbActions);
            this.Name = "EventSubmissionTestClient";
            this.Text = "Event Submission Test Client";
            this.gbActions.ResumeLayout(false);
            this.gbResults.ResumeLayout(false);
            this.gbFields.ResumeLayout(false);
            this.gbFields.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbActions;
        private System.Windows.Forms.Button btnRaiseAlert;
        private System.Windows.Forms.Button btnPing;
        private System.Windows.Forms.GroupBox gbResults;
        private System.Windows.Forms.GroupBox gbFields;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEventName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSiteCodeID;
        private System.Windows.Forms.TextBox txtBankID;
        private System.Windows.Forms.TextBox txtClientAcctID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMessage;
    }
}