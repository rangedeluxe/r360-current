﻿using System;
using System.Drawing;
using System.Security.Claims;
using System.Windows.Forms;
using WFS.RecHub.EventSubmission.Common;
using WFS.RecHub.EventSubmissionService.EventSubmissionServiceClient;
using WFS.RecHub.R360Shared;

namespace EventSubmissionTestClient
{
    public partial class EventSubmissionTestClient : Form
    {
        public readonly EventSubmissionServiceManager _EventSubmissionServiceManager = new EventSubmissionServiceManager();

        public EventSubmissionTestClient()
        {
            InitializeComponent();

            ClaimsPrincipal.Current.AddIdentity(new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.Sid, "{4a183c6f-13d6-4303-bf7a-f8b32deda643}") }));

        }

        private void btnPing_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            try
            {
                PingResponse response = _EventSubmissionServiceManager.Ping();

                if (response.Status == StatusCode.SUCCESS)
                {
                    richTextBox1.ForeColor = Color.Black;
                    richTextBox1.AppendText("Ping Response: " + response.responseString);
                }
                else
                {
                    richTextBox1.ForeColor = Color.Red;
                    richTextBox1.AppendText("Ping Error:  " + response);
                }
            }
            catch (Exception ex)
            {
                richTextBox1.ForeColor = Color.Red;
                richTextBox1.AppendText("Exception: " + ex.Message);
            }
        }

        private void btnRaiseAlert_Click(object sender, EventArgs e)
        {
            int ids;
            richTextBox1.Clear();

            EventLogSubmission submission = new EventLogSubmission();

            if (txtEventName.Text != String.Empty)
                submission.EventName = txtEventName.Text;
            else
            {
                richTextBox1.ForeColor = Color.Red;
                richTextBox1.AppendText("Error: Event Name is required.");
                return;
            }

            if (int.TryParse(txtSiteCodeID.Text, out ids))
                submission.SiteCodeID = ids;
            else
            {
                richTextBox1.ForeColor = Color.Red;
                richTextBox1.AppendText("Error: Site Code ID needs to be an Integer.");
                return;
            }

            if (txtBankID.Text != String.Empty)
            {
                if (int.TryParse(txtBankID.Text, out ids))
                    submission.SiteBankID = ids;
                else
                {
                    richTextBox1.ForeColor = Color.Red;
                    richTextBox1.AppendText("Error: Site Bank ID needs to be an Integer.");
                    return;
                }
            }

            if (txtClientAcctID.Text != String.Empty)
            {
                if (int.TryParse(txtClientAcctID.Text, out ids))
                    submission.SiteClientAccountID = ids;
                else
                {
                    richTextBox1.ForeColor = Color.Red;
                    richTextBox1.AppendText("Error: Site Client Account ID needs to be an Integer.");
                    return;
                }
            }

            if (txtMessage.Text != String.Empty)
                submission.Message = txtMessage.Text;

            try
            {
                EventSubmissionResponse response = _EventSubmissionServiceManager.RaiseAlert(submission);

                if (response.Status == StatusCode.SUCCESS)
                {
                    richTextBox1.ForeColor = Color.Black;
                    richTextBox1.AppendText("Submission Response: " + response.responseBoolean);
                }
                else
                {
                    richTextBox1.ForeColor = Color.Red;
                    richTextBox1.AppendText("Submission Error:  " + response);
                }
            }
            catch (Exception ex)
            {
                richTextBox1.ForeColor = Color.Red;
                richTextBox1.AppendText("Exception: " + ex.Message);
            }
        }
    }
}
