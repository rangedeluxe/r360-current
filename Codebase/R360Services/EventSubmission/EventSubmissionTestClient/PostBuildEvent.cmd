﻿REM TargetPath = %1 
REM TargetDir = %2
REM ProjectDir = %3
REM OutDir = %4

if %4=="bin\Debug\." goto CreateDebuggingConfig
goto End

:CreateDebuggingConfig
echo Info: Setting Debugging Configuration
copy %2\..\..\..\..\..\..\Ini\WCFClients.web.config %2\WCFClients.config
%3\..\..\ServiceShares\ConfigFileModifier\%4\ConfigFileModifier.exe %2\WCFClients.config /replace:localhost/EventSubmission;;;localhost.fiddler:44306/EventSubmission

:End
echo Info: Post Build Event Complete