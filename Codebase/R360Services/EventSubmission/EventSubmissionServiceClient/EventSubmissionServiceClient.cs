﻿using System;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using WFS.LTA.Common;
using WFS.RecHub.EventSubmission.Common;
using WFS.RecHub.R360Shared;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Eric Smasal 
 * * Date:   07/25/2013
 * *
 * * Purpose:  Service Client
 * *
 * * Modification History
 * WI 99880 EAS 07/25/2013  Initial Creation
 * WI 143458 RDS 05/22/2014	Move binding to config file
* ******************************************************************************/

namespace WFS.RecHub.EventSubmissionService.EventSubmissionServiceClient
{
    internal class EventSubmissionServiceClient : _ServicesClientBase
    {
        private readonly IEventSubmissionService _EventSubmissionService = null;

        public EventSubmissionServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
			_EventSubmissionService = R360ServiceFactory.Create<IEventSubmissionService>();
        }

		private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
		{
			// All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
			try
			{
                using (new OperationContextScope((IContextChannel)_EventSubmissionService))
				{
					// Set context inside of operation scope
					R360ServiceContext.Init(this.SiteKey);

					return operation();
				}
			}
			catch (FaultException<EventSubmission.Common.ServerFaultException> ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw (new Exception(ex.Message, ex));
			}
			catch (FaultException ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw (new Exception(ex.Message, ex));
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, methodName);
				throw;
			}
		}

        public PingResponse Ping()
        {
            return Do(_EventSubmissionService.Ping);
        }

        public EventSubmissionResponse RaiseAlert(EventLogSubmission eventSubmission)
        {
            return Do(() => _EventSubmissionService.RaiseAlert(eventSubmission));
        }
    }
}
