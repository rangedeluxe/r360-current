﻿using System.Runtime.Serialization;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Eric Smasal
* Date:   07/23/2013
*
* Purpose: Event Submission contract
*
* Modification History
* WI 99881 EAS 07/23/2013
*   - Initial Creation
*********************************************************************************/

namespace WFS.RecHub.EventSubmission.Common
{
    [DataContract]
    public class EventLogSubmission
    {
        [DataMember]
        public string EventName { get; set; }

        [DataMember]
        public int SiteCodeID { get; set; }

        [DataMember]
        public int? SiteBankID { get; set; }
        
        [DataMember]
        public int? SiteClientAccountID { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
