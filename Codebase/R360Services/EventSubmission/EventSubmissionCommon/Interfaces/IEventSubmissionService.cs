﻿using System.Runtime.Serialization;
using System.ServiceModel;
using WFS.RecHub.R360Shared;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Eric Smasal
* Date:   07/25/2013
*
* Purpose: Event Submission Service WCF Interface
*
* Modification History
* WI 99880 EAS  07/25/2013
*   - Initial Creation
*********************************************************************************/
namespace WFS.RecHub.EventSubmission.Common
{
    [ServiceContract(Namespace = "urn:wausaufs.com:services:EventSubmissionService")]
    public interface IEventSubmissionService
    {
        [OperationContract]
        PingResponse Ping();

        [OperationContract]
        EventSubmissionResponse RaiseAlert(EventLogSubmission eventSubmission);
    }

    //**************************************
    // Common Data Contracts
    //**************************************
    [DataContract]
    public class ServerFaultException
    {
        [DataMember]
        public string errorcode;

        [DataMember]
        public string message;

        [DataMember]
        public string details;
    }

}

