﻿using WFS.RecHub.EventSubmission.Common;
using WFS.RecHub.EventSubmission.EventSubmissionAPI;
using WFS.RecHub.R360Shared;
/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Eric Smasal
 * * Date:   07/25/2013
 * *
 * * Purpose: Adapts the data returned from EventSubmissionDAL into the EventSubmissionDTO objects or object arrays
 * *            within Response objects to be transmitted via WCF.  This inherits and conforms to the 
 * *            IEventSubmissionService interface.
 * *
 * * Modification History
 * WI 99880 EAS 07/25/2013 Initial Creation
* ******************************************************************************/
namespace WFS.RecHub.EventSubmission
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EventSubmissionService" in code, svc and config file together.
	// NOTE: In order to launch WCF Test Client for testing this service, please select EventSubmissionService.svc or EventSubmissionService.svc.cs at the Solution Explorer and start debugging.
	public class EventSubmissionService : LTAService, IEventSubmissionService
	{
        public PingResponse Ping()
		{
			return FailOnError((ignored) =>
			{
				EventLog.logEvent("Ping Received", "EventSubmissionSvc", RecHub.Common.MessageType.Information, RecHub.Common.MessageImportance.Debug);
				PingResponse response = new PingResponse();
				response.responseString = "Pong";
				return response;
			});
		}

		public EventSubmissionResponse RaiseAlert(EventLogSubmission eventSubmission)
		{
			return FailOnError((eventSubmissionAPI) =>
			{
				return ((EventSubmissionServiceManager)eventSubmissionAPI).RaiseAlert(eventSubmission);
			});
		}

        protected override APIBase NewAPIInst(R360Shared.R360ServiceContext serviceContext) {
          return new EventSubmissionServiceManager(serviceContext);
        }
    }
}
