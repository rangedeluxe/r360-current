﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using WFS.RecHub.Common.Crypto;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public static class ConnectionStrings
    {
        private static string Decrypt(string encryptedString, string title)
        {
            string result;
            var crypto = new cCrypto3DES();
            if (!crypto.Decrypt(encryptedString, out result))
                throw new InvalidOperationException($"Error decrypting {title}");
            return result;
        }
        public static string GetConnectionString()
        {
            var connectionStringTemplate = ConfigurationManager.AppSettings["ConnectionString"];

            var isConnectionStringInvalid =
                String.IsNullOrEmpty(connectionStringTemplate) ||
                connectionStringTemplate.StartsWith("@@");
            if (isConnectionStringInvalid)
                throw new InvalidOperationException("Config file does not contain a valid connection string");

            var connectionStringBuilder = new SqlConnectionStringBuilder(connectionStringTemplate);
            connectionStringBuilder.UserID = Decrypt(connectionStringBuilder.UserID, "Connection string user ID");
            connectionStringBuilder.Password = Decrypt(connectionStringBuilder.Password, "Connection string password");
            return connectionStringBuilder.ConnectionString;
        }
    }
}