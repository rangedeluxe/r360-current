﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected void SetProperty<T>(ref T backingField, T value, [CallerMemberName] string propertyName = null,
            IEnumerable<string> additionalProperties = null)
        {
            if (Equals(backingField, value))
                return;
            backingField = value;
            NotifyPropertyChanged(propertyName);
            if (additionalProperties != null)
            {
                foreach (var additionalPropertyName in additionalProperties)
                    NotifyPropertyChanged(additionalPropertyName);
            }
        }
    }
}