﻿using System;
using System.Windows.Input;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class DataEntryFieldViewModel : ViewModelBase
    {
        private string _name = "";
        private string _value = "";

        public DataEntryFieldViewModel()
        {
            DeleteCommand = new DelegateCommand(() => Deleted?.Invoke(this, EventArgs.Empty));
        }

        public ICommand DeleteCommand { get; }
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }
        public string Value
        {
            get { return _value; }
            set { SetProperty(ref _value, value); }
        }

        public event EventHandler Deleted;

        public DataEntryField GetDataEntryField()
        {
            return new DataEntryField
            {
                Name = Name,
                Value = Value,
            };
        }
    }
}