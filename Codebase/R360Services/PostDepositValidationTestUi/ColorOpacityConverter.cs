﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class ColorOpacityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Color))
                return null;

            var color = (Color) value;
            var opacity = System.Convert.ToDouble(parameter);
            return Color.FromArgb((byte) Math.Round(opacity * 255), color.R, color.G, color.B);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}