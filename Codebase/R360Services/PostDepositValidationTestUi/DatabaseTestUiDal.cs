using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class DatabaseTestUiDal : ITestUiDal
    {
        private readonly string _connectionString;

        public DatabaseTestUiDal(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IReadOnlyList<PaymentSource>> GetPaymentSourcesAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "RecHubData.usp_dimBatchSources_Get";
                    command.Parameters.Add(new SqlParameter("@parmActiveOnly", SqlDbType.Bit) {Value = true});
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        var results = new List<PaymentSource>();
                        while (await reader.ReadAsync())
                        {
                            results.Add(new PaymentSource(
                                paymentSourceKey: Convert.ToInt32(reader["BatchSourceKey"]),
                                longName: (string) reader["LongName"]));
                        }
                        return results;
                    }
                }
            }
        }
    }
}