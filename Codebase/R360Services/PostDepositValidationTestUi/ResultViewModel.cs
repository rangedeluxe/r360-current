﻿using System;
using System.Text;
using System.Windows.Input;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class ResultViewModel : ViewModelBase
    {
        private string _message;

        public ResultViewModel(ITransactionSource transactionSource)
        {
            TransactionSource = transactionSource;
            ValidateCommand = new DelegateCommand(Validate);
        }

        public string Message
        {
            get { return _message; }
            private set { SetProperty(ref _message, value); }
        }
        private ITransactionSource TransactionSource { get; }
        public ICommand ValidateCommand { get; }

        private void Validate()
        {
            try
            {
                var transaction = TransactionSource.GetTransaction();
                var validator = new UiValidator(transaction);
                var response = validator.Validate();

                var message = new StringBuilder();
                var totalExceptions = response.PaymentErrors.Count + response.StubErrors.Count;
                message.AppendLine($"{totalExceptions} exception(s)");
                message.AppendLine();

                foreach (var pair in response.PaymentErrors)
                {
                    var payment = pair.Key;
                    var errors = pair.Value;

                    var index = transaction.Payments.IndexOf(payment);
                    message.AppendLine($"Payment {index + 1}");
                    foreach (var error in errors)
                        message.AppendLine($"    {error.Field}: {error.Error}");
                }
                foreach (var pair in response.StubErrors)
                {
                    var stub = pair.Key;
                    var errors = pair.Value;

                    var index = transaction.Stubs.IndexOf(stub);
                    message.AppendLine($"Stub {index + 1}");
                    foreach (var error in errors)
                        message.AppendLine($"    {error.Field}: {error.Error}");
                }

                Message = message.ToString();
            }
            catch (Exception ex)
            {
                Message = "Error:\r\n" + ex.Message;
            }
        }
    }
}