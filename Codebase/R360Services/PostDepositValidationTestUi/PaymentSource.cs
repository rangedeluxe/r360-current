﻿namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class PaymentSource
    {
        public PaymentSource(int paymentSourceKey, string longName)
        {
            PaymentSourceKey = paymentSourceKey;
            LongName = longName;
        }

        public string LongName { get; }
        public int PaymentSourceKey { get; }
    }
}