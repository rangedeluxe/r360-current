﻿using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public interface ITransactionSource
    {
        Transaction GetTransaction();
    }
}