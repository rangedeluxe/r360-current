﻿using System;
using System.Collections.Generic;
using System.Windows;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class TransactionViewModel : ViewModelBase, ITransactionSource
    {
        private PaymentSource _paymentSource;
        private IReadOnlyList<PaymentSource> _paymentSources;
        private string _siteBankId;
        private string _siteWorkgroupId;

        public PaymentSource PaymentSource
        {
            get { return _paymentSource; }
            set { SetProperty(ref _paymentSource, value); }
        }
        public IReadOnlyList<PaymentSource> PaymentSources
        {
            get { return _paymentSources; }
            set
            {
                SetProperty(ref _paymentSources, value, additionalProperties: new[]
                {
                    nameof(PaymentSourcesLoadingVisibility)
                });
            }
        }
        public Visibility PaymentSourcesLoadingVisibility => PaymentSources == null
            ? Visibility.Visible
            : Visibility.Collapsed;
        public string SiteBankId
        {
            get { return _siteBankId; }
            set { SetProperty(ref _siteBankId, value); }
        }
        public string SiteWorkgroupId
        {
            get { return _siteWorkgroupId; }
            set { SetProperty(ref _siteWorkgroupId, value); }
        }

        public PaymentListViewModel Payments { get; } = new PaymentListViewModel();
        public StubListViewModel Stubs { get; } = new StubListViewModel();

        public Transaction GetTransaction()
        {
            return new Transaction
            {
                SiteBankId = ParseInt(SiteBankId, "Site Bank ID"),
                SiteWorkgroupId = ParseInt(SiteWorkgroupId, "Site Workgroup ID"),
                PaymentSourceKey = PaymentSource?.PaymentSourceKey ?? 0,
                Payments = Payments.GetPayments(),
                Stubs = Stubs.GetStubs(),
            };
        }
        public async void Initialize(ITestUiDal dal)
        {
            // Called during UI startup.
            try
            {
                PaymentSources = await dal.GetPaymentSourcesAsync();
            }
            catch (Exception ex)
            {
                // Not the best place for an error message, but at least it shows where the problem was
                PaymentSources = new[]
                {
                    new PaymentSource(0, ex.Message),
                };
            }
        }
        private int ParseInt(string value, string title)
        {
            int result;
            if (!int.TryParse(value, out result))
                throw new InvalidOperationException($"{title}: Expected a number but found '{value}'");
            return result;
        }
    }
}