﻿using System.Windows;
using System.Windows.Media;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public partial class ItemPanel
    {
        public static readonly DependencyProperty BaseColorProperty = DependencyProperty.Register(
            name: "BaseColor",
            propertyType: typeof(Color),
            ownerType: typeof(ItemPanel),
            typeMetadata: new FrameworkPropertyMetadata(Colors.Gray));
        public static readonly DependencyProperty FooterContentProperty = DependencyProperty.Register(
            name: "FooterContent",
            propertyType: typeof(object),
            ownerType: typeof(ItemPanel));

        public ItemPanel()
        {
            InitializeComponent();
        }

        public Color BaseColor
        {
            get { return (Color) GetValue(BaseColorProperty); }
            set { SetValue(BaseColorProperty, value); }
        }
        public object FooterContent
        {
            get { return GetValue(FooterContentProperty); }
            set { SetValue(FooterContentProperty, value); }
        }
    }
}
