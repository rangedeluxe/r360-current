﻿using System;
using System.Windows.Input;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class PaymentViewModel
    {
        public PaymentViewModel()
        {
            DeleteCommand = new DelegateCommand(() => Deleted?.Invoke(this, EventArgs.Empty));
        }

        public DataEntryFieldListViewModel DataEntryFields { get; } = new DataEntryFieldListViewModel();
        public ICommand DeleteCommand { get; }

        public event EventHandler Deleted;

        public Payment GetPayment()
        {
            return new Payment
            {
                DataEntryFields = DataEntryFields.GetDataEntryFields(),
            };
        }
    }
}