﻿using System;
using System.Windows;
using System.Windows.Input;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public partial class CloseButton
    {
        public static DependencyProperty CommandProperty = DependencyProperty.Register(
            name: "Command",
            propertyType: typeof(ICommand),
            ownerType: typeof(CloseButton));

        public CloseButton()
        {
            InitializeComponent();
        }

        public ICommand Command
        {
            get { return (ICommand) GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
    }
}
