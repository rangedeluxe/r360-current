﻿namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class MainViewModel
    {
        public MainViewModel()
        {
            Transaction = new TransactionViewModel();
            Result = new ResultViewModel(Transaction);
        }

        public ResultViewModel Result { get; }
        public TransactionViewModel Transaction { get; }

        public void Initialize(ITestUiDal dal)
        {
            Transaction.Initialize(dal);
        }
    }
}