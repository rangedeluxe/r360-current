﻿using WFS.RecHub.PostDepositBusinessRules;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class UiValidator
    {
        private readonly Transaction _transaction;

        public UiValidator(Transaction transaction)
        {
            _transaction = transaction;
        }

        public DetailedValidationResponse<Stub> Validate()
        {
            var ruleProvider = new DatabaseRuleProvider(ConnectionStrings.GetConnectionString());
            var businessRulesEngine = new BusinessRulesEngine(ruleProvider);
            return businessRulesEngine.DetailedValidation(_transaction);
        }
    }
}