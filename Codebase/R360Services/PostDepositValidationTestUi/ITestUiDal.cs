﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public interface ITestUiDal
    {
        Task<IReadOnlyList<PaymentSource>> GetPaymentSourcesAsync();
    }
}