﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class PaymentListViewModel : ViewModelBase
    {
        public PaymentListViewModel()
        {
            Payments = new ObservableCollection<PaymentViewModel>();
            Payments.CollectionChanged += (sender, e) => NotifyVisibilitiesChanged();
            AddCommand = new DelegateCommand(AddPayment);
        }

        public ICommand AddCommand { get; }
        public Visibility NoPaymentsVisibility => !Payments.Any() ? Visibility.Visible : Visibility.Collapsed;
        public ObservableCollection<PaymentViewModel> Payments { get; }

        private void AddPayment()
        {
            var paymentViewModel = new PaymentViewModel();
            paymentViewModel.Deleted += (sender, e) => Payments.Remove(paymentViewModel);
            Payments.Add(paymentViewModel);
        }
        public IList<Payment> GetPayments()
        {
            return Payments.Select(p => p.GetPayment()).ToList();
        }
        private void NotifyVisibilitiesChanged()
        {
            NotifyPropertyChanged(nameof(NoPaymentsVisibility));
        }
    }
}