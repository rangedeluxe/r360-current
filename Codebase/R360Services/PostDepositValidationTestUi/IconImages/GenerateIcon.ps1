﻿# Icon credits:
# 1489783590_lab.png:
#   From https://www.iconfinder.com/icons/202318/lab_test-tube_icon#size=512
#   Free for commercial use

$ErrorActionPreference = "Stop"

if (!(Get-Command conjure -ErrorAction SilentlyContinue)) {
	throw "ImageMagick must be installed and on the PATH."
}

$convertArgs = @(
    ".\1489783590_lab.png[256x256]"
    ".\1489783590_lab.png[64x64]"
    ".\1489783590_lab.png[48x48]"
    ".\1489783590_lab.png[32x32]"
    "..\Lab.ico"
)

convert @convertArgs
