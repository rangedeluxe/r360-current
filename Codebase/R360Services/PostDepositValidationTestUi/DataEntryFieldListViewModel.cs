﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class DataEntryFieldListViewModel : ViewModelBase
    {
        public DataEntryFieldListViewModel()
        {
            Fields = new ObservableCollection<DataEntryFieldViewModel>();
            Fields.CollectionChanged += (sender, e) => NotifyVisibilitiesChanged();
            AddCommand = new DelegateCommand(AddField);
        }

        public ICommand AddCommand { get; }
        public ObservableCollection<DataEntryFieldViewModel> Fields { get; }
        public Visibility FieldsVisibility => Fields.Any() ? Visibility.Visible : Visibility.Collapsed;
        public Visibility NoFieldsVisibility => !Fields.Any() ? Visibility.Visible : Visibility.Collapsed;

        private void AddField()
        {
            var fieldViewModel = new DataEntryFieldViewModel();
            fieldViewModel.Deleted += (sender, e) => Fields.Remove(fieldViewModel);
            Fields.Add(fieldViewModel);
        }
        public IList<DataEntryField> GetDataEntryFields()
        {
            return Fields.Select(field => field.GetDataEntryField()).ToList();
        }
        private void NotifyVisibilitiesChanged()
        {
            NotifyPropertyChanged(nameof(FieldsVisibility));
            NotifyPropertyChanged(nameof(NoFieldsVisibility));
        }
    }
}