﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositValidationTestUi
{
    public class StubListViewModel : ViewModelBase
    {
        public StubListViewModel()
        {
            Stubs = new ObservableCollection<StubViewModel>();
            Stubs.CollectionChanged += (sender, e) => NotifyVisibilitiesChanged();
            AddCommand = new DelegateCommand(AddStub);
        }

        public ICommand AddCommand { get; }
        public Visibility NoStubsVisibility => !Stubs.Any() ? Visibility.Visible : Visibility.Collapsed;
        public ObservableCollection<StubViewModel> Stubs { get; }

        private void AddStub()
        {
            var stubViewModel = new StubViewModel();
            stubViewModel.Deleted += (sender, e) => Stubs.Remove(stubViewModel);
            Stubs.Add(stubViewModel);
        }
        public IList<Stub> GetStubs()
        {
            return Stubs.Select(s => s.GetStub()).ToList();
        }
        private void NotifyVisibilitiesChanged()
        {
            NotifyPropertyChanged(nameof(NoStubsVisibility));
        }
    }
}