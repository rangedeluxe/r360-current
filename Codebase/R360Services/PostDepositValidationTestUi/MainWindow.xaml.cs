﻿namespace WFS.RecHub.PostDepositValidationTestUi
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += (sender, e) => Initialize();
        }

        private void Initialize()
        {
            var viewModel = new MainViewModel();
            viewModel.Initialize(new DatabaseTestUiDal(ConnectionStrings.GetConnectionString()));
            DataContext = viewModel;
        }
    }
}
