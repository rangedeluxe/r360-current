﻿using System;
using System.Data;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Jon Sucha
* Date:     1/14/2009
*
* Purpose:  Online Session Class Library.
*
* Modification History
* 11/07/03 CR 5443 JCS
*    -Added EmulationID and EmulationUser object to track emulation session and user.
* 11/24/2003 CR 5937 JCS
*    -Added IsLoginSuccessful property (Session.IsSuccess)
* 08/19/2004 CR 9010 JCS
*    -Added SessionCustomer object, modified to support Online Customer enhancements.
* 10/11/2004 CR 8454 EJG
*    -Added new properties, IsSessionEnded and IsSessionExpired.
*    -Set the properties in the RestoreSession() function.
*    -Changed IsSessionActive property to be active only if IsSessionExpired and IsSessionEnded = False.
* 09/12/2005 CR 13591 EJG
*    -Modified RestoreSession() to call ServerOptions.LoadOLPreferences().
* 11/06/2006 CR 18947 JMC
*    -Modified RestoreSession() function to connect to the ipoLogon service
*     to retrieve its' values.
* CR 28522 JMC 01/06/2010
*    -Changed name of options class to be specific to this application
* CR 32263 08/15/2011  WJS
*    -Added support for moved preferences
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90260 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96309 CRG 04/30/2013
*     Change the NameSpace, Framework, Using's and Flower Boxes for OLServicesAPI
* * WI 154797 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* * WI 156704 TWE 08/01/2014
*      Adjust the calls for user preferences
* WI 157331 TWE 08/06/2014
*    Add permissions node to base document - remove warnings
******************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class SessionInfo : BaseOLAPI, ISessionInfo
    {
        //-------------------------------------------------------------------------------
        // Private member variables declaration
        //-------------------------------------------------------------------------------
        private IUserDal _userDal;
        private cSessionDAL _SessionDAL = null;
        private Guid _SessionID;
        private bool _IsSessionActive;
        private cUserRAAM _SessionUser;
        private System.DateTime _LogonDateTime;
        private System.DateTime _LastPageServed;
        private int _PageCounter;
        private bool _IsLoginSuccessful;

        //User has ended their session or automatically timed out
        private bool _IsSessionEnded;

        //Session is expired
        private bool _IsSessionExpired;

        public SessionInfo(IServiceContext serviceContext)
            : base(serviceContext)
        {
            RestoreSession(serviceContext.GetSessionID());
        }

        private IUserDal UserDal
        {
            get
            {
                if (_userDal == null)
                {
                    if (!string.IsNullOrEmpty(SiteKey))
                    {
                        _userDal = new UserDal(SiteKey);
                    }
                }
                return _userDal;
            }
        }
        public bool IsSessionActive
        {

            // TODO: This property should reflect current status:
            // - User.IsActive, etc.
            // - Customer.IsInternet, etc.

            get
            {
                return (_IsSessionActive);
            }
        }

        public DateTime LogonDateTime
        {
            get
            {
                return (_LogonDateTime);
            }
        }

        public DateTime LastPageServed
        {
            get
            {
                return (_LastPageServed);
            }
        }

        public int PageCounter
        {
            get
            {
                return (_PageCounter);
            }
        }

        public bool IsLoginSuccessful
        {
            get
            {
                return (_IsLoginSuccessful);
            }
        }

        public bool IsSessionEnded
        {
            get
            {
                return (_IsSessionEnded);
            }
        }

        public bool IsSessionExpired
        {
            get
            {
                return (_IsSessionExpired);
            }
        }

        public cOLSiteOptions ServerOptions
        {
            get
            {
                if (_OLSiteOptions == null)
                {
                    try
                    {
                        _OLSiteOptions = new cOLSiteOptions(this.SiteKey);
                    }
                    catch (Exception)
                    {

                    }
                }

                return (_OLSiteOptions);
            }
        }
        private cSessionDAL SessionDAL
        {
            get
            {
                if (_SessionDAL == null)
                {
                    _SessionDAL = new cSessionDAL(this.SiteKey);
                }

                return (_SessionDAL);
            }
        }
        public cUserRAAM SessionUser
        {

            get
            {
                return (_SessionUser);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && !Disposed)
            {
                if (_userDal != null)
                {
                    _userDal.Dispose();
                }
                if (_SessionDAL != null)
                {
                    _SessionDAL.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        public bool RestoreSession(Guid vSessionID)
        {
            DataTable dt = null;
            bool bolRetVal = false;

            _SessionID = vSessionID;

            // Release SiteKey dependent objects
            _OLSiteOptions = null;
            _EventLog = null;

            if (vSessionID != Guid.Empty)
            {

                if (SessionDAL.GetSession(vSessionID, out dt))
                {

                    if ((dt != null)
                        && (dt.Rows != null)
                        && (dt.Rows.Count > 0))
                    {

                        if (((int)dt.Rows[0]["UserID"]) > 0)
                        {

                            _LogonDateTime = (DateTime)dt.Rows[0]["LogonDateTime"];
                            try
                            {
                                _LastPageServed = (DateTime)dt.Rows[0]["LastPageServed"];
                            }
                            catch
                            {
                                _LastPageServed = DateTime.Now;
                            }

                            _PageCounter = (int)dt.Rows[0]["PageCounter"];

                            // Restore session user
                            _SessionUser = GetUserByID((int)dt.Rows[0]["UserID"]);

                            //Verify if session ended
                            if ((bool)dt.Rows[0]["IsSessionEnded"])
                            {
                                _IsSessionEnded = true;
                            }
                            else
                            {
                                _IsSessionEnded = false;
                            }

                            if (!ServerOptions.LoadOLPreferences(this.SessionUser.UserID))
                            {
                                EventLog.logEvent("Unable to load OLPreferences. System defaults will be used instead.", this.GetType().Name, MessageType.Information);
                            }

                            // Verify session has not expired
                            if (ipoDateTimeLib.DateDiff(DateInterval.Day, _LastPageServed, DateTime.Now) <= ServerOptions.Preferences.SessionExpiration)
                                if ((bool)dt.Rows[0]["IsSuccess"])
                                {
                                    // _IsSessionActive = True
                                    _IsSessionExpired = false;
                                }
                                else
                                {
                                    // _IsSessionActive = False
                                    _IsSessionExpired = true;
                                }

                            //Set IsSessionActive
                            if (!_IsSessionEnded && !_IsSessionExpired)
                            {
                                //An active session can only be where Session has not ended and has not expired
                                _IsSessionActive = true;
                            }
                            else
                            {
                                _IsSessionActive = false;
                            }

                            // Check if logon was successful
                            if ((bool)dt.Rows[0]["IsSuccess"])
                            {
                                _IsLoginSuccessful = true;
                            }
                            else
                            {
                                _IsLoginSuccessful = false;
                            }

                            bolRetVal = true;
                        }
                    }
                    else
                    {
                        _IsSessionActive = false;
                        bolRetVal = false;
                        throw (new InvalidSessionException("Unable to restore session. Session does not exist."));
                    }

                    if (dt != null)
                    {
                        dt.Dispose();
                    }
                }
            }

            return bolRetVal;
        }

        /// <summary>
        /// Retrieve cUser object for input UserID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        private cUserRAAM GetUserByID(int UserID)
        {

            cUserRAAM objRetVal = null;
            DataTable dt = null;
            DataRow dr;
            cUserRAAM objUser;

            try
            {
                if (UserDal.GetUser(UserID, out dt))
                {
                    if (dt.Rows.Count > 0)
                    {
                        dr = dt.Rows[0];
                        objUser = new cUserRAAM();
                        if (objUser.LoadDataRow(ref dr))
                        {
                            objRetVal = objUser;
                        }

                        objUser = null;
                    }

                    dt.Dispose();
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetUserByID");
            }

            return (objRetVal);
        }

        protected override bool ValidateSID(out int userID)
        {
            throw new NotImplementedException();
        }

        protected override ActivityCodes RequestType
        {
            get { throw new NotImplementedException(); }
        }
    }
}
