﻿using System;
using System.Collections.Generic;
using System.Xml;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Shared;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information
 * * of WFS and contain WFS trade secrets.  These materials may not be used,
 * * copied, modified or disclosed except as expressly permitted in writing by
 * * WFS (see the WFS license agreement for details).  All copies, modifications
 * * and derivative works of these materials are property of WFS.
 * *
 * * Author: Brian Holmes
 * * Date: 06/17/2014
 * *
 * * Purpose:
 * *
 * * Modification History
 * * WI 146250 BDH 06/17/2014 Created
* WI 154797 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 156704 TWE 08/01/2014
*      Adjust the calls for user preferences
* WI 157331 TWE 08/06/2014
*    Add permissions node to base document - fix stored procedure call
********************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public abstract class BaseOLAPI : APIBase, IDisposable
    {
        protected cOLSiteOptions _OLSiteOptions = null;
        private static cSiteOptions _SiteOptions = null;
        private static string _siteKey;
        protected cEventLog _EventLog = null;
        private static ltaLog _LtaEventLog;
        private List<cMsg> _Messages = new List<cMsg>();

        private Guid _SessionID;
        private Guid _EmulationID;
        protected IServiceContext _serviceContext;
        protected ISessionInfo _SessionInfo;

        public BaseOLAPI(IServiceContext serviceContext)
            : base(serviceContext)
        {
            _serviceContext = serviceContext;
            _SessionID = _serviceContext.GetSessionID();
        }

        /// <summary>
        /// True if <see cref="Dispose"/> has been called.
        /// </summary>
        protected bool Disposed { get; private set; }

        // definition for SessionID SOAP Header
        public Guid SessionID
        {
            get
            {
                return _SessionID;
            }
            set
            {
                _SessionID = value;
            }
        }

        // definition for EmulationID SOAP Header
        public Guid EmulationID
        {
            get
            {
                return _EmulationID;
            }
            set
            {
                _EmulationID = value;
            }
        }

        public ISessionInfo Session
        {
            get
            {
                if (_SessionInfo == null)
                {
                    _SessionInfo = CreateSession();
                }
                return _SessionInfo;
            }
            set
            {
                _SessionInfo = value;
                _SessionID = value.SessionID;
                _EmulationID = value.EmulationID;
            }
        }

        public cOLSiteOptions OLSiteOptions
        {
            get
            {
                if (_OLSiteOptions == null)
                {
                    _OLSiteOptions = new cOLSiteOptions(this.SiteKey);
                }
                return (_OLSiteOptions);
            }
        }

        protected static cSiteOptions SiteOptions
        {
            get
            {
                if (_SiteOptions == null)
                {
                    _SiteOptions = new cSiteOptions(_siteKey);
                }
                return _SiteOptions;
            }
        }

        protected List<cMsg> Messages
        {
            get
            {
                return (_Messages);
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog EventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(OLSiteOptions.logFilePath,
                                              OLSiteOptions.logFileMaxSize,
                                              (MessageImportance)OLSiteOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        protected static ltaLog LtaEventLog
        {
            get
            {
                if (_LtaEventLog == null)
                {
                    _LtaEventLog = new ltaLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (LTAMessageImportance)SiteOptions.loggingDepth);
                }

                return _LtaEventLog;
            }
        }

        protected enum MsgType
        {
            Error,
            Warning,
            Information
        }

        protected class cMsg
        {

            public MsgType type;
            public string text;

            public cMsg(MsgType Type, string Text)
            {
                this.type = Type;
                this.text = Text;
            }
        }

        //protected void AddMessage(MsgType msgType, string msgText)
        //{
        //    _Messages.Add(new cMsg(msgType, msgText));
        //}

        //protected void AppendMessages(XmlDocument xmlDoc)
        //{
        //    XmlNode nodeMessages;
        //    XmlNode nodeMessage;

        //    nodeMessages = ipoXmlLib.addElement(xmlDoc.DocumentElement, "Messages", "");
        //    foreach (cMsg msg in this.Messages)
        //    {
        //        nodeMessage = ipoXmlLib.addElement(nodeMessages, "Message", "");
        //        ipoXmlLib.addAttribute(nodeMessage, "Type", msg.type.ToString());
        //        ipoXmlLib.addAttribute(nodeMessage, "Text", msg.text);
        //    }
        //}
        protected virtual ISessionInfo CreateSession()
        {
            return new SessionInfo(_serviceContext);
        }
        protected string GetRequestIDFromXml(XmlDocument xmlDoc)
        {
            XmlNode attReqID = xmlDoc.DocumentElement.Attributes.GetNamedItem("ReqID");
            if (attReqID != null)
            {
                return (attReqID.Value);
            }
            else
            {
                return (string.Empty);
            }
        }

        protected void SaveXmlLog(XmlDocument xmlDoc,
                                  string requestID,
                                  string requestTitle)
        {
            try
            {
                if (OLSiteOptions.OnlineAPIXmlSavePath.Length > 0)
                {
                    xmlDoc.Save(System.IO.Path.Combine(OLSiteOptions.OnlineAPIXmlSavePath
                                                     , DateTime.Now.ToString("yyyyMMdd-HHmmss_") + requestID + "_" + requestTitle + ".xml"));
                }
            }
            catch { }
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!Disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (_SessionInfo != null)
                    {
                        _SessionInfo.Dispose();
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                Disposed = true;
            }
        }
    }
}
