﻿using System;
using System.Collections.Generic;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public interface ILockboxAPI : IDisposable
    {
        ISessionInfo Session { set; }

        BaseGenericResponse<cLockbox> GetLockboxByID(Guid OLLockboxID);
        BaseGenericResponse<Dictionary<int, string>> GetPaymentTypes();
        BaseGenericResponse<cOLLockboxes> GetUserLockboxes();
    }
}
