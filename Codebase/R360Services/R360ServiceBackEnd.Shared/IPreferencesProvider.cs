﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.R360Services.R360ServicesAPI.DataProviders
{
    public interface IPreferencesProvider
    {
        IEnumerable<cOLPreference> GetPreferences();
        cOLPreference GetPreferenceByName(string name);
    }
}
