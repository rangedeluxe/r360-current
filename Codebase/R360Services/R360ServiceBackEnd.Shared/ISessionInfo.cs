﻿using System;
using WFS.RecHub.Common;

namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public interface ISessionInfo : IDisposable
    {
        Guid EmulationID { get; }
        cOLSiteOptions OLSiteOptions { get; }
        cOLSiteOptions ServerOptions { get; }
        Guid SessionID { get; }
        cUserRAAM SessionUser { get; }

        Guid GetSession();
    }
}
