﻿using System;
using System.Collections;
using System.Collections.Specialized;
using WFS.RecHub.BusinessCommon;
using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 22114 JMC 11/12/2007
*    -Ported application to .Net from VB.
* CR 28522 JMC 01/06/2010
*    -Settings class now inherits from cSiteOptions.
* CR 32263 WJS 08/15/2011
*    -Added support for moved preferences
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 70371 TWE 02/07/2013 
*     Add TimeZoneBias for notification display
* WI 90260 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 90136 WJS 3/4/2013 
*     FP:Add TimeZoneBias for notification display
* WI 96309 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for OLServicesAPI
* WI 156704 TWE 08/01/2014
*     Adjust the calls for user preferences
******************************************************************************/
namespace WFS.RecHub.R360Services.R360ServicesAPI
{
    public class cOLSiteOptions : cSiteOptions
    {
        cOLPreferences _preferences = null;
        private string _CENDSStagingPath;
        private bool _StageCENDSFiles;
        private string _ExcludedPWDCharacters;
        private int _DecisionDeadlineWarning;
        private int _TimeZoneBias;
        private string _TimeZoneLabel;
        private bool _AdjustDaylightSavings;

        public cOLSiteOptions(string vSiteKey)
            : base(vSiteKey)
        {
            StringCollection colSiteOptions = ipoINILib.GetINISection(vSiteKey);
            string strKey;
            string strValue;

            const string INIKEY_STAGE_CENDS_FILES = "StageCENDSFiles";
            const string INIKEY_CENDS_STAGING_PATH = "CENDSStagingPath";
            const string INIKEY_DECISION_DEADLINE_WARNING = "DecisionDeadlineWarning";
            const string INIKEY_EXCLUDED_PWD_CHARS = "ExcludedPWDCharacters";
            const string INIKEY_TIME_ZONE_BIAS = "TimeZoneBias";   //in minutes
            const string INIKEY_TIME_ZONE_LABEL = "TimeZoneLabel";
            const string INIKEY_ADJUST_DALIGHT_SAVINGS = "AdjustDaylightSavings";

            bool bolTemp;
            int intTemp;

            System.Collections.IEnumerator myEnumerator;

            _StageCENDSFiles = false;
            _CENDSStagingPath = string.Empty;
            _DecisionDeadlineWarning = 15;
            _TimeZoneBias = -300;
            _TimeZoneLabel = "Eastern";
            _AdjustDaylightSavings = true;

            try
            {

                //Load section options.
                myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
                while (myEnumerator.MoveNext())
                {

                    strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                    strValue = myEnumerator.Current.ToString().Substring(strKey.Length + 1);

                    if (strKey.ToLower() == INIKEY_STAGE_CENDS_FILES.ToLower())
                    {
                        if (bool.TryParse(strValue, out bolTemp))
                        {
                            _StageCENDSFiles = bolTemp;
                        }
                        else
                        {
                            _StageCENDSFiles = false;
                        }
                    }
                    else if (strKey.ToLower() == INIKEY_CENDS_STAGING_PATH.ToLower())
                    {
                        _CENDSStagingPath = strValue;
                    }                   
                    else if (strKey.ToLower() == INIKEY_DECISION_DEADLINE_WARNING.ToLower())
                    {
                        if (int.TryParse(strValue, out intTemp))
                        {
                            _DecisionDeadlineWarning = intTemp;
                        }
                        else
                        {
                            _DecisionDeadlineWarning = 0;
                        }
                    }
                    //CR 14921 EJG 12/05/2005 - Retrieve setting from ini for excluded characters.
                    //                          NOTE: The INI key is misspelled.
                    else if (strKey.ToLower() == INIKEY_EXCLUDED_PWD_CHARS.ToLower())
                    {
                        _ExcludedPWDCharacters = strValue;
                    }
                    else if (strKey.ToLower() == INIKEY_TIME_ZONE_BIAS.ToLower())
                    {
                        if (int.TryParse(strValue, out intTemp))
                        {
                            _TimeZoneBias = intTemp;
                        }
                        else
                        {
                            _TimeZoneBias = -300;
                        }
                    }
                    else if (strKey.ToLower() == INIKEY_TIME_ZONE_LABEL.ToLower())
                    {
                        _TimeZoneLabel = strValue;
                    }
                    else if (strKey.ToLower() == INIKEY_ADJUST_DALIGHT_SAVINGS.ToLower())
                    {
                        if (bool.TryParse(strValue, out bolTemp))
                        {
                            _AdjustDaylightSavings = bolTemp;
                        }
                        else
                        {
                            _AdjustDaylightSavings = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new Exception("Unable to load site definition for site key " + vSiteKey, ex));
            }
        }

        public bool LoadOLPreferences()
        {
            OLPreferences olPref = new OLPreferences(siteKey);
            bool rtnval = olPref.LoadOLPreferences();
            _preferences = olPref.GetPrefs();
            return rtnval;
        }

        public bool LoadOLPreferences(int userID)
        {
            OLPreferences olPref = new OLPreferences(siteKey);
            bool rtnval = olPref.LoadOLPreferences(userID);
            _preferences = olPref.GetPrefs();
            return rtnval;
        }

        public bool StageCENDSFiles
        {
            get
            {
                return (_StageCENDSFiles);
            }
        }

        public string CENDSStagingPath
        {
            get
            {
                return (_CENDSStagingPath);
            }
        }

        public string ExcludedPWDCharacters
        {
            get
            {
                if (_ExcludedPWDCharacters == null)
                {
                    return (string.Empty);
                }
                else
                {
                    return (_ExcludedPWDCharacters);
                }
            }
        }
        public int DecisionDeadlineWarning
        {
            get
            {
                return (_DecisionDeadlineWarning);
            }
        }

        public cOLPreferences Preferences
        {
            get
            {
                return _preferences;
            }
        }

        public int TimeZoneBias
        {
            get
            {
                return _TimeZoneBias;
            }
        }

        public string TimeZoneLabel
        {
            get
            {
                return _TimeZoneLabel;
            }
        }

        public bool AdjustDaylightSavings
        {
            get
            {
                return _AdjustDaylightSavings;
            }
        }
    }
}
