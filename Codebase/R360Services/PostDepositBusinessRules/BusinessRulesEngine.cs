﻿using System;
using System.Collections.Generic;
using System.Linq;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace WFS.RecHub.PostDepositBusinessRules
{
    public class BusinessRulesEngine
    {
        public BusinessRulesEngine(IRuleProvider ruleProvider)
        {
            RuleProvider = ruleProvider;
        }

        private IRuleProvider RuleProvider { get; }

        private IReadOnlyDictionary<Payment, DataEntryError[]> GetDetailedPaymentValidationErrors(
            IEnumerable<Payment> payments, Rules rules)
        {
            var dataEntryFieldRules = rules.DataEntryFieldRules.Where(d => d.IsCheck && !(d.FieldName == "Payer" || d.FieldName == "RemitterName")).ToList();

            //Payer is a special case since it's not really a DE field
            var payer = rules.DataEntryFieldRules.SingleOrDefault(x => x.IsCheck && (x.FieldName == "Payer" || x.FieldName == "RemitterName"));

            var result = new Dictionary<Payment, DataEntryError[]>();

            foreach (var payment in payments)
            {
                var errorList = new List<DataEntryError>();
                if (payer != null && string.IsNullOrEmpty(payment.Payer) && payer.IsRequired)
                {
                    errorList.Add(new DataEntryError
                    {
                        Field = payer.FieldName,
                        Error = "Field is required-"
                    });

                }
                var matchDataEntryFields =
                (payment.DataEntryFields.Select(d1 => d1.Name)
                    .Intersect(dataEntryFieldRules.Select(d2 => d2.FieldName),
                        StringComparer.InvariantCultureIgnoreCase)).ToList();

                var noMatchDataEntryFields =
                    dataEntryFieldRules.Select(d1 => d1.FieldName)
                        .Except(payment.DataEntryFields.Select(d2 => d2.Name),
                            StringComparer.InvariantCultureIgnoreCase).ToList();

                foreach (var dataEntryField in matchDataEntryFields)
                {
                    if (string.IsNullOrEmpty(payment.DataEntryFields
                        .Where(d => String.Equals(d.Name, dataEntryField, StringComparison.InvariantCultureIgnoreCase))
                        .FirstOrDefault()
                        .Value?.Trim()))
                        errorList.Add(new DataEntryError
                        {
                            Field = dataEntryField,
                            Error = "Field is required"
                        });
                }

                foreach (var dataEntryField in noMatchDataEntryFields)
                {
                    errorList.Add(new DataEntryError
                    {
                        Field = dataEntryField,
                        Error = "Field is required"
                    });
                }
                if (errorList.Count > 0)
                {
                    result.Add(payment, errorList.ToArray());
                }
            }
            return result;
        }
        private IEnumerable<DataEntryError> GetNonStubValidationErrors(IEnumerable<Stub> stubs, Rules rules)
        {
            if (!stubs.Any())
            {
                var dataEntryFieldRules = rules.DataEntryFieldRules.Where(d => !d.IsCheck).ToList();
                var requiredSubFields = dataEntryFieldRules.Where(rule => rule.IsRequired);
                foreach (var stubField in requiredSubFields)
                {
                    yield return new DataEntryError
                    {
                        Field = stubField.FieldName,
                        Error = "Field is required",
                    };
                }
            }
        }
        private IReadOnlyDictionary<TStub, DataEntryError[]> GetDetailedStubValidationErrors<TStub>(
            IEnumerable<TStub> stubs, Rules rules)
            where TStub : Stub
        {
            var dataEntryFieldRules = rules.DataEntryFieldRules.Where(d => !d.IsCheck).ToList();
            var result = new Dictionary<TStub, DataEntryError[]>();

            if (stubs.Count() == 0)
            {
            }
            else
            {
                foreach (var stub in stubs)
                {
                    var errorList = new List<DataEntryError>();
                    var matchDataEntryFields =
                    (stub.DataEntryFields.Select(d1 => d1.Name)
                        .Intersect(dataEntryFieldRules.Select(d2 => d2.FieldName),
                            StringComparer.InvariantCultureIgnoreCase)).ToList();

                    var noMatchDataEntryFields =
                        dataEntryFieldRules.Select(d1 => d1.FieldName)
                            .Except(stub.DataEntryFields.Select(d2 => d2.Name),
                                StringComparer.InvariantCultureIgnoreCase).ToList();

                    foreach (var dataEntryField in matchDataEntryFields)
                    {
                        if (string.IsNullOrEmpty(stub.DataEntryFields
                            .Where(
                                d => String.Equals(d.Name, dataEntryField, StringComparison.InvariantCultureIgnoreCase))
                            .FirstOrDefault()
                            .Value?.Trim()))
                            errorList.Add(new DataEntryError
                            {
                                Field = dataEntryField,
                                Error = "Field is required"
                            });
                    }

                    foreach (var dataEntryField in noMatchDataEntryFields)
                    {
                        errorList.Add(new DataEntryError
                        {
                            Field = dataEntryField,
                            Error = "Field is required"
                        });
                    }
                    if (errorList.Count > 0)
                    {
                        result.Add(stub, errorList.ToArray());
                    }
                }
            }
            return result;
        }
        public ValidationResponse Validate(Transaction transaction)
        {
            var detailedValidation = DetailedValidation(transaction);
            return new ValidationResponse(detailedValidation.GetErrorStrings());
        }
        public DetailedValidationResponse<TStub> DetailedValidation<TStub>(Transaction<TStub> transaction)
            where TStub : Stub
        {
            var rules = RuleProvider.GetRules(transaction.SiteBankId, transaction.SiteWorkgroupId,
                transaction.PaymentSourceKey);

            return new DetailedValidationResponse<TStub>(
                stubErrors: GetDetailedStubValidationErrors(transaction.Stubs, rules),
                nonStubErrors: GetNonStubValidationErrors(transaction.Stubs, rules).ToList(),
                paymentErrors: GetDetailedPaymentValidationErrors(transaction.Payments, rules));
        }

        public DetailedValidationResponse<TStub> PaymentDetailedValidation<TStub>(Transaction<TStub> transaction)
            where TStub : Stub
        {
            var rules = RuleProvider.GetRules(transaction.SiteBankId, transaction.SiteWorkgroupId,
                        transaction.PaymentSourceKey);
            return new DetailedValidationResponse<TStub>(
               stubErrors: null,
               nonStubErrors: null,
               paymentErrors: GetDetailedPaymentValidationErrors(transaction.Payments, rules));
        }


    }
}