﻿namespace WFS.RecHub.PostDepositBusinessRules.Dtos
{
    public class DataEntryFieldRule
    {
        public bool IsCheck { get; set; }
        public string FieldName { get; set; }
        public bool IsRequired { get; set; }
    }
}
