﻿using System.Collections.Generic;
using System.Linq;

namespace WFS.RecHub.PostDepositBusinessRules.Dtos
{
    public class DetailedValidationResponse<TStub>
        where TStub : Stub
    {
        public DetailedValidationResponse(IReadOnlyDictionary<Payment, DataEntryError[]> paymentErrors,
            IReadOnlyDictionary<TStub, DataEntryError[]> stubErrors, IReadOnlyList<DataEntryError> nonStubErrors)
        {
            PaymentErrors = paymentErrors;
            StubErrors = stubErrors;
            NonStubErrors = nonStubErrors;
        }

        /// <summary>
        /// Exceptions associated with a payment in the transaction.
        /// </summary>
        public IReadOnlyDictionary<Payment, DataEntryError[]> PaymentErrors { get; }
        /// <summary>
        /// Exceptions associated with a stub in the transaction.
        /// </summary>
        public IReadOnlyDictionary<TStub, DataEntryError[]> StubErrors { get; }
        /// <summary>
        /// Exceptions associated with the *absence* of a stub in the transaction.
        /// E.g., the user wants Invoice Number to be required in every transaction, but there were no stubs.
        /// </summary>
        public IReadOnlyList<DataEntryError> NonStubErrors { get; }

        public IReadOnlyList<string> GetErrorStrings()
        {
            var paymentErrorStrings =
                from paymentErrors in PaymentErrors
                let errors = paymentErrors.Value
                from error in errors
                select error.Error;
            var stubErrorStrings =
                from stubErrors in StubErrors
                let errors = stubErrors.Value
                from error in errors
                select error.Error;
            var nonStubErrorStrings =
                from error in NonStubErrors
                select error.Error;

            return paymentErrorStrings
                .Concat(stubErrorStrings)
                .Concat(nonStubErrorStrings)
                .ToList();
        }
    }
}
