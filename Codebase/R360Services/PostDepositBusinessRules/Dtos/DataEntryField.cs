﻿namespace WFS.RecHub.PostDepositBusinessRules.Dtos
{
    public class DataEntryField
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}