﻿using System.Collections.Generic;

namespace WFS.RecHub.PostDepositBusinessRules.Dtos
{
    public class Payment
    {
        public int CheckSequence { get; set; }
        public string Payer { get; set; }
        public IList<DataEntryField> DataEntryFields { get; set; } = new List<DataEntryField>();
    }
}