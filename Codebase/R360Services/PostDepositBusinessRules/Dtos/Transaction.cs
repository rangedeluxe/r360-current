﻿using System.Collections.Generic;

namespace WFS.RecHub.PostDepositBusinessRules.Dtos
{
    public class Transaction : Transaction<Stub>
    {
    }

    public class Transaction<TStub>
        where TStub : Stub
    {
        public int SiteBankId { get; set; }
        public int SiteWorkgroupId { get; set; }
        public int PaymentSourceKey { get; set; }
        public IList<Payment> Payments { get; set; } = new List<Payment>();
        public IList<TStub> Stubs { get; set; } = new List<TStub>();
    }
}