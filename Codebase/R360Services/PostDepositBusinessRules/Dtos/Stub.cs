﻿using System.Collections.Generic;

namespace WFS.RecHub.PostDepositBusinessRules.Dtos
{
    public class Stub
    {
        public IList<DataEntryField> DataEntryFields { get; set; } = new List<DataEntryField>();
    }
}