﻿using System.Collections.Generic;
using System.Linq;

namespace WFS.RecHub.PostDepositBusinessRules.Dtos
{
    public class ValidationResponse
    {
        public ValidationResponse(IReadOnlyList<string> errors)
        {
            Errors = errors;
        }

        public IReadOnlyList<string> Errors { get; }
        public bool Success => !Errors.Any();
    }
}