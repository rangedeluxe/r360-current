﻿namespace WFS.RecHub.PostDepositBusinessRules.Dtos
{
    public class DataEntryError
    {
        public string Field { get; set; }
        public string Error { get; set; }
    }
}
