﻿using System.Collections.Generic;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositBusinessRules.RuleProvider
{
    public class Rules
    {
        public IList<DataEntryFieldRule> DataEntryFieldRules { get; set; } = new List<DataEntryFieldRule>();
        //TODO: Add stuff here
    }
}