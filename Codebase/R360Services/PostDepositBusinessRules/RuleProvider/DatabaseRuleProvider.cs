﻿using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositBusinessRules.RuleProvider
{
    public class DatabaseRuleProvider : IRuleProvider
    {
        private readonly string _connectionString;

        private SqlParameter BuildParameter(string parameterName, SqlDbType dbType, object value,
            ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter();

            param.ParameterName = parameterName;
            param.SqlDbType = dbType;
            param.Value = value;
            param.Direction = direction;
            return param;
        }
        public DatabaseRuleProvider(string connectionString)
        {
            _connectionString = connectionString;
        }

        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey)
        {
            var rules = new Rules();

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "RecHubData.usp_dimWorkgroupDataEntryColumns_Get_BusinessRules";
                command.Parameters.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId, ParameterDirection.Input));
                command.Parameters.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteWorkgroupId, ParameterDirection.Input));
                command.Parameters.Add(BuildParameter("@parmBatchSourceKey", SqlDbType.SmallInt, batchSourceKey, ParameterDirection.Input));

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        rules.DataEntryFieldRules.Add(new DataEntryFieldRule
                        {
                            IsCheck = (bool) reader["IsCheck"],
                            FieldName = reader["FieldName"].ToString(),
                            IsRequired = (bool) reader["IsRequired"]
                        });
                    }
                }
                connection.Close();

            };
            return rules;
        }
    }
}