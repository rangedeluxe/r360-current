﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace WFS.RecHub.PostDepositBusinessRules.RuleProvider
{
    public class WorkgroupBusinessRulesDatabaseProvider : IRuleProvider
    {
        private readonly string _connectionString;

        private SqlParameter BuildParameter(string parameterName, SqlDbType dbType, object value,
            ParameterDirection direction)
        {
            SqlParameter param = new SqlParameter();

            param.ParameterName = parameterName;
            param.SqlDbType = dbType;
            param.Value = value;
            param.Direction = direction;
            return param;
        }

        public WorkgroupBusinessRulesDatabaseProvider(string connectionString)
        {
            _connectionString = connectionString;
        }

        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey=0)
        {
            var rules = new Rules();
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "RecHubData.usp_dimWorkgroupBusinessRules_Get";
                command.Parameters.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankId, ParameterDirection.Input));
                command.Parameters.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteWorkgroupId, ParameterDirection.Input));

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if ((bool) reader["PostDepositPayerRequired"])
                        {
                            rules.DataEntryFieldRules.Add(new DataEntryFieldRule
                            {
                                IsCheck = true,
                                FieldName = reader["FieldName"].ToString(),
                                IsRequired = true
                            });
                        }
                    }
                }

                connection.Close();
            };
            return rules;
        }
    }
}
