﻿namespace WFS.RecHub.PostDepositBusinessRules.RuleProvider
{
    public interface IRuleProvider
    {
        //TODO: Rename batchSourceKey to paymentSourceKey (after refactoring the implementations to simplify tests)
        Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey);
    }
}