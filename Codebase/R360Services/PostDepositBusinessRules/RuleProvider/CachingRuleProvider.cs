﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFS.RecHub.PostDepositBusinessRules.RuleProvider
{
    public class CachingRuleProvider : IRuleProvider
    {
        private readonly IRuleProvider _ruleProvider;
        private Rules _cachedRules;

        private int? _siteBankId = null;
        private int? _siteWorkgroupId = null;
        private int? _batchSourceKey = null;
        public CachingRuleProvider(IRuleProvider ruleProvider)
        {
            _ruleProvider = ruleProvider;
        }

        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey)
        {
            if (_siteBankId != siteBankId ||
                _siteWorkgroupId != siteWorkgroupId ||
                _batchSourceKey != batchSourceKey)
            {
                _cachedRules = _ruleProvider.GetRules(siteBankId, siteWorkgroupId, batchSourceKey);
                _siteBankId = siteBankId;
                _siteWorkgroupId = siteWorkgroupId;
                _batchSourceKey = batchSourceKey;
            }
            return _cachedRules;
        }
    }
}
