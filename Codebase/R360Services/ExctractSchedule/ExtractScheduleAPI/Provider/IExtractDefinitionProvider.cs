﻿using ExtractScheduleCommon.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
* WI 146351 DJW 623/2014
*  Added Logging/Auditing support.
* WI 158888 BLR 08/25/2014
*   - Created the GetXML method.
*******************************************************************************/

namespace ExtractScheduleAPI.Services
{
    public interface IExtractDefinitionProvider : IDisposable
    {
        /// <summary>
        /// Gets all definitions.
        /// </summary>
        /// <returns>Enumerable of Extract Definitions</returns>
        IEnumerable<ExtractDefinitionDTO> GetAllDefinitions();

        /// <summary>
        /// Gets all definitions which are changed after changedAfter.
        /// </summary>
        /// <param name="changedAfter"></param>
        /// <returns>Enumerable of Extract Definitions</returns>
        IEnumerable<ExtractDefinitionDTO> GetAllDefinitions(DateTime? changedAfter);

        /// <summary>
        /// Adds a new definition to the provider. Sets the definition.ExtractDefinitionID after
        /// a successful addition.
        /// </summary>
        /// <param name="definition"></param>
        /// <returns>True if a successful addition</returns>
        bool AddDefinition(ExtractDefinitionDTO definition);

        /// <summary>
        /// Removes a definition from the provider.
        /// </summary>
        /// <param name="definition"></param>
        /// <returns>True if a successful removal</returns>
        bool RemoveDefinition(ExtractDefinitionDTO definition);

        /// <summary>
        /// Updates the definition with the ExtractDefinitionID.
        /// </summary>
        /// <param name="definition"></param>
        /// <returns>True if a successful update</returns>
        bool UpdateDefinition(ExtractDefinitionDTO definition);

        bool UpdateExtractName(int definitionId, string extractName);

        /// <summary>
        /// Downloads the definition XML, audits done in the stored proc.
        /// </summary>
        /// <param name="definitionId"></param>
        /// <returns></returns>
        ExtractDefinitionDTO GetDefinitionXML(int definitionId);
    }
}
