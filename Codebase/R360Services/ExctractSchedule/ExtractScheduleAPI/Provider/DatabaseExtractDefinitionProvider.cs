﻿using ExtractScheduleCommon.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
* WI 140642 DJW 5/8/2014
*   - Added support for the original name, and minor code cleanup.
* WI 141430 DJW 5/13/2014
*   - Handle null checking for default values.
* WI 146351 DJW 623/2014
*   - Added Logging/Auditing support.
* RDS 06/30/2014
*   - Convert to use R360Shared
* WI 158888 BLR 08/25/2014
*   - Created the GetXML method.
* WI 159239 BLR 08/18/2014
*   - Removed Definition from GetAll call. 
*   - Added Definition Size.  
* WI 159962 BLR 08/20/2014
*   - Grabbing the UserID based on the RAAM SID. 
*******************************************************************************/

namespace ExtractScheduleAPI.Services
{
    /// <summary>
    /// ExtractDefinitionService for the Database.
    /// See IExtractScheduleService for documentation on individual methods.
    /// </summary>
    public class DatabaseExtractDefinitionProvider : APIBase, IExtractDefinitionProvider
    {
        private readonly cExtractDefinitionsDAL definitionsDal;
        private readonly R360ServicesDAL r360servicesDal;

        public DatabaseExtractDefinitionProvider(R360ServiceContext serviceContext) 
			: base(serviceContext)
        {
            definitionsDal = new cExtractDefinitionsDAL(serviceContext.SiteKey);
            r360servicesDal = new R360ServicesDAL(serviceContext.SiteKey);
        }

        public IEnumerable<ExtractDefinitionDTO> GetAllDefinitions()
        {
            DataTable table;
            definitionsDal.GetExtractDefinitions(null, out table);
            return DefinitionsToList(table);
        }

        public IEnumerable<ExtractDefinitionDTO> GetAllDefinitions(DateTime? changedAfter)
        {
            DataTable table;
            definitionsDal.GetExtractDefinitions(changedAfter, out table);
            return DefinitionsToList(table);
        }

        public bool AddDefinition(ExtractDefinitionDTO definition)
        {
            var now = DateTime.Now;
            int insertedId = 0;
            int userId = GetUserId();
            var success = definitionsDal.AddExtractDefinitionFile(definition.ExtractName, definition.ExtractDefinition.Length, definition.ExtractDefinition, (int)definition.ExtractDefinitionType, definition.ExtractFilename, userId, out insertedId);
            if (!success)
                return false;

            definition.ExtractDefinitionId = insertedId;

            return true;
        }

        public bool RemoveDefinition(ExtractDefinitionDTO definition)
        {
            int userId = GetUserId();
            return definitionsDal.DeleteExtractDefition((int)definition.ExtractDefinitionId, userId);
        }

        public bool UpdateDefinition(ExtractDefinitionDTO definition)
        {
            int userId = GetUserId();
			bool filesuc = true;
			if (definition.ExtractDefinitionType != ExtractDefinitionType.Billing)
				filesuc = definitionsDal.UpdateExtractDefinitionFile((int)definition.ExtractDefinitionId, definition.ExtractDefinition.Length, definition.ExtractDefinition, definition.ExtractFilename, (int)definition.ExtractDefinitionType, userId);
            var namesuc = definitionsDal.UpdateExtractName((int)definition.ExtractDefinitionId, definition.ExtractName, userId);
            return filesuc && namesuc;
        }
        public bool UpdateExtractName(int definitionId, string extractName)
        {
            int userId = GetUserId();
            return definitionsDal.UpdateExtractName(definitionId, extractName, userId);
        }

        private List<ExtractDefinitionDTO> DefinitionsToList(DataTable dt)
        {
            List<ExtractDefinitionDTO> items = null;

            if (dt == null)
                return items;

            items = new List<ExtractDefinitionDTO>();

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ExtractDefinitionDTO dto = new ExtractDefinitionDTO();
                    dto.ExtractDefinitionId = dr.Field<long>("ExtractDefinitionID");
                    dto.ExtractDefinitionSize = dr.Field<int>("ExtractDefinitionSizeKb");

                    //this can be null, handle default type
                    string s = Trim(dr, "ExtractDefinitionType");
                    int etype = 1;
                    if (s.Length > 0)
                    {
                        if (!int.TryParse(s, out etype))
                            etype = 1;
                   }
                    dto.ExtractDefinitionType = (ExtractDefinitionType)etype;

                    dto.ExtractFilename = Trim(dr, "ExtractFilename");
                    dto.ExtractName = Trim(dr, "ExtractName");
                    items.Add(dto);
                }
            }
            return items;

        }

        private string Trim(DataRow dr, string column)
        {
            string result = string.Empty;

            if (dr == null)
                return result;

            if (dr[column] == null )
                return result;

            if (dr[column] == DBNull.Value)
                return result;

            return dr[column].ToString().Trim();

        }
        private int GetUserId()
        {
			int userId;
			ValidateSID(out userId);
            return userId;
        }

        public void Dispose()
        {
            definitionsDal.Dispose();
        }

		protected override bool ValidateSID(out int userID)
		{
            DataTable dt;
            userID = 0;
            var sid = _ServiceContext.GetSID();
            
            // Call the database, on failure there will be logs.
            var datasuccess = r360servicesDal.GetUserIDBySID(sid, out dt);
            if (!datasuccess)
                return false;

            // Check for data
            if (!dt.HasData())
                return false;

            // Finally parse the UserID.
            return int.TryParse(dt.Rows[0]["UserID"].ToString(), out userID);
		}

		protected override ActivityCodes RequestType
		{
			get { throw new NotImplementedException(); }
		}


        public ExtractDefinitionDTO GetDefinitionXML(int definitionId)
        {
            string defname;
            string xmlString;
            var userid = GetUserId();
            definitionsDal.GetExtractDefinitionFile(definitionId, userid, out defname, out xmlString);
            return new ExtractDefinitionDTO()
            {
                ExtractDefinition = xmlString,
                ExtractFilename = defname
            };
        }
    }
}
