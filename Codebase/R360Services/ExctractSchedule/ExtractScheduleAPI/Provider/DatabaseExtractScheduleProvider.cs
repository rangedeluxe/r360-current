﻿using ExtractScheduleCommon.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using WFS.RecHub.R360Shared;
using WFS.RecHub.ApplicationBlocks.DataAccess.Extensions;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
* WI 138899 DJW 05/08/2014
*   Added support for Monthly Schedule Type.
* WI 141430 DJW 5/13/2014
*   - Handle null checking for default values.
* WI 146351 DJW 623/2014
*  Added Logging/Auditing support.
*           RDS 06/30/2014
*  Convert to use R360Shared
* WI 159962 BLR 08/20/2014
*   - Grabbing the UserID based on the RAAM SID.
*******************************************************************************/

namespace ExtractScheduleAPI.Services
{
    /// <summary>
    /// Database Extract Schedule Service, in charge of passing data between the DAL project.
    /// See IExtractScheduleService for documentation on individual methods.
    /// </summary>
    public class DatabaseExtractScheduleProvider : APIBase, IExtractScheduleProvider
    {
        private readonly cExtractSchedulesDal schedulesdal;
        private readonly R360ServicesDAL r360servicesDal;

        public DatabaseExtractScheduleProvider(R360ServiceContext serviceContext)
			: base(serviceContext)
        {
            schedulesdal = new cExtractSchedulesDal(serviceContext.SiteKey);
            r360servicesDal = new R360ServicesDAL(serviceContext.SiteKey);
        }

        public IEnumerable<ExtractScheduleDTO> GetAllSchedules()
        {
            DataTable table;
            int records;
            schedulesdal.GetExtractSchedule(null, null, null, null, out table, out records);
            return SchedulesToList(table);
        }

        public bool AddSchedule(ExtractScheduleDTO schedule)
        {
            var id = -1L;
            int userId = GetUserId();
            schedulesdal.AddExtractSchedule(out id, schedule.ExtractDefinitionId, schedule.Description, schedule.IsActive, schedule.DaysOfWeek, schedule.ScheduleTime, schedule.ExtractRunArguments, userId, schedule.DayInMonth, (int)schedule.ScheduleType);
            if (id == -1L)
                return false;
            schedule.ExtractScheduleId = id;
            return true;
        }

        public bool RemoveSchedule(ExtractScheduleDTO schedule)
        {
            int num;
            int userId = GetUserId();
            return schedulesdal.DeleteExtractSchedule(schedule.ExtractScheduleId, schedule.ExtractDefinitionId, userId, out num);
        }

        public bool UpdateSchedule(ExtractScheduleDTO schedule)
        {
            int num;
            int userId = GetUserId();
            return schedulesdal.UpdateExtractSchedule(schedule.ExtractScheduleId, schedule.ExtractDefinitionId, schedule.Description, schedule.IsActive, schedule.DaysOfWeek, schedule.ScheduleTime, schedule.ExtractRunArguments, userId, out num, schedule.DayInMonth, (int)schedule.ScheduleType);
        }

        public void Dispose()
        {
            schedulesdal.Dispose();
        }


        private List<ExtractScheduleDTO> SchedulesToList(DataTable dt)
        {
            List<ExtractScheduleDTO> items = null;

            if (dt == null)
                return items;

            items = new List<ExtractScheduleDTO>();

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ExtractScheduleDTO dto = new ExtractScheduleDTO();

                    //this can be null, handle as default
                    int dayInMonth = 0;
                    string s = Trim(dr, "DayInMonth");
                    int.TryParse(s, out dayInMonth);
                    dto.DayInMonth = dayInMonth;
                    dto.DaysOfWeek = Trim(dr, "DaysOfWeek");
                    dto.Description = Trim(dr, "Description");
                    dto.ExtractDefinitionId = dr.Field<long>("ExtractDefinitionID");
                    dto.ExtractRunArguments = Trim(dr, "ExtractRunArguments");
                    dto.ExtractScheduleId = dr.Field<long>("ExtractScheduleID");
                    dto.IsActive = dr.Field<bool>("IsActive");
                    dto.ModificationDate = DateTime.Parse( Trim(dr, "ModificationDate" ) );
                    dto.ScheduleTime = TimeSpan.Parse(Trim(dr, "ScheduleTime"));

                    //check for null schedule types
                    s = Trim(dr, "ScheduleType");
                    int st = 1;
                    if (s.Length > 0)
                    {
                        if (!int.TryParse(s, out st))
                            st = 1;
                    }
                    dto.ScheduleType = (ExtractScheduleType)st;

                    items.Add(dto);
                }
            }
            return items;

        }
        private string Trim(DataRow dr, string column)
        {
            string result = string.Empty;

            if (dr == null)
                return result;

            if (dr[column] == null)
                return result;

            if (dr[column] == DBNull.Value)
                return result;

            return dr[column].ToString().Trim();

        }
        private int GetUserId()
        {
			int userId;
			ValidateSID(out userId);
			return userId;
        }

        protected override bool ValidateSID(out int userID)
        {
            DataTable dt;
            userID = 0;
            var sid = _ServiceContext.GetSID();

            // Call the database, on failure there will be logs.
            var datasuccess = r360servicesDal.GetUserIDBySID(sid, out dt);
            if (!datasuccess)
                return false;

            // Check for data
            if (!dt.HasData())
                return false;

            // Finally parse the UserID.
            return int.TryParse(dt.Rows[0]["UserID"].ToString(), out userID);
        }

		protected override ActivityCodes RequestType
		{
			get { throw new NotImplementedException(); }
		}
	}
}