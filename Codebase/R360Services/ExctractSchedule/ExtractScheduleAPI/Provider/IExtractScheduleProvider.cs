﻿using ExtractScheduleCommon.BusinessObjects;
using System;
using System.Collections.Generic;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleAPI.Services
{
    public interface IExtractScheduleProvider : IDisposable
    {
        /// <summary>
        /// Gets all schedules.
        /// </summary>
        /// <returns>Enumerable of Extract Schedules</returns>
        IEnumerable<ExtractScheduleDTO> GetAllSchedules();

        /// <summary>
        /// Adds a new schedule to the provider. Sets the schedule.ExtractDefinitionID after
        /// a successful addition.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns>True if a successful addition</returns>
        bool AddSchedule(ExtractScheduleDTO schedule);

        /// <summary>
        /// Removes a schedule from the provider.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns>True if a successful removal</returns>
        bool RemoveSchedule(ExtractScheduleDTO schedule);

        /// <summary>
        /// Updates the schedule with the ExtractScheduleID.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns>True if a successful update</returns>
        bool UpdateSchedule(ExtractScheduleDTO schedule);
    }
}
