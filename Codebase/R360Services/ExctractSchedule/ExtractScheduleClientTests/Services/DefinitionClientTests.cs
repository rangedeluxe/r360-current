﻿using ExtractScheduleClient;
using ExtractScheduleCommon.BusinessObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleClientTests.Services
{
    /// <summary>
    /// Test class contains normal tests on the Definition side of the Client Service.
    /// </summary>
    [TestClass]
    public class DefinitionClientTests
    {
        private readonly IExtractScheduleClient configurationClient;
        public DefinitionClientTests()
        {
            configurationClient = new ExtractScheduleClient.ExtractScheduleClient();
        }

        /// <summary>
        /// Tests the data provider, making sure we're getting data.
        /// </summary>
        [TestMethod, Ignore]
        public void TestGetAllDefinitions()
        {
            // Act
            var defs = configurationClient.GetAllDefinitions();

            // Assert
            Assert.IsTrue(defs.Any());
        }

        /// <summary>
        /// Tests the data provider, asking for active definitions on a date.
        /// </summary>
        [TestMethod]
        public void TestGetDefinitionsChangedDateNow()
        {
            // Arrange
            var date = DateTime.Now;

            // Act
            var defs = configurationClient.GetAllDefinitions(date);

            // Assert : There should be no definitions changed after 'Now'. 
            Assert.IsFalse(defs.Any());
        }

        /// <summary>
        /// Tests the data provider, asking for active definitions on a date.
        /// </summary>
        [TestMethod, Ignore]
        public void TestGetDefinitionsChangedDateEarly()
        {
            // Arrange
            var date = DateTime.Now - new TimeSpan(3650, 0, 0, 0);

            // Act
            var defs = configurationClient.GetAllDefinitions(date);

            // Assert : There should be changes made between today and 10 years ago.
            Assert.IsTrue(defs.Any());
        }

        /// <summary>
        /// Tests adding into the data provider - legitimate data.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddDefinition()
        {
            // Arrange
            var definition = new ExtractDefinitionDTO()
            {
                ExtractDefinition = File.ReadAllText(@"Resources/FullPaymentHierarchyDefinition.xml"),
                ExtractName = "Full Payment Hierarchy Definition",
                ExtractDefinitionId = -1
            };

            // Act
            configurationClient.AddDefinition(definition);

            // Assert : Our ID has changed, and is > 0.
            Assert.IsTrue(definition.ExtractDefinitionId > 0);

            // Cleanup
            configurationClient.RemoveDefinition(definition);
        }

        /// <summary>
        /// Tests removing a definition from the data provider.
        /// </summary>
        [TestMethod, Ignore]
        public void TestRemoveDefinition()
        {
            // Arrange
            var definition = new ExtractDefinitionDTO()
            {
                ExtractDefinition = File.ReadAllText(@"Resources/FullPaymentHierarchyDefinition.xml"),
                ExtractName = "Sample Payment Definition Test Removal",
                ExtractDefinitionId = -1
            };

            // Act on Addition
            configurationClient.AddDefinition(definition);

            // Assert our test data is ready.
            Assert.IsTrue(configurationClient.GetAllDefinitions().Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId));

            // Act on Removal
            configurationClient.RemoveDefinition(definition);

            // Assert it's gone.
            Assert.IsFalse(configurationClient.GetAllDefinitions().Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId));
        }

        /// <summary>
        /// Tests updating to the provider. 
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateDefinition()
        {
            // Arrange
            var definition = new ExtractDefinitionDTO()
            {
                ExtractDefinition = File.ReadAllText(@"Resources/FullPaymentHierarchyDefinition.xml"),
                ExtractName = "Sample Payment Definition Test Removal",
                ExtractDefinitionId = -1
            };

            // Act on Addition
            configurationClient.AddDefinition(definition);

            var newname = "Banana";

            // Assert our test data is ready.
            Assert.IsTrue(configurationClient.GetAllDefinitions().Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId));
            Assert.IsFalse(configurationClient.GetAllDefinitions()
                .Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId && x.ExtractName == newname));

            // Act on Update
            definition.ExtractName = newname;
            configurationClient.UpdateDefinition(definition);

            // Assert the provider updated
            Assert.IsTrue(configurationClient.GetAllDefinitions()
                .Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId && x.ExtractName == newname));

            // Cleanup
            configurationClient.RemoveDefinition(definition);
        }
    }
}