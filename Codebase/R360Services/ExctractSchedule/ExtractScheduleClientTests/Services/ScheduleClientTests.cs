﻿using ExtractScheduleClient;
using ExtractScheduleCommon.BusinessObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleTest.ClientTests.Services
{
    /// <summary>
    /// Test class contains normal tests on the Schedule side of the Client Service.
    /// </summary>
    [TestClass]
    public class ScheduleClientTests
    {
        private readonly IExtractScheduleClient configurationClient;

        public ScheduleClientTests()
        {
            // Arrange
            configurationClient = new ExtractScheduleClient.ExtractScheduleClient();
        }

        /// <summary>
        /// Tests the data provider, makes sure we are getting something back.
        /// </summary>
        [TestMethod, Ignore]
        public void TestGetAllSchedules()
        {
            // Act
            var scheds = configurationClient.GetAllSchedules();

            // Assert
            Assert.IsTrue(scheds.Any());
        }

        /// <summary>
        /// Tests adding into the data provider - legitimate data.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddSchedule()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010011",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0)
            };

            // Act
            configurationClient.AddSchedule(schedule);

            // Assert : Our ID has changed, and is > 0.
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Tests removing a schedule from the data provider.
        /// </summary>
        [TestMethod, Ignore]
        public void TestRemoveSchedule()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010011",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0)
            };

            // Act on Addition
            configurationClient.AddSchedule(schedule);

            // Assert our test data is ready.
            Assert.IsTrue(configurationClient.GetAllSchedules().Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId));

            // Act on Removal
            configurationClient.RemoveSchedule(schedule);

            // Assert it's gone.
            Assert.IsFalse(configurationClient.GetAllSchedules().Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId));
        }

        /// <summary>
        /// Tests updating to the provider. 
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateSchedule()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010011",
                Description = "Test Schedule 123",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0)
            };

            // Act on Addition
            configurationClient.AddSchedule(schedule);

            var newname = "Banana";

            // Assert our test data is ready.
            Assert.IsTrue(configurationClient.GetAllSchedules().Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId));
            Assert.IsFalse(configurationClient.GetAllSchedules()
                .Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId && x.Description == newname));

            // Act on Update
            schedule.Description = newname;
            configurationClient.UpdateSchedule(schedule);

            // Assert the provider updated
            Assert.IsTrue(configurationClient.GetAllSchedules()
                .Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId && x.Description == newname));

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }
    }
}