﻿using ExtractScheduleClient;
using ExtractScheduleCommon.BusinessObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose:
*
* Modification History
*
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleClientTests.Services
{
    /// <summary>
    /// Test class contains robustness tests on the Schedule side of the Client Service.
    /// </summary>
    [TestClass]
    public class ScheduleClientRobustnessTests
    {
        private readonly IExtractScheduleClient configurationClient;
        public ScheduleClientRobustnessTests()
        {
            configurationClient = new ExtractScheduleClient.ExtractScheduleClient();
        }

        /// <summary>
        /// Null Days of week : Test should fail.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageDaysOfWeek1()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = null,
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, schedule.ExtractScheduleId);
        }

        /// <summary>
        /// Garbage Days of week : Test should fail.
        ///
        /// TODO: DAL allows this, currently a failing test.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageDaysOfWeek2()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, schedule.ExtractScheduleId);
        }

        /// <summary>
        /// Garbage Days of week : Test should fail.
        ///
        /// TODO: DAL allows this, currently a failing test.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageDaysOfWeek3()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "BANANAS!!!",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, schedule.ExtractScheduleId);
        }

        /// <summary>
        /// Garbage Days of week : Test should fail.
        ///
        /// TODO: DAL allows this, currently a failing test.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageDaysOfWeek4()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, schedule.ExtractScheduleId);
        }

        /// <summary>
        /// Garbage Description : Null value, test should fail.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageDescription1()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0100010",
                Description = null,
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, schedule.ExtractScheduleId);
        }

        /// <summary>
        /// Garbage Description, but should pass as we should allow all values
        /// into this field.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageDescription2()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0100010",
                Description = "!@#$%^&*()_+{}:\">?[];'./-=1234567890 VBNM<>,lp['/",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage ExtractRunArguments : Null value, test should
        /// fail.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageExtractRunArguments1()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0100010",
                Description = "Test Schedule",
                ExtractRunArguments = null,
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, schedule.ExtractScheduleId);
        }

        /// <summary>
        /// Garbage ExtractRunArguments : Empty value, test should pass
        /// as this is an optional field.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageExtractRunArguments2()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0100010",
                Description = "Test Schedule",
                ExtractRunArguments = String.Empty,
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage ScheduleTime : Zero Value, test should pass.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageScheduleTime1()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0100010",
                Description = "Test Schedule",
                ExtractRunArguments = String.Empty,
                ExtractScheduleId = -1,
                ScheduleTime = TimeSpan.Zero,
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage ScheduleTime : Min Value, test should fail as
        /// SQL does not support this range.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageScheduleTime2()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0100010",
                Description = "Test Schedule",
                ExtractRunArguments = String.Empty,
                ExtractScheduleId = -1,
                ScheduleTime = TimeSpan.MinValue,
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, schedule.ExtractScheduleId);
        }

        /// <summary>
        /// Garbage ScheduleTime : Max Value, test should fail as
        /// SQL does not support this range.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddScheduleGarbageScheduleTime3()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0100010",
                Description = "Test Schedule",
                ExtractRunArguments = String.Empty,
                ExtractScheduleId = -1,
                ScheduleTime = TimeSpan.MaxValue,
                IsActive = true
            };

            // Act
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, schedule.ExtractScheduleId);
        }

        /// <summary>
        /// Garbage ExtractScheduleID : -1, test should fail as the DB cannot
        /// find this row to remove.
        ///
        /// TODO :: The DAL returns true even though nothing is removed.
        /// Currently a failing test.
        /// </summary>
        [TestMethod]
        public void TestRemoveScheduleGarbage()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractScheduleId = -1
            };

            // Act
            var success = configurationClient.RemoveSchedule(schedule);

            // Assert : Removal has failed.
            Assert.IsFalse(success);
        }

        /// <summary>
        /// Null Days of week : Test should fail.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageDaysOfWeek1()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.DaysOfWeek = null;
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Failure on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage Days of week : Test should fail.
        ///
        /// TODO: DAL allows this, currently a failing test.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageDaysOfWeek2()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.DaysOfWeek = "";
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Failure on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage Days of week : Test should fail.
        ///
        /// TODO: DAL allows this, currently a failing test.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageDaysOfWeek3()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.DaysOfWeek = "BANANAS!!!!!";
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Failure on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage Days of week : Test should fail.
        ///
        /// TODO: DAL allows this, currently a failing test.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageDaysOfWeek4()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.DaysOfWeek = "0";
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Failure on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage Description : Null value, test should fail.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageDescription1()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.Description = null;
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Failure on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage Description, but should pass as we should allow all values
        /// into this field.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageDescription2()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.Description = "!@#$%^&*()_+{}:\">?[];'./-=1234567890 VBNM<>,lp['/";
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Success on Update
            Assert.IsTrue(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage ExtractRunArguments : Null value, test should
        /// fail.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageExtractRunArguments1()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.ExtractRunArguments = null;
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Failure on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage ExtractRunArguments : Empty value, test should pass
        /// as this is an optional field.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageExtractRunArguments2()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.ExtractRunArguments = String.Empty;
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Success on Update.
            Assert.IsTrue(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage ScheduleTime : Zero Value, test should pass.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageScheduleTime1()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.ScheduleTime = TimeSpan.Zero;
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Success on Update.
            Assert.IsTrue(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage ScheduleTime : Min Value, test should fail as
        /// SQL does not support this range.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageScheduleTime2()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.ScheduleTime = TimeSpan.MinValue;
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Success on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Garbage ScheduleTime : Max Value, test should fail as
        /// SQL does not support this range.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateScheduleGarbageScheduleTime3()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = configurationClient.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010010",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0),
                IsActive = true
            };

            // Act on Add
            var success = configurationClient.AddSchedule(schedule);

            // Assert : Addition has passed, and our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Act on Update
            schedule.ScheduleTime = TimeSpan.MaxValue;
            success = configurationClient.UpdateSchedule(schedule);

            // Assert : Success on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveSchedule(schedule);
        }
    }
}