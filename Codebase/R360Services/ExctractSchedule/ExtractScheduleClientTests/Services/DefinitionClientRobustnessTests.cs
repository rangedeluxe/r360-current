﻿using ExtractScheduleClient;
using ExtractScheduleCommon.BusinessObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleClientTests.Services
{
    /// <summary>
    /// Test class contains robustness tests on the Definition side of the Client Service.
    /// </summary>
    [TestClass]
    public class DefinitionClientRobustnessTests
    {
        private readonly IExtractScheduleClient configurationClient;
        public DefinitionClientRobustnessTests()
        {
            configurationClient = new ExtractScheduleClient.ExtractScheduleClient();
        }

        /// <summary>
        /// Tests the data provider, asking for active definitions since the beginning of time.
        /// </summary>
        [TestMethod]
        public void TestGetDefinitionsChangedBeginningOfTime()
        {
            // Arrange
            var date = DateTime.MinValue;

            // Act
            var defs = configurationClient.GetAllDefinitions(date);

            // Assert : There should not be definitions changed after the beginning of time.
            //   Note, this is DateTime.MinValue, not SqlDateTime.MinValue
            Assert.IsFalse(defs.Any());
        }

        /// <summary>
        /// Tests the data provider, asking for active definitions since the beginning of time.
        /// </summary>
        [TestMethod, Ignore]
        public void TestGetDefinitionsChangedBeginningOfTimeSQL()
        {
            // Arrange
            var date = SqlDateTime.MinValue.Value;

            // Act
            var defs = configurationClient.GetAllDefinitions(date);

            // Assert : There should be definitions changed after the beginning of time.
            Assert.IsTrue(defs.Any());
        }

        /// <summary>
        /// Tests the data provider, asking for active definitions since the end of time.
        /// </summary>
        [TestMethod]
        public void TestGetDefinitionsChangedEndOfTime()
        {
            // Arrange
            var date = DateTime.MaxValue;

            // Act
            var defs = configurationClient.GetAllDefinitions(date);

            // Assert : There should be no definitions changed after the end of time. 
            Assert.IsFalse(defs.Any());
        }

        /// <summary>
        /// Adds a bunch of random data which should be allowed.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddDefinitionGarbageData1()
        {
            // Arrange
            var def = new ExtractDefinitionDTO()
            {
                ExtractName = "!@#$%^&*()_+{}:>?<>'qwer tyuiop[asdfghjklzxcvbnm,/*-+741258963/",
                ExtractDefinition = File.ReadAllText(@"Resources/FullPaymentHierarchyDefinition.xml"),
                ExtractDefinitionId = -1
            };

            // Act
            var success = configurationClient.AddDefinition(def);

            // Assert : Everything passed successfully
            Assert.IsTrue(success);
            Assert.IsTrue(def.ExtractDefinitionId > 0);

            // Cleanup
            configurationClient.RemoveDefinition(def);
        }

        /// <summary>
        /// Adds a bunch of random data which should not be allowed.
        /// </summary>
        [TestMethod]
        public void TestAddDefinitionGarbageData2()
        {
            // Arrange
            var def = new ExtractDefinitionDTO()
            {
                ExtractName = "!@#$%^&*()_+{}:>?<>'qwer tyuiop[asdfghjklzxcvbnm,/*-+741258963/",
                ExtractDefinition = "@RT&Y(*UPkq0 --- THIS IS NOT VALID XML --- %*^(#_U#JKP",
                ExtractDefinitionId = -1
            };

            // Act
            var success = configurationClient.AddDefinition(def);

            // Assert : XML validation failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, def.ExtractDefinitionId);
        }

        /// <summary>
        /// Tests invalid XML (General tag is not closed).
        /// </summary>
        [TestMethod]
        public void TestAddDefinitionGarbageDataXMLFile()
        {
            // Arrange
            var def = new ExtractDefinitionDTO()
            {
                ExtractName = "!@#$%^&*()_+{}:>?<>'qwer tyuiop[asdfghjklzxcvbnm,/*-+741258963/",
                ExtractDefinition = "<ExtractDef><GENERAL></ExtractDef>",
                ExtractDefinitionId = -1
            };

            // Act
            var success = configurationClient.AddDefinition(def);

            // Assert : XML validation failed.
            Assert.IsFalse(success);
            Assert.AreEqual(-1, def.ExtractDefinitionId);
        }

        /// <summary>
        /// This is the Control for the previous garbage test, and should pass.
        /// (the general tag is closed)
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddDefinitionGarbageDataXMLFileControl()
        {
            // Arrange
            var def = new ExtractDefinitionDTO()
            {
                ExtractName = "!@#$%^&*()_+{}:>?<>'qwer tyuiop[asdfghjklzxcvbnm,/*-+741258963/",
                ExtractDefinition = "<ExtractDef><GENERAL /></ExtractDef>",
                ExtractDefinitionId = -1
            };

            // Act
            var success = configurationClient.AddDefinition(def);

            // Assert : Everything passed successfully
            Assert.IsTrue(success);
            Assert.IsTrue(def.ExtractDefinitionId > 0);

            // Cleanup
            configurationClient.RemoveDefinition(def);
        }

        /// <summary>
        /// Tests the remove function of a definition, with an ID
        /// of a definition which does not exist.
        /// 
        /// TODO: The DAL returns TRUE, even though we cannot find
        /// this Definition in the database. Currently a failing test.
        /// </summary>
        [TestMethod]
        public void TestRemoveDefinitionGarbageData1()
        {
            // Arrange
            var def = new ExtractDefinitionDTO()
            {
                ExtractDefinitionId = -1
            };

            // Act
            var success = configurationClient.RemoveDefinition(def);

            // Assert : The definition was not removed.
            Assert.IsFalse(success);
        }

        /// <summary>
        /// Tests invalid XML (General tag is not closed).
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateDefinitionGarbageData1()
        {
            // Arrange
            var def = new ExtractDefinitionDTO()
            {
                ExtractName = "!@#$%^&*()_+{}:>?<>'qwer tyuiop[asdfghjklzxcvbnm,/*-+741258963/",
                ExtractDefinition = "<ExtractDef><GENERAL /></ExtractDef>",
                ExtractDefinitionId = -1
            };

            // Act on Add
            var success = configurationClient.AddDefinition(def);

            // Assert : Our test data is ready.
            Assert.IsTrue(success);
            Assert.IsTrue(def.ExtractDefinitionId > 0);

            // Act on Update
            def.ExtractDefinition = "<ExtractDef><GENERAL></ExtractDef>";
            success = configurationClient.UpdateDefinition(def);

            // Assert Failure on Update.
            Assert.IsFalse(success);

            // Cleanup
            configurationClient.RemoveDefinition(def);
        }
    }
}