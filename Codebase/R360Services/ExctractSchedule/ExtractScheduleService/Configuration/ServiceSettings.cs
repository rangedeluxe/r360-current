﻿using ExtractScheduleCommon.Configuration;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleService.Configuration
{
    /// <summary>
    /// Contains the settings from the Application Configuration.
    /// </summary>
    public class ServiceSettings : BaseLogSettings
    {
        [ConfigurationSetting]
        public string localIni { get; set; }

        public ServiceSettings(IConfigurationProvider configProvider)
            : base(configProvider)
        {
        }

        public override void ValidateSettings()
        {
            base.ValidateSettings();

            // Check LocalINI File
            if (string.IsNullOrWhiteSpace(localIni))
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("localIni");
            if (!File.Exists(localIni))
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("localIni");
        }
    }
}