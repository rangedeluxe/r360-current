﻿using ExtractScheduleAPI.Services;
using ExtractScheduleCommon.API;
using ExtractScheduleCommon.Configuration;
using ExtractScheduleService.Configuration;
using System;
using System.Collections.Generic;
using System.ServiceModel.Web;
using WFS.LTA.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
* WI 146351 DJW 623/2014
*  Added Logging/Auditing support.
*           RDS 06/30/2014
*  Convert to use R360Shared
* WI 158888 BLR 08/25/2014
*   - Created the GetXML method.
*******************************************************************************/

namespace ExtractScheduleService
{
    /// <summary>
    /// The main entry-point for the Extract Configuration Service WCF application.
    /// See IExtractScheduleService for documentation on individual methods.
    /// </summary>
    public class ExtractScheduleService : LTAService, IExtractScheduleService
    {
        private readonly IConfigurationProvider configurationProvider;
        private readonly ServiceSettings settings;

        public ExtractScheduleService()
			: base()
        {
            // TODO: Dependency Injection?
            configurationProvider = new ConfigurationManagerProvider();
            settings = new ServiceSettings(configurationProvider);

            // Boot up the logger.
            if (CommonLogLib.Settings == null)
                CommonLogLib.Settings = settings;

            // Validate settings
            try
            {
                settings.ValidateSettings();
            }
            catch(Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
        }

        public IEnumerable<ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO> GetAllDefinitions()
        {
            try
            {
                return new DatabaseExtractDefinitionProvider(ServiceContext)
                    .GetAllDefinitions();
            }
            catch (Exception e) 
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return null;
        }

        public IEnumerable<ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO> GetActiveDefinitions(DateTime? changedAfter)
        {
            try
            {
				return new DatabaseExtractDefinitionProvider(ServiceContext)
                    .GetAllDefinitions(changedAfter);
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return null;
        }

        public int AddDefinition(ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO definition)
        {
            try
            {
				return new DatabaseExtractDefinitionProvider(ServiceContext).AddDefinition(definition)
                    ? (int)definition.ExtractDefinitionId
                    : -1;
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return -1;
        }

        public bool RemoveDefinition(ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO definition)
        {
            try
            {
				return new DatabaseExtractDefinitionProvider(ServiceContext)
                    .RemoveDefinition(definition);
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return false;
        }

        public bool UpdateDefinition(ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO definition)
        {
            try
            {
                return new DatabaseExtractDefinitionProvider(ServiceContext)
                    .UpdateDefinition(definition);
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return false;
        }
        public bool UpdateExtractName(int definitionId, string extractName)
        {
            try
            {
				return new DatabaseExtractDefinitionProvider(ServiceContext).UpdateExtractName(definitionId, extractName);
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return false;
        }

        public IEnumerable<ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO> GetAllSchedules()
        {
            try
            {
				return new DatabaseExtractScheduleProvider(ServiceContext)
                    .GetAllSchedules();
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return null;
        }

        public int AddSchedule(ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO schedule)
        {
            try
            {
				return new DatabaseExtractScheduleProvider(ServiceContext).AddSchedule(schedule)
                    ? (int)schedule.ExtractScheduleId
                    : -1;
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return -1;
        }

        public bool RemoveSchedule(ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO schedule)
        {
            try
            {
				return new DatabaseExtractScheduleProvider(ServiceContext)
                    .RemoveSchedule(schedule);
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return false;
        }

        public bool UpdateSchedule(ExtractScheduleCommon.BusinessObjects.ExtractScheduleDTO schedule)
        {
            try
            {
				return new DatabaseExtractScheduleProvider(ServiceContext)
                    .UpdateSchedule(schedule);
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return false;
        }

		protected override APIBase NewAPIInst(R360ServiceContext serviceContext)
		{
			// There are two API's used by this service... which one would we return...?  Needs refactoring...
			throw new NotImplementedException();
		}

        public ExtractScheduleCommon.BusinessObjects.ExtractDefinitionDTO GetDefinitionXML(int definitionid)
        {
            try
            {
                return new DatabaseExtractDefinitionProvider(ServiceContext)
                    .GetDefinitionXML(definitionid);
            }
            catch (Exception e)
            {
                CommonLogLib.LogErrorLTA(e, LTAMessageImportance.Essential);
            }
            return null;
        }
    }
}