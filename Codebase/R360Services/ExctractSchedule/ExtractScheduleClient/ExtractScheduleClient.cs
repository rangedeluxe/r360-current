﻿using ExtractScheduleCommon;
using ExtractScheduleCommon.API;
using ExtractScheduleCommon.BusinessObjects;
using ExtractScheduleCommon.Configuration;
using ExtractScheduleService;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using WFS.LTA.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
* WI 146351 DJW 623/2014
*  Added Logging/Auditing support.
*           RDS 06/30/2014
*  Convert to use R360Shared
* WI 158888 BLR 08/25/2014
*   - Created the GetXML method.
*******************************************************************************/

namespace ExtractScheduleClient
{
    /// <summary>
    /// Main client-side service to communicate with the Server WCF Service.
    /// See IExtractScheduleClient for documentation on individual methods.
    /// </summary>
    public class ExtractScheduleClient : IExtractScheduleClient
    {
		private readonly IExtractScheduleService _ExtractService = null;

        private readonly IConfigurationProvider configurationProvider;
        private readonly BaseLogSettings settings;
        public ExtractScheduleClient()
        {
            configurationProvider = new ConfigurationManagerProvider();
            settings = new BaseLogSettings(configurationProvider);

            // Boot up the logger, but these settings should be active already.
            if (CommonLogLib.Settings == null)
                CommonLogLib.Settings = settings;

            // Validate settings
            try
            {
                settings.ValidateSettings();
            }
            catch(Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }

			try
			{
				if (LogManager.IsDefault) LogManager.Logger = CommonLogLib.CreateLTALogMessage().LTAtoILogger(this.GetType().Name);
				_ExtractService = R360ServiceFactory.Create<IExtractScheduleService>();
			}
			catch (Exception ex)
			{
				CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
			}
        }

		private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
		{
			// All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
			if (_ExtractService == null)
				throw new Exception("Configuration Error - check log for details");

			try
			{
				using (new OperationContextScope((IContextChannel)_ExtractService))
				{
					// Set context inside of operation scope
					if (LogManager.IsDefault) LogManager.Logger = CommonLogLib.CreateLTALogMessage().LTAtoILogger(this.GetType().Name);
					R360ServiceContext.Init(ConfigHelpers.GetAppSetting("SiteKey", Constants.DEFAULT_CONFIGURATION_SITEKEY));

					return operation();
				}
			}
			catch (Exception ex)
			{
				CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
				throw;
			}
		}
        public IEnumerable<ExtractDefinitionDTO> GetAllDefinitions()
        {
			try
			{
				return Do(() => _ExtractService.GetAllDefinitions())
					?? new ExtractDefinitionDTO[0];
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return new ExtractDefinitionDTO[0];
        }

        public IEnumerable<ExtractDefinitionDTO> GetAllDefinitions(DateTime? changedAfter)
        {
            try
            {
                return Do(() => _ExtractService.GetActiveDefinitions(changedAfter))
                    ?? new ExtractDefinitionDTO[0];
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return new ExtractDefinitionDTO[0];
        }

        public bool AddDefinition(ExtractDefinitionDTO definition)
        {
            try
            {
                var now = DateTime.Now;
                var id = Do(() => _ExtractService.AddDefinition(definition));
                if (id <= 0)
                    return false;

                definition.ExtractDefinitionId = id;

                return true;
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return false;
        }

        public bool RemoveDefinition(ExtractDefinitionDTO definition)
        {
            try
            {
                return Do(() => _ExtractService.RemoveDefinition(definition));
            }
            catch(Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return false;
        }

        public bool UpdateDefinition(ExtractDefinitionDTO definition)
        {
            try
            {
				return Do(() => _ExtractService.UpdateDefinition(definition));
            }
            catch(Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return false;
        }

        public bool UpdateExtractName(int definitionId, string extractName)
        {
            try
            {
                return Do(() => _ExtractService.UpdateExtractName(definitionId, extractName));
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return false;
        }

        public IEnumerable<ExtractScheduleDTO> GetAllSchedules()
        {
            try
            {
                return Do(() => _ExtractService.GetAllSchedules())
                    ?? new ExtractScheduleDTO[0];
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return new ExtractScheduleDTO[0];
        }

        public bool AddSchedule(ExtractScheduleDTO schedule)
        {
            try
            {
                var id = Do(() => _ExtractService.AddSchedule(schedule));
                if (id <= 0)
                    return false;

                schedule.ExtractScheduleId = id;

                return true;
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return false;
        }

        public bool RemoveSchedule(ExtractScheduleDTO schedule)
        {
            try
            {
                return Do(() => _ExtractService.RemoveSchedule(schedule));
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return false;
        }

        public bool UpdateSchedule(ExtractScheduleDTO schedule)
        {
            try
            {
                return Do(() => _ExtractService.UpdateSchedule(schedule));
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return false;
        }

        public ExtractDefinitionDTO GetDefinitionXML(int definitionId)
        {
            try
            {
                return Do(() => _ExtractService.GetDefinitionXML(definitionId));
            }
            catch (Exception ex)
            {
                CommonLogLib.LogErrorLTA(ex, LTAMessageImportance.Essential);
            }
            return null;
        }
    }
}
