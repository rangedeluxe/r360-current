﻿using ExtractScheduleCommon.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
* WI 146351 DJW 623/2014
*  Added Logging/Auditing support.
* WI 158888 BLR 08/25/2014
*   - Created the GetXML method.
*******************************************************************************/

namespace ExtractScheduleClient
{
    public interface IExtractScheduleClient
    {
        /// <summary>
        /// Gets all active definitions.
        /// </summary>
        /// <returns>Enumerable of Extract Definitions</returns>
        IEnumerable<ExtractDefinitionDTO> GetAllDefinitions();

        /// <summary>
        /// Gets all active definitions which are changed after changedAfter.
        /// </summary>
        /// <param name="changedAfter"></param>
        /// <returns>Enumerable of Extract Definitions</returns>
        IEnumerable<ExtractDefinitionDTO> GetAllDefinitions(DateTime? changedAfter);

        /// <summary>
        /// Adds a new definition to the provider. Sets the definition.ExtractDefinitionID after
        /// a successful addition.
        /// </summary>
        /// <param name="definition"></param>
        /// <returns>True if a successful addition</returns>
        bool AddDefinition(ExtractDefinitionDTO definition);

        /// <summary>
        /// Removes a definition from the provider.
        /// </summary>
        /// <param name="definition"></param>
        /// <returns>True if a successful addition</returns>
        bool RemoveDefinition(ExtractDefinitionDTO definition);

        /// <summary>
        /// Updates the definition with the ExtractDefinitionID.
        /// </summary>
        /// <param name="definition"></param>
        /// <returns>True if a successful update</returns>
        bool UpdateDefinition(ExtractDefinitionDTO definition);

        /// <summary>
        /// Updates the definition's name.
        /// </summary>
        /// <param name="definitionId">Id of the definition to be updated.</param>
        /// <param name="extractName">New extract name.</param>
        /// <returns>True if a successful update</returns>
        bool UpdateExtractName(int definitionId, string extractName);

        /// <summary>
        /// Gets all active schedules.
        /// </summary>
        /// <returns>Enumerable of Extract Schedules</returns>
        IEnumerable<ExtractScheduleDTO> GetAllSchedules();

        /// <summary>
        /// Adds a new schedule to the provider. Sets the schedule.ExtractDefinitionID after
        /// a successful addition.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns>True if a successful addition</returns>
        bool AddSchedule(ExtractScheduleDTO schedule);

        /// <summary>
        /// Removes a schedule from the provider.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns>True if a successful removal</returns>
        bool RemoveSchedule(ExtractScheduleDTO schedule);

        /// <summary>
        /// Updates the schedule with the ExtractScheduleID.
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns>True if a successful update</returns>
        bool UpdateSchedule(ExtractScheduleDTO schedule);

        /// <summary>
        /// Returns the XML definition.
        /// </summary>
        /// <param name="definitionId"></param>
        /// <returns></returns>
        ExtractDefinitionDTO GetDefinitionXML(int definitionId);
    }
}
