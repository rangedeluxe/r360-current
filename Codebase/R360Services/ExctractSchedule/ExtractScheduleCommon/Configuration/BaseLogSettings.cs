﻿using System;
using System.IO;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleCommon.Configuration
{
    /// <summary>
    /// Provides application settings for basic logging functionality. 
    /// </summary>
    public class BaseLogSettings : BaseConfigurationSettings
    {
        [ConfigurationSetting]
        public string LogFilePath { get; set; }
        [ConfigurationSetting]
        public string LogFileMaxSize { get; set; }
        [ConfigurationSetting]
        public string LoggingDepth { get; set; }

        public int LogFileMaxSizeInt { get; set; }
        public int LoggingDepthInt { get; set; }

        public BaseLogSettings(IConfigurationProvider configurationProvider)
            : base (configurationProvider)
        {
            LoadSettings();
        }

        private void LoadSettings()
        {
            // Conversions
            int tempvalueint;
            int.TryParse(LogFileMaxSize, out tempvalueint);
            LogFileMaxSizeInt = tempvalueint;

            int.TryParse(LoggingDepth, out tempvalueint);
            LoggingDepthInt = tempvalueint;
        }

        public override void ValidateSettings()
        {
            // Check LogFilePath
            if (string.IsNullOrWhiteSpace(LogFilePath))
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("LogFilePath");
            try
            {
                if (!Directory.Exists(Path.GetDirectoryName(LogFilePath)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(LogFilePath));
                }
            }
            catch (Exception)
            {
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("LogFilePath");
            }
            // Check LogFileMaxSize
            if (string.IsNullOrWhiteSpace(LogFileMaxSize))
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("LogFileMaxSize");
            int value = -1;
            if (!int.TryParse(LogFileMaxSize, out value))
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("LogFileMaxSize");
            if (value < 0)
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("LogFileMaxSize");

            // Check LoggingDepth
            value = -1;
            if (string.IsNullOrWhiteSpace(LoggingDepth))
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("LoggingDepth");
            if (!int.TryParse(LoggingDepth, out value))
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("LoggingDepth");
            if (value < 0 || value > 2)
                throw new ExtractScheduleCommon.Configuration.ConfigurationException("LoggingDepth");
        }
    }
}
