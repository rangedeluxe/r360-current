﻿
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleCommon.Configuration
{
    public interface IConfigurationProvider
    {
        /// <summary>
        /// Returns a string 'Value' given the parameter 'key'.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string GetSetting(string key);

        /// <summary>
        /// In case a setting needs to be set dynamically. (The tests do)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        void SetSetting(string key, string value);
    }
}
