﻿
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleCommon
{
    public static class Constants
    {
        public const string CONFIGURATION_EXCEPTION_MESSAGE = "Validation failed on configuration setting '{0}'.";
        public const string DEFAULT_CONFIGURATION_SITEKEY = "IPOnline";

        public const string HEADER_SITE_KEY = "SiteKey";

        public static string Format(string constant, params object[] paras)
        {
            return string.Format(CONFIGURATION_EXCEPTION_MESSAGE, paras);
        }
    }
}
