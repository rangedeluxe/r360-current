﻿using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
* WI 138899 DJW 05/08/2014
*   Added support for Monthly schedule type.
* WI 141430 DJW 05/13/2014
*   Moved the enum outside the object
*******************************************************************************/

namespace ExtractScheduleCommon.BusinessObjects
{
    /// <summary>
    /// This is a typical data transfer object used in multiple projects. This
    /// implementation was copied from another.
    /// </summary>
    public class ExtractScheduleDTO
    {
        public Int64 ExtractScheduleId { get; set; }
        public Int64 ExtractDefinitionId { get; set; }
        public DateTime ModificationDate { get; set; }
        public string DaysOfWeek { get; set; }
        public TimeSpan ScheduleTime { get; set; }
        public string Description { get; set; }
        public string ExtractRunArguments { get; set; }
        public bool IsActive { get; set; }
        public ExtractScheduleType ScheduleType { get; set; }
        public int DayInMonth { get; set; }

        // Not a property, we don't want this deserializing.
        public DateTime LastTimeRun;

        public ExtractScheduleDTO()
        {
            this.LastTimeRun = DateTime.Parse("1/1/1900");
        }

    }
    
    public enum ExtractScheduleType : int
    {
        Unknown = 0,
        Weekly = 1,
        Monthly = 2
    }



}
