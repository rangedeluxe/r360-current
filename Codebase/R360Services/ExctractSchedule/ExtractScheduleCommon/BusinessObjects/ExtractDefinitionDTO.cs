﻿using System;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
* WI 138889 DJW 05/08/2014
* WI 140642 DJW 05/08/2014
*   Added support for ExtractBillings
*   And the original filename of the upload.
* WI 159239 BLR 08/18/2014
*   - Added Definition Size.  
*******************************************************************************/

namespace ExtractScheduleCommon.BusinessObjects
{
    /// <summary>
    /// This is a typical data transfer object used in multiple projects. This
    /// implementation was copied from another.
    /// </summary>
    public class ExtractDefinitionDTO
    {
        public string ExtractDefinition { get; set; }
        public string ExtractName { get; set; }
        public Int64 ExtractDefinitionId { get; set; }
        public ExtractDefinitionType ExtractDefinitionType { get; set; }
        public string ExtractFilename { get; set; }
        public int ExtractDefinitionSize { get; set; }
    }

    public enum ExtractDefinitionType : int
    {
        Unknown = 0,
        Wizard = 1,
        Billing = 2
    }

}
