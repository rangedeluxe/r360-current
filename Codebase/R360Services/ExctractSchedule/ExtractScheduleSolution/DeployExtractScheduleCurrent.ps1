#Shell to Common R360Services Deployment Script, with correct parameters
$thisScript = Split-Path $MyInvocation.MyCommand.Path 
$commonScript = Join-Path $thisScript "..\..\ServiceShares\R360BuildDeployment.ps1"
$commonScript = [System.IO.Path]::GetFullPath($commonScript)

$commandWithParameters = "$commonScript" `
    + " -service RecHub.R360_Service.ExtractSchedule" `
    + " -appConfig:web.config" `
		+ " -configSubFolder:_PublishedWebsites\ExtractScheduleService" `
    + " -folderName:RecHubExtractScheduleService" `
    + " -dlls:`"" `
		+ "ExtractDefinitionsDAL.dll," `
		+ "ExtractScheduleAPI.dll," `
		+ "ExtractScheduleCommon.dll," `
		+ "ExtractSchedulesDal.dll," `
		+ "ExtractScheduleService.dll," `
	    + "DALBase.dll," `
	    + "ipoCrypto.dll," `
	    + "ipoDB.dll," `
	    + "ipoLib.dll," `
	    + "ipoLog.dll," `
		+ "ltaLog.dll," `
		+ "R360RaamClient.dll," `
	    + "R360Shared.dll," `
		+ "R360DataServiceDAL.dll," `
	    + "Wfs.Raam.Core.dll," `
		+ "Wfs.Raam.Service.Authorization.Contracts.dll," `
	    + "Thinktecture.IdentityModel.dll," `
		+ "WFS.RecHub.ApplicationBlocks.DataAccess.dll," `
		+ "WFS.RecHub.ApplicationBlocks.Common.dll" `
        + "`"" `
    + " -web" `

Write-Host "Invoking Deployment Script with parameters: " $commandWithParameters

invoke-expression $commandWithParameters

Write-Host "Deployment Complete"