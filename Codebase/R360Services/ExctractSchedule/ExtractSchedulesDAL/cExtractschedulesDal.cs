﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using System.Data.SqlClient;
/*******************************************************************************
*
*    Module: cExtractDal
*  Filename: cExtractDal.cs
*    Author: Dave Parker
*
* Revisions:
*
* ----------------------
* CR 97760 DRP 05/09/2013
*   -Initial release version.
* WI 103467 DRP 05/29/2013
*   -Added ExtractRunArguments to usp and DAL
* WI  97719 DRP 05/30/2013
*   -Added UserID as a parameter to DEL, UPD, INS calls to allow audit by user to work
* WI 107430 DRP 06/26/2013
*   -Added try/catch to DAL, switched from SQL statement to SP call on GetExtractNames();
* WI 109747 EAS 07/30/2013
*   DAL components called with RecHubAdmin Connection Type 
* WI 138899 DJW 05/08/2014
*   Added support for Monthly Schedule type.
* WI 141347 DJS 5/13/2014
*  Added the overloaded methods to keep cExtractSchedulesDal compatible with OLAdmin
* WI 146351 DJW 6/23/2014
*  Added Logging/Auditing support.
******************************************************************************/
namespace WFS.RecHub.DAL 
{
    public class cExtractSchedulesDal : _DALBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="cExtractDal"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
        public cExtractSchedulesDal(string vSiteKey)
			: base(vSiteKey) {
		}

        public cExtractSchedulesDal(string vSiteKey, ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }

        #region Extract Defintions
        /// <summary>
        /// Get the active extract definitions
        /// </summary>
        /// <param name="UpdatedSince">Optional, get updates since this datetime</param>
        /// <param name="results">DataTable with selected records</param>
        /// <returns>true = success</returns>
        public bool GetActiveExtractDefinitions(DateTime? UpdatedSince, out DataTable results)
        {
            bool bolRetVal = false;
            results = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            try
            {
                arParms = new List<SqlParameter>();
                if (UpdatedSince != null)
                    arParms.Add(BuildParameter("@modifiedAfter", SqlDbType.DateTime, UpdatedSince, ParameterDirection.Input));

                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractDefinitions_GetAfterModDate", parms, out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetActiveExtractDefinitions(DateTime? UpdatedSince, out DataTable results)");
            }
            return bolRetVal;
        }
        /// <summary>
        /// temp procedure to get the extract names and ids.
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        public bool GetExtractNames(out DataTable results)
        {
            bool bolRetVal = false;
            results = null;

            SqlParameter[] parms;
            SqlParameter parmTotalRecords;
            List<SqlParameter> arParms = new List<SqlParameter>();
            try
            {
                arParms.Add(BuildParameter("@parmStartRecord", SqlDbType.Int, 1, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmRecordsPerPage", SqlDbType.Int, -1, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmActiveOnly", SqlDbType.Bit, 0, ParameterDirection.Input));
                parmTotalRecords = BuildParameter("@parmTotalRecords", SqlDbType.Int, 0, ParameterDirection.Output);
                arParms.Add(parmTotalRecords);

                parms = arParms.ToArray();

                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractDefinitions_Get", parms, out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetExtractNames(out DataSet results)");
            }
            
            return bolRetVal;
        }

        #endregion

        #region Extract Schedules
        /// <summary>
        /// Get the active extract schedule
        /// </summary>
        /// <param name="results">DataTable with selected records</param>
        /// <returns>true = success</returns>
        public bool GetActiveExtractSchedule(out DataTable results)
        {
            bool bolRetVal = false;
            results = null;
            try
            {
                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractSchedules_GetActiveList", out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetActiveExtractSchedule(out DataTable results)");
            }
 
            return bolRetVal;
        }

        /// <summary>
        /// Get the all or selected extract schedule(s)
        /// </summary>
        /// <param name="ExtractDefinitionId">NULL for select all, otherwise the defintion id to filter on</param>
        /// <param name="ExtractScheduleId">NULL for all, otherwise the schedule id to filter on</param>
        /// <param name="FirstRec">for paging, the first record number to get.</param>
        /// <param name="LastRec">for paging, the last record number to get.</param>
        /// <param name="NumberOfRecords">the number of records in the whole table.</param>
        /// <param name="results">DataTable with selected records</param>
        /// <returns>true = success</returns>
        public bool GetExtractSchedule(Int64? ExtractDefinitionId, Int64? ExtractScheduleId, Int64? FirstRec, Int64? LastRec, out DataTable results, out int NumberOfRecords)
        {
            bool bolRetVal = false;
            results = null;
            NumberOfRecords = 0;
            try
            {
                SqlParameter outRecs = BuildParameter("@NumberOfRecords", SqlDbType.Int, 0, ParameterDirection.Output);

                SqlParameter[] parms = {BuildParameter("@ExtractDefinitionId", SqlDbType.BigInt, ExtractDefinitionId, ParameterDirection.Input), 
                                    BuildParameter("@ExtractScheduleId", SqlDbType.BigInt, ExtractScheduleId, ParameterDirection.Input),
                                    BuildParameter("@FirstRec", SqlDbType.BigInt, FirstRec, ParameterDirection.Input),
                                    BuildParameter("@LastRec", SqlDbType.BigInt, LastRec, ParameterDirection.Input),
                                    outRecs
                                   };
                
                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractSchedules_Get", parms, out results);
                NumberOfRecords = (int)outRecs.Value;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetExtractSchedule(Int64? ExtractDefinitionId, Int64? ExtractScheduleId, Int64? FirstRec, Int64? LastRec, out DataTable results, out int NumberOfRecords)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Deletes one or extract schedule records.  Set ExtractScheduleId to delete one record; set ExtractDefinitionId to delete all for that extract ID
        /// </summary>
        /// <param name="ExtractScheduleId">the schedule record id to delete</param>
        /// <param name="ExtractDefinitionId">The definition id that is being deleted.</param>
        /// <param name="UserId">The userid number for who is making the changes</param>
        /// <param name="NumberOfRecords">Number of records affected by the delete.</param>
        /// <returns>true= success</returns>
        public bool DeleteExtractSchedule(Int64? ExtractScheduleId, Int64? ExtractDefinitionId, int? UserId, out int NumberOfRecords)
        {
            bool bolRetVal = false;
            NumberOfRecords = 0;
            try
            {

                SqlParameter outRecs = BuildParameter("@NumberOfRecords", SqlDbType.Int, 0, ParameterDirection.Output);

                if (ExtractScheduleId == null && ExtractDefinitionId == null)
                    throw new Exception("Both ExtractScheduleId and ExtractDefinitionId cannot be null at the same time");

                SqlParameter[] parms = { BuildParameter("@ExtractScheduleId", SqlDbType.BigInt, ExtractScheduleId, ParameterDirection.Input),
                                     BuildParameter("@ExtractDefinitionId", SqlDbType.BigInt, ExtractDefinitionId, ParameterDirection.Input),
                                     BuildParameter("@UserID", SqlDbType.Int, UserId, ParameterDirection.Input),
                                     outRecs
                                   };

                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractSchedules_Del", parms);
                NumberOfRecords = (int)outRecs.Value;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteExtractSchedule(Int64? ExtractScheduleId, Int64? ExtractDefinitionId, Int64? UserId, out int NumberOfRecords)");
            }

            return bolRetVal;
        }

        /// <summary>
        /// Updates the fields in the Extract Schedule table
        /// </summary>
        /// <param name="ExtractScheduleId">Which record to update</param>
        /// <param name="ExtractDefinitionId">the definition id</param>
        /// <param name="Description">Schedule description</param>
        /// <param name="IsActive">1 = Active schedule record</param>
        /// <param name="DaysOfWeek">7 character string, first char=Sunday; 1 = on, 0=off</param>
        /// <param name="ScheduleTime">Time of day (time past midnight) to run this extract</param>
        /// <param name="ExtractRunArguments">The string of switches to pass to the extract run program</param>
        /// <param name="UserId">The user id number for the person making the changes</param>
        /// <param name="NumberOfRecords">ouput, the number of records changed by the update</param>
        /// <returns>true=update successful</returns>
        public bool UpdateExtractSchedule(Int64 ExtractScheduleId,
             Int64 ExtractDefinitionId,
             string Description,
             bool IsActive,
             string DaysOfWeek,
             TimeSpan ScheduleTime,
             string ExtractRunArguments,
              int? UserId,
              out int NumberOfRecords
         )
        {
            return UpdateExtractSchedule(ExtractScheduleId,
               ExtractDefinitionId,
               Description,
               IsActive,
               DaysOfWeek,
               ScheduleTime,
               ExtractRunArguments,
               UserId,
               out NumberOfRecords,
               0,
               1);
        }

        public bool UpdateExtractSchedule(Int64 ExtractScheduleId,
            Int64 ExtractDefinitionId, 
            string Description,
            bool IsActive,
            string DaysOfWeek,
            TimeSpan ScheduleTime,
            string ExtractRunArguments,
             int? UserId,
             out int NumberOfRecords,
            int DayInMonth,
            int ScheduleType
        )

        {
            bool bolRetVal = false;
            NumberOfRecords = 0;
            try
            {
                SqlParameter outRecs = BuildParameter("@NumberOfRecords", SqlDbType.Int, 0, ParameterDirection.Output);

                SqlParameter[] parms = {BuildParameter("@ExtractDefinitionId", SqlDbType.BigInt, ExtractDefinitionId, ParameterDirection.Input), 
                                    BuildParameter("@ExtractScheduleId", SqlDbType.BigInt, ExtractScheduleId, ParameterDirection.Input),
                                    BuildParameter("@Description", SqlDbType.VarChar, Description, ParameterDirection.Input),
                                    BuildParameter("@IsActive", SqlDbType.Bit, IsActive, ParameterDirection.Input),
                                    BuildParameter("@DaysOfWeek", SqlDbType.VarChar, DaysOfWeek, ParameterDirection.Input),
                                    BuildParameter("@ExtractRunArguments", SqlDbType.VarChar, ExtractRunArguments, ParameterDirection.Input),
                                    BuildParameter("@ScheduleTime", SqlDbType.Time, ScheduleTime, ParameterDirection.Input),
                                    BuildParameter("@UserID", SqlDbType.Int, UserId, ParameterDirection.Input),
                                    BuildParameter("@DayInMonth", SqlDbType.Int, DayInMonth, ParameterDirection.Input),
                                    BuildParameter("@ScheduleType", SqlDbType.Int, ScheduleType, ParameterDirection.Input),
                                    outRecs
                                  };

                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractSchedules_Upd", parms);
                NumberOfRecords = (int)outRecs.Value;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateExtractSchedule(...)");
            }

            return bolRetVal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ExtractScheduleId">Output parameter, the id of the record just added</param>
        /// <param name="ExtractDefinitionId">the definition id</param>
        /// <param name="Description">Schedule description</param>
        /// <param name="IsActive">1 = Active schedule record</param>
        /// <param name="DaysOfWeek">7 character string, first char=Sunday; 1 = on, 0=off</param>
        /// <param name="ScheduleTime">Time of day (time past midnight) to run this extract</param>
        /// <param name="ExtractRunArguments">The string of switches to pass to the extract runtime</param>
        /// <param name="UserId">The user id number of the person adding the record</param>
        /// <returns>true=insert successful</returns>
        public bool AddExtractSchedule(out Int64 ExtractScheduleId,
            Int64 ExtractDefinitionId,
            string Description,
            bool IsActive,
            string DaysOfWeek,
            TimeSpan ScheduleTime,
            string ExtractRunArguments,
             int? UserId
        )
        {
            return AddExtractSchedule(out ExtractScheduleId,
            ExtractDefinitionId,
            Description,
            IsActive,
            DaysOfWeek,
            ScheduleTime,
            ExtractRunArguments,
            UserId,
            0,
            1);

        }
        public bool AddExtractSchedule(out Int64 ExtractScheduleId,
            Int64 ExtractDefinitionId,
            string Description,
            bool IsActive,
            string DaysOfWeek,
            TimeSpan ScheduleTime,
            string ExtractRunArguments,
             int? UserId,
            int DayInMonth,
            int ScheduleType
        )
        {
            bool bolRetVal = false;
            ExtractScheduleId = -1;
            try
            {
                ExtractScheduleId = 0;
                SqlParameter newId = BuildParameter("@ExtractScheduleId", SqlDbType.BigInt, 0, ParameterDirection.Output);
                SqlParameter[] parms = {BuildParameter("@ExtractDefinitionId", SqlDbType.BigInt, ExtractDefinitionId, ParameterDirection.Input), 
                                    newId,
                                    BuildParameter("@Description", SqlDbType.VarChar, Description, ParameterDirection.Input),
                                    BuildParameter("@IsActive", SqlDbType.Bit, IsActive, ParameterDirection.Input),
                                    BuildParameter("@DaysOfWeek", SqlDbType.VarChar, DaysOfWeek, ParameterDirection.Input),
                                    BuildParameter("@ScheduleTime", SqlDbType.Time, ScheduleTime, ParameterDirection.Input),
                                    BuildParameter("@ExtractRunArguments", SqlDbType.VarChar, ExtractRunArguments, ParameterDirection.Input),
                                    BuildParameter("@UserID", SqlDbType.Int, UserId, ParameterDirection.Input),
                                    BuildParameter("@DayInMonth", SqlDbType.Int, DayInMonth, ParameterDirection.Input),
                                    BuildParameter("@ScheduleType", SqlDbType.Int, ScheduleType, ParameterDirection.Input)
                                  };

                bolRetVal = Database.executeProcedure("RecHubConfig.usp_ExtractSchedules_Ins", parms);
                if (bolRetVal)
                    ExtractScheduleId = (Int64)newId.Value;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "AddExtractSchedule(...)");
            }

            return bolRetVal;
        }
        #endregion
    }
}
