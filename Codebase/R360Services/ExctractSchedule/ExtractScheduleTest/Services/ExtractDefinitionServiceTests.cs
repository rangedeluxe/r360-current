﻿using ExtractScheduleAPI.Services;
using ExtractScheduleCommon;
using ExtractScheduleCommon.BusinessObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose:
*
* Modification History
*
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleTest.Services
{
    /// <summary>
    /// Class tests the API Extract Definition Services (Server-Side, after a Client Call)
    /// </summary>
    [TestClass]
    public class ExtractDefinitionServiceTests
    {
        private readonly IExtractDefinitionProvider extractDefinitionService;

        public ExtractDefinitionServiceTests()
        {
            // Arrange
            extractDefinitionService = new DatabaseExtractDefinitionProvider(R360ServiceContext.Current); // TODO: Allow Faking the SiteKey = Constants.DEFAULT_CONFIGURATION_SITEKEY
        }

        /// <summary>
        /// Tests the data provider, makes sure we are getting something back.
        /// </summary>
        [TestMethod, Ignore]
        public void TestGetAllDefinitions()
        {
            // Act
            var defs = extractDefinitionService.GetAllDefinitions();

            // Assert
            Assert.IsTrue(defs.Any());
        }

        /// <summary>
        /// Tests the data provider, asking for active definitions on a date.
        /// </summary>
        [TestMethod, Ignore]
        public void TestGetDefinitionsChangedDateNow()
        {
            // Arrange
            var date = DateTime.Now;

            // Act
            var defs = extractDefinitionService.GetAllDefinitions(date);

            // Assert : There should be no definitions changed after 'Now'.
            Assert.IsFalse(defs.Any());
        }

        /// <summary>
        /// Tests the data provider, asking for active definitions on a date.
        /// </summary>
        [TestMethod, Ignore]
        public void TestGetDefinitionsChangedDateEarly()
        {
            // Arrange
            var date = DateTime.Now - new TimeSpan(3650, 0, 0, 0);

            // Act
            var defs = extractDefinitionService.GetAllDefinitions(date);

            // Assert : There should be changes made between today and 10 years ago.
            Assert.IsTrue(defs.Any());
        }

        /// <summary>
        /// Tests adding into the data provider - legitimate data.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddDefinition()
        {
            // Arrange
            var definition = new ExtractDefinitionDTO()
            {
                ExtractDefinition = File.ReadAllText(@"Resources/FullPaymentHierarchyDefinition.xml"),
                ExtractName = "Full Payment Hierarchy Definition",
                ExtractDefinitionId = -1
            };

            // Act
            extractDefinitionService.AddDefinition(definition);

            // Assert : Our ID has changed, and is > 0.
            Assert.IsTrue(definition.ExtractDefinitionId > 0);

            // Cleanup
            extractDefinitionService.RemoveDefinition(definition);
        }

        /// <summary>
        /// Tests removing a definition from the data provider.
        /// </summary>
        [TestMethod, Ignore]
        public void TestRemoveDefinition()
        {
            // Arrange
            var definition = new ExtractDefinitionDTO()
            {
                ExtractDefinition = File.ReadAllText(@"Resources/FullPaymentHierarchyDefinition.xml"),
                ExtractName = "Sample Payment Definition Test Removal",
                ExtractDefinitionId = -1
            };

            // Act on Addition
            extractDefinitionService.AddDefinition(definition);

            // Assert our test data is ready.
            Assert.IsTrue(extractDefinitionService.GetAllDefinitions().Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId));

            // Act on Removal
            extractDefinitionService.RemoveDefinition(definition);

            // Assert it's gone.
            Assert.IsFalse(extractDefinitionService.GetAllDefinitions().Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId));
        }

        /// <summary>
        /// Tests updating to the provider.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateDefinition()
        {
            // Arrange
            var definition = new ExtractDefinitionDTO()
            {
                ExtractDefinition = File.ReadAllText(@"Resources/FullPaymentHierarchyDefinition.xml"),
                ExtractName = "Sample Payment Definition Test Removal",
                ExtractDefinitionId = -1
            };

            // Act on Addition
            extractDefinitionService.AddDefinition(definition);

            var newname = "Banana";

            // Assert our test data is ready.
            Assert.IsTrue(extractDefinitionService.GetAllDefinitions().Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId));
            Assert.IsFalse(extractDefinitionService.GetAllDefinitions()
                .Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId && x.ExtractName == newname));

            // Act on Update
            definition.ExtractName = newname;
            extractDefinitionService.UpdateDefinition(definition);

            // Assert the provider updated
            Assert.IsTrue(extractDefinitionService.GetAllDefinitions()
                .Any(x => x.ExtractDefinitionId == definition.ExtractDefinitionId && x.ExtractName == newname));

            // Cleanup
            extractDefinitionService.RemoveDefinition(definition);
        }
    }
}