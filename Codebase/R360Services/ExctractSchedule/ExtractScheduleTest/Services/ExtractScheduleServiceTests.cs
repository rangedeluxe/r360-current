﻿using ExtractScheduleAPI.Services;
using ExtractScheduleCommon;
using ExtractScheduleCommon.BusinessObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose:
*
* Modification History
*
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleTest.Services
{
    /// <summary>
    /// Class tests the API Extract Schedule Services (Server-Side, after a Client Call)
    /// </summary>
    [TestClass]
    public class ExtractScheduleServiceTests
    {
        private readonly IExtractScheduleProvider extractScheduleService;
        private readonly IExtractDefinitionProvider extractDefinitionService;

        public ExtractScheduleServiceTests()
        {
            // Arrange
			extractScheduleService = new DatabaseExtractScheduleProvider(R360ServiceContext.Current);	// TODO: Impersonate SiteKey: Constants.DEFAULT_CONFIGURATION_SITEKEY
			extractDefinitionService = new DatabaseExtractDefinitionProvider(R360ServiceContext.Current); // TODO: Impersonate SiteKey: Constants.DEFAULT_CONFIGURATION_SITEKEY
        }

        /// <summary>
        /// Tests the data provider, makes sure we are getting something back.
        /// </summary>
        [TestMethod, Ignore]
        public void TestGetAllSchedules()
        {
            // Act
            var scheds = extractScheduleService.GetAllSchedules();

            // Assert
            Assert.IsTrue(scheds.Any());
        }

        /// <summary>
        /// Tests adding into the data provider - legitimate data.
        /// </summary>
        [TestMethod, Ignore]
        public void TestAddSchedule()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = extractDefinitionService.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010011",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0)
            };

            // Act
            extractScheduleService.AddSchedule(schedule);

            // Assert : Our ID has changed, and is > 0.
            Assert.IsTrue(schedule.ExtractScheduleId > 0);

            // Cleanup
            extractScheduleService.RemoveSchedule(schedule);
        }

        /// <summary>
        /// Tests removing a schedule from the data provider.
        /// </summary>
        [TestMethod, Ignore]
        public void TestRemoveSchedule()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = extractDefinitionService.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010011",
                Description = "Test Schedule",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0)
            };

            // Act on Addition
            extractScheduleService.AddSchedule(schedule);

            // Assert our test data is ready.
            Assert.IsTrue(extractScheduleService.GetAllSchedules().Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId));

            // Act on Removal
            extractScheduleService.RemoveSchedule(schedule);

            // Assert it's gone.
            Assert.IsFalse(extractScheduleService.GetAllSchedules().Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId));
        }

        /// <summary>
        /// Tests updating to the provider.
        /// </summary>
        [TestMethod, Ignore]
        public void TestUpdateSchedule()
        {
            // Arrange
            var schedule = new ExtractScheduleDTO()
            {
                ExtractDefinitionId = extractDefinitionService.GetAllDefinitions()
                    .First()
                    .ExtractDefinitionId,
                DaysOfWeek = "0010011",
                Description = "Test Schedule 123",
                ExtractRunArguments = "/p:02/28/2006",
                ExtractScheduleId = -1,
                ScheduleTime = new TimeSpan(0, 0, 0)
            };

            // Act on Addition
            extractScheduleService.AddSchedule(schedule);

            var newname = "Banana";

            // Assert our test data is ready.
            Assert.IsTrue(extractScheduleService.GetAllSchedules().Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId));
            Assert.IsFalse(extractScheduleService.GetAllSchedules()
                .Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId && x.Description == newname));

            // Act on Update
            schedule.Description = newname;
            extractScheduleService.UpdateSchedule(schedule);

            // Assert the provider updated
            Assert.IsTrue(extractScheduleService.GetAllSchedules()
                .Any(x => x.ExtractScheduleId == schedule.ExtractScheduleId && x.Description == newname));

            // Cleanup
            extractScheduleService.RemoveSchedule(schedule);
        }
    }
}