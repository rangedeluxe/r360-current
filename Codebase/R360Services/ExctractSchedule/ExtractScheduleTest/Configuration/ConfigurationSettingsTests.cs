﻿using ExtractScheduleCommon.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 03/26/2014
*
* Purpose: 
*
* Modification History
* 
* WI 134263 BLR 03/26/2014
*   - Initial release version.
*******************************************************************************/

namespace ExtractScheduleTest.Configuration
{
    [TestClass]
    public class ConfigurationSettingsTests
    {
        private readonly IConfigurationProvider configurationProvider;
        private BaseLogSettings settings;
        public ConfigurationSettingsTests()
        {
            configurationProvider = new MockConfigurationProvider();
        }

        /// <summary>
        /// Tests a series of valid data. Test should pass.
        /// </summary>
        [TestMethod]
        public void TestValidConfigurationSettings()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert nothing thrown
                settings.ValidateSettings();
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// Tests null data, exceptions should be thrown.
        /// </summary>
        [TestMethod]
        public void TestNullConfigurationSettings()
        {
            // Arrange
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LogFilePath settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLogFilePath1()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", null);
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LogFilePath settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLogFilePath2()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", string.Empty);
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LogFilePath settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLogFilePath3()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", "#$%^&*(OL<MGYUIOL>:{_");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LogFilePath settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLogFilePath4()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"M:\INVALID\FOLDER\PATH");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LogFileMaxSize settings.
        /// </summary>
        [TestMethod]
        public void TestFailingMaxFileSize1()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", null);
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LogFileMaxSize settings.
        /// </summary>
        [TestMethod]
        public void TestFailingMaxFileSize2()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", string.Empty);
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LogFileMaxSize settings.
        /// </summary>
        [TestMethod]
        public void TestFailingMaxFileSize3()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "NOTANUMBER");
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LogFileMaxSize settings.
        /// </summary>
        [TestMethod]
        public void TestFailingMaxFileSize4()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "-1");
            configurationProvider.SetSetting("LoggingDepth", "2");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LoggingDepth settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLoggingDepth1()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", null);
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LoggingDepth settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLoggingDepth2()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", string.Empty);
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LoggingDepth settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLoggingDepth3()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "Garbage String");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LoggingDepth settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLoggingDepth4()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "-1");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LoggingDepth settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLoggingDepth5()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "3");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }

        /// <summary>
        /// Tests garbage LoggingDepth settings.
        /// </summary>
        [TestMethod]
        public void TestFailingLoggingDepth6()
        {
            // Arrange
            configurationProvider.SetSetting("LogFilePath", @"C:\Data\Logs\{0:yyyyMMdd}_ExtractScheduleServiceLog.txt");
            configurationProvider.SetSetting("LogFileMaxSize", "10240");
            configurationProvider.SetSetting("LoggingDepth", "10");
            settings = new BaseLogSettings(configurationProvider);

            // Act
            try
            {
                // Assert ConfigurationException thrown
                settings.ValidateSettings();
                Assert.Fail();
            }
            catch (ConfigurationException)
            {
                Assert.IsTrue(true);
            }
        }
    }
}