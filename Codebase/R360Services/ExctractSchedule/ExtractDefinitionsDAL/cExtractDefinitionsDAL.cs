﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using System.Data.SqlClient;
using System.Xml;
/*******************************************************************************
*
*    Module: cExtractDefinitionsDAL
*  Filename: cExtractDefinitionsDAL.cs
*    Author: Eric Smasal
*
* Revisions:
*
* ----------------------
* CR 97662 EAS 05/23/2013
*   -Initial release version.
* WI 109747 EAS 07/30/2013
*   DAL components called with RecHubAdmin Connection Type
* WI 138899 DJW 5/8/2014
* WI 140642 DJW 5/8/2014
* 	Added support for ExtractBilling and storing the original Extract name.
* WI 141347 DJS 5/13/2014
*  Added the overloaded methods to keep cExtractDefinitionsDAL compatible with OLAdmin
* WI 146351 DJW 623/2014
*  Added Logging/Auditing support.
* WI 158888 BLR 08/15/2014
*  Altered the DownloadXML sproc a bit to support audits. 
******************************************************************************/
namespace WFS.RecHub.DAL
{
    public class cExtractDefinitionsDAL : _DALBase
    {
        #region Member Variables

        #region Constants

        const string db_Schema = "RecHubConfig";
        const string extracts_GetAfterModDate = "usp_ExtractDefinitions_GetAfterModDate";
        const string extracts_GetDefinitions = "usp_ExtractDefinitions_Get";
        const string extracts_GetByExtractDefID = "usp_ExtractDefinitions_Get_ByExtractDefID";
        const string extracts_UpdateExtractDefFile = "usp_ExtractDefinitions_Upd_ExtractDefFile";
        const string extracts_UpdateExtractName = "usp_ExtractDefinitions_Upd_ExtractName";
        const string extracts_InsertExtractDefFile = "usp_ExtractDefinitions_Ins_ExtractDefFile";
        const string extracts_DeleteByExtractDefID = "usp_ExtractDefinitions_Del_ByExtractDefID";
        const string procEventAuditIns = "RecHubCommon.usp_WFS_EventAudit_Ins";

        #endregion

        #endregion

        #region Constructors

        public cExtractDefinitionsDAL(string vSiteKey)
            : base(vSiteKey)
        {
        }

        public cExtractDefinitionsDAL(string vSiteKey, ConnectionType enmConnection)
            : base(vSiteKey, enmConnection) { }

        #endregion

        #region Methods

        #region Public Methods

        #region Extract Retrieval

        public bool GetExtractDefinitions(DateTime? UpdatedSince, out DataTable results)
        {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            arParms = new List<SqlParameter>();
            if (UpdatedSince != null)
                arParms.Add(BuildParameter("@modifiedAfter", SqlDbType.DateTime, UpdatedSince, ParameterDirection.Input));


            parms = arParms.ToArray();
            bolRetVal = Database.executeProcedure(db_Schema + "." + extracts_GetAfterModDate, parms, out results);
            return bolRetVal;
        }

        public bool GetExtractDefinitionsAll(int startRecord, int maxRows, out int totalRows, out DataTable dt)
        {
            bool bRtnval = false;
            dt = null;
            totalRows = 0;
            SqlParameter[] parms;
            SqlParameter parmTotalRecords;

            List<SqlParameter> arParms = new List<SqlParameter>();
            arParms.Add(BuildParameter("@parmStartRecord", SqlDbType.Int, startRecord, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmRecordsPerPage", SqlDbType.Int, maxRows, ParameterDirection.Input));
            parmTotalRecords = BuildParameter("@parmTotalRecords", SqlDbType.Int, 0, ParameterDirection.Output);
            arParms.Add(parmTotalRecords);

            parms = arParms.ToArray();

            try
            {
                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_GetDefinitions, parms, out dt);

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetExtractData(out DataTable dt)");
            }

            if (bRtnval)
            {
                int result;
                if (int.TryParse(parmTotalRecords.Value.ToString(), out result))
                    totalRows = result;
            }

            return bRtnval;
        }

        public bool GetExtractDefinitionFile(int extractID, int userid, out string extractName, out string xmlString)
        {
            bool bRtnval = false;
            xmlString = "";
            SqlParameter[] parms;
            SqlParameter parmExtractName;

            extractName = null;

            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userid, ParameterDirection.Input));
                parmExtractName = BuildParameter("@parmExtractName", SqlDbType.VarChar, null, ParameterDirection.Output);
                arParms.Add(parmExtractName);

                parms = arParms.ToArray();
                DataTable dt;
                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_GetByExtractDefID, parms, out dt);

                if (bRtnval)
                {
                    extractName = (string)dt.Rows[0]["ExtractFileName"];
                    xmlString=(string)dt.Rows[0]["ExtractDefinition"];
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetExtractDefinition(int extractID, out XmlDocument xmlDocument)");
            }

            return bRtnval;
        }

        #endregion

        #region Extract Update
        /// <summary>
        /// Added for OLAdmin support
        /// </summary>
        /// <param name="extractID"></param>
        /// <param name="fileSize"></param>
        /// <param name="extractDefinitionXml"></param>
        /// <returns></returns>
        public bool UpdateExtractDefinitionFile(int extractID, int fileSize, string extractDefinitionXml, int userId)
        {
            return UpdateExtractDefinitionFile(extractID, fileSize, extractDefinitionXml, string.Empty, 1, userId);
        }
        public bool UpdateExtractDefinitionFile(int extractID, int fileSize, string extractDefinitionXml, string fileName, int extractType, int userId)
        {
            bool bRtnval = false;
            SqlParameter[] parms;

            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractDefSizeKb", SqlDbType.Int, fileSize, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractDefinition", SqlDbType.VarChar, fileSize, extractDefinitionXml));
                arParms.Add(BuildParameter("@parmExtractFilename", SqlDbType.VarChar, fileName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractDefinitionType", SqlDbType.Int, extractType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));

                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_UpdateExtractDefFile, parms);

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateExtractDefinitionFile(int extractID, int fileSize, string extractDefinitionXml, string fileName, int extractType)");
            }

            WriteAuditEvent("Extract Config Manager", "Extract Definition File Updated", userId, "RecHubConfig", "ExtractDefinitions", "ExtractDefinitionID", extractID.ToString(), "UPDATE Extract Definition ID " + extractID.ToString());

            return bRtnval;
        }

        public bool UpdateExtractNamesBatch(DataTable dt, int userId)
        {
            bool bRtnval = false;

            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    UpdateExtractName((int)row["ExtractDefID"], row["ExtractName"].ToString(), userId);

                    WriteAuditEvent("Extract Config Manager", "Extract Definition Updated", 0, "RecHubConfig", "ExtractDefinitions", "ExtractName", row["ExtractName"].ToString(), "UPDATE Extract Name: " + row["ExtractName"].ToString());
                }

                bRtnval = true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateExtractNamesBatch(DataTable dt)");
            }

            return bRtnval;
        }

        public bool UpdateExtractName(int extractID, string extractName, int userId)
        {
            bool bRtnval = false;
            SqlParameter[] parms;

            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractName", SqlDbType.VarChar, extractName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_UpdateExtractName, parms);

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "UpdateExtractName(int extractID, string extractName)");
            }

            return bRtnval;
        }

        #endregion

        #region Extract Insert
        /// <summary>
        /// Added for OLAdmin support
        /// </summary>
        /// <param name="name"></param>
        /// <param name="fileSize"></param>
        /// <param name="extractDefinitionXml"></param>
        /// <returns></returns>
        public bool AddExtractDefinitionFile(string name, int fileSize, string extractDefinitionXml, int userId)
        {
            int insertedId = 0;
            return AddExtractDefinitionFile(name, fileSize, extractDefinitionXml, 1, string.Empty, userId, out insertedId);

        }
        public bool AddExtractDefinitionFile(string name, int fileSize, string extractDefinitionXml, int extractDefinitionType, string extractFilename, int userId, out  int insertedId)
        {
            bool bRtnval = false;
            SqlParameter[] parms;
            SqlParameter parmInsertedID;
            insertedId = 0;
            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmExtractDefSizeKb", SqlDbType.Int, fileSize, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractDefinition", SqlDbType.VarChar, fileSize, extractDefinitionXml));
                arParms.Add(BuildParameter("@parmExtractName", SqlDbType.VarChar, name, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractDefinitionType", SqlDbType.Int, extractDefinitionType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmExtractFilename", SqlDbType.VarChar, extractFilename, ParameterDirection.Input));
                parmInsertedID = BuildParameter("@parmExtractInsertedID", SqlDbType.Int, null, ParameterDirection.Output);
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
                arParms.Add(parmInsertedID);

                parms = arParms.ToArray();

                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_InsertExtractDefFile, parms);
                if (parmInsertedID.Value != null)
                {
                    int.TryParse(parmInsertedID.Value.ToString(), out insertedId );
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "AddExtractDefinitionFile(string name, int fileSize, string extractDefinitionXml, int extractDefinitionType, string extractFilename, out  int insertedId)");
            }

            if (bRtnval)
            {
                WriteAuditEvent("Extract Config Manager", "Extract Definition Inserted", userId, "RecHubConfig", "ExtractDefinitions", "ExtractName", name, "INSERT Extract Definition.  Name: " + name);
            }

            return bRtnval;
        }

        #endregion

        #region Extract Delete

        public bool DeleteExtractDefition(int extractID, int userId)
        {
            bool bRtnval = false;
            SqlParameter[] parms;

            try
            {
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));

                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_DeleteByExtractDefID, parms);

            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "DeleteExtractDefition(int extractID)");
            }

            if (bRtnval)
            {
                WriteAuditEvent("Extract Config Manager", "Extract Definition Deleted", userId, "RecHubConfig", "ExtractDefinitions", "ExtractDefinitionID", extractID.ToString(), "DELETE Extract Definition ID " + extractID.ToString());
            }

            return bRtnval;
        }

        #endregion

        #region Audits

        public bool WriteAuditEvent(string applicationName, string auditType, int userID, string schemaName, string tableName, string columnName, string auditValue, string auditMessage)
        {
            bool bRetVal = false;
            List<SqlParameter> arParms = new List<SqlParameter>();

            try
            {
                arParms.Add(BuildParameter("@parmApplicationName", SqlDbType.VarChar, applicationName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEventName", SqlDbType.VarChar, auditType, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmAuditMessage", SqlDbType.VarChar, auditMessage, ParameterDirection.Input));

                SqlParameter[] parms = arParms.ToArray();

                bRetVal = Database.executeProcedure(procEventAuditIns, parms);
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Unable to execute procedure: " + procEventAuditIns, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bRetVal = false;
            }
            return bRetVal;
        }

        #endregion

        #endregion

        #endregion

    }
}
