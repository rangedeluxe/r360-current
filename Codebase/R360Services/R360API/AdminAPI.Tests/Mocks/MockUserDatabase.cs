﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using ApiModels;
using APIDataAccess;

namespace AdminAPI.Tests.Mocks
{
    [ExcludeFromCodeCoverage]
    public class MockUserDatabase : IUserDatabase
    {
        public Task<UserModel> GetUserBySessionIdAsync(Guid sessionId)
        {
           return Task.Run(() => new UserModel { UserId = 1, LogonName = "TestUser" });
        }
    }
}
