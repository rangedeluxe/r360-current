﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using ApiModels;
using APIDataAccess;

namespace PayerAPI.Tests.Mocks
{
    [ExcludeFromCodeCoverage]
    public class MockPayerDatabase : IPayerDatabase
    {
        private int mockUserId = 0;
        private string mockUserName = "TestUser";

        // Testable cases for GetPayerAsync:
        //  1.  Passing bankId=99999 and clientAccountId=999 with any routingNumber/account values triggers an exception
        //      to test exception handling code.
        //  2.  Passing bankId=99999 and clientAccountId=888 with any routingNumber/account values returns a null, indicating no data
        //      retrieved.
        //  3.  Passing any other bankId/clientAccountId with routingNumber=null and account=null returns a 2-item list of sample
        //      payer objects for the given bankId/clientAccountId.
        //  4.  Passing any other bankId/clientAccountId/routingNumber/account specific combination returns a 1-item list of a sample
        //      payer object using the specified bankId/clientAccountId/routingNumber/account.
        public async Task<IEnumerable<PayerModel>> GetPayerAsync(int userId, int bankId, int clientAccountId, string routingNumber = null, string account = null)
        {
            // Using bank=99999 and client=999 and RT="999999999" and account="999999" triggers exception
            if ((bankId == 99999) && (clientAccountId == 999))
                throw new NotImplementedException();
            // Using bank=99999 and client=999 triggers "no data" response
            if ((bankId==99999) && (clientAccountId==888))
                return null;
            // Using null RT and account, return sample data
            if ((routingNumber == null) && (account == null))
            {
                return new List<PayerModel>
                {
                    new PayerModel
                    {
                        SiteBankId = bankId,
                        SiteClientAccountId = clientAccountId,
                        RoutingNumber = "104000016",
                        Account = "12345678",
                        PayerName = "Sample Payer Name",
                        IsDefault = false,
                        CreatedBy = mockUserName,
                        CreationDate = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt"),
                        ModifiedBy = mockUserName,
                        ModificationDate = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt")
                    },
                    new PayerModel
                    {
                        SiteBankId = bankId,
                        SiteClientAccountId = clientAccountId,
                        RoutingNumber = "104000029",
                        Account = "87654321",
                        PayerName = "Sample Payer Name 2",
                        IsDefault = false,
                        CreatedBy = mockUserName,
                        CreationDate = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt"),
                        ModifiedBy = mockUserName,
                        ModificationDate = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt")
                    }
                    
                };
            }
            // Otherwise, return a one-item list with just the specified info plus a test name
            return new List<PayerModel>
            {
                new PayerModel
                {
                    SiteBankId = bankId,
                    SiteClientAccountId = clientAccountId,
                    RoutingNumber = routingNumber,
                    Account = account,
                    PayerName = "Sample Payer Name",
                    IsDefault = false,
                    CreatedBy = mockUserName,
                    CreationDate = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt"),
                    ModifiedBy = mockUserName,
                    ModificationDate = DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt")
                }
            };
        }

        // Testable cases:
        //  1.  Passing payer.SiteBankId=99999 and payer.SiteClientAccountId=999 triggers an exception
        //  2.  Passing any other values results in success.
        public async Task UpsertPayerAsync(int userId, PayerModel payer)
        {
            if ((payer.SiteBankId== 99999) && (payer.SiteClientAccountId==999))
                throw new NotImplementedException();
        }

        // Testable cases:
        //  1.  Passing bankId=99999 and siteClientAccountId=999 triggers an exception
        //  2.  Passing any other values results in success.
        public async Task DeletePayerAsync(int userId, int bankId, int clientAccountId, string routingNumber, string account)
        {
            if ((bankId == 99999) && (clientAccountId == 999))
                throw new NotImplementedException();
        }

        public async Task<string> GetBankNameAsync(int bankId)
        {
            return "Bank Name";
        }

        public async Task<string> GetClientAccountNameAsync(int bankId, int clientAccountId)
        {
            return "Client Account Name";
        }

        public async Task<UserModel> GetUserBySessionIdAsync(Guid sessionId)
        {
            return new UserModel()
            {
                UserId = mockUserId,
                LogonName = mockUserName,
                CreatedBy = mockUserName,
                CreationDate = DateTime.Now,
                ModifiedBy = mockUserName,
                ModificationDate = DateTime.Now
            };
        }
    }
}
