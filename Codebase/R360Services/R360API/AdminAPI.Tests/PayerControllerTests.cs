using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNetCore.Mvc;
using ApiCommon.Models;
using ApiCommon.Tests.Mocks;
using ApiModels;
using AdminAPI.Tests.Mocks;
using AdminAPI.Controllers;
using PayerAPI.Tests.Mocks;

namespace PayerAPI.Tests
{
    [TestClass]
    public class PayerControllerTests
    {
        private MockPayerDatabase _db;
        private MockUserDatabase _userDB;
        private MockLogger<PayersController> _log;
        private MockClaimLookupHelper _claimLookupHelper;
        private PayersController _payersController;
        private PageViewRequestModel _payerRequest;

        [TestInitialize]
        public void Init()
        {
            _db = new MockPayerDatabase();
            _userDB = new MockUserDatabase();
            _log = new MockLogger<PayersController>();
            _claimLookupHelper = new MockClaimLookupHelper();
            _payersController = new PayersController(_log, _db, _userDB, _claimLookupHelper);
            _payerRequest = new PageViewRequestModel() { isPageView = false };
        }

        [TestMethod]
        public void CanGetPayerListForClient()
        {
            var response = (ServiceResponse)((ObjectResult)_payersController.Get(0,0,_payerRequest).Result).Value;
            var workgroupPayers = (WorkgroupPayersModel)response.Data;
            Assert.IsTrue(workgroupPayers.Payers.ToList().Count > 0);
        }

        [TestMethod]
        public void CanGetPayerByRTAccount()
        {
            var response = (ServiceResponse)((ObjectResult)_payersController.Get(0, 0, "104000016", "123456", _payerRequest).Result).Value;
            var workgroupPayers = (WorkgroupPayersModel)response.Data;
            Assert.IsTrue(workgroupPayers.Payers.ToList().Count == 1);
        }

        [TestMethod]
        public void CanHandlePayerRequestValidationErrors()
        {
            ServiceResponse response;

            response = (ServiceResponse)((ObjectResult)_payersController.Get(-1, -1, _payerRequest).Result).Value;
            Assert.IsTrue(response.Errors.Any());

            response = (ServiceResponse)((ObjectResult)_payersController.Get(-1,-1,null,null, _payerRequest).Result).Value;
            Assert.IsTrue(response.Errors.Any());

            response = (ServiceResponse)((ObjectResult)_payersController.Get(0,0, "104000016",null, _payerRequest).Result).Value;
            Assert.IsTrue(response.Errors.Any());

            response = (ServiceResponse)((ObjectResult)_payersController.Get(0,0,null,"123456", _payerRequest).Result).Value;
            Assert.IsTrue(response.Errors.Any());
        }

        [TestMethod]
        public void CanHandleGetException()
        {
            ServiceResponse response;
            response = (ServiceResponse)((ObjectResult)_payersController.Get(99999,999, _payerRequest).Result).Value;
            Assert.IsTrue(response.Errors.Any());
            response = (ServiceResponse)((ObjectResult)_payersController.Get(99999, 999, "999999999", "999999", _payerRequest).Result).Value;
            Assert.IsTrue(response.Errors.Any());
        }

        [TestMethod]
        public void CanUpsertPayer()
        {
            var payer = new PayerModel
            {
                SiteBankId = 0,
                SiteClientAccountId = 0,
                RoutingNumber = "104000016",
                Account = "123456",
                PayerName = "Payer Name Test",
                IsDefault = true
            };
            var response = (ServiceResponse)((ObjectResult)_payersController.Post(payer).Result).Value;
            Assert.IsTrue(response.Success);
        }

        [TestMethod]
        public void CanHandleUpsertInvalid()
        {
            // First test - invalid bank
            var payer = new PayerModel
            {
                SiteBankId = -1,
                SiteClientAccountId = 0,
                RoutingNumber = "104000016",
                Account = "123456",
                PayerName = "Payer Name Test",
                IsDefault = true
            };
            var response = (ServiceResponse)((ObjectResult)_payersController.Post(payer).Result).Value;
            Assert.IsFalse(response.Success);

            // Next test - invalid workgroup
            payer.SiteBankId = 0;
            payer.SiteClientAccountId = -1;
            response = (ServiceResponse)((ObjectResult)_payersController.Post(payer).Result).Value;
            Assert.IsFalse(response.Success);

            // Next test - invalid routing number
            payer.SiteClientAccountId = 0;
            payer.RoutingNumber = string.Empty;
            response = (ServiceResponse)((ObjectResult)_payersController.Post(payer).Result).Value;
            Assert.IsFalse(response.Success);

            // Next test - invalid account number
            payer.RoutingNumber = "104000016";
            payer.Account = string.Empty;
            response = (ServiceResponse)((ObjectResult)_payersController.Post(payer).Result).Value;
            Assert.IsFalse(response.Success);

            // Next test - invalid "modified by" name
            payer.Account = "123456";
            var saveName = _claimLookupHelper.name;
            _claimLookupHelper.name = string.Empty;
            response = (ServiceResponse)((ObjectResult)_payersController.Post(payer).Result).Value;
            _claimLookupHelper.name = saveName;
            Assert.IsFalse(response.Success);
        }

        [TestMethod]
        public void CanHandleUpsertException()
        {
            var payer = new PayerModel
            {
                SiteBankId = 99999,
                SiteClientAccountId = 999,
                RoutingNumber = "104000016",
                Account = "123456",
                PayerName = string.Empty,
                IsDefault = true
            };
            var response = (ServiceResponse)((ObjectResult)_payersController.Post(payer).Result).Value;
            Assert.IsTrue(response.Errors.Any());
        }

        [TestMethod]
        public void CanDeletePayer()
        {
            var response = (ServiceResponse)((ObjectResult)_payersController.Delete(0, 0, string.Empty, string.Empty).Result).Value;
            Assert.IsTrue(response.Success);
        }

        [TestMethod]
        public void CanHandleDeleteException()
        {
            var response = (ServiceResponse)((ObjectResult)_payersController.Delete(99999,999,string.Empty,string.Empty).Result).Value;
            Assert.IsTrue(response.Errors.Any());
        }
    }
}
