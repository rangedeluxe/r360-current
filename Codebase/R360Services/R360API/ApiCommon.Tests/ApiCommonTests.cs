﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using ApiCommon.Tests.Mocks;
using ApiCommon.DataProviders;
using ApiCommon.Models;
using ApiCommon.Helpers;
using ApiModels;

namespace PayerAPI.Tests
{
    [TestClass]
    public class ApiCommonTests
    {
        private IConfiguration _configuration = null;
        private AppSettings _appSettings;
        private IUserDataProvider _dp = null;
        private MockUserDataProvider _mdp = null;
        private const string _testName = "Test Name";

        [TestInitialize]
        public void Init()
        {
            _configuration = (new ConfigurationBuilder().AddJsonFile("appsettings.json")).Build();
            _appSettings = _configuration.Get<AppSettings>();
            _dp = new UserDataProvider(new MockLogger<UserDataProvider>(), _appSettings,new MockUserDataAccess());
            _mdp = new MockUserDataProvider(new MockLogger<MockUserDataProvider>(), _appSettings);
        }

        [TestMethod]
        public void CanGenerateToken()
        {
            // Note that this "test" is really here only to allow for generation of a token if needed for development testing.
            //  It will always succeed.
            string token = string.Empty;
            try
            {
                token = _mdp.GetTestToken();
            }
            catch (Exception)
            {
                token = string.Empty;
            }
            Assert.IsTrue(token != null);
        }

        [TestMethod]
        public void TestWorkgroupRightsHasValues()
        {
            var workgroupRights = new WorkgroupRights() { SiteBankId = "99", SiteClientAccountId = "999" };
            Assert.IsTrue(workgroupRights.HasValues());
            Assert.IsTrue(workgroupRights.SiteBankId == "99");
            Assert.IsTrue(workgroupRights.SiteClientAccountId == "999");
        }

        [TestMethod]
        public void TestClaimLookupHelper()
        {
            var httpContext = new DefaultHttpContext();
            var identity = new ClaimsIdentity();
            identity.AddClaim(new Claim("name", _testName));
            httpContext.User.AddIdentity(identity);

            var helper = new ClaimLookupHelper();
            var claimValue = helper.LookupClaim(httpContext, "name");
            Assert.IsTrue(claimValue == _testName);
            claimValue = helper.LookupClaim(httpContext, "xxx");
            Assert.IsTrue(string.IsNullOrEmpty(claimValue));
        }

        [TestMethod]
        public void TestPayerModel()
        {
            var payer = new PayerModel()
            {
                SiteBankId = 0,
                SiteClientAccountId = 0,
                RoutingNumber = "104000016",
                Account = "123456",
                PayerName = _testName,
                IsDefault = false,
                CreatedBy = _testName,
                CreationDate = "12/31/2018 05:00:00 PM",
                ModifiedBy = _testName,
                ModificationDate = "12/31/2018 05:00:00 PM"
            };
            Assert.IsTrue(payer.SiteBankId == 0);
            Assert.IsTrue(payer.SiteClientAccountId == 0);
            Assert.IsTrue(!string.IsNullOrEmpty(payer.RoutingNumber));
            Assert.IsTrue(!string.IsNullOrEmpty(payer.Account));
            Assert.IsTrue(payer.PayerName == _testName);
            Assert.IsTrue(payer.CreatedBy == _testName);
            Assert.IsTrue(payer.ModifiedBy == _testName);
            payer.FormatDates("MM-dd-yyyy hh:mm:ss tt");
            Assert.IsTrue(payer.CreationDate == "12-31-2018 05:00:00 PM");
            Assert.IsTrue(payer.ModificationDate == "12-31-2018 05:00:00 PM");
        }

        [TestMethod]
        public void TestUserModel()
        {
            var user = new UserModel()
            {
                UserId = 0,
                LogonName = _testName,
                CreationDate = DateTime.Now,
                CreatedBy = _testName,
                ModificationDate = DateTime.Now,
                ModifiedBy = _testName
            };
            Assert.IsTrue(user.UserId == 0);
            Assert.IsTrue(user.LogonName == _testName);
            Assert.IsTrue(user.CreatedBy == _testName);
            Assert.IsTrue(user.ModifiedBy == _testName);
            Assert.IsTrue(user.CreationDate.HasValue && user.ModificationDate.HasValue);
        }

        [TestMethod]
        public void TestWorkgroupPayersModel()
        {
            var workgroupPayer = new WorkgroupPayersModel()
            {
                SiteBankId = 0,
                SiteBankName = _testName,
                SiteClientAccountId = 0,
                SiteClientAccountName = _testName
            };
            Assert.IsTrue(workgroupPayer.SiteBankId == 0);
            Assert.IsTrue(workgroupPayer.SiteClientAccountId == 0);
            Assert.IsTrue(workgroupPayer.SiteBankName == _testName);
            Assert.IsTrue(workgroupPayer.SiteClientAccountName == _testName);
        }

        [TestMethod]
        public void TestUserDataProvider()
        {
            var workgroupRights = new WorkgroupRights();
            var claimsPrincipal = new ClaimsPrincipal();
            var identity = new ClaimsIdentity();

            // This test should throw exception in mock, which HasRights() should catch and return false
            identity.AddClaim(new Claim("sid", "B1EC87B1-91FA-4F28-AB02-CFA401378774"));
            claimsPrincipal.AddIdentity(identity);
            workgroupRights.BankId = "99";
            workgroupRights.WorkgroupId = "999";
            Assert.IsFalse(_dp.HasRights(claimsPrincipal, workgroupRights));

            // This test should work successfully
            workgroupRights.BankId = "0";
            workgroupRights.WorkgroupId = "0";
            Assert.IsTrue(_dp.HasRights(claimsPrincipal, workgroupRights));

            // This test should fail because the sessionid is blank
            claimsPrincipal = new ClaimsPrincipal();
            identity = new ClaimsIdentity();
            identity.AddClaim(new Claim("sid", string.Empty));
            claimsPrincipal.AddIdentity(identity);
            Assert.IsFalse(_dp.HasRights(claimsPrincipal, workgroupRights));
        }
    }
}
