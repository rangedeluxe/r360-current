﻿using System;
using System.Security.Claims;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;
using ApiCommon;
using ApiCommon.Abstractions;
using ApiCommon.DataProviders;
using ApiCommon.Models;

namespace ApiCommon.Tests.Mocks
{
    [ExcludeFromCodeCoverage]
    public class MockUserDataProvider : IAuthenticator, IAuthorizer<WorkgroupRights>, IUserDataProvider
    {
        private readonly ILogger<MockUserDataProvider> _logger;
        private readonly AppSettings _appSettings;

        public MockUserDataProvider()
        {
        }

        public MockUserDataProvider(ILogger<MockUserDataProvider> logger, AppSettings appSettings)
        {
            _logger = logger;
            _appSettings = appSettings;
        }

        public TokenClaims Authenticate()
        {
            return new TokenClaims
            {
                CredentialsValidated = true,
                SessionId = "7f3a15ac-929d-4a70-bfb9-bc9240e12ea3",
                EntityId = "WFS",
                UserId = "1",
                UserFullName = "Test FullName",
                UserLogonName = "testname"
            };
        }

        public bool HasRights(ClaimsPrincipal claimsPrincipal, WorkgroupRights rightsContext)
        {
            return true;
        }

        public string GetTestToken()
        {
            return Extensions.GenerateToken(this, _appSettings);
        }

    }
}
