﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Http;
using ApiCommon.Helpers;

namespace ApiCommon.Tests.Mocks
{
    [ExcludeFromCodeCoverage]
    public class MockClaimLookupHelper : IClaimLookupHelper
    {
        public string name = "Test Name";
        public string uid = "1";
        public string sid = "C472CF7C-64F2-4350-ABD7-21AAB31CC099";

        public MockClaimLookupHelper()
        {
        }

        public MockClaimLookupHelper(string Name, string Uid, string Sid)
        {
            name = Name;
            uid = Uid;
            sid = Sid;
        }

        public string LookupClaim(HttpContext httpContext, string claimName)
        {
            switch (claimName.ToLower())
            {
                case "sub":
                case "name":
                    return name;
                case "uid":
                    return uid;
                case "sid":
                case "sessionid":
                    return sid;
            }
            return string.Empty;
        }
    }
}
