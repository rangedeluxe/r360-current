﻿using System;
using ApiCommon.DataProviders;

namespace ApiCommon.Tests.Mocks
{
    public class MockUserDataAccess : IUserDataAccess
    {
        public bool HasWorkgroupEntitlement(string sessionId, int bankId, int workgroupId)
        {
            if ((bankId == 99) && (workgroupId == 999))
                throw new NotImplementedException();
            return true;
        }
    }
}
