﻿using APIDataAccess;
using ApiModels;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdminApi.xTests.Mocks
{
    public class BanksMock : Mock<IBankDatabase>
    {
        public void SetupAddReturns(int bankId, string bankName)
        {
            Setup(x => x.GetBank(It.IsAny<int>())).ReturnsAsync(new BankModel
            {
                BankId = bankId, 
                BankName = bankName
            }
            );
        }
    }
}
