using AdminAPI.Controllers;
using ApiCommon.Helpers;
using APIDataAccess;
using ApiModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using Xunit;

namespace AdminApi.xTest
{
    public class OptionalSettingsTests
    {
        private ILogger<OptionalSettingsController> Logger => new Mock<ILogger<OptionalSettingsController>>().Object;

        private IWorkgroupDatabase MockWorkgroupRepo
        {
            get
            {
                var mockRepo = new Mock<IWorkgroupDatabase>();
                mockRepo.Setup(x => x.GetOptionalSettings(It.IsAny<int>())).ReturnsAsync(new List<WorkgroupOptionalSettingsModel>
                {
                    new WorkgroupOptionalSettingsModel {
                        BankId = 1,
                        BankName = "Test Bank 1",
                        LongName = "Test Workgroup Long Name 1",
                        WorkgroupId = 1001,
                        IsActive = true
                    },
                    new WorkgroupOptionalSettingsModel {
                        BankId = 2,
                        BankName = "Test Bank 2",
                        LongName = "Test Workgroup Long Name 2",
                        WorkgroupId = 1002,
                        IsActive = true
                    },
                    new WorkgroupOptionalSettingsModel {
                        BankId = 3,
                        BankName = "Test Bank 3",
                        LongName = "Test Workgroup Long Name 3",
                        WorkgroupId = 1003,
                        IsActive = true
                    }
                });
                return mockRepo.Object;
            }
        }

        private IBankDatabase MockBankRepo
        {
            get
            {
                var mockRepo = new Mock<IBankDatabase>();
                mockRepo.Setup(x => x.GetBank(It.IsAny<int>())).ReturnsAsync(new BankModel
                {
                  BankId = 1,
                  BankName = "Test Bank 1",
                       
                });
                return mockRepo.Object;
            }
        }

        private IUserDatabase MockUserRepo
        {
            get
            {
                var mockRepo = new Mock<IUserDatabase>();
                mockRepo.Setup(x => x.GetUserBySessionIdAsync(It.IsAny<Guid>())).ReturnsAsync(new UserModel
                {
                   LogonName = "TestUser",
                   UserId = 1111
                });
                return mockRepo.Object;
            }
        }

        private IEntitiesDatabase MockEntityRepo
        {
            get
            {
                var mockRepo = new Mock<IEntitiesDatabase>();
                mockRepo.Setup(x => x.GetEntityAsync(It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(new EntityModel
                {
                    EntityID = 1,
                    EntityName = "Test Entity"
                });
                return mockRepo.Object;
            }
        }

        private IClaimLookupHelper MockClaimsRepo
        {
            get
            {
                var mockRepo = new Mock<IClaimLookupHelper>();
                mockRepo.Setup(x => x.LookupClaim(It.IsAny<HttpContext>(), JwtRegisteredClaimNames.Sid)).Returns(Guid.NewGuid().ToString());
                return mockRepo.Object;
            }
        }

        [Fact]
        public async Task TestGetOptionalSettingsByEntityId()
        {
            var controller = new OptionalSettingsController(Logger, MockEntityRepo, MockUserRepo, MockWorkgroupRepo, MockBankRepo, MockClaimsRepo);
            var result = await controller.Get(1);

            var success = Assert.IsType<OkObjectResult>(result);
            Debug.Write(success.StatusCode);
        }
    }
}
