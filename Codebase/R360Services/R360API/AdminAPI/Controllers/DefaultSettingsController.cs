﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ApiCommon;
using ApiCommon.Helpers;
using APIDataAccess;
using System.IdentityModel.Tokens.Jwt;
using ApiModels;

namespace AdminAPI.Controllers
{
    [ApiController]
    public class DefaultSettingsController : ApiBaseController
    {
        private readonly ILogger _logger;
        private readonly IEntitiesDatabase _db;
        private readonly IUserDatabase _userDb;
        private readonly IClaimLookupHelper _claimLookupHelper;

        public DefaultSettingsController(ILogger<DefaultSettingsController> logger, IEntitiesDatabase entitiesDatabase, IUserDatabase userDatabase, IClaimLookupHelper claimLookupHelper)
        {
            _logger = logger;
            _db = entitiesDatabase;
            _userDb = userDatabase;
            _claimLookupHelper = claimLookupHelper;
        }

        [Authorize]
        [HttpGet("api/entities/{entityId}/defaultsettings")]
        public async Task<ActionResult> Get(int entityId)
        {
            try
            {
                var sid = _claimLookupHelper.LookupClaim(HttpContext, JwtRegisteredClaimNames.Sid);
                var user = await _userDb.GetUserBySessionIdAsync(new Guid(sid));
                var userId =(user?.UserId ?? -1);

                EntityModel response = await _db.GetOrCreateEntityWorkgroupDefaultsAsync(entityId, userId); ;
                if( response == null)
                {
                    return BadRequest("Error getting or creating workgroup default settings");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown getting or creating workgroup default settings from database; bad request returned to caller.");
                return BadRequest("Error getting or creating workgroup default settings");
            }
        }
    }
}