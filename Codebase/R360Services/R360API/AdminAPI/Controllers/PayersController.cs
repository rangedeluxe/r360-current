﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ApiCommon;
using ApiCommon.Authorization;
using ApiCommon.Helpers;
using ApiModels;
using APIDataAccess;

namespace AdminAPI.Controllers
{
    [ApiController]
    public class PayersController : ApiBaseController
    {
        private ILogger _logger;
        private IPayerDatabase _db = null;
        private IUserDatabase _userDb = null;
        private IClaimLookupHelper _claimLookupHelper = null;

        public PayersController(ILogger<PayersController> logger, IPayerDatabase payerDatabase, IUserDatabase userDatabase,  IClaimLookupHelper claimLookupHelper)
        {
            _logger = logger;
            _db = payerDatabase;
            _userDb = userDatabase;
            _claimLookupHelper = claimLookupHelper;
        }

        [Authorize(Policy = "HasWorkgroupRights")]
        [HttpGet("api/banks/{bankId}/workgroups/{workgroupId}/payers")]
        public async Task<ActionResult>Get(int bankId, int workgroupId, [FromQuery]PageViewRequestModel requestContext)
        {
            var errors = ValidatePayerRequest(bankId,workgroupId,null,null);
            if (errors.Any())
                return (BadRequest(CreateErrorResponse(errors)));
            try
            {
                var workgroupPayers = new WorkgroupPayersModel();
                workgroupPayers.SiteBankId = bankId;
                workgroupPayers.SiteBankName = await _db.GetBankNameAsync(bankId);
                workgroupPayers.SiteClientAccountId = workgroupId;
                workgroupPayers.SiteClientAccountName = await _db.GetClientAccountNameAsync(bankId, workgroupId);
                workgroupPayers.Payers = await _db.GetPayerAsync(await GetUserIdAsync(), bankId, workgroupId, null, null);
                return Ok(CreateSuccessResponse(workgroupPayers));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown retrieving payer(s) from database; bad request returned to caller.");
                return BadRequest(CreateErrorResponse("Exception getting payer(s): " + ex.Message));
            }
        }

        [Authorize(Policy = "HasWorkgroupRights")]
        [HttpGet("api/banks/{bankId}/workgroups/{workgroupId}/payers/routing/{routingNumber}/account/{account}")]
        public async Task<ActionResult> Get(int bankId, int workgroupId, string routingNumber, string account, [FromQuery]PageViewRequestModel requestContext)
        {
            var errors = ValidatePayerRequest(bankId, workgroupId, routingNumber, account);
            if (errors.Any())
                return (BadRequest(CreateErrorResponse(errors)));

            try
            {
                var workgroupPayers = new WorkgroupPayersModel();
                workgroupPayers.SiteBankId = bankId;
                workgroupPayers.SiteBankName = await _db.GetBankNameAsync(bankId);
                workgroupPayers.SiteClientAccountId = workgroupId;
                workgroupPayers.SiteClientAccountName = await _db.GetClientAccountNameAsync(bankId, workgroupId);
                workgroupPayers.Payers = await _db.GetPayerAsync(await GetUserIdAsync(), bankId, workgroupId, routingNumber, account);
                return Ok(CreateSuccessResponse(workgroupPayers));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown retrieving payer(s) from database; bad request returned to caller.");
                return BadRequest(CreateErrorResponse("Exception getting payer(s): " + ex.Message));
            }
        }

        [Authorize(Policy = "HasWorkgroupRights")]
        [HttpPost("api/payers")]
        public async Task<ActionResult> Post([FromBody] PayerModel value)
        {
            try
            {
                value.ModifiedBy = GetUserName();
                var errors = ValidatePayerUpsert(value);
                if (errors.Any())
                    return (BadRequest(CreateErrorResponse(errors)));
                await _db.UpsertPayerAsync(await GetUserIdAsync(), value);
                return Ok(CreateSuccessResponse(value));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown adding/updating payer in database; bad request returned to caller.");
                return BadRequest(CreateErrorResponse("Exception adding/updating payer: " + ex.Message));
            }
        }

        [Authorize(Policy = "HasWorkgroupRights")]
        [HttpDelete("api/banks/{bankId}/workgroups/{workgroupId}/routing/{routingNumber}/account/{account}")]
        public async Task<ActionResult> Delete(int bankId, int workgroupId, string routingNumber, string account)
        {
            try
            {
                await _db.DeletePayerAsync(await GetUserIdAsync(), bankId, workgroupId, routingNumber, account);
                return Ok(CreateSuccessResponse(null));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown deleting payer from database; bad request returned to caller.");
                return BadRequest(CreateErrorResponse("Exception deleting payer: " + ex.Message));
            }
        }

        private IEnumerable<string> ValidatePayerRequest(int bankId, int workgroupId, string routingNumber, string account)
        {
            var errors = new List<string>();

            if (bankId < 0)
                errors.Add($"Invalid bankId {bankId}, (must be greater than or equal to zero).");

            if (workgroupId < 0)
                errors.Add($"Invalid workgroupId {workgroupId}, (must be greater than or equal to zero).");

            if (string.IsNullOrEmpty(routingNumber) && !string.IsNullOrEmpty(account))
                errors.Add("Must provide RoutingNumber if Account is supplied.");

            if (!string.IsNullOrEmpty(routingNumber) && string.IsNullOrEmpty(account))
                errors.Add("Must provide Account if RoutingNumber is supplied.");

            return errors;
        }

        private IEnumerable<string>ValidatePayerUpsert(PayerModel payer)
        {
            var errors = new List<string>();

            if (payer.SiteBankId < 0)
                errors.Add($"Invalid BankId {payer.SiteBankId}, (must be greater than or equal to zero).");
            if (payer.SiteClientAccountId < 0)
                errors.Add($"Invalid WorkgroupId {payer.SiteClientAccountId}, (must be greater than or equal to zero).");
            if (string.IsNullOrEmpty(payer.ModifiedBy))
                errors.Add($"User name (from authentication token) is required for audit purposes.");
            return errors;
        }

        private async Task<int> GetUserIdAsync()
        {
            var sid = _claimLookupHelper.LookupClaim(HttpContext, JwtRegisteredClaimNames.Sid);
            var user = await _userDb.GetUserBySessionIdAsync(new Guid(sid));
            return (user?.UserId ?? -1);
        }

        private string GetUserName()
        {
            var name = _claimLookupHelper.LookupClaim(HttpContext, CustomClaimTypes.NameClaim);
            if (string.IsNullOrEmpty(name))
                name = _claimLookupHelper.LookupClaim(HttpContext, JwtRegisteredClaimNames.Sub);
            return name;
        }
    }
}
