﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ApiCommon;
using ApiCommon.Helpers;
using APIDataAccess;
using System.IdentityModel.Tokens.Jwt;
using ApiModels;
using System.Collections.Generic;
using System.Linq;

namespace AdminAPI.Controllers
{
    [ApiController]
    public class OptionalSettingsController : ApiBaseController
    {
        private readonly ILogger _logger;
        private readonly IEntitiesDatabase _entitiesDb;
        private readonly IUserDatabase _userDb;
        private readonly IWorkgroupDatabase _workgroupDb;
        private readonly IBankDatabase _bankDb;
        private readonly IClaimLookupHelper _claimLookupHelper;

        private UserModel _user;

        #region Contructors

        public OptionalSettingsController(ILogger<OptionalSettingsController> logger, IEntitiesDatabase entitiesDatabase, IUserDatabase userDatabase, IWorkgroupDatabase workgroupDatabase, IBankDatabase bankDatabase, IClaimLookupHelper claimLookupHelper)
        {
            _logger = logger;
            _entitiesDb = entitiesDatabase;
            _userDb = userDatabase;
            _workgroupDb = workgroupDatabase;
            _bankDb = bankDatabase;
            _claimLookupHelper = claimLookupHelper;
        }

        #endregion

        #region public methods
        [Authorize]
        [HttpGet("api/entities/{entityId}/optionalsettings")]
        public async Task<ActionResult> Get(int entityId)
        {
            try
            {
                // Verify the User is logged in and has access
                bool bValidUser = await ValidateUser();
                if (bValidUser)
                {
                    IEnumerable<WorkgroupOptionalSettingsModel> response = await _workgroupDb.GetOptionalSettings(entityId);
                    await PopulateBankName(response);

                    if (response == null)
                    {
                        return BadRequest("Error getting workgroup optional settings");
                    }
                    return Ok(response);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                var message = "ERROR - Exception thrown getting workgroup optional settings";
                _logger.LogError(ex, $"{message} from database; bad request returned to caller.");
                return BadRequest(message);
            }
        }

        #endregion

        #region private methods

        private async Task<bool> ValidateUser()
        {
            var sid = _claimLookupHelper.LookupClaim(HttpContext, JwtRegisteredClaimNames.Sid);
            _user = await _userDb.GetUserBySessionIdAsync(new Guid(sid));
            return (_user != null);
        }


        /* >>>>>>> COMMENTED OUT UNTIL WE GET RAAM INTERFACE */
        //private async void populateBankName(IEnumerable<WorkgroupOptionalSettingsModel> workgroups, int entityId)
        //{
        //    var banks = await _bankDb.GetBanksForEntity(entityId);

        //    // update the Workgroup model with a Bank Name
        //    foreach (var wg in workgroups)
        //    {
        //        var bank = banks.FirstOrDefault(d => d.BankId == wg.BankId);
        //        wg.BankName = bank != null ? bank.BankName : string.Empty;
        //    }
        //}

        private async Task PopulateBankName(IEnumerable<WorkgroupOptionalSettingsModel> workgroups)
        {
            foreach (var workgroup in workgroups)
            {
                var bank = await _bankDb.GetBank(workgroup.BankId);
                workgroup.BankName = bank.BankName;
            }
        }
    }

    #endregion
}