﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ApiCommon;
using ApiCommon.Helpers;
using ApiCommon.Models;
using APIDataAccess;
using AutoMapper;
using APIDataAccess.Profiles;

namespace AdminAPI
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        #region Properties
        public IConfiguration Configuration { get; }
        public string[] CorsUris { get; set; }
        #endregion

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            LoadConfigurations();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddAutoMapper(typeof(MappingProfile));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddTransient<IPayerDatabase, PayerDatabase>();
            services.AddTransient<IUserDatabase, UserDatabase>();
            services.AddTransient<IEntitiesDatabase, EntitiesDatabase>();
            services.AddTransient<IWorkgroupDatabase, WorkgroupDatabase>();
            services.AddTransient<IBankDatabase, BankDatabase>();
            services.AddTransient<IClaimLookupHelper, ClaimLookupHelper>();

            services.Configure<AppSettings>(Configuration);

            services.ConfigureJwtAuthentication(Configuration.Get<AppSettings>());

            services.ConfigureAuthorization();

            services.AddCors(options =>
            {
                // this defines a CORS policy to be used later.
                options.AddPolicy("CorsPolicy", policy =>
                {
                    policy.WithOrigins(CorsUris)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseMvc();
        }

        private void LoadConfigurations()
        {
            var section = Configuration.GetSection("AppSettings");
            var corsuris = section["CorsUris"];
            if (!string.IsNullOrEmpty(corsuris))
                CorsUris = corsuris.Split(",");
        }
    }
}
