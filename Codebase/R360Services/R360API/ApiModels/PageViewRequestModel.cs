﻿using System.Diagnostics.CodeAnalysis;

namespace ApiModels
{
    public class PageViewRequestModel
    {
        public bool? isPageView { get; set; }
    }
}
