﻿using System;
using Newtonsoft.Json;

namespace ApiModels
{
    public class PayerModel
    {
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        [JsonConverter(typeof(NullToEmptyConverter))]
        public string RoutingNumber { get; set; }
        [JsonConverter(typeof(NullToEmptyConverter))]
        public string Account { get; set; }
        [JsonConverter(typeof(NullToEmptyConverter))]
        public string PayerName { get; set; }
        public bool IsDefault { get; set; }
        public string CreatedBy { get; set; }
        public string CreationDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ModificationDate { get; set; }

        public void FormatDates(string format)
        {
            var creationDate = CreationDate;
            var modificationDate = ModificationDate;
            try
            {
                DateTime dt;
                dt = DateTime.Parse(creationDate);
                CreationDate = dt.ToString(format);
                dt = DateTime.Parse(modificationDate);
                ModificationDate = dt.ToString(format);
            }
            catch (Exception)
            {
                CreationDate = creationDate;
                ModificationDate = modificationDate;
            }
        }
    }

    internal class NullToEmptyConverter : JsonConverter
    {
        private JsonSerializer _stringSerializer = new JsonSerializer();

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string value = _stringSerializer.Deserialize<string>(reader);

            if (string.IsNullOrEmpty(value))
            {
                value = "";
            }

            return value;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            _stringSerializer.Serialize(writer, value);
        }
    }
}
