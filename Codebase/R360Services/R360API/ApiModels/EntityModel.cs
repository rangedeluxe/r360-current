﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiModels
{
    public class EntityModel
    {
        public int EntityID { get; set; }

        public string EntityName { get; set; }

        public bool DisplayBatchID { get; set; }

        public ImageDisplayModes PaymentImageDisplayMode { get; set; }

        public ImageDisplayModes DocumentImageDisplayMode { get; set; }

        public int ViewingDays { get; set; }

        public int MaximumSearchDays { get; set; }

        public string BillingAccount { get; set; }

        public string BillingField1 { get; set; }

        public string BillingField2 { get; set; }
    }
    public enum ImageDisplayModes
    {
        UseInheritedSetting = 0,
        Both = 1,
        FrontOnly = 2,
    }
}


