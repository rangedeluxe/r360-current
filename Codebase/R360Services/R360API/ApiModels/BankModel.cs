﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiModels
{
    public class BankModel
    {
        public int BankId { get; set; }
        public string BankName { get; set; }
    }
}
