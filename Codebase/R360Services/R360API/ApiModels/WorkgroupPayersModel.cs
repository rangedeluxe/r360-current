﻿using System;
using System.Collections.Generic;


namespace ApiModels
{
    public class WorkgroupPayersModel
    {
        public int SiteBankId { get; set; }
        public string SiteBankName { get; set; }
        public int SiteClientAccountId { get; set; }
        public string SiteClientAccountName { get; set; }
        public IEnumerable<PayerModel> Payers { get; set; }
    }
}
