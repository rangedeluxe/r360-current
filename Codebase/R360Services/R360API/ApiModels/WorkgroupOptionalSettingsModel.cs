﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiModels
{
    public class WorkgroupOptionalSettingsModel
    {
        public int WorkgroupId { get; set; }
        public string LongName { get; set; }
        public int BankId { get; set; }
        public string BankName { get; set; }
        public bool IsActive { get; set; }
    }
}
