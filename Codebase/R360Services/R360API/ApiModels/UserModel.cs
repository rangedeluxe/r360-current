﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace ApiModels
{
    public class UserModel
    {
        public int? UserId { get; set; }
        public string LogonName { get; set; }
        public DateTime? CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
