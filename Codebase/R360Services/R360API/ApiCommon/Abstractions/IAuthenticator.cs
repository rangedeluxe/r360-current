﻿using System;
using ApiCommon.Models;

namespace ApiCommon.Abstractions
{
    public interface IAuthenticator
    {
        TokenClaims Authenticate();
    }
}
