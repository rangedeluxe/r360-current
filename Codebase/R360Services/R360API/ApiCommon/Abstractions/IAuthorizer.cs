﻿using System.Security.Claims;

namespace ApiCommon.Abstractions
{
    public interface IAuthorizer<TRightsContext>
    {
        bool HasRights(ClaimsPrincipal claimsPrincipal, TRightsContext rightsContext);
    }
}
