﻿using System;

namespace ApiCommon.DataProviders
{
    public interface IUserDataAccess
    {
        bool HasWorkgroupEntitlement(string sessionId, int bankId, int workgroupId);
    }
}
