﻿using System;
using System.Security.Claims;
using ApiCommon.Models;

namespace ApiCommon.DataProviders
{
    public interface IUserDataProvider
    {
        bool HasRights(ClaimsPrincipal claimsPrincipal, WorkgroupRights rightsContext);
    }
}
