﻿using System;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ApiCommon.Abstractions;
using ApiCommon.Models;

namespace ApiCommon.DataProviders
{
    public class UserDataProvider : IAuthorizer<WorkgroupRights>, IUserDataProvider
    {
        private ILogger<UserDataProvider> _logger;
        private IUserDataAccess _userDataAccess;

        [ExcludeFromCodeCoverage]
        public UserDataProvider(ILogger<UserDataProvider> logger, IOptions<AppSettings> appSettings)
        {
            Init(logger, appSettings?.Value, null);
        }

        public UserDataProvider(ILogger<UserDataProvider> logger, AppSettings appSettings, IUserDataAccess userDataAccess)
        {
            Init(logger, appSettings, userDataAccess);
        }

        public void Init(ILogger<UserDataProvider> logger, AppSettings appSettings, IUserDataAccess userDataAccess = null)
        {
            _logger = logger;
            _userDataAccess = userDataAccess;
            if (_userDataAccess == null)
                _userDataAccess = new UserDataAccess(logger, appSettings);
        }

        public bool HasRights(ClaimsPrincipal claimsPrincipal, WorkgroupRights rightsContext)
        {
            try
            {
                var sessionId = claimsPrincipal?.FindFirst(JwtRegisteredClaimNames.Sid)?.Value ?? string.Empty;
                if (string.IsNullOrEmpty(sessionId))
                    return false;
                int bankId = Convert.ToInt32(rightsContext.BankId);
                int workgroupId = Convert.ToInt32(rightsContext.WorkgroupId);
                return _userDataAccess.HasWorkgroupEntitlement(sessionId, bankId, workgroupId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Exception thrown in HasRights().");
                return false;
            }
        }

    }
}
