﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Dapper;
using ApiCommon.Models;

namespace ApiCommon.DataProviders
{
    [ExcludeFromCodeCoverage]
    public class UserDataAccess : IUserDataAccess
    {
        private readonly ILogger<UserDataProvider> _logger;
        private string _connectionString = string.Empty;
        private const string CONNECTIONSTRING = "database:connectionString";
        private const string HASSESSIONWORKGROUPENTITLEMENTSQL = "SELECT RecHubUser.udf_HasSessionWorkgroupEntitlement(@parmSessionId,@parmBankId,@parmWorkgroupId)";

        public UserDataAccess(ILogger<UserDataProvider> logger, AppSettings appSettings)
        {
            _logger = logger;
            _connectionString = appSettings?.Database?.R360ConnectionString?.ToDecryptedString().Trim();
            if (string.IsNullOrEmpty(_connectionString))
                _connectionString = appSettings?.Database?.ConnectionString?.ToDecryptedString().Trim();
        }

        public bool HasWorkgroupEntitlement(string sessionId, int bankId, int workgroupId)
        {
            try
            {
                bool hasentitlement = false;
                using (var db = new SqlConnection(_connectionString))
                {
                    db.Open();
                    hasentitlement = true;
                    // NOTE: This call is to a SQL function, not a stored proc, so this code is a little
                    //          different than the usual pattern.
                    hasentitlement = db.ExecuteScalar<bool>(HASSESSIONWORKGROUPENTITLEMENTSQL,
                        new
                        {
                            @parmSessionId = new Guid(sessionId),
                            @parmBankId = bankId,
                            @parmWorkgroupId = workgroupId
                        },
                        commandType: CommandType.Text);
                }
                return hasentitlement;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Exception thrown in HasWorkgroupEntitlement() checking workgroup entitlement for bank {bankId}, workgroup {workgroupId} on session {sessionId.ToString()}.");
                return false;
            }
        }
    }
}
