﻿using System;
using System.Collections.Generic;

namespace ApiCommon.Models
{
    public class AppSettings
    {
        public DatabaseSettings Database { get; set; }
        public IdentitySettings Identity { get; set; }
    }
}
