﻿using System;

namespace ApiCommon.Models
{
    public class IdentitySettings
    {
        public string Thumbprint { get { return _Thumbprint; } set { _Thumbprint = value.ToLower(); } }
        public string Issuer { get { return _Issuer; } set { _Issuer = value.ToLower(); } }
        public string Audience { get { return _Audience; } set { _Audience = value.ToLower(); } }
        public int ExpirationMinutes { get; set; }
        public string TokenEndpoint { get; set; }
        public bool ShowPII { get; set; } = false;

        private string _Thumbprint = string.Empty;
        private string _Issuer = string.Empty;
        private string _Audience = string.Empty;
    }
}
