﻿using System;

namespace ApiCommon.Models
{
    public class DatabaseSettings
    {
        public string R360ConnectionString { get; set; }
        public string RaamConnectionString { get; set; }
        public string ConnectionString { get; set; }
    }
}
