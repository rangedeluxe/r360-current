﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using ApiCommon.Authorization;

namespace ApiCommon.Models
{
    public class TokenClaims
    {
        public bool CredentialsValidated { get; set; }
        public string SessionId { get; set; }
        public string EntityId { get; set; }
        public string UserId { get; set; }
        public string UserFullName { get; set; }
        public string UserLogonName { get; set; }

        public TokenClaims()
        {
            CredentialsValidated = false;
            SessionId = string.Empty;
            EntityId = string.Empty;
            UserId = string.Empty;
            UserFullName = string.Empty;
            UserLogonName = string.Empty;
        }

        public IEnumerable<Claim> ToClaims()
        {
            var claims = new List<Claim>();

            if (!string.IsNullOrEmpty(UserLogonName))
                claims.Add(new Claim(JwtRegisteredClaimNames.Sub, UserLogonName));

            if (!string.IsNullOrEmpty(SessionId))
                claims.Add(new Claim(JwtRegisteredClaimNames.Sid, SessionId.ToString()));

            if (!string.IsNullOrEmpty(EntityId))
                claims.Add(new Claim(CustomClaimTypes.EntityIdClaim, EntityId));

            if (!string.IsNullOrEmpty(UserFullName))
                claims.Add(new Claim(CustomClaimTypes.NameClaim, UserFullName));

            if (!string.IsNullOrEmpty(UserId))
                claims.Add(new Claim(CustomClaimTypes.UserIdClaim, UserId));

            return claims;
        }

    }
}
