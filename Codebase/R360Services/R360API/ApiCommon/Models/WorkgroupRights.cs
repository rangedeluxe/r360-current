﻿using System;
using Newtonsoft.Json;

namespace ApiCommon.Models
{
    public class WorkgroupRights
    {
        public string BankId { get; set; } = null;
        public string WorkgroupId { get; set; } = null;
        public string SiteBankId { get { return BankId; } set { BankId = value; } }
        public string SiteClientAccountId { get { return WorkgroupId; } set { WorkgroupId = value; } }

        public bool HasValues()
        {
            return (!string.IsNullOrEmpty(BankId)) && (!string.IsNullOrEmpty(WorkgroupId));
        }
    }
}
