﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ApiCommon.Models
{
    [ExcludeFromCodeCoverage]
    public class ServiceResponse
    {
        public bool Success { get; set; }

        public IEnumerable<string> Errors { get; set; }

        public object Data { get; set; }
    }
}
