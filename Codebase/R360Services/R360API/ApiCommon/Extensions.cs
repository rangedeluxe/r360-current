﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using ApiCommon.Abstractions;
using ApiCommon.Authorization;
using ApiCommon.Models;
using ApiCommon.DataProviders;
using System.Security.Cryptography.X509Certificates;

namespace ApiCommon
{
    [ExcludeFromCodeCoverage]
    public static class Extensions
    {
        public static void ConfigureJwtAuthentication(this IServiceCollection services, AppSettings configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("Must provide configuration information for authentication.");

            if (configuration.Identity == null)
                throw new ArgumentNullException("Must provide identity configuration information for authentication.");

            var identity = configuration.Identity;

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var certificate = FindByThumbprint(identity.Thumbprint, StoreName.My, StoreLocation.LocalMachine);
            var key = new X509SecurityKey(certificate);

            IdentityModelEventSource.ShowPII = configuration.Identity.ShowPII;

            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,

                        ValidIssuer = identity.Issuer,
                        ValidAudience = identity.Audience, 
                        IssuerSigningKey = key
                    };

                    options.Events = new JwtBearerEvents()
                    {

                        OnChallenge = context =>
                        {
                            context.Response.Redirect(identity.TokenEndpoint);
                            return Task.CompletedTask;
                        }
                    };

                });
        }

        private static X509Certificate2 FindByThumbprint(string thumbprint, StoreName storeName, StoreLocation storeLocation)
        {
            var certificateStore = new X509Store(storeName, storeLocation);
            certificateStore.Open(OpenFlags.ReadOnly);

            foreach (var certificate in certificateStore.Certificates)
            {
                if (string.Equals(certificate?.Thumbprint, thumbprint, StringComparison.CurrentCultureIgnoreCase))
                {
                    certificateStore.Close();
                    return certificate;
                }
            }
            throw new ArgumentException($"Cannot find certificate with thumbprint {thumbprint} in certificate store");
        }

        public static void ConfigureAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("HasWorkgroupRights", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.AddRequirements(new HasWorkgroupRightsRequirement());
                });
            });

            services.AddScoped<IAuthorizationHandler, HasWorkgroupRightsHandler>();
            services.AddTransient(typeof(IAuthorizer<WorkgroupRights>), typeof(UserDataProvider));
        }

        public static string GenerateToken(this IAuthenticator authenticator, AppSettings configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("Must provide configuration information to generate token.");

            if (configuration?.Identity == null)
                throw new ArgumentNullException("Must provide identity configuration information for authentication.");

            var claims = authenticator.Authenticate().ToClaims();
            var identity = configuration.Identity;
            var signing = new X509SigningCredentials(FindByThumbprint(identity.Thumbprint, StoreName.My, StoreLocation.LocalMachine));
            var jwtToken = new JwtSecurityToken
            (
                issuer: identity.Issuer,
                audience: identity.Audience,
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(identity.ExpirationMinutes == 0 ? 30 : identity.ExpirationMinutes),
                signingCredentials: signing
            );

            return new JwtSecurityTokenHandler().WriteToken(jwtToken);
        }

        public static string ToDecryptedString(this string value)
        {
            var crypto = new DpapiCrypto.CryptoDpapi();
            var response = crypto.Decrypt(value);
            if (response.Status == DpapiCrypto.Dto.DpapiStatusCode.SUCCESS)
                return response.ReturnedString;

            return value;
        }

    }
}

