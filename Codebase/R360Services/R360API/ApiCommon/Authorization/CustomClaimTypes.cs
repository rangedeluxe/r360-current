﻿using System;

namespace ApiCommon.Authorization
{
    public static class CustomClaimTypes
    {
        public static readonly string NameClaim = "name";
        public static readonly string EntityIdClaim = "eid";
        public static readonly string UserIdClaim = "uid";
    }
}
