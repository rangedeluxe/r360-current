﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using Newtonsoft.Json;
using ApiCommon.Abstractions;
using ApiCommon.Models;

namespace ApiCommon.Authorization
{
    [ExcludeFromCodeCoverage]
    public class HasWorkgroupRightsHandler : AuthorizationHandler<HasWorkgroupRightsRequirement>
    {
        private readonly IAuthorizer<WorkgroupRights> authorizer = null;
        private readonly ILogger<HasWorkgroupRightsHandler> logger = null;

        public HasWorkgroupRightsHandler(ILogger<HasWorkgroupRightsHandler> loggerContext, IAuthorizer<WorkgroupRights> authorizerContext)
        {
            authorizer = authorizerContext;
            logger = loggerContext;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, HasWorkgroupRightsRequirement requirement)
        {
            var filterContext = context.Resource as AuthorizationFilterContext;
            if (filterContext == null)
            {
                context.Fail();
                return Task.CompletedTask;
            }

            // First look for the bankid/workgroupid in the body of the request
            var req = filterContext.HttpContext.Request;
            req.EnableRewind();
            var bodyContent = new StreamReader(req.Body, Encoding.UTF8, true, 1024, true).ReadToEnd();
            req.Body.Position = 0;
            WorkgroupRights workgroupRights = JsonConvert.DeserializeObject<WorkgroupRights>(bodyContent) ?? new WorkgroupRights();

            // If it's not in the body, and we have route data, look in the route data.
            if ((!workgroupRights.HasValues()) && (filterContext.RouteData != null))
            {
                workgroupRights.BankId = filterContext.RouteData.Values["bankId"]?.ToString();
                workgroupRights.WorkgroupId = filterContext.RouteData.Values["workgroupId"]?.ToString();
            }

            // If no bankid/workgroupid is found in either body or route data, don't authorize.
            if (!workgroupRights.HasValues())
            {
                context.Fail();
                return Task.CompletedTask;
            }

            if (!authorizer.HasRights(context.User, workgroupRights))
            {
                context.Fail();
                return Task.CompletedTask;
            }

            // all checks out
            context.Succeed(requirement);
            return Task.CompletedTask;
        }
    }
}
