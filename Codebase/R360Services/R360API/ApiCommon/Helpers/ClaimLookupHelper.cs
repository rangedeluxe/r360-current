﻿using System;
using Microsoft.AspNetCore.Http;

namespace ApiCommon.Helpers
{
    public class ClaimLookupHelper : IClaimLookupHelper
    {
        public string LookupClaim(HttpContext httpContext, string claimName)
        {
            return (httpContext?.User?.FindFirst(claimName)?.Value ?? string.Empty).Trim();
        }
    }
}
