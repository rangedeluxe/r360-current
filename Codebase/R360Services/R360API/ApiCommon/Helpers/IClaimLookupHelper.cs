﻿using System;
using Microsoft.AspNetCore.Http;

namespace ApiCommon.Helpers
{
    public interface IClaimLookupHelper
    {
        string LookupClaim(HttpContext httpContext, string claimName);
    }
}
