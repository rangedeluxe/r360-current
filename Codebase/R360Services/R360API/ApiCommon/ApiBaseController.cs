﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ApiCommon.Models;
using ApiCommon.Authorization;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using ApiCommon.Helpers;
using System;

namespace ApiCommon
{
    public class ApiBaseController : ControllerBase
    {
        protected ServiceResponse CreateResponse(bool success, object results, IEnumerable<string> errorMessages = null)
        {
            var response = new ServiceResponse
            {
                Success = success,
                Errors = errorMessages,
                Data = results
            };
            return response;
        }

        protected ServiceResponse CreateSuccessResponse(object results)
        {
            return CreateResponse(true, results, null);
        }

        protected ServiceResponse CreateErrorResponse(IEnumerable<string> errorMessages)
        {
            return CreateResponse(false, null, errorMessages);
        }

        protected ServiceResponse CreateErrorResponse(string errorMessage)
        {
            var errors = new List<string>();
            errors.Add(errorMessage);
            return CreateErrorResponse(errors);
        }
    }
}
