﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using APIDataAccess.Model;
using ApiModels;
using AutoMapper;
using Dapper;
using DatabaseHelpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace APIDataAccess
{
    public class BankDatabase : IBankDatabase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private string _connectionstring = string.Empty;

        // AppSettings
        private const string CONNECTIONSTRING = "database:connectionString";
        private const string DATEFORMAT = "AppSettings:dateFormat";

        // Stored Procs
        private const string GET_BANK_PROC = "RecHubData.usp_dimBanks_Get_BySiteBankID";
        private const string GET_BANKS_BY_ENTITY_PROC = "RecHubData.usp_dimBanks_Get_ByFI";

        public BankDatabase(ILogger<IWorkgroupDatabase> logger, IConfiguration configuration, IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _mapper = mapper;
            _connectionstring = new DatabaseConnectionString(_logger).GetConnectionString(CONNECTIONSTRING, _configuration);
        }

        public async Task<BankModel> GetBank(int bankId)
        {
            try
            {
                DimBank bank = null;
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    bank = db.QueryFirst<DimBank>(GET_BANK_PROC,
                        new
                        {
                            @parmSiteBankID = bankId
                        },
                        commandType: CommandType.StoredProcedure);
                }

                // Map Query to the Return Type
                return _mapper.Map<DimBank, BankModel>(bank); ;
            }
            catch (Exception ex)
            {
                // Log the exception, then re-throw it for the caller to catch and get all the details
                _logger.LogError(ex, $"Exception thrown calling stored procedure {GET_BANK_PROC} in BankDatabase.GetBank().");
                throw;
            }
        }

        public async Task<IEnumerable<BankModel>> GetBanksForEntity(int entityId)
        {
            try
            {
                List<DimBank> banks = null;
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    banks = db.Query<DimBank>(GET_BANKS_BY_ENTITY_PROC,
                        new
                        {
                            @parmFIEntityID = entityId
                        },
                        commandType: CommandType.StoredProcedure).AsList();
                }

                // Map Query to the Return Type
                return _mapper.Map<IEnumerable<DimBank>, IEnumerable<BankModel>>(banks); ;
            }
            catch (Exception ex)
            {
                // Log the exception, then re-throw it for the caller to catch and get all the details
                _logger.LogError(ex, $"Exception thrown calling stored procedure {GET_BANKS_BY_ENTITY_PROC} in BankDatabase.GetBanksForEntity().");
                throw;
            }
        }
    }
}
