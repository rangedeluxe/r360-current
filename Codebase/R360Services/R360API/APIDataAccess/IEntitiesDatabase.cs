﻿using ApiModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIDataAccess
{
    public interface IEntitiesDatabase
    {
        Task<EntityModel> GetOrCreateEntityWorkgroupDefaultsAsync(int entityId, int userId);
        Task<EntityModel> GetEntityAsync(int userId, int entityId);
        Task<bool> AddUpdateEntityAsync(int userId, EntityModel entity);
        Task<string> GetViewingDaysPreferenceAsync();
    }
}
