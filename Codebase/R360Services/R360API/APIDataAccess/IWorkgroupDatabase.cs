﻿using ApiModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APIDataAccess
{
    public interface IWorkgroupDatabase
    {
       Task<IEnumerable<WorkgroupOptionalSettingsModel>> GetOptionalSettings(int workgroupId);
    }
}
