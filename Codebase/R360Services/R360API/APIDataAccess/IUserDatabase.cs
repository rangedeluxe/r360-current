﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiModels;

namespace APIDataAccess
{
    public interface IUserDatabase
    {
        Task<UserModel> GetUserBySessionIdAsync(Guid sessionId);
    }
}
