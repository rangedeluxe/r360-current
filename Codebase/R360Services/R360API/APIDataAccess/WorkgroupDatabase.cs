﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using APIDataAccess.Model;
using ApiModels;
using AutoMapper;
using Dapper;
using DatabaseHelpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace APIDataAccess
{
    public class WorkgroupDatabase : IWorkgroupDatabase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private string _connectionstring = string.Empty;

        // AppSettings
        private const string CONNECTIONSTRING = "database:connectionString";
        private const string DATEFORMAT = "AppSettings:dateFormat";

        // Stored Procs
        private const string GET_OPTIONAL_SETTINGS_PROC = "RecHubData.usp_dimClientAccounts_Get_ByEntityID";

        public WorkgroupDatabase(ILogger<IWorkgroupDatabase> logger, IConfiguration configuration, IMapper mapper)
        {
            _logger = logger;
            _configuration = configuration;
            _mapper = mapper;
            _connectionstring = new DatabaseConnectionString(_logger).GetConnectionString(CONNECTIONSTRING, _configuration);
        }

        public async Task<IEnumerable<WorkgroupOptionalSettingsModel>> GetOptionalSettings(int entityId)
        {
            try
            {
                List<DimClientAccount> clientAccounts = null;
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    clientAccounts = db.Query<DimClientAccount>(GET_OPTIONAL_SETTINGS_PROC,
                        new
                        {
                            @parmEntityID = entityId
                        },
                        commandType: CommandType.StoredProcedure).AsList();
                }

                // Map Query to the Return Type
                return  _mapper.Map<IEnumerable<DimClientAccount>, IEnumerable<WorkgroupOptionalSettingsModel>>(clientAccounts); 
            }
            catch (Exception ex)
            {
                // Log the exception, then re-throw it for the caller to catch and get all the details
                _logger.LogError(ex, $"Exception thrown calling stored procedure {GET_OPTIONAL_SETTINGS_PROC} in WorkgroupDatabase.GetOptionalSettings().");
                throw;
            }

        }
    }
}
