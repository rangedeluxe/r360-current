﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DatabaseHelpers;
using ApiModels;

namespace APIDataAccess
{
    [ExcludeFromCodeCoverage]
    public class UserDatabase : IUserDatabase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private const string CONNECTIONSTRING = "database:connectionString";
        private const string GETUSERBYSESSIONPROC = "RecHubUser.usp_Users_Get_BySessionID";
        private string _connectionstring = string.Empty;

        public UserDatabase(ILogger<UserDatabase> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            _connectionstring = new DatabaseConnectionString(_logger).GetConnectionString(CONNECTIONSTRING, _configuration);
        }

      

        public async Task<UserModel> GetUserBySessionIdAsync(Guid sessionId)
        {
            try
            {
                UserModel user = null;

                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    user = db.QueryFirst<UserModel>(GETUSERBYSESSIONPROC,
                        new
                        {
                            @parmSessionID = sessionId
                        },
                        commandType: CommandType.StoredProcedure);
                }
                return user;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown calling stored procedure " + GETUSERBYSESSIONPROC + " in PayerDatabase.GetUserBySessionId().");
                throw;
            }
        }
    }
}
