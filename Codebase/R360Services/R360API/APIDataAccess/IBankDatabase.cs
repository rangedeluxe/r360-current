﻿using ApiModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace APIDataAccess
{
    public interface IBankDatabase
    {
        Task<BankModel> GetBank(int bankId);
        Task<IEnumerable<BankModel>> GetBanksForEntity(int entityId);
    }
}
