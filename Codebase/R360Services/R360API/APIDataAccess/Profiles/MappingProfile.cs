﻿using APIDataAccess.Model;
using ApiModels;
using AutoMapper;

namespace APIDataAccess.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DimClientAccount, WorkgroupOptionalSettingsModel>()
                .ForMember(d => d.WorkgroupId, opt => opt.MapFrom(src => src.SiteClientAccountID))
                .ForMember(d => d.BankId, opt => opt.MapFrom(src => src.SiteBankID));

            CreateMap<DimBank, BankModel>()
               .ForMember(d => d.BankId, opt => opt.MapFrom(src => src.SiteBankID));

        }
        
    }
}
