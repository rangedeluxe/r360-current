﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiModels;

namespace APIDataAccess
{
    public interface IPayerDatabase
    {
        Task<IEnumerable<PayerModel>>GetPayerAsync(int userId, int bankId, int clientAccountId, string routingNumber = null, string account = null);
        Task UpsertPayerAsync(int userId, PayerModel payer);
        Task DeletePayerAsync(int userId, int bankId, int clientAccountId, string routingNumber, string account);
        Task<string> GetBankNameAsync(int bankId);
        Task<string> GetClientAccountNameAsync(int bankId, int clientAccountId);
    }
}
