﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DatabaseHelpers;
using ApiModels;

namespace APIDataAccess
{
    [ExcludeFromCodeCoverage]
    public class EntitiesDatabase : IEntitiesDatabase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private const string CONNECTIONSTRING = "database:connectionString";
        private const string DATEFORMAT = "AppSettings:dateFormat";
        private const string GETENTITITYBYIDPROC = "RecHubUser.usp_OLEntities_Get_ByEntityID";
        private const string UPSERTENTITYPROC = "RecHubUser.usp_OLEntities_Upsert";
        private const string GETVIEWINGDAYSPROC = "RecHubUser.usp_OLPreferences_Get_Default_ByPreferenceName";
        private string _connectionstring = string.Empty;

        public EntitiesDatabase(ILogger<EntitiesDatabase> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            _connectionstring = new DatabaseConnectionString(_logger).GetConnectionString(CONNECTIONSTRING, _configuration);
        }

        public async Task<EntityModel> GetOrCreateEntityWorkgroupDefaultsAsync(int entityId, int userId)
        {
            try
            {
                var entity = await GetEntityAsync(entityId, userId);
                if (entity == null)
                {
                    var viewingDaysString = await GetViewingDaysPreferenceAsync();
                    int viewingDays;
                    int.TryParse(viewingDaysString, out viewingDays);
                    if (viewingDays <= 0)
                        viewingDays = 365;

                    entity = new EntityModel
                    {
                        EntityID = entityId,
                        EntityName = "",
                        DocumentImageDisplayMode = ImageDisplayModes.UseInheritedSetting,
                        PaymentImageDisplayMode = ImageDisplayModes.UseInheritedSetting,
                        DisplayBatchID = false,
                        ViewingDays = viewingDays,
                        MaximumSearchDays = 365,
                    };
                    bool bSuccess = await AddUpdateEntityAsync(userId, entity);
                    if (!bSuccess)
                    {
                        throw new InvalidOperationException("Error saving entity workgroup defaults: ");
                    }
                }
                return entity;
            }
            catch (Exception ex)
            {
                // Log the exception, then re-throw it for the caller to catch and get all the details
                _logger.LogError(ex, "Exception thrown in  GetOrCreateEntityWorkgroupDefaultsAsync().");
                throw;
            }
        }

        public async Task<EntityModel> GetEntityAsync(int entityId, int userId)
        {
            try
            {
                _logger.LogDebug( "Calling stored procedure " + GETENTITITYBYIDPROC + " @parmUserId = " + userId + ", @parmEntityId = " + entityId);
                EntityModel entity = null;
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    entity = db.QueryFirst<EntityModel>(GETENTITITYBYIDPROC,
                        new
                        {
                            @parmUserId = userId,
                            @parmEntityId = entityId
                        },
                        commandType: CommandType.StoredProcedure);
                }
                return entity;
            }
            // If no rows are returned calling the stored proc, return an empty string for the name
            catch (InvalidOperationException)
            {
                return null;
            }
            // For all other exceptions log it, then re-throw it for the caller to catch with all details still intact.
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown calling stored procedure " + GETENTITITYBYIDPROC + " in EntityDatabase.GetEntityAsync.");
                throw;
            }
        }

        public async Task<bool> AddUpdateEntityAsync(int userId, EntityModel entity)
        {
            try
            {
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    db.Query(UPSERTENTITYPROC,
                            new
                            {
                                @parmUserId = userId,
                                @parmEntityId = entity.EntityID,
                                @parmDisplayBatchId = entity.DisplayBatchID,
                                @parmPaymentImageDisplayMode = (int)entity.PaymentImageDisplayMode,
                                @parmDocumentImageDisplayMode = (int)entity.DocumentImageDisplayMode,
                                @parmViewingDays = entity.ViewingDays,
                                @parmMaximumSearchDays = entity.MaximumSearchDays,
                                @parmBillingAccount = entity.BillingAccount ?? string.Empty,
                                @parmBillingField1 = entity.BillingField1 ?? string.Empty,
                                @parmBillingField2 = entity.BillingField2 ?? string.Empty,
                                @paramEntityName = entity.EntityName ?? string.Empty,
                            },
                            commandType: CommandType.StoredProcedure);
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown calling stored procedure " + UPSERTENTITYPROC + " in EntitiesDatabase.AddUpdateEntityAsync().");
                throw;
            }
        }


        public async Task<string> GetViewingDaysPreferenceAsync()
        {
            try
            {
                string viewSetting = "";
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    viewSetting = db.QueryFirst<string>(GETVIEWINGDAYSPROC,
                        new
                        {
                            @parmPreferenceName = "ViewingDays"
                        },
                        commandType: CommandType.StoredProcedure);
                }
                return viewSetting;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown calling stored procedure " + GETVIEWINGDAYSPROC + " in EntitiesDatabase.GetViewingDaysPreferenceAsync().");
                throw;
            }
        }
    }
}