﻿
namespace APIDataAccess.Model
{
    public class DimClientAccount
    {
        public int ClientAccountKey {get; set; }
		public int SiteBankID {get; set; }
		public int SiteClientAccountID {get; set; }
		public bool IsActive {get; set; }
		public string ShortName {get; set; }
		public string LongName { get; set; }
    }
}
