﻿using System;
using System.Collections.Generic;
using System.Text;

namespace APIDataAccess.Model
{
    public class DimBank
    {
        public int BankKey { get; set; }
        public int SiteBankID { get; set; }
        public string BankName { get; set; }
        public int EntityID { get; set; }
    }
}
