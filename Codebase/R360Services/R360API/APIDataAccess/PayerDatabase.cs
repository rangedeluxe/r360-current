﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Diagnostics.CodeAnalysis;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DatabaseHelpers;
using ApiModels;
using AutoMapper;

namespace APIDataAccess
{
    [ExcludeFromCodeCoverage]
    public class PayerDatabase : IPayerDatabase
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private const string CONNECTIONSTRING = "database:connectionString";
        private const string DATEFORMAT = "AppSettings:dateFormat";
        private const string GETPAYERSPROC = "RecHubData.usp_WorkgroupPayers_Get";
        private const string UPSERTPAYERSPROC = "RecHubData.usp_WorkgroupPayers_UpSert";
        private const string DELETEPAYERSPROC = "RecHubData.usp_WorkgroupPayers_Delete";
        private const string GETBANKNAMEPROC = "RecHubData.usp_dimBanks_Get_BySiteBankID";
        private const string GETCLIENTNAMEPROC = "RecHubData.usp_dimClientAccounts_Get_NameBySiteBankIDSiteClientAccountID";
        private const string INSERTAUDITEVENTPROC = "RecHubCommon.usp_WFS_EventAudit_Ins";
        private const string GETUSERBYSESSIONPROC = "RecHubUser.usp_Users_Get_BySessionID";
        private const string APPLICATIONNAME = "Payer Maintenance";
        private const string PAGEVIEWEVENTNAME = "Viewed Page";
        private const string GETDATAEVENTNAME = "Submitted / Retrieved Data";
        private string _connectionstring = string.Empty;
        private string _dateformat = "MM/dd/yyyy hh:mm:ss tt";

        public PayerDatabase(ILogger<PayerDatabase> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
         
            _connectionstring = new DatabaseConnectionString(_logger).GetConnectionString(CONNECTIONSTRING, _configuration);
            if (!string.IsNullOrEmpty(_configuration[DATEFORMAT]))
                _dateformat = _configuration[DATEFORMAT].Trim();
        }

        public async Task<IEnumerable<PayerModel>> GetPayerAsync(int userId, int bankId, int clientAccountId, string routingNumber = null, string account = null)
        {
            try
            {
                List<PayerModel> payers = null;
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    payers = db.Query<PayerModel>(GETPAYERSPROC, 
                        new
                        {
                            @parmSiteBankID = bankId,
                            @parmSiteClientAccountID = clientAccountId,
                            @parmRoutingNumber = routingNumber,
                            @parmAccount = account
                        }, 
                        commandType: CommandType.StoredProcedure).AsList();
                    foreach (PayerModel payer in payers)
                    {
                        payer.SiteBankId = bankId;
                        payer.SiteClientAccountId = clientAccountId;
                        payer.FormatDates(_dateformat);
                    }
                }
                return payers;
            }
            catch (Exception ex)
            {
                // Log the exception, then re-throw it for the caller to catch and get all the details
                _logger.LogError(ex,"Exception thrown calling stored procedure " + GETPAYERSPROC + " in PayerDatabase.GetPayer().");
                throw;
            }
        }

        public async Task UpsertPayerAsync(int userId, PayerModel payer)
        {
            try
            {
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    db.Query<PayerModel>(UPSERTPAYERSPROC,
                        new
                        {
                            parmSiteBankId = payer.SiteBankId,
                            parmSiteClientAccountId = payer.SiteClientAccountId,
                            parmRoutingNumber = payer.RoutingNumber,
                            parmAccount = payer.Account,
                            parmPayerName = payer.PayerName,
                            parmIsDefault = payer.IsDefault,
                            parmWorkgroupPayerKey = 0,
                            parmUserId = userId,
                            parmUserName = payer.ModifiedBy ?? string.Empty
                        }, 
                        commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                // Log the exception, then re-throw it for the caller to catch and get all the details
                _logger.LogError(ex, "Exception thrown calling stored procedure " + UPSERTPAYERSPROC + " in PayerDatabase.UpsertPayer().");
                throw;
            }
        }

        public async Task DeletePayerAsync(int userId, int bankId, int clientAccountId, string routingNumber, string account)
        {
            try
            {
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    db.Query<PayerModel>(DELETEPAYERSPROC,
                        new
                        {
                            parmSiteBankId = bankId,
                            parmSiteClientAccountId = clientAccountId,
                            parmRoutingNumber = routingNumber,
                            parmAccount = account,
                            parmUserId = userId
                        },
                        commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                // Log the exception, then re-throw it for the caller to catch and get all the details
                _logger.LogError(ex, "Exception thrown calling stored procedure " + DELETEPAYERSPROC + " in PayerDatabase.DeletePayer().");
                throw;
            }
        }

        public async Task<string> GetBankNameAsync(int bankId)
        {
            try
            {
                var bankName = string.Empty;
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    var bank = db.QueryFirst(GETBANKNAMEPROC,
                        new
                        {
                            @parmSiteBankID = bankId
                        },
                        commandType: CommandType.StoredProcedure);
                    bankName = bank.BankName;
                }
                return bankName;
            }
            // If no rows are returned calling the stored proc, return an empty string for the name
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
            // For all other exceptions log it, then re-throw it for the caller to catch with all details still intact.
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown calling stored procedure " + GETBANKNAMEPROC + " in PayerDatabase.GetBankNameAsync().");
                throw;
            }
        }

        public async Task<string> GetClientAccountNameAsync(int bankId, int clientAccountId)
        {
            try
            {
                var clientName = string.Empty;
                using (var db = new SqlConnection(_connectionstring))
                {
                    await db.OpenAsync();
                    var client = db.QueryFirst(GETCLIENTNAMEPROC,
                        new
                        {
                            @parmSiteBankID = bankId,
                            @parmSiteClientAccountId = clientAccountId
                        },
                        commandType: CommandType.StoredProcedure);
                    clientName = client.LongName;
                }
                return clientName;
            }
            // If no rows are returned calling the stored proc, return an empty string for the name
            catch (InvalidOperationException)
            {
                return string.Empty;
            }
            // For all other exceptions log it, then re-throw it for the caller to catch with all details still intact.
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown calling stored procedure " + GETCLIENTNAMEPROC + " in PayerDatabase.GetClientAccountNameAsync().");
                throw;
            }
        }

        private void AuditEventMessage(SqlConnection db, string eventName, string eventType, int userId, string auditMessage)
        {
            try
            {
                db.Query(INSERTAUDITEVENTPROC,
                    new
                    {
                        parmApplicationName = APPLICATIONNAME,
                        parmEventName = eventName,
                        parmEventType = eventType,
                        parmUserID = userId,
                        parmAuditMessage = auditMessage
                    },
                    commandType: CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception thrown calling stored procedure " + INSERTAUDITEVENTPROC + " in PayerDatabase.AuditEventMessage().");
                throw;
            }
        }
    }
}
