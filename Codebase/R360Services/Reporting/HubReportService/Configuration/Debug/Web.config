﻿<?xml version="1.0"?>
<configuration>
  <configSections>
    <section name="system.identityModel" type="System.IdentityModel.Configuration.SystemIdentityModelSection, System.IdentityModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089"/>
    <section name="wfs.raam.trustedsubsystem.credential" type="Wfs.Raam.Core.Configuration.TrustedSubsystemCredentialSection, Wfs.Raam.Core"/>
    <section name="wfs.raam.core" type="Wfs.Raam.Core.Configuration.CoreSettingsSection, Wfs.Raam.Core"/>
  </configSections>
  <system.identityModel configSource="identity.app.config"/>
  <appSettings>
    <add key="siteKey" value="IPOnline"/>
    <add key="localIni" value="C:\WFSApps\RecHub\bin2\RecHub.ini"/>
    <add key="WCFConfigLocation" value="C:\WFSApps\RecHub\bin2\WCFClients.app.config"/>
  </appSettings>
  <!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.6" />
      </system.Web>
  -->
  <system.web>
    <httpRuntime targetFramework="4.6"/>
    <compilation targetFramework="4.6" debug="true"/>
  </system.web>
  <system.serviceModel>
    <serviceHostingEnvironment aspNetCompatibilityEnabled="true" multipleSiteBindingsEnabled="true">
      <serviceActivations>
        <add relativeAddress="ReportService.svc" service="WFS.RecHub.HubReport.ReportService"/>
      </serviceActivations>
    </serviceHostingEnvironment>
    <bindings>
      <ws2007FederationHttpBinding>
        <!-- Standard RAAM-Enabled HTTPS Binding -->
        <binding name="WfsRaamBinding" maxReceivedMessageSize="67108864" messageEncoding="Text">
          <readerQuotas maxDepth="2147483647" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647"/>
          <security mode="TransportWithMessageCredential">
            <message establishSecurityContext="false" issuedKeyType="BearerKey"/>
          </security>
        </binding>
      </ws2007FederationHttpBinding>
    </bindings>
    <services>
      <service behaviorConfiguration="Secure.ServiceBehavior" name="WFS.RecHub.HubReport.ReportService">
        <endpoint address="" binding="ws2007FederationHttpBinding" bindingConfiguration="WfsRaamBinding" contract="WFS.RecHub.HubReport.Common.IHubReportService"/>
      </service>
    </services>
    <behaviors>
      <serviceBehaviors>
        <behavior name="">
          <serviceMetadata httpGetEnabled="true" httpsGetEnabled="true"/>
          <serviceDebug includeExceptionDetailInFaults="false"/>
        </behavior>
        <behavior name="Secure.ServiceBehavior">
          <serviceDebug includeExceptionDetailInFaults="false"/>
          <serviceAuthorization principalPermissionMode="Always"/>
          <serviceCredentials useIdentityConfiguration="true"/>
        </behavior>
      </serviceBehaviors>
    </behaviors>
    <diagnostics>
      <messageLogging logEntireMessage="true" logMalformedMessages="true" logMessagesAtServiceLevel="true" logMessagesAtTransportLevel="true" maxMessagesToLog="3000" maxSizeOfMessageToLog="2000"/>
    </diagnostics>
  </system.serviceModel>
  <wfs.raam.core applicationcontextswitchenabled="true"/>
  <system.webServer>
    <modules runAllManagedModulesForAllRequests="true"/>
    <!--
        To browse web app root directory during debugging, set the value below to true.
        Set to false before deployment to avoid disclosing web app folder information.
      -->
    <directoryBrowse enabled="false"/>
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31BF3856AD364E35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages.Razor" publicKeyToken="31BF3856AD364E35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30AD4FE6B2A6AEED" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-11.0.0.0" newVersion="11.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Razor" publicKeyToken="31BF3856AD364E35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages.Deployment" publicKeyToken="31BF3856AD364E35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31BF3856AD364E35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.diagnostics>
    <trace autoflush="true"/>
    <sources>
      <!--  switchValue indicates level of logging service should record
        Critical        - 	  Logs the following exceptions: OutOfMemoryException,ThreadAbortException,StackOverflowException,
                              ConfigurationErrorsException, SEHException,Application start errors,Failfast events,System hangs,
                              Poison messages: message traces that cause the application to fail
        Error           -     All exceptions are logged.
        Warning         -     Events loggeC: The application is receiving more requests than its throttling settings allow, 
                              the receiving queue is near its maximum configured capacity, timeout has exceeded.Credentials are rejected
        Information     -     Messages helpful for monitoring and diagnosing system status, measuring performance or profiling are generated.
        Verbose         -     Positive events are logged. Events that mark successful milestones.Low level events for both user code and servicing are emitted.
        ActivityTracing -     Flow events between processing activities and components.
        All             -     All events are logged.
        Off             -     No logs.
      -->
      <source propagateActivity="true" name="System.ServiceModel" switchValue="Error,ActivityTracing">
        <listeners>
          <clear/>
          <add type="System.Diagnostics.DefaultTraceListener" name="Default">
            <filter type=""/>
          </add>
          <add name="CircularTraceListener" />
        </listeners>
      </source>
      <source name="Wfs.Raam.Core" switchName="Wfs.Raam.Core.Switch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <clear/>
          <add name="WfsRaamCoreTraceListener" initializeData="C:\WFSApps\Logs\RecHubReportingService.core.svclog" type="System.Diagnostics.XmlWriterTraceListener" traceOutputOptions="Timestamp"/>
        </listeners>
      </source>
    </sources>
    <switches>
      <add name="Wfs.Raam.Core.Switch" value="Verbose"/>
    </switches>
    <sharedListeners>
      <add name="CircularTraceListener" type="WFS.System.Tracing.CircularTraceListener, CircularTraceListener, Version=1.0, Culture=neutral"
     initializeData="C:\WFSApps\Logs\RecHubReportingService_Trace.svclog" maxFileSizeKB="5000" />
    </sharedListeners>
  </system.diagnostics>
</configuration>