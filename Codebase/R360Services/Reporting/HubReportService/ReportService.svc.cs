﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.ServiceModel;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.HubReportAPI;
using WFS.RecHub.R360Shared;
using Wfs.Raam.Core;
using System.Data;
using HubReportCommon.Responses;

/******************************************************************************
 * ** WAUSAU Financial Systems (WFS)
 * ** Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
 * *******************************************************************************
 * *******************************************************************************
 * ** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
 * *******************************************************************************
 * * Copyright c 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
 * * other trademarks cited herein are property of their respective owners.
 * * These materials are unpublished confidential and proprietary information 
 * * of WFS and contain WFS trade secrets.  These materials may not be used, 
 * * copied, modified or disclosed except as expressly permitted in writing by 
 * * WFS (see the WFS license agreement for details).  All copies, modifications 
 * * and derivative works of these materials are property of WFS.
 * *
* Author:   Ryan Schwantes
* Date:     03/11/2013
*
* Purpose:  Receivables Hub Report Service (WCF).
*
* Modification History
*   03/11/2013 RDS
*        - Created class
* * WI 107711 CEJ 10/16/2013
*   Create the ability log userActivity in the sessionLog from the R360 Services
******************************************************************************/
namespace WFS.RecHub.HubReport
{
  public class ReportService : LTAService, IHubReportService
  {
    #region IHubReportService

    private T FailOnError<T>(Func<ReportServiceAPI, T> operation, [CallerMemberName] string procName = null) where T : BaseResponse, new()
    {
      try
      {
        ReportServiceAPI r360API = new ReportServiceAPI(ServiceContext);
        r360API.LogEvent += EventLog.logEvent;
        return operation(r360API);
      }
      catch (Exception ex)
      {
        LogGeneralException(ex, procName);
        return new T() { Status = StatusCode.FAIL };
      }

      // TODO: Catch other exception types...
    }

	public PingResponse Ping()
	{
		return FailOnError((ignored) =>
		{
			EventLog.logEvent("Ping Received for " + OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString(), this.GetType().Name, RecHub.Common.MessageType.Information, RecHub.Common.MessageImportance.Debug);
			PingResponse response = new PingResponse();
			response.responseString = "Pong";
			response.Status = StatusCode.SUCCESS;
			return response;
		});
	}

    public ReportGroupsResponse GetReportGroups()
    {
      return FailOnError((reportService) =>
      {
        return reportService.GetReportGroups();
      });
    }

    public ReportConfigurationsResponse GetReportsForGroups()
    {
      return FailOnError((reportService) =>
      {
        return reportService.GetReportsForGroups();
      });
    }

    public ReportConfigurationResponse GetReportConfigurationById(int reportId)
    {
      return FailOnError((reportService) =>
      {
        return reportService.GetReportConfigurationById(reportId);
      });
    }

    public ReportConfigurationResponse GetReportConfigurationByName(string reportName)
    {
      return FailOnError((reportService) =>
      {
        return reportService.GetReportConfigurationByName(reportName);
      });
    }

    public ReportParametersResponse GetParametersForReport(int reportId)
    {
      return FailOnError((reportService) =>
      {
        return reportService.GetParametersForReport(reportId);
      });
    }

    #region Parameter Selection Values

    public AuditPaymentSourceResponse GetPaymentSourceList()
    {
        return FailOnError((reportService) =>
        {
            return reportService.GetPaymentSourceList();
        });
    }

    public AuditEventTypeResponse GetEventTypeList()
    {
        return FailOnError((reportService) =>
        {
            return reportService.GetAuditEventTypes();
        });
    }

    #endregion

	public InvokeReportResponse InvokeReportAsync(string reportName, List<ParameterValueDto> parameters)
	{
		return FailOnError((reportService) =>
		{
			return reportService.InvokeReportByNameAsync(reportName, parameters);
		});
	}

	public InvokeReportResponse InvokeReportByIdAsync(int reportId, List<ParameterValueDto> parameters)
	{
		return FailOnError((reportService) =>
		{
			return reportService.InvokeReportByIdAsync(reportId, parameters);
		});
	}

	public ReportInstanceResponse GetReportInstance(Guid instanceId)
    {
      return FailOnError((reportService) =>
      {
        return reportService.GetReportInstance(instanceId);
      });
    }

    public ByteArrayResponse GetReportPDF(Guid instanceId)
    {
      return FailOnError((reportService) =>
      {
        return reportService.GetReportPDF(instanceId);
      });
    }

    #endregion

    protected override APIBase NewAPIInst(R360Shared.R360ServiceContext serviceContext)
    {
      return new ReportServiceAPI(serviceContext);
    }
  }
}
