﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.HubReport.Common;

namespace WFS.RecHub.DAL
{
    public class HubReportDAL : _DALBase
	{
		public HubReportDAL(string vSiteKey)
            : base(vSiteKey)
		{
		}

        public bool GetUserIDBySID(Guid SID, out DataTable results)
        {
            bool bReturnValue = false;

            results = null;
            SqlParameter[] parms;
            try
            {
                parms = new SqlParameter[] {
                    BuildParameter("@parmSID", SqlDbType.UniqueIdentifier, SID, ParameterDirection.Input)
                };
                bReturnValue = Database.executeProcedure("RecHubUser.usp_Users_UserID_Get_BySID",
                        parms, out results);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetUserIDBySID(Guid SID, out DataTable results)");
                bReturnValue = false;
            }

            return bReturnValue;
        }

        public Guid GetSIDByUserID(int userId)
        {
            bool bReturnValue = false;
            var sid = Guid.Empty;
            DataTable results = null;
            SqlParameter[] parms;
            try
            {
                parms = new SqlParameter[] {
                    BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input)
                };
                bReturnValue = Database.executeProcedure("RecHubUser.usp_Users_Get_ByUserID",
                        parms, out results);
                if (results != null && results.Rows.Count > 0)
                {
                    sid = new Guid(Convert.ToString(results.Rows[0]["RA3MSID"]));
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "GetSIDByUserID(int userId, out DataTable results)");
                bReturnValue = false;
            }

            return sid;
        }

        #region Report Configuration
        public bool GetReportGroups(out DataTable dt)
        {
            dt = null;
            try
            {
                if (Database.executeProcedure("RecHubConfig.usp_ReportGroups_Get", out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetReportGroups");
            }

            return false;
        }

        public bool GetReportConfigurationForGroups(out DataTable dt)
        {
            dt = null;

            try
            {
                if (Database.executeProcedure("RecHubConfig.usp_ReportConfiguration_Get_ForGroups", out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetReportConfigurationForGroups");
            }

            return false;
        }

        public bool GetReportConfigurationByName(string reportName, out DataTable dt)
        {
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            dt = null;

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmReportName", SqlDbType.VarChar, reportName, ParameterDirection.Input));

                parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubConfig.usp_ReportConfiguration_Get_ByReportName", parms, out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetReportConfigurationByName");
            }

            return false;
        }

        public bool GetReportConfigurationById(int reportId, out DataTable dt)
        {
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            dt = null;

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmReportID", SqlDbType.Int, reportId, ParameterDirection.Input));

                parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubConfig.usp_ReportConfiguration_Get_ByReportId", parms, out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetReportConfigurationById");
            }

            return false;
        }

        public bool GetParametersForReport(int reportId, out DataTable dt)
        {
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            dt = null;
            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmReportID", SqlDbType.Int, reportId, ParameterDirection.Input));

                parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubConfig.usp_ReportParameter_Get_ByReportID", parms, out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetReportParameterList");
            }

            return false;
        }

        #endregion



        public bool AddReportInstance(int userId, int reportID, ReportStatus status, DateTime startTime, DateTime endTime, out ReportInstanceDto reportInstance)
        {
            SqlParameter[] parms;
            List<SqlParameter> arParms;
            SqlParameter parmReportInstanceKey;

            reportInstance = null;

            Guid instanceID = Guid.NewGuid();

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmInstanceID", SqlDbType.UniqueIdentifier, instanceID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmReportID", SqlDbType.Int, reportID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmReportStatus", SqlDbType.TinyInt, (byte)status, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmStartTime", SqlDbType.DateTime, startTime == DateTime.MinValue ? (object)DBNull.Value : startTime, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEndTime", SqlDbType.DateTime, endTime == DateTime.MinValue ? (object)DBNull.Value : endTime, ParameterDirection.Input));

                parmReportInstanceKey = BuildParameter("@parmReportInstanceKey", SqlDbType.BigInt, DBNull.Value, ParameterDirection.Output);
                arParms.Add(parmReportInstanceKey);

                parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubUser.usp_ReportInstance_Ins", parms))
                {
                    if (parmReportInstanceKey.Value != DBNull.Value)
                    {
                        reportInstance = new ReportInstanceDto()
                        {
                            ReportInstanceKey = (long)parmReportInstanceKey.Value,
                            InstanceID = instanceID,
                            UserID = userId,
                            ReportID = reportID,
                            Status = status,
                            StartTime = startTime,
                            EndTime = endTime
                        };
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "AddReportInstance");
            }

            return false;
        }


        public bool UpdateReportInstanceStatus(ReportInstanceDto reportInstance)
        {
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            try
            {
                DataTable dt = null;
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmInstanceID", SqlDbType.UniqueIdentifier, reportInstance.InstanceID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmReportStatus", SqlDbType.TinyInt, (int)reportInstance.Status, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEndTime", SqlDbType.DateTime, reportInstance.EndTime == DateTime.MinValue ? (object)DBNull.Value : reportInstance.EndTime, ParameterDirection.Input));

                parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubUser.usp_ReportInstance_Upd_Status", parms, out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "UpdateReportInstanceStatus");
            }

            return false;
        }

        public bool GetReportInstance(long reportInstanceKey, out DataTable dt)
        {
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            dt = null;

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmReportInstanceKey", SqlDbType.BigInt, reportInstanceKey, ParameterDirection.Input));

                parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubUser.usp_ReportInstance_Get_ByReportInstanceKey", parms, out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetReportInstance");
            }

            return false;
        }

        public bool GetReportInstanceByInstanceId(Guid instanceId, out DataTable dt)
        {
            SqlParameter[] parms;
            List<SqlParameter> arParms;

            dt = null;

            try
            {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmInstanceID", SqlDbType.UniqueIdentifier, instanceId, ParameterDirection.Input));

                parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubUser.usp_ReportInstance_Get_ByInstanceID", parms, out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetReportInstanceByInstanceId");
            }

            return false;
        }

		public bool GetAuditUsers(out DataTable dt)
		{
			dt = null;

			try
			{
				if (Database.executeProcedure("RecHubUser.usp_Users_Get_All", out dt))
					return true;
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().ToString(), "GetAuditUsers");
			}

			return false;
		}

        public bool GetPaymentSources(Guid sessionId, out DataTable dt)
        {
            dt = null;

            try
            {
                var arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmSessionID", SqlDbType.UniqueIdentifier, sessionId, ParameterDirection.Input));
                var parms = arParms.ToArray();
                if (Database.executeProcedure("RecHubData.usp_ClientAccountBatchSources_Get_BySessionID", parms, out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetPaymentSources");
            }

            return false;
        }

        public bool GetEventTypeParameter(out DataTable dt)
        {
            dt = null;
            try
            {
                if (Database.executeProcedure("RecHubAudit.usp_dimAuditEvent_Get_EventType", out dt))
                    return true;
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().ToString(), "GetEventTypeParameter");
            }

            return false;
        }
	}
}
