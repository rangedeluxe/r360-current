﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.HubReportAPI
{
    public class cReportSiteOptions : cReportServiceOptions
    {
        private static cReportServiceOptions _DefaultServicesOptions = null;
        private static cReportServiceOptions DefaultServicesOptions
        {
            get
            {
                if (_DefaultServicesOptions == null)
                    _DefaultServicesOptions = new cReportServiceOptions();
                return _DefaultServicesOptions;
            }
        }

        public cReportSiteOptions(string siteKey)
            : base(siteKey)
		{
		}

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public override string SSRS_URL
        {
            get
            {
                if (_SSRS_URL == null)
                    return DefaultServicesOptions.SSRS_URL;
                return _SSRS_URL;
            }
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public override int SSRS_Timeout
        {
            get
            {
                if (_SSRS_Timeout < 0)
                    return DefaultServicesOptions.SSRS_Timeout;
                return _SSRS_Timeout;
            }
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public override string SSRS_EncryptedReportUserName
        {
            get
            {
                if (_SSRS_EncryptedReportUserName == null)
                    return DefaultServicesOptions.SSRS_EncryptedReportUserName;
                return _SSRS_EncryptedReportUserName;
            }
        }


        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public override string SSRS_EncryptedReportUserPassword
        {
            get
            {
                if (_SSRS_EncryptedReportUserPassword == null)
                    return DefaultServicesOptions.SSRS_EncryptedReportUserPassword;
                return _SSRS_EncryptedReportUserPassword;
            }
        }


        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public override string SSRS_EncryptedReportUserDomain
        {
            get
            {
                if (_SSRS_EncryptedReportUserDomain == null)
                    return DefaultServicesOptions.SSRS_EncryptedReportUserDomain;
                return _SSRS_EncryptedReportUserDomain;
            }
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public override string SSRS_ReportPath
        {
            get
            {
                if (_SSRS_ReportPath == null)
                    return DefaultServicesOptions.SSRS_ReportPath;
                return _SSRS_ReportPath;
            }
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public override string ReportFileShare
        {
            get
            {
                if (_reportFileShare == null)
                    return DefaultServicesOptions.ReportFileShare;
                return _reportFileShare;
            }
        }
    }
}
