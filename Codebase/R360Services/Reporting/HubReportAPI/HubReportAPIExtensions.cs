﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.HubReport.Common;

namespace WFS.RecHub.HubReportAPI
{
    public static class HubReportAPIExtensions
    {
        public static ReportConfigurationDto ConvertToReportConfigurationDto(this DataRow dr)
        {
            ReportConfigurationDto reportConfig = null;

            if (dr.Table.Columns.Count > 0)
            {
                reportConfig = new ReportConfigurationDto();
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case "reportid":
                            reportConfig.ReportID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "groupid":
                            reportConfig.GroupID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "ordervalue":
                            reportConfig.OrderValue = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case "raamresourcename":
							reportConfig.RaamResourceName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "reportname":
                            reportConfig.ReportName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "xmlparametername":
                            reportConfig.XmlParameterName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "reportfilename":
                            reportConfig.ReportFileName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                    }
                }
            }
            return reportConfig;
        }

        public static ReportGroupDto ConvertToReportGroupDto(this DataRow dr)
        {
            ReportGroupDto group = new ReportGroupDto();
            if ( dr.Table.Columns.Count > 0 )
            {
                foreach ( DataColumn dc in dr.Table.Columns )
                {
                    switch ( dc.ColumnName.ToLower() )
                    {
					    case "groupid":
                            group.GroupID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
						    break;
					    case "ordervalue":
                            group.OrderValue = ipoLib.NVL(ref dr, dc.ColumnName, -1);
						    break;
					    case "groupname":
                            group.GroupName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
						    break;
                    }
                }
            }
            
            return group;
        }

        public static ReportParameterDto ConvertToReportParameterDto(this DataRow dr)
        {
            ReportParameterDto reportParm = new ReportParameterDto();
            if (dr.Table.Columns.Count > 0)
            {
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case "reportid":
                            reportParm.ReportID = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "ordervalue":
                            reportParm.OrderValue = ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "displayname":
                            reportParm.DisplayName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "parametername":
                            reportParm.ParameterName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                        case "parametertype":
                            try
                            {
                                reportParm.ParameterType = (ReportParameterType)ipoLib.NVL(ref dr, dc.ColumnName, (int)ReportStatus.None);
                            }
                            catch
                            {
                                reportParm.ParameterType = ReportParameterType.None;
                            }
                            break;
                        case "isrequired":
                            reportParm.IsRequired = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                        case "allowmultiple":
                            reportParm.AllowMultiple = ipoLib.NVL(ref dr, dc.ColumnName, false);
                            break;
                    }
                }
            }
            return reportParm;
        }

        public static ReportInstanceDto ConvertToReportInstanceDto(this DataRow dr)
        {
            ReportInstanceDto instance = new ReportInstanceDto();
            if (dr.Table.Columns.Count > 0)
            {
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case "reportinstancekey":
                            //TODO: Add a method to the ipoLib to support long values
                            //_ReportInstanceKey = ipoLib.NVL(ref dr, dc.ColumnName, 0L);
                            instance.ReportInstanceKey = dr.IsNull(dc.ColumnName) ? 0L : (long)dr[dc.ColumnName];
                            break;
                        case "instanceid":
                            instance.InstanceID = ipoLib.NVL(ref dr, dc.ColumnName, Guid.Empty);
                            break;
                        case "userid":
                            instance.UserID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case "reportid":
                            instance.ReportID = ipoLib.NVL(ref dr, dc.ColumnName, -1);
                            break;
                        case "reportstatus":
                            try
                            {
                                instance.Status = (ReportStatus)ipoLib.NVL(ref dr, dc.ColumnName, (byte)ReportStatus.None);
                            }
                            catch
                            {
                                instance.Status = ReportStatus.None;
                            }
                            break;
                        case "starttime":
                            instance.StartTime = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                        case "endtime":
                            instance.EndTime = ipoLib.NVL(ref dr, dc.ColumnName, DateTime.MinValue);
                            break;
                    }
                }
            }
            return instance;
        }

        public static AuditPaymentSourceDto ConvertToAuditPaymentSourceDto(this DataRow dr)
        {
            AuditPaymentSourceDto paymentSourceDto = new AuditPaymentSourceDto();
            if (dr.Table.Columns.Count > 0)
            {
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    switch (dc.ColumnName.ToLower())
                    {
                        case "batchsourcekey":
                            paymentSourceDto.ID = Convert.ToInt32(dr[dc.ColumnName].ToString()); //ipoLib.NVL(ref dr, dc.ColumnName, 0);
                            break;
                        case "longname":
                            paymentSourceDto.ShortName = ipoLib.NVL(ref dr, dc.ColumnName, string.Empty);
                            break;
                    }
                }
            }
            return paymentSourceDto;
        }
	}
}
