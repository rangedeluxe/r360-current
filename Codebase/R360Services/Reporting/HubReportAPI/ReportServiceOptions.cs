﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.Common.Crypto;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Ryan Schwantes
* Date:     03/11/2013
*
* Purpose:  Receivables Hub Report Service - Service Options.
*
* Modification History
*   03/11/2013 RDS
*        - Created class
******************************************************************************/
namespace WFS.RecHub.HubReportAPI
{
	public class cReportServiceOptions
	{
        private const string INISECTION_HUB_REPORT_SERVICE = "HubReportSvc"; 
        
        internal const string INIKEY_HOST_WCF_HUBSERVICES = "HostWCFHubReportServices";
        internal const string INIKEY_HUBSERVICE_URL = "HubReportService_URL";
        internal const string INIKEY_EXPOSEWSDL = "ExposeWSDL";
        internal const string INIKEY_SSRS_URL = "SSRS_URL";
        internal const string INIKEY_SSRS_TIMEOUT = "SSRS_Timeout";
        internal const string INIKEY_SSRS_REPORT_USER_NAME = "SSRS_ReportUserName";
        internal const string INIKEY_SSRS_REPORT_USER_PASSWORD = "SSRS_ReportUserPassword";
        internal const string INIKEY_SSRS_REPORT_USER_DOMAIN = "SSRS_ReportUserDomain";
        internal const string INIKEY_SSRS_REPORT_PATH = "SSRS_ReportPath";
        internal const string INIKEY_REPORT_FILE_SHARE = "ReportFileShare";

		internal static readonly List<string> lstTrueValues = new List<string>(new string[] {"true", "t", "yes", "1"});

        //These values can be overriden by a derived class as they can change by site
        //I.E. The default HubReportSvc section should contain them, but the Site Section can contain overrides.
        protected string _SSRS_URL = null;
        protected int _SSRS_Timeout = -1;
        protected string _SSRS_EncryptedReportUserName = null;
        protected string _SSRS_EncryptedReportUserPassword = null;
        protected string _SSRS_EncryptedReportUserDomain = null;
        protected string _SSRS_ReportPath = null;
        protected string _reportFileShare = null;

        public cReportServiceOptions()
        {
            LoadReportServiceOptions(INISECTION_HUB_REPORT_SERVICE);

            SetDefaultsAsNeeded();
        }

		public cReportServiceOptions(string siteKey)
		{
            LoadReportServiceOptions(siteKey);
        }

        protected void LoadReportServiceOptions(string section)
        {
            StringCollection colSiteOptions = ipoINILib.GetINISection(section);
			string strKey;
			string strValue;

			foreach (var entry in colSiteOptions)
			{
				strKey = entry.Substring(0, entry.IndexOf('='));
				strValue = entry.Substring(strKey.Length + 1);

				if (string.Compare(strKey, INIKEY_HOST_WCF_HUBSERVICES, StringComparison.OrdinalIgnoreCase) == 0)
				{
					Host_WCF_HubReportServices = lstTrueValues.Contains(strValue, StringComparer.OrdinalIgnoreCase);
				}
				else if (string.Compare(strKey, INIKEY_HUBSERVICE_URL, StringComparison.OrdinalIgnoreCase) == 0)
				{
					HubReportService_URL = strValue;
				}
                else if (string.Compare(strKey, INIKEY_EXPOSEWSDL, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    Expose_WSDL = lstTrueValues.Contains(strValue, StringComparer.OrdinalIgnoreCase);
                }
                else if (string.Compare(strKey, cReportServiceOptions.INIKEY_SSRS_URL, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    _SSRS_URL = strValue;
                }
                else if (string.Compare(strKey, cReportServiceOptions.INIKEY_SSRS_TIMEOUT, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    int timeout = 0;
                    if (!int.TryParse(strValue, out timeout))
                        _SSRS_Timeout = timeout;
                }
                else if (string.Compare(strKey, cReportServiceOptions.INIKEY_SSRS_REPORT_USER_NAME, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    _SSRS_EncryptedReportUserName = strValue;
                }
                else if (string.Compare(strKey, cReportServiceOptions.INIKEY_SSRS_REPORT_USER_PASSWORD, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    _SSRS_EncryptedReportUserPassword = strValue;
                }
                else if (string.Compare(strKey, cReportServiceOptions.INIKEY_SSRS_REPORT_USER_DOMAIN, StringComparison.OrdinalIgnoreCase) == 0)
                {
                     _SSRS_EncryptedReportUserDomain = strValue;
                }
                else if (string.Compare(strKey, cReportServiceOptions.INIKEY_SSRS_REPORT_PATH, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    if (strValue.Length > 0)
                    {
                        //make sure the report path is in a valid format
                        _SSRS_ReportPath = string.Empty;
                        if (strValue[0] != '/')
                            _SSRS_ReportPath = "/";
                        _SSRS_ReportPath += strValue;
                        if (_SSRS_ReportPath[_SSRS_ReportPath.Length - 1] != '/')
                            _SSRS_ReportPath += "/";
                    }
                }
                else if (string.Compare(strKey, cReportServiceOptions.INIKEY_REPORT_FILE_SHARE, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    _reportFileShare = strValue;
                }
			}
		}

        private void SetDefaultsAsNeeded()
        {
            if (_SSRS_Timeout <= 0)
                _SSRS_Timeout = 5 * 60 * 1000;//default to 5 minutes
        }

        //************************************************************************
        /// <author>Ryan Schwantes</author>
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public bool Host_WCF_HubReportServices
        {
            get;
            set;
        }

        //************************************************************************
        /// <author>Ryan Schwantes</author> 
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public string HubReportService_URL
        {
            get;
            set;
        }

        //************************************************************************
        /// <author>Ryan Schwantes</author> 
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public bool Expose_WSDL
        {
            get;
            set;
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>
        //************************************************************************
        public virtual string SSRS_URL
        {
            get { return _SSRS_URL; }
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public virtual int SSRS_Timeout
        {
            get { return _SSRS_Timeout; }
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public virtual string SSRS_EncryptedReportUserName
        {
            get { return _SSRS_EncryptedReportUserName; }
        }


        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public virtual string SSRS_EncryptedReportUserPassword
        {
            get { return _SSRS_EncryptedReportUserPassword; }
        }


        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public virtual string SSRS_EncryptedReportUserDomain
        {
            get { return _SSRS_EncryptedReportUserDomain; }
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public virtual string SSRS_ReportPath
        {
            get { return _SSRS_ReportPath; }
        }

        //************************************************************************
        /// <author>Kyle Colden</author> 
        /// <summary>
        /// 
        /// </summary>  
        //************************************************************************
        public virtual string ReportFileShare
        {
            get { return _reportFileShare; }
        }

        //************************************************************************
		/// <author>Ryan Schwantes</author> 
		/// <summary>
		/// 
		/// </summary>
		//************************************************************************
		public string[] CheckSettings()
		{
			List<string> lstErrors = new List<string>();

			if (Host_WCF_HubReportServices)
			{
				if (string.IsNullOrWhiteSpace(HubReportService_URL))
				{
					lstErrors.Add("The WCF Host setting is on but no Service URL is specified");
				}
				else
				{
					if (!Uri.IsWellFormedUriString(HubReportService_URL, UriKind.Absolute))
					{
						lstErrors.Add(string.Format("{0} is an invalid URL", INIKEY_HUBSERVICE_URL));
					}
				}
			}
			return lstErrors.ToArray();
		}
	}
}
