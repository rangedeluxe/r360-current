using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using WFS.RecHub.DAL;
using WFS.RecHub.Common;
using WFS.RecHub.R360Shared;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Net;
using WFS.RecHub.Common.Crypto;
using WFS.RecHub.HubReportAPI.SqlReportingExecution2005;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360RaamClient;
using Wfs.Raam.Service.Authorization.Contracts.DataContracts;
using System.Xml.Linq;
using HubReportCommon.Responses;
using System.Security.Claims;
using Wfs.Raam.Core;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Ryan Schwantes
* Date:     03/11/2013
*
* Purpose:  Receivables Hub Report Service - Service class.
*
* Modification History
*   03/11/2013 RDS
*        - Created class
* WI 107711 CEJ 10/17/2013
*   Create the ability log userActivity in the sessionLog from the R360 Services
******************************************************************************/
namespace WFS.RecHub.HubReportAPI
{
    public class ReportServiceAPI : APIBase
    {
        private HubReportDAL _ReportDAL = null;

        public ReportServiceAPI(R360ServiceContext serviceContext)
            : base(serviceContext)
        {
            if (R360Shared.LogManager.IsDefault) R360Shared.LogManager.Logger = new R360Shared.ConfigHelpers.Logger(o => OnLogEvent(o, this.GetType().Name, MessageType.Information, MessageImportance.Verbose), o => OnLogEvent(o, this.GetType().Name, MessageType.Warning, MessageImportance.Essential));
        }

        protected override bool ValidateSID(out int userID)
        {
            bool bReturnValue = false;
            userID = -1;
            DataTable dtResults;

            if (ReportDAL.GetUserIDBySID(_ServiceContext.GetSID(), out dtResults))
            {
                userID = Convert.ToInt32(dtResults.Rows[0]["UserID"]);
                bReturnValue = true;
            }

            return bReturnValue;
        }

        protected virtual T ValidateUserIdAndDo<T>(Action<T, int> operation, [CallerMemberName] string procName = null)
            where T : BaseResponse, new()
        {
            // Initialize result
            T result = new T();
            result.Status = StatusCode.FAIL;

            try
            {
                // Validate / get the user ID
                int userID;
                if (ValidateSIDAndLog(procName, out userID))
                {
                    LogActivity(procName);
                    // Perform the requested operation
                    operation(result, userID);
                }
                else
                {
                    result.Errors.Add("Invalid SID");
                }
            }
            catch (Exception ex)
            {
                // Log exception, and return a generic message since we don't know what it says
                result.Errors.Add("Unexpected error in " + procName);
                OnErrorEvent(ex, this.GetType().Name, procName);
            }

            return result;
        }

        public ReportGroupsResponse GetReportGroups()
        {
            return ValidateUserIdAndDo<ReportGroupsResponse>((response, userId) =>
            {
                //Call DAL to get the report groups
                DataTable dt = null;
                if (ReportDAL.GetReportGroups(out dt))
                {
                    //Nothing to tie into for permissions here, the permissions are applied by report

                    // Convert to DTO Type
                    response.ReportGroups = new List<ReportGroupDto>(dt.Rows.Count);
                    foreach (DataRow row in dt.Rows)
                    {
                        response.ReportGroups.Add(row.ConvertToReportGroupDto());
                    }

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public ReportConfigurationsResponse GetReportsForGroups()
        {
            var userIdClaim = ClaimsPrincipal.Current.FindFirst(WfsClaimTypes.UserId);

            return ValidateUserIdAndDo<ReportConfigurationsResponse>((response, userId) =>
            {
                //Call DAL to get the report groups
                DataTable dt = null;
                if (ReportDAL.GetReportConfigurationForGroups(out dt))
                {
                    // Convert to DTO Type
                    List<ReportConfigurationDto> reportConfigs;
                    LoadReportConfigurationDto(userId, dt, out reportConfigs);
                    response.ReportConfigurations = reportConfigs;

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public ReportConfigurationResponse GetReportConfigurationById(int reportId)
        {
            return ValidateUserIdAndDo<ReportConfigurationResponse>((response, userId) =>
            {
                //Call DAL to get the report groups
                ReportConfigurationDto reportConfig;
                if (GetReportConfigurationById(userId, reportId, out reportConfig))
				{
                    response.ReportConfiguration = reportConfig;

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public ReportConfigurationResponse GetReportConfigurationByName(string reportName)
        {
            return ValidateUserIdAndDo<ReportConfigurationResponse>((response, userId) =>
            {
                ReportConfigurationDto reportConfig;
                if (GetReportConfigurationByName(userId, reportName, out reportConfig))
                {
                    response.ReportConfiguration = reportConfig;

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public ReportParametersResponse GetParametersForReport(int reportId)
        {
            return ValidateUserIdAndDo<ReportParametersResponse>((response, userId) =>
            {
                List<ReportParameterDto> reportParameters;
                if (GetParametersForReport(reportId, out reportParameters))
                {
                    response.ReportParameters = reportParameters;

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }



        public AuditPaymentSourceResponse GetPaymentSourceList()
        {
            return ValidateUserIdAndDo<AuditPaymentSourceResponse>((response, userId) =>
            {
                response.AuditPaymentSources = null;

                DataTable paymentSourcesDT;

                if (ReportDAL.GetPaymentSources(base.GetSession(), out paymentSourcesDT))
                {
                    response.AuditPaymentSources = new List<AuditPaymentSourceDto>(paymentSourcesDT.Rows.Count);

                    for (int i = 0; i < paymentSourcesDT.Rows.Count; i++)
                    {
                        response.AuditPaymentSources.Add(paymentSourcesDT.Rows[i].ConvertToAuditPaymentSourceDto());
                    }

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

		public InvokeReportResponse InvokeReportByNameAsync(string reportName, List<ParameterValueDto> parameters)
		{
			return ValidateUserIdAndDo<InvokeReportResponse>((response, userId) =>
			{
				ReportConfigurationDto reportConfig = null;
				if (!GetReportConfigurationByName(userId, reportName, out reportConfig))
					return;

				InvokeReportAsync(userId, reportConfig, parameters, response);
			});
		}

		public InvokeReportResponse InvokeReportByIdAsync(int reportID, List<ParameterValueDto> parameters)
        {
            return ValidateUserIdAndDo<InvokeReportResponse>((response, userId) =>
            {
                ReportConfigurationDto reportConfig = null;
				if (!GetReportConfigurationById(userId, reportID, out reportConfig))
                    return;

				InvokeReportAsync(userId, reportConfig, parameters, response);
            });
        }

		public void InvokeReportAsync(int userId, ReportConfigurationDto reportConfig, List<ParameterValueDto> parameters, InvokeReportResponse response)
        {
            List<ReportParameterDto> reportParameters = null;
            ReportInstanceDto reportInstance = null;
            string parametersXML = null;

            if (!GetParametersForReport(reportConfig.ReportID, out reportParameters))
                return;

            if (!ValidateParameterList(reportParameters, parameters))
                return;

            AddEntityBreadcrumbs(reportParameters, parameters);

            ParseSpecial(reportParameters, parameters);

            parametersXML = BuildParameterXML(reportParameters, parameters, reportConfig.ReportName);

            if (!ReportDAL.AddReportInstance(userId, reportConfig.ReportID, ReportStatus.InProgress, DateTime.Now, DateTime.MinValue, out reportInstance))
                return;

            if (reportInstance.InstanceID != Guid.Empty)
            {
                Task.Factory.StartNew(() =>
                {
                    GenerateReportAsync(SiteKey, GetSession(), ReportDAL, reportInstance, reportConfig, parametersXML);
                });
            }

            response.ReportInstanceId = reportInstance.InstanceID;

            // Return success
            response.Status = StatusCode.SUCCESS;
        }

        public ReportInstanceResponse GetReportInstance(Guid instanceId)
        {
            return ValidateUserIdAndDo<ReportInstanceResponse>((response, userId) =>
            {
                ReportInstanceDto reportInstance;
                if (GetReportInstance(userId, instanceId, out reportInstance))
                {
                    response.ReportInstance = reportInstance;

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public ByteArrayResponse GetReportPDF(Guid instanceId)
        {
            return ValidateUserIdAndDo<ByteArrayResponse>((response, userId) =>
            {
                ReportInstanceDto reportInstance = null;
                if (GetReportInstance(userId, instanceId, out reportInstance))
                {
                    if (reportInstance.Status == ReportStatus.Completed)
                    {
                        cReportSiteOptions siteOptions = new cReportSiteOptions(SiteKey);

                        string reportFilePath = GetReportFilePath(siteOptions.ReportFileShare, reportInstance.InstanceID);

                        response.Bytes = File.ReadAllBytes(reportFilePath);
                        response.MimeType = GetReportMimeType(reportFilePath);

                        //update the report instanced to loaded
                        reportInstance.Status = ReportStatus.Loaded;
                        ReportDAL.UpdateReportInstanceStatus(reportInstance);

                        try
                        {
                            File.Delete(reportFilePath);
                        }
                        catch (Exception ex)
                        {
                            OnErrorEvent(ex, this.GetType().ToString(), "GetReport");
                        }

                        // Return success
                        response.Status = StatusCode.SUCCESS;
                    }
                }
            });
        }

        public AuditEventTypeResponse GetAuditEventTypes()
        {
            return ValidateUserIdAndDo<AuditEventTypeResponse>((response, userId) =>
            {
                response.EventTypes = null;

                DataTable eventTypesDT;

                if (ReportDAL.GetEventTypeParameter(out eventTypesDT))
                {
                    response.EventTypes = new List<string>(eventTypesDT.Rows.Count);

                    for (int i = 0; i < eventTypesDT.Rows.Count; i++)
                    {
                        response.EventTypes.Add(eventTypesDT.Rows[i][0].ToString());
                    }

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
            });
        }

        public AuditEventTypeResponse GetRAAMAuditEventTypes()
        {
            return ValidateUserIdAndDo<AuditEventTypeResponse>((response, userId) =>
            {
                response.EventTypes = null;

                List<string> raamEventTypes = null; // CAll RAAMCLient Here
                if (raamEventTypes != null && raamEventTypes.Count > 0)
                {
                    response.EventTypes = new List<string>();
                    response.EventTypes.AddRange(raamEventTypes);

                    // Return success
                    response.Status = StatusCode.SUCCESS;
                }
                else
                {
                    response.Status = StatusCode.FAIL;
                }
            });
        }

        #region Private Helper Methods
		private bool GetReportConfigurationById(int userId, int reportId, out ReportConfigurationDto reportConfig)
		{
			reportConfig = null;

			//Call DAL to get the report groups
			DataTable dt = null;
			if (ReportDAL.GetReportConfigurationById(reportId, out dt))
				return LoadReportConfigurationDto(userId, dt, out reportConfig);
			return false;
		}

        private bool GetReportConfigurationByName(int userId, string reportName, out ReportConfigurationDto reportConfig)
        {
            reportConfig = null;

            //Call DAL to get the report groups
            DataTable dt = null;
            if (ReportDAL.GetReportConfigurationByName(reportName, out dt))
                return LoadReportConfigurationDto(userId, dt, out reportConfig);
            return false;
        }

        private bool LoadReportConfigurationDto(int userId, DataTable dt, out ReportConfigurationDto reportConfig)
        {
            List<ReportConfigurationDto> reportConfigs;
            if (LoadReportConfigurationDto(userId, dt, out reportConfigs))
            {
                if (reportConfigs.Count > 0)
                {
                    reportConfig = reportConfigs[0];
                    return true;
                }
            }
            reportConfig = null;
            return false;
        }

        private bool LoadReportConfigurationDto(int userId, DataTable dt, out List<ReportConfigurationDto> reportConfigs)
        {
            reportConfigs = null;

            ReportConfigurationDto config = null;
            // Convert to DTO Type
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    config = row.ConvertToReportConfigurationDto();
                    // TODO: Convert to RAAM Permmissions
                    if (R360Permissions.Current.Allowed(config.RaamResourceName, R360Permissions.ActionType.View))
                    {
                        if (reportConfigs == null)
                            reportConfigs = new List<ReportConfigurationDto>();
                        reportConfigs.Add(config);
                    }
                    else
                    {
                        OnLogEvent(string.Format("User {0} does not have the required permissions {1}", userId, config.RaamResourceName), this.GetType().ToString(), MessageType.Information, MessageImportance.Debug);
                    }
                }
            }
            return reportConfigs != null && reportConfigs.Count > 0;
        }

        private bool GetParametersForReport(int reportId, out List<ReportParameterDto> parameters)
        {
            parameters = null;

            //Call DAL to get the report paramters
            DataTable dt = null;
            if (ReportDAL.GetParametersForReport(reportId, out dt))
            {
                //Convert to DTO Type
                parameters = new List<ReportParameterDto>(dt.Rows.Count);
                foreach (DataRow row in dt.Rows)
                {
                    parameters.Add(row.ConvertToReportParameterDto());
                }
                return true;
            }
            return false;
        }

        private bool GetReportInstance(int userId, Guid instanceId, out ReportInstanceDto reportInstance)
        {
            reportInstance = null;

            //Call DAL to get the report instance
            DataTable dt = null;
            if (ReportDAL.GetReportInstanceByInstanceId(instanceId, out dt))
            {
                //Convert to DTO Type
                if (dt.Rows.Count > 0)
                {
                    ReportInstanceDto instance = dt.Rows[0].ConvertToReportInstanceDto();
                    if (instance.UserID == userId)
                    {
                        reportInstance = instance;
                        return true;
                    }

                    OnLogEvent(string.Format("Report instance {0} belonging to user {1} was attempted to be loaded by user {2}", instanceId, instance.UserID, userId), "GetReportInstance", MessageType.Warning, MessageImportance.Essential);
                }
            }
            return false;
        }

        private bool ValidateParameterList(List<ReportParameterDto> reportParameters, List<ParameterValueDto> selectedParameters)
        {
            //first verify that we have the required parameters
            if (reportParameters != null)
            {
                foreach (ReportParameterDto parm in reportParameters)
                {
                    ParameterValueDto selParm = null;
                    if (selectedParameters != null)
                        selParm = selectedParameters.Find(o => o.Name == parm.ParameterName);

                    bool hasValue = false;
                    if (selParm != null)
                    {
                        if (parm.AllowMultiple)
                        {
                            if (selParm.ValueList != null && selParm.ValueList.Count > 0 && !string.IsNullOrEmpty(selParm.ValueList[0]))
                                hasValue = true;
                            else if (!string.IsNullOrEmpty(selParm.Value))
                            {
                                OnLogEvent(string.Format("Multiple select parameter expected for paremeter {0}, but a single value type was sent.", parm.ParameterName), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                                return false;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(selParm.Value))
                                hasValue = true;
                            else if (selParm.ValueList != null && selParm.ValueList.Count > 0 && !string.IsNullOrEmpty(selParm.ValueList[0]))
                            {
                                OnLogEvent(string.Format("Single select parameter expected for paremeter {0}, but a multiple select type was sent.", parm.ParameterName), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                                return false;
                            }
                        }
                    }

                    if (parm.IsRequired && !hasValue)
                    {
                        OnLogEvent(string.Format("Required paremeter {0} was not supplied.", parm.ParameterName), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        return false;
                    }
                }
            }

            //next verify the parameters we sent in belong to this report
            if (selectedParameters != null)
            {
                foreach (ParameterValueDto selParm in selectedParameters)
                {
                    //we already validated all the parameters that we expect and their values, so just make sure we didn't send extra
                    if (!reportParameters.Exists(o => o.ParameterName == selParm.Name))
                    {
                        OnLogEvent(string.Format("Extra, unexpected parameter {0} was supplied.", selParm.Name), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        return false;
                    }
                }
            }
            return true;
        }

        private string BuildParameterXML(List<ReportParameterDto> reportParameters, List<ParameterValueDto> selectedParameters, string reportName)
        {
            StringBuilder sb = new StringBuilder();
            var entityId = ClaimsPrincipal.Current.FindFirst(WfsClaimTypes.EntityId);
            sb.Append("<Parameters>");

            if (selectedParameters != null)
            {
                foreach (ParameterValueDto selParm in selectedParameters)
                {
                    ReportParameterDto parm = reportParameters.Find(o => o.ParameterName == selParm.Name);
                    if (parm == null)
                    {
                        OnLogEvent(string.Format("Parameter {0} was unexpectedly supplied", selParm.Name), this.GetType().ToString(), MessageType.Information, MessageImportance.Verbose);
                        continue;
                    }

                    if (parm.AllowMultiple && selParm.ValueList != null)
                    {
                        sb.Append("<" + selParm.Name + "s>");
                        sb.Append(string.Concat(selParm.ValueList.Select(o => "<" + selParm.Name + ">" + o + "</" + selParm.Name + ">")));
                        sb.Append("</" + selParm.Name + "s>");
                    }
                    else if (!parm.AllowMultiple && selParm.Value != null)
                    {
                        sb.Append("<" + selParm.Name + ">" + selParm.Value + "</" + selParm.Name + ">");
                    }
                }
                //This parameter is added here instead of the db to avoid a security risk and unnecesary overhead
                if (reportName == "Extract Audit Report" && entityId != null)
                {
                    sb.Append("<ENT>" + entityId.Value + "</ENT>");
                }
            }

            sb.Append("</Parameters>");
            return sb.ToString();
        }

        private string GetGeneratedType(string xmlParameters)
        {
            List<XElement> generatedTypeElement = new List<XElement>();
            XDocument xmldoc = XDocument.Parse(xmlParameters);
            generatedTypeElement.AddRange(xmldoc.Root.Elements("GT").ToList());

            if (generatedTypeElement.Count > 0)
            {
                if (generatedTypeElement[0].Value.ToUpper() == "CSV")
                    return "CSV";
                else
                    return "PDF";
            }
            else
                return "PDF"; // Default action
        }

        private string GetReportFilePath(string reportDirectory, Guid reportInstanceID)
        {
            string[] files = Directory.GetFiles(reportDirectory, reportInstanceID.ToString() + ".*");

            return files[0];
        }

        private string GetReportMimeType(string reportFilePath)
        {
            switch (Path.GetExtension(reportFilePath))
            {
                case ".csv":
                    return "text/csv";
                default:
                    return "application/pdf";
            }
        }

        #endregion

        #region SSRS Integration

        private void GenerateReportAsync(string siteKey, Guid sessionID, HubReportDAL reportDAL, ReportInstanceDto reportInstance, ReportConfigurationDto reportConfig, string parametersXml)
        {
            try
            {
                GenerateReport(siteKey, sessionID, reportInstance, reportConfig, parametersXml);
                reportInstance.Status = ReportStatus.Completed;
            }
            catch (Exception ex)
            {
                reportInstance.Status = ReportStatus.Failed;
                OnLogEvent("Error generating report: " + ex.ToString(), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                throw;
            }
            finally
            {
                reportInstance.EndTime = DateTime.Now;

                //if this fails, there's not much we can do, the client's going to keep calling in and
                // seeing that the status is in progress until they give up.
                reportDAL.UpdateReportInstanceStatus(reportInstance);
            }
        }

        private void GenerateReport(string siteKey, Guid sessionID, ReportInstanceDto reportInstance, ReportConfigurationDto reportConfig, string xmlParameters)
        {
            cReportSiteOptions siteOptions = new cReportSiteOptions(siteKey);
            var userIdClaim = ClaimsPrincipal.Current.FindFirst(WfsClaimTypes.UserId);

            // Validate / complete the XML parameters
            if (!xmlParameters.Contains("<Parameters>"))
                xmlParameters = "<Parameters>" + xmlParameters + "</Parameters>";

            string generateType = GetGeneratedType(xmlParameters);

            Byte[] result = null;

            try
            {
                OnLogEvent(string.Format("Generating Report: {0},{1},{2}", reportConfig.ReportName, reportInstance.UserID, xmlParameters), this.GetType().ToString(), MessageType.Information, MessageImportance.Verbose);
                SqlReportingExecution2005.ReportExecutionService rs2 = new SqlReportingExecution2005.ReportExecutionService();

                rs2.Url = siteOptions.SSRS_URL;
                rs2.Timeout = siteOptions.SSRS_Timeout;

                // Assign credentials
                rs2.Credentials = GetCredentials(siteOptions);

                // Create Out Parameters
                string encoding;
                string mimetype;
                string fileExtension;
                Warning[] warnings;
                string[] streamids;
                string deviceinfo = "<DeviceInfo>" +
                                    "<HTMLFragment>True</HTMLFragment>" +
                                    "<StreamRoot>/</StreamRoot>" +
                                    "</DeviceInfo>";

                // Passing the Parameters to the Report
                // Create an array of the values for the report parameters
                // Also sets the XmlParameterName configured for the report with the xmlParameters argument
                ParameterValue[] parametersIn = new ParameterValue[1];

                ParameterValue paramValue = new ParameterValue();
                paramValue.Label = reportConfig.XmlParameterName;
                paramValue.Name = reportConfig.XmlParameterName;
                paramValue.Value = xmlParameters;

                parametersIn[0] = paramValue;

                SqlReportingExecution2005.ExecutionHeader exHeader = new SqlReportingExecution2005.ExecutionHeader();
                rs2.ExecutionHeaderValue = exHeader;

                //We need this or the report won't run.  We just end up setting the report parameters again
                //so I'm not exactly sure why this has to happen twice.
                SqlReportingExecution2005.ExecutionInfo exInfo = new SqlReportingExecution2005.ExecutionInfo();

                exInfo = rs2.LoadReport(siteOptions.SSRS_ReportPath + reportConfig.ReportFileName, null);

                //Populate the parameters for the report
                if (exInfo.Parameters.Length > 0)
                {
                    parametersIn = new ParameterValue[exInfo.Parameters.Length];
                    for (int i = 0; i < exInfo.Parameters.Length; i++)
                    {
                        parametersIn[i] = new ParameterValue()
                        {
                            Label = exInfo.Parameters[i].Name,
                            Name = exInfo.Parameters[i].Name
                        };

                        if (parametersIn[i].Name == reportConfig.XmlParameterName)
                            parametersIn[i].Value = xmlParameters;
                        else if (parametersIn[i].Name == "parmUserID")
                            parametersIn[i].Value = reportInstance.UserID.ToString();
                        else if (parametersIn[i].Name == "parmRAAMUserID")
                            parametersIn[i].Value = userIdClaim.Value.ToString();
                        else if (parametersIn[i].Name == "parmSessionID")
                            parametersIn[i].Value = sessionID.ToString();
                    }
                }

                rs2.SetExecutionParameters(parametersIn, "en-us");

                //Get the report output based on the output type.  We don't do anything with the output parameters at this time
                result = rs2.Render(generateType, deviceinfo, out fileExtension, out mimetype, out encoding, out warnings, out streamids);

                File.WriteAllBytes(Path.Combine(siteOptions.ReportFileShare, reportInstance.InstanceID.ToString() + "." + fileExtension), result);

                System.Threading.Thread.Sleep(10000);
            }
            catch (Exception ex)
            {
                OnErrorEvent(ex, this.GetType().ToString(), "GenerateReport");
                OnLogEvent(string.Format("GenerateReport failed for {0}, user id {1}, parameters {2}", reportConfig.ReportName, reportInstance.UserID, xmlParameters), this.GetType().ToString(), MessageType.Information, MessageImportance.Debug);

                throw new ApplicationException("Error Getting Report -- " + ex.Message, ex);
            }
        }

        /// <summary>
        /// This helper generates the credentials used when accessing SSRS, based on the
        /// report user, password, and domain configured
        /// </summary>
        /// <returns></returns>
        internal ICredentials GetCredentials(cReportSiteOptions siteOptions)
        {
            if (siteOptions.SSRS_EncryptedReportUserName == null)
                return CredentialCache.DefaultCredentials;
            else
            {
                NetworkCredential defaultCred = new NetworkCredential();
                defaultCred.UserName = Decrypt(siteOptions.SSRS_EncryptedReportUserName);
                defaultCred.Password = Decrypt(siteOptions.SSRS_EncryptedReportUserPassword);
                string domain = Decrypt(siteOptions.SSRS_EncryptedReportUserDomain);
                if (domain.Length > 0)
                    defaultCred.Domain = domain;
                return defaultCred;
            }
        }

        private string Decrypt(string encrypted)
        {
            try
            {
                cCrypto3DES cryp3DES = new cCrypto3DES();
                string decrypted = null;
                if (!cryp3DES.Decrypt(encrypted, out decrypted))
                    OnLogEvent("Unable to decrypt string: " + cryp3DES.LastException.Message, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                return decrypted;
            }
            catch (Exception ex)
            {
                OnErrorEvent(ex, this.GetType().Name, "Decrypt");
                return null;
            }
        }

        private void ParseSpecial(List<ReportParameterDto> reportParameters, List<ParameterValueDto> parameters)
        {
            // The GRP Parameter actually contains the Entity Parameter too.
            // Split the values and create 2 parameters
            var parm = parameters.FirstOrDefault(p => p.Name == "GRP");
            if (parm != null)
            {
                var parms = parm.Value.Split('_');
                parameters.Remove(parm);
                parameters.Add(new ParameterValueDto { Name = "ENT", Value = parms[0] });
                parameters.Add(new ParameterValueDto { Name = "GRP", Value = parms[1] });

                reportParameters.Add(new ReportParameterDto { ParameterName = "ENT", ParameterType = ReportParameterType.Entity, IsRequired = true, AllowMultiple = false });
            }
        }

        private void AddEntityBreadcrumbs(List<ReportParameterDto> reportParameters, List<ParameterValueDto> parameters)
        {
            // The RAAM Parameter actually contains Nodes 
            // for the Entity and BreadCrumbs 
            // format: "<RAAM>
            //            <Entity EntityID="1" BreadCrumb="/WFS" />
            //            <Entity EntityID="2" BreadCrumb="/WFS/COMPANY1" />
            //			  <Entity EntityID="3" BreadCrumb="/WFS/COMPANY1/SUBCOMPANY2" />
            //		   </RAAM>"
            var parm = reportParameters.FirstOrDefault(p => p.ParameterName == "RAAM");
            var entity = parameters.FirstOrDefault(p => p.Name == "ENT");
            var user = parameters.FirstOrDefault(p => p.Name == "USER");

            RaamClient client = new RaamClient(LogManager.Logger);
            var ebc = new List<EntityBreadcrumbDTO>();
            var success = false;

            if (parm != null && entity != null)
            {
                success = client.GetEntityBreadcrumbList(Convert.ToInt32(entity.Value), ebc, true);
            }
            else if (parm != null && user != null)
            {
                var userSid = ReportDAL.GetSIDByUserID(Convert.ToInt32(user.Value));
                var raamUser = client.GetUserByUserSID(userSid);
				if (raamUser != null)
					success = client.GetEntityBreadcrumbList(raamUser.EntityId, ebc, true);
            }
            else if (parm != null)
            {
                var entityClaim = ClaimsPrincipal.Current.FindFirst(WfsClaimTypes.EntityId);
                success = client.GetEntityBreadcrumbList(Convert.ToInt32(entityClaim.Value), ebc, true);
            }

            if (success)
            {
                var sb = new StringBuilder();
                foreach (var e in ebc)
                {
                    sb.Append(e.ToXML());
                }
                var newParam = new ParameterValueDto { Name = parm.ParameterName, Value = sb.ToString() };
                parameters.Add(newParam);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected HubReportDAL ReportDAL
        {
            get
            {
                if (_ReportDAL == null)
                {
                    if (!string.IsNullOrEmpty(SiteKey))
                    {
                        _ReportDAL = new HubReportDAL(SiteKey);
                    }
                }
                return (_ReportDAL);
            }
        }

        protected override ActivityCodes RequestType
        {
            get
            {
                return ActivityCodes.acHubReportWebServiceCall;
            }
        }

    }
}

