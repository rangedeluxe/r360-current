﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.R360Shared;

namespace HubReportCommon.Responses
{
    public class AuditEventTypeResponse : BaseResponse
    {
       public List<string> EventTypes { get; set; }
    }
}
