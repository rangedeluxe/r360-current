﻿using System.Collections.Generic;
using WFS.RecHub.R360Shared;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Eric Smasal
* Date: 06/11/2013
*
* Purpose: Audit Payment Source Response Class
*
* Modification History
*
*********************************************************************************/
namespace WFS.RecHub.HubReport.Common
{
    public class AuditPaymentSourceResponse : BaseResponse
    {
        public AuditPaymentSourceResponse()
            : base()
        {
        }

        public List<AuditPaymentSourceDto> AuditPaymentSources { get; set; }
    }
}
