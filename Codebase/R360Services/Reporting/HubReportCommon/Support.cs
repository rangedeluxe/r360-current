﻿using System.Runtime.Serialization;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:  
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New File
******************************************************************************/
namespace WFS.RecHub.HubReport.Common
{

	[DataContract]
	public enum ReportStatus
	{
		[EnumMember]
		None,

		[EnumMember]
		InProgress,

		[EnumMember]
		Completed,

		[EnumMember]
		Failed,

		[EnumMember]
		Loaded,
	}


	[DataContract]
	public enum ReportParameterType
	{
		[EnumMember]
		None = 0,

		[EnumMember]
		BeginDate = 1,	// For Single Date, just use "BeginDate"; the UI will have to interpret the label based on the presence / absense of the EndDate parameter...

		[EnumMember]
		EndDate = 2,

		[EnumMember]
		GroupBy = 3,

		[EnumMember]
		SortBy = 4,

		[EnumMember]
		SortDir = 5,

		[EnumMember]
		Entity = 6,

		[EnumMember]
		Batch = 7,

		[EnumMember]
		TransactionId = 8,

		[EnumMember]
		ItemId = 9,

		[EnumMember]
		User = 10, //R360 Users (for RAAM users see RAAMUser)

		[EnumMember]
		PaymentSource = 11,

		[EnumMember]
		ExternalWGRefID = 12,

        [EnumMember]
        ReportGenerationType = 14,

        [EnumMember]
        EventType = 15,

        [EnumMember]
        Group = 16,

        [EnumMember]
        RAAM = 17,

        [EnumMember]
        RAAMEventType = 18, // This is the Audit Event Type comming from RAAM

        [EnumMember]
        ShowReceivablesChart = 19,

        [EnumMember]
        ShowExceptionsChart = 20,

		[EnumMember]
		RAAMUser = 21 //RAAM Users (For R360 Users see User)

		// TODO: Other standard parameters...
	}
}