﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using HubReportCommon.Responses;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.HubReport.Common
{
  [ServiceContract(Namespace = "urn:wausaufs.com:services:HubReportService")]
  public interface IHubReportService
  {
    // Service Management
    [OperationContract]
    PingResponse Ping();

    // Reporting Interface
    [OperationContract]
    ReportGroupsResponse GetReportGroups();

    [OperationContract]
    ReportConfigurationsResponse GetReportsForGroups();

    [OperationContract]
    ReportConfigurationResponse GetReportConfigurationById(int id);

    [OperationContract]
    ReportConfigurationResponse GetReportConfigurationByName(string reportName);

    [OperationContract]
    ReportParametersResponse GetParametersForReport(int reportID);

    #region Parameter Selection Values

    [OperationContract]
    AuditPaymentSourceResponse GetPaymentSourceList();

    [OperationContract]
    AuditEventTypeResponse GetEventTypeList();

    #endregion

	[OperationContract]
	InvokeReportResponse InvokeReportAsync(string ReportName, List<ParameterValueDto> parameters);

	[OperationContract]
	InvokeReportResponse InvokeReportByIdAsync(int reportId, List<ParameterValueDto> parameters);

	[OperationContract]
    ReportInstanceResponse GetReportInstance(Guid instanceId);

    [OperationContract]
    ByteArrayResponse GetReportPDF(Guid instanceId);
  }

  //**************************************
  // Common Data Contracts
  //**************************************
  [DataContract]
  public class ServerFaultException
  {
    [DataMember]
    public string errorcode;

    [DataMember]
    public string message;

    [DataMember]
    public string details;
  }

}
