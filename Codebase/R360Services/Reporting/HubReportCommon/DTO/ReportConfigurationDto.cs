﻿using System;
using System.Data;
using System.Runtime.Serialization;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Common Report Configuration data transfer object
*  Filename: ReportConfigurationDto.cs
*    Author: Kyle Colden
*
* Revisions:
* ----------------------
******************************************************************************/
namespace WFS.RecHub.HubReport.Common
{
    [DataContract]
    public class ReportConfigurationDto
    {
        [DataMember(IsRequired = true)]
        public int ReportID;

        [DataMember(IsRequired = true)]
        public int GroupID;

        [DataMember(IsRequired = true)]
        public int OrderValue;

        [DataMember(IsRequired = true)]
        public string RaamResourceName;

        [DataMember(IsRequired = true)]
        public string ReportName;

        [DataMember(IsRequired = true)]
        public string XmlParameterName;

        [DataMember(IsRequired = true)]
        public string ReportFileName;
	}
}
