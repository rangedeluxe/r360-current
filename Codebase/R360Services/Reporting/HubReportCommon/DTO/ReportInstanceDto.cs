﻿using System;
using System.Data;
using System.Runtime.Serialization;
using System.Xml;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Common Report Instance data transfer object
*  Filename: ReportInstanceDto.cs
*    Author: Kyle Colden
*
* Revisions:
* ----------------------
******************************************************************************/
namespace WFS.RecHub.HubReport.Common
{
    [DataContract]
    public class ReportInstanceDto
    {
        [DataMember(IsRequired = true)]
        public long ReportInstanceKey;

        [DataMember(IsRequired = true)]
        public Guid InstanceID;

        [DataMember(IsRequired = true)]
        public int UserID;

        [DataMember(IsRequired = true)]
        public int ReportID;

        [DataMember(IsRequired = true)]
        public ReportStatus Status;

        [DataMember(IsRequired = true)]
        public DateTime StartTime;

        [DataMember(IsRequired = true)]
        public DateTime EndTime;
	}
}
