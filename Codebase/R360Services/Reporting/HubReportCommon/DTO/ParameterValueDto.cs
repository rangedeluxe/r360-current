﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*    Module: Common Parameter Value data transfer object
*  Filename: ParameterValueDto.cs
*    Author: Kyle Colden
*
* Revisions:
* ----------------------
******************************************************************************/
namespace WFS.RecHub.HubReport.Common
{
    [DataContract]
    public class ParameterValueDto
    {
        [DataMember(IsRequired = true)]
        public string Name;

        [DataMember(IsRequired = false)]
        public string Value;

        [DataMember(IsRequired = false)]
		public List<string> ValueList;
    }
}
