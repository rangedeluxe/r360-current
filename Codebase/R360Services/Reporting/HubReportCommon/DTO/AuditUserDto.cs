﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;

namespace WFS.RecHub.HubReport.Common
{
  [DataContract]
  public class AuditUserDto
  {
    [DataMember(IsRequired = true)]
    public int UserID;

    [DataMember(IsRequired = true)]
    public Guid SID;
  }
}
