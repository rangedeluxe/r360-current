﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.ReportServicesClient;

namespace HubReportServiceTestClient
{
	class MainViewModel : ViewModelBase
	{
		private ReportServiceManager _mgr;
		private ReportServiceManager Mgr
		{
			get
			{
				if (_mgr == null)
				{
					_mgr = new ReportServiceManager();
				}
				return _mgr;
			}
		}

		public MainViewModel()
		{
			_cmdPing = new SimpleCommand(Ping);

			InitLog();
		}

		#region Ping

		private async void Ping()
		{
			// Set UI Label
			PingText = "Pinging...";
			ErrorText = string.Empty;

			try
			{
				var mgr = Mgr;

				// Perform call asynchronously
				PingResponse response = await Task.Run<PingResponse>(() => mgr.Ping());

				// Display result
				string text = response.Status.ToString() + ": ";
				if (response.Errors != null && response.Errors.Count > 0)
					text += response.Errors.Aggregate((o1, o2) => o1 + "\r\n" + o2);
				else
					text += response.responseString;
				PingText = text;
			}
			catch (Exception ex)
			{
				// Display any error
				PingText = "(Failed)";
				ErrorText = ex.ToString();
			}

			// After any action, check for log updates
			CheckLog();
		}

		private readonly SimpleCommand _cmdPing;
		public ICommand CmdPing
		{
			get { return _cmdPing; }
		}

		private string _pingText = "(Not Run)";
		public string PingText
		{
			get { return _pingText; }
			set
			{
				_pingText = string.Format("{0}: {1}", DateTime.Now.ToString("HH:mm:ss"), value);
				OnPropertyChanged();
			}
		}

		#endregion

		#region Generic Error Display

		private string _errorText;
		public string ErrorText
		{
			get { return _errorText; }
			set
			{
				_errorText = value;
				OnPropertyChanged();
			}
		}

		#endregion

		#region Log File Monitoring

		private string _logText;
		public string LogText
		{
			get { return _logText; }
			set
			{
				_logText = value;
				OnPropertyChanged();
			}
		}

		// Read IPOnline.ini path from appSettings, and read log name from IPONline.ini...
		private string _logPath = null;
		private long _startLogOffset = 0;
		private void InitLog()
		{
			try
			{
				// Detect log path
				string localIniPath = ConfigurationManager.AppSettings["localIni"];
				if (string.IsNullOrEmpty(localIniPath))
				{
					LogText = "(INI File Not Configured)";
				}
				else
				{
					string siteKey = "[" + ConfigurationManager.AppSettings["siteKey"] + "]";
					string[] iniSettings = File.ReadAllLines(localIniPath);
					int sectionIndex = Array.IndexOf(iniSettings, siteKey);
					if (sectionIndex < 0) sectionIndex = 0;
					string logFilePathSetting = iniSettings.Skip(sectionIndex).FirstOrDefault(o => o.StartsWith("LogFile="));
					if (logFilePathSetting != null)
						_logPath = logFilePathSetting.Split(new char[] { '=' }, 2)[1];

					if (string.IsNullOrEmpty(_logPath))
					{
						LogText = "(Log File Not Configured)";
					}
					else if (File.Exists(_logPath))
					{
						LogText = "(Log File Found)";
						using (var f = File.OpenRead(_logPath))
						{
							_startLogOffset = f.Length;
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogText = ex.ToString();
			}
		}

		private void CheckLog()
		{
			try
			{
				if (string.IsNullOrEmpty(_logPath))
				{
					LogText = "(Log File Not Configured)";
				}
				else if (File.Exists(_logPath))
				{
					using (var f = File.OpenRead(_logPath))
					{
						f.Seek(_startLogOffset, SeekOrigin.Begin);
						StreamReader sr = new StreamReader(f);
						LogText = sr.ReadToEnd();
					}
				}
				else
				{
					LogText = "(Log File Not Found: " + _logPath + ")";
				}
			}
			catch (Exception ex)
			{
				LogText = ex.ToString();
			}
		}

		#endregion

	}
}
