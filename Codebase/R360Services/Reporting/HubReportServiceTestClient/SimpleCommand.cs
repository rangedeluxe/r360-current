﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace HubReportServiceTestClient
{
	internal class SimpleCommand : ICommand
	{
		private readonly Action _cmdAction;
		public SimpleCommand(Action cmdAction)
		{
			_cmdAction = cmdAction;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object ignored)
		{
			_cmdAction();
		}

#pragma warning disable 0067 // This event is part of the ICommand interface, but SimpleCommand will never raise it so suppress the warning about "not used"
		public event EventHandler CanExecuteChanged;
#pragma warning restore 0067 // Restore normal warning processing
	}

	internal class SimpleCommand<T> : ICommand
	{
		private readonly Action<T> _cmdAction;
		public SimpleCommand(Action<T> cmdAction)
		{
			_cmdAction = cmdAction;
		}

		public bool CanExecute(object parameter)
		{
			return true;
		}

		public void Execute(object parameter)
		{
			T p = (T)Convert.ChangeType(parameter, typeof(T));
			_cmdAction(p);
		}

#pragma warning disable 0067 // This event is part of the ICommand interface, but SimpleCommand will never raise it so suppress the warning about "not used"
		public event EventHandler CanExecuteChanged;
#pragma warning restore 0067 // Restore normal warning processing
	}
}
