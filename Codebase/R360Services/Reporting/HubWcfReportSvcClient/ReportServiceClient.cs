﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.ServiceModel;
using HubReportCommon.Responses;
using WFS.LTA.Common;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.ReportServicesClient
{
    public class ReportServiceClient : _ServicesClientBase, IHubReportService
	{
		#region Construction

        private readonly IHubReportService _ReportService = null;

        public ReportServiceClient(string vSiteKey, ltaLog log)
            : base(vSiteKey, log)
        {
			if (LogManager.IsDefault) LogManager.Logger = log.LTAtoILogger(this.GetType().Name);
			_ReportService = R360ServiceFactory.Create<IHubReportService>();
        }

        #endregion

        private T Do<T>(Func<T> operation, [CallerMemberName] string methodName = null)
        {
            // All Service calls must be wrapped in an Operation Context, and log exceptions.  This wrapper method does that.
            try
            {
                using (new OperationContextScope((IContextChannel)_ReportService))
                {
                    // Set context inside of operation scope
                    R360ServiceContext.Init(this.SiteKey);

                    return operation();
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, methodName);
                throw;
            }
        }


		#region Service Verfication

		public PingResponse Ping()
		{
            return Do(_ReportService.Ping);
		}

		#endregion

		#region Report Definitions

        public ReportGroupsResponse GetReportGroups()
        {
            return Do(_ReportService.GetReportGroups);
        }

        public ReportConfigurationsResponse GetReportsForGroups()
		{
            return Do(_ReportService.GetReportsForGroups);
		}

        public ReportConfigurationResponse GetReportConfigurationById(int reportId)
        {
            return Do(() => _ReportService.GetReportConfigurationById(reportId));
        }

        public ReportConfigurationResponse GetReportConfigurationByName(string reportName)
        {
            return Do(() => _ReportService.GetReportConfigurationByName(reportName));
        }

        public ReportParametersResponse GetParametersForReport(int reportID)
        {
            return Do(() => _ReportService.GetParametersForReport(reportID));
        }

		#endregion

        #region Parameter Selection Values

        public AuditPaymentSourceResponse GetPaymentSourceList()
        {
            return Do(_ReportService.GetPaymentSourceList);
        }

        public AuditEventTypeResponse GetEventTypeList()
        {
            return Do(_ReportService.GetEventTypeList);
        }


        #endregion

        #region Report Generation
        public InvokeReportResponse InvokeReportAsync(string reportName, List<ParameterValueDto> parameters)
        {
            return Do(() => _ReportService.InvokeReportAsync(reportName, parameters));
        }

		public InvokeReportResponse InvokeReportByIdAsync(int reportID, List<ParameterValueDto> parameters)
		{
			return Do(() => _ReportService.InvokeReportByIdAsync(reportID, parameters));
		}

        public ReportInstanceResponse GetReportInstance(Guid instanceId)
        {
            return Do(() => _ReportService.GetReportInstance(instanceId));
        }

        public ByteArrayResponse GetReportPDF(Guid instanceId)
        {
            return Do(() => _ReportService.GetReportPDF(instanceId));
        }
        #endregion
    }
}
