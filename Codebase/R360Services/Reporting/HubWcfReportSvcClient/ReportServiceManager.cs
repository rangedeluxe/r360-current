﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HubReportCommon.Responses;
using WFS.LTA.Common;
using WFS.RecHub.Common;
using WFS.RecHub.HubReport.Common;
using WFS.RecHub.R360Shared;

namespace WFS.RecHub.ReportServicesClient
{
    public class ReportServiceManager : IHubReportService
    {
        #region Construction / Initialization

        // Initialization parameters
		private const string LogSource = "ReportServiceManager";
		private static LTA.Common.ltaLog _eventLog;
        private static string _siteKey;
		private static cSiteOptions _SiteOptions = null;
        private readonly ReportServiceClient _client;

		/// <summary>
		/// Uses SiteKey to retrieve site options from the local .ini file.
		/// </summary>
		protected static cSiteOptions SiteOptions
		{
			get
			{
				if (_SiteOptions == null)
				{
					_SiteOptions = new cSiteOptions(_siteKey);
				}
				return _SiteOptions;
			}
		}

		/// <summary>
		/// Logging component using settings from the local siteOptions object.
		/// </summary>
		protected static ltaLog EventLog
		{
			get
			{
				if (_eventLog == null)
				{
					_eventLog = new ltaLog(SiteOptions.logFilePath,
											  SiteOptions.logFileMaxSize,
											  (LTAMessageImportance)SiteOptions.loggingDepth);
				}

				return _eventLog;
			}
		}

        static ReportServiceManager()
        {
			// Make sure the site key is initialized before any other properties are used
			GetSiteKey();
        }

        public ReportServiceManager()
        {
            // Create connection context
            try
            {
				_client = new ReportServiceClient(_siteKey, EventLog);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, LogSource, ".ctor");

                // Consructors should not throw exceptions -- just initialize to null and report errors later...
                _client = null;
            }
        }

		// Generic app setting access
		private static string GetAppSetting(string settingName, string defaultValue)
		{
			try
			{
				string value = ConfigurationManager.AppSettings[settingName];
				if (string.IsNullOrEmpty(value))
					value = defaultValue;

				if (value != defaultValue)
					EventLog.logEvent(string.Format("Using '{0}' = '{1}'", settingName, value), LogSource, LTA.Common.LTAMessageImportance.Verbose);

				return value;
			}
			catch (Exception ex)
			{
				EventLog.logWarning(string.Format("Error reading configuration setting {0} - {1}", settingName, ex.Message), LogSource, LTA.Common.LTAMessageImportance.Verbose);
				return defaultValue;
			}
		}

		// Specialied method for site key that properly initializes the EventLog after the site key has been set
		private static void GetSiteKey()
		{
			string defaultValue = "IPOnline";
			try
			{
				string value = ConfigurationManager.AppSettings["siteKey"];
				if (string.IsNullOrEmpty(value))
					value = defaultValue;

				_siteKey = value;

				EventLog.logEvent(string.Format("Using SiteKey = '{0}'", value), LogSource, LTA.Common.LTAMessageImportance.Verbose);
			}
			catch (Exception ex)
			{
				_siteKey = defaultValue;

				EventLog.logWarning(string.Format("Error reading configuration setting 'siteKey' - {0}", ex.Message), LogSource, LTA.Common.LTAMessageImportance.Verbose);
			}
		}

        private void ValidateConnection()
        {
            if (_client == null)
                throw new Exception("Configuration Error - check log for details");
        }

        #endregion

        #region Service Verfication

        public PingResponse Ping()
        {
			ValidateConnection();
            return _client.Ping();
        }

        #endregion

        #region Report Definitions

        public ReportGroupsResponse GetReportGroups()
        {
			ValidateConnection();
			return _client.GetReportGroups();
        }

        public ReportConfigurationsResponse GetReportsForGroups()
        {
			ValidateConnection();
			return _client.GetReportsForGroups();
        }

        public ReportConfigurationResponse GetReportConfigurationById(int reportId)
        {
			ValidateConnection();
			return _client.GetReportConfigurationById(reportId);
        }

        public ReportConfigurationResponse GetReportConfigurationByName(string reportName)
        {
			ValidateConnection();
			return _client.GetReportConfigurationByName(reportName);
        }

        public ReportParametersResponse GetParametersForReport(int reportID)
        {
			ValidateConnection();
			return _client.GetParametersForReport(reportID);
        }

        #endregion

        #region Parameter Selection Values

        public AuditPaymentSourceResponse GetPaymentSourceList()
        {
            return _client.GetPaymentSourceList();
        }

        public AuditEventTypeResponse GetEventTypeList()
        {
            return _client.GetEventTypeList();
        }

        #endregion

        #region Report Generation
        public InvokeReportResponse InvokeReportAsync(string reportName, List<ParameterValueDto> parameters)
        {
			ValidateConnection();
			return _client.InvokeReportAsync(reportName, parameters);
        }

		public InvokeReportResponse InvokeReportByIdAsync(int reportID, List<ParameterValueDto> parameters)
		{
			ValidateConnection();
			return _client.InvokeReportByIdAsync(reportID, parameters);
		}

        public ReportInstanceResponse GetReportInstance(Guid instanceId)
        {
			ValidateConnection();
			return _client.GetReportInstance(instanceId);
        }

        public ByteArrayResponse GetReportPDF(Guid instanceId)
        {
			ValidateConnection();
			return _client.GetReportPDF(instanceId);
        }
        #endregion
    }
}
