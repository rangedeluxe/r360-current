﻿# This script is intended to be run from the web tier.

$ErrorActionPreference = 'Stop'
$AppServer = 'R36016QaApp01'
$ClientServer = 'R36016QaClnt01'
$FileServerIis = 'R36016QaFile01'
$FileServerNoIis = 'R36016QaNoIis1'

function Get-ServerConfigFiles {
    param (
        [string] $Server,
        [string] $PathRelativeToRecHub
    )
    $RecHubPath = "\\$Server.qalabs.nwk\D$\WFSApps\RecHub"
    $Path = Join-Path $RecHubPath $PathRelativeToRecHub

    $RecHubPathRegex = "^" + [System.Text.RegularExpressions.Regex]::Escape($RecHubPath) + "\\?"

    Get-Item $Path | ForEach-Object {
        $Path = $_.FullName
        [PSCustomObject] @{
            Server = $Server
            RelativePath = $Path -replace $RecHubPathRegex, ""
            FullPath = $Path
        }
    }
}

function Get-ConfigFiles {
    Get-ServerConfigFiles $AppServer "bin2\WCFClients.app.config"
    Get-ServerConfigFiles $ClientServer "bin\*ImportClient*.exe.config"
}

function Get-FileTierConfiguration {
    Get-ConfigFiles | ForEach-Object {
        $Content = Get-Content $_.FullPath -Raw
        $IsIis = $Content -match "\b$FileServerIis\b"
        $IsNoIis = $Content -match "\b$FileServerNoIis\b"
        if ($IsIis -and $IsNoIis) {
            throw "File '$($_.FullPath)' refers to both the IIS server ($FileServerIis) and the NoIIS server ($FileServerNoIis)"
        }
        if (!$IsIis -and !$IsNoIis) {
            throw "File '$($_.FullPath)' does not refer to any recognized file server"
        }

        [PSCustomObject] @{
            Server = $_.Server
            RelativePath = $_.RelativePath
            Configuration = if ($IsIis) {
                "IIS"
            } else {
                "NoIIS"
            }
        }
    }
}

function Set-FileTierConfiguration {
    param (
        [ValidateSet("IIS", "NoIIS")] [string] $NewConfiguration
    )

    $SourceServer = if ($NewConfiguration -eq "IIS") { $FileServerNoIis } else { $FileServerIis }
    $TargetServer = if ($NewConfiguration -eq "IIS") { $FileServerIis } else { $FileServerNoIis }

    $BackupDirectory = Join-Path $PSScriptRoot "Backup"
    if (!(Test-Path $BackupDirectory)) {
        New-Item $BackupDirectory -ItemType Directory | Out-Null
    }
    Write-Host "Backing up config files to $BackupDirectory"

    Get-ConfigFiles | ForEach-Object {
        $Path = $_.FullPath
        $BackupPath = Join-Path $BackupDirectory (Split-Path -Leaf $Path)
        Copy-Item $Path $BackupPath -Force

        $SourceContent = Get-Content $Path -Raw
        $TargetContent = $SourceContent -replace "\b$SourceServer\b", $TargetServer
        Set-Content -Path $Path -Value $TargetContent -Encoding Utf8
        [PSCustomObject] @{
            Server = $_.Server
            RelativePath = $_.RelativePath
            Action = "Changed to $NewConfiguration"
        }
    }
}

function Get-ServicesToRestart {
    Get-Service -Name "w3svc" -ComputerName $AppServer
    Get-Service -DisplayName "WFS * Import Client Service" -ComputerName $ClientServer
}

function Restart-ServiceWithStatus {
    param (
        $Service
    )
    process {
        Write-Host "Restarting '$($_.DisplayName)' service on $($_.MachineName)..."
        Restart-Service $_
        Write-Host "Restarted successfully."
    }
}

Get-FileTierConfiguration | Out-Default

Write-Host "Type 'IIS' or 'NoIIS' to change configuration, or just press ENTER to exit."
$NewConfiguration = Read-Host
if ($NewConfiguration) {
    Set-FileTierConfiguration $NewConfiguration | Out-Default
    Get-ServicesToRestart | Restart-ServiceWithStatus

    Write-Host
    Write-Host "Success! Press ENTER to exit."
    Read-Host
}
